package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Map;

public class DiscrepancyMemberDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer hixEnrollmentId;
	private Map<String, Object> relationshipWithSubscriber;
	private Map<String, Object> exchgIndivIdentifier;
	private Map<String, Object> firstName;
	private Map<String, Object> middleName;
	private Map<String, Object> lastName;
	private Map<String, Object> memberFullName;
	private Map<String, Object> gender;
	private Map<String, Object> SSN; //Requires masking
	private Map<String, Object> memberEffectiveStartDate;
	private Map<String, Object> memberEffectiveEndDate;
	private Map<String, Object> memberDateOfBirth;
	private Map<String, Object> tobaccoUsage;
	
	
	public Integer getHixEnrollmentId() {
		return hixEnrollmentId;
	}
	public void setHixEnrollmentId(Integer hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}
	public Map<String, Object> getRelationshipWithSubscriber() {
		return relationshipWithSubscriber;
	}
	public void setRelationshipWithSubscriber(Map<String, Object> relationshipWithSubscriber) {
		this.relationshipWithSubscriber = relationshipWithSubscriber;
	}
	public Map<String, Object> getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(Map<String, Object> exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public Map<String, Object> getFirstName() {
		return firstName;
	}
	public void setFirstName(Map<String, Object> firstName) {
		this.firstName = firstName;
	}
	public Map<String, Object> getMiddleName() {
		return middleName;
	}
	public void setMiddleName(Map<String, Object> middleName) {
		this.middleName = middleName;
	}
	public Map<String, Object> getLastName() {
		return lastName;
	}
	public void setLastName(Map<String, Object> lastName) {
		this.lastName = lastName;
	}
	public Map<String, Object> getMemberFullName() {
		return memberFullName;
	}
	public void setMemberFullName(Map<String, Object> memberFullName) {
		this.memberFullName = memberFullName;
	}
	public Map<String, Object> getGender() {
		return gender;
	}
	public void setGender(Map<String, Object> gender) {
		this.gender = gender;
	}
	public Map<String, Object> getSSN() {
		return SSN;
	}
	public void setSSN(Map<String, Object> sSN) {
		SSN = sSN;
	}
	public Map<String, Object> getMemberEffectiveStartDate() {
		return memberEffectiveStartDate;
	}
	public void setMemberEffectiveStartDate(Map<String, Object> memberEffectiveStartDate) {
		this.memberEffectiveStartDate = memberEffectiveStartDate;
	}
	public Map<String, Object> getMemberEffectiveEndDate() {
		return memberEffectiveEndDate;
	}
	public void setMemberEffectiveEndDate(Map<String, Object> memberEffectiveEndDate) {
		this.memberEffectiveEndDate = memberEffectiveEndDate;
	}
	public Map<String, Object> getMemberDateOfBirth() {
		return memberDateOfBirth;
	}
	public void setMemberDateOfBirth(Map<String, Object> memberDateOfBirth) {
		this.memberDateOfBirth = memberDateOfBirth;
	}
	public Map<String, Object> getTobaccoUsage() {
		return tobaccoUsage;
	}
	public void setTobaccoUsage(Map<String, Object> tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}
}
