package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name="ACCOUNT_TRANSFER_MIGRATION")
public class AccountTransferMigration implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum StatusType {
		APPLICATION_CREATED("APPLICATION_CREATED"),
		CMR_LINKED("HOUSEHOLD_LINKED"),
		ENROLLMENT_LINKED("ENROLLMENT_LINKED");
		
		String value = null;
		
		StatusType(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	@Id
	@SequenceGenerator(name="AT_MIGRATION_ID_GENERATOR", sequenceName="AT_MIGRATION_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AT_MIGRATION_ID_GENERATOR")
	private Long id;
	
	@Column(name="TRANSFER_ID")
	private String transferId;

	@Column(name="ENROLLMENT_ID")
	private Long enrollmentId;

	@Column(name="HOUSEHOLD_CASE_ID")
	private String householdCaseId;

	@Column(name="CMR_HOUSEHOLD_ID")
	private Long cmrHouseholdId;
	
	@Column(name="NEW_SSAP_APPLICATION_ID")
	private Long newSsapApplicationId;
	
	@Column(name="OLD_SSAP_APPLICATION_ID")
	private Long oldSsapApplicationId;
	
	@Column(name="LAST_UPDATE_DATE")
	private Timestamp lastUpdateTimestamp;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="GI_MONITOR_ID")
	private Integer giMonitorId;
	
	@Column(name="ACTIVITY_DATE")
	private Timestamp activityDate;
	
	@Column(name="CREATION_DATE")
	private Timestamp creationDate;

	public String getTransferId() {
		return transferId;
	}

	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}

	public Long getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	public Long getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Long cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Long getNewSsapApplicationId() {
		return newSsapApplicationId;
	}

	public void setNewSsapApplicationId(Long newSsapApplicationId) {
		this.newSsapApplicationId = newSsapApplicationId;
	}

	public Long getOldSsapApplicationId() {
		return oldSsapApplicationId;
	}

	public void setOldSsapApplicationId(Long oldSsapApplicationId) {
		this.oldSsapApplicationId = oldSsapApplicationId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getGiMonitorId() {
		return giMonitorId;
	}

	public void setGiMonitorId(Integer giMonitorId) {
		this.giMonitorId = giMonitorId;
	}

	public Timestamp getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Timestamp activityDate) {
		this.activityDate = activityDate;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

}
