package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlanTenantRequestDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<PlanTenantDTO> planTenantList = new ArrayList<PlanTenantDTO>();
	List<Integer> planIdList = new ArrayList<Integer>();
	String selectedTenants;
	
	
	public PlanTenantRequestDTO(){
		
	}


	public List<PlanTenantDTO> getPlanTenantList() {
		return planTenantList;
	}


	public void setPlanTenantList(List<PlanTenantDTO> planTenantList) {
		this.planTenantList = planTenantList;
	}


	public String getSelectedTenants() {
		return selectedTenants;
	}


	public void setSelectedTenants(String selectedTenants) {
		this.selectedTenants = selectedTenants;
	}


	public List<Integer> getPlanIdList() {
		return planIdList;
	}


	public void setPlanIdList(List<Integer> planIdList) {
		this.planIdList = planIdList;
	}
	
	

}
