package com.getinsured.hix.dto.cap;

public class CapEmployerContribution implements Comparable<CapEmployerContribution> {

	private String relationToPrimary;
	private String hraContriDisplayType;
	private String hraContriAmount;
	private int displayOrder;
	
	
	public enum HraContributionTypeEnum{
		NO_HRA_CONTRIBUTION("NA"),
		FAMILY_LUMPSUM_DEPOSIT("Lump Sum"),
		FAMILY_ANNUAL_FUNDING("year"),
		FAMILY_QUARTERLY_FUNDING("quarter"),
		FAMILY_MONTHLY_FUNDING("month"),
		INDIVIDUAL_LUMPSUM_DEPOSIT("Lump Sum"),
		INDIVIDUAL_ANNUAL_FUNDING("year"),
		INDIVIDUAL_QUARTERLY_FUNDING("quarter"),
		INDIVIDUAL_MONTHLY_FUNDING("month");

		
		HraContributionTypeEnum(String hraContriType){
				this.hraContriType=hraContriType;
		}
		String hraContriType;
		
		@Override
	    public String toString() {
	        return hraContriType;
	    }
	};
	
	
	
	public String getRelationToPrimary() {
		return relationToPrimary;
	}
	public void setRelationToPrimary(String memberType) {
		this.relationToPrimary = memberType;
	}
	
	public String getHraContriAmount() {
		return hraContriAmount;
	}
	public void setHraContriAmount(String hraContriAmount) {
		this.hraContriAmount = hraContriAmount;
	}
	
	public String getHraContriDisplayType() {
		return hraContriDisplayType;
	}
	public void setHraContriDisplayType(String hraContriDisplayType) {
		this.hraContriDisplayType = hraContriDisplayType;
	}
	
	public int getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	@Override
	public int compareTo(CapEmployerContribution capEmpContri) {
		return (this.displayOrder - capEmpContri.displayOrder);
	}
	
}
