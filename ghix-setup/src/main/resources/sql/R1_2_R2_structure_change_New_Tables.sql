DROP TABLE BATCH_JOB_EXECUTION CASCADE CONSTRAINTS
;
DROP TABLE BATCH_JOB_EXECUTION_CONTEXT CASCADE CONSTRAINTS
;
DROP TABLE BATCH_JOB_INSTANCE CASCADE CONSTRAINTS
;
DROP TABLE BATCH_JOB_PARAMS CASCADE CONSTRAINTS
;
DROP TABLE BATCH_STEP_EXECUTION CASCADE CONSTRAINTS
;
DROP TABLE BATCH_STEP_EXECUTION_CONTEXT CASCADE CONSTRAINTS
;
DROP TABLE CONTENT CASCADE CONSTRAINTS
;
DROP TABLE COST_SHARE CASCADE CONSTRAINTS
;
DROP TABLE CREDIT_CARD_INFO CASCADE CONSTRAINTS
;
DROP TABLE DCS_TIER_TYPE CASCADE CONSTRAINTS
;
DROP TABLE DCS_TYPE CASCADE CONSTRAINTS
;
DROP TABLE DRUG_CODE_TIER_CODE_TYPE CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_ENROLLMENT_ITEMS CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_ENROLLMENTS CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_INVOICES CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_INVOICES_LINEITEMS CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_NOTIFICATION CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_PAYMENT_INVOICE CASCADE CONSTRAINTS
;
DROP TABLE EMPLOYER_PAYMENTS CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE_AUD CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE_RACE CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE_RACE_AUD CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE_RELATIONSHIP CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE_RELATIONSHIP_AUD CASCADE CONSTRAINTS
;
DROP TABLE ENROLLEE_UPDATE_SEND_STATUS CASCADE CONSTRAINTS
;
DROP TABLE ENROLLMENT CASCADE CONSTRAINTS
;
DROP TABLE ENROLLMENT_AUD CASCADE CONSTRAINTS
;
DROP TABLE ENROLLMENT_ESIGNATURE CASCADE CONSTRAINTS
;
DROP TABLE ENROLLMENT_EVENT CASCADE CONSTRAINTS
;
DROP TABLE ESIGNATURE CASCADE CONSTRAINTS
;
DROP TABLE ESIGNATURE_AUD CASCADE CONSTRAINTS
;
DROP TABLE EXTERNAL_EMPLOYER CASCADE CONSTRAINTS
;
DROP TABLE EXTERNAL_INDIVIDUAL CASCADE CONSTRAINTS
;
DROP TABLE FACILITY CASCADE CONSTRAINTS
;
DROP TABLE FACILITY_ADDRESS CASCADE CONSTRAINTS
;
DROP TABLE FACILITY_SPECIALITY_RELATION CASCADE CONSTRAINTS
;
DROP TABLE FORMULARY CASCADE CONSTRAINTS
;
DROP TABLE FORMULARY_COST_SHARE_BENEFIT CASCADE CONSTRAINTS
;
DROP TABLE I18NTEXT CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_ACCREDIATION CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_DOCUMENT CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_INVOICES CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_INVOICES_LINEITEMS CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_PAYMENT_DETAIL CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_PAYMENT_INVOICE CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_PAYMENTS CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_QUALITY_RATING CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_REPRESENTATIVE CASCADE CONSTRAINTS
;
DROP TABLE ISSUERS CASCADE CONSTRAINTS
;
DROP TABLE ISSUERS_AUD CASCADE CONSTRAINTS
;
DROP TABLE NCQA CASCADE CONSTRAINTS
;
DROP TABLE NETWORK CASCADE CONSTRAINTS
;
DROP TABLE NOTICE_TYPES CASCADE CONSTRAINTS
;
DROP TABLE NOTICE_TYPES_AUD CASCADE CONSTRAINTS
;
DROP TABLE NOTICES CASCADE CONSTRAINTS
;
DROP TABLE NOTIFICATION CASCADE CONSTRAINTS
;
DROP TABLE PHYSICIAN CASCADE CONSTRAINTS
;
DROP TABLE PHYSICIAN_ADDRESS CASCADE CONSTRAINTS
;
DROP TABLE PHYSICIAN_SPECIALITY_RELATION CASCADE CONSTRAINTS
;
DROP TABLE PLAN CASCADE CONSTRAINTS
;
DROP TABLE PLAN_AUD CASCADE CONSTRAINTS
;
DROP TABLE PLAN_DENTAL CASCADE CONSTRAINTS
;
DROP TABLE PLAN_DENTAL_AUD CASCADE CONSTRAINTS
;
DROP TABLE PLAN_DENTAL_BENEFIT CASCADE CONSTRAINTS
;
DROP TABLE PLAN_HEALTH CASCADE CONSTRAINTS
;
DROP TABLE PLAN_HEALTH_AUD CASCADE CONSTRAINTS
;
DROP TABLE PLAN_HEALTH_BENEFIT CASCADE CONSTRAINTS
;
DROP TABLE PLAN_QUALITY_RATING CASCADE CONSTRAINTS
;
DROP TABLE PLD_GROUP CASCADE CONSTRAINTS
;
DROP TABLE PLD_GROUP_PERSON CASCADE CONSTRAINTS
;
DROP TABLE PLD_HOUSEHOLD CASCADE CONSTRAINTS
;
DROP TABLE PLD_HOUSEHOLD_PERSON CASCADE CONSTRAINTS
;
DROP TABLE PLD_ORDER CASCADE CONSTRAINTS
;
DROP TABLE PLD_ORDER_ITEM CASCADE CONSTRAINTS
;
DROP TABLE PLD_PERSON CASCADE CONSTRAINTS
;
DROP TABLE PM_PLAN_RATE CASCADE CONSTRAINTS
;
DROP TABLE PM_RATING_AREA CASCADE CONSTRAINTS
;
DROP TABLE PM_SERVICE_AREA CASCADE CONSTRAINTS
;
DROP TABLE PM_ZIP_COUNTY_RATING_AREA CASCADE CONSTRAINTS
;
DROP TABLE PRESCREEN_DATA CASCADE CONSTRAINTS
;
DROP TABLE PROCESSINSTANCEINFO CASCADE CONSTRAINTS
;
DROP TABLE PROCESSINSTANCELOG CASCADE CONSTRAINTS
;
DROP TABLE PROVIDER CASCADE CONSTRAINTS
;
DROP TABLE PROVIDER_NETWORK CASCADE CONSTRAINTS
;
DROP TABLE PROVIDER_RATING CASCADE CONSTRAINTS
; 
DROP TABLE RATE_REVIEW_DATA CASCADE CONSTRAINTS
;
DROP TABLE REASSIGNMENT CASCADE CONSTRAINTS
;
DROP TABLE REGION CASCADE CONSTRAINTS
;
DROP TABLE SERFF_DOCUMENT CASCADE CONSTRAINTS
;
DROP TABLE SERFF_PLAN_MGMT CASCADE CONSTRAINTS
;
DROP TABLE SERFF_TEMPLATE_STATUS CASCADE CONSTRAINTS
;
DROP TABLE SESSIONINFO CASCADE CONSTRAINTS
;
DROP TABLE SPECIALTY CASCADE CONSTRAINTS
;
DROP TABLE TAX_YEAR CASCADE CONSTRAINTS
;
DROP TABLE URAC CASCADE CONSTRAINTS
;
--- Deprecated tables
DROP TABLE ISSUER_SERVICE_AREA CASCADE CONSTRAINTS
;
DROP TABLE ISSUER_TRANCSPARENCY_DATA  CASCADE CONSTRAINTS
;
-- 
-- TABLE: BATCH_JOB_EXECUTION 
--

CREATE TABLE BATCH_JOB_EXECUTION(
    JOB_EXECUTION_ID    NUMBER(19, 0)     NOT NULL,
    VERSION             NUMBER(19, 0),
    JOB_INSTANCE_ID     NUMBER(19, 0)     NOT NULL,
    CREATE_TIME         TIMESTAMP(6)      NOT NULL,
    START_TIME          TIMESTAMP(6),
    END_TIME            TIMESTAMP(6),
    STATUS              VARCHAR2(10),
    EXIT_CODE           VARCHAR2(100),
    EXIT_MESSAGE        VARCHAR2(2500),
    LAST_UPDATED        TIMESTAMP(6),
    CONSTRAINT PK_BATCH_JOB_EXEC PRIMARY KEY (JOB_EXECUTION_ID)
)
;



COMMENT ON TABLE BATCH_JOB_EXECUTION IS 'This table holds all information relevant to the JobExecution object. Every time a Job is run there will always be a new JobExecution, and a new row in this table'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.JOB_EXECUTION_ID IS 'Primary key that uniquely identifies this execution. The value of this column is obtainable by calling the getId method of the JobExecution object'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.VERSION IS 'This column specify an optimistic locking strategy when dealing with updates to the database'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.JOB_INSTANCE_ID IS 'Foreign key from the BATCH_JOB_INSTANCE table indicating the instance to which this execution belongs. There may be more than one execution per instance'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.CREATE_TIME IS 'Timestamp representing the time that the execution was created'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.START_TIME IS 'Timestamp representing the time the execution was started'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.END_TIME IS 'Timestamp representing the time the execution was finished, regardless of success or failure. An empty value in this column even though the job is not currently running indicates that there has been some type of error and the framework was unable to perform a last save before failing'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.STATUS IS 'Character string representing the status of the execution. This may be COMPLETED, STARTED, etc. The object representation of this column is the BatchStatus enumeration'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.EXIT_CODE IS 'Character string representing the exit code of the execution. In the case of a command line job, this may be converted into a number'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.EXIT_MESSAGE IS 'Character string representing a more detailed description of how the job exited. In the case of failure, this might include as much of the stack trace as is possible'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION.LAST_UPDATED IS 'Timestamp representing the last time this execution was persisted'
;
-- 
-- TABLE: BATCH_JOB_EXECUTION_CONTEXT 
--

CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT(
    JOB_EXECUTION_ID      NUMBER(19, 0)     NOT NULL,
    SHORT_CONTEXT         VARCHAR2(2500)    NOT NULL,
    SERIALIZED_CONTEXT    CLOB,
    CONSTRAINT PK_BATCH_JOB_EXEC_CONTEXT PRIMARY KEY (JOB_EXECUTION_ID)
)
;



COMMENT ON TABLE BATCH_JOB_EXECUTION_CONTEXT IS 'This table holds all information relevant to an Jobs ExecutionContext. There is exactly one Job ExecutionContext per JobExecution, and it contains all of the job-level data that is needed for a particular job execution. This data typically represents the state that must be retrieved after a failure so that a JobInstance can start from where it left off'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION_CONTEXT.JOB_EXECUTION_ID IS 'Foreign key representing the JobExecution to which the context belongs. There may be more than one row associated to a given execution'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION_CONTEXT.SHORT_CONTEXT IS 'A string version of the SERIALIZED_CONTEXT'
;
COMMENT ON COLUMN BATCH_JOB_EXECUTION_CONTEXT.SERIALIZED_CONTEXT IS 'The entire context, serialized'
;
-- 
-- TABLE: BATCH_JOB_INSTANCE 
--

CREATE TABLE BATCH_JOB_INSTANCE(
    JOB_INSTANCE_ID    NUMBER(19, 0)    NOT NULL,
    VERSION            NUMBER(19, 0),
    JOB_NAME           VARCHAR2(100)    NOT NULL,
    JOB_KEY            VARCHAR2(32)     NOT NULL,
    CONSTRAINT PK_BATCH_JOB_INSTANCE PRIMARY KEY (JOB_INSTANCE_ID),
    CONSTRAINT JOB_INST_UN  UNIQUE (JOB_NAME, JOB_KEY)
)
;



COMMENT ON TABLE BATCH_JOB_INSTANCE IS 'This table holds all information relevant to a JobInstance, and serves as the top of the overall hierarchy'
;
COMMENT ON COLUMN BATCH_JOB_INSTANCE.JOB_INSTANCE_ID IS 'The unique id that will identify the instance, which is also the primary key. The value of this column should be obtainable by calling the getId method on JobInstance'
;
COMMENT ON COLUMN BATCH_JOB_INSTANCE.VERSION IS 'This column specify an optimistic locking strategy when dealing with updates to the database'
;
COMMENT ON COLUMN BATCH_JOB_INSTANCE.JOB_NAME IS 'Name of the job obtained from the Job object. Because it is required to identify the instance, it must not be null'
;
COMMENT ON COLUMN BATCH_JOB_INSTANCE.JOB_KEY IS 'A serialization of the JobParameters that uniquely identifies separate instances of the same job from one another. (JobInstances with the same job name must have different JobParameters, and thus, different JOB_KEY values)'
;
-- 
-- TABLE: BATCH_JOB_PARAMS 
--

CREATE TABLE BATCH_JOB_PARAMS(
    JOB_INSTANCE_ID    NUMBER(19, 0)    NOT NULL,
    TYPE_CD            VARCHAR2(6)      NOT NULL,
    KEY_NAME           VARCHAR2(100)    NOT NULL,
    STRING_VAL         VARCHAR2(250),
    DATE_VAL           TIMESTAMP(6),
    LONG_VAL           NUMBER(19, 0),
    DOUBLE_VAL         NUMBER
)
;



COMMENT ON TABLE BATCH_JOB_PARAMS IS 'This table holds all information relevant to the JobParameters object. It contains 0 or more key/value pairs that together uniquely identify a JobInstance and serve as a record of the parameters a job was run with. It should be noted that the table has been denormalized. Rather than creating a separate table for each type, there is one table with a column indicating the type'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.JOB_INSTANCE_ID IS 'Foreign Key from the BATCH_JOB_INSTANCE table that indicates the job instance the parameter entry belongs to. It should be noted that multiple rows (i.e key/value pairs) may exist for each instance'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.TYPE_CD IS 'String representation of the type of value stored, which can be either a string, date, long, or double. Because the type must be known, it cannot be null'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.KEY_NAME IS 'The parameter key'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.STRING_VAL IS 'Parameter value, if the type is string'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.DATE_VAL IS 'Parameter value, if the type is date'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.LONG_VAL IS 'Parameter value, if the type is a long'
;
COMMENT ON COLUMN BATCH_JOB_PARAMS.DOUBLE_VAL IS 'Parameter value, if the type is double'
;
-- 
-- TABLE: BATCH_STEP_EXECUTION 
--

CREATE TABLE BATCH_STEP_EXECUTION(
    STEP_EXECUTION_ID     NUMBER(19, 0)     NOT NULL,
    VERSION               NUMBER(19, 0)     NOT NULL,
    STEP_NAME             VARCHAR2(100)     NOT NULL,
    JOB_EXECUTION_ID      NUMBER(19, 0)     NOT NULL,
    START_TIME            TIMESTAMP(6)      NOT NULL,
    END_TIME              TIMESTAMP(6),
    STATUS                VARCHAR2(10),
    COMMIT_COUNT          NUMBER(19, 0),
    READ_COUNT            NUMBER(19, 0),
    FILTER_COUNT          NUMBER(19, 0),
    WRITE_COUNT           NUMBER(19, 0),
    READ_SKIP_COUNT       NUMBER(19, 0),
    WRITE_SKIP_COUNT      NUMBER(19, 0),
    PROCESS_SKIP_COUNT    NUMBER(19, 0),
    ROLLBACK_COUNT        NUMBER(19, 0),
    EXIT_CODE             VARCHAR2(100),
    EXIT_MESSAGE          VARCHAR2(2500),
    LAST_UPDATED          TIMESTAMP(6),
    CONSTRAINT PK_BATCH_STEP_EXEC PRIMARY KEY (STEP_EXECUTION_ID)
)
;



COMMENT ON TABLE BATCH_STEP_EXECUTION IS 'This table holds all information relevant to the StepExecution object. This table is very similar in many ways to the BATCH_JOB_EXECUTION table and there will always be at least one entry per Step for each JobExecution created'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.STEP_EXECUTION_ID IS 'Primary key that uniquely identifies this execution. The value of this column should be obtainable by calling the getId method of the StepExecution object'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.VERSION IS 'This column specify an optimistic locking strategy when dealing with updates to the database'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.STEP_NAME IS 'The name of the step to which this execution belongs'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.JOB_EXECUTION_ID IS 'Foreign key from the BATCH_JOB_EXECUTION table indicating the JobExecution to which this StepExecution belongs. There may be only one StepExecution for a given JobExecution for a given Step name'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.START_TIME IS 'Timestamp representing the time the execution was started'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.END_TIME IS 'Timestamp representing the time the execution was finished, regardless of success or failure. An empty value in this column even though the job is not currently running indicates that there has been some type of error and the framework was unable to perform a last save before failing'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.STATUS IS 'Character string representing the status of the execution. This may be COMPLETED, STARTED, etc. The object representation of this column is the BatchStatus enumeration'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.COMMIT_COUNT IS 'The number of times in which the step has committed a transaction during this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.READ_COUNT IS 'The number of items read during this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.FILTER_COUNT IS 'The number of items filtered out of this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.WRITE_COUNT IS 'The number of items written and committed during this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.READ_SKIP_COUNT IS 'The number of items skipped on read during this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.WRITE_SKIP_COUNT IS 'The number of items skipped on write during this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.PROCESS_SKIP_COUNT IS 'The number of items skipped during processing during this execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.ROLLBACK_COUNT IS 'The number of rollbacks during this execution. Note that this count includes each time rollback occurs, including rollbacks for retry and those in the skip recovery procedure'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.EXIT_CODE IS 'Character string representing the exit code of the execution. In the case of a command line job, this may be converted into a number'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.EXIT_MESSAGE IS 'Character string representing a more detailed description of how the job exited. In the case of failure, this might include as much of the stack trace as is possible'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION.LAST_UPDATED IS 'Timestamp representing the last time this execution was persisted'
;
-- 
-- TABLE: BATCH_STEP_EXECUTION_CONTEXT 
--

CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT(
    STEP_EXECUTION_ID     NUMBER(19, 0)     NOT NULL,
    SHORT_CONTEXT         VARCHAR2(2500)    NOT NULL,
    SERIALIZED_CONTEXT    CLOB,
    CONSTRAINT PK_BATCH_STEP_EXEC_CONTEXT PRIMARY KEY (STEP_EXECUTION_ID)
)
;



COMMENT ON TABLE BATCH_STEP_EXECUTION_CONTEXT IS 'The BATCH_STEP_EXECUTION_CONTEXT table holds all information relevant to an Steps ExecutionContext. There is exactly one ExecutionContext per StepExecution, and it contains all of the data that needs to persisted for a particular step execution. This data typically represents the state that must be retrieved after a failure so that a JobInstance can start from where it left off'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION_CONTEXT.STEP_EXECUTION_ID IS 'Foreign key representing the StepExecution to which the context belongs. There may be more than one row associated to a given execution'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION_CONTEXT.SHORT_CONTEXT IS 'A string version of the SERIALIZED_CONTEXT'
;
COMMENT ON COLUMN BATCH_STEP_EXECUTION_CONTEXT.SERIALIZED_CONTEXT IS 'The entire context, serialized'
;
-- 
-- TABLE: CONTENT 
--

CREATE TABLE CONTENT(
    ID         NUMBER(19, 0)    NOT NULL,
    CONTENT    BLOB,
    CONSTRAINT PK_CONTENT PRIMARY KEY (ID)
)
;



-- 
-- TABLE: COST_SHARE 
--

CREATE TABLE COST_SHARE(
    ID                               NUMBER           NOT NULL,
    FORMULARY_ID                     NUMBER           NOT NULL,
    COST_SHARE_TYPE                  VARCHAR2(100)    NOT NULL,
    COPAYMENT_AMOUNT                 NUMBER(38, 2)     DEFAULT 0 NOT NULL,
    COINSURANCE_PERCENT              NUMBER(5, 2)     DEFAULT 0 NOT NULL,
    NETWORK_COST_TYPE                VARCHAR2(20)     NOT NULL,
    DRUG_PRESCRIPTION_PERIOD         NUMBER           NOT NULL,
    DRUG_PRESCRIPTION_PERIOD_UNIT    VARCHAR2(10)     NOT NULL,
    PHARMACY_CODE                    VARCHAR2(20)     NOT NULL,
    CREATION_TIMESTAMP               TIMESTAMP(6)     NOT NULL,
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6)     NOT NULL,
    CONSTRAINT PK_COST_SHARE PRIMARY KEY (ID)
)
;



COMMENT ON TABLE COST_SHARE IS 'Table COST_SHARE persists information about cost sharing type and portion for a formulary data provided by SERFF.'
;
COMMENT ON COLUMN COST_SHARE.ID IS 'Primary key for COST_SHARE table.'
;
COMMENT ON COLUMN COST_SHARE.FORMULARY_ID IS 'Foreign key linking COST_SHARE to FORMULARY table.'
;
COMMENT ON COLUMN COST_SHARE.COST_SHARE_TYPE IS 'SERFF - Cost sharing type data determining copayment/coinsurance portion.'
;
COMMENT ON COLUMN COST_SHARE.COPAYMENT_AMOUNT IS 'SERFF - Indicates copayment amount portion.'
;
COMMENT ON COLUMN COST_SHARE.COINSURANCE_PERCENT IS 'SERFF - Indicates coinsurance percent of amount portion.'
;
COMMENT ON COLUMN COST_SHARE.NETWORK_COST_TYPE IS 'SERFF - Network association indicator.'
;
COMMENT ON COLUMN COST_SHARE.DRUG_PRESCRIPTION_PERIOD IS 'SERFF - Drug Cost Sharing Supply duration period count.'
;
COMMENT ON COLUMN COST_SHARE.DRUG_PRESCRIPTION_PERIOD_UNIT IS 'Drug Cost Sharing Supply duration period count unit.'
;
COMMENT ON COLUMN COST_SHARE.PHARMACY_CODE IS 'Drug Cost Sharing Pharmacy Code (Retail, Mail order).'
;
COMMENT ON COLUMN COST_SHARE.CREATION_TIMESTAMP IS 'Record creation timestamp.'
;
COMMENT ON COLUMN COST_SHARE.LAST_UPDATE_TIMESTAMP IS 'Record updated timestamp.'
;
-- 
-- TABLE: CREDIT_CARD_INFO 
--

CREATE TABLE CREDIT_CARD_INFO(
    ID                  NUMBER(10, 0)     NOT NULL,
    CARD_NUMBER         VARCHAR2(1020),
    CARD_TYPE           VARCHAR2(1020),
    CREATED             TIMESTAMP(6),
    EXPIRATION_MONTH    VARCHAR2(1020),
    EXPIRATION_YEAR     VARCHAR2(1020),
    UPDATED             TIMESTAMP(6),
    NAME_ON_CARD        VARCHAR2(1020),
    CONSTRAINT PK_CREDIT_CARD_INFO PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN CREDIT_CARD_INFO.ID IS 'Primary Key, associated with Sequence CREDIT_CARD_INFO_SEQ'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.CARD_NUMBER IS '16 digit encrypted credit card number'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.CARD_TYPE IS 'Type of card values can be MasterCard, Visa or AmericanExpress'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.CREATED IS 'Date on which Payment method with type Credit card is created'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.EXPIRATION_MONTH IS 'Credit card Expiration Month values in the format MM'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.EXPIRATION_YEAR IS 'Credit card Expiration Year values in the format YYYY'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.UPDATED IS 'Last date on which Payment details updated'
;
COMMENT ON COLUMN CREDIT_CARD_INFO.NAME_ON_CARD IS 'Customer name on Credit card'
;
-- 
-- TABLE: DCS_TIER_TYPE 
--

CREATE TABLE DCS_TIER_TYPE(
    ID                        NUMBER(10, 0)    NOT NULL,
    TIER_ID                   VARCHAR2(200)    NOT NULL,
    FORMULARY_ID              NUMBER(10, 0)    NOT NULL,
    CREATION_TIMESTAMP        TIMESTAMP(6)     NOT NULL,
    LAST_UPDATED_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    CONSTRAINT PK_DCS_TIER_TYPE PRIMARY KEY (ID)
)
;



COMMENT ON TABLE DCS_TIER_TYPE IS 'This table describes information about a Drug Cost Sharing Tier with respect to Formulary.'
;
COMMENT ON COLUMN DCS_TIER_TYPE.ID IS 'Primary key for DCS_TIER_TYPE table.'
;
COMMENT ON COLUMN DCS_TIER_TYPE.TIER_ID IS 'SERFF - Drug tier ID.'
;
COMMENT ON COLUMN DCS_TIER_TYPE.FORMULARY_ID IS 'Foreign Key linking to FORMULARY table.'
;
COMMENT ON COLUMN DCS_TIER_TYPE.CREATION_TIMESTAMP IS 'Record creation timestamp.'
;
COMMENT ON COLUMN DCS_TIER_TYPE.LAST_UPDATED_TIMESTAMP IS 'Record updated timestamp.'
;
-- 
-- TABLE: DCS_TYPE 
--

CREATE TABLE DCS_TYPE(
    ID                         NUMBER(10, 0)    NOT NULL,
    DCS_TIER_TYPE_ID           NUMBER(10, 0)    NOT NULL,
    IN_NETWORK_INDICATOR       CHAR(1)          DEFAULT 'N' NOT NULL,
    PHARMACY_CODE              VARCHAR2(200)    NOT NULL,
    MEASURE_POINT_VALUE        NUMBER(10, 0)    NOT NULL,
    TIME_UNIT_CODE             VARCHAR2(10)     NOT NULL,
    DCS_CODE                   VARCHAR2(200)    NOT NULL,
    DCS_COPAYMENT_AMOUNT       NUMBER(38, 2)     DEFAULT 0 NOT NULL,
    DCS_COINSURANCE_PERCENT    NUMBER(3, 0)     DEFAULT 0 NOT NULL,
    BENEFIT_OFFERED            CHAR(1)          DEFAULT 'N' NOT NULL,
    CREATION_TIMESTAMP         TIMESTAMP(6)     NOT NULL,
    LAST_UPDATED_TIMESTAMP     TIMESTAMP(6)     NOT NULL,
    CONSTRAINT PK_DCS_TYPE PRIMARY KEY (ID)
)
;



COMMENT ON TABLE DCS_TYPE IS 'This table describes information about Drug Cost Sharing with respect to Drug Cost Sharing Tier Type.'
;
COMMENT ON COLUMN DCS_TYPE.ID IS 'Primary key for DCS_TYPE table.'
;
COMMENT ON COLUMN DCS_TYPE.DCS_TIER_TYPE_ID IS 'Foreign Key linking to DCS_TIER_TYPE table.'
;
COMMENT ON COLUMN DCS_TYPE.IN_NETWORK_INDICATOR IS 'SERFF - In Network indicator'
;
COMMENT ON COLUMN DCS_TYPE.PHARMACY_CODE IS 'SERFF - Drug Cost Sharing Pharmacy Code.'
;
COMMENT ON COLUMN DCS_TYPE.MEASURE_POINT_VALUE IS 'SERFF - Drug Cost Sharing Supply duration Measure point value'
;
COMMENT ON COLUMN DCS_TYPE.TIME_UNIT_CODE IS 'SERFF - Drug Cost Sharing Supply duration Time Unit Code.'
;
COMMENT ON COLUMN DCS_TYPE.DCS_CODE IS 'SERFF - Drug Cost Sharing Code.'
;
COMMENT ON COLUMN DCS_TYPE.DCS_COPAYMENT_AMOUNT IS 'SERFF - Indicates copayment amount.'
;
COMMENT ON COLUMN DCS_TYPE.DCS_COINSURANCE_PERCENT IS 'SERFF - Indicates coinsurance percent of amount.'
;
COMMENT ON COLUMN DCS_TYPE.BENEFIT_OFFERED IS 'SERFF - Indicates whether Pharmacy Benefit is offered'
;
COMMENT ON COLUMN DCS_TYPE.CREATION_TIMESTAMP IS 'Record creation timestamp.'
;
COMMENT ON COLUMN DCS_TYPE.LAST_UPDATED_TIMESTAMP IS 'Record updated timestamp.'
;
-- 
-- TABLE: DRUG_CODE_TIER_CODE_TYPE 
--

CREATE TABLE DRUG_CODE_TIER_CODE_TYPE(
    ID                        NUMBER(10, 0)    NOT NULL,
    NAME                      VARCHAR2(200)    NOT NULL,
    DCS_TIER_TYPE_ID          NUMBER(10, 0)    NOT NULL,
    CREATION_TIMESTAMP        TIMESTAMP(6)     NOT NULL,
    LAST_UPDATED_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    CONSTRAINT PK_DRUG_CODE_TIER_CODE_TYPE PRIMARY KEY (ID)
)
;



COMMENT ON TABLE DRUG_CODE_TIER_CODE_TYPE IS 'This table describes information about a Drug Code Tier Category Code Type with respect to Drug Cost Sharing Tier Type.'
;
COMMENT ON COLUMN DRUG_CODE_TIER_CODE_TYPE.ID IS 'Primary key for DRUG_CODE_TIER_CODE_TYPE table.'
;
COMMENT ON COLUMN DRUG_CODE_TIER_CODE_TYPE.NAME IS 'SERFF - Drug Tier Type Simple name.'
;
COMMENT ON COLUMN DRUG_CODE_TIER_CODE_TYPE.DCS_TIER_TYPE_ID IS 'Foreign Key linking to DCS_TIER_TYPE table.'
;
COMMENT ON COLUMN DRUG_CODE_TIER_CODE_TYPE.CREATION_TIMESTAMP IS 'Record creation timestamp.'
;
COMMENT ON COLUMN DRUG_CODE_TIER_CODE_TYPE.LAST_UPDATED_TIMESTAMP IS 'Record updated timestamp.'
;
-- 
-- TABLE: EDI_RESPONSE_LOG 
--

CREATE TABLE EDI_RESPONSE_LOG(
    ID                   NUMBER(10, 0),
    HIOS_ISSUER_ID       VARCHAR2(25),
    JOB_EXECUTION_ID     NUMBER(10, 0),
    JOB_INSTANCE_ID      NUMBER(10, 0),
    STEP_EXECUTION_ID    NUMBER(10, 0),
    JOB_STEP_NAME        VARCHAR2(60),
    RESPONSE_CODE        VARCHAR2(60),
    CREATED_ON           TIMESTAMP(6),
    UPDATED_ON           TIMESTAMP(6)
)
;



-- 
-- TABLE: EE_DESIGNATE_ASSISTERS 
--

CREATE TABLE EE_DESIGNATE_ASSISTERS(
    ID                        NUMBER            NOT NULL,
    ASSISTER_ID               NUMBER,
    ENTITY_ID                 NUMBER,
    INDIVIDUAL_ID             NUMBER,
    STATUS                    VARCHAR2(1020),
    ESIGN_BY                  VARCHAR2(1020),
    ESIGN_DATE                TIMESTAMP(6),
    DELEGATION_CODE           VARCHAR2(10),
    SHOW_SWITCH_ROLE_POPUP    CHAR(1)           DEFAULT 'Y',
    CREATION_TIMESTAMP        TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP     TIMESTAMP(6),
    CREATED_BY                NUMBER,
    LAST_UPDATED_BY           NUMBER,
    CONSTRAINT PK_EE_DESIGNATE_ASSISTERS PRIMARY KEY (ID)
)
;



COMMENT ON TABLE EE_DESIGNATE_ASSISTERS IS 'This table contains data of entity/assister individual designation details'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.ASSISTER_ID IS 'Assister Id to which individual is designated'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.ENTITY_ID IS 'Entity id of assister to which individual is designated'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.INDIVIDUAL_ID IS 'Individual id who designated the assister'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.STATUS IS 'Individual designation status'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.ESIGN_BY IS 'Esignature by user'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.ESIGN_DATE IS 'Esignature date'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.DELEGATION_CODE IS 'AHBX delegation code'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.SHOW_SWITCH_ROLE_POPUP IS 'Switch to Individual role user preference for Broker'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.CREATION_TIMESTAMP IS 'Record creation date'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.LAST_UPDATE_TIMESTAMP IS 'Record updation date'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.CREATED_BY IS 'User who created the record'
;
COMMENT ON COLUMN EE_DESIGNATE_ASSISTERS.LAST_UPDATED_BY IS 'User who created the record'
;
-- 
-- TABLE: EMPLOYER_ELIGIBILITY_STATUS 
--

CREATE TABLE EMPLOYER_ELIGIBILITY_STATUS(
    ID                                NUMBER(10, 0)     NOT NULL,
    ELIGIBILITY_STATUS                VARCHAR2(30)      NOT NULL,
    ELIGIBITY_DECISION_FACTOR_NOTE    VARCHAR2(2000 CHAR),
    CREATED_BY                        NUMBER(10, 0),
    EMPLOYER_ID                       NUMBER(10, 0)     NOT NULL,
    CREATION_TIMESTAMP                TIMESTAMP(6)      NOT NULL,
    CONSTRAINT PK_EMPLOYER_ELIGIBILITY_STATUS PRIMARY KEY (ID)
)
;



-- 
-- TABLE: EMPLOYER_ENROLLMENT_ITEMS 
--

CREATE TABLE EMPLOYER_ENROLLMENT_ITEMS(
    ID                        NUMBER          NOT NULL,
    AVG_PREMIUM               FLOAT(126),
    CREATION_TIMESTAMP        TIMESTAMP(6)    NOT NULL,
    PLAN_TIER                 VARCHAR2(80),
    PREMIUM                   FLOAT(126),
    LAST_UPDATE_TIMESTAMP     TIMESTAMP(6)    NOT NULL,
    EMPLOYER_ENROLLMENT_ID    NUMBER,
    BENCHMARK_PLAN_ID         NUMBER,
    CREATED_BY                NUMBER,
    LAST_UPDATED_BY           NUMBER,
    CONSTRAINT PK_EMPLOYER_ENROLLMENT_ITEMS PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 8;
;



COMMENT ON TABLE EMPLOYER_ENROLLMENT_ITEMS IS 'This entity maintains information of employer enrollment items'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.AVG_PREMIUM IS 'The average of the premiums of the plan'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.PLAN_TIER IS 'Plan level. The values can be: PLATINUM, GOLD, SILVER, BRONZE.'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.PREMIUM IS 'Plan Premium'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.LAST_UPDATE_TIMESTAMP IS 'Last updated date for this record.'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.EMPLOYER_ENROLLMENT_ID IS 'Employer Order that is corresponding to this order item'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.BENCHMARK_PLAN_ID IS 'Plan id of the order item'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENT_ITEMS.LAST_UPDATED_BY IS 'User did the last update'
;
-- 
-- TABLE: EMPLOYER_ENROLLMENTS 
--

CREATE TABLE EMPLOYER_ENROLLMENTS(
    ID                        NUMBER            NOT NULL,
    COVERAGE_START_DATE       TIMESTAMP(6),
    COVERAGE_END_DATE         TIMESTAMP(6),
    DEPENDENT_CONTRIBUTION    FLOAT(126),
    EMPLOYEE_CONTRIBUTION     FLOAT(126),
    CREATION_TIMESTAMP        TIMESTAMP(6)      NOT NULL,
    OPEN_ENROLLMENT_END       TIMESTAMP(6),
    OPEN_ENROLLMENT_START     TIMESTAMP(6),
    STATUS                    VARCHAR2(40 CHAR),
    LAST_UPDATE_TIMESTAMP     TIMESTAMP(6)      NOT NULL,
    EMPLOYER_ID               NUMBER,
    CREATED_BY                NUMBER,
    LAST_UPDATED_BY           NUMBER,
    TERMINATION_REASON        VARCHAR2(80),
    TERMINATION_DATE          DATE,
    ESIGNATURE                VARCHAR2(1020),
    CONSTRAINT PK_EMPLOYER_ENROLLMENTS PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 8
;



COMMENT ON TABLE EMPLOYER_ENROLLMENTS IS 'This entity maintains information of employer enrollments'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.COVERAGE_START_DATE IS 'Coverage start date for policy'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.COVERAGE_END_DATE IS 'Coverage end date for policy'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.DEPENDENT_CONTRIBUTION IS 'Amount for Dependant coverage'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.EMPLOYEE_CONTRIBUTION IS 'Amount for employee coverage'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.OPEN_ENROLLMENT_END IS 'Open enrollment end date'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.OPEN_ENROLLMENT_START IS 'Open enrollment start date'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.STATUS IS 'Status of the enrollment.  The values can be: CART, PENDING, ACTIVE, CANCELLED, TERMINATED'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.LAST_UPDATE_TIMESTAMP IS 'Last updated date for this record.'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.EMPLOYER_ID IS 'Foreign key to Employer table'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.CREATED_BY IS 'User id who created this record.'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.TERMINATION_REASON IS 'Stores termination reason selected by employer'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.TERMINATION_DATE IS 'Stores termination date selected by employer'
;
COMMENT ON COLUMN EMPLOYER_ENROLLMENTS.ESIGNATURE IS 'Stores e-signature entered by employer'
;
-- 
-- TABLE: EMPLOYER_INVOICES 
--

CREATE TABLE EMPLOYER_INVOICES(
    ID                              NUMBER(10, 0)    NOT NULL,
    EMPLOYER_ID                     NUMBER(10, 0),
    CASE_ID                         VARCHAR2(20),
    STATEMENT_DATE                  DATE,
    INVOICE_NUMBER                  VARCHAR2(25),
    PERIOD_COVERED                  VARCHAR2(25),
    PAYMENT_DUE_DATE                DATE,
    AMOUNT_DUE_FROM_LAST_INVOICE    FLOAT(126),
    TOTAL_PAYMENT_RECEIVED          FLOAT(126),
    PREMIUMS_THIS_PERIOD            FLOAT(126),
    ADJUSTMENTS                     FLOAT(126),
    EXCHANGE_FEES                   FLOAT(126),
    TOTAL_AMOUNT_DUE                FLOAT(126),
    AMOUNT_ENCLOSED                 FLOAT(126),
    PAID_STATUS                     VARCHAR2(20),
    ECM_DOC_ID                      VARCHAR2(400),
    INVOICE_TYPE                    VARCHAR2(400),
    IS_ACTIVE                       VARCHAR2(1),
    REISSUE_ID                      NUMBER(10, 0),
    GL_CODE                         NUMBER(10, 0),
    CONSTRAINT PK_EMPLOYER_INVOICES PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN EMPLOYER_INVOICES.ID IS 'Primary Key, associated with Sequence EMPLOYER_INVOICES_SEQ'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.EMPLOYER_ID IS 'Id of Employer whose invoice is generated'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.STATEMENT_DATE IS 'Date on which invoice is generated'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.INVOICE_NUMBER IS 'Unique no of generated invoice, used to identify invoice'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.PERIOD_COVERED IS 'Period covered under this active invoice'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.PAYMENT_DUE_DATE IS 'Last date of premium payment'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Due amount carried from last paid or unpaid invoice'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.TOTAL_PAYMENT_RECEIVED IS 'Total Payment received from employer this period'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.PREMIUMS_THIS_PERIOD IS 'Generated premium of invoice this period'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.ADJUSTMENTS IS 'Adjustment of premium from the previous paid invoice if any'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.EXCHANGE_FEES IS 'Exchanges fees occured for total amount due '
;
COMMENT ON COLUMN EMPLOYER_INVOICES.TOTAL_AMOUNT_DUE IS 'Total amount due from employer invoices'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.AMOUNT_ENCLOSED IS ', the actual amt paid by employer, that is the amount entered in Payment screen by the user'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.PAID_STATUS IS 'Status of payment, value can be  DUE,PAID or PARTIALLY_PAID'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.ECM_DOC_ID IS 'Alfresco ECM Doc if for the generated invoice pdf'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.INVOICE_TYPE IS 'Type of Generated invoice, value can be BINDER_INVOICE or NORMAL'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.IS_ACTIVE IS 'For the invoice active or inactive, value can be Y or N'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.REISSUE_ID IS 'If new reissue invoice for same period is generated then the previous invoice id is set here'
;
COMMENT ON COLUMN EMPLOYER_INVOICES.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements'
;
-- 
-- TABLE: EMPLOYER_INVOICES_LINEITEMS 
--

CREATE TABLE EMPLOYER_INVOICES_LINEITEMS(
    ID                       NUMBER(10, 0)    NOT NULL,
    EMPLOYER_ID              NUMBER(10, 0),
    POLICY_NUMBER            VARCHAR2(50),
    EMPLOYEE_ID              NUMBER(10, 0),
    CARRIER_NAME             VARCHAR2(400),
    PLAN_TYPE                VARCHAR2(40),
    COVERAGE_TYPE            VARCHAR2(20),
    PERSONS_COVERED          VARCHAR2(20),
    TOTAL_PREMIUM            FLOAT(126),
    EMPLOYEE_CONTRIBUTION    FLOAT(126),
    RETRO_ADJUSTMENTS        FLOAT(126),
    NET_AMOUNT               FLOAT(126),
    PERIOD_COVERED           VARCHAR2(25),
    INVOICE_ID               NUMBER(10, 0),
    ISSUER_ID                NUMBER(10, 0),
    PAYMENT_RECEIVED         FLOAT(126),
    PAID_STATUS              VARCHAR2(20),
    PAID_DATE                DATE,
    PAID_TO_ISSUER           VARCHAR2(1)      DEFAULT 'N',
    ENROLLMENT_ID            NUMBER,
    CONSTRAINT PK_EMPLOYER_INVOICES_LINEITEMS PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.ID IS 'Primary Key, associated with Sequence EMPLOYER_LINEITEMS_SEQ'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.EMPLOYER_ID IS 'Employer Id whom employee belongs to'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.POLICY_NUMBER IS 'Policy No of the opted plan'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.EMPLOYEE_ID IS 'Foreign constraint refrence table Employees, for employee id '
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.CARRIER_NAME IS 'Issuer Name whose plan is opted by employee'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PLAN_TYPE IS 'Plan type opted by the employee ffrom the said issuer'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.COVERAGE_TYPE IS 'Plan coverage offered by Issuer'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PERSONS_COVERED IS 'Persons id of thee persons covered under this plan'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.TOTAL_PREMIUM IS 'Total premium of the opted plan'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.EMPLOYEE_CONTRIBUTION IS 'Employee contribution of the piad amount for the lineitem'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.RETRO_ADJUSTMENTS IS 'It is the adjustment made with respect to last invoice i.e if on last invoice we charge for an employee and he resigned, then we have to deduct that much amount. Those adjustments are stored here'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.NET_AMOUNT IS 'amount to be paid by employer for this plan selected by employee'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PERIOD_COVERED IS 'Period covered under this plan'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.INVOICE_ID IS 'Foreign constraint reference table Employer_invoices'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.ISSUER_ID IS 'Foreign constraint reference table Issuers'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAYMENT_RECEIVED IS 'Payment received by the employer against the selected plan of Employee'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAID_STATUS IS 'Payment status value can be  DUE,PAID or PARTIALLY_PAID;'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAID_DATE IS 'Date on which premium payment is done'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAID_TO_ISSUER IS 'Whether this payment is paid to issuer or not values can be Y or N'
;
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.ENROLLMENT_ID IS 'Column to store enrollment reference'
;
-- 
-- TABLE: EMPLOYER_NOTIFICATION 
--

CREATE TABLE EMPLOYER_NOTIFICATION(
    ID                     NUMBER(10, 0)    NOT NULL,
    NOTICES_ID             NUMBER(10, 0),
    EMPLOYER_INVOICE_ID    NUMBER(10, 0),
    DATE_CREATED           TIMESTAMP(6),
    STATUS                 VARCHAR2(20),
    NUMBER_OF_ATTEMPTS     NUMBER(10, 0),
    ADDITIONAL_COMMENTS    VARCHAR2(20),
    UPDATED_DATE           TIMESTAMP(6),
    CONSTRAINT PK_EMPLOYER_NOTIFICATION PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN EMPLOYER_NOTIFICATION.NOTICES_ID IS 'notice id'
;
COMMENT ON COLUMN EMPLOYER_NOTIFICATION.EMPLOYER_INVOICE_ID IS 'employer invoice id'
;
COMMENT ON COLUMN EMPLOYER_NOTIFICATION.DATE_CREATED IS 'created date'
;
COMMENT ON COLUMN EMPLOYER_NOTIFICATION.STATUS IS 'notification status'
;
COMMENT ON COLUMN EMPLOYER_NOTIFICATION.NUMBER_OF_ATTEMPTS IS 'no of attempts'
;
COMMENT ON COLUMN EMPLOYER_NOTIFICATION.ADDITIONAL_COMMENTS IS 'additional comments'
;
COMMENT ON COLUMN EMPLOYER_NOTIFICATION.UPDATED_DATE IS 'updated date'
;
-- 
-- TABLE: EMPLOYER_PAYMENT_INVOICE 
--

CREATE TABLE EMPLOYER_PAYMENT_INVOICE(
    ID                NUMBER(10, 0)    NOT NULL,
    INVOICE_ID        NUMBER(10, 0),
    CREATION_DATE     DATE,
    AMOUNT            FLOAT(126),
    EXCESS_AMOUNT     FLOAT(126),
    GL_CODE           NUMBER(10, 0),
    CONFIRM_NUMBER    NUMBER(10, 0),
    CONSTRAINT PK_EMPLOYER_PAYMENT_INVOICE PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.ID IS 'Primary Key, associated with Sequence EMPLOYER_PAYINVOICE_SEQ'
;
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.INVOICE_ID IS 'Employer Invoice Id, Foreign constraint with Employer_invoices'
;
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.CREATION_DATE IS 'Payment creation or paid date'
;
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.AMOUNT IS 'Actual Amount paid'
;
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.EXCESS_AMOUNT IS 'Amount paid more than the invoice amount'
;
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements'
;
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.CONFIRM_NUMBER IS 'Column stores confirmation number after making a payment as a reference of tx'
;
-- 
-- TABLE: EMPLOYER_PAYMENTS 
--

CREATE TABLE EMPLOYER_PAYMENTS(
    ID                              NUMBER(10, 0)     NOT NULL,
    CASE_ID                         VARCHAR2(20),
    STATEMENT_DATE                  DATE,
    INVOICE_NUMBER                  VARCHAR2(25),
    PERIOD_COVERED                  VARCHAR2(25),
    PAYMENT_DUE_DATE                DATE,
    AMOUNT_DUE_FROM_LAST_INVOICE    FLOAT(126),
    TOTAL_PAYMENT_RECEIVED          FLOAT(126),
    PREMIUMS_THIS_PERIOD            FLOAT(126),
    ADJUSTMENTS                     FLOAT(126),
    EXCHANGE_FEES                   FLOAT(126),
    TOTAL_AMOUNT_DUE                FLOAT(126),
    AMOUNT_ENCLOSED                 FLOAT(126),
    PAYMENT_TYPE_ID                 NUMBER(10, 0),
    EMPLOYER_PAYMENT_INVOICE_ID     NUMBER(10, 0),
    STATUS                          VARCHAR2(20),
    TRANSACTION_ID                  VARCHAR2(50),
    IS_REFUND                       VARCHAR2(1),
    IS_PARTIAL_PAYMENT              CHAR(1),
    RESPONSE                        VARCHAR2(1020),
    REFUND_AMT                      FLOAT(126),
    CONSTRAINT PK_EMPLOYER_PAYMENTS PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN EMPLOYER_PAYMENTS.ID IS 'Primary Key, associated with Sequence EMPLOYERPAY_SEQ'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.STATEMENT_DATE IS 'the date on which invoice is generated'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.INVOICE_NUMBER IS 'Employer Invoice no, payment made against this generated invoice'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PERIOD_COVERED IS 'Period covered for this invoice'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PAYMENT_DUE_DATE IS 'Premium payment due date'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Amount due for this Employer from his last invoice'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.TOTAL_PAYMENT_RECEIVED IS 'Total payment received from employer'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PREMIUMS_THIS_PERIOD IS 'Premium this period for the employer'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.ADJUSTMENTS IS 'Adjustment of the premium amount if any'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.EXCHANGE_FEES IS 'Exchange fees for the generated invoice'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.TOTAL_AMOUNT_DUE IS 'Actual amount due'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.AMOUNT_ENCLOSED IS 'The actual amt paid by employer, that is the amount entered in Payment screen by the user'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PAYMENT_TYPE_ID IS 'Foreign constraint reference table PAYMENT_METHODS, employer added payment method'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.EMPLOYER_PAYMENT_INVOICE_ID IS 'Foreign constraint reference table EMPLOYER_PAYMENT_INVOICE'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.STATUS IS 'Payment status whether Paid or PARTIALLY_PAID'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.TRANSACTION_ID IS 'Cybersource transaction id, used to store payment trxn info'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.IS_REFUND IS 'for the payment Refund or not, values can be N or Y'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.IS_PARTIAL_PAYMENT IS 'Is partial payment or full payment, Values can be N or Y'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.RESPONSE IS 'Cybersource response on the payment done.'
;
COMMENT ON COLUMN EMPLOYER_PAYMENTS.REFUND_AMT IS 'Refund amount on the payment if any'
;
-- 
-- TABLE: ENROLLEE 
--

CREATE TABLE ENROLLEE(
    ID                               NUMBER           NOT NULL,
    TAX_ID_NUMBER                    VARCHAR2(80),
    HEALTH_COVERAGE_POLICY_NO        VARCHAR2(50),
    LAST_NAME                        VARCHAR2(100),
    FIRST_NAME                       VARCHAR2(100),
    MIDDLE_NAME                      VARCHAR2(100),
    SUFFIX                           VARCHAR2(40),
    PRIMARY_PHONE_NO                 VARCHAR2(25),
    SECONDARY_PHONE_NO               VARCHAR2(256),
    HOME_ADDRESS_ID                  NUMBER,
    BIRTH_DATE                       TIMESTAMP(6),
    GENDER_LKP                       NUMBER,
    MAILING_ADDRESS_ID               NUMBER,
    ENROLLMENT_ID                    NUMBER,
    PREF_SMS                         VARCHAR2(256),
    PREF_EMAIL                       VARCHAR2(256),
    TOTAL_INDV_RESPONSIBILITY_AMT    NUMBER,
    MAINTENANCE_EFFECTIVE_DATE       TIMESTAMP(6),
    RATING_AREA                      VARCHAR2(50),
    LAST_PREMIUM_PAID_DATE           DATE,
    TOT_EMP_RESPONSIBILITY_AMT       NUMBER,
    CUSTODIAL_PARENT_ID              NUMBER,
    CITIZENSHIP_STATUS_LKP           NUMBER,
    TOBACCO_USAGE_LKP                NUMBER,
    LANGUAGE_SPOKEN_LKP              NUMBER,
    LANGUAGE_WRITTEN_LKP             NUMBER,
    MEMBER_EMPLOYER_ID               NUMBER,
    ENROLLEE_STATUS_LKP              NUMBER,
    MARITAL_STATUS_LKP               NUMBER,
    RACE_ETHNICITY_LKP               NUMBER,
    NATIONAL_INDIV_IDENTIFIER        VARCHAR2(80),
    EFFECTIVE_START_DATE             TIMESTAMP(6),
    EFFECTIVE_END_DATE               TIMESTAMP(6),
    EXCHG_INDIV_IDENTIFIER           VARCHAR2(200),
    ISSUER_INDIV_IDENTIFIER          VARCHAR2(50),
    PERSON_TYPE_LKP                  NUMBER,
    RESP_PERSON_ID_CODE              VARCHAR2(3),
    CREATION_TIMESTAMP               TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6),
    CREATED_BY                       NUMBER,
    LAST_UPDATED_BY                  NUMBER,
    RELATIONSHIP_HCP_LKP             NUMBER,
    DEATH_DATE                       DATE,
    ENROLLMENT_REASON                VARCHAR2(1 CHAR),
    LAST_EVENT_ID                    NUMBER,
    EMPLOYEE_ID                      NUMBER,
    CONSTRAINT PK_ENROLLEE PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLEE_ALT  UNIQUE (ENROLLMENT_ID, EXCHG_INDIV_IDENTIFIER, ENROLLEE_STATUS_LKP, PERSON_TYPE_LKP)
)
PARTITION BY HASH(id) PARTITIONS 16
;



COMMENT ON TABLE ENROLLEE IS 'This entity contains the enrollee information and the lookupvalues, enrollment events information'
;
COMMENT ON COLUMN ENROLLEE.ID IS 'Primary Key'
;
COMMENT ON COLUMN ENROLLEE.TAX_ID_NUMBER IS 'Tax id number'
;
COMMENT ON COLUMN ENROLLEE.HEALTH_COVERAGE_POLICY_NO IS 'Health coverage policy number'
;
COMMENT ON COLUMN ENROLLEE.LAST_NAME IS 'Last Name'
;
COMMENT ON COLUMN ENROLLEE.FIRST_NAME IS 'First name'
;
COMMENT ON COLUMN ENROLLEE.MIDDLE_NAME IS 'Middle name'
;
COMMENT ON COLUMN ENROLLEE.SUFFIX IS 'Suffix. Possible values I, II, III, JR, SR'
;
COMMENT ON COLUMN ENROLLEE.PRIMARY_PHONE_NO IS 'Primary phone number'
;
COMMENT ON COLUMN ENROLLEE.SECONDARY_PHONE_NO IS 'Secondary phone number'
;
COMMENT ON COLUMN ENROLLEE.HOME_ADDRESS_ID IS 'Home address id'
;
COMMENT ON COLUMN ENROLLEE.BIRTH_DATE IS 'Birth date'
;
COMMENT ON COLUMN ENROLLEE.GENDER_LKP IS 'Gender lkp'
;
COMMENT ON COLUMN ENROLLEE.MAILING_ADDRESS_ID IS 'Mailing address id'
;
COMMENT ON COLUMN ENROLLEE.ENROLLMENT_ID IS 'Enrollment id'
;
COMMENT ON COLUMN ENROLLEE.PREF_SMS IS 'Preferred SMS for communication'
;
COMMENT ON COLUMN ENROLLEE.PREF_EMAIL IS 'Preferred Email for communication'
;
COMMENT ON COLUMN ENROLLEE.TOTAL_INDV_RESPONSIBILITY_AMT IS 'Total individual responsibility amount'
;
COMMENT ON COLUMN ENROLLEE.MAINTENANCE_EFFECTIVE_DATE IS 'Maintenance effective data'
;
COMMENT ON COLUMN ENROLLEE.RATING_AREA IS 'Rating area'
;
COMMENT ON COLUMN ENROLLEE.LAST_PREMIUM_PAID_DATE IS 'Last premium paid date'
;
COMMENT ON COLUMN ENROLLEE.TOT_EMP_RESPONSIBILITY_AMT IS 'Total employer responsibility amount '
;
COMMENT ON COLUMN ENROLLEE.CUSTODIAL_PARENT_ID IS 'Custodial Parent ID'
;
COMMENT ON COLUMN ENROLLEE.CITIZENSHIP_STATUS_LKP IS 'Citizenship status'
;
COMMENT ON COLUMN ENROLLEE.TOBACCO_USAGE_LKP IS 'Tobacco usage lkp'
;
COMMENT ON COLUMN ENROLLEE.LANGUAGE_SPOKEN_LKP IS 'Language spoken lkp'
;
COMMENT ON COLUMN ENROLLEE.LANGUAGE_WRITTEN_LKP IS 'Language written lkp'
;
COMMENT ON COLUMN ENROLLEE.MEMBER_EMPLOYER_ID IS 'Member�s employer id'
;
COMMENT ON COLUMN ENROLLEE.ENROLLEE_STATUS_LKP IS 'Enrollee status lkp'
;
COMMENT ON COLUMN ENROLLEE.MARITAL_STATUS_LKP IS 'Marital status lkp'
;
COMMENT ON COLUMN ENROLLEE.RACE_ETHNICITY_LKP IS 'Race ethnicity lkp.  Foreign Key to the LOOKUPVALUE table.'
;
COMMENT ON COLUMN ENROLLEE.NATIONAL_INDIV_IDENTIFIER IS 'National individual identifier'
;
COMMENT ON COLUMN ENROLLEE.EFFECTIVE_START_DATE IS 'Tax id number'
;
COMMENT ON COLUMN ENROLLEE.EFFECTIVE_END_DATE IS 'Effective end date'
;
COMMENT ON COLUMN ENROLLEE.EXCHG_INDIV_IDENTIFIER IS 'Exchange individual identifier'
;
COMMENT ON COLUMN ENROLLEE.ISSUER_INDIV_IDENTIFIER IS 'Issuer individual identifier'
;
COMMENT ON COLUMN ENROLLEE.PERSON_TYPE_LKP IS 'Person type lkp'
;
COMMENT ON COLUMN ENROLLEE.RESP_PERSON_ID_CODE IS 'The tax  code of the responsible person '
;
COMMENT ON COLUMN ENROLLEE.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN ENROLLEE.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN ENROLLEE.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN ENROLLEE.RELATIONSHIP_HCP_LKP IS 'Foreign Key to the LOOKUPVALUE table. (Relationship to Household contact person)'
;
COMMENT ON COLUMN ENROLLEE.DEATH_DATE IS 'death date to process the dis-enrollments events'
;
COMMENT ON COLUMN ENROLLEE.ENROLLMENT_REASON IS 'Enrollment Reason store the enrollment type - I, A, S , N'
;
COMMENT ON COLUMN ENROLLEE.LAST_EVENT_ID IS 'Last Event Id'
;
COMMENT ON COLUMN ENROLLEE.EMPLOYEE_ID IS 'Employee id'
;
-- 
-- TABLE: ENROLLEE_AUD 
--

CREATE TABLE ENROLLEE_AUD(
    ID                               NUMBER           NOT NULL,
    REV                              NUMBER(10, 0)    NOT NULL,
    REVTYPE                          NUMBER(3, 0),
    TAX_ID_NUMBER                    VARCHAR2(80),
    HEALTH_COVERAGE_POLICY_NO        VARCHAR2(50),
    LAST_NAME                        VARCHAR2(100),
    FIRST_NAME                       VARCHAR2(100),
    MIDDLE_NAME                      VARCHAR2(100),
    SUFFIX                           VARCHAR2(40),
    PRIMARY_PHONE_NO                 VARCHAR2(25),
    SECONDARY_PHONE_NO               VARCHAR2(256),
    HOME_ADDRESS_ID                  NUMBER,
    BIRTH_DATE                       TIMESTAMP(6),
    GENDER_LKP                       NUMBER,
    MAILING_ADDRESS_ID               NUMBER,
    ENROLLMENT_ID                    NUMBER,
    PREF_SMS                         VARCHAR2(256),
    PREF_EMAIL                       VARCHAR2(256),
    TOTAL_INDV_RESPONSIBILITY_AMT    NUMBER,
    MAINTENANCE_EFFECTIVE_DATE       TIMESTAMP(6),
    RATING_AREA                      VARCHAR2(50),
    LAST_PREMIUM_PAID_DATE           DATE,
    TOT_EMP_RESPONSIBILITY_AMT       NUMBER,
    CUSTODIAL_PARENT_ID              NUMBER,
    CITIZENSHIP_STATUS_LKP           NUMBER,
    TOBACCO_USAGE_LKP                NUMBER,
    LANGUAGE_SPOKEN_LKP              NUMBER,
    LANGUAGE_WRITTEN_LKP             NUMBER,
    MEMBER_EMPLOYER_ID               NUMBER,
    ENROLLEE_STATUS_LKP              NUMBER,
    MARITAL_STATUS_LKP               NUMBER,
    RACE_ETHNICITY_LKP               NUMBER,
    NATIONAL_INDIV_IDENTIFIER        VARCHAR2(80),
    SIGNATURE_DATE                   DATE,
    EFFECTIVE_START_DATE             TIMESTAMP(6),
    EFFECTIVE_END_DATE               TIMESTAMP(6),
    EXCHG_INDIV_IDENTIFIER           VARCHAR2(200),
    ISSUER_INDIV_IDENTIFIER          VARCHAR2(50),
    PERSON_TYPE_LKP                  NUMBER,
    RESP_PERSON_ID_CODE              VARCHAR2(3),
    CREATION_TIMESTAMP               TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6),
    CREATED_BY                       NUMBER,
    LAST_UPDATED_BY                  NUMBER,
    RELATIONSHIP_HCP_LKP             NUMBER,
    DEATH_DATE                       DATE,
    ENROLLMENT_REASON                VARCHAR2(1 CHAR),
    LAST_EVENT_ID                    NUMBER,
    EMPLOYEE_ID                      NUMBER
)
PARTITION BY HASH(id) PARTITIONS 16
;



COMMENT ON COLUMN ENROLLEE_AUD.ID IS 'Primary Key'
;
COMMENT ON COLUMN ENROLLEE_AUD.TAX_ID_NUMBER IS 'Tax id number'
;
COMMENT ON COLUMN ENROLLEE_AUD.HEALTH_COVERAGE_POLICY_NO IS 'Health coverage policy number'
;
COMMENT ON COLUMN ENROLLEE_AUD.LAST_NAME IS 'Last Name'
;
COMMENT ON COLUMN ENROLLEE_AUD.FIRST_NAME IS 'First name'
;
COMMENT ON COLUMN ENROLLEE_AUD.MIDDLE_NAME IS 'Middle name'
;
COMMENT ON COLUMN ENROLLEE_AUD.SUFFIX IS 'Suffix. Possible values I, II, III, JR, SR'
;
COMMENT ON COLUMN ENROLLEE_AUD.PRIMARY_PHONE_NO IS 'Primary phone number'
;
COMMENT ON COLUMN ENROLLEE_AUD.SECONDARY_PHONE_NO IS 'Secondary phone number'
;
COMMENT ON COLUMN ENROLLEE_AUD.HOME_ADDRESS_ID IS 'Home address id'
;
COMMENT ON COLUMN ENROLLEE_AUD.BIRTH_DATE IS 'Birth date'
;
COMMENT ON COLUMN ENROLLEE_AUD.GENDER_LKP IS 'Gender lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.MAILING_ADDRESS_ID IS 'Mailing address id'
;
COMMENT ON COLUMN ENROLLEE_AUD.ENROLLMENT_ID IS 'Enrollment id'
;
COMMENT ON COLUMN ENROLLEE_AUD.PREF_SMS IS 'Preferred SMS for communication'
;
COMMENT ON COLUMN ENROLLEE_AUD.PREF_EMAIL IS 'Preferred Email for communication'
;
COMMENT ON COLUMN ENROLLEE_AUD.TOTAL_INDV_RESPONSIBILITY_AMT IS 'Total individual responsibility amount'
;
COMMENT ON COLUMN ENROLLEE_AUD.MAINTENANCE_EFFECTIVE_DATE IS 'Maintenance effective data'
;
COMMENT ON COLUMN ENROLLEE_AUD.RATING_AREA IS 'Rating area'
;
COMMENT ON COLUMN ENROLLEE_AUD.LAST_PREMIUM_PAID_DATE IS 'Last premium paid date'
;
COMMENT ON COLUMN ENROLLEE_AUD.TOT_EMP_RESPONSIBILITY_AMT IS 'Total employer responsibility amount '
;
COMMENT ON COLUMN ENROLLEE_AUD.CUSTODIAL_PARENT_ID IS 'Custodial Parent ID'
;
COMMENT ON COLUMN ENROLLEE_AUD.CITIZENSHIP_STATUS_LKP IS 'Citizenship status'
;
COMMENT ON COLUMN ENROLLEE_AUD.TOBACCO_USAGE_LKP IS 'Tobacco usage lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.LANGUAGE_SPOKEN_LKP IS 'Language spoken lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.LANGUAGE_WRITTEN_LKP IS 'Language written lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.MEMBER_EMPLOYER_ID IS 'Member�s employer id'
;
COMMENT ON COLUMN ENROLLEE_AUD.ENROLLEE_STATUS_LKP IS 'Enrollee status lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.MARITAL_STATUS_LKP IS 'Marital status lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.RACE_ETHNICITY_LKP IS 'Race ethnicity lkp.  Foreign Key to the LOOKUPVALUE table.'
;
COMMENT ON COLUMN ENROLLEE_AUD.NATIONAL_INDIV_IDENTIFIER IS 'National individual identifier'
;
COMMENT ON COLUMN ENROLLEE_AUD.EFFECTIVE_START_DATE IS 'Tax id number'
;
COMMENT ON COLUMN ENROLLEE_AUD.EFFECTIVE_END_DATE IS 'Effective end date'
;
COMMENT ON COLUMN ENROLLEE_AUD.EXCHG_INDIV_IDENTIFIER IS 'Exchange individual identifier'
;
COMMENT ON COLUMN ENROLLEE_AUD.ISSUER_INDIV_IDENTIFIER IS 'Issuer individual identifier'
;
COMMENT ON COLUMN ENROLLEE_AUD.PERSON_TYPE_LKP IS 'Person type lkp'
;
COMMENT ON COLUMN ENROLLEE_AUD.RESP_PERSON_ID_CODE IS 'The tax  code of the responsible person '
;
COMMENT ON COLUMN ENROLLEE_AUD.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN ENROLLEE_AUD.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE_AUD.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN ENROLLEE_AUD.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN ENROLLEE_AUD.RELATIONSHIP_HCP_LKP IS 'Foreign Key to the LOOKUPVALUE table. (Relationship to Household contact person)'
;
COMMENT ON COLUMN ENROLLEE_AUD.DEATH_DATE IS 'death date to process the dis-enrollments events'
;
COMMENT ON COLUMN ENROLLEE_AUD.ENROLLMENT_REASON IS 'Enrollment Reason store the enrollment type - I, A, S , N'
;
COMMENT ON COLUMN ENROLLEE_AUD.LAST_EVENT_ID IS 'Last Event Id'
;
COMMENT ON COLUMN ENROLLEE_AUD.EMPLOYEE_ID IS 'Employee id'
;
-- 
-- TABLE: ENROLLEE_RACE 
--

CREATE TABLE ENROLLEE_RACE(
    ID                       NUMBER           NOT NULL,
    ENROLEE_ID               NUMBER,
    RACE_ETHNICITY_LKP       NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    RACE_DESCRIPTION         VARCHAR2(512),
    CONSTRAINT PK_ENROLEE_RACE PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLEE_RACE_ALT  UNIQUE (ENROLEE_ID, RACE_ETHNICITY_LKP)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE ENROLLEE_RACE IS 'This entity contains the enrollee and race information'
;
COMMENT ON COLUMN ENROLLEE_RACE.ID IS 'Primary Key of the table'
;
COMMENT ON COLUMN ENROLLEE_RACE.ENROLEE_ID IS 'Foreign key of Enrollee table ID'
;
COMMENT ON COLUMN ENROLLEE_RACE.RACE_ETHNICITY_LKP IS 'Race ethnicity lkp.  Foreign Key to the LOOKUPVALUE table.'
;
COMMENT ON COLUMN ENROLLEE_RACE.CREATION_TIMESTAMP IS 'The enrollee race created on particular date'
;
COMMENT ON COLUMN ENROLLEE_RACE.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE_RACE.RACE_DESCRIPTION IS 'Description of the enrollee race'
;
-- 
-- TABLE: ENROLLEE_RACE_AUD 
--

CREATE TABLE ENROLLEE_RACE_AUD(
    ID                       NUMBER           NOT NULL,
    REV                      NUMBER(10, 0)    NOT NULL,
    REVTYPE                  NUMBER(3, 0),
    ENROLEE_ID               NUMBER,
    RACE_ETHNICITY_LKP       NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    RACE_DESCRIPTION         VARCHAR2(512)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON COLUMN ENROLLEE_RACE_AUD.ID IS 'Primary Key of the table'
;
COMMENT ON COLUMN ENROLLEE_RACE_AUD.ENROLEE_ID IS 'Foreign key of Enrollee table ID'
;
COMMENT ON COLUMN ENROLLEE_RACE_AUD.RACE_ETHNICITY_LKP IS 'Race ethnicity lkp.  Foreign Key to the LOOKUPVALUE table.'
;
COMMENT ON COLUMN ENROLLEE_RACE_AUD.CREATION_TIMESTAMP IS 'The enrollee race created on particular date'
;
COMMENT ON COLUMN ENROLLEE_RACE_AUD.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE_RACE_AUD.RACE_DESCRIPTION IS 'Description of the enrollee race'
;
-- 
-- TABLE: ENROLLEE_RELATIONSHIP 
--

CREATE TABLE ENROLLEE_RELATIONSHIP(
    ID                       NUMBER          NOT NULL,
    SOURCE_ENROLEE_ID        NUMBER,
    TARGET_ENROLEE_ID        NUMBER,
    RELATIONSHIP_LKP         NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    RELATIONSHIP_HCP_LKP     NUMBER,
    CONSTRAINT PK_ENROLLEE_RELATIONSHIP PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLEE_RELATIONSHIP_ALT  UNIQUE (SOURCE_ENROLEE_ID, TARGET_ENROLEE_ID, RELATIONSHIP_LKP)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE ENROLLEE_RELATIONSHIP IS 'This entity contains the enrollee and relationship lookup information'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.ID IS 'Primary Key of the table'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.SOURCE_ENROLEE_ID IS 'Foreign key to the Enrollee table'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.TARGET_ENROLEE_ID IS 'Foreign key to the Enrollee table '
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.RELATIONSHIP_LKP IS 'Relationship. Foreign Key to the LOOKUPVALUE table. '
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.CREATION_TIMESTAMP IS 'The enrollee relationship is created on particular date'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.RELATIONSHIP_HCP_LKP IS 'Foreign Key to the LOOKUPVALUE table. (Relationship to Household contact person)'
;
-- 
-- TABLE: ENROLLEE_RELATIONSHIP_AUD 
--

CREATE TABLE ENROLLEE_RELATIONSHIP_AUD(
    ID                       NUMBER           NOT NULL,
    REV                      NUMBER(10, 0)    NOT NULL,
    REVTYPE                  NUMBER(3, 0),
    SOURCE_ENROLEE_ID        NUMBER,
    TARGET_ENROLEE_ID        NUMBER,
    RELATIONSHIP_LKP         NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    RELATIONSHIP_HCP_LKP     NUMBER
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.ID IS 'Primary Key of the table'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.SOURCE_ENROLEE_ID IS 'Foreign key to the Enrollee table'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.TARGET_ENROLEE_ID IS 'Foreign key to the Enrollee table '
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.RELATIONSHIP_LKP IS 'Relationship. Foreign Key to the LOOKUPVALUE table. '
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.CREATION_TIMESTAMP IS 'The enrollee relationship is created on particular date'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP_AUD.RELATIONSHIP_HCP_LKP IS 'Foreign Key to the LOOKUPVALUE table. (Relationship to Household contact person)'
;
-- 
-- TABLE: ENROLLEE_UPDATE_SEND_STATUS 
--

CREATE TABLE ENROLLEE_UPDATE_SEND_STATUS(
    ID                        NUMBER           NOT NULL,
    ENROLLEE_ID               NUMBER,
    EXCHG_INDIV_IDENTIFIER    VARCHAR2(200),
    ENROLLEE_STAUS            NUMBER,
    AHBX_STATUS_CODE          VARCHAR2(20),
    AHBX_STATUS_DESC          VARCHAR2(500),
    CREATION_TIMESTAMP        TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP     TIMESTAMP(6),
    CREATED_BY                NUMBER,
    LAST_UPDATED_BY           NUMBER,
    CONSTRAINT PK_ENROLLEE_UPDATE_SEND_STATUS PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLEE_UPDATE_SEND_ALT  UNIQUE (ENROLLEE_ID, EXCHG_INDIV_IDENTIFIER, ENROLLEE_STAUS)
)
PARTITION BY HASH(id) PARTITIONS 16
;



COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.ID IS 'Primary Key'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.ENROLLEE_ID IS 'Foreign key from Enrollee'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.EXCHG_INDIV_IDENTIFIER IS 'Exchange Individual Identifier'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.ENROLLEE_STAUS IS 'Enrollee Status'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.AHBX_STATUS_CODE IS 'AHBX Status Code'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.AHBX_STATUS_DESC IS 'AHBX Status Description'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN ENROLLEE_UPDATE_SEND_STATUS.LAST_UPDATED_BY IS 'User did the last update'
;
-- 
-- TABLE: ENROLLMENT 
--

CREATE TABLE ENROLLMENT(
    ID                              NUMBER           NOT NULL,
    SPONSOR_NAME                    VARCHAR2(60),
    INSURER_NAME                    VARCHAR2(60),
    INSURER_TAX_ID_NUMBER           VARCHAR2(80),
    BENEFIT_EFFECTIVE_DATE          TIMESTAMP(6),
    BENEFIT_END_DATE                TIMESTAMP(6),
    APTC_AMT                        FLOAT(126),
    CSR_AMT                         FLOAT(126),
    GROUP_POLICY_NUMBER             VARCHAR2(50),
    FINANCIAL_ID                    NUMBER,
    PLAN_ID                         NUMBER,
    IND_ORDER_ITEMS_ID              NUMBER,
    ISSUER_ID                       NUMBER,
    EMPLOYER_ID                     NUMBER,
    EMPLOYEE_PLAN_ID                NUMBER,
    EXCHG_SUBSCRIBER_IDENTIFIER     VARCHAR2(200),
    ISSUER_SUBSCRIBER_IDENTIFIER    VARCHAR2(50),
    BROKER_TPA_FLAG                 VARCHAR2(2),
    AGENT_BROKER_NAME               VARCHAR2(60),
    CMS_PLAN_ID                     VARCHAR2(80),
    BROKER_FED_TAX_PAYER_ID         NUMBER(9, 0),
    BROKER_TPA_ACCOUNT_NUMBER_1     VARCHAR2(35),
    BROKER_TPA_ACCOUNT_NUMBER_2     VARCHAR2(35),
    INSURANCE_TYPE_LKP              NUMBER,
    ENROLLMENT_STATUS_LKP           NUMBER,
    ENROLLMENT_TYPE_LKP             NUMBER,
    WORKSITE_ID                     NUMBER,
    PLAN_COVERAGE_LEVEL             VARCHAR2(25),
    FAMILY_COVERAGE_TIER            VARCHAR2(25),
    EMPLOYEE_CONTRIBUTION           FLOAT(126),
    EMPLOYER_CONTRIBUTION           FLOAT(126),
    SPONSOR_TAX_ID_NUMBER           VARCHAR2(80),
    SPONSOR_EIN                     VARCHAR2(80),
    SUBSCRIBER_NAME                 VARCHAR2(60),
    HOUSEHOLD_CASE_ID               VARCHAR2(10),
    PLAN_NAME                       VARCHAR2(400),
    CREATED_BY                      NUMBER,
    LAST_UPDATED_BY                 NUMBER,
    CREATION_TIMESTAMP              TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP           TIMESTAMP(6),
    ESIGNATURE_ID                   NUMBER,
    GROSS_PREMIUM_AMT               FLOAT(126),
    NET_PREMIUM_AMT                 FLOAT(126),
    SADP                            CHAR(1),
    ASSISTER_BROKER_ID              NUMBER,
    BROKER_ROLE                     VARCHAR2(20),
    PLAN_LEVEL                      VARCHAR2(20),
    PLAN_NUMBER                     VARCHAR2(100 CHAR),
    ENROLLMENT_CONFIRMATION_DATE    TIMESTAMP(6),
    EMPLOYER_CASE_ID                VARCHAR2(20 CHAR),
    CONSTRAINT PK_ENROLLMENT PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLMENT_ALT  UNIQUE (PLAN_ID, ISSUER_ID, EXCHG_SUBSCRIBER_IDENTIFIER, HOUSEHOLD_CASE_ID, ENROLLMENT_STATUS_LKP)
)
PARTITION BY HASH(id) PARTITIONS 16
;



COMMENT ON TABLE ENROLLMENT IS 'This entity contains the enrollment information including the order items, employer, lookupvalues information'
;
COMMENT ON COLUMN ENROLLMENT.ID IS 'Primary Key'
;
COMMENT ON COLUMN ENROLLMENT.SPONSOR_NAME IS 'Sponsor name'
;
COMMENT ON COLUMN ENROLLMENT.INSURER_NAME IS 'Insurer name'
;
COMMENT ON COLUMN ENROLLMENT.INSURER_TAX_ID_NUMBER IS 'Insurer tax id'
;
COMMENT ON COLUMN ENROLLMENT.BENEFIT_EFFECTIVE_DATE IS 'The effective date of the benefit'
;
COMMENT ON COLUMN ENROLLMENT.BENEFIT_END_DATE IS 'The end date of the benefit'
;
COMMENT ON COLUMN ENROLLMENT.APTC_AMT IS 'APTC (Advanced Payments of the premium tax credit) amount'
;
COMMENT ON COLUMN ENROLLMENT.CSR_AMT IS 'CSR (Cost-sharing reductions) amount'
;
COMMENT ON COLUMN ENROLLMENT.GROUP_POLICY_NUMBER IS 'Group policy number'
;
COMMENT ON COLUMN ENROLLMENT.FINANCIAL_ID IS 'Financial id'
;
COMMENT ON COLUMN ENROLLMENT.PLAN_ID IS 'Foreign KEY to the PLAN table'
;
COMMENT ON COLUMN ENROLLMENT.IND_ORDER_ITEMS_ID IS 'Individual order �s items id'
;
COMMENT ON COLUMN ENROLLMENT.ISSUER_ID IS 'Issuer id'
;
COMMENT ON COLUMN ENROLLMENT.EMPLOYER_ID IS 'Employer id'
;
COMMENT ON COLUMN ENROLLMENT.EMPLOYEE_PLAN_ID IS 'Employee plan id'
;
COMMENT ON COLUMN ENROLLMENT.EXCHG_SUBSCRIBER_IDENTIFIER IS 'Exchange subscriber identifier'
;
COMMENT ON COLUMN ENROLLMENT.ISSUER_SUBSCRIBER_IDENTIFIER IS 'Issuer subscriber identifier'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_TPA_FLAG IS 'BrokerTPA flag'
;
COMMENT ON COLUMN ENROLLMENT.AGENT_BROKER_NAME IS 'Agent/broker name'
;
COMMENT ON COLUMN ENROLLMENT.CMS_PLAN_ID IS 'CMS plan id'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_FED_TAX_PAYER_ID IS 'Broker�s Federal tax id'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_TPA_ACCOUNT_NUMBER_1 IS 'Broker�s  TPA (Third-party Administrator) account number 1'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_TPA_ACCOUNT_NUMBER_2 IS 'Broker�s  TPA (Third-party Administrator) account number 2'
;
COMMENT ON COLUMN ENROLLMENT.INSURANCE_TYPE_LKP IS 'Insurance type lkp'
;
COMMENT ON COLUMN ENROLLMENT.ENROLLMENT_STATUS_LKP IS 'Enrollment status lkp'
;
COMMENT ON COLUMN ENROLLMENT.ENROLLMENT_TYPE_LKP IS 'Enrollment type from lookup table'
;
COMMENT ON COLUMN ENROLLMENT.WORKSITE_ID IS 'WORKSITE location address'
;
COMMENT ON COLUMN ENROLLMENT.PLAN_COVERAGE_LEVEL IS 'Plan coverage level'
;
COMMENT ON COLUMN ENROLLMENT.FAMILY_COVERAGE_TIER IS 'Family coverage tier'
;
COMMENT ON COLUMN ENROLLMENT.EMPLOYEE_CONTRIBUTION IS 'Employee contribution'
;
COMMENT ON COLUMN ENROLLMENT.EMPLOYER_CONTRIBUTION IS 'Employee contribution'
;
COMMENT ON COLUMN ENROLLMENT.SPONSOR_TAX_ID_NUMBER IS 'Sponsor tax id number'
;
COMMENT ON COLUMN ENROLLMENT.SPONSOR_EIN IS 'Sponsor EIN  (Federal Tax ID )'
;
COMMENT ON COLUMN ENROLLMENT.SUBSCRIBER_NAME IS 'Subscriber name'
;
COMMENT ON COLUMN ENROLLMENT.HOUSEHOLD_CASE_ID IS 'Household case id'
;
COMMENT ON COLUMN ENROLLMENT.PLAN_NAME IS 'Plan name'
;
COMMENT ON COLUMN ENROLLMENT.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN ENROLLMENT.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN ENROLLMENT.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN ENROLLMENT.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLMENT.ESIGNATURE_ID IS 'Foreign Key of Enrollment esignature table ID'
;
COMMENT ON COLUMN ENROLLMENT.GROSS_PREMIUM_AMT IS 'Gross Premium Amount'
;
COMMENT ON COLUMN ENROLLMENT.NET_PREMIUM_AMT IS 'Net Premium Amount'
;
COMMENT ON COLUMN ENROLLMENT.SADP IS 'Stand Alone Dental Plan(SADP)-Plan Selection flow for pediatric dental'
;
COMMENT ON COLUMN ENROLLMENT.PLAN_NUMBER IS 'plan number'
;
-- 
-- TABLE: ENROLLMENT_AUD 
--

CREATE TABLE ENROLLMENT_AUD(
    ID                              NUMBER           NOT NULL,
    REV                             NUMBER(10, 0)    NOT NULL,
    REVTYPE                         NUMBER(3, 0),
    SPONSOR_NAME                    VARCHAR2(60),
    INSURER_NAME                    VARCHAR2(60),
    INSURER_TAX_ID_NUMBER           VARCHAR2(80),
    BENEFIT_EFFECTIVE_DATE          TIMESTAMP(6),
    BENEFIT_END_DATE                TIMESTAMP(6),
    APTC_AMT                        FLOAT(126),
    CSR_AMT                         FLOAT(126),
    GROUP_POLICY_NUMBER             VARCHAR2(50),
    FINANCIAL_ID                    NUMBER,
    PLAN_ID                         NUMBER,
    IND_ORDER_ITEMS_ID              NUMBER,
    ISSUER_ID                       NUMBER,
    EMPLOYER_ID                     NUMBER,
    EMPLOYEE_PLAN_ID                NUMBER,
    EXCHG_SUBSCRIBER_IDENTIFIER     VARCHAR2(200),
    ISSUER_SUBSCRIBER_IDENTIFIER    VARCHAR2(50),
    BROKER_TPA_FLAG                 VARCHAR2(2),
    AGENT_BROKER_NAME               VARCHAR2(60),
    CMS_PLAN_ID                     VARCHAR2(80),
    BROKER_FED_TAX_PAYER_ID         NUMBER(9, 0),
    BROKER_TPA_ACCOUNT_NUMBER_1     VARCHAR2(35),
    BROKER_TPA_ACCOUNT_NUMBER_2     VARCHAR2(35),
    INSURANCE_TYPE_LKP              NUMBER,
    ENROLLMENT_STATUS_LKP           NUMBER,
    ENROLLMENT_TYPE_LKP             NUMBER,
    WORKSITE_ID                     NUMBER,
    PLAN_COVERAGE_LEVEL             VARCHAR2(25),
    FAMILY_COVERAGE_TIER            VARCHAR2(25),
    EMPLOYEE_CONTRIBUTION           FLOAT(126),
    EMPLOYER_CONTRIBUTION           FLOAT(126),
    SPONSOR_TAX_ID_NUMBER           VARCHAR2(80),
    SPONSOR_EIN                     VARCHAR2(80),
    SUBSCRIBER_NAME                 VARCHAR2(60),
    HOUSEHOLD_CASE_ID               VARCHAR2(10),
    PLAN_NAME                       VARCHAR2(400),
    CREATED_BY                      NUMBER,
    LAST_UPDATED_BY                 NUMBER,
    CREATION_TIMESTAMP              TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP           TIMESTAMP(6),
    RESPONSIBLE_PERSON_ID           NUMBER,
    HOUSEHOLD_CONTACT_ID            NUMBER,
    ESIGNATURE_ID                   NUMBER,
    GROSS_PREMIUM_AMT               FLOAT(126),
    NET_PREMIUM_AMT                 FLOAT(126),
    SADP                            CHAR(1),
    ASSISTER_BROKER_ID              NUMBER,
    BROKER_ROLE                     VARCHAR2(20),
    PLAN_LEVEL                      VARCHAR2(20),
    PLAN_NUMBER                     VARCHAR2(100 CHAR),
    ENROLLMENT_CONFIRMATION_DATE    TIMESTAMP(6),
    EMPLOYER_CASE_ID                VARCHAR2(20 CHAR)
)
PARTITION BY HASH(id) PARTITIONS 16
;



COMMENT ON COLUMN ENROLLMENT_AUD.ID IS 'Primary Key'
;
COMMENT ON COLUMN ENROLLMENT_AUD.SPONSOR_NAME IS 'Sponsor name'
;
COMMENT ON COLUMN ENROLLMENT_AUD.INSURER_NAME IS 'Insurer name'
;
COMMENT ON COLUMN ENROLLMENT_AUD.INSURER_TAX_ID_NUMBER IS 'Insurer tax id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BENEFIT_EFFECTIVE_DATE IS 'The effective date of the benefit'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BENEFIT_END_DATE IS 'The end date of the benefit'
;
COMMENT ON COLUMN ENROLLMENT_AUD.APTC_AMT IS 'APTC (Advanced Payments of the premium tax credit) amount'
;
COMMENT ON COLUMN ENROLLMENT_AUD.CSR_AMT IS 'CSR (Cost-sharing reductions) amount'
;
COMMENT ON COLUMN ENROLLMENT_AUD.GROUP_POLICY_NUMBER IS 'Group policy number'
;
COMMENT ON COLUMN ENROLLMENT_AUD.FINANCIAL_ID IS 'Financial id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.PLAN_ID IS 'Foreign KEY to the PLAN table'
;
COMMENT ON COLUMN ENROLLMENT_AUD.IND_ORDER_ITEMS_ID IS 'Individual order �s items id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.ISSUER_ID IS 'Issuer id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.EMPLOYER_ID IS 'Employer id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.EMPLOYEE_PLAN_ID IS 'Employee plan id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.EXCHG_SUBSCRIBER_IDENTIFIER IS 'Exchange subscriber identifier'
;
COMMENT ON COLUMN ENROLLMENT_AUD.ISSUER_SUBSCRIBER_IDENTIFIER IS 'Issuer subscriber identifier'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_TPA_FLAG IS 'BrokerTPA flag'
;
COMMENT ON COLUMN ENROLLMENT_AUD.AGENT_BROKER_NAME IS 'Agent/broker name'
;
COMMENT ON COLUMN ENROLLMENT_AUD.CMS_PLAN_ID IS 'CMS plan id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_FED_TAX_PAYER_ID IS 'Broker�s Federal tax id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_TPA_ACCOUNT_NUMBER_1 IS 'Broker�s  TPA (Third-party Administrator) account number 1'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_TPA_ACCOUNT_NUMBER_2 IS 'Broker�s  TPA (Third-party Administrator) account number 2'
;
COMMENT ON COLUMN ENROLLMENT_AUD.INSURANCE_TYPE_LKP IS 'Insurance type lkp'
;
COMMENT ON COLUMN ENROLLMENT_AUD.ENROLLMENT_STATUS_LKP IS 'Enrollment status lkp'
;
COMMENT ON COLUMN ENROLLMENT_AUD.ENROLLMENT_TYPE_LKP IS 'Enrollment type from lookup table'
;
COMMENT ON COLUMN ENROLLMENT_AUD.WORKSITE_ID IS 'WORKSITE location address'
;
COMMENT ON COLUMN ENROLLMENT_AUD.PLAN_COVERAGE_LEVEL IS 'Plan coverage level'
;
COMMENT ON COLUMN ENROLLMENT_AUD.FAMILY_COVERAGE_TIER IS 'Family coverage tier'
;
COMMENT ON COLUMN ENROLLMENT_AUD.EMPLOYEE_CONTRIBUTION IS 'Employee contribution'
;
COMMENT ON COLUMN ENROLLMENT_AUD.EMPLOYER_CONTRIBUTION IS 'Employee contribution'
;
COMMENT ON COLUMN ENROLLMENT_AUD.SPONSOR_TAX_ID_NUMBER IS 'Sponsor tax id number'
;
COMMENT ON COLUMN ENROLLMENT_AUD.SPONSOR_EIN IS 'Sponsor EIN  (Federal Tax ID )'
;
COMMENT ON COLUMN ENROLLMENT_AUD.SUBSCRIBER_NAME IS 'Subscriber name'
;
COMMENT ON COLUMN ENROLLMENT_AUD.HOUSEHOLD_CASE_ID IS 'Household case id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.PLAN_NAME IS 'Plan name'
;
COMMENT ON COLUMN ENROLLMENT_AUD.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN ENROLLMENT_AUD.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN ENROLLMENT_AUD.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN ENROLLMENT_AUD.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLMENT_AUD.RESPONSIBLE_PERSON_ID IS 'Foreign Key to the ENROLLEE  table '
;
COMMENT ON COLUMN ENROLLMENT_AUD.HOUSEHOLD_CONTACT_ID IS 'Foreign Key of Enrollee table ID'
;
COMMENT ON COLUMN ENROLLMENT_AUD.ESIGNATURE_ID IS 'Foreign Key of Enrollment esignature table ID'
;
COMMENT ON COLUMN ENROLLMENT_AUD.GROSS_PREMIUM_AMT IS 'Gross Premium Amount'
;
COMMENT ON COLUMN ENROLLMENT_AUD.NET_PREMIUM_AMT IS 'Net Premium Amount'
;
COMMENT ON COLUMN ENROLLMENT_AUD.SADP IS 'Stand Alone Dental Plan(SADP)-Plan Selection flow for pediatric dental'
;
COMMENT ON COLUMN ENROLLMENT_AUD.PLAN_NUMBER IS 'plan number'
;
-- 
-- TABLE: ENROLLMENT_ESIGNATURE 
--

CREATE TABLE ENROLLMENT_ESIGNATURE(
    ID               NUMBER    NOT NULL,
    ENROLLMENT_ID    NUMBER,
    ESIGNATURE_ID    NUMBER,
    CONSTRAINT PK_ENROLL_ESIGNATURE_ID PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLMENT_ESIGNATURE_ALT  UNIQUE (ENROLLMENT_ID, ESIGNATURE_ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE ENROLLMENT_ESIGNATURE IS 'This entity contains the joining information tables of enrollment and esignature'
;
COMMENT ON COLUMN ENROLLMENT_ESIGNATURE.ID IS 'Primary Key of the table'
;
COMMENT ON COLUMN ENROLLMENT_ESIGNATURE.ENROLLMENT_ID IS 'Foreign Key of the Enrollment table'
;
COMMENT ON COLUMN ENROLLMENT_ESIGNATURE.ESIGNATURE_ID IS 'Foreign Key of of esignature table'
;
-- 
-- TABLE: ENROLLMENT_EVENT 
--

CREATE TABLE ENROLLMENT_EVENT(
    ID                            NUMBER           NOT NULL,
    ENROLLEE_ID                   NUMBER,
    ENROLLMENT_ID                 NUMBER,
    CREATION_TIMESTAMP            TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP         TIMESTAMP(6),
    EVENT_TYPE_LKP                NUMBER,
    EVENT_REASON_LKP              NUMBER,
    CREATED_BY                    NUMBER(10, 0),
    LAST_UPDATED_BY               NUMBER(10, 0),
    SPCL_ENROLLMENT_REASON_LKP    NUMBER,
    CONSTRAINT PK_ENROLLMENT_EVENT PRIMARY KEY (ID),
    CONSTRAINT UK_ENROLLMENT_EVENT_ALT  UNIQUE (ENROLLMENT_ID, ENROLLEE_ID, EVENT_TYPE_LKP, EVENT_REASON_LKP, CREATION_TIMESTAMP)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE ENROLLMENT_EVENT IS 'This entity contains the enrollment event like event type, event reason lookup information'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.ID IS 'Primary Key'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.ENROLLEE_ID IS 'Enrollee id'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.ENROLLMENT_ID IS 'Enrollment id'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.EVENT_TYPE_LKP IS 'Event type lkp'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.EVENT_REASON_LKP IS 'Event reason lkp'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.CREATED_BY IS 'User id who created this record'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN ENROLLMENT_EVENT.SPCL_ENROLLMENT_REASON_LKP IS 'Special Enrollment Reason Code'
;
-- 
-- TABLE: ENROLLMENT_EVENT_AUD 
--

CREATE TABLE ENROLLMENT_EVENT_AUD(
    ID                            NUMBER(10, 0)    NOT NULL,
    REV                           NUMBER(10, 0)    NOT NULL,
    REVTYPE                       NUMBER(3, 0),
    ENROLLEE_ID                   NUMBER,
    ENROLLMENT_ID                 NUMBER,
    EVENT_TYPE_LKP                NUMBER,
    EVENT_REASON_LKP              NUMBER,
    SPCL_ENROLLMENT_REASON_LKP    NUMBER,
    CREATED_BY                    NUMBER(10, 0),
    LAST_UPDATED_BY               NUMBER(10, 0),
    CREATION_TIMESTAMP            TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP         TIMESTAMP(6)
)
PARTITION BY HASH(id) PARTITIONS 4
;



-- 
-- TABLE: ESIGNATURE 
--

CREATE TABLE ESIGNATURE(
    ID             NUMBER(10, 0)     NOT NULL,
    MODULE_NAME    VARCHAR2(100),
    REF_ID         NUMBER(10, 0),
    FIRST_NAME     VARCHAR2(100),
    LAST_NAME      VARCHAR2(100),
    STATEMENT      VARCHAR2(1020),
    ESIGN          VARCHAR2(500),
    ESIGN_DATE     DATE,
    ESIGN_BY       NUMBER(10, 0),
    CONSTRAINT PK_ESIGNATURE_ID PRIMARY KEY (ID),
    CONSTRAINT UK_ESIGNATURE_ALT  UNIQUE (MODULE_NAME, REF_ID, ESIGN_DATE, ESIGN_BY)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE ESIGNATURE IS 'This entity maintains Esignature details'
;
COMMENT ON COLUMN ESIGNATURE.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN ESIGNATURE.REF_ID IS 'generated to store the enrollment id referred'
;
COMMENT ON COLUMN ESIGNATURE.FIRST_NAME IS 'generated to store First Name'
;
COMMENT ON COLUMN ESIGNATURE.LAST_NAME IS 'generated to store Last Name'
;
COMMENT ON COLUMN ESIGNATURE.STATEMENT IS 'generated to store esignature statement'
;
COMMENT ON COLUMN ESIGNATURE.ESIGN IS 'generated to store esignature'
;
COMMENT ON COLUMN ESIGNATURE.ESIGN_DATE IS 'generated to store esignature date '
;
COMMENT ON COLUMN ESIGNATURE.ESIGN_BY IS 'generated to store esignature by'
;
-- 
-- TABLE: ESIGNATURE_AUD 
--

CREATE TABLE ESIGNATURE_AUD(
    ID             NUMBER(10, 0)     NOT NULL,
    REV            NUMBER(10, 0)     NOT NULL,
    REVTYPE        NUMBER(3, 0),
    MODULE_NAME    VARCHAR2(100),
    REF_ID         NUMBER(10, 0),
    FIRST_NAME     VARCHAR2(100),
    LAST_NAME      VARCHAR2(100),
    STATEMENT      VARCHAR2(1020),
    ESIGN          VARCHAR2(500),
    ESIGN_DATE     DATE,
    ESIGN_BY       NUMBER(10, 0)
)
PARTITION BY HASH(id) PARTITIONS 4
;



-- 
-- TABLE: EXCHG_PARTNERLOOKUP 
--

CREATE TABLE EXCHG_PARTNERLOOKUP(
    ID                       NUMBER(10, 0)    NOT NULL,
    INSURERNAME              VARCHAR2(50),
    ISA05                    VARCHAR2(2),
    ISA06                    VARCHAR2(15),
    ISA07                    VARCHAR2(2),
    ISA08                    VARCHAR2(15),
    ISA15                    VARCHAR2(1),
    GS02                     VARCHAR2(15),
    GS03                     VARCHAR2(15),
    GS08                     VARCHAR2(12),
    ST01                     VARCHAR2(15),
    CREATION_TIMESTAMP       TIMESTAMP(6)     NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    PICKUP_LOCATION          VARCHAR2(500),
    DROPOFF_LOCATION         VARCHAR2(500),
    ISA13                    NUMBER(9, 0),
    HIOS_ISSUER_ID           VARCHAR2(5 CHAR),
    CONSTRAINT PK_EXCHG_PARTNERLOOKUP PRIMARY KEY (ID)
)
;



COMMENT ON TABLE EXCHG_PARTNERLOOKUP IS 'Information on exchange data (EDI) with partners'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.ISA05 IS 'ISA sender qualifier'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.ISA06 IS 'ISA sender qualifier ID'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.ISA07 IS 'ISA receiver qualifier'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.ISA08 IS 'ISA receiver qualifier ID'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.ISA15 IS 'ISA T (Test) or P (Production)'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.GS02 IS 'GS sender ID'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.GS03 IS 'GS receiver ID'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.GS08 IS 'GS transaction verison'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.ST01 IS 'Transaction set'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.PICKUP_LOCATION IS 'Column to store pickup location of xml or edi files'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.DROPOFF_LOCATION IS 'Column to store dropoff location of xml or edi files'
;
COMMENT ON COLUMN EXCHG_PARTNERLOOKUP.HIOS_ISSUER_ID IS 'Change column type from NUMBER(5) to VARCHAR2(5 CHAR)'
;
-- 
-- TABLE: EXTERNAL_EMPLOYER 
--

CREATE TABLE EXTERNAL_EMPLOYER(
    ID                             NUMBER(10, 0)    NOT NULL,
    EMPLOYER_CASE_ID               NUMBER(10, 0)    NOT NULL,
    APPLICATION_INITIATED_DATE     TIMESTAMP(6),
    APPLICATION_END_DATE           TIMESTAMP(6),
    APPLICATION_SUBMIT_DATE        TIMESTAMP(6),
    APPLICATION_WITHDRAWAL_DATE    TIMESTAMP(6),
    AVG_ANNUAL_SALARY              FLOAT(126)       DEFAULT 0,
    CONTACT_FIRST_NAME             VARCHAR2(50 CHAR),
    CONTACT_LAST_NAME              VARCHAR2(50 CHAR),
    BUSINESS_LEGAL_NAME            VARCHAR2(100 CHAR),
    CITY                           VARCHAR2(50 CHAR),
    COUNTRY                        VARCHAR2(100 CHAR),
    ADDRESS1                       VARCHAR2(100 CHAR),
    ADDRESS2                       VARCHAR2(100 CHAR),
    STATE                          VARCHAR2(2 CHAR),
    ZIP                            VARCHAR2(10 CHAR),
    CONTACT_EMAIL                  VARCHAR2(255 CHAR),
    CONTACT_NUMBER                 NUMBER(30, 0),
    ENROLLMENT_STATUS              VARCHAR2(50 CHAR),
    ELIGIBILITY_STATUS             VARCHAR2(50 CHAR),
    TOTAL_ENROLLEES                NUMBER(10, 0),
    FEDERAL_EIN                    NUMBER(20, 0),
    STATE_EIN                      NUMBER(20, 0),
    COMMUNICATION_PREF             VARCHAR2(50 CHAR),
    REPORTING_NAME                 VARCHAR2(100 CHAR),
    TOTAL_EMPLOYEES                NUMBER(10, 0),
    FULLTIME_EMP                   NUMBER(10, 0),
    CONSTRAINT PK_EXTERNAL_EMPLOYER PRIMARY KEY (ID)
)
;



COMMENT ON TABLE EXTERNAL_EMPLOYER IS 'This entity maintains information of external employers'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.EMPLOYER_CASE_ID IS 'AHBX Employer id'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.APPLICATION_INITIATED_DATE IS 'Application initiated date'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.APPLICATION_END_DATE IS 'Application end date'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.APPLICATION_SUBMIT_DATE IS 'Application submit date'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.APPLICATION_WITHDRAWAL_DATE IS 'Application withdrawal date'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.AVG_ANNUAL_SALARY IS 'Average annual salary of the external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.CONTACT_FIRST_NAME IS 'First name of external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.CONTACT_LAST_NAME IS 'Last name of external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.BUSINESS_LEGAL_NAME IS 'Business name of external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.CITY IS 'City field of address'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.COUNTRY IS 'Country'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.ADDRESS1 IS 'Street Address of the External Employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.ADDRESS2 IS 'Street Address of the External Employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.STATE IS 'State'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.ZIP IS 'Zipcode'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.CONTACT_EMAIL IS 'Email of the external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.CONTACT_NUMBER IS 'Contact no. of the external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.ENROLLMENT_STATUS IS 'Enrollement status for the external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.ELIGIBILITY_STATUS IS 'Eligibility status for this record'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.TOTAL_ENROLLEES IS 'Total no. of enrollees'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.FEDERAL_EIN IS 'Organisation Federal reg. Number'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.STATE_EIN IS 'Organisation State reg. Number'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.COMMUNICATION_PREF IS 'Employer preference of communication'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.REPORTING_NAME IS 'Reporting name of the external employer'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.TOTAL_EMPLOYEES IS 'Number of Employees in Organisation'
;
COMMENT ON COLUMN EXTERNAL_EMPLOYER.FULLTIME_EMP IS 'Full time employees count'
;
-- 
-- TABLE: EXTERNAL_INDIVIDUAL 
--

CREATE TABLE EXTERNAL_INDIVIDUAL(
    ID                          NUMBER(10, 0)    NOT NULL,
    INDIVIDUAL_CASE_ID          NUMBER(10, 0)    NOT NULL,
    FIRSTNAME                   VARCHAR2(50 CHAR),
    MIDDLENAME                  VARCHAR2(50 CHAR),
    LASTNAME                    VARCHAR2(50 CHAR),
    CASE_START_DATE             TIMESTAMP(6),
    CASE_END_DATE               TIMESTAMP(6),
    PREF_COMM_METHOD            VARCHAR2(50 CHAR),
    PREF_SPOKEN_LANG            VARCHAR2(50 CHAR),
    PREF_WRITTEN_LANG           VARCHAR2(50 CHAR),
    GENDER                      VARCHAR2(1 CHAR),
    SSN                         NUMBER(9, 0),
    CITIZEN                     VARCHAR2(50 CHAR),
    DOB                         TIMESTAMP(6),
    EMAILID                     VARCHAR2(255 CHAR),
    PRI_PHONE                   NUMBER(10, 0),
    ADDRESSLINE1                VARCHAR2(255 CHAR),
    ADDRESSLINE2                VARCHAR2(100 CHAR),
    CITY                        VARCHAR2(100 CHAR),
    STATE                       VARCHAR2(2 CHAR),
    ZIP                         VARCHAR2(10 CHAR),
    SUFFIX                      VARCHAR2(10 CHAR),
    HEADOFHOUSEHOLDNAME         VARCHAR2(150 CHAR),
    NUMBEROFHOUSEHOLDMEMBERS    NUMBER(2, 0),
    APPLICATIONSTATUS           VARCHAR2(100 CHAR),
    CASESTATUS                  VARCHAR2(100 CHAR),
    CONTACTADDRESSCOUNTY        VARCHAR2(2 CHAR),
    COUNTY                      VARCHAR2(100 CHAR),
    CONSTRAINT PK_EXTERNAL_INDIVIDUAL PRIMARY KEY (ID)
)
;



COMMENT ON TABLE EXTERNAL_INDIVIDUAL IS 'THIS TABLE CONTAINS EXTERNAL INDIVIDUAL DATA'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.ID IS 'ID. PRIMARY KEY ASSIGNED UNIQUELY TO EACH RECORD'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.INDIVIDUAL_CASE_ID IS 'INDIVIDUAL_CASE_ID ASSOCIATED WITH EACH INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.FIRSTNAME IS 'FIRST NAME OF INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.MIDDLENAME IS 'MIDDLE NAME OF INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.LASTNAME IS 'LAST NAME OF INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.CASE_START_DATE IS 'START DATE FOR INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.CASE_END_DATE IS 'END DATE FOR INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.PREF_COMM_METHOD IS 'PREFFERED COMMUNICATION METHOD'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.PREF_SPOKEN_LANG IS 'PREFFERED COMMUNICATION LANGUAGE'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.PREF_WRITTEN_LANG IS 'PREFFERED WRITTEN LANGUAGE'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.GENDER IS 'GENDER OF INDIVIDUAL. M FOR MALE,F FOR FEMALE'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.SSN IS 'SOCIAL SECURITY NUMBER OF INDIVIDUAL.'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.CITIZEN IS 'CITIZEN'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.DOB IS 'DATE OF BIRTH OF INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.EMAILID IS 'EMAILID OF INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.PRI_PHONE IS 'PRIMARY PHONE OF THE INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.ADDRESSLINE1 IS 'ADDRESS LINE 1 OF THE CONTACT ADDRESS FOR THE INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.ADDRESSLINE2 IS 'ADDRESS LINE 2 OF THE CONTACT ADDRESS FOR THE INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.CITY IS 'CITY'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.STATE IS 'STATE'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.ZIP IS 'ZIP CODE'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.SUFFIX IS 'SUFFIX FOR INDIVIDUAL'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.HEADOFHOUSEHOLDNAME IS 'NAME OF HEAD OF HOUSEHOLD'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.NUMBEROFHOUSEHOLDMEMBERS IS 'NUMBER OF HOUSEHOLD MEMBERS'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.APPLICATIONSTATUS IS 'Status of individual Application'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.CASESTATUS IS 'Status of case for individual'
;
COMMENT ON COLUMN EXTERNAL_INDIVIDUAL.CONTACTADDRESSCOUNTY IS 'Address County'
;
-- 
-- TABLE: FACILITY 
--

CREATE TABLE FACILITY(
    ID                         NUMBER            NOT NULL,
    CREATION_TIMESTAMP         TIMESTAMP(6)      NOT NULL,
    MEDICAL_GROUP              VARCHAR2(1000),
    TYPE                       VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP      TIMESTAMP(6)      NOT NULL,
    WEBSITE_URL                VARCHAR2(1000),
    PROVIDER_ID                NUMBER,
    AHA_ID                     VARCHAR2(100),
    FACILITY_TYPE_INDICATOR    VARCHAR2(1000),
    CONSTRAINT PK_FACILITY PRIMARY KEY (ID)
)
;



COMMENT ON TABLE FACILITY IS 'This table describes information about a medical facility such as hospital, radiology lab, x-ray lab.'
;
COMMENT ON COLUMN FACILITY.ID IS 'Primary Key'
;
COMMENT ON COLUMN FACILITY.CREATION_TIMESTAMP IS 'Creation time/date for this record.'
;
COMMENT ON COLUMN FACILITY.MEDICAL_GROUP IS 'Name of the Medical Group (if this facility is a medical group)'
;
COMMENT ON COLUMN FACILITY.TYPE IS 'Type of medical facility'
;
COMMENT ON COLUMN FACILITY.LAST_UPDATE_TIMESTAMP IS 'Last updated time/date for this record.'
;
COMMENT ON COLUMN FACILITY.WEBSITE_URL IS 'URL for the website for this medical facility.'
;
COMMENT ON COLUMN FACILITY.PROVIDER_ID IS 'Foreign Key into the provider table. A medical facility is a type of provider.'
;
COMMENT ON COLUMN FACILITY.AHA_ID IS 'American Hospital Association ID (in case this facility is a hospital)'
;
COMMENT ON COLUMN FACILITY.FACILITY_TYPE_INDICATOR IS 'Should be null for individuals/practitioners, and only populated for facilities.Use | to delimit  facility types'
;
-- 
-- TABLE: FACILITY_ADDRESS 
--

CREATE TABLE FACILITY_ADDRESS(
    ID                       NUMBER            NOT NULL,
    ADDRESS1                 VARCHAR2(1000),
    CITY                     VARCHAR2(80),
    COUNTY                   VARCHAR2(80),
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    EXTENDED_ZIPCODE         VARCHAR2(10),
    FAX                      VARCHAR2(60),
    PHONE_NUMBER             VARCHAR2(25),
    STATE                    VARCHAR2(8),
    STATUS                   VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    ZIP                      VARCHAR2(40),
    FACILITY_ID              NUMBER,
    ADDRESS2                 VARCHAR2(1000),
    LATTITUDE                NUMBER(15, 9),
    LONGITUDE                NUMBER(15, 9),
    CONSTRAINT PK_FACILITY_ADDRESS PRIMARY KEY (ID)
)
;



COMMENT ON TABLE FACILITY_ADDRESS IS 'This table contains the address information for a medical facility.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.ID IS 'Primary Key'
;
COMMENT ON COLUMN FACILITY_ADDRESS.ADDRESS1 IS 'Part 1 of the medical facility address'
;
COMMENT ON COLUMN FACILITY_ADDRESS.CITY IS 'City where the medical facility is located.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.COUNTY IS 'County where the medical facility is located'
;
COMMENT ON COLUMN FACILITY_ADDRESS.CREATION_TIMESTAMP IS 'Creation date time for this database record.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.EXTENDED_ZIPCODE IS 'Extended (9-digit) zip code.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.FAX IS 'Fax number for the medical faciltiy'
;
COMMENT ON COLUMN FACILITY_ADDRESS.PHONE_NUMBER IS 'Phone number for the medical facility.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.STATE IS 'State where the medical facility is located.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.STATUS IS 'Status of the medical facility - Open, Closed etc.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.LAST_UPDATE_TIMESTAMP IS 'Last updated time/date for this database record.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.ZIP IS '5 digit zip code for medical facilitys location.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.FACILITY_ID IS 'Foreign Key into the FACILITY table.'
;
COMMENT ON COLUMN FACILITY_ADDRESS.ADDRESS2 IS 'Part 2 of the medical facility address.'
;
-- 
-- TABLE: FACILITY_SPECIALITY_RELATION 
--

CREATE TABLE FACILITY_SPECIALITY_RELATION(
    ID              NUMBER    NOT NULL,
    FACILITY_ID     NUMBER,
    SPECIALTY_ID    NUMBER,
    CONSTRAINT PK_FACILITY_SP_REL PRIMARY KEY (ID)
)
;



COMMENT ON TABLE FACILITY_SPECIALITY_RELATION IS 'This table describes the relationship between a medical facility and its area of specialization. Area of specialization is described in a table called SPECIALTY.'
;
COMMENT ON COLUMN FACILITY_SPECIALITY_RELATION.ID IS 'Primary Key'
;
COMMENT ON COLUMN FACILITY_SPECIALITY_RELATION.FACILITY_ID IS 'Foreign Key into the FACILITY table'
;
COMMENT ON COLUMN FACILITY_SPECIALITY_RELATION.SPECIALTY_ID IS 'Foreign Key into the SPECIALTY table'
;
-- 
-- TABLE: FORMULARY 
--

CREATE TABLE FORMULARY(
    ID                       NUMBER(10, 0)    NOT NULL,
    FORMULARY_ID             VARCHAR2(200)    NOT NULL,
    FORMULARY_URL            VARCHAR2(800)    NOT NULL,
    ISSUER_ID                NUMBER(10, 0)    NOT NULL,
    CREATION_TIMESTAMP       TIMESTAMP(6)     NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    CONSTRAINT PK_FORMULARY PRIMARY KEY (ID)
)
;



COMMENT ON TABLE FORMULARY IS 'This table describes information about Formulary.'
;
COMMENT ON COLUMN FORMULARY.ID IS 'Primary key for FORMULARY table.'
;
COMMENT ON COLUMN FORMULARY.FORMULARY_ID IS 'SERFF - Unique identifier for formulary.'
;
COMMENT ON COLUMN FORMULARY.FORMULARY_URL IS 'SERFF - URL/web address for your formulary document.'
;
COMMENT ON COLUMN FORMULARY.ISSUER_ID IS 'Foreign Key linking to Issuers table.'
;
COMMENT ON COLUMN FORMULARY.CREATION_TIMESTAMP IS 'Record creation timestamp.'
;
COMMENT ON COLUMN FORMULARY.LAST_UPDATE_TIMESTAMP IS 'Record updated timestamp.'
;
-- 
-- TABLE: FORMULARY_COST_SHARE_BENEFIT 
--

CREATE TABLE FORMULARY_COST_SHARE_BENEFIT(
    ID                               NUMBER          NOT NULL,
    FORMULARY_ID                     NUMBER          NOT NULL,
    ONE_MON_OUT_NW_RETAIL_OFFERED    CHAR(1)         DEFAULT 'N' NOT NULL,
    THREE_MON_IN_NW_MAIL_OFFERED     CHAR(1)         DEFAULT 'N' NOT NULL,
    THREE_MON_OUT_NW_MAIL_OFFERED    CHAR(1)         DEFAULT 'N' NOT NULL,
    CREATION_TIMESTAMP               TIMESTAMP(6)    NOT NULL,
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6)    NOT NULL,
    CONSTRAINT PK_FORMULARY_COST_SHARE_BEN PRIMARY KEY (ID)
)
;



COMMENT ON TABLE FORMULARY_COST_SHARE_BENEFIT IS 'Table FORMULARY_COST_SHARE_BENEFIT persists information about benefit offerings for a formulary data provided by SERFF.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.ID IS 'Primary key for FORMULARY_COST_SHARE_BENEFIT table.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.FORMULARY_ID IS 'Foreign key linking FORMULARY_COST_SHARE_BENEFIT to FORMULARY table.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.ONE_MON_OUT_NW_RETAIL_OFFERED IS 'SERFF - Indicate whether 1 Month In-Network Retail Pharmacy is offered?.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.THREE_MON_IN_NW_MAIL_OFFERED IS 'SERFF - Indicate whether 1 Month Out-of-Network Retail Pharmacy is offered?.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.THREE_MON_OUT_NW_MAIL_OFFERED IS 'SERFF - Indicate whether 3 Month In-Network Mail Order Pharmacy Benefit is offered?.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.CREATION_TIMESTAMP IS 'Record creation timestamp.'
;
COMMENT ON COLUMN FORMULARY_COST_SHARE_BENEFIT.LAST_UPDATE_TIMESTAMP IS 'Record updated timestamp.'
;
-- 
-- TABLE: I18NTEXT 
--

CREATE TABLE I18NTEXT(
    ID                               NUMBER(19, 0)     NOT NULL,
    LANGUAGE                         VARCHAR2(1020),
    TEXT                             CLOB,
    TASK_SUBJECTS_ID                 NUMBER(19, 0),
    TASK_NAMES_ID                    NUMBER(19, 0),
    TASK_DESCRIPTIONS_ID             NUMBER(19, 0),
    REASSIGNMENT_DOCUMENTATION_ID    NUMBER(19, 0),
    NOTIFICATION_SUBJECTS_ID         NUMBER(19, 0),
    NOTIFICATION_NAMES_ID            NUMBER(19, 0),
    NOTIFICATION_DOCUMENTATION_ID    NUMBER(19, 0),
    NOTIFICATION_DESCRIPTIONS_ID     NUMBER(19, 0),
    DEADLINE_DOCUMENTATION_ID        NUMBER(19, 0),
    CONSTRAINT PK_I18NTEXT PRIMARY KEY (ID)
)
;



-- 
-- TABLE: ISSUER_ACCREDITATION 
--

CREATE TABLE ISSUER_ACCREDITATION(
    ID                       NUMBER            NOT NULL,
    EXPIRATION_DATE          TIMESTAMP(6),
    ACCREDITATION_STATUS     VARCHAR2(80),
    ISSUER_ID                NUMBER,
    NCQA_ORG_ID              VARCHAR2(1020),
    NCQA_SUB_ID              VARCHAR2(1020),
    URAC_APP_NUM             VARCHAR2(1020),
    CREATION_TIMESTAMP       TIMESTAMP(6),
    CREATED_BY               NUMBER(10, 0),
    LAST_UPDATE_TIMESTAMP    VARCHAR2(1020),
    LAST_UPDATED_BY          NUMBER(10, 0),
    CONSTRAINT PK_ISSUER_ACCREDIATION PRIMARY KEY (ID)
)
;



COMMENT ON TABLE ISSUER_ACCREDITATION IS 'This table contains information for accrediting an issuer on the health insurance exchange.'
;
COMMENT ON COLUMN ISSUER_ACCREDITATION.ID IS 'Primary Key'
;
COMMENT ON COLUMN ISSUER_ACCREDITATION.EXPIRATION_DATE IS 'Date when the Issuer was accredited.'
;
COMMENT ON COLUMN ISSUER_ACCREDITATION.ACCREDITATION_STATUS IS 'Issuer Accreditation Status'
;
COMMENT ON COLUMN ISSUER_ACCREDITATION.ISSUER_ID IS 'Foreign Key into the ISSUERS table'
;
-- 
-- TABLE: ISSUER_DOCUMENT 
--

CREATE TABLE ISSUER_DOCUMENT(
    ID                       NUMBER           NOT NULL,
    CREATION_TIMESTAMP       TIMESTAMP(6)     NOT NULL,
    CREATED_BY               NUMBER,
    DOCUMENT_NAME            VARCHAR2(400),
    DOCUMENT_TYPE            VARCHAR2(200),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    LAST_UPDATED_BY          NUMBER,
    ISSUER_ID                NUMBER,
    CONSTRAINT PK_ISSUER_DOCUMENT PRIMARY KEY (ID)
)
;



COMMENT ON TABLE ISSUER_DOCUMENT IS 'This table is used to capture list of documents uploaded by issuer.'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.ID IS 'Primary Key'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.CREATION_TIMESTAMP IS 'Time/Date of creation for this database record'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.CREATED_BY IS 'Name of user that created this issuer document.'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.DOCUMENT_NAME IS 'Name of document uploaded for the Issuer'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.DOCUMENT_TYPE IS 'Type of document upload '
;
COMMENT ON COLUMN ISSUER_DOCUMENT.LAST_UPDATE_TIMESTAMP IS 'Name of user that last update this document.'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.LAST_UPDATED_BY IS 'Time/Date of last update for this database record'
;
COMMENT ON COLUMN ISSUER_DOCUMENT.ISSUER_ID IS 'Foreign Key into the ISSUERS table.'
;
-- 
-- TABLE: ISSUER_INVOICES 
--

CREATE TABLE ISSUER_INVOICES(
    ID                               NUMBER(10, 0)    NOT NULL,
    ISSUER_ID                        NUMBER(10, 0),
    CASE_ID                          VARCHAR2(20),
    STATEMENT_DATE                   DATE,
    INVOICE_NUMBER                   VARCHAR2(25),
    PERIOD_COVERED                   VARCHAR2(25),
    PAYMENT_DUE_DATE                 DATE,
    AMOUNT_DUE_FROM_LAST_INVOICE     FLOAT(126),
    TOTAL_PAYMENT_PAID               FLOAT(126),
    PREMIUMS_THIS_PERIOD             FLOAT(126),
    ADJUSTMENTS                      FLOAT(126),
    EXCHANGE_FEES                    FLOAT(126),
    TOTAL_AMOUNT_DUE                 FLOAT(126),
    AMOUNT_RECEIVED_FROM_EMPLOYER    FLOAT(126),
    AMOUNT_ENCLOSED                  FLOAT(126),
    PAID_STATUS                      VARCHAR2(20),
    ECM_DOC_ID                       VARCHAR2(400),
    IS_ACTIVE                        VARCHAR2(1)      DEFAULT 'N',
    GL_CODE                          NUMBER(10, 0),
    CONSTRAINT PK_ISSUER_INVOICES PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN ISSUER_INVOICES.ID IS 'Primary Key, associated with Sequence ISSUER_INVOICES_SEQ'
;
COMMENT ON COLUMN ISSUER_INVOICES.ISSUER_ID IS 'Foreign constraint refrence table Issuers, for the issuer whose invoice is generated'
;
COMMENT ON COLUMN ISSUER_INVOICES.STATEMENT_DATE IS 'Date on which invoice is generated'
;
COMMENT ON COLUMN ISSUER_INVOICES.INVOICE_NUMBER IS 'Unique invoice no for each generated invoice.'
;
COMMENT ON COLUMN ISSUER_INVOICES.PERIOD_COVERED IS 'Period covered for this invoice'
;
COMMENT ON COLUMN ISSUER_INVOICES.PAYMENT_DUE_DATE IS 'Payment due date for this invoice'
;
COMMENT ON COLUMN ISSUER_INVOICES.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Amount due from last invoice if any'
;
COMMENT ON COLUMN ISSUER_INVOICES.TOTAL_PAYMENT_PAID IS 'Total payment paid for this invoice'
;
COMMENT ON COLUMN ISSUER_INVOICES.PREMIUMS_THIS_PERIOD IS 'Employers total premium this period for this issuer'
;
COMMENT ON COLUMN ISSUER_INVOICES.ADJUSTMENTS IS 'Settlement mount adjustment if any'
;
COMMENT ON COLUMN ISSUER_INVOICES.EXCHANGE_FEES IS 'Total exchange fees on total amount due'
;
COMMENT ON COLUMN ISSUER_INVOICES.TOTAL_AMOUNT_DUE IS 'Sum of amount due this period'
;
COMMENT ON COLUMN ISSUER_INVOICES.AMOUNT_RECEIVED_FROM_EMPLOYER IS 'Amount received from employer for this issuer against this period'
;
COMMENT ON COLUMN ISSUER_INVOICES.AMOUNT_ENCLOSED IS 'The actual amt to be paid to issuer'
;
COMMENT ON COLUMN ISSUER_INVOICES.PAID_STATUS IS 'Paid Status value can be DUE, PAID or PARTIALLY_PAID'
;
COMMENT ON COLUMN ISSUER_INVOICES.ECM_DOC_ID IS 'Alfresco ECM doc if for the generated PDF reports'
;
COMMENT ON COLUMN ISSUER_INVOICES.IS_ACTIVE IS 'Stands for whether this invoice is active or not value can be Y or N'
;
COMMENT ON COLUMN ISSUER_INVOICES.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements'
;
-- 
-- TABLE: ISSUER_INVOICES_LINEITEMS 
--

CREATE TABLE ISSUER_INVOICES_LINEITEMS(
    ID                               NUMBER(10, 0)    NOT NULL,
    EMPLOYER_ID                      NUMBER(10, 0),
    POLICY_NUMBER                    VARCHAR2(50),
    EMPLOYEE_ID                      NUMBER(10, 0),
    CARRIER_NAME                     VARCHAR2(400),
    PLAN_TYPE                        VARCHAR2(40),
    COVERAGE_TYPE                    VARCHAR2(20),
    PERSONS_COVERED                  VARCHAR2(20),
    TOTAL_PREMIUM                    FLOAT(126),
    NET_AMOUNT                       FLOAT(126),
    AMOUNT_RECEIVED_FROM_EMPLOYER    FLOAT(126),
    PERIOD_COVERED                   VARCHAR2(25),
    INVOICE_ID                       NUMBER(10, 0),
    RETRO_ADJUSTMENTS                FLOAT(126)       DEFAULT '0.00',
    USER_FEE                         FLOAT(126)       DEFAULT '0',
    AMOUNT_PAID_TO_ISSUER            FLOAT(126)       DEFAULT '0',
    EMP_INV_LINEITEMS_ID             NUMBER(10, 0),
    ENROLLMENT_ID                    NUMBER,
    EXTERNAL_EMPLOYEE_ID             VARCHAR2(20),
    CONSTRAINT PK_ISSUER_INVOICES_LINEITEMS PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.ID IS 'Primary Key, associated with Sequence ISSUER_INVOICES_LINEITEMS_SEQ'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EMPLOYER_ID IS 'Employer Id for the opted plan with associated Issuer'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.POLICY_NUMBER IS 'Policy no of the policy opted by employee'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EMPLOYEE_ID IS 'Employeed Id covered under the plan'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.CARRIER_NAME IS 'Plan provided name'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.PLAN_TYPE IS 'Plan opted by Employee under said Carrier'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.COVERAGE_TYPE IS 'Provided Coverage type opted by Employee'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.PERSONS_COVERED IS 'Persons id covered under this plan'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.TOTAL_PREMIUM IS 'Toatl premium amount this period'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.NET_AMOUNT IS 'Total amount to be paid to the issuer'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.AMOUNT_RECEIVED_FROM_EMPLOYER IS 'Amount received from employer for this invoice'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.PERIOD_COVERED IS 'Period covered for the respective invoice'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.INVOICE_ID IS 'Foreign constraint reference table ISSUER_INVOICES'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.USER_FEE IS 'Total exchange fees against the Total premium'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.AMOUNT_PAID_TO_ISSUER IS 'Total amount paid to issuer after deduction of Exchange fees'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EMP_INV_LINEITEMS_ID IS 'Foreign constraint reference table EMPLOYER_INVOICES_LINEITEMS'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.ENROLLMENT_ID IS 'Column to store enrollment reference'
;
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EXTERNAL_EMPLOYEE_ID IS 'To store external employee id for IND42 flow'
;
-- 
-- TABLE: ISSUER_PAYMENT_DETAIL 
--

CREATE TABLE ISSUER_PAYMENT_DETAIL(
    ID                   NUMBER(10, 0)     NOT NULL,
    TRANSACTION_ID       NUMBER(10, 0),
    RESPONSE             VARCHAR2(1020),
    AMOUNT_PAID          FLOAT(126),
    PAYMENT_DATE         TIMESTAMP(6),
    ISSUER_PAYMENT_ID    NUMBER(10, 0),
    PAYMENT_TYPE_ID      NUMBER(10, 0),
    IS_EDI_GENERATED     VARCHAR2(1),
    CONSTRAINT PK_ISSUER_PAYMENT_DETAIL PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.ID IS 'Primary Key, associated with Sequence ISSUER_PAYMENT_DETAIL_SEQ'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.TRANSACTION_ID IS 'Payment done to issuer, Cybersource transaction id'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.RESPONSE IS 'Cybersource payment response or remarks'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.AMOUNT_PAID IS 'Amount paid to issuer'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.PAYMENT_DATE IS 'Date on which payment is done'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.ISSUER_PAYMENT_ID IS 'Foreign constraint refrence table ISSUER_PAYMENTS'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.PAYMENT_TYPE_ID IS 'Foreign constraint reference table Payment_Methods, payment made via PaymentMethods'
;
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.IS_EDI_GENERATED IS 'Column to store remittance report flag'
;
-- 
-- TABLE: ISSUER_PAYMENT_INVOICE 
--

CREATE TABLE ISSUER_PAYMENT_INVOICE(
    ID               NUMBER(10, 0)    NOT NULL,
    INVOICE_ID       NUMBER(10, 0),
    CREATION_DATE    TIMESTAMP(6),
    AMOUNT           FLOAT(126),
    GL_CODE          NUMBER(10, 0),
    CONSTRAINT PK_ISSUER_PAYMENT_INVOICE PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.ID IS 'Primary Key, associated with Sequence ISSUER_PAYINVOICE_SEQ'
;
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.INVOICE_ID IS 'Foreign constraint reference ISSUER_INVOICES'
;
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.CREATION_DATE IS 'Date on which the payment is made'
;
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.AMOUNT IS 'Toatal amount paid to issuer'
;
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements'
;
-- 
-- TABLE: ISSUER_PAYMENTS 
--

CREATE TABLE ISSUER_PAYMENTS(
    ID                              NUMBER(10, 0)    NOT NULL,
    CASE_ID                         VARCHAR2(20),
    STATEMENT_DATE                  DATE,
    INVOICE_NUMBER                  VARCHAR2(25),
    PERIOD_COVERED                  VARCHAR2(25),
    PAYMENT_DUE_DATE                DATE,
    AMOUNT_DUE_FROM_LAST_INVOICE    FLOAT(126),
    TOTAL_PAYMENT_RECEIVED          FLOAT(126),
    PREMIUMS_THIS_PERIOD            FLOAT(126),
    ADJUSTMENTS                     FLOAT(126),
    EXCHANGE_FEES                   FLOAT(126),
    TOTAL_AMOUNT_DUE                FLOAT(126),
    AMOUNT_ENCLOSED                 FLOAT(126),
    ISSUER_PAYMENT_INVOICE_ID       NUMBER(10, 0),
    STATUS                          VARCHAR2(20),
    IS_REFUND                       CHAR(1),
    IS_PARTIAL_PAYMENT              CHAR(1),
    CONSTRAINT PK_ISSUER_PAYMENTS PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN ISSUER_PAYMENTS.ID IS 'Primary Key, associated with Sequence ISSUER_PAYMENTS_SEQ'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.STATEMENT_DATE IS 'The date on which invoice is generated'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.INVOICE_NUMBER IS 'Issuer Invoice Number'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.PERIOD_COVERED IS 'Issuer invoice payment period'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.PAYMENT_DUE_DATE IS 'Issuer payment due date'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Amount due from last invoice'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.TOTAL_PAYMENT_RECEIVED IS 'Total payment paid to Issuer by exchange'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.PREMIUMS_THIS_PERIOD IS 'Total employer premium amount for this periods invoice'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.ADJUSTMENTS IS 'Adjustment amount if any'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.EXCHANGE_FEES IS 'Exchange fees deducted for the payment paid to issuer'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.TOTAL_AMOUNT_DUE IS 'Total to be paid to Issuer'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.AMOUNT_ENCLOSED IS 'The actual amt to be paid to issuer'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.ISSUER_PAYMENT_INVOICE_ID IS 'Foreign constraint reference table ISSUER_PAYMENT_INVOICE'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.STATUS IS 'Payment status valuce can be PAID or PARTIALLY_PAID '
;
COMMENT ON COLUMN ISSUER_PAYMENTS.IS_REFUND IS 'Is this payment considered for refund, value can be Y or N'
;
COMMENT ON COLUMN ISSUER_PAYMENTS.IS_PARTIAL_PAYMENT IS 'For Partial payment values can be Y or N'
;
-- 
-- TABLE: ISSUER_QUALITY_RATING 
--

CREATE TABLE ISSUER_QUALITY_RATING(
    ID                        NUMBER           NOT NULL,
    QUALITY_RATING            VARCHAR2(40),
    QUALITY_RATING_DETAILS    VARCHAR2(800),
    QUALITY_SOURCE            VARCHAR2(200),
    ISSUER_ID                 NUMBER,
    EFFECTIVE_DATE            DATE             NOT NULL,
    EFFECTIVE_END_DATE        DATE,
    CONSTRAINT PK_ISSUER_QUALITY_RATING PRIMARY KEY (ID)
)
;



COMMENT ON TABLE ISSUER_QUALITY_RATING IS 'This table describes informaton about quality ratings for an issuer.'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.ID IS 'Primary Key'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.QUALITY_RATING IS 'Quality ratings summary for an issuer.'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.QUALITY_RATING_DETAILS IS 'Quality ratings details for an issuer.'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.QUALITY_SOURCE IS 'Quality ratings source for an issuer.'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.ISSUER_ID IS 'Foreign Key into the ISSUERS table.'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.EFFECTIVE_DATE IS 'Quality Rating Effective Date.'
;
COMMENT ON COLUMN ISSUER_QUALITY_RATING.EFFECTIVE_END_DATE IS 'Quality Rating End Date'
;
-- 
-- TABLE: ISSUER_REPRESENTATIVE 
--

CREATE TABLE ISSUER_REPRESENTATIVE(
    ID                      NUMBER            NOT NULL,
    ISSUER_ID               NUMBER,
    USER_ID                 NUMBER,
    PRIMARY_CONTACT         VARCHAR2(3),
    RESPONSE_CODE           NUMBER,
    RESPONSE_DESCRIPTION    VARCHAR2(4000 CHAR),
    DELEGATION_CODE         VARCHAR2(10),
    UPDATEDBY               NUMBER(10, 0),
    LOCATION_ID             NUMBER,
    CONSTRAINT PK_ISSUER_REPRESENTATIVE PRIMARY KEY (ID)
)
;



COMMENT ON TABLE ISSUER_REPRESENTATIVE IS 'This table contains information about issuer representatives.'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.ID IS 'Primary Key'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.ISSUER_ID IS 'Foreign Key into ISSUERS Table'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.USER_ID IS 'Foreign Key into USERS table'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.PRIMARY_CONTACT IS 'Whether the representative is a Primary Contact or not.'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.RESPONSE_CODE IS 'AHBX response code'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.RESPONSE_DESCRIPTION IS 'AHBX response description'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.DELEGATION_CODE IS 'AHBX delegation code'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.UPDATEDBY IS 'column for storing userID updating record'
;
COMMENT ON COLUMN ISSUER_REPRESENTATIVE.LOCATION_ID IS 'Issuer Representative''s location reference'
;
-- 
-- TABLE: ISSUERS 
--

CREATE TABLE ISSUERS(
    ID                             NUMBER            NOT NULL,
    ADDRESS_LINE1                  VARCHAR2(800),
    CERTIFICATION_STATUS           VARCHAR2(80),
    CITY                           VARCHAR2(120),
    CONTACT_PERSON                 VARCHAR2(200),
    CREATION_TIMESTAMP             TIMESTAMP(6)      NOT NULL,
    EFFECTIVE_END_DATE             TIMESTAMP(6),
    EFFECTIVE_START_DATE           TIMESTAMP(6),
    DECERTIFIED_ON                 TIMESTAMP(6),
    CERTIFIED_ON                   TIMESTAMP(6),
    EMAIL_ADDRESS                  VARCHAR2(100),
    ENROLLMENT_URL                 VARCHAR2(1000),
    INITIAL_PAYMENT                VARCHAR2(800),
    LICENSE_NUMBER                 VARCHAR2(120),
    LICENSE_STATUS                 VARCHAR2(120),
    NAME                           VARCHAR2(400),
    PHONE_NUMBER                   VARCHAR2(25),
    RECURRING_PAYMENT              VARCHAR2(40),
    SITE_URL                       VARCHAR2(1000),
    STATE                          VARCHAR2(8),
    LAST_UPDATE_TIMESTAMP          TIMESTAMP(6)      NOT NULL,
    ZIP                            VARCHAR2(20),
    FEDERAL_EIN                    VARCHAR2(15),
    NAIC_COMPANY_CODE              VARCHAR2(25),
    NAIC_GROUP_CODE                VARCHAR2(25),
    ADDRESS_LINE2                  VARCHAR2(200),
    ISSUER_ACCREDITATION           VARCHAR2(10),
    ACCREDITING_ENTITY             VARCHAR2(50),
    CERTIFICATION_DOC              VARCHAR2(400),
    COMPANY_LEGAL_NAME             VARCHAR2(400),
    COMPANY_LOGO                   VARCHAR2(400),
    STATE_OF_DOMICILE              VARCHAR2(2),
    COMPANY_ADDRESS_LINE1          VARCHAR2(200),
    COMPANY_ADDRESS_LINE2          VARCHAR2(200),
    COMPANY_CITY                   VARCHAR2(30),
    COMPANY_STATE                  VARCHAR2(2),
    COMPANY_SITE_URL               VARCHAR2(1000),
    INDV_CUST_SERVICE_PHONE        VARCHAR2(1024),
    INDV_CUST_SERVICE_PHONE_EXT    VARCHAR2(1024),
    INDV_CUST_SERVICE_TOLL_FREE    VARCHAR2(1024),
    INDV_CUST_SERVICE_TTY          VARCHAR2(1024),
    INDV_SITE_URL                  VARCHAR2(1000),
    SHOP_CUST_SERVICE_PHONE        VARCHAR2(1024),
    SHOP_CUST_SERVICE_PHONE_EXT    VARCHAR2(1024),
    SHOP_CUST_SERVICE_TOLL_FREE    VARCHAR2(1024),
    SHOP_CUST_SERVICE_TTY          VARCHAR2(1024),
    SHOP_SITE_URL                  VARCHAR2(200),
    COMMENT_ID                     NUMBER,
    SHORT_NAME                     VARCHAR2(100),
    TXN_834_VERSION                VARCHAR2(100)     DEFAULT 'X220A1',
    TXN_820_VERSION                VARCHAR2(100)     DEFAULT 'X306',
    ACCREDITATION_EXP_DATE         TIMESTAMP(6),
    MARKETING_NAME                 VARCHAR2(200),
    ORG_CHART                      BLOB,
    LAST_UPDATED_BY                NUMBER(10, 0),
    COMPANY_ZIP                    VARCHAR2(9),
    HIOS_ISSUER_ID                 VARCHAR2(5 CHAR),
    CONSTRAINT PK_ISSUERS PRIMARY KEY (ID)
)
;



COMMENT ON TABLE ISSUERS IS 'This table is used to capture issuer data.'
;
COMMENT ON COLUMN ISSUERS.ID IS 'Primary Key'
;
COMMENT ON COLUMN ISSUERS.ADDRESS_LINE1 IS 'Line 1 of Address for the Issuer.'
;
COMMENT ON COLUMN ISSUERS.CERTIFICATION_STATUS IS 'Certification Status of the Issuer.'
;
COMMENT ON COLUMN ISSUERS.CITY IS 'City where the Issuer is located.'
;
COMMENT ON COLUMN ISSUERS.CONTACT_PERSON IS 'Contact Person for the issuer'
;
COMMENT ON COLUMN ISSUERS.CREATION_TIMESTAMP IS 'Date/Time when this database record was created.'
;
COMMENT ON COLUMN ISSUERS.EFFECTIVE_END_DATE IS 'Effective end date for this Issuer.'
;
COMMENT ON COLUMN ISSUERS.EFFECTIVE_START_DATE IS 'Effective start date for this Issuer'
;
COMMENT ON COLUMN ISSUERS.DECERTIFIED_ON IS 'Date/Time when this Issuer was decertified.'
;
COMMENT ON COLUMN ISSUERS.CERTIFIED_ON IS 'Date/Time when this Issuer was certified.'
;
COMMENT ON COLUMN ISSUERS.EMAIL_ADDRESS IS 'Issuer email.'
;
COMMENT ON COLUMN ISSUERS.ENROLLMENT_URL IS 'Issuer enrollment URL'
;
COMMENT ON COLUMN ISSUERS.INITIAL_PAYMENT IS 'Initial payment type that the Issuer accepts.'
;
COMMENT ON COLUMN ISSUERS.LICENSE_NUMBER IS 'Issuer license number with the State Department of Insurance.'
;
COMMENT ON COLUMN ISSUERS.LICENSE_STATUS IS 'Issuer license status with the State Department of Insurance.'
;
COMMENT ON COLUMN ISSUERS.NAME IS 'Name of the Issuer'
;
COMMENT ON COLUMN ISSUERS.PHONE_NUMBER IS 'Phone number for the issuer'
;
COMMENT ON COLUMN ISSUERS.RECURRING_PAYMENT IS 'Recurring payment types accepted by the Issuer'
;
COMMENT ON COLUMN ISSUERS.SITE_URL IS 'Site URL for the Issuer'
;
COMMENT ON COLUMN ISSUERS.STATE IS 'State location of the Issuer'
;
COMMENT ON COLUMN ISSUERS.LAST_UPDATE_TIMESTAMP IS 'Last Updated date/time for this database record.'
;
COMMENT ON COLUMN ISSUERS.ZIP IS 'Zip code for the location of the issuer'
;
COMMENT ON COLUMN ISSUERS.FEDERAL_EIN IS 'Issuer Federal Employer Id'
;
COMMENT ON COLUMN ISSUERS.NAIC_COMPANY_CODE IS 'Issuer credential information with NAIC'
;
COMMENT ON COLUMN ISSUERS.NAIC_GROUP_CODE IS 'Issuer credential information with NAIC'
;
COMMENT ON COLUMN ISSUERS.ADDRESS_LINE2 IS 'Line 2 of Address for the Issuer'
;
COMMENT ON COLUMN ISSUERS.ISSUER_ACCREDITATION IS 'Used to Issuer Accreditation Status.'
;
COMMENT ON COLUMN ISSUERS.ACCREDITING_ENTITY IS 'Issuer accrediting entity.'
;
COMMENT ON COLUMN ISSUERS.CERTIFICATION_DOC IS 'Issuer certification document with the Exchange.'
;
COMMENT ON COLUMN ISSUERS.COMPANY_LEGAL_NAME IS 'Issuer legal name.'
;
COMMENT ON COLUMN ISSUERS.COMPANY_LOGO IS 'Issuer company logo.'
;
COMMENT ON COLUMN ISSUERS.STATE_OF_DOMICILE IS 'Issuer state of domicile.'
;
COMMENT ON COLUMN ISSUERS.COMPANY_ADDRESS_LINE1 IS 'Issuer company line 1 of address.'
;
COMMENT ON COLUMN ISSUERS.COMPANY_ADDRESS_LINE2 IS 'Issuer company line 2 of address.'
;
COMMENT ON COLUMN ISSUERS.COMPANY_CITY IS 'Issuer company city.'
;
COMMENT ON COLUMN ISSUERS.COMPANY_STATE IS 'Issuer company state'
;
COMMENT ON COLUMN ISSUERS.COMPANY_SITE_URL IS 'Issuer company site URL.'
;
COMMENT ON COLUMN ISSUERS.INDV_CUST_SERVICE_PHONE IS 'Individual Exchange customer service phone number'
;
COMMENT ON COLUMN ISSUERS.INDV_CUST_SERVICE_PHONE_EXT IS 'Individual Exchange customer service phone number extension'
;
COMMENT ON COLUMN ISSUERS.INDV_CUST_SERVICE_TOLL_FREE IS 'Individual Exchange customer service phone number toll free number.'
;
COMMENT ON COLUMN ISSUERS.INDV_CUST_SERVICE_TTY IS 'Individual Exchange customer service phone number TTY number.'
;
COMMENT ON COLUMN ISSUERS.INDV_SITE_URL IS 'Individual Exchange customer service site URL'
;
COMMENT ON COLUMN ISSUERS.SHOP_CUST_SERVICE_PHONE IS 'SHOP Exchange customer service phone number'
;
COMMENT ON COLUMN ISSUERS.SHOP_CUST_SERVICE_PHONE_EXT IS 'SHOP Exchange customer service phone number extension'
;
COMMENT ON COLUMN ISSUERS.SHOP_CUST_SERVICE_TOLL_FREE IS 'SHOP Exchange customer service phone number toll free number.'
;
COMMENT ON COLUMN ISSUERS.SHOP_CUST_SERVICE_TTY IS 'SHOP Exchange customer service phone number TTY number.'
;
COMMENT ON COLUMN ISSUERS.SHOP_SITE_URL IS 'SHOP Exchange customer service site URL'
;
COMMENT ON COLUMN ISSUERS.COMMENT_ID IS 'Issuer comment id for comment system.'
;
COMMENT ON COLUMN ISSUERS.SHORT_NAME IS 'Issuer short name'
;
COMMENT ON COLUMN ISSUERS.TXN_834_VERSION IS 'EDI 834 Transaction Version number'
;
COMMENT ON COLUMN ISSUERS.TXN_820_VERSION IS 'EDI 820 Transaction Version number'
;
COMMENT ON COLUMN ISSUERS.ACCREDITATION_EXP_DATE IS 'Used to store accreditation expiration date.'
;
COMMENT ON COLUMN ISSUERS.MARKETING_NAME IS 'Issuers Marketing Name.'
;
COMMENT ON COLUMN ISSUERS.ORG_CHART IS 'Issuers Organization Chart.'
;
COMMENT ON COLUMN ISSUERS.LAST_UPDATED_BY IS 'column for storing userID updating record'
;
COMMENT ON COLUMN ISSUERS.COMPANY_ZIP IS 'company zip holds postal codes'
;
COMMENT ON COLUMN ISSUERS.HIOS_ISSUER_ID IS 'Change column type from NUMBER(5) to VARCHAR2(5 CHAR)'
;
-- 
-- TABLE: ISSUERS_AUD 
--

CREATE TABLE ISSUERS_AUD(
    ID                             NUMBER            NOT NULL,
    ADDRESS_LINE1                  VARCHAR2(800),
    CERTIFICATION_STATUS           VARCHAR2(80),
    CITY                           VARCHAR2(120),
    CONTACT_PERSON                 VARCHAR2(200),
    CREATION_TIMESTAMP             TIMESTAMP(6)      NOT NULL,
    EFFECTIVE_END_DATE             TIMESTAMP(6),
    EFFECTIVE_START_DATE           TIMESTAMP(6),
    DECERTIFIED_ON                 TIMESTAMP(6),
    CERTIFIED_ON                   TIMESTAMP(6),
    EMAIL_ADDRESS                  VARCHAR2(100),
    ENROLLMENT_URL                 VARCHAR2(1000),
    INITIAL_PAYMENT                VARCHAR2(800),
    LICENSE_NUMBER                 VARCHAR2(120),
    LICENSE_STATUS                 VARCHAR2(120),
    NAME                           VARCHAR2(400),
    PHONE_NUMBER                   VARCHAR2(25),
    RECURRING_PAYMENT              VARCHAR2(40),
    SITE_URL                       VARCHAR2(1000),
    STATE                          VARCHAR2(8),
    LAST_UPDATE_TIMESTAMP          TIMESTAMP(6)      NOT NULL,
    ZIP                            VARCHAR2(20),
    USER_ID                        NUMBER,
    FEDERAL_EIN                    VARCHAR2(15),
    NAIC_COMPANY_CODE              VARCHAR2(25),
    NAIC_GROUP_CODE                VARCHAR2(25),
    ADDRESS_LINE2                  VARCHAR2(200),
    ISSUER_ACCREDITATION           VARCHAR2(10),
    ACCREDITING_ENTITY             VARCHAR2(50),
    CERTIFICATION_DOC              VARCHAR2(400),
    COMPANY_LEGAL_NAME             VARCHAR2(400),
    COMPANY_LOGO                   VARCHAR2(400),
    STATE_OF_DOMICILE              VARCHAR2(2),
    COMPANY_ADDRESS_LINE1          VARCHAR2(200),
    COMPANY_ADDRESS_LINE2          VARCHAR2(200),
    COMPANY_CITY                   VARCHAR2(30),
    COMPANY_STATE                  VARCHAR2(2),
    COMPANY_SITE_URL               VARCHAR2(1000),
    INDV_CUST_SERVICE_PHONE        VARCHAR2(1024),
    INDV_CUST_SERVICE_PHONE_EXT    VARCHAR2(1024),
    INDV_CUST_SERVICE_TOLL_FREE    VARCHAR2(1024),
    INDV_CUST_SERVICE_TTY          VARCHAR2(1024),
    INDV_SITE_URL                  VARCHAR2(1000),
    SHOP_CUST_SERVICE_PHONE        VARCHAR2(1024),
    SHOP_CUST_SERVICE_PHONE_EXT    VARCHAR2(1024),
    SHOP_CUST_SERVICE_TOLL_FREE    VARCHAR2(1024),
    SHOP_CUST_SERVICE_TTY          VARCHAR2(1024),
    SHOP_SITE_URL                  VARCHAR2(200),
    REV                            NUMBER            NOT NULL,
    REVTYPE                        NUMBER(3, 0),
    COMMENT_ID                     NUMBER,
    SHORT_NAME                     VARCHAR2(100),
    TXN_834_VERSION                VARCHAR2(100),
    TXN_820_VERSION                VARCHAR2(100),
    ACCREDITATION_EXP_DATE         TIMESTAMP(6),
    MARKETING_NAME                 VARCHAR2(200),
    ORG_CHART                      BLOB,
    LAST_UPDATED_BY                NUMBER(10, 0),
    COMPANY_ZIP                    VARCHAR2(9),
    HIOS_ISSUER_ID                 VARCHAR2(5 CHAR)
)
;



COMMENT ON TABLE ISSUERS_AUD IS 'This is a audit table of Issuer data. For audit trail we use this table.'
;
COMMENT ON COLUMN ISSUERS_AUD.ID IS 'Primary Key'
;
COMMENT ON COLUMN ISSUERS_AUD.ADDRESS_LINE1 IS 'Line 1 of Address for the Issuer.'
;
COMMENT ON COLUMN ISSUERS_AUD.CERTIFICATION_STATUS IS 'Certification Status of the Issuer.'
;
COMMENT ON COLUMN ISSUERS_AUD.CITY IS 'City where the Issuer is located.'
;
COMMENT ON COLUMN ISSUERS_AUD.CONTACT_PERSON IS 'Contact Person for the issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.CREATION_TIMESTAMP IS 'Date/Time when this database record was created.'
;
COMMENT ON COLUMN ISSUERS_AUD.EFFECTIVE_END_DATE IS 'Effective end date for this Issuer.'
;
COMMENT ON COLUMN ISSUERS_AUD.EFFECTIVE_START_DATE IS 'Effective start date for this Issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.DECERTIFIED_ON IS 'Date/Time when this Issuer was decertified.'
;
COMMENT ON COLUMN ISSUERS_AUD.CERTIFIED_ON IS 'Date/Time when this Issuer was certified.'
;
COMMENT ON COLUMN ISSUERS_AUD.EMAIL_ADDRESS IS 'Issuer email.'
;
COMMENT ON COLUMN ISSUERS_AUD.ENROLLMENT_URL IS 'Issuer enrollment URL'
;
COMMENT ON COLUMN ISSUERS_AUD.INITIAL_PAYMENT IS 'Initial payment type that the Issuer accepts.'
;
COMMENT ON COLUMN ISSUERS_AUD.LICENSE_NUMBER IS 'Issuer license number with the State Department of Insurance.'
;
COMMENT ON COLUMN ISSUERS_AUD.LICENSE_STATUS IS 'Issuer license status with the State Department of Insurance.'
;
COMMENT ON COLUMN ISSUERS_AUD.NAME IS 'Name of the Issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.PHONE_NUMBER IS 'Phone number for the issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.RECURRING_PAYMENT IS 'Recurring payment types accepted by the Issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.SITE_URL IS 'Site URL for the Issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.STATE IS 'State location of the Issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.LAST_UPDATE_TIMESTAMP IS 'Last Updated date/time for this database record.'
;
COMMENT ON COLUMN ISSUERS_AUD.ZIP IS 'Zip code for the location of the issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.USER_ID IS 'User Id (deprecated)'
;
COMMENT ON COLUMN ISSUERS_AUD.FEDERAL_EIN IS 'Issuer Federal Employer Id'
;
COMMENT ON COLUMN ISSUERS_AUD.NAIC_COMPANY_CODE IS 'Issuer credential information with NAIC'
;
COMMENT ON COLUMN ISSUERS_AUD.NAIC_GROUP_CODE IS 'Issuer credential information with NAIC'
;
COMMENT ON COLUMN ISSUERS_AUD.ADDRESS_LINE2 IS 'Line 2 of Address for the Issuer'
;
COMMENT ON COLUMN ISSUERS_AUD.ISSUER_ACCREDITATION IS 'Used to Issuer Accreditation Status.'
;
COMMENT ON COLUMN ISSUERS_AUD.ACCREDITING_ENTITY IS 'Issuer accrediting entity.'
;
COMMENT ON COLUMN ISSUERS_AUD.CERTIFICATION_DOC IS 'Issuer certification document with the Exchange.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_LEGAL_NAME IS 'Issuer legal name.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_LOGO IS 'Issuer company logo.'
;
COMMENT ON COLUMN ISSUERS_AUD.STATE_OF_DOMICILE IS 'Issuer state of domicile.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_ADDRESS_LINE1 IS 'Issuer company line 1 of address.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_ADDRESS_LINE2 IS 'Issuer company line 2 of address.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_CITY IS 'Issuer company city.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_STATE IS 'Issuer company state'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_SITE_URL IS 'Issuer company site URL.'
;
COMMENT ON COLUMN ISSUERS_AUD.INDV_CUST_SERVICE_PHONE IS 'Individual Exchange customer service phone number'
;
COMMENT ON COLUMN ISSUERS_AUD.INDV_CUST_SERVICE_PHONE_EXT IS 'Individual Exchange customer service phone number extension'
;
COMMENT ON COLUMN ISSUERS_AUD.INDV_CUST_SERVICE_TOLL_FREE IS 'Individual Exchange customer service phone number toll free number.'
;
COMMENT ON COLUMN ISSUERS_AUD.INDV_CUST_SERVICE_TTY IS 'Individual Exchange customer service phone number TTY number.'
;
COMMENT ON COLUMN ISSUERS_AUD.INDV_SITE_URL IS 'Individual Exchange customer service site URL'
;
COMMENT ON COLUMN ISSUERS_AUD.SHOP_CUST_SERVICE_PHONE IS 'SHOP Exchange customer service phone number'
;
COMMENT ON COLUMN ISSUERS_AUD.SHOP_CUST_SERVICE_PHONE_EXT IS 'SHOP Exchange customer service phone number extension'
;
COMMENT ON COLUMN ISSUERS_AUD.SHOP_CUST_SERVICE_TOLL_FREE IS 'SHOP Exchange customer service phone number toll free number.'
;
COMMENT ON COLUMN ISSUERS_AUD.SHOP_CUST_SERVICE_TTY IS 'SHOP Exchange customer service phone number TTY number.'
;
COMMENT ON COLUMN ISSUERS_AUD.SHOP_SITE_URL IS 'SHOP Exchange customer service site URL'
;
COMMENT ON COLUMN ISSUERS_AUD.REV IS 'Revision number of the audit record.'
;
COMMENT ON COLUMN ISSUERS_AUD.REVTYPE IS 'Type of revision of audit record.'
;
COMMENT ON COLUMN ISSUERS_AUD.COMMENT_ID IS 'Issuer comment id for comment system.'
;
COMMENT ON COLUMN ISSUERS_AUD.SHORT_NAME IS 'Issuer short name'
;
COMMENT ON COLUMN ISSUERS_AUD.TXN_834_VERSION IS 'EDI 834 Transaction Version number'
;
COMMENT ON COLUMN ISSUERS_AUD.TXN_820_VERSION IS 'EDI 820 Transaction Version number'
;
COMMENT ON COLUMN ISSUERS_AUD.ACCREDITATION_EXP_DATE IS 'Used to store accreditation expiration date.'
;
COMMENT ON COLUMN ISSUERS_AUD.MARKETING_NAME IS 'Issuers Marketing Name.'
;
COMMENT ON COLUMN ISSUERS_AUD.ORG_CHART IS 'Issuers Organization Chart.'
;
COMMENT ON COLUMN ISSUERS_AUD.LAST_UPDATED_BY IS 'column for storing userID updating record'
;
COMMENT ON COLUMN ISSUERS_AUD.COMPANY_ZIP IS 'company zip holds postal codes'
;
COMMENT ON COLUMN ISSUERS_AUD.HIOS_ISSUER_ID IS 'Change column type from NUMBER(5) to VARCHAR2(5 CHAR)'
;
-- 
-- TABLE: LOCATIONS_AUD 
--

CREATE TABLE LOCATIONS_AUD(
    ID                       NUMBER(10, 0)     NOT NULL,
    REV                      NUMBER(10, 0)     NOT NULL,
    REVTYPE                  NUMBER(3, 0),
    ADD_ON_ZIP               NUMBER(10, 0),
    ADDRESS1                 VARCHAR2(1020),
    ADDRESS2                 VARCHAR2(1020),
    CITY                     VARCHAR2(1020),
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LATTITUDE                NUMBER(15, 9),
    LONGITUDE                NUMBER(15, 9),
    STATE                    VARCHAR2(60 CHAR),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    ZIP                      NUMBER(10, 0),
    COUNTY                   VARCHAR2(60),
    RDI                      VARCHAR2(1),
    COUNTY_CODE              VARCHAR2(30)
)
;



-- 
-- TABLE: LOOKUP_TYPE_AUD 
--

CREATE TABLE LOOKUP_TYPE_AUD(
    LOOKUP_TYPE_ID    NUMBER(10, 0)     NOT NULL,
    REV               NUMBER(10, 0)     NOT NULL,
    NAME              VARCHAR2(240),
    DATA_TYPE         VARCHAR2(240),
    DESCRIPTION       VARCHAR2(1020)
)
;



-- 
-- TABLE: LOOKUP_VALUE_AUD 
--

CREATE TABLE LOOKUP_VALUE_AUD(
    LOOKUP_VALUE_ID           NUMBER(10, 0)     NOT NULL,
    REV                       NUMBER(10, 0)     NOT NULL,
    LOOKUP_TYPE_ID            NUMBER(10, 0),
    LOOKUP_VALUE_CODE         NUMBER(10, 0),
    RACE_ETHNICITY_LKP        VARCHAR2(240),
    LOOKUP_VALUE_LABEL        VARCHAR2(240),
    PARENT_LOOKUP_VALUE_ID    VARCHAR2(240),
    ISOBSOLETE                VARCHAR2(1),
    DESCRIPTION               VARCHAR2(1020)
)
;



-- 
-- TABLE: MEMBER_DETAILS 
--

CREATE TABLE MEMBER_DETAILS(
    ID                      NUMBER            NOT NULL,
    EMPLOYEE_ID             NUMBER,
    FIRST_NAME              VARCHAR2(100),
    LAST_NAME               VARCHAR2(100),
    TYPE                    VARCHAR2(40 CHAR),
    EMAIL_ADDRESS           VARCHAR2(100 CHAR),
    CONTACT_PHONE_NUMBER    VARCHAR2(25 CHAR),
    BIRTH_DAY               TIMESTAMP(6),
    GENDER                  VARCHAR2(10 CHAR),
    SSN                     VARCHAR2(1020),
    SMOKER                  VARCHAR2(20),
    HOME_LOCATION_ID        NUMBER,
    NATIVE_AMR              VARCHAR2(20),
    MIDDLE_INITIAL          VARCHAR2(2 CHAR),
    CONSTRAINT PK_MEMBER_DETAILS_ID PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN MEMBER_DETAILS.SSN IS 'Social Security Number of the employee'
;
COMMENT ON COLUMN MEMBER_DETAILS.MIDDLE_INITIAL IS 'Employee Middle Name Initials'
;
-- 
-- TABLE: NCQA 
--

CREATE TABLE NCQA(
    ID                      NUMBER(20, 0)     NOT NULL,
    HIOS_ISSUER_ID          NUMBER(5, 0)      NOT NULL,
    NCQA_ORG_ID             VARCHAR2(1020),
    NCQA_SUB_ID             VARCHAR2(1020),
    ACCREDITATION_STATUS    VARCHAR2(20),
    EXPIRATION_DATE         TIMESTAMP(6),
    CONSTRAINT PK_NCQA PRIMARY KEY (ID)
)
;



-- 
-- TABLE: NETWORK 
--

CREATE TABLE NETWORK(
    ID                       NUMBER           NOT NULL,
    CREATION_TIMESTAMP       TIMESTAMP(6)     NOT NULL,
    EFFECTIVE_END_DATE       TIMESTAMP(6),
    EFFECTIVE_START_DATE     TIMESTAMP(6),
    NAME                     VARCHAR2(400),
    TYPE                     VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    ISSUER_ID                NUMBER,
    NETWORK_ID               VARCHAR2(6),
    NETWORK_URL              VARCHAR2(300),
    CONSTRAINT PK_NETWORK PRIMARY KEY (ID)
)
;



COMMENT ON TABLE NETWORK IS 'This table describes details about a provider network e.g. PPO or HMO.'
;
COMMENT ON COLUMN NETWORK.ID IS 'Primary Key'
;
COMMENT ON COLUMN NETWORK.CREATION_TIMESTAMP IS 'Time/Date when this database record was created.'
;
COMMENT ON COLUMN NETWORK.EFFECTIVE_END_DATE IS 'Effective End date for Provider Network on the Exchange.'
;
COMMENT ON COLUMN NETWORK.EFFECTIVE_START_DATE IS 'Effective End date for Provider Network on the Exchange.'
;
COMMENT ON COLUMN NETWORK.NAME IS 'Name of the provider network.'
;
COMMENT ON COLUMN NETWORK.TYPE IS 'Type of Provider Network e.g. PPO, HMO etc'
;
COMMENT ON COLUMN NETWORK.LAST_UPDATE_TIMESTAMP IS 'Last update date/time for this database record.'
;
COMMENT ON COLUMN NETWORK.ISSUER_ID IS 'Foreign Key into the issuer table.'
;
COMMENT ON COLUMN NETWORK.NETWORK_ID IS 'The name network ID associated with the Provider Network.'
;
COMMENT ON COLUMN NETWORK.NETWORK_URL IS 'The network URL associated with the Provider Network.'
;
-- 
-- TABLE: NOTICE_TYPES 
--

CREATE TABLE NOTICE_TYPES(
    ID                       NUMBER            NOT NULL,
    NAME                     VARCHAR2(400 CHAR) NOT NULL,
    LANGUAGE                 VARCHAR2(100 CHAR) NOT NULL,
    USER_TYPE                VARCHAR2(32)      NOT NULL,
    TYPE                     VARCHAR2(40 CHAR) NOT NULL,
    EXTERNAL_ID              VARCHAR2(1020 CHAR),
    TEMPLATE_LOCATION        VARCHAR2(1020),
    EMAIL_SUBJECT            VARCHAR2(1020),
    EMAIL_FROM               VARCHAR2(1020),
    EMAIL_CLASS              VARCHAR2(1020),
    EFFECTIVE_DATE           DATE              NOT NULL,
    TERMINATION_DATE         DATE,
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    CONSTRAINT PK_NOTICE_TYPES PRIMARY KEY (ID),
    CONSTRAINT NOTICE_UC  UNIQUE (NAME, LANGUAGE)
)
;



-- 
-- TABLE: NOTICE_TYPES_AUD 
--

CREATE TABLE NOTICE_TYPES_AUD(
    ID                       NUMBER            NOT NULL,
    REV                      NUMBER(10, 0)     NOT NULL,
    REVTYPE                  NUMBER(3, 0),
    NAME                     VARCHAR2(400 CHAR) NOT NULL,
    LANGUAGE                 VARCHAR2(100 CHAR) NOT NULL,
    USER_TYPE                VARCHAR2(32)      NOT NULL,
    TYPE                     VARCHAR2(40 CHAR) NOT NULL,
    EXTERNAL_ID              VARCHAR2(1020 CHAR),
    TEMPLATE_LOCATION        VARCHAR2(1020),
    EMAIL_SUBJECT            VARCHAR2(1020),
    EMAIL_FROM               VARCHAR2(1020),
    EMAIL_CLASS              VARCHAR2(1020),
    EFFECTIVE_DATE           DATE              NOT NULL,
    TERMINATION_DATE         DATE,
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    CONSTRAINT PK_NOTICE_TYPES_AUD PRIMARY KEY (ID, REV)
)
;



COMMENT ON COLUMN NOTICE_TYPES_AUD.REV IS 'Revision number of the record.Foreign Key to the REVISION_INFO table'
;
COMMENT ON COLUMN NOTICE_TYPES_AUD.REVTYPE IS 'Type of revision'
;
-- 
-- TABLE: NOTICES 
--

CREATE TABLE NOTICES(
    ID                       NUMBER            NOT NULL,
    ATTACHMENT               VARCHAR2(1020),
    BCC_ADDRESS              VARCHAR2(1020),
    CC_EMAIL_ADDRESS         VARCHAR2(100 CHAR),
    CLICK_COUNT              NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    EMAIL_BODY               CLOB,
    EMAIL_ADDRESS            VARCHAR2(100 CHAR),
    FROM_EMAIL_ADDRESS       VARCHAR2(100 CHAR),
    KEY_ID                   NUMBER,
    KEY_NAME                 VARCHAR2(1020),
    REPLY_TO                 VARCHAR2(1020),
    RET_ADDRESS              VARCHAR2(1020),
    SENT_DATE                TIMESTAMP(6),
    STATUS                   VARCHAR2(40 CHAR),
    SUBJECT                  VARCHAR2(1020),
    TO_ADDRESS               VARCHAR2(1020),
    UNSUBSCRIBED             VARCHAR2(40 CHAR),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    VIEWED_DATE              TIMESTAMP(6),
    NOTICE_TYPE_ID           NUMBER,
    USER_ID                  NUMBER,
    ECM_ID                   VARCHAR2(400),
    CONSTRAINT PK_NOTICES PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN NOTICES.ECM_ID IS 'generated notice ECM ID'
;
-- 
-- TABLE: NOTIFICATION 
--

CREATE TABLE NOTIFICATION(
    DTYPE                          VARCHAR2(124)    NOT NULL,
    ID                             NUMBER(19, 0)    NOT NULL,
    PRIORITY                       NUMBER(10, 0)    NOT NULL,
    ESCALATION_NOTIFICATIONS_ID    NUMBER(19, 0),
    CONSTRAINT PK_NOTIFICATION PRIMARY KEY (ID)
)
;



-- 
-- TABLE: PHYSICIAN 
--

CREATE TABLE PHYSICIAN(
    ID                        NUMBER            NOT NULL,
    AFFILIATED_HOSPITAL       VARCHAR2(1000),
    BOARD_CERTIFICATION       VARCHAR2(1000),
    CREATION_TIMESTAMP        TIMESTAMP(6)      NOT NULL,
    EDUCATION                 VARCHAR2(1000),
    GENDER                    VARCHAR2(10),
    GRADUATION_YEAR           VARCHAR2(40),
    LANGUAGES                 VARCHAR2(1020),
    MEDICAL_GROUP             VARCHAR2(1000),
    ACCEPTING_NEW_PATIENTS    VARCHAR2(4),
    TITLE                     VARCHAR2(100),
    TYPE                      VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP     TIMESTAMP(6)      NOT NULL,
    WEBSITE_URL               VARCHAR2(1000),
    PROVIDER_ID               NUMBER,
    DEA_NUMBER                VARCHAR2(120),
    INTERNSHIP                VARCHAR2(100),
    RESIDENCY                 VARCHAR2(100),
    EMAIL_ADDRESS             VARCHAR2(100),
    YEARS_OF_EXP              VARCHAR2(8),
    BIRTH_DAY                 TIMESTAMP(6),
    SUFFIX                    VARCHAR2(10 CHAR),
    FIRST_NAME                VARCHAR2(100),
    MIDDLE_NAME               VARCHAR2(100),
    LAST_NAME                 VARCHAR2(100),
    CONSTRAINT PK_PHYSICIAN PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PHYSICIAN IS 'This table describes information about a physician on the Exchange. Typically a physician is a part of one or more physician networks (PPO/HMO) etc.'
;
COMMENT ON COLUMN PHYSICIAN.ID IS 'Primary Key'
;
COMMENT ON COLUMN PHYSICIAN.AFFILIATED_HOSPITAL IS 'Hospitals with which the physician is affilated.'
;
COMMENT ON COLUMN PHYSICIAN.BOARD_CERTIFICATION IS 'Board Certification information for the physician'
;
COMMENT ON COLUMN PHYSICIAN.CREATION_TIMESTAMP IS 'Date/Time of creation of this database record.'
;
COMMENT ON COLUMN PHYSICIAN.EDUCATION IS 'Education information for the physician'
;
COMMENT ON COLUMN PHYSICIAN.GENDER IS 'Gender of the physician (M/F).'
;
COMMENT ON COLUMN PHYSICIAN.GRADUATION_YEAR IS 'Year of Graduation for the physician'
;
COMMENT ON COLUMN PHYSICIAN.LANGUAGES IS 'Languages spoken by the physician.'
;
COMMENT ON COLUMN PHYSICIAN.MEDICAL_GROUP IS 'Medical Group that the physician belongs to.'
;
COMMENT ON COLUMN PHYSICIAN.ACCEPTING_NEW_PATIENTS IS 'Whether the physician is accepting new patients.'
;
COMMENT ON COLUMN PHYSICIAN.TITLE IS 'Title of the physician.'
;
COMMENT ON COLUMN PHYSICIAN.TYPE IS 'Type of physician e.g. dentist, doctor etc.'
;
COMMENT ON COLUMN PHYSICIAN.LAST_UPDATE_TIMESTAMP IS 'Last updated date for this database record.'
;
COMMENT ON COLUMN PHYSICIAN.WEBSITE_URL IS 'URL for the physician website.'
;
COMMENT ON COLUMN PHYSICIAN.PROVIDER_ID IS 'Foreign key into the provider table.'
;
COMMENT ON COLUMN PHYSICIAN.DEA_NUMBER IS 'Drug Enforcement Agency number for the physician.'
;
COMMENT ON COLUMN PHYSICIAN.INTERNSHIP IS 'Internship information for the physician'
;
COMMENT ON COLUMN PHYSICIAN.RESIDENCY IS 'Residency information for the physician.'
;
COMMENT ON COLUMN PHYSICIAN.EMAIL_ADDRESS IS 'E-mail address of the physician.'
;
COMMENT ON COLUMN PHYSICIAN.YEARS_OF_EXP IS 'Years of Experience of the physician.'
;
COMMENT ON COLUMN PHYSICIAN.BIRTH_DAY IS 'Date of Birth of the physician.'
;
COMMENT ON COLUMN PHYSICIAN.SUFFIX IS 'For example: Jr.,Sr. Should be null for facilities and only populated for individuals/practitioners'
;
COMMENT ON COLUMN PHYSICIAN.FIRST_NAME IS 'Should be null for facilities and only populated for individuals/practitioners'
;
COMMENT ON COLUMN PHYSICIAN.MIDDLE_NAME IS 'Should be null for facilities and only populated for individuals/practitioners'
;
COMMENT ON COLUMN PHYSICIAN.LAST_NAME IS 'Should be null for facilities and only populated for individuals/practitioners'
;
-- 
-- TABLE: PHYSICIAN_ADDRESS 
--

CREATE TABLE PHYSICIAN_ADDRESS(
    ID                       NUMBER            NOT NULL,
    ADDRESS1                 VARCHAR2(1000),
    CITY                     VARCHAR2(80),
    COUNTY                   VARCHAR2(80),
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    EXTENDED_ZIPCODE         VARCHAR2(10),
    FAX                      VARCHAR2(60),
    PHONE_NUMBER             VARCHAR2(25),
    STATE                    VARCHAR2(8),
    STATUS                   VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    ZIP                      VARCHAR2(20),
    PHYSICIAN_ID             NUMBER,
    ADDRESS2                 VARCHAR2(1000),
    LATTITUDE                NUMBER(15, 9),
    LONGITUDE                NUMBER(15, 9),
    CONSTRAINT PK_PHYSICIAN_ADDRESS PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PHYSICIAN_ADDRESS IS 'This table describes information about a physician address. A physician can be associated with on or addresses since they can practice in one or more physical locations.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.ID IS 'Primary Key'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.ADDRESS1 IS 'Part 1 of the physician address'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.CITY IS 'City of the physician address'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.COUNTY IS 'County of the physician address.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.CREATION_TIMESTAMP IS 'Date/Time of the creation date for this database record.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.EXTENDED_ZIPCODE IS 'Extended zip code of the physician address.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.FAX IS 'Fax number of the physician.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.PHONE_NUMBER IS 'Phone number of the physician office.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.STATE IS 'State where the physician address is located'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.STATUS IS 'Status of the address (active, inactive etc.)'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.LAST_UPDATE_TIMESTAMP IS 'Last updated date of this database record.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.ZIP IS 'Zip code of the physician address.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.PHYSICIAN_ID IS 'Foreign Key into PHYSICIAN table.'
;
COMMENT ON COLUMN PHYSICIAN_ADDRESS.ADDRESS2 IS 'Part 2 of the physician address.'
;
-- 
-- TABLE: PHYSICIAN_SPECIALITY_RELATION 
--

CREATE TABLE PHYSICIAN_SPECIALITY_RELATION(
    ID              NUMBER    NOT NULL,
    PHYSICIAN_ID    NUMBER,
    SPECIALTY_ID    NUMBER,
    CONSTRAINT PK_PHYSICIAN_SP_REL PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PHYSICIAN_SPECIALITY_RELATION IS 'This table describes relationship between a physician and their speciality.'
;
COMMENT ON COLUMN PHYSICIAN_SPECIALITY_RELATION.ID IS 'Primary Key'
;
COMMENT ON COLUMN PHYSICIAN_SPECIALITY_RELATION.PHYSICIAN_ID IS 'Foreign Key into PHYSICIAN table.'
;
COMMENT ON COLUMN PHYSICIAN_SPECIALITY_RELATION.SPECIALTY_ID IS 'Foreign Key into SPECIALTY table'
;
-- 
-- TABLE: PLAN 
--

CREATE TABLE PLAN(
    ID                            NUMBER            NOT NULL,
    CREATION_TIMESTAMP            TIMESTAMP(6)      NOT NULL,
    CREATED_BY                    VARCHAR2(60),
    END_DATE                      TIMESTAMP(6),
    INSURANCE_TYPE                VARCHAR2(40),
    ISSUER_PLAN_NUMBER            VARCHAR2(120),
    MARKET                        VARCHAR2(1020),
    NAME                          VARCHAR2(400),
    NETWORK_TYPE                  VARCHAR2(20),
    START_DATE                    TIMESTAMP(6),
    STATUS                        VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP         TIMESTAMP(6)      NOT NULL,
    LAST_UPDATED_BY               VARCHAR2(60),
    ISSUER_ID                     NUMBER,
    PROVIDER_NETWORK_ID           NUMBER,
    CERTIFIEDBY                   VARCHAR2(15),
    CERTIFIED_ON                  TIMESTAMP(6),
    BROCHURE                      VARCHAR2(255),
    HSA                           VARCHAR2(3)       DEFAULT 'NO',
    ENROLLMENT_AVAIL              VARCHAR2(60),
    ENROLLMENT_AVAIL_EFFDATE      TIMESTAMP(6),
    SUPPORT_FILE                  VARCHAR2(100),
    ISSUER_VERIFICATION_STATUS    VARCHAR2(100),
    COMMENT_ID                    NUMBER,
    FORMULARLY_ID                 VARCHAR2(150 CHAR),
    FORMULARLY_URL                VARCHAR2(1000),
    VERIFIED                      VARCHAR2(4),
    HIOSPRODUCT_ID                VARCHAR2(25),
    HPID                          VARCHAR2(25),
    NETWORK_ID                    VARCHAR2(25),
    SERVICE_AREA_ID               NUMBER,
    STATE                         VARCHAR2(20),
    CONSTRAINT PK_PLAN PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLAN IS 'This table a plan in the Health Insurance Exchange. Plans could be health insurnance plans or dental plans etc.'
;
COMMENT ON COLUMN PLAN.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN.CREATION_TIMESTAMP IS 'Time/Date when this database record was created.'
;
COMMENT ON COLUMN PLAN.CREATED_BY IS 'User that created this database record.'
;
COMMENT ON COLUMN PLAN.END_DATE IS 'End Date of this plan on the Exchange.'
;
COMMENT ON COLUMN PLAN.INSURANCE_TYPE IS 'Type of Insurance e.g. Health, Dental etc'
;
COMMENT ON COLUMN PLAN.ISSUER_PLAN_NUMBER IS 'Issuer internal id for this plan.'
;
COMMENT ON COLUMN PLAN.MARKET IS 'Insurance Market where this plan is sold e.g. Individual, SHOP'
;
COMMENT ON COLUMN PLAN.NAME IS 'Name of this plan.'
;
COMMENT ON COLUMN PLAN.NETWORK_TYPE IS 'Network type associated with this plan e.g. HMO, PPO etc.'
;
COMMENT ON COLUMN PLAN.START_DATE IS 'Start date for this plan on the Exchange.'
;
COMMENT ON COLUMN PLAN.STATUS IS 'Status of the plan on the Exchange.'
;
COMMENT ON COLUMN PLAN.LAST_UPDATE_TIMESTAMP IS 'Last Updated date/time of this database record.'
;
COMMENT ON COLUMN PLAN.LAST_UPDATED_BY IS 'User that last update this database record.'
;
COMMENT ON COLUMN PLAN.ISSUER_ID IS 'Foreign Key into the ISSUERS table.'
;
COMMENT ON COLUMN PLAN.PROVIDER_NETWORK_ID IS 'Foreign Key into the PROVIDER_NETWORK table,'
;
COMMENT ON COLUMN PLAN.CERTIFIEDBY IS 'User that certified this plan on the Exchange.'
;
COMMENT ON COLUMN PLAN.CERTIFIED_ON IS 'Time/Date when this plan was certified.j'
;
COMMENT ON COLUMN PLAN.BROCHURE IS 'Brochure for this plan describing details/benefits of this plan.'
;
COMMENT ON COLUMN PLAN.HSA IS 'Is the plan HSA qualified?'
;
COMMENT ON COLUMN PLAN.ENROLLMENT_AVAIL IS 'Is Enrollment available for this plan?'
;
COMMENT ON COLUMN PLAN.ENROLLMENT_AVAIL_EFFDATE IS 'Defines the enrollment effectve date for this plan.'
;
COMMENT ON COLUMN PLAN.SUPPORT_FILE IS 'Support File name (deprecated)'
;
COMMENT ON COLUMN PLAN.ISSUER_VERIFICATION_STATUS IS 'Issuer verification status for this plan.'
;
COMMENT ON COLUMN PLAN.COMMENT_ID IS 'Comment for uploaded document.'
;
COMMENT ON COLUMN PLAN.FORMULARLY_ID IS 'Used to store formularly_id.'
;
COMMENT ON COLUMN PLAN.FORMULARLY_URL IS 'Used to Store formularly url.'
;
COMMENT ON COLUMN PLAN.VERIFIED IS 'Used to store verified status value as Yes/No.'
;
COMMENT ON COLUMN PLAN.HIOSPRODUCT_ID IS 'HIOS Product Id'
;
COMMENT ON COLUMN PLAN.HPID IS 'HPID'
;
COMMENT ON COLUMN PLAN.NETWORK_ID IS 'NETWORK ID'
;
COMMENT ON COLUMN PLAN.SERVICE_AREA_ID IS 'Capture Primary key of PM_ISSUER_SERVICE_AREA table'
;
COMMENT ON COLUMN PLAN.STATE IS 'Used to store Plan State.'
;
-- 
-- TABLE: PLAN_AUD 
--

CREATE TABLE PLAN_AUD(
    ID                            NUMBER            NOT NULL,
    REV                           NUMBER            NOT NULL,
    REVTYPE                       NUMBER(3, 0),
    END_DATE                      TIMESTAMP(6),
    INSURANCE_TYPE                VARCHAR2(40),
    ISSUER_PLAN_NUMBER            VARCHAR2(120),
    PROVIDER_NETWORK_ID           NUMBER,
    MARKET                        VARCHAR2(1020),
    NAME                          VARCHAR2(400),
    NETWORK_TYPE                  VARCHAR2(20),
    START_DATE                    TIMESTAMP(6),
    STATUS                        VARCHAR2(40),
    ISSUER_ID                     NUMBER,
    CERTIFIEDBY                   VARCHAR2(15),
    CERTIFIED_ON                  TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP         TIMESTAMP(6),
    LAST_UPDATED_BY               VARCHAR2(60),
    BROCHURE                      VARCHAR2(255),
    ENROLLMENT_AVAIL              VARCHAR2(60),
    ENROLLMENT_AVAIL_EFFDATE      TIMESTAMP(6),
    SUPPORT_FILE                  VARCHAR2(100),
    ISSUER_VERIFICATION_STATUS    VARCHAR2(100),
    COMMENT_ID                    NUMBER,
    FORMULARLY_ID                 VARCHAR2(150 CHAR),
    FORMULARLY_URL                VARCHAR2(1000),
    HIOSPRODUCT_ID                VARCHAR2(25),
    HPID                          VARCHAR2(25),
    NETWORK_ID                    VARCHAR2(25),
    SERVICE_AREA_ID               NUMBER,
    STATE                         VARCHAR2(20),
    CREATED_BY                    VARCHAR2(60),
    CREATION_TIMESTAMP            TIMESTAMP(6),
    HSA                           VARCHAR2(3),
    VERIFIED                      VARCHAR2(4),
    CONSTRAINT PK_PLAN_AUD PRIMARY KEY (ID, REV)
)
;



COMMENT ON TABLE PLAN_AUD IS 'This is an audit table of Plan data. For audit trail we use this table.'
;
COMMENT ON COLUMN PLAN_AUD.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_AUD.REV IS 'Used to store Revision of Plan.'
;
COMMENT ON COLUMN PLAN_AUD.REVTYPE IS 'Type of revision of audit record.'
;
COMMENT ON COLUMN PLAN_AUD.END_DATE IS 'End Date of this plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_AUD.INSURANCE_TYPE IS 'Type of Insurance e.g. Health, Dental etc'
;
COMMENT ON COLUMN PLAN_AUD.ISSUER_PLAN_NUMBER IS 'Issuer internal id for this plan.'
;
COMMENT ON COLUMN PLAN_AUD.PROVIDER_NETWORK_ID IS 'Foreign Key into the PROVIDER_NETWORK table,'
;
COMMENT ON COLUMN PLAN_AUD.MARKET IS 'Insurance Market where this plan is sold e.g. Individual, SHOP'
;
COMMENT ON COLUMN PLAN_AUD.NAME IS 'Name of this plan.'
;
COMMENT ON COLUMN PLAN_AUD.NETWORK_TYPE IS 'Network type associated with this plan e.g. HMO, PPO etc.'
;
COMMENT ON COLUMN PLAN_AUD.START_DATE IS 'Start date for this plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_AUD.STATUS IS 'Status of the plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_AUD.ISSUER_ID IS 'Foreign Key into the ISSUERS table.'
;
COMMENT ON COLUMN PLAN_AUD.CERTIFIEDBY IS 'User that certified this plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_AUD.CERTIFIED_ON IS 'Time/Date when this plan was certified.'
;
COMMENT ON COLUMN PLAN_AUD.LAST_UPDATE_TIMESTAMP IS 'Last Updated date/time of this database record.'
;
COMMENT ON COLUMN PLAN_AUD.LAST_UPDATED_BY IS 'User that last update this database record.'
;
COMMENT ON COLUMN PLAN_AUD.BROCHURE IS 'Brochure for this plan describing details/benefits of this plan.'
;
COMMENT ON COLUMN PLAN_AUD.ENROLLMENT_AVAIL IS 'Is Enrollment available for this plan?'
;
COMMENT ON COLUMN PLAN_AUD.ENROLLMENT_AVAIL_EFFDATE IS 'Defines the enrollment effectve date for this plan.'
;
COMMENT ON COLUMN PLAN_AUD.SUPPORT_FILE IS 'Support File name (deprecated)'
;
COMMENT ON COLUMN PLAN_AUD.ISSUER_VERIFICATION_STATUS IS 'Issuer verification status for this plan.'
;
COMMENT ON COLUMN PLAN_AUD.COMMENT_ID IS 'Comment for uploaded document.'
;
COMMENT ON COLUMN PLAN_AUD.FORMULARLY_ID IS 'Foreign Key into FORMULARLY table (deprecated)'
;
COMMENT ON COLUMN PLAN_AUD.FORMULARLY_URL IS 'URL for Formularly for this plan.'
;
COMMENT ON COLUMN PLAN_AUD.HIOSPRODUCT_ID IS 'HIOS Product Id'
;
COMMENT ON COLUMN PLAN_AUD.HPID IS 'HPID'
;
COMMENT ON COLUMN PLAN_AUD.NETWORK_ID IS 'NETWORK ID'
;
COMMENT ON COLUMN PLAN_AUD.SERVICE_AREA_ID IS 'Auditable column of PLAN.SERVICE_AREA_ID'
;
COMMENT ON COLUMN PLAN_AUD.STATE IS 'Used to store Plan State.'
;
COMMENT ON COLUMN PLAN_AUD.CREATED_BY IS 'User that created this database record.'
;
COMMENT ON COLUMN PLAN_AUD.CREATION_TIMESTAMP IS 'Time/Date when this database record was created.'
;
COMMENT ON COLUMN PLAN_AUD.HSA IS 'Is the plan HSA qualified?'
;
COMMENT ON COLUMN PLAN_AUD.VERIFIED IS 'Used to store verified status value as Yes/No.'
;
-- 
-- TABLE: PLAN_DENTAL 
--

CREATE TABLE PLAN_DENTAL(
    ID                               NUMBER            NOT NULL,
    ACTURIAL_VALUE_CERTIFICATE       VARCHAR2(400),
    COST_SHARING                     VARCHAR2(20),
    EFFECTIVE_END_DATE               TIMESTAMP(6),
    EFFECTIVE_START_DATE             TIMESTAMP(6),
    PARENT_PLAN_ID                   NUMBER,
    PLAN_ID                          NUMBER,
    PLAN_LEVEL                       VARCHAR2(40),
    STATUS                           VARCHAR2(40),
    CREATION_TIMESTAMP               TIMESTAMP(6)      NOT NULL,
    CREATED_BY                       VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6)      NOT NULL,
    BENEFIT_FILE                     VARCHAR2(100),
    RATE_FILE                        VARCHAR2(100),
    RATE_EFFECTIVE_DATE              TIMESTAMP(6),
    BENEFIT_EFFECTIVE_DATE           TIMESTAMP(6),
    COMMENT_ID                       NUMBER,
    LAST_UPDATED_BY                  NUMBER(15, 9),
    OUT_OF_COUNTRY_COVERAGE          VARCHAR2(5),
    OUT_OF_SVC_AREA_COVERAGE         VARCHAR2(5),
    NATIONAL_NETWORK                 VARCHAR2(5),
    IS_QHP                           VARCHAR2(1020),
    PLAN_LEVEL_EXCLUSIONS            VARCHAR2(1020),
    AV_CALC_OUTPUT_NUMBER            VARCHAR2(1020),
    MULTIPLE_NETWORK_TIERS           VARCHAR2(1020),
    TIER1_UTIL                       VARCHAR2(1020),
    TIER2_UTIL                       VARCHAR2(1020),
    EHB_APPT_FOR_PEDIATRIC_DENTAL    VARCHAR2(20),
    GUARANTEED_VS_ESTIMATED_RATE     VARCHAR2(20),
    OUT_OF_COUNTRY_COVERAGE_DESC     VARCHAR2(4000),
    OUT_OF_SVC_AREA_COVERAGE_DESC    VARCHAR2(4000),
    CONSTRAINT PK_PLAN_DENTAL PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLAN_DENTAL IS 'This table defines schema for a dental plan.'
;
COMMENT ON COLUMN PLAN_DENTAL.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_DENTAL.ACTURIAL_VALUE_CERTIFICATE IS 'Acturial value certificate for the dental plan'
;
COMMENT ON COLUMN PLAN_DENTAL.COST_SHARING IS 'Cost Sharing type for the dental plan.'
;
COMMENT ON COLUMN PLAN_DENTAL.EFFECTIVE_END_DATE IS 'Effective end date of the plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_DENTAL.EFFECTIVE_START_DATE IS 'Effective start date of the plan on the Exchange'
;
COMMENT ON COLUMN PLAN_DENTAL.PARENT_PLAN_ID IS 'Parent Plan Id of this dental plan. Points to PLAN_DENTAL.ID'
;
COMMENT ON COLUMN PLAN_DENTAL.PLAN_ID IS 'Foreign Key into the PLAN table.'
;
COMMENT ON COLUMN PLAN_DENTAL.PLAN_LEVEL IS 'Plan Level for this plan.'
;
COMMENT ON COLUMN PLAN_DENTAL.STATUS IS 'Status of the plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_DENTAL.CREATION_TIMESTAMP IS 'Time/Date of createion of this database record.'
;
COMMENT ON COLUMN PLAN_DENTAL.CREATED_BY IS 'User that created this database record.'
;
COMMENT ON COLUMN PLAN_DENTAL.LAST_UPDATE_TIMESTAMP IS 'Time/Date of last update of this database record.'
;
COMMENT ON COLUMN PLAN_DENTAL.BENEFIT_FILE IS 'Pointer to the benefit file on the Content Management system.'
;
COMMENT ON COLUMN PLAN_DENTAL.RATE_FILE IS 'Pointer to the dental plan rates file on the Content Management system.'
;
COMMENT ON COLUMN PLAN_DENTAL.RATE_EFFECTIVE_DATE IS 'Effective Start date for the rates.'
;
COMMENT ON COLUMN PLAN_DENTAL.BENEFIT_EFFECTIVE_DATE IS 'Effective Start date for the benefits.'
;
COMMENT ON COLUMN PLAN_DENTAL.COMMENT_ID IS 'Comment for uploaded document.'
;
COMMENT ON COLUMN PLAN_DENTAL.LAST_UPDATED_BY IS 'Used to store Last Updated By.'
;
COMMENT ON COLUMN PLAN_DENTAL.OUT_OF_COUNTRY_COVERAGE IS 'Used to store Out of Country Coverage.'
;
COMMENT ON COLUMN PLAN_DENTAL.OUT_OF_SVC_AREA_COVERAGE IS 'Used to store Out of Service Area Coverage.'
;
COMMENT ON COLUMN PLAN_DENTAL.NATIONAL_NETWORK IS 'Used to store National Network.'
;
COMMENT ON COLUMN PLAN_DENTAL.IS_QHP IS 'is_qhp'
;
COMMENT ON COLUMN PLAN_DENTAL.PLAN_LEVEL_EXCLUSIONS IS 'plan_level_exclusions'
;
COMMENT ON COLUMN PLAN_DENTAL.AV_CALC_OUTPUT_NUMBER IS 'av_calc_output_number'
;
COMMENT ON COLUMN PLAN_DENTAL.MULTIPLE_NETWORK_TIERS IS 'multiple_network_tiers'
;
COMMENT ON COLUMN PLAN_DENTAL.TIER1_UTIL IS 'tier1_util'
;
COMMENT ON COLUMN PLAN_DENTAL.TIER2_UTIL IS 'tier2_util'
;
COMMENT ON COLUMN PLAN_DENTAL.EHB_APPT_FOR_PEDIATRIC_DENTAL IS 'ehbApportionmentForPediatricDental'
;
COMMENT ON COLUMN PLAN_DENTAL.GUARANTEED_VS_ESTIMATED_RATE IS 'guaranteedVsEstimatedRate'
;
COMMENT ON COLUMN PLAN_DENTAL.OUT_OF_COUNTRY_COVERAGE_DESC IS 'Out of country coverage description'
;
COMMENT ON COLUMN PLAN_DENTAL.OUT_OF_SVC_AREA_COVERAGE_DESC IS 'Out of service area coverage description'
;
-- 
-- TABLE: PLAN_DENTAL_AUD 
--

CREATE TABLE PLAN_DENTAL_AUD(
    ID                               NUMBER            NOT NULL,
    REV                              NUMBER            NOT NULL,
    REVTYPE                          NUMBER(3, 0),
    ACTURIAL_VALUE_CERTIFICATE       VARCHAR2(400),
    COST_SHARING                     VARCHAR2(20),
    EFFECTIVE_END_DATE               TIMESTAMP(6),
    EFFECTIVE_START_DATE             TIMESTAMP(6),
    PARENT_PLAN_ID                   NUMBER,
    PLAN_LEVEL                       VARCHAR2(40),
    STATUS                           VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6),
    PLAN_ID                          NUMBER,
    BENEFIT_FILE                     VARCHAR2(100),
    RATE_FILE                        VARCHAR2(100),
    RATE_EFFECTIVE_DATE              TIMESTAMP(6),
    BENEFIT_EFFECTIVE_DATE           TIMESTAMP(6),
    COMMENT_ID                       NUMBER,
    LAST_UPDATED_BY                  NUMBER(15, 9),
    OUT_OF_COUNTRY_COVERAGE          VARCHAR2(5),
    OUT_OF_SVC_AREA_COVERAGE         VARCHAR2(5),
    NATIONAL_NETWORK                 VARCHAR2(5),
    IS_QHP                           VARCHAR2(1020),
    PLAN_LEVEL_EXCLUSIONS            VARCHAR2(1020),
    AV_CALC_OUTPUT_NUMBER            VARCHAR2(1020),
    MULTIPLE_NETWORK_TIERS           VARCHAR2(1020),
    TIER1_UTIL                       VARCHAR2(1020),
    TIER2_UTIL                       VARCHAR2(1020),
    EHB_APPT_FOR_PEDIATRIC_DENTAL    VARCHAR2(20),
    GUARANTEED_VS_ESTIMATED_RATE     VARCHAR2(20),
    OUT_OF_COUNTRY_COVERAGE_DESC     VARCHAR2(4000),
    OUT_OF_SVC_AREA_COVERAGE_DESC    VARCHAR2(4000),
    CREATED_BY                       VARCHAR2(60),
    CREATION_TIMESTAMP               TIMESTAMP(6)
)
;



COMMENT ON TABLE PLAN_DENTAL_AUD IS 'This is an audit table of Dental Plan data. For audit trail we use this table.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.REV IS 'Revision number of the audit record.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.REVTYPE IS 'Type of revision of the audit record.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.ACTURIAL_VALUE_CERTIFICATE IS 'Acturial value certificate for the dental plan'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.COST_SHARING IS 'Cost Sharing type for the dental plan.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.EFFECTIVE_END_DATE IS 'Effective end date of the plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.EFFECTIVE_START_DATE IS 'Effective start date of the plan on the Exchange'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.PARENT_PLAN_ID IS 'Parent Plan Id of this dental plan. Points to PLAN_DENTAL.ID'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.PLAN_LEVEL IS 'Plan Level for this plan.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.STATUS IS 'Status of the plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.LAST_UPDATE_TIMESTAMP IS 'Time/Date of last update of this database record.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.PLAN_ID IS 'Foreign Key into the PLAN table.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.BENEFIT_FILE IS 'Pointer to the benefit file on the Content Management system.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.RATE_FILE IS 'Pointer to the dental plan rates file on the Content Management system.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.RATE_EFFECTIVE_DATE IS 'Effective Start date for the rates.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.BENEFIT_EFFECTIVE_DATE IS 'Effective Start date for the benefits.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.COMMENT_ID IS 'Used for storing Comment Id.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.LAST_UPDATED_BY IS 'Used for storing Last Update By.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.OUT_OF_COUNTRY_COVERAGE IS 'Used for storing Out of Country Coverage.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.OUT_OF_SVC_AREA_COVERAGE IS 'Used for storing Out of Service Area Coverage.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.NATIONAL_NETWORK IS 'Used to store National Network.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.IS_QHP IS 'is_qhp'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.PLAN_LEVEL_EXCLUSIONS IS 'plan_level_exclusions'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.AV_CALC_OUTPUT_NUMBER IS 'av_calc_output_number'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.MULTIPLE_NETWORK_TIERS IS 'multiple_network_tiers'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.TIER1_UTIL IS 'tier1_util'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.TIER2_UTIL IS 'tier2_util'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.EHB_APPT_FOR_PEDIATRIC_DENTAL IS 'ehbApportionmentForPediatricDental'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.GUARANTEED_VS_ESTIMATED_RATE IS 'guaranteedVsEstimatedRate'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.OUT_OF_COUNTRY_COVERAGE_DESC IS 'Out of country coverage description'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.OUT_OF_SVC_AREA_COVERAGE_DESC IS 'Out of service area coverage description'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.CREATED_BY IS 'User that created this database record.'
;
COMMENT ON COLUMN PLAN_DENTAL_AUD.CREATION_TIMESTAMP IS 'Time/Date when this database record was created.'
;
-- 
-- TABLE: PLAN_DENTAL_BENEFIT 
--

CREATE TABLE PLAN_DENTAL_BENEFIT(
    ID                               NUMBER            NOT NULL,
    PLAN_DENTAL_ID                   NUMBER,
    EXCEPTIONS                       VARCHAR2(255),
    LIMITATION_ATTR                  VARCHAR2(50),
    LIMITATION                       VARCHAR2(40),
    NAME                             VARCHAR2(400),
    EFFECTIVE_START_DATE             TIMESTAMP(6),
    EFFECTIVE_END_DATE               TIMESTAMP(6),
    NETWORK_SUBJ_DEDUCT              VARCHAR2(255),
    NETWORK_EXCL_FRM_MOOP            VARCHAR2(255),
    NONNETWORK_SUBJ_DEDUCT           VARCHAR2(255),
    NONNETWORK_EXCL_FRM_MOOP         VARCHAR2(255),
    IS_EHB                           VARCHAR2(40),
    IS_COVERED                       VARCHAR2(40),
    MIN_STAY                         VARCHAR2(40),
    EXPLANATION                      VARCHAR2(1020),
    SUBJECT_TO_IN_NET_DEDUCTIBLE     VARCHAR2(255),
    SUBJECT_TO_OUT_NET_DEDUCTIBLE    VARCHAR2(255),
    EXCLUDED_FROM_IN_NET_MOOP        VARCHAR2(255),
    EXCLUDED_FROM_OUT_OF_NET_MOOP    VARCHAR2(255),
    NETWORK_T1_COPAY_VAL             VARCHAR2(100 CHAR),
    NETWORK_T1_COPAY_ATTR            VARCHAR2(100 CHAR),
    NETWORK_T1_COINSURANCE_VAL       VARCHAR2(100 CHAR),
    NETWORK_T1_COINSURANCE_ATTR      VARCHAR2(100 CHAR),
    NETWORK_T2_COPAY_VAL             VARCHAR2(100 CHAR),
    NETWORK_T2_COPAY_ATTR            VARCHAR2(100 CHAR),
    NETWORK_T2_COINSURANCE_VAL       VARCHAR2(100 CHAR),
    NETWORK_T2_COINSURANCE_ATTR      VARCHAR2(100 CHAR),
    OUTNETWORK_COPAY_VAL             VARCHAR2(100 CHAR),
    OUTNETWORK_COPAY_ATTR            VARCHAR2(100 CHAR),
    OUTNETWORK_COINSURANCE_VAL       VARCHAR2(100 CHAR),
    OUTNETWORK_COINSURANCE_ATTR      VARCHAR2(100 CHAR),
    CONSTRAINT PK_PLAN_DENTAL_BENEFIT PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLAN_DENTAL_BENEFIT IS 'We capture dental plan benefits in this table. This table describes all benefits and their In network/Out Network attributes, limitations, exceptions values in details.'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.PLAN_DENTAL_ID IS 'Foregin Key into PLAN_DENTAL table.'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.EXCEPTIONS IS 'In-Network Exceptions information for the dental plan'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.LIMITATION_ATTR IS 'In-Network limitation attributes'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.LIMITATION IS 'In-Network limitation values'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NAME IS 'Name of the dental benefit being described.'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.EFFECTIVE_START_DATE IS 'Effective start date for the benefit.'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.EFFECTIVE_END_DATE IS 'Effective end date for this benefit.'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_SUBJ_DEDUCT IS 'In-network subject to deductible'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_EXCL_FRM_MOOP IS 'In-network exclude from Maximum Out of Pocket)'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NONNETWORK_SUBJ_DEDUCT IS 'Out-of-network subject to deductible'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NONNETWORK_EXCL_FRM_MOOP IS 'Out-of-network exclude from Maximum Out of Pocket'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.IS_EHB IS 'HIOS Product Id'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.IS_COVERED IS 'isBenefitCovered'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.MIN_STAY IS 'minimumStay'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.EXPLANATION IS 'explanation'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.SUBJECT_TO_IN_NET_DEDUCTIBLE IS 'subjectToDeductibleTier1'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.SUBJECT_TO_OUT_NET_DEDUCTIBLE IS 'subjectToDeductibleTier2'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.EXCLUDED_FROM_IN_NET_MOOP IS 'excludedInNetworkMOOP'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.EXCLUDED_FROM_OUT_OF_NET_MOOP IS 'excludedOutOfNetworkMOOP'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T1_COPAY_VAL IS 'Column to store TIER 1 Copay Value'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T1_COPAY_ATTR IS 'Column to store TIER 1 Copay Attr'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T1_COINSURANCE_VAL IS 'Column to store TIER 1 Coinsurance Value'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T1_COINSURANCE_ATTR IS 'Column to store TIER 1 Coinsurance Attr'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T2_COPAY_VAL IS 'Column to store TIER 2 Copay Value'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T2_COPAY_ATTR IS 'Column to store TIER 2 Copay Attr'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T2_COINSURANCE_VAL IS 'Column to store TIER 2 Coinsurance Value'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.NETWORK_T2_COINSURANCE_ATTR IS 'Column to store TIER 2 Coinsurance Attr'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.OUTNETWORK_COPAY_VAL IS 'Column to store Out of network Copay Value'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.OUTNETWORK_COPAY_ATTR IS 'Column to store Out of network Copay Attr'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.OUTNETWORK_COINSURANCE_VAL IS 'Column to store Out of network Coinsurance Value'
;
COMMENT ON COLUMN PLAN_DENTAL_BENEFIT.OUTNETWORK_COINSURANCE_ATTR IS 'Column to store Out of network Coinsurance Attr'
;
-- 
-- TABLE: PLAN_DENTAL_COST 
--

CREATE TABLE PLAN_DENTAL_COST(
    ID                              NUMBER(10, 0)    NOT NULL,
    PLAN_DENTAL_ID                  NUMBER(10, 0)    NOT NULL,
    NAME                            VARCHAR2(255)    NOT NULL,
    IN_NETWORK_IND                  NUMBER(10, 2),
    IN_NETWORK_FLY                  NUMBER(10, 2),
    IN_NETWORK_TIER2_IND            NUMBER(10, 2),
    IN_NETWORK_TIER2_FLY            NUMBER(10, 2),
    OUT_NETWORK_IND                 NUMBER(10, 2),
    OUT_NETWORK_FLY                 NUMBER(10, 2),
    COMBINED_IN_OUT_NETWORK_IND     NUMBER(10, 2),
    COMBINED_IN_OUT_NETWORK_FLY     NUMBER(10, 2),
    COMB_DEF_COINS_NETWORK_TIER1    NUMBER(10, 2),
    COMB_DEF_COINS_NETWORK_TIER2    NUMBER(10, 2),
    CONSTRAINT PK_PLAN_DENTAL_COST_ID PRIMARY KEY (ID),
    CONSTRAINT UC_PLAN_DENTAL_COST_01  UNIQUE (PLAN_DENTAL_ID, NAME)
)
;



COMMENT ON TABLE PLAN_DENTAL_COST IS 'Information of Plan Dental Cost will be stored in this table'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.ID IS 'Primary Key.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.PLAN_DENTAL_ID IS 'Foregin Key into PLAN_DENTAL table.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.NAME IS 'Name of the Dental Plan.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.IN_NETWORK_IND IS 'In network of Individual.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.IN_NETWORK_FLY IS 'In network of Family.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.IN_NETWORK_TIER2_IND IS 'In network of Tier 2 Individual.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.IN_NETWORK_TIER2_FLY IS 'In network of Tier 2 Family.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.OUT_NETWORK_IND IS 'Out network of Individual.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.OUT_NETWORK_FLY IS 'Out network of Family.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.COMBINED_IN_OUT_NETWORK_IND IS 'Combined In and Out Network of Individual.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.COMBINED_IN_OUT_NETWORK_FLY IS 'Combined In and Out Network of Family.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.COMB_DEF_COINS_NETWORK_TIER1 IS 'Combined CoInsurance in Network Tier 1.'
;
COMMENT ON COLUMN PLAN_DENTAL_COST.COMB_DEF_COINS_NETWORK_TIER2 IS 'Combined CoInsurance in Network Tier 2.'
;
-- 
-- TABLE: PLAN_HEALTH 
--

CREATE TABLE PLAN_HEALTH(
    ID                               NUMBER            NOT NULL,
    ACTURIAL_VALUE_CERTIFICATE       VARCHAR2(400),
    COST_SHARING                     VARCHAR2(20),
    CREATION_TIMESTAMP               TIMESTAMP(6)      NOT NULL,
    CREATED_BY                       VARCHAR2(40),
    PARENT_PLAN_ID                   NUMBER,
    PLAN_LEVEL                       VARCHAR2(40),
    STATUS                           VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6)      NOT NULL,
    PLAN_ID                          NUMBER,
    BENEFIT_FILE                     VARCHAR2(100),
    RATE_FILE                        VARCHAR2(100),
    RATE_EFFECTIVE_DATE              TIMESTAMP(6),
    BENEFIT_EFFECTIVE_DATE           TIMESTAMP(6),
    COMMENT_ID                       NUMBER,
    EHB_COVERED                      VARCHAR2(40),
    CSR_ADV_PAY_AMT                  FLOAT(126),
    LAST_UPDATED_BY                  NUMBER(15, 9),
    UNIQUE_PLAN_DESIGN               VARCHAR2(1020),
    IS_QHP                           VARCHAR2(1020),
    PREGNANCY_NOTICE                 VARCHAR2(1020),
    SPECIALIST_REFERAL               VARCHAR2(1020),
    PLAN_LEVEL_EXCLUSIONS            VARCHAR2(1020),
    CHILD_ONLY_OFFERING              VARCHAR2(1020),
    WELLNESS_PROGRAM_OFFERED         VARCHAR2(1020),
    DISEASE_MGMT_PROGRAM_OFFERED     VARCHAR2(1020),
    BENEFITS_URL                     VARCHAR2(1020),
    ENROLLMENT_URL                   VARCHAR2(1020),
    AV_CALC_OUTPUT_NUMBER            VARCHAR2(1020),
    MULTIPLE_NETWORK_TIERS           VARCHAR2(1020),
    TIER1_UTIL                       VARCHAR2(1020),
    TIER2_UTIL                       VARCHAR2(1020),
    SBC_DOC_NAME                     VARCHAR2(1020),
    SBC_UCM_ID                       VARCHAR2(1020),
    IS_MED_DRUG_DEDUCT_INTEGRATED    VARCHAR2(40),
    IS_MED_DRUG_MAX_OP_INTEGRATED    VARCHAR2(40),
    PLAN_ATTRIBUTES                  BLOB,
    OUT_OF_COUNTRY_COVERAGE          VARCHAR2(5),
    OUT_OF_SVC_AREA_COVERAGE         VARCHAR2(5),
    NATIONAL_NETWORK                 VARCHAR2(5),
    OUT_OF_COUNTRY_COVERAGE_DESC     VARCHAR2(4000),
    OUT_OF_SVC_AREA_COVERAGE_DESC    VARCHAR2(4000),
    PLAN_SBC_SCENARIO_ID             NUMBER(19, 0),
    CONSTRAINT PK_PLAN_HEALTH PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLAN_HEALTH IS 'This is an audit table of Health Plan data. We use it for audit trail for Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_HEALTH.ACTURIAL_VALUE_CERTIFICATE IS 'Acturial value certificate for health plan.'
;
COMMENT ON COLUMN PLAN_HEALTH.COST_SHARING IS 'Cost sharing attribute for this plan'
;
COMMENT ON COLUMN PLAN_HEALTH.CREATION_TIMESTAMP IS 'Time/Date when this database record was created.'
;
COMMENT ON COLUMN PLAN_HEALTH.CREATED_BY IS 'User that created this database record.'
;
COMMENT ON COLUMN PLAN_HEALTH.PARENT_PLAN_ID IS 'Parent Plan for this health plan. Can be NULL too'
;
COMMENT ON COLUMN PLAN_HEALTH.PLAN_LEVEL IS 'Plan level for this health p'
;
COMMENT ON COLUMN PLAN_HEALTH.STATUS IS 'Status of the health plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_HEALTH.LAST_UPDATE_TIMESTAMP IS 'Date/Time when this database record was updated.'
;
COMMENT ON COLUMN PLAN_HEALTH.PLAN_ID IS 'Foreign KEY into PLAN table'
;
COMMENT ON COLUMN PLAN_HEALTH.BENEFIT_FILE IS 'Pointer to benefit file information on the Content Management system'
;
COMMENT ON COLUMN PLAN_HEALTH.RATE_FILE IS 'Pointer to benefit file information on the Content Management system.'
;
COMMENT ON COLUMN PLAN_HEALTH.RATE_EFFECTIVE_DATE IS 'Effective date for rate information.'
;
COMMENT ON COLUMN PLAN_HEALTH.BENEFIT_EFFECTIVE_DATE IS 'Effective date for benefit information'
;
COMMENT ON COLUMN PLAN_HEALTH.COMMENT_ID IS 'Comment for uploaded document.'
;
COMMENT ON COLUMN PLAN_HEALTH.EHB_COVERED IS 'Covered Essential Health Benefits information.'
;
COMMENT ON COLUMN PLAN_HEALTH.CSR_ADV_PAY_AMT IS 'column contains Cost Sharing Reduction Advance Payment Amount'
;
COMMENT ON COLUMN PLAN_HEALTH.LAST_UPDATED_BY IS 'Health Plan Last Updated By'
;
COMMENT ON COLUMN PLAN_HEALTH.UNIQUE_PLAN_DESIGN IS 'unique_plan_design'
;
COMMENT ON COLUMN PLAN_HEALTH.IS_QHP IS 'is_qhp'
;
COMMENT ON COLUMN PLAN_HEALTH.PREGNANCY_NOTICE IS 'pregnancy_notice'
;
COMMENT ON COLUMN PLAN_HEALTH.SPECIALIST_REFERAL IS 'specialist_referal'
;
COMMENT ON COLUMN PLAN_HEALTH.PLAN_LEVEL_EXCLUSIONS IS 'plan_level_exclusions'
;
COMMENT ON COLUMN PLAN_HEALTH.CHILD_ONLY_OFFERING IS 'child_only_offering'
;
COMMENT ON COLUMN PLAN_HEALTH.WELLNESS_PROGRAM_OFFERED IS 'wellness_program_offered'
;
COMMENT ON COLUMN PLAN_HEALTH.DISEASE_MGMT_PROGRAM_OFFERED IS 'disease_mgmt_program_offered'
;
COMMENT ON COLUMN PLAN_HEALTH.BENEFITS_URL IS 'benefits_url'
;
COMMENT ON COLUMN PLAN_HEALTH.ENROLLMENT_URL IS 'enrollment_url'
;
COMMENT ON COLUMN PLAN_HEALTH.AV_CALC_OUTPUT_NUMBER IS 'av_calc_output_number'
;
COMMENT ON COLUMN PLAN_HEALTH.MULTIPLE_NETWORK_TIERS IS 'multiple_network_tiers'
;
COMMENT ON COLUMN PLAN_HEALTH.TIER1_UTIL IS 'tier1_util'
;
COMMENT ON COLUMN PLAN_HEALTH.TIER2_UTIL IS 'tier2_util'
;
COMMENT ON COLUMN PLAN_HEALTH.SBC_DOC_NAME IS 'This is column tell about SupDocName � the name of the supporting document'
;
COMMENT ON COLUMN PLAN_HEALTH.SBC_UCM_ID IS 'This is column where the location to PDF'
;
COMMENT ON COLUMN PLAN_HEALTH.IS_MED_DRUG_DEDUCT_INTEGRATED IS 'This is column tells if Medical and Drug Deductibles are Integrated or Not'
;
COMMENT ON COLUMN PLAN_HEALTH.IS_MED_DRUG_MAX_OP_INTEGRATED IS 'This is column tells If Medical and Drug Maximum out of Pockets are Integrated'
;
COMMENT ON COLUMN PLAN_HEALTH.PLAN_ATTRIBUTES IS 'Attribute of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH.OUT_OF_COUNTRY_COVERAGE IS 'Out of Country Coverage of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH.OUT_OF_SVC_AREA_COVERAGE IS 'Out of Service Area Coverage of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH.NATIONAL_NETWORK IS 'National Network of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH.OUT_OF_COUNTRY_COVERAGE_DESC IS 'Increasing size'
;
COMMENT ON COLUMN PLAN_HEALTH.OUT_OF_SVC_AREA_COVERAGE_DESC IS 'Increasing size'
;
COMMENT ON COLUMN PLAN_HEALTH.PLAN_SBC_SCENARIO_ID IS 'Foreign Key refrence to PLAN_SBC_SCENARIO table'
;
-- 
-- TABLE: PLAN_HEALTH_AUD 
--

CREATE TABLE PLAN_HEALTH_AUD(
    ID                               NUMBER            NOT NULL,
    REV                              NUMBER            NOT NULL,
    REVTYPE                          NUMBER(3, 0),
    ACTURIAL_VALUE_CERTIFICATE       VARCHAR2(400),
    COST_SHARING                     VARCHAR2(20),
    PARENT_PLAN_ID                   NUMBER,
    PLAN_LEVEL                       VARCHAR2(40),
    STATUS                           VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP            TIMESTAMP(6),
    PLAN_ID                          NUMBER,
    BENEFIT_FILE                     VARCHAR2(100),
    RATE_FILE                        VARCHAR2(100),
    RATE_EFFECTIVE_DATE              TIMESTAMP(6),
    BENEFIT_EFFECTIVE_DATE           TIMESTAMP(6),
    COMMENT_ID                       NUMBER,
    EHB_COVERED                      VARCHAR2(40),
    CSR_ADV_PAY_AMT                  FLOAT(126),
    LAST_UPDATED_BY                  NUMBER(15, 9),
    UNIQUE_PLAN_DESIGN               VARCHAR2(1020),
    IS_QHP                           VARCHAR2(1020),
    PREGNANCY_NOTICE                 VARCHAR2(1020),
    SPECIALIST_REFERAL               VARCHAR2(1020),
    PLAN_LEVEL_EXCLUSIONS            VARCHAR2(1020),
    CHILD_ONLY_OFFERING              VARCHAR2(1020),
    WELLNESS_PROGRAM_OFFERED         VARCHAR2(1020),
    DISEASE_MGMT_PROGRAM_OFFERED     VARCHAR2(1020),
    BENEFITS_URL                     VARCHAR2(1020),
    ENROLLMENT_URL                   VARCHAR2(1020),
    AV_CALC_OUTPUT_NUMBER            VARCHAR2(1020),
    MULTIPLE_NETWORK_TIERS           VARCHAR2(1020),
    TIER1_UTIL                       VARCHAR2(1020),
    TIER2_UTIL                       VARCHAR2(1020),
    SBC_DOC_NAME                     VARCHAR2(1020),
    SBC_UCM_ID                       VARCHAR2(1020),
    IS_MED_DRUG_DEDUCT_INTEGRATED    VARCHAR2(40),
    IS_MED_DRUG_MAX_OP_INTEGRATED    VARCHAR2(40),
    PLAN_ATTRIBUTES                  BLOB,
    OUT_OF_COUNTRY_COVERAGE          VARCHAR2(5),
    OUT_OF_SVC_AREA_COVERAGE         VARCHAR2(5),
    NATIONAL_NETWORK                 VARCHAR2(5),
    OUT_OF_COUNTRY_COVERAGE_DESC     VARCHAR2(4000),
    OUT_OF_SVC_AREA_COVERAGE_DESC    VARCHAR2(4000),
    CREATED_BY                       VARCHAR2(60),
    CREATION_TIMESTAMP               TIMESTAMP(6),
    PLAN_SBC_SCENARIO_ID             NUMBER(19, 0),
    CONSTRAINT PK_PLAN_HEALTH_AUD PRIMARY KEY (ID, REV)
)
;



COMMENT ON TABLE PLAN_HEALTH_AUD IS 'Information of Plan Health Auditing data will be stored in this table'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.REV IS 'Revision number of the audit record.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.REVTYPE IS 'Type of revision of audit record.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.ACTURIAL_VALUE_CERTIFICATE IS 'Acturial value certificate for health plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.COST_SHARING IS 'Cost sharing attribute for this plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.PARENT_PLAN_ID IS 'Parent Plan for this health plan. Can be NULL too'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.PLAN_LEVEL IS 'Plan level for this health p'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.STATUS IS 'Status of the health plan on the Exchange.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.LAST_UPDATE_TIMESTAMP IS 'Date/Time when this database record was updated.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.PLAN_ID IS 'Foreign KEY into PLAN table'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.BENEFIT_FILE IS 'Pointer to benefit file information on the Content Management system'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.RATE_FILE IS 'Pointer to benefit file information on the Content Management system.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.RATE_EFFECTIVE_DATE IS 'Effective date for rate information.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.BENEFIT_EFFECTIVE_DATE IS 'Effective date for benefit information'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.COMMENT_ID IS 'Comment for uploaded document.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.EHB_COVERED IS 'Covered Essential Health Benefits information.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.CSR_ADV_PAY_AMT IS 'column contains Cost Sharing Reduction Advance Payment Amount'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.LAST_UPDATED_BY IS 'Health Plan Last Update By'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.UNIQUE_PLAN_DESIGN IS 'unique_plan_design'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.IS_QHP IS 'is_qhp'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.PREGNANCY_NOTICE IS 'pregnancy_notice'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.SPECIALIST_REFERAL IS 'specialist_referal'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.PLAN_LEVEL_EXCLUSIONS IS 'plan_level_exclusions'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.CHILD_ONLY_OFFERING IS 'child_only_offering'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.WELLNESS_PROGRAM_OFFERED IS 'wellness_program_offered'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.DISEASE_MGMT_PROGRAM_OFFERED IS 'disease_mgmt_program_offered'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.BENEFITS_URL IS 'benefits_url'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.ENROLLMENT_URL IS 'enrollment_url'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.AV_CALC_OUTPUT_NUMBER IS 'av_calc_output_number'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.MULTIPLE_NETWORK_TIERS IS 'multiple_network_tiers'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.TIER1_UTIL IS 'tier1_util'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.TIER2_UTIL IS 'tier2_util'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.SBC_DOC_NAME IS 'This is column tell about SupDocName � the name of the supporting document'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.SBC_UCM_ID IS 'This is column where the location to PDF'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.IS_MED_DRUG_DEDUCT_INTEGRATED IS 'This is column tells if Medical and Drug Deductibles are Integrated or Not'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.IS_MED_DRUG_MAX_OP_INTEGRATED IS 'This is column tells If Medical and Drug Maximum out of Pockets are Integrated'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.PLAN_ATTRIBUTES IS 'Attributes of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.OUT_OF_COUNTRY_COVERAGE IS 'Out of Country Coverage of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.OUT_OF_SVC_AREA_COVERAGE IS 'Out of Service Area Coverage of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.NATIONAL_NETWORK IS 'National Network of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.OUT_OF_COUNTRY_COVERAGE_DESC IS 'Description of Out of Country Coverage of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.OUT_OF_SVC_AREA_COVERAGE_DESC IS 'Description of Out of Service Area Coverage of Health Plan'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.CREATED_BY IS 'User that created this database record.'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.CREATION_TIMESTAMP IS 'Time/Date when this database record was created.'
;
-- 
-- TABLE: PLAN_HEALTH_BENEFIT 
--

CREATE TABLE PLAN_HEALTH_BENEFIT(
    ID                                NUMBER            NOT NULL,
    PLAN_HEALTH_ID                    NUMBER,
    LIMITATION                        VARCHAR2(20),
    LIMITATION_ATTR                   VARCHAR2(50),
    EXCEPTIONS                        VARCHAR2(255),
    NAME                              VARCHAR2(400)     NOT NULL,
    EFFECTIVE_START_DATE              TIMESTAMP(6),
    EFFECTIVE_END_DATE                TIMESTAMP(6),
    SUBJECT_TO_IN_NET_DEDUCTIBLE      VARCHAR2(255),
    EXCLUDED_FROM_IN_NET_MOOP         VARCHAR2(255),
    SUBJECT_TO_OUT_NET_DEDUCTIBLE     VARCHAR2(255),
    EXCLUDED_FROM_OUT_OF_NET_MOOP     VARCHAR2(255),
    COMBINED_IN_AND_OUT_OF_NETWORK    VARCHAR2(255),
    COMBINED_IN_AND_OUT_NET_ATTR      VARCHAR2(255),
    IS_EHB                            VARCHAR2(40),
    IS_COVERED                        VARCHAR2(40),
    MIN_STAY                          VARCHAR2(40),
    EXPLANATION                       VARCHAR2(1020),
    IN_NETWORK_INDIVIDUAL             NUMBER,
    IN_NETWORK_FAMILY                 NUMBER,
    IN_NETWORK_TIER_TWO_INDIVIDUAL    NUMBER,
    IN_NETWORK_TIER_TWO_FAMILY        NUMBER,
    OUT_OF_NETWORK_INDIVIDUAL         NUMBER,
    OUT_OF_NETWORK_FAMILY             NUMBER,
    COMBINED_IN_OUT_NETWORK_IND       NUMBER,
    COMBINED_IN_OUT_NETWORK_FAMILY    NUMBER,
    NETWORK_T1_COPAY_VAL              VARCHAR2(100 CHAR),
    NETWORK_T1_COPAY_ATTR             VARCHAR2(100 CHAR),
    NETWORK_T1_COINSURANCE_VAL        VARCHAR2(100 CHAR),
    NETWORK_T1_COINSURANCE_ATTR       VARCHAR2(100 CHAR),
    NETWORK_T2_COPAY_VAL              VARCHAR2(100 CHAR),
    NETWORK_T2_COPAY_ATTR             VARCHAR2(100 CHAR),
    NETWORK_T2_COINSURANCE_VAL        VARCHAR2(100 CHAR),
    NETWORK_T2_COINSURANCE_ATTR       VARCHAR2(100 CHAR),
    OUTNETWORK_COPAY_VAL              VARCHAR2(100 CHAR),
    OUTNETWORK_COPAY_ATTR             VARCHAR2(100 CHAR),
    OUTNETWORK_COINSURANCE_VAL        VARCHAR2(100 CHAR),
    OUTNETWORK_COINSURANCE_ATTR       VARCHAR2(100 CHAR),
    CONSTRAINT PK_PLAN_HEALTH_BENEFIT PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLAN_HEALTH_BENEFIT IS 'We capture health plan benefits in this table. This table describes all benefits and their In network/Out Network attributes, limitations, exceptions values in details.'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.PLAN_HEALTH_ID IS 'Foregin Key into PLAN_HEALTH table.'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.LIMITATION IS 'In-Network limitation values'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.LIMITATION_ATTR IS 'In-Network limitation attributes'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.EXCEPTIONS IS 'In-Network Exceptions information for the health plan'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NAME IS 'Name of the dental benefit being described.'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.EFFECTIVE_START_DATE IS 'Effective start date for the benefit.'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.EFFECTIVE_END_DATE IS 'Effective end date for this benefit.'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.SUBJECT_TO_IN_NET_DEDUCTIBLE IS 'In-network subject to deductible'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.EXCLUDED_FROM_IN_NET_MOOP IS 'In-network exclude from Maximum Out of Pocket)'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.SUBJECT_TO_OUT_NET_DEDUCTIBLE IS 'Out-of-network subject to deductible'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.EXCLUDED_FROM_OUT_OF_NET_MOOP IS 'Out-of-network exclude from Maximum Out of Pocket'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.COMBINED_IN_AND_OUT_OF_NETWORK IS 'Combined in and out of network benefit value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.COMBINED_IN_AND_OUT_NET_ATTR IS 'Combined in and out of network benefit value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.IS_EHB IS 'HIOS Product Id'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.IS_COVERED IS 'HPID'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.MIN_STAY IS 'NETWORK ID'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.EXPLANATION IS 'NETWORK ID'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.IN_NETWORK_INDIVIDUAL IS 'In network individual'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.IN_NETWORK_FAMILY IS 'In network family'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.IN_NETWORK_TIER_TWO_INDIVIDUAL IS 'In network tier two individual'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.IN_NETWORK_TIER_TWO_FAMILY IS 'In network tier two family'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.OUT_OF_NETWORK_INDIVIDUAL IS 'out of network individual'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.OUT_OF_NETWORK_FAMILY IS 'out of network family'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.COMBINED_IN_OUT_NETWORK_IND IS 'combined in out network individual'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.COMBINED_IN_OUT_NETWORK_FAMILY IS 'combined in out network family'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T1_COPAY_VAL IS 'Column to store TIER 1 Copay Value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T1_COPAY_ATTR IS 'Column to store TIER 1 Copay Attr'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T1_COINSURANCE_VAL IS 'Column to store TIER 1 Coinsurance Value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T1_COINSURANCE_ATTR IS 'Column to store TIER 1 Coinsurance Attr'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T2_COPAY_VAL IS 'Column to store TIER 2 Copay Value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T2_COPAY_ATTR IS 'Column to store TIER 2 Copay Attr'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T2_COINSURANCE_VAL IS 'Column to store TIER 2 Coinsurance Value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.NETWORK_T2_COINSURANCE_ATTR IS 'Column to store TIER 2 Coinsurance Attr'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.OUTNETWORK_COPAY_VAL IS 'Column to store Out of network Copay Value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.OUTNETWORK_COPAY_ATTR IS 'Column to store Out of network Copay Attr'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.OUTNETWORK_COINSURANCE_VAL IS 'Column to store Out of network Coinsurance Value'
;
COMMENT ON COLUMN PLAN_HEALTH_BENEFIT.OUTNETWORK_COINSURANCE_ATTR IS 'Column to store Out of network Coinsurance Attr'
;
-- 
-- TABLE: PLAN_HEALTH_COST 
--

CREATE TABLE PLAN_HEALTH_COST(
    ID                              NUMBER(10, 0)    NOT NULL,
    PLAN_HEALTH_ID                  NUMBER(10, 0)    NOT NULL,
    NAME                            VARCHAR2(255)    NOT NULL,
    IN_NETWORK_IND                  NUMBER(10, 2),
    IN_NETWORK_FLY                  NUMBER(10, 2),
    IN_NETWORK_TIER2_IND            NUMBER(10, 2),
    IN_NETWORK_TIER2_FLY            NUMBER(10, 2),
    OUT_NETWORK_IND                 NUMBER(10, 2),
    OUT_NETWORK_FLY                 NUMBER(10, 2),
    COMBINED_IN_OUT_NETWORK_IND     NUMBER(10, 2),
    COMBINED_IN_OUT_NETWORK_FLY     NUMBER(10, 2),
    COMB_DEF_COINS_NETWORK_TIER1    NUMBER(10, 2),
    COMB_DEF_COINS_NETWORK_TIER2    NUMBER(10, 2),
    CONSTRAINT PK_PLAN_HEALTH_COST_ID PRIMARY KEY (ID),
    CONSTRAINT UC_PLAN_HEALTH_COST_01  UNIQUE (PLAN_HEALTH_ID, NAME)
)
;



COMMENT ON TABLE PLAN_HEALTH_COST IS 'Information of Plan Health Cost will be stored in this table'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.ID IS 'Primary Key.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.PLAN_HEALTH_ID IS 'Foregin Key into PLAN_HEALTH table.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.NAME IS 'Name of the Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.IN_NETWORK_IND IS 'In Network of Individual Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.IN_NETWORK_FLY IS 'In Network of Family Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.IN_NETWORK_TIER2_IND IS 'In Network of Tire 2 Individual Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.IN_NETWORK_TIER2_FLY IS 'In Network of Tire 2 Family Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.OUT_NETWORK_IND IS 'Out Network of Individual Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.OUT_NETWORK_FLY IS 'Out Network of Family Health Plan.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.COMBINED_IN_OUT_NETWORK_IND IS 'Combined In and Out Network of Individual.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.COMBINED_IN_OUT_NETWORK_FLY IS 'Combined In and Out Network of Family.'
;
COMMENT ON COLUMN PLAN_HEALTH_COST.COMB_DEF_COINS_NETWORK_TIER1 IS 'Combined Def. of CoInsurance Network Tier 1.'
;
-- 
-- TABLE: PLAN_QUALITY_RATING 
--

CREATE TABLE PLAN_QUALITY_RATING(
    ID                        NUMBER           NOT NULL,
    QUALITY_RATING            VARCHAR2(40),
    QUALITY_RATING_DETAILS    VARCHAR2(800),
    QUALITY_SOURCE            VARCHAR2(200),
    PLAN_ID                   NUMBER,
    CONSTRAINT PK_PLAN_QUALITY_RATING PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLAN_QUALITY_RATING IS 'This table describes informaton about quality ratings for a plan.'
;
COMMENT ON COLUMN PLAN_QUALITY_RATING.ID IS 'Primary Key'
;
COMMENT ON COLUMN PLAN_QUALITY_RATING.QUALITY_RATING IS 'Quality ratings summary for a plan.'
;
COMMENT ON COLUMN PLAN_QUALITY_RATING.QUALITY_RATING_DETAILS IS 'Quality ratings details for a plan.'
;
COMMENT ON COLUMN PLAN_QUALITY_RATING.QUALITY_SOURCE IS 'Quality ratings source for a plan.'
;
COMMENT ON COLUMN PLAN_QUALITY_RATING.PLAN_ID IS 'Foreign Key into the PLAN table.'
;
-- 
-- TABLE: PLAN_SBC_SCENARIO 
--

CREATE TABLE PLAN_SBC_SCENARIO(
    ID                      NUMBER(10, 0)    NOT NULL,
    BABY_COINSURANCE        NUMBER(10, 0),
    BABY_COPAYMENT          NUMBER(10, 0),
    BABY_DEDUCTIBLE         NUMBER(10, 0),
    BABY_LIMIT              NUMBER(10, 0),
    DIABETES_COINSURANCE    NUMBER(10, 0),
    DIABETES_COPAY          NUMBER(10, 0),
    DIABETES_DEDUCTIBLE     NUMBER(10, 0),
    DIABETES_LIMIT          NUMBER(10, 0),
    CONSTRAINT PK_SBC_SCENARIO PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN PLAN_SBC_SCENARIO.ID IS 'PK for PLAN_SBC_SCENARIO'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.BABY_COINSURANCE IS 'Cost_Share_Variances_1-N.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.BABY_COPAYMENT IS 'Cost_Share_Variances_1-M.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.BABY_DEDUCTIBLE IS 'Cost_Share_Variances_1-L.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.BABY_LIMIT IS 'Cost_Share_Variances_1-O.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.DIABETES_COINSURANCE IS 'Cost_Share_Variances_1-R.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.DIABETES_COPAY IS 'Cost_Share_Variances_1-Q.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.DIABETES_DEDUCTIBLE IS 'Cost_Share_Variances_1-P.8'
;
COMMENT ON COLUMN PLAN_SBC_SCENARIO.DIABETES_LIMIT IS 'Cost_Share_Variances_1-S.8'
;
-- 
-- TABLE: PLD_GROUP 
--

CREATE TABLE PLD_GROUP(
    ID                       NUMBER            NOT NULL,
    GROUP_NAME               VARCHAR2(256),
    PLD_HOUSEHOLD_ID         NUMBER,
    COVERAGE_START_DATE      TIMESTAMP(6),
    COVERAGE_END_DATE        TIMESTAMP(6),
    PLAN_PREF                VARCHAR2(1020),
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    CREATED_BY               NUMBER,
    LAST_UPDATED_BY          NUMBER,
    MONTHLY_APTC             FLOAT(126)        DEFAULT 0,
    YEARLY_APTC              FLOAT(126)        DEFAULT 0,
    PROVIDERS                VARCHAR2(4000),
    CONSTRAINT PK_PLD_GROUPS PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLD_GROUP IS 'This entity represents the group of household members'
;
COMMENT ON COLUMN PLD_GROUP.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_GROUP.GROUP_NAME IS 'Name of the group'
;
COMMENT ON COLUMN PLD_GROUP.PLD_HOUSEHOLD_ID IS 'Foreign key to the PLD_HOUSEHOLD table'
;
COMMENT ON COLUMN PLD_GROUP.COVERAGE_START_DATE IS 'Benefit/Plan coverage start date'
;
COMMENT ON COLUMN PLD_GROUP.COVERAGE_END_DATE IS 'Benefit/Plan coverage end date'
;
COMMENT ON COLUMN PLD_GROUP.PLAN_PREF IS 'Blob that stores user provided plan preferences that is used for sorting and filtering plans'
;
COMMENT ON COLUMN PLD_GROUP.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_GROUP.LAST_UPDATE_TIMESTAMP IS 'Last updated date for this record'
;
COMMENT ON COLUMN PLD_GROUP.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_GROUP.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN PLD_GROUP.MONTHLY_APTC IS 'The applied monthly tax credit amount'
;
COMMENT ON COLUMN PLD_GROUP.YEARLY_APTC IS 'The applied yearly tax credit amount'
;
COMMENT ON COLUMN PLD_GROUP.PROVIDERS IS 'Blob that stores user provided medical conditions and usage which is used for cost estimation.'
;
-- 
-- TABLE: PLD_GROUP_PERSON 
--

CREATE TABLE PLD_GROUP_PERSON(
    ID                       NUMBER          NOT NULL,
    PLD_PERSON_ID            NUMBER,
    PLD_GROUP_ID             NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6)    NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)    NOT NULL,
    CREATED_BY               NUMBER,
    LAST_UPDATED_BY          NUMBER,
    CONSTRAINT PK_PLD_GROUP_PERSON PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PLD_GROUP_PERSON IS 'This entity links the member to corresponding group'
;
COMMENT ON COLUMN PLD_GROUP_PERSON.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_GROUP_PERSON.PLD_PERSON_ID IS 'Foreign key to the PLD_PERSON table'
;
COMMENT ON COLUMN PLD_GROUP_PERSON.PLD_GROUP_ID IS 'Foreign key to the PLD_GROUP table '
;
COMMENT ON COLUMN PLD_GROUP_PERSON.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_GROUP_PERSON.LAST_UPDATE_TIMESTAMP IS 'Last updated date for this record'
;
COMMENT ON COLUMN PLD_GROUP_PERSON.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_GROUP_PERSON.LAST_UPDATED_BY IS 'User did the last update'
;
-- 
-- TABLE: PLD_HOUSEHOLD 
--

CREATE TABLE PLD_HOUSEHOLD(
    ID                       NUMBER            NOT NULL,
    EXTERNAL_ID              VARCHAR2(1020),
    ENROLLMENT_TYPE          CHAR(1),
    MONTHLY_APTC             FLOAT(126)        DEFAULT 0,
    YEARLY_APTC              FLOAT(126)        DEFAULT 0,
    COST_SHARING             VARCHAR2(20),
    SHOPPING_GROUP           VARCHAR2(20)      DEFAULT 'HOUSEHOLD',
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    CREATED_BY               NUMBER,
    LAST_UPDATED_BY          NUMBER,
    COVERAGE_START_DATE      TIMESTAMP(6),
    PROGRAM_TYPE             VARCHAR2(20 CHAR),
    SHOPPING_ID              VARCHAR2(200 CHAR) NOT NULL,
    HOUSEHOLD_DATA           CLOB,
    EXTERNAL_EMPLOYER_ID     VARCHAR2(1020),
    IS_SUBSIDIZED            VARCHAR2(10 CHAR),
    EMPLOYER_ID              NUMBER,
    CONSTRAINT PK_PLD_HOUSEHOLD PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PLD_HOUSEHOLD IS 'This entity represents the input household data in the exchange for plan shopping.'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.EXTERNAL_ID IS 'An identifier to uniquely identify the household in AHBX'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.ENROLLMENT_TYPE IS 'An indicator to denote the type of enrollment (Initial, Annual)'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.MONTHLY_APTC IS 'The monthly tax credit amount based on the Individual.s income which is provided on an advance basis to an eligible Individual enrolled in a QHP through the Exchange'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.YEARLY_APTC IS 'The annual tax credit amount based on the individual�s income which is provided on an advance basis to an eligible Individual enrolled in a QHP through the Exchange'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.COST_SHARING IS 'Cost Sharing Reductions (CSR) Category'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.SHOPPING_GROUP IS 'Shopping Group.  The values can be: INDIVIDUAL, HOUSEHOLD, CUSTOM'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.COVERAGE_START_DATE IS 'Benefit/Plan year start date defined by eligibility'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.PROGRAM_TYPE IS 'Program type.  The values can be: INDIVIDUAL or SHOP'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.SHOPPING_ID IS 'Unique ID to identify the shopping transaction.  Need more information'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.HOUSEHOLD_DATA IS 'Eligibility data for household'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.EXTERNAL_EMPLOYER_ID IS 'External employer id'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.IS_SUBSIDIZED IS 'Stores YES/NO to indicate whether the household will see subsidized/unsubsidized plans'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.EMPLOYER_ID IS 'Internal employer id'
;
-- 
-- TABLE: PLD_HOUSEHOLD_PERSON 
--

CREATE TABLE PLD_HOUSEHOLD_PERSON(
    ID                       NUMBER          NOT NULL,
    PLD_HOUSEHOLD_ID         NUMBER,
    PLD_PERSON_ID            NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6)    NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)    NOT NULL,
    CREATED_BY               NUMBER,
    LAST_UPDATED_BY          NUMBER,
    CONSTRAINT PLD_HOUSEHOLD_PERSON_PK PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PLD_HOUSEHOLD_PERSON IS 'This entity links the member to corresponding household'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.PLD_HOUSEHOLD_ID IS 'Foreign key to the PLD_HOUSEHOLD table'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.PLD_PERSON_ID IS 'Foreign key to the PLD_PERSON table'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_HOUSEHOLD_PERSON.LAST_UPDATED_BY IS 'User did the last update'
;
-- 
-- TABLE: PLD_ORDER 
--

CREATE TABLE PLD_ORDER(
    ID                       NUMBER           NOT NULL,
    PROVIDERS                VARCHAR2(500),
    QUALITY_RATING           VARCHAR2(500),
    STATUS                   VARCHAR2(40),
    PLD_HOUSEHOLD_ID         NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6)     NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    CREATED_BY               NUMBER,
    LAST_UPDATED_BY          NUMBER,
    SADP_FLAG                VARCHAR2(20 CHAR),
    CONSTRAINT PK_PLD_ORDER PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PLD_ORDER IS 'This entity represents the order created during individual plan selection'
;
COMMENT ON COLUMN PLD_ORDER.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_ORDER.PROVIDERS IS 'List of the providers chosen during individual plan selection'
;
COMMENT ON COLUMN PLD_ORDER.QUALITY_RATING IS 'List of the plan quality ratings specified by user'
;
COMMENT ON COLUMN PLD_ORDER.STATUS IS 'Status of Order (CART/ORDER)'
;
COMMENT ON COLUMN PLD_ORDER.PLD_HOUSEHOLD_ID IS 'Foreign key to the PLD_HOUSEHOLD table'
;
COMMENT ON COLUMN PLD_ORDER.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_ORDER.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN PLD_ORDER.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_ORDER.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN PLD_ORDER.SADP_FLAG IS 'Is it a Stand Alone Dental Plan (SADP)?'
;
-- 
-- TABLE: PLD_ORDER_ITEM 
--

CREATE TABLE PLD_ORDER_ITEM(
    ID                       NUMBER          NOT NULL,
    PLD_ORDER_ID             NUMBER,
    PLAN_ID                  NUMBER,
    CREATION_TIMESTAMP       TIMESTAMP(6),
    PREMIUM                  FLOAT(126),
    PLD_GROUP_ID             NUMBER          NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)    NOT NULL,
    CREATED_BY               NUMBER,
    LAST_UPDATED_BY          NUMBER,
    QUOTING_DATA             CLOB,
    INSURANCE_TYPE           VARCHAR2(40),
    CONSTRAINT PK_PLD_ORDER_ITEM PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PLD_ORDER_ITEM IS 'This entity represents the plan chosen for each order as part of individual plan selection'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.PLD_ORDER_ID IS 'Foreign key to the PLD_ORDER table'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.PLAN_ID IS 'Foreign KEY to the PLAN table'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.PREMIUM IS 'Plan premium'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.PLD_GROUP_ID IS 'Foreign key to the PLD_GROUP table'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN PLD_ORDER_ITEM.QUOTING_DATA IS 'Quoting data of the order item'
;
-- 
-- TABLE: PLD_PERSON 
--

CREATE TABLE PLD_PERSON(
    ID                         NUMBER            NOT NULL,
    BIRTH_DAY                  TIMESTAMP(6),
    FIRST_NAME                 VARCHAR2(100),
    LAST_NAME                  VARCHAR2(100),
    EXTERNAL_ID                VARCHAR2(1020),
    RELATIONSHIP               VARCHAR2(1020),
    CREATION_TIMESTAMP         TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP      TIMESTAMP(6),
    CREATED_BY                 NUMBER,
    LAST_UPDATED_BY            NUMBER,
    QUOTING_COUNTY             VARCHAR2(30 CHAR),
    PERSON_DATA                CLOB,
    COVERAGE_ELIGIBLE          VARCHAR2(20 CHAR),
    CATASTROPHIC_ELIGIBLE      VARCHAR2(20 CHAR),
    FIN_HARDSHIP_ELIGIBLE      VARCHAR2(20 CHAR),
    CHILDONLY_PLAN_ELIGIBLE    VARCHAR2(20 CHAR),
    SHOP_RELATION              VARCHAR2(10 CHAR),
    QUOTING_ZIP                VARCHAR2(10),
    HOME_ZIP                   VARCHAR2(10),
    TOBACCO                    VARCHAR2(2 CHAR),
    CONSTRAINT PK_PDL_PERSON PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PLD_PERSON IS 'This entity represents the household member'
;
COMMENT ON COLUMN PLD_PERSON.ID IS 'Auto generated identity column'
;
COMMENT ON COLUMN PLD_PERSON.BIRTH_DAY IS 'Date of Birth of the member'
;
COMMENT ON COLUMN PLD_PERSON.FIRST_NAME IS 'First Name of the member'
;
COMMENT ON COLUMN PLD_PERSON.LAST_NAME IS 'Last Name of the member'
;
COMMENT ON COLUMN PLD_PERSON.EXTERNAL_ID IS 'An identifier to uniquely identify each member of the household'
;
COMMENT ON COLUMN PLD_PERSON.RELATIONSHIP IS 'Relationship to the household contact'
;
COMMENT ON COLUMN PLD_PERSON.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN PLD_PERSON.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update'
;
COMMENT ON COLUMN PLD_PERSON.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PLD_PERSON.LAST_UPDATED_BY IS 'User did the last update'
;
COMMENT ON COLUMN PLD_PERSON.QUOTING_COUNTY IS 'quoting  county code used to  quote employee'
;
COMMENT ON COLUMN PLD_PERSON.PERSON_DATA IS 'Eligibility data for member, stored as xml blob'
;
COMMENT ON COLUMN PLD_PERSON.COVERAGE_ELIGIBLE IS 'Eligible for subsidy with QHP plans'
;
COMMENT ON COLUMN PLD_PERSON.CATASTROPHIC_ELIGIBLE IS 'The eligibility for catastrophic plans'
;
COMMENT ON COLUMN PLD_PERSON.FIN_HARDSHIP_ELIGIBLE IS 'Eligible for financial hardship exemption'
;
COMMENT ON COLUMN PLD_PERSON.CHILDONLY_PLAN_ELIGIBLE IS 'The eligibility for child-only plans'
;
COMMENT ON COLUMN PLD_PERSON.SHOP_RELATION IS 'Relation of shop members'
;
-- 
-- TABLE: PM_ISSUER_SERVICE_AREA 
--

CREATE TABLE PM_ISSUER_SERVICE_AREA(
    ID                       NUMBER(19, 0)     NOT NULL,
    SERFF_SERVICE_AREA_ID    VARCHAR2(1020),
    SERVICE_AREA_NAME        VARCHAR2(1020),
    COMPLETE_STATE           VARCHAR2(20),
    ISSUER_STATE             VARCHAR2(1020),
    HIOS_ISSUER_ID           VARCHAR2(5 CHAR),
    CONSTRAINT PK_ISSUER_SERVICE_AREA PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PM_ISSUER_SERVICE_AREA IS 'Plan Management Issuer Servive Area information will be stored in this table'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA.ID IS 'Primary Key'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA.SERFF_SERVICE_AREA_ID IS 'Id for the service area'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA.SERVICE_AREA_NAME IS 'Name of the service area'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA.COMPLETE_STATE IS 'Flag to indicate if the serivce area covers entire state'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA.ISSUER_STATE IS 'ISSUER_STATE'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA.HIOS_ISSUER_ID IS 'Change column type from NUMBER(5) to VARCHAR2(5 CHAR)'
;
-- 
-- TABLE: PM_ISSUER_SERVICE_AREA_EXT 
--

CREATE TABLE PM_ISSUER_SERVICE_AREA_EXT(
    ID                              NUMBER(19, 0)     NOT NULL,
    ISSUER_SERVICE_AREA_ID          NUMBER(19, 0),
    COUNTY                          VARCHAR2(1000 CHAR),
    PARTIAL_COUNTY                  VARCHAR2(20 CHAR),
    PARTIAL_COUNTY_JUSTIFICATION    VARCHAR2(1000 CHAR),
    CONSTRAINT PK_ISSUER_SERVICE_AREA_EXT PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA_EXT.ID IS 'Primary Key'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA_EXT.ISSUER_SERVICE_AREA_ID IS 'Foreign Key refrence to PM_ISSUER_SERVICE_AREA table'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA_EXT.COUNTY IS 'County name'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA_EXT.PARTIAL_COUNTY IS 'Flag to indicate service area covers only partial county'
;
COMMENT ON COLUMN PM_ISSUER_SERVICE_AREA_EXT.PARTIAL_COUNTY_JUSTIFICATION IS 'Justification text for partial county'
;
-- 
-- TABLE: PM_PLAN_RATE 
--

CREATE TABLE PM_PLAN_RATE(
    ID                       NUMBER          NOT NULL,
    RATING_AREA_ID           NUMBER,
    PLAN_ID                  NUMBER,
    MIN_AGE                  NUMBER,
    MAX_AGE                  NUMBER,
    TOBACCO                  VARCHAR2(1),
    RATE                     FLOAT(126),
    EFFECTIVE_START_DATE     TIMESTAMP(6),
    EFFECTIVE_END_DATE       TIMESTAMP(6),
    CREATION_TIMESTAMP       TIMESTAMP(6),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    RATE_OPTION              VARCHAR2(1 CHAR),
    IS_DELETED               VARCHAR2(1 CHAR) DEFAULT 'N',
    CONSTRAINT PK_PM_PLAN_RATE PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PM_PLAN_RATE IS 'Plan Management Plan Rate information will be stored in this table'
;
COMMENT ON COLUMN PM_PLAN_RATE.ID IS 'Primary key of this table'
;
COMMENT ON COLUMN PM_PLAN_RATE.RATING_AREA_ID IS 'Foreign key. Capture Primary key of PM_RATING_AREA table'
;
COMMENT ON COLUMN PM_PLAN_RATE.PLAN_ID IS 'Foreign key. Capture Primary key of PM_PLAN table'
;
COMMENT ON COLUMN PM_PLAN_RATE.MIN_AGE IS 'Capture minimum age range'
;
COMMENT ON COLUMN PM_PLAN_RATE.MAX_AGE IS 'Capture maximum age range'
;
COMMENT ON COLUMN PM_PLAN_RATE.TOBACCO IS 'Y or N to indicate if the rate type is for tobacco user or not'
;
COMMENT ON COLUMN PM_PLAN_RATE.RATE IS 'Insurance Rate or Premium'
;
COMMENT ON COLUMN PM_PLAN_RATE.EFFECTIVE_START_DATE IS 'Effective Start Date for this rate'
;
COMMENT ON COLUMN PM_PLAN_RATE.EFFECTIVE_END_DATE IS 'Effective end Date for this rate'
;
COMMENT ON COLUMN PM_PLAN_RATE.CREATION_TIMESTAMP IS 'Record creation time stamp'
;
COMMENT ON COLUMN PM_PLAN_RATE.LAST_UPDATE_TIMESTAMP IS 'Record updation time stamp'
;
COMMENT ON COLUMN PM_PLAN_RATE.RATE_OPTION IS 'A- Age Option, F-Family Option. For family option look up values are available in look up table.'
;
COMMENT ON COLUMN PM_PLAN_RATE.IS_DELETED IS 'Describes whether it is a deleted RATES'
;
-- 
-- TABLE: PM_RATING_AREA 
--

CREATE TABLE PM_RATING_AREA(
    ID             NUMBER          NOT NULL,
    STATE          VARCHAR2(2 CHAR),
    RATING_AREA    VARCHAR2(15 CHAR),
    CONSTRAINT PK_PM_RATING_AREA PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PM_RATING_AREA IS 'Plan Management Rating Area information will be stored in this table'
;
COMMENT ON COLUMN PM_RATING_AREA.ID IS 'Primary key of this table'
;
COMMENT ON COLUMN PM_RATING_AREA.STATE IS 'Capture state abbreviation'
;
COMMENT ON COLUMN PM_RATING_AREA.RATING_AREA IS 'Capture rating area name'
;
-- 
-- TABLE: PM_SERVICE_AREA 
--

CREATE TABLE PM_SERVICE_AREA(
    ID                     NUMBER           NOT NULL,
    SERVICE_AREA_ID        NUMBER,
    ZIP                    VARCHAR2(5),
    COUNTY                 VARCHAR2(25),
    FIPS                   VARCHAR2(5),
    STATE                  VARCHAR2(2),
    SERVICE_AREA_EXT_ID    NUMBER(19, 0),
    CONSTRAINT PK_PM_SERVICE_AREA PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PM_SERVICE_AREA IS 'Plan Management Service Area information will be stored in this table'
;
COMMENT ON COLUMN PM_SERVICE_AREA.ID IS 'Primary key of this table'
;
COMMENT ON COLUMN PM_SERVICE_AREA.SERVICE_AREA_ID IS 'Foreign key. Capture Primary key of PM_ISSUER_SERVICE_AREA table'
;
COMMENT ON COLUMN PM_SERVICE_AREA.ZIP IS 'Capture zip code'
;
COMMENT ON COLUMN PM_SERVICE_AREA.COUNTY IS 'Capture county name'
;
COMMENT ON COLUMN PM_SERVICE_AREA.FIPS IS 'Capture state+county FIPS code'
;
COMMENT ON COLUMN PM_SERVICE_AREA.STATE IS 'Capture state abbreviation'
;
COMMENT ON COLUMN PM_SERVICE_AREA.SERVICE_AREA_EXT_ID IS 'Foreign Key refrence to PM_ISSUER_SERVICE_AREA_EXT table'
;
-- 
-- TABLE: PM_ZIP_COUNTY_RATING_AREA 
--

CREATE TABLE PM_ZIP_COUNTY_RATING_AREA(
    ID                NUMBER          NOT NULL,
    RATING_AREA_ID    NUMBER,
    ZIP               VARCHAR2(5),
    COUNTY            VARCHAR2(25),
    CONSTRAINT PK_PM_ZIP_COUNTY_RATING_AREA PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PM_ZIP_COUNTY_RATING_AREA IS 'Plan Management Zip County Rating Area information will be stored in this table'
;
COMMENT ON COLUMN PM_ZIP_COUNTY_RATING_AREA.ID IS 'Primary key of this table'
;
COMMENT ON COLUMN PM_ZIP_COUNTY_RATING_AREA.RATING_AREA_ID IS 'Foreign key. Capture Primary key of PM_RATING_AREA table'
;
COMMENT ON COLUMN PM_ZIP_COUNTY_RATING_AREA.ZIP IS 'Capture zip code'
;
COMMENT ON COLUMN PM_ZIP_COUNTY_RATING_AREA.COUNTY IS 'Capture county name'
;
-- 
-- TABLE: PRESCREEN_DATA 
--

CREATE TABLE PRESCREEN_DATA(
    ID                        NUMBER            NOT NULL,
    SESSION_ID                VARCHAR2(250 CHAR),
    FIRST_NAME                VARCHAR2(100 CHAR),
    IS_MARRIED                VARCHAR2(1 CHAR),
    NO_OF_DEPENDENTS          NUMBER,
    HOUSEHOLD_INCOME          NUMBER,
    IS_DISABLED               VARCHAR2(1 CHAR),
    ALIMONY_DEDUCTION         NUMBER,
    STUDENT_LOAN_DEDUCTION    NUMBER,
    RESULTS                   VARCHAR2(4000 CHAR),
    FINAL_CREDIT_DISPLAYED    NUMBER,
    FINAL_PLAN_SHOWED         VARCHAR2(100 CHAR),
    SCREEN_VISITED            NUMBER,
    SESSION_START_TS          TIMESTAMP(6),
    SESSION_END_TS            TIMESTAMP(6),
    TOTAL_TIME_SPENT          NUMBER,
    CREATED_BY                NUMBER,
    CREATION_TIMESTAMP        TIMESTAMP(6),
    LAST_UPDATED_BY           NUMBER,
    LAST_UPDATE_TIMESTAMP     TIMESTAMP(6),
    CLAIMANT_DOB              DATE,
    INPUT_PAGE                VARCHAR2(1 CHAR),
    SESSION_DATA_CLOB         CLOB,
    STATE_CD                  VARCHAR2(2 CHAR),
    CSR                       VARCHAR2(50 CHAR),
    FPL                       NUMBER,
    RESULTS_TYPE              VARCHAR2(50 CHAR),
    PREMIUM_VALUE             NUMBER,
    BENCHMARK_STATUS          VARCHAR2(1 CHAR),
    BENCHMARK_STATUS_DESC     VARCHAR2(256 CHAR),
    COUNTY                    VARCHAR2(50 CHAR),
    ZIPCODE                   VARCHAR2(5 CHAR),
    SOURCE                    VARCHAR2(20 CHAR),
    EMAIL_ADDRESS             CLOB,
    CONSTRAINT PRESCREEN_DATA_PK PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PRESCREEN_DATA IS 'This table is used to hold data from users for Eligibility Pre-screener'
;
COMMENT ON COLUMN PRESCREEN_DATA.ID IS 'Primary Key'
;
COMMENT ON COLUMN PRESCREEN_DATA.SESSION_ID IS 'Session Id of the current HTTP session'
;
COMMENT ON COLUMN PRESCREEN_DATA.FIRST_NAME IS 'First Name of the Household Contact'
;
COMMENT ON COLUMN PRESCREEN_DATA.IS_MARRIED IS 'Is the person married ? Possible values are Y-Yes, N-No'
;
COMMENT ON COLUMN PRESCREEN_DATA.NO_OF_DEPENDENTS IS 'How many no. of dependents will be claimed for tax filing. This should not include the the applicants spouse'
;
COMMENT ON COLUMN PRESCREEN_DATA.HOUSEHOLD_INCOME IS 'Total household income'
;
COMMENT ON COLUMN PRESCREEN_DATA.IS_DISABLED IS 'Is anyone in the household disabled ? Possible values are Y-Yes, N-No'
;
COMMENT ON COLUMN PRESCREEN_DATA.ALIMONY_DEDUCTION IS 'Total alimony applicable for the household'
;
COMMENT ON COLUMN PRESCREEN_DATA.STUDENT_LOAN_DEDUCTION IS 'Student loan amount applicable for dedcutions'
;
COMMENT ON COLUMN PRESCREEN_DATA.RESULTS IS 'Final Results displayed'
;
COMMENT ON COLUMN PRESCREEN_DATA.FINAL_CREDIT_DISPLAYED IS 'Final Credit Displayed'
;
COMMENT ON COLUMN PRESCREEN_DATA.FINAL_PLAN_SHOWED IS 'Final Plan displayed ID'
;
COMMENT ON COLUMN PRESCREEN_DATA.SCREEN_VISITED IS 'Possible values are 1-Profile, 2-Household, 3-Income, 4-Deductions, 5-Results'
;
COMMENT ON COLUMN PRESCREEN_DATA.SESSION_START_TS IS 'Time when the user started the current session'
;
COMMENT ON COLUMN PRESCREEN_DATA.SESSION_END_TS IS 'Time when the user finished the current session. This is updated everytime user moves from one section to another'
;
COMMENT ON COLUMN PRESCREEN_DATA.TOTAL_TIME_SPENT IS 'Total time spent in the session'
;
COMMENT ON COLUMN PRESCREEN_DATA.CREATED_BY IS 'User Id who created this record'
;
COMMENT ON COLUMN PRESCREEN_DATA.CREATION_TIMESTAMP IS 'Created On Time Stamp'
;
COMMENT ON COLUMN PRESCREEN_DATA.LAST_UPDATED_BY IS 'User Id who updated this record'
;
COMMENT ON COLUMN PRESCREEN_DATA.LAST_UPDATE_TIMESTAMP IS 'Updated on time stamp'
;
COMMENT ON COLUMN PRESCREEN_DATA.CLAIMANT_DOB IS 'Primary Applicant/Claimant''s DOB'
;
COMMENT ON COLUMN PRESCREEN_DATA.INPUT_PAGE IS 'Possible values are P�Product, M-Market'
;
COMMENT ON COLUMN PRESCREEN_DATA.SESSION_DATA_CLOB IS 'CLOB data containing details of session data'
;
COMMENT ON COLUMN PRESCREEN_DATA.STATE_CD IS 'Two character state code/abbreviation'
;
COMMENT ON COLUMN PRESCREEN_DATA.CSR IS 'CSR value if applicable'
;
COMMENT ON COLUMN PRESCREEN_DATA.FPL IS 'FPL value for the estimation'
;
COMMENT ON COLUMN PRESCREEN_DATA.RESULTS_TYPE IS 'Possible values are Medicaid, Medicare, CS1, CS4, CS5, CS6'
;
COMMENT ON COLUMN PRESCREEN_DATA.PREMIUM_VALUE IS 'Benchmark Premium value for plan (monthly)'
;
COMMENT ON COLUMN PRESCREEN_DATA.BENCHMARK_STATUS IS 'Whether call to Benchmark Premium API was successful. Possible values are Y, N'
;
COMMENT ON COLUMN PRESCREEN_DATA.BENCHMARK_STATUS_DESC IS 'In case of success - ''Success''. In case of failure, ''ErrorCode:ErrorDesc'' from the Benchmark API response'
;
COMMENT ON COLUMN PRESCREEN_DATA.COUNTY IS 'County based on zip code'
;
COMMENT ON COLUMN PRESCREEN_DATA.ZIPCODE IS 'Zip code'
;
COMMENT ON COLUMN PRESCREEN_DATA.SOURCE IS 'Source of Request given by URL Paramater, E.g. phix, jc etc.'
;
COMMENT ON COLUMN PRESCREEN_DATA.EMAIL_ADDRESS IS 'Store e-mail Id of primary applicant for a particular result type'
;
-- 
-- TABLE: PROCESSINSTANCEINFO 
--

CREATE TABLE PROCESSINSTANCEINFO(
    INSTANCEID                  NUMBER(19, 0)     NOT NULL,
    OPTLOCK                     NUMBER(10, 0),
    PROCESSID                   VARCHAR2(1020),
    STARTDATE                   TIMESTAMP(6),
    LASTREADDATE                TIMESTAMP(6),
    LASTMODIFICATIONDATE        TIMESTAMP(6),
    STATE                       NUMBER(10, 0)     NOT NULL,
    PROCESSINSTANCEBYTEARRAY    BLOB,
    CONSTRAINT PK_PROCESSINSTANCEINFO PRIMARY KEY (INSTANCEID)
)
;



-- 
-- TABLE: PROCESSINSTANCELOG 
--

CREATE TABLE PROCESSINSTANCELOG(
    ID                   NUMBER(19, 0)     NOT NULL,
    PROCESSINSTANCEID    NUMBER(19, 0),
    PROCESSID            VARCHAR2(1020),
    START_DATE           TIMESTAMP(6),
    END_DATE             TIMESTAMP(6),
    CONSTRAINT PK_PROCESSINSTANCELOG PRIMARY KEY (ID)
)
;



-- 
-- TABLE: PROVIDER 
--

CREATE TABLE PROVIDER(
    ID                       NUMBER            NOT NULL,
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    DATASOURCE               VARCHAR2(60),
    DATASOURCE_REF_ID        VARCHAR2(120),
    LICENSE_NUMBER           VARCHAR2(100),
    NAME                     VARCHAR2(400),
    NPI_NUMBER               VARCHAR2(100),
    STATUS                   VARCHAR2(40),
    TYPE                     VARCHAR2(40),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)      NOT NULL,
    UPI_NUMBER               VARCHAR2(100),
    MEDICARE_PROVIDER_ID     VARCHAR2(120),
    TAX_ID                   VARCHAR2(120),
    OFFICE_HOURS             VARCHAR2(1020),
    LAST_UPDATE_DATE         TIMESTAMP(6),
    UPIN                     VARCHAR2(60 CHAR),
    FFS_PROVIDER_COUNTRY     VARCHAR2(30 CHAR),
    SSN                      VARCHAR2(100),
    MEDICAL_PROVIDER_ID      VARCHAR2(9 CHAR),
    LICENSING_STATE          VARCHAR2(2 CHAR),
    PCP_ID                   VARCHAR2(15 CHAR),
    LOCATION_ID              NUMBER,
    TAXONOMY_CODE            VARCHAR2(10 CHAR),
    LANGUAGES_LOCATION       VARCHAR2(1 CHAR),
    ACCESSIBILITY_CODE       VARCHAR2(1 CHAR),
    FFS_PROVIDER             VARCHAR2(1 CHAR),
    SANCTION_STATUS          VARCHAR2(1 CHAR),
    GROUP_KEY                VARCHAR2(100),
    CONSTRAINT PK_PROVIDER PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PROVIDER IS 'Schema that describes a provider in the Exchange. A provider can be either an physician or a facility. This schema is the base table. Specific data elements for Physicians and Facilities are captures in PHYSICIAN and FACILITY tables.'
;
COMMENT ON COLUMN PROVIDER.ID IS 'Primary Key'
;
COMMENT ON COLUMN PROVIDER.CREATION_TIMESTAMP IS 'Creation date/time of this database record.'
;
COMMENT ON COLUMN PROVIDER.DATASOURCE IS 'Datasource for this provider data.'
;
COMMENT ON COLUMN PROVIDER.DATASOURCE_REF_ID IS 'Data source reference for this record.'
;
COMMENT ON COLUMN PROVIDER.LICENSE_NUMBER IS 'Provider license number with the state'
;
COMMENT ON COLUMN PROVIDER.NAME IS 'Name of the Provider'
;
COMMENT ON COLUMN PROVIDER.NPI_NUMBER IS 'NPI (National Provider Id) number for the provider'
;
COMMENT ON COLUMN PROVIDER.STATUS IS 'Status of Provider in the Exchange.'
;
COMMENT ON COLUMN PROVIDER.TYPE IS 'Type of Provider - physician/facility'
;
COMMENT ON COLUMN PROVIDER.LAST_UPDATE_TIMESTAMP IS 'Last Updated date/time of this database record.'
;
COMMENT ON COLUMN PROVIDER.UPI_NUMBER IS 'Universal Provider Id (UPI) for this provider'
;
COMMENT ON COLUMN PROVIDER.MEDICARE_PROVIDER_ID IS 'Medicare provider id'
;
COMMENT ON COLUMN PROVIDER.TAX_ID IS 'Tax ID for this provider.'
;
COMMENT ON COLUMN PROVIDER.OFFICE_HOURS IS 'Office Hours kept by this provider.'
;
COMMENT ON COLUMN PROVIDER.LAST_UPDATE_DATE IS 'Updation date of this record from the original data source.'
;
COMMENT ON COLUMN PROVIDER.UPIN IS 'Unique Physician Identification Number (UPIN) of Provider'
;
COMMENT ON COLUMN PROVIDER.FFS_PROVIDER_COUNTRY IS 'If FFS Provider fields is Y then populate FFS Provider County'
;
COMMENT ON COLUMN PROVIDER.SSN IS 'Numeric. without separators - Should be null for facilities and only populated for individuals/practitioners'
;
COMMENT ON COLUMN PROVIDER.MEDICAL_PROVIDER_ID IS 'Required for Medi-Cal Plans only'
;
COMMENT ON COLUMN PROVIDER.LICENSING_STATE IS 'Two character state code preferred'
;
COMMENT ON COLUMN PROVIDER.PCP_ID IS 'Required for Medi-Cal Plans only'
;
COMMENT ON COLUMN PROVIDER.LOCATION_ID IS 'Required for Medi-Cal Plans only. This field should uniquely identify each location for a provider. PCP ID can be used if it uniquely identifies each location for each provider'
;
COMMENT ON COLUMN PROVIDER.TAXONOMY_CODE IS 'Standard Taxonomy Codes required | separated list of specialties'
;
COMMENT ON COLUMN PROVIDER.LANGUAGES_LOCATION IS 'Language spoken at location (L), by Provider (P), or both (B)'
;
COMMENT ON COLUMN PROVIDER.ACCESSIBILITY_CODE IS 'Medi-Cal Plans only. Parking(P), Building (B), Reception/Waiting (W), Exam Room(E), Exam Table/Scale (T), Restroom (R)'
;
COMMENT ON COLUMN PROVIDER.FFS_PROVIDER IS 'Y, N, or null'
;
COMMENT ON COLUMN PROVIDER.SANCTION_STATUS IS 'Y, N, or null'
;
COMMENT ON COLUMN PROVIDER.GROUP_KEY IS 'Column to store group_key'
;
-- 
-- TABLE: PROVIDER_NETWORK 
--

CREATE TABLE PROVIDER_NETWORK(
    ID              NUMBER          NOT NULL,
    NETWORK_ID      NUMBER,
    PROVIDER_ID     NUMBER,
    NETWORK_TIER    VARCHAR2(10),
    CONSTRAINT PK_PROVIDER_NETWORK PRIMARY KEY (ID)
)
PARTITION BY HASH(id) PARTITIONS 4
;



COMMENT ON TABLE PROVIDER_NETWORK IS 'Schema that captures relationship between PROVIDER and NETWORK tables..'
;
COMMENT ON COLUMN PROVIDER_NETWORK.ID IS 'Primary Key'
;
COMMENT ON COLUMN PROVIDER_NETWORK.NETWORK_ID IS 'Foreign Key into NETWORK table'
;
COMMENT ON COLUMN PROVIDER_NETWORK.PROVIDER_ID IS 'Foreign Key into ISSUER table'
;
COMMENT ON COLUMN PROVIDER_NETWORK.NETWORK_TIER IS 'Column to store network_tier'
;
-- 
-- TABLE: PROVIDER_RATING 
--

CREATE TABLE PROVIDER_RATING(
    ID                       NUMBER(10, 0)    NOT NULL,
    CREATION_TIMESTAMP       TIMESTAMP(6)     NOT NULL,
    OVERALL_RATING           VARCHAR2(20),
    SOURCE                   VARCHAR2(200),
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6)     NOT NULL,
    PROVIDER_ID              NUMBER,
    CONSTRAINT PK_PROVIDER_RATING PRIMARY KEY (ID)
)
;



COMMENT ON TABLE PROVIDER_RATING IS 'Schema that captures ratings about providers (physicians, hospitals, facilties etc'
;
COMMENT ON COLUMN PROVIDER_RATING.ID IS 'Primary Key'
;
COMMENT ON COLUMN PROVIDER_RATING.CREATION_TIMESTAMP IS 'Creation date/time of this database record'
;
COMMENT ON COLUMN PROVIDER_RATING.OVERALL_RATING IS 'Overall rating for the provider'
;
COMMENT ON COLUMN PROVIDER_RATING.SOURCE IS 'Source of rating data'
;
COMMENT ON COLUMN PROVIDER_RATING.LAST_UPDATE_TIMESTAMP IS 'Last update date for this database record.'
;
COMMENT ON COLUMN PROVIDER_RATING.PROVIDER_ID IS 'Foreign Key into PROVIDER table.'
;
-- 
-- TABLE: QUEUE_USER 
--

CREATE TABLE QUEUE_USER(
    ID          NUMBER    NOT NULL,
    GROUP_ID    NUMBER,
    USER_ID     NUMBER,
    CONSTRAINT PK_QUEUEUSER_ID PRIMARY KEY (ID)
)
;



COMMENT ON TABLE QUEUE_USER IS 'The entity holds the information of the queues and their respective users.'
;
COMMENT ON COLUMN QUEUE_USER.ID IS 'Primary Key (surrogate key)'
;
COMMENT ON COLUMN QUEUE_USER.GROUP_ID IS 'Queue Id.  It mpas to one of the queues.'
;
COMMENT ON COLUMN QUEUE_USER.USER_ID IS 'User Id. Associated with the queue'
;
-- 
-- TABLE: RATE_REVIEW_DATA 
--

CREATE TABLE RATE_REVIEW_DATA(
    ID                             NUMBER           NOT NULL,
    CREATION_TIMESTAMP             TIMESTAMP(6)     NOT NULL,
    MEDICAL_LOSS_RATIO             VARCHAR2(40),
    NUMBER_OF_AFFECTED_INSURED     NUMBER,
    NUMBER_OF_APPLICATIONS         NUMBER,
    NUMBER_OF_DENIALS              NUMBER(10, 0),
    RATE_CHANGE_EFFECTIVE_DATE     TIMESTAMP(6),
    RATE_CHANGE_REQUESTED          VARCHAR2(40),
    RATE_CHANGE_SUBMISSION_DATE    TIMESTAMP(6),
    RATE_INCREASE_PERCENTAGE       VARCHAR2(40),
    RATE_REVIEW_STATUS             VARCHAR2(40),
    SERFF_ID                       VARCHAR2(80),
    LAST_UPDATE_TIMESTAMP          TIMESTAMP(6)     NOT NULL,
    ISSUER_ID                      NUMBER,
    PLAN_ID                        NUMBER(10, 0),
    CONSTRAINT PK_RATE_REVIEW_DATA PRIMARY KEY (ID)
)
;



COMMENT ON TABLE RATE_REVIEW_DATA IS 'Schema describing rate review data for plan.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.ID IS 'Primary Key'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.CREATION_TIMESTAMP IS 'Primary Key'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.MEDICAL_LOSS_RATIO IS 'Medical Loss Ratio for thihs plan'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.NUMBER_OF_AFFECTED_INSURED IS 'Number of insured people affected by this rate change.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.NUMBER_OF_APPLICATIONS IS 'Number of applications for this plan'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.NUMBER_OF_DENIALS IS 'Number of denials for this specific plan.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.RATE_CHANGE_EFFECTIVE_DATE IS 'Effective date for the rate change.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.RATE_CHANGE_REQUESTED IS 'Date when the rate change is requested.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.RATE_CHANGE_SUBMISSION_DATE IS 'Rate Change submission date.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.RATE_INCREASE_PERCENTAGE IS 'Percentage by which premiums were raised'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.RATE_REVIEW_STATUS IS 'Rate Review Status - new, pending, approved... etc'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.SERFF_ID IS 'SERFF id for this rate review.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.LAST_UPDATE_TIMESTAMP IS 'Last updated date/time for this database record.'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.ISSUER_ID IS 'Foreign Key into ISSUERS table'
;
COMMENT ON COLUMN RATE_REVIEW_DATA.PLAN_ID IS 'Foreign Key into PLAN table'
;
-- 
-- TABLE: REASSIGNMENT 
--

CREATE TABLE REASSIGNMENT(
    ID                             NUMBER(19, 0)    NOT NULL,
    ESCALATION_REASSIGNMENTS_ID    NUMBER(19, 0),
    CONSTRAINT PK_REASSIGNMENT PRIMARY KEY (ID)
)
;



-- 
-- TABLE: REGION 
--

CREATE TABLE REGION(
    ID             NUMBER(10, 0)    NOT NULL,
    COUNTY         VARCHAR2(255),
    ZIP            VARCHAR2(5),
    REGION_NAME    VARCHAR2(100),
    COUNTY_FIPS    VARCHAR2(5 CHAR),
    CONSTRAINT PK_REGION PRIMARY KEY (ID)
)
;



COMMENT ON TABLE REGION IS 'This table captures the relationship between a zip code and the geographic region it belongs to. Quoting for Individual/SHOP Plans for Health/Dental is primarily done based on regions'
;
COMMENT ON COLUMN REGION.ID IS 'Primary Key'
;
COMMENT ON COLUMN REGION.COUNTY IS 'County for the zip code'
;
COMMENT ON COLUMN REGION.ZIP IS 'Zip Code'
;
COMMENT ON COLUMN REGION.REGION_NAME IS 'Region name that used to describe a collection of zip codes.'
;
COMMENT ON COLUMN REGION.COUNTY_FIPS IS 'FIPS of state + county'
;
-- 
-- TABLE: SERFF_DOCUMENT 
--

CREATE TABLE SERFF_DOCUMENT(
    SERFF_DOC_ID          NUMBER            NOT NULL,
    SERFF_REQ_ID          NUMBER            NOT NULL,
    ECM_DOC_ID            VARCHAR2(1000 CHAR),
    DOCUMENT_NAME         VARCHAR2(1000 CHAR),
    DOCUMENT_TYPE         VARCHAR2(10 CHAR),
    DOCUMENT_SIZE         NUMBER            NOT NULL,
    CREATION_TIMESTAMP    TIMESTAMP(6),
    CONSTRAINT SERFF_DOCUMENT_PK PRIMARY KEY (SERFF_DOC_ID)
)
;



COMMENT ON TABLE SERFF_DOCUMENT IS 'This table is used to store information of the attachments received in the web service calls made to SERFF service'
;
COMMENT ON COLUMN SERFF_DOCUMENT.SERFF_DOC_ID IS 'SERFF Document ID'
;
COMMENT ON COLUMN SERFF_DOCUMENT.SERFF_REQ_ID IS 'SERFF Request ID. This is FK to SERFF_PLAN_MGMT table'
;
COMMENT ON COLUMN SERFF_DOCUMENT.ECM_DOC_ID IS 'Document id from ECM'
;
COMMENT ON COLUMN SERFF_DOCUMENT.DOCUMENT_NAME IS 'Document Name'
;
COMMENT ON COLUMN SERFF_DOCUMENT.DOCUMENT_TYPE IS 'File extension of the document (E.g pdf, jpg etc)'
;
COMMENT ON COLUMN SERFF_DOCUMENT.DOCUMENT_SIZE IS 'Size of the document in Bytes'
;
COMMENT ON COLUMN SERFF_DOCUMENT.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
-- 
-- TABLE: SERFF_PLAN_MGMT 
--

CREATE TABLE SERFF_PLAN_MGMT(
    SERFF_REQ_ID             NUMBER            NOT NULL,
    CALLER_IP                VARCHAR2(50 CHAR),
    CALLER_TYPE              VARCHAR2(1 CHAR)  NOT NULL,
    OPERATION                VARCHAR2(100 CHAR) NOT NULL,
    PROCESS_IP               VARCHAR2(40 CHAR),
    START_TIME               TIMESTAMP(6)      NOT NULL,
    END_TIME                 TIMESTAMP(6),
    REQUEST_STATUS           VARCHAR2(1 CHAR)  NOT NULL,
    REMARKS                  VARCHAR2(4000 CHAR),
    ATTACHMENTS_LIST         VARCHAR2(1000 CHAR),
    REQUEST_STATE            VARCHAR2(1 CHAR),
    REQUEST_STATE_DESC       VARCHAR2(1000 CHAR),
    ENV                      VARCHAR2(5 CHAR),
    CREATION_TIMESTAMP       TIMESTAMP(6)      NOT NULL,
    CREATED_BY               VARCHAR2(20 CHAR) NOT NULL,
    LAST_UPDATE_TIMESTAMP    TIMESTAMP(6),
    ISSUER_ID                VARCHAR2(50 CHAR),
    ISSUER_NAME              VARCHAR2(100 CHAR),
    HIOS_PRODUCT_ID          VARCHAR2(50 CHAR),
    PLAN_ID                  VARCHAR2(50 CHAR),
    PLAN_NAME                VARCHAR2(100 CHAR),
    SERFF_TRACK_NUM          VARCHAR2(100 CHAR),
    STATE_TRACK_NUM          VARCHAR2(100 CHAR),
    REQUEST_XML              CLOB,
    RESPONSE_XML             CLOB,
    PM_RESPONSE_XML          CLOB,
    CONSTRAINT SERFF_PLAN_MGMT_PK PRIMARY KEY (SERFF_REQ_ID)
)
;



COMMENT ON TABLE SERFF_PLAN_MGMT IS 'This table holds data for the web service calls made to SERFF service'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.SERFF_REQ_ID IS 'Primary Key'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.CALLER_IP IS 'Caller IP Address. This helps to track Apache, SOAP UI or UI'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.CALLER_TYPE IS 'Possible values are S-SERFF, T-SOAP UI / JMeter, U-UI, O-Others'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.OPERATION IS 'Name of SERFF Operation E.g Transfer Plan'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.PROCESS_IP IS 'Processes IP Address where this job is processed. Useful in clustered environment.'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.START_TIME IS 'Start Time of the request'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.END_TIME IS 'End Time of the request'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.REQUEST_STATUS IS 'Possible values are S-Success, F-Failure'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.REMARKS IS 'In case of failure, this column will have error details or exception message'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.ATTACHMENTS_LIST IS 'Comma separated text of attachments in given XML. Issuer (Administrative) Data - ID, Plan/Benefit Data  PBD, Network Data  ND, Service Area Data - SAD, Rate Data - RD, Essential Community Providers Data - ECPD, Prescription Drug Data - PDD, Rate Review Data - RDD'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.REQUEST_STATE IS 'Possible values are B-Begin, E-End, P-Progress, W-Wait'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.REQUEST_STATE_DESC IS 'State of the Request Description. This gives process state for given request.'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.ENV IS 'Environment from where the request is made E. g. PROD, DEV, QA, PILOT etc'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.CREATION_TIMESTAMP IS 'Creation timestamp of the record'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.CREATED_BY IS 'Application or user name who initiated process through UI'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.LAST_UPDATE_TIMESTAMP IS 'Timestamp when record was last updated'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.ISSUER_ID IS 'Issuer ID from the request.'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.ISSUER_NAME IS 'Issuer Name from the request tag companyName"'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.HIOS_PRODUCT_ID IS 'HIOS Product Id from the request tag hiosProductId'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.PLAN_ID IS 'Value of the request tag standardComponentId'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.PLAN_NAME IS 'Issuer Name from the request tag planName'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.SERFF_TRACK_NUM IS 'Value of the request tag serffTrackingNumber'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.STATE_TRACK_NUM IS 'Value of the request tag stateTrackingNumber'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.REQUEST_XML IS 'Request XML for the webservice call'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.RESPONSE_XML IS 'Response XML for the webservice call'
;
COMMENT ON COLUMN SERFF_PLAN_MGMT.PM_RESPONSE_XML IS 'Response XML for the webservice call to Plan Management'
;
-- 
-- TABLE: SERFF_TEMPLATES 
--

CREATE TABLE SERFF_TEMPLATES(
    ID               NUMBER(10, 0)    NOT NULL,
    PLAN_ID          VARCHAR2(120 CHAR) NOT NULL,
    ADMIN_LMD        TIMESTAMP(6),
    NETWORK_LMD      TIMESTAMP(6),
    SVCAREA_LMD      TIMESTAMP(6),
    PLAN_LMD         TIMESTAMP(6),
    RATES_LMD        TIMESTAMP(6),
    PRES_DRUG_LMD    TIMESTAMP(6),
    CONSTRAINT PK_SERFF_TEMPLATES PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN SERFF_TEMPLATES.ID IS 'PK for SERFF_TEMPLATES'
;
COMMENT ON COLUMN SERFF_TEMPLATES.PLAN_ID IS 'Column to hold Plan Number'
;
COMMENT ON COLUMN SERFF_TEMPLATES.ADMIN_LMD IS 'Column to hold Last modified date of Admin template'
;
COMMENT ON COLUMN SERFF_TEMPLATES.NETWORK_LMD IS 'Column to hold Last modified date of Network template'
;
COMMENT ON COLUMN SERFF_TEMPLATES.SVCAREA_LMD IS 'Column to hold Last modified date of Service Area template'
;
COMMENT ON COLUMN SERFF_TEMPLATES.PLAN_LMD IS 'Column to hold Last modified date of Plan template'
;
COMMENT ON COLUMN SERFF_TEMPLATES.RATES_LMD IS 'Column to hold Last modified date of Rates template'
;
COMMENT ON COLUMN SERFF_TEMPLATES.PRES_DRUG_LMD IS 'Column to hold Last modified date of Prescription Drug template'
;
-- 
-- TABLE: SESSIONINFO 
--

CREATE TABLE SESSIONINFO(
    ID                      NUMBER(10, 0)    NOT NULL,
    LASTMODIFICATIONDATE    TIMESTAMP(6),
    RULESBYTEARRAY          BLOB,
    STARTDATE               TIMESTAMP(6),
    OPTLOCK                 NUMBER(10, 0),
    CONSTRAINT PK_SESSIONINFO PRIMARY KEY (ID)
)
;



-- 
-- TABLE: SPECIALTY 
--

CREATE TABLE SPECIALTY(
    ID                     NUMBER            NOT NULL,
    DESCRIPTION            VARCHAR2(1020),
    GROUP_NAME             VARCHAR2(256),
    NAME                   VARCHAR2(400),
    PARENT_SPECIALTY_ID    NUMBER(10, 0),
    CODE                   VARCHAR2(1000),
    CONSTRAINT PK_SPECIALTY PRIMARY KEY (ID)
)
;



COMMENT ON TABLE SPECIALTY IS 'Describes the provider (physician and facility) specialties. Also captures the relationshp between the parent and child specialties in the table.'
;
COMMENT ON COLUMN SPECIALTY.ID IS 'Primary Key'
;
COMMENT ON COLUMN SPECIALTY.DESCRIPTION IS 'Description of the specialty'
;
COMMENT ON COLUMN SPECIALTY.GROUP_NAME IS 'Group name for the provider specialty.'
;
COMMENT ON COLUMN SPECIALTY.NAME IS 'Name of Provider specialty'
;
COMMENT ON COLUMN SPECIALTY.PARENT_SPECIALTY_ID IS 'Parent ID for this specialty'
;
COMMENT ON COLUMN SPECIALTY.CODE IS 'Map to CMS Specialty Code Standard. | separated list of specialties'
;
-- 
-- TABLE: TAX_YEAR 
--

CREATE TABLE TAX_YEAR(
    ID                        NUMBER          NOT NULL,
    YEAR_OF_EFFECTIVE_DATE    VARCHAR2(10 CHAR),
    TAX_DUE_DATE              VARCHAR2(50 CHAR),
    TAX_DUE_YEAR              VARCHAR2(10 CHAR),
    CONSTRAINT PK_TAX_YEAR PRIMARY KEY (ID)
)
;



COMMENT ON COLUMN TAX_YEAR.ID IS 'Primary key'
;
COMMENT ON COLUMN TAX_YEAR.YEAR_OF_EFFECTIVE_DATE IS 'The year for the effective date of coverage'
;
COMMENT ON COLUMN TAX_YEAR.TAX_DUE_DATE IS 'Due date of effective date of coverage'
;
COMMENT ON COLUMN TAX_YEAR.TAX_DUE_YEAR IS 'Due year of effective date of coverage'
;
-- 
-- TABLE: URAC 
--

CREATE TABLE URAC(
    ID                      NUMBER(20, 0)     NOT NULL,
    HIOS_ISSUER_ID          NUMBER(5, 0)      NOT NULL,
    APP_NUM                 VARCHAR2(1020),
    ACCREDITATION_STATUS    VARCHAR2(20),
    EXPIRATION_DATE         TIMESTAMP(6),
    CONSTRAINT PK_URAC PRIMARY KEY (ID)
)
;



-- 
-- INDEX: FK_CITIZENSHIP_STATUS_LKP_IND 
--

CREATE INDEX FK_CITIZENSHIP_STATUS_LKP_IND ON ENROLLEE(CITIZENSHIP_STATUS_LKP)
;
-- 
-- INDEX: FK_CUSTODIAL_ENROLLEE_ID_IND 
--

CREATE INDEX FK_CUSTODIAL_ENROLLEE_ID_IND ON ENROLLEE(CUSTODIAL_PARENT_ID)
;
-- 
-- INDEX: FK_ENROLLEE_CREATED_BY_IND 
--

CREATE INDEX FK_ENROLLEE_CREATED_BY_IND ON ENROLLEE(CREATED_BY)
;
-- 
-- INDEX: FK_ENROLLEE_HOME_LOCATION_IND 
--

CREATE INDEX FK_ENROLLEE_HOME_LOCATION_IND ON ENROLLEE(HOME_ADDRESS_ID)
;
-- 
-- INDEX: FK_ENROLLEE_MAILING_LOCATI_IND 
--

CREATE INDEX FK_ENROLLEE_MAILING_LOCATI_IND ON ENROLLEE(MAILING_ADDRESS_ID)
;
-- 
-- INDEX: FK_ENROLLEE_STATUS_LKP_IND 
--

CREATE INDEX FK_ENROLLEE_STATUS_LKP_IND ON ENROLLEE(ENROLLEE_STATUS_LKP)
;
-- 
-- INDEX: FK_ENROLLEE_UPDATED_BY_IND 
--

CREATE INDEX FK_ENROLLEE_UPDATED_BY_IND ON ENROLLEE(LAST_UPDATED_BY)
;
-- 
-- INDEX: FK_LANGUAGE_SPOKEN_LKP_IND 
--

CREATE INDEX FK_LANGUAGE_SPOKEN_LKP_IND ON ENROLLEE(LANGUAGE_SPOKEN_LKP)
;
-- 
-- INDEX: FK_LANGUAGE_WRITTEN_LKP_IND 
--

CREATE INDEX FK_LANGUAGE_WRITTEN_LKP_IND ON ENROLLEE(LANGUAGE_WRITTEN_LKP)
;
-- 
-- INDEX: FK_MARITAL_STATUS_LKP_ID_IND 
--

CREATE INDEX FK_MARITAL_STATUS_LKP_ID_IND ON ENROLLEE(MARITAL_STATUS_LKP)
;
-- 
-- INDEX: FK_PERSON_TYPE_LKP_IND 
--

CREATE INDEX FK_PERSON_TYPE_LKP_IND ON ENROLLEE(PERSON_TYPE_LKP)
;
-- 
-- INDEX: FK_RACE_ETHNICITY_LKP_ID_IND 
--

CREATE INDEX FK_RACE_ETHNICITY_LKP_ID_IND ON ENROLLEE(RACE_ETHNICITY_LKP)
;
-- 
-- INDEX: FK_RELATIONTOHCP_LKP_VALUE_IND 
--

CREATE INDEX FK_RELATIONTOHCP_LKP_VALUE_IND ON ENROLLEE(RELATIONSHIP_HCP_LKP)
;
-- 
-- INDEX: FK_TOBACCO_USAGE_LKP_IND 
--

CREATE INDEX FK_TOBACCO_USAGE_LKP_IND ON ENROLLEE(TOBACCO_USAGE_LKP)
;
-- 
-- INDEX: CITIZENSHIP_STATUS_LKP_IND 
--

CREATE INDEX CITIZENSHIP_STATUS_LKP_IND ON ENROLLEE_AUD(CITIZENSHIP_STATUS_LKP)
;
-- 
-- INDEX: FK_GENDER_LOOKUP_VALUE_ID_IND 
--

CREATE INDEX FK_GENDER_LOOKUP_VALUE_ID_IND ON ENROLLEE_AUD(GENDER_LKP)
;
-- 
-- INDEX: FK_MARITAL_STATUS_LKP_IND 
--

CREATE INDEX FK_MARITAL_STATUS_LKP_IND ON ENROLLEE_AUD(MARITAL_STATUS_LKP)
;
-- 
-- INDEX: FK_RACE_ETHINITY_LKP_IND 
--

CREATE INDEX FK_RACE_ETHINITY_LKP_IND ON ENROLLEE_AUD(RACE_ETHNICITY_LKP)
;
-- 
-- INDEX: FK_RACE_LOOKUP_VALUE_ID_IND 
--

CREATE INDEX FK_RACE_LOOKUP_VALUE_ID_IND ON ENROLLEE_RACE(RACE_ETHNICITY_LKP)
;
-- 
-- INDEX: FK_ENROLLEE_LKP_VALUE_ID_IND 
--

CREATE INDEX FK_ENROLLEE_LKP_VALUE_ID_IND ON ENROLLEE_RELATIONSHIP(RELATIONSHIP_LKP) 
;
-- 
-- INDEX: FK_ENROLLEE_TRRELATIONSHIP_IND 
--

CREATE INDEX FK_ENROLLEE_TRRELATIONSHIP_IND ON ENROLLEE_RELATIONSHIP(TARGET_ENROLEE_ID) 
;
-- 
-- INDEX: FK_RELATIONHCP_LKP_VALUE_I_IND 
--

CREATE INDEX FK_RELATIONHCP_LKP_VALUE_I_IND ON ENROLLEE_RELATIONSHIP(RELATIONSHIP_HCP_LKP) 
;
-- 
-- INDEX: FK_ENROLLEE_SEND_CREATED_B_IND 
--

CREATE INDEX FK_ENROLLEE_SEND_CREATED_B_IND ON ENROLLEE_UPDATE_SEND_STATUS(CREATED_BY)
;
-- 
-- INDEX: FK_ENROLLEE_SEND_UPDATED_B_IND 
--

CREATE INDEX FK_ENROLLEE_SEND_UPDATED_B_IND ON ENROLLEE_UPDATE_SEND_STATUS(LAST_UPDATED_BY)
;
-- 
-- INDEX: FK_ENROLLEE_STAUS_IND 
--

CREATE INDEX FK_ENROLLEE_STAUS_IND ON ENROLLEE_UPDATE_SEND_STATUS(ENROLLEE_STAUS)
;
-- 
-- INDEX: FK_ENROLLMENT_CREATED_BY_IND 
--

CREATE INDEX FK_ENROLLMENT_CREATED_BY_IND ON ENROLLMENT(CREATED_BY)
;
-- 
-- INDEX: FK_ENROLLMENT_EMPR_ID_IND 
--

CREATE INDEX FK_ENROLLMENT_EMPR_ID_IND ON ENROLLMENT(EMPLOYER_ID)
;
-- 
-- INDEX: FK_ENROLLMENT_FINANCIAL_ID_IND 
--

CREATE INDEX FK_ENROLLMENT_FINANCIAL_ID_IND ON ENROLLMENT(FINANCIAL_ID)
;
-- 
-- INDEX: FK_ENROLLMENT_ISSUER_ID_IND 
--

CREATE INDEX FK_ENROLLMENT_ISSUER_ID_IND ON ENROLLMENT(ISSUER_ID)
;
-- 
-- INDEX: FK_ENROLLMENT_STATUS_LKP_I_IND 
--

CREATE INDEX FK_ENROLLMENT_STATUS_LKP_I_IND ON ENROLLMENT(ENROLLMENT_STATUS_LKP)
;
-- 
-- INDEX: FK_ENROLLMENT_TYPE_LKP_ID_IND 
--

CREATE INDEX FK_ENROLLMENT_TYPE_LKP_ID_IND ON ENROLLMENT(ENROLLMENT_TYPE_LKP)
;
-- 
-- INDEX: FK_ENROLLMENT_UPDATED_BY_IND 
--

CREATE INDEX FK_ENROLLMENT_UPDATED_BY_IND ON ENROLLMENT(LAST_UPDATED_BY)
;
-- 
-- INDEX: FK_ESIGENROLL_ID_IND 
--

CREATE INDEX FK_ESIGENROLL_ID_IND ON ENROLLMENT(ESIGNATURE_ID)
;
-- 
-- INDEX: FK_INSURANCE_LOOKUP_VALUE__IND 
--

CREATE INDEX FK_INSURANCE_LOOKUP_VALUE__IND ON ENROLLMENT(INSURANCE_TYPE_LKP)
;
-- 
-- INDEX: FK_PLD_ORDER_ITEMS_ENRLMNT_IND 
--

CREATE INDEX FK_PLD_ORDER_ITEMS_ENRLMNT_IND ON ENROLLMENT(IND_ORDER_ITEMS_ID)
;
-- 
-- INDEX: FK_WORKSITE_ID_EMP_LOCATIO_IND 
--

CREATE INDEX FK_WORKSITE_ID_EMP_LOCATIO_IND ON ENROLLMENT(WORKSITE_ID)
;
-- 
-- INDEX: FK_ESIG_ENROLL_ID_IND 
--

CREATE INDEX FK_ESIG_ENROLL_ID_IND ON ENROLLMENT_ESIGNATURE(ESIGNATURE_ID) 
;
-- 
-- INDEX: FK_ENROLLMENT_EVENT_ENROLL_IND 
--

CREATE INDEX FK_ENROLLMENT_EVENT_ENROLL_IND ON ENROLLMENT_EVENT(ENROLLEE_ID)
;
-- 
-- INDEX: FK_EVENT_REASON_LKP_IND 
--

CREATE INDEX FK_EVENT_REASON_LKP_IND ON ENROLLMENT_EVENT(EVENT_REASON_LKP)
;
-- 
-- INDEX: FK_EVENT_TYPE_LKP_IND 
--

CREATE INDEX FK_EVENT_TYPE_LKP_IND ON ENROLLMENT_EVENT(EVENT_TYPE_LKP)
;
-- 
-- INDEX: FK_ESIGN_BY_ACCOUNTUSER_ID_IND 
--

CREATE INDEX FK_ESIGN_BY_ACCOUNTUSER_ID_IND ON ESIGNATURE(ESIGN_BY)
;
-- 
-- INDEX: UC_PLD_GROUP_01 
--

CREATE INDEX UC_PLD_GROUP_01 ON PLD_GROUP(PLD_HOUSEHOLD_ID, GROUP_NAME)
;
-- 
-- INDEX: UC_PLD_GROUP_PERSON_01 
--

CREATE INDEX UC_PLD_GROUP_PERSON_01 ON PLD_GROUP_PERSON(PLD_GROUP_ID, PLD_PERSON_ID) 
;
-- 
-- INDEX: UC_PLD_HOUSEHOLD_01 
--

CREATE INDEX UC_PLD_HOUSEHOLD_01 ON PLD_HOUSEHOLD(SHOPPING_ID)
;
-- 
-- INDEX: UC_PLD_HOUSEHOLD_PERSON 
--

CREATE INDEX UC_PLD_HOUSEHOLD_PERSON ON PLD_HOUSEHOLD_PERSON(PLD_HOUSEHOLD_ID, PLD_PERSON_ID) 
;
-- 
-- INDEX: UC_PLD_ORDER 
--

CREATE INDEX UC_PLD_ORDER ON PLD_ORDER(PLD_HOUSEHOLD_ID)
;
-- 
-- INDEX: UC_PLD_ORDER_ITEM 
--

CREATE INDEX UC_PLD_ORDER_ITEM ON PLD_ORDER_ITEM(PLD_GROUP_ID, PLAN_ID)
;
-- 
-- INDEX: REGION_NAME_IDX 
--

CREATE INDEX REGION_NAME_IDX ON REGION(REGION_NAME)
;
-- 
-- INDEX: REGION_ZIP_IDX 
--

CREATE INDEX REGION_ZIP_IDX ON REGION(ZIP)
;
-- 
-- INDEX: ECM_DOC_ID_UC 
--

CREATE INDEX ECM_DOC_ID_UC ON SERFF_DOCUMENT(ECM_DOC_ID)
;
-- 
-- INDEX: SERFF_REQ_ID_IDX 
--

CREATE INDEX SERFF_REQ_ID_IDX ON SERFF_DOCUMENT(SERFF_REQ_ID)
;
-- 
-- TABLE: BATCH_JOB_EXECUTION 
--

ALTER TABLE BATCH_JOB_EXECUTION ADD CONSTRAINT JOB_INST_EXEC_FK 
    FOREIGN KEY (JOB_INSTANCE_ID)
    REFERENCES BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
;


-- 
-- TABLE: BATCH_JOB_EXECUTION_CONTEXT 
--

ALTER TABLE BATCH_JOB_EXECUTION_CONTEXT ADD CONSTRAINT JOB_EXEC_CTX_FK 
    FOREIGN KEY (JOB_EXECUTION_ID)
    REFERENCES BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
;


-- 
-- TABLE: BATCH_JOB_PARAMS 
--

ALTER TABLE BATCH_JOB_PARAMS ADD CONSTRAINT JOB_INST_PARAMS_FK 
    FOREIGN KEY (JOB_INSTANCE_ID)
    REFERENCES BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
;


-- 
-- TABLE: BATCH_STEP_EXECUTION 
--

ALTER TABLE BATCH_STEP_EXECUTION ADD CONSTRAINT JOB_EXEC_STEP_FK 
    FOREIGN KEY (JOB_EXECUTION_ID)
    REFERENCES BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
;


-- 
-- TABLE: BATCH_STEP_EXECUTION_CONTEXT 
--

ALTER TABLE BATCH_STEP_EXECUTION_CONTEXT ADD CONSTRAINT STEP_EXEC_CTX_FK 
    FOREIGN KEY (STEP_EXECUTION_ID)
    REFERENCES BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
;


-- 
-- TABLE: COST_SHARE 
--

ALTER TABLE COST_SHARE ADD CONSTRAINT FK_COST_SHARE_FORMULARY_ID 
    FOREIGN KEY (FORMULARY_ID)
    REFERENCES FORMULARY(ID)
;


-- 
-- TABLE: DCS_TIER_TYPE 
--

ALTER TABLE DCS_TIER_TYPE ADD CONSTRAINT FK_DCS_TIER_TYPE_FORMULARY_ID 
    FOREIGN KEY (FORMULARY_ID)
    REFERENCES FORMULARY(ID)
;


-- 
-- TABLE: DCS_TYPE 
--

ALTER TABLE DCS_TYPE ADD CONSTRAINT FK_DCS_TYPE_DCS_TIER_TYPE_ID 
    FOREIGN KEY (DCS_TIER_TYPE_ID)
    REFERENCES DCS_TIER_TYPE(ID)
;


-- 
-- TABLE: DRUG_CODE_TIER_CODE_TYPE 
--

ALTER TABLE DRUG_CODE_TIER_CODE_TYPE ADD CONSTRAINT FK_DCTCT_DCS_TIER_TYPE_ID 
    FOREIGN KEY (DCS_TIER_TYPE_ID)
    REFERENCES DCS_TIER_TYPE(ID)
;


-- 
-- TABLE: EE_DESIGNATE_ASSISTERS 
--

ALTER TABLE EE_DESIGNATE_ASSISTERS ADD CONSTRAINT FK_DESIGNATE_ASSISTERS_ASSID 
    FOREIGN KEY (ASSISTER_ID)
    REFERENCES EE_ASSISTERS(ID)
;

ALTER TABLE EE_DESIGNATE_ASSISTERS ADD CONSTRAINT FK_DESIGNATE_ASSISTERS_ENTID 
    FOREIGN KEY (ENTITY_ID)
    REFERENCES EE_ENTITIES(ID)
;


-- 
-- TABLE: EMPLOYER_ELIGIBILITY_STATUS 
--

ALTER TABLE EMPLOYER_ELIGIBILITY_STATUS ADD CONSTRAINT FK_EES_CREATED_BY 
    FOREIGN KEY (CREATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE EMPLOYER_ELIGIBILITY_STATUS ADD CONSTRAINT FK_EES_EMPLOYER_ID 
    FOREIGN KEY (EMPLOYER_ID)
    REFERENCES EMPLOYERS(ID)
;


-- 
-- TABLE: EMPLOYER_ENROLLMENT_ITEMS 
--

ALTER TABLE EMPLOYER_ENROLLMENT_ITEMS ADD CONSTRAINT FK_EMPRENITEM_RS01_ID 
    FOREIGN KEY (BENCHMARK_PLAN_ID)
    REFERENCES PLAN(ID)
;

ALTER TABLE EMPLOYER_ENROLLMENT_ITEMS ADD CONSTRAINT FK_EMPRENRT_RS01_ID 
    FOREIGN KEY (EMPLOYER_ENROLLMENT_ID)
    REFERENCES EMPLOYER_ENROLLMENTS(ID)
;


-- 
-- TABLE: EMPLOYER_ENROLLMENTS 
--

ALTER TABLE EMPLOYER_ENROLLMENTS ADD CONSTRAINT FK_EMPRENR_RS01_ID 
    FOREIGN KEY (EMPLOYER_ID)
    REFERENCES EMPLOYERS(ID)
;


-- 
-- TABLE: EMPLOYER_INVOICES 
--

ALTER TABLE EMPLOYER_INVOICES ADD CONSTRAINT FK_EMPLOYER_INVOICE 
    FOREIGN KEY (EMPLOYER_ID)
    REFERENCES EMPLOYERS(ID)
;


-- 
-- TABLE: EMPLOYER_INVOICES_LINEITEMS 
--

ALTER TABLE EMPLOYER_INVOICES_LINEITEMS ADD CONSTRAINT FK_EMP_INV_ENROLLMENT_ID 
    FOREIGN KEY (ENROLLMENT_ID)
    REFERENCES ENROLLMENT(ID)
;

ALTER TABLE EMPLOYER_INVOICES_LINEITEMS ADD CONSTRAINT FK_EMPLOYEE_INVOICE 
    FOREIGN KEY (INVOICE_ID)
    REFERENCES EMPLOYER_INVOICES(ID)
;

ALTER TABLE EMPLOYER_INVOICES_LINEITEMS ADD CONSTRAINT FK_EMPLOYEE_LINEITEMS 
    FOREIGN KEY (EMPLOYEE_ID)
    REFERENCES EMPLOYEES(ID)
;

ALTER TABLE EMPLOYER_INVOICES_LINEITEMS ADD CONSTRAINT FK_EMPLOYER_LINEITEMS 
    FOREIGN KEY (EMPLOYER_ID)
    REFERENCES EMPLOYERS(ID)
;

ALTER TABLE EMPLOYER_INVOICES_LINEITEMS ADD CONSTRAINT FK_EMPLOYERS_INV_ISSUER_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: EMPLOYER_NOTIFICATION 
--

ALTER TABLE EMPLOYER_NOTIFICATION ADD CONSTRAINT FK_NOTICES_ID 
    FOREIGN KEY (NOTICES_ID)
    REFERENCES NOTICES(ID)
;

ALTER TABLE EMPLOYER_NOTIFICATION ADD CONSTRAINT FK_NOTIFY_EMPINVOICE_ID 
    FOREIGN KEY (EMPLOYER_INVOICE_ID)
    REFERENCES EMPLOYER_INVOICES(ID)
;


-- 
-- TABLE: EMPLOYER_PAYMENT_INVOICE 
--

ALTER TABLE EMPLOYER_PAYMENT_INVOICE ADD CONSTRAINT FK_EMPLOYER_PAYMENTINVOICE 
    FOREIGN KEY (INVOICE_ID)
    REFERENCES EMPLOYER_INVOICES(ID)
;


-- 
-- TABLE: EMPLOYER_PAYMENTS 
--

ALTER TABLE EMPLOYER_PAYMENTS ADD CONSTRAINT FK_EMPLOYER_PAYMENTS 
    FOREIGN KEY (PAYMENT_TYPE_ID)
    REFERENCES PAYMENT_METHODS(ID)
;

ALTER TABLE EMPLOYER_PAYMENTS ADD CONSTRAINT FK_EMPLOYERPAY 
    FOREIGN KEY (EMPLOYER_PAYMENT_INVOICE_ID)
    REFERENCES EMPLOYER_PAYMENT_INVOICE(ID)
;


-- 
-- TABLE: ENROLLEE 
--

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_CITIZENSHIP_STATUS_LKP 
    FOREIGN KEY (CITIZENSHIP_STATUS_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_CUSTODIAL_ENROLLEE_ID 
    FOREIGN KEY (CUSTODIAL_PARENT_ID)
    REFERENCES ENROLLEE(ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_ENROLLEE_CREATED_BY 
    FOREIGN KEY (CREATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_ENROLLEE_ENROLLMENT 
    FOREIGN KEY (ENROLLMENT_ID)
    REFERENCES ENROLLMENT(ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_ENROLLEE_HOME_LOCATION 
    FOREIGN KEY (HOME_ADDRESS_ID)
    REFERENCES LOCATIONS(ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_ENROLLEE_MAILING_LOCATION 
    FOREIGN KEY (MAILING_ADDRESS_ID)
    REFERENCES LOCATIONS(ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_ENROLLEE_STATUS_LKP 
    FOREIGN KEY (ENROLLEE_STATUS_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_ENROLLEE_UPDATED_BY 
    FOREIGN KEY (LAST_UPDATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_LANGUAGE_SPOKEN_LKP 
    FOREIGN KEY (LANGUAGE_SPOKEN_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_LANGUAGE_WRITTEN_LKP 
    FOREIGN KEY (LANGUAGE_WRITTEN_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_MARITAL_STATUS_LKP_ID 
    FOREIGN KEY (MARITAL_STATUS_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_PERSON_TYPE_LKP 
    FOREIGN KEY (PERSON_TYPE_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_RACE_ETHNICITY_LKP_ID 
    FOREIGN KEY (RACE_ETHNICITY_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_RELATIONTOHCP_LKP_VALUE_ID 
    FOREIGN KEY (RELATIONSHIP_HCP_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE ADD CONSTRAINT FK_TOBACCO_USAGE_LKP 
    FOREIGN KEY (TOBACCO_USAGE_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;


-- 
-- TABLE: ENROLLEE_RACE 
--

ALTER TABLE ENROLLEE_RACE ADD CONSTRAINT FK_RACE_LOOKUP_VALUE_ID 
    FOREIGN KEY (RACE_ETHNICITY_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;


-- 
-- TABLE: ENROLLEE_RELATIONSHIP 
--

ALTER TABLE ENROLLEE_RELATIONSHIP ADD CONSTRAINT FK_ENROLLEE_LKP_VALUE_ID 
    FOREIGN KEY (RELATIONSHIP_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE_RELATIONSHIP ADD CONSTRAINT FK_ENROLLEE_SRRELATIONSHIP_ID 
    FOREIGN KEY (SOURCE_ENROLEE_ID)
    REFERENCES ENROLLEE(ID)
;

ALTER TABLE ENROLLEE_RELATIONSHIP ADD CONSTRAINT FK_ENROLLEE_TRRELATIONSHIP_ID 
    FOREIGN KEY (TARGET_ENROLEE_ID)
    REFERENCES ENROLLEE(ID)
;

ALTER TABLE ENROLLEE_RELATIONSHIP ADD CONSTRAINT FK_RELATIONHCP_LKP_VALUE_ID 
    FOREIGN KEY (RELATIONSHIP_HCP_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;


-- 
-- TABLE: ENROLLEE_UPDATE_SEND_STATUS 
--

ALTER TABLE ENROLLEE_UPDATE_SEND_STATUS ADD CONSTRAINT FK_ENROLLEE_SEND_CREATED_BY 
    FOREIGN KEY (CREATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLEE_UPDATE_SEND_STATUS ADD CONSTRAINT FK_ENROLLEE_SEND_UPDATED_BY 
    FOREIGN KEY (LAST_UPDATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLEE_UPDATE_SEND_STATUS ADD CONSTRAINT FK_ENROLLEE_STAUS 
    FOREIGN KEY (ENROLLEE_STAUS)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLEE_UPDATE_SEND_STATUS ADD CONSTRAINT FK_ENROLLMENT_UPDATE_SEND 
    FOREIGN KEY (ENROLLEE_ID)
    REFERENCES ENROLLEE(ID)
;


-- 
-- TABLE: ENROLLMENT 
--

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_CREATED_BY 
    FOREIGN KEY (CREATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_EMPR_ID 
    FOREIGN KEY (EMPLOYER_ID)
    REFERENCES EMPLOYERS(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_FINANCIAL_ID 
    FOREIGN KEY (FINANCIAL_ID)
    REFERENCES FINANCIAL_INFO(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_ISSUER_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_PLAN_ID 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_STATUS_LKP_ID 
    FOREIGN KEY (ENROLLMENT_STATUS_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_TYPE_LKP_ID 
    FOREIGN KEY (ENROLLMENT_TYPE_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ENROLLMENT_UPDATED_BY 
    FOREIGN KEY (LAST_UPDATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_ESIGENROLL_ID 
    FOREIGN KEY (ESIGNATURE_ID)
    REFERENCES ENROLLMENT_ESIGNATURE(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_INSURANCE_LOOKUP_VALUE_ID 
    FOREIGN KEY (INSURANCE_TYPE_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_PLD_ORDER_ITEMS_ENRLMNT 
    FOREIGN KEY (IND_ORDER_ITEMS_ID)
    REFERENCES PLD_ORDER_ITEM(ID)
;

ALTER TABLE ENROLLMENT ADD CONSTRAINT FK_WORKSITE_ID_EMP_LOCATION_ID 
    FOREIGN KEY (WORKSITE_ID)
    REFERENCES EMPLOYER_LOCATIONS(ID)
;


-- 
-- TABLE: ENROLLMENT_ESIGNATURE 
--

ALTER TABLE ENROLLMENT_ESIGNATURE ADD CONSTRAINT FK_ENROLLMENT_ESIGNATURE_ID 
    FOREIGN KEY (ENROLLMENT_ID)
    REFERENCES ENROLLMENT(ID)
;

ALTER TABLE ENROLLMENT_ESIGNATURE ADD CONSTRAINT FK_ESIG_ENROLL_ID 
    FOREIGN KEY (ESIGNATURE_ID)
    REFERENCES ESIGNATURE(ID)
;


-- 
-- TABLE: ENROLLMENT_EVENT 
--

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_ENROLLMENT_EVENT_CREATED_BY 
    FOREIGN KEY (CREATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_ENROLLMENT_EVENT_ENROLLEE 
    FOREIGN KEY (ENROLLEE_ID)
    REFERENCES ENROLLEE(ID)
;

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_ENROLLMENT_EVENT_ENROLLMENT 
    FOREIGN KEY (ENROLLMENT_ID)
    REFERENCES ENROLLMENT(ID)
;

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_ENROLLMENT_EVENT_UPDATED_BY 
    FOREIGN KEY (LAST_UPDATED_BY)
    REFERENCES USERS(ID)
;

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_EVENT_REASON_LKP 
    FOREIGN KEY (EVENT_REASON_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_EVENT_TYPE_LKP 
    FOREIGN KEY (EVENT_TYPE_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;

ALTER TABLE ENROLLMENT_EVENT ADD CONSTRAINT FK_SPCL_ENROLLMENT_REASON_LKP 
    FOREIGN KEY (SPCL_ENROLLMENT_REASON_LKP)
    REFERENCES LOOKUP_VALUE(LOOKUP_VALUE_ID)
;


-- 
-- TABLE: ESIGNATURE 
--

ALTER TABLE ESIGNATURE ADD CONSTRAINT FK_ESIGN_BY_ACCOUNTUSER_ID 
    FOREIGN KEY (ESIGN_BY)
    REFERENCES USERS(ID)
;


-- 
-- TABLE: FACILITY 
--

ALTER TABLE FACILITY ADD CONSTRAINT FK_FACIL_RS01_PROVID 
    FOREIGN KEY (PROVIDER_ID)
    REFERENCES PROVIDER(ID)
;


-- 
-- TABLE: FACILITY_ADDRESS 
--

ALTER TABLE FACILITY_ADDRESS ADD CONSTRAINT FK_FADD_RS01_FID 
    FOREIGN KEY (FACILITY_ID)
    REFERENCES FACILITY(ID)
;


-- 
-- TABLE: FACILITY_SPECIALITY_RELATION 
--

ALTER TABLE FACILITY_SPECIALITY_RELATION ADD CONSTRAINT FK_FSREL_RS01_FID 
    FOREIGN KEY (FACILITY_ID)
    REFERENCES FACILITY(ID)
;

ALTER TABLE FACILITY_SPECIALITY_RELATION ADD CONSTRAINT FK_FSREL_RS01_SID 
    FOREIGN KEY (SPECIALTY_ID)
    REFERENCES SPECIALTY(ID)
;


-- 
-- TABLE: FORMULARY 
--

ALTER TABLE FORMULARY ADD CONSTRAINT FK_FORMULARY_ISSUER_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: FORMULARY_COST_SHARE_BENEFIT 
--

ALTER TABLE FORMULARY_COST_SHARE_BENEFIT ADD CONSTRAINT FK_FORMULARY_CSB_FORMULARY_ID 
    FOREIGN KEY (FORMULARY_ID)
    REFERENCES FORMULARY(ID)
;


-- 
-- TABLE: I18NTEXT 
--

ALTER TABLE I18NTEXT ADD CONSTRAINT FK_I18_RS01_NOTDID 
    FOREIGN KEY (NOTIFICATION_DOCUMENTATION_ID)
    REFERENCES NOTIFICATION(ID)
;

ALTER TABLE I18NTEXT ADD CONSTRAINT FK_I18_RS01_NOTID 
    FOREIGN KEY (NOTIFICATION_DESCRIPTIONS_ID)
    REFERENCES NOTIFICATION(ID)
;

ALTER TABLE I18NTEXT ADD CONSTRAINT FK_I18_RS01_NOTNID 
    FOREIGN KEY (NOTIFICATION_NAMES_ID)
    REFERENCES NOTIFICATION(ID)
;

ALTER TABLE I18NTEXT ADD CONSTRAINT FK_I18_RS01_NOTSID 
    FOREIGN KEY (NOTIFICATION_SUBJECTS_ID)
    REFERENCES NOTIFICATION(ID)
;

ALTER TABLE I18NTEXT ADD CONSTRAINT FK_I18_RS01_REASID 
    FOREIGN KEY (REASSIGNMENT_DOCUMENTATION_ID)
    REFERENCES REASSIGNMENT(ID)
;


-- 
-- TABLE: ISSUER_ACCREDITATION 
--

ALTER TABLE ISSUER_ACCREDITATION ADD CONSTRAINT FK_ISSU_AC_RS01_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: ISSUER_DOCUMENT 
--

ALTER TABLE ISSUER_DOCUMENT ADD CONSTRAINT FK_ISSU_DO_RS01_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: ISSUER_INVOICES 
--

ALTER TABLE ISSUER_INVOICES ADD CONSTRAINT FK_ISSUER_INVOICES_ISSUER_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: ISSUER_INVOICES_LINEITEMS 
--

ALTER TABLE ISSUER_INVOICES_LINEITEMS ADD CONSTRAINT FK_EMP_INV_LI_ID 
    FOREIGN KEY (EMP_INV_LINEITEMS_ID)
    REFERENCES EMPLOYER_INVOICES_LINEITEMS(ID)
;

ALTER TABLE ISSUER_INVOICES_LINEITEMS ADD CONSTRAINT FK_ISS_INV_ENROLLMENT_ID 
    FOREIGN KEY (ENROLLMENT_ID)
    REFERENCES ENROLLMENT(ID)
;

ALTER TABLE ISSUER_INVOICES_LINEITEMS ADD CONSTRAINT FK_ISSUER_INV_ITEMS_EMPID 
    FOREIGN KEY (EMPLOYEE_ID)
    REFERENCES EMPLOYEES(ID)
;

ALTER TABLE ISSUER_INVOICES_LINEITEMS ADD CONSTRAINT FK_ISSUER_INV_ITEMS_EMPR_ID 
    FOREIGN KEY (EMPLOYER_ID)
    REFERENCES EMPLOYERS(ID)
;

ALTER TABLE ISSUER_INVOICES_LINEITEMS ADD CONSTRAINT FK_ISSUER_INVITEMS_INVOICE_ID 
    FOREIGN KEY (INVOICE_ID)
    REFERENCES ISSUER_INVOICES(ID)
;


-- 
-- TABLE: ISSUER_PAYMENT_DETAIL 
--

ALTER TABLE ISSUER_PAYMENT_DETAIL ADD CONSTRAINT FK_ISSUER_PAYMENT_ID 
    FOREIGN KEY (ISSUER_PAYMENT_ID)
    REFERENCES ISSUER_PAYMENTS(ID)
;

ALTER TABLE ISSUER_PAYMENT_DETAIL ADD CONSTRAINT FK_ISSUER_PAYMENTS 
    FOREIGN KEY (PAYMENT_TYPE_ID)
    REFERENCES PAYMENT_METHODS(ID)
;


-- 
-- TABLE: ISSUER_PAYMENT_INVOICE 
--

ALTER TABLE ISSUER_PAYMENT_INVOICE ADD CONSTRAINT FK_ISSUER_PAYMENT_INVOICE 
    FOREIGN KEY (INVOICE_ID)
    REFERENCES ISSUER_INVOICES(ID)
;


-- 
-- TABLE: ISSUER_PAYMENTS 
--

ALTER TABLE ISSUER_PAYMENTS ADD CONSTRAINT FK_ISSUER_INVOICE_ID 
    FOREIGN KEY (ISSUER_PAYMENT_INVOICE_ID)
    REFERENCES ISSUER_PAYMENT_INVOICE(ID)
;


-- 
-- TABLE: ISSUER_QUALITY_RATING 
--

ALTER TABLE ISSUER_QUALITY_RATING ADD CONSTRAINT FK_ISSU_QA_RS01_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: ISSUER_REPRESENTATIVE 
--

ALTER TABLE ISSUER_REPRESENTATIVE ADD CONSTRAINT FK_ISSU_RE_RS01_ID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;

ALTER TABLE ISSUER_REPRESENTATIVE ADD CONSTRAINT FK_ISSU_RE_RS01_UID 
    FOREIGN KEY (USER_ID)
    REFERENCES USERS(ID)
;

ALTER TABLE ISSUER_REPRESENTATIVE ADD CONSTRAINT FK_ISSUER_REP_LOCATION 
    FOREIGN KEY (LOCATION_ID)
    REFERENCES LOCATIONS(ID)
;


-- 
-- TABLE: MEMBER_DETAILS 
--

ALTER TABLE MEMBER_DETAILS ADD CONSTRAINT FK_MEMBER2EMPLOYEE_ID 
    FOREIGN KEY (EMPLOYEE_ID)
    REFERENCES EMPLOYEES(ID)
;

ALTER TABLE MEMBER_DETAILS ADD CONSTRAINT FK_MEMBER2LOCATION_ID 
    FOREIGN KEY (HOME_LOCATION_ID)
    REFERENCES LOCATIONS(ID)
;


-- 
-- TABLE: NETWORK 
--

ALTER TABLE NETWORK ADD CONSTRAINT FK_NET_RS01_ISSID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: NOTICE_TYPES_AUD 
--

ALTER TABLE NOTICE_TYPES_AUD ADD CONSTRAINT FK_NOTICE_TYPES_AUD_RS01_ID 
    FOREIGN KEY (ID)
    REFERENCES NOTICE_TYPES(ID)
;

ALTER TABLE NOTICE_TYPES_AUD ADD CONSTRAINT FK_NOTICE_TYPES_AUD_RS02_REVID 
    FOREIGN KEY (REV)
    REFERENCES REVISION_INFO(REVISION_ID)
;


-- 
-- TABLE: NOTICES 
--

ALTER TABLE NOTICES ADD CONSTRAINT FK_NOT_RS01_NTID 
    FOREIGN KEY (NOTICE_TYPE_ID)
    REFERENCES NOTICE_TYPES(ID)
;

ALTER TABLE NOTICES ADD CONSTRAINT FK_NOT_RS01_UID 
    FOREIGN KEY (USER_ID)
    REFERENCES USERS(ID)
;


-- 
-- TABLE: PHYSICIAN 
--

ALTER TABLE PHYSICIAN ADD CONSTRAINT FK_PHYS_RS01_PROID 
    FOREIGN KEY (PROVIDER_ID)
    REFERENCES PROVIDER(ID)
;


-- 
-- TABLE: PHYSICIAN_ADDRESS 
--

ALTER TABLE PHYSICIAN_ADDRESS ADD CONSTRAINT FK_PHYS_RS01_ADDID 
    FOREIGN KEY (PHYSICIAN_ID)
    REFERENCES PHYSICIAN(ID)
;


-- 
-- TABLE: PHYSICIAN_SPECIALITY_RELATION 
--

ALTER TABLE PHYSICIAN_SPECIALITY_RELATION ADD CONSTRAINT FK_PHY_S_RS01_PHYID 
    FOREIGN KEY (PHYSICIAN_ID)
    REFERENCES PHYSICIAN(ID)
;

ALTER TABLE PHYSICIAN_SPECIALITY_RELATION ADD CONSTRAINT FK_PHY_S_RS01_SID 
    FOREIGN KEY (SPECIALTY_ID)
    REFERENCES SPECIALTY(ID)
;


-- 
-- TABLE: PLAN 
--

ALTER TABLE PLAN ADD CONSTRAINT FK_PLAN_RS01_ISSID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;


-- 
-- TABLE: PLAN_AUD 
--

ALTER TABLE PLAN_AUD ADD CONSTRAINT FK_PLAN_AUD_RS01_ID 
    FOREIGN KEY (ID)
    REFERENCES PLAN(ID)
;

ALTER TABLE PLAN_AUD ADD CONSTRAINT FK_PLAN_AUD_RS02_REVID 
    FOREIGN KEY (REV)
    REFERENCES REVISION_INFO(REVISION_ID)
;


-- 
-- TABLE: PLAN_DENTAL 
--

ALTER TABLE PLAN_DENTAL ADD CONSTRAINT FK_PLAN_D_RS01_PID 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;


-- 
-- TABLE: PLAN_DENTAL_BENEFIT 
--

ALTER TABLE PLAN_DENTAL_BENEFIT ADD CONSTRAINT FK_PLA_D_B_RS01_PDID 
    FOREIGN KEY (PLAN_DENTAL_ID)
    REFERENCES PLAN_DENTAL(ID)
;


-- 
-- TABLE: PLAN_DENTAL_COST 
--

ALTER TABLE PLAN_DENTAL_COST ADD CONSTRAINT FK_PHC_PLAN_DENTAL_ID 
    FOREIGN KEY (PLAN_DENTAL_ID)
    REFERENCES PLAN_DENTAL(ID)
;


-- 
-- TABLE: PLAN_HEALTH 
--

ALTER TABLE PLAN_HEALTH ADD CONSTRAINT FK_PLAN_H_RS01_PID 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;

ALTER TABLE PLAN_HEALTH ADD CONSTRAINT FK_PLAN_SBC_SCENARIO 
    FOREIGN KEY (PLAN_SBC_SCENARIO_ID)
    REFERENCES PLAN_SBC_SCENARIO(ID)
;


-- 
-- TABLE: PLAN_HEALTH_AUD 
--

ALTER TABLE PLAN_HEALTH_AUD ADD CONSTRAINT FK_PLAN__HEALTH_AUD_RS01_ID 
    FOREIGN KEY (ID)
    REFERENCES PLAN_HEALTH(ID)
;

ALTER TABLE PLAN_HEALTH_AUD ADD CONSTRAINT FK_PLAN__HEALTH_AUD_RS02_REVID 
    FOREIGN KEY (REV)
    REFERENCES REVISION_INFO(REVISION_ID)
;


-- 
-- TABLE: PLAN_HEALTH_BENEFIT 
--

ALTER TABLE PLAN_HEALTH_BENEFIT ADD CONSTRAINT FK_PLA_H_B_RS01_PHID 
    FOREIGN KEY (PLAN_HEALTH_ID)
    REFERENCES PLAN_HEALTH(ID)
;


-- 
-- TABLE: PLAN_HEALTH_COST 
--

ALTER TABLE PLAN_HEALTH_COST ADD CONSTRAINT FK_PHC_PLAN_HEALTH_ID 
    FOREIGN KEY (PLAN_HEALTH_ID)
    REFERENCES PLAN_HEALTH(ID)
;


-- 
-- TABLE: PLAN_QUALITY_RATING 
--

ALTER TABLE PLAN_QUALITY_RATING ADD CONSTRAINT FK_PLA_Q_RS01_PID 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;


-- 
-- TABLE: PLD_GROUP 
--

ALTER TABLE PLD_GROUP ADD CONSTRAINT FK_PLDGRPHH_RS01_ID 
    FOREIGN KEY (PLD_HOUSEHOLD_ID)
    REFERENCES PLD_HOUSEHOLD(ID)
;


-- 
-- TABLE: PLD_GROUP_PERSON 
--

ALTER TABLE PLD_GROUP_PERSON ADD CONSTRAINT FK_PLDGRPPSN_RS01_ID 
    FOREIGN KEY (PLD_GROUP_ID)
    REFERENCES PLD_GROUP(ID)
;

ALTER TABLE PLD_GROUP_PERSON ADD CONSTRAINT FK_PLDGRPPSN_RS02_ID 
    FOREIGN KEY (PLD_PERSON_ID)
    REFERENCES PLD_PERSON(ID)
;


-- 
-- TABLE: PLD_HOUSEHOLD_PERSON 
--

ALTER TABLE PLD_HOUSEHOLD_PERSON ADD CONSTRAINT FK_PLDHLDPSN_PLDHLD_ID 
    FOREIGN KEY (PLD_HOUSEHOLD_ID)
    REFERENCES PLD_HOUSEHOLD(ID)
;

ALTER TABLE PLD_HOUSEHOLD_PERSON ADD CONSTRAINT FK_PLDHLDPSN_PLDPSN_ID 
    FOREIGN KEY (PLD_PERSON_ID)
    REFERENCES PLD_PERSON(ID)
;


-- 
-- TABLE: PLD_ORDER 
--

ALTER TABLE PLD_ORDER ADD CONSTRAINT FK_PLDGRP_RS01_ID 
    FOREIGN KEY (PLD_HOUSEHOLD_ID)
    REFERENCES PLD_HOUSEHOLD(ID)
;


-- 
-- TABLE: PLD_ORDER_ITEM 
--

ALTER TABLE PLD_ORDER_ITEM ADD CONSTRAINT FK_PLDRORD_RS01_ID 
    FOREIGN KEY (PLD_ORDER_ID)
    REFERENCES PLD_ORDER(ID)
;

ALTER TABLE PLD_ORDER_ITEM ADD CONSTRAINT FK_PLDRORGRP_RS01_ID 
    FOREIGN KEY (PLD_GROUP_ID)
    REFERENCES PLD_GROUP(ID)
;

ALTER TABLE PLD_ORDER_ITEM ADD CONSTRAINT FK_PLDRORITE_RS01_ID 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;


-- 
-- TABLE: PM_ISSUER_SERVICE_AREA_EXT 
--

ALTER TABLE PM_ISSUER_SERVICE_AREA_EXT ADD CONSTRAINT FK_ISSUER_SERVICE_AREA 
    FOREIGN KEY (ISSUER_SERVICE_AREA_ID)
    REFERENCES PM_ISSUER_SERVICE_AREA(ID)
;


-- 
-- TABLE: PM_PLAN_RATE 
--

ALTER TABLE PM_PLAN_RATE ADD CONSTRAINT FK_PM_PLAN_ID_01 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;

ALTER TABLE PM_PLAN_RATE ADD CONSTRAINT FK_PM_RATING_AREA_ID_02 
    FOREIGN KEY (RATING_AREA_ID)
    REFERENCES PM_RATING_AREA(ID)
;


-- 
-- TABLE: PM_SERVICE_AREA 
--

ALTER TABLE PM_SERVICE_AREA ADD CONSTRAINT FK_PM_ISSUER_SERVICE_AREA_EXT 
    FOREIGN KEY (SERVICE_AREA_EXT_ID)
    REFERENCES PM_ISSUER_SERVICE_AREA_EXT(ID)
;

ALTER TABLE PM_SERVICE_AREA ADD CONSTRAINT FK_PM_SERVICE_AREA_ID_01 
    FOREIGN KEY (SERVICE_AREA_ID)
    REFERENCES PM_ISSUER_SERVICE_AREA(ID)
;


-- 
-- TABLE: PM_ZIP_COUNTY_RATING_AREA 
--

ALTER TABLE PM_ZIP_COUNTY_RATING_AREA ADD CONSTRAINT FK_PM_RATING_AREA_ID_01 
    FOREIGN KEY (RATING_AREA_ID)
    REFERENCES PM_RATING_AREA(ID)
;


-- 
-- TABLE: PROVIDER_NETWORK 
--

ALTER TABLE PROVIDER_NETWORK ADD CONSTRAINT FK_PNET_RS01_NID 
    FOREIGN KEY (NETWORK_ID)
    REFERENCES NETWORK(ID)
;

ALTER TABLE PROVIDER_NETWORK ADD CONSTRAINT FK_PNET_RS01_PID 
    FOREIGN KEY (PROVIDER_ID)
    REFERENCES PROVIDER(ID)
;


-- 
-- TABLE: PROVIDER_RATING 
--

ALTER TABLE PROVIDER_RATING ADD CONSTRAINT FK_PRAT_RS01_PID 
    FOREIGN KEY (PROVIDER_ID)
    REFERENCES PROVIDER(ID)
;


-- 
-- TABLE: RATE_REVIEW_DATA 
--

ALTER TABLE RATE_REVIEW_DATA ADD CONSTRAINT FK_RRD_RS01_IID 
    FOREIGN KEY (ISSUER_ID)
    REFERENCES ISSUERS(ID)
;

ALTER TABLE RATE_REVIEW_DATA ADD CONSTRAINT FK_RRD_RS01_PID 
    FOREIGN KEY (PLAN_ID)
    REFERENCES PLAN(ID)
;


-- 
-- TABLE: SERFF_DOCUMENT 
--

ALTER TABLE SERFF_DOCUMENT ADD CONSTRAINT SERFF_DOCUMENT_SERFF_PM_FK 
    FOREIGN KEY (SERFF_REQ_ID)
    REFERENCES SERFF_PLAN_MGMT(SERFF_REQ_ID)
;

