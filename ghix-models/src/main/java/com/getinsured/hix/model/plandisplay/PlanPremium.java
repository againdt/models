package com.getinsured.hix.model.plandisplay;

public class PlanPremium {
	
	private Float avgIndiPremium;
	private String planName;
	private Float totalDepePremium;
	private Float totalIndiPremium;
	//avgIndiPremium":"190.13","planName":"Lowest priced","totalDepePremium":"0.0","totalIndiPremium":"190.13
	
	public Float getAvgIndiPremium() {
		return avgIndiPremium;
	}
	public void setAvgIndiPremium(Float avgIndiPremium) {
		this.avgIndiPremium = avgIndiPremium;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getTotalDepePremium() {
		return totalDepePremium;
	}
	public void setTotalDepePremium(Float totalDepePremium) {
		this.totalDepePremium = totalDepePremium;
	}
	public Float getTotalIndiPremium() {
		return totalIndiPremium;
	}
	public void setTotalIndiPremium(Float totalIndiPremium) {
		this.totalIndiPremium = totalIndiPremium;
	}

	public static PlanPremium clone(PlanPremium planPremium) {
		PlanPremium planPremiumClone = new PlanPremium();
		planPremiumClone.setPlanName(planPremium.getPlanName());
		planPremiumClone.setAvgIndiPremium(planPremium.getAvgIndiPremium());
		planPremiumClone.setTotalDepePremium(planPremium.getTotalDepePremium());
		planPremiumClone.setTotalIndiPremium(planPremium.getTotalIndiPremium());
		return planPremiumClone;
	}
}
