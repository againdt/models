package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class PersonSSNIdentification{
  @JsonProperty("IdentificationID")
  
  private IdentificationID IdentificationID;
  @JsonProperty("IdentificationSourceText")
  
  private Object IdentificationSourceText;
  public void setIdentificationID(IdentificationID IdentificationID){
   this.IdentificationID=IdentificationID;
  }
  public IdentificationID getIdentificationID(){
   return IdentificationID;
  }
  public void setIdentificationSourceText(Object IdentificationSourceText){
   this.IdentificationSourceText=IdentificationSourceText;
  }
  public Object getIdentificationSourceText(){
   return IdentificationSourceText;
  }
}