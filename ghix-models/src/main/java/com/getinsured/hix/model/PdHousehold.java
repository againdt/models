package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;

@Entity
@Table(name="PD_HOUSEHOLD")
public class PdHousehold implements Serializable
{	
	private static final long serialVersionUID = 1L;	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pd_household_seq")
	@SequenceGenerator(name = "pd_household_seq", sequenceName = "pd_household_seq", allocationSize = 1)
	private Long id;

	@Column(name = "external_id")
	private String externalId;
	
	@Column(name="enrollment_type")
	@Enumerated(EnumType.STRING)
	private PlanDisplayEnum.EnrollmentType enrollmentType;
	
	@Column(name = "max_subsidy")
	private Float maxSubsidy;
	
	@Column(name = "health_subsidy")
	private Float healthSubsidy;
	
	@Column(name = "dental_subsidy")
	private Float dentalSubsidy;
	
	@Column(name = "cost_sharing")
	@Enumerated(EnumType.STRING)
	private PlanDisplayEnum.CostSharing costSharing;
	
	@Column(name = "coverage_start_date")
	private Date coverageStartDate;
	
	@Column(name="shopping_type")
	@Enumerated(EnumType.STRING)
	private PlanDisplayEnum.ShoppingType shoppingType;
	
	@Column(name="shopping_id")
	private String shoppingId;
	
	@Column(name="household_data")
	private String householdData;
	
	@Column(name="employer_id")
	private Long employerId;
	
	@Column(name="elig_lead_id")
	private Long eligLeadId;
	
	@Column(name="application_id")
	private Long applicationId;
	
	@Column(name="keep_only")
	@Enumerated(EnumType.STRING)
	private PlanDisplayEnum.YorN keepOnly;
	
	@Column(name="auto_renewal")
	@Enumerated(EnumType.STRING)
	private PlanDisplayEnum.YorN autoRenewal;
	
	@Column(name="state_exchange_code")
	private String stateExchangeCode;
	
	@Column(name = "request_type")
	@Enumerated(EnumType.STRING)
	private PlanDisplayEnum.RequestType requestType;
	
	@Column(name="health_subscriber_id")
	private String healthSubscriberId;    

	@Column(name="dental_subscriber_id")
	private String dentalSubscriberId;  

	@Column(name="plan_pref")
	private String planPref;  	
	
	@Column(name="gi_ws_payload_id")
	private Long giWsPayloadId;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="creation_timestamp")
	private Date creationTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date lastUpdateTimestamp;
    
    @Column(name = "providers")
	private String providers;
    
    @Column(name="prescriptions")
	private String prescriptions ;
    
    @Column(name="preview_id")
	private String previewId;
    
    @Column(name = "financial_effective_date")
	private Date financialEffectiveDate;
    
    @Column(name = "aptc_for_keep")
	private Float aptcForKeep;

    @Column(name = "state_subsidy")
	private BigDecimal stateSubsidy;

    @Column(name = "max_state_subsidy")
    private BigDecimal maxStateSubsidy;
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public PlanDisplayEnum.EnrollmentType getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(PlanDisplayEnum.EnrollmentType enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public Float getMaxSubsidy() {
		return maxSubsidy;
	}

	public void setMaxSubsidy(Float maxSubsidy) {
		this.maxSubsidy = maxSubsidy;
	}

	public Float getHealthSubsidy() {
		return healthSubsidy;
	}

	public void setHealthSubsidy(Float healthSubsidy) {
		this.healthSubsidy = healthSubsidy;
	}

	public Float getDentalSubsidy() {
		return dentalSubsidy;
	}

	public void setDentalSubsidy(Float dentalSubsidy) {
		this.dentalSubsidy = dentalSubsidy;
	}

	public PlanDisplayEnum.CostSharing getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(PlanDisplayEnum.CostSharing costSharing) {
		this.costSharing = costSharing;
	}

	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public PlanDisplayEnum.ShoppingType getShoppingType() {
		return shoppingType;
	}

	public void setShoppingType(PlanDisplayEnum.ShoppingType shoppingType) {
		this.shoppingType = shoppingType;
	}

	public String getShoppingId() {
		return shoppingId;
	}

	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}

	public String getHouseholdData() {
		return householdData;
	}

	public void setHouseholdData(String householdData) {
		this.householdData = householdData;
	}

	public Long getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Long employerId) {
		this.employerId = employerId;
	}

	public Long getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(Long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public PlanDisplayEnum.YorN getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(PlanDisplayEnum.YorN keepOnly) {
		this.keepOnly = keepOnly;
	}

	public PlanDisplayEnum.YorN getAutoRenewal() {
		return autoRenewal;
	}

	public void setAutoRenewal(PlanDisplayEnum.YorN autoRenewal) {
		this.autoRenewal = autoRenewal;
	}

	public String getStateExchangeCode() {
		return stateExchangeCode;
	}

	public void setStateExchangeCode(String stateExchangeCode) {
		this.stateExchangeCode = stateExchangeCode;
	}

	public PlanDisplayEnum.RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(PlanDisplayEnum.RequestType requestType) {
		this.requestType = requestType;
	}

	public String getHealthSubscriberId() {
		return healthSubscriberId;
	}

	public void setHealthSubscriberId(String healthSubscriberId) {
		this.healthSubscriberId = healthSubscriberId;
	}

	public String getDentalSubscriberId() {
		return dentalSubscriberId;
	}

	public void setDentalSubscriberId(String dentalSubscriberId) {
		this.dentalSubscriberId = dentalSubscriberId;
	}
	
	public String getPlanPref() {
		return planPref;
	}

	public void setPlanPref(String planPref) {
		this.planPref = planPref;
	}
	
	public Long getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Long giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
	public String getProviders() {
		return providers;
	}

	public void setProviders(String providers) {
		this.providers = providers;
	}

	public String getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(String prescriptions) {
		this.prescriptions = prescriptions;
	}

	public String getPreviewId() {
		return previewId;
	}

	public void setPreviewId(String previewId) {
		this.previewId = previewId;
	}

	@PrePersist
	public void prePersist() {
		this.setShoppingId(UUID.randomUUID().toString());
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}

	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}

	public Float getAptcForKeep() {
		return aptcForKeep;
	}

	public void setAptcForKeep(Float aptcForKeep) {
		this.aptcForKeep = aptcForKeep;
	}

	public BigDecimal getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(BigDecimal stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}

	public BigDecimal getMaxStateSubsidy() {
		return maxStateSubsidy;
	}

	public void setMaxStateSubsidy(BigDecimal maxStateSubsidy) {
		this.maxStateSubsidy = maxStateSubsidy;
	}
}
