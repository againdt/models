package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

public class AgencyDetailsListDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private long totalRecords;
	private List<AgencyDetailsDto> agencysList;
	private List<String> certificationStatus;
	
	
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public List<AgencyDetailsDto> getAgencysList() {
		return agencysList;
	}
	public void setAgencysList(List<AgencyDetailsDto> agencysList) {
		this.agencysList = agencysList;
	}
	public List<String> getCertificationStatus() {
		return certificationStatus;
	}
	public void setCertificationStatus(List<String> certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	
}