package com.getinsured.hix.dto.planmgmt.microservice;

public class SerffTransferSearchRequestDTO extends SearchDTO {

	private String hiosIssuerID;
	private String startDate;
	private String status;

	
	public SerffTransferSearchRequestDTO() {
		super();
	}
	public String getHiosIssuerID() {
		return hiosIssuerID;
	}
	public void setHiosIssuerID(String hiosIssuerID) {
		this.hiosIssuerID = hiosIssuerID;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
