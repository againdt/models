
Assumptions 
1. Oracle client is installed on the server.
   Preferably, do so on database server.

2. Step 1 to Step 3 are done. They are just as a reference here.  
   Go to Step 4.

Step 1.  create schemata
         <SCHEMA PREFIX>_OWNER, <SCHEMA PREFIX>_USER, <SCHEMA PREFIX>_READONLY
         
         create role
         <SCHEMA PREFIX>_USER_ROLE, <SCHEMA PREFIX>_READONLY_ROLE
         
         grant role to appropriate schema:
         
         grant <SCHEMA PREFIX>_USER_ROLE to <SCHEMA PREFIX>_USER;
         
         grant <SCHEMA PREFIX>_READONLY_ROLE to <SCHEMA PREFIX>_READONLY
         
         Example
         GHIX1_OWNER, GHIX1_USER, GHIX1_READONLY
         GHIX1_USER_ROLE, GHIX1_READONLY_ROLE
         
In addition, an Oracle account DEV_JT and its Password DEV_JT is created with DBA privilege.
It is used in scripts: grantandcrsynonym.sh and grantandcrsynonym.sh
If necessary, make the update accordingly.
         
Step 2.  Following specific environment instruction, change/update configuration.properties 

Example 
cd /home/oracle/liquibase/ghix-setup/conf
vi configuration.properties

Step 3.   Following specific environment instruction,
 run liquibase to create database objects in <SCHEMA PREFIX>_OWNER

Example
cd /home/oracle/liquibase/
   ./liquibase-setup.sh

Step 4. Wait for the deployment to complete the "liquibase" procedure, then run grant and create synonyms

Step 4.1 
Create a directory in oracle account and copy all files under "grantandsynonym" to it.
Then, to go the directory.

Example
/home/oracle/bk_setup/grant_and_cr_synonym_dir

cd /home/oracle/bk_setup/grant_and_cr_synonym_dir

Step 4.2
update DB_ENV.sh

Assuming the Oracle client installed on this machine,
 make sure ORACLE_BASE, ORACLE_HOME, PATH, LD_LIBRARY_PATH, and EZCONNECT
 are setup appropriately:

export ORACLE_BASE=/u01/app/oracle
export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/db_1
export PATH=/usr/sbin:$PATH
export PATH=$ORACLE_HOME/bin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib

export EZCONNECT=//ghixdb.com:1521/ghixdb

### For database on ghixpilot.com
===> export EZCONNECT=//ghixpilot.com:1521/devdb
or
### For database on phixdbproduction
===> export EZCONNECT=//10.182.133.87:1521/ghixdb

Step 5

Assumption
An Oracle account DEV_JT and its Password DEV_JT is created with DBA privilege.
It is used in scripts: grantandcrsynonym.sh and grantandcrsynonym.sh
If necessary, make the update accordingly.

To grant DMLs and create synonyms for the objects of <SCHEMA PREFIX>_OWNER  in <SCHEMA PREFIX>_USER, do:
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym.sh <SCHEMA PREFIX>_USER <SCHEMA PREFIX>_OWNER
Example
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym.sh GHIX1_USER GHIX1_OWNER

To grant readonly (SELECT) and create synonyms for the objects of <SCHEMA PREFIX>_OWNER  in <SCHEMA PREFIX>_READONLY, do:
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym_readonly.sh <SCHEMA PREFIX>_READONLY <SCHEMA PREFIX>_OWNER
Example
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym_readonly.sh GHIX1_READONLY GHIX1_OWNER
