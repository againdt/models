package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrollmentPremium1095PDFDTO implements Serializable{
	private String monthlyEnrollmentPremium;
	private String monthlySLCSP;
	private String monthlyAPTC;
	public String getMonthlyEnrollmentPremium() {
		return monthlyEnrollmentPremium;
	}
	public void setMonthlyEnrollmentPremium(String monthlyEnrollmentPremium) {
		this.monthlyEnrollmentPremium = monthlyEnrollmentPremium;
	}
	public String getMonthlySLCSP() {
		return monthlySLCSP;
	}
	public void setMonthlySLCSP(String monthlySLCSP) {
		this.monthlySLCSP = monthlySLCSP;
	}
	public String getMonthlyAPTC() {
		return monthlyAPTC;
	}
	public void setMonthlyAPTC(String monthlyAPTC) {
		this.monthlyAPTC = monthlyAPTC;
	}
	
	
	
}
