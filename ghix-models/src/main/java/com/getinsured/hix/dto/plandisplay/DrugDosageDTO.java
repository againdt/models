package com.getinsured.hix.dto.plandisplay;

public class DrugDosageDTO {

	private String drugDosageID;
	private String drugNDC;
	private String drugDosageName;
	private String drugFullName;
	private String drugRxCui;
	private String strength;
	private String commonDaysOfSupply;
	private String genericDosageID;
	private String genericNDC;
	private String genericDosageName;
	private boolean isCommonDosage = false;
	
	public String getDrugDosageID() {
		return drugDosageID;
	}
	public void setDrugDosageID(String drugDosageID) {
		this.drugDosageID = drugDosageID;
	}
	public String getDrugNDC() {
		return drugNDC;
	}
	public void setDrugNDC(String drugNDC) {
		this.drugNDC = drugNDC;
	}
	public String getDrugDosageName() {
		return drugDosageName;
	}
	public void setDrugDosageName(String drugDosageName) {
		this.drugDosageName = drugDosageName;
	}
	public String getDrugRxCui() {
		return drugRxCui;
	}
	public void setDrugRxCui(String drugRxCui) {
		this.drugRxCui = drugRxCui;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getCommonDaysOfSupply() {
		return commonDaysOfSupply;
	}
	public void setCommonDaysOfSupply(String commonDaysOfSupply) {
		this.commonDaysOfSupply = commonDaysOfSupply;
	}
	public String getGenericDosageID() {
		return genericDosageID;
	}
	public void setGenericDosageID(String genericDosageID) {
		this.genericDosageID = genericDosageID;
	}
	public String getGenericNDC() {
		return genericNDC;
	}
	public void setGenericNDC(String genericNDC) {
		this.genericNDC = genericNDC;
	}
	public String getGenericDosageName() {
		return genericDosageName;
	}
	public void setGenericDosageName(String genericDosageName) {
		this.genericDosageName = genericDosageName;
	}
	public boolean isCommonDosage() {
		return isCommonDosage;
	}
	public void setCommonDosage(boolean isCommonDosage) {
		this.isCommonDosage = isCommonDosage;
	}
	public String getDrugFullName() {
		return drugFullName;
	}
	public void setDrugFullName(String drugFullName) {
		this.drugFullName = drugFullName;
	}
	
}
