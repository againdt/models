package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GHIXResponse;
import com.google.gson.Gson;

public class PlanCrossWalkResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	String isAvailable;
	String hiosPlanNumber;
	

	/**
	 * @return the flag
	 */
	public String getIsAvailable() {
		return isAvailable;
	}

	/**
	 * @param flag the flag to set
	 */
	public void setIsAvailable(String flag) {
		this.isAvailable = flag;
	}

	/**
	 * @return the hiosPlanNumber
	 */
	public String getHiosPlanNumber() {
		return hiosPlanNumber;
	}

	/**
	 * @param hiosPlanNumber the hiosPlanNumber to set
	 */
	public void setHiosPlanNumber(String hiosPlanNumber) {
		this.hiosPlanNumber = hiosPlanNumber;
	}

	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}
}
