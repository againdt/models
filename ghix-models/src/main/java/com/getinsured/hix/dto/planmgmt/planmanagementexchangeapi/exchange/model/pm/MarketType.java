
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MarketType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Individual"/>
 *     &lt;enumeration value="SHOP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "")
@XmlEnum
@XmlRootElement(name="marketType")
public enum MarketType {

    @XmlEnumValue("Individual")
    INDIVIDUAL("Individual"),
    SHOP("SHOP");
    private final String value;

    MarketType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MarketType fromValue(String v) {
        for (MarketType c: MarketType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
