insert into PERMISSIONS_GROUP (ID,NAME) values (1,'Account Management');
insert into PERMISSIONS_GROUP (ID,NAME) values (2,'Application Configuration');
insert into PERMISSIONS_GROUP (ID,NAME) values (3,'Customer Management');
insert into PERMISSIONS_GROUP (ID,NAME) values (4,'Enrollment Reconciliation');
insert into PERMISSIONS_GROUP (ID,NAME) values (5,'Miscellaneous');
insert into PERMISSIONS_GROUP (ID,NAME) values (6,'Partner Management');
insert into PERMISSIONS_GROUP (ID,NAME) values (7,'Plan Management');
insert into PERMISSIONS_GROUP (ID,NAME) values (8,'Ticket Management');
 commit;
DROP SEQUENCE PERMISSIONS_GROUP_SEQ;
CREATE SEQUENCE PERMISSIONS_GROUP_SEQ START WITH 1008;

