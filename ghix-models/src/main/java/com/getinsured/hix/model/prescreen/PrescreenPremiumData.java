package com.getinsured.hix.model.prescreen;

/**
 * This model stores the various QHPs premium details for each member
 * passed by Eligibility module. This data is returned as response to Eligibility
 * 
 * @author bhatia_s
 * @since 27 Sep 2013
 */
public class PrescreenPremiumData {

	private int numberOfIssuers;
	private int numberOfQHPs;
	private int numberOfCatQHPs;
	private int numberOfBronzeQHPs;
	private int numberOfSilverQHPs;
	private int numberOfGoldQHPs;
	private int numberOfPlatinumQHPs;
	private double lowestCatPremium;
	private double lowestSilverPremium;
	private double secLowestSilverPremium;
	private double lowestGoldPremium;
	private double lowestBronzePremium;
	/**
	 * @return the lowestBronzePremium
	 */
	public double getLowestBronzePremium() {
		return lowestBronzePremium;
	}
	/**
	 * @param lowestBronzePremium the lowestBronzePremium to set
	 */
	public void setLowestBronzePremium(double lowestBronzePremium) {
		this.lowestBronzePremium = lowestBronzePremium;
	}
	private boolean isPremiumFound;
	/**
	 * @return the numberOfIssuers
	 */
	public int getNumberOfIssuers() {
		return numberOfIssuers;
	}
	/**
	 * @param numberOfIssuers the numberOfIssuers to set
	 */
	public void setNumberOfIssuers(int numberOfIssuers) {
		this.numberOfIssuers = numberOfIssuers;
	}
	/**
	 * @return the numberOfQHPs
	 */
	public int getNumberOfQHPs() {
		return numberOfQHPs;
	}
	/**
	 * @param numberOfQHPs the numberOfQHPs to set
	 */
	public void setNumberOfQHPs(int numberOfQHPs) {
		this.numberOfQHPs = numberOfQHPs;
	}
	/**
	 * @return the numberOfCatQHPs
	 */
	public int getNumberOfCatQHPs() {
		return numberOfCatQHPs;
	}
	/**
	 * @param numberOfCatQHPs the numberOfCatQHPs to set
	 */
	public void setNumberOfCatQHPs(int numberOfCatQHPs) {
		this.numberOfCatQHPs = numberOfCatQHPs;
	}
	/**
	 * @return the numberOfBronzeQHPs
	 */
	public int getNumberOfBronzeQHPs() {
		return numberOfBronzeQHPs;
	}
	/**
	 * @param numberOfBronzeQHPs the numberOfBronzeQHPs to set
	 */
	public void setNumberOfBronzeQHPs(int numberOfBronzeQHPs) {
		this.numberOfBronzeQHPs = numberOfBronzeQHPs;
	}
	/**
	 * @return the numberOfSilverQHPs
	 */
	public int getNumberOfSilverQHPs() {
		return numberOfSilverQHPs;
	}
	/**
	 * @param numberOfSilverQHPs the numberOfSilverQHPs to set
	 */
	public void setNumberOfSilverQHPs(int numberOfSilverQHPs) {
		this.numberOfSilverQHPs = numberOfSilverQHPs;
	}
	/**
	 * @return the numberOfGoldQHPs
	 */
	public int getNumberOfGoldQHPs() {
		return numberOfGoldQHPs;
	}
	/**
	 * @param numberOfGoldQHPs the numberOfGoldQHPs to set
	 */
	public void setNumberOfGoldQHPs(int numberOfGoldQHPs) {
		this.numberOfGoldQHPs = numberOfGoldQHPs;
	}
	/**
	 * @return the numberOfPlatinumQHPs
	 */
	public int getNumberOfPlatinumQHPs() {
		return numberOfPlatinumQHPs;
	}
	/**
	 * @param numberOfPlatinumQHPs the numberOfPlatinumQHPs to set
	 */
	public void setNumberOfPlatinumQHPs(int numberOfPlatinumQHPs) {
		this.numberOfPlatinumQHPs = numberOfPlatinumQHPs;
	}
	/**
	 * @return the lowestCatPremium
	 */
	public double getLowestCatPremium() {
		return lowestCatPremium;
	}
	/**
	 * @param lowestCatPremium the lowestCatPremium to set
	 */
	public void setLowestCatPremium(double lowestCatPremium) {
		this.lowestCatPremium = lowestCatPremium;
	}
	/**
	 * @return the lowestSilverPremium
	 */
	public double getLowestSilverPremium() {
		return lowestSilverPremium;
	}
	/**
	 * @param lowestSilverPremium the lowestSilverPremium to set
	 */
	public void setLowestSilverPremium(double lowestSilverPremium) {
		this.lowestSilverPremium = lowestSilverPremium;
	}
	/**
	 * @return the secLowestSilverPremium
	 */
	public double getSecLowestSilverPremium() {
		return secLowestSilverPremium;
	}
	/**
	 * @param secLowestSilverPremium the secLowestSilverPremium to set
	 */
	public void setSecLowestSilverPremium(double secLowestSilverPremium) {
		this.secLowestSilverPremium = secLowestSilverPremium;
	}
	/**
	 * @return the lowestGoldPremium
	 */
	public double getLowestGoldPremium() {
		return lowestGoldPremium;
	}
	/**
	 * @param lowestGoldPremium the lowestGoldPremium to set
	 */
	public void setLowestGoldPremium(double lowestGoldPremium) {
		this.lowestGoldPremium = lowestGoldPremium;
	}
	public boolean isPremiumFound() {
		return isPremiumFound;
	}
	public void setPremiumFound(boolean isPremiumFound) {
		this.isPremiumFound = isPremiumFound;
	}
	
}
