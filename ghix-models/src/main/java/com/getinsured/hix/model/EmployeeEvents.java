package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employee_events")
public class EmployeeEvents implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_EVENTS_SEQ")
	@SequenceGenerator(name = "EMPLOYEE_EVENTS_SEQ", sequenceName = "EMPLOYEE_EVENTS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="employee_id")
	private Integer employeeId;
	
	@Column(name="event_type")
	private String eventType;
	
	@Column(name="event_code")
	private String eventCode;
	
	@Column(name="special_event_code")
	private String spEventCode;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "event_date")
	private Date eventDate;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;
	
	@Column(name="created_by")
	private Integer createdBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	public String getSpEventCode() {
		return spEventCode;
	}

	public void setSpEventCode(String spEventCode) {
		this.spEventCode = spEventCode;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
	}
}
