package com.getinsured.hix.dto.plandisplay.ind71g;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class IND71GHouseHoldContact {

	@Size(min=1, max=80,message="houseHoldContactId" + IND71Enums.LENGTH_ERROR )
	private String houseHoldContactId;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="houseHoldContactFirstName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="houseHoldContactFirstName" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactFirstName;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="houseHoldContactMiddleName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="houseHoldContactMiddleName" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactMiddleName;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="houseHoldContactLastName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="houseHoldContactLastName" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactLastName;
    
    @Size(min=IND71Enums.SUFFIX_MIN_LENGTH, max=IND71Enums.SUFFIX_MAX_LENGTH,message="houseHoldContactSuffix" + IND71Enums.LENGTH_ERROR )
    private String houseHoldContactSuffix;
    
    @Size(min=IND71Enums.SSN_MIN_LENGTH, max=IND71Enums.SSN_MAX_LENGTH,message="houseHoldContactFederalTaxIdNumber" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.SSN_REGEX,message="responsiblePersonSsn" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactFederalTaxIdNumber;
    
    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="houseHoldContactPrimaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="houseHoldContactPrimaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactPrimaryPhone;
    
    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="houseHoldContactSecondaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="houseHoldContactSecondaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactSecondaryPhone;
    
    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="houseHoldContactPreferredPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="houseHoldContactPreferredPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactPreferredPhone;
    
    @Pattern(regexp=IND71Enums.EMAIL_REGEX,message="houseHoldContactPreferredEmail" + IND71Enums.INVALID_EMAIL_ERROR)
    private String houseHoldContactPreferredEmail;
    
    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="houseHoldContactHomeAddress1" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="houseHoldContactHomeAddress1" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactHomeAddress1;
    
    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="houseHoldContactHomeAddress2" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="houseHoldContactHomeAddress2" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactHomeAddress2;
    
    @Size(min=IND71Enums.CITY_MIN_LENGTH, max=IND71Enums.CITY_MAX_LENGTH,message="houseHoldContactHomeCity" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.CITY_REGEX,message="houseHoldContactHomeCity" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactHomeCity;
    
    @Size(min=IND71Enums.STATE_MIN_LENGTH, max=IND71Enums.STATE_MAX_LENGTH,message="houseHoldContactHomeState" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.STATE_REGEX,message="houseHoldContactHomeState" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactHomeState;
    
    @Size(min=IND71Enums.ZIP_MIN_LENGTH, max=IND71Enums.ZIP_MAX_LENGTH,message="houseHoldContactHomeZip" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ZIP_REGEX,message="houseHoldContactHomeZip" + IND71Enums.INVALID_REGEX_ERROR)
    private String houseHoldContactHomeZip;

    public String getHouseHoldContactId() {
        return houseHoldContactId;
    }

    public void setHouseHoldContactId(String houseHoldContactId) {
        this.houseHoldContactId = houseHoldContactId;
    }

    public String getHouseHoldContactFirstName() {
        return houseHoldContactFirstName;
    }

    public void setHouseHoldContactFirstName(String houseHoldContactFirstName) {
        this.houseHoldContactFirstName = houseHoldContactFirstName;
    }

    public String getHouseHoldContactMiddleName() {
        return houseHoldContactMiddleName;
    }

    public void setHouseHoldContactMiddleName(String houseHoldContactMiddleName) {
        this.houseHoldContactMiddleName = houseHoldContactMiddleName;
    }

    public String getHouseHoldContactLastName() {
        return houseHoldContactLastName;
    }

    public void setHouseHoldContactLastName(String houseHoldContactLastName) {
        this.houseHoldContactLastName = houseHoldContactLastName;
    }

    public String getHouseHoldContactSuffix() {
        return houseHoldContactSuffix;
    }

    public void setHouseHoldContactSuffix(String houseHoldContactSuffix) {
        this.houseHoldContactSuffix = houseHoldContactSuffix;
    }

    public String getHouseHoldContactFederalTaxIdNumber() {
        return houseHoldContactFederalTaxIdNumber;
    }

    public void setHouseHoldContactFederalTaxIdNumber(String houseHoldContactFederalTaxIdNumber) {
        this.houseHoldContactFederalTaxIdNumber = houseHoldContactFederalTaxIdNumber;
    }

    public String getHouseHoldContactPrimaryPhone() {
        return houseHoldContactPrimaryPhone;
    }

    public void setHouseHoldContactPrimaryPhone(String houseHoldContactPrimaryPhone) {
        this.houseHoldContactPrimaryPhone = houseHoldContactPrimaryPhone;
    }

    public String getHouseHoldContactSecondaryPhone() {
        return houseHoldContactSecondaryPhone;
    }

    public void setHouseHoldContactSecondaryPhone(String houseHoldContactSecondaryPhone) {
        this.houseHoldContactSecondaryPhone = houseHoldContactSecondaryPhone;
    }

    public String getHouseHoldContactPreferredPhone() {
        return houseHoldContactPreferredPhone;
    }

    public void setHouseHoldContactPreferredPhone(String houseHoldContactPreferredPhone) {
        this.houseHoldContactPreferredPhone = houseHoldContactPreferredPhone;
    }

    public String getHouseHoldContactPreferredEmail() {
        return houseHoldContactPreferredEmail;
    }

    public void setHouseHoldContactPreferredEmail(String houseHoldContactPreferredEmail) {
        this.houseHoldContactPreferredEmail = houseHoldContactPreferredEmail;
    }

    public String getHouseHoldContactHomeAddress1() {
        return houseHoldContactHomeAddress1;
    }

    public void setHouseHoldContactHomeAddress1(String houseHoldContactHomeAddress1) {
        this.houseHoldContactHomeAddress1 = houseHoldContactHomeAddress1;
    }

    public String getHouseHoldContactHomeAddress2() {
        return houseHoldContactHomeAddress2;
    }

    public void setHouseHoldContactHomeAddress2(String houseHoldContactHomeAddress2) {
        this.houseHoldContactHomeAddress2 = houseHoldContactHomeAddress2;
    }

    public String getHouseHoldContactHomeCity() {
        return houseHoldContactHomeCity;
    }

    public void setHouseHoldContactHomeCity(String houseHoldContactHomeCity) {
        this.houseHoldContactHomeCity = houseHoldContactHomeCity;
    }

    public String getHouseHoldContactHomeState() {
        return houseHoldContactHomeState;
    }

    public void setHouseHoldContactHomeState(String houseHoldContactHomeState) {
        this.houseHoldContactHomeState = houseHoldContactHomeState;
    }

    public String getHouseHoldContactHomeZip() {
        return houseHoldContactHomeZip;
    }

    public void setHouseHoldContactHomeZip(String houseHoldContactHomeZip) {
        this.houseHoldContactHomeZip = houseHoldContactHomeZip;
    }
}
