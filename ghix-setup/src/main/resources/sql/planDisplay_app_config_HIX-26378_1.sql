delete from gi_app_config where PROPERTY_KEY='planSelection.QualitySection';

delete from gi_app_config where PROPERTY_KEY='planSelection.SplitHousehold';

delete from gi_app_config where PROPERTY_KEY='planSelection.Providers';

delete from gi_app_config where PROPERTY_KEY='planSelection.DoctorSection';

delete from gi_app_config where PROPERTY_KEY='planSelection.FacilitySection';

delete from gi_app_config where PROPERTY_KEY='planSelection.DentistSection';

delete from gi_app_config where PROPERTY_KEY='planSelection.QualityRating';

delete from gi_app_config where PROPERTY_KEY='planSelection.ProvidersLink';

delete from gi_app_config where PROPERTY_KEY='planSelection.MultipleItemsEntry';

delete from gi_app_config where PROPERTY_KEY='planSelection.MinPremiumPerMember';

delete from gi_app_config where PROPERTY_KEY='preferences.EstimateCost';

delete from gi_app_config where PROPERTY_KEY='preferences.FindDoctor';

delete from gi_app_config where PROPERTY_KEY='preferences.ShopDental';

delete from gi_app_config where PROPERTY_KEY='preferences.FindDentist';


insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.QualitySection','OFF','Flag to show/hide quality Section',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.SplitHousehold','OFF','Flag to show/hide splithousehold',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.Providers','OFF','Flag to show/hide providers',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.DoctorSection','OFF','Flag to show/hide doctors section',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.FacilitySection','OFF','Flag to show/hide facility section',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.DentistSection','OFF','Flag to show/hide dentists section',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.QualityRating','OFF','Flag to show/hide quality rating',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.ProvidersLink','ON','Flag to show/hide provider link',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.MultipleItemsEntry','ON','Flag to show/hide MultipleItemsEntry',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'planSelection.MinPremiumPerMember','0','Value of minimum amount to be paid per member for a plan',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'preferences.EstimateCost','ON','Flag to show/hide EstimateCost on preferences page',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'preferences.FindDoctor','OFF','Flag to show/hide doctors section on preferences page',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'preferences.ShopDental','OFF','Flag to show/hide shop dental on preferences page',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) values (GI_APP_CONFIG_SEQ.nextval,'preferences.FindDentist','OFF','Flag to show/hide find dentists on preferences page',null,current_timestamp,null,current_timestamp);