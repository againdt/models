package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.FTP_STATUS;

/**
 * The persistent class for the serff_plan_documents_job table.
 * 
 */
@Entity
@Table(name = "serff_plan_documents_job")
public class PlanDocumentsJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERFF_PLAN_DOCUMENTS_JOB_SEQ")
	@SequenceGenerator(name = "SERFF_PLAN_DOCUMENTS_JOB_SEQ", sequenceName = "SERFF_PLAN_DOCUMENTS_JOB_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "DOCUMENT_TYPE")
	private String documentType;

	@Column(name = "DOC_NAME_NEW")
	private String documentNewName;

	@Column(name = "DOC_NAME_OLD")
	private String documentOldName;

	@Column(name = "ERROR_MSG")
	private String errorMessage;

	@Column(name = "HIOS_PRODUCT_ID")
	private String hiosProductId;

	@Column(name = "ISSUER_PLAN_NUMBER")
	private String issuerPlanNumber;

	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;

	@Column(name = "PLAN_ID")
	private String planId;

	@Column(name = "UCM_ID_NEW")
	private String ucmNewId;

	@Column(name = "UCM_ID_OLD")
	private String ucmOldId;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private FTP_STATUS ftpStatus;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNewName() {
		return documentNewName;
	}

	public void setDocumentNewName(String documentNewName) {
		this.documentNewName = documentNewName;
	}

	public String getDocumentOldName() {
		return documentOldName;
	}

	public void setDocumentOldName(String documentOldName) {
		this.documentOldName = documentOldName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getHiosProductId() {
		return hiosProductId;
	}

	public void setHiosProductId(String hiosProductId) {
		this.hiosProductId = hiosProductId;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getUcmNewId() {
		return ucmNewId;
	}

	public void setUcmNewId(String ucmNewId) {
		this.ucmNewId = ucmNewId;
	}

	public String getUcmOldId() {
		return ucmOldId;
	}

	public void setUcmOldId(String ucmOldId) {
		this.ucmOldId = ucmOldId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public FTP_STATUS getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(FTP_STATUS ftpStatus) {
		this.ftpStatus = ftpStatus;
	}
	
}
