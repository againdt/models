package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the specialty database table.
 * 
 */

@Entity
@Table(name="specialty")
public class Specialty implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public enum GroupName{
		 DOCTOR,DENTIST,FACILITY;
	 }
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Specialty_Seq")
	@SequenceGenerator(name = "Specialty_Seq", sequenceName = "specialty_seq", allocationSize = 1)
	private int id;

	@Column(name = "parent_specialty_id")
	private int parentSpecialtyId;

	@Column(name = "name", length = 80)
	private String name;
	
	@Column(name = "code", length = 80)
	private String code;

	@Column(name = "description", length = 500)
	private String description;

	@Column(name = "group_name", length = 255)
	@Enumerated(EnumType.STRING)
	private GroupName groupName;

	@Column(name = "taxonomy_code", length = 50)
	private String taxonomyCode;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "specialty", fetch = FetchType.LAZY)
	private Set<FacilitySpecialityRelation> facilitySpecialRelation = new HashSet<FacilitySpecialityRelation>(0);

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "specialty", fetch = FetchType.LAZY)
	private Set<PhysicianSpecialityRelation> physicianSpecialRelation = new HashSet<PhysicianSpecialityRelation>(0);
	
	@Transient
	private String datasourceRefId;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentSpecialtyId() {
		return parentSpecialtyId;
	}

	public void setParentSpecialtyId(int parentSpecialtyId) {
		this.parentSpecialtyId = parentSpecialtyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public GroupName getGroupName() {
		return groupName;
	}

	public void setGroupName(GroupName groupName) {
		this.groupName = groupName;
	}

	public String getTaxonomyCode() {
		return taxonomyCode;
	}

	public void setTaxonomyCode(String taxonomyCode) {
		this.taxonomyCode = taxonomyCode;
	}

	public String getDatasourceRefId() {
		return datasourceRefId;
	}

	public void setDatasourceRefId(String datasourceRefId) {
		this.datasourceRefId = datasourceRefId;
	}

	public Set<FacilitySpecialityRelation> getFacilitySpecialRelation() {
		return facilitySpecialRelation;
	}

	public void setFacilitySpecialRelation(
			Set<FacilitySpecialityRelation> facilitySpecialRelation) {
		this.facilitySpecialRelation = facilitySpecialRelation;
	}

	public Set<PhysicianSpecialityRelation> getPhysicianSpecialRelation() {
		return physicianSpecialRelation;
	}

	public void setPhysicianSpecialRelation(
			Set<PhysicianSpecialityRelation> physicianSpecialRelation) {
		this.physicianSpecialRelation = physicianSpecialRelation;
	}


}