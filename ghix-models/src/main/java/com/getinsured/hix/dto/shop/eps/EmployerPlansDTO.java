package com.getinsured.hix.dto.shop.eps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class EmployerPlansDTO {
	 private static final String NETWKVALUE = "netwkValue";
	 private static final String REGEX_D = "\\d.*";
	 private static final String OOP_MAX_FLY = "OOP_MAX_FLY";
	 private static final String DEDUCTIBLE_FLY = "DEDUCTIBLE_FLY";
	 private static final String PRIMARY_VISIT  = "PRIMARY_VISIT";
	 
	 private Map<Integer, PlanDTO> plansList = new HashMap<Integer, PlanDTO>();
	 private Map<String, TierDTO> tierList = new HashMap<String, TierDTO>();
	 private Map<Integer, EmployeePlanDTO> employeesPlanData = new HashMap<Integer, EmployeePlanDTO>();
	 public static enum PlanLevel { 
		 BRONZE, SILVER, GOLD, PLATINUM; 
		};
		
 public EmployerPlansDTO(){
	 
	 for(PlanLevel planLevel:PlanLevel.values()){
		 TierDTO tierDTO = new TierDTO();
		 List<Integer> planIds = new ArrayList<Integer>();
		 tierDTO.setPlanIds(planIds);
		 tierDTO.setEmpIds(new HashSet<Integer>());
		 tierDTO.setTierName(planLevel.toString());
		 tierList.put(planLevel.toString().toUpperCase(), tierDTO);
	 }
 }
 
 public PlanDTO getRawPlanDTO(){
	 return new PlanDTO();
 }
 
 public Map<Integer, PlanDTO> getPlansList() {
	return plansList;
}
 
public Map<String, TierDTO> getTierList() {
	return tierList;
}

public Map<Integer, EmployeePlanDTO> getEmployeesPlanData() {
	return employeesPlanData;
}

public void saveRawPlanDTO(PlanDTO planDto) throws Exception{
	 this.validatePlanLevel(planDto);
	 plansList.put(planDto.getPlanId(), planDto);
 }
 
 private void createTierList(PlanDTO planDto){
	 TierDTO tierDTO = tierList.get(planDto.getLevel().toUpperCase());
	 tierDTO.getPlanIds().add(planDto.getPlanId());
 }
 
 private void validatePlanLevel(PlanDTO planDto) throws Exception{
	 if(PlanLevel.valueOf(planDto.getLevel().toUpperCase()) == null){
		 throw new Exception("Invalid Plan Level : " + planDto.getLevel() + " Plan Id: " + planDto.getPlanId());
	 }
 }
 
 	// reference Plans - Map<PlanId,HashMap<EmployeeId,HashMap<ProertyName,Value>>> 
  public void saveEmployerReferencePlans(Map<Integer,HashMap<Integer,HashMap<String,Float>>> referencePlans){
	  
	  List<Integer> invalidPlans = new ArrayList<Integer>();
	  // loop Plans
	  //for (Map.Entry<Integer,HashMap<Integer,HashMap<String,Float>>> entry : referencePlans.entrySet())
	  for(PlanDTO planDTO:plansList.values())
	  {
		  // TODO - check for null value
		  //PlanDTO planDTO =  plansList.get(entry.getKey());
		  HashMap<Integer,HashMap<String,Float>> employeeData =  referencePlans.get(planDTO.getPlanId());
		  
		  if(employeeData == null || employeeData.size() == 0){
			  invalidPlans.add(planDTO.getPlanId());
			  continue;
		  }
		  
		  this.createTierList(planDTO);
		  
		  Float totalPremium = 0f;
		  Float indvPremium = 0f;
		  Float depenPremium = 0f;
		  
		  // loop for all employee in Plan
		 for(Map.Entry<Integer, HashMap<String,Float>> employeeEntry : employeeData.entrySet()){
			 EmployeePlanDTO employeePlanDTO = employeesPlanData.get(employeeEntry.getKey());
			 
			 totalPremium = employeeEntry.getValue().get("totalPremium");
			 indvPremium = employeeEntry.getValue().get("indvPremium");
			 depenPremium = employeeEntry.getValue().get("depenPremium");

			 totalPremium = (totalPremium != null ? totalPremium : 0f);
			 indvPremium = (indvPremium != null ? indvPremium : 0f);
			 depenPremium = (depenPremium != null ? depenPremium : 0f);
			 
			 if(totalPremium == 0f){
				 continue;
			 }
			 
			 if(employeePlanDTO == null){
				 employeePlanDTO = new EmployeePlanDTO();
				 employeePlanDTO.setEmployee_id(employeeEntry.getKey());
				 employeesPlanData.put(employeePlanDTO.getEmployee_id(), employeePlanDTO);
			 }
			 
			 employeePlanDTO.saveAndUpdatePremiumn(totalPremium, indvPremium, depenPremium, PlanLevel.valueOf(planDTO.getLevel()));
			 planDTO.addEmployeesPremiums(totalPremium, indvPremium, depenPremium, employeeEntry.getKey());
		 }
		
		 TierDTO tierDTO = tierList.get(planDTO.getLevel());
		 tierDTO.getNotAvailabilityList().removeAll(planDTO.getEmployeeIds()); 
		 tierDTO.updatePremium(planDTO.getTotalPremium(),planDTO.getIndvPremium());
		 tierDTO.getEmpIds().addAll(planDTO.getEmployeeIds());
	  }
	  
	  for(Integer planId:invalidPlans){
		  plansList.remove(planId);
	  }
	  
	  //Create TierList with only those tier in which plan size is > 0
	  Map<String, TierDTO> newTierList  = new HashMap<String, TierDTO>();
	  for (TierDTO tierOBJ : this.tierList.values()) {
		  if(tierOBJ.getPlanIds().size() > 0){
			  newTierList.put(tierOBJ.getTierName(), tierOBJ);
		  }
	  }
	  this.tierList = newTierList;
  }
}
