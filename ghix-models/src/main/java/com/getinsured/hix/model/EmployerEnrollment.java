package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;


/**
 * The persistent class for the employer enrollments database table.
 * 
 */
@Audited
@Entity
@Table(name = "employer_enrollments")
public class EmployerEnrollment implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Status {
		CART, PENDING, ACTIVE, CANCELLED, TERMINATED, DELETED;
	}
	
	public enum ParticipationMeets { YES, NO; }
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EmployerEnrollment_Seq")
	@SequenceGenerator(name = "EmployerEnrollment_Seq", sequenceName = "employer_enrollments_seq", allocationSize = 1)
	private int id;

	@NotAudited
	@ManyToOne
	@JoinColumn(name = "employer_id")
	private Employer employer;
	
	@Column(name = "employee_contribution")
	private float employeeContribution;
	
	@Column(name = "dependent_contribution")
	private float dependentContribution;
	
	@Column(name = "emp_dental_contribution")
	private int empDentalContribution;
	
	@Column(name = "depe_dental_contribution")
	private int depeDentalContribution;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "open_enrollment_start")
	private Date openEnrollmentStart;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "open_enrollment_end")
	private Date openEnrollmentEnd;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "coverage_start_date")
	private Date coverageDateStart;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "coverage_end_date")
	private Date coverageDateEnd;

	@Column(name = "status", length = 20)
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;
	
	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;

	@Fetch(FetchMode.SELECT)
	@OneToMany(mappedBy="employerEnrollment", fetch=FetchType.LAZY )
	private Set<EmployerEnrollmentItem> employerEnrollmentItems;
	
	@Column(name="TERMINATION_REASON")
    private String terminationReason;
	
	@Column(name="ESIGNATURE")
    private String eSignature;
	
	@Column(name="TERMINATION_DATE")
    private Date terminationDate;
	
	@Column(name = "participation_meets")
	@Enumerated(EnumType.STRING)
	private ParticipationMeets participationMeets;
	
	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="esign_date")
	private Date esignDate;
	
	@NotAudited
	@Column(name="plan_pref")
	private String planPref;

	@NotAudited
	@Column(name = "last_visited")
	private String lastVisited;
	
	@Column(name="QUOTING_ZIP")
    private String quotingZip;
	
	@Column(name="QUOTING_COUNTYCODE")
    private String quotingCountyCode;
	
	public EmployerEnrollment() {
		this.setParticipationMeets(ParticipationMeets.NO);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public Date getOpenEnrollmentStart() {
		return openEnrollmentStart;
	}

	public void setOpenEnrollmentStart(Date openEnrollmentStart) {
		this.openEnrollmentStart = openEnrollmentStart;
	}

	public Date getOpenEnrollmentEnd() {
		return openEnrollmentEnd;
	}

	public void setOpenEnrollmentEnd(Date openEnrollmentEnd) {
		this.openEnrollmentEnd = openEnrollmentEnd;
	}
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public float getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public float getDependentContribution() {
		return dependentContribution;
	}

	public void setDependentContribution(float dependentContribution) {
		this.dependentContribution = dependentContribution;
	}
	
	public int getEmpDentalContribution() {
		return empDentalContribution;
	}

	public void setEmpDentalContribution(int empDentalContribution) {
		this.empDentalContribution = empDentalContribution;
	}

	public int getDepeDentalContribution() {
		return depeDentalContribution;
	}

	public void setDepeDentalContribution(int depeDentalContribution) {
		this.depeDentalContribution = depeDentalContribution;
	}

	public Date getCoverageDateStart() {
		return coverageDateStart;
	}

	public void setCoverageDateStart(Date coverageDateStart) {
		this.coverageDateStart = coverageDateStart;
	}

	public Date getCoverageDateEnd() {
		return coverageDateEnd;
	}

	public void setCoverageDateEnd(Date coverageDateEnd) {
		this.coverageDateEnd = coverageDateEnd;
	}
	
	public ParticipationMeets getParticipationMeets() {
		return participationMeets;
	}

	public void setParticipationMeets(ParticipationMeets participationMeets) {
		this.participationMeets = participationMeets;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
		if(this.getParticipationMeets() == null){
			this.setParticipationMeets(ParticipationMeets.NO);
		}
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
		if(this.getParticipationMeets() == null){
			this.setParticipationMeets(ParticipationMeets.NO);
		}
	}

	public Set<EmployerEnrollmentItem> getEmployerEnrollmentItems() {
		return employerEnrollmentItems;
	}

	public void setEmployerEnrollmentItems(
			Set<EmployerEnrollmentItem> employerEnrollmentItems) {
		this.employerEnrollmentItems = employerEnrollmentItems;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public String geteSignature() {
		return eSignature;
	}

	public void seteSignature(String eSignature) {
		this.eSignature = eSignature;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}

	public String getPlanPref() {
		return planPref;
	}

	public void setPlanPref(String planPref) {
		this.planPref = planPref;
	}

	public String getLastVisited() {
		return lastVisited;
	}

	public void setLastVisited(String lastVisited) {
		this.lastVisited = lastVisited;
	}

	public String getQuotingZip() {
		return quotingZip;
	}

	public void setQuotingZip(String quotingZip) {
		this.quotingZip = quotingZip;
	}

	public String getQuotingCountyCode() {
		return quotingCountyCode;
	}

	public void setQuotingCountyCode(String quotingCountyCode) {
		this.quotingCountyCode = quotingCountyCode;
	}
}
