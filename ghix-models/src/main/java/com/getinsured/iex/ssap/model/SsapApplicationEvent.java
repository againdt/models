package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.timeshift.sql.TSTimestamp;


/**
 * The persistent class for the SSAP_APPLICATION_EVENTS database table.
 * 
 */
@Audited
@Entity
@Table(name="SSAP_APPLICATION_EVENTS")
@NamedQuery(name="SsapApplicationEvent.findAll", query="SELECT s FROM SsapApplicationEvent s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SsapApplicationEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_APPLICATION_EVENTS_ID_GENERATOR", sequenceName="SSAP_APPLICATION_EVENTS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICATION_EVENTS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	
	@Column(name="OVERWRITTEN_COVERAGE_DATE")
	private Timestamp coverageStartDate;
	
	@NotAudited
	@Column(name="COVERAGE_END_DATE")
	private Timestamp coverageEndDate;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;
	
	@NotAudited
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@NotAudited
	@Column(name="KEEP_ONLY")
	private String keepOnly;
	
	@NotAudited
	@Column(name="CHANGE_PLAN")
	private String changePlan = "N";
	
	@NotAudited
	@Column(name="VERIFICATIONS")
	private String verification;
	
	@NotAudited
	@Column(name="ACTION_ENABLED")
	private String actionEnabled;
	
	@NotAudited
	@Column(name="CREATION_TIMESTAMP")
	private Timestamp createdTimeStamp;
	
	@NotAudited
	@Column(name="EVENT_DATE")
	private Timestamp eventDate;
	
	@NotAudited
	@Column(name="REPORT_START_DATE")
	private Timestamp reportStartDate;
	
	@NotAudited
	@Column(name="REPORT_END_DATE")
	private Timestamp reportEndDate;
	
	@Column(name="ENROLLMENT_START_DATE")
	private Timestamp enrollmentStartDate;
	
	@Column(name="ENROLLMENT_END_DATE")
	private Timestamp enrollmentEndDate;
	
	@NotAudited
	@Column(name="EVENT_TYPE")
	@Enumerated(EnumType.STRING)
	private SsapApplicationEventTypeEnum eventType;
	
	
	
	//bi-directional many-to-one association to SsapApplication
	//@ManyToOne(fetch=FetchType.LAZY)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SSAP_APPLICATION_ID", nullable=false)
	//@JsonBackReference
	private SsapApplication ssapApplication;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy="ssapAplicationEvent", cascade = CascadeType.PERSIST)
	private List<SsapApplicantEvent> ssapApplicantEvents;
	
	public SsapApplicationEvent() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVerification() {
		return verification;
	}

	public void setVerification(String verification) {
		this.verification = verification;
	}

	public String getActionEnabled() {
		return actionEnabled;
	}

	public void setActionEnabled(String actionEnabled) {
		this.actionEnabled = actionEnabled;
	}

	public Timestamp getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Timestamp coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Timestamp getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(Timestamp coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Timestamp getEventDate() {
		return eventDate;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public Timestamp getReportStartDate() {
		return reportStartDate;
	}

	public void setReportStartDate(Timestamp reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	public Timestamp getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(Timestamp reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public Timestamp getEnrollmentStartDate() {
		return enrollmentStartDate;
	}

	public void setEnrollmentStartDate(Timestamp enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}

	public Timestamp getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Timestamp enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public SsapApplication getSsapApplication() {
		return ssapApplication;
	}

	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	public SsapApplicationEventTypeEnum getEventType() {
		return eventType;
	}

	public void setEventType(SsapApplicationEventTypeEnum eventType) {
		this.eventType = eventType;
	}

	

	public List<SsapApplicantEvent> getSsapApplicantEvents() {
		return ssapApplicantEvents;
	}

	/**
	 * @see HIX-59175
	 * @return List<SsapApplicantEvent>
	 * 
	 * Non-financial LCE collects multiple events for each applicant and picks one as the preferred event based on their priority logic.
	 * Financial LCE generates only one event per change report.
	 * 
	 * This method returns all applicant events which have the precedence indicator set to Y.
	 * 
	 */
	public List<SsapApplicantEvent> filteredPreferredSsapApplicantEvents() {
		List<SsapApplicantEvent> preferredApplicantEvents = new ArrayList<SsapApplicantEvent>(0);
		if (null != this.ssapApplicantEvents) {
			for (SsapApplicantEvent applicantEvent : this.ssapApplicantEvents) {
				if (applicantEvent.getEventPrecedenceIndicator() != null && "Y".equalsIgnoreCase(applicantEvent.getEventPrecedenceIndicator().name())) {
					preferredApplicantEvents.add(applicantEvent);
				}
			}
		}
		return preferredApplicantEvents;
	}


	public void setSsapApplicantEvents(List<SsapApplicantEvent> ssapApplicantEvents) {
		this.ssapApplicantEvents = ssapApplicantEvents;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}
	
	
	public String getChangePlan() {
		return changePlan;
	}

	public void setChangePlan(String changePlan) {
		this.changePlan = changePlan;
	}


	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		// Add a comment to this line
		this.setCreatedTimeStamp(new TSTimestamp());
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new TSTimestamp());
	}
}
