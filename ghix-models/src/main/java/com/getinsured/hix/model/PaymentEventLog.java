package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * @since 09-Oct-2013
 * @author Sharma_k
 *
 */

@Audited
@Entity
@Table(name = "payment_event_logs")
public class PaymentEventLog  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public enum PaymentStatus{ CONFIRMED, PENDING, FAILED; }
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_event_logs_seq")
	@SequenceGenerator(name = "payment_event_logs_seq", sequenceName = "payment_event_logs_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "transaction_id")
	private String transactionId;
	
	@Column(name = "txn_reason_code")
	private String txnReasonCode;
	
	@Column(name = "txn_status")
	private String txnStatus;
	
	@Column(name = "event_type")
	private String eventType;
	
	@ManyToOne
    @JoinColumn(name="employer_payments_id")
	private EmployerPayments employerPayments;
	
	@ManyToOne
    @JoinColumn(name="isuer_payment_detail_id")
	private IssuerPaymentDetail issuerPaymentDetail;
	
	@ManyToOne
    @JoinColumn(name="issuer_remittance_id")
	private IssuerRemittance issuerRemittance;
	
	@ManyToOne
    @JoinColumn(name="employer_invoices_id")
	private EmployerInvoices employerInvoices;
	
	@Column(name = "ecm_doc_id")
	private String ecmDocId;
	
	@Column(name = "merchant_ref_code")
	private String merchantRefCode;
	
	@Column(name = "request_id")
	private String requestId;
	
	@Column(name="response")
	private String response;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date updatedOn;
	
	@Enumerated(EnumType.STRING)
	@Column(name="payment_status")
	private PaymentStatus paymentStatus;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the txnReasonCode
	 */
	public String getTxnReasonCode() {
		return txnReasonCode;
	}

	/**
	 * @param txnReasonCode the txnReasonCode to set
	 */
	public void setTxnReasonCode(String txnReasonCode) {
		this.txnReasonCode = txnReasonCode;
	}


	/**
	 * @return the txnStatus
	 */
	public String getTxnStatus() {
		return txnStatus;
	}

	/**
	 * @param txnStatus the txnStatus to set
	 */
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the employerPayments
	 */
	public EmployerPayments getEmployerPayments() {
		return employerPayments;
	}

	/**
	 * @param employerPayments the employerPayments to set
	 */
	public void setEmployerPayments(EmployerPayments employerPayments) {
		this.employerPayments = employerPayments;
	}

	/**
	 * @return the issuerPaymentDetail
	 */
	public IssuerPaymentDetail getIssuerPaymentDetail() {
		return issuerPaymentDetail;
	}

	/**
	 * @param issuerPaymentDetail the issuerPaymentDetail to set
	 */
	public void setIssuerPaymentDetail(IssuerPaymentDetail issuerPaymentDetail) {
		this.issuerPaymentDetail = issuerPaymentDetail;
	}

	/**
	 * @return the ecmDocId
	 */
	public String getEcmDocId() {
		return ecmDocId;
	}

	/**
	 * @param ecmDocId the ecmDocId to set
	 */
	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}

	/**
	 * @return the merchantRefCode
	 */
	public String getMerchantRefCode() {
		return merchantRefCode;
	}

	/**
	 * @param merchantRefCode the merchantRefCode to set
	 */
	public void setMerchantRefCode(String merchantRefCode) {
		this.merchantRefCode = merchantRefCode;
	}

	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the issuerRemittance
	 */
	public IssuerRemittance getIssuerRemittance() {
		return issuerRemittance;
	}

	/**
	 * @param issuerRemittance the issuerRemittance to set
	 */
	public void setIssuerRemittance(IssuerRemittance issuerRemittance) {
		this.issuerRemittance = issuerRemittance;
	}

	/**
	 * @return the employerInvoices
	 */
	public EmployerInvoices getEmployerInvoices() {
		return employerInvoices;
	}

	/**
	 * @param employerInvoices the employerInvoices to set
	 */
	public void setEmployerInvoices(EmployerInvoices employerInvoices) {
		this.employerInvoices = employerInvoices;
	}

	/**
	 * @return the paymentStatus
	 */
	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentEventLog [id=" + id + ", transactionId=" + transactionId
				+ ", txnReasonCode=" + txnReasonCode + ", txnStatus="
				+ txnStatus + ", eventType=" + eventType
				+ ", employerPayments=" + employerPayments
				+ ", issuerPaymentDetail=" + issuerPaymentDetail
				+ ", ecmDocId=" + ecmDocId + ", merchantRefCode="
				+ merchantRefCode + ", requestId=" + requestId + "]";
	}
}
