package com.getinsured.hix.dto.cap.consumerapp.mapper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.dto.cap.consumerapp.EnrollmentDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.lookup.repository.ILookupRepository;
import com.getinsured.hix.platform.security.service.UserService;

public class CapEnrollmentMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(CapEnrollmentMapper.class);
	@Autowired UserService userService;
	@Autowired ILookupRepository iLookupRepository;
	
	/**
	 * This method will copy the entity data to dto
	 * @param enrollment
	 * @return
	 */
	public EnrollmentDTO getDtoFromObject(Enrollment enrollment) {
		LOGGER.info("Copying data from Enrollment object to DTO");
		
		EnrollmentDTO dto =  null;
		if(enrollment != null) {
			dto =  new EnrollmentDTO();
			
			dto.setCapAgentId(String.valueOf(enrollment.getCapAgentId()));
			dto.setCapAgentName(this.getCapAgentName(enrollment));
			dto.setCarrierAppId(enrollment.getCarrierAppId());
			dto.setCmrHouseholdId(enrollment.getCmrHouseHoldId());
			dto.setConfirmationNo(enrollment.getConfirmationNumber());
			if(null!=enrollment.getEnrollmentStatusLkp()){
				dto.setEnrollmentStatusLookup(enrollment.getEnrollmentStatusLkp().getLookupValueId());
				dto.setEnrollmentStatusLookupCode(enrollment.getEnrollmentStatusLkp().getLookupValueCode());	
			}
			dto.setExchangeAssignPolicyNo(enrollment.getExchangeAssignPolicyNo());
			if(null!=enrollment.getGrossPremiumAmt()){
			   dto.setGrossPremiumAmount(enrollment.getGrossPremiumAmt());
			}
			if(null!=enrollment.getAptcAmt()){
				   dto.setAptcAmnt(enrollment.getAptcAmt());
			}
			dto.setGroupPolicyNumber(enrollment.getGroupPolicyNumber());
			dto.setId(enrollment.getId());
		
			dto.setIssuerAssignPolicyNo(enrollment.getIssuerAssignPolicyNo());
		
			dto.setIssuerName(enrollment.getInsurerName());
			if(null!=enrollment.getNetPremiumAmt()){
				dto.setNetPremiumAmount(enrollment.getNetPremiumAmt());	
			}
			dto.setSsapApplicationId(enrollment.getSsapApplicationid());
			dto.setStatus(enrollment.getEnrollmentStatusLabel());
			dto.setBenefitEffectiveDate(enrollment.getBenefitEffectiveDate());
			if(null!=enrollment.getPrefferedContactTime()){
				dto.setPreferredContactTime(enrollment.getPrefferedContactTime().getLookupValueLabel());	
			}
			LookupValue insuranceTypeLookup = enrollment.getInsuranceTypeLkp();
			if(null!=insuranceTypeLookup){
				dto.setInsuranceType(enrollment.getInsuranceTypeLkp().getLookupValueLabel());
				dto.setInsuranceTypeLookup(enrollment.getInsuranceTypeLkp().getLookupValueId());	
			}
			/*Issuer issuer = enrollment.getIssuer();
			if(null!=issuer){
				dto.setIssuerId(issuer.getId());
				dto.setIssuerPassword(issuer.getProducerPassword());
				dto.setIssuerUserName(issuer.getProducerUserName());
				dto.setCarrierAppUrl(issuer.getApplicationUrl());
			}*/
			dto.setIssuerId(enrollment.getIssuerId());
			dto.setCarrierAppUrl(enrollment.getCarrierAppUrl());
			
			//TODO get exchange type from enrollment.exchangeType if possible
		}
		
		return dto;
	}

	/**
	 * This method will fetch the user name from account user table.
	 * @param capAgentId
	 * @return
	 */
	private String getCapAgentName(Enrollment enrollment) {
		String capAgentName = StringUtils.EMPTY;
		
		if(enrollment.getCapAgentId() !=null && enrollment.getCapAgentId() > 0) {
			try {
				AccountUser user = userService.findById(enrollment.getCapAgentId());
				capAgentName =  (user != null) ? user.getFullName() : capAgentName;
			} catch (Exception e) {
				LOGGER.error("No assister user object found for: CAP AGENT ID ="+enrollment.getCapAgentId() +" ENROLLMENT ID="+enrollment.getId() + " SSAP APPLICATION ID=" +enrollment.getSsapApplicationid());
			}
		}
		
		return capAgentName;
	}

}
