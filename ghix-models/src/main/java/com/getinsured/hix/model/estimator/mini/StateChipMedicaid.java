/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * This is the model class for STATE_MEDICAID_CHIP table
 * @author Nikhil
 * @since 09 July, 2013
 */
@Entity
@Table(name="STATE_MEDICAID_CHIP")
@DynamicInsert
@DynamicUpdate
public class StateChipMedicaid implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7308559909138436618L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STATE_MEDICAID_CHIP_SEQ")
	@SequenceGenerator(name = "STATE_MEDICAID_CHIP_SEQ", sequenceName = "STATE_MEDICAID_CHIP_SEQ", allocationSize = 1)
	private long id;
	
	@Column(name = "STATE")
	private String state;
	
	@Column(name = "STATE_CODE")
	private String stateCode;
	
	@Column(name = "CHIP_INCOME_LIMIT_0_1")
	private double chipIncomeLimit0to1;
	
	@Column(name = "CHIP_INCOME_LIMIT_1_5")
	private double chipIncomeLimit1to5;
	@Column(name = "CHIP_INCOME_LIMIT_6_18")
	private double chipIncomeLimit6to18;
	@Column(name = "CHIP_INCOME_LIMIT_18_19")
	private double chipIncomeLimit18to19;
	@Column(name = "MEDICAID_INCOME_LIMIT_PARENTS")
	private double medicaidParentThreshold;
	@Column(name = "MEDICAID_INCOME_LIMIT_OTHERS")
	private double medicaidOthersThreshold;
	
	
	@Column(name = "MEDICAID_LIMIT_PREGNANT")
	private double medicaidPregnantThreshold;
	
	
	@Column(name = "SBE_WEBSITE")
	private String sbeWebsite;
	
	@Column(name = "SBE_PHONE")
	private String sbePhone;
	
	@Column(name = "MEDICAID_EXPANSION")
	private String medicaidExpansion;
	
	@Column(name = "PRIMARY_SALES_CHANNEL")
	private String primarySalesChannel;
	
	@Column(name = "SBE_NAME")
	private String sbeName;
	
	@Column(name = "GI_NUMBER")
	private String giNumber;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;




	}

	public String getSbeWebsite() {
		return sbeWebsite;
	}

	public void setSbeWebsite(String sbeWebsite) {
		this.sbeWebsite = sbeWebsite;
	}

	public String getSbePhone() {
		return sbePhone;
	}

	public void setSbePhone(String sbePhone) {
		this.sbePhone = sbePhone;
	}

	public String getMedicaidExpansion() {
		return medicaidExpansion;
	}

	public void setMedicaidExpansion(String medicaidExpansion) {
		this.medicaidExpansion = medicaidExpansion;
	}

	public String getPrimarySalesChannel() {
		return primarySalesChannel;
	}

	public void setPrimarySalesChannel(String primarySalesChannel) {
		this.primarySalesChannel = primarySalesChannel;
	}

	public String getSbeName() {
		return sbeName;
	}

	public void setSbeName(String sbeName) {
		this.sbeName = sbeName;
	}

	public String getGiNumber() {
		return giNumber;
	}

	public void setGiNumber(String giNumber) {
		this.giNumber = giNumber;
	}
	public double getChipIncomeLimit0to1() {
		return chipIncomeLimit0to1;
	}
	public void setChipIncomeLimit0to1(double chipIncomeLimit0to1) {
		this.chipIncomeLimit0to1 = chipIncomeLimit0to1;
	}
	public double getChipIncomeLimit1to5() {
		return chipIncomeLimit1to5;
	}
	public void setChipIncomeLimit1to5(double chipIncomeLimit1to5) {
		this.chipIncomeLimit1to5 = chipIncomeLimit1to5;
	}
	public double getChipIncomeLimit6to18() {
		return chipIncomeLimit6to18;
	}
	public void setChipIncomeLimit6to18(double chipIncomeLimit6to18) {
		this.chipIncomeLimit6to18 = chipIncomeLimit6to18;
	}
	public double getChipIncomeLimit18to19() {
		return chipIncomeLimit18to19;
	}
	public void setChipIncomeLimit18to19(double chipIncomeLimit18to19) {
		this.chipIncomeLimit18to19 = chipIncomeLimit18to19;
	}
	public double getMedicaidParentThreshold() {
		return medicaidParentThreshold;
	}
	public void setMedicaidParentThreshold(double medicaidParentThreshold) {
		this.medicaidParentThreshold = medicaidParentThreshold;
	}
	public double getMedicaidOthersThreshold() {
		return medicaidOthersThreshold;
	}
	public void setMedicaidOthersThreshold(double medicaidOthersThreshold) {
		this.medicaidOthersThreshold = medicaidOthersThreshold;
	}
	public double getMedicaidPregnantThreshold() {
		return medicaidPregnantThreshold;
	}

	public void setMedicaidPregnantThreshold(double medicaidPregnantThreshold) {
		this.medicaidPregnantThreshold = medicaidPregnantThreshold;
	}
}
