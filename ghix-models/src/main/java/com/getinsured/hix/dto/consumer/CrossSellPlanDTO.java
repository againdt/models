package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Kunal Dav
 * @since 20 August, 2014
 */
public class CrossSellPlanDTO implements Serializable, Comparable<CrossSellPlanDTO> {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private Float premium;
	private String encryptedPremiumBeforeCredit;
	private String encryptedPremiumAfterCredit;
	private String issuerLogo;
	private Map<String, Map<String, String>> planCosts;
	private String encodedPremium;
	private Map<String, Map<String, String>> planBenefits;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	public Map<String, Map<String, String>> getPlanCosts() {
		return planCosts;
	}
	public void setPlanCosts(Map<String, Map<String, String>> planCosts) {
		this.planCosts = planCosts;
	}
	
	public String getEncryptedPremiumBeforeCredit() {
		return encryptedPremiumBeforeCredit;
	}
	public void setEncryptedPremiumBeforeCredit(String encryptedPremiumBeforeCredit) {
		this.encryptedPremiumBeforeCredit = encryptedPremiumBeforeCredit;
	}
	public String getEncryptedPremiumAfterCredit() {
		return encryptedPremiumAfterCredit;
	}
	public void setEncryptedPremiumAfterCredit(String encryptedPremiumAfterCredit) {
		this.encryptedPremiumAfterCredit = encryptedPremiumAfterCredit;
	}
	
	@Override
	public int compareTo(CrossSellPlanDTO otherCrossSellPlanDTO) {
		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if (this == otherCrossSellPlanDTO)  return EQUAL;
		if (this.premium < otherCrossSellPlanDTO.premium)   return BEFORE;
		if (this.premium >  otherCrossSellPlanDTO.premium)  return AFTER;

		return EQUAL;
	}
	@Override
	public String toString() {
		return "CrossSellPlanDTO [id=" + id + ", name=" + name + ", premium="
				+ premium + ", issuerLogo=" + issuerLogo + ", planCosts="
				+ planCosts + " , planBenefits="+planBenefits+"]";
	}
	public String getEncodedPremium() {
		return encodedPremium;
	}
	public void setEncodedPremium(String encodedPremium) {
		this.encodedPremium = encodedPremium;
	}
	/**
	 * @return the planBenefits
	 */
	public Map<String, Map<String, String>> getPlanBenefits() {
		return planBenefits;
	}
	/**
	 * @param planBenefits the planBenefits to set
	 */
	public void setPlanBenefits(Map<String, Map<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}
	
}
