
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.enrollment._1095.cy_2018.treasury.irs.common.AcknowledgementStatusCodeType;
import com.getinsured.enrollment._1095.cy_2018.treasury.irs.common.ErrorMessageDetailType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Form 1094A Acknowledgement Status Group Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-08-20&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;Global type for the 1095A Acknowledgement from IRS to Exchange&lt;/DescriptionTxt&gt;
 * 					&lt;DataElementID/&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095AAckStatusGrpType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095AAckStatusGrpType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}AcknowledgementStatusCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}ErrorMessageDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095AAckStatusGrpType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "acknowledgementStatusCd",
    "errorMessageDetail"
})
public class Form1095AAckStatusGrpType {

    @XmlElement(name = "AcknowledgementStatusCd", namespace = "urn:us:gov:treasury:irs:common", required = true)
    @XmlSchemaType(name = "string")
    protected AcknowledgementStatusCodeType acknowledgementStatusCd;
    @XmlElement(name = "ErrorMessageDetail", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected ErrorMessageDetailType errorMessageDetail;

    /**
     * Gets the value of the acknowledgementStatusCd property.
     * 
     * @return
     *     possible object is
     *     {@link AcknowledgementStatusCodeType }
     *     
     */
    public AcknowledgementStatusCodeType getAcknowledgementStatusCd() {
        return acknowledgementStatusCd;
    }

    /**
     * Sets the value of the acknowledgementStatusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcknowledgementStatusCodeType }
     *     
     */
    public void setAcknowledgementStatusCd(AcknowledgementStatusCodeType value) {
        this.acknowledgementStatusCd = value;
    }

    /**
     * Gets the value of the errorMessageDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorMessageDetailType }
     *     
     */
    public ErrorMessageDetailType getErrorMessageDetail() {
        return errorMessageDetail;
    }

    /**
     * Sets the value of the errorMessageDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorMessageDetailType }
     *     
     */
    public void setErrorMessageDetail(ErrorMessageDetailType value) {
        this.errorMessageDetail = value;
    }

}
