DROP function IF EXISTS TRUNCATETABLE_POSTGRES(table_name VARCHAR, statusMsg OUT VARCHAR);

CREATE OR REPLACE FUNCTION TRUNCATETABLE_POSTGRES(table_name VARCHAR)
RETURNS varchar
--RETURNS void
AS $Q$

DECLARE
	statusMsg varchar;
BEGIN
  	execute 'TRUNCATE TABLE ' || table_name;

  	statusMsg := table_name || ' truncated Successfully';
	RAISE INFO 'Procedure TRUNCATETABLE_POSTGRES Ended !!';
	RAISE NOTICE '%', statusMsg;
	RETURN statusMsg;

	EXCEPTION WHEN OTHERS THEN
		BEGIN
			statusMsg := 'Exception while Truncating table : ' || table_name ||', '|| SQLERRM;
			RAISE INFO '%', statusMsg;
			RETURN statusMsg;
		END;
END;
$Q$ 
LANGUAGE plpgsql;