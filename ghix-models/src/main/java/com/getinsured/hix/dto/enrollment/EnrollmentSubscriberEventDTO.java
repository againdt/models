package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrollmentSubscriberEventDTO implements Serializable{
	
	private String eventReasonCode;
	private String eventTypeCode;
	private String eventCreatedOn;
	/**
	 * @return the eventReasonCode
	 */
	public String getEventReasonCode() {
		return eventReasonCode;
	}
	/**
	 * @param eventReasonCode the eventReasonCode to set
	 */
	public void setEventReasonCode(String eventReasonCode) {
		this.eventReasonCode = eventReasonCode;
	}
	/**
	 * @return the eventTypeCode
	 */
	public String getEventTypeCode() {
		return eventTypeCode;
	}
	/**
	 * @param eventTypeCode the eventTypeCode to set
	 */
	public void setEventTypeCode(String eventTypeCode) {
		this.eventTypeCode = eventTypeCode;
	}
	public String getEventCreatedOn() {
		return eventCreatedOn;
	}
	public void setEventCreatedOn(String eventCreatedOn) {
		this.eventCreatedOn = eventCreatedOn;
	}
	
}
