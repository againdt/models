package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 **
 * The persistent class for generating the unique Broker Number for Broker
 * 
 */
@Entity
@Table(name = "BROKER_CERTIFICATION_NUMBER")
public class BrokerCertificationNumber implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BROKER_CERTIFICATION_NO_SEQ")
	@SequenceGenerator(name = "BROKER_CERTIFICATION_NO_SEQ", sequenceName = "BROKER_CERTIFICATION_NO_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "BROKER_ID")
	private Integer brokerId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	@Override
	public String toString() {
		return "BrokerCertificationNumber details: ID = "+id+", BrokerId = "+brokerId;
	}
}
