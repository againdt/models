/**
 * 
 */
package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.ssap.ConsumerApplication;

/**
 * @author Mishra_m
 *
 */
public class ConsumerResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Household household;
	public List<Member> members;
	public List<Prescription> prescriptions;
	private List<HouseholdEnrollment> householdEnrollments;
	private List<ConsumerApplication> ssapApplications;

	private long eligLeadId;

	private String zip;
	
	public Household getHousehold() {
		return household;
	}

	public void setHousehold(Household household) {
		this.household = household;
	}
	
	public List<Member> getMembers(){
		return members;
	}
	
	public void setMembers(List<Member> members){
		 this.members =  members;
	}

	public List<Prescription> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public List<HouseholdEnrollment> getHouseholdEnrollments() {
		return householdEnrollments;
	}

	public void setHouseholdEnrollments(
			List<HouseholdEnrollment> householdEnrollments) {
		this.householdEnrollments = householdEnrollments;
	}
	
	
	public long getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public List<ConsumerApplication> getSsapApplications() {
		return ssapApplications;
	}

	public void setSsapApplications(List<ConsumerApplication> ssapApplications) {
		this.ssapApplications = ssapApplications;
	}

	
}
