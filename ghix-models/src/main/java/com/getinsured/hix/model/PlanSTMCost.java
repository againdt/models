package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "PLAN_STM_COST")
public class PlanSTMCost implements Serializable {

	private static final long serialVersionUID = -5571153719761494144L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "planSTMCost_Seq")
	@SequenceGenerator(name = "planSTMCost_Seq", sequenceName = "PLAN_STM_COST_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_STM_ID")
	private PlanSTM planSTM;

	@Column(name = "NAME")
	private String name;

	@Column(name = "IN_NETWORK_IND")
	private Integer inNetworkInd;

	@Column(name = "FAMILY_DESCRIPTION")
	private String familyDescription;

	@Column(name = "INDV_DESCRIPTION")
	private String individualDescription;

	@Column(name = "SEPARATE_DEDT_PRESCPT_DRUG")
	private String separateDedtPrescptDrug;

	@Column(name = "LAST_UPDATE_BY")
	private Integer lastUpdateBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimeStamp;

	public PlanSTMCost() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setLastUpdateTimeStamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimeStamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanSTM getPlanSTM() {
		return planSTM;
	}

	public void setPlanSTM(PlanSTM planSTM) {
		this.planSTM = planSTM;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getInNetworkInd() {
		return inNetworkInd;
	}

	public void setInNetworkInd(Integer inNetworkInd) {
		this.inNetworkInd = inNetworkInd;
	}

	public String getFamilyDescription() {
		return familyDescription;
	}

	public void setFamilyDescription(String familyDescription) {
		this.familyDescription = familyDescription;
	}

	public String getIndividualDescription() {
		return individualDescription;
	}

	public void setIndividualDescription(String individualDescription) {
		this.individualDescription = individualDescription;
	}

	public String getSeparateDedtPrescptDrug() {
		return separateDedtPrescptDrug;
	}

	public void setSeparateDedtPrescptDrug(String separateDedtPrescptDrug) {
		this.separateDedtPrescptDrug = separateDedtPrescptDrug;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}
}
