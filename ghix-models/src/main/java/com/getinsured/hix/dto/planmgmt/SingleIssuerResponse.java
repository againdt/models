/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.IssuerDocument;
/**
 * This is the response object of issuer service
 * 
 * @author gorai_k
 *
 */
public class SingleIssuerResponse extends GHIXResponse implements Serializable{
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String federalEin;
	private String naicCompanyCode;
	private String naicGroupCode;
	private String hiosIssuerId;
	private String accreditingEntity;
	private String licenseNumber;
	private String licenseStatus;
	private String certificationStatus;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String phoneNumber;
	private String emailAddress;
	private String enrollmentUrl;
	private String contactPerson;
	private String initialPayment;
	private String recurringPayment;
	private String siteUrl;
	private String companyLegalName;
	private String companyLogo;
	private String stateOfDomicile;
	private String companyAddressLine1;
	private String companyAddressLine2;
	private String companyCity;
	private String companyState;
	private String companyZip;
	private String companySiteUrl;
	private String indvCustServicePhone;
	private String indvCustServicePhoneExt;
	private String indvCustServiceTollFree;
	private String indvCustServiceTTY;
	private String indvSiteUrl;
	private String shopCustServicePhone;
	private String shopCustServicePhoneExt;
	private String shopCustServiceTollFree;
	private String shopCustServiceTTY;
	private String shopSiteUrl;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	private String certificationDoc;
	private String shortName;
	private String txn834Version;
	private String txn820Version;
	private Date accreditationExpDate;
	private List<IssuerDocument> issuerDocuments;
	private Integer commentId;
	private String marketingName;
	private Integer lastUpdatedBy;
	private String nationalProducerNumber;
	private String agentFirstName;
	private String agentLastName;
	private String paymentUrl;
	private String onExchangeDisclaimer;
	private String offExchangeDisclaimer;
	private String producerUrl;
	private String producerUserName;
	private String brokerId;
	private String writingAgent;
	private String brokerPhone;
	private String brokerFax;
	private String brokerEmail;
	private String producerPassword;
	private String applicationUrl;
	private String d2C;
	private Date certifiedOn;
	private Date decertifiedOn;

	private Integer brandNameId;
	private String brandName;
	private String isDeleted;
	private String deletedBy;
	private String brandUrl;
	private byte[] orgChart;
	private List<Integer> issuerIdList;
	private byte[] logo;
	
	/**
	 * @return the orgChart
	 */
	public byte[] getOrgChart() {
		return orgChart;
	}

	/**
	 * @param orgChart the orgChart to set
	 */
	public void setOrgChart(byte[] orgChart) {
		this.orgChart = orgChart;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the federalEin
	 */
	public String getFederalEin() {
		return federalEin;
	}

	/**
	 * @param federalEin the federalEin to set
	 */
	public void setFederalEin(String federalEin) {
		this.federalEin = federalEin;
	}

	/**
	 * @return the naicCompanyCode
	 */
	public String getNaicCompanyCode() {
		return naicCompanyCode;
	}

	/**
	 * @param naicCompanyCode the naicCompanyCode to set
	 */
	public void setNaicCompanyCode(String naicCompanyCode) {
		this.naicCompanyCode = naicCompanyCode;
	}

	/**
	 * @return the naicGroupCode
	 */
	public String getNaicGroupCode() {
		return naicGroupCode;
	}

	/**
	 * @param naicGroupCode the naicGroupCode to set
	 */
	public void setNaicGroupCode(String naicGroupCode) {
		this.naicGroupCode = naicGroupCode;
	}

	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	/**
	 * @param hiosIssuerId the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	/**
	 * @return the accreditingEntity
	 */
	public String getAccreditingEntity() {
		return accreditingEntity;
	}

	/**
	 * @param accreditingEntity the accreditingEntity to set
	 */
	public void setAccreditingEntity(String accreditingEntity) {
		this.accreditingEntity = accreditingEntity;
	}

	/**
	 * @return the licenseNumber
	 */
	public String getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * @param licenseNumber the licenseNumber to set
	 */
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the licenseStatus
	 */
	public String getLicenseStatus() {
		return licenseStatus;
	}

	/**
	 * @param licenseStatus the licenseStatus to set
	 */
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}

	/**
	 * @return the certificationStatus
	 */
	public String getCertificationStatus() {
		return certificationStatus;
	}

	/**
	 * @param certificationStatus the certificationStatus to set
	 */
	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the enrollmentUrl
	 */
	public String getEnrollmentUrl() {
		return enrollmentUrl;
	}

	/**
	 * @param enrollmentUrl the enrollmentUrl to set
	 */
	public void setEnrollmentUrl(String enrollmentUrl) {
		this.enrollmentUrl = enrollmentUrl;
	}

	/**
	 * @return the contactPerson
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson the contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return the initialPayment
	 */
	public String getInitialPayment() {
		return initialPayment;
	}

	/**
	 * @param initialPayment the initialPayment to set
	 */
	public void setInitialPayment(String initialPayment) {
		this.initialPayment = initialPayment;
	}

	/**
	 * @return the recurringPayment
	 */
	public String getRecurringPayment() {
		return recurringPayment;
	}

	/**
	 * @param recurringPayment the recurringPayment to set
	 */
	public void setRecurringPayment(String recurringPayment) {
		this.recurringPayment = recurringPayment;
	}

	/**
	 * @return the siteUrl
	 */
	public String getSiteUrl() {
		return siteUrl;
	}

	/**
	 * @param siteUrl the siteUrl to set
	 */
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	/**
	 * @return the companyLegalName
	 */
	public String getCompanyLegalName() {
		return companyLegalName;
	}

	/**
	 * @param companyLegalName the companyLegalName to set
	 */
	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}

	/**
	 * @return the companyLogo
	 */
	public String getCompanyLogo() {
		return companyLogo;
	}

	/**
	 * @param companyLogo the companyLogo to set
	 */
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	/**
	 * @return the stateOfDomicile
	 */
	public String getStateOfDomicile() {
		return stateOfDomicile;
	}

	/**
	 * @param stateOfDomicile the stateOfDomicile to set
	 */
	public void setStateOfDomicile(String stateOfDomicile) {
		this.stateOfDomicile = stateOfDomicile;
	}

	/**
	 * @return the companyAddressLine1
	 */
	public String getCompanyAddressLine1() {
		return companyAddressLine1;
	}

	/**
	 * @param companyAddressLine1 the companyAddressLine1 to set
	 */
	public void setCompanyAddressLine1(String companyAddressLine1) {
		this.companyAddressLine1 = companyAddressLine1;
	}

	/**
	 * @return the companyAddressLine2
	 */
	public String getCompanyAddressLine2() {
		return companyAddressLine2;
	}

	/**
	 * @param companyAddressLine2 the companyAddressLine2 to set
	 */
	public void setCompanyAddressLine2(String companyAddressLine2) {
		this.companyAddressLine2 = companyAddressLine2;
	}

	/**
	 * @return the companyCity
	 */
	public String getCompanyCity() {
		return companyCity;
	}

	/**
	 * @param companyCity the companyCity to set
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	/**
	 * @return the companyState
	 */
	public String getCompanyState() {
		return companyState;
	}

	/**
	 * @param companyState the companyState to set
	 */
	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	/**
	 * @return the companyZip
	 */
	public String getCompanyZip() {
		return companyZip;
	}

	/**
	 * @param companyZip the companyZip to set
	 */
	public void setCompanyZip(String companyZip) {
		this.companyZip = companyZip;
	}

	/**
	 * @return the companySiteUrl
	 */
	public String getCompanySiteUrl() {
		return companySiteUrl;
	}

	/**
	 * @param companySiteUrl the companySiteUrl to set
	 */
	public void setCompanySiteUrl(String companySiteUrl) {
		this.companySiteUrl = companySiteUrl;
	}

	/**
	 * @return the indvCustServicePhone
	 */
	public String getIndvCustServicePhone() {
		return indvCustServicePhone;
	}

	/**
	 * @param indvCustServicePhone the indvCustServicePhone to set
	 */
	public void setIndvCustServicePhone(String indvCustServicePhone) {
		this.indvCustServicePhone = indvCustServicePhone;
	}

	/**
	 * @return the indvCustServicePhoneExt
	 */
	public String getIndvCustServicePhoneExt() {
		return indvCustServicePhoneExt;
	}

	/**
	 * @param indvCustServicePhoneExt the indvCustServicePhoneExt to set
	 */
	public void setIndvCustServicePhoneExt(String indvCustServicePhoneExt) {
		this.indvCustServicePhoneExt = indvCustServicePhoneExt;
	}

	/**
	 * @return the indvCustServiceTollFree
	 */
	public String getIndvCustServiceTollFree() {
		return indvCustServiceTollFree;
	}

	/**
	 * @param indvCustServiceTollFree the indvCustServiceTollFree to set
	 */
	public void setIndvCustServiceTollFree(String indvCustServiceTollFree) {
		this.indvCustServiceTollFree = indvCustServiceTollFree;
	}

	/**
	 * @return the indvCustServiceTTY
	 */
	public String getIndvCustServiceTTY() {
		return indvCustServiceTTY;
	}

	/**
	 * @param indvCustServiceTTY the indvCustServiceTTY to set
	 */
	public void setIndvCustServiceTTY(String indvCustServiceTTY) {
		this.indvCustServiceTTY = indvCustServiceTTY;
	}

	/**
	 * @return the indvSiteUrl
	 */
	public String getIndvSiteUrl() {
		return indvSiteUrl;
	}

	/**
	 * @param indvSiteUrl the indvSiteUrl to set
	 */
	public void setIndvSiteUrl(String indvSiteUrl) {
		this.indvSiteUrl = indvSiteUrl;
	}

	/**
	 * @return the shopCustServicePhone
	 */
	public String getShopCustServicePhone() {
		return shopCustServicePhone;
	}

	/**
	 * @param shopCustServicePhone the shopCustServicePhone to set
	 */
	public void setShopCustServicePhone(String shopCustServicePhone) {
		this.shopCustServicePhone = shopCustServicePhone;
	}

	/**
	 * @return the shopCustServicePhoneExt
	 */
	public String getShopCustServicePhoneExt() {
		return shopCustServicePhoneExt;
	}

	/**
	 * @param shopCustServicePhoneExt the shopCustServicePhoneExt to set
	 */
	public void setShopCustServicePhoneExt(String shopCustServicePhoneExt) {
		this.shopCustServicePhoneExt = shopCustServicePhoneExt;
	}

	/**
	 * @return the shopCustServiceTollFree
	 */
	public String getShopCustServiceTollFree() {
		return shopCustServiceTollFree;
	}

	/**
	 * @param shopCustServiceTollFree the shopCustServiceTollFree to set
	 */
	public void setShopCustServiceTollFree(String shopCustServiceTollFree) {
		this.shopCustServiceTollFree = shopCustServiceTollFree;
	}

	/**
	 * @return the shopCustServiceTTY
	 */
	public String getShopCustServiceTTY() {
		return shopCustServiceTTY;
	}

	/**
	 * @param shopCustServiceTTY the shopCustServiceTTY to set
	 */
	public void setShopCustServiceTTY(String shopCustServiceTTY) {
		this.shopCustServiceTTY = shopCustServiceTTY;
	}

	/**
	 * @return the shopSiteUrl
	 */
	public String getShopSiteUrl() {
		return shopSiteUrl;
	}

	/**
	 * @param shopSiteUrl the shopSiteUrl to set
	 */
	public void setShopSiteUrl(String shopSiteUrl) {
		this.shopSiteUrl = shopSiteUrl;
	}

	/**
	 * @return the effectiveStartDate
	 */
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	/**
	 * @param effectiveStartDate the effectiveStartDate to set
	 */
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	/**
	 * @return the effectiveEndDate
	 */
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	/**
	 * @param effectiveEndDate the effectiveEndDate to set
	 */
	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the lastUpdateTimestamp
	 */
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	/**
	 * @param lastUpdateTimestamp the lastUpdateTimestamp to set
	 */
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	/**
	 * @return the certificationDoc
	 */
	public String getCertificationDoc() {
		return certificationDoc;
	}

	/**
	 * @param certificationDoc the certificationDoc to set
	 */
	public void setCertificationDoc(String certificationDoc) {
		this.certificationDoc = certificationDoc;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the txn834Version
	 */
	public String getTxn834Version() {
		return txn834Version;
	}

	/**
	 * @param txn834Version the txn834Version to set
	 */
	public void setTxn834Version(String txn834Version) {
		this.txn834Version = txn834Version;
	}

	/**
	 * @return the txn820Version
	 */
	public String getTxn820Version() {
		return txn820Version;
	}

	/**
	 * @param txn820Version the txn820Version to set
	 */
	public void setTxn820Version(String txn820Version) {
		this.txn820Version = txn820Version;
	}

	/**
	 * @return the accreditationExpDate
	 */
	public Date getAccreditationExpDate() {
		return accreditationExpDate;
	}

	/**
	 * @param accreditationExpDate the accreditationExpDate to set
	 */
	public void setAccreditationExpDate(Date accreditationExpDate) {
		this.accreditationExpDate = accreditationExpDate;
	}

	/**
	 * @return the issuerDocuments
	 */
	public List<IssuerDocument> getIssuerDocuments() {
		return issuerDocuments;
	}

	/**
	 * @param issuerDocuments the issuerDocuments to set
	 */
	public void setIssuerDocuments(List<IssuerDocument> issuerDocuments) {
		this.issuerDocuments = issuerDocuments;
	}

	/**
	 * @return the commentId
	 */
	public Integer getCommentId() {
		return commentId;
	}

	/**
	 * @param commentId the commentId to set
	 */
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	/**
	 * @return the marketingName
	 */
	public String getMarketingName() {
		return marketingName;
	}

	/**
	 * @param marketingName the marketingName to set
	 */
	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/**
	 * @return the nationalProducerNumber
	 */
	public String getNationalProducerNumber() {
		return nationalProducerNumber;
	}

	/**
	 * @param nationalProducerNumber the nationalProducerNumber to set
	 */
	public void setNationalProducerNumber(String nationalProducerNumber) {
		this.nationalProducerNumber = nationalProducerNumber;
	}

	/**
	 * @return the agentFirstName
	 */
	public String getAgentFirstName() {
		return agentFirstName;
	}

	/**
	 * @param agentFirstName the agentFirstName to set
	 */
	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	/**
	 * @return the agentLastName
	 */
	public String getAgentLastName() {
		return agentLastName;
	}

	/**
	 * @param agentLastName the agentLastName to set
	 */
	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	/**
	 * @return the paymentUrl
	 */
	public String getPaymentUrl() {
		return paymentUrl;
	}

	/**
	 * @param paymentUrl the paymentUrl to set
	 */
	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	/**
	 * @return the onExchangeDisclaimer
	 */
	public String getOnExchangeDisclaimer() {
		return onExchangeDisclaimer;
	}

	/**
	 * @param onExchangeDisclaimer the onExchangeDisclaimer to set
	 */
	public void setOnExchangeDisclaimer(String onExchangeDisclaimer) {
		this.onExchangeDisclaimer = onExchangeDisclaimer;
	}

	/**
	 * @return the offExchangeDisclaimer
	 */
	public String getOffExchangeDisclaimer() {
		return offExchangeDisclaimer;
	}

	/**
	 * @param offExchangeDisclaimer the offExchangeDisclaimer to set
	 */
	public void setOffExchangeDisclaimer(String offExchangeDisclaimer) {
		this.offExchangeDisclaimer = offExchangeDisclaimer;
	}

	/**
	 * @return the producerUrl
	 */
	public String getProducerUrl() {
		return producerUrl;
	}

	/**
	 * @param producerUrl the producerUrl to set
	 */
	public void setProducerUrl(String producerUrl) {
		this.producerUrl = producerUrl;
	}

	/**
	 * @return the producerUserName
	 */
	public String getProducerUserName() {
		return producerUserName;
	}

	/**
	 * @param producerUserName the producerUserName to set
	 */
	public void setProducerUserName(String producerUserName) {
		this.producerUserName = producerUserName;
	}

	/**
	 * @return the brokerId
	 */
	public String getBrokerId() {
		return brokerId;
	}

	/**
	 * @param brokerId the brokerId to set
	 */
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	/**
	 * @return the writingAgent
	 */
	public String getWritingAgent() {
		return writingAgent;
	}

	/**
	 * @param writingAgent the writingAgent to set
	 */
	public void setWritingAgent(String writingAgent) {
		this.writingAgent = writingAgent;
	}

	/**
	 * @return the brokerPhone
	 */
	public String getBrokerPhone() {
		return brokerPhone;
	}

	/**
	 * @param brokerPhone the brokerPhone to set
	 */
	public void setBrokerPhone(String brokerPhone) {
		this.brokerPhone = brokerPhone;
	}

	/**
	 * @return the brokerFax
	 */
	public String getBrokerFax() {
		return brokerFax;
	}

	/**
	 * @param brokerFax the brokerFax to set
	 */
	public void setBrokerFax(String brokerFax) {
		this.brokerFax = brokerFax;
	}

	/**
	 * @return the brokerEmail
	 */
	public String getBrokerEmail() {
		return brokerEmail;
	}

	/**
	 * @param brokerEmail the brokerEmail to set
	 */
	public void setBrokerEmail(String brokerEmail) {
		this.brokerEmail = brokerEmail;
	}

	/**
	 * @return the producerPassword
	 */
	public String getProducerPassword() {
		return producerPassword;
	}

	/**
	 * @param producerPassword the producerPassword to set
	 */
	public void setProducerPassword(String producerPassword) {
		this.producerPassword = producerPassword;
	}

	/**
	 * @return the applicationUrl
	 */
	public String getApplicationUrl() {
		return applicationUrl;
	}

	/**
	 * @param applicationUrl the applicationUrl to set
	 */
	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	/**
	 * @return the d2C
	 */
	public String getD2C() {
		return d2C;
	}

	/**
	 * @param d2c the d2C to set
	 */
	public void setD2C(String d2c) {
		d2C = d2c;
	}

	/**
	 * @return the certifiedOn
	 */
	public Date getCertifiedOn() {
		return certifiedOn;
	}

	/**
	 * @param certifiedOn the certifiedOn to set
	 */
	public void setCertifiedOn(Date certifiedOn) {
		this.certifiedOn = certifiedOn;
	}

	/**
	 * @return the decertifiedOn
	 */
	public Date getDecertifiedOn() {
		return decertifiedOn;
	}

	/**
	 * @param decertifiedOn the decertifiedOn to set
	 */
	public void setDecertifiedOn(Date decertifiedOn) {
		this.decertifiedOn = decertifiedOn;
	}

	/**
	 * @return the brandNameId
	 */
	public Integer getBrandNameId() {
		return brandNameId;
	}

	/**
	 * @param brandNameId the brandNameId to set
	 */
	public void setBrandNameId(Integer brandNameId) {
		this.brandNameId = brandNameId;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the isDeleted
	 */
	public String getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * @return the deletedBy
	 */
	public String getDeletedBy() {
		return deletedBy;
	}

	/**
	 * @param deletedBy the deletedBy to set
	 */
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	/**
	 * @return the brandUrl
	 */
	public String getBrandUrl() {
		return brandUrl;
	}

	/**
	 * @param brandUrl the brandUrl to set
	 */
	public void setBrandUrl(String brandUrl) {
		this.brandUrl = brandUrl;
	}

	public SingleIssuerResponse() {
		
	}

	/**
	 * @return the issuerIdList
	 */
	public List<Integer> getIssuerIdList() {
		return issuerIdList;
	}

	/**
	 * @param issuerIdList the issuerIdList to set
	 */
	public void setIssuerIdList(List<Integer> issuerIdList) {
		this.issuerIdList = issuerIdList;
	}

	/**
	 * @return the logo
	 */
	public byte[] getLogo() {
		return logo;
	}

	/**
	 * @param logo the logo to set
	 */
	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
}
