package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;

@XmlAccessorType(XmlAccessType.NONE)
@Audited
@Entity
@Table(name="ENROLLMENT_PREMIUM")
public class EnrollmentPremium implements Serializable, Comparable<EnrollmentPremium>
{
	private static final long serialVersionUID = 1L;
	public enum APTC_CHANGE_LOGIC{EDITTOOL, PRORATION;}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_PREMIUM_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_PREMIUM_SEQ", sequenceName = "ENROLLMENT_PREMIUM_SEQ", allocationSize = 1)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="ENROLLMENT_ID")
	private Enrollment enrollment;
	
	@Column(name = "MONTH")
	@XmlElement(name="monthNum")
	private Integer month;
	
	@Column(name = "YEAR")
	@XmlElement(name="yearNum")
	private Integer year;
	
	@Column(name = "GROSS_PREM_AMT")
	@XmlElement(name="grossPremiumAmt")
	private Float grossPremiumAmount;
	
	@Column(name = "APTC_AMT")
	@XmlElement(name="aptcAmt")
	private Float aptcAmount;

	@Column(name = "STATE_SUBSIDY_AMT")
	@XmlElement(name="stateSubsidyAmt")
	private BigDecimal stateSubsidyAmount;
	
	@Column(name = "NET_PREM_AMT")
	@XmlElement(name="netPremiumAmt")
	private Float netPremiumAmount;
	
	@Column(name = "SLCSP_EHB_AMT")
	private Float slcspPremiumAmount;
	
	@Column(name = "SLCSP_CMS_PLANID")
	private String slcspPlanid;
	
	@OneToOne
	@JoinColumn(name="LAST_EVENT_ID",nullable= true)
	private EnrollmentEvent lastEventId;
	
	@Column(name = "ACTUAL_GROSS_PREM_AMT")
	private Float actGrossPremiumAmount;
	
	@Column(name = "ACTUAL_APTC_AMT")
	private Float actAptcAmount;

	@Column(name = "ACTUAL_STATE_SUBSIDY_AMT")
	private BigDecimal actStateSubsidyAmount;
	
	@Transient
	@XmlElement(name="monthStartDate")
	private Date monthStartDate;

	@Column(name = "APTC_CHANGE_LOGIC")
	private String aptcChangeLogic;

	@Column(name = "STATE_SUBSIDY_CHANGE_LOGIC")
	private String stateSubsidyChangeLogic;
		
	@Column(name = "MAX_APTC")
	private Float maxAptc;

	@Column(name = "MAX_STATE_SUBSIDY")
	private BigDecimal maxStateSubsidy;
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the enrollment
	 */
	public Enrollment getEnrollment() {
		return enrollment;
	}

	/**
	 * @param enrollment the enrollment to set
	 */
	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the grossPremiumAmount
	 */
	public Float getGrossPremiumAmount() {
		return this.roundFloat(grossPremiumAmount);
	}

	/**
	 * @param grossPremiumAmount the grossPremiumAmount to set
	 */
	public void setGrossPremiumAmount(Float grossPremiumAmount) {
		this.grossPremiumAmount = this.roundFloat(grossPremiumAmount);
	}

	/**
	 * @return the aptcAmount
	 */
	public Float getAptcAmount() {
		return this.roundFloat(aptcAmount);
	}

	/**
	 * @param aptcAmount the aptcAmount to set
	 */
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = this.roundFloat(aptcAmount);
	}

	/**
	 * @return the netPremiumAmount
	 */
	public Float getNetPremiumAmount() {
		return this.roundFloat(netPremiumAmount);
	}

	/**
	 * @param netPremiumAmount the netPremiumAmount to set
	 */
	public void setNetPremiumAmount(Float netPremiumAmount) {
		this.netPremiumAmount = this.roundFloat(netPremiumAmount);
	}

	/**
	 * @return the slcspPremiumAmount
	 */
	public Float getSlcspPremiumAmount() {
		return this.roundFloat(slcspPremiumAmount);
	}

	/**
	 * @param slcspPremiumAmount the slcspPremiumAmount to set
	 */
	public void setSlcspPremiumAmount(Float slcspPremiumAmount) {
		this.slcspPremiumAmount = this.roundFloat(slcspPremiumAmount);
	}

	public String getSlcspPlanid() {
		return slcspPlanid;
	}

	public void setSlcspPlanid(String slcspPlanid) {
		this.slcspPlanid = slcspPlanid;
	}

	/**
	 * @return the lastEventId
	 */
	public EnrollmentEvent getLastEventId() {
		return lastEventId;
	}

	/**
	 * @param lastEventId the lastEventId to set
	 */
	public void setLastEventId(EnrollmentEvent lastEventId) {
		this.lastEventId = lastEventId;
	}	

	/**
	 * @return the actGrossPremiumAmount
	 */
	public Float getActGrossPremiumAmount() {
		return this.roundFloat(actGrossPremiumAmount);
	}

	/**
	 * @param actGrossPremiumAmount the actGrossPremiumAmount to set
	 */
	public void setActGrossPremiumAmount(Float actGrossPremiumAmount) {
		this.actGrossPremiumAmount = this.roundFloat(actGrossPremiumAmount);
	}

	/**
	 * @return the actAptcAmount
	 */
	public Float getActAptcAmount() {
		return this.roundFloat(actAptcAmount);
	}

	/**
	 * @param actAptcAmount the actAptcAmount to set
	 */
	public void setActAptcAmount(Float actAptcAmount) {
		this.actAptcAmount = this.roundFloat(actAptcAmount);
	}

	public Date getMonthStartDate() {
		return monthStartDate;
	}

	public void setMonthStartDate(Date monthStartDate) {
		this.monthStartDate = monthStartDate;
	}
	
	public String getAptcChangeLogic() {
		return aptcChangeLogic;
	}

	public void setAptcChangeLogic(String aptcChangeLogic) {
		this.aptcChangeLogic = aptcChangeLogic;
	}
	
	public Float getMaxAptc() {
		return maxAptc;
	}

	public void setMaxAptc(Float maxAptc) {
		this.maxAptc =this.roundFloat(maxAptc);
	}

	public BigDecimal getStateSubsidyAmount() {
		return stateSubsidyAmount;
	}

	public void setStateSubsidyAmount(BigDecimal stateSubsidyAmount) {
		this.stateSubsidyAmount = stateSubsidyAmount;
	}

	public BigDecimal getActStateSubsidyAmount() {
		return actStateSubsidyAmount;
	}

	public void setActStateSubsidyAmount(BigDecimal actStateSubsidyAmount) {
		this.actStateSubsidyAmount = actStateSubsidyAmount;
	}

	public String getStateSubsidyChangeLogic() {
		return stateSubsidyChangeLogic;
	}

	public void setStateSubsidyChangeLogic(String stateSubsidyChangeLogic) {
		this.stateSubsidyChangeLogic = stateSubsidyChangeLogic;
	}

	public BigDecimal getMaxStateSubsidy() {
		return maxStateSubsidy;
	}

	public void setMaxStateSubsidy(BigDecimal maxStateSubsidy) {
		this.maxStateSubsidy = maxStateSubsidy;
	}

	@Override
	public int compareTo(EnrollmentPremium prem) {
		if(prem!=null){
			return (Integer.compare(Integer.parseInt(prem.year+String.format("%02d", prem.month)), Integer.parseInt(this.year+String.format("%02d", this.month))) );
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
    private Float roundFloat(Float d) {
    	if(d!=null){
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    	}else{
    		return null;
    	}
    }
}
