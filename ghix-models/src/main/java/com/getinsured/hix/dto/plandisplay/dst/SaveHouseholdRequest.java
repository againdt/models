package com.getinsured.hix.dto.plandisplay.dst;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.plandisplay.PlanPremiumDTO;

public class SaveHouseholdRequest{
	private String zipCode;
	private Integer noOfMembers;
	private Integer coverageYear;
	private String coverageStartDate;
	private String currentHiosId;
	private List<PlanPremiumDTO> planPremiumList;
	private List<Map<String,String>> members;
	private String householdId;
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Integer getNoOfMembers() {
		return noOfMembers;
	}
	public void setNoOfMembers(Integer noOfMembers) {
		this.noOfMembers = noOfMembers;
	}
	public Integer getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getCurrentHiosId() {
		return currentHiosId;
	}
	public void setCurrentHiosId(String currentHiosId) {
		this.currentHiosId = currentHiosId;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public List<PlanPremiumDTO> getPlanPremiumList() {
		return planPremiumList;
	}
	public void setPlanPremiumList(List<PlanPremiumDTO> planPremiumList) {
		this.planPremiumList = planPremiumList;
	}
	public List<Map<String, String>> getMembers() {
		return members;
	}
	public void setMembers(List<Map<String, String>> members) {
		this.members = members;
	}
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
}
