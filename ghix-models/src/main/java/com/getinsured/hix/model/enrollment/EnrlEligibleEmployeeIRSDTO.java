package com.getinsured.hix.model.enrollment;

/*
 * @author ajinkya_m
 * Since 27/2/2015
 * 
 */
public class EnrlEligibleEmployeeIRSDTO {

	private Long employeeAppId;
	private String employeeEligibilityStartDate; // Required in MM/dd/yyyy format
	private String firstName;
	private String middleName;
	private String suffixName;
	private String lastName;
	private String SSN;

	public Long getEmployeeAppId() {
		return employeeAppId;
	}

	public void setEmployeeAppId(Long employeeAppId) {
		this.employeeAppId = employeeAppId;
	}

	public String getEmployeeEligibilityStartDate() {
		return employeeEligibilityStartDate;
	}

	public void setEmployeeEligibilityStartDate(
			String employeeEligibilityStartDate) {
		this.employeeEligibilityStartDate = employeeEligibilityStartDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String sSN) {
		SSN = sSN;
	}

	public String getSuffixName() {
		return suffixName;
	}

	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

}
