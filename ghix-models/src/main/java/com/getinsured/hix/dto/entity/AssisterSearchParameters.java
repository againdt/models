package com.getinsured.hix.dto.entity;

import java.io.Serializable;

/**
 * Used for holding Assister Search parameters.
 */
public class AssisterSearchParameters implements Serializable {
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String entityName;
	private String status;
	private String fromDate;
	private String toDate;
	private String certificationStatus;
	private String statusActive;
	private String statusInActive;
	private String assisterNumber;
	

	public String getAssisterNumber() {
		return assisterNumber;
	}

	public void setAssisterNumber(String assisterNumber) {
		this.assisterNumber = assisterNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public String getStatusActive() {
		return statusActive;
	}

	public void setStatusActive(String statusActive) {
		this.statusActive = statusActive;
	}

	public String getStatusInActive() {
		return statusInActive;
	}

	public void setStatusInActive(String statusInActive) {
		this.statusInActive = statusInActive;
	}

	@Override
	public String toString() {
		return "AssisterSearchParameters details: FirstName = "+firstName+", LastName = "+lastName+", EntityName = "+entityName+", " +
				"Status = "+status+", CertificationStatus = "+certificationStatus+", StatusActive = "+statusActive+", StatusInActive = "+statusInActive;
	}
}
