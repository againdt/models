package com.getinsured.hix.model.estimator.mini;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="qualifying_events_lkp")

public class QualifyingEventsLkp implements Serializable {

		private static final long serialVersionUID = -7308559909138436618L;

		@Id
		@Column(name = "EVENT_ID")
		private long id;
		
		@Column(name = "EVENT_NAME" )
		private String eventName;
		
		@Column(name = "DISPLAY_TEXT" )
		private String displayText;
		
		@Column(name = "IMG_PATH" )
		private String imagePath;
		
		@Column(name = "IS_ACTIVE")
		private String isActive;
		
		

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getEventName() {
			return eventName;
		}

		public void setEventName(String eventName) {
			this.eventName = eventName;
		}

		public String getDisplayText() {
			return displayText;
		}

		public void setDisplayText(String displayText) {
			this.displayText = displayText;
		}

		public String getImagePath() {
			return imagePath;
		}

		public void setImagePath(String imagePath) {
			this.imagePath = imagePath;
		}

		public String getIsActive() {
			return isActive;
		}

		public void setIsActive(String isActive) {
			this.isActive = isActive;
		}

		
}
