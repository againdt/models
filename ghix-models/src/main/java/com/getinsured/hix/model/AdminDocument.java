package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity

@Table(name="admin_document")
public class AdminDocument implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static enum DOCUMENT_TYPE{
		QUALITY_RATING,STATE_RATING_DOCUMENT;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AdminDocument_Seq")
	@SequenceGenerator(name = "AdminDocument_Seq", sequenceName = "admin_document_seq", allocationSize = 1)
	private int ID;
	
	@Column(name="DOCUMENT_TYPE", length=50)
	private String documentType;

	@Column(name="DOCUMENT_NAME", length=100)
	private String documentName;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date creationTimestamp;

	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date lastUpdateTimestamp;
	
	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name="USER_ID")
	private int userID;

	@Column(name="COMMENT_ID")
	private int commentID;

	/*	New Field Added for HIX-HIX-14538_1	*/
	@Column(name="FILE_NAME")
	private String fileName;

	@Column(name="FILE_TYPE")
	private String fileType;

	@Column(name="FILE_SIZE")
	private Long fileSize;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		if(fileSize!=null){
			this.fileSize = fileSize;
		}else{
			this.fileSize=0L;
		}
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date createdDate) {
		this.creationTimestamp = createdDate;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updatedDdate) {
		this.lastUpdateTimestamp = updatedDdate;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer updatedBy) {
		this.lastUpdatedBy = updatedBy;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getCommentID() {
		return commentID;
	}

	public void setCommentID(int commentID) {
		this.commentID = commentID;
	}

	/* To AutoUpdate created dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
}
