package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class EnrollmentXMLValidationDTO implements Serializable
{
	public enum XMLValidationStatus{PASS, FAILED};
	private String serverURL;
	private Integer totalValidFileCount;
	private Integer totalInValidFileCount;
	private Integer totalFileCount;
	private String XSDFileName;
	private String validationProcessName;
	private XMLValidationStatus validationStatus;
	private List <EnrollmentXMLValidationDetails> enrollmentXMLValidationDetailsList;
	
	public String getServerURL() {
		return serverURL;
	}
	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}
	/**
	 * @return the totalValidFileCount
	 */
	public Integer getTotalValidFileCount() {
		return totalValidFileCount;
	}
	/**
	 * @param totalValidFileCount the totalValidFileCount to set
	 */
	public void setTotalValidFileCount(Integer totalValidFileCount) {
		this.totalValidFileCount = totalValidFileCount;
	}
	/**
	 * @return the totalInValidFileCount
	 */
	public Integer getTotalInValidFileCount() {
		return totalInValidFileCount;
	}
	/**
	 * @param totalInValidFileCount the totalInValidFileCount to set
	 */
	public void setTotalInValidFileCount(Integer totalInValidFileCount) {
		this.totalInValidFileCount = totalInValidFileCount;
	}
	/**
	 * @return the totalFileCount
	 */
	public Integer getTotalFileCount() {
		return totalFileCount;
	}
	/**
	 * @param totalFileCount the totalFileCount to set
	 */
	public void setTotalFileCount(Integer totalFileCount) {
		this.totalFileCount = totalFileCount;
	}
	/**
	 * @return the xSDFileName
	 */
	public String getXSDFileName() {
		return XSDFileName;
	}
	/**
	 * @param xSDFileName the xSDFileName to set
	 */
	public void setXSDFileName(String xSDFileName) {
		XSDFileName = xSDFileName;
	}
	/**
	 * @return the validationProcessName
	 */
	public String getValidationProcessName() {
		return validationProcessName;
	}
	/**
	 * @param validationProcessName the validationProcessName to set
	 */
	public void setValidationProcessName(String validationProcessName) {
		this.validationProcessName = validationProcessName;
	}
	/**
	 * @return the validationStatus
	 */
	public XMLValidationStatus getValidationStatus() {
		return validationStatus;
	}
	/**
	 * @param validationStatus the validationStatus to set
	 */
	public void setValidationStatus(XMLValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}
	/**
	 * @return the enrollmentXMLValidationDetailsList
	 */
	public List<EnrollmentXMLValidationDetails> getEnrollmentXMLValidationDetailsList() {
		return enrollmentXMLValidationDetailsList;
	}
	/**
	 * @param enrollmentXMLValidationDetailsList the enrollmentXMLValidationDetailsList to set
	 */
	public void setEnrollmentXMLValidationDetailsList(
			List<EnrollmentXMLValidationDetails> enrollmentXMLValidationDetailsList) {
		this.enrollmentXMLValidationDetailsList = enrollmentXMLValidationDetailsList;
	}
}
