/**
 * EmployerResponse.java
 * @author chalse_v
 * @version 1.0
 * @since Mar 22, 2013 
 */
package com.getinsured.hix.dto.planmgmt;

import java.util.List;

/**
 * 
 * @author chalse_v
 * @version 1.0
 * @since Mar 22, 2013 
 *
 */
public class EmployerResponse {

//	private List<Plans> plans;
	
	/**
	 * Constructor.
	 */
	public EmployerResponse() {
	}
	/**
	 * Getter for 'plans'.
	 *
	 * @return the plans.
	 */
//	public List<Plans> getPlans() {
//		return this.plans;
//	}
	/**
	 * Setter for 'plans'.
	 *
	 * @param plans the plans to set.
	 */
//	public void setPlans(List<Plans> plans) {
//		this.plans = plans;
//	}
}
