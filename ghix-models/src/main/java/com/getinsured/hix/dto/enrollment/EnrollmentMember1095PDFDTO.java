package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class EnrollmentMember1095PDFDTO implements Serializable{

	@Expose
	private String name;
	private String ssn;
	private String individualDOB;
	@Expose
	private String coverageStartDate;
	@Expose
	private String coverageTerminationDate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getIndividualDOB() {
		return individualDOB;
	}
	public void setIndividualDOB(String individualDOB) {
		this.individualDOB = individualDOB;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getCoverageTerminationDate() {
		return coverageTerminationDate;
	}
	public void setCoverageTerminationDate(String coverageTerminationDate) {
		this.coverageTerminationDate = coverageTerminationDate;
	}
	
	
}
