package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;

@Entity
@Table(name="ENROLLEE_UPDATE_SEND_STATUS")
public class EnrolleeUpdateSendStatus implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRLL_UPD_SEND_STATUS_SEQ")
	@SequenceGenerator(name = "ENRLL_UPD_SEND_STATUS_SEQ", sequenceName = "ENRLL_UPD_SEND_STATUS_SEQ", allocationSize = 1)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ENROLLEE_ID")
	private Enrollee enrollee;

	@Column(name="EXCHG_INDIV_IDENTIFIER")
	private String exchgIndivIdentifier;

	@OneToOne
	@JoinColumn(name = "ENROLLEE_STAUS")
	private LookupValue  enrolleeStatus;

	@Column(name="AHBX_STATUS_CODE")
	private String  ahbxStatusCode;

	@Column(name="AHBX_STATUS_DESC")
	private String ahbxStatusDesc;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP", nullable=false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP", nullable=false)
	private Date updatedOn;

	@OneToOne
	@JoinColumn(name="CREATED_BY")
	private AccountUser createdBy;

	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Enrollee getEnrollee() {
		return enrollee;
	}

	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}

	public LookupValue getEnrolleeStatus() {
		return enrolleeStatus;
	}

	public void setEnrolleeStatus(LookupValue enrolleeStatus) {
		this.enrolleeStatus = enrolleeStatus;
	}

	public String getAhbxStatusCode() {
		return ahbxStatusCode;
	}

	public void setAhbxStatusCode(String ahbxStatusCode) {
		this.ahbxStatusCode = ahbxStatusCode;
	}

	public String getAhbxStatusDesc() {
		return ahbxStatusDesc;
	}

	public void setAhbxStatusDesc(String ahbxStatusDesc) {
		this.ahbxStatusDesc = ahbxStatusDesc;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}
}
