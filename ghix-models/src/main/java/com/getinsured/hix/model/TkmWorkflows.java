package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author hardas_d
 * This model class is for table tkm_workflows The combination of the ticket category and its type is associated with a workflow definition(WORKFLOW_FILENAME)
 * from the workflow engine (a third-party package)
 */
@Entity
@Table(name="tkm_workflows")
public class TkmWorkflows implements Serializable{

	
	public enum TicketStatus { ACTIVE, INACTIVE };
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TkmWorkflows_Seq")
	@SequenceGenerator(name="TkmWorkflows_Seq", sequenceName="tkm_workflows_seq", allocationSize=1)
	@Column(name="tkm_workflow_id")
	private Integer id;

    @Column(name="ticket_category")
   // @Enumerated(value=EnumType.STRING)
    private String category;

    @NotEmpty
    @Column(name="ticket_type")
    private String type;

    @Column(name="workflow_filename")
    private String filename;
    
    @Column(name="description")
    private String description;

    @Column(name="status")
    @Enumerated(value=EnumType.STRING)
    private TicketStatus status;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "queue_id")
    TkmQueues tkmQueues;
    
    @Column(name="exp_in_days")
    private Integer expDate;
    
    @Column(name="email_config")
    private String emailConfig;
    
    @Column(name="PERMISSION_NAME")
    private String permission;
    
	public String getCategory() {
		return category;
	}

	public String getEmailConfig() {
		return emailConfig;
	}

	public void setEmailConfig(String emailConfig) {
		this.emailConfig = emailConfig;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	@Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="creation_timestamp")
    private Date created;
    
    @Column(name="created_by")
    private Integer creator;

    @Column(name="last_update_timestamp")
    private Date updated;

    @Column(name="last_updated_by")
    private Integer updator;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getUpdator() {
		return updator;
	}

	public void setUpdator(Integer updator) {
		this.updator = updator;
	}
	
	public TkmQueues getTkmQueues() {
		return tkmQueues;
	}

	public void setTkmQueues(TkmQueues tkmQueues) {
		this.tkmQueues = tkmQueues;
	}
	public Integer getExpDate() {
		return expDate;
	}

	public void setExpDate(Integer expDate) {
		this.expDate = expDate;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	@Override
	public String toString() {
		return "TkmWorkflows [id=" + id + ", category=" + category + ", type="
				+ type + ", filename=" + filename + ", description="
				+ description + ", status=" + status + ", created=" + created
				+ ", creator=" + creator + ", updated=" + updated
				+ ", updator=" + updator + "]";
	}
}
