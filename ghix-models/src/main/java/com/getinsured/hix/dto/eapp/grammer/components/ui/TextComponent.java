package com.getinsured.hix.dto.eapp.grammer.components.ui;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.eapp.grammer.UIComponent;
import com.getinsured.hix.dto.eapp.grammer.UIComponentType;
import com.getinsured.hix.dto.eapp.grammer.UIValidationRule;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class TextComponent extends UIComponent {
	
	public TextComponent(){
		super.type = UIComponentType.TEXT;
	}
	
	
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public UIComponent parse(String json) {
		decorate(json);
		return this;
	}
	
	

	@Override
	public String toString() {
		return "TextComponent [type=" + type + "parent=" + super.toString() + "]";
	}


	public static void main(String args[]){
		TextComponent comp = new TextComponent();
		String s=" {\n \"label\": \"First Name\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"width\": 12,\n \"inputWidth\": \"md\"\n }";
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		comp = gson.fromJson(s, TextComponent.class);
		System.out.println(comp);
		
	}





	@Override
	public String format() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}

	private void decorate(String json){
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		TextComponent comp = gson.fromJson(json, TextComponent.class);
		this.entity = comp.entity;
		this.label = comp.label;
		this.model = comp.model;
		this.type = comp.type;
		this.validation = comp.validation;
	}
	

	@Override
	public List<String> compile(String json) {
		decorate(json);
		List<String> errors = new ArrayList<String>();
		try {
			compileSummary(errors, UIValidationRule.REQUIRED, "label", this.label);
			compileSummary(errors, UIValidationRule.REQUIRED, "model", this.model);
			compileSummary(errors, UIValidationRule.REQUIRED, "type", this.type.name());
			
		}catch (Exception e) {
			e.printStackTrace();
			errors.add("Runtime error, contact dev");
		}
		return errors;
	}
	
	

}
