package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.Date;

import com.getinsured.hix.platform.util.DateUtil;

public class SerffTransferInfoDTO {

	private Long serffReqID;
	private String hiosIssuerID;
	private String planNumber;
	private String serffTrackNumber;
	private String stateTrackNumber;
	private String serffRequest;
	private String serffResponse;
	private String startDate;
	private String endDate;
	private String status;
	private String statusDescription;

	public Long getSerffReqID() {
		return serffReqID;
	}
	public void setSerffReqID(Long serffReqID) {
		this.serffReqID = serffReqID;
	}
	public String getHiosIssuerID() {
		return hiosIssuerID;
	}
	public void setHiosIssuerID(String hiosIssuerID) {
		this.hiosIssuerID = hiosIssuerID;
	}
	public String getPlanNumber() {
		return planNumber;
	}
	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}
	public String getSerffTrackNumber() {
		return serffTrackNumber;
	}
	public void setSerffTrackNumber(String serffTrackNumber) {
		this.serffTrackNumber = serffTrackNumber;
	}
	public String getStateTrackNumber() {
		return stateTrackNumber;
	}
	public void setStateTrackNumber(String stateTrackNumber) {
		this.stateTrackNumber = stateTrackNumber;
	}
	public String getSerffRequest() {
		return serffRequest;
	}
	public void setSerffRequest(String serffRequest) {
		this.serffRequest = serffRequest;
	}
	public String getSerffResponse() {
		return serffResponse;
	}
	public void setSerffResponse(String serffResponse) {
		this.serffResponse = serffResponse;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		if (null != startDate) {
			this.startDate = DateUtil.dateToString(startDate, "MMM dd, yyyy");
		}
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		if (null != endDate) {
			this.endDate = DateUtil.dateToString(endDate, "MMM dd, yyyy");
		}
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
