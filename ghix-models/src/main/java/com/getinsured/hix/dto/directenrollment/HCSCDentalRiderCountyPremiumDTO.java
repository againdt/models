package com.getinsured.hix.dto.directenrollment;

import java.math.BigDecimal;

/**
 * 
 * @author Vijay
 *
 */
public class HCSCDentalRiderCountyPremiumDTO {
	private int year;
	private String state;
	private String plan;
	private String displayLabel;
	private String hiosPlanId;
	private String county;
	private int formulaNumber;
	private BigDecimal adultRate;
	private BigDecimal childrenRate;
	private int numOfAdult;
	private int numOfChildren;
	private String totalPremium;
	private String docUrl;
		
	public HCSCDentalRiderCountyPremiumDTO() {
		super();		
	}


	public HCSCDentalRiderCountyPremiumDTO(int year, String state, String plan, String county, 
			int formulaNumber, BigDecimal adultRate, BigDecimal childenRate, int numOfAdult,
			int numOfChildren, String totalPremium, String hiosPlanId, String docUrl) {
		super();
		this.year = year;
		this.state = state;
		this.plan = plan;		
		this.county = county;
		this.formulaNumber = formulaNumber;
		this.adultRate = adultRate;
		this.childrenRate = childenRate;
		this.numOfAdult = numOfAdult;
		this.numOfChildren = numOfChildren;
		this.totalPremium = totalPremium;
		this.hiosPlanId = hiosPlanId;
		this.docUrl = docUrl;
	}
	
	public HCSCDentalRiderCountyPremiumDTO(int year, String state, String plan, String county, 
			int formulaNumber, BigDecimal adultRate, BigDecimal childenRate) {
		super();
		this.year = year;
		this.state = state;
		this.plan = plan;		
		this.county = county;
		this.formulaNumber = formulaNumber;
		this.adultRate = adultRate;
		this.childrenRate = childenRate;		
	}

	@Override
	public String toString() {
		return "HCSCDentalRiderCountyPremiumDTO [year =" + year + ", state=" + state + ", plan="
				+ plan + ", hiosPlanId = "+ hiosPlanId + ", county=" + county + ", formulaNumber = " + formulaNumber
				+ ", adultRate=" + adultRate + ", childrenRate=" 
				+ childrenRate + ", numOfAdult=" + numOfAdult + ", numOfChildren=" + numOfChildren 
				+ ", totalPremium=" + totalPremium + ", docUrl=" + docUrl + "]";
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getPlan() {
		return plan;
	}


	public void setPlan(String plan) {
		this.plan = plan;
	}


	public String getCounty() {
		return county;
	}


	public void setCounty(String county) {
		this.county = county;
	}


	public int getFormulaNumber() {
		return formulaNumber;
	}


	public void setFormulaNumber(int formulaNumber) {
		this.formulaNumber = formulaNumber;
	}


	public BigDecimal getAdultRate() {
		return adultRate;
	}


	public void setAdultRate(BigDecimal adultRate) {
		this.adultRate = adultRate;
	}	

	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public BigDecimal getChildrenRate() {
		return childrenRate;
	}


	public void setChildrenRate(BigDecimal childrenRate) {
		this.childrenRate = childrenRate;
	}


	public int getNumOfAdult() {
		return numOfAdult;
	}


	public void setNumOfAdult(int numOfAdult) {
		this.numOfAdult = numOfAdult;
	}


	public int getNumOfChildren() {
		return numOfChildren;
	}


	public void setNumOfChildren(int numOfChildren) {
		this.numOfChildren = numOfChildren;
	}


	public String getTotalPremium() {
		return totalPremium;
	}


	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}


	public String getHiosPlanId() {
		return hiosPlanId;
	}


	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}


	public String getDocUrl() {
		return docUrl;
	}


	public void setDocUrl(String docUrl) {
		this.docUrl = docUrl;
	}


	public String getDisplayLabel() {
		return displayLabel;
	}


	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}
	
}