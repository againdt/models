package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;

@Audited
@Entity
@Table(name="ENRL_RECON_DISCREPANCY")
public class EnrlReconDiscrepancy  implements Serializable, SortableEntity {
	
	private static final long serialVersionUID = 1L;
	
	public enum DiscrepancyStatus{OPEN, RESOLVED, AUTOFIXED, HOLD};
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_discrepancy_seq")
	@SequenceGenerator(name = "enrl_recon_discrepancy_seq", sequenceName = "ENRL_RECON_DISCREPANCY_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="FILE_ID")
	private Integer fileId;
	
	@Column(name="HIX_ENROLLMENT_ID")
	private Long hixEnrollmentId;
	
	@NotAudited
	@Column(name="ISSUER_ENROLLMENT_ID")
	private Integer issuerEnrollmentId;
	
	@Column(name="ENROLLEE_ID")
	private Integer enrolleeId;
	
	@NotAudited
	@Column(name="MEMBER_ID")
	private Integer memberId;
	
/*	@Column(name="ENRL_RECON_LKP_ID")
	private Integer enrlReconLkpId;*/
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
    @JoinColumn(name="ENRL_RECON_LKP_ID",nullable= false)
	private EnrlReconLkp enrlReconLkpId ;
	
	@NotAudited
	@Column(name="HIX_VALUE")
	private String hixValue;
	
	@NotAudited
	@Column(name="ISSUER_VALUE")
	private String issuerValue;
	
	@Column(name="STATUS")
	@Enumerated(EnumType.STRING)
	private DiscrepancyStatus status;
	
	@Column(name="DATE_RESOLVED")
	private Date dateResolved;
	
	@Column(name="IS_SNOOZE")
	private String isSnooze;
	
	@Column(name="REMARKS")
	private String remarks;
	
	@Column(name="IS_BAD_ADDRESS")
	private String isBadAddress;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	@Column(name="LAST_UPDATE_BY")
	private Integer lastUpdateBy;
	
	@Column(name="FIELD_NAME")
	private String fieldName;
	
	@Column(name="REPORT_SENT_STATUS")
	private String reportSentStatus;
	
	@NotAudited
	@Column(name="AUTOFIX_ERROR")
	private String autoFixError;
	
	@NotAudited
	@Column(name="REPORT_ERROR_MESSAGE")
	private String reportErrorMessage;
	
	@NotAudited
	@Column(name="SNAPSHOT_ID")
	private Integer snapshotId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Long getHixEnrollmentId() {
		return hixEnrollmentId;
	}

	public void setHixEnrollmentId(Long hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}

	public Integer getIssuerEnrollmentId() {
		return issuerEnrollmentId;
	}

	public void setIssuerEnrollmentId(Integer issuerEnrollmentId) {
		this.issuerEnrollmentId = issuerEnrollmentId;
	}

	public Integer getEnrolleeId() {
		return enrolleeId;
	}

	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	/*public Integer getEnrlReconLkpId() {
		return enrlReconLkpId;
	}

	public void setEnrlReconLkpId(Integer enrlReconLkpId) {
		this.enrlReconLkpId = enrlReconLkpId;
	}*/
	
	public EnrlReconLkp getEnrlReconLkpId() {
		return enrlReconLkpId;
	}

	public void setEnrlReconLkpId(EnrlReconLkp enrlReconLkpId) {
		this.enrlReconLkpId = enrlReconLkpId;
	}
	
	public String getHixValue() {
		return hixValue;
	}

	public void setHixValue(String hixValue) {
		this.hixValue = hixValue;
	}

	public String getIssuerValue() {
		return issuerValue;
	}

	public void setIssuerValue(String issuerValue) {
		this.issuerValue = issuerValue;
	}

	public DiscrepancyStatus getStatus() {
		return status;
	}

	public void setStatus(DiscrepancyStatus status) {
		this.status = status;
	}

	public Date getDateResolved() {
		return dateResolved;
	}

	public void setDateResolved(Date dateResolved) {
		this.dateResolved = dateResolved;
	}

	public String getIsSnooze() {
		return isSnooze;
	}

	public void setIsSnooze(String isSnooze) {
		this.isSnooze = isSnooze;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsBadAddress() {
		return isBadAddress;
	}

	public void setIsBadAddress(String isBadAddress) {
		this.isBadAddress = isBadAddress;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate());
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getReportSentStatus() {
		return reportSentStatus;
	}

	public void setReportSentStatus(String reportSentStatus) {
		this.reportSentStatus = reportSentStatus;
	}

	public String getReportErrorMessage() {
		return reportErrorMessage;
	}

	public void setReportErrorMessage(String reportErrorMessage) {
		this.reportErrorMessage = reportErrorMessage;
		if(null != reportErrorMessage && reportErrorMessage.length()>1000){
			this.reportErrorMessage = reportErrorMessage.substring(0, 1000);
		}
	}

	public String getAutoFixError() {
		return autoFixError;
	}

	public void setAutoFixError(String autoFixError) {
		this.autoFixError = autoFixError;
	}

	public Integer getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(Integer snapshotId) {
		this.snapshotId = snapshotId;
	}

	@Override
	public String toString() {
		return "EnrlReconDiscrepancy [id=" + id + ", fileId=" + fileId + ", hixEnrollmentId=" + hixEnrollmentId
				+ ", issuerEnrollmentId=" + issuerEnrollmentId + ", enrolleeId=" + enrolleeId + ", memberId=" + memberId
				+ ", enrlReconLkpId=" + enrlReconLkpId + ", hixValue=" + hixValue + ", issuerValue=" + issuerValue
				+ ", status=" + status + ", dateResolved=" + dateResolved + ", isSnooze=" + isSnooze + ", remarks="
				+ remarks + ", isBadAddress=" + isBadAddress + ", creationTimestamp=" + creationTimestamp
				+ ", lastUpdateTimestamp=" + lastUpdateTimestamp + ", lastUpdateBy=" + lastUpdateBy + ", fieldName="
				+ fieldName + ", reportSentStatus=" + reportSentStatus + ", autoFixError=" + autoFixError
				+ ", reportErrorMessage=" + reportErrorMessage + ", snapshotId=" + snapshotId + "]";
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
        columnNames.add("id");
        columnNames.add("creationTimestamp");
        columnNames.add("fileId");
        columnNames.add("hixEnrollmentId");
        columnNames.add("issuerEnrollmentId");
        columnNames.add("memberId");
        columnNames.add("status");
        columnNames.add("hixValue");
        columnNames.add("issuerValue");
        return columnNames;
	}
}
