package com.getinsured.hix.model.entity;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * The persistent class for table EE_LOCATIONS, to save addresses in Enrollment
 * Entity module.
 * 
 */
@Entity
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "EE_LOCATIONS")
public class EntityLocation implements Serializable, Comparable<EntityLocation> {

	private static final long serialVersionUID = 1L;
	private static final int PRIME = 31;
	
	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_LOCATIONS_SEQ")
	@SequenceGenerator(name = "EE_LOCATIONS_SEQ", sequenceName = "EE_LOCATIONS_SEQ", allocationSize = 1)
	private int id;

	@XmlElement(name = "address1")
	@Column(name = "ADDRESS1")
	private String address1;

	@XmlElement(name = "address2")
	@Column(name = "ADDRESS2")
	private String address2;

	@XmlElement(name = "city")
	@Column(name = "CITY")
	private String city;

	@XmlElement(name = "state")
	@Column(name = "STATE")
	private String state;
	@XmlElement(name = "zip")
	@Column(name = "ZIP")
	private String zip;

	@XmlElement(name = "county")
	@Column(name = "COUNTY")
	private String county;

	@Column(name = "LAT", columnDefinition = "decimal(15,9)")
	private double lat;

	@Column(name = "LON", columnDefinition = "decimal(15,9)")
	private double lon;
	// Residential Delivery Indicator (residential or commercial). R:
	// Residential, C:Commercial, U:Unknown
	@Column(name = "RDI")
	private String rdi;

	@XmlElement(name = "createdOn")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;

	@XmlElement(name = "updatedOn")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;

	public EntityLocation() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getRdi() {
		return rdi;
	}

	public void setRdi(String rdi) {
		this.rdi = rdi;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	@Override
	public String toString() {
		return "EntityLocation details: ID = "+id+", Zip = "+zip;
	}

	@Override
	public int compareTo(EntityLocation obj) {
		if (obj instanceof EntityLocation) {

			EntityLocation loc = obj;
			if (loc != null && checkAddress1(loc) && checkCity(loc) && checkState(loc)) {
				return checkZipCode(loc);
			}
		}
		return -1;
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int THIRTY_TWO = 32;
		int result = 1;
		result = PRIME * result
				+ ((address1 == null) ? 0 : address1.hashCode());
		result = PRIME * result
				+ ((address2 == null) ? 0 : address2.hashCode());
		result = PRIME * result + ((city == null) ? 0 : city.hashCode());
		result = PRIME * result + ((county == null) ? 0 : county.hashCode());
		result = PRIME * result
				+ ((createdOn == null) ? 0 : createdOn.hashCode());
		result = PRIME * result + id;
		long temp;
		temp = Double.doubleToLongBits(lat);
		result = PRIME * result + (int) (temp ^ (temp >>> THIRTY_TWO));
		temp = Double.doubleToLongBits(lon);
		result = PRIME * result + (int) (temp ^ (temp >>> THIRTY_TWO));
		result = PRIME * result + ((rdi == null) ? 0 : rdi.hashCode());
		result = PRIME * result + ((state == null) ? 0 : state.hashCode());
		result = PRIME * result
				+ ((updatedOn == null) ? 0 : updatedOn.hashCode());
		result = PRIME * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof EntityLocation)) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		EntityLocation other = (EntityLocation) obj;
		
		return checkEntityLocationData(other);
	}

	/**
	 * Method to check current object's attribute Entity Location data equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityLocationData(EntityLocation other) {
		return(checkAddressData(other) && checkCreatedUpdateDates(other) && checkId(other) && checkRdi(other));
	}

	/**
	 * Method to check current object's created and updated dates equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreatedUpdateDates(EntityLocation other) {
		return(checkCreatedOn(other) && checkUpdatedOn(other));
	}

	/**
	 * Method to check current object's address data equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkAddressData(EntityLocation other) {
		return (checkAddressPart(other) && checkCounty(other) && checkState(other) && checkLocationData(other));
	}

	/**
	 * Method to check current object's address part equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkAddressPart(EntityLocation other) {
		return (checkAddress1(other) && checkAddress2(other) && checkCity(other));
	}

	/**
	 * Method to check current object's location data equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLocationData(EntityLocation other) {
		return (0 == checkZipCode(other) && checkLat(other) && checkLon(other));
	}
	
	/**
	 * Method to check current object's attribute Address2 equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkAddress2(EntityLocation other) {
		if (address2 == null) {
			if (other.address2 != null) {
				return false;
			}
		} else if (!address2.equals(other.address2)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute county equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCounty(EntityLocation other) {
		if (county == null) {
			if (other.county != null) {
				return false;
			}
		} else if (!county.equals(other.county)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute createdOn equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreatedOn(EntityLocation other) {
		if (createdOn == null) {
			if (other.createdOn != null) {
				return false;
			}
		} else if (!createdOn.equals(other.createdOn)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute id equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkId(EntityLocation other) {
		if (id != other.id) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute lat equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLat(EntityLocation other) {
		if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat)) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method to check current object's attribute lon equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLon(EntityLocation other) {
		if (Double.doubleToLongBits(lon) != Double.doubleToLongBits(other.lon)) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method to check current object's attribute RDI equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkRdi(EntityLocation other) {
		if (rdi == null) {
			if (other.rdi != null) {
				return false;
			}
		} else if (!rdi.equals(other.rdi)) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method to check current object's attribute UpdatedOn equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdatedOn(EntityLocation other) {
		if (updatedOn == null) {
			if (other.updatedOn != null) {
				return false;
			}
		} else if (!updatedOn.equals(other.updatedOn)) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method to check current object's attribute Zip equality with given object.
	 * 
	 * @param loc The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private int checkZipCode(EntityLocation loc) {
		if (null == loc || null == this.getZip() || null == loc.zip) {
			return -1;
		} 
		
		if(this.getZip().equals(loc.getZip())) {
			return 0;
		}
		return -1;
	}

	/**
	 * Method to check current object's attribute State equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkState(EntityLocation other) {
		if (state == null) {
			if (other.state != null) {
				return false;
			}
		} else if (!state.trim().equalsIgnoreCase(other.state.trim())) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute City equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCity(EntityLocation other) {
		if (city == null) {
			if (other.city != null) {
				return false;
			}
		} else if (!city.trim().equalsIgnoreCase(other.city.trim())) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute Address1 equality with given object.
	 * 
	 * @param other The other EntityLocation instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkAddress1(EntityLocation other) {
		if (address1 == null) {
			if (other.address1 != null) {
				return false;
			}
		} else if (!address1.trim().equalsIgnoreCase(other.address1.trim())) {
			return false;
		}
		return true;
	}
}
