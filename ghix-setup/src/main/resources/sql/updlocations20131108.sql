merge into locations new_e using 
 ( 
   SELECT distinct e.*, zzz.zipcode, zzz.lattitude as latti, zzz.longitude as longi 
   FROM locations e, zipcodes zzz 
   WHERE e.zip = 
     (SELECT distinct zip FROM zipcodes z WHERE z.zipcode = e.zip 
     ) 
   AND e.lattitude = 0 
   AND e.longitude = 0 
   AND e.zip IS NOT NULL 
   and e.zip = zzz.zipcode 
   order by e.id 
 ) old_e on (new_e.id = old_e.id) 
 when matched then 
    update set new_e.lattitude = old_e.latti, new_e.longitude = old_e.longi; 
