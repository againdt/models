package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.List;

public class AffiliateInfoDTO implements Serializable {	
	private static final long serialVersionUID = -8057860569227277600L;
	private Integer affiliateId;
	private String  affiliateName;
	private Integer affiliateFlowId;
	private String affiliateFlowName;
	private String affiliateURL;
	private String affiliateFactSheetURL;
	private String affiliateGreeting;
	private String reasonForCall;
	private String nextStep;
	private List<String> availableProducts; // getAvailableAncillaries() -> List<String>

	private String hasEmployer;
	
	private AffiliateFlowDTO affiliateFlowDTO;
	/*
	 * all private, add getters/setters
	 */
	public Integer getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(Integer affiliateId) {
		this.affiliateId = affiliateId;
	}
	public String getAffiliateName() {
		return affiliateName;
	}
	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}
	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}
	public String getAffiliateFlowName() {
		return affiliateFlowName;
	}
	public void setAffiliateFlowName(String affiliateFlowName) {
		this.affiliateFlowName = affiliateFlowName;
	}
	public String getAffiliateURL() {
		return affiliateURL;
	}
	public void setAffiliateURL(String affiliateURL) {
		this.affiliateURL = affiliateURL;
	}
	public String getAffiliateFactSheetURL() {
		return affiliateFactSheetURL;
	}
	public void setAffiliateFactSheetURL(String affiliateFactSheetURL) {
		this.affiliateFactSheetURL = affiliateFactSheetURL;
	}
	public String getAffiliateGreeting() {
		return affiliateGreeting;
	}
	public void setAffiliateGreeting(String affiliateGreeting) {
		this.affiliateGreeting = affiliateGreeting;
	}
	public String getReasonForCall() {
		return reasonForCall;
	}
	public void setReasonForCall(String reasonForCall) {
		this.reasonForCall = reasonForCall;
	}
	public String getNextStep() {
		return nextStep;
	}
	public void setNextStep(String nextStep) {
		this.nextStep = nextStep;
	}
	public List<String> getAvailableProducts() {
		return availableProducts;
	}
	public void setAvailableProducts(List<String> availableProducts) {
		this.availableProducts = availableProducts;
	}
	
	public String getHasEmployer() {
		return hasEmployer;
	}
	
	public void setHasEmployer(String hasEmployer) {
		this.hasEmployer = hasEmployer;
	}
	
	public AffiliateFlowDTO getAffiliateFlowDTO() {
		return affiliateFlowDTO;
	}
	
	public void setAffiliateFlowDTO(AffiliateFlowDTO affiliateFlowDTO) {
		this.affiliateFlowDTO = affiliateFlowDTO;
	}
}
