package com.getinsured.hix.dto.crm;

import java.io.Serializable;

/**
 * DTO for displaying manual enrollment data 
 * on CRM screen
 * @author root
 *
 */
public class ManualEnrollment implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer enrollmentId;
	private String insuranceCompany;
	private String metalTier;
	private String planName;
	private Float premium;
	private String effectiveDate;
	private String exchangeType;
	private Float aptc;
	private String confirmationNumber;
	private Float gross;
	private String policyNumber;
	//if this enrollment is manual or automatic
	private boolean manualModality;
	private String enrollmentStatus;
	/**
	 * This value needs to be set to &nbsp;
	 * to avoid the ui alignment from breaking
	 */
	private String htmlPolicyNumber;
	private String htmlConfirmationNumber;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	public String getMetalTier() {
		return metalTier;
	}
	public void setMetalTier(String metalTier) {
		this.metalTier = metalTier;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public Float getAptc() {
		return aptc;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public Float getGross() {
		return gross;
	}
	public void setGross(Float gross) {
		this.gross = gross;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getHtmlPolicyNumber() {
		return htmlPolicyNumber;
	}
	public void setHtmlPolicyNumber(String htmlPolicyNumber) {
		this.htmlPolicyNumber = htmlPolicyNumber;
	}
	public String getHtmlConfirmationNumber() {
		return htmlConfirmationNumber;
	}
	public void setHtmlConfirmationNumber(String htmlConfirmationNumber) {
		this.htmlConfirmationNumber = htmlConfirmationNumber;
	}
	public boolean isManualModality() {
		return manualModality;
	}
	public void setManualModality(boolean manualModality) {
		this.manualModality = manualModality;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	
	
}
