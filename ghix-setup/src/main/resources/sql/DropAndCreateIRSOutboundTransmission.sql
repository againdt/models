  DROP TABLE "IRS_OUTBOUND_TRANSMISSION";
  
  CREATE TABLE "IRS_OUTBOUND_TRANSMISSION" 
   (	"ID" NUMBER, 
	"DOCUMENT_FILE_NAME" VARCHAR2(400 BYTE), 
	"HOUSEHOLD_CASE_ID" CLOB, 
	"BATCH_ID" VARCHAR2(400 BYTE), 
	"SUBMISSION_TYPE" CHAR(1), 
	"MONTH" VARCHAR2(50 BYTE), 
	"YEAR" VARCHAR2(50 BYTE)
   ) ;
    COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.ID
  IS 'primary key';
    COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.DOCUMENT_FILE_NAME
  IS
    'generated to store document file name in IRS Outbound file package';
    COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.HOUSEHOLD_CASE_ID
  IS
    'generated to store HOUSHOLDCASEID that was in outbound IRS Content File';
    COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.BATCH_ID
  IS
    'generated to store BATCHID that was generated in current timestamp format while packaging IRS outbound files';
     COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.SUBMISSION_TYPE
  IS
    'generated to store Submission Type for IRS Files whether it is Initial or Resubmission Values can be I or R';
     COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.MONTH
  IS
    'generated to store month for which IRS file was generated';
    COMMENT ON COLUMN IRS_OUTBOUND_TRANSMISSION.YEAR  
    IS 
   'generated to store Year for which IRS file was generated';
