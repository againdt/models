package com.getinsured.hix.model.emx;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.emx.EmployerSessionData;

 @Component
//@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class EmployerSessionUtil implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private EmployerSessionData sessionData = new EmployerSessionData();

	public EmployerSessionData getSessionData() {
		return sessionData;
	}

	public void setSessionData(EmployerSessionData sessionData) {
		this.sessionData = sessionData;
	}
	

}
