package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class ExternalEnrollmentDTO  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String planType ;
	private String enrollmentId;
	private String planId;
	
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
		
}
