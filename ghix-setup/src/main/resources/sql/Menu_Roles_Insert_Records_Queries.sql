insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) ='admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans'),(select id from roles where lower(name) ='admin' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='admin' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Leads'),(select id from roles where lower(name) ='admin' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Users'),(select id from roles where lower(name) ='admin' ),4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Affiliates'),(select id from roles where lower(name) ='admin' ),5,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Application Configuration'),(select id from roles where lower(name) ='admin' ),6,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) ='admin' ),7,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Commission'),(select id from roles where lower(name) ='admin' ),8,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin Config'),(select id from roles where lower(name) ='admin' ),9,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Payroll'),(select id from roles where lower(name) ='admin' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='PnL Reports'),(select id from roles where lower(name) ='admin' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Reports'),(select id from roles where lower(name) ='admin' ),12,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agents'),(select id from roles where lower(name) ='admin' ),13,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tenant Configuration'),(select id from roles where lower(name) ='admin' ),14,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) ='admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add New Issuer'),(select id from roles where lower(name) ='admin' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View Medicare Issuer Accounts'),(select id from roles where lower(name) ='admin' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View QHPs'),(select id from roles where lower(name) ='admin' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='New QHP Application'),(select id from roles where lower(name) ='admin' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View SADPs'),(select id from roles where lower(name) ='admin' ),12,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='New SADP Application'),(select id from roles where lower(name) ='admin' ),13,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add Plans'),(select id from roles where lower(name) ='admin' ),14,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plan Status'),(select id from roles where lower(name) ='admin' ),15,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans Util'),(select id from roles where lower(name) ='admin' ),16,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans PDF Util'),(select id from roles where lower(name) ='admin' ),17,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans PDF Status'),(select id from roles where lower(name) ='admin' ),18,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plan Reports'),(select id from roles where lower(name) ='admin' ),19,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View STMs'),(select id from roles where lower(name) ='admin' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View AMEs'),(select id from roles where lower(name) ='admin' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View VISIONs'),(select id from roles where lower(name) ='admin' ),22,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View Medicares'),(select id from roles where lower(name) ='admin' ),23,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Ancillaries'),(select id from roles where lower(name) ='admin' ),24,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create New Consumer'),(select id from roles where lower(name) ='admin' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='admin' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='DNC Tool'),(select id from roles where lower(name) ='admin' ),22,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Lead for O/C'),(select id from roles where lower(name) ='admin' ),23,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Leads'),(select id from roles where lower(name) ='admin' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add User'),(select id from roles where lower(name) ='admin' ),40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Users'),(select id from roles where lower(name) ='admin' ),41,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Teams'),(select id from roles where lower(name) ='admin' ),42,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add Affiliate  '),(select id from roles where lower(name) ='admin' ),50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Affiliate'),(select id from roles where lower(name) ='admin' ),51,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Flow'),(select id from roles where lower(name) ='admin' ),52,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Campaign'),(select id from roles where lower(name) ='admin' ),53,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Census Files'),(select id from roles where lower(name) ='admin' ),54,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Global Config  '),(select id from roles where lower(name) ='admin' ),60,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='UI Config '),(select id from roles where lower(name) ='admin' ),61,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='PHIX Flow Config '),(select id from roles where lower(name) ='admin' ),62,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Shop Config '),(select id from roles where lower(name) ='admin' ),63,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Enrollment Config '),(select id from roles where lower(name) ='admin' ),64,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plan Selection Config '),(select id from roles where lower(name) ='admin' ),65,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plan Management Config '),(select id from roles where lower(name) ='admin' ),66,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Financial Management Config '),(select id from roles where lower(name) ='admin' ),67,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Serff Config '),(select id from roles where lower(name) ='admin' ),68,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Security Config '),(select id from roles where lower(name) ='admin' ),69,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Cap Config '),(select id from roles where lower(name) ='admin' ),70,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='FFM Config '),(select id from roles where lower(name) ='admin' ),71,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Provision Config  '),(select id from roles where lower(name) ='admin' ),72,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='STARR Config'),(select id from roles where lower(name) ='admin' ),73,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) ='admin' ),70,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) ='admin' ),71,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Workgroup'),(select id from roles where lower(name) ='admin' ),72,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Commission'),(select id from roles where lower(name) ='admin' ),80,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='New Commission'),(select id from roles where lower(name) ='admin' ),81,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin'),(select id from roles where lower(name) ='admin' ),90,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Feed Config'),(select id from roles where lower(name) ='admin' ),91,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Feed Summary'),(select id from roles where lower(name) ='admin' ),92,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View Exceptions'),(select id from roles where lower(name) ='admin' ),93,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Commission Backlog'),(select id from roles where lower(name) ='admin' ),94,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Partner Payout'),(select id from roles where lower(name) ='admin' ),95,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agent Variable Pay'),(select id from roles where lower(name) ='admin' ),100,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='PnL Report - By Exchange Type'),(select id from roles where lower(name) ='admin' ),110,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='PnL Report - By States'),(select id from roles where lower(name) ='admin' ),111,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Count of Ticket vs Priority'),(select id from roles where lower(name) ='admin' ),120,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Count of ticket vs type'),(select id from roles where lower(name) ='admin' ),121,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Aging Report'),(select id from roles where lower(name) ='admin' ),122,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Pending Report'),(select id from roles where lower(name) ='admin' ),123,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Agents'),(select id from roles where lower(name) ='admin' ),130,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Branding'),(select id from roles where lower(name) ='admin' ),140,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Global Config'),(select id from roles where lower(name) ='admin' ),141,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP); 
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='csr' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Leads'),(select id from roles where lower(name) ='csr' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) ='csr' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Payroll'),(select id from roles where lower(name) ='csr' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='csr' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='DNC Tool'),(select id from roles where lower(name) ='csr' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='My Leads'),(select id from roles where lower(name) ='csr' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) ='csr' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) ='csr' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agent Variable Pay'),(select id from roles where lower(name) ='csr' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) ='issuer_admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans'),(select id from roles where lower(name) ='issuer_admin' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add New Issuer'),(select id from roles where lower(name) ='issuer_admin' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage QHPs'),(select id from roles where lower(name) ='issuer_admin' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage SADPs'),(select id from roles where lower(name) ='issuer_admin' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add Plans'),(select id from roles where lower(name) ='issuer_admin' ),12,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plan Status'),(select id from roles where lower(name) ='issuer_admin' ),13,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans Util'),(select id from roles where lower(name) ='issuer_admin' ),14,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans PDF Util'),(select id from roles where lower(name) ='issuer_admin' ),15,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans PDF Status'),(select id from roles where lower(name) ='issuer_admin' ),16,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View STMs'),(select id from roles where lower(name) ='issuer_admin' ),18,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View AMEs'),(select id from roles where lower(name) ='issuer_admin' ),19,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers Home'),(select id from roles where lower(name) ='issuer_representative' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans'),(select id from roles where lower(name) ='issuer_representative' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuer Account'),(select id from roles where lower(name) ='issuer_representative' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuer Home'),(select id from roles where lower(name) ='issuer_representative' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage QHPs'),(select id from roles where lower(name) ='issuer_representative' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage SADPs'),(select id from roles where lower(name) ='issuer_representative' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Authorized Representatives'),(select id from roles where lower(name) ='issuer_representative' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuer Profile'),(select id from roles where lower(name) ='issuer_representative' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) ='reconciliation_admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans'),(select id from roles where lower(name) ='reconciliation_admin' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='reconciliation_admin' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin Config'),(select id from roles where lower(name) ='reconciliation_admin' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) ='reconciliation_admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View QHPs'),(select id from roles where lower(name) ='reconciliation_admin' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View SADPs'),(select id from roles where lower(name) ='reconciliation_admin' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='reconciliation_admin' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin'),(select id from roles where lower(name) ='reconciliation_admin' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Feed Config'),(select id from roles where lower(name) ='reconciliation_admin' ),31,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Feed Summary'),(select id from roles where lower(name) ='reconciliation_admin' ),32,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View Exceptions'),(select id from roles where lower(name) ='reconciliation_admin' ),33,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Commission Backlog'),(select id from roles where lower(name) ='reconciliation_admin' ),34,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='sales_manager' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Leads'),(select id from roles where lower(name) ='sales_manager' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Users'),(select id from roles where lower(name) ='sales_manager' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) ='sales_manager' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin Config'),(select id from roles where lower(name) ='sales_manager' ),4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Payroll'),(select id from roles where lower(name) ='sales_manager' ),5,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agents'),(select id from roles where lower(name) ='sales_manager' ),6,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create New Consumer'),(select id from roles where lower(name) ='sales_manager' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='sales_manager' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='DNC Tool'),(select id from roles where lower(name) ='sales_manager' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Leads'),(select id from roles where lower(name) ='sales_manager' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add User'),(select id from roles where lower(name) ='sales_manager' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Teams'),(select id from roles where lower(name) ='sales_manager' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) ='sales_manager' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) ='sales_manager' ),31,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Workgroup'),(select id from roles where lower(name) ='sales_manager' ),32,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin'),(select id from roles where lower(name) ='sales_manager' ),40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agent Variable Pay'),(select id from roles where lower(name) ='sales_manager' ),50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Agents'),(select id from roles where lower(name) ='sales_manager' ),60,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) ='sales_operations' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans'),(select id from roles where lower(name) ='sales_operations' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='sales_operations' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Leads'),(select id from roles where lower(name) ='sales_operations' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Users'),(select id from roles where lower(name) ='sales_operations' ),4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) ='sales_operations' ),5,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin Config'),(select id from roles where lower(name) ='sales_operations' ),6,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agents'),(select id from roles where lower(name) ='sales_operations' ),7,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) ='sales_operations' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View QHPs'),(select id from roles where lower(name) ='sales_operations' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View SADPs'),(select id from roles where lower(name) ='sales_operations' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create New Consumer'),(select id from roles where lower(name) ='sales_operations' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='sales_operations' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Leads'),(select id from roles where lower(name) ='sales_operations' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add User'),(select id from roles where lower(name) ='sales_operations' ),40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Users'),(select id from roles where lower(name) ='sales_operations' ),41,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Teams'),(select id from roles where lower(name) ='sales_operations' ),42,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) ='sales_operations' ),50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) ='sales_operations' ),51,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Workgroup'),(select id from roles where lower(name) ='sales_operations' ),52,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin'),(select id from roles where lower(name) ='sales_operations' ),60,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Agents'),(select id from roles where lower(name) ='sales_operations' ),70,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);