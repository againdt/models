package com.getinsured.hix.model.batch;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * This is the model class for API_BATCH_CONFIG table
 * @author Kunal Dav
 * @since 10 September, 2014
 */
@Entity
@Table(name="API_BATCH_CONFIG")
@DynamicInsert
@DynamicUpdate
public class ApiBatchConfig implements Serializable {
	
	private static final long serialVersionUID = 4145478687678886242L;
	
	public enum ISENABLED {
		/** Is cron enabled?  Value: YES or NO */
		YES, NO;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "API_BATCH_CONFIG_SEQ")
	@SequenceGenerator(name = "API_BATCH_CONFIG_SEQ", sequenceName = "API_BATCH_CONFIG_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="API_TYPE")
	private String apiType;
	
	@Column(name = "IS_ENABLED")
	@Enumerated(EnumType.STRING)
	private ISENABLED isEnabled;
	
	@Column(name="CRON_EXPRESSION")
	private String cronExpression;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CONFIG_DATE")
	private Date configDate;
	
	@Column(name="API_DESC")
	private String apiDesc;
	
	@Column(name="AGENT_GREETING")
	private String agentGreeting;
	
	@Column(name="REASON_TO_CALL")
	private String reasonToCall;
	
	@Column(name="NEXT_STEPS")
	private String nextSteps;

	@Column(name="PREDEFINED_WAIT_TIME")
	private Integer preDefinedWaitTime;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}

	public ISENABLED getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(ISENABLED isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Date getConfigDate() {
		return configDate;
	}

	public void setConfigDate(Date configDate) {
		this.configDate = configDate;
	}
	
	public String getApiDesc() {
		return apiDesc;
	}

	public void setApiDesc(String apiDesc) {
		this.apiDesc = apiDesc;
	}

	public String getAgentGreeting() {
		return agentGreeting;
	}

	public void setAgentGreeting(String agentGreeting) {
		this.agentGreeting = agentGreeting;
	}

	public String getReasonToCall() {
		return reasonToCall;
	}

	public void setReasonToCall(String reasonToCall) {
		this.reasonToCall = reasonToCall;
	}

	public String getNextSteps() {
		return nextSteps;
	}

	public void setNextSteps(String nextSteps) {
		this.nextSteps = nextSteps;
	}

	@PrePersist
	public void prePersist() {
		this.setConfigDate(new TSDate());
	}

	@Override
	public String toString() {
		return "ApiBatchConfig [id=" + id + ", apiType=" + apiType
				+ ", isEnabled=" + isEnabled + ", cronExpression="
				+ cronExpression + ", configDate=" + configDate + ", apiDesc="
				+ apiDesc + ", agentGreeting=" + agentGreeting
				+ ", reasonToCall=" + reasonToCall + ", nextSteps=" + nextSteps
				+ ", preDefinedWaitTime=" + preDefinedWaitTime +"]";
	}

	public Integer getPreDefinedWaitTime() {
		return preDefinedWaitTime;
	}

	public void setPreDefinedWaitTime(Integer preDefinedWaitTime) {
		this.preDefinedWaitTime = preDefinedWaitTime;
	}

}
