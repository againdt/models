package com.getinsured.hix.model.ssap;

import java.util.ArrayList;
import java.util.List;

public class MultiHousehold {
    private String id;
    private boolean active;
    private String applicationCreationDate;
    private List<MultiHouseholdMember> members;

    public MultiHousehold() {
        this.members = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getApplicationCreationDate() {
        return applicationCreationDate;
    }

    public void setApplicationCreationDate(String applicationCreationDate) {
        this.applicationCreationDate = applicationCreationDate;
    }

    public List<MultiHouseholdMember> getMembers() {
        return members;
    }

    public void setMembers(List<MultiHouseholdMember> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "MultiHousehold{" +
                "id='" + id + '\'' +
                ", active=" + active +
                ", applicationCreationDate=" + applicationCreationDate +
                ", members=" + members +
                '}';
    }
}
