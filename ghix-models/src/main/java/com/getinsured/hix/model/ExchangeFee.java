package com.getinsured.hix.model;

import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "exchange_fees")
public class ExchangeFee {
	
	private static final long serialVersionUID = 1L;
	
	public enum PaidStatus { DUE, PAID, PARTIAL; }
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exchange_fees_seq")
	@SequenceGenerator(name = "exchange_fees_seq", sequenceName = "exchange_fees_seq", allocationSize = 1)
	private int id;
	
	@OneToOne
	@JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@OneToOne
	@JoinColumn(name = "issuer_remittance_id")
	private IssuerRemittance issuerRemittance;
	
	@OneToOne
	@JoinColumn(name = "issuer_payment_id")
	private IssuerPayments issuerPayment;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "updated_date")
	private Date updatedDate;
	
	
	@Column(name = "starting_date")
	private Date startingDate;
	
	@Column(name = "ending_date")
	private Date endingDate;
	
	@Column(name = "total_fee_balance_due")
	private BigDecimal totalFeeBalanceDue;
	
	@Column(name = "payment_received")
	private BigDecimal paymentReceived;
	
	@Column(name = "total_refund")
	private BigDecimal totalRefund;
	
	@Column(name = "total_payment")
	private BigDecimal totalPayment;
	
	@Column(name = "automatic_process")
	private String automaticProcess;
	
	@Column(name = "status", length=4)
	@Enumerated(EnumType.STRING)
	private PaidStatus status;
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedDate(new TSDate());
		this.setUpdatedDate(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedDate(new TSDate());
	}
	
	
	public int getId() {
		return id;
	}

	public void setStatus(PaidStatus status) {
		this.status = status;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * @return the issuerRemittance
	 */
	public IssuerRemittance getIssuerRemittance() {
		return issuerRemittance;
	}

	/**
	 * @param issuerRemittance the issuerRemittance to set
	 */
	public void setIssuerRemittance(IssuerRemittance issuerRemittance) {
		this.issuerRemittance = issuerRemittance;
	}

	public IssuerPayments getIssuerPayment() {
		return issuerPayment;
	}

	public void setIssuerPayment(IssuerPayments issuerPayment) {
		this.issuerPayment = issuerPayment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getEndingDate() {
		return endingDate;
	}

	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}

	public BigDecimal getTotalFeeBalanceDue() {
		return totalFeeBalanceDue;
	}

	public void setTotalFeeBalanceDue(BigDecimal totalFeeBalanceDue) {
		this.totalFeeBalanceDue = totalFeeBalanceDue;
	}

	public BigDecimal getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(BigDecimal paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public BigDecimal getTotalRefund() {
		return totalRefund;
	}

	public void setTotalRefund(BigDecimal totalRefund) {
		this.totalRefund = totalRefund;
	}

	public BigDecimal getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}

	public String getAutomaticProcess() {
		return automaticProcess;
	}

	public void setAutomaticProcess(String automaticProcess) {
		this.automaticProcess = automaticProcess;
	}

}
