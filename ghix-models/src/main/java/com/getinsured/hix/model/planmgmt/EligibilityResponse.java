package com.getinsured.hix.model.planmgmt;

import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

public class EligibilityResponse extends GHIXResponse{
	private boolean allPlansAvailableSilver;
	private boolean allPlansAvailableBronze;
	private double benchmarkPremium;
	private double benchmarkPremiumTobacco;
	private int numberBronzeAvailable;
	private int numberSilverAvailable;
	private Map<String, HashMap<String, HashMap<String, String>>>  plansInformation;

	public boolean isAllPlansAvailableSilver() {
		return allPlansAvailableSilver;
	}
	
	public void setAllPlansAvailableSilver(boolean allPlansAvailableSilver) {
		this.allPlansAvailableSilver = allPlansAvailableSilver;
	}
	
	public boolean isAllPlansAvailableBronze() {
		return allPlansAvailableBronze;
	}
	
	public void setAllPlansAvailableBronze(boolean allPlansAvailableBronze) {
		this.allPlansAvailableBronze = allPlansAvailableBronze;
	}
	
	public double getBenchmarkPremium() {
		return benchmarkPremium;
	}
	
	public void setBenchmarkPremium(double benchmarkPremium) {
		this.benchmarkPremium = benchmarkPremium;
	}
	
	public double getBenchmarkPremiumTobacco() {
		return benchmarkPremiumTobacco;
	}
	
	public void setBenchmarkPremiumTobacco(double benchmarkPremiumTobacco) {
		this.benchmarkPremiumTobacco = benchmarkPremiumTobacco;
	}
	
	public int getNumberBronzeAvailable() {
		return numberBronzeAvailable;
	}
	
	public void setNumberBronzeAvailable(int numberBronzeAvailable) {
		this.numberBronzeAvailable = numberBronzeAvailable;
	}
	
	public int getNumberSilverAvailable() {
		return numberSilverAvailable;
	}
	
	public void setNumberSilverAvailable(int numberSilverAvailable) {
		this.numberSilverAvailable = numberSilverAvailable;
	}
	
	public Map<String, HashMap<String, HashMap<String, String>>>  getPlansInformation() {
		return plansInformation;
	}
	
	public void setPlansInformation(Map<String, HashMap<String, HashMap<String, String>>>  plansInformation) {
		this.plansInformation = plansInformation;
	}
	
	@Override
	public String toString() {
		return "EligibilityResponse [allPlansAvailableSilver="
				+ allPlansAvailableSilver + ", allPlansAvailableBronze="
				+ allPlansAvailableBronze + ", benchmarkPremium="
				+ benchmarkPremium + ", benchmarkPremiumTobacco="
				+ benchmarkPremiumTobacco + ", numberBronzeAvailable="
				+ numberBronzeAvailable + ", numberSilverAvailable="
				+ numberSilverAvailable + ", plansInformation="
				+ plansInformation + "]";
	}
}
