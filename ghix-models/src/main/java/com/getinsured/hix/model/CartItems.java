package com.getinsured.hix.model;

public class CartItems {

	private int srNo;
	private int id;
	private int planId;
	private int planCount;
	private String name;
	private String issuer;
	private String issuerText;
	private String level;
	private Float premium;
	private Float avgPremium;
	private String coInsurance;
	private String deductible;
	private String officeVisit;
	private String oopMax;
	private int empCount;
	private int employeeTotalCount;
	private Float employerTotalCost;
	private Float fromEmployerTotalCost;
	private Float toEmployerTotalCost;
	private Float employerContrForEmp;
	private Float estimatedTaxCredit;
	private Float employerContrForEmpInPreimum;
	private Float employeePayInPreimum;
	private int notAffodableCount;
	private Float taxPenalty;
	
	public CartItems() {
	}
	
	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}
	
	public int getPlanCount() {
		return planCount;
	}

	public void setPlanCount(int planCount) {
		this.planCount = planCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getLevel() {
		return level;
	}
	
	public String getIssuerText() {
		return issuerText;
	}

	public void setIssuerText(String issuerText) {
		this.issuerText = issuerText;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}

	public Float getAvgPremium() {
		return avgPremium;
	}

	public void setAvgPremium(Float avgPremium) {
		this.avgPremium = avgPremium;
	}
	
	public String getCoInsurance() {
		return coInsurance;
	}

	public void setCoInsurance(String coInsurance) {
		this.coInsurance = coInsurance;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getOfficeVisit() {
		return officeVisit;
	}

	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}

	public String getOopMax() {
		return oopMax;
	}

	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}

	public int getEmpCount() {
		return empCount;
	}

	public void setEmpCount(int empCount) {
		this.empCount = empCount;
	}

	public int getEmployeeTotalCount() {
		return employeeTotalCount;
	}

	public void setEmployeeTotalCount(int employeeTotalCount) {
		this.employeeTotalCount = employeeTotalCount;
	}

	public Float getEmployerTotalCost() {
		return employerTotalCost;
	}

	public void setEmployerTotalCost(Float employerTotalCost) {
		this.employerTotalCost = employerTotalCost;
	}
	
	public Float getFromEmployerTotalCost() {
		return fromEmployerTotalCost;
	}

	public void setFromEmployerTotalCost(Float fromEmployerTotalCost) {
		this.fromEmployerTotalCost = fromEmployerTotalCost;
	}

	public Float getToEmployerTotalCost() {
		return toEmployerTotalCost;
	}

	public void setToEmployerTotalCost(Float toEmployerTotalCost) {
		this.toEmployerTotalCost = toEmployerTotalCost;
	}

	public Float getEmployerContrForEmp() {
		return employerContrForEmp;
	}

	public void setEmployerContrForEmp(Float employerContrForEmp) {
		this.employerContrForEmp = employerContrForEmp;
	}

	public Float getEstimatedTaxCredit() {
		return estimatedTaxCredit;
	}

	public void setEstimatedTaxCredit(Float estimatedTaxCredit) {
		this.estimatedTaxCredit = estimatedTaxCredit;
	}

	public Float getEmployerContrForEmpInPreimum() {
		return employerContrForEmpInPreimum;
	}

	public void setEmployerContrForEmpInPreimum(Float employerContrForEmpInPreimum) {
		this.employerContrForEmpInPreimum = employerContrForEmpInPreimum;
	}

	public Float getEmployeePayInPreimum() {
		return employeePayInPreimum;
	}

	public void setEmployeePayInPreimum(Float employeePayInPreimum) {
		this.employeePayInPreimum = employeePayInPreimum;
	}
	
	public int getNotAffodableCount() {
		return notAffodableCount;
	}

	public void setNotAffodableCount(int notAffodableCount) {
		this.notAffodableCount = notAffodableCount;
	}

	public Float getTaxPenalty() {
		return taxPenalty;
	}

	public void setTaxPenalty(Float taxPenalty) {
		this.taxPenalty = taxPenalty;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id 
				+ ", name=" + name 
				+ ", level=" + level 
				+ ", planId=" + planId
				+ ", planCount=" + planCount
				+ ", empCount=" + empCount 
				+ ", fromPremium=" + premium
				+ ", avgPremium=" + avgPremium
				+ ", issuer=" + issuer
				+ ", issuerText=" + issuerText
				+ ", coInsurance=" + coInsurance
				+ ", deductible=" + deductible
				+ ", officeVisit=" + officeVisit
				+ ", oopMax=" + oopMax
				+ ", employeeTotalCount=" + employeeTotalCount
				+ ", employerTotalCost=" + employerTotalCost
				+ ", fromEmployerTotalCost=" + fromEmployerTotalCost
				+ ", toEmployerTotalCost=" + toEmployerTotalCost
				+ ", employerContrForEmp=" + employerContrForEmp
				+ ", estimatedTaxCredit=" + estimatedTaxCredit
				+ ", employerContrForEmpInPreimum=" + employerContrForEmpInPreimum
				+ ", employeePayInPreimum=" + employeePayInPreimum
				+ "]";
	}
	
	
	
}
