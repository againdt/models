package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="ENROLLMENT_OUT_1095")
public class EnrollmentOut1095 implements Serializable
{
	public enum SubmissionType{
		INITIAL,RESUBMIT;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_OUT_1095_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_OUT_1095_SEQ", sequenceName = "ENROLLMENT_OUT_1095_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "DOCUMENT_FILE_NAME")
	private String documentFileName;
	
	@Column(name = "BATCH_ID")
	private String batchId;
	
	@Column(name = "SUBMISSION_TYPE")
	private String submissionType;
	
	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "YEAR")
	private String year;
	
	@Column(name = "ENROLLMENT_IDS")
	private String enrollmentIds;
	
	@Column (name = "DOCUMENT_FILE_SIZE")
	private Long documentFileSize;
	
	@Column(name = "TOTAL_ENROLLMENTS_PER_XML")
	private Integer totalEnrollmentsPerXml;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;
	
	@Column(name = "BATCH_CATEGORY_CODE")
	private String batchCategoryCode;
	
	@Column(name = "PACKAGE_FILE_NAME")
	private String packageFileName;
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the documentFileName
	 */
	public String getDocumentFileName() {
		return documentFileName;
	}

	/**
	 * @param documentFileName the documentFileName to set
	 */
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	/**
	 * @return the batchId
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return the submissionType
	 */
	public String getSubmissionType() {
		return submissionType;
	}

	/**
	 * @param submissionType the submissionType to set
	 */
	public void setSubmissionType(String submissionType) {
		this.submissionType = submissionType;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the enrollmentIds
	 */
	public String getEnrollmentIds() {
		return enrollmentIds;
	}

	/**
	 * @param enrollmentIds the enrollmentIds to set
	 */
	public void setEnrollmentIds(String enrollmentIds) {
		this.enrollmentIds = enrollmentIds;
	}

	/**
	 * @return the documentFileSize
	 */
	public Long getDocumentFileSize() {
		return documentFileSize;
	}

	/**
	 * @param documentFileSize the documentFileSize to set
	 */
	public void setDocumentFileSize(Long documentFileSize) {
		this.documentFileSize = documentFileSize;
	}

	/**
	 * @return the totalEnrollmentsPerXml
	 */
	public Integer getTotalEnrollmentsPerXml() {
		return totalEnrollmentsPerXml;
	}

	/**
	 * @param totalEnrollmentsPerXml the totalEnrollmentsPerXml to set
	 */
	public void setTotalEnrollmentsPerXml(Integer totalEnrollmentsPerXml) {
		this.totalEnrollmentsPerXml = totalEnrollmentsPerXml;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the batchCategoryCode
	 */
	public String getBatchCategoryCode() {
		return batchCategoryCode;
	}

	/**
	 * @param batchCategoryCode the batchCategoryCode to set
	 */
	public void setBatchCategoryCode(String batchCategoryCode) {
		this.batchCategoryCode = batchCategoryCode;
	}

	public String getPackageFileName() {
		return packageFileName;
	}

	public void setPackageFileName(String packageFileName) {
		this.packageFileName = packageFileName;
	}
}
