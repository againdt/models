/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * Enrollment DTO to store recipient information for Annual IRS processing
 * @author negi_s
 *
 */
public class EnrollmentAnnualIrsRecipientDTO implements Serializable{
	private EnrollmentIrsCoveredIndividualDTO recipient;
	private EnrollmentIrsCoveredIndividualDTO recipientSpouse;
	
	public EnrollmentIrsCoveredIndividualDTO getRecipient() {
		return recipient;
	}
	public void setRecipient(EnrollmentIrsCoveredIndividualDTO recipient) {
		this.recipient = recipient;
	}
	public EnrollmentIrsCoveredIndividualDTO getRecipientSpouse() {
		return recipientSpouse;
	}
	public void setRecipientSpouse(EnrollmentIrsCoveredIndividualDTO recipientSpouse) {
		this.recipientSpouse = recipientSpouse;
	}
	@Override
	public String toString() {
		return "EnrollmentAnnualIrsRecipientDTO [recipient=" + recipient
				+ ", recipientSpouse=" + recipientSpouse + "]";
	}
}
