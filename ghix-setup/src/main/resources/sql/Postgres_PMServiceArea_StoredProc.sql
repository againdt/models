
DROP function IF EXISTS synchronizepmservicearea(tableName IN VARCHAR, msg OUT VARCHAR);


----------------------------------------------------------------------
--This procedure is used to synchronise PM_SERVICE_AREA quarter table  
--Takes table name as parameter
--returns msg
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION synchronizePMServiceArea(tableName IN VARCHAR)
    RETURNS varchar 
AS $Q$
DECLARE
	msg varchar;
	issuerSrvcAreaid  PM_ISSUER_SERVICE_AREA.id%type;
	issuerState PM_ISSUER_SERVICE_AREA.issuer_state%type;
	completeState PM_ISSUER_SERVICE_AREA.complete_state%type ;
	curZipcode ZIPCODES.zipcode%type;
	curCounty ZIPCODES.county%type;
	countyName ZIPCODES.county%type;
	countyFips ZIPCODES.county_Fips%type;
	stateFips ZIPCODES.state_Fips%type;
	tempId INTEGER;
	tempExtID INTEGER;
	revNo INTEGER;
	revtime bigint;
	extId PM_ISSUER_SERVICE_AREA_EXT.id%type;
	extIdNew PM_ISSUER_SERVICE_AREA_EXT.id%type;
	extCounty PM_ISSUER_SERVICE_AREA_EXT.county%type;
	partialCounty  PM_ISSUER_SERVICE_AREA_EXT.partial_county%type;
	ziplist PM_ISSUER_SERVICE_AREA_EXT.ziplist%type;
	POS     INTEGER := 0;
	stmt_str_insert VARCHAR;
	isNodata VARCHAR := 'no data found';
	curState ZIPCODES.state%type;
	idCount INTEGER;    
	---------------------------------------------------------------------------------------------------
	--cursor to fetch all the ids from PM_ISSUER_SERVICE_AREA table for current and following years.
	---------------------------------------------------------------------------------------------------
	ISSUERSERVICEAREADATA_CUR CURSOR FOR
	SELECT id, issuer_state, complete_state FROM PM_ISSUER_SERVICE_AREA where APPLICABLE_YEAR>= (SELECT EXTRACT(YEAR FROM CURRENT_TIMESTAMP));  

	-------------------------------------------------------------------------------------------------------------
	-- cursor to fetch id , county , partial_county issuer_service_area_id from PM_ISSUER_SERVICE_AREA_EXT table.
	-------------------------------------------------------------------------------------------------------------
	ISSUER_SERVICE_AREA_EXT_CUR CURSOR (issuerServiceAreaID PM_ISSUER_SERVICE_AREA.id%type) FOR
		SELECT DISTINCT ext.id ,ext.county, ext.partial_county, ext.ISSUER_SERVICE_AREA_ID, ext.ziplist
		FROM PM_ISSUER_SERVICE_AREA_EXT ext, PM_ISSUER_SERVICE_AREA isa
		WHERE ext.ISSUER_SERVICE_AREA_ID=isa.ID
		AND ext.IS_DELETED= 'N'
		AND isa.ID = issuerServiceAreaID;
	--------------------------------------------------------------------
	-- cursor to  fetch county county_fips state from ZIPCODES table.
	--------------------------------------------------------------------
	ZIPCODES_CUR CURSOR  (ZIP ZIPCODES.ZIPCODE%TYPE, curCOUNTY PM_ISSUER_SERVICE_AREA_EXT.COUNTY%TYPE) FOR
		SELECT COUNTY, COUNTY_FIPS, STATE_FIPS FROM ZIPCODES WHERE ZIPCODE=ZIP AND COUNTY=curCOUNTY;	

	-----------------------------------------------------------------------------------------------------------
	--cursor to fetch zipcode, county, county_fips, state_Fips from ZIPCODES table for given state and county.
	-----------------------------------------------------------------------------------------------------------
	ZIPCODES_FROM_COUNTY_STATES CURSOR (extCounty PM_ISSUER_SERVICE_AREA_EXT.COUNTY%type,issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type) FOR
		SELECT DISTINCT zipcode, lower(county), county_fips, state_Fips, state
		FROM ZIPCODES
		WHERE LOWER(county) = LOWER(extCounty)
		AND state = issuerState;

	-----------------------------------------------------------------------------------
	--cursor to fetch zipcode, county, county_fips, state_Fips for given state.
	-----------------------------------------------------------------------------------
	ZIPS_FROM_STATE_CUR CURSOR (issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type) FOR
		SELECT DISTINCT zipcode, lower(county), county_fips, state_Fips, state
		FROM ZIPCODES
		WHERE state  = issuerState;
	
	BEGIN
		----------------------------------------------------------
		--statement to insert data into respective quarter table.
		----------------------------------------------------------
		stmt_str_insert := 'INSERT INTO ' || tableName || '(ID, SERVICE_AREA_ID, ZIP, COUNTY, FIPS, STATE, SERVICE_AREA_EXT_ID, IS_DELETED) 
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)';

		RAISE INFO 'Procedure synchronizePMServiceArea Initiated!!!';
		--------------------------------------------------------
		--fetch revNo from REVISION_INFO_SEQ for audit entry.
		--------------------------------------------------------
		SELECT nextval('REVISION_INFO_SEQ') INTO revNo;
  
		-------------------------------------------------------------------
		--[HIX-48149]get rev time for audit entry and insert into REVISION_INFO table
		-------------------------------------------------------------------
		SELECT cast(TO_NUMBER(TO_CHAR(current_timestamp, 'yymmddhh24miss'))as bigint) INTO revtime;
		INSERT INTO REVISION_INFO (revision_id  , revision_timestamp) VALUES(revNo , revtime);
	
		OPEN  ISSUERSERVICEAREADATA_CUR;
		---------------------------------------
		--main loop over each service_area_id
		---------------------------------------
		LOOP
			FETCH ISSUERSERVICEAREADATA_CUR INTO issuerSrvcAreaid, issuerState ,completeState;
			IF ISSUERSERVICEAREADATA_CUR%FOUND THEN
				isNodata := 'data found';
			END IF;
			EXIT WHEN issuerServiceAreaData_cur%NOTFOUND;
		
			----------------------------------------------------------------------------------------------------------------------
			-- if complete state ='Y' fetch all records from zipcodes table for that state and insert in respective quarter table.
			----------------------------------------------------------------------------------------------------------------------
			IF completeState = 'YES' THEN
				OPEN ZIPS_FROM_STATE_CUR(issuerState);
				LOOP
					FETCH ZIPS_FROM_STATE_CUR INTO curZipcode ,countyName ,countyFips,stateFips,curState;
					EXIT WHEN ZIPS_FROM_STATE_CUR%NOTFOUND;
							
					-------------------------------------------------------------------------------
					--check if issuer_service_area_id exist into PM_ISSUER_SERVICE_AREA_EXT table.
					--------------------------------------------------------------------------------
					SELECT COUNT(ID) INTO idCount FROM PM_ISSUER_SERVICE_AREA_EXT  WHERE 
						ISSUER_SERVICE_AREA_ID = issuerSrvcAreaid  
						AND LOWER(COUNTY) = LOWER(countyName) AND IS_DELETED ='N' ;
						
					IF idCount = 0 THEN
					
						----------------------------------------------------------------------------------------------------------------------------------------------------
						--if issuerSrvcAreaid entry is not present in PM_ISSUER_SERVICE_AREA_EXT get nextval from PM_ISSUER_SERVICE_AREA_EXT_SEQ and insert into this table .
						-----------------------------------------------------------------------------------------------------------------------------------------------------
						SELECT nextval('PM_ISSUER_SERVICE_AREA_EXT_SEQ') INTO extIdNew;	
						
						INSERT INTO PM_ISSUER_SERVICE_AREA_EXT (id, issuer_service_area_id, county, partial_county,
							partial_county_justification, is_deleted, ziplist) 
							VALUES (extIdNew, issuerSrvcAreaid, countyName , 'NO', NULL, 'N', NULL);
						
						----------------------------------------------------------------------
						--[HIX-48149]inserting audit entry in PM_ISSUER_SERVICE_AREA_EXT_AUD.
						----------------------------------------------------------------------
						INSERT INTO PM_ISSUER_SERVICE_AREA_EXT_AUD (id, rev, revtype, issuer_service_area_id, county, partial_county,
							partial_county_justification, is_deleted, ziplist) 
							VALUES (extIdNew, revNo, 0, issuerSrvcAreaid, countyName, 'NO', NULL, 'N', NULL);
						
					ELSE
						--------------------------------------------------------------
						--else get existing id from PM_ISSUER_SERVICE_AREA_EXT table.
						--------------------------------------------------------------
						SELECT ID into extIdNew  FROM PM_ISSUER_SERVICE_AREA_EXT  WHERE 
							ISSUER_SERVICE_AREA_ID = issuerSrvcAreaid  AND lower(COUNTY)=lower(countyName) AND IS_DELETED ='N' ;
					END IF ;

					-----------------------------------------------------
					--inserting data into PM_SERVICE_AREA Quarter table.
					-----------------------------------------------------
					SELECT nextval('PM_SERVICE_AREA_SEQ') INTO tempId;
					EXECUTE stmt_str_insert USING tempId, issuerSrvcAreaid, curZipcode, countyName,
						stateFips||countyFips, curState, extIdNew, 'N';

					-------------------------------------------------------
					--[HIX-48149]inserting audit data into PM_SERVICE_AREA_AUD table.
					--------------------------------------------------------
					INSERT INTO PM_SERVICE_AREA_AUD (id, rev, revtype, service_area_id, zip, county, fips, state, service_area_ext_id, is_deleted) 
						VALUES (tempId, revNo, 0, issuerSrvcAreaid, curZipcode, countyName, stateFips||countyFips, curState, extIdNew, 'N');
					
				END LOOP;
			
				CLOSE ZIPS_FROM_STATE_CUR;
			ELSE
				OPEN ISSUER_SERVICE_AREA_EXT_CUR(issuerSrvcAreaid);
				LOOP
				
			 		FETCH ISSUER_SERVICE_AREA_EXT_CUR INTO  extId ,extCounty,partialCounty ,issuerSrvcAreaid , ziplist ;
					EXIT WHEN ISSUER_SERVICE_AREA_EXT_CUR%NOTFOUND;
					---------------------------------------------------------------------------------------------------------------------------------------
					-- if partialCounty ='YES' fetch ziplist from PM_ISSUER_SERVICE_AREA_EXT table split with comma and insert in respective quarter table.
					---------------------------------------------------------------------------------------------------------------------------------------
					IF partialCounty = 'YES' THEN
						pos        := 1;
						WHILE (pos != 0) 
						LOOP
							pos     := INSTR(ZIPLIST,',',1,1);
							curZipcode := SUBSTR(ZIPLIST,1,pos-1);
							ziplist := SUBSTR(ZIPLIST,POS+1,LENGTH(ZIPLIST));  
							OPEN ZIPCODES_CUR(curZipcode, extCounty);
							FETCH ZIPCODES_CUR INTO curCounty ,countyFips ,stateFips; 
							---------------------------------------------------------
							--IF zipcode is less then length 5 , return and rollback .
							----------------------------------------------------------
							If Length(curZipcode)<5 Then
								msg := 'Error reading zipcodes from PM_ISSUER_SERVICE_AREA_EXT table';		    
								RAISE INFO '%',msg; 
								rollback;
								RETURN msg;
							END IF;
								
							SELECT nextval('PM_SERVICE_AREA_SEQ') INTO tempId;
							-----------------------------------------------------------
							--inserting data into PM_SERVICE_AREA Quarter table.
							-----------------------------------------------------------
							EXECUTE  stmt_str_insert USING tempId, issuerSrvcAreaid, curZipcode, curCounty, 
								stateFips||countyFips, curState, extId, 'N';
								
							------------------------------------------------------------
							--[HIX-48149]inserting audit data into PM_SERVICE_AREA_AUD table.
							------------------------------------------------------------
							INSERT INTO PM_SERVICE_AREA_AUD (id,rev, revtype, service_area_id, zip, county, fips, state, service_area_ext_id, is_deleted) 
								VALUES (tempId, revNo, 0, issuerSrvcAreaid, curZipcode, curCounty, stateFips||countyFips,
									curState, extId, 'N');
								
							CLOSE ZIPCODES_CUR ;
						END LOOP;
					ELSE
						-----------------------------------------------------------------------------------------------------------------------------------------
						-- if partialCounty ='NO' fetch all records from zipcode table for that particular county and issuerState and insert in respective table.
						------------------------------------------------------------------------------------------------------------------------------------------
						IF partialCounty ='NO' THEN
							OPEN ZIPCODES_FROM_COUNTY_STATES(extCounty , issuerState );
							LOOP
								FETCH ZIPCODES_FROM_COUNTY_STATES INTO curZipcode, curCounty, countyFips, stateFips, curState;
								EXIT WHEN ZIPCODES_FROM_COUNTY_STATES%NOTFOUND;
									SELECT nextval('PM_SERVICE_AREA_SEQ') INTO tempId;
									--insert data into PM_SERVICE_AREA Quarter table
									EXECUTE  stmt_str_insert USING tempId, issuerSrvcAreaid, curZipcode,
										curCounty, stateFips||countyFips, curState, extId, 'N';

									 -------------------------------------------------------
									 --inserting audit data into PM_SERVICE_AREA_AUD table.
									 -------------------------------------------------------
									INSERT INTO PM_SERVICE_AREA_AUD (id, rev, revtype, service_area_id, zip, county, fips,
									state, service_area_ext_id, is_deleted) VALUES  
									(tempId, revNo, 0, issuerSrvcAreaid, curZipcode, curCounty, stateFips||countyFips, curState, extId, 'N');
								END LOOP;
							CLOSE ZIPCODES_FROM_COUNTY_STATES;
						END IF;
					END IF;
				END LOOP;
				CLOSE ISSUER_SERVICE_AREA_EXT_CUR ;
			 
			END IF;
		----------------------	
		--closing main loop	.
		----------------------
		END LOOP;
		CLOSE ISSUERSERVICEAREADATA_CUR ; 
		IF isNodata = 'no data found' THEN  
			msg := 'no data found in PM_ISSUER_SERVICE_AREA';		
		ELSE
			msg := 'Successfully Executed';		    
		END IF;
		
		RAISE INFO 'Procedure synchronizePMServiceArea Ended!!!';
		RAISE NOTICE '%',msg;
		RETURN msg;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN 
		BEGIN
			msg := 'Select...InTo did not return any row.'; 
			RAISE INFO '%',msg; 
		END;
		WHEN others THEN
		BEGIN
      IF msg is NULL THEN
        msg := 'Unknown Exception Occured!';
      END IF;
			RAISE INFO '%',msg; 
		END;
		IF ISSUERSERVICEAREADATA_CUR%ISOPEN THEN
			CLOSE ISSUERSERVICEAREADATA_CUR;
		END IF;
		 
		IF ISSUER_SERVICE_AREA_EXT_CUR%ISOPEN THEN
			CLOSE ISSUER_SERVICE_AREA_EXT_CUR;
		END IF;

		IF ZIPCODES_CUR%ISOPEN THEN
			CLOSE ZIPCODES_CUR;
		END IF ;

		IF ZIPCODES_FROM_COUNTY_STATES%ISOPEN THEN
			CLOSE ZIPCODES_FROM_COUNTY_STATES;
		END IF ;
		IF ZIPS_FROM_STATE_CUR%ISOPEN THEN
			CLOSE ZIPS_FROM_STATE_CUR;
		END IF ;
		RETURN msg;
	END;
$Q$ 
LANGUAGE plpgsql;
