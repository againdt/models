package com.getinsured.hix.model.estimator.mini;

import java.util.Map;

public class MedicareLeadResponse {

	private Map<Integer,String> errors;
	
	private long leadId;

	private String status;

	public Map<Integer, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<Integer, String> errors) {
		this.errors = errors;
	}

	public long getLeadId() {
		return leadId;
	}

	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	}