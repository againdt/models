/**
 * 
 */
package com.getinsured.hix.platform.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author panda_p In this class we can define constants for GHIX application. These Constants can
 *         be either direct (define a constant of type psf & directly assign any value to it) or it
 *         can from config file. (define a constant of type psf & populate value from config file by
 *         using 'Value' attribute of annotation)
 */

@Component
public final class GhixConstantsModelBackup {

	public static String HUB_KEY_STORE_LOCATION;

	public static String HUB_KEY_STORE_FILE;
	
	public static String HUB_SERVICE_DEFINITION_DIR;
	
	public static String HUB_KEYSTORE_PASS;

	public static String DATABASE_TYPE;
	
	public static String ENROLLMENT_ID = "ENROLLMENT_ID";
	
	public static String MEMBER_ID = "MEMBER_ID";

	private GhixConstantsModelBackup() {
	}

	/***************************************************************************************/
	/********************** Common configuration properties *********************************/
	/***************************************************************************************/

	public static String MY_ACCOUNT_URL;
	
	public static String APP_URL;
	public static String PREVIOUS_PAGE_URL = "previousPageURL" ;
	
	public static String[] helathBenefitList = {"PRIMARY_VISIT", "SPECIAL_VISIT", "OTHER_PRACTITIONER_VISIT", "PREVENT_SCREEN_IMMU",
        "LABORATORY_SERVICES", "IMAGING_XRAY", "IMAGING_SCAN",
        "GENERIC", "PREFERRED_BRAND", "NON_PREFERRED_BRAND", "SPECIALTY_DRUGS",
        "OUTPATIENT_FACILITY_FEE", "OUTPATIENT_SURGERY_SERVICES",
        "EMERGENCY_SERVICES", "EMERGENCY_TRANSPORTATION", "URGENT_CARE",
        "MENTAL_HEALTH_OUT", "MENTAL_HEALTH_IN", "SUBSTANCE_OUTPATIENT_USE", "SUBSTANCE_INPATIENT_USE",
        "PRENATAL_POSTNATAL", "INPATIENT_HOSPITAL_SERVICE", "INPATIENT_PHY_SURGICAL_SERVICE",
        "HOME_HEALTH_SERVICES", "OUTPATIENT_REHAB_SERVICES", "HABILITATION", "SKILLED_NURSING_FACILITY", "DURABLE_MEDICAL_EQUIP", "HOSPICE_SERVICE", 
        "RTN_EYE_EXAM_CHILDREN", "GLASSES_CHILDREN",
        "ACCIDENTAL_DENTAL", "BASIC_DENTAL_CARE_ADULT", "BASIC_DENTAL_CARE_CHILD", "DENTAL_CHECKUP_CHILDREN", "MAJOR_DENTAL_CARE_CHILD", 
        "MAJOR_DENTAL_CARE_ADULT", "ORTHODONTIA_ADULT", "ORTHODONTIA_CHILD", "RTN_DENTAL_ADULT", 
        "ACUPUNCTURE", "CHIROPRACTIC", "REHABILITATIVE_PHYSICAL_THERAPY", "HEARING_AIDS", "NUTRITIONAL_COUNSELING", "WEIGHT_LOSS", "DELIVERY_IMP_MATERNITY_SERVICES",
        "REHABILITATIVE_SPEECH_THERAPY", "WELL_BABY", "ALLERGY_TESTING", "DIABETES_EDUCATION",
        "MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE","OUTPATIENT_SERVICES_OFFICE_VISIT","DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE","SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE","EMERGENCY_ROOM_PROFESSIONAL_FEE" };
       // "MAJOR_DENTAL_CARE_ADULT", "ORTHODONTIA_ADULT", "ORTHODONTIA_CHILD", "RTN_DENTAL_ADULT"};

	public static String[] dentalBenefitList = {"ACCIDENTAL_DENTAL", "BASIC_DENTAL_CARE_ADULT", "BASIC_DENTAL_CARE_CHILD", "DENTAL_CHECKUP_CHILDREN", "MAJOR_DENTAL_CARE_CHILD", 
        "MAJOR_DENTAL_CARE_ADULT", "ORTHODONTIA_ADULT", "ORTHODONTIA_CHILD", "RTN_DENTAL_ADULT", "TEETH_CLEANING", "X_RAY", "TOPICAL_FLOURIDE", "FILLINGS", "EXTRACTIONS", "ORAL_SURGERY", "CROWN", "ROOT_CANAL", "DENTURES", "ENDODONTICS", "IMPLANTS", "ORTHODONTIA"};
	
	public static String[] stmBenefitList = {"PRIMARY_VISIT", "SPECIAL_VISIT", "OTHER_PRACTITIONER_VISIT", "PREVENT_SCREEN_IMMU",
		"GENERIC","PREFERRED_BRAND", "NON_FORMULARY_DRUG", "DRUG_BENIFIT_LIMIT",
		"OUTPATIENT_SURGERY_SERVICES","LABORATORY_SERVICES", "IMAGING_XRAY",
		"HOSPITALIZATION", "EMERGENCY_SERVICES","URGENT_CARE",
		"PRENATAL_POSTNATAL","DELIVERY_IMP_MATERNITY_SERVICES",
		"CHIROPRACTIC", "MENTAL_HEALTH_OUT", "OUT_OF_NETWORK_COVERAGE", "OUT_OF_COUNTRY_COVERAGE", "OFFICE_VISIT"};
	
	public static String[] ameBenefitList = { "AMBULANCE", "ACCIDENTAL_DEATH",
			"ACCIDENTAL_DISMEMBERMENT", "HOSPITAL_STAY",
			"SUPPLEMENTAL_ACCIDENT", "FOLLOW_UP_TREATMENT",
			"EMERGENCY_TREATMENT", "SURGERY", "INITIAL_HOSPITALIZATION" ,"OTHER_BENEFITS"};
	
	public static String MASTER_HEAD_HOME; //HIX-21061
	
	@Value("#{configProp['masthead.home']}")
	public void setMASTER_HEAD_HOME(String mASTER_HEAD_HOME) {
		MASTER_HEAD_HOME = mASTER_HEAD_HOME;
	}
	
	@Value("#{configProp['appUrl']}")
	public void setAPP_URL(String appURL) {
		APP_URL = appURL;
	}

	@Value("#{configProp['security.myAccountUrl']}")
	public void setMY_ACCOUNT_URL(String myAccountUrl) {
		MY_ACCOUNT_URL = myAccountUrl;
	}

	public static String MY_INBOX_URL;

	@Value("#{configProp['security.myInboxUrl']}")
	public void setMY_INBOX_URL(String myInboxUrl) {
		MY_INBOX_URL = myInboxUrl;
	}

	public static String LOGIN_PAGE_URL;

	@Value("#{configProp['security.loginPageUrl']}")
	public void setLOGIN_PAGE_URL(String loginPageUrl) {
		LOGIN_PAGE_URL = loginPageUrl;
	}
	
	public static String HELP_PAGE_URL;	
	@Value("#{configProp['masthead.help']}")
	public void setHELP_PAGE_URL(String helpPageUrl) {
		HELP_PAGE_URL = helpPageUrl;
	}
	
	public static String CHAT_URL;	
	@Value("#{configProp['masthead.chatUrl']}")
	public void setCHAT_URL(String chatUrl) {
		CHAT_URL = chatUrl;
	}
	
	public static String CUSTOMER_SERVICE_PHONE;	
	@Value("#{configProp['masthead.customerServicePhone']}")
	public void setCUSTOMER_SERVICE_PHONE(String customerServicePhone) {
		CUSTOMER_SERVICE_PHONE = customerServicePhone;
	}
	
	public static String REDIRECT_ENROLLMENT_TO_BB_URL;	
	@Value("#{configProp['enrollment.redirectEnrollmentToBbUrl']}")
	public void setREDIRECT_ENROLLMENT_TO_BB_URL(String rEDIRECT_ENROLLMENT_TO_BB_URL) {
		REDIRECT_ENROLLMENT_TO_BB_URL = rEDIRECT_ENROLLMENT_TO_BB_URL;
	}
	
	/***************************************************************************************/
	/********************** Common configuration properties *********************************/
	/***************************************************************************************/
	public static String UPLOAD_PATH;
	public static String ISSUER_DOC_PATH;
	public static String EXCHANGE_NAME;
	public static String PLAN_DOC_PATH;
	public static String PROVIDER_DOC_PATH;
	public static String BROKER_DOC_PATH;
	public static String ENVIRONMENT;
	public static String GOOGLE_ANALYTICS_CODE;
	public static String GHIX_ENVIRONMENT;
	
	@Value("#{configProp.uploadPath}")
	public void setUPLOAD_PATH(String uploadPath) {
		UPLOAD_PATH = uploadPath;
	}
	@Value("#{configProp.ghixEnvironment}")
	public void setGHIX_ENVIRONMENT(String ghixEnvironment) {
		GHIX_ENVIRONMENT = ghixEnvironment;
	}
	@Value("#{configProp.issuer_doc_path}")
	public void setISSUER_DOC_PATH(String issuerDocPath) {
		ISSUER_DOC_PATH = issuerDocPath;
	}

	@Value("#{configProp.exchangename}")
	public void setEXCHANGE_NAME(String eXCHANGE_NAME) {
		EXCHANGE_NAME = eXCHANGE_NAME;
	}
	
	@Value("#{configProp.ghixEnvironment}")
	public void setENVIRONMENT(String eNVIRONMENT) {
		ENVIRONMENT = eNVIRONMENT;
	}
	
	@Value("#{configProp.googleAnalyticsTrackingCode}")
	public void setGOOGLE_ANALYTICS_CODE(String tRACKING_CODE) {
		GOOGLE_ANALYTICS_CODE = tRACKING_CODE;
	}

	/***************************************************************************************/
	/*********************** Broker configuration properties *****************************/
	/***************************************************************************************/
	@Value("#{configProp.broker_doc_path}")
	public void setBROKER_DOC_PATH(String brokerDocPath) {
		BROKER_DOC_PATH = brokerDocPath;
	}

	public static String SWITCH_URL_IND66;

	@Value("#{configProp['giToAhbxAccountSwitchUrl']}")
	public void setSWITCH_URL_IND66(String sWITCH_URL_IND66) {
		SWITCH_URL_IND66 = sWITCH_URL_IND66;
	}
	
	public static String BOB_DOWNLOAD_URL;
	
	@Value("#{configProp['bobDownloadURL']}")
	public void setBOB_DOWNLOAD_URL(String bOB_DOWNLOAD_URL) {
		BOB_DOWNLOAD_URL = bOB_DOWNLOAD_URL;
	}
	
/*
	// for build detail configuration
	public static String BUILD_NUMBER;
	public static String BUILD_DATE;
    public static String BUILD_BRANCH_NAME;
	
	@Value("#{buildProp['build.number']}")
	public void setBUILD_NUMBER(String bUILD_NUMBER) {
		BUILD_NUMBER = bUILD_NUMBER;
	}

	@Value("#{buildProp['build.timestamp']}")
	public void setBUILD_DATE(String bUILD_DATE) {
		BUILD_DATE = bUILD_DATE;
	}

	@Value("#{buildProp['branch.name']}")
	public void setBUILD_BRANCH_NAME(String bUILD_BRANCH_NAME) {
		BUILD_BRANCH_NAME = bUILD_BRANCH_NAME;
	}
*/

	/***************************************************************************************/
	/*********************** Provider/Plan configuration properties *****************************/
	/***************************************************************************************/
	public static String PROVIDER_UPLOADFILEPATH;
	public static String PROVIDER_PHYSICIAN_DOWNLOADFILEPATH;
	public static String PROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH;
	public static String PROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH;
	public static String PROVIDER_FACILITIES_DOWNLOADFILEPATH;
	public static String PROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH;
	public static String PROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH;
	public static final String ACKNOWLEDGE_200_OK = "200 OK";
	public static final String ENROLLMENT_ENTITY_DOC_PATH = "Enrollment Entity Documents";
	public static final String ASSISTER_IMAGE_PATH = "Assister Images";
	public static final String EXISTING_ASSISTER_IMAGE = "Existing Assister Images";

	@Value("#{configProp['provider.uploadFilePath']}")
	public void setPROVIDER_UPLOADFILEPATH(String pROVIDER_UPLOADFILEPATH) {
		PROVIDER_UPLOADFILEPATH = pROVIDER_UPLOADFILEPATH;
	}

	@Value("#{configProp['provider.physicians.downloadFilePath']}")
	public void setPROVIDER_PHYSICIAN_DOWNLOADFILEPATH(String pROVIDER_PHYSICIAN_DOWNLOADFILEPATH) {
		PROVIDER_PHYSICIAN_DOWNLOADFILEPATH = pROVIDER_PHYSICIAN_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.physiciansAddress.downloadFilePath']}")
	public void setPROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH(String pROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH) {
		PROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH = pROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.physiciansSpecialty.downloadFilePath']}")
	public void setPROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH(String pROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH) {
		PROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH = pROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.facilities.downloadFilePath']}")
	public void setPROVIDER_FACILITIES_DOWNLOADFILEPATH(String pROVIDER_FACILITIES_DOWNLOADFILEPATH) {
		PROVIDER_FACILITIES_DOWNLOADFILEPATH = pROVIDER_FACILITIES_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.facilitiesAddress.downloadFilePath']}")
	public void setPROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH(String pROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH) {
		PROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH = pROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.facilitiesSpecialty.downloadFilePath']}")
	public void setPROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH(String pROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH) {
		PROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH = pROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH;
	}

	@Value("#{configProp.plan_doc_path}")
	public void setPLAN_DOC_PATH(String planDocPath) {
		PLAN_DOC_PATH = planDocPath;
	}

	@Value("#{configProp.provider_doc_path}")
	public void setPROVIDER_DOC_PATH(String providerDocPath) {
		PROVIDER_DOC_PATH = providerDocPath;
	}
	

	/*****************************************************************************************************/
	/*********************** Employer SHOP Web Service configuration properties *************************/
	/*****************************************************************************************************/
	public static String TNS_GHIX_EMP_PLAN_SELECTION;
	public static String WSDL_GHIX_EMP_PLAN_SELECTION;
	public static String TNS_ACN_ORDER_ID_UPDATE;
	public static String WSDL_ACN_ORDER_ID_UPDATE;
	public static String TNS_ACN_EMP_DETAILS;
	public static String WSDL_ACN_EMP_DETAILS;

	/*@Value("#{configProp.tnsGhixEmpPlanSelection}")
	public void setTNS_GHIX_EMP_PLAN_SELECTION(String tNS_GHIX_EMP_PLAN_SELECTION) {
		TNS_GHIX_EMP_PLAN_SELECTION = tNS_GHIX_EMP_PLAN_SELECTION;
	}

	@Value("#{configProp.wsdlGhixEmpPlanSelection}")
	public void setWSDL_GHIX_EMP_PLAN_SELECTION(String wSDL_GHIX_EMP_PLAN_SELECTION) {
		WSDL_GHIX_EMP_PLAN_SELECTION = wSDL_GHIX_EMP_PLAN_SELECTION;
	}
*/
	/*@Value("#{configProp.tnsAcnOrderIdUpdate}")
	public void setTNS_ACN_ORDER_ID_UPDATE(String tNS_ACN_ORDER_ID_UPDATE) {
		TNS_ACN_ORDER_ID_UPDATE = tNS_ACN_ORDER_ID_UPDATE;
	}*/

	/*@Value("#{configProp.wsdlAcnOrderIdUpdate}")
	public void setWSDL_ACN_ORDER_ID_UPDATE(String wSDL_ACN_ORDER_ID_UPDATE) {
		WSDL_ACN_ORDER_ID_UPDATE = wSDL_ACN_ORDER_ID_UPDATE;
	}
*/
	/*@Value("#{configProp.tnsAcnEmployerDetails}")
	public void setTNS_ACN_EMP_DETAILS(String tNS_ACN_EMP_DETAILS) {
		TNS_ACN_EMP_DETAILS = tNS_ACN_EMP_DETAILS;
	}

	@Value("#{configProp.wsdlAcnEmployerDetails}")
	public void setWSDL_ACN_EMP_DETAILS(String wSDL_ACN_EMP_DETAILS) {
		WSDL_ACN_EMP_DETAILS = wSDL_ACN_EMP_DETAILS;
	}

	public static String AFTER_SHOP_EXTERNAL_REDIRECT_LINK;

	@Value("#{configProp.AfterShopExternalRedirectLink}")
	public void setACN_AFTER_SHOP_REDIRECT_LINK(String aFTER_SHOP_EXTERNAL_REDIRECT_LINK) {
		AFTER_SHOP_EXTERNAL_REDIRECT_LINK = aFTER_SHOP_EXTERNAL_REDIRECT_LINK;
	}
*/
	public static String AFTER_ENROLLMENT_ACN_REDIRECT_LINK;

	@Value("#{configProp['enrollment.afterEnrollmentAcnRedirectLink']}")
	public void setAFTER_ENROLLMENT_ACN_REDIRECT_LINK(String aFTER_ENROLLMENT_ACN_REDIRECT_LINK) {
		AFTER_ENROLLMENT_ACN_REDIRECT_LINK = aFTER_ENROLLMENT_ACN_REDIRECT_LINK;
	}
	
	public static String ADULT_DENTAL_PLAN_LINK;
	@Value("#{configProp['enrollment.findAdultDentalPlanLink']}")
	public void setADULT_DENTAL_PLAN_LINK(String aDULT_DENTAL_PLAN_LINK) {
		ADULT_DENTAL_PLAN_LINK = aDULT_DENTAL_PLAN_LINK;
	}

	/* Web service configuration for Eligibility module begins */
	public static String GET_INCOME_FOR_ANONYMOUS_PRESCREEN_URL="#";
	public static String GET_USER_APPLICATION="#";
	public static String SAVE_USER_APPLICATION="#";

	/* Web service configuration for Eligibility module ends */

	/*****************************************************************************************************/
	/*********************** Enrollment Web Service configuration properties *****************************/
	/*****************************************************************************************************/

	/*public static boolean ENRL_AHBX_WSDL_CALL_ENABLE;

	@Value("#{configProp['enrollment.ahbxWsdlCallEnable']}")
	public void setENRL_AHBX_WSDL_CALL_ENABLE(boolean eNRL_AHBX_WSDL_CALL_ENABLE) {
		ENRL_AHBX_WSDL_CALL_ENABLE = eNRL_AHBX_WSDL_CALL_ENABLE;
	}
	
	public static String ISA05;

	@Value("#{configProp['enrollment.ISA05']}")
	public void setISA05(String strISA05) {
		ISA05 = strISA05;
	}

	public static String ISA06;

	@Value("#{configProp['enrollment.ISA06']}")
	public void setISA06(String strISA06) {
		ISA06 = strISA06;
	}
	public static String GS08;
	@Value("#{configProp['enrollment.GS08']}")
	public void setGS08(String strGS08) {
		GS08 = strGS08;
	}*/
	/*
	 * public static String CREATE_INDIVIDUAL_ENROLLMENT_URL;
	 * @Value("#{configProp['enrollment.createIndividualEnrollmentUrl']}") public void
	 * setCREATE_INDIVIDUAL_ENROLLMENT_URL(String cREATE_INDIVIDUAL_ENROLLMENT_URL) {
	 * CREATE_INDIVIDUAL_ENROLLMENT_URL = cREATE_INDIVIDUAL_ENROLLMENT_URL; } public static String
	 * GET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL;
	 * @Value("#{configProp['enrollment.getIndividualPlanSelectionDetailsUrl']}") public void
	 * setGETINDIVIDUAL_PLAN_SELECTION_DETAILS_URL(String gET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL)
	 * { GET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL = gET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL; }
	 * public static String GET_INDIVIDUAL_PLAN_SELECTION_URL;
	 * @Value("#{configProp['enrollment.getIndividualPlanSelectionUrl']}") public void
	 * setGETINDIVIDUAL_PLAN_SELECTION_URL(String gET_INDIVIDUAL_PLAN_SELECTION_URL) {
	 * GET_INDIVIDUAL_PLAN_SELECTION_URL = gET_INDIVIDUAL_PLAN_SELECTION_URL; } public static String
	 * GET_VERIFY_PIN_URL;
	 * @Value("#{configProp['enrollment.getVerifyUrl']}") public void setGET_VERIFY_PIN_URL(String
	 * gET_VERIFY_PIN_URL) { GET_VERIFY_PIN_URL = gET_VERIFY_PIN_URL; } public static String
	 * FIND_BY_ORDERID_URL;
	 * @Value("#{configProp['enrollment.findbyorderidUrl']}") public void
	 * setFIND_BY_ORDERID_URL(String fIND_BY_ORDERID_URL) { FIND_BY_ORDERID_URL =
	 * fIND_BY_ORDERID_URL; } public static String SEARCH_ENROLLEE_URL;
	 * @Value("#{configProp['enrollment.searchEnrolleeUrl']}") public void
	 * setSEARCH_ENROLLEE_URL(String sEARCH_ENROLLEE_URL) { SEARCH_ENROLLEE_URL =
	 * sEARCH_ENROLLEE_URL; } public static String FIND_ENROLLEE_BY_ID_URL;
	 * @Value("#{configProp['enrollment.findEnrolleeByIdUrl']}") public void
	 * setFIND_ENROLLEE_BY_ID_URL(String fIND_ENROLLEE_BY_ID_URL) { FIND_ENROLLEE_BY_ID_URL =
	 * fIND_ENROLLEE_BY_ID_URL; }
	 */

	/*
	 * public static String FIND_ENROLLMENTS_BY_ISSUER_URL;
	 * @Value("#{configProp['enrollment.findEnrollmentsByIssuerUrl']}") public void
	 * setFIND_ENROLLMENTS_BY_ISSUER_URL(String fIND_ENROLLMENTS_BY_ISSUER_URL) {
	 * FIND_ENROLLMENTS_BY_ISSUER_URL = fIND_ENROLLMENTS_BY_ISSUER_URL; } public static String
	 * FIND_TAX_FILING_DATE_FOR_YEAR_URL;
	 * @Value("#{configProp['enrollment.findTaxFilingDateForYearUrl']}") public void
	 * setFIND_TAX_FILING_DATE_FOR_YEAR_URL(String fIND_TAX_FILING_DATE_FOR_YEAR_URL) {
	 * FIND_TAX_FILING_DATE_FOR_YEAR_URL = fIND_TAX_FILING_DATE_FOR_YEAR_URL; }
	 */

	/*
	 * public static boolean DISPLAY_SIGNATURE_PIN;
	 * @Value("#{configProp['enrollment.displaySignaturePIN']}") public void
	 * setDISPLAYSIGNATUREPIN(boolean DISPLAYSIGNATUREPIN) { DISPLAY_SIGNATURE_PIN =
	 * DISPLAYSIGNATUREPIN; }
	 */
	/*
	 * public static boolean DISPLAY_FILE_TAX_RETURN;
	 * @Value("#{configProp['enrollment.displayFileTaxReturn']}") public void
	 * setDISPLAYFILETAXRETURN(boolean DISPLAYFILETAXRETURN) { DISPLAY_FILE_TAX_RETURN =
	 * DISPLAYFILETAXRETURN; }
	 */

	/*
	 * public static boolean DISPLAY_CONFIRMATION_DISCLAIMERS;
	 * @Value("#{configProp['enrollment.displayConfirmationDisclaimers']}") public void
	 * setDISPLAYCONFIRMATIONDISCLAIMERS(boolean DISPLAYCONFIRMATIONDISCLAIMERS) {
	 * DISPLAY_CONFIRMATION_DISCLAIMERS = DISPLAYCONFIRMATIONDISCLAIMERS; }
	 */
	/*
	 * public static boolean DISPLAY_CONFIRMATION_MAKING_CHANGES_TO_YOUR_PLANS;
	 * @Value("#{configProp['enrollment.displayConfirmationMakingChangesToYourPlans']}") public void
	 * setDISPLAYCONFIRMATIONMAKINGCHANGESTOYOURPLANS(boolean
	 * DISPLAYCONFIRMATIONMAKINGCHANGESTOYOURPLANS) {
	 * DISPLAY_CONFIRMATION_MAKING_CHANGES_TO_YOUR_PLANS =
	 * DISPLAYCONFIRMATIONMAKINGCHANGESTOYOURPLANS; }
	 */

	public static String OUTPUTPATH;

	@Value("#{configProp.outputpath}")
	public void setOUTPUTPATH(String oUTPUTPATH) {
		OUTPUTPATH = oUTPUTPATH;
	}

	/*****************************************************************************************************/
	/*********************** Enrollment configuration properties *****************************************/
	/*****************************************************************************************************/

	/*****************************************************************************************************/
	/*********************** GHIX SERFF configuration properties *************************/
	/*****************************************************************************************************/
	
	public static String SERFF_CSR_HOME_PATH;
	
	@Value("#{configProp.SERFF_CSR_homePath}")
	public void setSerffCsrHomePath(String serfCsrHomePath) {
		SERFF_CSR_HOME_PATH = serfCsrHomePath;
	}
	
	public static String FTP_SERVER_UPLOAD_SUBPATH;
	
	@Value("#{configProp['Serff_Ftp_Server_Subpath']}")
	public void setFTPServerSubpath(String ftpServerPath) {
		FTP_SERVER_UPLOAD_SUBPATH = ftpServerPath;
	}
	
	public static String SERVICE_SERVER_LIST;

	@Value("#{configProp['SERFF_ServiceServerList']}")
	public void setServiceServerList(String serverList) {
		SERVICE_SERVER_LIST = serverList;
	}

	/*****************************************************************************************************/
	/*********************** Common GHix Application String Constants ************************************/
	/*****************************************************************************************************/
	public static String MAXUPLOADSIZEEXCEEDED_EXCEPTION = "Maxuploadsizeexceededexception";
	public static final String ANONYMOUS_PERSON_LIST = "ANONYMOUS_PERSON_LIST";
	public static final String INDIVIDUAL_PERSON_LIST = "INDIVIDUAL_PERSON_LIST";
	public static final String ACN_INDIV_PERSON_LIST = "ACN_INDIV_PERSON_LIST";
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	public static final String REQUIRED_DATE_FORMAT_WITH_OUT_SLASHES = "MMddyyyy";
	public static final String DISPLAY_DATE_FORMAT = "MMM dd, yyyy";
	public static final String DISPLAY_DATE_FORMAT1 = "yyyy-MM-dd hh:mm:ss";
	public static final String FILENAME_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String GROUP_ENRL_DATE_FORMAT = "yyMMdd";
	public static final String GROUP_ENRL_TIME_FORMAT = "HHmmss";
	public static final String CMS_ENRL_TIME_FORMAT = "HHmmssSSS";
	public static final String EMP_ENRL_DATE_FORMAT = "yyyymmdd";
	public static final String ANONYMOUS_INDIVIDUAL_TYPE = "ANONYMOUS_INDIVIDUAL_TYPE";
	public static final String HOUSEHOLD_ID = "HOUSEHOLD_ID";
	public static final String SYS_DATE = "SYS_DATE";
	public static final String SHOP_EMPLOYEE_CONTRIBUTION = "EMPLOYEE_CONTRIBUTION";
	public static final String SHOP_DEPENDENT_CONTRIBUTION = "DEPENDENT_CONTRIBUTION";
	public static final String EMAIL_TEMPLATE_DATE_FORMAT ="MMMMM dd, yyyy";

	public static final String INDIVIDUAL_ORDER_ID = "INDIVIDUAL_ORDER_ID";
	public static final String COVERAGE_START = "COVERAGE_START";
	public static final String COVERAGE_END = "COVERAGE_END";
	public static final String HOUSEHOLD_PREFERENCES = "HOUSEHOLD_PREFERENCES";
	public static final String MEDICAL_CONDITIONS = "MEDICAL_CONDITIONS";

	public static final String HOUSEHOLD = "HOUSEHOLD";
	public static final String HLD_ENROLLMENT_TYPE = "ENROLLMENT_TYPE";
	public static final String HLD_APTC = "APTC";
	public static final String HLD_CSR = "CSR";
	public static final String HLD_PRGM_TYPE = "ProgramType";

	public static final String CURRENT_GROUP_ID = "CURRENT_GROUP_ID";
	public static final String GROUP_LIST = "GROUP_LIST";

	public static final Integer PAGE_SIZE = 10;

	public static final String UNDERSCORE = "_";
	public static final String DOT = ".";
	public static final String FRONT_SLASH = "/";
	public static final String ISSUER_CERTI_FOLDER = "certificates";
	public static final String ISSUER_SIGN_APPLICATION_STATEMENT = "I have reviewed the issuer application approve of it to be listed on the exchange.";

	public static final String TOTAL_TAX_CREDIT = "TOTAL_TAX_CREDIT";

	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_PARTIAL_SUCCESS = "PARTIAL_SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final String INCORRECT_TYPE = "INCORRECT TYPE";

	public static final String CERTI_FOLDER = "certificates";
	public static final String BENEFITS_FOLDER = "benefits";
	public static final String RATES_FOLDER = "rates";
	public static final String AUTHORITY_FOLDER = "authority";
	public static final String DISCLOSURES_FOLDER = "disclosures";
	public static final String MARKETING_FOLDER = "marketing";
	public static final String ACCREDITATION_FOLDER = "accreditation";
	public static final String ADD_INFO_FOLDER = "additional_info";
	public static final String BROCHURE_FOLDER = "brochures";
	public static final String PLAN_SUPPORT_DOC_FOLDER = "plan_support";
	public static final String ADD_SUPPORT_FOLDER = "support";
	public static final String ORGANIZATION_FOLDER = "organization";

	public static final String ENROLLMENT_TYPE_SHOP = "24";
	public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	public static final String USER_NAME_CARRIER = "carrier@ghix.com";
	public static final String USER_NAME_EXCHANGE = "exchange@ghix.com";
	public static final String CA_834_INDV = "CA_834_INDV";
	public static final String CA_834_SHOP = "CA_834_SHOP";
	public static final String NM_834_INDV = "NM_834_INDV";
	public static final String NM_834_SHOP = "NM_834_SHOP";
	public static final String CA_834_RECONSHOP = "CA_834_RECONSHOP";
	public static final String CA_834_RECONINDIV = "CA_834_RECONINDV";
	public static final String NM_834_RECONSHOP = "NM_834_RECONSHOP";
	public static final String NM_834_RECONINDIV = "NM_834_RECONINDV";
	public static final String CA_820_SHOP = "CA_820_SHOP";
	public static final String CA_820_INDV = "CA_820_INDV";
	public static final String INDIVIDUAL = "Individual";
	public static final String INBOUND = "Inbound";
	public static final String OUTBOUND = "Outbound";
	public static final String SHOP = "SHOP";
	public static final String NM_820_SHOP = "NM_820_SHOP";
	public static final String EDI_834_SHOP = "834_SHOP";
	public static final String EDI_834_INDV = "834_INDV";
	public static final String SSAP_APPLICATION_ID = "ssapApplicationId";
	public static final int PAYMENT_METHOD_MIGRATION_LIMIT = 100;
	public static final String EMPTY_OR_NULL = "OrderItem Id Null or Empty";
	public static final String UPDATE_FAILED = "Update Failed for this OrderItem Id";
	public static final String UPDATE_SUCCESS = "OrderItem Status Updated Successfully ";

	public static final String ENGLSIH="ENGLISH";
	public static final String SPANISH="SPANISH";
	//public static String BRANCH_NAME;

	public static enum FileType {
		BENEFITS_FILE, RATES_FILE, ACTUARIAL_CERTIFICATES, AUTHORITY, MARKETING, DISCLOSURES, ACCREDITATION, ADDITIONAL_INFO, ADDITIONAL_SUPPORT, ORGANIZATION, CERTIFICATES, BROCHURE, PROVIDER
		// to add provider filetypes
	}

	/*public static String TARGET_SERVER_URL;

	@Value("#{configProp.targetServerUrl}")
	public void setTARGET_SERVER_URL(String target_server_url) {
		TARGET_SERVER_URL = target_server_url;
	}
*/
	public static String SMARTYSTREET_HTML_TOKEN;

	@Value("#{configProp['smartystreet.htmltoken']}")
	public void setSMARTYSTREET_HTML_TOKEN(String smartystreet_html_token) {
		SMARTYSTREET_HTML_TOKEN = smartystreet_html_token;
	}
/*
	@Value("#{configProp.BRANCH_NAME}")
	public void setBranchName(String branchName) {
		BRANCH_NAME = branchName;
	}*/

	/**
	 * SERFF Exception/Error Constants.
	 */
	public static final String DATA_ADMIN_DATA = "DATA_ADMIN_DATA";
	public static final String DATA_ECP_DATA = "DATA_ECP_DATA";
	public static final String DATA_BENEFITS = "DATA_BENEFITS";
	public static final String DATA_PRESCRIPTION_DRUG = "DATA_PRESCRIPTION_DRUG";
	public static final String DATA_SERVICE_AREA = "DATA_SERVICE_AREA";
	public static final String DATA_RATING_TABLE = "DATA_RATING_TABLE";
	public static final String DATA_RATING_RULES = "DATA_RATING_RULES";
	public static final String DATA_NETWORK_ID = "DATA_NETWORK_ID";
	public static final String DATA_DENTAL_BENEFITS = "DATA_DENTAL_BENEFITS";
	public static final String DATA_SBC = "sbc";
	public static final String PRESCRIPTION_DRUG_FILENAME_SUFFIX = "drug.xml";
	public static final String NETWORK_ID_FILENAME_SUFFIX = "network.xml";
	public static final String BENEFITS_FILENAME_SUFFIX = "plans.xml";
	public static final String RATING_TABLE_FILENAME_SUFFIX = "rate.xml";
	public static final String SERVICE_AREA_FILENAME_SUFFIX = "servicearea.xml";
	public static final long FTP_UPLOAD_MAX_SIZE = 314572800;
	public static final String FTP_UPLOAD_PLAN_PATH = "/uploadPlans/";
	public static final String FTP_BROCHURE_PATH = "dropbox";
	public static final String FTP_BROCHURE_SUCCESS_PATH = "success/ID_";
	public static final String FTP_BROCHURE_ERROR_PATH = "error/ID_";

	public static String getFTPUploadPlanPath() {
		return FTP_SERVER_UPLOAD_SUBPATH + FTP_UPLOAD_PLAN_PATH;
	}
	
	/** Client Exceptions */
	public static final Integer EXCEPTION_REQUIRED_FIELDS_MISSING_CODE = 1001;
	public static final String EXCEPTION_REQUIRED_FIELDS_MISSING_DESC = "Required fields missing.";

	public static final Integer EXCEPTION_INVALID_PARAMETER_CODE = 1002;
	public static final String EXCEPTION_INVALID_PARAMETER_DESC = "Invalid parameter.";

	public static final Integer EXCEPTION_NO_OBJECTS_WERE_FOUND_CODE = 1003;
	public static final String EXCEPTION_NO_OBJECTS_WERE_FOUND_DESC = "No objects were found.";

	public static final Integer EXCEPTION_FIELDS_CONTAIN_NO_DATA_CODE = 1004;
	public static final String EXCEPTION_FIELDS_CONTAIN_NO_DATA_DESC = "The fields contain no data.";

	public static final Integer EXCEPTION_OPERATION_NOT_SUPPORTED_CODE = 1005;
	public static final String EXCEPTION_OPERATION_NOT_SUPPORTED_DESC = "The operation is not implemented and should not be called.";

	/** Server Error */
	public static final Integer EXCEPTION_INTERNAL_DATABASE_ERROR_CODE = 2001;
	public static final String EXCEPTION_INTERNAL_DATABASE_ERROR_DESC = "Internal database error.";

	public static final Integer EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE = 2002;
	public static final String EXCEPTION_INTERNAL_APPLICATION_ERROR_DESC = "Internal application error.";

	public static final Integer EXCEPTION_IO_ERROR_CODE = 2003;
	public static final String EXCEPTION_IO_ERROR_DESC = "IO error occurred.";

	public static final Integer EXCEPTION_NULL_POINTER_EXCEPTION_CODE = 2004;
	public static final String EXCEPTION_NULL_POINTER_EXCEPTION_DESC = "Null pointer exception.";

	/**********************************/

	public static String PLAN_STATUS_LOADED = "LOADED";

	public static String PRESCREEN_FB_SHARE_URL="#";
	public static String PRESCREEN_TWITTER_SHARE_URL="#";
	public static String PRESCREEN_GPLUS_SHARE_URL="#";

	/*****************************************************************************************************/
	/*********************** GHIX SERFF Web Service configuration properties *************************/
	/*****************************************************************************************************/

	public static String GHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL;

	@Value("#{configProp['ghix_serff.plan.status.update']}")
	public void setGHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL(String gHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL) {
		GHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL = gHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL;
	}

	public static final Integer PLAN_NOT_FOUND_EHEALTH_ERROR_CODE = 4001;
	public static final String PLAN_NOT_FOUND_EHEALTH_DESC = "No Plans found for eHealth";
	public static final Integer EHEALTH_SERVICE_DOWN = 4002;
	public static final Integer EHEALTH_INTERNAL_ERROR = 4003;

	public static final Integer PLAN_NOT_FOUND_QUOTIT_ERROR_CODE = 4004;
	public static final String PLAN_NOT_FOUND_QUOTIT_DESC = "No Plans found for quotit";
	public static final Integer QUOTIT_SERVICE_DOWN = 4005;
	public static final Integer QUOTIT_INTERNAL_ERROR = 4006;

	public static final Integer PLAN_NOT_FOUND_UHC_ERROR_CODE = 4007;
	public static final String PLAN_NOT_FOUND_UHC_DESC = "No Plans found for UHC";
	public static final Integer UHC_SERVICE_DOWN = 4008;
	public static final Integer UHC_INTERNAL_ERROR = 4009;

	public static final Integer INVALID_STATE = 4010;
	public static final Integer INVALID_ZIPCODE = 4011;
	public static final Integer SQL_EXCEPTION = 4012;

	public static final Integer FAULT_EHEALTH = 4013;
	public static final Integer FAULT_UHC = 4014;
	public static final Integer FAULT_QUOTIT = 4015;
	
	public static final Integer INVALID_ISSUER = 4020;

	public static final Integer TEASER_PLAN_EXCEPTION_CODE = 5001;
	public static final String TEASER_PLAN_EXCEPTION_DESC = "Error occured while retrieving second lowest silver non tobacco plan";
	public static final Integer PLAN_NOT_FOUND = 5002;
	public static final String PLAN_NOT_FOUND_DESC = "No Silver Plan found from Teaser API";
		
	public static final String PHIX = "PHIX";
	public static final String CA = "STATE";
	public static final String NM = "NM";
	public static final String MS = "MS";

	public static final Integer TEASER_PLAN_SLSNTP_ERRORCODE = 5001;
	public static final Integer TEASER_PLAN_SLSTP_ERRORCODE = 5003;
	public static final Integer TEASER_PLAN_LSP_ERRORCODE = 5004;
	public static final Integer TEASER_PLAN_LBP_ERRORCODE = 5005;

	public static final String TEASER_PLAN_SLSNTP_ERRORCODE_DESC = "Error occured while retrieving second lowest silver non tobacco plan";

	public static final String TEASER_PLAN_SLSTP_ERRORCODE_DESC = "Error occured while retrieving second lowest silver tobacco plan";

	public static final String TEASER_PLAN_LSP_ERRORCODE_DESC = "Error occured while retrieving lowest silver tobacco plan";

	public static final String TEASER_PLAN_LBP_ERRORCODE_DESC = "Error occured while retrieving lowest bronze plan";


	/***************************************************************************************/
	/********************** Plan Display properties *********************************/
	/***************************************************************************************/
	public static final String SUBSCRIBER_DATA = "subscriberData";
	public static final String SUBSCRIBER_MEMBER_ID = "subscriberMemberId";
	public static final String SUBSCRIBER_MEMBER_COUNTY = "subscriberMemberCounty";
	public static final String SUBSCRIBER_MEMBER_ZIP = "subscriberMemberZip";

	public static final String STATE_CODE = "stateCode";
	public static final String EXCHANGE_TYPE = "exchangeType";
	
	public static final String MIN_PREMIUM_PER_MEMBER = "planSelection.MinPremiumPerMember";
	

	public static final Integer ELIGIBILITY_RESPONSE_ERRORCODE = 6001;

	public static final String ELIGIBILITY_RESPONSE_ERRORCODE_DESC = "Error occured while retrieving Eligibility Response";
	
	public static String ANON_APPLY_URL;
	
	public static final String V_HIGH_MEDICAL_VAL = "vHighMedicalVal";

	public static final String HIGH_MEDICAL_VAL = "highMedicalVal";

	public static final String MODERATE_MEDICAL_VAL = "moderateMedicalVal";

	public static final String LOW_MEDICAL_VAL = "lowMedicalVal";

	public static final String V_HIGH_DRUG_USE_VAL = "vHighDrugUseVal";

	public static final String HIGH_DRUG_USE_VAL = "highDrugUseVal";

	public static final String MODERATE_DRUG_USE_VAL = "moderateDrugUseVal";

	public static final String LOW_DRUG_USE_VAL = "lowDrugUseVal";
	
	@Value("#{configProp['plandisplay.anonymousApplyURL']}")
	public void setANON_APPLY_URL(String aNON_APPLY_URL) {
		ANON_APPLY_URL = aNON_APPLY_URL;
	}
	
	// two factor authentication properties and keys
	/*public static boolean IS_ENABLE2FACT_AUTH;	
	public static String TWO_FACTOR_AUTHENTICATION_ROLES;
	public static String DUO_SECRET_KEY;
	public static String DUO_INTEGRATION_KEY;
	public static String DUO_APPLICATION_SECRET_KEY;
	public static String DUO_HOSTNAME;
	
	// HIX-29686 - Darshan Hardas : Server changes as new license is generated for duo security
	@Value("#{configProp['duoHostName']}")
	public void setDUO_HOSTNAME(String dUO_HOSTNAME) {
		DUO_HOSTNAME = dUO_HOSTNAME;
	}

	@Value("#{configProp['isEnable2FactAuth']}")
	public void setIS_ENABLE2FACT_AUTH(boolean iS_ENABLE2FACT_AUTH) {
		IS_ENABLE2FACT_AUTH = iS_ENABLE2FACT_AUTH;
	}
	
	@Value("#{configProp['twoFactorAuthenticationRoles']}")
	public void setTWO_FACTOR_AUTHENTICATION_ROLES(String two_Factor_Authenticaton_Roles) {
		TWO_FACTOR_AUTHENTICATION_ROLES = two_Factor_Authenticaton_Roles;
	}
	
	@Value("#{configProp['duoSecretKeyForTwoFactAuth']}")
	public void setDUO_SECRET_KEY(String duo_Secret_Key) {
		DUO_SECRET_KEY = duo_Secret_Key;
	}
	
	@Value("#{configProp['duoIntegrationKeyForTwoFactAuth']}")
	public void setDUO_INTEGRATION_KEY(String duo_Integration_Key) {
		DUO_INTEGRATION_KEY = duo_Integration_Key;
	}
	
	@Value("#{configProp['duoApplicationSecretKeyForTwoFactAuth']}")
	public void setDUO_APPLICATION_SECRET_KEY(String duo_Application_Secret_Key) {
		DUO_APPLICATION_SECRET_KEY = duo_Application_Secret_Key;
	}
*/

	// FFM Integration properties
	//public static String DEFAULT_NPN;
	/*
	 * Get the default NPN for getinsured.com to be used with FFM integration
	 */
	/*@Value("#{configProp['defaultNPN:']}")
	public void setDEFAULT_NPN(String defaultNPN) {
		DEFAULT_NPN = defaultNPN;
	}*/
	//Cybersource Merchant ID required for transactions
	
	/*public static String MERCHANTID;
	
	@Value("#{configProp.merchantID}")
	public  void setMERCHANTID(String mERCHANTID) {
		MERCHANTID = mERCHANTID;
	}*/
	

	/***************************************************************************************/
	/********************** ERROR CODES FOR FINANCE MODULE *********************************/
	/***************************************************************************************/
	public static final String FIN_20012 = "FINANCE-20012";
	public static final String FIN_20013 = "FINANCE-20013";
	public static final String FIN_20014 = "FINANCE-20014";
	public static final String FIN_20015 = "FINANCE-20015";
	public static final String FIN_20016 = "FINANCE-20016";
	public static final String FIN_20017 = "FINANCE-20017";
	public static final String FIN_20018 = "FINANCE-20018";
	public static final String FIN_20019 = "FINANCE-20019";
	
	public static final String ERROR_SUFFIX_FIN = "FINANCE-";
	
	public static String STRENNUS_DATA_DIR;
	public static String STRENNUS_JOB_LOG_DIR;
	
	@Value("#{configProp.strennus_data_dir}")
	public void setSTRENNUS_DATA_DIR(String strennusDataDir) {
		STRENNUS_DATA_DIR = System.getProperty("GHIX_HOME")+strennusDataDir;
	}
	
	@Value("#{configProp.strennus_job_log_dir}")
	public void setSTRENNUS_JOB_LOG_DIR(String strennusJobLogDir){
		STRENNUS_JOB_LOG_DIR = strennusJobLogDir;
	}
	
	//public static final String USER_NAME = "userName";
	
	/****************************************************************************************/
	/********************* HUB Configuration ************************************************/
	/****************************************************************************************/
	@Value("#{configProp.hubKeyStoreDir}")
	public void setHubKeyStoreLocation(String hubKeyStoreLocation){
		HUB_KEY_STORE_LOCATION = hubKeyStoreLocation;
	}
	
	@Value("#{configProp.hubKeyStoreFile}")
	public void setHubKeyStoreFile(String hubKeyStoreFile){
		HUB_KEY_STORE_FILE = hubKeyStoreFile;
	}
	
	@Value("#{configProp.hubServiceDefinitionDir}")
	public void setServiceDefinitionDirectory(String serviceDefDir){
		HUB_SERVICE_DEFINITION_DIR = serviceDefDir;
	}
	
	@Value("#{configProp.hubKeyStorePass}")
	public void setHubKeyStorePassword(String pass){
		HUB_KEYSTORE_PASS = pass;	
	}
	
	/*******************************************************************************************
	 ******************CA Provider Search related configuration properties
	 *******************************************************************************************/
	public static String PROVIDER_TEXONOMY_FILE_NAME;
	@Value("#{configProp['provider.enclarity.texonomy.file']}")
	public void setTexonomyFileName(String texonomyFileName) {

		if (StringUtils.isNotBlank(texonomyFileName)) {
			PROVIDER_TEXONOMY_FILE_NAME = texonomyFileName;
		}
		else {
			PROVIDER_TEXONOMY_FILE_NAME = "texonomy_speciality.csv";
		}
	}
	
	public static String ENCLARITY_FACILITY_FILE_PREFIX;
	@Value("#{configProp['provider.enclarity.facility.file']}")
	public void setEnclarityFacilitiesDataFile(
			String enclarityFacilitiesDataFile) {

		if (StringUtils.isNotBlank(enclarityFacilitiesDataFile)) {
			ENCLARITY_FACILITY_FILE_PREFIX = enclarityFacilitiesDataFile;
		}
		else {
			ENCLARITY_FACILITY_FILE_PREFIX = "calhx_FAC_dedupe";
		}
	}

	public static String ENCLARITY_DATA_DIR;
	@Value("#{configProp['provider.enclarity.data.dir']}")
	public void setEnclarityDataDir(String enclarityDataDir) {

		if (StringUtils.isNotBlank(enclarityDataDir)) {
			ENCLARITY_DATA_DIR = enclarityDataDir;
		}
		else if (StringUtils.isNotBlank(ENCLARITY_METADATA_DIR)) {
			ENCLARITY_DATA_DIR = ENCLARITY_METADATA_DIR;
		}
	}
	public static String ENCLARITY_INDIVIDUAL_FILE_PREFIX;
	@Value("#{configProp['provider.enclarity.individual.file']}")
	public void setEnclarityIndividualDataFile(
			String enclarityIndividualDataFile) {

		if (StringUtils.isNotBlank(enclarityIndividualDataFile)) {
			ENCLARITY_INDIVIDUAL_FILE_PREFIX = enclarityIndividualDataFile;
		}
		else {
			ENCLARITY_INDIVIDUAL_FILE_PREFIX = "calhx_IDV_dedupe";
		}
	}

	public static String ENCLARITY_METADATA_DIR;
	@Value("#{configProp['provider.enclarity.metadata.dir']}")
	public void setEnclarityMetaDataDir(String enclarityDataDir) {
		ENCLARITY_METADATA_DIR = enclarityDataDir;

		if (StringUtils.isBlank(ENCLARITY_DATA_DIR)) {
			ENCLARITY_DATA_DIR = ENCLARITY_METADATA_DIR;
		}
	}
	public static String PROVIDER_FIELDS_METADATA;
	@Value("#{configProp['provider.enclarity.metadata.file']}")
	public void setFieldMetaDataFileName(String fieldMetaDataFileName) {

		if (StringUtils.isNotBlank(fieldMetaDataFileName)) {
			PROVIDER_FIELDS_METADATA = fieldMetaDataFileName;
		}
		else {
			PROVIDER_FIELDS_METADATA = "ProviderMetadata.xml";
		}
	}
	
	public static String PROVIDER_ZIPCODE_FILE;
	@Value("#{configProp['provider.ghixca.zipcode.file']}")
	public void setZipCodeFileName(String zipCodeFileName) {

		if (StringUtils.isNotBlank(zipCodeFileName)) {
			PROVIDER_ZIPCODE_FILE = zipCodeFileName;
		}
		else {
			PROVIDER_ZIPCODE_FILE = "zipcodes_geo_map.csv";
		}
	}
	
	public static String ZIPCODE_FILE_DIR;
	@Value("#{configProp['zipcode.file.dir']}")
	public void setZipCodeFileDir(String zipCodeFileDir) {
		ZIPCODE_FILE_DIR = zipCodeFileDir;
	}
	
	public static String ZIPCODE_FILE;
	@Value("#{configProp['zipcode.file']}")
	public void setZipCodeFile(String zipCodeFile) {
		ZIPCODE_FILE = zipCodeFile;
	}
	
	
	
	
	
	
	
	
	/*******************************************************************************************/
	/********************************** FINANCE REST CALL CONSTANT *****************************/
	/*******************************************************************************************/
	public static final String FIN_CANCEL_INV_EMPLOYER_ID_KEY = "FIN_CANCEL_INV_EMPLOYER_ID";
	public static final String FIN_CANCEL_INV_INVOICE_TYPE_KEY = "FIN_CANCEL_INV_INVOICE_TYPE";
	
	/*****************************************************************************************/
	/********************* Payment Redirect SAML Assertion related constants *****************/
	/*****************************************************************************************/
	public static final String SAML_RESPONSE                  = "SAMLResponse";
	public static final String SAML_RESPONSE_XML                  = "SAML_ASSERTION_RES_XML";
	
	public static final class FinancePaymentMethodResponse
	{
		public static final String PAYMENT_METHOD_LIST_KEY = "PAYMENT_METHOD_LIST";
		public static final String RECORD_COUNT_KEY = "RECORD_COUNT";
	}	

	@Value("#{configProp['database.type']}")
	public void setDatabaseType(String dbType){
		DATABASE_TYPE = dbType;
	}
	
	public static final String RELATIONSHIP_CODE_DEPENDENT = "DEPENDENT";
	public static final String RELATIONSHIP_CODE_SPOUSE = "SPOUSE";
	public static final String RELATIONSHIP_CODE_CHILD = "CHILD";
 	public static final String RELATIONSHIP_CODE_SELF = "SELF";
	public static final Map<String, String> realtionWithSubscriberMap;
	static {
		realtionWithSubscriberMap = new HashMap<String, String>();

		realtionWithSubscriberMap.put("01", RELATIONSHIP_CODE_SPOUSE);
		realtionWithSubscriberMap.put("03", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("04", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("05", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("06", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("07", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("08", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("09", RELATIONSHIP_CODE_CHILD);
		realtionWithSubscriberMap.put("10", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("11", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("12", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("13", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("14", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("15", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("16", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("17", RELATIONSHIP_CODE_CHILD);
		realtionWithSubscriberMap.put("18", RELATIONSHIP_CODE_SELF);
		realtionWithSubscriberMap.put("19", RELATIONSHIP_CODE_CHILD);
		realtionWithSubscriberMap.put("23", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("24", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("25", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("26", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("31", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("38", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("53", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("60", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("D2", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("G8", RELATIONSHIP_CODE_DEPENDENT);
		realtionWithSubscriberMap.put("G9", RELATIONSHIP_CODE_DEPENDENT);
	}
}
