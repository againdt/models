package com.getinsured.hix.dto.plandisplay;

import java.util.List;

public class APTCCalculatorResponse {

	private List<APTCPlanResponseDTO> plans;
		
	public List<APTCPlanResponseDTO> getPlans() {
		return plans;
	}
	public void setPlans(List<APTCPlanResponseDTO> plans) {
		this.plans = plans;
	}	
}
