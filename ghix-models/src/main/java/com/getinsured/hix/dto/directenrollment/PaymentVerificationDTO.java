package com.getinsured.hix.dto.directenrollment;

public class PaymentVerificationDTO extends PaymentDTO {
	protected String guid;
	protected String fname;
	protected String lname;
	protected String street;
	protected String city;
	protected String state;
	protected String zip;
	protected String email;
	protected String phone;
	protected double amount;

	public PaymentVerificationDTO() {
	}

	public PaymentVerificationDTO(PaymentDTO payment) {
		this.bank = payment.getBank();
		this.creditcard = payment.getCreditcard();
		this.payee = payment.getPayee();
		this.payeeCheck = payment.getPayeeCheck();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		
		sb.append(", lname {" + lname + "}");
		sb.append(", street {" + street + "}");
		sb.append(", city {" + city + "}");
		sb.append(", state {" + state + "}");
		sb.append(", zip {" + zip + "}");
		sb.append(", email {" + email + "}");
		sb.append(", phone {" + phone + "}");
		sb.append(", amount {" + amount + "}");
		
		return sb.toString();
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
