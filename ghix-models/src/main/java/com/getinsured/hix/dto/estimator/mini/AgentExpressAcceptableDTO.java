package com.getinsured.hix.dto.estimator.mini;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class AgentExpressAcceptableDTO {

	private String firstname;
	private String lastname;
	private String source;
	private String email;
	private String address;
	private String zip;
	private String city;
	private String state;
	private String phone;
	private String status;
	private Boolean archived;
    private String annualIncome;
    private String hraContributions;
    private String externalId;

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	private List<AgentExpressMemberDTO> people;

	public AgentExpressAcceptableDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getArchived() {
		return archived;
	}

	public void setArchived(Boolean archived) {
		this.archived = archived;
	}

	public String getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}

	public String getHraContributions() {
		return hraContributions;
	}

	public void setHraContributions(String hraContributions) {
		this.hraContributions = hraContributions;
	}

	public List<AgentExpressMemberDTO> getPeople() {
		return people;
	}

	public void setMemberInPeopleList(AgentExpressMemberDTO agentExpressMemberDTO) {

		if (null == this.people) {
			this.people = new ArrayList<AgentExpressMemberDTO>();
		}
		people.add(agentExpressMemberDTO);
	}
}
