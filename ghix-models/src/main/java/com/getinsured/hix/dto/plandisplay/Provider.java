package com.getinsured.hix.dto.plandisplay;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class Provider implements JSONAware {
	
	private final String DR_PREFIX = "Dr. ";
	
	private String groupKey;
	private String name;
	private String providerType;
	private List<ProviderAddress> providerAddress;
	private String specialty;
	private float distanceFromUserLocation;
	private String languages;
	private List<String> networkId;
	private String networkTierId;
	private String acceptingNewPatients;
	private String medicalGroupAffiliation;
	private String hospitalAffiliation;
	private String accessibilityCodes;
	private String locationId;
	private String sanctionStatus;
	private String providerTypeIndicator;
	private String strenuus_id;
	private String suffix;
	private List<String> practicePhones;
	private List<String> phones;
	private List<String> identifiers;
	
	private Map<String, Integer> specialties = new HashMap<String, Integer>();
	
	
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProviderType() {
		return providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public List<ProviderAddress> getProviderAddress() {
		return providerAddress;
	}
	public void setProviderAddress(List<ProviderAddress> providerAddress) {
		this.providerAddress = providerAddress;
	}
	public String getSpecialty() {
		return specialty;
	}
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public float getDistanceFromUserLocation() {
		return distanceFromUserLocation;
	}

	public void setDistanceFromUserLocation(float distanceFromUserLocation) {
		this.distanceFromUserLocation = distanceFromUserLocation;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}
	
	public List<String> getNetworkId() {
		return networkId;
	}

	public void setNetworkId(List<String> networkId) {
		this.networkId = networkId;
	}

	public String getNetworkTierId() {
		return networkTierId;
	}

	public void setNetworkTierId(String networkTierId) {
		this.networkTierId = networkTierId;
	}

	public String getAcceptingNewPatients() {
		return acceptingNewPatients;
	}

	public void setAcceptingNewPatients(String acceptingNewPatients) {
		this.acceptingNewPatients = acceptingNewPatients;
	}

	public String getMedicalGroupAffiliation() {
		return medicalGroupAffiliation;
	}

	public void setMedicalGroupAffiliation(String medicalGroupAffiliation) {
		this.medicalGroupAffiliation = medicalGroupAffiliation;
	}

	public String getHospitalAffiliation() {
		return hospitalAffiliation;
	}

	public void setHospitalAffiliation(String hospitalAffiliation) {
		this.hospitalAffiliation = hospitalAffiliation;
	}

	public String getAccessibilityCodes() {
		return accessibilityCodes;
	}

	public void setAccessibilityCodes(String accessibilityCodes) {
		this.accessibilityCodes = accessibilityCodes;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getSanctionStatus() {
		return sanctionStatus;
	}

	public void setSanctionStatus(String sanctionStatus) {
		this.sanctionStatus = sanctionStatus;
	}

	public String getProviderTypeIndicator() {
		return providerTypeIndicator;
	}

	public void setProviderTypeIndicator(String providerTypeIndicator) {
		this.providerTypeIndicator = providerTypeIndicator;
	}

	public String getStrenuus_id() {
		return strenuus_id;
	}

	public void setStrenuus_id(String strenuus_id) {
		this.strenuus_id = strenuus_id;
	}

	//QuickFix for last digit missing
	public Map<String, String> getIdentifiers() {
		
		Map<String, String> idMap = new HashMap<String, String>();
		if(this.identifiers!=null){
			for(String tmpStr:this.identifiers){
				int midIndex = tmpStr.indexOf(":");
				int endIndex = tmpStr.length();
				idMap.put(tmpStr.subSequence(0, midIndex).toString(), tmpStr.subSequence(midIndex+1, endIndex).toString());
			}
		}
		
	  return idMap;
	}

	public void setIdentifiers(List<String> identifiers) {
		this.identifiers = identifiers;
	}

	public Map<String, Integer> getSpecialties() {
		return specialties;
	}

	public void setSpecialties(Map<String, Integer> specialties) {
		this.specialties = specialties;
	}

	public List<String> getPracticePhones() {
		return practicePhones;
	}
	
	public void setPracticePhones(List<String> practicePhones) {
		this.practicePhones = practicePhones;
	}
	
	public List<String> getPhones() {
		return phones;
	}
	
	public void setPhones(List<String> phones) {
		this.phones = phones;
	}
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put("groupKey", this.groupKey);
		obj.put("name", this.name);
		obj.put("languages", this.languages);
		obj.put("specialty", this.specialty);
		obj.put("networkId",this.networkId);
		obj.put("networkTierId",this.networkTierId);
		obj.put("providerAddress", providerAddress);
		obj.put("providerType", this.providerType);
		obj.put("acceptingNewPatients", this.acceptingNewPatients);
		obj.put("medicalGroupAffiliation", this.medicalGroupAffiliation);
		obj.put("hospitalAffiliation", this.hospitalAffiliation);
		obj.put("accessibilityCodes", this.accessibilityCodes);
		obj.put("locationId", this.locationId);
		obj.put("sanctionStatus", this.sanctionStatus);
		obj.put("providerTypeIndicator", this.providerTypeIndicator);
		obj.put("strenuus_id", this.strenuus_id);
		obj.put("suffix", this.suffix);
		obj.put("identifiers", getIdentifiers());
		obj.put("practicePhones",this.practicePhones);

		return obj.toJSONString();
	}
	
}
