package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_RECON_DATA")
public class EnrlReconData  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_data_seq")
	@SequenceGenerator(name = "enrl_recon_data_seq", sequenceName = "ENRL_RECON_DATA_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="FILE_ID")
	private Integer fileId;
	
	@Column(name="ENROLLMENT_ID")
	private Long enrollmentId;
	
	@Column(name="EXCHG_ASSIGNED_SUB_ID")
	private Integer exchgAssignedSubId;
	
	@Column(name="EXCHG_ASSIGNED_MEMBER_ID")
	private Integer exchgAssignedMemberId;
	
	@Column(name="ISSUER_ASSIGNED_POLICY_ID")
	private Long issuerAssignedPolicyId;
	
	@Column(name="ISSUER_ASSIGNED_SUBSCRIBER_ID")
	private String issuerAssignedSubscriberId;
	
	@Column(name="ISSUER_ASSIGNED_MEMBER_ID")
	private String issuerAssignedMemberId;
	
	@Column(name="SUBSCRIBER_INDICATOR")
	private String subscriberIndicator;
	
	@Column(name="QHP_IDENTIFIER")
	private String qhpIdentifier;
	
	@Column(name="BENEFIT_START_DATE")
	private Date benefitStartDate;
	
	@Column(name="BENEFIT_END_DATE")
	private Date benefitEndDate;
	
	@Column(name="TOT_PREMIUM_EFF_DATE")
	private Date totPremiumEffDate;
	
	@Column(name="TOT_PREMIUM_END_DATE")
	private Date totPremiumEndDate;
	
	@Column(name="INDIV_PREMIUM_EFF_DATE")
	private Date indivPremiumEffDate;
	
	@Column(name="INDIV_PREMIUM_END_DATE")
	private Date indivPremiumEndDate;
	
	@Column(name="COVERAGE_YEAR")
	private Integer coverageYear;
	
	@Column(name="IN_DATA")
	private String inData;
	
	@Column(name="RECORD_CODE")
	private Integer recordCode;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Column(name="SNAPSHOT_GEN_STATUS")
	private String reconGenStatus;
	
	@Column(name="SNAPSHOT_GEN_ERROR_MSG")
	private String reconErrorMessage;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Long getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Integer getExchgAssignedSubId() {
		return exchgAssignedSubId;
	}

	public void setExchgAssignedSubId(Integer exchgAssignedSubId) {
		this.exchgAssignedSubId = exchgAssignedSubId;
	}

	public Integer getExchgAssignedMemberId() {
		return exchgAssignedMemberId;
	}

	public void setExchgAssignedMemberId(Integer exchgAssignedMemberId) {
		this.exchgAssignedMemberId = exchgAssignedMemberId;
	}

	public Long getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}

	public void setIssuerAssignedPolicyId(Long issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}

	public String getIssuerAssignedSubscriberId() {
		return issuerAssignedSubscriberId;
	}

	public void setIssuerAssignedSubscriberId(String issuerAssignedSubscriberId) {
		this.issuerAssignedSubscriberId = issuerAssignedSubscriberId;
	}

	public String getIssuerAssignedMemberId() {
		return issuerAssignedMemberId;
	}

	public void setIssuerAssignedMemberId(String issuerAssignedMemberId) {
		this.issuerAssignedMemberId = issuerAssignedMemberId;
	}

	public String getSubscriberIndicator() {
		return subscriberIndicator;
	}

	public void setSubscriberIndicator(String subscriberIndicator) {
		this.subscriberIndicator = subscriberIndicator;
	}

	public String getQhpIdentifier() {
		return qhpIdentifier;
	}

	public void setQhpIdentifier(String qhpIdentifier) {
		this.qhpIdentifier = qhpIdentifier;
	}

	public Date getBenefitStartDate() {
		return benefitStartDate;
	}

	public void setBenefitStartDate(Date benefitStartDate) {
		this.benefitStartDate = benefitStartDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public Date getTotPremiumEffDate() {
		return totPremiumEffDate;
	}

	public void setTotPremiumEffDate(Date totPremiumEffDate) {
		this.totPremiumEffDate = totPremiumEffDate;
	}

	public Date getTotPremiumEndDate() {
		return totPremiumEndDate;
	}

	public void setTotPremiumEndDate(Date totPremiumEndDate) {
		this.totPremiumEndDate = totPremiumEndDate;
	}

	public Date getIndivPremiumEffDate() {
		return indivPremiumEffDate;
	}

	public void setIndivPremiumEffDate(Date indivPremiumEffDate) {
		this.indivPremiumEffDate = indivPremiumEffDate;
	}

	public Date getIndivPremiumEndDate() {
		return indivPremiumEndDate;
	}

	public void setIndivPremiumEndDate(Date indivPremiumEndDate) {
		this.indivPremiumEndDate = indivPremiumEndDate;
	}

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getInData() {
		return inData;
	}

	public void setInData(String inData) {
		this.inData = inData;
	}

	public Integer getRecordCode() {
		return recordCode;
	}

	public void setRecordCode(Integer recordCode) {
		this.recordCode = recordCode;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getReconGenStatus() {
		return reconGenStatus;
	}

	public void setReconGenStatus(String reconGenStatus) {
		this.reconGenStatus = reconGenStatus;
	}

	public String getReconErrorMessage() {
		return reconErrorMessage;
	}

	public void setReconErrorMessage(String reconErrorMessage) {
		this.reconErrorMessage = reconErrorMessage;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
	}

}
