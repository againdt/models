package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class SerffTransferSearchResponseDTO extends GhixResponseDTO {

	private Integer totalRecordCount;
	private Integer pageSize;
	private Integer currentRecordCount;
	private List<SerffTransferInfoDTO> serffTransferInfoList;

	public SerffTransferSearchResponseDTO() {
		super();
	}

	public List<SerffTransferInfoDTO> getSerffTransferInfoList() {
		return serffTransferInfoList;
	}

	public void setSerffTransferInfo(SerffTransferInfoDTO serffTransferInfoDTO) {
		if (CollectionUtils.isEmpty(this.serffTransferInfoList)) {
			this.serffTransferInfoList = new ArrayList<SerffTransferInfoDTO>();
		}
		this.serffTransferInfoList.add(serffTransferInfoDTO);
	}

	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCurrentRecordCount() {
		return currentRecordCount;
	}

	public void setCurrentRecordCount(Integer currentRecordCount) {
		this.currentRecordCount = currentRecordCount;
	}

}
