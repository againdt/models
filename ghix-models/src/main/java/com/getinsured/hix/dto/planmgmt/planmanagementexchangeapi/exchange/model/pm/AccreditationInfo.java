
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccreditationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccreditationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ncqaInfo" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}NcqaInfo"/>
 *         &lt;element name="uracInfo" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}UracInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ncqaInfo",
    "uracInfo"
})
@XmlRootElement(name="accreditationInfo")
public class AccreditationInfo {

    @XmlElement(required = true, nillable = true)
    protected NcqaInfo ncqaInfo;
    @XmlElement(required = true, nillable = true)
    protected UracInfo uracInfo;

    /**
     * Gets the value of the ncqaInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NcqaInfo }
     *     
     */
    public NcqaInfo getNcqaInfo() {
        return ncqaInfo;
    }

    /**
     * Sets the value of the ncqaInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NcqaInfo }
     *     
     */
    public void setNcqaInfo(NcqaInfo value) {
        this.ncqaInfo = value;
    }

    /**
     * Gets the value of the uracInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UracInfo }
     *     
     */
    public UracInfo getUracInfo() {
        return uracInfo;
    }

    /**
     * Sets the value of the uracInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UracInfo }
     *     
     */
    public void setUracInfo(UracInfo value) {
        this.uracInfo = value;
    }

}
