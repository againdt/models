package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class CitizenshipInfo implements Serializable{
	private static final long serialVersionUID = 1L;

	private boolean isUSCitizen;
	
	//MM/dd/yyyy
	private String arrivalDate;
	
	/**
	 * i-94
	 */
	private String insIDNumber;

	public boolean isUSCitizen() {
		return isUSCitizen;
	}

	public void setUSCitizen(boolean isUSCitizen) {
		this.isUSCitizen = isUSCitizen;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getInsIDNumber() {
		return insIDNumber;
	}

	public void setInsIDNumber(String insIDNumber) {
		this.insIDNumber = insIDNumber;
	}
	
	public static CitizenshipInfo getSampleInstance(){
		CitizenshipInfo info = new CitizenshipInfo();
		{
			info.setUSCitizen(false);
			info.setInsIDNumber("i948934" + (int)(1000*Math.random()));
			info.setArrivalDate("12/12/2014");
		}
		
		return info;
	}
	

}
