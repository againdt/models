package com.getinsured.hix.cap.drx;

public class  ArrayOfSessionResponse {
	
	private Session Session;

	public Session getSession() {
		return Session;
	}

	public void setSession(Session session) {
		this.Session = session;
	}

	public static class Session {
		private String Expires;
		private String SessionID;
		private String Status;
		
		public String getExpires() {
			return Expires;
		}
		public void setExpires(String expires) {
			Expires = expires;
		}
		public String getSessionID() {
			return SessionID;
		}
		public void setSessionID(String sessionID) {
			SessionID = sessionID;
		}
		public String getStatus() {
			return Status;
		}
		public void setStatus(String status) {
			Status = status;
		}
	}
}