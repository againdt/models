package com.getinsured.hix.dto.plandisplay.ind71g;

public class IND71Enums {
	public static final int NAME_MIN_LENGTH = 0;
	public static final int NAME_MAX_LENGTH = 60; 
	public static final String NAME_REGEX = "[0-9a-zA-Z .',-]+";
	public static final String LENGTH_ERROR = " is not matching allowed length {min}-{max}.";
	public static final String INVALID_REGEX_ERROR = "  is invalid.";
	
	public static final String RELATION_REGEX = "01|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|23|24|25|26|31|38|53|60|D2|G8|G9";
	
	public static final String DATE_REGEX = "(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/[\\d]{4}";
	
	public static final int SSN_MIN_LENGTH = 9;
	public static final int SSN_MAX_LENGTH = 9;
	public static final String SSN_REGEX = "[0-9]+";
	
	public static final int ADDRESS_MIN_LENGTH = 0;
	public static final int ADDRESS_MAX_LENGTH = 88;
	public static final String ADDRESS_REGEX = "[0-9a-zA-Z .'#,\\\\//-]+";
	
	public static final int STATE_MIN_LENGTH = 1;
	public static final int STATE_MAX_LENGTH = 2;
	public static final String STATE_REGEX = "[0-9a-zA-Z .'#,\\-]+";

	public static final int ZIP_MIN_LENGTH = 5;
	public static final int ZIP_MAX_LENGTH = 9;
	public static final String ZIP_REGEX = "[0-9]+";

	public static final int COUNTY_MIN_LENGTH = 3;
	public static final int COUNTY_MAX_LENGTH = 5;
	public static final String COUNTY_REGEX = "[0-9]+";
	
	public static final int PHONE_LENGTH = 10;
	public static final int PHONE_MAX_LENGTH = 10;
	public static final String PHONE_REGEX = "[0-9]+";
	
	public static final int CITY_MIN_LENGTH = 0;
	public static final int CITY_MAX_LENGTH = 100;
	public static final String CITY_REGEX = "[0-9a-zA-Z .'#,\\-]+";
	
	public static final int SUFFIX_MIN_LENGTH = 0;
	public static final int SUFFIX_MAX_LENGTH = 10;
	
	public static final String EMAIL_REGEX ="^[/\\+_a-zA-Z0-9-]+(\\.[/\\+_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,20})";
	public static final String INVALID_EMAIL_ERROR =  " is not valid email.";
	
	public static final String YES_NO_REGEX = "Y|N";
	
	public static final String RESPONSIBLE_PERSON_TYPE = "QD|S1";
	
	public static final String MRC_REGEX = "01|02|03|04|05|06|07|08|09|10|11|14|15|16|17|18|20|21|22|25|26|27|28|29|31|32|33|37|38|39|40|41|43|59|AA|AB|AC|AD|AE|AF|AG|AH|AI|AJ|AL|EC|XN|XT";
	
	public static final String RELATIONSHIP_REGEX = "01|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|23|24|25|26|31|38|53|60|D2|G8|G9";
	
	public static final String GENDER_REGEX = "M|F";
	
	public static final String MARITAL_STATUS_REGEX = "B|D|I|M|R|S|U|W|X";
	
	public static final String CITIZENSHIP_STATUS_REGEX = "1|2|3|4|5|6|7";

	public static final String SPOKEN_LANGUAGE_CODE_REGEX = "eng|ara|hye|fas|khmr|cesm|cmn|hmn|kor|rus|spa|tgl|vie";
	
	public static final String WRITTEN_LANGUAGE_CODE_REGEX = "eng|ara|hye|fas|khmr|zho|hmn|kor|rus|spa|tgl|vie";
	
	public static final String YES_REGEX = "Y";
	
	public static final String CSR_REGEX = "CS1|CS2|CS3|CS4|CS5|CS6";
	public static final String INSURANCE_TYPE_REGEX = "Health|Dental";
	
	public static final String REQUIRED_FIELD = " is required field.";
	
	public static final int RESPONSIBLE_PERSON_MIN_LENGTH = 1;
	public static final int RESPONSIBLE_PERSON_MAX_LENGTH = 35;
	public static final String RESPONSIBLE_PERSON_REGEX = "[0-9a-zA-Z .'-]+";
	
	public static final String CUSTODIAL_PARENT_ERROR = " is blank while CustodialParentId is present.";
	public static final String CUSTODIAL_PARENT_ID_ERROR = " is present while CustodialParentId is absent.";

	public enum CSRType {
		CS1,CS2,CS3,CS4,CS5,CS6
	};

	public enum RoleType {
		Agent,Assister
	};
	
	public enum YesNoVal {
		Y,N
	};
	
	public enum InsuranceType {
		Health, Dental
	};
	
	public enum EnrollmentType {
		S
	};
	
	public enum PersonType {
		QD,S1
	};
}
