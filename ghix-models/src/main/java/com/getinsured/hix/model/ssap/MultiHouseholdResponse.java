package com.getinsured.hix.model.ssap;

import java.util.List;

public class MultiHouseholdResponse {
    private List<MultiHousehold> households;

    public List<MultiHousehold> getHouseholds() {
        return households;
    }

    public void setHouseholds(List<MultiHousehold> households) {
        this.households = households;
    }

    @Override
    public String toString() {
        return "MultiHouseHoldResponse{" +
                "households=" + households +
                '}';
    }
}
