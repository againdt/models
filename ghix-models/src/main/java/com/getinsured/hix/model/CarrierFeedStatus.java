package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CARRIER_FEED_STATUS")
public class CarrierFeedStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARRIER_FEED_STATUS_SEQ")
	@SequenceGenerator(name = "CARRIER_FEED_STATUS_SEQ", sequenceName = "CARRIER_FEED_STATUS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "CARRIER_STATUS", length = 100)
	private String carrierStatus;
	
	@Column(name = "GI_STATUS", length = 100)
	private String giStatus;
	
	@Column(name = "CARRIER_NAME", length = 100)
	private String carrierName;
	
	@Column(name="ISSUER_ID")
	private Integer issuerId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCarrierStatus() {
		return carrierStatus;
	}

	public void setCarrierStatus(String carrierStatus) {
		this.carrierStatus = carrierStatus;
	}

	public String getGiStatus() {
		return giStatus;
	}

	public void setGiStatus(String giStatus) {
		this.giStatus = giStatus;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
