package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;
import java.util.Date;

public class EligibilityProgramDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String eligibilityType;
	private String eligibilityIndicator;
	private Date eligibilityStartDate;
	private Date eligibilityEndDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEligibilityType() {
		return eligibilityType;
	}
	public void setEligibilityType(String eligibilityType) {
		this.eligibilityType = eligibilityType;
	}
	public String getEligibilityIndicator() {
		return eligibilityIndicator;
	}
	public void setEligibilityIndicator(String eligibilityIndicator) {
		this.eligibilityIndicator = eligibilityIndicator;
	}
	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}
	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}
	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}
	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	
}
