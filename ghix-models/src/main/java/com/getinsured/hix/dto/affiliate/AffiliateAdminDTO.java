package com.getinsured.hix.dto.affiliate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.model.AccountUser;

/**
 * Holds user admin to affiliate relationships.
 * <p>
 *   This is used on Manage Affiliate Admins screen. Any user that has
 *   access to manage affiliates can become an Affiliate Admin. Single
 *   user can administer multiple affiliates. Both user and affiliate must be
 *   part of the same tenant.
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 2/2/17
 */
public class AffiliateAdminDTO implements Serializable
{
  private static final long serialVersionUID = 763635029148791315L;

  /* Role names that can add/manage affiliate admins */
  @JsonIgnore
  private static final List<String> roleNames = new ArrayList<>(1);

  static
  {
    roleNames.add("AFFILIATE_ADMIN");
   // roleNames.add("TENANT_ADMIN");
    //roleNames.add("OPERATIONS_ADMIN");
  }

  private AccountUser user;
  private List<Affiliate> affiliates;

  public AccountUser getUser()
  {
    return user;
  }

  public void setUser(AccountUser user)
  {
    this.user = user;
  }

  public List<Affiliate> getAffiliates()
  {
    return affiliates;
  }

  public void setAffiliates(List<Affiliate> affiliates)
  {
    this.affiliates = affiliates;
  }

  public static List<String> getAllowedAdminRoleNames()
  {
    return roleNames;
  }
}