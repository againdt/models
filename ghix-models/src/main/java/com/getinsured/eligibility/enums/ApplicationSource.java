package com.getinsured.eligibility.enums;

/*
 * Added by @yjadhav
 */

public enum ApplicationSource {


	ONLINE_APPLICATION("ON", "Online Application"),
	PAPER_WALKIN_APPLICATION("WI", "Paper/Walk-in Application"),
	REFERRALS_FROM_STATE("RF", "Referral Application"),
	CONVERSION_INCASE_OF_ID("CN", "Converted Application"),
	PHONE_APPLICANT("PH", "Phone Application"),
	DATA_MIGRATION_AT("DM", "Data Migration Application");

	private String	applicationSourceCode;

	private String description;

	private ApplicationSource(String applicationSourceCode, String description){
		this.applicationSourceCode = applicationSourceCode;
		this.description = description;
	}

	public String getApplicationSourceCode(){
		return applicationSourceCode;
	}

	public String getDescription() {
		return description;
	}

}
