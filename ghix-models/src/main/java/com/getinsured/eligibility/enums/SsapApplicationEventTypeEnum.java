package com.getinsured.eligibility.enums;

public enum SsapApplicationEventTypeEnum {
	
	SEP,OE,QEP;
	
	public String value() {
		return name();
	}

	
	public static SsapApplicationEventTypeEnum fromValue(String v) {
		SsapApplicationEventTypeEnum data = null;
		for (SsapApplicationEventTypeEnum c : SsapApplicationEventTypeEnum.class
				.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	@Override
	public String toString() {
		return value();
	}


}
