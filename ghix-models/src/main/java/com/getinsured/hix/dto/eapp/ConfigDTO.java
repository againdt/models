package com.getinsured.hix.dto.eapp;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.dto.eapp.application.EappApplication;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO for fetching a config
 * 
 * @author root
 *
 */
public class ConfigDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String brand;
	private String year;
	private ProductTypeEnum product;
	private String state;
	private DocumentTypeEnum docType;
	private byte[] content;
	private String disclaimerEcmId;

	private String ecmId;

	public String getEcmId() {
		return ecmId;
	}

	public void setEcmId(String ecmId) {
		this.ecmId = ecmId;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public DocumentTypeEnum getDocType() {
		return docType;
	}

	public void setDocType(DocumentTypeEnum docType) {
		this.docType = docType;
	}

	public ProductTypeEnum getProduct() {
		return product;
	}

	public void setProduct(ProductTypeEnum product) {
		this.product = product;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
	
	public String getDisclaimerEcmId() {
		return disclaimerEcmId;
	}

	public void setDisclaimerEcmId(String disclaimerEcmId) {
		this.disclaimerEcmId = disclaimerEcmId;
	}

	public static ConfigDTO getSampleInstance() {
		ConfigDTO dto = new ConfigDTO();
		dto.setBrand("aetna");
		dto.setDocType(DocumentTypeEnum.UI_TEMPLATE);
		dto.setProduct(ProductTypeEnum.HLT);
		dto.setYear("2015");
		dto.setState("FL");

		return dto;
	}
	
	public static ConfigDTO getInstance(String brand, String state, String year,
				String product, String type, String content) throws Exception {
			ConfigDTO dto = new ConfigDTO();
			dto.setBrand(brand);
			dto.setState(state);
			dto.setYear(year);
			dto.setProduct(ProductTypeEnum.valueOf(ProductTypeEnum.class, product));
			dto.setDocType(DocumentTypeEnum.valueOf(type));
			dto.setContent(content == null ? null : content.getBytes(Charset
					.forName("UTF-8")));
			return dto;
	}

	public static ConfigDTO getInstance(EappApplication application,
			DocumentTypeEnum document) {
		ConfigDTO dto = new ConfigDTO();
		dto.setBrand(application.getIssuer().getBrand());
		dto.setState(application.getHousehold().getCensusData().getState());

		/**
		 * FIXME: fetch year from the following date value
		 */
		// dto.setYear(application.getEffectiveStartDate())
		dto.setYear(application.getYear());
		dto.setProduct(application.getPlan().getInsuranceType());
		dto.setDocType(document);
		return dto;
	}
	
	/**
	 * After merging pdf and uitemplate, we need this interface to fetch either entities. 
	 * @param fullText
	 * @param param
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String getTemplate(ConfigDTO dto, String param) throws UnsupportedEncodingException{
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		StringBuffer sb = new StringBuffer(new String(dto.getContent(), "UTF-8"));
		HashMap<String, Object> uimap = null;
		uimap = gson.fromJson(sb.toString(), HashMap.class);
		Map<String, Object> uitemplate = (Map<String, Object>) uimap.get(param);
		
		return gson.toJson(uitemplate);
	}
	
	@SuppressWarnings("unchecked")
	public static void setTemplate(ConfigDTO dto, String parentKey, String childKey, String value) throws UnsupportedEncodingException{
		StringBuffer sb = new StringBuffer(new String(dto.getContent(), "UTF-8"));
		HashMap<String, Object> uimap = null;
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		uimap = gson.fromJson(sb.toString(), HashMap.class);
		
		Map<String, Object> childObject = gson.fromJson(value, HashMap.class);
		Map<String, Object> uitemplate = (Map<String, Object>) uimap.get(parentKey);
		uitemplate.put(parentKey, childObject.get(childKey));
		
		dto.setContent(gson.toJson(uitemplate).getBytes());
	}
	
	public static String decoratePdfMapping(ConfigDTO dto) throws UnsupportedEncodingException{
		String rawPdfMap = new String(getTemplate(dto, "pdfMapper"));
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		/**
		 * In the following five lines we put the fields as a root 
		 * element for other key value items.
		 * 
		 * this code would stay until PDFFiller is modified to expect key:value pair
		 * 
		 * Also prefix $ symbol for pdf to work
		 */
		Map<String, Object> keyVals = gson.fromJson(rawPdfMap, HashMap.class);
		
//		for(Map.Entry<String, Object> entry : keyVals.entrySet()){
//			/**
//			 * this code is not safe for complex objects
//			 */
//			if(!(entry.getValue() instanceof StringMap)){
//				val =(String)entry.getValue();
//				if(!StringUtils.isEmpty(val)){
//					val = "$"+val;
//					entry.setValue(val);
//				}
//				
//			}
//			
//		}
		Map<String, Object> rootMap = new HashMap<String, Object>();
		rootMap.put("fields", keyVals);
		
		return gson.toJson(rootMap);
		
	}

	@Override
	public String toString() {
		return "ConfigDTO [brand=" + brand + ", year=" + year + ", product="
				+ product + ", state=" + state + ", docType=" + docType
				+ ", disclaimerEcmId=" + disclaimerEcmId + ", ecmId=" + ecmId
				+ "]";
	}
	
	

}
