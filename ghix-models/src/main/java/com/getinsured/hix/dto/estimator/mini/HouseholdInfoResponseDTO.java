package com.getinsured.hix.dto.estimator.mini;

import java.time.Instant;

import org.springframework.http.HttpStatus;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * Create Response DTO for outbound HouseholdInfo API to send pre-65 Retiree/Non-Retiree + Provider + Prescription data to AgentExpress.
 */
public class HouseholdInfoResponseDTO {

	private HttpStatus statusCode;
	private String message; // optional: success message or error payload
	private long executionDuration;
	private long startTime;

	private String redirectId; // Redirect ID from Agent Express API

	public enum RedirectType {
		CONSUMER, AGENT;
	}

	public HouseholdInfoResponseDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getExecutionDuration() {
		return executionDuration;
	}

	public void startResponse() {
		startTime = Instant.now().toEpochMilli();
	}

	public void endResponse() {
		long endTime = Instant.now().toEpochMilli();
		executionDuration = endTime - startTime;
	}

	public String getRedirectId() {
		return redirectId;
	}

	public void setRedirectId(String redirectId) {
		this.redirectId = redirectId;
	}
}
