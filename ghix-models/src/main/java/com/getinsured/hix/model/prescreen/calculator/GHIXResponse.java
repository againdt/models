package com.getinsured.hix.model.prescreen.calculator;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

/**
 * This is the Base Response for all Responses in the project. Each response
 * must have status success/failed. errMsg, errorCode, execDuration are
 * optional.
 * 
 * @author polimetla_b
 * 
 */
public class GHIXResponse {
	private String appID;
	private String status;
	private String errMsg;
	private int errCode;
	private long execDuration;
	private long startTime;

	private String moduleStatusCode;
	
	public void startResponse() {
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		execDuration = endTime - startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public long getExecDuration() {
		return execDuration;
	}

	public void setExecDuration(long execDuration) {
		this.execDuration = execDuration;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getModuleStatusCode() {
		return moduleStatusCode;
	}

	public void setModuleStatusCode(String moduleStatusCode) {
		this.moduleStatusCode = moduleStatusCode;
	}
	
	

}
