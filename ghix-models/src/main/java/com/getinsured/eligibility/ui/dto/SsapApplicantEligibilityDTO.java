package com.getinsured.eligibility.ui.dto;

import java.util.Map;
import java.util.Set;

public class SsapApplicantEligibilityDTO {

	private String firstName;

	private String middleName;

	private String lastName;
    
	private String suffix;

	private long personId;

	private boolean isVerificationCompleted;

	private Map<String, String> verificationMap;

	private Set<EligibilityProgramDTO> eligibilities;

	private boolean conditional;

	private boolean applyingForCoverage;

	private boolean medicaidAssessed;

	private boolean healthLinkEligible;

	private String applicantGuid;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<EligibilityProgramDTO> getEligibilities() {
		return eligibilities;
	}

	public void setEligibilities(Set<EligibilityProgramDTO> eligibilities) {
		this.eligibilities = eligibilities;
	}

	public Map<String, String> getVerificationMap() {
		return verificationMap;
	}

	public void setVerificationMap(Map<String, String> verificationMap) {
		this.verificationMap = verificationMap;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public boolean isVerificationCompleted() {
		return isVerificationCompleted;
	}

	public void setVerificationCompleted(boolean isVerificationCompleted) {
		this.isVerificationCompleted = isVerificationCompleted;
	}
	
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public boolean isConditional() {
		return conditional;
	}

	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	public boolean isApplyingForCoverage() {
		return applyingForCoverage;
	}

	public void setApplyingForCoverage(boolean applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public boolean isMedicaidAssessed() {
		return medicaidAssessed;
	}

	public void setMedicaidAssessed(boolean medicaidAssessed) {
		this.medicaidAssessed = medicaidAssessed;
	}

	public boolean isHealthLinkEligible() {
		return healthLinkEligible;
	}

	public void setHealthLinkEligible(boolean healthLinkEligible) {
		this.healthLinkEligible = healthLinkEligible;
	}

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}
}
