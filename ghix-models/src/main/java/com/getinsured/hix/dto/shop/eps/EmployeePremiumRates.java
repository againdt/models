package com.getinsured.hix.dto.shop.eps;

public class EmployeePremiumRates {

	private Float totalPremium = 0f;
	private Float indvPremium = 0f;
	private Float depePremium = 0f;
	
	public EmployeePremiumRates(Float totalPremium, Float indvPremium,Float depePremium) {
		this.totalPremium = totalPremium;
		this.indvPremium = indvPremium;
		this.depePremium = depePremium;
	}
	public Float getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(Float totalPremium) {
		this.totalPremium = totalPremium;
	}
	public Float getIndvPremium() {
		return indvPremium;
	}
	public void setIndvPremium(Float indvPremium) {
		this.indvPremium = indvPremium;
	}
	public Float getDepePremium() {
		return depePremium;
	}
	public void setDepePremium(Float depePremium) {
		this.depePremium = depePremium;
	}
	
	
}
