package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "ENRL_CMS_IN_MISSING_POLICY")
public class EnrollmentCmsInMissingPolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_IN_MSNG_POL_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_IN_MSNG_POL_SEQ", sequenceName = "ENRL_CMS_IN_MSNG_POL_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name="EXCHANGE_ASSIGNED_POLICY_ID")
	private String exchangeAssignedPolicyId;
	
	@Column(name="QHP_ID")
	private String qhpId;
	
	@Column(name="CURRENT_CMS_POLICY_STATUS")
	private String currentCmsPolicyStatus;
	
	@ManyToOne
	@JoinColumn(name="ENRL_CMS_IN_ID")
	private EnrollmentCmsIn enrlCmsIn;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExchangeAssignedPolicyId() {
		return exchangeAssignedPolicyId;
	}

	public EnrollmentCmsIn getEnrlCmsIn() {
		return enrlCmsIn;
	}

	public void setEnrlCmsIn(EnrollmentCmsIn enrlCmsIn) {
		this.enrlCmsIn = enrlCmsIn;
	}

	public void setExchangeAssignedPolicyId(String exchangeAssignedPolicyId) {
		this.exchangeAssignedPolicyId = exchangeAssignedPolicyId;
	}

	public String getQhpId() {
		return qhpId;
	}

	public void setQhpId(String qhpId) {
		this.qhpId = qhpId;
	}

	public String getCurrentCmsPolicyStatus() {
		return currentCmsPolicyStatus;
	}

	public void setCurrentCmsPolicyStatus(String currentCmsPolicyStatus) {
		this.currentCmsPolicyStatus = currentCmsPolicyStatus;
	}
}
