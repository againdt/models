package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENROLLMENT_ISSUER_COMMISION")
public class EnrollmentIssuerCommision  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENR_ISSUER_COMMISION_SEQ")
	@SequenceGenerator(name = "ENR_ISSUER_COMMISION_SEQ", sequenceName = "ENR_ISSUER_COMMISION_SEQ", allocationSize = 1)
	private int id;
	
	@OneToOne
	@JoinColumn(name = "ENROLLMENT_ID")
	private Enrollment enrollment;
	
	@Column(name = "INITIAL_COMM_AMT", nullable = true)
	private Double initialCommissionAmt;
	
	@Column(name = "INITIAL_COMM_PERCENT", nullable = true)
	private Double initialCommissionPercent;
	
	@Column(name = "RECURRING_COMM_AMT", nullable = true)
	private Double recurringCommissionAmt;
	
	@Column(name = "RECURRING_COMM_PERCENT", nullable = true)
	private Double recurringCommissionPercent;
	
	@Column(name = "INITIAL_COMM_DOLLAR_AMT", nullable = true)
	private Double initialCommDollarAmt;
	
	@Column(name = "RECURRING_COMM_DOLLAR_AMT", nullable = true)
	private Double recurringCommDollarAmt;
	
	@Column(name = "NON_COMMISSION_FLAG", nullable = true)
	private String nonCommissionFlag;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public Double getInitialCommissionAmt() {
		return initialCommissionAmt;
	}

	public void setInitialCommissionAmt(Double initialCommissionAmt) {
		this.initialCommissionAmt = initialCommissionAmt;
	}

	public Double getInitialCommissionPercent() {
		return initialCommissionPercent;
	}

	public void setInitialCommissionPercent(Double initialCommissionPercent) {
		this.initialCommissionPercent = initialCommissionPercent;
	}

	public Double getRecurringCommissionAmt() {
		return recurringCommissionAmt;
	}

	public void setRecurringCommissionAmt(Double recurringCommissionAmt) {
		this.recurringCommissionAmt = recurringCommissionAmt;
	}

	public Double getRecurringCommissionPercent() {
		return recurringCommissionPercent;
	}

	public void setRecurringCommissionPercent(Double recurringCommissionPercent) {
		this.recurringCommissionPercent = recurringCommissionPercent;
	}

	public Double getInitialCommDollarAmt() {
		return initialCommDollarAmt;
	}

	public void setInitialCommDollarAmt(Double initialCommDollarAmt) {
		this.initialCommDollarAmt = initialCommDollarAmt;
	}

	public Double getRecurringCommDollarAmt() {
		return recurringCommDollarAmt;
	}

	public void setRecurringCommDollarAmt(Double recurringCommDollarAmt) {
		this.recurringCommDollarAmt = recurringCommDollarAmt;
	}

	public String getNonCommissionFlag() {
		return nonCommissionFlag;
	}

	public void setNonCommissionFlag(String nonCommissionFlag) {
		this.nonCommissionFlag = nonCommissionFlag;
	}	

}
