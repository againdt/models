package com.getinsured.hix.dto.eapp.grammer;


/**
 * Used for select/radio options
 * @author root
 *
 */
public class UIOption {
	
	private String label;
	private String value;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public static UIOption getSampleInstance(){
		UIOption option = new UIOption();
		int seed = (int)(Math.random()*300);
		option.setLabel("label" + seed);
		option.setValue("value" + seed);
		return option;
	}

}
