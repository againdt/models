package com.getinsured.hix.dto.broker;

import java.util.List;

public class IndividualDTO {
	private long id;
	private long individualid;
	private String fname;
	private String minitial;	
	private String lname;
	private String csdate;
	private String cedate;
	private String communicationPref;
	private String prefspokenlang;

	private String writtenlangpref;

	private String gender;

	private long ssn;

	private String citizen;

	private String dob;

	private long phone;

	private String emailid;

	private String addressline1;

	private String addressline2;

	private String zip;

	private String county;

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	private String city;

	private String state;
	
	private	String applicationStatus;
	
	private String caseStatus;
	
	private String numberOfHouseholdMembers;
	
	public String getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

	public String getNumberOfHouseholdMembers() {
		return numberOfHouseholdMembers;
	}

	public void setNumberOfHouseholdMembers(String numberOfHouseholdMembers) {
		this.numberOfHouseholdMembers = numberOfHouseholdMembers;
	}

	private String suffix;
    private List<Long> idlist;
	public List<Long> getIdlist() {
		return idlist;
	}

	public void setIdlist(List<Long> idlist) {
		this.idlist = idlist;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMinitial() {
		return minitial;
	}

	public void setMinitial(String minitial) {
		this.minitial = minitial;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCsdate() {
		return csdate;
	}

	public void setCsdate(String csdate) {
		this.csdate = csdate;
	}

	public String getCedate() {
		return cedate;
	}

	public void setCedate(String cedate) {
		this.cedate = cedate;
	}

	public String getCommunicationPref() {
		return communicationPref;
	}

	public void setCommunicationPref(String communicationPref) {
		this.communicationPref = communicationPref;
	}

	public String getPrefspokenlang() {
		return prefspokenlang;
	}

	public void setPrefspokenlang(String prefspokenlang) {
		this.prefspokenlang = prefspokenlang;
	}

	public String getWrittenlangpref() {
		return writtenlangpref;
	}

	public void setWrittenlangpref(String writtenlangpref) {
		this.writtenlangpref = writtenlangpref;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getSsn() {
		return ssn;
	}

	public void setSsn(long ssn) {
		this.ssn = ssn;
	}

	public String getCitizen() {
		return citizen;
	}

	public void setCitizen(String citizen) {
		this.citizen = citizen;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getAddressline1() {
		return addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public long getIndividualid() {
		return individualid;
	}

	public void setIndividualid(long individualid) {
		this.individualid = individualid;
	}

	@Override
	public String toString() {
		return "IndividualDTO details: Id = "+id+", Individualid = "+individualid;
	}
}
