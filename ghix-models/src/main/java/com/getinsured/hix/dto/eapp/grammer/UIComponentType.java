package com.getinsured.hix.dto.eapp.grammer;

import com.google.gson.annotations.SerializedName;

public enum UIComponentType {
	@SerializedName("text")
	TEXT,
	@SerializedName("radio")
	RADIO, 
	@SerializedName("checkbox")
	CHECKBOX, 
	@SerializedName("select")
	SELECT, 
	@SerializedName("section")
	SECTION,
	@SerializedName("group")
	GROUP;
	
	public static UIComponentType getInstance(String type){
		type = type.toUpperCase();
		return UIComponentType.valueOf(type);
	}
	
	public static void main(String args[]){
		System.out.println(UIComponentType.getInstance("text"));
	}

}
