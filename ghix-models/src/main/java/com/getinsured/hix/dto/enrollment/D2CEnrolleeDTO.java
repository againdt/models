package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.Location;

public class D2CEnrolleeDTO implements Serializable{

	private String lastName;
	private String firstName;
	private String middleName;
	private Location homeAddress;
	private Date birthDate;
	private String gender;
	private Location mailingAddress;
	private String tobaccoUsage;
	private String maritalStatus;
	private String raceEthnicity;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private String personType; //SUBSCRIBER/ENROLLEE
	private String relationshipHcp;
	private Character enrollmentReason; //Init/Special
	private String exchgIndivIdentifier;
	private String primaryPhoneNo;
	private String prefEmail;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Location getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Location homeAddress) {
		this.homeAddress = homeAddress;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Location getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(Location mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public String getTobaccoUsage() {
		return tobaccoUsage;
	}

	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getRaceEthnicity() {
		return raceEthnicity;
	}

	public void setRaceEthnicity(String raceEthnicity) {
		this.raceEthnicity = raceEthnicity;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getRelationshipHcp() {
		return relationshipHcp;
	}

	public void setRelationshipHcp(String relationshipHcp) {
		this.relationshipHcp = relationshipHcp;
	}

	public Character getEnrollmentReason() {
		return enrollmentReason;
	}

	public void setEnrollmentReason(Character enrollmentReason) {
		this.enrollmentReason = enrollmentReason;
	}

	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}

	public String getPrimaryPhoneNo() {
		return primaryPhoneNo;
	}

	public void setPrimaryPhoneNo(String primaryPhoneNo) {
		this.primaryPhoneNo = primaryPhoneNo;
	}

	public String getPrefEmail() {
		return prefEmail;
	}

	public void setPrefEmail(String prefEmail) {
		this.prefEmail = prefEmail;
	}

}
