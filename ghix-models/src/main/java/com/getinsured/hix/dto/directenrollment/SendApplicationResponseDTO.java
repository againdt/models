package com.getinsured.hix.dto.directenrollment;

public class SendApplicationResponseDTO {
	private DirectEnrollmentStatus.SendStatus status;
	/**
	 * confirmation/policy number 
	 */
	private String confirmationNumber;
	
	private String webserviceResponse;
	
	public DirectEnrollmentStatus.SendStatus getStatus() {
		return status;
	}
	public void setStatus(DirectEnrollmentStatus.SendStatus status) {
		this.status = status;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public String getWebserviceResponse() {
		return webserviceResponse;
	}
	public void setWebserviceResponse(String webserviceResponse) {
		this.webserviceResponse = webserviceResponse;
	}
		
	
}
