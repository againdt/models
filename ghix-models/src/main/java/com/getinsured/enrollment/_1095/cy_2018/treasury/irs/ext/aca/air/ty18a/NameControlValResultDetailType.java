
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Name Control Validation Result Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2012-11-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;The type for the Name Control Validation Results detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for NameControlValResultDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NameControlValResultDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}FileSourceCd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}NameControlTypeCd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}NameControlValResultCd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TaxpayerValidityCd" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameControlValResultDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "fileSourceCd",
    "nameControlTypeCd",
    "nameControlValResultCd",
    "taxpayerValidityCd"
})
public class NameControlValResultDetailType {

    @XmlElement(name = "FileSourceCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String fileSourceCd;
    @XmlElement(name = "NameControlTypeCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    @XmlSchemaType(name = "string")
    protected NameControlTypeCodeType nameControlTypeCd;
    @XmlElement(name = "NameControlValResultCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    @XmlSchemaType(name = "string")
    protected NameControlValResultCodeType nameControlValResultCd;
    @XmlElement(name = "TaxpayerValidityCd", namespace = "urn:us:gov:treasury:irs:common")
    protected BigInteger taxpayerValidityCd;

    /**
     * Gets the value of the fileSourceCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileSourceCd() {
        return fileSourceCd;
    }

    /**
     * Sets the value of the fileSourceCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileSourceCd(String value) {
        this.fileSourceCd = value;
    }

    /**
     * Gets the value of the nameControlTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link NameControlTypeCodeType }
     *     
     */
    public NameControlTypeCodeType getNameControlTypeCd() {
        return nameControlTypeCd;
    }

    /**
     * Sets the value of the nameControlTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameControlTypeCodeType }
     *     
     */
    public void setNameControlTypeCd(NameControlTypeCodeType value) {
        this.nameControlTypeCd = value;
    }

    /**
     * Gets the value of the nameControlValResultCd property.
     * 
     * @return
     *     possible object is
     *     {@link NameControlValResultCodeType }
     *     
     */
    public NameControlValResultCodeType getNameControlValResultCd() {
        return nameControlValResultCd;
    }

    /**
     * Sets the value of the nameControlValResultCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameControlValResultCodeType }
     *     
     */
    public void setNameControlValResultCd(NameControlValResultCodeType value) {
        this.nameControlValResultCd = value;
    }

    /**
     * Gets the value of the taxpayerValidityCd property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTaxpayerValidityCd() {
        return taxpayerValidityCd;
    }

    /**
     * Sets the value of the taxpayerValidityCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTaxpayerValidityCd(BigInteger value) {
        this.taxpayerValidityCd = value;
    }

}
