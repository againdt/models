
package com.serff.planmanagementserffapi.common.model.pm;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;



public class ObjectFactory {

    private final static QName _InsurerPlanId_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "insurerPlanId");
    private final static QName _ExchangePlanId_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "exchangePlanId");
    private final static QName _StatePlanId_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "statePlanId");
    private final static QName _InsurerSuppliedData_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "insurerSuppliedData");
    private final static QName _ExchangeWorkflowStatus_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "exchangeWorkflowStatus");
    private final static QName _AttachmentData_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "attachmentData");
    private final static QName _RequiredInsurerSuppliedData_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "requiredInsurerSuppliedData");
    private final static QName _FileName_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "fileName");
    private final static QName _ExchangeSuppliedData_QNAME = new QName("http://www.serff.com/planManagementSerffApi/common/model/pm", "exchangeSuppliedData");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.serff.planmanagementserffapi.common.model.pm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidationErrors }
     * 
     */
    public ValidationErrors createValidationErrors() {
        return new ValidationErrors();
    }

    /**
     * Create an instance of {@link ValidationError }
     * 
     */
    public ValidationError createValidationError() {
        return new ValidationError();
    }

    /**
     * Create an instance of {@link Attachments }
     * 
     */
    public Attachments createAttachments() {
        return new Attachments();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "insurerPlanId")
    public JAXBElement<String> createInsurerPlanId(String value) {
        return new JAXBElement<String>(_InsurerPlanId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "exchangePlanId")
    public JAXBElement<String> createExchangePlanId(String value) {
        return new JAXBElement<String>(_ExchangePlanId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "statePlanId")
    public JAXBElement<String> createStatePlanId(String value) {
        return new JAXBElement<String>(_StatePlanId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "insurerSuppliedData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createInsurerSuppliedData(DataHandler value) {
        return new JAXBElement<DataHandler>(_InsurerSuppliedData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "exchangeWorkflowStatus")
    public JAXBElement<String> createExchangeWorkflowStatus(String value) {
        return new JAXBElement<String>(_ExchangeWorkflowStatus_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "attachmentData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createAttachmentData(DataHandler value) {
        return new JAXBElement<DataHandler>(_AttachmentData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "requiredInsurerSuppliedData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createRequiredInsurerSuppliedData(DataHandler value) {
        return new JAXBElement<DataHandler>(_RequiredInsurerSuppliedData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "fileName")
    public JAXBElement<String> createFileName(String value) {
        return new JAXBElement<String>(_FileName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/common/model/pm", name = "exchangeSuppliedData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createExchangeSuppliedData(DataHandler value) {
        return new JAXBElement<DataHandler>(_ExchangeSuppliedData_QNAME, DataHandler.class, null, value);
    }

}
