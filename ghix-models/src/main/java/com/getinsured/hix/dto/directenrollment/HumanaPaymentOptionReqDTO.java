package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;

public class HumanaPaymentOptionReqDTO implements Serializable {

	
	private static final long serialVersionUID = -5501256696179860133L;
	
	private String state;
	
	private String planTypeName;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPlanTypeName() {
		return planTypeName;
	}

	public void setPlanTypeName(String planTypeName) {
		this.planTypeName = planTypeName;
	}

}
