package com.getinsured.hix.dto.shop;


public interface IShopAPIError {
	enum ShopAPIErrors {
		
		NO_ERROR(0,""),
		INVALID_REQUEST(101, "Invalid Request"),
		EMPLOYER_ENROLLMENT_NOT_FOUND(104, "Employer Enrollment not found"),
		EMPLOYER_ENROLLMENT_INVALID_STATUS(105, "Employer Enrollment is in  CART/CANCELLED/TERMINATED/DELETED status"),
		EMPLOYER_ENROLLMENT_ACTIVE_CANCELLED_STATUS_REQ(106, "Only ACTIVE and CANCELLED enrollment status is accepted for Employer Enrollment Status PENDING."),
		EMPLOYER_ENROLLMENT_TERMINATED_STATUS_REQ(107,"Only TERMINATED enrollment status is accepted for Employer Enrollment Status ACTIVE."),
		EMPLOYER_ACTIVE_ENROLLMENT_EXIST(108,"Invalid request, Employer Enrollment already set to ACTIVE Status."),
		MULTIPLE_EMPLOYER_ENROLLMENT_EXIST(109,"More then one Employer Enrollment found.");
		
		private final int id;
		private final String message;

		ShopAPIErrors(int id, String message) {
			this.id = id;
			this.message = message;
		}

		public int getId() { return id; }
		public String getMessage() { return message; }
		};
	
		ShopAPIErrors shopAPIErrors = null;
}
