package com.getinsured.hix.dto.calldata;

public class CallDataIncomingRequestDTO {
	private String interaction_id;  // example : "9835f90a-3a10-4db8-8fce-2a8a3c64a125",
	private String client_id;  // example : "12936",
	private String client_name;  // example : "MEDICARE-GetInsured",
	private String program_id;  // example : "45083",
	private String program_name;  // example : "Medicare-GetInsured",
	private String campaign_id;  // example : "120311",
	private String campaign_name;  // example : "GetInsured Medicare-Website",
	private String in_outbound_dialout;  // example : "Incoming",
	private String affiliateID;  // example : "179",
	private String flowID;  // example : "896",
	private String dnis;  // example : "8009066609",
	private String caller_ani;  // example : "1111111111",
	private String call_start_time;  // example : "3\/21\/2018 2:32:33 PM",
	private String call_end_time;  // example : "3\/21\/2018 2:50:34 PM",
	private String last_agent_id;  // example : "swright@getinsured.com",
	private String last_agent_full_name;  // example : "Sylvia Doe",
	private String agent_count;  // example : "1",
	private String agent_ring_time_in_secs;  // example : "5",
	private String caller_hold_length_in_secs;  // example : "0",
	private String caller_queue_length_in_secs;  // example : "5",
	private String caller_talk_time_in_secs;  // example : "75",
	private String agent_talk_time_in_secs;  // example : "75",
	private String agent_after_call_work_time_in_secs;  // example : "26",
	private String total_caller_time_in_secs;  // example : "123",
	private String total_agent_time_secs;  // example : "123",
	private String state_code;  // example : "CA",
	private String after_hours;  // example : "0",
	private String voicemail;  // example : "0",
	private boolean abandoned;  // example : "FALSE",
	private String final_disposition;  // example : "Sale Closed",
	private boolean external_transfer_call;  // example : "FALSE"

	
	public String getInteraction_id() {
		return interaction_id;
	}
	public void setInteraction_id(String interaction_id) {
		this.interaction_id = interaction_id;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getProgram_id() {
		return program_id;
	}
	public void setProgram_id(String program_id) {
		this.program_id = program_id;
	}
	public String getProgram_name() {
		return program_name;
	}
	public void setProgram_name(String program_name) {
		this.program_name = program_name;
	}
	public String getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}
	public String getCampaign_name() {
		return campaign_name;
	}
	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}
	public String getIn_outbound_dialout() {
		return in_outbound_dialout;
	}
	public void setIn_outbound_dialout(String in_outbound_dialout) {
		this.in_outbound_dialout = in_outbound_dialout;
	}
	public String getAffiliateID() {
		return affiliateID;
	}
	public void setAffiliateID(String affiliateID) {
		this.affiliateID = affiliateID;
	}
	public String getFlowID() {
		return flowID;
	}
	public void setFlowID(String flowID) {
		this.flowID = flowID;
	}
	public String getDnis() {
		return dnis;
	}
	public void setDnis(String dnis) {
		this.dnis = dnis;
	}
	public String getCaller_ani() {
		return caller_ani;
	}
	public void setCaller_ani(String caller_ani) {
		this.caller_ani = caller_ani;
	}
	public String getCall_start_time() {
		return call_start_time;
	}
	public void setCall_start_time(String call_start_time) {
		this.call_start_time = call_start_time;
	}
	public String getCall_end_time() {
		return call_end_time;
	}
	public void setCall_end_time(String call_end_time) {
		this.call_end_time = call_end_time;
	}
	public String getLast_agent_id() {
		return last_agent_id;
	}
	public void setLast_agent_id(String last_agent_id) {
		this.last_agent_id = last_agent_id;
	}
	public String getLast_agent_full_name() {
		return last_agent_full_name;
	}
	public void setLast_agent_full_name(String last_agent_full_name) {
		this.last_agent_full_name = last_agent_full_name;
	}
	public String getAgent_count() {
		return agent_count;
	}
	public void setAgent_count(String agent_count) {
		this.agent_count = agent_count;
	}
	public String getAgent_ring_time_in_secs() {
		return agent_ring_time_in_secs;
	}
	public void setAgent_ring_time_in_secs(String agent_ring_time_in_secs) {
		this.agent_ring_time_in_secs = agent_ring_time_in_secs;
	}
	public String getCaller_hold_length_in_secs() {
		return caller_hold_length_in_secs;
	}
	public void setCaller_hold_length_in_secs(String caller_hold_length_in_secs) {
		this.caller_hold_length_in_secs = caller_hold_length_in_secs;
	}
	public String getCaller_queue_length_in_secs() {
		return caller_queue_length_in_secs;
	}
	public void setCaller_queue_length_in_secs(String caller_queue_length_in_secs) {
		this.caller_queue_length_in_secs = caller_queue_length_in_secs;
	}
	public String getCaller_talk_time_in_secs() {
		return caller_talk_time_in_secs;
	}
	public void setCaller_talk_time_in_secs(String caller_talk_time_in_secs) {
		this.caller_talk_time_in_secs = caller_talk_time_in_secs;
	}
	public String getAgent_talk_time_in_secs() {
		return agent_talk_time_in_secs;
	}
	public void setAgent_talk_time_in_secs(String agent_talk_time_in_secs) {
		this.agent_talk_time_in_secs = agent_talk_time_in_secs;
	}
	public String getAgent_after_call_work_time_in_secs() {
		return agent_after_call_work_time_in_secs;
	}
	public void setAgent_after_call_work_time_in_secs(String agent_after_call_work_time_in_secs) {
		this.agent_after_call_work_time_in_secs = agent_after_call_work_time_in_secs;
	}
	public String getTotal_caller_time_in_secs() {
		return total_caller_time_in_secs;
	}
	public void setTotal_caller_time_in_secs(String total_caller_time_in_secs) {
		this.total_caller_time_in_secs = total_caller_time_in_secs;
	}
	public String getTotal_agent_time_secs() {
		return total_agent_time_secs;
	}
	public void setTotal_agent_time_secs(String total_agent_time_secs) {
		this.total_agent_time_secs = total_agent_time_secs;
	}
	public String getState_code() {
		return state_code;
	}
	public void setState_code(String state_code) {
		this.state_code = state_code;
	}
	public String getAfter_hours() {
		return after_hours;
	}
	public void setAfter_hours(String after_hours) {
		this.after_hours = after_hours;
	}
	public String getVoicemail() {
		return voicemail;
	}
	public void setVoicemail(String voicemail) {
		this.voicemail = voicemail;
	}
	
	public String getFinal_disposition() {
		return final_disposition;
	}
	public void setFinal_disposition(String final_disposition) {
		this.final_disposition = final_disposition;
	}
	
	public boolean isAbandoned() {
		return abandoned;
	}
	public void setAbandoned(boolean abandoned) {
		this.abandoned = abandoned;
	}
	public boolean isExternal_transfer_call() {
		return external_transfer_call;
	}
	public void setExternal_transfer_call(boolean external_transfer_call) {
		this.external_transfer_call = external_transfer_call;
	}
		
}
