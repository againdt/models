package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import com.getinsured.hix.model.AccountUser;

public class EnrollmentBrokerUpdateDTO implements Serializable{

	private Integer enrollmentId ;
	private String houseHoldCaseId ;
	private String exchgIndividualIdentifier;
	private Integer employerEnrollmentId;
	private Integer oldAssisterBrokerId;
	private Integer assisterBrokerId;
	private String roleType; 			/**	possible values AGENT/ASSISTER	*/
	private String addremoveAction; 	/**	possible values Add/ Remove 	*/
	private String agentBrokerName ;
	private String brokerTPAFlag;		/**	possible values Y/N             */
	private String stateLicenseNumber;	/** relates to StateLicenseNumber() */
	private String stateEINNumber; 		/** relates to StateEINNumber  	   	*/
	private String brokerTPAAccountNumber2 ;
	private String marketType ;			/**	possible values FI/24       	*/
	private String transferStartDate;
	private String transferEndDate;
	private AccountUser user;
	private String agentNPN;
	
	
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}
	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}
	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}
	public Integer getAssisterBrokerId() {
		return assisterBrokerId;
	}
	public void setAssisterBrokerId(Integer assisterBrokerId) {
		this.assisterBrokerId = assisterBrokerId;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	public String getAddremoveAction() {
		return addremoveAction;
	}
	public void setAddremoveAction(String addremoveAction) {
		this.addremoveAction = addremoveAction;
	}
	public String getAgentBrokerName() {
		return agentBrokerName;
	}
	public void setAgentBrokerName(String agentBrokerName) {
		this.agentBrokerName = agentBrokerName;
	}
	public String getBrokerTPAFlag() {
		return brokerTPAFlag;
	}
	public void setBrokerTPAFlag(String brokerTPAFlag) {
		this.brokerTPAFlag = brokerTPAFlag;
	}
	public String getStateLicenseNumber() {
		return stateLicenseNumber;
	}
	public void setStateLicenseNumber(String stateLicenseNumber) {
		this.stateLicenseNumber = stateLicenseNumber;
	}
	public String getStateEINNumber() {
		
		return stateEINNumber;
	}
	public void setStateEINNumber(String stateEINNumber) {
		this.stateEINNumber = stateEINNumber;
	}
	public String getBrokerTPAAccountNumber2() {
		return brokerTPAAccountNumber2;
	}
	public void setBrokerTPAAccountNumber2(String brokerTPAAccountNumber2) {
		this.brokerTPAAccountNumber2 = brokerTPAAccountNumber2;
	}
	public String getMarketType() {
		return marketType;
	}
	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
	
	public String getExchgIndividualIdentifier() {
		return exchgIndividualIdentifier;
	}
	public void setExchgIndividualIdentifier(String exchgIndividualIdentifier) {
		this.exchgIndividualIdentifier = exchgIndividualIdentifier;
	}
			
	public Integer getOldAssisterBrokerId() {
		return oldAssisterBrokerId;
	}
	public void setOldAssisterBrokerId(Integer oldAssisterBrokerId) {
		this.oldAssisterBrokerId = oldAssisterBrokerId;
	}
	
	public String getTransferStartDate() {
		return transferStartDate;
	}
	public void setTransferStartDate(String transferStartDate) {
		this.transferStartDate = transferStartDate;
	}
	public String getTransferEndDate() {
		return transferEndDate;
	}
	public void setTransferEndDate(String transferEndDate) {
		this.transferEndDate = transferEndDate;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	
	public AccountUser getUser() {
		return user;
	}
	public void setUser(AccountUser user) {
		this.user = user;
	}
	
	public String getAgentNPN() {
		return agentNPN;
	}
	public void setAgentNPN(String agentNPN) {
		this.agentNPN = agentNPN;
	}
	@Override
	public String toString() {
		return "EnrollmentBrokerUpdateDTO [enrollmentId=" + enrollmentId + ",houseHoldCaseId=" + houseHoldCaseId + ", exchgIndividualIdentifier="
				+ exchgIndividualIdentifier + ", employerEnrollmentId=" + employerEnrollmentId
				+ ", oldAssisterBrokerId=" + oldAssisterBrokerId + ", assisterBrokerId=" + assisterBrokerId
				+ ", roleType=" + roleType + ", addremoveAction=" + addremoveAction + ", agentBrokerName="
				+ agentBrokerName + ", brokerTPAFlag=" + brokerTPAFlag + ", stateLicenseNumber=" + stateLicenseNumber
				+ ", stateEINNumber=" + stateEINNumber + ", brokerTPAAccountNumber2=" + brokerTPAAccountNumber2
				+ ", marketType=" + marketType + ", transferStartDate=" + transferStartDate + ", transferEndDate="
				+ transferEndDate + "]";
	}
	
	
	
}
