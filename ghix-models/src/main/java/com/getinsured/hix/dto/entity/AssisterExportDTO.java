package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.Date;

public class AssisterExportDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String firstName;
	private String lastName;
	private String entityName;
	private String certificationStatus;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private Long assisterNumber;
	private String primaryPhoneNumber;
	private String emailAddress;
	private Date recertificationDate;
	private String prefMethodOfCommunication;
	private int pendingRequestsCount;
	private int activeRequestsCount;
	private int inactiveRequestsCount;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Long getAssisterNumber() {
		return assisterNumber;
	}

	public void setAssisterNumber(Long assisterNumber) {
		this.assisterNumber = assisterNumber;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Date getRecertificationDate() {
		return recertificationDate;
	}

	public void setRecertificationDate(Date recertificationDate) {
		this.recertificationDate = recertificationDate;
	}

	public String getPrefMethodOfCommunication() {
		return prefMethodOfCommunication;
	}

	public void setPrefMethodOfCommunication(String prefMethodOfCommunication) {
		this.prefMethodOfCommunication = prefMethodOfCommunication;
	}

	public int getPendingRequestsCount() {
		return pendingRequestsCount;
	}

	public void setPendingRequestsCount(int pendingRequestsCount) {
		this.pendingRequestsCount = pendingRequestsCount;
	}

	public int getActiveRequestsCount() {
		return activeRequestsCount;
	}

	public void setActiveRequestsCount(int activeRequestsCount) {
		this.activeRequestsCount = activeRequestsCount;
	}

	public int getInactiveRequestsCount() {
		return inactiveRequestsCount;
	}

	public void setInactiveRequestsCount(int inactiveRequestsCount) {
		this.inactiveRequestsCount = inactiveRequestsCount;
	}

}
