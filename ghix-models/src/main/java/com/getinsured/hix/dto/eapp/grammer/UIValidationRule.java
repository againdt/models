package com.getinsured.hix.dto.eapp.grammer;

import org.springframework.util.StringUtils;

public enum UIValidationRule {
	NAME("name"), REQUIRED("required"), EMAIL("email");
	
	private String value;
	
	private UIValidationRule(String val) {
		this.value = val;
	}
	
	/**
	 * Applies given rule to input text
	 * @param rule
	 * @param text
	 * @return
	 */
	public static boolean valid(UIValidationRule rule, Object text){
		switch(rule){
		case NAME: 
				break;
		case REQUIRED:
				if(text instanceof String){
					if(StringUtils.isEmpty(text)){
						return false;
					}
				}
				/**
				 * for cases where it is an object and not present
				 */
				if(text==null){
					return false;
				}
				
				return true;
		case EMAIL:
				break;
		}
		
		return false;
	}
	
}
