/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Srikanth Nakka
 *
 */
public class EnrollmentDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "externalEnrollmentId cannot be empty.")
	@Length(max = 250, message = "Invalid externalEnrollmentId, MaxLength=250")
	private String externalEnrollmentId;
	
	@Valid
	private CarrierInfo carrierInfo;
	
	@Valid
	private AgentInfo agentInfo;
	
	@Valid
	private PlanInfo planInfo;
	
	private EnrollmentIds enrollmentIds;

	@Valid
	private EnrollmentDates enrollmentDates;
		
	@NotEmpty(message = "status cannot be empty.")
	private String status;
	
	private String verificationReason;
	private String renewalFlag;
	private String exchangeType;
	private Boolean isCommissonableFlag;
	
	@NotEmpty(message = "tenantCode cannot be empty.")
	private String tenantCode;
	
	@Valid
	private List<Enrollee> enrollees;

	public String getExternalEnrollmentId() {
		return externalEnrollmentId;
	}

	public void setExternalEnrollmentId(String externalEnrollmentId) {
		this.externalEnrollmentId = externalEnrollmentId;
	}

	public CarrierInfo getCarrierInfo() {
		return carrierInfo;
	}

	public void setCarrierInfo(CarrierInfo carrierInfo) {
		this.carrierInfo = carrierInfo;
	}
	
	/**
	 * @return the agentInfo
	 */
	public AgentInfo getAgentInfo() {
		return agentInfo;
	}

	/**
	 * @param agentInfo the agentInfo to set
	 */
	public void setAgentInfo(AgentInfo agentInfo) {
		this.agentInfo = agentInfo;
	}

	public PlanInfo getPlanInfo() {
		return planInfo;
	}

	public void setPlanInfo(PlanInfo planInfo) {
		this.planInfo = planInfo;
	}

	public EnrollmentIds getEnrollmentIds() {
		return enrollmentIds;
	}

	public void setEnrollmentIds(EnrollmentIds enrollmentIds) {
		this.enrollmentIds = enrollmentIds;
	}

	public EnrollmentDates getEnrollmentDates() {
		return enrollmentDates;
	}

	public void setEnrollmentDates(EnrollmentDates enrollmentDates) {
		this.enrollmentDates = enrollmentDates;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVerificationReason() {
		return verificationReason;
	}

	public void setVerificationReason(String verificationReason) {
		this.verificationReason = verificationReason;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public Boolean getIsCommissonableFlag() {
		return isCommissonableFlag;
	}

	public void setIsCommissonableFlag(Boolean isCommissonableFlag) {
		this.isCommissonableFlag = isCommissonableFlag;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public List<Enrollee> getEnrollees() {
		return enrollees;
	}

	public void setEnrollees(List<Enrollee> enrollees) {
		this.enrollees = enrollees;
	}
	
	
}
