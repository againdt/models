ALTER TABLE BROKERS MODIFY(COMMENTS  VARCHAR2(4000 CHAR))
;
ALTER TABLE BROKERS MODIFY(ABOUT_ME  VARCHAR2(4000 CHAR))
;
ALTER TABLE BROKERS MODIFY(RESPONSE_DESCRIPTION  VARCHAR2(4000 CHAR))
;
ALTER TABLE BROKERS_AUD MODIFY(COMMENTS  VARCHAR2(4000 CHAR))
;
ALTER TABLE BROKERS_AUD MODIFY(ABOUT_ME  VARCHAR2(4000 CHAR))
;
ALTER TABLE COMMENTS MODIFY(COMMENT_TEXT  VARCHAR2(4000 CHAR))
;
ALTER TABLE EE_ASSISTERS MODIFY(COMMENTS  VARCHAR2(4000 CHAR))
;
ALTER TABLE EE_ASSISTERS_AUD MODIFY(COMMENTS  VARCHAR2(4000 CHAR))
;
ALTER TABLE EE_ENTITIES MODIFY(COMMENTS  VARCHAR2(4000 CHAR))
;
ALTER TABLE EE_ENTITIES_AUD MODIFY(COMMENTS  VARCHAR2(4000 CHAR))
;
ALTER TABLE EMPLOYEES ADD LAST_VISITED VARCHAR2(200 CHAR)     NULL
;
COMMENT ON COLUMN EMPLOYEES.LAST_VISITED IS
'Employee last visited link.'
;
ALTER TABLE EMPLOYEES ADD SHOPPING_ID VARCHAR2(100 CHAR)     NULL
;
COMMENT ON COLUMN EMPLOYEES.SHOPPING_ID IS
'Employee Plan Selection Shopping Id.'
;
ALTER TABLE EMPLOYERS ADD PARTICIPATION_MEETS VARCHAR2(5 CHAR)     NULL
;
COMMENT ON COLUMN EMPLOYERS.PARTICIPATION_MEETS IS
'Stores YES;NO to indicate whether the Employer Participation Meets or Not.'
;
ALTER TABLE EMPLOYERS ADD CONTACT_LOCATION NUMBER     NULL
;
COMMENT ON COLUMN EMPLOYERS.CONTACT_LOCATION IS
'Employer contact location id'
;
ALTER TABLE EMPLOYERS ADD ELIGIBITY_DECISION_FACTOR_NOTE VARCHAR2(2000 CHAR)     NULL
;
ALTER TABLE EMPLOYERS_AUD ADD PARTICIPATION_MEETS VARCHAR2(5 CHAR)     NULL
;
COMMENT ON COLUMN EMPLOYERS_AUD.PARTICIPATION_MEETS IS
'Stores YES;NO to indicate whether the Employer Participation Meets or Not.'
;
ALTER TABLE EMPLOYERS_AUD ADD CONTACT_LOCATION NUMBER(10)     NULL
;
COMMENT ON COLUMN EMPLOYERS_AUD.CONTACT_LOCATION IS
'Stores employers contact location.'
;
ALTER TABLE EMPLOYERS_AUD ADD ELIGIBITY_DECISION_FACTOR_NOTE VARCHAR2(2000 CHAR)     NULL
;
ALTER TABLE LOCATIONS ADD COUNTY_CODE VARCHAR2(30 CHAR)     NULL
;
COMMENT ON COLUMN LOCATIONS.COUNTY_CODE IS
'County Code'
;
ALTER TABLE PAYMENT_METHODS DROP CONSTRAINT FK_EMPLOYER_ID
;
ALTER TABLE PAYMENT_METHODS DROP (EMPLOYER_ID)
;
ALTER TABLE PERMISSIONS MODIFY(NAME   NULL)
;
ALTER TABLE ROLE_PERMISSIONS MODIFY(ROLE_ID   NULL)
;
ALTER TABLE ROLE_PERMISSIONS MODIFY(PERMISSION_ID   NULL)
;
ALTER TABLE EMPLOYERS ADD CONSTRAINT FK_CONTACT_LOCATION_ID
FOREIGN KEY (CONTACT_LOCATION)
REFERENCES LOCATIONS (ID)
ENABLE VALIDATE
;

-- Drop Constraint, Rename and Create Table SQL

ALTER TABLE ZIPCODES DROP PRIMARY KEY DROP INDEX
;
ALTER TABLE ZIPCODES RENAME TO ZIPCODES_08132013220900000
;
CREATE TABLE ZIPCODES
(
    ID          NUMBER             NOT NULL,
    ZIPCODE     VARCHAR2(5 CHAR)       NULL,
    CITY        VARCHAR2(60 CHAR)      NULL,
    CITY_TYPE   VARCHAR2(60 CHAR)  DEFAULT 'B' NOT NULL,
    COUNTY      VARCHAR2(60 CHAR)      NULL,
    COUNTY_FIPS VARCHAR2(60 CHAR)      NULL,
    STATE_NAME  VARCHAR2(100 CHAR)     NULL,
    STATE       VARCHAR2(2 CHAR)       NULL,
    STATE_FIPS  VARCHAR2(10 CHAR)      NULL,
    MSA         VARCHAR2(16 CHAR)      NULL,
    AREA_CODE   VARCHAR2(3 CHAR)       NULL,
    TIME_ZONE   VARCHAR2(64 CHAR)      NULL,
    GMT_OFFSET  NUMBER             DEFAULT 0 NOT NULL,
    DST         VARCHAR2(1 CHAR)       NULL,
    LATTITUDE   NUMBER                 NULL,
    LONGITUDE   NUMBER                 NULL
)
;
COMMENT ON COLUMN ZIPCODES.ZIPCODE IS
'Zip code'
;
COMMENT ON COLUMN ZIPCODES.STATE_NAME IS
'Name of the State to which the zip code belongs to'
;
COMMENT ON COLUMN ZIPCODES.STATE IS
'Two character state code;abbreviation'
;
COMMENT ON COLUMN ZIPCODES.STATE_FIPS IS
'Currently state FIPS is included in county FIPS'
;
COMMENT ON COLUMN ZIPCODES.MSA IS
'Metropolitan Statistical Area code'
;
COMMENT ON COLUMN ZIPCODES.AREA_CODE IS
'The most prevalent area code for the ZIP code'
;
COMMENT ON COLUMN ZIPCODES.TIME_ZONE IS
'Timezone in the zip'
;
COMMENT ON COLUMN ZIPCODES.GMT_OFFSET IS
'The GMT offset indicates the difference in hours between GMT and the time zone for that particular ZIP Code'
;
COMMENT ON COLUMN ZIPCODES.DST IS
'Does zipcode participate in Daylight Saving Timg. Possible values are Y or N'
;
COMMENT ON COLUMN ZIPCODES.LATTITUDE IS
'The Latitude of the geographic centroid of the ZIP code area'
;
COMMENT ON COLUMN ZIPCODES.LONGITUDE IS
'The Longitude of the geographic centroid of the ZIP code area'
;

-- Insert Data SQL

ALTER SESSION ENABLE PARALLEL DML
;
INSERT INTO ZIPCODES(
                               ID,
                               ZIPCODE,
                               CITY,
                               CITY_TYPE,
                               COUNTY,
                               COUNTY_FIPS,
                               STATE_NAME,
                               STATE,
                               STATE_FIPS,
                               MSA,
                               AREA_CODE,
                               TIME_ZONE,
                               GMT_OFFSET,
                               DST,
                               LATTITUDE,
                               LONGITUDE
                              )
                        SELECT 
                               ID,
                               ZIPCODE,
                               CITY,
                               CITY_TYPE,
                               COUNTY,
                               COUNTY_FIPS,
                               SUBSTR(STATE_NAME, 1, 100),
                               SUBSTR(STATE, 1, 2),
                               STATE_FIPS,
                               MSA,
                               SUBSTR(AREA_CODE, 1, 3),
                               TIME_ZONE,
                               GMT_OFFSET,
                               SUBSTR(DST, 1, 1),
                               LATTITUDE,
                               LONGITUDE
                          FROM ZIPCODES_08132013220900000 
;
COMMIT
;
ALTER TABLE ZIPCODES LOGGING
;

-- Add Constraint SQL

ALTER TABLE ZIPCODES ADD CONSTRAINT PK_ZIPCODES
PRIMARY KEY (ID)
;

-- clean up duplicate LOOKUP_TYPE with no child table

delete from LOOKUP_TYPE
where LOOKUP_TYPE_ID in (
select a.LOOKUP_TYPE_ID
from LOOKUP_TYPE a, LOOKUP_VALUE b
where a.LOOKUP_TYPE_ID=b.LOOKUP_TYPE_ID (+)
and b.LOOKUP_TYPE_ID is null
and a.name in (select name from LOOKUP_TYPE group by name having count(*) >1)
);


