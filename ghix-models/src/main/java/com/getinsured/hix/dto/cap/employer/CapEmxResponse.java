package com.getinsured.hix.dto.cap.employer;

import java.io.Serializable;
import com.getinsured.hix.model.emx.EmxEmployerDTO;

/**
 * Response DTO to get the data from EMX for employer related transactions.
 * @author hardas_d
 *
 */
public class CapEmxResponse implements Serializable {

	private static final long serialVersionUID = 4360948301669577408L;
	private String status;
	private String errorMsg;

	private Integer employerId;
	private EmxEmployerDTO employerDTO;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}
	
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public EmxEmployerDTO getEmployerDTO() {
		return employerDTO;
	}

	public void setEmployerDTO(EmxEmployerDTO employerDTO) {
		this.employerDTO = employerDTO;
	}

}
