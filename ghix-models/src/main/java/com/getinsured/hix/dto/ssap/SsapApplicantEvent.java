package com.getinsured.hix.dto.ssap;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;

@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class SsapApplicantEvent {
	
	private Long id;
	
	@NotNull
	private Integer lastUpdatedUserId;
	
	@NotNull
	private ApplicantEventValidationStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getLastUpdatedUserId() {
		return lastUpdatedUserId;
	}

	public void setLastUpdatedUserId(Integer lastUpdatedUserId) {
		this.lastUpdatedUserId = lastUpdatedUserId;
	}

	public ApplicantEventValidationStatus getStatus() {
		return status;
	}

	public void setStatus(ApplicantEventValidationStatus status) {
		this.status = status;
	}
	
	

}
