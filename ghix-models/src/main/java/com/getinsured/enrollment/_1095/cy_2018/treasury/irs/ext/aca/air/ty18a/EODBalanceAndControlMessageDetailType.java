
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;EOD Balance And Control Message Detail
 * 					&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for EODBalanceAndControlMessageDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EODBalanceAndControlMessageDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EODFileTransmissionDt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TotalAugmentedFilesCnt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AugmentedFileDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EODBalanceAndControlMessageDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "eodFileTransmissionDt",
    "totalAugmentedFilesCnt",
    "augmentedFileDetail"
})
public class EODBalanceAndControlMessageDetailType {

    @XmlElement(name = "EODFileTransmissionDt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar eodFileTransmissionDt;
    @XmlElement(name = "TotalAugmentedFilesCnt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String totalAugmentedFilesCnt;
    @XmlElement(name = "AugmentedFileDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<AugmentedFileDetailType> augmentedFileDetail;

    /**
     * Gets the value of the eodFileTransmissionDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEODFileTransmissionDt() {
        return eodFileTransmissionDt;
    }

    /**
     * Sets the value of the eodFileTransmissionDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEODFileTransmissionDt(XMLGregorianCalendar value) {
        this.eodFileTransmissionDt = value;
    }

    /**
     * Gets the value of the totalAugmentedFilesCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalAugmentedFilesCnt() {
        return totalAugmentedFilesCnt;
    }

    /**
     * Sets the value of the totalAugmentedFilesCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalAugmentedFilesCnt(String value) {
        this.totalAugmentedFilesCnt = value;
    }

    /**
     * Gets the value of the augmentedFileDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the augmentedFileDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAugmentedFileDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AugmentedFileDetailType }
     * 
     * 
     */
    public List<AugmentedFileDetailType> getAugmentedFileDetail() {
        if (augmentedFileDetail == null) {
            augmentedFileDetail = new ArrayList<AugmentedFileDetailType>();
        }
        return this.augmentedFileDetail;
    }

}
