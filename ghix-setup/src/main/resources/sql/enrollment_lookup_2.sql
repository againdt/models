
INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'GENDER', 'string');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'F','Female','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'M','Male','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'U','Unknown','');

INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'RACE', 'string');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'7','Not Provided','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'8','Not Applicable','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'A','Asian or Pacific Islander','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'B','Black','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'C','Caucasian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'D','Subcontinent Asian American','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'E','Other Race or Ethnicity','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'F','Asian Pacific American','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'G','Native American','');


INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'H','Hispanic','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'I','American Indian or Alaskan Native','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'J','Native Hawaiian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'N','Black (Non-Hispanic)','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'O','White (Non-Hispanic)','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'P','Pacific Islander','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'Z','Mutually Defined','');

INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'MARITAL_STATUS','string');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'B','Registered Domestic Partner','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'D','Divorced','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'I','Single','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'M','Married','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'R','Unreported','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'S','Separated','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'U','Unmarried (Single or Divorced or Widowed)','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'W','Widowed','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'X','Legally Separated','');


INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'TOBACCO_USAGE','string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'N','No Tobacco Use','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'T','Tobacco Use','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'U','Unknown Tobacco Use','');

INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'CITIZENSHIP_STATUS','string');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1','U.S. Citizen','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'2','Non-Resident Alien','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'3','Resident Alien','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'4','Illegal Alien','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'5','Alien','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'6','U.S. Citizen - Non-Resident','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'7','U.S. Citizen - Resident','');


INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'LANGUAGE','string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'EN','English','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'ES','Spanish','');

INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'ENROLLMENT_STATUS','string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'CANCEL','Cancelled','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'TERM','Terminated','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'CONFIRM','Enrolled','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'ORDER_CONFIRMED','Order Confirmed','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'PAYMENT_RECEIVED','Payment Received','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'PENDING','Pending','');

