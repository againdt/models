#!/bin/bash
source DB_ENV.sh
DATALOAD_DIR=/home/oracle/dataload_dir
#cd /home/oracle/dataload_dir

if [ $# -ne 2 ]
then
  echo " "
  echo "Usage $0 <SCHEMA_OWNER> <PASSWORD>"
  echo "Example: $0 GHIX1_OWNER GHIX1_OWNER"
  echo " "
  exit 2
fi

echo $1
echo $2

cat $DATALOAD_DIR/initial_setup_TEMPLATE.sql|sed "s/TEMPLATE_OWNER/$1/g" >initial_setup_$1.sql 
sqlplus  /nolog <<EOF1
connect dev_jt/DEV_JT@$EZCONNECT 
@$DATALOAD_DIR/initial_setup_$1.sql 
EOF1

echo $1
echo $2
sqlplus $1/$2@$EZCONNECT @$DATALOAD_DIR/EXT_PASSIVE_ENROLLMENT_DATA.sql 
wait
##sqlplus $1/$2@$EZCONNECT @$DATALOAD_DIR/CARRIER_DATA_ID_HIST.sql
##wait

