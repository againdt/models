package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Gender;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;

@Entity
@Table(name="PD_PERSON")
public class PdPerson implements Serializable
{	
	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pd_person_seq")
	@SequenceGenerator(name = "pd_person_seq", sequenceName = "pd_person_seq", allocationSize = 1)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pd_household_id")
	private PdHousehold pdHousehold;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="birth_day")
	private Date birthDay;
	
	@Column(name="external_id")
    private String externalId;
    
    @Column(name="relationship")
    @Enumerated(EnumType.STRING)
    private Relationship relationship;
    
    @Column(name = "zip_code")
   	private String zipCode;
    
    @Column(name = "county_code")
   	private String countyCode;
    
    @Column(name="tobacco")
    @Enumerated(EnumType.STRING)
	private YorN tobacco;
    
    @Column(name = "catastrophic_eligible")
	@Enumerated(EnumType.STRING)
	private YorN catastrophicEligible;
    
    @Column(name = "health_coverage_start_date")
	private Date healthCoverageStartDate;
    
    @Column(name = "dental_coverage_start_date")
	private Date dentalCoverageStartDate;
    
	@Column(name = "health_subsidy")
	private Float healthSubsidy;
	
	@Column(name = "dental_subsidy")
	private Float dentalSubsidy;
	
	@Column(name = "seek_coverage")
	@Enumerated(EnumType.STRING)
	private YorN seekCoverage;
	
	@Column(name="person_data")
	private String personData;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="creation_timestamp")
	private Date creationTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date lastUpdateTimestamp;    
	
    @Column(name = "new_person_flag")
	@Enumerated(EnumType.STRING)
	private YorN newPersonFlag;
    
    @Column(name = "maintenance_reason_code")
	private String maintenanceReasonCode;
	
	@Column(name="health_enrollment_id")
    private Long healthEnrollmentId;
	
	@Column(name="dental_enrollment_id")
    private Long dentalEnrollmentId;
	
	@Column(name="gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;
  
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public PdHousehold getPdHousehold() {
		return pdHousehold;
	}

	public void setPdHousehold(PdHousehold pdHousehold) {
		this.pdHousehold = pdHousehold;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Relationship getRelationship() {
		return relationship;
	}

	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public YorN getTobacco() {
		return tobacco;
	}

	public void setTobacco(YorN tobacco) {
		this.tobacco = tobacco;
	}

	public YorN getCatastrophicEligible() {
		return catastrophicEligible;
	}

	public void setCatastrophicEligible(YorN catastrophicEligible) {
		this.catastrophicEligible = catastrophicEligible;
	}

	public Date getHealthCoverageStartDate() {
		return healthCoverageStartDate;
	}

	public void setHealthCoverageStartDate(Date healthCoverageStartDate) {
		this.healthCoverageStartDate = healthCoverageStartDate;
	}
	
	public Date getDentalCoverageStartDate() {
		return dentalCoverageStartDate;
	}

	public void setDentalCoverageStartDate(Date dentalCoverageStartDate) {
		this.dentalCoverageStartDate = dentalCoverageStartDate;
	}

	public Float getHealthSubsidy() {
		return healthSubsidy;
	}

	public void setHealthSubsidy(Float healthSubsidy) {
		this.healthSubsidy = healthSubsidy;
	}

	public Float getDentalSubsidy() {
		return dentalSubsidy;
	}

	public void setDentalSubsidy(Float dentalSubsidy) {
		this.dentalSubsidy = dentalSubsidy;
	}

	public YorN getSeekCoverage() {
		return seekCoverage;
	}

	public void setSeekCoverage(YorN seekCoverage) {
		this.seekCoverage = seekCoverage;
	}

	public String getPersonData() {
		return personData;
	}

	public void setPersonData(String personData) {
		this.personData = personData;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	public YorN getNewPersonFlag() {
		return newPersonFlag;
	}

	public void setNewPersonFlag(YorN newPersonFlag) {
		this.newPersonFlag = newPersonFlag;
	}

	public String getMaintenanceReasonCode() {
		return maintenanceReasonCode;
	}

	public void setMaintenanceReasonCode(String maintenanceReasonCode) {
		this.maintenanceReasonCode = maintenanceReasonCode;
	}
	
	public Long getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	public void setHealthEnrollmentId(Long healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}
	
	public Long getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(Long dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

}
