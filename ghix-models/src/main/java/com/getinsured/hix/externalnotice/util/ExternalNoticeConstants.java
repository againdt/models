package com.getinsured.hix.externalnotice.util;

import org.springframework.stereotype.Component;

@Component
public class ExternalNoticeConstants {
	
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int TEN = 10;
	public static final int TWENTY_THREE = 23;
	public static final int FIFTY_NINE = 59;
	public static final int NINE_NINETY_NINE = 999;
	public static final int THREE_NINE_NINE_NINE = 3999;
	public static final String SUCCESS ="SUCCESS";
	
	public static final int ERROR_CODE_200 = 200;
    public static final int ERROR_CODE_201 = 201;
    public static final int ERROR_CODE_202 = 202;
    public static final int ERROR_CODE_203 = 203;
    public static final int ERROR_CODE_204 = 204;
    public static final int ERROR_CODE_205 = 205;
    public static final int ERROR_CODE_206 = 206;
    public static final int ERROR_CODE_207 = 207;
    public static final int ERROR_CODE_208 = 208;
    public static final int ERROR_CODE_209 = 209;
    public static final int ERROR_CODE_210 = 210;
    
    public static final String BATCH_JOB_STATUS= "COMPLETED";
    public static final String BATCH_DATE_FORMAT = "yyyyMMddHHmmss";
    public static final String DEFAULT_BATCH_START_DATE = "19000101010000";
	public static final String BATCH_JOB_STATUS_STOPPING = "STOPPING";
	public static final String BATCH_JOB_STATUS_STOPPED = "STOPPED";
	public static final String BATCH_STOP_MSG="Manually requested to Stop Job, so throwing non skippable exception to stop immediately";

    public static final String DOB_FORMAT = "MM/dd/YYYY";
    public static final String COVERAGE_YEAR_FORMAT = "YYYY";
    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String STATUS_SENT = "SENT";
    public static final String STATUS_FAILED = "FAILED";
    public static final String ERROR_RESPONSE = "Error";
    public static final String RESPONSE = "Response";
    public static final String CODE = "code";
    public static final String ERROR_RESPONSE_200 = "200";
    public static final String PDF_1095 = "1095A_Form";

}