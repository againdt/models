//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.18 at 02:15:10 PM PDT 
//


package com.getinsured.hix.dto.planmgmt.template.pdben.extension;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.hix.dto.planmgmt.template.pdben.hix.pm.DrugCostSharingTierType;
import com.getinsured.hix.dto.planmgmt.template.pdben.niem.proxy.xsd.AnyURI;



/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.ffe.formulary.extension._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IssuerFormulary_QNAME = new QName("http://formulary.ffe.cms.gov/extension/1.0", "IssuerFormulary");
    private final static QName _Issuer_QNAME = new QName("http://formulary.ffe.cms.gov/extension/1.0", "Issuer");
    private final static QName _DrugCostSharingTier_QNAME = new QName("http://formulary.ffe.cms.gov/extension/1.0", "DrugCostSharingTier");
    private final static QName _FormularyURI_QNAME = new QName("http://formulary.ffe.cms.gov/extension/1.0", "FormularyURI");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.ffe.formulary.extension._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MyClassType }
     * 
     */
    public MyClassType createMyClassType() {
        return new MyClassType();
    }

    /**
     * Create an instance of {@link MyClass1Type }
     * 
     */
    public MyClass1Type createMyClass1Type() {
        return new MyClass1Type();
    }

    /**
     * Create an instance of {@link MyClass2Type }
     * 
     */
    public MyClass2Type createMyClass2Type() {
        return new MyClass2Type();
    }

    /**
     * Create an instance of {@link IssuerType }
     * 
     */
    public IssuerType createIssuerType() {
        return new IssuerType();
    }

    /**
     * Create an instance of {@link FormularyType }
     * 
     */
    public FormularyType createFormularyType() {
        return new FormularyType();
    }

    /**
     * Create an instance of {@link PayloadType }
     * 
     */
    public PayloadType createPayloadType() {
        return new PayloadType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://formulary.ffe.cms.gov/extension/1.0", name = "IssuerFormulary")
    public JAXBElement<FormularyType> createIssuerFormulary(FormularyType value) {
        return new JAXBElement<FormularyType>(_IssuerFormulary_QNAME, FormularyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssuerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://formulary.ffe.cms.gov/extension/1.0", name = "Issuer")
    public JAXBElement<IssuerType> createIssuer(IssuerType value) {
        return new JAXBElement<IssuerType>(_Issuer_QNAME, IssuerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DrugCostSharingTierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://formulary.ffe.cms.gov/extension/1.0", name = "DrugCostSharingTier")
    public JAXBElement<DrugCostSharingTierType> createDrugCostSharingTier(DrugCostSharingTierType value) {
        return new JAXBElement<DrugCostSharingTierType>(_DrugCostSharingTier_QNAME, DrugCostSharingTierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnyURI }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://formulary.ffe.cms.gov/extension/1.0", name = "FormularyURI")
    public JAXBElement<AnyURI> createFormularyURI(AnyURI value) {
        return new JAXBElement<AnyURI>(_FormularyURI_QNAME, AnyURI.class, null, value);
    }

}
