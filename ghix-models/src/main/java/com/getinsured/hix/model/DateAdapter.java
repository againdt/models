package com.getinsured.hix.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateAdapter extends XmlAdapter<String, Date> {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private static final Logger LOGGER = LoggerFactory.getLogger(DateAdapter.class);
    @Override
    public String marshal(Date date) throws Exception {
        return date.toString();
    }

    @Override
    public Date unmarshal(String date) throws Exception {
      //  System.out.println("UNMAShaling date : "+ date +" TO "+dateFormat.parse(date));
    	LOGGER.info("UNMAShaling date : "+ date +" TO "+dateFormat.parse(date));
    	return dateFormat.parse(date);
    }

}
