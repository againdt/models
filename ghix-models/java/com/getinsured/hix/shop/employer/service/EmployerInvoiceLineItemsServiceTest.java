package com.getinsured.hix.shop.employer.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoices;

public class EmployerInvoiceLineItemsServiceTest extends BaseTest{

	@Autowired	private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	
	@Test
	public void testfindEmployerInvoicesById() {
		EmployerInvoices employerInvoices = new EmployerInvoices();
		employerInvoices.setId(2);
		List<EmployerInvoiceLineItems> listLineItems = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
		System.out.println("test "+listLineItems.size());
	}
	
}
