package com.getinsured.hix.dto.eapp.workflow;

public enum WorkflowName {
	SENDAPP, EMAIL, TICKET
}
