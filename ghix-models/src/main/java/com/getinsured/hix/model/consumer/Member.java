package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.getinsured.hix.dto.consumer.MemberExtensionDto;
import com.getinsured.hix.dto.eapp.application.EappApplication;
import com.getinsured.hix.model.Location;
import com.getinsured.json.type.JsonBinaryType;



/**
 * The persistent class for the CMR_MEMBER database table.
 * 
 */
@Entity
@Table(name="CMR_MEMBER")
@DynamicInsert
@DynamicUpdate
@TypeDefs({
    @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class Member implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public enum MemberBooleanFlag { YES, NO;}
   	public enum EligibilityProgramValues { MEDICAID, MEDICARE, CHIP, APTC, QHP, BLANK ;}
	public enum EligibilityStateValues { INITIAL_ELIGIBILITY_NEEDED , INITIAL_ELIGIBILITY_COMPLETE , FINAL_ELIGIBILITY_COMPLETE;}
	public enum Gender{ M, F;	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CMR_MEMBER_SEQ")
	@SequenceGenerator(name = "CMR_MEMBER_SEQ", sequenceName = "CMR_MEMBER_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@Column(name = "MIDDLE_NAME")
	private String middleName;
	
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "BIRTH_DATE")
	private Date birthDate;
	
	@Column(name = "GENDER")
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	@Column(name = "SMOKER")
	@Enumerated(EnumType.STRING)
	private MemberBooleanFlag smoker;
	
	@Column(name = "TAX_ID_NUMBER")
	private String taxIDNumber;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable=false)
	private Date created;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name="LOCATION_ID")
	private Location contactLocation;
	
	@ManyToOne
	@JoinColumn(name="CMR_HOUSEHOLD_ID")
	private Household household; 
	
	@Column(name="FULL_TIME_STUDENT")
	@Enumerated(EnumType.STRING)
    private MemberBooleanFlag fullTimeStudent;
	
	@Column(name="PHONE_NUMBER")
    private String phoneNumber;
	
	@Column(name="EMAIL_ADDRESS")
    private String email;
	
	@Column(name="ELIGIBILITY_END_DATE")
    private Date eligibilityEndDate;
    
	@Column(name="ELIGIBILITY_START_DATE")
    private Date eligibilityStartDate;
	
	@Column(name="ELIGIBILITY_PROGRAM")
	@Enumerated(EnumType.STRING)
    private EligibilityProgramValues eligibilityProgram;
	
	@Column(name="ELIGIBILITY_STATUS")
	@Enumerated(EnumType.STRING)
    private EligibilityStateValues eligibilityStatus;
	
	@Column(name="IS_HOUSEHOLD_CONTACT")
	@Enumerated(EnumType.STRING)
    private MemberBooleanFlag isHouseHoldContact;
	
	@Column(name="IS_SEEKING_COVERAGE")
	@Enumerated(EnumType.STRING)
    private MemberBooleanFlag isSeekingCoverage;
	
	@Column(name="SSN")
    private String ssn;
	
	@Column(name="APTC_ELIGIBILITY_INDICATOR")
	@Enumerated(EnumType.STRING)
    private MemberBooleanFlag aptcEligibilityIndicator;
	
	@Column(name="APTC_ELIGIBILITY_END_DATE")
    private Date aptcEligibilityEndDate;
    
	@Column(name="APTC_ELIGIBILITY_START_DATE")
    private Date aptcEligibilityStartDate;
	
	@Column(name="CSR_ELIGIBILITY_INDICATOR")
	@Enumerated(EnumType.STRING)
    private MemberBooleanFlag csrEligibilityIndicator;
	
	@Column(name="CSR_ELIGIBILITY_END_DATE")
    private Date csrEligibilityEndDate;
    
	@Column(name="CSR_ELIGIBILITY_START_DATE")
    private Date csrEligibilityStartDate;
	
	@Column(name="CSR_LEVEL")
    private String csrLevel;
	
	@Column(name="FFE_APPLICANT_ID")
    private Long ffeApplicantId;
	
	@Column(name="MARITAL_STATUS")
	@Enumerated(EnumType.STRING)
    private MemberBooleanFlag maritalStatus;
	
	@Column(name="MONTHLY_APTC_AMOUNT")
    private Integer monthlyAptcAmount;
	
	@Column(name = "RELATIONSHIP")
	private String relationship;
	
	@Column(name = "PREFERRED_TIME_TO_CONTACT")
	private String preferredTimeToContact;
	
	@Type(type = "jsonb")
	@Column(columnDefinition = "json",name="MEMBER_EXTENSION")
	private MemberExtensionDto memberExtension;

	@Type(type = "jsonb")
	@Column(columnDefinition = "json",name="LAUNCH_SCRIPT_DATA")
	private EappApplication launchScriptData;
	
	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}
	
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name="MAILING_LOCATION_ID")
	private Location mailingLocation;

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getTaxIDNumber() {
		return taxIDNumber;
	}

	public void setTaxIDNumber(String taxIDNumber) {
		this.taxIDNumber = taxIDNumber;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Location getContactLocation() {
		return contactLocation;
	}

	public void setContactLocation(Location contactLocation) {
		this.contactLocation = contactLocation;
	}

	public Household getHousehold() {
		return household;
	}

	public void setHousehold(Household household) {
		this.household = household;
	}
	
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public MemberBooleanFlag getSmoker() {
		return smoker;
	}

	public void setSmoker(MemberBooleanFlag smoker) {
		this.smoker = smoker;
	}

	public MemberBooleanFlag getFullTimeStudent() {
		return fullTimeStudent;
	}

	public void setFullTimeStudent(MemberBooleanFlag fullTimeStudent) {
		this.fullTimeStudent = fullTimeStudent;
	}

	public EligibilityProgramValues getEligibilityProgram() {
		return eligibilityProgram;
	}

	public void setEligibilityProgram(EligibilityProgramValues eligibilityProgram) {
		this.eligibilityProgram = eligibilityProgram;
	}

	public EligibilityStateValues getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStateValues eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public MemberBooleanFlag getIsHouseHoldContact() {
		return isHouseHoldContact;
	}

	public void setIsHouseHoldContact(MemberBooleanFlag isHouseHoldContact) {
		this.isHouseHoldContact = isHouseHoldContact;
	}

	public MemberBooleanFlag getIsSeekingCoverage() {
		return isSeekingCoverage;
	}

	public void setIsSeekingCoverage(MemberBooleanFlag isSeekingCoverage) {
		this.isSeekingCoverage = isSeekingCoverage;
	}
	
	public MemberBooleanFlag getAptcEligibilityIndicator() {
		return aptcEligibilityIndicator;
	}

	public void setAptcEligibilityIndicator(MemberBooleanFlag aptcEligibilityIndicator) {
		this.aptcEligibilityIndicator = aptcEligibilityIndicator;
	}

	public Date getAptcEligibilityStartDate() {
		return aptcEligibilityStartDate;
	}

	public void setAptcEligibilityStartDate(Date aptcEligibilityStartDate) {
		this.aptcEligibilityStartDate = aptcEligibilityStartDate;
	}
	
	public Date getAptcEligibilityEndDate() {
		return aptcEligibilityEndDate;
	}

	public void setAptcEligibilityEndDate(Date aptcEligibilityEndDate) {
		this.aptcEligibilityEndDate = aptcEligibilityEndDate;
	}

	public MemberBooleanFlag getCsrEligibilityIndicator() {
		return csrEligibilityIndicator;
	}

	public void setCsrEligibilityIndicator(MemberBooleanFlag csrEligibilityIndicator) {
		this.csrEligibilityIndicator = csrEligibilityIndicator;
	}

	public Date getCsrEligibilityEndDate() {
		return csrEligibilityEndDate;
	}

	public void setCsrEligibilityEndDate(Date csrEligibilityEndDate) {
		this.csrEligibilityEndDate = csrEligibilityEndDate;
	}

	public Date getCsrEligibilityStartDate() {
		return csrEligibilityStartDate;
	}

	public void setCsrEligibilityStartDate(Date csrEligibilityStartDate) {
		this.csrEligibilityStartDate = csrEligibilityStartDate;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public Long getFfeApplicantId() {
		return ffeApplicantId;
	}

	public void setFfeApplicantId(Long ffeApplicantId) {
		this.ffeApplicantId = ffeApplicantId;
	}

	public MemberBooleanFlag getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MemberBooleanFlag maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Integer getMonthlyAptcAmount() {
		return monthlyAptcAmount;
	}

	public void setMonthlyAptcAmount(Integer monthlyAptcAmount) {
		this.monthlyAptcAmount = monthlyAptcAmount;
	}
	
		
	public Location getMailingLocation() {
		return mailingLocation;
	}

	public void setMailingLocation(Location mailingLocation) {
		this.mailingLocation = mailingLocation;
	}
	

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getPreferredTimeToContact() {
		return preferredTimeToContact;
	}

	public void setPreferredTimeToContact(String preferredTimeToContact) {
		this.preferredTimeToContact = preferredTimeToContact;
	}
	
	

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public MemberExtensionDto getMemberExtension() {
		return memberExtension;
	}

	public void setMemberExtension(MemberExtensionDto memberExtension) {
		this.memberExtension = memberExtension;
	}

	public EappApplication getLaunchScriptData() {
		return launchScriptData;
	}

	public void setLaunchScriptData(EappApplication launchScriptData) {
		this.launchScriptData = launchScriptData;
	}
}

