/**
 * 
 * @author santanu
 * @version 1.0
 * @since Aug 29, 2014 
 *
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.Map;

/**
 * @author Gajulapalli_K
 *
 */
public class AmePlan {
	private int id;
	private int planId;
	private String name;
	private Float premium;
	private int issuerId;
	private String issuerName;
	private String issuerLogo;	
	private Map<String, Map<String, String>> planBenefits;
	private String networkType;
	private String deductibleVal;
	private String deductibleAttrib;
	private String maxBenefitVal;
	private String maxBenefitAttrib;
	private String brochure;
	private String lmitationAndExclusions;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}		
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}		
	public Map<String, Map<String, String>> getPlanBenefits() {
		return planBenefits;
	}
	public void setPlanBenefits(Map<String, Map<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getDeductibleVal() {
		return deductibleVal;
	}
	public void setDeductibleVal(String deductibleVal) {
		this.deductibleVal = deductibleVal;
	}
	public String getDeductibleAttrib() {
		return deductibleAttrib;
	}
	public void setDeductibleAttrib(String deductibleAttrib) {
		this.deductibleAttrib = deductibleAttrib;
	}
	public String getMaxBenefitVal() {
		return maxBenefitVal;
	}
	public void setMaxBenefitVal(String maxBenefitVal) {
		this.maxBenefitVal = maxBenefitVal;
	}
	public String getMaxBenefitAttrib() {
		return maxBenefitAttrib;
	}
	public void setMaxBenefitAttrib(String maxBenefitAttrib) {
		this.maxBenefitAttrib = maxBenefitAttrib;
	}
	public String getBrochure() {
		return brochure;
	}
	public void setBrochure(String brochure) {
		this.brochure = brochure;
	}
	public String getLmitationAndExclusions() {
		return lmitationAndExclusions;
	}
	public void setLmitationAndExclusions(String lmitationAndExclusions) {
		this.lmitationAndExclusions = lmitationAndExclusions;
	}	

}
