
package com.serff.planmanagementexchangeapi.common.model.pm;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.serff.planmanagementexchangeapi.common.model.pm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FileName_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "fileName");
    private final static QName _AttachmentData_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "attachmentData");
    private final static QName _RequiredInsurerSuppliedData_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "requiredInsurerSuppliedData");
    private final static QName _SerffStatus_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "serffStatus");
    private final static QName _ScheduleItemStatus_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "scheduleItemStatus");
    private final static QName _ExchangeSuppliedData_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "exchangeSuppliedData");
    private final static QName _DateLastModified_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "dateLastModified");
    private final static QName _StandardComponentId_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "standardComponentId");
    private final static QName _ExchangeWorkflowStatus_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "exchangeWorkflowStatus");
    private final static QName _ExchangeWorkflowStatusChangeDate_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "exchangeWorkflowStatusChangeDate");
    private final static QName _InsurerSuppliedData_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "insurerSuppliedData");
    private final static QName _ScheduleItemStatusChangeDate_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "scheduleItemStatusChangeDate");
    private final static QName _SerffStatusChangeDate_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/pm", "serffStatusChangeDate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.serff.planmanagementexchangeapi.common.model.pm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SupportingDocument }
     * 
     */
    public SupportingDocument createSupportingDocument() {
        return new SupportingDocument();
    }

    /**
     * Create an instance of {@link ValidationErrors }
     * 
     */
    public ValidationErrors createValidationErrors() {
        return new ValidationErrors();
    }

    /**
     * Create an instance of {@link Attachments }
     * 
     */
    public Attachments createAttachments() {
        return new Attachments();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link SupportingDocuments }
     * 
     */
    public SupportingDocuments createSupportingDocuments() {
        return new SupportingDocuments();
    }

    /**
     * Create an instance of {@link ValidationError }
     * 
     */
    public ValidationError createValidationError() {
        return new ValidationError();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "fileName")
    public JAXBElement<String> createFileName(String value) {
        return new JAXBElement<String>(_FileName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "attachmentData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createAttachmentData(DataHandler value) {
        return new JAXBElement<DataHandler>(_AttachmentData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "requiredInsurerSuppliedData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createRequiredInsurerSuppliedData(DataHandler value) {
        return new JAXBElement<DataHandler>(_RequiredInsurerSuppliedData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SerffStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "serffStatus")
    public JAXBElement<SerffStatus> createSerffStatus(SerffStatus value) {
        return new JAXBElement<SerffStatus>(_SerffStatus_QNAME, SerffStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "scheduleItemStatus")
    public JAXBElement<String> createScheduleItemStatus(String value) {
        return new JAXBElement<String>(_ScheduleItemStatus_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "exchangeSuppliedData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createExchangeSuppliedData(DataHandler value) {
        return new JAXBElement<DataHandler>(_ExchangeSuppliedData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "dateLastModified")
    public JAXBElement<XMLGregorianCalendar> createDateLastModified(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateLastModified_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "standardComponentId")
    public JAXBElement<String> createStandardComponentId(String value) {
        return new JAXBElement<String>(_StandardComponentId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "exchangeWorkflowStatus")
    public JAXBElement<String> createExchangeWorkflowStatus(String value) {
        return new JAXBElement<String>(_ExchangeWorkflowStatus_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "exchangeWorkflowStatusChangeDate")
    public JAXBElement<XMLGregorianCalendar> createExchangeWorkflowStatusChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ExchangeWorkflowStatusChangeDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "insurerSuppliedData")
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createInsurerSuppliedData(DataHandler value) {
        return new JAXBElement<DataHandler>(_InsurerSuppliedData_QNAME, DataHandler.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "scheduleItemStatusChangeDate")
    public JAXBElement<XMLGregorianCalendar> createScheduleItemStatusChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ScheduleItemStatusChangeDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", name = "serffStatusChangeDate")
    public JAXBElement<XMLGregorianCalendar> createSerffStatusChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SerffStatusChangeDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

}
