package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerEnrollmentResponseDTO extends GhixResponseDTO {

	private String hiosIssuerId;
	private String issuerName;
	private String companyLogo;
	private List<EnrollmentFlowDTO> enrollmentFlowDTOList;

	public IssuerEnrollmentResponseDTO() {
		super();
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public List<EnrollmentFlowDTO> getEnrollmentFlowDTOList() {
		return enrollmentFlowDTOList;
	}

	public void setEnrollmentFlowDTO(EnrollmentFlowDTO enrollmentFlowDTO) {

		if (CollectionUtils.isEmpty(this.enrollmentFlowDTOList)) {
			this.enrollmentFlowDTOList = new ArrayList<EnrollmentFlowDTO>();
		}
		this.enrollmentFlowDTOList.add(enrollmentFlowDTO);
	}
}
