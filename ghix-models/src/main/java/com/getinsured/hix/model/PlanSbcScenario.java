package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the PLAN_SBC_SCENARIO database table.
 * @author Vani
 */

@Entity
@Table(name="plan_sbc_scenario")
public class PlanSbcScenario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Plan_sbc_scenario_seq")
	@SequenceGenerator(name = "Plan_sbc_scenario_seq", sequenceName = "plan_sbc_scenario_seq", allocationSize = 1)
	private int id;
	
	@Column(name="baby_coinsurance")
	private Integer babyCoinsurance ;
	
	@Column(name="baby_copayment")
	private Integer babyCopayment ;
	
	@Column(name="baby_deductible")
	private Integer babyDeductible ;
	
	@Column(name="baby_limit")
	private Integer  babyLimit;
	
	@Column(name="diabetes_coinsurance")
	private Integer diabetesCoinsurance ;
	
	@Column(name="diabetes_copay")
	private Integer diabetesCopay ;
	
	@Column(name="diabetes_deductible")
	private Integer diabetesDeductible;
	
	@Column(name="diabetes_limit")
	private Integer diabetesLimit ;
	
	@Column(name="FRACTURE_COINSURANCE")
	private Integer fractureCoinsurance ;
	
	@Column(name="FRACTURE_COPAY")
	private Integer fractureCopay ;
	
	@Column(name="FRACTURE_DEDUCTIBLE")
	private Integer fractureDeductible;
	
	@Column(name="FRACTURE_LIMIT")
	private Integer fractureLimit ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getBabyCoinsurance() {
		return babyCoinsurance;
	}

	public void setBabyCoinsurance(Integer babyCoinsurance) {
		this.babyCoinsurance = babyCoinsurance;
	}

	public Integer getBabyCopayment() {
		return babyCopayment;
	}

	public void setBabyCopayment(Integer babyCopayment) {
		this.babyCopayment = babyCopayment;
	}

	public Integer getBabyDeductible() {
		return babyDeductible;
	}

	public void setBabyDeductible(Integer babyDeductible) {
		this.babyDeductible = babyDeductible;
	}

	public Integer getBabyLimit() {
		return babyLimit;
	}

	public void setBabyLimit(Integer babyLimit) {
		this.babyLimit = babyLimit;
	}

	public Integer getDiabetesCoinsurance() {
		return diabetesCoinsurance;
	}

	public void setDiabetesCoinsurance(Integer diabetesCoinsurance) {
		this.diabetesCoinsurance = diabetesCoinsurance;
	}

	public Integer getDiabetesCopay() {
		return diabetesCopay;
	}

	public void setDiabetesCopay(Integer diabetesCopay) {
		this.diabetesCopay = diabetesCopay;
	}

	public Integer getDiabetesDeductible() {
		return diabetesDeductible;
	}

	public void setDiabetesDeductible(Integer diabetesDeductible) {
		this.diabetesDeductible = diabetesDeductible;
	}

	public Integer getDiabetesLimit() {
		return diabetesLimit;
	}

	public void setDiabetesLimit(Integer diabetesLimit) {
		this.diabetesLimit = diabetesLimit;
	}
	
	public Integer getFractureCoinsurance() {
		return fractureCoinsurance;
	}

	public void setFractureCoinsurance(Integer fractureCoinsurance) {
		this.fractureCoinsurance = fractureCoinsurance;
	}

	public Integer getFractureCopay() {
		return fractureCopay;
	}

	public void setFractureCopay(Integer fractureCopay) {
		this.fractureCopay = fractureCopay;
	}

	public Integer getFractureDeductible() {
		return fractureDeductible;
	}

	public void setFractureDeductible(Integer fractureDeductible) {
		this.fractureDeductible = fractureDeductible;
	}

	public Integer getFractureLimit() {
		return fractureLimit;
	}

	public void setFractureLimit(Integer fractureLimit) {
		this.fractureLimit = fractureLimit;
	}


}
