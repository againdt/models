/**
 * 
 * @author santanu
 * @version 1.0
 * @since July 29, 2014 
 *
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.Map;

public class CrossSellPlan {
	private int id;
	private String name;
	private Float premium;
	private String issuerLogo;	
	private Map<String, Map<String, String>> planCosts;
	private Map<String, Map<String, String>> planBenefits;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}	
	public Map<String, Map<String, String>> getPlanCosts() {
		return planCosts;
	}
	public void setPlanCosts(Map<String, Map<String, String>> planCosts) {
		this.planCosts = planCosts;
	}
	/**
	 * @return the planBenefits
	 */
	public Map<String, Map<String, String>> getPlanBenefits() {
		return planBenefits;
	}
	/**
	 * @param planBenefits the planBenefits to set
	 */
	public void setPlanBenefits(Map<String, Map<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}
}
