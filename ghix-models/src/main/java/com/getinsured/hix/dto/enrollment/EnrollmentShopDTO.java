package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author rajaramesh_g
 * @since 08/30/2013
 *
 */
public class EnrollmentShopDTO implements Serializable{

	private Integer planId;
	private String planName;
	private Float monthlyPremium;
	private Float employerContribution;
	private Float monthlyPayrollDeduction;
	private String houseHoldCaseId;
	private Integer enrollmentId;
	private Integer issuerId;
	private String planType;
	private String planLevel;
	private String enrollmentStatusValue;
	private String enrollmentStatusLabel;
	private Date terminationDate;
	private Date coverageStartDate;
	private Date coverageEndDate;
	private String reasonForTermination;
	private String coveragePolicyNumber;

	private Long employeeApplicationId;
	private Integer employerEnrollmentId;
	private String enrollmentStatus;
	private String enrollmentType;
	private String cmsPlanId;
	
	private List<EnrolleeShopDTO>  enrolleeShopDTOList;

	private Float netPremiumAmt;
	private Float aptcAmt ;
	private BigDecimal stateSubsidyAmt;
	private Date enrollmentCreationTimestamp;
	private boolean isEnrollmentCancelTermForNonPayment;
	/* Jira Id: HIX-79245*/
	private Float essEhbPrmAmt;
	private Float dntlEhbAmt;
	private Float essentialHealthBenefitPrmPercent;
	private Float dntlEssentialHealthBenefitPrmDollarVal;
	private Date enrollmentConfirmationDate;
	
	private String premiumEffDate; //HIX-87871
	private String renewalFlag = "N"; //HIX-109012
	
	public EnrollmentShopDTO()
	{	/*Default*/	}
	
	public EnrollmentShopDTO(Integer planId, String planName, String planLevel,
			Integer enrollmentId, Float monthlyPremium, Float employerContribution, Float monthlyPayrollDeduction, String houseHoldCaseId,
			Long employeeApplicationId, Integer employerEnrollmentId, Float aptcAmt, BigDecimal stateSubsidyAmt, Float netPremiumAmt,
			Date enrollmentCreationTimestamp, String planType, Integer issuerId,
			Date coverageStartDate, Date coverageEndDate, String enrollmentStatusValue, String enrollmentStatusLabel, String cmsPlanId,
			Float essEhbPrmAmt, Float dntlEhbAmt, Float essentialHealthBenefitPrmPercent, Float dntlEssentialHealthBenefitPrmDollarVal, Date enrollmentConfirmationDate,
			String renewalFlag)
	{
		this.planId = planId;
		this.planName = planName;
		this.planLevel = planLevel;
		this.enrollmentId = enrollmentId;
		this.monthlyPremium = monthlyPremium;
		this.employerContribution = employerContribution;
		this.monthlyPayrollDeduction = monthlyPayrollDeduction;
		this.houseHoldCaseId = houseHoldCaseId;
		this.employeeApplicationId = employeeApplicationId;
		this.employerEnrollmentId = employerEnrollmentId;
		this.aptcAmt = aptcAmt;
		this.stateSubsidyAmt = stateSubsidyAmt;
		this.netPremiumAmt = netPremiumAmt;
		this.enrollmentCreationTimestamp = enrollmentCreationTimestamp;
		this.planType = planType;
		this.issuerId = issuerId;
		this.coverageStartDate = coverageStartDate;
		this.coverageEndDate = coverageEndDate;
		this.enrollmentStatusValue = enrollmentStatusValue;
		this.enrollmentStatusLabel = enrollmentStatusLabel;
		this.cmsPlanId = cmsPlanId;
		this.essEhbPrmAmt = essEhbPrmAmt;
		this.dntlEhbAmt = dntlEhbAmt;
		this.essentialHealthBenefitPrmPercent = essentialHealthBenefitPrmPercent;
		this.dntlEssentialHealthBenefitPrmDollarVal = dntlEssentialHealthBenefitPrmDollarVal;
		this.enrollmentConfirmationDate = enrollmentConfirmationDate;
		this.renewalFlag = renewalFlag;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(Float monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	public Float getEmployerContribution() {
		return employerContribution;
	}
	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}
	public Float getMonthlyPayrollDeduction() {
		return monthlyPayrollDeduction;
	}
	public void setMonthlyPayrollDeduction(Float monthlyPayrollDeduction) {
		this.monthlyPayrollDeduction = monthlyPayrollDeduction;
	}
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public List<EnrolleeShopDTO> getEnrolleeShopDTOList() {
		return enrolleeShopDTOList;
	}
	public void setEnrolleeShopDTOList(List<EnrolleeShopDTO> enrolleeShopDTOList) {
		this.enrolleeShopDTOList = enrolleeShopDTOList;
	}
	public String getEnrollmentStatusValue() {
		return enrollmentStatusValue;
	}
	public void setEnrollmentStatusValue(String enrollmentStatusValue) {
		this.enrollmentStatusValue = enrollmentStatusValue;
	}
	public String getEnrollmentStatusLabel() {
		return enrollmentStatusLabel;
	}
	public void setEnrollmentStatusLabel(String enrollmentStatusLabel) {
		this.enrollmentStatusLabel = enrollmentStatusLabel;
	}
	public Date getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	public String getReasonForTermination() {
		return reasonForTermination;
	}
	public void setReasonForTermination(String reasonForTermination) {
		this.reasonForTermination = reasonForTermination;
	}

	public String getCoveragePolicyNumber() {
		return coveragePolicyNumber;
	}
	public void setCoveragePolicyNumber(String coveragePolicyNumber) {
		this.coveragePolicyNumber = coveragePolicyNumber;
	}
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public Date getCoverageEndDate() {
		return coverageEndDate;
	}
	public void setCoverageEndDate(Date coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	public Long getEmployeeApplicationId() {
		return employeeApplicationId;
	}
	public void setEmployeeApplicationId(Long employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}
	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}
	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}
	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}
	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}
	public Float getAptcAmt() {
		return aptcAmt;
	}
	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}
	public Date getEnrollmentCreationTimestamp() {
		return enrollmentCreationTimestamp;
	}
	public void setEnrollmentCreationTimestamp(
			Date enrollmentCreationTimestamp) {
		this.enrollmentCreationTimestamp = enrollmentCreationTimestamp;
	}
	public boolean isEnrollmentCancelTermForNonPayment() {
		return isEnrollmentCancelTermForNonPayment;
	}
	public void setEnrollmentCancelTermForNonPayment(boolean isEnrollmentCancelTermForNonPayment) {
		this.isEnrollmentCancelTermForNonPayment = isEnrollmentCancelTermForNonPayment;
	}
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getCmsPlanId() {
		return cmsPlanId;
	}

	public void setCmsPlanId(String cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}

	/**
	 * @return the essEhbPrmAmt
	 */
	public Float getEssEhbPrmAmt() {
		return essEhbPrmAmt;
	}

	/**
	 * @param essEhbPrmAmt the essEhbPrmAmt to set
	 */
	public void setEssEhbPrmAmt(Float essEhbPrmAmt) {
		this.essEhbPrmAmt = essEhbPrmAmt;
	}

	public Float getDntlEhbAmt() {
		return dntlEhbAmt;
	}

	public void setDntlEhbAmt(Float dntlEhbAmt) {
		this.dntlEhbAmt = dntlEhbAmt;
	}

	/**
	 * @return the essentialHealthBenefitPrmPercent
	 */
	public Float getEssentialHealthBenefitPrmPercent() {
		return essentialHealthBenefitPrmPercent;
	}

	/**
	 * @param essentialHealthBenefitPrmPercent the essentialHealthBenefitPrmPercent to set
	 */
	public void setEssentialHealthBenefitPrmPercent(Float essentialHealthBenefitPrmPercent) {
		this.essentialHealthBenefitPrmPercent = essentialHealthBenefitPrmPercent;
	}

	public Float getDntlEssentialHealthBenefitPrmDollarVal() {
		return dntlEssentialHealthBenefitPrmDollarVal;
	}

	public void setDntlEssentialHealthBenefitPrmDollarVal(
			Float dntlEssentialHealthBenefitPrmDollarVal) {
		this.dntlEssentialHealthBenefitPrmDollarVal = dntlEssentialHealthBenefitPrmDollarVal;
	}

	public Date getEnrollmentConfirmationDate() {
		return enrollmentConfirmationDate;
	}

	public void setEnrollmentConfirmationDate(Date enrollmentConfirmationDate) {
		this.enrollmentConfirmationDate = enrollmentConfirmationDate;
	}

	public String getPremiumEffDate() {
		return premiumEffDate;
	}

	public void setPremiumEffDate(String premiumEffDate) {
		this.premiumEffDate = premiumEffDate;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}
	
	public boolean isAptcEligible() {
		return null != this.aptcAmt; 
	}
	
	public boolean isCsrEligible() {
		return null != this.cmsPlanId && !this.cmsPlanId.trim().endsWith("01");
	}

	public BigDecimal getStateSubsidyAmt() {
		return stateSubsidyAmt;
	}

	public void setStateSubsidyAmt(BigDecimal stateSubsidyAmt) {
		this.stateSubsidyAmt = stateSubsidyAmt;
	}
}