package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_1095_VALIDATION_REPORT")
public class Enrollment1095ValidationReport implements Serializable{
	public enum ProcessedFlag{Y, N};
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_1095_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_1095_SEQ", sequenceName = "ENROLLMENT_1095_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="ENROLLMENT_1095_ID")
	private Enrollment1095 enrollment1095;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;
	
	@Column(name = "ERROR_TYPE")
	private String errorType;
	
	@Column(name = "PROCESSED", length=1)
	@Enumerated(EnumType.STRING)
	private ProcessedFlag processed;
	
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Enrollment1095 getEnrollment1095() {
		return enrollment1095;
	}

	public void setEnrollment1095(Enrollment1095 enrollment1095) {
		this.enrollment1095 = enrollment1095;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public ProcessedFlag getProcessed() {
		return processed;
	}

	public void setProcessed(ProcessedFlag processed) {
		this.processed = processed;
	}

}
