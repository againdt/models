package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

public class LowestPremiumResponseDTO extends GHIXResponse implements Serializable{

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private float onExchangePremium;
	private float offExchangePremium;
	
	
	/**
	 * @return the onExchangePremium
	 */
	public float getOnExchangePremium() {
		return onExchangePremium;
	}
	/**
	 * @param onExchangePremium the onExchangePremium to set
	 */
	public void setOnExchangePremium(float onExchangePremium) {
		this.onExchangePremium = onExchangePremium;
	}
	/**
	 * @return the offExchangePremium
	 */
	public float getOffExchangePremium() {
		return offExchangePremium;
	}
	/**
	 * @param offExchangePremium the offExchangePremium to set
	 */
	public void setOffExchangePremium(float offExchangePremium) {
		this.offExchangePremium = offExchangePremium;
	}
	
	
}
