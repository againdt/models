package com.getinsured.hix.model.plandisplay;

import java.util.List;

public class PdNetworkCompareRequest {
	private String householdId;
	private List<String> hiosIdList;
	private String zipCode;
	private Integer radius;
	
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public List<String> getHiosIdList() {
		return hiosIdList;
	}
	public void setHiosIdList(List<String> hiosIdList) {
		this.hiosIdList = hiosIdList;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
}
