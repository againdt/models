package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * The persistent class for the ISSUER_PAYMENT_DETAIL database table.
 * @author kuldeep
 */

@Audited
@Entity
@Table(name = "issuer_payment_detail")
public class IssuerPaymentDetail implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public enum PaymentProcessingStatus
	{
		ACCEPT, REJECT, ERROR, NOPAYMENTMETHOD;
	}
	
	//Flag P stands for In_Process
	public enum EDIGeneratedFlag
	{
		Y, N, P;
	}
	
	public enum ExchangePaymentType
	{ PREM, PREMADJ;}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issuer_payment_detail_seq")
	@SequenceGenerator(name = "issuer_payment_detail_seq", sequenceName = "issuer_payment_detail_seq", allocationSize = 1)
	private int id;

	@Column(name = "transaction_id")
	private String transactionId;

/*	@Column(name = "response")
	private String response;*/

	@Column(name = "amount_paid")
	private BigDecimal amount;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "payment_date", nullable = false)
	private Date paymentDate;
	
	@OneToOne
	@JoinColumn(name = "issuer_payment_id")
	private IssuerPayments issuerPayment;
	
	@OneToOne
	@JoinColumn(name = "payment_type_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private PaymentMethods paymentMethods;
	
	@Column(name = "is_edi_generated")
	@Enumerated(EnumType.STRING)
	private EDIGeneratedFlag isEdiGenerated;
	
	/*@Column(name = "pymt_processing_resp")
	@Enumerated(EnumType.STRING)
	private PaymentProcessingStatus paymentProcessingStatus;*/
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Column(name = "merchant_ref_code")
	private String merchantRefCode;
	
	/*@Column(name="reconciliation_id")
	private String reconciliationId;
	
	@Column(name="processor_transaction_id")
	private String processorTransactionID;*/
	
	@Column(name="exchange_payment_type")
	@Enumerated(EnumType.STRING)
	private ExchangePaymentType exchangePaymentType;
	
	@ManyToOne
	@JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/*public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}*/
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getPaymentDate()
	{
		return paymentDate;
	}
	
	public void setPaymentDate(Date paymentDate)
	{
		this.paymentDate = paymentDate;
	}
	
	public IssuerPayments getIssuerPayment() {
		return issuerPayment;
	}

	public void setIssuerPayment(IssuerPayments issuerPayment) {
		this.issuerPayment = issuerPayment;
	}

	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	
	public EDIGeneratedFlag getIsEdiGenerated() {
		return isEdiGenerated;
	}

	public void setIsEdiGenerated(EDIGeneratedFlag isEdiGenerated) {
		this.isEdiGenerated = isEdiGenerated;
	}

	/*public PaymentProcessingStatus getPaymentProcessingStatus() {
		return paymentProcessingStatus;
	}

	public void setPaymentProcessingStatus(
			PaymentProcessingStatus paymentProcessingStatus) {
		this.paymentProcessingStatus = paymentProcessingStatus;
	}*/
	

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setPaymentDate(new TSDate());
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	//Kept this update flow since when we re run rejected payment this date need to be updated
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
	//	this.setPaymentDate(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	

	/**
	 * @return the merchantRefCode
	 */
	public String getMerchantRefCode() {
		return merchantRefCode;
	}

	/**
	 * @param merchantRefCode the merchantRefCode to set
	 */
	public void setMerchantRefCode(String merchantRefCode) {
		this.merchantRefCode = merchantRefCode;
	}

	/**
	 * @return the reconciliationId
	 */
	/*public String getReconciliationId() {
		return reconciliationId;
	}*/

	/**
	 * @param reconciliationId the reconciliationId to set
	 */
	/*public void setReconciliationId(String reconciliationId) {
		this.reconciliationId = reconciliationId;
	}*/
	
	/**
	 * @return the processorTransactionID
	 */
	/*public String getProcessorTransactionID() {
		return processorTransactionID;
	}*/

	/**
	 * @param processorTransactionID the processorTransactionID to set
	 */
	/*public void setProcessorTransactionID(String processorTransactionID) {
		this.processorTransactionID = processorTransactionID;
	}*/

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the exchangePaymentType
	 */
	public ExchangePaymentType getExchangePaymentType() {
		return exchangePaymentType;
	}

	/**
	 * @param exchangePaymentType the exchangePaymentType to set
	 */
	public void setExchangePaymentType(ExchangePaymentType exchangePaymentType) {
		this.exchangePaymentType = exchangePaymentType;
	}


	/**
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Override
	public String toString() {
		//return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
		//HIX-25009
		return "IssuerPaymentDetail [id=" + id + ", transactionId="+ transactionId + "]";
	}

}
