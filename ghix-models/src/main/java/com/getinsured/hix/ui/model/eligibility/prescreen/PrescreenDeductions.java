package com.getinsured.hix.ui.model.eligibility.prescreen;

/**
 * This class encapsulates the deductions information for a household for that
 * tax year as input in the Deductions page. The default values of the types of
 * deductions are 0.0.
 * 
 * @author Sunil Desu
 * @since April 16 2013
 */
public class PrescreenDeductions {
	private double alimonyPaid;
	private double studentLoans;

	public double getAlimonyPaid() {
		return alimonyPaid;
	}

	public void setAlimonyPaid(double alimonyPaid) {
		this.alimonyPaid = alimonyPaid;
	}

	public double getStudentLoans() {
		return studentLoans;
	}

	public void setStudentLoans(double studentLoans) {
		this.studentLoans = studentLoans;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrescreenDeductions [alimonyPaid=");
		builder.append(alimonyPaid);
		builder.append(", studentLoans=");
		builder.append(studentLoans);
		builder.append("]");
		return builder.toString();
	}

}
