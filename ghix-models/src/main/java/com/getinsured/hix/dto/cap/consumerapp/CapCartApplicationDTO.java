package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.estimator.mini.Member;

public class CapCartApplicationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8344606376608714367L;
	
	private String ssapId;
	private String enryptedSsapId;
	private String state;
	private String zipCode;
	private String premiumGross;
	private String aptc;
	private String premiumNet;
	private String servicedBy;
	private String stage;
	private String creationDate;
	private List<Member> applicantDTOList;
	
	private PlanInfoDTO planInfoDTO;
	
	public String getSsapId() {
		return ssapId;
	}
	public void setSsapId(String ssapId) {
		this.ssapId = ssapId;
	}
	public String getEnryptedSsapId() {
		return enryptedSsapId;
	}
	public void setEnryptedSsapId(String enryptedSsapId) {
		this.enryptedSsapId = enryptedSsapId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getPremiumGross() {
		return premiumGross;
	}
	public void setPremiumGross(String premiumGross) {
		this.premiumGross = premiumGross;
	}
	public String getAptc() {
		return aptc;
	}
	public void setAptc(String aptc) {
		this.aptc = aptc;
	}
	public String getPremiumNet() {
		return premiumNet;
	}
	public void setPremiumNet(String premiumNet) {
		this.premiumNet = premiumNet;
	}
	public String getServicedBy() {
		return servicedBy;
	}
	public void setServicedBy(String servicedBy) {
		this.servicedBy = servicedBy;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public List<Member> getApplicantDTOList() {
		return applicantDTOList;
	}
	public void setApplicantDTOList(List<Member> applicantDTOList) {
		this.applicantDTOList = applicantDTOList;
	}
	public PlanInfoDTO getPlanInfoDTO() {
		return planInfoDTO;
	}
	public void setPlanInfoDTO(PlanInfoDTO planInfoDTO) {
		this.planInfoDTO = planInfoDTO;
	}
	
}
