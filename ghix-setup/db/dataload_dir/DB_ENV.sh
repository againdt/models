#!/bin/bash
#
export TMP=/tmp
export TMPDIR=$TMP
# ORACLE_HOSTNAME=ghixdb; export ORACLE_HOSTNAME
export ORACLE_BASE=/apps/oracle 
export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/db_1
export ORACLE_SID=devdb
export ORACLE_TERM=xterm
export PATH=/usr/sbin:$PATH
export PATH=$ORACLE_HOME/bin:$PATH

export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
#
export EZCONNECT=//ghixpilot.com:1521/devdb
#


