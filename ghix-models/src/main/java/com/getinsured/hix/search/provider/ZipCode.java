package com.getinsured.hix.search.provider;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class ZipCode implements Serializable,JSONAware {
	private static final long serialVersionUID = 1L;

	private int id;
	private String zip;
	private String city;
	private String cityType;
	private String county;
	private String countyFips;
	private String stateName;
	private String state;
	private String stateFips;
	private String msa;
	private String areaCode;
	private String timeZone;
	private int gmtOffset;
	private String dst;
	private double lat;
	private double lon;
	private String countyCode;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityType() {
		return cityType;
	}

	public void setCityType(String cityType) {
		this.cityType = cityType;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountyFips() {
		return countyFips;
	}

	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateFips() {
		return stateFips;
	}

	public void setStateFips(String stateFips) {
		this.stateFips = stateFips;
	}

	public String getMsa() {
		return msa;
	}

	public void setMsa(String msa) {
		this.msa = msa;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public int getGmtOffset() {
		return gmtOffset;
	}

	public void setGmtOffset(int gmtOffset) {
		this.gmtOffset = gmtOffset;
	}

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getCountyCode() {
		this.setCountyCode((this.getStateFips()==null?"":this.getStateFips())+(this.getCountyFips()==null?"":this.getCountyFips()));
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	//since there are some records with duplication zipcode, checking id for equals and hashcode
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj){
			return true;
		}
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		// object must be Test at this point
		ZipCode zipCode = (ZipCode)obj;
		return id == zipCode.id;
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 31 * hash + id;

		return hash;
	}
	
	/*
	 * Do not change the String values used here, it should match exactly with Solr Schema
	 */
	public boolean setValue(String fieldName, Object object){
		if(fieldName.equals("ZipCode")){
			this.zip = (String)object;
		}else if(fieldName.equals("City")){
			this.city = (String)object;
		}else if(fieldName.equals("State")){
			this.state = (String)object;
		}else if(fieldName.equals("County")){
			this.county = (String)object;
		}else if(fieldName.equals("AreaCode")){
			this.areaCode = (String)object; 
		}else if(fieldName.equals("CityType")){
			this.cityType = (String)object;
		}else if(fieldName.equals("Zip_location")){
			String[] coordinates = ((String)object).split(",");
			if(coordinates.length != 2){
				return false;
			}
			this.lat = Double.valueOf(coordinates[0]);
			this.lon = Double.valueOf(coordinates[1]);
		}else if(fieldName.equals("StateFIPS")){
			this.stateFips = (String)object;
		}else if(fieldName.equals("CountyFIPS")){
			this.countyFips = (String)object;
		}else{
			return false;
		}
		return true;
	}

	public void setFieldValues(Map<String, Object> fieldMap) {
		Set<Map.Entry<String, Object>> s = fieldMap.entrySet();
		Iterator<Map.Entry<String, Object>> cursor = s.iterator();
		Map.Entry<String, Object> me = null;
		while(cursor.hasNext()){
			me = cursor.next();
			this.setValue(me.getKey(), (String)me.getValue());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.zip != null){
			obj.put("zip", this.zip);
		}
		if(this.city != null){
			obj.put("city", this.city);
		}
		if(this.cityType!= null){
			obj.put("cityType", this.cityType);
		}
		if(this.county!= null){
			obj.put("county", this.county);
		}
		if(this.countyFips!= null){
			obj.put("countyFips", this.countyFips);
		}
		if(this.stateName!= null){
			obj.put("stateName", this.stateName);
		}
		if(this.state!= null){
			obj.put("state", this.state);
		}
		if(this.stateFips != null){
			obj.put("stateFips", this.stateFips);
		}
		if(this.msa != null){
			obj.put("msa", this.msa);
		}
		if(this.areaCode != null){
			obj.put("areaCode", this.areaCode);
		}
		if(this.timeZone != null){
			obj.put("timeZone", this.timeZone);
		}
		obj.put("gmtOffset", this.gmtOffset);
		if(this.dst!= null){
			obj.put("dst", this.dst);
		}
		obj.put("lat", this.lat);
		obj.put("lon", this.lon);
		if(this.stateFips!= null && this.countyFips != null){
			obj.put("countyCode", this.stateFips+this.countyFips);
		}
		return obj.toJSONString();
	}

}

