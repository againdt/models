package com.getinsured.hix.dto.finance;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.PaymentMethods;


public class PaymentMethodResponse extends GHIXResponse implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> paymentMethodMap = null;
	
	private List<PaymentMethods> paymentMethodList = null;
	
	private PaymentMethods PaymentMethods = null;
	
	private List<Map<Integer, Object>> paymentMethodResponseList = null;
	
	/**
	 * A page is a sublist of a list of objects.
	 */
	private Page<PaymentMethods> pagePaymentMethods = null;

	private String responseString = null;
	private Map<String, Object> responsedMap = null;
	
	public Page<PaymentMethods> getPagePaymentMethods() {
		return pagePaymentMethods;
	}

	public void setPagePaymentMethods(Page<PaymentMethods> pagePaymentMethods) {
		this.pagePaymentMethods = pagePaymentMethods;
	}

	/**
	 * @return the paymentMethodMap
	 */
	public Map<String, Object> getPaymentMethodMap() {
		return paymentMethodMap;
	}

	/**
	 * @param paymentMethodMap the paymentMethodMap to set
	 */
	public void setPaymentMethodMap(Map<String, Object> paymentMethodMap) {
		this.paymentMethodMap = paymentMethodMap;
	}

	/**
	 * @return the paymentMethodList
	 */
	public List<PaymentMethods> getPaymentMethodList() {
		return paymentMethodList;
	}

	/**
	 * @param paymentMethodList the paymentMethodList to set
	 */
	public void setPaymentMethodList(List<PaymentMethods> paymentMethodList) {
		this.paymentMethodList = paymentMethodList;
	}

	/**
	 * @return the paymentMethods
	 */
	public PaymentMethods getPaymentMethods() {
		return PaymentMethods;
	}

	/**
	 * @param paymentMethods the paymentMethods to set
	 */
	public void setPaymentMethods(PaymentMethods paymentMethods) {
		PaymentMethods = paymentMethods;
	}

	public List<Map<Integer, Object>> getPaymentMethodResponseList() {
		return paymentMethodResponseList;
	}

	public void setPaymentMethodResponseList(
			List<Map<Integer, Object>> paymentMethodResponseList) {
		this.paymentMethodResponseList = paymentMethodResponseList;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public Map<String, Object> getResponsedMap() {
		return responsedMap;
	}

	public void setResponsedMap(Map<String, Object> responsedMap) {
		this.responsedMap = responsedMap;
	}
}
