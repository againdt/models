--liquibase formatted sql

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Add_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Appeals')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Complaints')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Contribution_Selection')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Edit_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Employer_Plan_Selection_wizard')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Manage_Employee_List')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Participation_Report')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Payment_Report')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Renew_Coverage')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Review_Employee_List')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Search_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Account_Information')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Authorized_Representatives')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Available_Plans')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Dependent_Details')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Eligibility_Results')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Employer_Dashboard')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Invoices')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Payments')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Plan_Selections')));



INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Add_Employees')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Add_Payment_Method')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Add/Remove_Authorized_Employer_Representatives')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Cancel_Open_Enrollment')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Delete_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Edit_Account_Information')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Edit_Authorized_Representatives')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Enroll_on_Behalf_of_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Make_Payment')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Manage_Payment_Methods')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Terminate_Coverage')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Terminate_Employee')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Terminate_Employer_Account')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Waive_Coverage')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Waive_Employee_Coverage')));

