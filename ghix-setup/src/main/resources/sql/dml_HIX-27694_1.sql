INSERT
INTO NOTICE_TYPES
  (
    ID,
    NAME,
    LANGUAGE,
    USER_TYPE,
    TYPE,
    EXTERNAL_ID,
    TEMPLATE_LOCATION,
    EMAIL_SUBJECT,
    EMAIL_FROM,
    EMAIL_CLASS,
    EFFECTIVE_DATE,
    TERMINATION_DATE,
    CREATION_TIMESTAMP,
    LAST_UPDATE_TIMESTAMP
  )
  VALUES
  (
    notice_types_seq.NEXTVAL,
    'Employee Enrollment Status Confirmation Email Notification Template',
    'US_EN',
    'ENROLLMENT',
    'PDF',
    NULL,
    'notificationTemplate/EnrollmentStatusConfirmNotification.html',
    '',
    NULL,
    'EnrollmentStatusConfirmNotification',
    to_date('04-MAR-13','DD-MON-RR'),
    to_date('31-JUL-20','DD-MON-RR'),
    to_timestamp('05-SEP-13 05.12.37.453575000 AM','DD-MON-RR HH.MI.SS.FF AM'),
    to_timestamp('05-SEP-13 05.12.37.453575000 AM','DD-MON-RR HH.MI.SS.FF AM')
  );