package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;

public class PdHouseholdDataResponse implements Serializable{	
	private static final long serialVersionUID = 1L;
	
	private String householdData;
	private PdPreferencesDTO pdPreferencesDTO;
	
	public String getHouseholdData() {
		return householdData;
	}
	public void setHouseholdData(String householdData) {
		this.householdData = householdData;
	}
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}

	
}
