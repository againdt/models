package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Model class is used to keep tracking records of External web services (e.g. Quotit service).
 *
 * @author sharma_va
 */
@Entity
@Table(name = "EXT_WS_CALL_LOG")
public class ExtWSCallLogs implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum STATUS {
		IN_PROGRESS, COMPLETED, FAILED;
	}

	public enum REQUESTED_BY {
		BATCH, UI;
	}

	public enum BATCH_STATE {
		BEGIN, BETWEEN, END;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXT_WS_CALL_LOG_SEQ")
	@SequenceGenerator(name = "EXT_WS_CALL_LOG_SEQ", sequenceName = "EXT_WS_CALL_LOG_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "PARENT_REQ_ID")
	private Integer parentReqId;

	@Column(name = "EXTERNAL_REQ_ID")
	private Integer externalReqId;

	@Column(name = "EXTERNAL_POD_ID")
	private String externalProdId;

	@Column(name = "METHOD_NAME")
	private String methodName;

	@Column(name = "END_POINT")
	private String endPoint;

	@Column(name = "SYSTEM_IP")
	private String systemIp;

	@Column(name = "START_TIME")
	private Date startTime;

	@Column(name = "END_TIME")
	private Date endTime;

	@Column(name = "EXTERNAL_EXEC_TIME")
	private Integer externalExecTime;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "REQUEST_PARAMS")
	private String requestParams;

	@Column(name = "REQUEST")
	private String request;

	@Column(name = "RESPONSE")
	private String response;

	@Column(name = "EXCEPTION_MESSAGE")
	private String exceptionMessage;

	@Column(name = "REMARKS")
	private String remarks;

	@Column(name = "EXECUTED_BY")
	private Integer executedBy;
	
	@Column(name = "REQUESTED_BY")
	private String requestedBy;
	
	@Column(name = "BATCH_STATE")
	private String batchState;
	
	public ExtWSCallLogs() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getParentReqId() {
		return parentReqId;
	}

	public void setParentReqId(Integer parentReqId) {
		this.parentReqId = parentReqId;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getSystemIp() {
		return systemIp;
	}

	public void setSystemIp(String systemIp) {
		this.systemIp = systemIp;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(String requestParams) {
		this.requestParams = requestParams;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getExecutedBy() {
		return executedBy;
	}

	public void setExecutedBy(Integer executedBy) {
		this.executedBy = executedBy;
	}

	public Integer getExternalReqId() {
		return externalReqId;
	}

	public void setExternalReqId(Integer externalReqId) {
		this.externalReqId = externalReqId;
	}

	public String getExternalProdId() {
		return externalProdId;
	}

	public void setExternalProdId(String externalProdId) {
		this.externalProdId = externalProdId;
	}

	public Integer getExternalExecTime() {
		return externalExecTime;
	}

	public void setExternalExecTime(Integer externalExecTime) {
		this.externalExecTime = externalExecTime;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getBatchState() {
		return batchState;
	}

	public void setBatchState(String batchState) {
		this.batchState = batchState;
	}
}
