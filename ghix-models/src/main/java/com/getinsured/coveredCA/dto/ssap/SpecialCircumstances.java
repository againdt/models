
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class SpecialCircumstances {
	
	@Expose
	private Boolean disabilityIndicator=false;
	@Expose
	private Boolean requestHelpPayingMedicalBillsLast3MonthsIndicator= false;
	@Expose
	private Boolean pregnantIndicator=false;
	@Expose
    private Integer numberBabiesExpectedInPregnancy;
	@Expose
	private String expectedDeliveryDate;
	@Expose
	private Boolean tribalManualVerificationIndicator=false;
	 
	 
	 
    
	 
  
    public Integer getNumberBabiesExpectedInPregnancy() {
        return numberBabiesExpectedInPregnancy;
    }

    public void setNumberBabiesExpectedInPregnancy(Integer numberBabiesExpectedInPregnancy) {
        this.numberBabiesExpectedInPregnancy = numberBabiesExpectedInPregnancy;
    }

    public Boolean getPregnantIndicator() {
        return pregnantIndicator;
    }

    public void setPregnantIndicator(Boolean pregnantIndicator) {
        this.pregnantIndicator = pregnantIndicator;
    }

    
    public Boolean getTribalManualVerificationIndicator() {
        return tribalManualVerificationIndicator;
    }

    public void setTribalManualVerificationIndicator(Boolean tribalManualVerificationIndicator) {
        this.tribalManualVerificationIndicator = tribalManualVerificationIndicator;
    }

    
    public Boolean getDisabilityIndicator() {
		return disabilityIndicator;
	}

	public void setDisabilityIndicator(Boolean disabilityIndicator) {
		this.disabilityIndicator = disabilityIndicator;
	}

	public Boolean getRequestHelpPayingMedicalBillsLast3MonthsIndicator() {
		return requestHelpPayingMedicalBillsLast3MonthsIndicator;
	}

	public void setRequestHelpPayingMedicalBillsLast3MonthsIndicator(
			Boolean requestHelpPayingMedicalBillsLast3MonthsIndicator) {
		this.requestHelpPayingMedicalBillsLast3MonthsIndicator = requestHelpPayingMedicalBillsLast3MonthsIndicator;
	}

	public String getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(String expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	
	


}
