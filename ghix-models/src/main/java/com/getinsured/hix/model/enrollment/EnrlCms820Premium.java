package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="ENRL_CMS_820_PREMIUM")
public class EnrlCms820Premium  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_820_PREMIUM_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_820_PREMIUM_SEQ", sequenceName = "ENRL_CMS_820_PREMIUM_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="ENROLLMENT_ID")
	private Long enrollmentId;
	
	@Column(name="MONTH")
	private Integer month;
	
	@Column(name="APTC_AMT")
	private BigDecimal aptcAmt;

	@Column(name="CSR_AMT")
	private BigDecimal csrAmt;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public BigDecimal getAptcAmt() {
		return aptcAmt;
	}

	public void setAptcAmt(BigDecimal aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public BigDecimal getCsrAmt() {
		return csrAmt;
	}

	public void setCsrAmt(BigDecimal csrAmt) {
		this.csrAmt = csrAmt;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
	}

}
