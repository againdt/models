package com.getinsured.hix.dto.shop;

import java.util.List;

public class GetEmployeesDTO {

	private List<Integer> employeeIds;

	public List<Integer> getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(List<Integer> employeeIds) {
		this.employeeIds = employeeIds;
	}
}
