CREATE OR REPLACE 
VIEW ACCOUNT_PAYABLE(ID, ISSUER_ID, ISSUER_NAME, INVOICE_NUMBER, STATEMENT_DATE, TOTAL_AMOUNT_DUE, PAID_AMOUNT, PAID_STATUS, BALANCE_AMOUNT) 
AS
SELECT rownum, i.id, i.name, inv.invoice_number, trunc(inv.statement_date),
inv.total_amount_due, inv.total_payment_paid, inv.paid_status, cast((inv.total_amount_due - inv.total_payment_paid) as float(126)) 
FROM issuer_invoices inv, issuers i 
WHERE i.id = inv.issuer_id 
AND inv.is_active = 'Y' 
AND inv.paid_status in ('DUE','PARTIALLY_PAID') 
WITH READ ONLY;


CREATE OR REPLACE 
VIEW ACCOUNT_RECEIVEABLE(ID, EMPLOYER_ID, EMPLOYER_NAME, INVOICE_NUMBER, STATEMENT_DATE, TOTAL_AMOUNT_DUE, PAID_AMOUNT, PENDING_AMOUNT, PAID_STATUS) 
AS 
SELECT rownum, e.id, e.name, inv.invoice_number, trunc(inv.statement_date), inv.total_amount_due, inv.total_payment_received,
cast((inv.total_amount_due - inv.total_payment_received) as float(126)), inv.paid_status 
FROM employer_invoices inv , employers e 
WHERE e.id = inv.employer_id  
AND inv.paid_status in ('DUE','PARTIALLY_PAID') 
AND inv.is_ACTIVE = 'Y' 
WITH READ ONLY;


CREATE OR REPLACE
VIEW QHP_ACCOUNT_RECEIVEABLE(ROW_NUMBER, EMPLOYER_ID, EMPLOYER_NAME, PLAN_TYPE, NET_AMOUNT, PAYMENT_RECEIVED, PENDING_AMOUNT) 
AS 
SELECT ROW_NUMBER() OVER (ORDER BY sum(inv.payment_received) DESC),inv.employer_id, e.name, inv.plan_type, cast(sum(inv.net_amount) as float(126)), cast(sum(inv.payment_received) as float(126)),
cast((sum(inv.net_amount) - sum(inv.payment_received))as float(126)) 
FROM Employers e, employer_invoices_lineitems inv, employer_invoices einv 
WHERE e.id = inv.employer_id 
AND einv.id = inv.invoice_id 
AND einv.is_active = 'Y' 
AND einv.paid_status in('DUE', 'PARTIALLY_PAID') 
GROUP BY inv.employer_id, inv.plan_type, e.name;
