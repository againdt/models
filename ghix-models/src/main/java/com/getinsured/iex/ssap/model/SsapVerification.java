package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SSAP_VERIFICATIONS database table.
 *
 */
@Entity
@Table(name="SSAP_VERIFICATIONS")
@NamedQuery(name="SsapVerification.findAll", query="SELECT s FROM SsapVerification s")
public class SsapVerification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_VERIFICATIONS_ID_GENERATOR", sequenceName="SSAP_VERIFICATIONS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_VERIFICATIONS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@Column(name="EFFECTIVE_END_DATE")
	private Timestamp effectiveEndDate;

	@Column(name="EFFECTIVE_START_DATE")
	private Timestamp effectiveStartDate;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@Column(length=100)
	private String source;

	@Column(name="VERFICATION_RESULTS", length=200)
	private String verficationResults;

	@Column(name="VERIFICATION_STATUS", length=50)
	private String verificationStatus;

	@Column(name="VERIFICATION_TYPE", length=100)
	private String verificationType;

	//bi-directional many-to-one association to SsapApplicant
	//@ManyToOne(fetch=FetchType.LAZY)
	@ManyToOne
	@JoinColumn(name="SSAP_APPLICANT_ID", nullable=false)
	private SsapApplicant ssapApplicant;

	//bi-directional many-to-one association to SsapVerificationGiwspayload
	@OneToMany(cascade = CascadeType.ALL, mappedBy="ssapVerification")
	private List<SsapVerificationGiwspayload> ssapVerificationGiwspayloads;

	public SsapVerification() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEffectiveEndDate() {
		return this.effectiveEndDate;
	}

	public void setEffectiveEndDate(Timestamp effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Timestamp getEffectiveStartDate() {
		return this.effectiveStartDate;
	}

	public void setEffectiveStartDate(Timestamp effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getVerficationResults() {
		return this.verficationResults;
	}

	public void setVerficationResults(String verficationResults) {
		this.verficationResults = verficationResults;
	}

	public String getVerificationStatus() {
		return this.verificationStatus;
	}

	public void setVerificationStatus(String verificationStatus) {
		this.verificationStatus = verificationStatus;
	}

	public String getVerificationType() {
		return this.verificationType;
	}

	public void setVerificationType(String verificationType) {
		this.verificationType = verificationType;
	}

	public SsapApplicant getSsapApplicant() {
		return this.ssapApplicant;
	}

	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}

	public List<SsapVerificationGiwspayload> getSsapVerificationGiwspayloads() {
		return this.ssapVerificationGiwspayloads;
	}

	public void setSsapVerificationGiwspayloads(List<SsapVerificationGiwspayload> ssapVerificationGiwspayloads) {
		this.ssapVerificationGiwspayloads = ssapVerificationGiwspayloads;
	}

	public SsapVerificationGiwspayload addSsapVerificationGiwspayload(SsapVerificationGiwspayload ssapVerificationGiwspayload) {
		getSsapVerificationGiwspayloads().add(ssapVerificationGiwspayload);
		ssapVerificationGiwspayload.setSsapVerification(this);

		return ssapVerificationGiwspayload;
	}

	public SsapVerificationGiwspayload removeSsapVerificationGiwspayload(SsapVerificationGiwspayload ssapVerificationGiwspayload) {
		getSsapVerificationGiwspayloads().remove(ssapVerificationGiwspayload);
		ssapVerificationGiwspayload.setSsapVerification(null);

		return ssapVerificationGiwspayload;
	}

}