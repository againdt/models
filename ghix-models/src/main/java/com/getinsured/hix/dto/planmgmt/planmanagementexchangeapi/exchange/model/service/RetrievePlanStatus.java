
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetrievePlanStatus complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="RetrievePlanStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}planId"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "planId"
})
@XmlRootElement(name="retrievePlanStatus")
public class RetrievePlanStatus {

    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    protected String planId;

    /**
     * Gets the value of the planId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPlanId(String value) {
        this.planId = value;
    }
    @Override
	public String toString() {
		return "RetrievePlanStatus [planId=" + planId + "]";
	}
}
