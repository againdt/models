package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class Incomes{
  @JsonProperty("amount")
  
  private Integer amount;
  @JsonProperty("phone")
  
  private String phone;
  @JsonProperty("name")
  
  private String name;
  @JsonProperty("type")
  
  private String type;
  public void setAmount(Integer amount){
   this.amount=amount;
  }
  public Integer getAmount(){
   return amount;
  }
  public void setPhone(String phone){
   this.phone=phone;
  }
  public String getPhone(){
   return phone;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setType(String type){
   this.type=type;
  }
  public String getType(){
   return type;
  }
}