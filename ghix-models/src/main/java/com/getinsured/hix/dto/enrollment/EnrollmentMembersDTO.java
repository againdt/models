package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrollmentMembersDTO  implements Serializable{

	private String memberId;
	private String externalMemberId;
	private String enrollmentId;
	
	public EnrollmentMembersDTO() {
			}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getExternalMemberId() {
		return externalMemberId;
	}
	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public EnrollmentMembersDTO(String memberId, String externalMemberId, String enrollmentId) {
		super();
		this.memberId = memberId;
		this.externalMemberId = externalMemberId;
		this.enrollmentId = enrollmentId;
	}
}
