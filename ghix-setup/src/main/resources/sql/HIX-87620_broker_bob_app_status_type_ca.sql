INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE, DESCRIPTION)
			VALUES (LOOKUPTYPE_SEQ.nextval,'APPLICATION_STATUS_TYPE','string','Application Status Type');

			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'WD', 'WITHDRAWN', NULL, 'N', 'Withdrawn', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'CM', 'COMPLETED', NULL, 'N', 'Completed', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'SU', 'SUBMITTED', NULL, 'N', 'Submitted', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'IP', 'IN PROGRESS', NULL, 'N', 'In Progress', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'CL', 'CLOSED - NOT COMPLETED', NULL, 'N', 'Closed - Not Completed', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'RN', 'RENEWAL', NULL, 'N', 'Renewal', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'RW', 'RAC-WITHDRAWN', NULL, 'N', 'RAC - Withdrawn', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'APPLICATION_STATUS_TYPE')
			,'TR', 'TERMINATED', NULL, 'N', 'Terminated', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));