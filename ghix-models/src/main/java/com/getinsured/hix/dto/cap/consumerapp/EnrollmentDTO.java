package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.Date;

public class EnrollmentDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7640745681315813269L;
	private int id;
	private int enrollmentStatusLookup;
	private String enrollmentStatusLookupCode;
	private int insuranceTypeLookup;
	private String status;
	private String insuranceType; // Health/Dental/ShortTerm

	private float grossPremiumAmount;
	private float netPremiumAmount;
	private float aptcAmnt;
	private String carrierAppId;
	private String confirmationNo;
	private String groupPolicyNumber;

	private int cmrHouseholdId;
	private Long ssapApplicationId;
	private String capAgentId;
	private String capAgentName;

	private int issuerId;
	private String issuerName;
	private String issuerUserName;
	private String issuerPassword;

	private String carrierAppUrl;

	private String exchangeAssignPolicyNo;
	private String issuerAssignPolicyNo;
	private String preferredContactTime;
	private Date   benefitEffectiveDate;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEnrollmentStatusLookup() {
		return enrollmentStatusLookup;
	}

	public void setEnrollmentStatusLookup(int enrollmentStatusLookup) {
		this.enrollmentStatusLookup = enrollmentStatusLookup;
	}

	public String getEnrollmentStatusLookupCode() {
		return enrollmentStatusLookupCode;
	}

	public void setEnrollmentStatusLookupCode(String enrollmentStatusLookupCode) {
		this.enrollmentStatusLookupCode = enrollmentStatusLookupCode;
	}

	public int getInsuranceTypeLookup() {
		return insuranceTypeLookup;
	}

	public void setInsuranceTypeLookup(int insuranceTypeLookup) {
		this.insuranceTypeLookup = insuranceTypeLookup;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public float getGrossPremiumAmount() {
		return grossPremiumAmount;
	}

	public void setGrossPremiumAmount(float grossPremiumAmount) {
		this.grossPremiumAmount = grossPremiumAmount;
	}

	public float getNetPremiumAmount() {
		return netPremiumAmount;
	}

	public void setNetPremiumAmount(float netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}

	public float getAptcAmnt() {
		return aptcAmnt;
	}

	public void setAptcAmnt(float aptcAmnt) {
		this.aptcAmnt = aptcAmnt;
	}

	public String getCarrierAppId() {
		return carrierAppId;
	}

	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}

	public String getConfirmationNo() {
		return confirmationNo;
	}

	public void setConfirmationNo(String confirmationNo) {
		this.confirmationNo = confirmationNo;
	}

	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}

	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}

	public int getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(int cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}



	public String getCapAgentId() {
		return capAgentId;
	}

	public void setCapAgentId(String capAgentId) {
		this.capAgentId = capAgentId;
	}

	public String getCapAgentName() {
		return capAgentName;
	}

	public void setCapAgentName(String capAgentName) {
		this.capAgentName = capAgentName;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getIssuerUserName() {
		return issuerUserName;
	}

	public void setIssuerUserName(String issuerUserName) {
		this.issuerUserName = issuerUserName;
	}

	public String getIssuerPassword() {
		return issuerPassword;
	}

	public void setIssuerPassword(String issuerPassword) {
		this.issuerPassword = issuerPassword;
	}

	public String getCarrierAppUrl() {
		return carrierAppUrl;
	}

	public void setCarrierAppUrl(String carrierAppUrl) {
		this.carrierAppUrl = carrierAppUrl;
	}

	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}

	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}

	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}

	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}

	public String getPreferredContactTime() {
		return preferredContactTime;
	}

	public void setPreferredContactTime(String preferredContactTime) {
		this.preferredContactTime = preferredContactTime;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

}
