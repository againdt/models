package com.getinsured.hix.dto.eapp.workflow;

public class WorkflowStep {
	private WorkflowName name;
	private Object args;
	private boolean enabled;
	
	public WorkflowStep(){
		
	}
	
	public WorkflowStep(WorkflowName name){
		this.name = name;
		this.enabled = true;
	}

	public WorkflowName getName() {
		return name;
	}

	public void setName(WorkflowName name) {
		this.name = name;
	}

	public Object getArgs() {
		return args;
	}

	public void setArgs(Object args) {
		this.args = args;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	

}
