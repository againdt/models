/**
 * 
 * @author santanu
 * @version 1.0
 * @since June 13, 2016 
 * 
 * This DTO to response object for Life Span Quoting of Enrollment
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class LifeSpanEnrollmentQuotingResponseDTO  extends GHIXResponse implements Serializable {

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String planName;
	private int planYear;
	private String costSharing;
	private String hiosPlanId;
	private String planLevel;
	private String issuerName;
	private int issuerId;
	private String issuerTaxId;
	private String issuerLogo;
	private float csrAmount;
	private String hiosIssuerId;
	private String ehbPercentage;
	private String pediatricDentalComponent;
	private String planDecertificationDate;
	private String stateEhbMandatePercentage;
	
	private List<LifeSpanPeriodLoopDTO> periodLoopList;
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the planYear
	 */
	public int getPlanYear() {
		return planYear;
	}
	/**
	 * @param planYear the planYear to set
	 */
	public void setPlanYear(int planYear) {
		this.planYear = planYear;
	}
	/**
	 * @return the costSharing
	 */
	public String getCostSharing() {
		return costSharing;
	}
	/**
	 * @param costSharing the costSharing to set
	 */
	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}
	/**
	 * @return the hiosPlanId
	 */
	public String getHiosPlanId() {
		return hiosPlanId;
	}
	/**
	 * @param hiosPlanId the hiosPlanId to set
	 */
	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}
	/**
	 * @return the planLevel
	 */
	public String getPlanLevel() {
		return planLevel;
	}
	/**
	 * @param planLevel the planLevel to set
	 */
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the issuerId
	 */
	public int getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerTaxId
	 */
	public String getIssuerTaxId() {
		return issuerTaxId;
	}
	/**
	 * @param issuerTaxId the issuerTaxId to set
	 */
	public void setIssuerTaxId(String issuerTaxId) {
		this.issuerTaxId = issuerTaxId;
	}
	/**
	 * @return the issuerLogo
	 */
	public String getIssuerLogo() {
		return issuerLogo;
	}
	/**
	 * @param issuerLogo the issuerLogo to set
	 */
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	/**
	 * @return the csrAmount
	 */
	public float getCsrAmount() {
		return csrAmount;
	}
	/**
	 * @param csrAmount the csrAmount to set
	 */
	public void setCsrAmount(float csrAmount) {
		this.csrAmount = csrAmount;
	}
	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	/**
	 * @param hiosIssuerId the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	/**
	 * @return the ehbPercentage
	 */
	public String getEhbPercentage() {
		return ehbPercentage;
	}
	/**
	 * @param ehbPercentage the ehbPercentage to set
	 */
	public void setEhbPercentage(String ehbPercentage) {
		this.ehbPercentage = ehbPercentage;
	}
	/**
	 * @return the pediatricDentalComponent
	 */
	public String getPediatricDentalComponent() {
		return pediatricDentalComponent;
	}
	/**
	 * @param pediatricDentalComponent the pediatricDentalComponent to set
	 */
	public void setPediatricDentalComponent(String pediatricDentalComponent) {
		this.pediatricDentalComponent = pediatricDentalComponent;
	}
	/**
	 * @return the planDecertificationDate
	 */
	public String getPlanDecertificationDate() {
		return planDecertificationDate;
	}
	/**
	 * @param planDecertificationDate the planDecertificationDate to set
	 */
	public void setPlanDecertificationDate(String planDecertificationDate) {
		this.planDecertificationDate = planDecertificationDate;
	}
	/**
	 * @return the stateEhbMandatePercentage
	 */
	public String getStateEhbMandatePercentage() {
		return stateEhbMandatePercentage;
	}
	/**
	 * @param stateEhbMandatePercentage the stateEhbMandatePercentage to set
	 */
	public void setStateEhbMandatePercentage(String stateEhbMandatePercentage) {
		this.stateEhbMandatePercentage = stateEhbMandatePercentage;
	}
	/**
	 * @return the periodLoopList
	 */
	public List<LifeSpanPeriodLoopDTO> getPeriodLoopList() {
		return periodLoopList;
	}
	/**
	 * @param periodLoopList the periodLoopList to set
	 */
	public void setPeriodLoopList(List<LifeSpanPeriodLoopDTO> periodLoopList) {
		this.periodLoopList = periodLoopList;
	}
	
	
}
