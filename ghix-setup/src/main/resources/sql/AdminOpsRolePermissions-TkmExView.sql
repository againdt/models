INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
		(select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_EXTENDEDLIST_TICKET')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
		(select id from roles where name='ADMIN'), (select id from permissions   where name=UPPER('TKM_EXTENDEDLIST_TICKET')));
