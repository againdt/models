package com.getinsured.hix.dto.cap.appt;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class SlotDataDto implements Serializable {

    private static final long serialVersionUID = -3853460206000173140L;
    private String apptTime;
    private String timeSlot;
    private int count;
    // HIX-87940 Add timezone field for get slots api.
    private String timezone;
    private Date appointmentDate; // slot start time
    private Date appointmentEndDate; // slot end time
    private Date endDate;
    public static String HOUR_MINUTE_FORMAT = "hh:mm a";


    public SlotDataDto() {
        super();
    }

    public SlotDataDto(Date appointmentDate, Date appointmentEndDate, TimeZone timeZone) {
        DateFormat formatter = new SimpleDateFormat(HOUR_MINUTE_FORMAT);
        this.appointmentDate = appointmentDate;
        this.appointmentEndDate = appointmentEndDate;
        this.timezone = timeZone.getID();
        this.apptTime = removeLeadingZero(formatter.format(appointmentDate));
        String szEndTime = removeLeadingZero(formatter.format(appointmentEndDate));
        this.timeSlot = this.apptTime + " - " + szEndTime;
        this.count = 0;
    }

    // HIX-88225: Remove beginning 0 in timeSlots which have a single digit hour for getSlots
    public static String removeLeadingZero(String s) {
        if (s.startsWith("0")) {
            s = s.replaceFirst("0", "");
        }
        return s;
    }

    public String getApptTime() {
        return apptTime;
    }

    public void setApptTime(String apptTime) {
        this.apptTime = apptTime;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getAppointmentEndDate() {
        return appointmentEndDate;
    }

    public void setAppointmentEndDate(Date appointmentEndDate) {
        this.appointmentEndDate = appointmentEndDate;
    }

    @Override
    public String toString() {

        return "ApptDate:" + appointmentDate + ", Slot:" + timeSlot;

    }
}
