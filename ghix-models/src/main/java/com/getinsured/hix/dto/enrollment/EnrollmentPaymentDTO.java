package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class EnrollmentPaymentDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4352472414920333455L;
	private Integer enrollmentId;
	private String cmsPlanId;
	private Float grossPremiumAmt;
	private Float netPremiumAmt;
	private Float aptcAmt;
	private Date benefitEffectiveDate;
	private String insuranceType;
	private String firstName;
	private String middleName;
	private String lastName;
	private String addressLine1;
	private String addressLine2;
	private String state;
	private String city;
	private String zip;
	private String phoneNumber;
	private String contactEmail;
	private String prefferedEmail;
	private String enrolleeNames;
	private String paymentUrl;
	private String hiosIssuerId;
	private String name;
    private String exchgSubscriberIdentifier;
    private String paymentTxnId;
    private String caseId;
    private String preferredLanguage;
    private String countyCode;
    private String previousPaymentTxnId;
    
    List<EnrolleePaymentDTO> enrolleePaymentDtoList;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getCmsPlanId() {
		return cmsPlanId;
	}
	public void setCmsPlanId(String cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}
	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}
	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}
	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}
	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}
	public Float getAptcAmt() {
		if(this.aptcAmt == null) {
			return Float.valueOf(0f);
		}
		return aptcAmt;
	}
	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getPrefferedEmail() {
		return prefferedEmail;
	}
	public void setPrefferedEmail(String prefferedEmail) {
		this.prefferedEmail = prefferedEmail;
	}
	public String getEnrolleeNames() {
		return enrolleeNames;
	}
	public void setEnrolleeNames(String enrolleeNames) {
		this.enrolleeNames = enrolleeNames;
	}
	public String getPaymentUrl() {
		return paymentUrl;
	}
	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}
	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}
	public String getPaymentTxnId() {
		return paymentTxnId;
	}
	public void setPaymentTxnId(String paymentTxnId) {
		this.paymentTxnId = paymentTxnId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	public String getCaseId() {
		return caseId;
	}
	public String getPreferredLanguage() {
		return preferredLanguage;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getPreviousPaymentTxnId() {
		return previousPaymentTxnId;
	}
	public void setPreviousPaymentTxnId(String previousPaymentTxnId) {
		this.previousPaymentTxnId = previousPaymentTxnId;
	}
	public List<EnrolleePaymentDTO> getEnrolleePaymentDtoList() {
		return enrolleePaymentDtoList;
	}
	public void setEnrolleePaymentDtoList(List<EnrolleePaymentDTO> enrolleePaymentDtoList) {
		this.enrolleePaymentDtoList = enrolleePaymentDtoList;
	}
	@Override
	public String toString() {
		return "EnrollmentPaymentDTO [enrollmentId=" + enrollmentId + ", cmsPlanId=" + cmsPlanId + ", grossPremiumAmt="
				+ grossPremiumAmt + ", netPremiumAmt=" + netPremiumAmt + ", aptcAmt=" + aptcAmt
				+ ", benefitEffectiveDate=" + benefitEffectiveDate + ", insuranceType=" + insuranceType + ", firstName="
				+ firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", state=" + state + ", city=" + city + ", zip=" + zip
				+ ", phoneNumber=" + phoneNumber + ", contactEmail=" + contactEmail + ", prefferedEmail="
				+ prefferedEmail + ", enrolleeNames=" + enrolleeNames + ", paymentUrl=" + paymentUrl + ", hiosIssuerId="
				+ hiosIssuerId + ", name=" + name + ", exchgSubscriberIdentifier=" + exchgSubscriberIdentifier
				+ ", paymentTxnId=" + paymentTxnId + ", caseId=" + caseId + ", preferredLanguage=" + preferredLanguage
				+ ", countyCode=" + countyCode + ", previousPaymentTxnId=" + previousPaymentTxnId
				+ ", enrolleePaymentDtoList=" + enrolleePaymentDtoList + "]";
	}
	
}
