package com.getinsured.hix.model.emx;

import java.io.Serializable;

public class EmployerSessionData implements Serializable  {

	
	private static final long serialVersionUID = 1230951042501540394L;
	
	private Long affiliateFlowAccessId;
	private Long affiliateId;
	private Integer flowId;
	private boolean isAccessCodeVerified;
	private Long employerId;
	private String employerPhoneNumber;
	private Boolean emxCustomHeader=false;
	
	public Long getAffiliateFlowAccessId() {
		return affiliateFlowAccessId;
	}
	public void setAffiliateFlowAccessId(Long affiliateFlowAccessId) {
		this.affiliateFlowAccessId = affiliateFlowAccessId;
	}
	public Long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}
	public Integer getFlowId() {
		return flowId;
	}
	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}
	public boolean isAccessCodeVerified() {
		return isAccessCodeVerified;
	}
	public void setAccessCodeVerified(boolean isAccessCodeVerified) {
		this.isAccessCodeVerified = isAccessCodeVerified;
	}
	public Long getEmployerId() {
		return employerId;
	}
	public void setEmployerId(Long employerId) {
		this.employerId = employerId;
	}
	
	public String getEmployerPhoneNumber() {
		return employerPhoneNumber;
	}
	public void setEmployerPhoneNumber(String employerPhoneNumber) {
		this.employerPhoneNumber = employerPhoneNumber;
	}
	public Boolean getEmxCustomHeader() {
		return emxCustomHeader;
	}
	public void setEmxCustomHeader(Boolean emxCustomHeader) {
		this.emxCustomHeader = emxCustomHeader;
	}
	 
}
