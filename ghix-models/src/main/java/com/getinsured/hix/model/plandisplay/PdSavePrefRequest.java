package com.getinsured.hix.model.plandisplay;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;

public class PdSavePrefRequest {
	
	private Long householdId;
	private PdPreferencesDTO pdPreferencesDTO;
	
	public Long getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}
}
