package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ENRL_CMS_OUT_DTL")
public class EnrollmentCmsOutDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_OUT_DTL_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_OUT_DTL_SEQ", sequenceName = "ENRL_CMS_OUT_DTL_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name = "ENROLLMENT_ID")
	private Integer enrollmentId;

	@Column(name = "ENRL_CMS_OUT_ID")
	private Integer enrlCmsOutId;

	@Column(name = "HIOS_ISSUER_ID")
	private String hiosIssuerId;

	@Column(name = "COVERAGE_YEAR")
	private Integer coverageYear;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;
	

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	/**
	 * @return the enrlCmsOutId
	 */
	public Integer getEnrlCmsOutId() {
		return enrlCmsOutId;
	}

	/**
	 * @param enrlCmsOutId the enrlCmsOutId to set
	 */
	public void setEnrlCmsOutId(Integer enrlCmsOutId) {
		this.enrlCmsOutId = enrlCmsOutId;
	}

	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	/**
	 * @param hiosIssuerId the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	/**
	 * @return the coverageYear
	 */
	public Integer getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
	}
}
