package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EXTRACTION_JOB_STATUS")
public class ExtractionJobStatus implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXTRACTION_JOB_STATUS_SEQ")
	@SequenceGenerator(name = "EXTRACTION_JOB_STATUS_SEQ", sequenceName = "EXTRACTION_JOB_STATUS_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "STATUS")
	private String status;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="START_DATE")
	private Date startDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="END_DATE")
	private Date endDate;

	@Column(name="JOB_EXECUTION_ID")
	private Long jobExecutionId;

	@Column(name="JOB_NAME")
	private String jobName;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON")
	private Date createdOn;
	
	@Column(name="EXCEPTION")
	private  String exception;
	
	@Column(name="FAILED_ENROLLMENT_IDS")
	private String failedEnrollmentIds;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getFailedEnrollmentIds() {
		return failedEnrollmentIds;
	}

	public void setFailedEnrollmentIds(String failedEnrollmentIds) {
		this.failedEnrollmentIds = failedEnrollmentIds;
	}
}
