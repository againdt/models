package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FamilyRelationshipCode{
  @JsonProperty("Value")
  
  private Integer Value;
  public void setValue(Integer Value){
   this.Value=Value;
  }
  public Integer getValue(){
   return Value;
  }
}