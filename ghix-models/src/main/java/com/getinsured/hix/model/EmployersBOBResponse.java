package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.broker.EmployerBOBDetailsDTO;

/**
 * BO for external employer data
 *
 */
public class EmployersBOBResponse extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private int responseCode;
	private String responseDescription;
	private List<EmployerBOBDetailsDTO> employersBOBDetailsDTOList;
	private EmployerBOBDetailsDTO employerBOBDetailsDTO;
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDescription() {
		return responseDescription;
	}
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	public List<EmployerBOBDetailsDTO> getEmployersBOBDetailsDTOList() {
		return employersBOBDetailsDTOList;
	}
	public void setEmployersBOBDetailsDTOList(
			List<EmployerBOBDetailsDTO> employersBOBDetailsDTOList) {
		this.employersBOBDetailsDTOList = employersBOBDetailsDTOList;
	}
	public EmployerBOBDetailsDTO getEmployerBOBDetailsDTO() {
		return employerBOBDetailsDTO;
	}
	public void setEmployerBOBDetailsDTO(EmployerBOBDetailsDTO employerBOBDetailsDTO) {
		this.employerBOBDetailsDTO = employerBOBDetailsDTO;
	}
	
    @Override
    public String toString() {
    	return new StringBuilder().append("EmployersBOBResponse details: responseCode=").append(responseCode).append(", responseDescription=").append(responseDescription).append(", employerBOBDetailsDTO=").append(employerBOBDetailsDTO).toString(); 
    }
}
