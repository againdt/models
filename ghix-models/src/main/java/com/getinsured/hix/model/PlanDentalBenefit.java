package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@SuppressWarnings("serial")
@Entity
@Table(name="plan_dental_benefit")
public class PlanDentalBenefit implements Serializable,BaseBenefit<PlanDental> {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanDentalBenefit_Seq")
	@SequenceGenerator(name = "PlanDentalBenefit_Seq", sequenceName = "plan_dental_benefit_seq", allocationSize = 1)
	private int id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
    @JoinColumn(name="plan_dental_id")
	private PlanDental plan;

	@Column(name="name", length=150)
	private String name;


	/*@Column(name="network_value", length=20)
	private String networkValue;


	@Column(name="network_attr", length=50)
	private String networkAttribute;*/


	@Column(name="limitation", length=20)
	private String networkLimitation;


	@Column(name="limitation_attr", length=50)
	private String networkLimitationAttribute;

	@Column(name="network_exceptions", length=255)
	private String networkExceptions;


	/*@Column(name="nonnetwork_value", length=20)
	private String nonnetworkValue;


	@Column(name="nonnetwork_attr", length=50)
	private String nonnetworkAttribute;


	@Column(name="nonnetwork_limitation", length=20)
	private String nonnetworkLimitation;


	@Column(name="nonnetwork_limitation_attr", length=50)
	private String nonnetworkLimitationAttribute;


	@Column(name="nonnetwork_exceptions", length=255)
	private String nonnetworkExceptions;*/

	@Temporal(value = TemporalType.DATE)
	@Column(name="effective_start_date")
	private Date effStartDate;

	@Temporal(value = TemporalType.DATE)
	@Column(name="effective_end_date")
	private Date effEndDate;

	@Column(name="network_subj_deduct", length=255)
	private String networkSubjDeduct;

	@Column(name="network_excl_frm_moop", length=255)
	private String networkExclFrmMoop;

	@Column(name="nonnetwork_subj_deduct", length=255)
	private String nonnetworkSubjDeduct;

	@Column(name="nonnetwork_excl_frm_moop", length=255)
	private String nonnetworkExclFrmMoop;

	//End: New columns for HIX-9907
	@Column(name="is_ehb")
	private String isEHB;

	@Column(name="is_covered")
	private String isCovered;

	@Column(name="min_stay")
	private String minStay;

	@Column(name="explanation")
	private String explanation;

	@Column(name="subject_to_in_net_deductible",length=255)
	private String subjectToInNetworkDuductible;

	@Column(name="excluded_from_in_net_moop",length=255)
	private String excludedFromInNetworkMoop;

	@Column(name="subject_to_out_net_deductible",length=255)
	private String subjectToOutNetworkDeductible;

	@Column(name="excluded_from_out_of_net_moop",length=255)
	private String excludedFromOutOfNetworkMoop;

	/*@Column(name="network_tier2_value",length=255)
	private String network_tier2_value;*/
	//End: New columns for HIX-9907

	//HIX-13596 New columns for copay/coinsurance starts
	@Column(name="NETWORK_T1_COPAY_VAL")
	private String networkT1CopayVal;

	@Column(name="NETWORK_T1_COPAY_ATTR")
	private String networkT1CopayAttr;

	@Column(name="NETWORK_T1_COINSURANCE_VAL")
	private String networkT1CoinsurVal;

	@Column(name="NETWORK_T1_COINSURANCE_ATTR")
	private String networkT1CoinsurAttr;

	@Column(name="NETWORK_T2_COPAY_VAL")
	private String networkT2CopayVal;

	@Column(name="NETWORK_T2_COPAY_ATTR")
	private String networkT2CopayAttr;

	@Column(name="NETWORK_T2_COINSURANCE_VAL")
	private String networkT2CoinsurVal;

	@Column(name="NETWORK_T2_COINSURANCE_ATTR")
	private String networkT2CoinsurAttr;

	@Column(name="OUTNETWORK_COPAY_VAL")
	private String outOfNetworkCopayVal;

	@Column(name="OUTNETWORK_COPAY_ATTR")
	private String outOfNetworkCopayAttr;

	@Column(name="OUTNETWORK_COINSURANCE_VAL")
	private String outOfNetworkCoinsurVal;

	@Column(name="OUTNETWORK_COINSURANCE_ATTR")
	private String outOfNetworkCoinsurAttr;
	//HIX-13596 New columns for copay/coinsurance ends

	@Column(name = "network_t1_display")
	private String networkT1display;

	@Column(name = "network_t2_display")
	private String networkT2display;

	@Column(name = "outnetwork_display")
	private String outOfNetworkDisplay;

	@Column(name = "network_t1_tile_display")
	private String networkT1TileDisplay;

	@Column(name = "coinsurance_in_network_tier1")
	private String coinsuranceInNetworkTier1;

	@Column(name = "coinsurance_in_network_tier2")
	private String coinsuranceInNetworkTier2;

	@Column(name = "coinsurance_out_of_network")
	private String coinsuranceOutOfNetwork;

	@Column(name = "copay_in_network_tier1")
	private String copayInNetworkTier1;

	@Column(name = "copay_in_network_tier2")
	private String copayInNetworkTier2;

	@Column(name = "copay_out_of_network")
	private String copayOutOfNetwork;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "created_by", length = 10)
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "last_updated_by", length = 10)
	private Integer lastUpdatedBy;

	/*	Adding new Column for HIX-25118	*/
	@Column(name = "LIMIT_EXCEP_DISPLAY")
	private String limitExcepDisplay;

	public String getLimitExcepDisplay() {
		return limitExcepDisplay;
	}

	public void setLimitExcepDisplay(String limitExcepDisplay) {
		this.limitExcepDisplay = limitExcepDisplay;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanDental getPlanDental() {
		return plan;
	}

	public void setPlanDental(PlanDental plan) {
		this.plan = plan;
	}

	@Override
	public void setParent(PlanDental parentPlan)
	{
		setPlanDental(parentPlan);
	}



	/*public String getNetworkValue() {
		return networkValue;
	}

	public void setNetworkValue(String networkValue) {
		this.networkValue = networkValue;
	}

	public String getNetworkAttribute() {
		return networkAttribute;
	}

	public void setNetworkAttribute(String networkAttribute) {
		this.networkAttribute = networkAttribute;
	}
*/
	public String getNetworkLimitation() {
		return networkLimitation;
	}

	public void setNetworkLimitation(String networkLimitation) {
		this.networkLimitation = networkLimitation;
	}

	public String getNetworkLimitationAttribute() {
		return networkLimitationAttribute;
	}

	public void setNetworkLimitationAttribute(String networkLimitationAttribute) {
		this.networkLimitationAttribute = networkLimitationAttribute;
	}

	public String getNetworkExceptions() {
		return networkExceptions;
	}

	public void setNetworkExceptions(String networkExceptions) {
		this.networkExceptions = networkExceptions;
	}

	/*public String getNonnetworkValue() {
		return nonnetworkValue;
	}

	public void setNonnetworkValue(String nonnetworkValue) {
		this.nonnetworkValue = nonnetworkValue;
	}

	public String getNonnetworkAttribute() {
		return nonnetworkAttribute;
	}

	public void setNonnetworkAttribute(String nonnetworkAttribute) {
		this.nonnetworkAttribute = nonnetworkAttribute;
	}

	public String getNonnetworkLimitation() {
		return nonnetworkLimitation;
	}

	public void setNonnetworkLimitation(String nonnetworkLimitation) {
		this.nonnetworkLimitation = nonnetworkLimitation;
	}

	public String getNonnetworkLimitationAttribute() {
		return nonnetworkLimitationAttribute;
	}

	public void setNonnetworkLimitationAttribute(
			String nonnetworkLimitationAttribute) {
		this.nonnetworkLimitationAttribute = nonnetworkLimitationAttribute;
	}

	public String getNonnetworkExceptions() {
		return nonnetworkExceptions;
	}

	public void setNonnetworkExceptions(String nonnetworkExceptions) {
		this.nonnetworkExceptions = nonnetworkExceptions;
	}*/

	public Date getEffStartDate() {
		return effStartDate;
	}

	public void setEffStartDate(Date startDate) {
		this.effStartDate = startDate;
	}

	public Date getEffEndDate() {
		return effEndDate;
	}

	public void setEffEndDate(Date endDate) {
		this.effEndDate = endDate;
	}

	public String getNetworkSubjDeduct() {
		return networkSubjDeduct;
	}

	public void setNetworkSubjDeduct(String networkSubjDeduct) {
		this.networkSubjDeduct = networkSubjDeduct;
	}

	public String getNetworkExclFrmMoop() {
		return networkExclFrmMoop;
	}

	public void setNetworkExclFrmMoop(String networkExclFrmMoop) {
		this.networkExclFrmMoop = networkExclFrmMoop;
	}

	public String getNonnetworkSubjDeduct() {
		return nonnetworkSubjDeduct;
	}

	public void setNonnetworkSubjDeduct(String nonnetworkSubjDeduct) {
		this.nonnetworkSubjDeduct = nonnetworkSubjDeduct;
	}

	public String getNonnetworkExclFrmMoop() {
		return nonnetworkExclFrmMoop;
	}

	public void setNonnetworkExclFrmMoop(String nonnetworkExclFrmMoop) {
		this.nonnetworkExclFrmMoop = nonnetworkExclFrmMoop;
	}

	public String getIsEHB() {
		return isEHB;
	}

	public void setIsEHB(String isEHB) {
		this.isEHB = isEHB;
	}

	public String getIsCovered() {
		return isCovered;
	}

	public void setIsCovered(String isCovered) {
		this.isCovered = isCovered;
	}

	public String getMinStay() {
		return minStay;
	}

	public void setMinStay(String minStay) {
		this.minStay = minStay;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public String getSubjectToInNetworkDuductible() {
		return subjectToInNetworkDuductible;
	}

	public void setSubjectToInNetworkDuductible(String subjectToInNetworkDuductible) {
		this.subjectToInNetworkDuductible = subjectToInNetworkDuductible;
	}

	public String getExcludedFromInNetworkMoop() {
		return excludedFromInNetworkMoop;
	}

	public void setExcludedFromInNetworkMoop(String excludedFromInNetworkMoop) {
		this.excludedFromInNetworkMoop = excludedFromInNetworkMoop;
	}

	public String getSubjectToOutNetworkDeductible() {
		return subjectToOutNetworkDeductible;
	}

	public void setSubjectToOutNetworkDeductible(
			String subjectToOutNetworkDeductible) {
		this.subjectToOutNetworkDeductible = subjectToOutNetworkDeductible;
	}

	public String getExcludedFromOutOfNetworkMoop() {
		return excludedFromOutOfNetworkMoop;
	}

	public void setExcludedFromOutOfNetworkMoop(String excludedFromOutOfNetworkMoop) {
		this.excludedFromOutOfNetworkMoop = excludedFromOutOfNetworkMoop;
	}

	/*public String getNetwork_tier2_value() {
		return network_tier2_value;
	}

	public void setNetwork_tier2_value(String network_tier2_value) {
		this.network_tier2_value = network_tier2_value;
	}*/

	public String getNetworkT1CopayVal() {
		return networkT1CopayVal;
	}

	public void setNetworkT1CopayVal(String networkT1CopayVal) {
		this.networkT1CopayVal = networkT1CopayVal;
	}

	public String getNetworkT1CopayAttr() {
		return networkT1CopayAttr;
	}

	public void setNetworkT1CopayAttr(String networkT1CopayAttr) {
		this.networkT1CopayAttr = networkT1CopayAttr;
	}

	public String getNetworkT1CoinsurVal() {
		return networkT1CoinsurVal;
	}

	public void setNetworkT1CoinsurVal(String networkT1CoinsurVal) {
		this.networkT1CoinsurVal = networkT1CoinsurVal;
	}

	public String getNetworkT1CoinsurAttr() {
		return networkT1CoinsurAttr;
	}

	public void setNetworkT1CoinsurAttr(String networkT1CoinsurAttr) {
		this.networkT1CoinsurAttr = networkT1CoinsurAttr;
	}

	public String getNetworkT2CopayVal() {
		return networkT2CopayVal;
	}

	public void setNetworkT2CopayVal(String networkT2CopayVal) {
		this.networkT2CopayVal = networkT2CopayVal;
	}

	public String getNetworkT2CopayAttr() {
		return networkT2CopayAttr;
	}

	public void setNetworkT2CopayAttr(String networkT2CopayAttr) {
		this.networkT2CopayAttr = networkT2CopayAttr;
	}

	public String getNetworkT2CoinsurVal() {
		return networkT2CoinsurVal;
	}

	public void setNetworkT2CoinsurVal(String networkT2CoinsurVal) {
		this.networkT2CoinsurVal = networkT2CoinsurVal;
	}

	public String getNetworkT2CoinsurAttr() {
		return networkT2CoinsurAttr;
	}

	public void setNetworkT2CoinsurAttr(String networkT2CoinsurAttr) {
		this.networkT2CoinsurAttr = networkT2CoinsurAttr;
	}

	public String getOutOfNetworkCopayVal() {
		return outOfNetworkCopayVal;
	}

	public void setOutOfNetworkCopayVal(String outOfNetworkCopayVal) {
		this.outOfNetworkCopayVal = outOfNetworkCopayVal;
	}

	public String getOutOfNetworkCopayAttr() {
		return outOfNetworkCopayAttr;
	}

	public void setOutOfNetworkCopayAttr(String outOfNetworkCopayAttr) {
		this.outOfNetworkCopayAttr = outOfNetworkCopayAttr;
	}

	public String getOutOfNetworkCoinsurVal() {
		return outOfNetworkCoinsurVal;
	}

	public void setOutOfNetworkCoinsurVal(String outOfNetworkCoinsurVal) {
		this.outOfNetworkCoinsurVal = outOfNetworkCoinsurVal;
	}

	public String getOutOfNetworkCoinsurAttr() {
		return outOfNetworkCoinsurAttr;
	}

	public void setOutOfNetworkCoinsurAttr(String outOfNetworkCoinsurAttr) {
		this.outOfNetworkCoinsurAttr = outOfNetworkCoinsurAttr;
	}

	public String getNetworkT1display() {
		return networkT1display;
	}

	public void setNetworkT1display(String networkT1display) {
		this.networkT1display = networkT1display;
	}

	public String getNetworkT2display() {
		return networkT2display;
	}

	public void setNetworkT2display(String networkT2display) {
		this.networkT2display = networkT2display;
	}

	public String getOutOfNetworkDisplay() {
		return outOfNetworkDisplay;
	}

	public void setOutOfNetworkDisplay(String outOfNetworkDisplay) {
		this.outOfNetworkDisplay = outOfNetworkDisplay;
	}

	public String getNetworkT1TileDisplay() {
		return networkT1TileDisplay;
	}

	public void setNetworkT1TileDisplay(String networkT1TileDisplay) {
		this.networkT1TileDisplay = networkT1TileDisplay;
	}

	public String getCoinsuranceInNetworkTier1() {
		return coinsuranceInNetworkTier1;
	}

	public void setCoinsuranceInNetworkTier1(String coinsuranceInNetworkTier1) {
		this.coinsuranceInNetworkTier1 = coinsuranceInNetworkTier1;
	}

	public String getCoinsuranceInNetworkTier2() {
		return coinsuranceInNetworkTier2;
	}

	public void setCoinsuranceInNetworkTier2(String coinsuranceInNetworkTier2) {
		this.coinsuranceInNetworkTier2 = coinsuranceInNetworkTier2;
	}

	public String getCoinsuranceOutOfNetwork() {
		return coinsuranceOutOfNetwork;
	}

	public void setCoinsuranceOutOfNetwork(String coinsuranceOutOfNetwork) {
		this.coinsuranceOutOfNetwork = coinsuranceOutOfNetwork;
	}

	public String getCopayInNetworkTier1() {
		return copayInNetworkTier1;
	}

	public void setCopayInNetworkTier1(String copayInNetworkTier1) {
		this.copayInNetworkTier1 = copayInNetworkTier1;
	}

	public String getCopayInNetworkTier2() {
		return copayInNetworkTier2;
	}

	public void setCopayInNetworkTier2(String copayInNetworkTier2) {
		this.copayInNetworkTier2 = copayInNetworkTier2;
	}

	public String getCopayOutOfNetwork() {
		return copayOutOfNetwork;
	}

	public void setCopayOutOfNetwork(String copayOutOfNetwork) {
		this.copayOutOfNetwork = copayOutOfNetwork;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
