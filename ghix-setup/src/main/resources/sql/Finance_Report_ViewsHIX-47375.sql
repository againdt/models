CREATE OR REPLACE 
VIEW ACCOUNT_PAYABLE(ID, ISSUER_ID, ISSUER_NAME, INVOICE_NUMBER, STATEMENT_DATE, TOTAL_AMOUNT_DUE, PAID_AMOUNT, PAID_STATUS, BALANCE_AMOUNT) 
AS
SELECT rownum, i.id, i.name, inv.remittance_number, trunc(inv.statement_date),
inv.total_amount_due, inv.total_payment_paid, inv.paid_status, cast((inv.total_amount_due - inv.total_payment_paid) as float(126)) 
FROM issuer_Remittance inv, issuers i 
WHERE i.id = inv.issuer_id 
AND inv.is_active = 'Y' 
AND inv.paid_status in ('DUE','PARTIALLY_PAID') 
WITH READ ONLY;



CREATE OR REPLACE VIEW REMITTANCE(ID, INVOICE_NUMBER, STATEMENT_DATE, EMPLOYER_NAME, EMPLOYEE_NAME, CARRIER_NAME, PLAN_TYPE, STATUS,
NET_AMOUNT,AMOUNT_PAID_TO_ISSUER, DUE_AMOUNT, EXCHANGE_FEES, DEDUCTED_EXCHANGE_FEES, PAYMENT_DATE, TRANSACTION_ID, PAYMENT_TYPE, ACCOUNT_NUMBER) AS
SELECT rownum, inv.invoice_number, trunc(inv.statement_date), emp.name, empl.name , empLin.carrier_name, empLin.plan_type, inv.status, li.net_amount, li.amount_paid_to_issuer,
cast((li.net_amount - li.amount_paid_to_issuer) as float(126)) as "DUE_AMOUNT",li.user_fee as "EXCHANGE_FEES",
cast((li.amount_received_from_employer - li.amount_paid_to_issuer) as float(126)) as "DEDUCTED_EXCHANGE_FEES",
trunc(dtl.payment_date),dtl.transaction_id, pay.Payment_type, bnk.account_number
FROM ISSUER_PAYMENTS inv, ISSUER_PAYMENT_DETAIL dtl, PAYMENT_METHODS pay,
BANK_INFO bnk, ISSUER_REMITTANCE_LINEITEM li,EMPLOYER_INVOICE_LINEITEMS empLin, Employers emp, Employees empl
WHERE inv.id = dtl.ISSUER_PAYMENT_ID
AND pay.id = dtl.payment_type_id
AND bnk.id = (SELECT bank_info_id FROM financial_info WHERE id = pay.financial_info_id)
AND li.remittance_id = (SELECT invoice_id FROM issuer_payment_invoice WHERE id = inv.issuer_payment_invoice_id)
AND li.EMP_INV_LINEITEMS_ID = empLin.id
AND emp.id = (SELECT distinct(employer_id) from ISSUER_REMITTANCE_LINEITEM WHERE id = li.id)
AND empl.id = (SELECT distinct(employee_id) from ISSUER_REMITTANCE_LINEITEM WHERE id = li.id)
WITH READ ONLY;