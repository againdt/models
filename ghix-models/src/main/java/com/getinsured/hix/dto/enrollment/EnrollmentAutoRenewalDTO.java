package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import com.getinsured.hix.model.plandisplay.PdOrderResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;

public class EnrollmentAutoRenewalDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String orderid;
	private String enrollmentType;
	private AutoRenewalRequest autoRenewalRequest;
	private PdOrderResponse pdOrderResponse;
	private String globalId;
	private String correlationId;
	
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	public AutoRenewalRequest getAutoRenewalRequest() {
		return autoRenewalRequest;
	}
	public void setAutoRenewalRequest(AutoRenewalRequest autoRenewalRequest) {
		this.autoRenewalRequest = autoRenewalRequest;
	}
	public PdOrderResponse getPdOrderResponse() {
		return pdOrderResponse;
	}
	public void setPdOrderResponse(PdOrderResponse pdOrderResponse) {
		this.pdOrderResponse = pdOrderResponse;
	}
	public String getGlobalId() {
		return globalId;
	}
	public void setGlobalId(String globalId) {
		this.globalId = globalId;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

}
