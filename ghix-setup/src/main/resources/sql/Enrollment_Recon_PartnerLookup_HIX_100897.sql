			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(632,'BEST Life and Health Insurance Company',17859,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(633,'BEST Life and Health Insurance Company',17859,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(634,'BEST Life and Health Insurance Company',17859, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(635,'BEST Life and Health Insurance Company',17859, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(636,'Blue Cross of Idaho',61589,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(637,'Blue Cross of Idaho',61589,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(638,'Blue Cross of Idaho',61589, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(639,'Blue Cross of Idaho',61589, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(640,'BridgeSpan Health',59765,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(641,'BridgeSpan Health',59765,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(642,'BridgeSpan Health',59765, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(643,'BridgeSpan Health',59765, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(644,'Delta Dental of Idaho',46641,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(645,'Delta Dental of Idaho',46641,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(646,'Delta Dental of Idaho',46641, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(647,'Delta Dental of Idaho',46641, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(648,'Dentegra Dental',86139,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(649,'Dentegra Dental',86139,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(650,'Dentegra Dental',86139, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(651,'Dentegra Dental',86139, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(652,'Mountain Health CO-OP',38128,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(653,'Mountain Health CO-OP',38128,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(654,'Mountain Health CO-OP',38128, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(655,'Mountain Health CO-OP',38128, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(656,'PacificSource',60597,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(657,'PacificSource',60597,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(658,'PacificSource',60597, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(659,'PacificSource',60597, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(660,'SelectHealth',26002,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(661,'SelectHealth',26002,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(662,'SelectHealth',26002, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(663,'SelectHealth',26002, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			
			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(664,'Willamette Dental of Idaho, Inc.',22610,'FI','Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(665,'Willamette Dental of Idaho, Inc.',22610,'FI','Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(666,'Willamette Dental of Idaho, Inc.',22610, 24,'Outbound','T','REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',34,current_timestamp,current_timestamp);

			INSERT into EXCHG_PARTNERLOOKUP  (ID,INSURERNAME,HIOS_ISSUER_ID,MARKET,DIRECTION,isa15,ST01,SOURCE_DIR,TARGET_DIR,INPROCESS_DIR,ARCHIVE_DIR,ROLE_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP)
			VALUES(667,'Willamette Dental of Idaho, Inc.',22610, 24,'Inbound','T','REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',35,current_timestamp,current_timestamp);
			
			
			INSERT INTO EXCHG_FILEPATTERNROLE(ID, NAME, FILEPATTERN_IDS) VALUES (34,'5010 MONTHLY RECON OUT',55);
			INSERT INTO EXCHG_FILEPATTERNROLE(ID, NAME, FILEPATTERN_IDS) VALUES (35,'5010 MONTHLY RECON OUT',56);
			
			INSERT INTO EXCHG_FILEPATTERN(ID, DIRECTION, FILETYPE, FILEPATTERN) VALUES(55,'O','PAS','to_<HIOS_Issuer_ID>_INDV_MONTHLYDISCREPANCY_<YYYY>_<HHMMSSmmm>.OUT');
			INSERT INTO EXCHG_FILEPATTERN(ID, DIRECTION, FILETYPE, FILEPATTERN) VALUES(56,'I','PAS','from_<HIOS_Issuer_ID>_INDV_MONTHLYRECON_<YYYY>_<YYYYMMDDHHMMSS>.IN');
