package com.getinsured.coveredCA.dto.proxy;

import java.util.List;

import com.getinsured.coveredCA.dto.ssap.ExchangeStreamlinedApplication;

public class CoveredCAProxyRequest {
	
	public List<Questionnaire> questionnaire;
	public ExchangeStreamlinedApplication exchangeStreamlinedApplication;
	
	
	public List<Questionnaire> getQuestionnaire() {
		return questionnaire;
	}
	
	public void setQuestionnaire(List<Questionnaire> questionnaire) {
		this.questionnaire = questionnaire;
	}
	public ExchangeStreamlinedApplication getExchangeStreamlinedApplication() {
		return exchangeStreamlinedApplication;
	}
	public void setExchangeStreamlinedApplication(
			ExchangeStreamlinedApplication exchangeStreamlinedApplication) {
		this.exchangeStreamlinedApplication = exchangeStreamlinedApplication;
	}
	
	
	
	
}
