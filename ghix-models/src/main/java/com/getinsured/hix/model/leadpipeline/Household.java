package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

public class Household implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String encryptedId;
	
	private String leadCreationDate;
	
	private String lastUpdatedDate;	
	 
	private String phoneNumber;
	 
	private String state;	
	 
	private String zip;
	 
	private String timeZone;
	
	private String eligLeadId;
	
	private String dob;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEncryptedId() {
		return encryptedId;
	}

	public void setEncryptedId(String encryptedId) {
		this.encryptedId = encryptedId;
	}

	public String getLeadCreationDate() {
		return leadCreationDate;
	}

	public void setLeadCreationDate(String leadCreationDate) {
		this.leadCreationDate = leadCreationDate;
	}

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(String eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}	
	 

}
