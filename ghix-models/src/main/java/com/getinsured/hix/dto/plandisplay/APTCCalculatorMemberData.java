package com.getinsured.hix.dto.plandisplay;

public class APTCCalculatorMemberData {

	private long id;
	private int age;
	private Float premium;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	
}
