package com.getinsured.hix.model.serff;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

@Entity
@Table(name = "PUF_QHP")
//@org.hibernate.annotations.Entity(dynamicInsert = true, dynamicUpdate = true)
public class PufHealthVO implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}

	public enum FTP_STATUS {
		IN_PROGRESS, COMPLETED, FAILED;
	}

	public enum JOB_STATUS {
		HOLD, IN_PROGRESS, COMPLETED, FAILED;
	}

	@XmlElement(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PUF_QHP_SEQ")
	@SequenceGenerator(name = "PUF_QHP_SEQ", sequenceName = "PUF_QHP_SEQ", allocationSize = 1)
	private int id; // Column ID 0

	@Column(name = "STATE")
	private String state; // Column ID 1

	@Column(name = "COUNTY")
	private String county; // Column ID 2

	@Column(name = "METAL_LEVEL")
	private String metalLevel; // Column ID 3

	@Column(name = "ISSUER_NAME")
	private String issuerName; // Column ID 4

	@Column(name = "PLAN_ID_STANDARD_COMPONENT")
	private String planIdStandardComponent; // Column ID 5

	@Column(name = "PLAN_MARKETING_NAME")
	private String planMarketingName; // Column ID 6

	@Column(name = "PLAN_TYPE")
	private String planType; // Column ID 7

	@Column(name = "RATING_AREA")
	private String ratingArea; // Column ID 8

	@Column(name = "CHILD_ONLY_OFFERING")
	private String childOnlyOffering; // Column ID 9

	@Column(name = "SOURCE")
	private String source; // Column ID 10

	@Column(name = "CS_PHONE_NUMBER_LOCAL")
	private String csPhoneNumberLocal; // Column ID 11

	@Column(name = "CS_PHONE_NUMBER_TOLL_FREE")
	private String csPhoneNumberTollFree; // Column ID 12

	@Column(name = "CS_PHONE_NUMBER_TTY")
	private String csPhoneNumberTty; // Column ID 13

	@Column(name = "NETWORK_URL")
	private String networkUrl; // Column ID 14

	@Column(name = "PLAN_BROCHURE_URL")
	private String planBrochureUrl; // Column ID 15

	@Column(name = "SUMMARY_OF_BENEFIT_URL")
	private String summaryOfBenefitUrl; // Column ID 16

	@Column(name = "DRUG_FORMULAR_URL")
	private String drugFormularUrl; // Column ID 17

	@Column(name = "ADULT_DENTAL")
	private String adultDental; // Column ID 18

	@Column(name = "CHILD_DENTAL")
	private String childDental; // Column ID 19

	@Column(name = "PREMIUM_SCENARIOS")
	private String premiumScenarios; // Column ID 20

	@Column(name = "PREMIUM_CHILD")
	private Double premiumChild; // Column ID 21

	@Column(name = "PREMIUM_A_I_AGE_21")
	private Double premiumAIAge21; // Column ID 22

	@Column(name = "PREMIUM_A_I_AGE_27")
	private Double premiumAIAge27; // Column ID 23

	@Column(name = "PREMIUM_A_I_AGE_30")
	private Double premiumAIAge30; // Column ID 24

	@Column(name = "PREMIUM_A_I_AGE_40")
	private Double premiumAIAge40; // Column ID 25

	@Column(name = "PREMIUM_A_I_AGE_50")
	private Double premiumAIAge50; // Column ID 26

	@Column(name = "PREMIUM_A_I_AGE_60")
	private Double premiumAIAge60; // Column ID 27

	@Column(name = "PREMIUM_COUPLE_21")
	private Double premiumCouple21; // Column ID 28

	@Column(name = "PREMIUM_COUPLE_30")
	private Double premiumCouple30; // Column ID 29

	@Column(name = "PREMIUM_COUPLE_40")
	private Double premiumCouple40; // Column ID 30

	@Column(name = "PREMIUM_COUPLE_50")
	private Double premiumCouple50; // Column ID 31

	@Column(name = "PREMIUM_COUPLE_60")
	private Double premiumCouple60; // Column ID 32

	@Column(name = "COUPLE_1_CHILD_AGE_21")
	private Double couple1ChildAge21; // Column ID 33

	@Column(name = "COUPLE_1_CHILD_AGE_30")
	private Double couple1ChildAge30; // Column ID 34

	@Column(name = "COUPLE_1_CHILD_AGE_40")
	private Double couple1ChildAge40; // Column ID 35

	@Column(name = "COUPLE_1_CHILD_AGE_50")
	private Double couple1ChildAge50; // Column ID 36

	@Column(name = "COUPLE_2_CHILDREN_AGE_21")
	private Double couple2ChildrenAge21; // Column ID 37

	@Column(name = "COUPLE_2_CHILDREN_AGE_30")
	private Double couple2ChildrenAge30; // Column ID 38

	@Column(name = "COUPLE_2_CHILDREN_AGE_40")
	private Double couple2ChildrenAge40; // Column ID 39

	@Column(name = "COUPLE_2_CHILDREN_AGE_50")
	private Double couple2ChildrenAge50; // Column ID 40

	@Column(name = "COUPLE_3CHILDREN_AGE_21")
	private Double couple3childrenAge21; // Column ID 41

	@Column(name = "COUPLE_3CHILDREN_AGE_30")
	private Double couple3childrenAge30; // Column ID 42

	@Column(name = "COUPLE_3CHILDREN_AGE_40")
	private Double couple3childrenAge40; // Column ID 43

	@Column(name = "COUPLE_3CHILDREN_AGE_50")
	private Double couple3childrenAge50; // Column ID 44

	@Column(name = "INDIVIDUAL_1_CHILD_AGE_21")
	private Double individual1ChildAge21; // Column ID 45

	@Column(name = "INDIVIDUAL_1_CHILD_AGE_30")
	private Double individual1ChildAge30; // Column ID 46

	@Column(name = "INDIVIDUAL_1_CHILD_AGE_40")
	private Double individual1ChildAge40; // Column ID 47

	@Column(name = "INDIVIDUAL_1_CHILD_AGE_50")
	private Double individual1ChildAge50; // Column ID 48

	@Column(name = "INDIVIDUAL_2_CHILDREN_AGE_21")
	private Double individual2ChildrenAge21; // Column ID 49

	@Column(name = "INDIVIDUAL_2_CHILDREN_AGE_30")
	private Double individual2ChildrenAge30; // Column ID 50

	@Column(name = "INDIVIDUAL_2_CHILDREN_AGE_40")
	private Double individual2ChildrenAge40; // Column ID 51

	@Column(name = "INDIVIDUAL_2_CHILDREN_AGE_50")
	private Double individual2ChildrenAge50; // Column ID 52

	@Column(name = "INDIVIDUAL_3CHILDREN_AGE_21")
	private Double individual3childrenAge21; // Column ID 53

	@Column(name = "INDIVIDUAL_3CHILDREN_AGE_30")
	private Double individual3childrenAge30; // Column ID 54

	@Column(name = "INDIVIDUAL_3CHILDREN_AGE_40")
	private Double individual3childrenAge40; // Column ID 55

	@Column(name = "INDIVIDUAL_3CHILDREN_AGE_50")
	private Double individual3childrenAge50; // Column ID 56

	@Column(name = "STANDARD_PLAN_COST_SHARING")
	private String standardPlanCostSharing; // Column ID 57

	@Column(name = "MEDICAL_DEDUCTIBLE_I_STANDARD")
	private String medicalDeductibleIStandard; // Column ID 58

	@Column(name = "DRUG_DEDUCTIBLE_I_STANDARD")
	private String drugDeductibleIStandard; // Column ID 59

	@Column(name = "MEDICAL_DEDUCTIBLE_F_STANDARD")
	private String medicalDeductibleFStandard; // Column ID 60

	@Column(name = "DRUG_DEDUCTIBLE_F_STANDARD")
	private String drugDeductibleFStandard; // Column ID 61

	@Column(name = "MEDICAL_MAXOUTFPCKT_I_STANDARD")
	private String medicalMaxoutfpcktIStandard; // Column ID 62

	@Column(name = "DRUG_MAXOUTFPCKT_I_STANDARD")
	private String drugMaxoutfpcktIStandard; // Column ID 63

	@Column(name = "MEDICAL_MAXOUTFPCKT_F_STANDARD")
	private String medicalMaxoutfpcktFStandard; // Column ID 64

	@Column(name = "DRUG_MAXOUTFPCKT_F_STANDARD")
	private String drugMaxoutfpcktFStandard; // Column ID 65

	@Column(name = "PRIMARY_PHYSICIAN_STANDARD")
	private String primaryPhysicianStandard; // Column ID 66

	@Column(name = "SPECIALIST_STANDARD")
	private String specialistStandard; // Column ID 67

	@Column(name = "EMERGENCY_ROOM_STANDARD")
	private String emergencyRoomStandard; // Column ID 68

	@Column(name = "INPATIENT_FACILITY_STANDARD")
	private String inpatientFacilityStandard; // Column ID 69

	@Column(name = "INPATIENT_PHYSICIAN_STANDARD")
	private String inpatientPhysicianStandard; // Column ID 70

	@Column(name = "GENERICDRUGS_STANDARD")
	private String genericdrugsStandard; // Column ID 71

	@Column(name = "PREFERRED_BRAND_DRUGS_STANDARD")
	private String preferredBrandDrugsStandard; // Column ID 72

	@Column(name = "NONPREFRRDBRANDDRUGS_STANDARD")
	private String nonprefrrdbranddrugsStandard; // Column ID 73

	@Column(name = "SPECIALTY_DRUGS_STANDARD")
	private String specialtyDrugsStandard; // Column ID 74

	@Column(name = "T73PCT_SILVERPLANCOSTSHARING")
	private String t73pctSilverplancostsharing; // Column ID 75

	@Column(name = "MEDICAL_DEDUCTIBLE_I_73_PCT")
	private String medicalDeductibleI73Pct; // Column ID 76

	@Column(name = "DRUG_DEDUCTIBLE_I_73_PCT")
	private String drugDeductibleI73Pct; // Column ID 77

	@Column(name = "MEDICAL_DEDUCTIBLE_F_73_PCT")
	private String medicalDeductibleF73Pct; // Column ID 78

	@Column(name = "DRUG_DEDUCTIBLE_F_73_PCT")
	private String drugDeductibleF73Pct; // Column ID 79

	@Column(name = "MEDICAL_MAXOUTFPCKT_I_73_PCT")
	private String medicalMaxoutfpcktI73Pct; // Column ID 80

	@Column(name = "DRUG_MAXOUTFPCKT_I_73_PCT")
	private String drugMaxoutfpcktI73Pct; // Column ID 81

	@Column(name = "MEDICAL_MAXOUTFPCKT_F_73_PCT")
	private String medicalMaxoutfpcktF73Pct; // Column ID 82

	@Column(name = "DRUG_MAXOUTFPCKT_F_73_PCT")
	private String drugMaxoutfpcktF73Pct; // Column ID 83

	@Column(name = "PRIMARY_PHYSICIAN_73_PCT")
	private String primaryPhysician73Pct; // Column ID 84

	@Column(name = "SPECIALIST_73_PCT")
	private String specialist73Pct; // Column ID 85

	@Column(name = "EMERGENCY_ROOM_73_PCT")
	private String emergencyRoom73Pct; // Column ID 86

	@Column(name = "INPATIENT_FACILITY_73_PCT")
	private String inpatientFacility73Pct; // Column ID 87

	@Column(name = "INPATIENT_PHYSICIAN_73_PCT")
	private String inpatientPhysician73Pct; // Column ID 88

	@Column(name = "GENERIC_DRUGS_73_PCT")
	private String genericDrugs73Pct; // Column ID 89

	@Column(name = "PREFERREDBRANDDRUGS_73_PCT")
	private String preferredbranddrugs73Pct; // Column ID 90

	@Column(name = "NONPREFRRDBRANDDRUGS_73_PCT")
	private String nonprefrrdbranddrugs73Pct; // Column ID 91

	@Column(name = "SPECIALTY_DRUGS_73_PCT")
	private String specialtyDrugs73Pct; // Column ID 92

	@Column(name = "T87PCT_SILVERPLANCOSTSHARING")
	private String t87pctSilverplancostsharing; // Column ID 93

	@Column(name = "MEDICAL_DEDUCTIBLE_I_87_PCT")
	private String medicalDeductibleI87Pct; // Column ID 94

	@Column(name = "DRUG_DEDUCTIBLE_I_87_PCT")
	private String drugDeductibleI87Pct; // Column ID 95

	@Column(name = "MEDICAL_DEDUCTIBLE_F_87_PCT")
	private String medicalDeductibleF87Pct; // Column ID 96

	@Column(name = "DRUG_DEDUCTIBLE_F_87_PCT")
	private String drugDeductibleF87Pct; // Column ID 97

	@Column(name = "MEDICAL_MAXOUTFPCKT_I_87_PCT")
	private String medicalMaxoutfpcktI87Pct; // Column ID 98

	@Column(name = "DRUG_MAXOUTFPCKT_I_87_PCT")
	private String drugMaxoutfpcktI87Pct; // Column ID 99

	@Column(name = "MEDICAL_MAXOUTFPCKT_F_87_PCT")
	private String medicalMaxoutfpcktF87Pct; // Column ID 100

	@Column(name = "DRUG_MAXOUTFPCKT_F_87_PCT")
	private String drugMaxoutfpcktF87Pct; // Column ID 101

	@Column(name = "PRIMARY_PHYSICIAN_87_PCT")
	private String primaryPhysician87Pct; // Column ID 102

	@Column(name = "SPECIALIST_87_PCT")
	private String specialist87Pct; // Column ID 103

	@Column(name = "EMERGENCY_ROOM_87_PCT")
	private String emergencyRoom87Pct; // Column ID 104

	@Column(name = "INPATIENT_FACILITY_87_PCT")
	private String inpatientFacility87Pct; // Column ID 105

	@Column(name = "INPATIENT_PHYSICIAN_87_PCT")
	private String inpatientPhysician87Pct; // Column ID 106

	@Column(name = "GENERIC_DRUGS_87_PCT")
	private String genericDrugs87Pct; // Column ID 107

	@Column(name = "PREFERREDBRANDDRUGS_87_PCT")
	private String preferredbranddrugs87Pct; // Column ID 108

	@Column(name = "NONPREFRRDBRANDDRUGS_87_PCT")
	private String nonprefrrdbranddrugs87Pct; // Column ID 109

	@Column(name = "SPECIALTY_DRUGS_87_PCT")
	private String specialtyDrugs87Pct; // Column ID 110

	@Column(name = "T94PCT_SILVERPLANCOSTSHARING")
	private String t94pctSilverplancostsharing; // Column ID 111

	@Column(name = "MEDICAL_DEDUCTIBLE_I_94_PCT")
	private String medicalDeductibleI94Pct; // Column ID 112

	@Column(name = "DRUG_DEDUCTIBLE_I_94_PCT")
	private String drugDeductibleI94Pct; // Column ID 113

	@Column(name = "MEDICAL_DEDUCTIBLE_F_94_PCT")
	private String medicalDeductibleF94Pct; // Column ID 114

	@Column(name = "DRUG_DEDUCTIBLE_F_94_PCT")
	private String drugDeductibleF94Pct; // Column ID 115

	@Column(name = "MEDICAL_MAXOUTFPCKT_I_94_PCT")
	private String medicalMaxoutfpcktI94Pct; // Column ID 116

	@Column(name = "DRUG_MAXOUTFPCKT_I_94_PCT")
	private String drugMaxoutfpcktI94Pct; // Column ID 117

	@Column(name = "MEDICAL_MAXOUTFPCKT_F_94_PCT")
	private String medicalMaxoutfpcktF94Pct; // Column ID 118

	@Column(name = "DRUG_MAXOUTFPCKT_F_94_PCT")
	private String drugMaxoutfpcktF94Pct; // Column ID 119

	@Column(name = "PRIMARY_PHYSICIAN_94_PCT")
	private String primaryPhysician94Pct; // Column ID 120

	@Column(name = "SPECIALIST_94_PCT")
	private String specialist94Pct; // Column ID 121

	@Column(name = "EMERGENCY_ROOM_94_PCT")
	private String emergencyRoom94Pct; // Column ID 122

	@Column(name = "INPATIENT_FACILITY_94_PCT")
	private String inpatientFacility94Pct; // Column ID 123

	@Column(name = "INPATIENT_PHYSICIAN_94_PCT")
	private String inpatientPhysician94Pct; // Column ID 124

	@Column(name = "GENERIC_DRUGS_94_PCT")
	private String genericDrugs94Pct; // Column ID 125

	@Column(name = "PREFERREDBRANDDRUGS_94_PCT")
	private String preferredbranddrugs94Pct; // Column ID 126

	@Column(name = "NONPREFRRDBRANDDRUGS_94_PCT")
	private String nonprefrrdbranddrugs94Pct; // Column ID 127

	@Column(name = "SPECIALTY_DRUGS_94_PCT")
	private String specialtyDrugs94Pct; // Column ID 128

	@Column(name = "IS_DELETE")
	//@ Enumerated(EnumType.STRING)
	private String isDelete; // Column ID 129

	@Column(name = "JOB_STATUS")
	@Enumerated(EnumType.STRING)
	private JOB_STATUS jobStatus; // Column ID 130

	@Column(name = "FTP_STATUS")
	@Enumerated(EnumType.STRING)
	private FTP_STATUS ftpStatus; // Column ID 131

	@Column(name = "ERROR_CODE")
	private Integer errorCode; // Column ID 132

	@Column(name = "ERROR_MESSAGE")
	private String errorMessage; // Column ID 133

	@Column(name = "DURATION")
	private Integer duration; // Column ID 134

	@Column(name = "SERFF_ERROR_CODE")
	private Integer serffErrorCode; // Column ID 135

	@Column(name = "SERFF_ERROR_MSG")
	private String serffErrorMsg; // Column ID 136

	@Column(name = "CREATION_TIMESTAMP")
	private Date creationTimestamp; // Column ID 137

	@Column(name = "CREATED_BY")
	private Integer createdBy; // Column ID 138

	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp; // Column ID 139

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy; // Column ID 140

	@Column(name = "DELETED_TIMESTAMP")
	private Date deletedTimestamp; // Column ID 141

	@Column(name = "DELETED_BY")
	private Integer deletedBy; // Column ID 142

	@Column(name = "PRIORITY")
	private Integer priority; // Column ID 143

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getMetalLevel() {
		return metalLevel;
	}

	public void setMetalLevel(String metalLevel) {
		this.metalLevel = metalLevel;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPlanIdStandardComponent() {
		return planIdStandardComponent;
	}

	public void setPlanIdStandardComponent(String planIdStandardComponent) {
		this.planIdStandardComponent = planIdStandardComponent;
	}

	public String getPlanMarketingName() {
		return planMarketingName;
	}

	public void setPlanMarketingName(String planMarketingName) {
		this.planMarketingName = planMarketingName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public String getChildOnlyOffering() {
		return childOnlyOffering;
	}

	public void setChildOnlyOffering(String childOnlyOffering) {
		this.childOnlyOffering = childOnlyOffering;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCsPhoneNumberLocal() {
		return csPhoneNumberLocal;
	}

	public void setCsPhoneNumberLocal(String csPhoneNumberLocal) {
		this.csPhoneNumberLocal = csPhoneNumberLocal;
	}

	public String getCsPhoneNumberTollFree() {
		return csPhoneNumberTollFree;
	}

	public void setCsPhoneNumberTollFree(String csPhoneNumberTollFree) {
		this.csPhoneNumberTollFree = csPhoneNumberTollFree;
	}

	public String getCsPhoneNumberTty() {
		return csPhoneNumberTty;
	}

	public void setCsPhoneNumberTty(String csPhoneNumberTty) {
		this.csPhoneNumberTty = csPhoneNumberTty;
	}

	public String getNetworkUrl() {
		return networkUrl;
	}

	public void setNetworkUrl(String networkUrl) {
		this.networkUrl = networkUrl;
	}

	public String getPlanBrochureUrl() {
		return planBrochureUrl;
	}

	public void setPlanBrochureUrl(String planBrochureUrl) {
		this.planBrochureUrl = planBrochureUrl;
	}

	public String getSummaryOfBenefitUrl() {
		return summaryOfBenefitUrl;
	}

	public void setSummaryOfBenefitUrl(String summaryOfBenefitUrl) {
		this.summaryOfBenefitUrl = summaryOfBenefitUrl;
	}

	public String getDrugFormularUrl() {
		return drugFormularUrl;
	}

	public void setDrugFormularUrl(String drugFormularUrl) {
		this.drugFormularUrl = drugFormularUrl;
	}

	public String getAdultDental() {
		return adultDental;
	}

	public void setAdultDental(String adultDental) {
		this.adultDental = adultDental;
	}

	public String getChildDental() {
		return childDental;
	}

	public void setChildDental(String childDental) {
		this.childDental = childDental;
	}

	public String getPremiumScenarios() {
		return premiumScenarios;
	}

	public void setPremiumScenarios(String premiumScenarios) {
		this.premiumScenarios = premiumScenarios;
	}

	public Double getPremiumChild() {
		return premiumChild;
	}

	public void setPremiumChild(Double premiumChild) {
		this.premiumChild = premiumChild;
	}

	public Double getPremiumAIAge21() {
		return premiumAIAge21;
	}

	public void setPremiumAIAge21(Double premiumAIAge21) {
		this.premiumAIAge21 = premiumAIAge21;
	}

	public Double getPremiumAIAge27() {
		return premiumAIAge27;
	}

	public void setPremiumAIAge27(Double premiumAIAge27) {
		this.premiumAIAge27 = premiumAIAge27;
	}

	public Double getPremiumAIAge30() {
		return premiumAIAge30;
	}

	public void setPremiumAIAge30(Double premiumAIAge30) {
		this.premiumAIAge30 = premiumAIAge30;
	}

	public Double getPremiumAIAge40() {
		return premiumAIAge40;
	}

	public void setPremiumAIAge40(Double premiumAIAge40) {
		this.premiumAIAge40 = premiumAIAge40;
	}

	public Double getPremiumAIAge50() {
		return premiumAIAge50;
	}

	public void setPremiumAIAge50(Double premiumAIAge50) {
		this.premiumAIAge50 = premiumAIAge50;
	}

	public Double getPremiumAIAge60() {
		return premiumAIAge60;
	}

	public void setPremiumAIAge60(Double premiumAIAge60) {
		this.premiumAIAge60 = premiumAIAge60;
	}

	public Double getPremiumCouple21() {
		return premiumCouple21;
	}

	public void setPremiumCouple21(Double premiumCouple21) {
		this.premiumCouple21 = premiumCouple21;
	}

	public Double getPremiumCouple30() {
		return premiumCouple30;
	}

	public void setPremiumCouple30(Double premiumCouple30) {
		this.premiumCouple30 = premiumCouple30;
	}

	public Double getPremiumCouple40() {
		return premiumCouple40;
	}

	public void setPremiumCouple40(Double premiumCouple40) {
		this.premiumCouple40 = premiumCouple40;
	}

	public Double getPremiumCouple50() {
		return premiumCouple50;
	}

	public void setPremiumCouple50(Double premiumCouple50) {
		this.premiumCouple50 = premiumCouple50;
	}

	public Double getPremiumCouple60() {
		return premiumCouple60;
	}

	public void setPremiumCouple60(Double premiumCouple60) {
		this.premiumCouple60 = premiumCouple60;
	}

	public Double getCouple1ChildAge21() {
		return couple1ChildAge21;
	}

	public void setCouple1ChildAge21(Double couple1ChildAge21) {
		this.couple1ChildAge21 = couple1ChildAge21;
	}

	public Double getCouple1ChildAge30() {
		return couple1ChildAge30;
	}

	public void setCouple1ChildAge30(Double couple1ChildAge30) {
		this.couple1ChildAge30 = couple1ChildAge30;
	}

	public Double getCouple1ChildAge40() {
		return couple1ChildAge40;
	}

	public void setCouple1ChildAge40(Double couple1ChildAge40) {
		this.couple1ChildAge40 = couple1ChildAge40;
	}

	public Double getCouple1ChildAge50() {
		return couple1ChildAge50;
	}

	public void setCouple1ChildAge50(Double couple1ChildAge50) {
		this.couple1ChildAge50 = couple1ChildAge50;
	}

	public Double getCouple2ChildrenAge21() {
		return couple2ChildrenAge21;
	}

	public void setCouple2ChildrenAge21(Double couple2ChildrenAge21) {
		this.couple2ChildrenAge21 = couple2ChildrenAge21;
	}

	public Double getCouple2ChildrenAge30() {
		return couple2ChildrenAge30;
	}

	public void setCouple2ChildrenAge30(Double couple2ChildrenAge30) {
		this.couple2ChildrenAge30 = couple2ChildrenAge30;
	}

	public Double getCouple2ChildrenAge40() {
		return couple2ChildrenAge40;
	}

	public void setCouple2ChildrenAge40(Double couple2ChildrenAge40) {
		this.couple2ChildrenAge40 = couple2ChildrenAge40;
	}

	public Double getCouple2ChildrenAge50() {
		return couple2ChildrenAge50;
	}

	public void setCouple2ChildrenAge50(Double couple2ChildrenAge50) {
		this.couple2ChildrenAge50 = couple2ChildrenAge50;
	}

	public Double getCouple3childrenAge21() {
		return couple3childrenAge21;
	}

	public void setCouple3childrenAge21(Double couple3childrenAge21) {
		this.couple3childrenAge21 = couple3childrenAge21;
	}

	public Double getCouple3childrenAge30() {
		return couple3childrenAge30;
	}

	public void setCouple3childrenAge30(Double couple3childrenAge30) {
		this.couple3childrenAge30 = couple3childrenAge30;
	}

	public Double getCouple3childrenAge40() {
		return couple3childrenAge40;
	}

	public void setCouple3childrenAge40(Double couple3childrenAge40) {
		this.couple3childrenAge40 = couple3childrenAge40;
	}

	public Double getCouple3childrenAge50() {
		return couple3childrenAge50;
	}

	public void setCouple3childrenAge50(Double couple3childrenAge50) {
		this.couple3childrenAge50 = couple3childrenAge50;
	}

	public Double getIndividual1ChildAge21() {
		return individual1ChildAge21;
	}

	public void setIndividual1ChildAge21(Double individual1ChildAge21) {
		this.individual1ChildAge21 = individual1ChildAge21;
	}

	public Double getIndividual1ChildAge30() {
		return individual1ChildAge30;
	}

	public void setIndividual1ChildAge30(Double individual1ChildAge30) {
		this.individual1ChildAge30 = individual1ChildAge30;
	}

	public Double getIndividual1ChildAge40() {
		return individual1ChildAge40;
	}

	public void setIndividual1ChildAge40(Double individual1ChildAge40) {
		this.individual1ChildAge40 = individual1ChildAge40;
	}

	public Double getIndividual1ChildAge50() {
		return individual1ChildAge50;
	}

	public void setIndividual1ChildAge50(Double individual1ChildAge50) {
		this.individual1ChildAge50 = individual1ChildAge50;
	}

	public Double getIndividual2ChildrenAge21() {
		return individual2ChildrenAge21;
	}

	public void setIndividual2ChildrenAge21(Double individual2ChildrenAge21) {
		this.individual2ChildrenAge21 = individual2ChildrenAge21;
	}

	public Double getIndividual2ChildrenAge30() {
		return individual2ChildrenAge30;
	}

	public void setIndividual2ChildrenAge30(Double individual2ChildrenAge30) {
		this.individual2ChildrenAge30 = individual2ChildrenAge30;
	}

	public Double getIndividual2ChildrenAge40() {
		return individual2ChildrenAge40;
	}

	public void setIndividual2ChildrenAge40(Double individual2ChildrenAge40) {
		this.individual2ChildrenAge40 = individual2ChildrenAge40;
	}

	public Double getIndividual2ChildrenAge50() {
		return individual2ChildrenAge50;
	}

	public void setIndividual2ChildrenAge50(Double individual2ChildrenAge50) {
		this.individual2ChildrenAge50 = individual2ChildrenAge50;
	}

	public Double getIndividual3childrenAge21() {
		return individual3childrenAge21;
	}

	public void setIndividual3childrenAge21(Double individual3childrenAge21) {
		this.individual3childrenAge21 = individual3childrenAge21;
	}

	public Double getIndividual3childrenAge30() {
		return individual3childrenAge30;
	}

	public void setIndividual3childrenAge30(Double individual3childrenAge30) {
		this.individual3childrenAge30 = individual3childrenAge30;
	}

	public Double getIndividual3childrenAge40() {
		return individual3childrenAge40;
	}

	public void setIndividual3childrenAge40(Double individual3childrenAge40) {
		this.individual3childrenAge40 = individual3childrenAge40;
	}

	public Double getIndividual3childrenAge50() {
		return individual3childrenAge50;
	}

	public void setIndividual3childrenAge50(Double individual3childrenAge50) {
		this.individual3childrenAge50 = individual3childrenAge50;
	}

	public String getStandardPlanCostSharing() {
		return standardPlanCostSharing;
	}

	public void setStandardPlanCostSharing(String standardPlanCostSharing) {
		this.standardPlanCostSharing = standardPlanCostSharing;
	}

	public String getMedicalDeductibleIStandard() {
		return medicalDeductibleIStandard;
	}

	public void setMedicalDeductibleIStandard(String medicalDeductibleIStandard) {
		this.medicalDeductibleIStandard = medicalDeductibleIStandard;
	}

	public String getDrugDeductibleIStandard() {
		return drugDeductibleIStandard;
	}

	public void setDrugDeductibleIStandard(String drugDeductibleIStandard) {
		this.drugDeductibleIStandard = drugDeductibleIStandard;
	}

	public String getMedicalDeductibleFStandard() {
		return medicalDeductibleFStandard;
	}

	public void setMedicalDeductibleFStandard(String medicalDeductibleFStandard) {
		this.medicalDeductibleFStandard = medicalDeductibleFStandard;
	}

	public String getDrugDeductibleFStandard() {
		return drugDeductibleFStandard;
	}

	public void setDrugDeductibleFStandard(String drugDeductibleFStandard) {
		this.drugDeductibleFStandard = drugDeductibleFStandard;
	}

	public String getMedicalMaxoutfpcktIStandard() {
		return medicalMaxoutfpcktIStandard;
	}

	public void setMedicalMaxoutfpcktIStandard(
			String medicalMaxoutfpcktIStandard) {
		this.medicalMaxoutfpcktIStandard = medicalMaxoutfpcktIStandard;
	}

	public String getDrugMaxoutfpcktIStandard() {
		return drugMaxoutfpcktIStandard;
	}

	public void setDrugMaxoutfpcktIStandard(String drugMaxoutfpcktIStandard) {
		this.drugMaxoutfpcktIStandard = drugMaxoutfpcktIStandard;
	}

	public String getMedicalMaxoutfpcktFStandard() {
		return medicalMaxoutfpcktFStandard;
	}

	public void setMedicalMaxoutfpcktFStandard(
			String medicalMaxoutfpcktFStandard) {
		this.medicalMaxoutfpcktFStandard = medicalMaxoutfpcktFStandard;
	}

	public String getDrugMaxoutfpcktFStandard() {
		return drugMaxoutfpcktFStandard;
	}

	public void setDrugMaxoutfpcktFStandard(String drugMaxoutfpcktFStandard) {
		this.drugMaxoutfpcktFStandard = drugMaxoutfpcktFStandard;
	}

	public String getPrimaryPhysicianStandard() {
		return primaryPhysicianStandard;
	}

	public void setPrimaryPhysicianStandard(String primaryPhysicianStandard) {
		this.primaryPhysicianStandard = primaryPhysicianStandard;
	}

	public String getSpecialistStandard() {
		return specialistStandard;
	}

	public void setSpecialistStandard(String specialistStandard) {
		this.specialistStandard = specialistStandard;
	}

	public String getEmergencyRoomStandard() {
		return emergencyRoomStandard;
	}

	public void setEmergencyRoomStandard(String emergencyRoomStandard) {
		this.emergencyRoomStandard = emergencyRoomStandard;
	}

	public String getInpatientFacilityStandard() {
		return inpatientFacilityStandard;
	}

	public void setInpatientFacilityStandard(String inpatientFacilityStandard) {
		this.inpatientFacilityStandard = inpatientFacilityStandard;
	}

	public String getInpatientPhysicianStandard() {
		return inpatientPhysicianStandard;
	}

	public void setInpatientPhysicianStandard(String inpatientPhysicianStandard) {
		this.inpatientPhysicianStandard = inpatientPhysicianStandard;
	}

	public String getGenericdrugsStandard() {
		return genericdrugsStandard;
	}

	public void setGenericdrugsStandard(String genericdrugsStandard) {
		this.genericdrugsStandard = genericdrugsStandard;
	}

	public String getPreferredBrandDrugsStandard() {
		return preferredBrandDrugsStandard;
	}

	public void setPreferredBrandDrugsStandard(
			String preferredBrandDrugsStandard) {
		this.preferredBrandDrugsStandard = preferredBrandDrugsStandard;
	}

	public String getNonprefrrdbranddrugsStandard() {
		return nonprefrrdbranddrugsStandard;
	}

	public void setNonprefrrdbranddrugsStandard(
			String nonprefrrdbranddrugsStandard) {
		this.nonprefrrdbranddrugsStandard = nonprefrrdbranddrugsStandard;
	}

	public String getSpecialtyDrugsStandard() {
		return specialtyDrugsStandard;
	}

	public void setSpecialtyDrugsStandard(String specialtyDrugsStandard) {
		this.specialtyDrugsStandard = specialtyDrugsStandard;
	}

	public String getT73pctSilverplancostsharing() {
		return t73pctSilverplancostsharing;
	}

	public void setT73pctSilverplancostsharing(
			String t73pctSilverplancostsharing) {
		this.t73pctSilverplancostsharing = t73pctSilverplancostsharing;
	}

	public String getMedicalDeductibleI73Pct() {
		return medicalDeductibleI73Pct;
	}

	public void setMedicalDeductibleI73Pct(String medicalDeductibleI73Pct) {
		this.medicalDeductibleI73Pct = medicalDeductibleI73Pct;
	}

	public String getDrugDeductibleI73Pct() {
		return drugDeductibleI73Pct;
	}

	public void setDrugDeductibleI73Pct(String drugDeductibleI73Pct) {
		this.drugDeductibleI73Pct = drugDeductibleI73Pct;
	}

	public String getMedicalDeductibleF73Pct() {
		return medicalDeductibleF73Pct;
	}

	public void setMedicalDeductibleF73Pct(String medicalDeductibleF73Pct) {
		this.medicalDeductibleF73Pct = medicalDeductibleF73Pct;
	}

	public String getDrugDeductibleF73Pct() {
		return drugDeductibleF73Pct;
	}

	public void setDrugDeductibleF73Pct(String drugDeductibleF73Pct) {
		this.drugDeductibleF73Pct = drugDeductibleF73Pct;
	}

	public String getMedicalMaxoutfpcktI73Pct() {
		return medicalMaxoutfpcktI73Pct;
	}

	public void setMedicalMaxoutfpcktI73Pct(String medicalMaxoutfpcktI73Pct) {
		this.medicalMaxoutfpcktI73Pct = medicalMaxoutfpcktI73Pct;
	}

	public String getDrugMaxoutfpcktI73Pct() {
		return drugMaxoutfpcktI73Pct;
	}

	public void setDrugMaxoutfpcktI73Pct(String drugMaxoutfpcktI73Pct) {
		this.drugMaxoutfpcktI73Pct = drugMaxoutfpcktI73Pct;
	}

	public String getMedicalMaxoutfpcktF73Pct() {
		return medicalMaxoutfpcktF73Pct;
	}

	public void setMedicalMaxoutfpcktF73Pct(String medicalMaxoutfpcktF73Pct) {
		this.medicalMaxoutfpcktF73Pct = medicalMaxoutfpcktF73Pct;
	}

	public String getDrugMaxoutfpcktF73Pct() {
		return drugMaxoutfpcktF73Pct;
	}

	public void setDrugMaxoutfpcktF73Pct(String drugMaxoutfpcktF73Pct) {
		this.drugMaxoutfpcktF73Pct = drugMaxoutfpcktF73Pct;
	}

	public String getPrimaryPhysician73Pct() {
		return primaryPhysician73Pct;
	}

	public void setPrimaryPhysician73Pct(String primaryPhysician73Pct) {
		this.primaryPhysician73Pct = primaryPhysician73Pct;
	}

	public String getSpecialist73Pct() {
		return specialist73Pct;
	}

	public void setSpecialist73Pct(String specialist73Pct) {
		this.specialist73Pct = specialist73Pct;
	}

	public String getEmergencyRoom73Pct() {
		return emergencyRoom73Pct;
	}

	public void setEmergencyRoom73Pct(String emergencyRoom73Pct) {
		this.emergencyRoom73Pct = emergencyRoom73Pct;
	}

	public String getInpatientFacility73Pct() {
		return inpatientFacility73Pct;
	}

	public void setInpatientFacility73Pct(String inpatientFacility73Pct) {
		this.inpatientFacility73Pct = inpatientFacility73Pct;
	}

	public String getInpatientPhysician73Pct() {
		return inpatientPhysician73Pct;
	}

	public void setInpatientPhysician73Pct(String inpatientPhysician73Pct) {
		this.inpatientPhysician73Pct = inpatientPhysician73Pct;
	}

	public String getGenericDrugs73Pct() {
		return genericDrugs73Pct;
	}

	public void setGenericDrugs73Pct(String genericDrugs73Pct) {
		this.genericDrugs73Pct = genericDrugs73Pct;
	}

	public String getPreferredbranddrugs73Pct() {
		return preferredbranddrugs73Pct;
	}

	public void setPreferredbranddrugs73Pct(String preferredbranddrugs73Pct) {
		this.preferredbranddrugs73Pct = preferredbranddrugs73Pct;
	}

	public String getNonprefrrdbranddrugs73Pct() {
		return nonprefrrdbranddrugs73Pct;
	}

	public void setNonprefrrdbranddrugs73Pct(String nonprefrrdbranddrugs73Pct) {
		this.nonprefrrdbranddrugs73Pct = nonprefrrdbranddrugs73Pct;
	}

	public String getSpecialtyDrugs73Pct() {
		return specialtyDrugs73Pct;
	}

	public void setSpecialtyDrugs73Pct(String specialtyDrugs73Pct) {
		this.specialtyDrugs73Pct = specialtyDrugs73Pct;
	}

	public String getT87pctSilverplancostsharing() {
		return t87pctSilverplancostsharing;
	}

	public void setT87pctSilverplancostsharing(
			String t87pctSilverplancostsharing) {
		this.t87pctSilverplancostsharing = t87pctSilverplancostsharing;
	}

	public String getMedicalDeductibleI87Pct() {
		return medicalDeductibleI87Pct;
	}

	public void setMedicalDeductibleI87Pct(String medicalDeductibleI87Pct) {
		this.medicalDeductibleI87Pct = medicalDeductibleI87Pct;
	}

	public String getDrugDeductibleI87Pct() {
		return drugDeductibleI87Pct;
	}

	public void setDrugDeductibleI87Pct(String drugDeductibleI87Pct) {
		this.drugDeductibleI87Pct = drugDeductibleI87Pct;
	}

	public String getMedicalDeductibleF87Pct() {
		return medicalDeductibleF87Pct;
	}

	public void setMedicalDeductibleF87Pct(String medicalDeductibleF87Pct) {
		this.medicalDeductibleF87Pct = medicalDeductibleF87Pct;
	}

	public String getDrugDeductibleF87Pct() {
		return drugDeductibleF87Pct;
	}

	public void setDrugDeductibleF87Pct(String drugDeductibleF87Pct) {
		this.drugDeductibleF87Pct = drugDeductibleF87Pct;
	}

	public String getMedicalMaxoutfpcktI87Pct() {
		return medicalMaxoutfpcktI87Pct;
	}

	public void setMedicalMaxoutfpcktI87Pct(String medicalMaxoutfpcktI87Pct) {
		this.medicalMaxoutfpcktI87Pct = medicalMaxoutfpcktI87Pct;
	}

	public String getDrugMaxoutfpcktI87Pct() {
		return drugMaxoutfpcktI87Pct;
	}

	public void setDrugMaxoutfpcktI87Pct(String drugMaxoutfpcktI87Pct) {
		this.drugMaxoutfpcktI87Pct = drugMaxoutfpcktI87Pct;
	}

	public String getMedicalMaxoutfpcktF87Pct() {
		return medicalMaxoutfpcktF87Pct;
	}

	public void setMedicalMaxoutfpcktF87Pct(String medicalMaxoutfpcktF87Pct) {
		this.medicalMaxoutfpcktF87Pct = medicalMaxoutfpcktF87Pct;
	}

	public String getDrugMaxoutfpcktF87Pct() {
		return drugMaxoutfpcktF87Pct;
	}

	public void setDrugMaxoutfpcktF87Pct(String drugMaxoutfpcktF87Pct) {
		this.drugMaxoutfpcktF87Pct = drugMaxoutfpcktF87Pct;
	}

	public String getPrimaryPhysician87Pct() {
		return primaryPhysician87Pct;
	}

	public void setPrimaryPhysician87Pct(String primaryPhysician87Pct) {
		this.primaryPhysician87Pct = primaryPhysician87Pct;
	}

	public String getSpecialist87Pct() {
		return specialist87Pct;
	}

	public void setSpecialist87Pct(String specialist87Pct) {
		this.specialist87Pct = specialist87Pct;
	}

	public String getEmergencyRoom87Pct() {
		return emergencyRoom87Pct;
	}

	public void setEmergencyRoom87Pct(String emergencyRoom87Pct) {
		this.emergencyRoom87Pct = emergencyRoom87Pct;
	}

	public String getInpatientFacility87Pct() {
		return inpatientFacility87Pct;
	}

	public void setInpatientFacility87Pct(String inpatientFacility87Pct) {
		this.inpatientFacility87Pct = inpatientFacility87Pct;
	}

	public String getInpatientPhysician87Pct() {
		return inpatientPhysician87Pct;
	}

	public void setInpatientPhysician87Pct(String inpatientPhysician87Pct) {
		this.inpatientPhysician87Pct = inpatientPhysician87Pct;
	}

	public String getGenericDrugs87Pct() {
		return genericDrugs87Pct;
	}

	public void setGenericDrugs87Pct(String genericDrugs87Pct) {
		this.genericDrugs87Pct = genericDrugs87Pct;
	}

	public String getPreferredbranddrugs87Pct() {
		return preferredbranddrugs87Pct;
	}

	public void setPreferredbranddrugs87Pct(String preferredbranddrugs87Pct) {
		this.preferredbranddrugs87Pct = preferredbranddrugs87Pct;
	}

	public String getNonprefrrdbranddrugs87Pct() {
		return nonprefrrdbranddrugs87Pct;
	}

	public void setNonprefrrdbranddrugs87Pct(String nonprefrrdbranddrugs87Pct) {
		this.nonprefrrdbranddrugs87Pct = nonprefrrdbranddrugs87Pct;
	}

	public String getSpecialtyDrugs87Pct() {
		return specialtyDrugs87Pct;
	}

	public void setSpecialtyDrugs87Pct(String specialtyDrugs87Pct) {
		this.specialtyDrugs87Pct = specialtyDrugs87Pct;
	}

	public String getT94pctSilverplancostsharing() {
		return t94pctSilverplancostsharing;
	}

	public void setT94pctSilverplancostsharing(
			String t94pctSilverplancostsharing) {
		this.t94pctSilverplancostsharing = t94pctSilverplancostsharing;
	}

	public String getMedicalDeductibleI94Pct() {
		return medicalDeductibleI94Pct;
	}

	public void setMedicalDeductibleI94Pct(String medicalDeductibleI94Pct) {
		this.medicalDeductibleI94Pct = medicalDeductibleI94Pct;
	}

	public String getDrugDeductibleI94Pct() {
		return drugDeductibleI94Pct;
	}

	public void setDrugDeductibleI94Pct(String drugDeductibleI94Pct) {
		this.drugDeductibleI94Pct = drugDeductibleI94Pct;
	}

	public String getMedicalDeductibleF94Pct() {
		return medicalDeductibleF94Pct;
	}

	public void setMedicalDeductibleF94Pct(String medicalDeductibleF94Pct) {
		this.medicalDeductibleF94Pct = medicalDeductibleF94Pct;
	}

	public String getDrugDeductibleF94Pct() {
		return drugDeductibleF94Pct;
	}

	public void setDrugDeductibleF94Pct(String drugDeductibleF94Pct) {
		this.drugDeductibleF94Pct = drugDeductibleF94Pct;
	}

	public String getMedicalMaxoutfpcktI94Pct() {
		return medicalMaxoutfpcktI94Pct;
	}

	public void setMedicalMaxoutfpcktI94Pct(String medicalMaxoutfpcktI94Pct) {
		this.medicalMaxoutfpcktI94Pct = medicalMaxoutfpcktI94Pct;
	}

	public String getDrugMaxoutfpcktI94Pct() {
		return drugMaxoutfpcktI94Pct;
	}

	public void setDrugMaxoutfpcktI94Pct(String drugMaxoutfpcktI94Pct) {
		this.drugMaxoutfpcktI94Pct = drugMaxoutfpcktI94Pct;
	}

	public String getMedicalMaxoutfpcktF94Pct() {
		return medicalMaxoutfpcktF94Pct;
	}

	public void setMedicalMaxoutfpcktF94Pct(String medicalMaxoutfpcktF94Pct) {
		this.medicalMaxoutfpcktF94Pct = medicalMaxoutfpcktF94Pct;
	}

	public String getDrugMaxoutfpcktF94Pct() {
		return drugMaxoutfpcktF94Pct;
	}

	public void setDrugMaxoutfpcktF94Pct(String drugMaxoutfpcktF94Pct) {
		this.drugMaxoutfpcktF94Pct = drugMaxoutfpcktF94Pct;
	}

	public String getPrimaryPhysician94Pct() {
		return primaryPhysician94Pct;
	}

	public void setPrimaryPhysician94Pct(String primaryPhysician94Pct) {
		this.primaryPhysician94Pct = primaryPhysician94Pct;
	}

	public String getSpecialist94Pct() {
		return specialist94Pct;
	}

	public void setSpecialist94Pct(String specialist94Pct) {
		this.specialist94Pct = specialist94Pct;
	}

	public String getEmergencyRoom94Pct() {
		return emergencyRoom94Pct;
	}

	public void setEmergencyRoom94Pct(String emergencyRoom94Pct) {
		this.emergencyRoom94Pct = emergencyRoom94Pct;
	}

	public String getInpatientFacility94Pct() {
		return inpatientFacility94Pct;
	}

	public void setInpatientFacility94Pct(String inpatientFacility94Pct) {
		this.inpatientFacility94Pct = inpatientFacility94Pct;
	}

	public String getInpatientPhysician94Pct() {
		return inpatientPhysician94Pct;
	}

	public void setInpatientPhysician94Pct(String inpatientPhysician94Pct) {
		this.inpatientPhysician94Pct = inpatientPhysician94Pct;
	}

	public String getGenericDrugs94Pct() {
		return genericDrugs94Pct;
	}

	public void setGenericDrugs94Pct(String genericDrugs94Pct) {
		this.genericDrugs94Pct = genericDrugs94Pct;
	}

	public String getPreferredbranddrugs94Pct() {
		return preferredbranddrugs94Pct;
	}

	public void setPreferredbranddrugs94Pct(String preferredbranddrugs94Pct) {
		this.preferredbranddrugs94Pct = preferredbranddrugs94Pct;
	}

	public String getNonprefrrdbranddrugs94Pct() {
		return nonprefrrdbranddrugs94Pct;
	}

	public void setNonprefrrdbranddrugs94Pct(String nonprefrrdbranddrugs94Pct) {
		this.nonprefrrdbranddrugs94Pct = nonprefrrdbranddrugs94Pct;
	}

	public String getSpecialtyDrugs94Pct() {
		return specialtyDrugs94Pct;
	}

	public void setSpecialtyDrugs94Pct(String specialtyDrugs94Pct) {
		this.specialtyDrugs94Pct = specialtyDrugs94Pct;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public JOB_STATUS getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(JOB_STATUS jobStatus) {
		this.jobStatus = jobStatus;
	}

	public FTP_STATUS getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(FTP_STATUS ftpStatus) {
		this.ftpStatus = ftpStatus;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getSerffErrorCode() {
		return serffErrorCode;
	}

	public void setSerffErrorCode(Integer serffErrorCode) {
		this.serffErrorCode = serffErrorCode;
	}

	public String getSerffErrorMsg() {
		return serffErrorMsg;
	}

	public void setSerffErrorMsg(String serffErrorMsg) {
		this.serffErrorMsg = serffErrorMsg;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getDeletedTimestamp() {
		return deletedTimestamp;
	}

	public void setDeletedTimestamp(Date deletedTimestamp) {
		this.deletedTimestamp = deletedTimestamp;
	}

	public Integer getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}
}
