/**
 * 
 */
package com.getinsured.planmgmt.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * @author gorai_k
 *
 */
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GreaterThanCurrentDateValidator.class)
@Documented
public @interface GreaterThanCurrentDate {
	String message() default "{err.greaterThanCurrentDate}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
