package com.getinsured.hix.dto.consumer.ssap;

import java.math.BigDecimal;
import java.util.List;

public class AptcHistoryRequest {
	
	private BigDecimal cmrHouseholdId;
	private long coverageYear;
	private List<String> months;
	
	public BigDecimal getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(BigDecimal cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	public long getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}
	public List<String> getMonths() {
		return months;
	}
	public void setMonths(List<String> months) {
		this.months = months;
	}
}
