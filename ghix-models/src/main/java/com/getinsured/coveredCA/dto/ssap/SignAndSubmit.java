package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

public class SignAndSubmit {
	
	
	@Expose
	private String renewedPeriod;
	@Expose
	private Boolean changeMyInfoIndicator;
	@Expose
	private Boolean provideTrueAnswersIndicator;
	@Expose
	private String electronicPIN = "1234";
	@Expose
	private String eSignName;
	
	
	
	
	
	public String geteSignName() {
		return eSignName;
	}
	public void seteSignName(String eSignName) {
		this.eSignName = eSignName;
	}
	public Boolean getProvideTrueAnswersIndicator() {
		return provideTrueAnswersIndicator;
	}
	public void setProvideTrueAnswersIndicator(Boolean provideTrueAnswersIndicator) {
		this.provideTrueAnswersIndicator = provideTrueAnswersIndicator;
	}
	
	public String getRenewedPeriod() {
		return renewedPeriod;
	}
	public void setRenewedPeriod(String renewedPeriod) {
		this.renewedPeriod = renewedPeriod;
	}
	
	public Boolean getChangeMyInfoIndicator() {
		return changeMyInfoIndicator;
	}
	public void setChangeMyInfoIndicator(Boolean changeMyInfoIndicator) {
		this.changeMyInfoIndicator = changeMyInfoIndicator;
	}
	public String getElectronicPIN() {
		return electronicPIN;
	}
	public void setElectronicPIN(String electronicPIN) {
		this.electronicPIN = electronicPIN;
	}
	
	
	
}
