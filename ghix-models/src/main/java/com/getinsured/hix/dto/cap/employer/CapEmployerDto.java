package com.getinsured.hix.dto.cap.employer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.account.QuestionAnswer;

public class CapEmployerDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1175378092854033875L;
	/**
	 * 
	 */
	private String companyName;
	private String accessCode;
	private String contactFirstName;
	private String contactLastName;
	private String contactPhoneNumber;
	private String contactEmail;
	private String employerId;
	private String state;
	private String zip;
	private String updateTimeStamp;
	private String email;
	private String address;
	private String apptAddr;
	private String city;
	
	private Integer affiliateId;
	private String affiliateName;
	
	private Integer affiliateFlowId;
	private String affiliateFlowName;
	private Long affiliateFlowAccessId;
	
	private Integer numberOfEmployees;
	private Long userId;
	
	private List<QuestionAnswer> securityQuestions = new ArrayList<QuestionAnswer>();

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getEmployerId() {
		return employerId;
	}

	public void setEmployerId(String employerId) {
		this.employerId = employerId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getUpdateTimeStamp() {
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(String updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getApptAddr() {
		return apptAddr;
	}
	
	public void setApptAddr(String apptAddr) {
		this.apptAddr = apptAddr;
	}
	
	public void setAffiliateId(Integer affiliateId) {
		this.affiliateId = affiliateId;
	}
	
	public Integer getAffiliateId() {
		return affiliateId;
	}

	public Integer getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public void setNumberOfEmployees(Integer numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getAffiliateName() {
		return affiliateName;
	}

	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	
	public Long getAffiliateFlowAccessId() {
		return affiliateFlowAccessId;
	}
	
	public void setAffiliateFlowAccessId(Long affiliateFlowAccessId) {
		this.affiliateFlowAccessId = affiliateFlowAccessId;
	}

	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	public String getAffiliateFlowName() {
		return affiliateFlowName;
	}

	public void setAffiliateFlowName(String affiliateFlowName) {
		this.affiliateFlowName = affiliateFlowName;
	}

	public List<QuestionAnswer> getSecurityQuestions() {
		return securityQuestions;
	}

	public void setSecurityQuestions(List<QuestionAnswer> securityQuestions) {
		this.securityQuestions = securityQuestions;
	}
	
}
