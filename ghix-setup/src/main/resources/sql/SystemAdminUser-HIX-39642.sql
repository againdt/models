
--For more information related to this sql file refer jira: HIX-39642

--Inserting new Role SYSTEM
Insert into ROLES (ID,CREATED,DESCRIPTION,
LANDING_PAGE,NAME,UPDATED,POST_REG_URL, LABEL,
privileged, max_retry_count) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'SYSTEM',
(select landing_page from roles where name='ADMIN'),'SYSTEM',SYSTIMESTAMP, (select POST_REG_URL from roles where name='ADMIN'),'System',
(select privileged from roles where name='ADMIN'), (select max_retry_count from roles where name='ADMIN'));

--Inserting new user financesys@ghix.com
Insert into USERS (ID,COMMUNICATION_PREF,CONFIRMED,CREATED,EMAIL,FIRST_NAME,LAST_NAME,
PASSWORD,RECOVERY,TITLE,
UPDATED,USERNAME,PHONE,STATUS) 
values (USERS_SEQ.NEXTVAL,'Email',1,SYSTIMESTAMP,'financesys@ghix.com','Finance','Admin',
'$2a$10$ZlqKFKD5JAOFPu.bwn.Jnenmv2GzzakqVg0XllyY4naZTbRCNNtkK',SYS.STANDARD.TO_CHAR(SYS_GUID()),null,
SYSTIMESTAMP,'financesys@ghix.com',null,'Active');

--Inserting User_roles mapping for financesys@ghix.com user
Insert Into User_Roles(Id,Created,Updated,Role_Id,User_Id,Default_Role ) 
values 
(User_Roles_Seq.Nextval, Systimestamp, Systimestamp, 
(select id from Roles where Name = 'SYSTEM'), (select id from Users where Username = 'financesys@ghix.com'),'Y');
