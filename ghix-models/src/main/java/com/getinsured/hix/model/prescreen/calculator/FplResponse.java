package com.getinsured.hix.model.prescreen.calculator;

/**
 * This class encapsulates the response values of the FPL calculator.
 * <p>
 * FPL percentage is encapsulated in addition to the inherited fields from
 * GHIXResponse.
 * </p>
 * 
 * @author Sunil Desu
 * @since April 11 2013
 * 
 */
public class FplResponse extends GHIXResponse {
	private double fplPercentage;

	public double getFplPercentage() {
		return fplPercentage;
	}

	public void setFplPercentage(double fplPercentage) {
		this.fplPercentage = fplPercentage;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("FplResponse [Fpl Percentage=");
		sb.append(fplPercentage);
		sb.append(", Execution Duration (ms)=");
		sb.append(getExecDuration());
		sb.append(", Error Code =");
		sb.append(getErrCode());
		sb.append(", Error Message=");
		sb.append(getErrMsg());
		sb.append("]");

		return sb.toString();
	}

}
