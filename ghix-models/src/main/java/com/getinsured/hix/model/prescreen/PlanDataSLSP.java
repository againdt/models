package com.getinsured.hix.model.prescreen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.model.RatingArea;

@Entity
@Table(name="PUF_SLSP")
public class PlanDataSLSP implements Serializable{

	private static final long serialVersionUID = 4376872127315053281L;
	
	 @EmbeddedId
	 private PlanDataKey id;
	
//	@Column(name="ZIPCODE")
//	private String zipCode;
//	
//	@Column(name="FIPS")
//	private String countyCode;
//	
//	
//	@Column(name="YEAR")
//	private int year;
//	 
//	@Column(name="STATE")
//	private String state;
		
	
	@Column(name="CARRIER_COUNT")
	private int numOfIssuers;
	
	@Column(name="PLAN_COUNT")
	private int numOfQhps;
	
	@Column(name="LSP21")
	private double silverQhpCost;
	
	@Column(name="LBP21")
	private double bronzeQhpCost;
	
	@Column(name="SLSP21")
	private double secondSilverQhpCost;
	
	/**
	 * @return the numOfIssuers
	 */
	public int getNumOfIssuers() {
		return numOfIssuers;
	}

	/**
	 * @param numOfIssuers the numOfIssuers to set
	 */
	public void setNumOfIssuers(int numOfIssuers) {
		this.numOfIssuers = numOfIssuers;
	}

	/**
	 * @return the numOfQhps
	 */
	public int getNumOfQhps() {
		return numOfQhps;
	}

	/**
	 * @param numOfQhps the numOfQhps to set
	 */
	public void setNumOfQhps(int numOfQhps) {
		this.numOfQhps = numOfQhps;
	}

	
	/**
	 * @return the silverQhpCost
	 */
	public double getSilverQhpCost() {
		return silverQhpCost;
	}

//	public String getZipCode() {
//		return zipCode;
//	}
//
//	public void setZipCode(String zipCode) {
//		this.zipCode = zipCode;
//	}
//
//	public String getCountyCode() {
//		return countyCode;
//	}
//
//	public void setCountyCode(String countyCode) {
//		this.countyCode = countyCode;
//	}
//
//	public int getYear() {
//		return year;
//	}
//
//	public void setYear(int year) {
//		this.year = year;
//	}

	/**
	 * @param silverQhpCost the silverQhpCost to set
	 */
	public void setSilverQhpCost(double silverQhpCost) {
		this.silverQhpCost = silverQhpCost;
	}

	/**
	 * @return the bronzeQhpCost
	 */
	public double getBronzeQhpCost() {
		return bronzeQhpCost;
	}

	/**
	 * @param bronzeQhpCost the bronzeQhpCost to set
	 */
	public void setBronzeQhpCost(double bronzeQhpCost) {
		this.bronzeQhpCost = bronzeQhpCost;
	}

	/**
	 * @return the secondSilverQhpCost
	 */
	public double getSecondSilverQhpCost() {
		return secondSilverQhpCost;
	}

	/**
	 * @param secondSilverQhpCost the secondSilverQhpCost to set
	 */
	public void setSecondSilverQhpCost(double secondSilverQhpCost) {
		this.secondSilverQhpCost = secondSilverQhpCost;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}
	public PlanDataKey getId() {
		return id;
	}

	public void setId(PlanDataKey id) {
		this.id = id;
	}

	
}
