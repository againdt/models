package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerCommissionResponseDTO extends GhixResponseDTO {

	private String hiosIssuerId;
	private String issuerName;
	private String companyLogo;
	private List<IssuerCommissionDTO> issuerCommissionDTOList;

	public IssuerCommissionResponseDTO() {
		super();
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public List<IssuerCommissionDTO> getIssuerCommissionDTOList() {
		return issuerCommissionDTOList;
	}

	public void setIssuerCommissionDTO(IssuerCommissionDTO issuerCommissionDTO) {

		if (CollectionUtils.isEmpty(this.issuerCommissionDTOList)) {
			this.issuerCommissionDTOList = new ArrayList<IssuerCommissionDTO>();
		}
		this.issuerCommissionDTOList.add(issuerCommissionDTO);
	}
}
