/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Srikanth Nakka
 *
 */
public class Location implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String homeAddressLine1;
	private String homeAddressLine2;
	private String homeAddressCity;
	
	@NotEmpty(message = "homeAddressState cannot be empty.")
	private String homeAddressState;
	@NotEmpty(message = "homeAddressZip cannot be empty.")
	private String homeAddressZip;
	
	public String getHomeAddressLine1() {
		return homeAddressLine1;
	}
	public void setHomeAddressLine1(String homeAddressLine1) {
		this.homeAddressLine1 = homeAddressLine1;
	}
	public String getHomeAddressLine2() {
		return homeAddressLine2;
	}
	public void setHomeAddressLine2(String homeAddressLine2) {
		this.homeAddressLine2 = homeAddressLine2;
	}
	public String getHomeAddressCity() {
		return homeAddressCity;
	}
	public void setHomeAddressCity(String homeAddressCity) {
		this.homeAddressCity = homeAddressCity;
	}
	public String getHomeAddressState() {
		return homeAddressState;
	}
	public void setHomeAddressState(String homeAddressState) {
		this.homeAddressState = homeAddressState;
	}
	public String getHomeAddressZip() {
		return homeAddressZip;
	}
	public void setHomeAddressZip(String homeAddressZip) {
		this.homeAddressZip = homeAddressZip;
	}
	
	

	
}
