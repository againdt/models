/**
 * 
 */
package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * Pojo class for STM HCC Area Factor 
 * @author sharma_va
 *
 */


@Audited
@Entity
@Table(name ="PM_STM_HCC_AREA_FACTOR")
public class STMHCCPlansAreaFactor {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STM_HCC_AREA_FACTOR_SEQ")
	@SequenceGenerator(name = "STM_HCC_AREA_FACTOR_SEQ", sequenceName = "STM_HCC_AREA_FACTOR_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "state")
	private String state;
	
	@Column(name = "min_zip")
	private String minZip;
	
	@Column(name = "max_zip")
	private String maxZip;
	
	@Column(name = "area_factor_val")
	private Double areaFactorVal;
	
	@Column(name = "support_full_state")
	private String supportFullState;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp" , nullable = false)
	private Date creationTimestamp ;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp" , nullable = false)
	private Date lastUpdateTimestamp;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	@Column(name = "is_deleted")
	private String isDeleted;
	
	@Column(name = "carrier_hios_id")
	private String carrierHiosId;
	
	@Column(name = "effective_start_date")
	private Date effectiveStartDate;
	
	@Column(name = "effective_end_date")
	private Date effectiveEndDate;
	
	public STMHCCPlansAreaFactor(){
		
	}
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMinZip() {
		return minZip;
	}

	public void setMinZip(String minZip) {
		this.minZip = minZip;
	}

	public String getMaxZip() {
		return maxZip;
	}

	public void setMaxZip(String maxZip) {
		this.maxZip = maxZip;
	}

	public Double getAreaFactorVal() {
		return areaFactorVal;
	}

	public void setAreaFactorVal(Double areaFactorVal) {
		this.areaFactorVal = areaFactorVal;
	}

	public String getSupportFullState() {
		return supportFullState;
	}

	public void setSupportFullState(String supportFullState) {
		this.supportFullState = supportFullState;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCarrierHiosId() {
		return carrierHiosId;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setCarrierHiosId(String carrierHiosId) {
		this.carrierHiosId = carrierHiosId;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	
}
