package com.getinsured.coveredCA.dto.proxy;

import java.io.Serializable;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */

public class Answer implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4112878330911710309L;
	private int value;
	private String text;
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	

}
