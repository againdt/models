package com.getinsured.hix.model.entity;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.hix.model.AccountUser;

/**
 * The persistent class for table EE_ENTITIES, to save Enrollment Entity
 * information.
 * 
 */
@Audited
@Entity
@Table(name = "EE_ENTITIES")
public class EnrollmentEntity implements Serializable {

	private static final int NINE = 9;
	private static final int PRIME = 31;
	private static final long serialVersionUID = 1L;

	public enum registrationSteps {
		ENTITY_INFO, POPULATION_SERVED, PRIMARY, SUBSITE, ENTITY_CONTACT, ASSISTER, DOCUMENT_UPLOAD, PAYMENT_INFO, REGISTRATION_STATUS
	}

	public EnrollmentEntity() {
	}
	
	public interface EntityInformation extends Default{
		
	}
	public interface EntityContactInformation extends Default{
		
	}
	
	public interface EntityInformationGrant extends Default{
		
	}
	public interface EntityInformationFaxNumber extends Default{
		
	}
	public interface EntityContactPrimaryContactSecondaryPhoneNumber extends Default{
		
	}
	
	public interface EntityContactFinancialContactSecondaryPhoneNumber extends Default{
		
	}
	public interface EntityRegistrationRenewalDate extends Default{
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_ENTITIES_SEQ")
	@SequenceGenerator(name = "EE_ENTITIES_SEQ", sequenceName = "EE_ENTITIES_SEQ", allocationSize = 1)
	private int id;

	@NotEmpty(message="{label.validateEntityType}",groups=EnrollmentEntity.EntityInformation.class)
	@Column(name = "ENTITY_TYPE")
	private String entityType;
	
	@NotEmpty(message="{label.validateEntityName}",groups=EnrollmentEntity.EntityInformation.class)
	@Size(min=1,max=100,groups=EnrollmentEntity.EntityInformation.class)
	@Column(name = "ENTITY_NAME")
	private String entityName;
	
	@NotEmpty(message="{label.validateBusinessLegalName}",groups=EnrollmentEntity.EntityInformation.class)
	@Size(max=100,groups=EnrollmentEntity.EntityInformation.class)
	@Column(name = "BUSINESS_LEGAL_NAME")
	private String businessLegalName;

	//Pattern is required because @Email doesn't validate as per requirement.
	@Pattern(regexp="^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$",groups=EnrollmentEntity.EntityInformation.class)
	@NotEmpty(message="{label.validatePrimaryEmailAddress}",groups=EnrollmentEntity.EntityInformation.class)
	@Email(message="{label.validatePleaseEnterValidEmail}",groups=EnrollmentEntity.EntityInformation.class)
	@Size(max=100,groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRIMARY_EMAIL_ADDRESS")
	private String primaryEmailAddress;

	@NotEmpty(message="{label.validatePrimaryPhoneNo}",groups=EnrollmentEntity.EntityInformation.class)
	@Pattern(regexp="^[1-9]{1}[0-9]{9}$",message="{label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9}",groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRIMARY_PHONE_NUMBER")
	private String primaryPhoneNumber;

	
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="{label.validateSecondaryPhoneNo}",groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "SECONDARY_PHONE_NUMBER")
	private String secondaryPhoneNumber;
	
	@NotEmpty(groups=EnrollmentEntity.EntityInformationFaxNumber.class)
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="{label.validateFaxNumber}",groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FAX_NUMBER")
	private String faxNumber;

	@NotEmpty(message="{label.validateCommunicationPreference}",groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "COMMUNICATION_PREF")
	private String communicationPreference;

	//@NotEmpty(message="{label.validateFederalTaxID}",groups=EnrollmentEntity.EntityInformation.class)
	//@Pattern(regexp="[0-9]*",message="{label.validatePleaseEnterValidFederalID}",groups=EnrollmentEntity.EntityInformation.class)
	//@Size(min=9,max=9,groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FEDERAL_TAX_ID")
	private Long federalTaxID;

	@NotEmpty(message="{label.validateStateTaxID}",groups=EnrollmentEntity.EntityInformation.class)
	@Pattern(regexp="[0-9a-zA-Z-]*",message="{label.validatePleaseEnterValidStateTaxID}",groups=EnrollmentEntity.EntityInformation.class)
	@Size(max=12,groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "STATE_TAX_ID")
	private String stateTaxID;

	@NotEmpty(message="{label.validateORG_TYPE}",groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "ORG_TYPE")
	private String orgType;

	@NotEmpty(message="{label.validatCountiesServed}",groups=EnrollmentEntity.EntityInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "COUNTY_SERVED")
	private String countiesServed;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "RECEIVED_PAYMENTS")
	private String receivedPayments;

	@NotEmpty(groups=EnrollmentEntity.EntityInformationGrant.class)
	@Size(max=20)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "GRANT_CONTRACT")
	private String grantContractNo;

	@NotEmpty(groups=EnrollmentEntity.EntityInformationGrant.class)
	@Pattern(regexp="^[0-9]*[\\.]{0,}[0-9]*$",groups=EnrollmentEntity.EntityInformationGrant.class)
	@Size(max=15,groups=EnrollmentEntity.EntityInformationGrant.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "GRANT_AWARD_AMT")
	private String grantAwardAmount;

	@NotEmpty(groups=EnrollmentEntity.EntityRegistrationRenewalDate.class)
	@Column(name = "REGISTRATION_STATUS")
	private String registrationStatus;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "REGISTRATION_DATE")
	private Date registrationDate;

	//@Future(groups=EnrollmentEntity.EntityRegistrationRenewalDate.class)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "REGISTRATION_RENEWAL_DATE")
	private Date registrationRenewalDate;

	@NotEmpty(message="{label.validatePriContactName}",groups=EnrollmentEntity.EntityContactInformation.class)
	@Size(max=100,groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRI_CONTACT_NAME")
	private String priContactName;
	
	@NotEmpty(message="{label.validatePriContactEmailAddress}",groups=EnrollmentEntity.EntityContactInformation.class)
	@Size(max=100,groups=EnrollmentEntity.EntityContactInformation.class)
	@Pattern(regexp="^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$",groups=EnrollmentEntity.EntityContactInformation.class)
	@Email(message="{label.validatePriContactEmailAddress}")
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRI_CONTACT_EMAIL_ADDRESS")
	private String priContactEmailAddress;

	
	@NotEmpty(message="{label.validatePleaseEnterValidPhoneNumber}",groups=EnrollmentEntity.EntityContactInformation.class)
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)",message="{label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9}",groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRI_CONTACT_PRIMARY_PHONE")
	private String priContactPrimaryPhoneNumber;
	
	@NotEmpty(groups=EnrollmentEntity.EntityContactPrimaryContactSecondaryPhoneNumber.class)
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="{label.validatePleaseEnterValidSecondaryPhoneNumber}",groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRI_CONTACT_SECONDARY_PHONE")
	private String priContactSecondaryPhoneNumber;

	@NotEmpty(groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRI_COMMUNICATION_PREF")
	private String priCommunicationPreference;
	
	@NotEmpty(message="{label.validateFinContactName}",groups=EnrollmentEntity.EntityContactInformation.class)
	@Size(max=100,groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FIN_CONTACT_NAME")
	private String finContactName;

	@NotEmpty(message="{label.validateFinEmailAddress}",groups=EnrollmentEntity.EntityContactInformation.class)
	@Size(max=100,groups=EnrollmentEntity.EntityContactInformation.class)
	@Pattern(regexp="^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$",groups=EnrollmentEntity.EntityContactInformation.class)
	@Email(message="{label.validatePleaseEnterValidEmail}")
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FIN_CONTACT_EMAIL_ADDRESS")
	private String finEmailAddress;
	
	@NotEmpty(message="{label.validatePrimaryPhoneNo}",groups=EnrollmentEntity.EntityContactInformation.class)
	@Size(min=10,max=10,groups=EnrollmentEntity.EntityInformation.class)
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)",message="{label.validatePleaseEnterValidFinancialPrimaryPhoneNumber}",groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FIN_CONTACT_PRIMARY_PHONE")
	private String finPrimaryPhoneNumber;

	@NotEmpty(groups=EnrollmentEntity.EntityContactFinancialContactSecondaryPhoneNumber.class)
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="{label.validatePleaseEnterValidFinancialSecondaryPhoneNumber}",groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FIN_CONTACT_FAX_NUMBER")
	private String finFaxNumber;

	@NotEmpty(groups=EnrollmentEntity.EntityContactInformation.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "FIN_COMMUNICATION_PREF")
	private String finCommunicationPreference;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "ENTITY_NUMBER")
	private Long entityNumber;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "DOCUMENT_ID")
	private Integer documentId;

	// bi-directional one-to-one association to User
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "USER_ID")
	private AccountUser user;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;

	@Column(name = "status_change_date")
	private Date statusChangeDate;

	@Column(name = "DOCUMENT_ID_EE")
	private Integer documentIdEE;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public String getPrimaryEmailAddress() {
		return primaryEmailAddress;
	}

	public void setPrimaryEmailAddress(String primaryEmailAddress) {
		this.primaryEmailAddress = primaryEmailAddress;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getCommunicationPreference() {
		return communicationPreference;
	}

	public void setCommunicationPreference(String communicationPreference) {
		this.communicationPreference = communicationPreference;
	}

	public Long getFederalTaxID() {
		/*if(federalTaxID!=null){
			federalTaxID = appendZeros(federalTaxID, NINE);
		}*/
		return federalTaxID;
	}

	public void setFederalTaxID(Long federalTaxID) {
		this.federalTaxID = federalTaxID;
	}

	public String getStateTaxID() {
		return stateTaxID;
	}

	public void setStateTaxID(String stateTaxID) {
		this.stateTaxID = stateTaxID;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getCountiesServed() {
		return countiesServed;
	}

	public void setCountiesServed(String countiesServed) {
		this.countiesServed = countiesServed;
	}

	public String getReceivedPayments() {
		return receivedPayments;
	}

	public void setReceivedPayments(String receivedPayments) {
		this.receivedPayments = receivedPayments;
	}

	public String getGrantContractNo() {
		return grantContractNo;
	}

	public void setGrantContractNo(String grantContractNo) {
		this.grantContractNo = grantContractNo;
	}

	public String getGrantAwardAmount() {
		return grantAwardAmount;
	}

	public void setGrantAwardAmount(String grantAwardAmount) {
		this.grantAwardAmount = grantAwardAmount;
	}

	public String getRegistrationStatus() {
		return registrationStatus;
	}

	public void setRegistrationStatus(String registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getRegistrationRenewalDate() {
		return registrationRenewalDate;
	}

	public void setRegistrationRenewalDate(Date registrationRenewalDate) {
		this.registrationRenewalDate = registrationRenewalDate;
	}

	public String getPriContactName() {
		return priContactName;
	}

	public void setPriContactName(String priContactName) {
		this.priContactName = priContactName;
	}

	public String getPriContactEmailAddress() {
		return priContactEmailAddress;
	}

	public void setPriContactEmailAddress(String priContactEmailAddress) {
		this.priContactEmailAddress = priContactEmailAddress;
	}

	public String getPriContactPrimaryPhoneNumber() {
		return priContactPrimaryPhoneNumber;
	}

	public void setPriContactPrimaryPhoneNumber(String priContactPrimaryPhoneNumber) {
		this.priContactPrimaryPhoneNumber = priContactPrimaryPhoneNumber;
	}

	public String getPriContactSecondaryPhoneNumber() {
		return priContactSecondaryPhoneNumber;
	}

	public void setPriContactSecondaryPhoneNumber(String priContactSecondaryPhoneNumber) {
		this.priContactSecondaryPhoneNumber = priContactSecondaryPhoneNumber;
	}

	public String getPriCommunicationPreference() {
		return priCommunicationPreference;
	}

	public void setPriCommunicationPreference(String priCommunicationPreference) {
		this.priCommunicationPreference = priCommunicationPreference;
	}

	public String getFinContactName() {
		return finContactName;
	}

	public void setFinContactName(String finContactName) {
		this.finContactName = finContactName;
	}

	public String getFinEmailAddress() {
		return finEmailAddress;
	}

	public void setFinEmailAddress(String finEmailAddress) {
		this.finEmailAddress = finEmailAddress;
	}

	public String getFinPrimaryPhoneNumber() {
		return finPrimaryPhoneNumber;
	}

	public void setFinPrimaryPhoneNumber(String finPrimaryPhoneNumber) {
		this.finPrimaryPhoneNumber = finPrimaryPhoneNumber;
	}

	public String getFinFaxNumber() {
		return finFaxNumber;
	}

	public void setFinFaxNumber(String finFaxNumber) {
		this.finFaxNumber = finFaxNumber;
	}

	public String getFinCommunicationPreference() {
		return finCommunicationPreference;
	}

	public void setFinCommunicationPreference(String finCommunicationPreference) {
		this.finCommunicationPreference = finCommunicationPreference;
	}

	public Long getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(Long entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getStatusChangeDate() {
		return statusChangeDate;
	}

	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}

	public Integer getDocumentIdEE() {
		return documentIdEE;
	}

	public void setDocumentIdEE(Integer documentIdEE) {
		this.documentIdEE = documentIdEE;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());

	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	@Override
	public String toString() {
		return "Enrollment Entity Details: entityId = " + id + ", userId = " + (user != null ? user.getId() : "");
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 1;
		result = PRIME * result
				+ ((businessLegalName == null) ? 0 : businessLegalName
						.hashCode());
		result = PRIME * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = PRIME * result + ((communicationPreference == null) ? 0
						: communicationPreference.hashCode());
		result = PRIME * result
				+ ((countiesServed == null) ? 0 : countiesServed.hashCode());
		
		result = PRIME * result
				+ ((faxNumber == null) ? 0 : faxNumber.hashCode());
		result = PRIME * result + id;
		result = PRIME * result + ((orgType == null) ? 0 : orgType.hashCode());
		result = PRIME * result
				+ ((receivedPayments == null) ? 0 : receivedPayments.hashCode());

		result = hashGrantData(result);
		result = hashDocumnetIdData(result);
		result = hashEntityData(result);
		result = hashTaxData(result);
		result = hashFinInfo(result);
		result = hashPrimaryInfo(result);
		result = hashPhoneNumber(result);
		result = hashRegistrationAndStatusData(result);
		result = hashCreationData(result);		
		result = hashUpdateData(result);
		return result;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashGrantData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((grantAwardAmount == null) ? 0 : grantAwardAmount.hashCode());
		computedResult = PRIME * computedResult
				+ ((grantContractNo == null) ? 0 : grantContractNo.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashEntityData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((entityName == null) ? 0 : entityName.hashCode());
		computedResult = PRIME * computedResult
				+ ((entityNumber == null) ? 0 : entityNumber.hashCode());
		computedResult = PRIME * computedResult
				+ ((entityType == null) ? 0 : entityType.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashFinInfo(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((finCommunicationPreference == null) ? 0
						: finCommunicationPreference.hashCode());
		computedResult = PRIME * computedResult
				+ ((finContactName == null) ? 0 : finContactName.hashCode());
		computedResult = PRIME * computedResult
				+ ((finEmailAddress == null) ? 0 : finEmailAddress.hashCode());
		computedResult = PRIME * computedResult
				+ ((finFaxNumber == null) ? 0 : finFaxNumber.hashCode());
		computedResult = PRIME * computedResult
				+ ((finPrimaryPhoneNumber == null) ? 0 : finPrimaryPhoneNumber
						.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashPhoneNumber(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((primaryPhoneNumber == null) ? 0 : primaryPhoneNumber
						.hashCode());
		computedResult = PRIME * computedResult
				+ ((secondaryPhoneNumber == null) ? 0 : secondaryPhoneNumber
						.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashPrimaryInfo(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((priCommunicationPreference == null) ? 0
						: priCommunicationPreference.hashCode());
		computedResult = PRIME * computedResult
				+ ((priContactEmailAddress == null) ? 0
						: priContactEmailAddress.hashCode());
		computedResult = PRIME * computedResult
				+ ((priContactName == null) ? 0 : priContactName.hashCode());
		computedResult = PRIME * computedResult
				+ ((priContactPrimaryPhoneNumber == null) ? 0
						: priContactPrimaryPhoneNumber.hashCode());
		computedResult = PRIME * computedResult
				+ ((priContactSecondaryPhoneNumber == null) ? 0
						: priContactSecondaryPhoneNumber.hashCode());
		computedResult = PRIME * computedResult
				+ ((primaryEmailAddress == null) ? 0 : primaryEmailAddress
						.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashRegistrationAndStatusData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((registrationDate == null) ? 0 : registrationDate.hashCode());
		computedResult = PRIME * computedResult
				+ ((registrationRenewalDate == null) ? 0
						: registrationRenewalDate.hashCode());
		computedResult = PRIME * computedResult
				+ ((registrationStatus == null) ? 0 : registrationStatus
						.hashCode());
	
		computedResult = PRIME * computedResult
				+ ((statusChangeDate == null) ? 0 : statusChangeDate.hashCode());
		return result;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashTaxData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((federalTaxID == null) ? 0 : federalTaxID.hashCode());

		computedResult = PRIME * computedResult
				+ ((stateTaxID == null) ? 0 : stateTaxID.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashUpdateData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((updatedBy == null) ? 0 : updatedBy.hashCode());
		computedResult = PRIME * computedResult
				+ ((updatedOn == null) ? 0 : updatedOn.hashCode());
		computedResult = PRIME * computedResult + ((user == null) ? 0 : user.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashDocumnetIdData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((documentId == null) ? 0 : documentId.hashCode());
		computedResult = PRIME * computedResult
				+ ((documentIdEE == null) ? 0 : documentIdEE.hashCode());
		return computedResult;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashCreationData(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((createdBy == null) ? 0 : createdBy.hashCode());
		computedResult = PRIME * computedResult
				+ ((createdOn == null) ? 0 : createdOn.hashCode());
		return computedResult;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof EnrollmentEntity)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		EnrollmentEntity other = (EnrollmentEntity) obj;
		return checkEnrollmentEntityData(other);
	}
	
	/**
	 * Method to check current object's set of data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEnrollmentEntityData(EnrollmentEntity other) {		
		return (checkGroup1Data(other) && checkGroup2Data(other));
	}

	
	/**
	 * Method to check current object's first set of data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkGroup1Data(EnrollmentEntity other) {
		return (checkIdBusinessLegalNameCommentsData(other) && checkOrgTypeRegistrationDataCuntiesServedData(other) && checkEntityDataDocumentIdsStatusChangeDataCommunicationDataGrantInfoData(other));
		
	}
	
	/**
	 * Method to check current object's document ids, status change data and entity etc. data 
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityDataDocumentIdsStatusChangeDataCommunicationDataGrantInfoData(EnrollmentEntity other) {
		return (checkDocumentIds(other) && checkStatsuChangeDate(other) && checkEntityDataCommunicationDataGrantInfo(other));
	}
	
	/**
	 * Method to check current object's entity data, communication data and grant information 
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityDataCommunicationDataGrantInfo (EnrollmentEntity other) {
		return (checkEntityData(other) && checkCommunicationData(other) && checkGrantInfo(other));
	}
	
	/**
	 * Method to check current object's org type, registration data and counties served data
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkOrgTypeRegistrationDataCuntiesServedData(EnrollmentEntity other) {
		return (checkOrgType(other) && checkRegistrationData(other) && checkCountiesServed(other));
	}
	
	/**
	 * Method to check current object's id, business legal name and comments data
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkIdBusinessLegalNameCommentsData(EnrollmentEntity other) {
		return (checkId(other) && checkBusinessLegalName(other) && checkComments(other));
	}
	
	/**
	 * Method to check current object's second set of data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkGroup2Data(EnrollmentEntity other) {
		return (checkPIFIReceivedPaymentsDataTaxData(other) && checkCreationUpdateUserData(other));
	}
	
	/**
	 * Method to check current object's creator and updator's data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreationUpdateUserData(EnrollmentEntity other) {
		return (checkCreationData(other) && checkUpdateData(other) && checkUserData(other));
	}
	
	/**
	 * Method to check current object's primary contact's information, financial contact's information and tax data
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPIFIReceivedPaymentsDataTaxData(EnrollmentEntity other) {
		return (checkPrimaryInfo(other) && checkFinancialInfo(other) && checkReceivedPaymentsData(other) && checkTaxData(other));
	}
	
	/**
	 * Method to check current object's attribute 'businessLegalName' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkBusinessLegalName(EnrollmentEntity other) {
		if (businessLegalName == null) {
			if (other.businessLegalName != null) {
				return false;
			}
		} else if (!businessLegalName.equals(other.businessLegalName)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'comments' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkComments(EnrollmentEntity other) {
		if (comments == null) {
			if (other.comments != null) {
				return false;
			}
		} else if (!comments.equals(other.comments)) {
			return false;
		}

		return true;
	}
	
	/**
	 * Method to check current object's attribute 'countiesServed' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCountiesServed(EnrollmentEntity other) {
		if (countiesServed == null) {
			if (other.countiesServed != null) {
				return false;
			}
		} else if (!countiesServed.equals(other.countiesServed)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's communication data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCommunicationData(EnrollmentEntity other) {
		return(checkCommunicationPreference(other) && checkSecondaryPhoneNumber(other) && checkFaxNumber(other));
	}
	
	/**
	 * Method to check current object's attribute 'faxNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFaxNumber(EnrollmentEntity other) {
		if (faxNumber == null) {
			if (other.faxNumber != null) {
				return false;
			}
		} else if (!faxNumber.equals(other.faxNumber)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'secondaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSecondaryPhoneNumber(EnrollmentEntity other) {
		if (secondaryPhoneNumber == null) {
			if (other.secondaryPhoneNumber != null) {
				return false;
			}
		} else if (!secondaryPhoneNumber.equals(other.secondaryPhoneNumber)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'communicationPreference' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCommunicationPreference(EnrollmentEntity other) {
		if (communicationPreference == null) {
				if (other.communicationPreference != null) {
					return false;
				}
			} else if (!communicationPreference
					.equals(other.communicationPreference)) {
				return false;
			}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'id' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkId(EnrollmentEntity other) {
		if (id != other.id) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'orgType' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkOrgType(EnrollmentEntity other) {
		if (orgType == null) {
			if (other.orgType != null) {
				return false;
			}
		} else if (!orgType.equals(other.orgType)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'receivedPayments' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkReceivedPaymentsData(EnrollmentEntity other) {
		if (receivedPayments == null) {
			if (other.receivedPayments != null) {
				return false;
			}
		} else if (!receivedPayments.equals(other.receivedPayments)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'statusChangeDate' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkStatsuChangeDate(EnrollmentEntity other) {
		if (statusChangeDate == null) {
			if (other.statusChangeDate != null) {
				return false;
			}
		} else if (!statusChangeDate.equals(other.statusChangeDate)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's tax related data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkTaxData(EnrollmentEntity other) {
		return (checkFederalTaxID(other) && checkStateTaxID(other));
	}
	
	/**
	 * Method to check current object's attribute 'federalTaxID' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFederalTaxID(EnrollmentEntity other) {
		if (federalTaxID == null) {
			if (other.federalTaxID != null) {
				return false;
			}
		} else if (!federalTaxID.equals(other.federalTaxID)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'stateTaxID' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkStateTaxID(EnrollmentEntity other) {
		if (stateTaxID == null) {
			if (other.stateTaxID != null) {
				return false;
			}
		} else if (!stateTaxID.equals(other.stateTaxID)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'user' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUserData(EnrollmentEntity other) {
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's updater related data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdateData(EnrollmentEntity other) {
		return (checkUpdatedBy(other) && checkUpdatedOn(other));
	}
	
	/**
	 * Method to check current object's attribute 'updatedBy' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdatedBy(EnrollmentEntity other) {
		if (updatedBy == null) {
			if (other.updatedBy != null) {
				return false;
			}
		} else if (!updatedBy.equals(other.updatedBy)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'updatedOn' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdatedOn(EnrollmentEntity other) {
		if (updatedOn == null) {
			if (other.updatedOn != null) {
				return false;
			}
		} else if (!updatedOn.equals(other.updatedOn)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's registration date, registration renewal date and registration status
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkRegistrationData(EnrollmentEntity other) {
		return (checkRegistrationDate(other) && checkRegistrationRenewalDate(other) && checkRegistrationStatus(other));
	}
	
	/**
	 * Method to check current object's attribute 'registrationDate' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkRegistrationDate(EnrollmentEntity other) {
		if (registrationDate == null) {
			if (other.registrationDate != null) {
				return false;
			}
		} else if (!registrationDate.equals(other.registrationDate)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'registrationRenewalDate' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkRegistrationRenewalDate(EnrollmentEntity other) {
		if (registrationRenewalDate == null) {
			if (other.registrationRenewalDate != null) {
				return false;
			}
		} else if (!registrationRenewalDate
				.equals(other.registrationRenewalDate)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'registrationStatus' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkRegistrationStatus(EnrollmentEntity other) {
		if (registrationStatus == null) {
			if (other.registrationStatus != null) {
				return false;
			}
		} else if (!registrationStatus.equals(other.registrationStatus)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'priContactName' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryInfo(EnrollmentEntity other) {
		return (checkPhoneNumber(other) && checkPrimaryCommunicationContactDataPrimaryEmailAddressInfo(other));
	}

	/**
	 * Method to check current object's primary contact's primary communication preference, contact data and email address
	 * for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryCommunicationContactDataPrimaryEmailAddressInfo(EnrollmentEntity other) {
		return (checkPriCommunicationPreference(other) && checkPrimaryContactData(other) && checkPrimaryEmailAddress(other));
	}

	/**
	 * Method to check current object's attribute 'priCommunicationPreference' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPriCommunicationPreference(EnrollmentEntity other) {
		if (priCommunicationPreference == null) {
			if (other.priCommunicationPreference != null) {
				return false;
			}
		} else if (!priCommunicationPreference
				.equals(other.priCommunicationPreference)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's primary contact's contact data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryContactData(EnrollmentEntity other) {
		return (checkPriContactEmailAddress(other) && checkPriContactName(other));
	}

	/**
	 * Method to check current object's attribute 'priContactName' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPriContactName(EnrollmentEntity other) {
		if (priContactName == null) {
			if (other.priContactName != null) {
				return false;
			}
		} else if (!priContactName.equals(other.priContactName)) {
			return false;
		}
		return true;
	}


	/**
	 * Method to check current object's attribute 'priContactEmailAddress' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPriContactEmailAddress(EnrollmentEntity other) {
		if (priContactEmailAddress == null) {
			if (other.priContactEmailAddress != null) {
				return false;
			}
		} else if (!priContactEmailAddress.equals(other.priContactEmailAddress)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'primaryEmailAddress' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryEmailAddress(EnrollmentEntity other) {
		if (primaryEmailAddress == null) {
			if (other.primaryEmailAddress != null) {
				return false;
			}
		} else if (!primaryEmailAddress.equals(other.primaryEmailAddress)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's phone numbers for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPhoneNumber(EnrollmentEntity other) {
		return (checkPrimaryPhoneNumber(other) && checkPriContactPrimaryPhoneNumber(other) && checkPriContactSecondaryPhoneNumber(other));
	}

	/**
	 * Method to check current object's attribute 'priContactSecondaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPriContactSecondaryPhoneNumber(EnrollmentEntity other) {
		if (priContactSecondaryPhoneNumber == null) {
			if (other.priContactSecondaryPhoneNumber != null) {
				return false;
			}
		} else if (!priContactSecondaryPhoneNumber
				.equals(other.priContactSecondaryPhoneNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'priContactPrimaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPriContactPrimaryPhoneNumber(EnrollmentEntity other) {
		if (priContactPrimaryPhoneNumber == null) {
			if (other.priContactPrimaryPhoneNumber != null) {
				return false;
			}
		} else if (!priContactPrimaryPhoneNumber
				.equals(other.priContactPrimaryPhoneNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'primaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryPhoneNumber(EnrollmentEntity other) {
		if (primaryPhoneNumber == null) {
			if (other.primaryPhoneNumber != null) {
				return false;
			}
		} else if (!primaryPhoneNumber.equals(other.primaryPhoneNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's grant information for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkGrantInfo(EnrollmentEntity other) {
		return (checkGrantAwardAmount(other) && checkGrantContractNo(other));
	}


	/**
	 * Method to check current object's attribute 'grantAwardAmount' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkGrantAwardAmount(EnrollmentEntity other) {
		if (grantAwardAmount == null) {
			if (other.grantAwardAmount != null) {
				return false;
			}
		} else if (!grantAwardAmount.equals(other.grantAwardAmount)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'grantContractNo' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkGrantContractNo(EnrollmentEntity other) {
		if (grantContractNo == null) {
			if (other.grantContractNo != null) {
				return false;
			}
		} else if (!grantContractNo.equals(other.grantContractNo)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's financial contact's information for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinancialInfo(EnrollmentEntity other) {
		return (checkFinContactName(other) && checkFinCommunicationInfo(other) && checkFinNumberData(other));
	}

	/**
	 * Method to check current object's attribute 'finContactName' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinContactName(EnrollmentEntity other) {
		if (finContactName == null) {
			if (other.finContactName != null) {
				return false;
			}
		} else if (!finContactName.equals(other.finContactName)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's financial contact's communication information for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinCommunicationInfo(EnrollmentEntity other) {
		return (checkFinCommunicationPreference(other) && checkFinEmailAddress(other));
	}

	/**
	 * Method to check current object's attribute 'finEmailAddress' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinEmailAddress(EnrollmentEntity other) {
		if (finEmailAddress == null) {
			if (other.finEmailAddress != null) {
				return false;
			}
		} else if (!finEmailAddress.equals(other.finEmailAddress)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'finCommunicationPreference' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinCommunicationPreference(EnrollmentEntity other) {
		if (finCommunicationPreference == null) {
			if (other.finCommunicationPreference != null) {
				return false;
			}
		} else if (!finCommunicationPreference
				.equals(other.finCommunicationPreference)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's financial contact's numeric data attributes for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinNumberData(EnrollmentEntity other) {
		return (checkFinFaxNumber(other) && checkFinPrimaryPhoneNumber(other));
	}

	/**
	 * Method to check current object's attribute 'finFaxNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinFaxNumber(EnrollmentEntity other) {
		if (finFaxNumber == null) {
			if (other.finFaxNumber != null) {
				return false;
			}
		} else if (!finFaxNumber.equals(other.finFaxNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'finPrimaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFinPrimaryPhoneNumber(EnrollmentEntity other) {
		if (finPrimaryPhoneNumber == null) {
			if (other.finPrimaryPhoneNumber != null) {
				return false;
			}
		} else if (!finPrimaryPhoneNumber.equals(other.finPrimaryPhoneNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's entity data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityData(EnrollmentEntity other) {
		return (checkEntityName(other) && checkEntityNumber(other) && checkEntityType(other));
	}

	/**
	 * Method to check current object's attribute 'entityType' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityType(EnrollmentEntity other) {
		if (entityType == null) {
			if (other.entityType != null) {
				return false;
			}
		} else if (!entityType.equals(other.entityType)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'entityNumber' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityNumber(EnrollmentEntity other) {
		if (entityNumber == null) {
			if (other.entityNumber != null) {
				return false;
			}
		} else if (!entityNumber.equals(other.entityNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'entityName' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityName(EnrollmentEntity other) {
		if (entityName == null) {
			if (other.entityName != null) {
				return false;
			}
		} else if (!entityName.equals(other.entityName)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's document Ids for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkDocumentIds(EnrollmentEntity other) {
		return (checkDocumentId(other) && checkDocumentIdEE(other)); 
	}


	/**
	 * Method to check current object's attribute 'documentIdEE' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkDocumentIdEE(EnrollmentEntity other) {
		if (documentIdEE == null) {
			if (other.documentIdEE != null) {
				return false;
			}
		} else if (!documentIdEE.equals(other.documentIdEE)) {
			return false;
		}
		return true;
	}


	/**
	 * Method to check current object's attribute 'documentId' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkDocumentId(EnrollmentEntity other) {
		if (documentId == null) {
			if (other.documentId != null) {
				return false;
			}
		} else if (!documentId.equals(other.documentId)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's creation data for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreationData(EnrollmentEntity other) {
		if(checkCreatedBy(other) && checkCreatedOn(other)) {
			return true;
		}
		
		return false;
	}

	/**
	 * Method to check current object's attribute 'createdOn' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreatedOn(EnrollmentEntity other) {
		if (createdOn == null) {
			if (other.createdOn != null) {
				return false;
			}
		} else if (!createdOn.equals(other.createdOn)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'createdBy' for equality with given object.
	 * 
	 * @param other The other EnrollmentEntity instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreatedBy(EnrollmentEntity other) {
		if (createdBy == null) {
			if (other.createdBy != null) {
				return false;
			}
		} else if (!createdBy.equals(other.createdBy)) {
			return false;
		}
		return true;
	}

	private String appendZeros(String str, int length) {
		int numberLength;
		StringBuffer numberWithZeros = new StringBuffer();
		if (str.length() != length) {
			numberLength = length - str.length();
			for (int i = 0; i < numberLength; i++) {
				numberWithZeros.append(0);
			}
		}
		return numberWithZeros.append(str).toString();
	}
}
