CREATE TABLE "PRE_ELIGIBILITY_RESULTS"
  (
    "ID"               NUMBER,
    "LEAD_ID"          NUMBER,
    "ZIPCODE"          VARCHAR2(5 CHAR),
    "COUNTY_CODE"      VARCHAR2(5 CHAR),
    "FAMILY_SIZE"      NUMBER,
    "NO_OF_APPLICANTS" NUMBER,
    "HOUSEHOLD_INCOME" NUMBER,
    "MEMBER_DATA"      VARCHAR2(4000 BYTE),
    "API_OUTPUT" CLOB,
    "OVERALL_API_STATUS" VARCHAR2(10 CHAR),
    "ERROR_CODE"         NUMBER,
    "ERROR_MSG"          VARCHAR2(250 CHAR),
    "STATUS"             VARCHAR2(50 CHAR),
    "STAGE"              VARCHAR2(50 CHAR),
    "CSR"                VARCHAR2(50 CHAR),
    "APTC"               VARCHAR2(50 CHAR),
    "PREMIUM"            VARCHAR2(50 CHAR),
    "COVERAGE_START_DATE" TIMESTAMP (6),
    "COVERAGE_YEAR"   NUMBER,
    "CREATED_BY"      NUMBER,
    "LAST_UPDATED_BY" NUMBER,
    "CREATION_TIMESTAMP" TIMESTAMP (6),
    "LAST_UPDATE_TIMESTAMP" TIMESTAMP (6),
    CONSTRAINT "PK_PRE_ELIGIBILITY_RESULTS" PRIMARY KEY ("ID") ,
    CONSTRAINT "UK_LEAD_ID_COVERAGE_YEAR" UNIQUE ("LEAD_ID", "COVERAGE_YEAR"),
    CONSTRAINT "FK_ELIG_LEAD" FOREIGN KEY ("LEAD_ID") REFERENCES "ELIG_LEAD" ("ID") ENABLE
  );
COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."ID"
IS
  'Primary Key';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."LEAD_ID"
IS
  'Foreign key reference to ELIG_LEAD table';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."ZIPCODE"
IS
  'Zipcode where the coverage is applied for';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."COUNTY_CODE"
IS
  'County code where the coverage is applied for';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."FAMILY_SIZE"
IS
  'Number of members in the family';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."NO_OF_APPLICANTS"
IS
  'Number of applicants that the plan has to be quoted for';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."HOUSEHOLD_INCOME"
IS
  'Annual household income';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."MEMBER_DATA"
IS
  'CLOB containing data for household members';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."API_OUTPUT"
IS
  'CLOB containing response data of the estimator API';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."OVERALL_API_STATUS"
IS
  'Success or Failure depending on the execution of the API request';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."ERROR_CODE"
IS
  'Error code in case of an error';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."ERROR_MSG"
IS
  'Error description in case of an error';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."STATUS"
IS
  'Possible Values : Redirected,Not Interested,Closed Online,Closed by Agent';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."STAGE"
IS
  'Possible Values : Welcome,Pre Eligibility,Pre shopping,Redirected to FFM/SBE,Shopping,Enrollment,Enrollment Completed';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."CSR"
IS
  'CSR level if applicable';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."APTC"
IS
  'Final APTC applicable for the household';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."PREMIUM"
IS
  'Monthly premium applicable before APTC is applied';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."COVERAGE_START_DATE"
IS
  'Used to capture coverage start date computed during pre-shopping';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."COVERAGE_YEAR"
IS
  'Coverage year for which pre-shopping was done';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."CREATED_BY"
IS
  'User Id who created this record';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."LAST_UPDATED_BY"
IS
  'User Id who updated this record';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."CREATION_TIMESTAMP"
IS
  'Created On Time Stamp';
  COMMENT ON COLUMN "PRE_ELIGIBILITY_RESULTS"."LAST_UPDATE_TIMESTAMP"
IS
  'Updated on Time stamp';

CREATE SEQUENCE PRE_ELIGIBILITY_RESULTS_SEQ;