--liquibase formatted sql

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ASSISTER_ADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('manage_assister')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('manage_entity')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));