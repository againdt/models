package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * 
 * @author sharma_v
 * The persistent class for the PM_RATING_AREA database table.
 */

@SuppressWarnings("serial")
@Entity
@Table(name = "pm_rating_area")
public class RatingArea implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RatingArea_Seq")
	@SequenceGenerator(name = "RatingArea_Seq", sequenceName = "rating_area_seq", allocationSize = 1)
	private int id;

	@Column(name = "state")
	private String state;
	
	@Column(name = "rating_area")
	private String ratingArea;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	
}
