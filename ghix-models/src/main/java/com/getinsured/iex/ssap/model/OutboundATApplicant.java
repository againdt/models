package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name = "OUTBOUND_AT_APPLICANT")
public class OutboundATApplicant implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "OUTBOUND_AT_APPLICANT_ID_GENERATOR", sequenceName = "OUTBOUND_AT_APPLICANT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OUTBOUND_AT_APPLICANT_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 22)
	private long id;
	
	@Column(name = "SSAP_APPLICATION_ID")
	private long ssapApplicationId;
		
	@Column(name="APPLICANT_GUID")
	private String applicantGuid;
	
	@Column(name = "ACTION", length = 100)
	private String action;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "ACTION_DATE")
	private Date actionDate;
	
	@Column(name = "INBOUND_AT_PAYLOAD_ID")
	private Integer inboundATPayloadId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_DETERMINATION_DATE")
	private Date eligibilityDeterminationDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimestamp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Integer getInboundATPayload() {
		return inboundATPayloadId;
	}

	public void setInboundATPayload(Integer inboundATPayload) {
		this.inboundATPayloadId = inboundATPayload;
	}

	public Date getEligibilityDeterminationDate() {
		return eligibilityDeterminationDate;
	}

	public void setEligibilityDeterminationDate(Date eligibilityDeterminationDate) {
		this.eligibilityDeterminationDate = eligibilityDeterminationDate;
	}
	
	public Date getCreatedDate() {
		return creationTimestamp;
	}

	public void setCreatedDate(Date createdDate) {
		this.creationTimestamp = createdDate;
	}

	public Date getUpdatedDate() {
		return lastUpdateTimestamp;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.lastUpdateTimestamp = updatedDate;
	}
	
	@PrePersist
	public void PrePersist() {
		this.setCreatedDate(new TSDate());
		this.setUpdatedDate(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedDate(new TSDate());
	}

}
