package com.getinsured.hix.dto.agency;

public class SearchAgencyDelegationDTO {

	private String individualFirstName;
	private String individualLastName;

	private String agentFirstName;
	private String agentLastName;

	/**
	 * Date should be in format MM/dd/yyyy
	 */
	private String fromDate;
	/**
	 * Date should be in format MM/dd/yyyy
	 */
	private String toDate;
	
	private String designationStatus;

	private String pageNumber;
	private Integer startPosition;
	private Integer pageSize;
	
	private String sortOrder;
	private String sortBy;
	
	private Long agencyId;

	public String getIndividualFirstName() {
		return individualFirstName;
	}

	public void setIndividualFirstName(String individualFirstName) {
		this.individualFirstName = individualFirstName;
	}

	public String getIndividualLastName() {
		return individualLastName;
	}

	public void setIndividualLastName(String individualLastName) {
		this.individualLastName = individualLastName;
	}

	public String getAgentFirstName() {
		return agentFirstName;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	 

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getDesignationStatus() {
		return designationStatus;
	}

	public void setDesignationStatus(String designationStatus) {
		this.designationStatus = designationStatus;
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Integer startPosition) {
		this.startPosition = startPosition;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

}
