package com.getinsured.hix.model.emx;

import java.io.Serializable;

public class EmxMemberVerificationDto implements Serializable {

	private static final long serialVersionUID = 1230951042501540394L;

	
	
	private String firstName;
	private String lastName;
	private long cmrHouseholdId;
	private String censusType;
	private long affiliateId;
	private long flowId;
	private String email;
	private String phone;
	private long leadId;
	private long employeeId;
	private long employerId;
	private String employerName;
	private String encryptCrmHouseHoldId;
	private String accountUserId;
	private String accountUserStatus;
	
	
	public String getAccountUserId() {
		return accountUserId;
	}
	public void setAccountUserId(String accountUserId) {
		this.accountUserId = accountUserId;
	}
	public String getAccountUserStatus() {
		return accountUserStatus;
	}
	public void setAccountUserStatus(String accountUserStatus) {
		this.accountUserStatus = accountUserStatus;
	}
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public long getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(long cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	public String getCensusType() {
		return censusType;
	}
	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}
	public long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}
	public long getFlowId() {
		return flowId;
	}
	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public long getLeadId() {
		return leadId;
	}
	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public long getEmployerId() {
		return employerId;
	}
	public void setEmployerId(long employerId) {
		this.employerId = employerId;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	public String getEncryptCrmHouseHoldId() {
		return encryptCrmHouseHoldId;
	}
	public void setEncryptCrmHouseHoldId(String encryptCrmHouseHoldId) {
		this.encryptCrmHouseHoldId = encryptCrmHouseHoldId;
	}
}
