package com.getinsured.eligibility.enums;

public enum AccountTransferProcessStatus {
	
	SUCCESS, FAILED, INPROGRESS,ATRECEIVED;
}