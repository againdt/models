package com.getinsured.hix.model.estimator.mini;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * MedicareleadRequest
 */

public class MedicareLeadRequest   {
	
  @JsonProperty("campaignId")
  private String campaignId = null;

  @JsonProperty("mcLeadId")
  private String mcLeadId = null;

  @JsonProperty("emailAddress")
  private String emailAddress = null;

  @JsonProperty("phoneNumber")
  private String phoneNumber = null;

  @JsonProperty("dateOfBirth")
  private String dateOfBirth = null;

  @JsonProperty("tenant")
  private String tenant = null;

  @JsonProperty("zipCode")
  private String zipCode = null;

  @JsonProperty("countyCode")
  private String countyCode = null;
  
  @JsonProperty("stateCode")
  private String stateCode = null;

  @JsonProperty("firstName")
  private String firstName = null;

  @JsonProperty("lastName")
  private String lastName = null;

  @JsonProperty("gender")
  private String gender = null;

  @JsonProperty("tobacco")
  private String tobacco = null;

  // Y or N
  @JsonProperty("planSelected")
  private String planSelected = "Y";

  // Y or N
  @JsonProperty("realTimeOutboundCall")
  private String realTimeOutboundCall = "Y";
  
  

@JsonProperty("planData")
  private MedicareLeadPlanData planData = null;

  public MedicareLeadRequest campaignId(String campaignId) {
    this.campaignId = campaignId;
    return this;
  }

   /**
   * Get campaignId
   * @return campaignId
  **/
  @NotNull
  public String getCampaignId() {
    return campaignId;
  }

  public void setCampaignId(String campaignId) {
    this.campaignId = campaignId;
  }

  public MedicareLeadRequest mcLeadId(String mcLeadId) {
    this.mcLeadId = mcLeadId;
    return this;
  }

   /**
   * Get mcLeadId
   * @return mcLeadId
  **/
  @NotNull
  public String getMcLeadId() {
    return mcLeadId;
  }

  public void setMcLeadId(String mcLeadId) {
    this.mcLeadId = mcLeadId;
  }

  public MedicareLeadRequest emailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

   /**
   * Get emailAddress
   * @return emailAddress
  **/
  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public MedicareLeadRequest phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

   /**
   * Get phoneNumber
   * @return phoneNumber
  **/
  @NotNull
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public MedicareLeadRequest dateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
    return this;
  }

   /**
   * Get dateOfBirth
   * @return dateOfBirth
  **/
  
  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public MedicareLeadRequest tenant(String tenant) {
    this.tenant = tenant;
    return this;
  }

   /**
   * Get tenant
   * @return tenant
  **/
  
  @NotNull
  public String getTenant() {
    return tenant;
  }

  public void setTenant(String tenant) {
    this.tenant = tenant;
  }

  public MedicareLeadRequest zipCode(String zipCode) {
    this.zipCode = zipCode;
    return this;
  }

   /**
   * Get zipCode
   * @return zipCode
  **/
  
  @NotNull
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public MedicareLeadRequest countyCode(String countyCode) {
    this.countyCode = countyCode;
    return this;
  }

  public MedicareLeadRequest stateCode(String stateCode) {
	    this.stateCode = stateCode;
	    return this;
	  }
  public String getStateCode() {
	    return stateCode;
	  }
 
  public void setStateCode(String stateCode) {
	    this.stateCode = stateCode;
	  }
   /**
   * Get countyCode
   * @return countyCode
  **/
  
  public String getCountyCode() {
    return countyCode;
  }

  public void setCountyCode(String countyCode) {
    this.countyCode = countyCode;
  }

  public MedicareLeadRequest firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * Get firstName
   * @return firstName
  **/
  
  @NotNull
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public MedicareLeadRequest lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * Get lastName
   * @return lastName
  **/
  
  @NotNull
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public MedicareLeadRequest gender(String gender) {
    this.gender = gender;
    return this;
  }

   /**
   * Get gender
   * @return gender
  **/
  
  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public MedicareLeadRequest tobacco(String tobacco) {
    this.tobacco = tobacco;
    return this;
  }

   /**
   * Get tobacco
   * @return tobacco
  **/
  
  public String getTobacco() {
    return tobacco;
  }

  public void setTobacco(String tobacco) {
    this.tobacco = tobacco;
  }

  public MedicareLeadRequest planData(MedicareLeadPlanData planData) {
    this.planData = planData;
    return this;
  }

   /**
   * Get planData
   * @return planData
  **/
  
  public MedicareLeadPlanData getPlanData() {
    return planData;
  }

  public void setPlanData(MedicareLeadPlanData planData) {
    this.planData = planData;
  }

  public String getPlanSelected() {
		return planSelected;
	}

	public void setPlanSelected(String planSelected) {
		this.planSelected = planSelected;
	}

	
	public String getRealTimeOutboundCall() {
		return realTimeOutboundCall;
	}

	public void setRealTimeOutboundCall(String realTimeOutboundCall) {
		this.realTimeOutboundCall = realTimeOutboundCall;
	}

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicareLeadRequest medicarelead = (MedicareLeadRequest) o;
    return Objects.equals(this.campaignId, medicarelead.campaignId) &&
        Objects.equals(this.mcLeadId, medicarelead.mcLeadId) &&
        Objects.equals(this.emailAddress, medicarelead.emailAddress) &&
        Objects.equals(this.phoneNumber, medicarelead.phoneNumber) &&
        Objects.equals(this.dateOfBirth, medicarelead.dateOfBirth) &&
        Objects.equals(this.tenant, medicarelead.tenant) &&
        Objects.equals(this.zipCode, medicarelead.zipCode) &&
        Objects.equals(this.countyCode, medicarelead.countyCode) &&
        Objects.equals(this.firstName, medicarelead.firstName) &&
        Objects.equals(this.lastName, medicarelead.lastName) &&
        Objects.equals(this.gender, medicarelead.gender) &&
        Objects.equals(this.tobacco, medicarelead.tobacco) &&
        Objects.equals(this.planData, medicarelead.planData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(campaignId, mcLeadId, emailAddress, phoneNumber, dateOfBirth, tenant, zipCode, countyCode, firstName, lastName, gender, tobacco, planData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Medicarelead {\n");
    sb.append("    campaignId: ").append(toIndentedString(campaignId)).append("\n");
    sb.append("    mcLeadId: ").append(toIndentedString(mcLeadId)).append("\n");
    sb.append("    emailAddress: ").append(toIndentedString(emailAddress)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    dateOfBirth: ").append(toIndentedString(dateOfBirth)).append("\n");
    sb.append("    tenant: ").append(toIndentedString(tenant)).append("\n");
    sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
    sb.append("    countyCode: ").append(toIndentedString(countyCode)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    tobacco: ").append(toIndentedString(tobacco)).append("\n");
    sb.append("    planData: ").append(toIndentedString(planData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

