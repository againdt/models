package com.getinsured.hix.dto.directenrollment.payment;

import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.dto.directenrollment.payment.PaymentConstants.PaymentError;
import com.getinsured.hix.dto.directenrollment.payment.PaymentConstants.PaymentMode;
import com.getinsured.hix.dto.directenrollment.payment.PaymentConstants.PaymentValidationProvider;

public class PaymentValidationResponseDTO {
	/**
	 * empty map means no errors.
	 * Key on this map is each PaymentMode requested as part of validation.
	 * This map should be populated only in case we have a validation error.
	 * Also the containing Object ie. DirectEnrollmentResponseDTO should be marked
	 * as Status.FAILURE
	 */
	private Map<PaymentMode, PaymentError> errors = new HashMap<PaymentMode, PaymentError>();
	/**
	 * Will be used to show appropriate error message once things go wrong.
	 */
	private PaymentValidationProvider provider;
	
	public PaymentValidationProvider getProvider() {
		return provider;
	}

	public void setProvider(PaymentValidationProvider provider) {
		this.provider = provider;
	}

	public Map<PaymentMode, PaymentError> getErrors() {
		return errors;
	}

	public void setErrors(Map<PaymentMode, PaymentError> errors) {
		this.errors = errors;
	}
	
}
