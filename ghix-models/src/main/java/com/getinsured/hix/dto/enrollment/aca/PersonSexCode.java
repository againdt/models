package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class PersonSexCode{
  @JsonProperty("Value")
  
  private Integer Value;
  public void setValue(Integer Value){
   this.Value=Value;
  }
  public Integer getValue(){
   return Value;
  }
}