package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="provider_mapping")
public class ProviderMapping  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private ProviderProductDomain providerProductDomain;
	private String networkKey;
	private Set<ProviderNetworkMapping> providerNetworkMappings = new HashSet<ProviderNetworkMapping>(0);

	@Id 
	@SequenceGenerator(name="PROVIDER_MAPPING_SEQ", sequenceName="PROVIDER_MAPPING_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="PROVIDER_MAPPING_SEQ")
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PRODUCT_ID")
	public ProviderProductDomain getProviderProductDomain() {
		return this.providerProductDomain;
	}

	public void setProviderProductDomain(ProviderProductDomain providerProductDomain) {
		this.providerProductDomain = providerProductDomain;
	}


	@Column(name="NETWORK_KEY", length=100)
	public String getNetworkKey() {
		return this.networkKey;
	}

	public void setNetworkKey(String networkKey) {
		this.networkKey = networkKey;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerMapping")
	public Set<ProviderNetworkMapping> getProviderNetworkMappings() {
		return this.providerNetworkMappings;
	}

	public void setProviderNetworkMappings(Set<ProviderNetworkMapping> providerNetworkMappings) {
		this.providerNetworkMappings = providerNetworkMappings;
	}

}


