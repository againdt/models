package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Krishna_prasad
 *
 */

public class EmployerData implements Serializable 
{
	
	private static final long serialVersionUID = 1L;
	private Integer employerId;
	private String coverageStartDate;
	private String planTier;
	
	public static EmployerData clone(EmployerData employerData)
	{
		EmployerData employerDataCopy = new EmployerData();
		employerDataCopy.setEmployerId(employerData.getEmployerId());
		employerDataCopy.setCoverageStartDate(employerData.getCoverageStartDate());
		employerDataCopy.setPlanTier(employerData.getPlanTier());
		
		return employerDataCopy;
	}
	
	public Integer getEmployerId() {
		return employerId;
	}
	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getPlanTier() {
		return planTier;
	}
	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}
	
}
