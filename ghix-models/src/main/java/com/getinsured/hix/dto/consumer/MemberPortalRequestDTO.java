package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;


/**
 * @author suhasini
 *
 */
public class MemberPortalRequestDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	List<MemberDTO> memberDTOList = new ArrayList<MemberDTO>();
	List<SsapApplicantDTO> applicantDTOList = new ArrayList<SsapApplicantDTO>();
	private Integer householdId;
	private String giHouseholdId;
	private Long ssapApplicationId;
	private Long eligLeadId;
	private Long anonymousEligLeadIdplicationId;
	List<SsapApplicationDTO> applicationDTOList = new ArrayList<SsapApplicationDTO>();
	
    public List<MemberDTO> getMemberDTOList() {
		return memberDTOList;
	}
	public void setMemberDTOList(List<MemberDTO> memberDTOList) {
		this.memberDTOList = memberDTOList;
	}
	public Integer getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}
	public String getGiHouseholdId() {
		return giHouseholdId;
	}
	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}
	public List<SsapApplicantDTO> getApplicantDTOList() {
		return applicantDTOList;
	}
	public void setApplicantDTOList(List<SsapApplicantDTO> applicantDTOList) {
		this.applicantDTOList = applicantDTOList;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public Long getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(Long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	public Long getAnonymousEligLeadIdplicationId() {
		return anonymousEligLeadIdplicationId;
	}
	public void setAnonymousEligLeadIdplicationId(
			Long anonymousEligLeadIdplicationId) {
		this.anonymousEligLeadIdplicationId = anonymousEligLeadIdplicationId;
	}
	
	public List<SsapApplicationDTO> getApplicationDTOList() {
		return applicationDTOList;
	}
	public void setApplicationDTOList(List<SsapApplicationDTO> applicationDTOList) {
		this.applicationDTOList = applicationDTOList;
	}
	@Override
	public String toString() {
		return "MemberPortalRequestDTO [memberDTOList=" + memberDTOList
				+ ", applicantDTOList=" + applicantDTOList + ", householdId="
				+ householdId + ", giHouseholdId=" + giHouseholdId
				+ ", ssapApplicationId=" + ssapApplicationId + ", eligLeadId="
				+ eligLeadId + ", anonymousEligLeadIdplicationId="
				+ anonymousEligLeadIdplicationId + ", applicationDTOList="
				+ applicationDTOList + "]";
	}
	
	

}
