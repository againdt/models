package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class InsurancePlanVariantCategoryNumericCode{
	@JsonProperty("Value")
	private String Value;

	public String getValue() {
		return Value;
	}
	
	public void setValue(String value) {
		Value = value;
	}
	  
  
}