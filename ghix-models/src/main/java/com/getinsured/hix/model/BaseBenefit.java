package com.getinsured.hix.model;

public interface BaseBenefit<P>
{
	public void setName(String name);
	public void setParent(P parentPlan);
}
