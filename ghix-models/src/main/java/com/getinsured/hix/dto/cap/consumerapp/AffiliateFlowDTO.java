package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class AffiliateFlowDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String affiliateId;
	private String flowId;
	private String flowName;
	private boolean emxGatingEnabled;
	private String capPrivacyShortText;
	private String capPrivacyLongText;
	private boolean enableCapDataSharingOption;
	private String accessPinType;
	private String householdPin;
	
	
	public String getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	
	public void setEmxGatingEnabled(boolean emxGatingEnabled) {
		this.emxGatingEnabled = emxGatingEnabled;
	}
	
	public boolean isEmxGatingEnabled() {
		return emxGatingEnabled;
	}

	public String getCapPrivacyShortText() {
		return capPrivacyShortText;
	}

	public void setCapPrivacyShortText(String capPrivacyShortText) {
		this.capPrivacyShortText = capPrivacyShortText;
	}

	public String getCapPrivacyLongText() {
		return capPrivacyLongText;
	}

	public void setCapPrivacyLongText(String capPrivacyLongText) {
		this.capPrivacyLongText = capPrivacyLongText;
	}

	public boolean isEnableCapDataSharingOption() {
		return enableCapDataSharingOption;
	}

	public void setEnableCapDataSharingOption(boolean enableCapDataSharingOption) {
		this.enableCapDataSharingOption = enableCapDataSharingOption;
	}
	
	public String getAccessPinType() {
		return accessPinType;
	}
	
	public void setAccessPinType(String accessPinType) {
		this.accessPinType = accessPinType;
	}
	public String getHouseholdPin() {
		return householdPin;
	}
	public void setHouseholdPin(String householdPin) {
		this.householdPin = householdPin;
	}
}
