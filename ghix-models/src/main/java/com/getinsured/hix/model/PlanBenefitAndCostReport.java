package com.getinsured.hix.model;

public class PlanBenefitAndCostReport {
	private Integer plan_id;
	/*	Used for Benefit	*/
	private String plan_name;
	private String issuer_plan_number;
	private String benefit_name;
	private String inNetworkCopay;
	private String inNetworkCoins;
	private String outNetworkCopay;
	private String outNetworkCoins;
	private String limitation;
	private String limitationattrib;
	
	/*	Used for Cost Share	*/
	private String deductible_name;
	private String inNetworkInd;
	private String inNetworkFly;
	private String outNetworkInd;
	private String outNetworkFly;
	private String combInOutNetworkInd;
	private String combInOutNetworkFly;
	
	/*	Used for Plan Rate */
	private Integer rating_area_id;
	private String ageRange;
	private String tobaco;
	private String planRates;
	
	/**
	 * @return the plan_id
	 */
	public Integer getPlan_id() {
		return plan_id;
	}
	/**
	 * @param plan_id the plan_id to set
	 */
	public void setPlan_id(Integer plan_id) {
		this.plan_id = plan_id;
	}
	/**
	 * @return the plan_name
	 */
	public String getPlan_name() {
		return plan_name;
	}
	/**
	 * @param plan_name the plan_name to set
	 */
	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}
	/**
	 * @return the issuer_plan_number
	 */
	public String getIssuer_plan_number() {
		return issuer_plan_number;
	}
	/**
	 * @param issuer_plan_number the issuer_plan_number to set
	 */
	public void setIssuer_plan_number(String issuer_plan_number) {
		this.issuer_plan_number = issuer_plan_number;
	}
	/**
	 * @return the benefit_name
	 */
	public String getBenefit_name() {
		return benefit_name;
	}
	/**
	 * @param benefit_name the benefit_name to set
	 */
	public void setBenefit_name(String benefit_name) {
		this.benefit_name = benefit_name;
	}
	/**
	 * @return the inNetworkCopay
	 */
	public String getInNetworkCopay() {
		return inNetworkCopay;
	}
	/**
	 * @param inNetworkCopay the inNetworkCopay to set
	 */
	public void setInNetworkCopay(String inNetworkCopay) {
		this.inNetworkCopay = inNetworkCopay;
	}
	/**
	 * @return the inNetworkCoins
	 */
	public String getInNetworkCoins() {
		return inNetworkCoins;
	}
	/**
	 * @param inNetworkCoins the inNetworkCoins to set
	 */
	public void setInNetworkCoins(String inNetworkCoins) {
		this.inNetworkCoins = inNetworkCoins;
	}
	/**
	 * @return the outNetworkCopay
	 */
	public String getOutNetworkCopay() {
		return outNetworkCopay;
	}
	/**
	 * @param outNetworkCopay the outNetworkCopay to set
	 */
	public void setOutNetworkCopay(String outNetworkCopay) {
		this.outNetworkCopay = outNetworkCopay;
	}
	/**
	 * @return the outNetworkCoins
	 */
	public String getOutNetworkCoins() {
		return outNetworkCoins;
	}
	/**
	 * @param outNetworkCoins the outNetworkCoins to set
	 */
	public void setOutNetworkCoins(String outNetworkCoins) {
		this.outNetworkCoins = outNetworkCoins;
	}
	/**
	 * @return the limitation
	 */
	public String getLimitation() {
		return limitation;
	}
	/**
	 * @param limitation the limitation to set
	 */
	public void setLimitation(String limitation) {
		this.limitation = limitation;
	}
	/**
	 * @return the limitationattrib
	 */
	public String getLimitationattrib() {
		return limitationattrib;
	}
	/**
	 * @param limitationattrib the limitationattrib to set
	 */
	public void setLimitationattrib(String limitationattrib) {
		this.limitationattrib = limitationattrib;
	}
	/**
	 * @return the deductible_name
	 */
	public String getDeductible_name() {
		return deductible_name;
	}
	/**
	 * @param deductible_name the deductible_name to set
	 */
	public void setDeductible_name(String deductible_name) {
		this.deductible_name = deductible_name;
	}
	/**
	 * @return the inNetworkInd
	 */
	public String getInNetworkInd() {
		return inNetworkInd;
	}
	/**
	 * @param inNetworkInd the inNetworkInd to set
	 */
	public void setInNetworkInd(String inNetworkInd) {
		this.inNetworkInd = inNetworkInd;
	}
	/**
	 * @return the inNetworkFly
	 */
	public String getInNetworkFly() {
		return inNetworkFly;
	}
	/**
	 * @param inNetworkFly the inNetworkFly to set
	 */
	public void setInNetworkFly(String inNetworkFly) {
		this.inNetworkFly = inNetworkFly;
	}
	/**
	 * @return the outNetworkInd
	 */
	public String getOutNetworkInd() {
		return outNetworkInd;
	}
	/**
	 * @param outNetworkInd the outNetworkInd to set
	 */
	public void setOutNetworkInd(String outNetworkInd) {
		this.outNetworkInd = outNetworkInd;
	}
	/**
	 * @return the outNetworkFly
	 */
	public String getOutNetworkFly() {
		return outNetworkFly;
	}
	/**
	 * @param outNetworkFly the outNetworkFly to set
	 */
	public void setOutNetworkFly(String outNetworkFly) {
		this.outNetworkFly = outNetworkFly;
	}
	/**
	 * @return the combInOutNetworkInd
	 */
	public String getCombInOutNetworkInd() {
		return combInOutNetworkInd;
	}
	/**
	 * @param combInOutNetworkInd the combInOutNetworkInd to set
	 */
	public void setCombInOutNetworkInd(String combInOutNetworkInd) {
		this.combInOutNetworkInd = combInOutNetworkInd;
	}
	/**
	 * @return the combInOutNetworkFly
	 */
	public String getCombInOutNetworkFly() {
		return combInOutNetworkFly;
	}
	/**
	 * @param combInOutNetworkFly the combInOutNetworkFly to set
	 */
	public void setCombInOutNetworkFly(String combInOutNetworkFly) {
		this.combInOutNetworkFly = combInOutNetworkFly;
	}
	/**
	 * @return the rating_area_id
	 */
	public Integer getRating_area_id() {
		return rating_area_id;
	}
	/**
	 * @param rating_area_id the rating_area_id to set
	 */
	public void setRating_area_id(Integer rating_area_id) {
		this.rating_area_id = rating_area_id;
	}
	/**
	 * @return the ageRange
	 */
	public String getAgeRange() {
		return ageRange;
	}
	/**
	 * @param ageRange the ageRange to set
	 */
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
	/**
	 * @return the tobaco
	 */
	public String getTobaco() {
		return tobaco;
	}
	/**
	 * @param tobaco the tobaco to set
	 */
	public void setTobaco(String tobaco) {
		this.tobaco = tobaco;
	}
	/**
	 * @return the planRates
	 */
	public String getPlanRates() {
		return planRates;
	}
	/**
	 * @param planRates the planRates to set
	 */
	public void setPlanRates(String planRates) {
		this.planRates = planRates;
	}
	
	
}
