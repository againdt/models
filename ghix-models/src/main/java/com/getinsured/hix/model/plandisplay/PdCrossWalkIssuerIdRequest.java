package com.getinsured.hix.model.plandisplay;

public class PdCrossWalkIssuerIdRequest {

	private String zipcode;
	private String countyCode;
	private String coverageDate;
	private String isEligibleForCatastrophic;
	private String renewalPlanId;
	private String csrValue;
	private PdSubscriberData pdSubscriberData;
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(String coverageDate) {
		this.coverageDate = coverageDate;
	}
	public String getIsEligibleForCatastrophic() {
		return isEligibleForCatastrophic;
	}
	public void setIsEligibleForCatastrophic(String isEligibleForCatastrophic) {
		this.isEligibleForCatastrophic = isEligibleForCatastrophic;
	}
	public String getRenewalPlanId() {
		return renewalPlanId;
	}
	public void setRenewalPlanId(String renewalPlanId) {
		this.renewalPlanId = renewalPlanId;
	}
	public String getCsrValue() {
		return csrValue;
	}
	public void setCsrValue(String csrValue) {
		this.csrValue = csrValue;
	}
	public PdSubscriberData getPdSubscriberData() {
		return pdSubscriberData;
	}
	public void setPdSubscriberData(PdSubscriberData pdSubscriberData) {
		this.pdSubscriberData = pdSubscriberData;
	}
	
}
