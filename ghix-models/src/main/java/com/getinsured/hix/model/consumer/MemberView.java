package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Member.MemberBooleanFlag;

/**
 * 
 * @author bhatt_s
 * Used as View Model for MyEligibility
 */
public class MemberView implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private String program;
	private String programName;
	private String eligibilityStatus;
	private String eligibilityPeriodStart;
	private String eligibilityPeriodEnd;
	private String email;
	private Location contactLocation;
	private Location mailingLocation;
	private String phoneNumber;
	private String preferredTimeToContact;
	private String gender;
	private String smoker;
	private String relationship;
    private MemberBooleanFlag isHouseHoldContact;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String fullTimeStudent;
    private String maritalStatus;
    
	
	
	public MemberView(){}
	
	
	public MemberView(String name, String program, String programName, String eligibilityStatus,
			String eligibilityPeriodStart, String eligibilityPeriodEnd) {
		super();
		this.name = name;
		this.program = program;
		this.programName = programName;
		this.eligibilityStatus = eligibilityStatus;
		this.eligibilityPeriodStart = eligibilityPeriodStart;
		this.eligibilityPeriodEnd = eligibilityPeriodEnd;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public String getEligibilityPeriodStart() {
		return eligibilityPeriodStart;
	}
	public void setEligibilityPeriodStart(String eligibilityPeriodStart) {
		this.eligibilityPeriodStart = eligibilityPeriodStart;
	}
	public String getEligibilityPeriodEnd() {
		return eligibilityPeriodEnd;
	}
	public void setEligibilityPeriodEnd(String eligibilityPeriodEnd) {
		this.eligibilityPeriodEnd = eligibilityPeriodEnd;
	}
	
	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Location getContactLocation() {
		return contactLocation;
	}


	public void setContactLocation(Location contactLocation) {
		this.contactLocation = contactLocation;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getPreferredTimeToContact() {
		return preferredTimeToContact;
	}


	public void setPreferredTimeToContact(String preferredTimeToContact) {
		this.preferredTimeToContact = preferredTimeToContact;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getSmoker() {
		return smoker;
	}


	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}


	public String getRelationship() {
		return relationship;
	}


	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	public MemberBooleanFlag getIsHouseHoldContact() {
		return isHouseHoldContact;
	}

	public void setIsHouseHoldContact(MemberBooleanFlag isHouseHoldContact) {
		this.isHouseHoldContact = isHouseHoldContact;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Date getBirthDate() {
		return birthDate;
	}


	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public String getFullTimeStudent() {
		return fullTimeStudent;
	}


	public void setFullTimeStudent(String fullTimeStudent) {
		this.fullTimeStudent = fullTimeStudent;
	}


	public Location getMailingLocation() {
		return mailingLocation;
	}


	public void setMailingLocation(Location mailingLocation) {
		this.mailingLocation = mailingLocation;
	}


	public String getMaritalStatus() {
		return maritalStatus;
	}


	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	
	
	
	

}
