package com.getinsured.hix.dto.planmgmt.microservice;

/**
 * DTO class is used to response created/updated data of SERFF_PLAN_MGMT_BATCH table.
 */
public class SerffPlanMgmtBatchResponseDTO extends GhixResponseDTO {

	private SerffPlanMgmtBatchDTO serffPlanMgmtBatchDTO;

	public SerffPlanMgmtBatchResponseDTO() {
	}

	public SerffPlanMgmtBatchDTO getSerffPlanMgmtBatchDTO() {
		return serffPlanMgmtBatchDTO;
	}

	public void setSerffPlanMgmtBatchDTO(SerffPlanMgmtBatchDTO serffPlanMgmtBatchDTO) {
		this.serffPlanMgmtBatchDTO = serffPlanMgmtBatchDTO;
	}
}
