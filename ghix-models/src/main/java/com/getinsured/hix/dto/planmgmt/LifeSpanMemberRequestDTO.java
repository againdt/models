package com.getinsured.hix.dto.planmgmt;

public class LifeSpanMemberRequestDTO {

	private int id;
	private String relation;
	private String dob;
	private String tobacco;
	private String coverageStartDate;
	private String daysActive;
	private int age;
	
	/**
	 * @return the Id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param Id the Id to set
	 */
	public void setId(int Id) {
		this.id = Id;
	}
	/**
	 * @return the relation
	 */
	public String getRelation() {
		return relation;
	}
	/**
	 * @param relation the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}
	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}	
	/**
	 * @return the tobacco
	 */
	public String getTobacco() {
		return tobacco;
	}
	/**
	 * @param tobacco the tobacco to set
	 */
	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}	
	/**
	 * @return the coverageStartDate
	 */
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	/**
	 * @param coverageStartDate the coverageStartDate to set
	 */
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	/**
	 * @return the daysActive
	 */
	public String getDaysActive() {
		return daysActive;
	}
	/**
	 * @param daysActive the daysActive to set
	 */
	public void setDaysActive(String daysActive) {
		this.daysActive = daysActive;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
