package com.getinsured.hix.dto.plandisplay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;

public class PdSession implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PdHouseholdDTO pdHouseholdDTO;
	private List<PdPersonDTO> pdPersonDTOList;
	private PdPreferencesDTO pdPreferencesDTO;
	
	private Date employerCoverageStartDate;
	private String employerPlanTier;
	private String employerWorksiteZip;
	private String employerWorksiteCounty;
	
	private PlanDisplayEnum.InsuranceType insuranceType;
	private PlanDisplayEnum.ExchangeType exchangeType;
	private String issuerId;
	private boolean showCatastrophicPlan;
	private PlanDisplayEnum.GPSVersion gpsVersion;
	
	private PlanDisplayEnum.FlowType flowType;
	private PlanDisplayEnum.EnrollmentFlowType specialEnrollmentFlowType;

	private Long initialHealthPlanId;
	private Float initialHealthPremium;
	private Float initialHealthSubsidy;
	private BigDecimal initialStateSubsidy;
	private String initialHealthSubscriberId;
	private Float initialMaxAPTC;
	private PlanDisplayEnum.YorN initialHealthPlanAvailable;
	
	private Long initialDentalPlanId;
	private Float initialDentalPremium;
	private Float initialDentalSubsidy;
	private String initialDentalSubscriberId;
	private PlanDisplayEnum.YorN initialDentalPlanAvailable;
	
	private ProductType productType;
	
	private String initialHealthCmsPlanId;
	
	public PdHouseholdDTO getPdHouseholdDTO() {
		return pdHouseholdDTO;
	}
	public void setPdHouseholdDTO(PdHouseholdDTO pdHouseholdDTO) {
		this.pdHouseholdDTO = pdHouseholdDTO;
	}
	
	public List<PdPersonDTO> getPdPersonDTOList() {
		return pdPersonDTOList;
	}
	public void setPdPersonDTOList(List<PdPersonDTO> pdPersonDTOList) {
		this.pdPersonDTOList = pdPersonDTOList;
	}
	
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}
	
	public Date getEmployerCoverageStartDate() {
		return employerCoverageStartDate;
	}
	public void setEmployerCoverageStartDate(Date employerCoverageStartDate) {
		this.employerCoverageStartDate = employerCoverageStartDate;
	}
	
	public String getEmployerPlanTier() {
		return employerPlanTier;
	}
	public void setEmployerPlanTier(String employerPlanTier) {
		this.employerPlanTier = employerPlanTier;
	}
	
	public String getEmployerWorksiteZip() {
		return employerWorksiteZip;
	}
	public void setEmployerWorksiteZip(String employerWorksiteZip) {
		this.employerWorksiteZip = employerWorksiteZip;
	}
	
	public String getEmployerWorksiteCounty() {
		return employerWorksiteCounty;
	}
	public void setEmployerWorksiteCounty(String employerWorksiteCounty) {
		this.employerWorksiteCounty = employerWorksiteCounty;
	}
	
	public PlanDisplayEnum.InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(PlanDisplayEnum.InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	
	public PlanDisplayEnum.ExchangeType getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(PlanDisplayEnum.ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}
	
	public String getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	
	public boolean getShowCatastrophicPlan() {
		return showCatastrophicPlan;
	}
	public void setShowCatastrophicPlan(boolean showCatastrophicPlan) {
		this.showCatastrophicPlan = showCatastrophicPlan;
	}
	
	public PlanDisplayEnum.FlowType getFlowType() {
		return flowType;
	}
	public void setFlowType(PlanDisplayEnum.FlowType flowType) {
		this.flowType = flowType;
	}
	
	public PlanDisplayEnum.EnrollmentFlowType getSpecialEnrollmentFlowType() {
		return specialEnrollmentFlowType;
	}

	public void setSpecialEnrollmentFlowType(PlanDisplayEnum.EnrollmentFlowType specialEnrollmentFlowType) {
		this.specialEnrollmentFlowType = specialEnrollmentFlowType;
	}
	
	public Long getInitialHealthPlanId() {
		return initialHealthPlanId;
	}
	
	public void setInitialHealthPlanId(Long initialHealthPlanId) {
		this.initialHealthPlanId = initialHealthPlanId;
	}
	
	public Float getInitialHealthPremium() {
		return initialHealthPremium;
	}
	public void setInitialHealthPremium(Float initialHealthPremium) {
		this.initialHealthPremium = initialHealthPremium;
	}
	
	public Float getInitialHealthSubsidy() {
		return initialHealthSubsidy;
	}
	public void setInitialHealthSubsidy(Float initialHealthSubsidy) {
		this.initialHealthSubsidy = initialHealthSubsidy;
	}
	
	public String getInitialHealthSubscriberId() {
		return initialHealthSubscriberId;
	}
	
	public void setInitialHealthSubscriberId(String initialHealthSubscriberId) {
		this.initialHealthSubscriberId = initialHealthSubscriberId;
	}
	
	public PlanDisplayEnum.YorN getInitialHealthPlanAvailable() {
		return initialHealthPlanAvailable;
	}

	public void setInitialHealthPlanAvailable(PlanDisplayEnum.YorN initialHealthPlanAvailable) {
		this.initialHealthPlanAvailable = initialHealthPlanAvailable;
	}
	
	public Long getInitialDentalPlanId() {
		return initialDentalPlanId;
	}
	
	public void setInitialDentalPlanId(Long initialDentalPlanId) {
		this.initialDentalPlanId = initialDentalPlanId;
	}
	
	public Float getInitialDentalPremium() {
		return initialDentalPremium;
	}
	public void setInitialDentalPremium(Float initialDentalPremium) {
		this.initialDentalPremium = initialDentalPremium;
	}
	
	public Float getInitialDentalSubsidy() {
		return initialDentalSubsidy;
	}
	public void setInitialDentalSubsidy(Float initialDentalSubsidy) {
		this.initialDentalSubsidy = initialDentalSubsidy;
	}
	
	public String getInitialDentalSubscriberId() {
		return initialDentalSubscriberId;
	}
	
	public void setInitialDentalSubscriberId(String initialDentalSubscriberId) {
		this.initialDentalSubscriberId = initialDentalSubscriberId;
	}
	
	public PlanDisplayEnum.YorN getInitialDentalPlanAvailable() {
		return initialDentalPlanAvailable;
	}

	public void setInitialDentalPlanAvailable(PlanDisplayEnum.YorN initialDentalPlanAvailable) {
		this.initialDentalPlanAvailable = initialDentalPlanAvailable;
	}
	
	public PlanDisplayEnum.GPSVersion getGpsVersion() {
		return gpsVersion;
	}
	
	public void setGpsVersion(PlanDisplayEnum.GPSVersion gpsVersion) {
		this.gpsVersion = gpsVersion;
	}
	
	public ProductType getProductType() {
		return productType;
	}
	
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	public String getInitialHealthCmsPlanId() {
		return initialHealthCmsPlanId;
	}
	
	public void setInitialHealthCmsPlanId(String initialHealthCmsPlanId) {
		this.initialHealthCmsPlanId = initialHealthCmsPlanId;
	}

	public BigDecimal getInitialStateSubsidy() {
		return initialStateSubsidy;
	}

	public void setInitialStateSubsidy(BigDecimal initialStateSubsidy) {
		this.initialStateSubsidy = initialStateSubsidy;
	}
	public Float getInitialMaxAPTC() {
		return initialMaxAPTC;
	}
	public void setInitialMaxAPTC(Float initialMaxAPTC) {
		this.initialMaxAPTC = initialMaxAPTC;
	}
}