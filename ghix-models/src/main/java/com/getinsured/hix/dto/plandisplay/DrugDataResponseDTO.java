package com.getinsured.hix.dto.plandisplay;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to provide Response of Drug Data.
 * 
 * @since October 23, 2018
 */
public class DrugDataResponseDTO {

	private Integer totalRecordCount;
	private Integer currentRecordCount;
	private Integer pageSize;
	private long executionDuration;
	private long startTime;

	private List<DrugDataDTO> drugDataList;

	public DrugDataResponseDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Integer getCurrentRecordCount() {
		return currentRecordCount;
	}

	public void setCurrentRecordCount(Integer currentRecordCount) {
		this.currentRecordCount = currentRecordCount;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public long getExecutionDuration() {
		return executionDuration;
	}

	public void startResponse() {
		startTime = Calendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = Calendar.getInstance().getTimeInMillis();
		executionDuration = endTime - startTime;
	}

	public List<DrugDataDTO> getDrugDataList() {
		return drugDataList;
	}

	public void setDrugDataList(List<DrugDataDTO> drugDataList) {
		this.drugDataList = drugDataList;
	}

	public void addDrugDataToList(DrugDataDTO drugDataDTO) {

		if (drugDataList == null) {
			drugDataList = new ArrayList<DrugDataDTO>();
		}
		drugDataList.add(drugDataDTO);
	}
}
