package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Member.Gender;
import com.getinsured.hix.model.consumer.Member.MemberBooleanFlag;

public class MemberDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String middleName;
	private Date   birthDate;
	private String preferredTimeToContact;
	private String relationshipCode;
	private Gender gender;
	private Location contactLocation;
	private MemberBooleanFlag isHouseHoldContact;
	private String memberId;
    private String phoneNumber;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getPreferredTimeToContact() {
		return preferredTimeToContact;
	}
	public void setPreferredTimeToContact(String preferredTimeToContact) {
		this.preferredTimeToContact = preferredTimeToContact;
	}
	
	public String getRelationshipCode() {
		return relationshipCode;
	}
	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}
	
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public Location getContactLocation() {
		return contactLocation;
	}
	public void setContactLocation(Location contactLocation) {
		this.contactLocation = contactLocation;
	}
	
	public MemberBooleanFlag getIsHouseHoldContact() {
		return isHouseHoldContact;
	}
	public void setIsHouseHoldContact(MemberBooleanFlag isHouseHoldContact) {
		this.isHouseHoldContact = isHouseHoldContact;
	}
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
		
}
