/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.GroupList;

/**
 * @author Mishra_m
 *
 */
public class PremiumTaxCreditResponse extends GHIXResponse{
	private double maxAPTC;
	private Map<String, Object> outputData;
	private List<GroupList> groupListArr;
	private List<GroupData> groupDataList;
	private String nestedStackTrace;
  
	public double getMaxAPTC() {
		return maxAPTC;
	}

	public void setMaxAPTC(double maxAPTC) {
		this.maxAPTC = maxAPTC;
	}
	
	public Map<String, Object> getOutputData() {
		return outputData;
	}

	public void setOutputData(Map<String, Object> outputData) {
		this.outputData = outputData;
	}

	public List<GroupList> getGroupListArr() {
		return groupListArr;
	}

	public void setGroupListArr(List<GroupList> groupListArr) {
		this.groupListArr = groupListArr;
	}

	public List<GroupData> getGroupDataList() {
		return groupDataList;
	}

	public void setGroupDataList(List<GroupData> groupDataList) {
		this.groupDataList = groupDataList;
	}

	public String getNestedStackTrace() {
		return nestedStackTrace;
	}

	public void setNestedStackTrace(String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}
	

}
