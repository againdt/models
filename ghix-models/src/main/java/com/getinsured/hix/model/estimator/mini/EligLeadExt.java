package com.getinsured.hix.model.estimator.mini;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.NotAudited;

import com.getinsured.hix.model.Location;

/**
 * This is pojo class for ELIG_LEAD_EXTENSION database table
 * 
 * @author Suresh Kancherla
 */
@Entity
@Table(name = "ELIG_LEAD_EXTENSION")
public class EligLeadExt implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum CensusType {
		// Valid CensusTypes are COBRA, RETIREE,CONTRIBUTED EMPLOYEE (a.k.a CE), NON-CONTRIBUTED EMPLOYEE (a.k.a NCE)
		COBRA("COBRA"), RETIREE("RETIREE"), CE("CE"),NCE("NCE"),INDIVIDUAL("INDIVIDUAL"),ASSOSIATION_MEMEBR("ASSOSIATION_MEMBER");
		
		CensusType(String type) {
			this.censusType = type;
		}

		String censusType;
	}

	public enum YesNo {
		Y, N
	}

	@NotAudited
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ELIG_LEAD_EXTENSION_SEQ")
	@SequenceGenerator(name = "ELIG_LEAD_EXTENSION_SEQ", sequenceName = "ELIG_LEAD_EXTENSION_SEQ", allocationSize = 1)
	private int id;

	@NotAudited
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ELIG_LEAD_ID")
	private EligLead lead;

	@NotAudited
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "LOCATION_ID")
	private Location address;
	
	@NotAudited
	@Column(name = "CENSUS_TYPE")
	@Enumerated(EnumType.STRING)
	private CensusType censusType;

	@NotAudited
	@Column(name = "ALTERNATE_PHONE")
	private String alternatePhone;

	@Temporal(TemporalType.DATE)
	@Column(name = "HRA_TRANSITION_DATE")
	private Date hraTransitionDate;

	@NotAudited
	@Column(name = "HRA_AMOUNT")
	private double householdHraAmount;

	@NotAudited
	@Temporal(TemporalType.DATE)
	@Column(name = "COBRA_ELECTION_DUE_DATE")
	private Date cobraElectionDueDate;

	@NotAudited
	@Column(name = "COBRA_PREMIUM_AMOUNT")
	private double cobraPremiumAmount;

	@NotAudited
	@Column(name = "ACCEPTED_TERMS_CONDITION")
	@Enumerated(EnumType.STRING)
	private YesNo acceptedTermsAndConditions;
	
	@NotAudited
	@Column(name = "QUALIFYING_EVENT_END_DATE")
	private Date qualifyingEventEndDate;
	
	
	@NotAudited
	@Column(name = "HOUSEHOLD_PIN")
	private String householdPin;
	
	@NotAudited
	@Column(name = "AFF_FLOW_ACCESS_ID")
	private long affFlowAccessId;
	
	
	@NotAudited
	@Column(name = "EMPLOYER_CONTRIBUTION_AMOUNT")
	private Double employerContributionAmount;
	
	@NotAudited
	@Column(name = "MEMBER_CONTRIBUTION_DATA")
	private String memberContributionData;
	
	

	@NotAudited
	@Column(name = "HRA_FUNDING_TYPE")
	private String householdHraFundingType;
	
	
	public String getHouseholdHraFundingType() {
		return householdHraFundingType;
	}

	public void setHouseholdHraFundingType(String householdHraFundingType) {
		this.householdHraFundingType = householdHraFundingType;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EligLead getLead() {
		return lead;
	}

	public void setLead(EligLead lead) {
		this.lead = lead;
	}

	public Location getAddress() {
		return address;
	}

	public void setAddress(Location address) {
		this.address = address;
	}

	public CensusType getCensusType() {
		return censusType;
	}

	public void setCensusType(CensusType censusType) {
		this.censusType = censusType;
	}

	public Date getHraTransitionDate() {
		return hraTransitionDate;
	}

	public void setHraTransitionDate(Date hraTransitionDate) {
		this.hraTransitionDate = hraTransitionDate;
	}

	public double getHouseholdHraAmount() {
		return householdHraAmount;
	}

	public void setHouseholdHraAmount(double householdHraAmount) {
		this.householdHraAmount = householdHraAmount;
	}

	public Date getCobraElectionDueDate() {
		return cobraElectionDueDate;
	}

	public void setCobraElectionDueDate(Date cobraElectionDueDate) {
		this.cobraElectionDueDate = cobraElectionDueDate;
	}

	public double getCobraPremiumAmount() {
		return cobraPremiumAmount;
	}

	public void setCobraPremiumAmount(double cobraPremiumAmount) {
		this.cobraPremiumAmount = cobraPremiumAmount;
	}

	public YesNo getAcceptedTermsAndConditions() {
		return acceptedTermsAndConditions;
	}

	public void setAcceptedTermsAndConditions(YesNo acceptedTermsAndConditions) {
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
	}

	public String getAlternatePhone() {
		return alternatePhone;
	}

	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	public Date getQualifyingEventEndDate() {
		return qualifyingEventEndDate;
	}

	public void setQualifyingEventEndDate(Date qualifyingEventEndDate) {
		this.qualifyingEventEndDate = qualifyingEventEndDate;
	}

	public String getHouseholdPin() {
		return householdPin;
	}

	public void setHouseholdPin(String householdPin) {
		this.householdPin = householdPin;
	}

	public long getAffFlowAccessId() {
		return affFlowAccessId;
	}

	public void setAffFlowAccessId(long affFlowAccessId) {
		this.affFlowAccessId = affFlowAccessId;
	}

	public Double getEmployerContributionAmount() {
		return employerContributionAmount;
	}

	public void setEmployerContributionAmount(Double employerContributionAmount) {
		this.employerContributionAmount = employerContributionAmount;
	}

	public String getMemberContributionData() {
		return memberContributionData;
	}

	public void setMemberContributionData(String memberContributionData) {
		this.memberContributionData = memberContributionData;
	}

	

}