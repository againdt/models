package com.getinsured.hix.dto.bb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Implemented for BenitfitBay Request
 * @author meher_a
 *
 */
public class BenefitBayRequestDTO implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	private String householdID;
	private String enrollmentID;
	
	private String carrierID;
	private String carrierName;
	
    private String planID;
    private String planName;
    private BigDecimal grossMonthlyPremium;
    private String planType;
    private String planTier;
    private BigInteger individualDeductible;
    private BigInteger individualMaximumOutOfPocket;
    private BigInteger familyDeductible;
    private BigInteger familyMaximumOutOfPocket;
    private String doctorVisitCost;
    private String genericPrescriptionCost;
    
	public String getHouseholdID() {
		return householdID;
	}
	public void setHouseholdID(String householdID) {
		this.householdID = householdID;
	}
	public String getEnrollmentID() {
		return enrollmentID;
	}
	public void setEnrollmentID(String enrollmentID) {
		this.enrollmentID = enrollmentID;
	}
	public String getCarrierID() {
		return carrierID;
	}
	public void setCarrierID(String carrierID) {
		this.carrierID = carrierID;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public String getPlanID() {
		return planID;
	}
	public void setPlanID(String planID) {
		this.planID = planID;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public BigDecimal getGrossMonthlyPremium() {
		return grossMonthlyPremium;
	}
	public void setGrossMonthlyPremium(BigDecimal grossMonthlyPremium) {
		this.grossMonthlyPremium = grossMonthlyPremium;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getPlanTier() {
		return planTier;
	}
	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}
	public BigInteger getIndividualDeductible() {
		return individualDeductible;
	}
	public void setIndividualDeductible(BigInteger individualDeductible) {
		this.individualDeductible = individualDeductible;
	}
	public BigInteger getIndividualMaximumOutOfPocket() {
		return individualMaximumOutOfPocket;
	}
	public void setIndividualMaximumOutOfPocket(
			BigInteger individualMaximumOutOfPocket) {
		this.individualMaximumOutOfPocket = individualMaximumOutOfPocket;
	}
	public BigInteger getFamilyDeductible() {
		return familyDeductible;
	}
	public void setFamilyDeductible(BigInteger familyDeductible) {
		this.familyDeductible = familyDeductible;
	}
	public BigInteger getFamilyMaximumOutOfPocket() {
		return familyMaximumOutOfPocket;
	}
	public void setFamilyMaximumOutOfPocket(BigInteger familyMaximumOutOfPocket) {
		this.familyMaximumOutOfPocket = familyMaximumOutOfPocket;
	}
	public String getDoctorVisitCost() {
		return doctorVisitCost;
	}
	public void setDoctorVisitCost(String doctorVisitCost) {
		this.doctorVisitCost = doctorVisitCost;
	}
	public String getGenericPrescriptionCost() {
		return genericPrescriptionCost;
	}
	public void setGenericPrescriptionCost(String genericPrescriptionCost) {
		this.genericPrescriptionCost = genericPrescriptionCost;
	}
}
