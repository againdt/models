/**
 * 
 */
package com.getinsured.hix.planmgmt.config;

import com.getinsured.hix.platform.config.PropertiesEnumMarker;

/**
 * @author gorai_k
 *
 */
public class PlanmgmtConfiguration {

	public enum PlanmgmtConfigurationEnum implements PropertiesEnumMarker{
		
		QDP_SUPPORTPLANLEVEL ("planManagement.QDP.SupportPlanLevel"),
		QUOTING_CONSIDERONLYMEMBERPMPM ("planManagement.Quoting.ConsiderOnlyMemberPMPM"),
		QUOTING_CONSIDERMAXCHILDREN ("planManagement.Quoting.ConsiderMaxChildren"),
		QUOTING_CONSIDERCHILDAGE ("planManagement.Quoting.ConsiderChildAge"),
		PLAN_MANAGEQHP_PLANDETAILS_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQHP.PlanDetails.DisplayEditButton"),
		PLAN_MANAGEQHP_PLANBENEFITS_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQHP.PlanBenefits.DisplayEditButton"),
		PLAN_MANAGEQHP_PLANRATES_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQHP.PlanRates.DisplayEditButton"),
		PLAN_MANAGEQHP_PROVIDERNETWORK_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQHP.ProviderNetwork.DisplayEditButton"),
		PLAN_MANAGEQHP_DISPLAYADDNEWQHPBUTTON ("planManagement.Plan.ManageQHP.DisplayAddNewQHPButton"),
		PLAN_MANAGEQHP_DISPLAYADDNEWQHPSUBMENU ("planManagement.Plan.ManageQHP.DisplayAddNewQHPSubMenu"),
		PLAN_VIEWQHPDETAILS_DISPLAYPROVIDERNETWORKTAB ("planManagement.Plan.ViewQHPDetails.DisplayProviderNetworkTab"),
		PLAN_VIEWQHPDETAILS_DISPLAYACTION ("planManagement.Plan.ViewQHPDetails.DisplayAction"),
		PLAN_MANAGEQDP_PLANDETAILS_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQDP.PlanDetails.DisplayEditButton"),
		PLAN_MANAGEQDP_PLANBENEFITS_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQDP.PlanBenefits.DisplayEditButton"),
		PLAN_MANAGEQDP_PLANRATES_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQDP.PlanRates.DisplayEditButton"),
		PLAN_MANAGEQDP_PROVIDERNETWORK_DISPLAYEDITBUTTON ("planManagement.Plan.ManageQDP.ProviderNetwork.DisplayEditButton"),
		PLAN_MANAGEQDP_DISPLAYADDNEWQDPBUTTON ("planManagement.Plan.ManageQDP.DisplayAddNewQDPButton"),
		PLAN_MANAGEQDP_DISPLAYADDNEWQDPSUBMENU ("planManagement.Plan.ManageQDP.DisplayAddNewQDPSubMenu"),
		PLAN_VIEWQDPDETAILS_DISPLAYPROVIDERNETWORKTAB ("planManagement.Plan.ViewQDPDetails.DisplayProviderNetworkTab"),
		PLAN_VIEWQDPDETAILS_DISPLAYACTION ("planManagement.Plan.ViewQDPDetails.DisplayAction"),
		ISSUERPORTAL_DISPLAYSTARTBUTTON ("planManagement.IssuerPortal.DisplayStartButton"),
		COMPUTEISSUERQUALITYRATING ("planManagement.computeIssuerQualityRating"),//
		SHOWUPLOADQUALITYRATINGMENU ("planManagement.showUploadQualityRatingMenu"),
		COMPANYLOGOFILESIZE ("planManagement.CompanyLogoFileSize"),
		CERTSUPPDOCFILESIZE ("planManagement.certSuppDocFileSize"),//
		UHCAPPLICATIONID ("planManagement.uhcApplicationId"),
		QUOTITBROKERID ("planManagement.quotitBrokerId"),
		QUOTITWEBSITEACCESSKEY ("planManagement.quotitWebsiteAccessKey"),
		QUOTITREMOTEACCESSKEY ("planManagement.quotitRemoteAccessKey"),
		QUOTITREMOTESOURCEKEY ("planManagement.quotitRemoteSourceKey"),
		//HealthBenefits
		DENTAL_CHECKUP_SERVICES("planManagement.HealthBenefits.Benefit.DENTAL_CHECKUP_SERVICES"),
		PRIMARY_VISIT("planManagement.HealthBenefits.Benefit.PRIMARY_VISIT"),
		SPECIAL_VISIT("planManagement.HealthBenefits.Benefit.SPECIAL_VISIT"),
		OTHER_PRACTITIONER_VISIT("planManagement.HealthBenefits.Benefit.OTHER_PRACTITIONER_VISIT"),
		OTHER_PRACTITIONER_VISIT2("planManagement.HealthBenefits.Benefit2.OTHER_PRACTITIONER_VISIT"),
		OUTPATIENT_FACILITY_FEE("planManagement.HealthBenefits.Benefit.OUTPATIENT_FACILITY_FEE"),
		OUTPATIENT_FACILITY_FEE2("planManagement.HealthBenefits.Benefit2.OUTPATIENT_FACILITY_FEE"),
		OUTPATIENT_SURGERY_SERVICES("planManagement.HealthBenefits.Benefit.OUTPATIENT_SURGERY_SERVICES"),
		HOSPICE_SERVICE("planManagement.HealthBenefits.Benefit.HOSPICE_SERVICE"),
		NON_EMERGENCY_CARE("planManagement.HealthBenefits.Benefit.NON_EMERGENCY_CARE"),
		RTN_DENTAL_ADULT("planManagement.HealthBenefits.Benefit.RTN_DENTAL_ADULT"),
		INFERTILITY("planManagement.HealthBenefits.Benefit.INFERTILITY"),
		CUSTODIAL_NURSHING_CARE("planManagement.HealthBenefits.Benefit.CUSTODIAL_NURSHING_CARE"),
		PRIVATE_NURSING("planManagement.HealthBenefits.Benefit.PRIVATE_NURSING"),
		RTN_EYE_EXAM_ADULT("planManagement.HealthBenefits.Benefit.RTN_EYE_EXAM_ADULT"),
		URGENT_CARE("planManagement.HealthBenefits.Benefit.URGENT_CARE"),
		HOME_HEALTH_SERVICES("planManagement.HealthBenefits.Benefit.HOME_HEALTH_SERVICES"),
		EMERGENCY_SERVICES("planManagement.HealthBenefits.Benefit.EMERGENCY_SERVICES"),
		EMERGENCY_TRANSPORTATION("planManagement.HealthBenefits.Benefit.EMERGENCY_TRANSPORTATION"),
		INPATIENT_HOSPITAL_SERVICE("planManagement.HealthBenefits.Benefit.INPATIENT_HOSPITAL_SERVICE"),
		INPATIENT_PHY_SURGICAL_SERVICE("planManagement.HealthBenefits.Benefit.INPATIENT_PHY_SURGICAL_SERVICE"),
		BARIATRIC_SURGERY("planManagement.HealthBenefits.Benefit.BARIATRIC_SURGERY"),
		COSMETIC_SURGERY("planManagement.HealthBenefits.Benefit.COSMETIC_SURGERY"),
		SKILLED_NURSING_FACILITY("planManagement.HealthBenefits.Benefit.SKILLED_NURSING_FACILITY"),
		PRENATAL_POSTNATAL("planManagement.HealthBenefits.Benefit.PRENATAL_POSTNATAL"),
		DELIVERY_IMP_MATERNITY_SERVICES("planManagement.HealthBenefits.Benefit.DELIVERY_IMP_MATERNITY_SERVICES"),
		MENTAL_HEALTH_OUT("planManagement.HealthBenefits.Benefit.MENTAL_HEALTH_OUT"),
		MENTAL_HEALTH_IN("planManagement.HealthBenefits.Benefit.MENTAL_HEALTH_IN"),
		SUBSTANCE_OUTPATIENT_USE("planManagement.HealthBenefits.Benefit.SUBSTANCE_OUTPATIENT_USE"),
		SUBSTANCE_INPATIENT_USE("planManagement.HealthBenefits.Benefit.SUBSTANCE_INPATIENT_USE"),
		GENERIC("planManagement.HealthBenefits.Benefit.GENERIC"),
		PREFERRED_BRAND("planManagement.HealthBenefits.Benefit.PREFERRED_BRAND"),
		NON_PREFERRED_BRAND("planManagement.HealthBenefits.Benefit.NON_PREFERRED_BRAND"),
		SPECIALTY_DRUGS("planManagement.HealthBenefits.Benefit.SPECIALTY_DRUGS"),
		OUTPATIENT_REHAB_SERVICES("planManagement.HealthBenefits.Benefit.OUTPATIENT_REHAB_SERVICES"),
		HABILITATION("planManagement.HealthBenefits.Benefit.HABILITATION"),
		CHIROPRACTIC("planManagement.HealthBenefits.Benefit.CHIROPRACTIC"),
		DURABLE_MEDICAL_EQUIP("planManagement.HealthBenefits.Benefit.DURABLE_MEDICAL_EQUIP"),
		HEARING_AIDS("planManagement.HealthBenefits.Benefit.HEARING_AIDS"),
		IMAGING_SCAN("planManagement.HealthBenefits.Benefit.IMAGING_SCAN"),
		PREVENT_SCREEN_IMMU("planManagement.HealthBenefits.Benefit.PREVENT_SCREEN_IMMU"),
		RTN_FOOT_CARE("planManagement.HealthBenefits.Benefit.RTN_FOOT_CARE"),
		ACUPUNCTURE("planManagement.HealthBenefits.Benefit.ACUPUNCTURE"),
		WEIGHT_LOSS("planManagement.HealthBenefits.Benefit.WEIGHT_LOSS"),
		RTN_EYE_EXAM_CHILDREN("planManagement.HealthBenefits.Benefit.RTN_EYE_EXAM_CHILDREN"),
		GLASSES_CHILDREN("planManagement.HealthBenefits.Benefit.GLASSES_CHILDREN"),
		DENTAL_CHECKUP_CHILDREN("planManagement.HealthBenefits.Benefit.DENTAL_CHECKUP_CHILDREN"),
		REHABILITATIVE_SPEECH_THERAPY("planManagement.HealthBenefits.Benefit.REHABILITATIVE_SPEECH_THERAPY"),
		REHABILITATIVE_PHYSICAL_THERAPY("planManagement.HealthBenefits.Benefit.REHABILITATIVE_PHYSICAL_THERAPY"),
		WELL_BABY("planManagement.HealthBenefits.Benefit.WELL_BABY"),
		LABORATORY_SERVICES("planManagement.HealthBenefits.Benefit.LABORATORY_SERVICES"),
		IMAGING_XRAY("planManagement.HealthBenefits.Benefit.IMAGING_XRAY"),
		BASIC_DENTAL_CARE_CHILD("planManagement.HealthBenefits.Benefit.BASIC_DENTAL_CARE_CHILD"),
		MAJOR_DENTAL_CARE_CHILD("planManagement.HealthBenefits.Benefit.MAJOR_DENTAL_CARE_CHILD"),
		ORTHODONTIA_CHILD("planManagement.HealthBenefits.Benefit.ORTHODONTIA_CHILD"),
		BASIC_DENTAL_CARE_ADULT("planManagement.HealthBenefits.Benefit.BASIC_DENTAL_CARE_ADULT"),
		ORTHODONTIA_ADULT("planManagement.HealthBenefits.Benefit.ORTHODONTIA_ADULT"),
		MAJOR_DENTAL_CARE_ADULT("planManagement.HealthBenefits.Benefit.MAJOR_DENTAL_CARE_ADULT"),
		ABORTION("planManagement.HealthBenefits.Benefit.ABORTION"),
		TRANSPLANT("planManagement.HealthBenefits.Benefit.TRANSPLANT"),
		ACCIDENTAL_DENTAL("planManagement.HealthBenefits.Benefit.ACCIDENTAL_DENTAL"),
		DIALYSIS("planManagement.HealthBenefits.Benefit.DIALYSIS"),
		ALLERGY_TESTING("planManagement.HealthBenefits.Benefit.ALLERGY_TESTING"),
		CHEMOTHERAPY("planManagement.HealthBenefits.Benefit.CHEMOTHERAPY"),
		RADIATION("planManagement.HealthBenefits.Benefit.RADIATION"),
		DIABETES_EDUCATION("planManagement.HealthBenefits.Benefit.DIABETES_EDUCATION"),
		PROTHETIC_DEVICES("planManagement.HealthBenefits.Benefit.PROTHETIC_DEVICES"),
		INFUSION_THERAPY("planManagement.HealthBenefits.Benefit.INFUSION_THERAPY"),
		JOINT_DISORDER("planManagement.HealthBenefits.Benefit.JOINT_DISORDER"),
		NUTRITIONAL_COUNSELING("planManagement.HealthBenefits.Benefit.NUTRITIONAL_COUNSELING"),
		RECONSTRUCTIVE_SURGERY("planManagement.HealthBenefits.Benefit.RECONSTRUCTIVE_SURGERY"),
		CLINICAL_TRIALS("planManagement.HealthBenefits.Benefit.CLINICAL_TRIALS"),
		DIABETES_CARE("planManagement.HealthBenefits.Benefit.DIABETES_CARE"),
		METABOLIC_DISORDER("planManagement.HealthBenefits.Benefit.METABOLIC_DISORDER"),
		NEWBORN_HEARING_SCREEN("planManagement.HealthBenefits.Benefit.NEWBORN_HEARING_SCREEN"),
		OFF_LABEL_DRUGS("planManagement.HealthBenefits.Benefit.OFF_LABEL_DRUGS"),
		DENTAL_ANESTHESIA("planManagement.HealthBenefits.Benefit.DENTAL_ANESTHESIA"),
		PRESCRIPTION_DRUG_OTHER("planManagement.HealthBenefits.Benefit.PRESCRIPTION_DRUG_OTHER"),
		DIETHY("planManagement.HealthBenefits.Benefit.DIETHY"),
		ORGAN_TRANSPLANT("planManagement.HealthBenefits.Benefit.ORGAN_TRANSPLANT"),
		SECOND_OPINION("planManagement.HealthBenefits.Benefit.SECOND_OPINION"),
		REHABILITATIVE_OCC_THERAPY("planManagement.HealthBenefits.Benefit.REHABILITATIVE_OCC_THERAPY"),
		REHABILITATIVE_PHYSICAL_THERAPY2("planManagement.HealthBenefits.Benefit2.REHABILITATIVE_PHYSICAL_THERAPY"),
		ACCIDENTAL_DENTAL2("planManagement.DentalBenefits.Benefit.ACCIDENTAL_DENTAL"),
		BASIC_DENTAL_CARE_ADULT2("planManagement.DentalBenefits.Benefit.BASIC_DENTAL_CARE_ADULT"),
		BASIC_DENTAL_CARE_CHILD2("planManagement.DentalBenefits.Benefit.BASIC_DENTAL_CARE_CHILD"),
		DENTAL_CHECKUP_CHILDREN2("planManagement.DentalBenefits.Benefit.DENTAL_CHECKUP_CHILDREN"),
		MAJOR_DENTAL_CARE_CHILD2("planManagement.DentalBenefits.Benefit.MAJOR_DENTAL_CARE_CHILD"),
		MAJOR_DENTAL_CARE_ADULT2("planManagement.DentalBenefits.Benefit.MAJOR_DENTAL_CARE_ADULT"),
		ORTHODONTIA_ADULT2("planManagement.DentalBenefits.Benefit.ORTHODONTIA_ADULT"),
		ORTHODONTIA_CHILD2("planManagement.DentalBenefits.Benefit.ORTHODONTIA_CHILD"),
		RTN_DENTAL_ADULT2("planManagement.DentalBenefits.Benefit.RTN_DENTAL_ADULT"),
		DENTAL_DEDUCTIBLE("planManagement.DentalBenefits.Benefit.DENTAL_DEDUCTIBLE"),
		DENTAL_ANNUAL_MAX("planManagement.DentalBenefits.Benefit.DENTAL_ANNUAL_MAX"),
		ORTHODONTICS_MAX("planManagement.DentalBenefits.Benefit.ORTHODONTICS_MAX"),
		DENTAL_OOP_MAX("planManagement.DentalBenefits.Benefit.DENTAL_OOP_MAX"),
		WAITING_PERIOD("planManagement.DentalBenefits.Benefit.WAITING_PERIOD"),
		PREVENTIVE_SERVICES("planManagement.DentalBenefits.Benefit.PREVENTIVE_SERVICES"),
		BASIC_SERVICES("planManagement.DentalBenefits.Benefit.BASIC_SERVICES"),
		RESTORATIVE_SERVICES("planManagement.DentalBenefits.Benefit.RESTORATIVE_SERVICES"),
		DIAGNOSTIC_PREVENTIVE_CHILD("planManagement.DentalBenefits.Benefit.DIAGNOSTIC_PREVENTIVE_CHILD"),
		MAX_OOP_MEDICAL("planManagement.HealthCosts.Cost.MAX_OOP_MEDICAL"),
		MAX_OOP_DRUG("planManagement.HealthCosts.Cost.MAX_OOP_DRUG"),
		MAX_OOP_INTG_MED_DRUG("planManagement.HealthCosts.Cost.MAX_OOP_INTG_MED_DRUG"),
		DEDUCTIBLE_MEDICAL("planManagement.HealthCosts.Cost.DEDUCTIBLE_MEDICAL"),
		DEDUCTIBLE_DRUG("planManagement.HealthCosts.Cost.DEDUCTIBLE_DRUG"),
		DEDUCTIBLE_INTG_MED_DRUG("planManagement.HealthCosts.Cost.DEDUCTIBLE_INTG_MED_DRUG"),
		DEDUCTIBLE_BRAND_NAME_DRUG("planManagement.HealthCosts.Cost.DEDUCTIBLE_BRAND_NAME_DRUG"),
		MAX_OOP_MEDICAL2("planManagement.DentalCosts.Cost.MAX_OOP_MEDICAL"),
		MAX_OOP_DRUG2("planManagement.DentalCosts.Cost.MAX_OOP_DRUG"),
		MAX_OOP_INTG_MED_DRUG2("planManagement.DentalCosts.Cost.MAX_OOP_INTG_MED_DRUG"),
		DEDUCTIBLE_MEDICAL2("planManagement.DentalCosts.Cost.DEDUCTIBLE_MEDICAL"),
		DEDUCTIBLE_DRUG2("planManagement.DentalCosts.Cost.DEDUCTIBLE_DRUG"),
		DEDUCTIBLE_INTG_MED_DRUG2("planManagement.DentalCosts.Cost.DEDUCTIBLE_INTG_MED_DRUG"),
		DEDUCTIBLE_BRAND_NAME_DRUG2("planManagement.DentalCosts.Cost.DEDUCTIBLE_BRAND_NAME_DRUG"),
		DEDUCTIBLE_DENTAL_CHILD("planManagement.DentalCosts.Cost.DEDUCTIBLE_DENTAL_CHILD"),
		DEDUCTIBLE_DENTAL_ADULT("planManagement.DentalCosts.Cost.DEDUCTIBLE_DENTAL_ADULT"),
		DEDUCTIBLE_DENTAL_ADULT2("planManagement.DentalCosts.Cost2.DEDUCTIBLE_DENTAL_ADULT"),
		MAX_OOP_DENTAL_CHILD("planManagement.DentalCosts.Cost.MAX_OOP_DENTAL_CHILD"),
		MAX_OOP_DENTAL_ADULT("planManagement.DentalCosts.Cost.MAX_OOP_DENTAL_ADULT"),
		COMPUTE_NETWORK_TRANSPARENCY_RATING ("planManagement.computeNetworkTransparencyRating"),
		
		NOTIFYTOISSUER("planManagement.planNotificationActivation.notifyToIssuer"),
		TURNONOROFFSSAP("planManagement.TurnOnOrOffSSAP"),

		// Medicaid properties - START
		DOCTOR_VISITS_PER_YEAR("serff.medicaid.DOCTOR_VISITS_PER_YEAR"),
		PHARMACY_MEDICATIONS("serff.medicaid.PHARMACY_MEDICATIONS"),
		HOSPITAL_STAY("serff.medicaid.HOSPITAL_STAY"),
		HOME_HEALTH_CARE("serff.medicaid.HOME_HEALTH_CARE"),
		DURABLE_MED_EQPMNT("serff.medicaid.DURABLE_MED_EQPMNT"),
		DENTAL("serff.medicaid.DENTAL"),
		ORAL_SURGERY("serff.medicaid.ORAL_SURGERY"),
		VISION("serff.medicaid.VISION"),
		EXTRA_SERVICE("serff.medicaid.EXTRA_SERVICE"),
		ADDITIONAL_SERVICE("serff.medicaid.ADDITIONAL_SERVICE"),
		// Medicaid properties - END

		// Medicare properties - START
		DRUG_DEDUCTIBLE("planManagement.MedicareBenefits.Benefit.DRUG_DEDUCTIBLE"),
		MONTHLY_PREMIUM("planManagement.MedicareBenefits.Benefit.MONTHLY_PREMIUM"),
		DRUG_COVERAGE_GAP("planManagement.MedicareBenefits.Benefit.DRUG_COVERAGE_GAP"),
		OVERALL_PLAN_RATING("planManagement.MedicareBenefits.Benefit.OVERALL_PLAN_RATING"),
		RX_COVERED("planManagement.MedicareBenefits.Benefit.RX_COVERED"),
		MAX_ANNUAL_COPAY("planManagement.MedicareBenefits.Benefit.MAX_ANNUAL_COPAY"),
		// Medicare properties - END

		// Load Plan Properties from SERFF - START
		MEDICAID_LIST("serff.medicaid"),
		HEALTH_COSTS_LIST("planManagement.HealthCosts.Cost"),
		DENTAL_COSTS_LIST("planManagement.DentalCosts.Cost"),
		HEALTH_BENEFITS_LIST("planManagement.HealthBenefits.Benefit"),
		DENTAL_BENEFITS_LIST("planManagement.DentalBenefits.Benefit"),
		MEDICARE_BENEFITS_LIST("planManagement.MedicareBenefits.Benefit"),
		// Load Plan Properties from SERFF - END

		SEND_BULK_UPDATE_NOTIFICATION("planManagement.sendBlukUpdateNotification"),
		//HIX-34749
		QUOTING_SMOKINGAGE ("planManagement.Quoting.SmokingAge"),
		CONSIDER_EHB_PORTION("planManagement.considerStateEHBPortion"),
		// As per HIX-55897 adding configuration for broker notification
		SEND_NOTIFICATION_TO_BROKER("planManagement.notifyBrokers"),
		QUOTING_CONSIDERMAXCHILDREN_QDP("planManagement.Quoting.QDPConsiderMaxChildren"),
		
		// As per HIX-54044 adding configurations
		IHC_IHCHPACODE("planManagement.ihc.IHCHPACODE"),
		ASSURANT_I_MQUOTING_TEST("planManagement.assurant.I_MQUOTING_TEST"),
		ASSURANT_PASSWORD("planManagement.assurant.PASSWORD"),
		ASSURANT_AGENT_NUMBER("planManagement.assurant.AGENT_NUMBER"),
		
		SHOW_MEDICARE_ISSUERS("planManagement.ShowMedicareIssuers"),
		SHOW_MEDICARE_PLANS("planManagement.ShowMedicarePlans"),
		MEDICARE_PLANS_SOURCE("planManagement.MedicareDefaultSource"),
		QUOTING_CONSIDER_CHILD_AGE_FOR_DENTAL("planManagement.Quoting.DentalAgeToLimitNumberOfChildren"),
		QUOTING_AGE_FOR_DENTAL_CHILD_ONLY_PLANS("planManagement.Quoting.AgeForDentalChildOnlyPlans"),
		ISSUER_PORTAL_DISABLE_EDIT_FEATURE("planManagement.IssuerPortal.DisableEditFeature"),
		
		/* HIX-81569 : new configuration for displaying terminate enrollment button on edit plan status page */
		TERMINATE_ENROLLMENT_FEATURE("planManagement.terminateEnrollmentFeature");
		
		
		private final String value;	  
		public String getValue(){return this.value;}
		
		PlanmgmtConfigurationEnum(String value) {
			this.value = value;
		}
	};

}
