package com.getinsured.hix.dto.broker;

public class IndividualHouseholdDTO {

	private Integer responseCode;
	private String responseDesc;
	private Long individualID;
	private Integer numberOfHouseholdMembers;
	private String headOfHouseHoldName;
	private String enrollStatus;
	private String eligStatus;
	private Double houseIncome;

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public Long getIndividualID() {
		return individualID;
	}

	public void setIndividualID(Long individualID) {
		this.individualID = individualID;
	}

	public Integer getNumberOfHouseholdMembers() {
		return numberOfHouseholdMembers;
	}

	public void setNumberOfHouseholdMembers(Integer numberOfHouseholdMembers) {
		this.numberOfHouseholdMembers = numberOfHouseholdMembers;
	}

	public String getHeadOfHouseHoldName() {
		return headOfHouseHoldName;
	}

	public void setHeadOfHouseHoldName(String headOfHouseHoldName) {
		this.headOfHouseHoldName = headOfHouseHoldName;
	}

	public String getEnrollStatus() {
		return enrollStatus;
	}

	public void setEnrollStatus(String enrollStatus) {
		this.enrollStatus = enrollStatus;
	}

	public String getEligStatus() {
		return eligStatus;
	}

	public void setEligStatus(String eligStatus) {
		this.eligStatus = eligStatus;
	}

	public Double getHouseIncome() {
		return houseIncome;
	}

	public void setHouseIncome(Double houseIncome) {
		this.houseIncome = houseIncome;
	}

}
