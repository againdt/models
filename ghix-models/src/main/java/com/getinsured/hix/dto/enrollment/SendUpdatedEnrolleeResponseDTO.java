/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @author panda_p
 *
 */
public class SendUpdatedEnrolleeResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6889496580327811058L;
	
	private String enrolleeId;
	private String exchgIndivIdentifier;
	private String ahbxStatusCode;
	private String ahbxStatusDescription;
	private String enrolleeStatus;
	
	
	public String getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(String enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public String getAhbxStatusCode() {
		return ahbxStatusCode;
	}
	public void setAhbxStatusCode(String ahbxStatusCode) {
		this.ahbxStatusCode = ahbxStatusCode;
	}
	public String getAhbxStatusDescription() {
		return ahbxStatusDescription;
	}
	public void setAhbxStatusDescription(String ahbxStatusDescription) {
		this.ahbxStatusDescription = ahbxStatusDescription;
	}
	public String getEnrolleeStatus() {
		return enrolleeStatus;
	}
	public void setEnrolleeStatus(String enrolleeStatus) {
		this.enrolleeStatus = enrolleeStatus;
	}
	
	@Override
	public String toString() {
		return "SendUpdatedEnrolleeResponseDTO [enrolleeId=" + enrolleeId
				+ ", exchgIndivIdentifier=" + exchgIndivIdentifier
				+ ", ahbxStatusCode=" + ahbxStatusCode
				+ ", ahbxStatusDescription=" + ahbxStatusDescription
				+ ", enrolleeStatus=" + enrolleeStatus + "]";
	}
	
	
	
}
