package com.getinsured.hix.model.enrollment;

import java.util.Date;

/**
 * 
 * @author meher_a
 * used to convert csv data to java object
 *
 */

public class ReconDetailCsvDto {
	
	private String recordCode;
	private String tradingPartnerId;
	private String spoeId;
	private String tenantId;
	private String hiosId;
	private String qhpidLookupKey;
	private String issuerExtractDate;
	private String issuerExtractTime;
	private String qiFirstName;
	private String qiMiddleName;
	private String qiLastName;
	private String qiBirthDate;
	private String qiGender;
	private String qiSocialSecurityNumber;
	private String subscriberIndicator;
	private String individualRelationshipCode;
	private String exchangeAssignedSubscriberId;
	private String exchangeAssignedMemberId;
	private String issuerAssignedSubscriberId;
	private String issuerAssignedMemberId;
	private String exchangeAssignedPolicyNumber;
	private String issuerAssignedPolicyId;
	private String residentialAddressLine1;
	private String residentialAddressLine2;
	private String residentialCityName;
	private String residentialStateCode;
	private String residentialZipCode;
	private String mailingAddressLine1;
	private String mailingAddressLine2;
	private String mailingAddressCity;
	private String mailingAddressStateCode;
	private String mailingAddressZipCode;
	private String residentialCountyCode;
	private String ratingArea;
	private String telephoneNumber;
	private String tobaccoUseCode;
	private String qhpIdentifier;
	private String benefitStartDate;
	private String benefitEndDate;
	private String appliedAptcAmount;
	private String appliedAptcEffectiveDate;
	private String appliedAptcEndDate;
	private String csrAmount;
	private String csrEffectiveDate;
	private String csrEndDate;
	private String totalPremiumAmount;
	private String totalPremiumEffectiveDate;
	private String totalPremiumEndDate;
	private String individualPremiumAmount;
	private String individualPremiumEffectiveDate;
	private String individualPremiumEndDate;
	private String initialPremiumPaidStatus;
	private String issuerAssignedRecordTraceNumber;
	private String coverageYear;
	private String paidThroughDate;
	private String endOfYearTerminationIndicator;
	private String agentBrokerName;
	private String agentBrokerNpn;
	
	private Date coverageStartDate;
	private Date coverageEndDate;
	
	public String getRecordCode() {
		return recordCode;
	}
	public void setRecordCode(String recordCode) {
		this.recordCode = returnNullIfEmpty(recordCode);
	}
	public String getTradingPartnerId() {
		return tradingPartnerId;
	}
	public void setTradingPartnerId(String tradingPartnerId) {
		this.tradingPartnerId = returnNullIfEmpty(tradingPartnerId);
	}
	public String getSpoeId() {
		return spoeId;
	}
	public void setSpoeId(String spoeId) {
		this.spoeId = returnNullIfEmpty(spoeId);
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = returnNullIfEmpty(tenantId);
	}
	public String getHiosId() {
		return hiosId;
	}
	public void setHiosId(String hiosId) {
		this.hiosId = returnNullIfEmpty(hiosId);
	}
	public String getQhpidLookupKey() {
		return qhpidLookupKey;
	}
	public void setQhpidLookupKey(String qhpidLookupKey) {
		this.qhpidLookupKey = returnNullIfEmpty(qhpidLookupKey);
	}
	public String getIssuerExtractDate() {
		return issuerExtractDate;
	}
	public void setIssuerExtractDate(String issuerExtractDate) {
		this.issuerExtractDate = returnNullIfEmpty(issuerExtractDate);
	}
	public String getIssuerExtractTime() {
		return issuerExtractTime;
	}
	public void setIssuerExtractTime(String issuerExtractTime) {
		this.issuerExtractTime = returnNullIfEmpty(issuerExtractTime);
	}
	public String getQiFirstName() {
		return qiFirstName;
	}
	public void setQiFirstName(String qiFirstName) {
		this.qiFirstName = returnNullIfEmpty(qiFirstName);
	}
	public String getQiMiddleName() {
		return qiMiddleName;
	}
	public void setQiMiddleName(String qiMiddleName) {
		this.qiMiddleName = returnNullIfEmpty(qiMiddleName);
	}
	public String getQiLastName() {
		return qiLastName;
	}
	public void setQiLastName(String qiLastName) {
		this.qiLastName = returnNullIfEmpty(qiLastName);
	}
	public String getQiBirthDate() {
		return qiBirthDate;
	}
	public void setQiBirthDate(String qiBirthDate) {
		this.qiBirthDate = returnNullIfEmpty(qiBirthDate);
	}
	public String getQiGender() {
		return qiGender;
	}
	public void setQiGender(String qiGender) {
		this.qiGender = returnNullIfEmpty(qiGender);
	}
	public String getQiSocialSecurityNumber() {
		return qiSocialSecurityNumber;
	}
	public void setQiSocialSecurityNumber(String qiSocialSecurityNumber) {
		this.qiSocialSecurityNumber = returnNullIfEmpty(qiSocialSecurityNumber);
	}
	public String getSubscriberIndicator() {
		return subscriberIndicator;
	}
	public void setSubscriberIndicator(String subscriberIndicator) {
		this.subscriberIndicator = returnNullIfEmpty(subscriberIndicator);
	}
	public String getIndividualRelationshipCode() {
		return individualRelationshipCode;
	}
	public void setIndividualRelationshipCode(String individualRelationshipCode) {
		this.individualRelationshipCode = returnNullIfEmpty(individualRelationshipCode);
	}
	public String getExchangeAssignedSubscriberId() {
		return exchangeAssignedSubscriberId;
	}
	public void setExchangeAssignedSubscriberId(String exchangeAssignedSubscriberId) {
		this.exchangeAssignedSubscriberId = returnNullIfEmpty(exchangeAssignedSubscriberId);
	}
	public String getExchangeAssignedMemberId() {
		return exchangeAssignedMemberId;
	}
	public void setExchangeAssignedMemberId(String exchangeAssignedMemberId) {
		this.exchangeAssignedMemberId = returnNullIfEmpty(exchangeAssignedMemberId);
	}
	public String getIssuerAssignedSubscriberId() {
		return issuerAssignedSubscriberId;
	}
	public void setIssuerAssignedSubscriberId(String issuerAssignedSubscriberId) {
		this.issuerAssignedSubscriberId = returnNullIfEmpty(issuerAssignedSubscriberId);
	}
	public String getIssuerAssignedMemberId() {
		return issuerAssignedMemberId;
	}
	public void setIssuerAssignedMemberId(String issuerAssignedMemberId) {
		this.issuerAssignedMemberId = returnNullIfEmpty(issuerAssignedMemberId);
	}
	public String getExchangeAssignedPolicyNumber() {
		return exchangeAssignedPolicyNumber;
	}
	public void setExchangeAssignedPolicyNumber(String exchangeAssignedPolicyNumber) {
		this.exchangeAssignedPolicyNumber = returnNullIfEmpty(exchangeAssignedPolicyNumber);
	}
	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}
	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = returnNullIfEmpty(issuerAssignedPolicyId);
	}
	public String getResidentialAddressLine1() {
		return residentialAddressLine1;
	}
	public void setResidentialAddressLine1(String residentialAddressLine1) {
		this.residentialAddressLine1 = returnNullIfEmpty(residentialAddressLine1);
	}
	public String getResidentialAddressLine2() {
		return residentialAddressLine2;
	}
	public void setResidentialAddressLine2(String residentialAddressLine2) {
		this.residentialAddressLine2 = returnNullIfEmpty(residentialAddressLine2);
	}
	public String getResidentialCityName() {
		return residentialCityName;
	}
	public void setResidentialCityName(String residentialCityName) {
		this.residentialCityName = returnNullIfEmpty(residentialCityName);
	}
	public String getResidentialStateCode() {
		return residentialStateCode;
	}
	public void setResidentialStateCode(String residentialStateCode) {
		this.residentialStateCode = returnNullIfEmpty(residentialStateCode);
	}
	public String getResidentialZipCode() {
		return residentialZipCode;
	}
	public void setResidentialZipCode(String residentialZipCode) {
		this.residentialZipCode = returnNullIfEmpty(residentialZipCode);
	}
	public String getMailingAddressLine1() {
		return mailingAddressLine1;
	}
	public void setMailingAddressLine1(String mailingAddressLine1) {
		this.mailingAddressLine1 = returnNullIfEmpty(mailingAddressLine1);
	}
	public String getMailingAddressLine2() {
		return mailingAddressLine2;
	}
	public void setMailingAddressLine2(String mailingAddressLine2) {
		this.mailingAddressLine2 = returnNullIfEmpty(mailingAddressLine2);
	}
	public String getMailingAddressCity() {
		return mailingAddressCity;
	}
	public void setMailingAddressCity(String mailingAddressCity) {
		this.mailingAddressCity = returnNullIfEmpty(mailingAddressCity);
	}
	public String getMailingAddressStateCode() {
		return mailingAddressStateCode;
	}
	public void setMailingAddressStateCode(String mailingAddressStateCode) {
		this.mailingAddressStateCode = returnNullIfEmpty(mailingAddressStateCode);
	}
	public String getMailingAddressZipCode() {
		return mailingAddressZipCode;
	}
	public void setMailingAddressZipCode(String mailingAddressZipCode) {
		this.mailingAddressZipCode = returnNullIfEmpty(mailingAddressZipCode);
	}
	public String getResidentialCountyCode() {
		return residentialCountyCode;
	}
	public void setResidentialCountyCode(String residentialCountyCode) {
		this.residentialCountyCode = returnNullIfEmpty(residentialCountyCode);
	}
	public String getRatingArea() {
		return ratingArea;
	}
	public void setRatingArea(String ratingArea) {
		this.ratingArea = returnNullIfEmpty(ratingArea);
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = returnNullIfEmpty(telephoneNumber);
	}
	public String getTobaccoUseCode() {
		return tobaccoUseCode;
	}
	public void setTobaccoUseCode(String tobaccoUseCode) {
		this.tobaccoUseCode = returnNullIfEmpty(tobaccoUseCode);
	}
	public String getQhpIdentifier() {
		return qhpIdentifier;
	}
	public void setQhpIdentifier(String qhpIdentifier) {
		this.qhpIdentifier = returnNullIfEmpty(qhpIdentifier);
	}
	public String getBenefitStartDate() {
		return benefitStartDate;
	}
	public void setBenefitStartDate(String benefitStartDate) {
		this.benefitStartDate = returnNullIfEmpty(benefitStartDate);
	}
	public String getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(String benefitEndDate) {
		this.benefitEndDate = returnNullIfEmpty(benefitEndDate);
	}
	public String getAppliedAptcAmount() {
		return appliedAptcAmount;
	}
	public void setAppliedAptcAmount(String appliedAptcAmount) {
		this.appliedAptcAmount = returnNullIfEmpty(appliedAptcAmount);
	}
	public String getAppliedAptcEffectiveDate() {
		return appliedAptcEffectiveDate;
	}
	public void setAppliedAptcEffectiveDate(String appliedAptcEffectiveDate) {
		this.appliedAptcEffectiveDate = returnNullIfEmpty(appliedAptcEffectiveDate);
	}
	public String getAppliedAptcEndDate() {
		return appliedAptcEndDate;
	}
	public void setAppliedAptcEndDate(String appliedAptcEndDate) {
		this.appliedAptcEndDate = returnNullIfEmpty(appliedAptcEndDate);
	}
	public String getCsrAmount() {
		return csrAmount;
	}
	public void setCsrAmount(String csrAmount) {
		this.csrAmount = returnNullIfEmpty(csrAmount);
	}
	public String getCsrEffectiveDate() {
		return csrEffectiveDate;
	}
	public void setCsrEffectiveDate(String csrEffectiveDate) {
		this.csrEffectiveDate = returnNullIfEmpty(csrEffectiveDate);
	}
	public String getCsrEndDate() {
		return csrEndDate;
	}
	public void setCsrEndDate(String csrEndDate) {
		this.csrEndDate = returnNullIfEmpty(csrEndDate);
	}
	public String getTotalPremiumAmount() {
		return totalPremiumAmount;
	}
	public void setTotalPremiumAmount(String totalPremiumAmount) {
		this.totalPremiumAmount = returnNullIfEmpty(totalPremiumAmount);
	}
	public String getTotalPremiumEffectiveDate() {
		return totalPremiumEffectiveDate;
	}
	public void setTotalPremiumEffectiveDate(String totalPremiumEffectiveDate) {
		this.totalPremiumEffectiveDate = returnNullIfEmpty(totalPremiumEffectiveDate);
	}
	public String getTotalPremiumEndDate() {
		return totalPremiumEndDate;
	}
	public void setTotalPremiumEndDate(String totalPremiumEndDate) {
		this.totalPremiumEndDate = returnNullIfEmpty(totalPremiumEndDate);
	}
	public String getIndividualPremiumAmount() {
		return individualPremiumAmount;
	}
	public void setIndividualPremiumAmount(String individualPremiumAmount) {
		this.individualPremiumAmount = returnNullIfEmpty(individualPremiumAmount);
	}
	public String getIndividualPremiumEffectiveDate() {
		return individualPremiumEffectiveDate;
	}
	public void setIndividualPremiumEffectiveDate(String individualPremiumEffectiveDate) {
		this.individualPremiumEffectiveDate = returnNullIfEmpty(individualPremiumEffectiveDate);
	}
	public String getIndividualPremiumEndDate() {
		return individualPremiumEndDate;
	}
	public void setIndividualPremiumEndDate(String individualPremiumEndDate) {
		this.individualPremiumEndDate = returnNullIfEmpty(individualPremiumEndDate);
	}
	public String getInitialPremiumPaidStatus() {
		return initialPremiumPaidStatus;
	}
	public void setInitialPremiumPaidStatus(String initialPremiumPaidStatus) {
		this.initialPremiumPaidStatus = returnNullIfEmpty(initialPremiumPaidStatus);
	}
	public String getIssuerAssignedRecordTraceNumber() {
		return issuerAssignedRecordTraceNumber;
	}
	public void setIssuerAssignedRecordTraceNumber(String issuerAssignedRecordTraceNumber) {
		this.issuerAssignedRecordTraceNumber = returnNullIfEmpty(issuerAssignedRecordTraceNumber);
	}
	public String getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = returnNullIfEmpty(coverageYear);
	}
	public String getPaidThroughDate() {
		return paidThroughDate;
	}
	public void setPaidThroughDate(String paidThroughDate) {
		this.paidThroughDate = returnNullIfEmpty(paidThroughDate);
	}
	public String getEndOfYearTerminationIndicator() {
		return endOfYearTerminationIndicator;
	}
	public void setEndOfYearTerminationIndicator(String endOfYearTerminationIndicator) {
		this.endOfYearTerminationIndicator = returnNullIfEmpty(endOfYearTerminationIndicator);
	}
	
	
	public String getAgentBrokerName() {
		return agentBrokerName;
	}
	public void setAgentBrokerName(String agentBrokerName) {
		this.agentBrokerName = agentBrokerName;
	}
	public String getAgentBrokerNpn() {
		return agentBrokerNpn;
	}
	public void setAgentBrokerNpn(String agentBrokerNpn) {
		this.agentBrokerNpn = returnNullIfEmpty(agentBrokerNpn);
	}
	
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public Date getCoverageEndDate() {
		return coverageEndDate;
	}
	public void setCoverageEndDate(Date coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	
	private String returnNullIfEmpty(String value){
		if(null != value && !value.trim().isEmpty()){
			return value.trim();
		}else{
			return null;
		}
	}
	
	@Override
	public String toString() {
		return "ReconDetailCsvDto [recordCode=" + recordCode
				+ ", tradingPartnerId=" + tradingPartnerId + ", spoeId="
				+ spoeId + ", tenantId=" + tenantId + ", hiosId=" + hiosId
				+ ", qhpidLookupKey=" + qhpidLookupKey + ", issuerExtractDate="
				+ issuerExtractDate + ", issuerExtractTime="
				+ issuerExtractTime + ", qiFirstName=" + qiFirstName
				+ ", qiMiddleName=" + qiMiddleName + ", qiLastName="
				+ qiLastName + ", qiBirthDate=" + qiBirthDate + ", qiGender="
				+ qiGender + ", qiSocialSecurityNumber="
				+ qiSocialSecurityNumber + ", subscriberIndicator="
				+ subscriberIndicator + ", individualRelationshipCode="
				+ individualRelationshipCode
				+ ", exchangeAssignedSubscriberId="
				+ exchangeAssignedSubscriberId + ", exchangeAssignedMemberId="
				+ exchangeAssignedMemberId + ", issuerAssignedSubscriberId="
				+ issuerAssignedSubscriberId + ", issuerAssignedMemberId="
				+ issuerAssignedMemberId + ", exchangeAssignedPolicyNumber="
				+ exchangeAssignedPolicyNumber + ", issuerAssignedPolicyId="
				+ issuerAssignedPolicyId + ", residentialAddressLine1="
				+ residentialAddressLine1 + ", residentialAddressLine2="
				+ residentialAddressLine2 + ", residentialCityName="
				+ residentialCityName + ", residentialStateCode="
				+ residentialStateCode + ", residentialZipCode="
				+ residentialZipCode + ", mailingAddressLine1="
				+ mailingAddressLine1 + ", mailingAddressLine2="
				+ mailingAddressLine2 + ", mailingAddressCity="
				+ mailingAddressCity + ", mailingAddressStateCode="
				+ mailingAddressStateCode + ", mailingAddressZipCode="
				+ mailingAddressZipCode + ", residentialCountyCode="
				+ residentialCountyCode + ", ratingArea=" + ratingArea
				+ ", telephoneNumber=" + telephoneNumber + ", tobaccoUseCode="
				+ tobaccoUseCode + ", qhpIdentifier=" + qhpIdentifier
				+ ", benefitStartDate=" + benefitStartDate
				+ ", benefitEndDate=" + benefitEndDate + ", appliedAptcAmount="
				+ appliedAptcAmount + ", appliedAptcEffectiveDate="
				+ appliedAptcEffectiveDate + ", appliedAptcEndDate="
				+ appliedAptcEndDate + ", csrAmount=" + csrAmount
				+ ", csrEffectiveDate=" + csrEffectiveDate + ", csrEndDate="
				+ csrEndDate + ", totalPremiumAmount=" + totalPremiumAmount
				+ ", totalPremiumEffectiveDate=" + totalPremiumEffectiveDate
				+ ", totalPremiumEndDate=" + totalPremiumEndDate
				+ ", individualPremiumAmount=" + individualPremiumAmount
				+ ", individualPremiumEffectiveDate="
				+ individualPremiumEffectiveDate
				+ ", individualPremiumEndDate=" + individualPremiumEndDate
				+ ", initialPremiumPaidStatus=" + initialPremiumPaidStatus
				+ ", issuerAssignedRecordTraceNumber="
				+ issuerAssignedRecordTraceNumber + ", coverageYear="
				+ coverageYear + ", paidThroughDate=" + paidThroughDate
				+ ", endOfYearTerminationIndicator="
				+ endOfYearTerminationIndicator + ", agentBrokerFirstName="
				+ agentBrokerName + ",  agentBrokerNpn=" + agentBrokerNpn
				+ "]";
		}
		
		
		public String toCsv() {
			String csv = recordCode + "," 
		            + tradingPartnerId + ","
					+ spoeId + "," 
		            + tenantId + "," 
					+ hiosId + "," 
		            + qhpidLookupKey + ","
					+ issuerExtractDate + ","
					+ issuerExtractTime + "," 
					+ qiFirstName+ "," 
					+ qiMiddleName + ","
					+ qiLastName + "," 
					+ qiBirthDate + ","
					+ qiGender + ","
					+ qiSocialSecurityNumber + ","
					+ subscriberIndicator + ","
					+ individualRelationshipCode+ ","
					+ exchangeAssignedSubscriberId + ","
					+ exchangeAssignedMemberId + ","
					+ issuerAssignedSubscriberId + ","
					+ issuerAssignedMemberId + ","
					+ exchangeAssignedPolicyNumber + ","
					+ issuerAssignedPolicyId + ","
					+ residentialAddressLine1 + ","
					+ residentialAddressLine2 + ","
					+ residentialCityName + ","
					+ residentialStateCode + ","
					+ residentialZipCode + ","
					+ mailingAddressLine1 + ","
					+ mailingAddressLine2 + ","
					+ mailingAddressCity + ","
					+ mailingAddressStateCode + ","
					+ mailingAddressZipCode + ","
					+ residentialCountyCode + "," 
					+ ratingArea+ "," 
					+ telephoneNumber + ","
					+ tobaccoUseCode + "," 
					+ qhpIdentifier+ "," 
					+ benefitStartDate+ "," 
					+ benefitEndDate + ","
					+ appliedAptcAmount + ","
					+ appliedAptcEffectiveDate + ","
					+ appliedAptcEndDate + "," 
					+ csrAmount+ "," 
					+ csrEffectiveDate + ","
					+ csrEndDate + "," 
					+ totalPremiumAmount+ "," 
					+ totalPremiumEffectiveDate+ "," 
					+ totalPremiumEndDate+ "," 
					+ individualPremiumAmount+ ","
					+ individualPremiumEffectiveDate+ "," 
					+ individualPremiumEndDate+ "," 
					+ initialPremiumPaidStatus+ ","
					+ issuerAssignedRecordTraceNumber + ","
					+ coverageYear + "," 
					+ paidThroughDate+ ","
					+ endOfYearTerminationIndicator + ","
					+ agentBrokerName + ","
					+ agentBrokerNpn;
			
		return  csv.replaceAll("null", "");
	}
	
		public String toCsv(char separator) {
			String csv =  recordCode + separator
		            + tradingPartnerId +separator
					+ spoeId +separator 
		            + tenantId +separator 
					+ hiosId +separator 
		            + qhpidLookupKey +separator
					+ issuerExtractDate +separator
					+ issuerExtractTime +separator 
					+ qiFirstName+separator 
					+ qiMiddleName +separator
					+ qiLastName +separator 
					+ qiBirthDate +separator
					+ qiGender +separator
					+ qiSocialSecurityNumber +separator
					+ subscriberIndicator +separator
					+ individualRelationshipCode+separator
					+ exchangeAssignedSubscriberId +separator
					+ exchangeAssignedMemberId +separator
					+ issuerAssignedSubscriberId +separator
					+ issuerAssignedMemberId +separator
					+ exchangeAssignedPolicyNumber +separator
					+ issuerAssignedPolicyId +separator
					+ residentialAddressLine1 +separator
					+ residentialAddressLine2 +separator
					+ residentialCityName +separator
					+ residentialStateCode +separator
					+ residentialZipCode +separator
					+ mailingAddressLine1 +separator
					+ mailingAddressLine2 +separator
					+ mailingAddressCity +separator
					+ mailingAddressStateCode +separator
					+ mailingAddressZipCode +separator
					+ residentialCountyCode +separator 
					+ ratingArea+separator 
					+ telephoneNumber +separator
					+ tobaccoUseCode +separator 
					+ qhpIdentifier+separator 
					+ benefitStartDate+separator 
					+ benefitEndDate +separator
					+ appliedAptcAmount +separator
					+ appliedAptcEffectiveDate +separator
					+ appliedAptcEndDate +separator 
					+ csrAmount+separator 
					+ csrEffectiveDate +separator
					+ csrEndDate +separator 
					+ totalPremiumAmount+separator 
					+ totalPremiumEffectiveDate+separator 
					+ totalPremiumEndDate+separator 
					+ individualPremiumAmount+separator
					+ individualPremiumEffectiveDate+separator 
					+ individualPremiumEndDate+separator 
					+ initialPremiumPaidStatus+separator
					+ issuerAssignedRecordTraceNumber +separator
					+ coverageYear +separator 
					+ paidThroughDate+separator
					+ endOfYearTerminationIndicator +separator
					+ agentBrokerName +separator
					+ agentBrokerNpn;
			return  csv.replaceAll("null", "");
	}
}
