package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GHIXResponse;
import com.google.gson.Gson;

//import lombok.Getter;
//import lombok.Setter;

//@Getter
//@Setter
public class PlanRequestDTO implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String planName;
	private String issuerPlanNumber;
	private String planStatus;
	private String issuerVerificationStatus;
	private String networkUrl;
	private String enrollAvailability;
	private String enrollAvailEffectiveDate;
	private String formularyUrl;
	private String sbcDocUrl;
	private String nonCommissionFlag;
	private int planYear;
	private int commentId;
	private String supportFile;
	private int updatedBy;
	
	

	public PlanRequestDTO(){
		 
	}
	
	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

	public String getIssuerVerificationStatus() {
		return issuerVerificationStatus;
	}

	public void setIssuerVerificationStatus(String issuerVerificationStatus) {
		this.issuerVerificationStatus = issuerVerificationStatus;
	}

	public String getNetworkUrl() {
		return networkUrl;
	}

	public void setNetworkUrl(String networkUrl) {
		this.networkUrl = networkUrl;
	}

	public String getEnrollAvailability() {
		return enrollAvailability;
	}

	public void setEnrollAvailability(String enrollAvailability) {
		this.enrollAvailability = enrollAvailability;
	}

	public String getEnrollAvailEffectiveDate() {
		return enrollAvailEffectiveDate;
	}

	public void setEnrollAvailEffectiveDate(String enrollAvailEffectiveDate) {
		this.enrollAvailEffectiveDate = enrollAvailEffectiveDate;
	}

	public String getFormularyUrl() {
		return formularyUrl;
	}

	public void setFormularyUrl(String formularyUrl) {
		this.formularyUrl = formularyUrl;
	}

	public String getSbcDocUrl() {
		return sbcDocUrl;
	}

	public void setSbcDocUrl(String sbcDocUrl) {
		this.sbcDocUrl = sbcDocUrl;
	}

	public String getNonCommissionFlag() {
		return nonCommissionFlag;
	}

	public void setNonCommissionFlag(String nonCommissionFlag) {
		this.nonCommissionFlag = nonCommissionFlag;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public String getSupportFile() {
		return supportFile;
	}

	public void setSupportFile(String supportFile) {
		this.supportFile = supportFile;
	}

	public static Comparator<PlanRequestDTO> getPlanResponseComparatorByPlanId() {
		return PlanResponseComparatorByPlanId;
	}

	public static void setPlanResponseComparatorByPlanId(Comparator<PlanRequestDTO> planResponseComparatorByPlanId) {
		PlanResponseComparatorByPlanId = planResponseComparatorByPlanId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getPlanYear() {
		return planYear;
	}

	public void setPlanYear(int planYear) {
		this.planYear = planYear;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	

	@Override
	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}


	public static Comparator<PlanRequestDTO> PlanResponseComparatorByPlanId = new Comparator<PlanRequestDTO>(){

		@Override
		public int compare(PlanRequestDTO obj1, PlanRequestDTO obj2) {
			Integer obj1PlanId = obj1.getPlanId();
			Integer obj2PlanId = obj2.getPlanId();
			return obj1PlanId.compareTo(obj2PlanId);
		}
	};
}
