package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

/**
 * Broker data transfer class.
 * 
 * @author kanthi_v
 * 
 */
public class BrokerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String firstName;
	private String lastName;
	private String companyName;
	private String federalEIN;

	private String address1;
	private String address2;
	private String city;
	private String state;
	private int zip;

	private String contactNumber;
	private String email;

	private String licenseNumber;
	private String certificationDate;
	private String certificationEndDate;

	private String directDepositFlagIndicator;
	private String routingNumber;
	private String nameOnAccount;
	private String accountNumber;
	private String accountType;
	
	private String certificationStatus;
	private int responseCode;
    private String responseDescription;
    private int employerId;
    
	public int getEmployerId() {
		return employerId;
	}

	public void setEmployerId(int employerId) {
		this.employerId = employerId;
	}

	public BrokerDTO() {	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(String federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}

	public String getCertificationEndDate() {
		return certificationEndDate;
	}

	public void setCertificationEndDate(String certificationEndDate) {
		this.certificationEndDate = certificationEndDate;
	}

	public String getDirectDepositFlagIndicator() {
		return directDepositFlagIndicator;
	}

	public void setDirectDepositFlagIndicator(String directDepositFlagIndicator) {
		this.directDepositFlagIndicator = directDepositFlagIndicator;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getNameOnAccount() {
		return nameOnAccount;
	}

	public void setNameOnAccount(String nameOnAccount) {
		this.nameOnAccount = nameOnAccount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	

	public int getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(int responseCode)
	{
		this.responseCode = responseCode;
	}

	public void setResponseDescription(String responseDescription)
	{
		this.responseDescription = responseDescription;
	}

	public String getResponseDescription()
	{
		return responseDescription;
	}

	
	@Override
	public String toString() {
		return "BrokerDTO details: Id = "+id;
	}

}
