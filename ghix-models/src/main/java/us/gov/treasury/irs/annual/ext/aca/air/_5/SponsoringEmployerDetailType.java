//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.11.18 at 12:04:19 PM IST 
//


package us.gov.treasury.irs.annual.ext.aca.air._5;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:5.0" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Sponsoring Employer Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-04-09&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial Version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;All the elements associated Sponsoring Employer
 * 					&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for SponsoringEmployerDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SponsoringEmployerDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}SponsoringEmployerName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}SponsoringEmployerEIN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}SponsoringEmployerMailingAddress"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SponsoringEmployerDetailType", propOrder = {
    "sponsoringEmployerName",
    "sponsoringEmployerEIN",
    "sponsoringEmployerMailingAddress",
    "errorDetail"
})
public class SponsoringEmployerDetailType {

    @XmlElement(name = "SponsoringEmployerName", required = true)
    protected String sponsoringEmployerName;
    @XmlElement(name = "SponsoringEmployerEIN", required = true)
    protected String sponsoringEmployerEIN;
    @XmlElement(name = "SponsoringEmployerMailingAddress", required = true)
    protected MailingAddressGrpType sponsoringEmployerMailingAddress;
    @XmlElement(name = "ErrorDetail")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the sponsoringEmployerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsoringEmployerName() {
        return sponsoringEmployerName;
    }

    /**
     * Sets the value of the sponsoringEmployerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsoringEmployerName(String value) {
        this.sponsoringEmployerName = value;
    }

    /**
     * Gets the value of the sponsoringEmployerEIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsoringEmployerEIN() {
        return sponsoringEmployerEIN;
    }

    /**
     * Sets the value of the sponsoringEmployerEIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsoringEmployerEIN(String value) {
        this.sponsoringEmployerEIN = value;
    }

    /**
     * Gets the value of the sponsoringEmployerMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public MailingAddressGrpType getSponsoringEmployerMailingAddress() {
        return sponsoringEmployerMailingAddress;
    }

    /**
     * Sets the value of the sponsoringEmployerMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public void setSponsoringEmployerMailingAddress(MailingAddressGrpType value) {
        this.sponsoringEmployerMailingAddress = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
