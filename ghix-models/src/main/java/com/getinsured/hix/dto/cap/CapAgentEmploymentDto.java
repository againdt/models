package com.getinsured.hix.dto.cap;

/*
 * Created by kaul_s on 2/26/15.
 */
public class CapAgentEmploymentDto {

	String hireDate;
	String salesManager;
	String teamName;
	String shiftName;
	Integer shiftId;
	Float shiftHours;
	String location;
	String CUICId;
	String status;
	int userId;
	String agentId;
	int teamId;
	String terminationDate;
	String startDate;
	String endDate;
	String salesManagerStartDate;
	String errorMessage;
	String agentName;

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

	public String getSalesManager() {
		return salesManager;
	}

	public void setSalesManager(String salesManager) {
		this.salesManager = salesManager;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public Integer getShiftId() {
		return shiftId;
	}

	public void setShiftId(Integer shiftId) {
		this.shiftId = shiftId;
	}

	public Float getShiftHours() {
		return shiftHours;
	}

	public void setShiftHours(Float shiftHours) {
		this.shiftHours = shiftHours;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCUICId() {
		return CUICId;
	}

	public void setCUICId(String cUICId) {
		CUICId = cUICId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSalesManagerStartDate() {
		return salesManagerStartDate;
	}

	public void setSalesManagerStartDate(String salesManagerStartDate) {
		this.salesManagerStartDate = salesManagerStartDate;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
	
}
