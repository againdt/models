package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

/**
 * 
 * @author santanu
 * @version 1.0
 * @since Apr 19, 2013 
 *
 */
public class PlanCrossWalkRequestDTO implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String hiosPlanNumber;
	private String zip;
	private String countyCode;
	private String effectiveDate;
	private String isEligibleForCatastrophic;
	
	/**
	 * Constructor.
	 */
	public PlanCrossWalkRequestDTO() {
	}
	

	/**
	 * @return the hiosPlanNumber
	 */
	public String getHiosPlanNumber() {
		return hiosPlanNumber;
	}


	/**
	 * @param hiosPlanNumber the hiosPlanNumber to set
	 */
	public void setHiosPlanNumber(String hiosPlanNumber) {
		this.hiosPlanNumber = hiosPlanNumber;
	}
	
	/**
	 * @return the countyCode
	 */
	public String getCountyCode() {
		return countyCode;
	}

	/**
	 * @param countyCode the countyCode to set
	 */
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	/**
	 * @return the isEligibleForCatastrophic
	 */
	public String getIsEligibleForCatastrophic() {
		return isEligibleForCatastrophic;
	}


	/**
	 * @param isEligibleForCatastrophic the isEligibleForCatastrophic to set
	 */
	public void setIsEligibleForCatastrophic(String isEligibleForCatastrophic) {
		this.isEligibleForCatastrophic = isEligibleForCatastrophic;
	}
	
}
