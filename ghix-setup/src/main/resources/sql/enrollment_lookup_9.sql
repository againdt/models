
INSERT INTO LOOKUP_VALUE
  (
    LOOKUP_VALUE_ID,
    LOOKUP_TYPE_ID,
    LOOKUP_VALUE_CODE,
    LOOKUP_VALUE_LABEL,
    DESCRIPTION
  )
  VALUES
  (
    LOOKUPVALUE_SEQ.NEXTVAL,
    (select lookup_type_id from lookup_type where name = 'ST01'),
    'Group Install',
    'Group Install',
    ''
  );
