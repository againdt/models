package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


@Entity
@Table(name="ENRL_RECON_DISCREPANCY_AUD")
public class EnrlReconDiscrepancyAud  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;

	@Id
	@Column(name = "REV")
	private int rev;
	
	@Column(name="REVTYPE")
	private int revtype;
	
	@Column(name="FILE_ID")
	private Integer fileId;
	
	@Column(name="HIX_ENROLLMENT_ID")
	private Long hixEnrollmentId;
	
	@Column(name="Enrollee_ID")
	private Integer enrolleeId;
	
	@Column(name="ENRL_RECON_LKP_ID")
	private Integer enrlReconLkpId;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="DATE_RESOLVED")
	private Date dateResolved;
	
	@Column(name="IS_SNOOZE")
	private String isSnooze;
	
	@Column(name="REMARKS")
	private String remarks;
	
	@Column(name="IS_BAD_ADDRESS")
	private String isBadAddress;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	@Column(name="LAST_UPDATE_BY")
	private Integer lastUpdateBy;
	
	@Column(name="FIELD_NAME")
	private String fieldName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public int getRevtype() {
		return revtype;
	}

	public void setRevtype(int revtype) {
		this.revtype = revtype;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Long getHixEnrollmentId() {
		return hixEnrollmentId;
	}

	public void setHixEnrollmentId(Long hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}

	public Integer getEnrolleeId() {
		return enrolleeId;
	}

	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}

	public Integer getEnrlReconLkpId() {
		return enrlReconLkpId;
	}

	public void setEnrlReconLkpId(Integer enrlReconLkpId) {
		this.enrlReconLkpId = enrlReconLkpId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateResolved() {
		return dateResolved;
	}

	public void setDateResolved(Date dateResolved) {
		this.dateResolved = dateResolved;
	}

	public String getIsSnooze() {
		return isSnooze;
	}

	public void setIsSnooze(String isSnooze) {
		this.isSnooze = isSnooze;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsBadAddress() {
		return isBadAddress;
	}

	public void setIsBadAddress(String isBadAddress) {
		this.isBadAddress = isBadAddress;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	

}
