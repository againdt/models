CREATE OR REPLACE FUNCTION DROP_TKM_ACTIVITI_CONSTRAINT(
 v_table_name USER_CONSTRAINTS.table_name%TYPE,
    v_column_name user_cons_columns.COLUMN_NAME%TYPE
)   
RETURNS VOID AS  
$$  
DECLARE  
v_constraint_name USER_CONSTRAINTS.constraint_name%TYPE;
v_sql_stmt VARCHAR2(1000); 
BEGIN  
 SELECT constraint_name
    INTO v_constraint_name
    FROM USER_CONSTRAINTS
    WHERE constraint_name = (SELECT constraint_name FROM user_cons_columns WHERE table_name = v_table_name
    AND COLUMN_NAME = v_column_name)
    AND search_condition is not null;
    BEGIN
    v_sql_stmt := 'ALTER TABLE '||v_table_name||' DROP CONSTRAINT '||v_constraint_name;
    EXECUTE v_sql_stmt;
    END;
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
END;  
$$ LANGUAGE plpgsql;