#!/bin/bash
#
if [ $# -ne 2 ]
then
  echo " "
  echo "Usage $0 <Schema Prefix> <default tablespace_name>"
  echo " Example $0 GHIX GHIX_DATA will create three schemata/users:"
  echo "  GHIX_OWNER, GHIX_USER, GHIX_READONLY with default tablespace GHIX_DATA"
  echo " It will also create two roles: GHIX_USER_ROLE and GHIX_READONLY_ROLE"
  echo " "
  exit 2
fi
cat cr_TEMPLATE_USERS.sh | sed "s/TEMPLATE_APP/$1/g" >cr_3_users_pfx_$1.sh
chmod 700 cr_3_users_pfx_$1.sh
cat re_cr_TEMPLATE_APP_READONLY.sql | sed "s/TEMPLATE_APP/$1/g" | sed "s/TEMPLATE_DATA/$2/g" >re_cr_$1_READONLY.sql
cat re_cr_TEMPLATE_APP_USER.sql | sed "s/TEMPLATE_APP/$1/g" | sed "s/TEMPLATE_DATA/$2/g" >re_cr_$1_USER.sql
cat re_cr_TEMPLATE_APP_OWNER.sql | sed "s/TEMPLATE_APP/$1/g" | sed "s/TEMPLATE_DATA/$2/g"|sed "s/TEMPLATE_INDEX/$1_INDEX/g"  >re_cr_$1_OWNER.sql
./cr_3_users_pfx_$1.sh
##
#
##

