
package com.serff.planmanagementserffapi.serff.model.service;

import java.util.ArrayList;
import java.util.List;


public class DataTemplateOutcomes {

    protected List<DataTemplateOutcome> dataTemplateOutcome;

    /**
     * Gets the value of the dataTemplateOutcome property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataTemplateOutcome property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataTemplateOutcome().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataTemplateOutcome }
     * 
     * 
     */
    public List<DataTemplateOutcome> getDataTemplateOutcome() {
        if (dataTemplateOutcome == null) {
            dataTemplateOutcome = new ArrayList<DataTemplateOutcome>();
        }
        return this.dataTemplateOutcome;
    }

}
