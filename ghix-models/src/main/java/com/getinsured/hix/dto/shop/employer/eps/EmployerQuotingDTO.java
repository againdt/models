package com.getinsured.hix.dto.shop.employer.eps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmployerQuotingDTO {
	private static final long serialVersionUID = 1L;
	
	private  ArrayList< ArrayList< ArrayList< Map<String,String> > > >  censusList;
	private String insType;
	private String effectiveDate;
	private String empPrimaryZip;
	private String empCountyCode;
	private String ehbCovered;
	private List<PlanDTO> planDTOList;
	private Map<String, String> metalTierAvailability;
	
	/**
	 * @return the metalTierAvailability
	 */
	public Map<String, String> getMetalTierAvailability() {
		return metalTierAvailability;
	}
	/**
	 * @param metalTierAvailability the metalTierAvailability to set
	 */
	public void setMetalTierAvailability(Map<String, String> metalTierAvailability) {
		this.metalTierAvailability = metalTierAvailability;
	}
	public ArrayList<ArrayList<ArrayList<Map<String, String>>>> getCensusList() {
		return censusList;
	}
	public void setCensusList(ArrayList<ArrayList<ArrayList<Map<String, String>>>> censusList) {
		this.censusList = censusList;
	}
	public String getInsType() {
		return insType;
	}
	public void setInsType(String insType) {
		this.insType = insType;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getEmpPrimaryZip() {
		return empPrimaryZip;
	}
	public void setEmpPrimaryZip(String empPrimaryZip) {
		this.empPrimaryZip = empPrimaryZip;
	}
	public String getEmpCountyCode() {
		return empCountyCode;
	}
	public void setEmpCountyCode(String empCountyCode) {
		this.empCountyCode = empCountyCode;
	}
	public String getEhbCovered() {
		return ehbCovered;
	}
	public void setEhbCovered(String ehbCovered) {
		this.ehbCovered = ehbCovered;
	}
	public List<PlanDTO> getPlanDTOList() {
		return planDTOList;
	}
	public void setPlanDTOList(List<PlanDTO> planDTOList) {
		this.planDTOList = planDTOList;
	}
	
	
}
