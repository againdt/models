--Update Medicaid/CHIP thresholds and Expansion flags
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=23, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Alabama';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 175,MEDICAID_INCOME_THRESHOLD=78, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Alaska';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 175,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Arizona';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Arkansas';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'California';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Colorado';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Connecticut';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Delaware';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'DC';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=56, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Florida';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 235,MEDICAID_INCOME_THRESHOLD=48, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Georgia';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Hawaii';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 185,MEDICAID_INCOME_THRESHOLD=37, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Idaho';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Illinois';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=24, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Indiana';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Iowa';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 225,MEDICAID_INCOME_THRESHOLD=31, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Kansas';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Kentucky';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=24, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Louisiana';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=133, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Maine';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Maryland';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Massachusetts';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Michigan';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 275,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Minnesota';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=29, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Mississippi';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=35, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Missouri';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=54, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Montana';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=58, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Nebraska';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Nevada';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=47, MEDICAID_EXPANSION = 'N' WHERE STATE = 'New Hampshire';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 350,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'New Jersey';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 235,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'New Mexico';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 400,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'New York';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=47, MEDICAID_EXPANSION = 'N' WHERE STATE = 'North Carolina';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 160,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'North Dakota';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=96, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Ohio';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 185,MEDICAID_INCOME_THRESHOLD=51, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Oklahoma';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Oregon';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Pennsylvania';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Rhode Island';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=89, MEDICAID_EXPANSION = 'N' WHERE STATE = 'South Carolina';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=50, MEDICAID_EXPANSION = 'N' WHERE STATE = 'South Dakota';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 250,MEDICAID_INCOME_THRESHOLD=122, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Tennessee';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=25, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Texas';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=42, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Utah';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Vermont';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=30, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Virginia';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'Washington';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=138, MEDICAID_EXPANSION = 'Y' WHERE STATE = 'West Virginia';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 300,MEDICAID_INCOME_THRESHOLD=200, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Wisconsin';
UPDATE STATE_MEDICAID_CHIP SET CHIP_INCOME_LIMIT = 200,MEDICAID_INCOME_THRESHOLD=50, MEDICAID_EXPANSION = 'N' WHERE STATE = 'Wyoming';

--Update SBE URL and Phone Numbers
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://coveredca.com/',SBE_PHONE = '800-300-1506' WHERE STATE='California';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.connectforhealthco.com/',SBE_PHONE = '855-752-6749' WHERE STATE='Colorado';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.accesshealthct.com/',SBE_PHONE = '855-805-4325' WHERE STATE='Connecticut';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.DCHealthLink.com ',SBE_PHONE = '855-532-5465' WHERE STATE='DC';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.hawaiihealthconnector.com/',SBE_PHONE = '877-628-5076' WHERE STATE='Hawaii';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://healthbenefitexchange.ky.gov/Pages/home.aspx',SBE_PHONE = '502-564-7940' WHERE STATE='Kentucky';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://marylandhbe.com/',SBE_PHONE = '410-358-5615' WHERE STATE='Maryland';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='https://www.mahealthconnector.org/portal/site/connector',SBE_PHONE = '877-623-6765' WHERE STATE='Massachusetts';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.mn.gov/hix/',SBE_PHONE = '855-366-7873' WHERE STATE='Minnesota';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.nevadahealthlink.com/',SBE_PHONE = '855-768-5465' WHERE STATE='Nevada';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://healthbenefitexchange.ny.gov/',SBE_PHONE = '855-693-6765' WHERE STATE='New York';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://coveroregon.com/',SBE_PHONE = '855-268-3767' WHERE STATE='Oregon';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.healthsourceri.com/',SBE_PHONE = '401-222-5192' WHERE STATE='Rhode Island';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://healthconnect.vermont.gov/',SBE_PHONE = '802-654-8854' WHERE STATE='Vermont';
UPDATE STATE_MEDICAID_CHIP SET SBE_WEBSITE ='http://www.wahbexchange.org/',SBE_PHONE = '855-923-4633' WHERE STATE='Washington';
COMMIT;