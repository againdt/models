package com.getinsured.hix.dto.enrollment.aca;
import java.util.List;

import com.getinsured.hix.dto.enrollment.aca.App;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EnrollmentAcaRequestDTO {
	 @JsonProperty("app")
	 private App app;
	
	@JsonProperty("externalKey")
	private String externalKey;

	@JsonProperty("userSettings")
    private UserSettings userSettings;
	
	// @JsonProperty("plans")
	// private List<Plans> plans;
	
	@JsonProperty("enrollment")
	private Enrollment enrollment;

	public void setApp(App app) {
		this.app = app;
	}

	public App getApp() {
		return app;
	}

	public void setExternalKey(String externalKey) {
		this.externalKey = externalKey;
	}

	public String getExternalKey() {
		return externalKey;
	}

	public void setUserSettings(UserSettings userSettings) {
		this.userSettings = userSettings;
	}

	public UserSettings getUserSettings() {
		return userSettings;
	}
//
//	public void setPlans(List<Plans> plans) {
//		this.plans = plans;
//	}

//	public List<Plans> getPlans() {
//		return plans;
//	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}
}
