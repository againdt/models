package com.getinsured.hix.dto.cap;

import java.math.BigDecimal;



/*
 * Created by kaul_s on 2/26/15.
 */
public class CapAgentDto {

	
	
	CapAgentUserDto agentUserDto;
	CapAgentEmploymentDto capAgentEmploymentDto;
	CapAgentFFMInfoDto agentFFMInfoDto;
	private int loggedInUserId;
	private String teamMemberMaxEndDate;
	public CapAgentDto() {
	}


	public CapAgentDto(Object[] entity) {
		agentUserDto = new CapAgentUserDto();
		mapAgentUserDto(entity);
		capAgentEmploymentDto = new CapAgentEmploymentDto();
		mapCapAgentEmploymentDto(entity);
		agentFFMInfoDto = new CapAgentFFMInfoDto();
		mapAgentFFMInfoDto(entity);
	}


	private void mapAgentFFMInfoDto(Object[] entity) {
		
	}


	private void mapCapAgentEmploymentDto(
			Object[] entity) {
		String agentId = String.valueOf(((BigDecimal) entity[3]));
		this.capAgentEmploymentDto.setAgentId(agentId);
		this.capAgentEmploymentDto.setTeamName((String) entity[5]);
		if(null!=entity[4]){
			this.capAgentEmploymentDto.setTeamId(((BigDecimal)entity[4]).intValue());
		}
		this.capAgentEmploymentDto.setLocation((String)entity[7]);
		this.capAgentEmploymentDto.setStatus((String)entity[8]);
	}


	private void mapAgentUserDto(Object[] entity) {
		this.agentUserDto.setFirstName((String) entity[1]);
		this.agentUserDto.setLastName((String) entity[2]);
		this.agentUserDto.setFullName((String) entity[1] + " " + entity[2]);
		this.agentUserDto.setEmail((String) entity[6]);
	}


	public CapAgentUserDto getAgentUserDto() {
		return agentUserDto;
	}

	public void setAgentUserDto(CapAgentUserDto agentUserDto) {
		this.agentUserDto = agentUserDto;
	}

	public CapAgentEmploymentDto getCapAgentEmploymentDto() {
		return capAgentEmploymentDto;
	}

	public void setCapAgentEmploymentDto(CapAgentEmploymentDto capAgentEmploymentDto) {
		this.capAgentEmploymentDto = capAgentEmploymentDto;
	}

	public CapAgentFFMInfoDto getAgentFFMInfoDto() {
		return agentFFMInfoDto;
	}

	public void setAgentFFMInfoDto(CapAgentFFMInfoDto agentFFMInfoDto) {
		this.agentFFMInfoDto = agentFFMInfoDto;
	}


	public int getLoggedInUserId() {
		return loggedInUserId;
	}


	public void setLoggedInUserId(int loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}


	public String getTeamMemberMaxEndDate() {
		return teamMemberMaxEndDate;
	}

	public void setTeamMemberMaxEndDate(String teamMemberMaxEndDate) {
		this.teamMemberMaxEndDate = teamMemberMaxEndDate;
	}	
}
