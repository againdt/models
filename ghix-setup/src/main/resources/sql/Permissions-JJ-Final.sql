--liquibase formatted sql

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'ADD_ACCREDITATION_DOCUMENTS');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add_Employees'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'ADD_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'ADD_NEW_ISSUER');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add_Payment_Method'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add/Remove_Authorized_Employer_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'ADD_REPRESENTATIVE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Appeals'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Cancel_Open_Enrollment'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Complaints'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Contribution_Selection'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Delete_Employee'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Account_Information'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_ACCREDITATION_DOCUMENTS');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Authorized_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_COMPNAY_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_INDIVIDUAL_MARKET_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_PLAN_BENEFITS');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Plan_Details'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_PLAN_RATES');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_REPRESENTATIVE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'EDIT_SHOP_MARKET_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Employer_Plan_Selection_wizard'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Enroll_on_Behalf_of_Employee'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Make_Payment'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_ASSISTER');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Authorized_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_broker'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Employee_List'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_ENTITY');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Financial_Information'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_issuer'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Payment_Methods'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_QHP_PLANS');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_REPRESENTATIVE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_SADP_PLANS');

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Participation_Report'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Payment_Report'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Renew_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Review_Employee_List'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Search_Employee'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Terminate_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Terminate_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Terminate_Employer_Account'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Upload_New_Benefits'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Upload_New_Rates'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Update_Provider_Network_ID'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Update_Enrollment_Availability'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Update_Certification_Status'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VERIFY_PLAN');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Account_Information'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_ACCREDITATION_DOCUMENTS');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Authorized_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Available_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Certification_Status'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_COMPNAY_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Dependent_Details'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Eligibility_Results'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Employer_Dashboard'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Enrollment_Availability'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Enrollment_Details '));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_FINANCIAL_LIST_INFO');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_INDIVIDUAL_MARKET_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Individual_Members'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Invoices'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_ISSUER_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Issuer_Reports'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Broker_Reports'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Payments'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Benefits'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Details'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_History'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Rates'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Reports'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Selections'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Provider_Network_ID'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Qualified_Dental_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Qualified_Health_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Qualified_Vision_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_SHOP_MARKET_PROFILE');
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_SHOP_Members'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Waive_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Waive_Employee_Coverage'));
