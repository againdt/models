package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

public class GhixResponseDTO {

	private Integer id;
	private Integer errorCode;
	private String errorMsg;
	private String status;
	private long executionDuration;
	private long startTime;

	public enum STATUS {
		SUCCESS, FAILED
	}

	public GhixResponseDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getExecutionDuration() {
		return executionDuration;
	}

	public void setExecutionDuration(long executionDuration) {
		this.executionDuration = executionDuration;
	}
	
	public void startResponse() {
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		executionDuration = endTime - startTime;
	}
}
