package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

public class BrokerListDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int responseCode;
    private String responseDescription;
	private List<BrokerDTO> designatedBrokersListResponse;
	
	public List<BrokerDTO> getDesignatedBrokersListResponse() {
		return designatedBrokersListResponse;
	}
	public void setDesignatedBrokersListResponse(
			List<BrokerDTO> designatedBrokersListResponse) {
		this.designatedBrokersListResponse = designatedBrokersListResponse;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDescription() {
		return responseDescription;
	}
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
		
}