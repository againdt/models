package com.getinsured.hix.dto.entity;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializerProvider;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class MapSerializer extends JsonSerializer<Map<String, Object>> {

	ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public void serialize(Map<String, Object> map, JsonGenerator jgen, SerializerProvider sp)
			throws IOException, JsonProcessingException {

		ObjectWriter objWriter = objectMapper.writer();
		jgen.writeStartObject();
		iterate(map, jgen, objWriter);
		jgen.writeEndObject();

	}

	private void iterate(Map map, JsonGenerator jgen, ObjectWriter writer) throws IOException, JsonProcessingException {

		Iterator itr = map.entrySet().iterator();
		while (itr.hasNext()) {
			Entry e = (Entry) itr.next();
			String key = null;
			if (e.getKey() instanceof String) {
				key = (String) e.getKey();
			} else {
				key = writer.writeValueAsString(e.getKey());
			}

			if (e.getValue() instanceof Map) {
				jgen.writeObjectFieldStart(key);
				iterate((Map<Object, Object>) e.getValue(), jgen, writer);
				jgen.writeEndObject();
			} else {
				jgen.writeObjectField(key, e.getValue());
			}
		}

	}

}
