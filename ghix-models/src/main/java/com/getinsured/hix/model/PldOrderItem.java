package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



public class PldOrderItem {
	public enum QuotingGroup {
		FAMILY, ADULT, CHILD;
	}
	private int id;
	
	private PldOrder pldOrder;

	private Plan plan;
	
	private Float premium;
	
	private Float planPremium;
	
	private String quotingData;

	private String insuranceType;
	
	private Date created;

	private Date updated;
	
	private Integer createdBy;
	
	private Integer updatedBy;
	
	private Float monthlyAptc;
	
	private QuotingGroup quotingGroup;
	
	private String contributionData;

    private Long ssapApplicationId;
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PldOrder getPldOrder() {
		return pldOrder;
	}

	public void setPldOrder(PldOrder pldOrder) {
		this.pldOrder = pldOrder;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	/*public PldGroup getPldGroup() {
		return pldGroup;
	}

	public void setPldGroup(PldGroup pldGroup) {
		this.pldGroup = pldGroup;
	}*/

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	public Float getPlanPremium() {
		return planPremium;
	}

	public void setPlanPremium(Float planPremium) {
		this.planPremium = planPremium;
	}

	public String getQuotingData() {
		return quotingData;
	}

	public void setQuotingData(String quotingData) {
		this.quotingData = quotingData;
	}
	
	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}

	public Float getMonthlyAptc() {
		return monthlyAptc;
	}

	public void setMonthlyAptc(Float monthlyAptc) {
		this.monthlyAptc = monthlyAptc;
	}
	
	public QuotingGroup getQuotingGroup() {
		return quotingGroup;
	}

	public void setQuotingGroup(QuotingGroup quotingGroup) {
		this.quotingGroup = quotingGroup;
	}

	public String getContributionData() {
		return contributionData;
	}

	public void setContributionData(String contributionData) {
		this.contributionData = contributionData;
	}


	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
}
