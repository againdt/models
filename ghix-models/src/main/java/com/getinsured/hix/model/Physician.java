package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the physician database table.
 * 
 */

@Entity
@Table(name="physician")
public class Physician implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum PhysicianType{ 
		DOCTOR, DENTIST, VISION; 
	}
	public enum PhysicianGender{ 
		M, F; 
	}
	public enum AcceptingNewPatients{ 
		YES,NO,U;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Physician_Seq")
	@SequenceGenerator(name = "Physician_Seq", sequenceName = "physician_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="provider_id")
	private Provider provider;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "physician", fetch = FetchType.EAGER)
	private Set<PhysicianSpecialityRelation> physicianSpecialRelation = new HashSet<PhysicianSpecialityRelation>(0);
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "physician", fetch = FetchType.EAGER)
	private Set<PhysicianAddress> physicianAddress = new HashSet<PhysicianAddress>(0);
	
	@Column(name="gi_type")
	@Enumerated(EnumType.STRING)
	private PhysicianType type;
	
	@Column(name="title")	
	private String title;
	
	@Column(name="gender")
	@Enumerated(EnumType.STRING)
	private PhysicianGender gender;
	
	@Column(name="graduation_year")
	private String graduationYear;
	
	@Column(name="accepting_new_patients")
	@Enumerated(EnumType.STRING)
	private AcceptingNewPatients acceptingNewPatients;
	
	@Column(name="dea_number")
	private String deaNumber;
	
	@Column(name="education")
	private String education;
	
	@Column(name="board_certification")
	private String boardCertification;
	
	@Column(name="affiliated_hospital")
	private String affiliatedHospital;
	
	@Column(name="medical_group")
	private String medicalGroup;
	
	@Column(name="languages")
	private String languages;
	
	@Column(name="internship")
	private String internship;
	
	@Column(name="residency")
	private String residency;
	
	@Column(name="website_url")
	private String websiteUrl;
	
	@Column(name="email_address")
	private String emailAddress;
	
	@Column(name="years_of_exp")
	private String yearsOfExp;
	
	@Temporal( TemporalType.DATE)
	@Column(name="birth_day")
	private Date birthDay;
	
	@Transient
	private Provider providerObj;
	
	@Transient
	private PhysicianAddress physicianAddressObj;
	
	@Transient
	private Specialty specialityObj;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	@Column(name="first_name")
	private String first_name;
	
	@Column(name="middle_name")
	private String middle_name;
	
	@Column(name="last_name")
	private String last_name;
	
	@Column(name="suffix")
	private String suffix;	
	
	@Transient 
	private Set<Specialty> specialitySet;

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	public PhysicianType getType() {
		return type;
	}

	public void setType(PhysicianType type) {
		this.type = type;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PhysicianGender getGender() {
		return gender;
	}

	public void setGender(PhysicianGender gender) {
		this.gender = gender;
	}

	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getBoardCertification() {
		return boardCertification;
	}

	public void setBoardCertification(String boardCertification) {
		this.boardCertification = boardCertification;
	}

	public String getAffiliatedHospital() {
		return affiliatedHospital;
	}

	public void setAffiliatedHospital(String affiliatedHospital) {
		this.affiliatedHospital = affiliatedHospital;
	}

	public String getMedicalGroup() {
		return medicalGroup;
	}

	public void setMedicalGroup(String medicalGroup) {
		this.medicalGroup = medicalGroup;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String website) {
		this.websiteUrl = website;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String email) {
		this.emailAddress = email;
	}

	public String getYearsOfExp() {
		return yearsOfExp;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date dob) {
		this.birthDay = dob;
	}

	public void setYearsOfExp(String yearsOfExp) {
		this.yearsOfExp = yearsOfExp;
	}

	public Set<PhysicianSpecialityRelation> getPhysicianSpecialRelation() {
		return physicianSpecialRelation;
	}

	public void setPhysicianSpecialRelation(
			Set<PhysicianSpecialityRelation> physicianSpecialRelation) {
		this.physicianSpecialRelation = physicianSpecialRelation;
	}
	
	public Provider getProviderObj() {
		return providerObj;
	}

	public void setProviderObj(Provider providerObj) {
		this.providerObj = providerObj;
	}

	public void setPhysicianAddressObj(PhysicianAddress physicianAddressObj) {
		this.physicianAddressObj = physicianAddressObj;
	}
	
	public PhysicianAddress getPhysicianAddressObj() {
		return physicianAddressObj;
	}
	
	public Set<PhysicianAddress> getPhysicianAddress() {
		return physicianAddress;
	}

	public void setPhysicianAddress(Set<PhysicianAddress> physicianAddress) {
		this.physicianAddress = physicianAddress;
	}

	public AcceptingNewPatients getAcceptingNewPatients() {
		return acceptingNewPatients;
	}

	public void setAcceptingNewPatients(AcceptingNewPatients acceptingNewPatients) {
		this.acceptingNewPatients = acceptingNewPatients;
	}

	public String getDeaNumber() {
		return deaNumber;
	}

	public void setDeaNumber(String deaNumber) {
		this.deaNumber = deaNumber;
	}

	public String getInternship() {
		return internship;
	}

	public void setInternship(String internship) {
		this.internship = internship;
	}

	public String getResidency() {
		return residency;
	}

	public void setResidency(String residency) {
		this.residency = residency;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Specialty getSpecialityObj() {
		return specialityObj;
	}

	public void setSpecialityObj(Specialty specialityObj) {
		this.specialityObj = specialityObj;
	}
	
	public Set<Specialty> getSpecialitySet() {
		return specialitySet;
	}

	public void setSpecialitySet(Set<Specialty> specialitySet) {
		this.specialitySet = specialitySet;
	}
	
}
