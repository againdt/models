package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class EnrollmentListWrapperDTO implements Serializable{

	private List<EnrollmentOffExchangeDTO> enrollmentOffExchangeDTOlist;

	public List<EnrollmentOffExchangeDTO> getEnrollmentOffExchangeDTOlist() {
		return enrollmentOffExchangeDTOlist;
	}

	public void setEnrollmentOffExchangeDTOlist(
			List<EnrollmentOffExchangeDTO> enrollmentOffExchangeDTOlist) {
		this.enrollmentOffExchangeDTOlist = enrollmentOffExchangeDTOlist;
	}
}
