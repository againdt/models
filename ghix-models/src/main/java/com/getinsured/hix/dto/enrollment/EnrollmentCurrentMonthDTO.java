/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Issuer;

/**
 * 
 * @author rajaramesh_g
 *
 */
public class EnrollmentCurrentMonthDTO implements Comparable<EnrollmentCurrentMonthDTO>, Serializable{

	private Integer enrollmentId;
	private Issuer issuer;
	private Employer employer;
	private String insuranceType;
	private String groupPolicyNumber;
	private Float grossPremiumAmt;
	private Date  benefitEffectiveDate;
	private Date benefitEndDate;
	private Integer employeeId;
	private Boolean isPending;
	private Integer noOfPersons;
	private String policyNumber;
	private Float employeeContribution;
	private Float employerContribution;
	private Date enrollmentCreatedDate;
	private Date terminationDate;
	private List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList;
	private Integer employerEnrollmentId;
	private List<EnrollmentSubscriberEventDTO> enrollmentSubscriberEventDtoList;
	private String insurerName;
	private Integer issuerId;
	private String enrollmentStatusValue;
	private String enrollmentStatusLabel;
	
	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}
	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Employer getEmployer() {
		return employer;
	}
	public void setEmployer(Employer employer) {
		this.employer = employer;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}
	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}
	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}
	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public Date getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Boolean getIsPending() {
		return isPending;
	}
	public void setIsPending(Boolean isPending) {
		this.isPending = isPending;
	}
	public Integer getNoOfPersons() {
		return noOfPersons;
	}
	public void setNoOfPersons(Integer noOfPersons) {
		this.noOfPersons = noOfPersons;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Float getEmployeeContribution() {
		return employeeContribution;
	}
	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}
	public Float getEmployerContribution() {
		return employerContribution;
	}
	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}
	public String getEnrollmentStatusValue() {
		return enrollmentStatusValue;
	}
	public void setEnrollmentStatusValue(String enrollmentStatusValue) {
		this.enrollmentStatusValue = enrollmentStatusValue;
	}
	public String getEnrollmentStatusLabel() {
		return enrollmentStatusLabel;
	}
	public void setEnrollmentStatusLabel(String enrollmentStatusLabel) {
		this.enrollmentStatusLabel = enrollmentStatusLabel;
	}
	public Date getEnrollmentCreatedDate() {
		return enrollmentCreatedDate;
	}
	public void setEnrollmentCreatedDate(Date enrollmentCreatedDate) {
		this.enrollmentCreatedDate = enrollmentCreatedDate;
	}
	public Date getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	public List<EnrolleeCurrentMonthDTO> getEnrolleeCurrentMonthDTOList() {
		return enrolleeCurrentMonthDTOList;
	}
	public void setEnrolleeCurrentMonthDTOList(
			List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList) {
		this.enrolleeCurrentMonthDTOList = enrolleeCurrentMonthDTOList;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean isEquals = false;
		if(this.enrollmentId.equals(((EnrollmentCurrentMonthDTO) obj).getEnrollmentId())){
			isEquals = true;
		}
		return isEquals;
	}
	@Override
	public int compareTo(EnrollmentCurrentMonthDTO obj) {
		return this.enrollmentId.compareTo(obj.getEnrollmentId());
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((enrollmentId == null) ? 0 : enrollmentId.hashCode());
		return result;
	}
	/**
	 * @return the enrollmentSubscriberEventDtoList
	 */
	public List<EnrollmentSubscriberEventDTO> getEnrollmentSubscriberEventDtoList() {
		return enrollmentSubscriberEventDtoList;
	}
	/**
	 * @param enrollmentSubscriberEventDtoList the enrollmentSubscriberEventDtoList to set
	 */
	public void setEnrollmentSubscriberEventDtoList(
			List<EnrollmentSubscriberEventDTO> enrollmentSubscriberEventDtoList) {
		this.enrollmentSubscriberEventDtoList = enrollmentSubscriberEventDtoList;
	}
	
	public String getInsurerName() {
		return insurerName;
	}
	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	
	
}
