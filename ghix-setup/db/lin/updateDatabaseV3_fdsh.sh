#!/bin/bash
echo " "
echo "Make sure jave version is 1.8 or above "
echo 

if [ $# -ne 1 ]
then
  echo " "
  echo "Usage $0 <profile in low-case>"
  echo "Usage $0 [fdsh]"
  echo "Example: $0 nm"
  echo " "
  exit 2
fi

echo $1
###
### For PostgreSQL
CONFL=../../conf/liquibase_postgres.properties
### For Oracle
###CONFL=../../conf/liquibase.properties

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml releaseLocks

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml update

date
