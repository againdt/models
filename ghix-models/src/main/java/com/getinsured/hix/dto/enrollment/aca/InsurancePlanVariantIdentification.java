package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsurancePlanVariantIdentification {

	@JsonProperty("IdentificationID")
	private IdentificationID IdentificationID;


	public IdentificationID getIdentificationID() {
		return IdentificationID;
	}

	public void setIdentificationID(IdentificationID identificationID) {
		this.IdentificationID = identificationID;
	}
}