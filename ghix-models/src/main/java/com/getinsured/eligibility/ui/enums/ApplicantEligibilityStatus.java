package com.getinsured.eligibility.ui.enums;

public enum ApplicantEligibilityStatus {


	APTC("Advanced Premium Tax Credit","Advanced Payment of Premium Tax Credit"), StateSubsidy("State Subsidy", "State Subsidy"), CHIP("Children's Health Insurance Program","Children's Health Insurance Program"),
	CSR("Cost Sharing Reduction","Cost Sharing Reduction"), MEDICAID("Medicaid","Medicaid"), QHP("Qualified Health Plan","Qualified Health Plan"), ASSESSED_MEDICAID("Medicaid Assessed","Medicaid Assessed"), ASSESSED_CHIP("Chip Assessed","Chip Assessed");

	private String	description;
	
	private String notificationDescription;

	private ApplicantEligibilityStatus(String description, String notificationDescription){
		this.description = description;
		this.notificationDescription=notificationDescription;
	}

	public String getDescription(){
		return description;
	}

	public String getNotificationDescription() {
		return notificationDescription;
	}

}
