
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;ACA Form Submission Response Header Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2012-09-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;Description&gt;A group header that provides ACA Form Submission request message related information&lt;/Description&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for ACABatchSubmissionResponseGrpHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ACABatchSubmissionResponseGrpHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}ACABusinessCorrelationId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}MessageSentTs"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ACABatchSubmissionResponseGrpHeaderType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "acaBusinessCorrelationId",
    "messageSentTs"
})
public class ACABatchSubmissionResponseGrpHeaderType {

    @XmlElement(name = "ACABusinessCorrelationId", namespace = "urn:us:gov:treasury:irs:common", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String acaBusinessCorrelationId;
    @XmlElement(name = "MessageSentTs", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String messageSentTs;

    /**
     * Gets the value of the acaBusinessCorrelationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACABusinessCorrelationId() {
        return acaBusinessCorrelationId;
    }

    /**
     * Sets the value of the acaBusinessCorrelationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACABusinessCorrelationId(String value) {
        this.acaBusinessCorrelationId = value;
    }

    /**
     * Gets the value of the messageSentTs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageSentTs() {
        return messageSentTs;
    }

    /**
     * Sets the value of the messageSentTs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageSentTs(String value) {
        this.messageSentTs = value;
    }

}
