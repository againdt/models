/**
 * 
 * @author santanu
 * @version 1.0
 * @since Jan 14, 2015 
 * 
 * This DTO to accept request object for Medicaid plan
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class MedicaidPlanRequestDTO implements Serializable {
	
	/*
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor.
	 */
	public MedicaidPlanRequestDTO() {}
	private String url;
	private String requestMethod;
	private String householdCaseId;
	private String zipCode;
	private String countyCode;
	private String effectiveDate;
	
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	/**
	 * @return the requestMethod
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * @param requestMethod the requestMethod to set
	 */
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * @return the householdCaseId
	 */
	public String getHouseholdCaseId() {
		return householdCaseId;
	}
	/**
	 * @param householdCaseId the householdCaseId to set
	 */
	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the countyCode
	 */
	public String getCountyCode() {
		return countyCode;
	}
	/**
	 * @param countyCode the countyCode to set
	 */
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

}
