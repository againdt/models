package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_RECON_MISSING_COUNT")
public class EnrlReconMissingCount implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_missing_count_seq")
	@SequenceGenerator(name = "enrl_recon_missing_count_seq", sequenceName = "ENRL_RECON_MISSING_COUNT_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name = "HIOS_ISSUER_ID", nullable = false)
	private String hiosIssuerId;
	
	@Column(name="COVERAGE_YEAR", nullable = false)
	private String coverageYear;
	
	@Column(name="PROCESSING_MONTH", nullable = false)
	private Integer processingMonth;
	
	@Column(name="PROCESSING_YEAR", nullable = false)
	private Integer processingYear;
	
	@Column(name="ENRL_CNT_MISSING_IN_HIX")
	private Integer enrlCntMissingInHix;
	
	@Column(name="ENRL_CNT_MISSING_IN_FILE")
	private Integer enrlCntMissingInFile;
	
	@Column(name="FILE_COUNT")
	private Integer fileCount;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public Integer getProcessingMonth() {
		return processingMonth;
	}

	public void setProcessingMonth(Integer processingMonth) {
		this.processingMonth = processingMonth;
	}

	public Integer getProcessingYear() {
		return processingYear;
	}

	public void setProcessingYear(Integer processingYear) {
		this.processingYear = processingYear;
	}

	public Integer getEnrlCntMissingInHix() {
		return enrlCntMissingInHix;
	}

	public void setEnrlCntMissingInHix(Integer enrlCntMissingInHix) {
		this.enrlCntMissingInHix = enrlCntMissingInHix;
	}

	public Integer getEnrlCntMissingInFile() {
		return enrlCntMissingInFile;
	}

	public void setEnrlCntMissingInFile(Integer enrlCntMissingInFile) {
		this.enrlCntMissingInFile = enrlCntMissingInFile;
	}

	public Integer getFileCount() {
		return fileCount;
	}

	public void setFileCount(Integer fileCount) {
		this.fileCount = fileCount;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
}
