package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the physician_speciality_relation database table.
 * 
 */
@Entity
@Table(name="physician_speciality_relation")
public class PhysicianSpecialityRelation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PhysicianSpecialityRelation_Seq")
	@SequenceGenerator(name = "PhysicianSpecialityRelation_Seq", sequenceName = "PHYSICIAN_SPECIALITY_RELAT_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="specialty_id")
	private Specialty specialty;
	
	@ManyToOne
    @JoinColumn(name="physician_id")
	private Physician physician;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Physician getPhysician() {
		return physician;
	}

	public void setPhysician(Physician physician) {
		this.physician = physician;
	}

	public Specialty getSpecialty() {
		return specialty;
	}

	public void setSpecialty(Specialty specialty) {
		this.specialty = specialty;
	}
	
			
}