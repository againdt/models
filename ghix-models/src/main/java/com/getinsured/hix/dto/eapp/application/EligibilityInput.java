package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Information captured as part of eligibility check
 * @author root
 *
 */
public class EligibilityInput implements Serializable{
	private static final long serialVersionUID = 1L;
	private String zipCode;
	private BigDecimal income;
	private String countyCode;
	private String countyName;
	private String state;
	
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public BigDecimal getIncome() {
		return income;
	}
	public void setIncome(BigDecimal income) {
		this.income = income;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public static EligibilityInput getInstance(String zipCode, String state){
		EligibilityInput input = new EligibilityInput();
		input.setState(state);
		input.setZipCode(zipCode);
		/**
		 * also fetch county name and county code
		 */
		
		return input;
	}

    public static EligibilityInput getInstance(String zipCode, String state, String countyCode){
        EligibilityInput input = getInstance(zipCode, state);
        input.setCountyCode(countyCode);
        return input;
    }
	
	
}
