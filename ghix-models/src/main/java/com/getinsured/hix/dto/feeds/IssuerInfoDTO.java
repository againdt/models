package com.getinsured.hix.dto.feeds;

public class IssuerInfoDTO {

	private Integer issuerId;
	private String issuerName;
	private String hiosIssuerId;
	
	
	/**
	 * @return the issuerId
	 */
	public Integer getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/** 
	 * @return
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	/**
	 * @param hiosIssuerId
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
}
