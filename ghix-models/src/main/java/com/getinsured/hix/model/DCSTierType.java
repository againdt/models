/**
 * 
 */
package com.getinsured.hix.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Persistence class 'DCSTierType' mapping to table 'DCS_TIER_TYPE'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 15, 2013 4:01:58 PM 
 *
 */
@Entity
@Table(name ="DCS_TIER_TYPE")
public class DCSTierType {
	@Id
	@SequenceGenerator(name = "DCSTierType_Seq", sequenceName = "DCS_TIER_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DCSTierType_Seq")
	private int id;
	
	@Column(name = "TIER_ID")
	@Length(max = 200)
	@NotNull
	private String tierId;
	
	@ManyToOne
	@JoinColumn(name = "FORMULARY_ID", referencedColumnName = "ID")
    private Formulary formularyId;
	
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp created;
	
	@NotNull
	@Column(name="LAST_UPDATED_TIMESTAMP")
	private Timestamp updated;
	
	/**
	 * Default constructor.
	 */
	public DCSTierType() {
	}
	/**
	 * Getter for 'id'.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Setter for 'id'.
	 *
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Getter for 'tierId'.
	 *
	 * @return the tierId
	 */
	public String getTierId() {
		return tierId;
	}
	/**
	 * Setter for 'tierId'.
	 *
	 * @param tierId the tierId to set
	 */
	public void setTierId(String tierId) {
		this.tierId = tierId;
	}
	/**
	 * Getter for 'formularyId'.
	 *
	 * @return the formularyId
	 */
	public Formulary getFormularyId() {
		return formularyId;
	}
	/**
	 * Setter for 'formularyId'.
	 *
	 * @param formularyId the formularyId to set
	 */
	public void setFormularyId(Formulary formularyId) {
		this.formularyId = formularyId;
	}
	/**
	 * Getter for 'created'.
	 *
	 * @return the created
	 */
	public Timestamp getCreated() {
		return created;
	}
	/**
	 * Setter for 'created'.
	 *
	 * @param created the created to set
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	/**
	 * Getter for 'updated'.
	 *
	 * @return the updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}
	/**
	 * Setter for 'updated'.
	 *
	 * @param updated the updated to set
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

}
