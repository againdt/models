package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

public class IssuersLegalNameResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Map<Integer, String> issuersLegalNameMap;

	public Map<Integer, String> getIssuersLegalNameMap() {
		return issuersLegalNameMap;
	}

	public void setIssuersLegalNameMap(Map<Integer, String> issuersLegalNameMap) {
		this.issuersLegalNameMap = issuersLegalNameMap;
	}

	
	
}
