//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.EnrollmentPeriodEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ProgramEligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ComplexObjectType;



/**
 * A data type for an assessment of a person's suitability to participate in a program based on various criteria.
 * 
 * <p>Java class for EligibilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EligibilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}EligibilityDateRange" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}EligibilityIndicator"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EligibilityType", propOrder = {
    "eligibilityDateRange",
    "eligibilityIndicator"
})
@XmlSeeAlso({
    ProgramEligibilityType.class,
    EnrollmentPeriodEligibilityType.class
})
public class EligibilityType
    extends ComplexObjectType
{

    @XmlElement(name = "EligibilityDateRange")
    protected DateRangeType eligibilityDateRange;
    @XmlElement(name = "EligibilityIndicator", required = true)
    protected Boolean eligibilityIndicator;

    /**
     * Gets the value of the eligibilityDateRange property.
     * 
     * @return
     *     possible object is
     *     {@link DateRangeType }
     *     
     */
    public DateRangeType getEligibilityDateRange() {
        return eligibilityDateRange;
    }

    /**
     * Sets the value of the eligibilityDateRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateRangeType }
     *     
     */
    public void setEligibilityDateRange(DateRangeType value) {
        this.eligibilityDateRange = value;
    }

    /**
     * Gets the value of the eligibilityIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEligibilityIndicator() {
        return eligibilityIndicator;
    }

    /**
     * Sets the value of the eligibilityIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEligibilityIndicator(Boolean value) {
        this.eligibilityIndicator = value;
    }

}
