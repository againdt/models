package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 *
 */
public class Income {
	
	@Expose
	private String householdMemberName;
	@Expose
	private String employerName;
	@Expose
	private Double amountPaid;
	@Expose
	private String howOften;
	@Expose
	private Double hoursPerWeek;
	@Expose
	private Integer daysPerWeek;
	@Expose
	private String firstDatePaid;
	@Expose
	private String lastDatePaid;
	@Expose
	private String oneTimeDatePaid;
	
	
	
	public Double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public String getHowOften() {
		return howOften;
	}
	public void setHowOften(String howOften) {
		this.howOften = howOften;
	}
	public Double getHoursPerWeek() {
		return hoursPerWeek;
	}
	public void setHoursPerWeek(Double hoursPerWeek) {
		this.hoursPerWeek = hoursPerWeek;
	}
	public Integer getDaysPerWeek() {
		return daysPerWeek;
	}
	public void setDaysPerWeek(Integer daysPerWeek) {
		this.daysPerWeek = daysPerWeek;
	}
	public String getFirstDatePaid() {
		return firstDatePaid;
	}
	public void setFirstDatePaid(String firstDatePaid) {
		this.firstDatePaid = firstDatePaid;
	}
	public String getLastDatePaid() {
		return lastDatePaid;
	}
	public void setLastDatePaid(String lastDatePaid) {
		this.lastDatePaid = lastDatePaid;
	}
	public String getHouseholdMemberName() {
		return householdMemberName;
	}
	public void setHouseholdMemberName(String householdMemberName) {
		this.householdMemberName = householdMemberName;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	
	
	public String getOneTimeDatePaid() {
		return oneTimeDatePaid;
	}

	public void setOneTimeDatePaid(String oneTimeDatePaid) {
		this.oneTimeDatePaid = oneTimeDatePaid;
	}
	
	
}
