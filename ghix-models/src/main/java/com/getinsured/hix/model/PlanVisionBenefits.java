package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * @author sharma_va
 *
 */

@Audited
@Entity
@Table(name = "PLAN_VISION_BENEFITS")
public class PlanVisionBenefits implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_VISION_BENEFITS_SEQ")
	@SequenceGenerator(name = "PLAN_VISION_BENEFITS_SEQ", sequenceName = "PLAN_VISION_BENEFITS_SEQ", allocationSize = 1)
	private int id;
	
	
	@ManyToOne
	@JoinColumn(name = "PLAN_VISION_ID")
	private PlanVision planVision;

	@Column(name = "BENEFIT_NAME")
	private String benefitName;
	
	@Column(name = "BENEFIT_DETAILS")
	private String benefitDetails;
	
	@Column(name = "COPAY_VAL")
	private Integer copayVal;
	
	@Column(name = "COPAY_ATTR")
	private String copayAttribute;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	public PlanVisionBenefits(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanVision getPlanVision() {
		return planVision;
	}

	public void setPlanVision(PlanVision planVision) {
		this.planVision = planVision;
	}

	public String getBenefitName() {
		return benefitName;
	}

	public void setBenefitName(String benefitName) {
		this.benefitName = benefitName;
	}

	public String getBenefitDetails() {
		return benefitDetails;
	}

	public void setBenefitDetails(String benefitDetails) {
		this.benefitDetails = benefitDetails;
	}

	public Integer getCopayVal() {
		return copayVal;
	}

	public void setCopayVal(Integer copayVal) {
		this.copayVal = copayVal;
	}

	public String getCopayAttribute() {
		return copayAttribute;
	}

	public void setCopayAttribute(String copayAttribute) {
		this.copayAttribute = copayAttribute;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	 /* To AutoUpdate created and updated dates while persisting object */
	 @PrePersist
	 public void prePersist() {
	  this.setCreationTimestamp(new TSDate());
	  this.setLastUpdateTimestamp(new TSDate());
	 }

	 /* To AutoUpdate updated dates while updating object */
	 @PreUpdate
	 public void preUpdate() {
	  this.setLastUpdateTimestamp(new TSDate());
	 }
	
}
