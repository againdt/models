package com.getinsured.hix.dto.eapp;

import com.getinsured.hix.dto.eapp.application.EappApplication;



/**
 * Contains all the details
 * needed by UI layer (Html/Angular)
 * to create web forms
 * @author root
 *
 */
public class UIRendererDTO {

	private String template;
	private EappApplication model;
	private String pdfMapping;
	
	
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public EappApplication getModel() {
		return model;
	}
	public void setModel(EappApplication model) {
		this.model = model;
	}
	public String getPdfMapping() {
		return pdfMapping;
	}
	public void setPdfMapping(String pdfMapping) {
		this.pdfMapping = pdfMapping;
	}
	
}
