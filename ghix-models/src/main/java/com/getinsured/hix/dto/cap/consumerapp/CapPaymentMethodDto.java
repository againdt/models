package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class CapPaymentMethodDto implements Serializable {

	private static final long serialVersionUID = -6285584419132348243L;

	private String comments;
	private String ecmDocumentId;
	private String paymentMethod;

	private Integer commentId;
	private Integer enrollmentId;
	private Integer paymentLookupId;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEcmDocumentId() {
		return ecmDocumentId;
	}

	public void setEcmDocumentId(String ecmDocumentId) {
		this.ecmDocumentId = ecmDocumentId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Integer getPaymentLookupId() {
		return paymentLookupId;
	}

	public void setPaymentLookupId(Integer paymentLookupId) {
		this.paymentLookupId = paymentLookupId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
