/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;

/**
 * @author negi_s
 * 
 */
public class EnrollmentDisEnrollmentDTO implements Serializable{
	
	public enum PLAN_TYPE {HLT, DEN};
	private Long ssapApplicationid;
	private List<Integer> employeeAppIdList;
	private String terminationDate;
	private String terminationReasonCode;
	private String lastPaidInvoiceDate;
	private String deathDate;
	private List<Enrollment> enrollmentList;
	private AccountUser updatedBy;
	private boolean allowRetroTermination=false;
	private boolean isInd56DisenrollmentCall = false;
	private  boolean nullifyDentalAPTCOnHealthTerm=false;
	private boolean send834=true;
	private Integer giWsPayloadId;
	private PLAN_TYPE planType;
	
	public boolean isAllowRetroTermination() {
		return allowRetroTermination;
	}
	public void setAllowRetroTermination(boolean allowRetroTermination) {
		this.allowRetroTermination = allowRetroTermination;
	}
	public Long getSsapApplicationid() {
		return ssapApplicationid;
	}
	public void setSsapApplicationid(Long ssapApplicationid) {
		this.ssapApplicationid = ssapApplicationid;
	}
	public String getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}
	public String getTerminationReasonCode() {
		return terminationReasonCode;
	}
	public void setTerminationReasonCode(String terminationReasonCode) {
		this.terminationReasonCode = terminationReasonCode;
	}
	public String getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
	public List<Integer> getEmployeeAppIdList() {
		return employeeAppIdList;
	}
	public void setEmployeeAppIdList(List<Integer> employeeAppIdList) {
		this.employeeAppIdList = employeeAppIdList;
	}
	@Override
	public String toString() {
		return "EnrollmentDisEnrollmentDTO [ssapApplicationid="
				+ ssapApplicationid + ", employeeAppIdList="
				+ employeeAppIdList + ", terminationDate=" + terminationDate
				+ ", terminationReasonCode=" + terminationReasonCode
				+ ", deathDate=" + deathDate + "]";
	}
	/**
	 * @return the enrollmentList
	 */
	public List<Enrollment> getEnrollmentList() {
		return enrollmentList;
	}
	/**
	 * @param enrollmentList the enrollmentList to set
	 */
	public void setEnrollmentList(List<Enrollment> enrollmentList) {
		this.enrollmentList = enrollmentList;
	}
	/**
	 * @return the updatedBy
	 */
	public AccountUser getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}
	public boolean isInd56DisenrollmentCall() {
		return isInd56DisenrollmentCall;
	}
	public void setInd56DisenrollmentCall(boolean isInd56DisenrollmentCall) {
		this.isInd56DisenrollmentCall = isInd56DisenrollmentCall;
	}
	public String getLastPaidInvoiceDate() {
		return lastPaidInvoiceDate;
	}
	public void setLastPaidInvoiceDate(String lastPaidInvoiceDate) {
		this.lastPaidInvoiceDate = lastPaidInvoiceDate;
	}
	public boolean isNullifyDentalAPTCOnHealthTerm() {
		return nullifyDentalAPTCOnHealthTerm;
	}
	public void setNullifyDentalAPTCOnHealthTerm(boolean nullifyDentalAPTCOnHealthTerm) {
		this.nullifyDentalAPTCOnHealthTerm = nullifyDentalAPTCOnHealthTerm;
	}
	public boolean isSend834() {
		return send834;
	}
	public void setSend834(boolean send834) {
		this.send834 = send834;
	}
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}
	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}
	public String getPlanType() {
		return planType != null ? planType.toString() : null;
	}
	public void setPlanType(PLAN_TYPE planType) {
		this.planType = planType;
	}
		
}
