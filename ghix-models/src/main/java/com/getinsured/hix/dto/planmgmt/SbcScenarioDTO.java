package com.getinsured.hix.dto.planmgmt;

public class SbcScenarioDTO {

	 	private Integer babyCoinsurance;
	    private Integer babyCopay;
	    private Integer babyDeductible;
	    private Integer babyLimit;
	   
	    private Integer diabetesCoinsurance;
	    private Integer diabetesCopay;
	    private Integer diabetesDeductible;
	    private Integer diabetesLimit;
	   
	    private Integer fractureCoinsurance;
	    private Integer fractureCopay;
	    private Integer fractureDeductible;
	    private Integer fractureLimit;
	   
	   
	    /**
	     * @return the babyCoinsurance
	     */
	    public Integer getBabyCoinsurance() {
	        return babyCoinsurance;
	    }
	    /**
	     * @param babyCoinsurance the babyCoinsurance to set
	     */
	    public void setBabyCoinsurance(Integer babyCoinsurance) {
	        this.babyCoinsurance = babyCoinsurance;
	    }
	    /**
	     * @return the babyCopay
	     */
	    public Integer getBabyCopay() {
	        return babyCopay;
	    }
	    /**
	     * @param babyCopay the babyCopay to set
	     */
	    public void setBabyCopay(Integer babyCopay) {
	        this.babyCopay = babyCopay;
	    }
	    /**
	     * @return the babyDeductible
	     */
	    public Integer getBabyDeductible() {
	        return babyDeductible;
	    }
	    /**
	     * @param babyDeductible the babyDeductible to set
	     */
	    public void setBabyDeductible(Integer babyDeductible) {
	        this.babyDeductible = babyDeductible;
	    }
	    /**
	     * @return the babyLimit
	     */
	    public Integer getBabyLimit() {
	        return babyLimit;
	    }
	    /**
	     * @param babyLimit the babyLimit to set
	     */
	    public void setBabyLimit(Integer babyLimit) {
	        this.babyLimit = babyLimit;
	    }
	    /**
	     * @return the diabetesCoinsurance
	     */
	    public Integer getDiabetesCoinsurance() {
	        return diabetesCoinsurance;
	    }
	    /**
	     * @param diabetesCoinsurance the diabetesCoinsurance to set
	     */
	    public void setDiabetesCoinsurance(Integer diabetesCoinsurance) {
	        this.diabetesCoinsurance = diabetesCoinsurance;
	    }
	    /**
	     * @return the diabetesCopay
	     */
	    public Integer getDiabetesCopay() {
	        return diabetesCopay;
	    }
	    /**
	     * @param diabetesCopay the diabetesCopay to set
	     */
	    public void setDiabetesCopay(Integer diabetesCopay) {
	        this.diabetesCopay = diabetesCopay;
	    }
	    /**
	     * @return the diabetesDeductible
	     */
	    public Integer getDiabetesDeductible() {
	        return diabetesDeductible;
	    }
	    /**
	     * @param diabetesDeductible the diabetesDeductible to set
	     */
	    public void setDiabetesDeductible(Integer diabetesDeductible) {
	        this.diabetesDeductible = diabetesDeductible;
	    }
	    /**
	     * @return the diabetesLimit
	     */
	    public Integer getDiabetesLimit() {
	        return diabetesLimit;
	    }
	    /**
	     * @param diabetesLimit the diabetesLimit to set
	     */
	    public void setDiabetesLimit(Integer diabetesLimit) {
	        this.diabetesLimit = diabetesLimit;
	    }
	    /**
	     * @return the fractureCoinsurance
	     */
	    public Integer getFractureCoinsurance() {
	        return fractureCoinsurance;
	    }
	    /**
	     * @param fractureCoinsurance the fractureCoinsurance to set
	     */
	    public void setFractureCoinsurance(Integer fractureCoinsurance) {
	        this.fractureCoinsurance = fractureCoinsurance;
	    }
	    /**
	     * @return the fractureCopay
	     */
	    public Integer getFractureCopay() {
	        return fractureCopay;
	    }
	    /**
	     * @param fractureCopay the fractureCopay to set
	     */
	    public void setFractureCopay(Integer fractureCopay) {
	        this.fractureCopay = fractureCopay;
	    }
	    /**
	     * @return the fractureDeductible
	     */
	    public Integer getFractureDeductible() {
	        return fractureDeductible;
	    }
	    /**
	     * @param fractureDeductible the fractureDeductible to set
	     */
	    public void setFractureDeductible(Integer fractureDeductible) {
	        this.fractureDeductible = fractureDeductible;
	    }
	    /**
	     * @return the fractureLimit
	     */
	    public Integer getFractureLimit() {
	        return fractureLimit;
	    }
	    /**
	     * @param fractureLimit the fractureLimit to set
	     */
	    public void setFractureLimit(Integer fractureLimit) {
	        this.fractureLimit = fractureLimit;
	    }
	   
}
