package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the PLD_SESSION_GPS database table.
 * 
 */
@Entity
@Table(name="pld_session_gps")
public class PldSessionGps  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLD_SESSION_GPS_SEQ")
	@SequenceGenerator(name = "PLD_SESSION_GPS_SEQ", sequenceName = "PLD_SESSION_GPS_SEQ", allocationSize = 1)
	private int id;

	@Column(name="SESSION_ID", nullable=false)
	private String sessionId;
	
	@Column(name="PLAN_REQUEST_NUM", nullable=false)
	private Integer planRequestNum;
	
	@Column(name="PLAN_ID", nullable=false)
	private Integer planId;
	
	@Column(name="GETINSURED_PLAN_SCORE")
	private Integer getinsuredPlanScore;
	
	@Column(name="ESTIMATED_EXPENSE")
	private Integer estimatedExpense;
	
	@Column(name="PROVIDERS_SEARCHED")
	private Integer providersSearched;
	
	@Column(name="PROVIDERS_INNETWORK")
	private Integer providersInnetwork;
	
	@Column(name="PROVIDERS_UNKNWNAFFIL")
	private Integer providersUnknwnaffil;
	
	@Column(name="PLAN_SELECTED")
	private Character planSelected;
	
	@Column(name="PLD_USER_SESSION_ID")
	private Integer pldUserSessionId;
		
	@Column(name="PLAN_SELECTED_AT_CHECKOUT")
	private Character planSelectedAtCheckout;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getPlanRequestNum() {
		return planRequestNum;
	}

	public void setPlanRequestNum(Integer planRequestNum) {
		this.planRequestNum = planRequestNum;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Integer getGetinsuredPlanScore() {
		return getinsuredPlanScore;
	}

	public void setGetinsuredPlanScore(Integer getinsuredPlanScore) {
		this.getinsuredPlanScore = getinsuredPlanScore;
	}

	public Integer getEstimatedExpense() {
		return estimatedExpense;
	}

	public void setEstimatedExpense(Integer estimatedExpense) {
		this.estimatedExpense = estimatedExpense;
	}

	public Integer getProvidersSearched() {
		return providersSearched;
	}

	public void setProvidersSearched(Integer providersSearched) {
		this.providersSearched = providersSearched;
	}

	public Integer getProvidersInnetwork() {
		return providersInnetwork;
	}

	public void setProvidersInnetwork(Integer providersInnetwork) {
		this.providersInnetwork = providersInnetwork;
	}

	public Integer getProvidersUnknwnaffil() {
		return providersUnknwnaffil;
	}

	public void setProvidersUnknwnaffil(Integer providersUnknwnaffil) {
		this.providersUnknwnaffil = providersUnknwnaffil;
	}

	public Character getPlanSelected() {
		return planSelected;
	}

	public void setPlanSelected(Character planSelected) {
		this.planSelected = planSelected;
	}

	public Integer getPldUserSessionId() {
		return pldUserSessionId;
	}

	public void setPldUserSessionId(Integer pldUserSessionId) {
		this.pldUserSessionId = pldUserSessionId;
	}

	public Character getPlanSelectedAtCheckout() {
		return planSelectedAtCheckout;
	}

	public void setPlanSelectedAtCheckout(Character planSelectedAtCheckout) {
		this.planSelectedAtCheckout = planSelectedAtCheckout;
	}

}
