package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.PdHousehold;

public class PdHouseholdDTO extends PdHousehold implements Cloneable {
	//test commit 14
	private static final long serialVersionUID = 1L;
	@Override
	  public PdHouseholdDTO clone() {
	    try {
	      return (PdHouseholdDTO)super.clone();
	    }
	    catch(CloneNotSupportedException e) {
	      throw new AssertionError(e);
	    }
	  }
}