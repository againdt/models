package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class AgencyAssistantSearchDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String agencyId;
	private String firstName;
	private String lastName;
	private String businessLegalName;
	private String staffId;
	private String approvalStatus;
	private Integer pageNumber;
	private String sortBy;
	private String sortOrder;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBusinessLegalName() {
		return businessLegalName;
	}
	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getAgencyId() {
		return agencyId;
	}
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}
	
	
}
