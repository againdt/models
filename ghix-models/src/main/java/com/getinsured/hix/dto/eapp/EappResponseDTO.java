package com.getinsured.hix.dto.eapp;

import java.util.ArrayList;
import java.util.List;

public class EappResponseDTO<T> {
	
	private List<String> errors = new ArrayList<String>();
	private boolean success = true;
	private T object;
	
	public T getObject() {
		return object;
	}
	public void setObject(T object) {
		this.object = object;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public void addError(String error){
		this.errors.add(error);
	}
	
	
}
