package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class FormularyPlanDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String formularyId;
	private String issuerPlanNumber;
	private String planName;
	
	public String getFormularyId() {
		return formularyId;
	}
	
	public void setFormularyId(String formularyId) {
		this.formularyId = formularyId;
	}
	
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	
	public String getPlanName() {
		return planName;
	}
	
	public void setPlanName(String planName) {
		this.planName = planName;
	}
}
