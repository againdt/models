package com.getinsured.hix.dto.eapp.workflow;

/**
 * Created by root on 7/27/16.
 */
public enum FlagName {
    AGENTENABLED, TENANTENABLED
}
