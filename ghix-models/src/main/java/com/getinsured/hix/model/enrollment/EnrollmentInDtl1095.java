package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.getinsured.hix.model.LookupValue;

@Audited
@Entity
@Table(name="ENROLLMENT_IN_DTL_1095")
public class EnrollmentInDtl1095 implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_IN_DTL_1095_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_IN_DTL_1095_SEQ", sequenceName = "ENROLLMENT_IN_DTL_1095_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "ENROLLMENT_IN_1095_ID")
	private EnrollmentIn1095 enrollmentIn1095;
	
	@Column(name = "ENROLLMENT_ID")
	private Integer enrollmentId;
	
	@Column(name = "ERROR_CODE")
	private String errorCode;
	
	@Column(name = "ERROR_MESSAGE")
	private String errorMessage;
	
	@Column(name = "IS_PROCESSED_FLAG")
	private String isProcessedFlag;
	
	@Column(name = "XPATH_CONTENT")
	private String xpathContent;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;
	
	@Column(name = "SCHEMA_ERROR_INFO")
	private String schemaErrorInfo;
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the enrollmentIn1095
	 */
	public EnrollmentIn1095 getEnrollmentIn1095() {
		return enrollmentIn1095;
	}

	/**
	 * @param enrollmentIn1095 the enrollmentIn1095 to set
	 */
	public void setEnrollmentIn1095(EnrollmentIn1095 enrollmentIn1095) {
		this.enrollmentIn1095 = enrollmentIn1095;
	}

	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the isProcessedFlag
	 */
	public String getIsProcessedFlag() {
		return isProcessedFlag;
	}

	/**
	 * @param isProcessedFlag the isProcessedFlag to set
	 */
	public void setIsProcessedFlag(String isProcessedFlag) {
		this.isProcessedFlag = isProcessedFlag;
	}

	/**
	 * @return the xpathContent
	 */
	public String getXpathContent() {
		return xpathContent;
	}

	/**
	 * @param xpathContent the xpathContent to set
	 */
	public void setXpathContent(String xpathContent) {
		this.xpathContent = xpathContent;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getSchemaErrorInfo() {
		return schemaErrorInfo;
	}

	public void setSchemaErrorInfo(String schemaErrorInfo) {
		this.schemaErrorInfo = schemaErrorInfo;
	}
	
}
