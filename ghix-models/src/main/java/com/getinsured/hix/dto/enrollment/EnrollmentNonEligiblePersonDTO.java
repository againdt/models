/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @author panda_p
 *
 */
public class EnrollmentNonEligiblePersonDTO implements Serializable{

	private String firstName;
	private String lastName;
	private String eligibilityReason;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEligibilityReason() {
		return eligibilityReason;
	}
	public void setEligibilityReason(String eligibilityReason) {
		this.eligibilityReason = eligibilityReason;
	}
	
	
	
}
