package com.getinsured.hix.model.eligibility;

import com.getinsured.hix.model.GHIXRequest;

/**
 * This is the Application request object. Each request
 * must have applicationId.
 * 
 * @author jyotisree
 * @deprecated - use EligibilityRequest 
 */
public class ApplicationRequest extends GHIXRequest {

	long applicationID;

	/**
	 * @return
	 */
	public long getApplicationID() {
		return applicationID;
	}

	/**
	 * @param applicationID
	 */
	public void setApplicationID(long applicationID) {
		this.applicationID = applicationID;
	}
	
	
	
	
}
