package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class PolicyInfo{
  @JsonProperty("metadata")
  
  private Object metadata;
  @JsonProperty("EnrollmentGroup")
  
  private EnrollmentGroup EnrollmentGroup;
  @JsonProperty("ActionEffectiveDate")
  
  private ActionEffectiveDate ActionEffectiveDate;
  @JsonProperty("AffectedInsurancePremium")
  
  private AffectedInsurancePremium AffectedInsurancePremium;
  public void setMetadata(Object metadata){
   this.metadata=metadata;
  }
  public Object getMetadata(){
   return metadata;
  }
  public void setEnrollmentGroup(EnrollmentGroup EnrollmentGroup){
   this.EnrollmentGroup=EnrollmentGroup;
  }
  public EnrollmentGroup getEnrollmentGroup(){
   return EnrollmentGroup;
  }
  public void setActionEffectiveDate(ActionEffectiveDate ActionEffectiveDate){
   this.ActionEffectiveDate=ActionEffectiveDate;
  }
  public ActionEffectiveDate getActionEffectiveDate(){
   return ActionEffectiveDate;
  }
  public void setAffectedInsurancePremium(AffectedInsurancePremium AffectedInsurancePremium){
   this.AffectedInsurancePremium=AffectedInsurancePremium;
  }
  public AffectedInsurancePremium getAffectedInsurancePremium(){
   return AffectedInsurancePremium;
  }
}