CREATE TABLE SSAP_APPLICANT_EVENTS(
    ID                           NUMBER    NOT NULL,
    SSAP_APPLICATION_EVENT_ID    NUMBER,
    SSAP_APPLICANT_ID            NUMBER,
    CONSTRAINT PK_SSAP_APPLICANT_EVENTS PRIMARY KEY (ID), 
    CONSTRAINT SSAP_APPLICANTS_SSAP_APPLICANT FOREIGN KEY (SSAP_APPLICANT_ID)
    REFERENCES SSAP_APPLICANTS(ID),
    CONSTRAINT SSAP_APPLICATION_EVENTS_SSAP_A FOREIGN KEY (SSAP_APPLICATION_EVENT_ID)
    REFERENCES SSAP_APPLICATION_EVENTS(ID),
	CONSTRAINT UK_SSAP_APPLICANT_EVENTS UNIQUE(SSAP_APPLICATION_EVENT_ID, SSAP_APPLICANT_ID)
)
;

COMMENT ON TABLE SSAP_APPLICANT_EVENTS IS 'Records of the events of the applicant in the application.'
;
COMMENT ON COLUMN SSAP_APPLICANT_EVENTS.SSAP_APPLICATION_EVENT_ID IS 'Foreign key from SSAP_APPLICATION_EVENTS.'
;
COMMENT ON COLUMN SSAP_APPLICANT_EVENTS.SSAP_APPLICANT_ID IS 'Foregin Key from SSAP_APPLICANTS table.'
;
CREATE INDEX SSAP_APPLICANT_EVENTS2_IND ON SSAP_APPLICANT_EVENTS(SSAP_APPLICANT_ID)
;
