package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for table EE_SITE_LOCATION_HOURS, to save the hours of
 * operation for site location.
 * 
 */
@Entity
@Table(name = "EE_SITE_LOCATION_HOURS")
public class SiteLocationHours implements Serializable, Comparable<SiteLocationHours> {
	public enum Days {
		Monday(1), Tuesday(2), Wednesday(3), Thursday(4), Friday(5), Saturday(6), Sunday(7);
		private int value;

		Days(int pValue) {
			this.setValue(pValue);
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}
	}

	private static final long serialVersionUID = 1L;
	private static final int PRIME = 31;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_SITE_LOCATION_HOURS_SEQ")
	@SequenceGenerator(name = "EE_SITE_LOCATION_HOURS_SEQ", sequenceName = "EE_SITE_LOCATION_HOURS_SEQ", allocationSize = 1)
	private int id;

	@OneToOne
	@JoinColumn(name = "EE_SITE_ID")
	private Site site;
	
	@NotEmpty
	@Column(name = "DAY")
	private String day;
	
	@NotEmpty
	@Column(name = "FROM_TIME")
	private String fromTime;

	@NotEmpty
	@Column(name = "TO_TIME")
	private String toTime;

	public SiteLocationHours() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	@Override
	public String toString() {
		return "SiteLocationHours details: ID = "+id+", SiteID: ["+(site != null ? site.getId() :"")+"]";
	}

	@Override
	public int compareTo(SiteLocationHours site2) {
		return Days.valueOf(this.day).compareTo(Days.valueOf(site2.day));
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 1;
		result = PRIME * result + ((day == null) ? 0 : day.hashCode());
		result = PRIME * result
				+ ((fromTime == null) ? 0 : fromTime.hashCode());
		result = PRIME * result + id;
		result = PRIME * result + ((site == null) ? 0 : site.hashCode());
		result = PRIME * result + ((toTime == null) ? 0 : toTime.hashCode());
		return result;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SiteLocationHours)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		SiteLocationHours other = (SiteLocationHours) obj;
		return checkSiteLocationHours(other);
	}

	/**
	 * Method to check current object's SiteLocationHours data for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSiteLocationHours(SiteLocationHours other) {
		return (checkDay(other) && checkId(other) && checkSite(other) && checkTimeData(other));
	}

	/**
	 * Method to check current object's time data for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkTimeData(SiteLocationHours other) {
		return (checkFromTime(other) && checkToTime(other));
	}

	/**
	 * Method to check current object's attribute 'toTime' for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkToTime(SiteLocationHours other) {
		if (toTime == null) {
			if (other.toTime != null) {
				return false;
			}
		} else if (!toTime.equals(other.toTime)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'fromTime' for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkFromTime(SiteLocationHours other) {
		if (fromTime == null) {
			if (other.fromTime != null) {
				return false;
			}
		} else if (!fromTime.equals(other.fromTime)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'site' for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSite(SiteLocationHours other) {
		if (site == null) {
			if (other.site != null) {
				return false;
			}
		} else if (!site.equals(other.site)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'id' for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkId(SiteLocationHours other) {
		if (id != other.id) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'day' for equality with given object.
	 * 
	 * @param other The other SiteLocationHours instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkDay(SiteLocationHours other) {
		if (day == null) {
			if (other.day != null) {
				return false;
			}
		} else if (!day.equals(other.day)) {
			return false;
		}
		return true;
	}

}