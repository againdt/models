package com.getinsured.hix.dto.broker;

import java.util.List;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;

public class HouseholdEligibilityInformationDTO {
	
	private String firstName;
	private String lastName;
	private String eligibilityStatus;
	private Double advancedPremiumTaxCredit;
	private String costSharingReduction;
	
	private List<SsapApplicantDTO> applicantEligibility;
		
	
	public List<SsapApplicantDTO> getApplicantEligibility() {
		return applicantEligibility;
	}
	public void setApplicantEligibility(List<SsapApplicantDTO> applicantEligibility) {
		this.applicantEligibility = applicantEligibility;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}	
	public String getCostSharingReduction() {
		return costSharingReduction;
	}
	public void setCostSharingReduction(String costSharingReduction) {
		this.costSharingReduction = costSharingReduction;
	}
	public Double getAdvancedPremiumTaxCredit() {
		return advancedPremiumTaxCredit;
	}
	public void setAdvancedPremiumTaxCredit(Double advancedPremiumTaxCredit) {
		this.advancedPremiumTaxCredit = advancedPremiumTaxCredit;
	}
	
	
	

}
