package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the GIMONITOR_GIWSPAYLOAD database table.
 *
 */
@Entity
@Table(name="GIMONITOR_GIWSPAYLOAD_EXT")
public class SsapMonitorPayload implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GIMONITOR_GIWSPAYLOAD_SEQ", sequenceName="GIMONITOR_GIWSPAYLOAD_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GIMONITOR_GIWSPAYLOAD_SEQ")
	@Column(unique=true, nullable=false, precision=22)
	private Long id;

	@Column(name="GI_WS_PAYLOAD_ID")
	private Long giWsPayloadId;

	@Column(name="GI_MONITOR_ID", nullable=false, precision=22)
	private Long giMonitorId;

	@Column(name="SSAP_APPLICATION_ID")
	private Long ssapApplicationId;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	@Column(name="CORRELATION_ID")
	private String correlationId;
	
	@Column(name="HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId;
	
	
	public SsapMonitorPayload() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGiWsPayloadId() {
		return this.giWsPayloadId;
	}

	public void setGiWsPayloadId(Long giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public Long getGiMonitorId() {
		return giMonitorId;
	}

	public void setGiMonitorId(Long giMonitorId) {
		this.giMonitorId = giMonitorId;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	@Override
	public String toString() {
		return "SsapMonitorPayload [id=" + id + ", giWsPayloadId="
				+ giWsPayloadId + ", giMonitorId=" + giMonitorId
				+ ", ssapApplicationId=" + ssapApplicationId
				+ ", creationTimestamp=" + creationTimestamp
				+ ", lastUpdateTimestamp=" + lastUpdateTimestamp
				+ ", correlationId=" + correlationId + ", houseHoldCaseId="
				+ houseHoldCaseId + "]";
	}
}