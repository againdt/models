package com.getinsured.hix.dto.agency;

import java.util.List;
 

public class AgencyCertificationStatusDto {
	String id;
	String creationDate;
	String certificationNumber;
	String certificationDate;
	String applicationSubmissionDate;
	String recertifyDate;
	String certificationStatus;
	String status;
	String comment;
	String declarationDocId;
	String supportDocId;
	String contractDocId;
	int agentCount;
	List<String> agencyCertificationStatusList;
	List<AgencyCertificationHistory> agencyCertificationHistory;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getCertificationNumber() {
		return certificationNumber;
	}
	public void setCertificationNumber(String certificationNumber) {
		this.certificationNumber = certificationNumber;
	}
	public String getCertificationDate() {
		return certificationDate;
	}
	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}
	public String getRecertifyDate() {
		return recertifyDate;
	}
	public void setRecertifyDate(String recertifyDate) {
		this.recertifyDate = recertifyDate;
	}
	public String getCertificationStatus() {
		return certificationStatus;
	}
	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	public List<AgencyCertificationHistory> getAgencyCertificationHistory() {
		return agencyCertificationHistory;
	}
	public void setAgencyCertificationHistory(List<AgencyCertificationHistory> agencyCertificationHistory) {
		this.agencyCertificationHistory = agencyCertificationHistory;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeclarationDocId() {
		return declarationDocId;
	}
	public void setDeclarationDocId(String declarationDocId) {
		this.declarationDocId = declarationDocId;
	}
	public String getSupportDocId() {
		return supportDocId;
	}
	public void setSupportDocId(String supportDocId) {
		this.supportDocId = supportDocId;
	}
	public String getContractDocId() {
		return contractDocId;
	}
	public void setContractDocId(String contractDocId) {
		this.contractDocId = contractDocId;
	}
	public List<String> getAgencyCertificationStatusList() {
		return agencyCertificationStatusList;
	}
	public void setAgencyCertificationStatusList(List<String> agencyCertificationStatusList) {
		this.agencyCertificationStatusList = agencyCertificationStatusList;
	}
	public String getApplicationSubmissionDate() {
		return applicationSubmissionDate;
	}
	public void setApplicationSubmissionDate(String applicationSubmissionDate) {
		this.applicationSubmissionDate = applicationSubmissionDate;
	}
	public int getAgentCount() {
		return agentCount;
	}
	public void setAgentCount(int agentCount) {
		this.agentCount = agentCount;
	}
	
}
