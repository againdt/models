Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VERIFY_PLAN');

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VERIFY_PLAN')));


