/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;


/**
 * Member Contribution Object, which holds the member level contribution Amounts
 * 
 * @author Suresh KAncherla
 * @since 10 July, 2015
 * 
 */
public class MemberContribution {

	private int memberNumber;

	private String memberId;

	 private Double hraAmount;

	private Double employerContributionAmount;

	private String dateOfBirth;

	private MemberRelationships relationshipToPrimary;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FamilyMember [memberNumber=");
		builder.append(memberNumber);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);

		builder.append(", relationshipToPrimary=")
				.append(relationshipToPrimary);
		builder.append("]");
		return builder.toString();
	}

	public int getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(int memberNumber) {
		this.memberNumber = memberNumber;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Double getEmployerContributionAmount() {
		return employerContributionAmount;
	}

	public void setEmployerContributionAmount(Double employerContributionAmount) {
		this.employerContributionAmount = employerContributionAmount;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

	public Double getHraAmount() {
		return hraAmount;
	}

	public void setHraAmount(Double hraAmount) {
		this.hraAmount = hraAmount;
	}

	/*
	 * public class MemberComparatorByMemberNumber implements
	 * Comparator<MemberContribution>{
	 * 
	 * @Override public int compare(MemberContribution memberObj1,
	 * MemberContribution memberObj2) { return memberObj1.getMemberNumber() <
	 * memberObj2.getMemberNumber() ? -1 : (memberObj1.getMemberNumber() ==
	 * memberObj2.getMemberNumber() ? 0 : 1) ; }
	 * 
	 * }
	 */

}
