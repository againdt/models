package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



/**
 * The persistent class for the facility_speciality_relation database table.
 * 
 */
@Entity

@Table(name="facility_speciality_relation")
public class FacilitySpecialityRelation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FacilitySpecialityRelation_Seq")
	@SequenceGenerator(name = "FacilitySpecialityRelation_Seq", sequenceName = "facility_speciality_relati_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="specialty_id")
	private Specialty specialty;
	
	@ManyToOne
    @JoinColumn(name="facility_id")
	private Facility facility;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Specialty getSpecialty() {
		return specialty;
	}

	public void setSpeciality(Specialty specialty) {
		this.specialty = specialty;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}
	
			
}