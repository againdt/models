package com.getinsured.hix.dto.eapp.grammer.components.ui;

import com.getinsured.hix.dto.eapp.grammer.UIComponent;
import com.getinsured.hix.dto.eapp.grammer.UIComponentType;

public class UIComponentTemplateFactory {
	
	public static UIComponent createUIComponentTemplate(String type){
		UIComponentType typeEnum = UIComponentType.getInstance(type);
		switch(typeEnum){
		
		case TEXT : return new TextComponent();
		case GROUP : return new GroupComponent();
		case RADIO : return new RadioComponent();
		case SELECT : return new SelectComponent();
		default : return new BaseComponent();
		}
		
	}
	
	
	
	
	
}
