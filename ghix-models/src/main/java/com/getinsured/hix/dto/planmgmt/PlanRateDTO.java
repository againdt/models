package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Date;

public class PlanRateDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ratingArea;
	private Integer minAge;
	private Integer maxAge;
	private String tabacco;
	private Float rate;
	private Date effectiveStartDate;
	private Date effectiveEndDate;

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public String getTabacco() {
		return tabacco;
	}

	public void setTabacco(String tabacco) {
		this.tabacco = tabacco;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

}
