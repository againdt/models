package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerSearchRequestDTO extends SearchDTO {

	private String brandNameId;
	private String d2cFlowYear;
	private String d2cFlowTypeId;
	private String hiosId;
	private String issuerName;
	private String status;
	private String tenantCode;
	private Boolean retrieveBrandNameList;
	private String enrollmentFlowType;
	
	private int applicabelYear;
	private String state;
	private String insuranceType;

	public IssuerSearchRequestDTO() {
	}

	public String getBrandNameId() {
		return brandNameId;
	}

	public void setBrandNameId(String brandNameId) {
		this.brandNameId = brandNameId;
	}

	public String getD2cFlowYear() {
		return d2cFlowYear;
	}

	public void setD2cFlowYear(String d2cFlowYear) {
		this.d2cFlowYear = d2cFlowYear;
	}

	public String getD2cFlowTypeId() {
		return d2cFlowTypeId;
	}

	public void setD2cFlowTypeId(String d2cFlowTypeId) {
		this.d2cFlowTypeId = d2cFlowTypeId;
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Boolean getRetrieveBrandNameList() {
		return retrieveBrandNameList;
	}

	public void setRetrieveBrandNameList(Boolean retrieveBrandNameList) {
		this.retrieveBrandNameList = retrieveBrandNameList;
	}

	public String getEnrollmentFlowType() {
		return enrollmentFlowType;
	}

	public void setEnrollmentFlowType(String enrollmentFlowType) {
		this.enrollmentFlowType = enrollmentFlowType;
	}

	public int getApplicabelYear() {
		return applicabelYear;
	}

	public void setApplicabelYear(int applicabelYear) {
		this.applicabelYear = applicabelYear;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	
	
}
