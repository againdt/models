INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='L2_CSR'), (select id from permissions where name='PORTAL_INBOX'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='PORTAL_ACCOUNT_SETTINGS'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='PORTAL_CHANGE_PASSWORD'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='PORTAL_CHANGE_SEC_QSTNS'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='PORTAL_APPEALS'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='PORTAL_REFERRALS'));	
