package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to get response for renewal status API.
 */
public class RenewalResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer statusCode;
	private String message; 
	private Object data;

	public RenewalResponse() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
