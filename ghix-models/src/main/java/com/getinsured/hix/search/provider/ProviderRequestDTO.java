package com.getinsured.hix.search.provider;

import java.io.Serializable;

public class ProviderRequestDTO implements Serializable{
	
	private static final long serialVersionUID = -997931219993336262L;
	
	private String searchKey;
	private String searchType;
	private String requestType;
	
	private String zipCode;
	private String otherLocation;
	private String distance;
	private ZipCode userLocation;
	
	
	private Integer currentPageNumber;
	//Start Record Num in SOLR
	private Integer recordStartNumber;
	//Rows per page in SOLR
	private Integer pageSize;
	
	//Filters
	private String language;
	private String gender;
	private String providerSpecialty;
	
	private String carrierNetworks;

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getOtherLocation() {
		return otherLocation;
	}

	public void setOtherLocation(String otherLocation) {
		this.otherLocation = otherLocation;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public ZipCode getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(ZipCode userLocation) {
		this.userLocation = userLocation;
	}

	public Integer getCurrentPageNumber() {
		return currentPageNumber;
	}

	public void setCurrentPageNumber(Integer currentPageNumber) {
		this.currentPageNumber = currentPageNumber;
	}

	public Integer getRecordStartNumber() {
		return (currentPageNumber!=0?((currentPageNumber-1) * this.pageSize):0);
	}

	public void setRecordStartNumber(Integer recordStartNumber) {
		this.recordStartNumber = recordStartNumber;
	}

	public Integer getPageSize() {
		return (this.pageSize != null && this.pageSize != 0 ? this.pageSize:4);
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProviderSpecialty() {
		return providerSpecialty;
	}

	public void setProviderSpecialty(String providerSpecialty) {
		this.providerSpecialty = providerSpecialty;
	}

	public String getCarrierNetworks() {
		return carrierNetworks;
	}

	public void setCarrierNetworks(String carrierNetworks) {
		this.carrierNetworks = carrierNetworks;
	}
	
}
