insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.IsExternal','FALSE','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.IsEmailActivation','TRUE','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.IsSecurityQuestionsRequired','TRUE','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.ActivationEmailExpirationDays.Employer','30','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.ActivationEmailExpirationDays.Issuer','15','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.ActivationEmailExpirationDays.Employee','30','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.UserRegistration.ActivationEmailExpirationDays.User','15','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.HideCaptcha','N','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.HideTermsAndConditions','N','',null,current_timestamp,null,current_timestamp);

