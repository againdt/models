package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import com.getinsured.hix.model.entity.Site;

public class SiteDTO  implements Serializable{
//	private int id;
//
//	private EnrollmentEntity entity;
//
//	private String primarySiteLocationName;
//
//	private String primaryEmailAddress;
//
//	private String primaryPhoneNumber;
//
//	private String secondaryPhoneNumber;
//
//	private EntityLocation mailingLocation;
//
//	private EntityLocation physicalLocation;
//
//	private Date createdOn;
//
//	private Date updatedOn;
//
//	private String createdBy;
//
//	private String updatedBy;
//
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	public EnrollmentEntity getEntity() {
//		return entity;
//	}
//
//	public void setEntity(EnrollmentEntity entity) {
//		this.entity = entity;
//	}
//
//	public String getPrimarySiteLocationName() {
//		return primarySiteLocationName;
//	}
//
//	public void setPrimarySiteLocationName(String primarySiteLocationName) {
//		this.primarySiteLocationName = primarySiteLocationName;
//	}
//
//	public String getPrimaryEmailAddress() {
//		return primaryEmailAddress;
//	}
//
//	public void setPrimaryEmailAddress(String primaryEmailAddress) {
//		this.primaryEmailAddress = primaryEmailAddress;
//	}
//
//	public String getPrimaryPhoneNumber() {
//		return primaryPhoneNumber;
//	}
//
//	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
//		this.primaryPhoneNumber = primaryPhoneNumber;
//	}
//
//	public String getSecondaryPhoneNumber() {
//		return secondaryPhoneNumber;
//	}
//
//	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
//		this.secondaryPhoneNumber = secondaryPhoneNumber;
//	}
//
//	public EntityLocation getMailingLocation() {
//		return mailingLocation;
//	}
//
//	public void setMailingLocation(EntityLocation mailingLocation) {
//		this.mailingLocation = mailingLocation;
//	}
//
//	public EntityLocation getPhysicalLocation() {
//		return physicalLocation;
//	}
//
//	public void setPhysicalLocation(EntityLocation physicalLocation) {
//		this.physicalLocation = physicalLocation;
//	}
//
//	public Date getCreatedOn() {
//		return createdOn;
//	}
//
//	public void setCreatedOn(Date createdOn) {
//		this.createdOn = createdOn;
//	}
//
//	public Date getUpdatedOn() {
//		return updatedOn;
//	}
//
//	public void setUpdatedOn(Date updatedOn) {
//		this.updatedOn = updatedOn;
//	}
//
//	public String getCreatedBy() {
//		return createdBy;
//	}
//
//	public void setCreatedBy(String createdBy) {
//		this.createdBy = createdBy;
//	}
//
//	public String getUpdatedBy() {
//		return updatedBy;
//	}
//
//	public void setUpdatedBy(String updatedBy) {
//		this.updatedBy = updatedBy;
//	}
	
	private Site site;

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}
	
	@Override
	public String toString() {
		return "SiteDTO details: Site = ["+site+"]";
	}
}
