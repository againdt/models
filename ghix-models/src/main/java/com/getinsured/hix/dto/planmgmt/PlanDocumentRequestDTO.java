package com.getinsured.hix.dto.planmgmt;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO class is used to set request for Plan Documents to update data in PLAN table.
 */
public class PlanDocumentRequestDTO {

	public static final int POS_BROCHURE_ECM = 0;
	public static final int POS_BROCHURE_FILE = 1;
	public static final int POS_SBC_ECM = 2;
	public static final int POS_SBC_FILE = 3;
	public static final int POS_PLAN_ISSUER_NUM = 4;
	public static final int POS_APPLICABLE_YEAR = 5;

	private Map<String, String[]> planIdAndDocumentsMap;

	public PlanDocumentRequestDTO() {
	}

	public Map<String, String[]> getPlanIdAndDocumentsMap() {
		return planIdAndDocumentsMap;
	}

	public void setPlanIdAndDocuments(String key, String valueArray[]) {

		if (null == planIdAndDocumentsMap) {
			planIdAndDocumentsMap = new HashMap<String, String[]>();
		}
		planIdAndDocumentsMap.put(key, valueArray);
	}
}
