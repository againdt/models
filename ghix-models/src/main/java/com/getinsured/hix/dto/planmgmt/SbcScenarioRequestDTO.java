/**
 * 
 * @author santanu
 * @version 1.0
 * @since May 26, 2017 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

public class SbcScenarioRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<String> hiosPlanIdList;
	
	private Integer applicableYear;

	/**
	 * @return the hiosPlanIdList
	 */
	public List<String> getHiosPlanIdList() {
		return hiosPlanIdList;
	}

	/**
	 * @param hiosPlanIdList the hiosPlanIdList to set
	 */
	public void setHiosPlanIdList(List<String> hiosPlanIdList) {
		this.hiosPlanIdList = hiosPlanIdList;
	}

	/**
	 * @return the applicableYear
	 */
	public Integer getApplicableYear() {
		return applicableYear;
	}

	/**
	 * @param applicableYear the applicableYear to set
	 */
	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}
	
	
}
