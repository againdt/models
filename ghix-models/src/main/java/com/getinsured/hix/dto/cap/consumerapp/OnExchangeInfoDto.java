package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class OnExchangeInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3631991040723913337L;

	private int id;
	private String state;
	private String url;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
