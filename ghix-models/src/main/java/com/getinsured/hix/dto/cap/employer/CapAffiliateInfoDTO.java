package com.getinsured.hix.dto.cap.employer;

import java.util.List;

import com.getinsured.hix.dto.cap.consumerapp.AffiliateInfoDTO;

public class CapAffiliateInfoDTO {
	
	boolean isDropDown;
	
	List<AffiliateInfoDTO> affiliateInfoDTO;
	
	public boolean isDropDown() {
		return isDropDown;
	}

	public void setDropDown(boolean isDropDown) {
		this.isDropDown = isDropDown;
	}

	public List<AffiliateInfoDTO> getAffiliateInfoDTO() {
		return affiliateInfoDTO;
	}

	public void setAffiliateInfoDTO(List<AffiliateInfoDTO> affiliateInfoDTO) {
		this.affiliateInfoDTO = affiliateInfoDTO;
	}

}