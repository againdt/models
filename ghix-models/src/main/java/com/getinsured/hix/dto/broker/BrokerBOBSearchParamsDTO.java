package com.getinsured.hix.dto.broker;

import java.util.List;

public class BrokerBOBSearchParamsDTO {

	private Integer startRecord;
	private Integer endRecord;
	private String firstName;
	private String lastName;
	private String applicationType;
	/**
	 * Current Status of the Application 
	 * No Application Found/Incomplete Application/Application Submitted/Eligible for Shopping/Enrolled in a Qualified Plan/Closed/Canceled Application/No Active Enrollment
	 */
	private String currentStatus;
	private String dueDate;
	private String nextStep;
	private String issuerval;
	private String sortBy;
	private String sortOder;
	private String coverageYear;
	private String applicationYear;
	private int totalIndividualCount;
	
	/*
	 * List of individual ids to be fetched.
	 */
	private List<Long> individualIds;
	
	/**
	 * An identifier to uniquely identify each Agent/Assister/Entity
	 */
	 private Long recordId;
	
	 /**
	  * Identifies the type of user i.e.Agent/Assister/Entity
	  */
	 private String recordType;
	 
	 /**
	  * An identifier to identify the type of delegations to be fetched Pending/Active/In-Active
	  */
	 private String designationStatus;
	 
	 /**
	  * An identifier to indicate page number to be fetched.
	  */
	 private Integer pageNumber;
	 
	 /**
	  * An identifier to indicate number of records on single page.
	  */
	 private Integer pageSize;
	 
	 /**
	  * An identifier to indicate User selected Request-From Date.
	  */
	 private String requestSentFrmDt;
	 
	 /**
	  * An identifier to indicate User selected Request-to Date.
	  */
	 private String requestSentToDt;
	 
	 /**
	  * An identifier to indicate User selected Inactive-Since-From Date.
	  */
	 private String inactiveSinceFrmDt;
	 
	 /**
	  * An identifier to indicate User selected Inactive-Since-to Date.
	  */
	 private String inactiveSinceToDt;
	 
	 /**
	  *  An identifier to indicate status of application
	  *  OPEN("OP")/SIGNED("SG")/SUBMITTED("SU")/ELIGIBILITY_RECEIVED("ER")/ENROLLED_OR_ACTIVE("EN")/CANCELLED("CC")/CLOSED("CL")/UNCLAIMED("UC")
	  */
	 private String aplicationStatus;
	 
	 
	 /**
	  *  An identifier to indicate status of application
	  *  Eligible/Ineligible/Conditional Eligible 
	  */
	 private String eligibilityStatus;
	 
	 private String counselorFirstName; 
	 
	 private String counselorLastName; 
	 
	 private String activeSinceFromDate;
	 
	 private String activeSinceToDate;
	
	 private String informationType;
	
	 /**
	  * An identifier to indicate state of enrollment wise Pending/Enrolled/Terminated/Cancelled
	  */
	 private String enrollmentStatus;
	
	public String getInformationType() {
		return informationType;
	}
	public void setInformationType(String informationType) {
		this.informationType = informationType;
	}
	public String getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOder() {
		return sortOder;
	}
	public void setSortOder(String sortOder) {
		this.sortOder = sortOder;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getNextStep() {
		return nextStep;
	}
	public void setNextStep(String nextStep) {
		this.nextStep = nextStep;
	}	
	public Integer getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(Integer startRecord) {
		this.startRecord = startRecord;
	}
	public Integer getEndRecord() {
		return endRecord;
	}
	public void setEndRecord(Integer endRecord) {
		this.endRecord = endRecord;
	}
	public String getIssuerval() {
		return issuerval;
	}
	public void setIssuerval(String issuerval) {
		this.issuerval = issuerval;
	}
	
	
	public List<Long> getIndividualIds() {
		return individualIds;
	}
	public void setIndividualIds(List<Long> individualIds) {
		this.individualIds = individualIds;
	}
	public Long getRecordId() {
		return recordId;
	}
	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getDesignationStatus() {
		return designationStatus;
	}
	public void setDesignationStatus(String designationStatus) {
		this.designationStatus = designationStatus;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getRequestSentFrmDt() {
		return requestSentFrmDt;
	}
	public void setRequestSentFrmDt(String requestSentFrmDt) {
		this.requestSentFrmDt = requestSentFrmDt;
	}
	public String getRequestSentToDt() {
		return requestSentToDt;
	}
	public void setRequestSentToDt(String requestSentToDt) {
		this.requestSentToDt = requestSentToDt;
	}
	public String getInactiveSinceFrmDt() {
		return inactiveSinceFrmDt;
	}
	public void setInactiveSinceFrmDt(String inactiveSinceFrmDt) {
		this.inactiveSinceFrmDt = inactiveSinceFrmDt;
	}
	public String getInactiveSinceToDt() {
		return inactiveSinceToDt;
	}
	public void setInactiveSinceToDt(String inactiveSinceToDt) {
		this.inactiveSinceToDt = inactiveSinceToDt;
	}
	public String getAplicationStatus() {
		return aplicationStatus;
	}
	public void setAplicationStatus(String aplicationStatus) {
		this.aplicationStatus = aplicationStatus;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public String getCounselorFirstName() {
		return counselorFirstName;
	}
	public void setCounselorFirstName(String counselorFirstName) {
		this.counselorFirstName = counselorFirstName;
	}
	public String getCounselorLastName() {
		return counselorLastName;
	}
	public void setCounselorLastName(String counselorLastName) {
		this.counselorLastName = counselorLastName;
	}
	public String getActiveSinceFromDate() {
		return activeSinceFromDate;
	}
	public void setActiveSinceFromDate(String activeSinceFromDate) {
		this.activeSinceFromDate = activeSinceFromDate;
	}
	public String getActiveSinceToDate() {
		return activeSinceToDate;
	}
	public void setActiveSinceToDate(String activeSinceToDate) {
		this.activeSinceToDate = activeSinceToDate;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public int getTotalIndividualCount() {
		return totalIndividualCount;
	}
	public void setTotalIndividualCount(int totalIndividualCount) {
		this.totalIndividualCount = totalIndividualCount;
	}
	public String getApplicationYear() {
		return applicationYear;
	}
	public void setApplicationYear(String applicationYear) {
		this.applicationYear = applicationYear;
	}

}
