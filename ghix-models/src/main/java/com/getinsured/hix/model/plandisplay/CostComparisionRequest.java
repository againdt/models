/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GroupList;
import com.getinsured.hix.model.PlanRateBenefit;

/**
 * @author Mishra_m
 *
 */
public class CostComparisionRequest {
	
	//private List<Long> planIds;
	private Map<String,Integer> personalUsageForMedical;
	private Map<String,Integer> personalUsageForDrugs;
	private int familyMembers;
	private List<PlanRateBenefit> plans;
	
	public Map<String, Integer> getPersonalUsageForMedical() {
		return personalUsageForMedical;
	}
	public void setPersonalUsageForMedical(
			Map<String, Integer> personalUsageForMedical) {
		this.personalUsageForMedical = personalUsageForMedical;
	}
	public Map<String, Integer> getPersonalUsageForDrugs() {
		return personalUsageForDrugs;
	}
	public void setPersonalUsageForDrugs(Map<String, Integer> personalUsageForDrugs) {
		this.personalUsageForDrugs = personalUsageForDrugs;
	}
	public int getFamilyMembers() {
		return familyMembers;
	}
	public void setFamilyMembers(int familyMembers) {
		this.familyMembers = familyMembers;
	}
	
	public List<PlanRateBenefit> getPlans() {
		return plans;
	}
	public void setPlans(List<PlanRateBenefit> plans) {
		this.plans = plans;
	}

}
