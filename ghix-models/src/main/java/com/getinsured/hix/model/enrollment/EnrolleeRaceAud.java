/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.LookupValue;


@Entity
@XmlAccessorType(XmlAccessType.NONE)
@IdClass(AudId.class)
@Table(name="ENROLLEE_RACE_AUD")
public class EnrolleeRaceAud implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	@Column(name = "REV")
	private int rev;
	
	@Column(name = "REVTYPE")
	private int revType;

	@XmlElement(name="raceEthnicityLkp")
	@OneToOne
	@JoinColumn(name = "RACE_ETHNICITY_LKP")
	private LookupValue raceEthnicityLkp;
	

	@ManyToOne(cascade= {CascadeType.ALL})
    @JoinColumn(name="ENROLEE_ID")
	private Enrollee enrollee;
	
	@Column(name="RACE_DESCRIPTION",nullable=true)
	private String raceDescription;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@OneToOne
	@JoinColumn(name="created_by")
	private AccountUser createdBy;
	
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;

	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public int getRevType() {
		return revType;
	}

	public void setRevType(int revType) {
		this.revType = revType;
	}

	public LookupValue getRaceEthnicityLkp() {
		return raceEthnicityLkp;
	}

	public void setRaceEthnicityLkp(LookupValue raceEthnicityLkp) {
		this.raceEthnicityLkp = raceEthnicityLkp;
	}

	public Enrollee getEnrollee() {
		return enrollee;
	}

	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	public String getRaceDescription() {
		return raceDescription;
	}

	public void setRaceDescription(String raceDescription) {
		this.raceDescription = raceDescription;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

}
