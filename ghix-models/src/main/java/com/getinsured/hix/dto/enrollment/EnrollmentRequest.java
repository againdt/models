/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.enrollment.EnrolleeAttributeEnum;
import com.getinsured.hix.model.enrollment.EnrollmentAttributeEnum;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.employerdisenrollment.EmployerDisEnrollmentRequest;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementRequest;

/**
 * @author panda_p
 *
 */
public class EnrollmentRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum EnrollmentStatus{
		APPROVED, INFORCE, WITHDRAWN, ESIG_PENDING,	DOES_NOT_EXIST, SUSPENDED,
		PAYMENT_PROCESSED, PAYMENT_FAILED, DECLINED, CANCEL, TERM, CONFIRM, ORDER_CONFIRMED,
		PAYMENT_RECEIVED, PENDING, ABORTED, ECOMMITTED, TERM_ACTIVE;
	}

	public enum Role{
		ASSISTER,AGENT
	}

	public enum MarketType {
		INDIVIDUAL("FI"), 
		SHOP("24"); 

		private final String marketTypeValue;

		private MarketType(String marketTypeValue) {
			this.marketTypeValue = marketTypeValue;
		}
		@Override
		public String toString() {
			return marketTypeValue;
		}
	}

	private List<Integer> idList;
	private Map<String,Object> mapValue;

	private Integer employerId;
	private Integer employeeId;
	private Integer enrollmentId;

	private Integer planId;

	private String issuerName;
	private String planName;
	private String planLevel;
	private String ratingRegion;
	private String startDate;
	private String endDate;
	private String planNumber;
	private Long employeeAppId;

	private Enum<MarketType> planMarket;
	private Enum<EnrollmentStatus> enrollmentStatus;
	private Enum<Role> role;

	private AdminUpdateIndividualRequest adminUpdateIndividualRequest;

	private EmployerDisEnrollmentRequest employerDisEnrollmentRequest;
	private Integer assisterBrokerId;
	private EnrollmentReinstatementRequest enrollmentReinstatementRequest;

	private String ffeAssignedPolNo;
	private String issuerAssignedPolNo;

	private String employeeName;

	private Date employerEnrollmentStartDate;
	private Date employerEnrollmentEndDate;

	private Integer employerEnrollmentId; 
	private EnrollmentDisEnrollmentDTO enrollmentDisEnrollmentDTO;
	private String status;
	
	private Float aptcAmt;
	private Date aptcEffectiveDate;
	private List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList;
	
	private EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateData;
	
	private String dateClosed;
	
	private EnrollmentMailingAddressUpdateRequest enrollmentMailingAddressUpdateRequest; 
	
	/*
	 * New Changes for Jira ID: HIX-71256
	 */
	private List<String> houseHoldCaseIdList;
	private List<EnrollmentStatus> enrollmentStatusList;
	private String coverageYear;
	private Long ssapApplicationId;
	private boolean isPremiumDataRequired = false;
	private List<EnrollmentAttributeEnum> enrollmentAttributeList;
	private List<EnrolleeAttributeEnum> enrolleeAttributeList;
	private String loggedInUserName;
	private List<String> memberIdList;
	private List<String> coverageYearList;
	private Integer subscriberEventId;

	private boolean isSepDataRequired = false;
	//HIX-109786 External Case Id list for wrapper API
	private List<String> externalCaseIdList;
	//HIX-112546 External member ID
	private String externalMemberId;
	private String changeEffectiveDate;

	public EnrollmentBrokerUpdateDTO getEnrollmentBrokerUpdateData() {
		return enrollmentBrokerUpdateData;
	}

	public void setEnrollmentBrokerUpdateData(
			EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateData) {
		this.enrollmentBrokerUpdateData = enrollmentBrokerUpdateData;
	}

	public Map<String, Object> getMapValue() {
		return mapValue;
	}
	
	public void setMapValue(Map<String, Object> mapValue) {
		this.mapValue = mapValue;
	}

	public AdminUpdateIndividualRequest getAdminUpdateIndividualRequest() {
		return adminUpdateIndividualRequest;
	}
	public void setAdminUpdateIndividualRequest(AdminUpdateIndividualRequest adminUpdateIndividualRequest) {
		this.adminUpdateIndividualRequest = adminUpdateIndividualRequest;
	}
	public Integer getEmployerId() {
		return employerId;
	}
	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Enum<EnrollmentStatus> getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(Enum<EnrollmentStatus> enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public Enum<MarketType> getPlanMarket() {
		return planMarket;
	}
	public void setPlanMarket(Enum<MarketType> planMarket) {
		this.planMarket = planMarket;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	public String getRatingRegion() {
		return ratingRegion;
	}
	public void setRatingRegion(String ratingRegion) {
		this.ratingRegion = ratingRegion;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getPlanNumber() {
		return planNumber;
	}
	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}
	public EmployerDisEnrollmentRequest getEmployerDisEnrollmentRequest() {
		return employerDisEnrollmentRequest;
	}
	public void setEmployerDisEnrollmentRequest(
			EmployerDisEnrollmentRequest employerDisEnrollmentRequest) {
		this.employerDisEnrollmentRequest = employerDisEnrollmentRequest;
	}
	public Integer getAssisterBrokerId() {
		return assisterBrokerId;
	}
	public void setAssisterBrokerId(Integer assisterBrokerId) {
		this.assisterBrokerId = assisterBrokerId;
	}
	public Enum<Role> getRole() {
		return role;
	}
	public void setRole(Enum<Role> role) {
		this.role = role;
	}
	public String getFfeAssignedPolNo() {
		return ffeAssignedPolNo;
	}
	public void setFfeAssignedPolNo(String ffeAssignedPolNo) {
		this.ffeAssignedPolNo = ffeAssignedPolNo;
	}
	public String getIssuerAssignedPolNo() {
		return issuerAssignedPolNo;
	}
	public void setIssuerAssignedPolNo(String issuerAssignedPolNo) {
		this.issuerAssignedPolNo = issuerAssignedPolNo;
	}
	public EnrollmentReinstatementRequest getEnrollmentReinstatementRequest() {
		return enrollmentReinstatementRequest;
	}
	public void setEnrollmentReinstatementRequest(
			EnrollmentReinstatementRequest enrollmentReinstatementRequest) {
		this.enrollmentReinstatementRequest = enrollmentReinstatementRequest;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getEmployerEnrollmentStartDate() {
		return employerEnrollmentStartDate;
	}
	public void setEmployerEnrollmentStartDate(Date employerEnrollmentStartDate) {
		this.employerEnrollmentStartDate = employerEnrollmentStartDate;
	}
	public Date getEmployerEnrollmentEndDate() {
		return employerEnrollmentEndDate;
	}
	public void setEmployerEnrollmentEndDate(Date employerEnrollmentEndDate) {
		this.employerEnrollmentEndDate = employerEnrollmentEndDate;
	}
	public Long getEmployeeAppId() {
		return employeeAppId;
	}
	public void setEmployeeAppId(Long employeeAppId) {
		this.employeeAppId = employeeAppId;
	}
	public List<Integer> getIdList() {
		return idList;
	}
	public void setIdList(List<Integer> idList) {
		this.idList = idList;
	}
	public EnrollmentDisEnrollmentDTO getEnrollmentDisEnrollmentDTO() {
		return enrollmentDisEnrollmentDTO;
	}
	public void setEnrollmentDisEnrollmentDTO(
			EnrollmentDisEnrollmentDTO enrollmentDisEnrollmentDTO) {
		this.enrollmentDisEnrollmentDTO = enrollmentDisEnrollmentDTO;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}
	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}
	
	public Float getAptcAmt() {
		return aptcAmt;
	}

	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}
	
	public Date getAptcEffectiveDate() {
		return aptcEffectiveDate;
	}

	public void setAptcEffectiveDate(Date aptcEffectiveDate) {
		this.aptcEffectiveDate = aptcEffectiveDate;
	}
	
	public List<EnrollmentAptcUpdateDto> getEnrollmentAptcUpdateDtoList() {
		return enrollmentAptcUpdateDtoList;
	}

	public void setEnrollmentAptcUpdateDtoList(
			List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList) {
		this.enrollmentAptcUpdateDtoList = enrollmentAptcUpdateDtoList;
	}
	
	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}

	/**
	 * @return the houseHoldCaseIdList
	 */
	public List<String> getHouseHoldCaseIdList() {
		return houseHoldCaseIdList;
	}

	/**
	 * @param houseHoldCaseIdList the houseHoldCaseIdList to set
	 */
	public void setHouseHoldCaseIdList(List<String> houseHoldCaseIdList) {
		this.houseHoldCaseIdList = houseHoldCaseIdList;
	}

	/**
	 * @return the enrollmentStatusList
	 */
	public List<EnrollmentStatus> getEnrollmentStatusList() {
		return enrollmentStatusList;
	}

	/**
	 * @param enrollmentStatusList the enrollmentStatusList to set
	 */
	public void setEnrollmentStatusList(List<EnrollmentStatus> enrollmentStatusList) {
		this.enrollmentStatusList = enrollmentStatusList;
	}

	/**
	 * @return the coverageYear
	 */
	public String getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	/**
	 * @return the ssapApplicationId
	 */
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	/**
	 * @param ssapApplicationId the ssapApplicationId to set
	 */
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	/**
	 * @return the isPremiumDataRequired
	 */
	public boolean isPremiumDataRequired() {
		return isPremiumDataRequired;
	}

	/**
	 * @param isPremiumDataRequired the isPremiumDataRequired to set
	 */
	public void setPremiumDataRequired(boolean isPremiumDataRequired) {
		this.isPremiumDataRequired = isPremiumDataRequired;
	}

	public List<EnrollmentAttributeEnum> getEnrollmentAttributeList() {
		if (enrollmentAttributeList == null) {
			enrollmentAttributeList = new ArrayList<EnrollmentAttributeEnum>();
        }
		return this.enrollmentAttributeList;
	}
	
	public List<EnrolleeAttributeEnum> getEnrolleeAttributeList() {
		if (enrolleeAttributeList == null) {
			enrolleeAttributeList = new ArrayList<EnrolleeAttributeEnum>();
        }
		return this.enrolleeAttributeList;
	}
	
	public String getLoggedInUserName() {
		return loggedInUserName;
	}
	public void setLoggedInUserName(String loggedInUserName) {
		this.loggedInUserName = loggedInUserName;
	}

	public List<String> getMemberIdList() {
		return memberIdList;
	}

	public void setMemberIdList(List<String> memberIdList) {
		this.memberIdList = memberIdList;
	}

	public List<String> getCoverageYearList() {
		return coverageYearList;
	}

	public void setCoverageYearList(List<String> coverageYearList) {
		this.coverageYearList = coverageYearList;
	}
	
	


	public EnrollmentMailingAddressUpdateRequest getEnrollmentMailingAddressUpdateRequest() {
		return enrollmentMailingAddressUpdateRequest;
	}

	public void setEnrollmentMailingAddressUpdateRequest(
			EnrollmentMailingAddressUpdateRequest enrollmentMailingAddressUpdateRequest) {
		this.enrollmentMailingAddressUpdateRequest = enrollmentMailingAddressUpdateRequest;
	}
	
	
	public boolean isSepDataRequired() {
		return isSepDataRequired;
	}

	public void setSepDataRequired(boolean isSepDataRequired) {
		this.isSepDataRequired = isSepDataRequired;
	}
	
	public Integer getSubscriberEventId() {
		return subscriberEventId;
	}

	public void setSubscriberEventId(Integer subscriberEventId) {
		this.subscriberEventId = subscriberEventId;
	}

	public List<String> getExternalCaseIdList() {
		return externalCaseIdList;
	}

	public void setExternalCaseIdList(List<String> externalCaseIdList) {
		this.externalCaseIdList = externalCaseIdList;
	}

	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public String getChangeEffectiveDate() {
		return changeEffectiveDate;
	}

	public void setChangeEffectiveDate(String changeEffectiveDate) {
		this.changeEffectiveDate = changeEffectiveDate;
	}

	@Override
	public String toString() {
		return "EnrollmentRequest [idList=" + idList + ", mapValue=" + mapValue + ", employerId=" + employerId
				+ ", employeeId=" + employeeId + ", enrollmentId=" + enrollmentId + ", planId=" + planId
				+ ", issuerName=" + issuerName + ", planName=" + planName + ", planLevel=" + planLevel
				+ ", ratingRegion=" + ratingRegion + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", planNumber=" + planNumber + ", employeeAppId=" + employeeAppId + ", planMarket=" + planMarket
				+ ", enrollmentStatus=" + enrollmentStatus + ", role=" + role + ", adminUpdateIndividualRequest="
				+ adminUpdateIndividualRequest + ", employerDisEnrollmentRequest=" + employerDisEnrollmentRequest
				+ ", assisterBrokerId=" + assisterBrokerId + ", enrollmentReinstatementRequest="
				+ enrollmentReinstatementRequest + ", ffeAssignedPolNo=" + ffeAssignedPolNo + ", issuerAssignedPolNo="
				+ issuerAssignedPolNo + ", employeeName=" + employeeName + ", employerEnrollmentStartDate="
				+ employerEnrollmentStartDate + ", employerEnrollmentEndDate=" + employerEnrollmentEndDate
				+ ", employerEnrollmentId=" + employerEnrollmentId + ", enrollmentDisEnrollmentDTO="
				+ enrollmentDisEnrollmentDTO + ", status=" + status + ", aptcAmt=" + aptcAmt + ", aptcEffectiveDate="
				+ aptcEffectiveDate + ", enrollmentAptcUpdateDtoList=" + enrollmentAptcUpdateDtoList
				+ ", enrollmentBrokerUpdateData=" + enrollmentBrokerUpdateData + ", dateClosed=" + dateClosed
				+ ", enrollmentMailingAddressUpdateRequest=" + enrollmentMailingAddressUpdateRequest
				+ ", houseHoldCaseIdList=" + houseHoldCaseIdList + ", enrollmentStatusList=" + enrollmentStatusList
				+ ", coverageYear=" + coverageYear + ", ssapApplicationId=" + ssapApplicationId
				+ ", isPremiumDataRequired=" + isPremiumDataRequired + ", enrollmentAttributeList="
				+ enrollmentAttributeList + ", enrolleeAttributeList=" + enrolleeAttributeList + ", loggedInUserName="
				+ loggedInUserName + ", memberIdList=" + memberIdList + ", coverageYearList=" + coverageYearList
				+ ", subscriberEventId=" + subscriberEventId + ", isSepDataRequired=" + isSepDataRequired
				+ ", externalCaseIdList=" + externalCaseIdList + ", externalMemberId=" + externalMemberId
				+ ", changeEffectiveDate=" + changeEffectiveDate + "]";
	}
}
