package com.getinsured.hix.dto.cap.consumerapp;

import java.math.BigDecimal;
import java.util.Date;

public class PolicyLiteDTO {

	private Integer cmrHouseholdId;
	private Long planId;
	private Long ssapId;
	private Integer enrollmentId;

	private String encCmrHouseholdId;
	private String encSsapId;
	
	private String status;
	private String insuranceType; // Health/Dental/ShortTerm
	private String exchangeType;
	private BigDecimal premium;
	private Date enrollmentDate;
	private Date enrollmentEffectiveDate;
	private Long numberOfApplicants;

	private String planName;
	private String carrierUrl;
	private String carrierName;
	private Float enrollmentPremium;
	private String enrollmentStatus;
	
	private  Date enrollmentCreatedOn ;
	private  Date benefitEffectiveDate;
	

	// private String state;
	// private String zipCode;

	public PolicyLiteDTO(String carrierName, String carrierUrl,
			String planName, java.lang.Long ssapId, String insuranceType,
			java.math.BigDecimal premium, String exchangeType,
			java.lang.Integer cmrHouseholdId, Object status,java.lang.Long numberOfApplicants) {

		this.carrierName = carrierName;
		this.carrierUrl = carrierUrl;
		this.planName = planName;
		this.insuranceType = insuranceType;
		this.ssapId = ssapId;
		this.premium = premium;
		this.exchangeType = exchangeType;
		this.numberOfApplicants = numberOfApplicants;
		this.cmrHouseholdId = cmrHouseholdId;
		this.status = status.toString();
	}
	
	

	public PolicyLiteDTO(String carrierName, String carrierUrl,
			String planName, java.lang.Integer enrollmentId, String insuranceType,
			java.lang.Float premium, String exchangeType,
			java.lang.Integer cmrHouseholdId,
			java.lang.Long numberOfApplicants, String enrollmentStatus, java.util.Date createdOn, java.util.Date benefitEffectiveDate) {

		this.carrierName = carrierName;
		this.carrierUrl = carrierUrl;
		this.planName = planName;
		this.insuranceType = insuranceType;
		this.enrollmentId = enrollmentId;
		this.enrollmentPremium = premium;
		this.exchangeType = exchangeType;
		this.cmrHouseholdId =cmrHouseholdId;
		this.numberOfApplicants = numberOfApplicants;
		this.enrollmentStatus = enrollmentStatus;
		this.enrollmentCreatedOn = createdOn;
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public long getSsapId() {
		return ssapId;
	}

	public void setSsapId(long ssapId) {
		this.ssapId = ssapId;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	public Date getEnrollmentDate() {
		return enrollmentDate;
	}

	public void setEnrollmentDate(Date enrollmentDate) {
		this.enrollmentDate = enrollmentDate;
	}

	public Date getEnrollmentEffectiveDate() {
		return enrollmentEffectiveDate;
	}

	public void setEnrollmentEffectiveDate(Date enrollmentEffectiveDate) {
		this.enrollmentEffectiveDate = enrollmentEffectiveDate;
	}

	public Long getNumberOfApplicants() {
		return numberOfApplicants;
	}

	public void setNumberOfApplicants(Long numberOfApplicants) {
		this.numberOfApplicants = numberOfApplicants;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCarrierUrl() {
		return carrierUrl;
	}

	public void setCarrierUrl(String carrierUrl) {
		this.carrierUrl = carrierUrl;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public Float getEnrollmentPremium() {
		return enrollmentPremium;
	}

	public void setEnrollmentPremium(Float enrollmentPremium) {
		this.enrollmentPremium = enrollmentPremium;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public Date getEnrollmentCreatedOn() {
		return enrollmentCreatedOn;
	}

	public void setEnrollmentCreatedOn(Date enrollmentCreatedOn) {
		this.enrollmentCreatedOn = enrollmentCreatedOn;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public String getEncCmrHouseholdId() {
		return encCmrHouseholdId;
	}
	
	public void setEncCmrHouseholdId(String encCmrHouseholdId) {
		this.encCmrHouseholdId = encCmrHouseholdId;
	}
	
	public String getEncSsapId() {
		return encSsapId;
	}
	
	public void setEncSsapId(String encSsapId) {
		this.encSsapId = encSsapId;
	}
}
