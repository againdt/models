CREATE OR REPLACE
PROCEDURE synchronizePMServiceArea(
    tableName IN VARCHAR2,
    msg OUT VARCHAR2 )
IS
  id PM_ISSUER_SERVICE_AREA.id%type;
  issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type;
  completeState PM_ISSUER_SERVICE_AREA.complete_state%type;
  zipcode ZIPCODES.zipcode%type;
  city ZIPCODES.city%type;
  county ZIPCODES.county%type;
  countyFips ZIPCODES.county_Fips%type;
  stateFips ZIPCODES.state_Fips%type;
  state zipcodes.state%type;
  extId PM_ISSUER_SERVICE_AREA_EXT.ID%type;
  extCounty PM_ISSUER_SERVICE_AREA_EXT.COUNTY%type;
  extPartialCounty PM_ISSUER_SERVICE_AREA_EXT.PARTIAL_COUNTY%type;
  extIssuerServiceAreaId PM_ISSUER_SERVICE_AREA_EXT.ISSUER_SERVICE_AREA_ID%type;
  pmServiceAreaRow PM_SERVICE_AREA%ROWTYPE;
  isNodata                      VARCHAR2(15) := 'no data found';
  stmt_str                      VARCHAR2(400);
  stmt_str2                     VARCHAR2(400);
  stmt_strForExtPartialCountyNo VARCHAR2(400);
  tempNull                      VARCHAR2(5) := NULL;
  tempId                        INTEGER;
  CURSOR issuerServiceAreaData_cur
  IS
    SELECT id, ISSUER_STATE, complete_state FROM PM_ISSUER_SERVICE_AREA;
  CURSOR zipcodesData_cur(issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type)
  IS
    SELECT DISTINCT zipcode,
      city,
      county,
      county_Fips,
      state_Fips
    FROM zipcodes
    WHERE STATE = issuerState;
  CURSOR countyPartialCounty_cur (issuerServiceAreaId PM_ISSUER_SERVICE_AREA.id%type)
  IS
    SELECT DISTINCT ext.county,
      ext.partial_county,
      ext.ISSUER_SERVICE_AREA_ID
    FROM PM_ISSUER_SERVICE_AREA_EXT ext,
      PM_ISSUER_SERVICE_AREA isa
    WHERE ext.ISSUER_SERVICE_AREA_ID=isa.ID
    AND isa.ID                      = issuerServiceAreaId;
  CURSOR zipcodesDataOnCounty_cur(extCounty PM_ISSUER_SERVICE_AREA_EXT.COUNTY%type,issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type)
  IS
    SELECT zipcode,
      county,
      county_fips,
      state_Fips,
      state
    FROM ZIPCODES
    WHERE county = extCounty
    AND state    = issuerState;
  CURSOR oldPmServiceAreaData_cur(extIssuerServiceAreaId PM_ISSUER_SERVICE_AREA_EXT.ISSUER_SERVICE_AREA_ID%type)
  IS
    SELECT * FROM PM_SERVICE_AREA WHERE service_area_id=extIssuerServiceAreaId;
BEGIN
  --createPmServiceAreaNewTable();   -- Not working due to previleges
  stmt_str                      := 'INSERT INTO ' || tableName || '(            
ID,            
SERVICE_AREA_ID,            
ZIP,            
COUNTY,            
FIPS,            
STATE,            
SERVICE_AREA_EXT_ID,            
IS_DELETED          
) VALUES       
(:pKey, :id, :zipcode, :county, :countyFips, :issuerState, :serviceAreaExtId, :isDeleted)';
  stmt_str2                     := 'INSERT INTO ' || tableName || '(            
ID,            
SERVICE_AREA_ID,            
ZIP,            
COUNTY,            
FIPS,            
STATE,            
SERVICE_AREA_EXT_ID,            
IS_DELETED          
) VALUES       
(:pKey, :id, :zipcode, :county, :countyFips, :issuerState, :serviceAreaExtId, :isDeleted)';
  stmt_strForExtPartialCountyNo := 'INSERT INTO ' || tableName || '(            
ID,            
SERVICE_AREA_ID,            
ZIP,            
COUNTY,            
FIPS,            
STATE,            
SERVICE_AREA_EXT_ID,            
IS_DELETED          
) VALUES       
(:pKey, :id, :zipcode, :county, :countyFips, :issuerState, :serviceAreaExtId, :isDeleted)';
  -- open cursor
  --DBMS_OUTPUT.PUT_LINE('begin processing');
  DBMS_OUTPUT.PUT_LINE('Opening Cursor issuerServiceAreaData_cur');
  OPEN issuerServiceAreaData_cur;
  DBMS_OUTPUT.PUT_LINE('Opened Cursor issuerServiceAreaData_cur');
  LOOP
    --Retrieve records from PM_ISSUER_SERVICE_AREA table
    FETCH issuerServiceAreaData_cur
    INTO id,
      issuerState,
      completeState;
    IF issuerServiceAreaData_cur%FOUND THEN
      isNodata := 'data found';
    END IF;
    EXIT
  WHEN issuerServiceAreaData_cur%NOTFOUND;
    IF completeState = 'Y' THEN
      --DBMS_OUTPUT.PUT_LINE('select data from PM_ISSUER_SERVICE_AREA for completeState = ' || completeState);
      --If Complete State Y then bring details of zipcode
      DBMS_OUTPUT.PUT_LINE('Opening Cursor zipcodesData_cur');
      OPEN zipcodesData_cur(issuerState);
      DBMS_OUTPUT.PUT_LINE('Opened Cursor zipcodesData_cur issuerState');
      LOOP
        FETCH zipcodesData_cur INTO zipcode,city,county,countyFips,stateFips;
        EXIT
      WHEN zipcodesData_cur%NOTFOUND;
        SELECT PM_SERVICE_AREA_SEQ.nextval INTO tempId FROM dual;
        EXECUTE IMMEDIATE stmt_str USING tempId,
        id,
        zipcode,
        county,
        stateFips||countyFips,
        issuerState,
        tempNull,
        'N';
      END LOOP;
      CLOSE zipcodesData_cur;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Opening Cursor countyPartialCounty_cur');
      OPEN countyPartialCounty_cur(id);
      DBMS_OUTPUT.PUT_LINE('Opened Cursor countyPartialCounty_cur');
      LOOP
        FETCH countyPartialCounty_cur
        INTO extCounty,
          extPartialCounty,
          extIssuerServiceAreaId;
        EXIT
      WHEN countyPartialCounty_cur%NOTFOUND;
        IF extPartialCounty = 'YES' THEN
          -- DBMS_OUTPUT.PUT_LINE('inserting again to PM_SERVICE_AREA_NEW...for extPartialCounty = Y');
          --when PARTIAL_COUNTY='YES' get old tables(PM_SERVICE_AREA) data to new one (PM_SERVICE_AREA_NEW)
          DBMS_OUTPUT.PUT_LINE('Opening Cursor oldPmServiceAreaData_cur');
          OPEN oldPmServiceAreaData_cur(extIssuerServiceAreaId);
          DBMS_OUTPUT.PUT_LINE('Opened Cursor oldPmServiceAreaData_cur');
          LOOP
            FETCH oldPmServiceAreaData_cur INTO pmServiceAreaRow;
            EXIT
          WHEN oldPmServiceAreaData_cur%NOTFOUND;
            SELECT PM_SERVICE_AREA_SEQ.nextval INTO tempId FROM dual;
            EXECUTE IMMEDIATE stmt_str2 USING tempId,
            pmServiceAreaRow.SERVICE_AREA_ID,
            pmServiceAreaRow.ZIP,
            pmServiceAreaRow.county,
            pmServiceAreaRow.FIPS,
            pmServiceAreaRow.STATE,
            pmServiceAreaRow.SERVICE_AREA_EXT_ID,
            pmServiceAreaRow.IS_DELETED;
          END LOOP;
          CLOSE oldPmServiceAreaData_cur;
          --DBMS_OUTPUT.PUT_LINE('inserted again to PM_SERVICE_AREA_NEW....for extPartialCounty = Y');
        ELSE
          IF extPartialCounty = 'NO' THEN --should we only match for NO or N is also needed
            --DBMS_OUTPUT.PUT_LINE('extPartialCounty = NO');
            --For PARTIAL_COUNTY='NO' get Zipcode details and insert to PM_SERVICE_AREA_NEW
            OPEN zipcodesDataOnCounty_cur(extCounty,issuerState);
            LOOP
              FETCH zipcodesDataOnCounty_cur INTO zipcode,county,countyFips,stateFips,state;
              EXIT
            WHEN zipcodesDataOnCounty_cur%NOTFOUND;
              SELECT PM_SERVICE_AREA_SEQ.nextval INTO tempId FROM dual;
              EXECUTE IMMEDIATE stmt_strForExtPartialCountyNo USING tempId,
              extIssuerServiceAreaId,
              zipcode,
              county,
              stateFips||countyFips,
              state,
              extId,
              'N';
            END LOOP;
            CLOSE zipcodesDataOnCounty_cur;
            --DBMS_OUTPUT.PUT_LINE('inserted again to PM_SERVICE_AREA_NEW....for extPartialCounty = NO');
          END IF;
        END IF;
      END LOOP;
      CLOSE countyPartialCounty_cur;
    END IF;
  END LOOP;
  CLOSE issuerServiceAreaData_cur;
  --rename the tables
  --renamePmServiceAreaNewTable();
  IF isNodata = 'no data found' THEN
    msg      := 'no data found in PM_ISSUER_SERVICE_AREA';
    DBMS_OUTPUT.PUT_LINE('no data found in PM_ISSUER_SERVICE_AREA');
  ELSE
    msg := 'Successfully Executed';
    --createDropSynonym(synonymExisting,tNmForSynonym,SCHEMA_OWNER,SCHEMA_USER);
  END IF;
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  IF issuerServiceAreaData_cur%ISOPEN THEN
    CLOSE issuerServiceAreaData_cur;
  END IF;
  IF zipcodesData_cur%ISOPEN THEN
    CLOSE zipcodesData_cur;
  END IF;
  IF countyPartialCounty_cur%ISOPEN THEN
    CLOSE countyPartialCounty_cur;
  END IF;
  IF zipcodesDataOnCounty_cur%ISOPEN THEN
    CLOSE zipcodesDataOnCounty_cur;
  END IF;
  IF oldPmServiceAreaData_cur%ISOPEN THEN
    CLOSE oldPmServiceAreaData_cur;
  END IF ;
  DBMS_OUTPUT.PUT_LINE('Exception Occured!!!!');
  msg := DBMS_UTILITY.FORMAT_ERROR_STACK;
  --dbms_output.put_line( dbms_utility.format_error_backtrace );
  DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
END;