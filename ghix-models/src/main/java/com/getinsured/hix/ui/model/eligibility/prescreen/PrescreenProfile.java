package com.getinsured.hix.ui.model.eligibility.prescreen;

/**
 * This class encapsulates the information on the profile page of the prescreen
 * application UI.
 * <p>
 * claimerName is the name of the claimer that is entered.
 * </p>
 * <p>
 * zipCode - corresponds to the zip code entered by the user.
 * </p>
 * <p>
 * isMarried - corresponds to the Married? check box on the UI. Default is
 * false.
 * </p>
 * <p>
 * numberOfDependents - corresponds to the number entered for the dependents.
 * </p>
 * <p>
 * isIncomePageLatest - this boolean will give information to reset the values
 * on the income page. When user makes any changes to income on the income page,
 * this boolean is set to true. If the user comes back to the profile page from
 * the income page and makes any modification to the household income value on
 * the profile page, this boolean is set to false, which should reset the income
 * popups on the income page. Default value is false.
 * </p>
 * 
 * @author Sunil Desu
 * @since April 16 2013
 */
public class PrescreenProfile {

	private String claimerName;
	private String zipCode;
	private boolean isMarried;
	private int numberOfDependents;
	private double taxHouseholdIncome;
	private boolean isIncomePageLatest;
	private String stateCode;
	private String county;
	
	public String getClaimerName() {
		return claimerName;
	}

	public void setClaimerName(String claimerName) {
		this.claimerName = claimerName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public boolean getIsMarried() {
		return isMarried;
	}

	public void setMarried(boolean isMarried) {
		this.isMarried = isMarried;
	}

	public int getNumberOfDependents() {
		return numberOfDependents;
	}

	public void setNumberOfDependents(int numberOfDependents) {
		this.numberOfDependents = numberOfDependents;
	}

	public double getTaxHouseholdIncome() {
		return taxHouseholdIncome;
	}

	public void setTaxHouseholdIncome(double taxHouseholdIncome) {
		this.taxHouseholdIncome = taxHouseholdIncome;
	}

	public boolean getIsIncomePageLatest() {
		return isIncomePageLatest;
	}

	public void setIncomePageLatest(boolean isIncomePageLatest) {
		this.isIncomePageLatest = isIncomePageLatest;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrescreenProfile [claimerName=");
		builder.append(claimerName);
		builder.append(", zipCode=");
		builder.append(zipCode);
		builder.append(", isMarried=");
		builder.append(isMarried);
		builder.append(", numberOfDependents=");
		builder.append(numberOfDependents);
		builder.append(", taxHouseholdIncome=");
		builder.append(taxHouseholdIncome);
		builder.append(", isIncomePageLatest=");
		builder.append(isIncomePageLatest);
		builder.append(", stateCode=");
		builder.append(stateCode);
		builder.append(", county=");
		builder.append(county);
		builder.append("]");
		return builder.toString();
	}

}
