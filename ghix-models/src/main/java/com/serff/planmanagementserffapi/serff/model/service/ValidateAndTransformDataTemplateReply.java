
package com.serff.planmanagementserffapi.serff.model.service;



public class ValidateAndTransformDataTemplateReply {

    protected String requestId;
    protected DataTemplateOutcomes dataTemplateOutcomes;

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the dataTemplateOutcomes property.
     * 
     * @return
     *     possible object is
     *     {@link DataTemplateOutcomes }
     *     
     */
    public DataTemplateOutcomes getDataTemplateOutcomes() {
        return dataTemplateOutcomes;
    }

    /**
     * Sets the value of the dataTemplateOutcomes property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataTemplateOutcomes }
     *     
     */
    public void setDataTemplateOutcomes(DataTemplateOutcomes value) {
        this.dataTemplateOutcomes = value;
    }

}
