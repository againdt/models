package com.getinsured.hix.model.enrollment;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * 
 * @author meher_a
 *
 */
@Audited
@Entity
@Table(name="ENRL_GROUP_PLAN")
public class GroupPlan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUP_PLAN_SEQ")
	@SequenceGenerator(name = "GROUP_PLAN_SEQ", sequenceName = "GROUP_PLAN_SEQ", allocationSize = 1)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "GROUP_ID")
	private GroupInstallation groupInstallation;
	
	@Column(name="PLAN_ID")
	private Integer planId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="UPDATED_ON",nullable=false)
	private Date updatedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public GroupInstallation getGroupInstallation() {
		return groupInstallation;
	}

	public void setGroupInstallation(GroupInstallation groupInstallation) {
		this.groupInstallation = groupInstallation;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}
}
