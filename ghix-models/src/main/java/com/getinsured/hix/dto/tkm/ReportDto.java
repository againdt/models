package com.getinsured.hix.dto.tkm;

import java.io.Serializable;
import java.util.List;

public class ReportDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private List<Integer> data;

	public enum CATEGORIES {
		LESSTHAN_ONE_DAY_OLD("Less than 1 Day Old"), 
		ONE_TO_THREE_DAYS_OLD("1-3 Days Old"), 
		THREE_TO_SEVEN_DAYS_OLD("3-7 Days Old"), 
		ONE_WEEK_TO_THREE_WEEKS_OLD("1 Week - 3 Weeks Old"), 
		MORETHAN_THREE_WEEKS("More Than 3 Weeks");

		private final String catogories;

		public String getCategories() {
			return catogories;
		}

		private CATEGORIES(String cd) {
			catogories = cd;
		}
		
		 @Override 
	     public String toString(){ 
	            return catogories; 
	     } 
		 
		 public static CATEGORIES getValue(String str) {
		        if (LESSTHAN_ONE_DAY_OLD.toString().equalsIgnoreCase(str)) {
		            return LESSTHAN_ONE_DAY_OLD;
		        } else if (ONE_TO_THREE_DAYS_OLD.toString().equalsIgnoreCase(str)) {
		            return ONE_TO_THREE_DAYS_OLD;
		        } else if (THREE_TO_SEVEN_DAYS_OLD.toString().equalsIgnoreCase(str)){
		            return THREE_TO_SEVEN_DAYS_OLD;
		        } else if (ONE_WEEK_TO_THREE_WEEKS_OLD.toString().equalsIgnoreCase(str)){
		            return ONE_WEEK_TO_THREE_WEEKS_OLD;
		        } else if (MORETHAN_THREE_WEEKS.toString().equalsIgnoreCase(str)){
		            return MORETHAN_THREE_WEEKS;
		        }
		        return null;
		    }
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getData() {
		return data;
	}

	public void setData(List<Integer> data) {
		this.data = data;
	}
	
	
}
