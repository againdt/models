//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.02.25 at 01:51:01 PM IST 
//


package com.serff.template.plan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HsaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HsaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="hsaEligibility" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="employerHSAHRAContributionIndicator" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO" minOccurs="0"/>
 *         &lt;element name="empContributionAmountForHSAOrHRA" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HsaVO", propOrder = {

})
public class HsaVO {

    @XmlElement(required = true)
    protected ExcelCellVO hsaEligibility;
    protected ExcelCellVO employerHSAHRAContributionIndicator;
    @XmlElement(required = true)
    protected ExcelCellVO empContributionAmountForHSAOrHRA;

    /**
     * Gets the value of the hsaEligibility property.
     * 
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *     
     */
    public ExcelCellVO getHsaEligibility() {
        return hsaEligibility;
    }

    /**
     * Sets the value of the hsaEligibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *     
     */
    public void setHsaEligibility(ExcelCellVO value) {
        this.hsaEligibility = value;
    }

    /**
     * Gets the value of the employerHSAHRAContributionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *     
     */
    public ExcelCellVO getEmployerHSAHRAContributionIndicator() {
        return employerHSAHRAContributionIndicator;
    }

    /**
     * Sets the value of the employerHSAHRAContributionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *     
     */
    public void setEmployerHSAHRAContributionIndicator(ExcelCellVO value) {
        this.employerHSAHRAContributionIndicator = value;
    }

    /**
     * Gets the value of the empContributionAmountForHSAOrHRA property.
     * 
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *     
     */
    public ExcelCellVO getEmpContributionAmountForHSAOrHRA() {
        return empContributionAmountForHSAOrHRA;
    }

    /**
     * Sets the value of the empContributionAmountForHSAOrHRA property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *     
     */
    public void setEmpContributionAmountForHSAOrHRA(ExcelCellVO value) {
        this.empContributionAmountForHSAOrHRA = value;
    }

}
