CREATE OR REPLACE
FUNCTION UPDATE_RATINGAREA_EFFDATE(effectiveYear VARCHAR)
RETURNS VOID AS  
$$ 
DECLARE
  enroll record;
BEGIN 
  FOR enroll IN (SELECT * FROM ENROLLEE e WHERE e.PERSON_TYPE_LKP IN (SELECT LV.LOOKUP_VALUE_ID FROM LOOKUP_VALUE LV, LOOKUP_TYPE LT  WHERE LT.LOOKUP_TYPE_ID=LV.LOOKUP_TYPE_ID AND LV.LOOKUP_VALUE_CODE IN('SUBSCRIBER', 'ENROLLEE') AND LT.NAME='PERSON_TYPE')
                                            AND TO_CHAR(e.EFFECTIVE_START_DATE, 'YYYY')=effectiveYear
                                            AND e.ENROLLEE_STATUS_LKP NOT IN (SELECT LV.LOOKUP_VALUE_ID FROM LOOKUP_VALUE LV, LOOKUP_TYPE LT WHERE LT.LOOKUP_TYPE_ID=LV.LOOKUP_TYPE_ID AND LV.LOOKUP_VALUE_CODE IN('ABORTED') AND LT.NAME='ENROLLMENT_STATUS')
                                      
  ) LOOP
  DECLARE
   oldRatingAreaEffDate     ENROLLEE.RATING_AREA_EFF_DATE%type;
   oldRatingArea            ENROLLEE.RATING_AREA%type;
   oldTotIndivRespAmt       ENROLLEE.TOTAL_INDV_RESPONSIBILITY_AMT%type;
   enrollAud record; 
  BEGIN
   FOR enrollAud IN (SELECT * FROM ENROLLEE_AUD ea WHERE ea.ID=enroll.ID ORDER BY ea.LAST_UPDATE_TIMESTAMP ASC) LOOP
    BEGIN
     IF enrollAud.REVTYPE=0 THEN
      oldRatingAreaEffDate    :=   enrollAud.EFFECTIVE_START_DATE;
      oldRatingArea           :=    enrollAud.RATING_AREA;
      
      UPDATE ENROLLEE_AUD eaa SET eaa.HEALTH_COV_PREM_EFF_DATE=eaa.EFFECTIVE_START_DATE, eaa.RATING_AREA_EFF_DATE=eaa.EFFECTIVE_START_DATE WHERE eaa.ID=enrollAud.ID and eaa.REV=enrollAud.REV;
     END IF;
     
      IF enrollAud.REVTYPE<>0 THEN
       IF enrollAud.RATING_AREA<>oldRatingArea THEN
        /*select netpremium effective date for new rating area effective date*/
        SELECT ena.TOT_INDV_RESP_EFF_DATE INTO oldRatingAreaEffDate FROM ENROLLMENT_AUD ena WHERE ena.ID=enrollAud.ENROLLMENT_ID and ena.REV=enrollAud.REV;
         IF oldRatingAreaEffDate IS NULL THEN
          SELECT MAX(ena.TOT_INDV_RESP_EFF_DATE) INTO oldRatingAreaEffDate FROM ENROLLMENT_AUD ena WHERE ena.ID=enrollAud.ENROLLMENT_ID and ena.LAST_UPDATE_TIMESTAMP= (SELECT MAX(LAST_UPDATE_TIMESTAMP) FROM ENROLLMENT_AUD WHERE LAST_UPDATE_TIMESTAMP<=(enrollAud.LAST_UPDATE_TIMESTAMP +interval '2' second));
        END IF;
       END IF;
       
      UPDATE ENROLLEE_AUD eaa SET eaa.RATING_AREA_EFF_DATE=oldRatingAreaEffDate WHERE eaa.ID=enrollAud.ID and eaa.REV=enrollAud.REV;
      oldRatingArea:= enrollAud.RATING_AREA;
      
     END IF;
    END;
   END LOOP;
    UPDATE ENROLLEE e SET  e.RATING_AREA_EFF_DATE=oldRatingAreaEffDate where e.ID=enroll.ID;
  END;
  END LOOP;
END;
$$ LANGUAGE plpgsql;