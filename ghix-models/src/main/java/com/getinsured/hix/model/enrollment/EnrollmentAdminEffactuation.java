package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name = "ENRL_ADMIN_EFFACTUATION")
public class EnrollmentAdminEffactuation implements Serializable {
	private static final long serialVersionUID = 1L;
	public enum PROCESSING_STATUS{
		NOT_PROCESSED, SUCCESS, FAILURE;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_ADMIN_EFFACTUATION_SEQ")
	@SequenceGenerator(name = "ENRL_ADMIN_EFFACTUATION_SEQ", sequenceName = "ENRL_ADMIN_EFFACTUATION_SEQ", allocationSize = 1)
	private Integer id;

	
	@Column(name = "HIOS_ISSUER_ID")
	private String hiosIssuerId;
	
	@Column(name = "EXCHG_ASSIGNED_POLICY_ID")
	private String exchgAssignedPolicyId;
	
	@Column(name = "EXCHG_ASSIGNED_MEMBER_ID")
	private String exchgAssignedMemberId;
	
	@Column(name = "SUBSCRIBER_FLAG")
	private String subscriberFlag;
	
	@Column(name = "ISSUER_ASSIGNED_POLICY_ID")
	private String issuerAssignedPolicyId;
	
	@Column(name = "ISSUER_ASSIGNED_INDIVIDUAL_ID")
	private String issuerAssignedIndId;
	
	@Column(name = "LAST_PREMIUM_PAID_DATE")
	private String lastPremiumPaidDate;
	
	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "SKIP_REASON")
	private String skipReason;
	
	@Column(name = "MEMBER_STATUS")
	private String memberStatus;
	
	@Column(name = "COVERAGE_END_DATE")
	private String coverageEndDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getExchgAssignedPolicyId() {
		return exchgAssignedPolicyId;
	}

	public void setExchgAssignedPolicyId(String exchgAssignedPolicyId) {
		this.exchgAssignedPolicyId = exchgAssignedPolicyId;
	}

	public String getExchgAssignedMemberId() {
		return exchgAssignedMemberId;
	}

	public void setExchgAssignedMemberId(String exchgAssignedMemberId) {
		this.exchgAssignedMemberId = exchgAssignedMemberId;
	}

	public String getSubscriberFlag() {
		return subscriberFlag;
	}

	public void setSubscriberFlag(String subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}

	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}

	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}

	public String getIssuerAssignedIndId() {
		return issuerAssignedIndId;
	}

	public void setIssuerAssignedIndId(String issuerAssignedIndId) {
		this.issuerAssignedIndId = issuerAssignedIndId;
	}

	public String getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}

	public void setLastPremiumPaidDate(String lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSkipReason() {
		return skipReason;
	}

	public void setSkipReason(String skipReason) {
		this.skipReason = skipReason;
	}

	public String getMemberStatus() {
		return memberStatus;
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

}
