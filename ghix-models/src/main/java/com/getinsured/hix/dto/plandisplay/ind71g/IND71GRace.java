package com.getinsured.hix.dto.plandisplay.ind71g;

public class IND71GRace {

    private String raceEthnicityCode;
    private String description;

    public String getRaceEthnicityCode() {
        return raceEthnicityCode;
    }

    public void setRaceEthnicityCode(String raceEthnicityCode) {
        this.raceEthnicityCode = raceEthnicityCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
