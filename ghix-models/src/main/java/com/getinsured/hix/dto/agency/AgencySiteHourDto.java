package com.getinsured.hix.dto.agency;

import org.hibernate.validator.constraints.NotBlank;

public class AgencySiteHourDto {
	
	private String id;
	@NotBlank
	private String day;
	@NotBlank
	private String fromTime;
	@NotBlank
	private String toTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	
	

}
