package com.getinsured.hix.model.estimator.mini;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Map;

public class CensusLeadResponse {
    private long startTime;
	
	private long execDuration;
	
	private Map<Integer,String> errors;
	
	private Map<Integer,String> warnings;
	
	private Map<String,String> info;
	
	private long leadId;

	private String status;
	
	public long getLeadId() {
		return leadId;
	}

	public Map<String, String> getInfo() {
		if(info == null){
			info = new HashMap<String, String>();
		}
		return info;
	}

	public void setInfo(Map<String, String> info) {
		this.info = info;
	}

	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}

	public void startResponse() {
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		execDuration = endTime - startTime;
	}
	
	public long getExecDuration() {
		return execDuration;
	}

	public void setExecDuration(long execDuration) {
		this.execDuration = execDuration;
	}

	public Map<Integer, String> getErrors() {
		
		if(errors == null){
			errors = new HashMap<Integer, String>();
		}
		
		return errors;
	}

	public void setErrors(Map<Integer, String> errors) {
		this.errors = errors;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<Integer, String> getWarnings() {
		if(warnings == null){
			warnings = new HashMap<Integer, String>();
		}
		return warnings;
	}

	public void setWarnings(Map<Integer, String> warnings) {
		this.warnings = warnings;
	}

}
