package com.getinsured.eligibility.referral.ui.dto;

/**
 * 
 * @author raguram_p
 *
 */
public class LceEventDTO {
	
	private String eventName;
	private String eventDate;
	private boolean selected;
	private int eventId;
	
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	@Override
	public String toString() {
		return "LceEventDTO [eventName=" + eventName + ", eventDate="
				+ eventDate + ", selected=" + selected + ", eventId=" + eventId
				+ "]";
	}
	
	
	
}
