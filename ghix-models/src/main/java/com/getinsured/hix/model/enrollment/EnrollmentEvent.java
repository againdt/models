/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;

/**
 * @author panda_p
 *
 */
@Audited
@XmlRootElement(name="enrollmentEvents")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name="ENROLLMENT_EVENT")
public class EnrollmentEvent implements Serializable, Comparable<EnrollmentEvent> {
	
	public enum TRANSACTION_IDENTIFIER{
		  APTC_CH, ADMIN_UPDATE,CARRIER_UPDATE,EDIT_TOOL,ADMIN_UPDATE_MAILING_ADDRESS, APTC_CH_HLT_DISENROLL, AUTO_DISENROLL_BATCH,
		  REINSTATEMENT, CANCEL_PENDING_BATCH, EMPLOYER_DISENROLLMENT, ASYNC_TERMINATOR, APTC_CH_HLT_CARRIER_DISENROLL, APTC_CH_HLT_EDITTOOL_DISENROLL,
		  SPECIAL_DISENROLLMENT, AUTOMATE_SPECIAL_DISENROLL, INITIAL_ENROLLMENT, RENEWAL_ENROLLMENT, SPECIAL_ENROLLMENT, RENEWAL_DISENROLLMENT,
		  SPECIAL_ADD, SPECIAL_UPDATE, AUTOMATE_SPECIAL_ADD, AUTOMATE_SPECIAL, CARRIER_INBOUND_DISENROLL, CARRIER_INBOUND_CONFIRM, AUTOMATE_SPECIAL_UPDATE,
		  ADMIN_EFFECTUATION_JOB, BROKER_DESIGNATE, BROKER_DEDESIGNATE, RECON_AUTOFIX_JOB, APPLICATION_DISENROLL, EMPLOYEE_DISENROLL, ENROLLMENT_DISENROLL, 
		  ENROLLMENT_EFFECTUATION, POPULATE_PREMIUM_BATCH_JOB, MRC29_QUOTING_DATE_UPDATE, EDIT_TOOL_QUOTING_DATE_UPDATE, ENRL_STATUS_CH_QUOTING_DATE_UPDATE ;
	}
	
	@XmlElement(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EnrollmentEvent_Seq")
	@SequenceGenerator(name = "EnrollmentEvent_Seq", sequenceName = "ENROLLMENT_EVENT_SEQ", allocationSize = 1)
	private Integer id;
	
	@XmlElement(name="eventTypeLkp")
	@OneToOne
    @JoinColumn(name="EVENT_TYPE_LKP")
	private LookupValue eventTypeLkp;
	

	@XmlElement(name="eventReasonLkp")
	@OneToOne
    @JoinColumn(name="EVENT_REASON_LKP")
	private LookupValue eventReasonLkp;

	@XmlElement(name="spclEnrollmentReasonLkp")
	@OneToOne
    @JoinColumn(name="SPCL_ENROLLMENT_REASON_LKP")
	private LookupValue spclEnrollmentReasonLkp;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ENROLLEE_ID")
	private Enrollee enrollee;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ENROLLMENT_ID")
	private Enrollment enrollment;
	
	
	@XmlElement(name="createdOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@XmlElement(name="updatedOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;

//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Audited
	@OneToOne
    @JoinColumn(name="created_by", nullable= true)
	private AccountUser createdBy;

//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Audited
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY", nullable= true)
    private AccountUser updatedBy;
	
	@Column(name="SEND_TO_CARRIER_FLAG")
	private String sendToCarrierFlag;
	
	@Column(name="EXTRACTION_STATUS")
	private String extractionStatus;
	
	@Column(name="SEND_RENEWAL_TAG")
	private String sendRenewalTag;
	
	@Column(name="TXN_IDENTIFIER")
	private String txnIdentifier;
	
	@XmlElement(name="qleIdentifier")
	@Column(name="QLE_IDENTIFIER")
	private String qleIdentifier;
	
	@Transient
	private String formatUpdatedOn;

	@Transient
	private String formatCreatedOn;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LookupValue getEventTypeLkp() {
		return eventTypeLkp;
	}

	public void setEventTypeLkp(LookupValue eventTypeLkp) {
		this.eventTypeLkp = eventTypeLkp;
	}

	public LookupValue getEventReasonLkp() {
		return eventReasonLkp;
	}

	public void setEventReasonLkp(LookupValue eventReasonLkp) {
		this.eventReasonLkp = eventReasonLkp;
	}
	

	public LookupValue getSpclEnrollmentReasonLkp() {
		return spclEnrollmentReasonLkp;
	}

	public void setSpclEnrollmentReasonLkp(LookupValue spclEnrollmentReasonLkp) {
		this.spclEnrollmentReasonLkp = spclEnrollmentReasonLkp;
	}

	public Enrollee getEnrollee() {
		return enrollee;
	}

	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}
	public String getFormatUpdatedOn() {
		return getFormatedDate(this.updatedOn);
	}

	public void setFormatUpdatedOn(String formatUpdatedOn) {
		this.formatUpdatedOn = formatUpdatedOn;
	}

	public String getFormatCreatedOn() {
		return getFormatedDate(this.createdOn);
	}

	public void setFormatCreatedOn(String formatCreatedOn) {
		this.formatCreatedOn = formatCreatedOn;
	}
	
	public String getFormatedDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmdd");
		String updatedOn = formatter.format(date);
		return updatedOn;

	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public int compareTo(EnrollmentEvent event) {
		if(event != null){
			return this.createdOn.compareTo(event.getCreatedOn());
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * @return the sendToCarrierFlag
	 */
	public String getSendToCarrierFlag() {
		return sendToCarrierFlag;
	}

	/**
	 * @param sendToCarrierFlag the sendToCarrierFlag to set
	 */
	public void setSendToCarrierFlag(String sendToCarrierFlag) {
		this.sendToCarrierFlag = sendToCarrierFlag;
	}
	
	public String getExtractionStatus() {
		return extractionStatus;
	}

	public void setExtractionStatus(String extractionStatus) {
		this.extractionStatus = extractionStatus;
	}

	public String getSendRenewalTag() {
		return sendRenewalTag;
	}

	public void setSendRenewalTag(String sendRenewalTag) {
		this.sendRenewalTag = sendRenewalTag;
	}

	public String getTxnIdentifier() {
		return txnIdentifier;
	}

	public void setTxnIdentifier(String txnIdentifier) {
		this.txnIdentifier = txnIdentifier;
	}

	public String getQleIdentifier() {
		return qleIdentifier;
	}

	public void setQleIdentifier(String qleIdentifier) {
		this.qleIdentifier = qleIdentifier;
	}
}
