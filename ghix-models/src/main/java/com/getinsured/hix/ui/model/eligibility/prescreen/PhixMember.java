package com.getinsured.hix.ui.model.eligibility.prescreen;

import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * This class encapsulates the household member information related to the
 * home page of eligibility section of the private exchange.
 * 
 * 
 * <li>dateOfBirth - Date of birth of the member in the mm/dd/yyyy format.</li>
 * 
 * <li>isTobaccoUser - Default is N</li>
 * 
 * <li>isSeekingCoverage - Default is N</li>
 * 
 * </ol>
 * </p>
 * 
 * @author Nikhil Talreja
 * @since August 20, 2013
 * 
 */
public class PhixMember {
	
	private String dateOfBirth;
	
	private String isTobaccoUser="N";
	
	private String isSeekingCoverage="Y";
	
	private String isNativeAmerican = "N";
	
	private MemberRelationships relationshipToPrimary;
	
	private String memberEligibility = "NA";
	
	private String medicaidChild = "N";
	
	private String isPregnant = "N";
	
	private String gender;
	
	private String externalMemberId; 
	
	

	public PhixMember(){
		
	}
	
	public PhixMember(String dateOfBirth, String isTobaccoUser, String isSeekingCoverage){
		this.dateOfBirth = dateOfBirth;
		this.isTobaccoUser = isTobaccoUser;
		this.isSeekingCoverage = isSeekingCoverage;
	}
	
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getIsTobaccoUser() {
		return isTobaccoUser;
	}

	public void setIsTobaccoUser(String isTobaccoUser) {
		this.isTobaccoUser = isTobaccoUser;
	}

	public String getIsSeekingCoverage() {
		return isSeekingCoverage;
	}
	public String getIsNativeAmerican() {
		return isNativeAmerican;
	}

	public void setIsNativeAmerican(String isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}

	public void setIsSeekingCoverage(String isSeekingCoverage) {
		this.isSeekingCoverage = isSeekingCoverage;
	}

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

	
	public String getMemberEligibility() {
		return memberEligibility;
	}

	public void setMemberEligibility(String memberEligibility) {
		this.memberEligibility = memberEligibility;
	}

	public String getMedicaidChild() {
		return medicaidChild;
	}

	public void setMedicaidChild(String medicaidChild) {
		this.medicaidChild = medicaidChild;
	}

	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}
	
	public String getIsPregnant() {
		return isPregnant;
	}

	public void setIsPregnant(String isPregnant) {
		this.isPregnant = isPregnant;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PhixMember [dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", isTobaccoUser=");
		builder.append(isTobaccoUser);
		builder.append(", isSeekingCoverage=");
		builder.append(isSeekingCoverage);
		builder.append(", relationshipToPrimary=").append(relationshipToPrimary);
		builder.append(", gender=").append(gender);
		builder.append(", memberEligibility=").append(memberEligibility);
		builder.append("]");
		return builder.toString();
	}
	
}
