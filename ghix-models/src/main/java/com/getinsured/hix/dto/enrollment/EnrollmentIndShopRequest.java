package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;

public class EnrollmentIndShopRequest extends IndividualInformationRequest implements Serializable {

	private String planId;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}
	
	
}
