package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

@Audited
@Entity
@Table(name = "ENRL_CMS_OUT")
public class EnrollmentCmsOut implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static enum actionEnum{SENT, SUCCESS, HOLD, REGEN};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_OUT_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_OUT_SEQ", sequenceName = "ENRL_CMS_OUT_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name = "MONTH")
	private Integer month;

	@Column(name = "YEAR")
	private Integer year;

	@Column(name = "COVERAGE_YEAR")
	private Integer coverageYear;

	@Column(name = "OUTBOUND_FILE_NAME")
	private String fileName;

	@Column(name = "OUTBOUND_FILE_SIZE")
	private Long fileSize;

	@Column(name = "HIOS_ISSUER_ID")
	private String hiosIssuerId;

	@Column(name = "FILE_ID")
	private String fileId;

	@Column(name = "ISSUER_FILE_SET_ID")
	private Long issuerFileSetId;

	@Column(name = "OUT_ENROLLMENT_COUNT")
	private Integer outEnrollmentCount;
	
	@Column(name = "ELIGIBLE_ENROLLMENT_COUNT")
	private Integer eligibleEnrollmentCount;

	@NotAudited
	@Column(name = "OUT_ENROLLMENT_IDS")
	private String outEnrollmentIds;

	@NotAudited
	@Column(name = "SKIP_RECORDS")
	private String skipRecords;

	@Column(name = "BATCH_EXECUTION_ID")
	private Long batchExecutionId;

	@Column(name = "INBOUND_STATUS")
	private String inboundStatus;

	@Column(name = "INBOUND_ACTION")
	private String inboundAction;

	@Column(name = "SBMS_SBMR_RCVD")
	private String sbmsSbmrRcvd;

	@Column(name = "ACCEPTED_COUNT")
	private Integer acceptedCount;

	@Column(name = "ACCEPTED_ERROR_WARN_COUNT")
	private Integer acceptedErrorWarnCount;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	@NotAudited
	@OneToMany(mappedBy="enrlCmsOut", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentCmsOutSkippedPolicy> enrollmentCmsOutSkippedPolicies;
	
	
	public List<EnrollmentCmsOutSkippedPolicy> getEnrollmentCmsOutSkippedPolicies() {
		return enrollmentCmsOutSkippedPolicies;
	}

	public void setEnrollmentCmsOutSkippedPolicies(List<EnrollmentCmsOutSkippedPolicy> enrollmentCmsOutSkippedPolicies) {
		this.enrollmentCmsOutSkippedPolicies = enrollmentCmsOutSkippedPolicies;
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the coverageYear
	 */
	public Integer getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear
	 *            the coverageYear to set
	 */
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the fileSize
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize
	 *            the fileSize to set
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	/**
	 * @param hiosIssuerId
	 *            the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId
	 *            the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * @return the issuerFileSetId
	 */
	public Long getIssuerFileSetId() {
		return issuerFileSetId;
	}

	/**
	 * @param issuerFileSetId
	 *            the issuerFileSetId to set
	 */
	public void setIssuerFileSetId(Long issuerFileSetId) {
		this.issuerFileSetId = issuerFileSetId;
	}

	/**
	 * @return the outEnrollmentCount
	 */
	public Integer getOutEnrollmentCount() {
		return outEnrollmentCount;
	}

	/**
	 * @param outEnrollmentCount
	 *            the outEnrollmentCount to set
	 */
	public void setOutEnrollmentCount(Integer outEnrollmentCount) {
		this.outEnrollmentCount = outEnrollmentCount;
	}

	public Integer getEligibleEnrollmentCount() {
		return eligibleEnrollmentCount;
	}

	public void setEligibleEnrollmentCount(Integer eligibleEnrollmentCount) {
		this.eligibleEnrollmentCount = eligibleEnrollmentCount;
	}

	/**
	 * @return the outEnrollmentIds
	 */
	public String getOutEnrollmentIds() {
		return outEnrollmentIds;
	}

	/**
	 * @param outEnrollmentIds
	 *            the outEnrollmentIds to set
	 */
	public void setOutEnrollmentIds(String outEnrollmentIds) {
		this.outEnrollmentIds = outEnrollmentIds;
	}

	/**
	 * @return the skipRecords
	 */
	public String getSkipRecords() {
		return skipRecords;
	}

	/**
	 * @param skipRecords
	 *            the skipRecords to set
	 */
	public void setSkipRecords(String skipRecords) {
		this.skipRecords = skipRecords;
	}

	/**
	 * @return the batchExecutionId
	 */
	public Long getBatchExecutionId() {
		return batchExecutionId;
	}

	/**
	 * @param batchExecutionId
	 *            the batchExecutionId to set
	 */
	public void setBatchExecutionId(Long batchExecutionId) {
		this.batchExecutionId = batchExecutionId;
	}

	/**
	 * @return the inboundStatus
	 */
	public String getInboundStatus() {
		return inboundStatus;
	}

	/**
	 * @param inboundStatus
	 *            the inboundStatus to set
	 */
	public void setInboundStatus(String inboundStatus) {
		this.inboundStatus = inboundStatus;
	}

	/**
	 * @return the inboundAction
	 */
	public String getInboundAction() {
		return inboundAction;
	}

	/**
	 * @param inboundAction
	 *            the inboundAction to set
	 */
	public void setInboundAction(String inboundAction) {
		this.inboundAction = inboundAction;
	}

	/**
	 * @return the sbmsSbmrRcvd
	 */
	public String getSbmsSbmrRcvd() {
		return sbmsSbmrRcvd;
	}

	/**
	 * @param sbmsSbmrRcvd
	 *            the sbmsSbmrRcvd to set
	 */
	public void setSbmsSbmrRcvd(String sbmsSbmrRcvd) {
		this.sbmsSbmrRcvd = sbmsSbmrRcvd;
	}

	/**
	 * @return the acceptedCount
	 */
	public Integer getAcceptedCount() {
		return acceptedCount;
	}

	/**
	 * @param acceptedCount
	 *            the acceptedCount to set
	 */
	public void setAcceptedCount(Integer acceptedCount) {
		this.acceptedCount = acceptedCount;
	}

	/**
	 * @return the acceptedErrorWarnCount
	 */
	public Integer getAcceptedErrorWarnCount() {
		return acceptedErrorWarnCount;
	}

	/**
	 * @param acceptedErrorWarnCount
	 *            the acceptedErrorWarnCount to set
	 */
	public void setAcceptedErrorWarnCount(Integer acceptedErrorWarnCount) {
		this.acceptedErrorWarnCount = acceptedErrorWarnCount;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

}
