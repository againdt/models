package com.getinsured.hix.model.agency;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the agency_site_hours database table.
 * 
 */
@Entity
@Table(name="agency_site_hours")
@NamedQuery(name="AgencySiteHour.findAll", query="SELECT a FROM AgencySiteHour a")
public class AgencySiteHour implements Serializable,Comparable<AgencySiteHour> {
	private static final long serialVersionUID = 1L;

	public enum Days {
		Monday(1), Tuesday(2), Wednesday(3), Thursday(4), Friday(5), Saturday(6), Sunday(7);
		private int value;

		Days(int pValue) {
			this.setValue(pValue);
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}
	}
	
	@Id
	@SequenceGenerator(name="AGENCY_SITE_HOURS_ID_GENERATOR", sequenceName="AGENCY_SITE_HOURS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AGENCY_SITE_HOURS_ID_GENERATOR")
	private Long id;

	private String day;

	@Column(name="from_time")
	private String fromTime;

	@Column(name="to_time")
	private String toTime;

	//uni-directional many-to-one association to AgencySite
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="agency_site_id")
	private AgencySite agencySite;

	public AgencySiteHour() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDay() {
		return this.day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getFromTime() {
		return this.fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return this.toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public AgencySite getAgencySite() {
		return this.agencySite;
	}

	public void setAgencySite(AgencySite agencySite) {
		this.agencySite = agencySite;
	}

	@Override
	public int compareTo(AgencySiteHour agencySiteHour) {
		return Days.valueOf(this.day).compareTo(Days.valueOf(agencySiteHour.day));
	
	}

}