/**
 * 
 */
package com.getinsured.hix.dto.offexchange;

import com.getinsured.hix.model.GHIXRequest;

/**
 * @author rajaramesh_g
 *
 */
public class OffExchangeRequest extends GHIXRequest{

	private String ticketId;
	private String status;
	private String carrierConfirmationNumber;
	private long eligLeadId;
	private boolean isIndividualMember;
	
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCarrierConfirmationNumber() {
		return carrierConfirmationNumber;
	}
	public void setCarrierConfirmationNumber(String carrierConfirmationNumber) {
		this.carrierConfirmationNumber = carrierConfirmationNumber;
	}
	public long getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	public boolean isIndividualMember() {
		return isIndividualMember;
	}
	public void setIndividualMember(boolean isIndividualMember) {
		this.isIndividualMember = isIndividualMember;
	}
	
}
