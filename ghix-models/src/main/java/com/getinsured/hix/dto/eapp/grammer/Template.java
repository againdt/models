package com.getinsured.hix.dto.eapp.grammer;

import java.util.List;

/**
 * All the templates implement these methods
 * @author root
 *
 */
public interface Template {
	public boolean isValid();
	public List<String> compile();
}
