package com.getinsured.iex.ssap.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SSAP_VERIFICATION_GIWSPAYLOAD database table.
 *
 */
@Entity
@Table(name="SSAP_VERIFICATION_GIWSPAYLOAD")
@NamedQuery(name="SsapVerificationGiwspayload.findAll", query="SELECT s FROM SsapVerificationGiwspayload s")
public class SsapVerificationGiwspayload implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_VERIFICATION_GIWSPAY_SEQ", sequenceName="SSAP_VERIFICATION_GIWSPAY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_VERIFICATION_GIWSPAY_SEQ")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="GI_WS_PAYLOAD_ID", nullable=false, precision=22)
	private java.math.BigDecimal giWsPayloadId;

	//bi-directional many-to-one association to SsapVerification
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SSAP_VERIFICATION_ID", nullable=false)
	private SsapVerification ssapVerification;

	public SsapVerificationGiwspayload() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.math.BigDecimal getGiWsPayloadId() {
		return this.giWsPayloadId;
	}

	public void setGiWsPayloadId(java.math.BigDecimal giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public SsapVerification getSsapVerification() {
		return this.ssapVerification;
	}

	public void setSsapVerification(SsapVerification ssapVerification) {
		this.ssapVerification = ssapVerification;
	}

}