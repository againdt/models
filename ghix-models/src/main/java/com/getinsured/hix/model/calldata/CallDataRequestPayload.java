package com.getinsured.hix.model.calldata;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.getinsured.hix.dto.calldata.CallDataIncomingRequestDTO;
import com.getinsured.hix.dto.calldata.CallDataModifiedRequestDTO;
import com.getinsured.json.type.JsonBinaryType;



/**
 * The persistent class for the CALL_DATA_REQUEST_PAYLOAD database table.
 * 
 */
@Entity
@Table(name="CALL_DATA_REQUEST_PAYLOAD")
@DynamicInsert
@DynamicUpdate
@TypeDefs({
	@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class CallDataRequestPayload implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CALL_DATA_REQUEST_PAYLOAD_SEQ")
	@SequenceGenerator(name = "CALL_DATA_REQUEST_PAYLOAD_SEQ", sequenceName = "CALL_DATA_REQUEST_PAYLOAD_SEQ", allocationSize = 1)
	private int id;


	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable=false)
	private Date created;

	@Type(type = "jsonb")
	@Column(columnDefinition = "json",name="RAW_REQUEST")
	private CallDataIncomingRequestDTO rawRequest;

	@Type(type = "jsonb")
	@Column(columnDefinition = "json",name="MODIFIED_REQUEST")
	private CallDataModifiedRequestDTO modifiedRequest;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATED_TIMESTAMP", nullable = false, updatable=false)
	private Date lastUpdatedTimestamp;

	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public CallDataIncomingRequestDTO getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(CallDataIncomingRequestDTO rawRequest) {
		this.rawRequest = rawRequest;
	}

	public CallDataModifiedRequestDTO getModifiedRequest() {
		return modifiedRequest;
	}

	public void setModifiedRequest(CallDataModifiedRequestDTO modifiedRequest) {
		this.modifiedRequest = modifiedRequest;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}




	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setLastUpdatedTimestamp(new TSDate());
	}
}
