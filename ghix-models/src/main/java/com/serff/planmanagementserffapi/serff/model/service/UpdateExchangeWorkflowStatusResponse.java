
package com.serff.planmanagementserffapi.serff.model.service;

import com.getinsured.hix.model.GHIXResponse;
import com.serff.planmanagementserffapi.common.model.service.ClientException;
import com.serff.planmanagementserffapi.common.model.service.ServerException;


public class UpdateExchangeWorkflowStatusResponse extends GHIXResponse {

    protected String exchangePlanId;
    protected String exchangeWorkflowStatus;
    protected ServerException serverException;
    protected ClientException clientException;

    /**
     * Gets the value of the exchangePlanId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangePlanId() {
        return exchangePlanId;
    }

    /**
     * Sets the value of the exchangePlanId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangePlanId(String value) {
        this.exchangePlanId = value;
    }

    /**
     * Gets the value of the exchangeWorkflowStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeWorkflowStatus() {
        return exchangeWorkflowStatus;
    }

    /**
     * Sets the value of the exchangeWorkflowStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeWorkflowStatus(String value) {
        this.exchangeWorkflowStatus = value;
    }

    /**
     * Gets the value of the serverException property.
     * 
     * @return
     *     possible object is
     *     {@link ServerException }
     *     
     */
    public ServerException getServerException() {
        return serverException;
    }

    /**
     * Sets the value of the serverException property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerException }
     *     
     */
    public void setServerException(ServerException value) {
        this.serverException = value;
    }

    /**
     * Gets the value of the clientException property.
     * 
     * @return
     *     possible object is
     *     {@link ClientException }
     *     
     */
    public ClientException getClientException() {
        return clientException;
    }

    /**
     * Sets the value of the clientException property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientException }
     *     
     */
    public void setClientException(ClientException value) {
        this.clientException = value;
    }

}
