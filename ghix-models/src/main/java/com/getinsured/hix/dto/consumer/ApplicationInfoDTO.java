package com.getinsured.hix.dto.consumer;

import java.io.Serializable;


public class ApplicationInfoDTO implements Serializable, Comparable<ApplicationInfoDTO>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	  /*
	   Set values from SSAPApplication domain into SSAPApplication DTO
	   
	   1. logostr  - "/admin/issuer/company/profile/logo/view/"+enrollment.getIssuer().getId();
	   2. issuerName - enrollment.getInsurerName()
	   3. planId
	   4. PlanUrl
	   5. PlanName
	   6. monthlyPremium  - enrollment.getGrossPremiumAmt()
	   7. aptcAmt - enrollment.getAptcAmt()*/
	
	  
	   int planId;
	   int adults;
	   int childs;
	   long ssapApplicationId;
	   long eligLeadId;
	   boolean aptc;
	   String logoStr;
	   String issuerName;
	   String planUrl;
	   String planName;
	   String monthlyPremiumBeforeCredit;
	   String monthlyPremiumAfterCredit;
	   String insuranceType;
	   String effectiveStartDate;
	   String effectiveStartDateWithoutSlash;
	   String cartId;	// Order Id
	   String stage;
	   String exchangeType;
	   String aptcAmount;
	   private int issuerId;
	   boolean isSsapLocked;
	   boolean isStaleApplication;
	   String offExFlowType; // Determine if an off-exchange major medical application is whether a D2C or SSAP_OFFEX or an ECOMMIT for order on Universal Cart
	   String state;
	   boolean isPostFFMApp;
	   
	 	   
	public String getLogoStr() {
		return logoStr;
	}
	public void setLogoStr(String logoStr) {
		this.logoStr = logoStr;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getPlanUrl() {
		return planUrl;
	}
	public void setPlanUrl(String planUrl) {
		this.planUrl = planUrl;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getMonthlyPremiumBeforeCredit() {
		return monthlyPremiumBeforeCredit;
	}
	public void setMonthlyPremiumBeforeCredit(String monthlyPremiumBeforeCredit) {
		this.monthlyPremiumBeforeCredit = monthlyPremiumBeforeCredit;
	}
	public String getMonthlyPremiumAfterCredit() {
		return monthlyPremiumAfterCredit;
	}
	public void setMonthlyPremiumAfterCredit(String monthlyPremiumAfterCredit) {
		this.monthlyPremiumAfterCredit = monthlyPremiumAfterCredit;
	}

	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public boolean isAptc() {
		return aptc;
	}
	public void setAptc(boolean aptc) {
		this.aptc = aptc;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getEffectiveStartDateWithoutSlash() {
		return effectiveStartDateWithoutSlash;
	}
	public void setEffectiveStartDateWithoutSlash(
			String effectiveStartDateWithoutSlash) {
		this.effectiveStartDateWithoutSlash = effectiveStartDateWithoutSlash;
	}
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public int getAdults() {
		return adults;
	}
	public void setAdults(int adults) {
		this.adults = adults;
	}
	public int getChilds() {
		return childs;
	}
	public void setChilds(int childs) {
		this.childs = childs;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;	
	}
	
	public long getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
		
	public String getAptcAmount() {
		return aptcAmount;
		
	}
	public void setAptcAmount(String aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	
	public boolean getIsSsapLocked() {
		return isSsapLocked;
	}
	public void setIsSsapLocked(boolean isSsapLocked) {
		this.isSsapLocked = isSsapLocked;
	}
	
	public boolean getIsStaleApplication() {
		return isStaleApplication;
	}
	public void setStaleApplication(boolean isStaleApplication) {
		this.isStaleApplication = isStaleApplication;
	}
	
	public String getOffExFlowType() {
		return offExFlowType;
	}
	public void setOffExFlowType(String offExFlowType) {
		this.offExFlowType = offExFlowType;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public boolean getIsPostFFMApp() {
		return isPostFFMApp;
	}
	
	public void setIsPostFFMApp(boolean isPostFFMApp) {
		this.isPostFFMApp = isPostFFMApp;
	}
	
	@Override
	public int compareTo(ApplicationInfoDTO otherAppInfoDTO) {
		 final int BEFORE = -1;
		 final int EQUAL = 0;
		 final int AFTER = 1;

		 if (this == otherAppInfoDTO)  return EQUAL;
		   
		 if (this.ssapApplicationId < otherAppInfoDTO.ssapApplicationId)   return BEFORE;
		 
		 if (this.ssapApplicationId >  otherAppInfoDTO.ssapApplicationId)  return AFTER;

		 return EQUAL;
	}
	
	
	
	  
}
