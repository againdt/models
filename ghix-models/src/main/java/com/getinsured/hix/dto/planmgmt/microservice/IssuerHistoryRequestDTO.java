package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerHistoryRequestDTO extends SearchDTO {

	private String hiosId;

	public IssuerHistoryRequestDTO() {
		super();
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}
}
