package com.getinsured.hix.model.estimator.mini;


/**
 * This object represents the new fields we want to add
 * as part of Census data for Cobra/ Early Retirees/ Small Bussiness Groups
 * 
 * @author Suresh Kancherla
 *
 */

public class CensusLeadRequest extends MiniEstimatorRequest {
	private String firstName;
	private String lastName;
	
	private String street;
	private String city;
	private String stateCode;
	
	//private String isOkToCall; Moved this attribute to Mini Estimator Request
	//private String tcpaComplianceLink;
	private String householdId;
	
	private String householdPin;//  4 digits SSN or AccessPIN based on aff/flow configuration
	

	
	private String censusType;
	private String acceptedTermsAndConditions;
	
	private String alternatePhone;
	
	private double hraAmount; // Household Level
	private String hraTransitionDate;
	
	private String cobraEndDate;
	private double cobraPremium;
	
	private long affFlowAccessId;
	
	private String qualifyingEventEndDate;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCensusType() {
		return censusType;
	}
	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}
	public double getHraAmount() {
		return hraAmount;
	}
	public void setHraAmount(double hraAmount) {
		this.hraAmount = hraAmount;
	}
	public String getHraTransitionDate() {
		return hraTransitionDate;
	}
	public void setHraTransitionDate(String hraTransitionDate) {
		this.hraTransitionDate = hraTransitionDate;
	}
	public String getCobraEndDate() {
		return cobraEndDate;
	}
	public void setCobraEndDate(String cobraEndDate) {
		this.cobraEndDate = cobraEndDate;
	}
	public double getCobraPremium() {
		return cobraPremium;
	}
	public void setCobraPremium(double cobraPremium) {
		this.cobraPremium = cobraPremium;
	}
	public String getAcceptedTermsAndConditions() {
		return acceptedTermsAndConditions;
	}
	public void setAcceptedTermsAndConditions(String acceptedTermsAndConditions) {
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
	}
	public String getAlternatePhone() {
		return alternatePhone;
	}
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getQualifyingEventEndDate() {
		return qualifyingEventEndDate;
	}
	public void setQualifyingEventEndDate(String qualifyingEventEndDate) {
		this.qualifyingEventEndDate = qualifyingEventEndDate;
	}
	public String getHouseholdPin() {
		return householdPin;
	}
	public void setHouseholdPin(String householdPin) {
		this.householdPin = householdPin;
	}
	public long getAffFlowAccessId() {
		return affFlowAccessId;
	}
	public void setAffFlowAccessId(long affFlowAccessId) {
		this.affFlowAccessId = affFlowAccessId;
	}
	
}
