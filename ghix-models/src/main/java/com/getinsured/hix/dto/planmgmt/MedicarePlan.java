/**
 * 
 * @author santanu
 * @version 1.0
 * @since Jan 27, 2015 
 *
 * Define Vision plan data in response 
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.Map;

public class MedicarePlan {
	
	private int id;	
	private String name;
	private int issuerId;
	private String issuerName;
	private String issuerLogo;
	private Float premium;	
	private String brochureUrl;
	private String networkType;
	private String isSNP;
	private String isMedicaid;
	private String benefitsLink;	
	private Map<String, Map<String, String>> benefits;
	private String insuranceType;
	private String planYear;
	private String planHiosId;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the issuerId
	 */
	public int getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the issuerLogo
	 */
	public String getIssuerLogo() {
		return issuerLogo;
	}
	/**
	 * @param issuerLogo the issuerLogo to set
	 */
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	
	/**
	 * @return the premium
	 */
	public Float getPremium() {
		return premium;
	}
	/**
	 * @param premium the premium to set
	 */
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	/**
	 * @return the brochureUrl
	 */
	public String getBrochureUrl() {
		return brochureUrl;
	}
	/**
	 * @param brochureUrl the brochureUrl to set
	 */
	public void setBrochureUrl(String brochureUrl) {
		this.brochureUrl = brochureUrl;
	}
	
	/**
	 * @return the networkType
	 */
	public String getNetworkType() {
		return networkType;
	}
	/**
	 * @param networkType the networkType to set
	 */
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	/**
	 * @return the isSNP
	 */
	public String getIsSNP() {
		return isSNP;
	}
	/**
	 * @param isSNP the isSNP to set
	 */
	public void setIsSNP(String isSNP) {
		this.isSNP = isSNP;
	}
	/**
	 * @return the isMedicaid
	 */
	public String getIsMedicaid() {
		return isMedicaid;
	}
	/**
	 * @param isMedicaid the isMedicaid to set
	 */
	public void setIsMedicaid(String isMedicaid) {
		this.isMedicaid = isMedicaid;
	}
	/**
	 * @return the benefitsLink
	 */
	public String getBenefitsLink() {
		return benefitsLink;
	}
	/**
	 * @param benefitsLink the benefitsLink to set
	 */
	public void setBenefitsLink(String benefitsLink) {
		this.benefitsLink = benefitsLink;
	}	
	/**
	 * @return the benefits
	 */
	public Map<String, Map<String, String>> getBenefits() {
		return benefits;
	}
	
	/**
	 * @param benefits the benefits to set
	 */
	public void setBenefits(Map<String, Map<String, String>> benefits) {
		this.benefits = benefits;
	}
	/**
	 * @return the insuranceType
	 */
	public String getInsuranceType() {
		return insuranceType;
	}
	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	/**
	 * @return the planYear
	 */
	public String getPlanYear() {
		return planYear;
	}
	/**
	 * @param planYear the planYear to set
	 */
	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}
	/**
	 * @return the planHiosId
	 */
	public String getPlanHiosId() {
		return planHiosId;
	}
	/**
	 * @param planHiosId the planHiosId to set
	 */
	public void setPlanHiosId(String planHiosId) {
		this.planHiosId = planHiosId;
	}
	

}
