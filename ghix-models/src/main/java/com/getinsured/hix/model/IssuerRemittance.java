package com.getinsured.hix.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;


/**
 * The persistent class for the ISSUER_INVOICES database table.
 * @author sharma_k
 */

@Audited
@Entity
@Table(name = "issuer_remittance")
public class IssuerRemittance implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public enum PaidStatus { DUE, PAID, PARTIALLY_PAID, IN_PROCESS; }
	
	public enum IsActive {Y, N;}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issuer_remittance_seq")
	@SequenceGenerator(name = "issuer_remittance_seq", sequenceName = "issuer_remittance_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@Column(name = "case_id")
	private String caseId;
	
	@Column(name = "statement_date")
	private Date statementDate;
	
	@Column(name = "remittance_number")
	private String remittanceNumber;
	
	@Column(name = "period_covered")
	private String periodCovered;
	
	@Column(name = "payment_due_date")
	private Date paymentDueDate;
	
	@Column(name = "amt_due_from_last_remittance")
	private BigDecimal amountDueFromLastRemittance;
	
	@Column(name = "total_payment_paid")
	private BigDecimal totalPaymentpaid;
	
	@Column(name = "premiums_this_period")
	private BigDecimal premiumThisPeriod;
	
	@Column(name = "adjustments")
	private BigDecimal adjustments;
	
	@Column(name = "exchange_fees")
	private BigDecimal exchangeFees;
	
	@Column(name = "total_amount_due")
	private BigDecimal totalAmountDue;
	
	@Column(name = "amount_enclosed")
	private BigDecimal amountEnclosed;
	
	@Column(name = "amount_received_from_employer")
	private BigDecimal amountReceivedFromEmployer;
	
	@Column(name = "paid_status", length=4)
	@Enumerated(EnumType.STRING)
	private PaidStatus paidStatus;
	
	@Column(name = "ecm_doc_id")
	private String ecmDocId;
	
	@OneToMany(mappedBy = "issuerRemittance", cascade = CascadeType.ALL)
	private List<IssuerRemittanceLineItem> issuerRemittanceLineItem;
	
	@Column(name = "is_active", length = 1)
	@Enumerated(EnumType.STRING)
	private IsActive isActive;
	
	@Column(name = "gl_code")
	private Integer glCode;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "paid_date_time")
	private Date paidDateTime;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "settlement_date_time")
	private Date settlementDateTime;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}
	
	/**
	 * @return the remittanceNumber
	 */
	public String getRemittanceNumber() {
		return remittanceNumber;
	}

	/**
	 * @param remittanceNumber the remittanceNumber to set
	 */
	public void setRemittanceNumber(String remittanceNumber) {
		this.remittanceNumber = remittanceNumber;
	}

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	
	public PaidStatus getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(PaidStatus paidStatus) {
		this.paidStatus = paidStatus;
	}
	
	public String getEcmDocId() {
		return ecmDocId;
	}

	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}
	
	/**
	 * @return the issuerRemittanceLineItem
	 */
	public List<IssuerRemittanceLineItem> getIssuerRemittanceLineItem() {
		return issuerRemittanceLineItem;
	}

	/**
	 * @param issuerRemittanceLineItem the issuerRemittanceLineItem to set
	 */
	public void setIssuerRemittanceLineItem(
			List<IssuerRemittanceLineItem> issuerRemittanceLineItem) {
		this.issuerRemittanceLineItem = issuerRemittanceLineItem;
	}

	public IsActive getIsActive()
	{
		return isActive;
	}
	
	public void setIsActive(IsActive isActive)
	{
		this.isActive = isActive;
	}
	
	@Override
	public String toString() {
		//return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
		//HIX-25009
		return "IssuerInvoices [id=" + id + ", caseId=" + caseId+ ", statementDate=" + statementDate + "]";
	}
	
	


	public Integer getGlCode() {
		return glCode;
	}

	public void setGlCode(Integer glCode) {
		this.glCode = glCode;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	/**
	 * @return the amountDueFromLastRemittance
	 */
	public BigDecimal getAmountDueFromLastRemittance() {
		return amountDueFromLastRemittance;
	}

	/**
	 * @param amountDueFromLastRemittance the amountDueFromLastRemittance to set
	 */
	public void setAmountDueFromLastRemittance(
			BigDecimal amountDueFromLastRemittance) {
		this.amountDueFromLastRemittance = amountDueFromLastRemittance;
	}

	public BigDecimal getTotalPaymentPaid() {
		return totalPaymentpaid;
	}

	public void setTotalPaymentPaid(BigDecimal totalPaymentpaid) {
		this.totalPaymentpaid = totalPaymentpaid;
	}

	public BigDecimal getPremiumThisPeriod() {
		return premiumThisPeriod;
	}

	public void setPremiumThisPeriod(BigDecimal premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}

	public BigDecimal getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(BigDecimal adjustments) {
		this.adjustments = adjustments;
	}

	public BigDecimal getExchangeFees() {
		return exchangeFees;
	}

	public void setExchangeFees(BigDecimal exchangeFees) {
		this.exchangeFees = exchangeFees;
	}

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getAmountEnclosed() {
		return amountEnclosed;
	}

	public void setAmountEnclosed(BigDecimal amountEnclosed) {
		this.amountEnclosed = amountEnclosed;
	}

	public BigDecimal getAmountReceivedFromEmployer() {
		return amountReceivedFromEmployer;
	}

	public void setAmountReceivedFromEmployer(BigDecimal amountReceivedFromEmployer) {
		this.amountReceivedFromEmployer = amountReceivedFromEmployer;
	}


	public Date getPaidDateTime() {
		return paidDateTime;
	}

	public void setPaidDateTime(Date paidDateTime) {
		this.paidDateTime = paidDateTime;
	}

	public Date getSettlementDateTime() {
		return settlementDateTime;
	}

	public void setSettlementDateTime(Date settlementDateTime) {
		this.settlementDateTime = settlementDateTime;
	}

	/**
	 * @return the totalPaymentpaid
	 */
	public BigDecimal getTotalPaymentpaid() {
		return totalPaymentpaid;
	}

	/**
	 * @param totalPaymentpaid the totalPaymentpaid to set
	 */
	public void setTotalPaymentpaid(BigDecimal totalPaymentpaid) {
		this.totalPaymentpaid = totalPaymentpaid;
	}
	
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

}
