package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the roles database table.
 *
 */

@Entity
@Table(name="issuer_accreditation")

public class IssuerAccrediation  implements Serializable{
	private static final long serialVersionUID = 1L;
	public static enum accreditation_status 	{
		APPROVED, PENDING, DISAPPROVED;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IssuerAccrediation_Seq")
	@SequenceGenerator(name = "IssuerAccrediation_Seq", sequenceName = "issuer_accrediation_seq", allocationSize = 1)
	private int id;

	@ManyToOne
    @JoinColumn(name="issuer_id")
	private Issuer issuer;

	@Temporal(value = TemporalType.DATE)
	@Column(name="accreditation_date")
	private Date accreditationDate;

	@Column(name="accreditation_status", length=20)
	private String accreditationStatus;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Date getAccreditationDate() {
		return accreditationDate;
	}

	public void setAccreditationDate(Date accreditationDate) {
		this.accreditationDate = accreditationDate;
	}

	public String getAccreditationStatus() {
		return accreditationStatus;
	}

	public void setAccreditationStatus(String accreditationStatus) {
		this.accreditationStatus = accreditationStatus;
	}
}