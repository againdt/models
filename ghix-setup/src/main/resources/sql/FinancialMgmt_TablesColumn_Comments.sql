COMMENT ON COLUMN BANK_ACC_INFO.ID IS 'Primary Key, associated with Sequence BANK_ACC_INFO_SEQ';
COMMENT ON COLUMN BANK_ACC_INFO.ROUTING_NUMBER IS '9 digit bank routing number';
COMMENT ON COLUMN BANK_ACC_INFO.TELEGRAPHIC_NAME IS 'Telegraphic code for thee bank';
COMMENT ON COLUMN BANK_ACC_INFO.CUSTOMER_NAME IS 'Bank Name';
COMMENT ON COLUMN BANK_ACC_INFO.STATE_ABBR  IS 'State or territory abbreviation';
COMMENT ON COLUMN BANK_ACC_INFO.CITY IS 'City in which bank is located';
COMMENT ON COLUMN BANK_ACC_INFO.FUNDS_TRANSFER_STATUS IS 'Status for fund transfer Y-eligible or N-ineligible';
COMMENT ON COLUMN BANK_ACC_INFO.FUNDS_SETTLEMENT_ONLY_STATUS IS 'Funds settlement-only status, Value can be S for settled or null';
COMMENT ON COLUMN BANK_ACC_INFO.BOOK_ENTRY_SEC_TRF_STATUS IS 'Book-Entry Securities transfer status:  Y-eligible or N-ineligible';
COMMENT ON COLUMN BANK_ACC_INFO.DATE_OF_LAST_REVISION IS 'Date of last revision: YYYYMMDD, or blank';


COMMENT ON COLUMN FEE_TYPE.ID IS 'Primary Key, associated with Sequence FEE_TYPE_SEQ';
COMMENT ON COLUMN FEE_TYPE.TYPE IS 'Type of Fees which need to be deducted from Employer payment';
COMMENT ON COLUMN FEE_TYPE.RANGE_FROM IS 'Fees range value, lowest fees rate';
COMMENT ON COLUMN FEE_TYPE.RANGE_TO IS 'Fees range value, higest fees rate';
COMMENT ON COLUMN FEE_TYPE.PERCENTAGE_RATE IS 'Rate of percentage which need to be deducted';
COMMENT ON COLUMN FEE_TYPE.CREATED_DATE IS 'New Type of fees creation date';
COMMENT ON COLUMN FEE_TYPE.UPDATED_DATE IS 'fees updation date';

COMMENT ON COLUMN EXCHANGE_FEES.ID IS 'Primary Key, associated with Sequence EXCHANGE_FEES_SEQ';
COMMENT ON COLUMN EXCHANGE_FEES.ISSUER_ID IS 'Issuer Id from which the fees is deducted';
COMMENT ON COLUMN EXCHANGE_FEES.ISSUER_INVOICE_ID IS 'Invoice Id of Issuer whose against the fees is deducted';
COMMENT ON COLUMN EXCHANGE_FEES.ISSUER_PAYMENT_ID IS 'Issuer Payment Id for payment records of fees';
COMMENT ON COLUMN EXCHANGE_FEES.CREATED_DATE IS 'exchange fees is taken on this date';
COMMENT ON COLUMN EXCHANGE_FEES.UPDATED_DATE IS 'exchange fees is updated on this date';
COMMENT ON COLUMN EXCHANGE_FEES.STARTING_DATE IS 'Starting date is month start date';
COMMENT ON COLUMN EXCHANGE_FEES.ENDING_DATE IS 'Ending date is month end date';
COMMENT ON COLUMN EXCHANGE_FEES.TOTAL_FEE_BALANCE_DUE IS 'Total Balance fees need to be taken against issuer invoice';
COMMENT ON COLUMN EXCHANGE_FEES.PAYMENT_RECEIVED IS 'Total exchange fees payment received from issuer payment';
COMMENT ON COLUMN EXCHANGE_FEES.TOTAL_PAYMENT IS 'total payment done to exchange';
COMMENT ON COLUMN EXCHANGE_FEES.TOTAL_REFUND IS 'Refund amount if any';
COMMENT ON COLUMN EXCHANGE_FEES.STATUS IS 'Status may be  DUE, PAID or PARTIAL';



COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.ID IS 'Primary Key, associated with Sequence EMPLOYER_PAYINVOICE_SEQ';
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.INVOICE_ID IS 'Employer Invoice Id, Foreign constraint with Employer_invoices';
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.CREATION_DATE IS 'Payment creation or paid date';
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.AMOUNT IS 'Actual Amount paid';
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.EXCESS_AMOUNT IS 'Amount paid more than the invoice amount';
COMMENT ON COLUMN EMPLOYER_PAYMENT_INVOICE.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements';



COMMENT ON COLUMN EMPLOYER_PAYMENTS.ID IS 'Primary Key, associated with Sequence EMPLOYERPAY_SEQ';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.INVOICE_NUMBER IS 'Employer Invoice no, payment made against this generated invoice';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PERIOD_COVERED IS 'Period covered for this invoice';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PAYMENT_DUE_DATE IS 'Premium payment due date';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Amount due for this Employer from his last invoice';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.TOTAL_PAYMENT_RECEIVED IS 'Total payment received from employer';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PREMIUMS_THIS_PERIOD IS 'Premium this period for the employer';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.ADJUSTMENTS IS 'Adjustment of the premium amount if any';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.EXCHANGE_FEES IS 'Exchange fees for the generated invoice';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.TOTAL_AMOUNT_DUE IS 'Actual amount due';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.PAYMENT_TYPE_ID IS 'Foreign constraint reference table PAYMENT_METHODS, employer added payment method';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.EMPLOYER_PAYMENT_INVOICE_ID IS 'Foreign constraint reference table EMPLOYER_PAYMENT_INVOICE';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.STATUS IS 'Payment status whether Paid or PARTIALLY_PAID';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.TRANSACTION_ID IS 'Cybersource transaction id, used to store payment trxn info';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.IS_REFUND IS 'for the payment Refund or not, values can be N or Y';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.IS_PARTIAL_PAYMENT IS 'Is partial payment or full payment, Values can be N or Y';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.RESPONSE IS 'Cybersource response on the payment done.';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.REFUND_AMT IS 'Refund amount on the payment if any';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.STATEMENT_DATE IS 'the date on which invoice is generated';
COMMENT ON COLUMN EMPLOYER_PAYMENTS.AMOUNT_ENCLOSED IS 'The actual amt paid by employer, that is the amount entered in Payment screen by the user';


COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.ID IS 'Primary Key, associated with Sequence ISSUER_PAYINVOICE_SEQ';
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.INVOICE_ID IS 'Foreign constraint reference ISSUER_INVOICES';
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.CREATION_DATE IS 'Date on which the payment is made';
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.AMOUNT IS 'Toatal amount paid to issuer';
COMMENT ON COLUMN ISSUER_PAYMENT_INVOICE.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements';

COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.ID IS 'Primary Key, associated with Sequence ISSUER_PAYMENT_DETAIL_SEQ';
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.TRANSACTION_ID IS 'Payment done to issuer, Cybersource transaction id';
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.RESPONSE IS 'Cybersource payment response or remarks';
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.AMOUNT_PAID IS 'Amount paid to issuer';
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.PAYMENT_DATE IS 'Date on which payment is done';
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.ISSUER_INVOICE_PAYMENT_ID IS 'Foreign constraint refrence table ISSUER_PAYMENTS';
COMMENT ON COLUMN ISSUER_PAYMENT_DETAIL.PAYMENT_TYPE_ID IS 'Foreign constraint reference table Payment_Methods, payment made via PaymentMethods';



COMMENT ON COLUMN ISSUER_PAYMENTS.ID IS 'Primary Key, associated with Sequence ISSUER_PAYMENTS_SEQ';
COMMENT ON COLUMN ISSUER_PAYMENTS.INVOICE_NUMBER IS 'Issuer Invoice Number';
COMMENT ON COLUMN ISSUER_PAYMENTS.PERIOD_COVERED IS 'Issuer invoice payment period';
COMMENT ON COLUMN ISSUER_PAYMENTS.PAYMENT_DUE_DATE IS 'Issuer payment due date';
COMMENT ON COLUMN ISSUER_PAYMENTS.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Amount due from last invoice';
COMMENT ON COLUMN ISSUER_PAYMENTS.TOTAL_PAYMENT_RECEIVED IS 'Total payment paid to Issuer by exchange';
COMMENT ON COLUMN ISSUER_PAYMENTS.PREMIUMS_THIS_PERIOD IS 'Total employer premium amount for this periods invoice';
COMMENT ON COLUMN ISSUER_PAYMENTS.ADJUSTMENTS IS 'Adjustment amount if any';
COMMENT ON COLUMN ISSUER_PAYMENTS.EXCHANGE_FEES IS 'Exchange fees deducted for the payment paid to issuer';
COMMENT ON COLUMN ISSUER_PAYMENTS.TOTAL_AMOUNT_DUE IS 'Total to be paid to Issuer';
COMMENT ON COLUMN ISSUER_PAYMENTS.ISSUER_PAYMENT_INVOICE_ID IS 'Foreign constraint reference table ISSUER_PAYMENT_INVOICE';
COMMENT ON COLUMN ISSUER_PAYMENTS.STATUS IS 'Payment status valuce can be PAID or PARTIALLY_PAID ';
COMMENT ON COLUMN ISSUER_PAYMENTS.IS_REFUND IS 'Is this payment considered for refund, value can be Y or N';
COMMENT ON COLUMN ISSUER_PAYMENTS.IS_PARTIAL_PAYMENT IS 'For Partial payment values can be Y or N';
COMMENT ON COLUMN ISSUER_PAYMENTS.STATEMENT_DATE IS 'The date on which invoice is generated';
COMMENT ON COLUMN ISSUER_PAYMENTS.AMOUNT_ENCLOSED IS 'The actual amt to be paid to issuer';



COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.ID IS 'Primary Key, associated with Sequence ISSUER_INVOICES_LINEITEMS_SEQ';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EMPLOYER_ID IS 'Employer Id for the opted plan with associated Issuer';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.POLICY_NUMBER IS 'Policy no of the policy opted by employee';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EMPLOYEE_ID IS 'Employeed Id covered under the plan';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.CARRIER_NAME IS 'Plan provided name';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.PLAN_TYPE IS 'Plan opted by Employee under said Carrier';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.PERSONS_COVERED IS 'Persons id covered under this plan';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.TOTAL_PREMIUM IS 'Toatl premium amount this period';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.NET_AMOUNT IS 'Total amount to be paid to the issuer';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.AMOUNT_RECEIVED_FROM_EMPLOYER IS 'Amount received from employer for this invoice';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.PERIOD_COVERED IS 'Period covered for the respective invoice';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.INVOICE_ID IS 'Foreign constraint reference table ISSUER_INVOICES';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.USER_FEE IS 'Total exchange fees against the Total premium';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.AMOUNT_PAID_TO_ISSUER IS 'Total amount paid to issuer after deduction of Exchange fees';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.EMP_INV_LINEITEMS_ID IS 'Foreign constraint reference table EMPLOYER_INVOICES_LINEITEMS';
COMMENT ON COLUMN ISSUER_INVOICES_LINEITEMS.COVERAGE_TYPE IS 'Provided Coverage type opted by Employee';

COMMENT ON COLUMN ISSUER_INVOICES.ID IS 'Primary Key, associated with Sequence ISSUER_INVOICES_SEQ';
COMMENT ON COLUMN ISSUER_INVOICES.ISSUER_ID IS 'Foreign constraint refrence table Issuers, for the issuer whose invoice is generated';
COMMENT ON COLUMN ISSUER_INVOICES.STATEMENT_DATE IS 'Date on which invoice is generated';
COMMENT ON COLUMN ISSUER_INVOICES.INVOICE_NUMBER IS 'Unique invoice no for each generated invoice.';
COMMENT ON COLUMN ISSUER_INVOICES.PERIOD_COVERED IS 'Period covered for this invoice';
COMMENT ON COLUMN ISSUER_INVOICES.PAYMENT_DUE_DATE IS 'Payment due date for this invoice';
COMMENT ON COLUMN ISSUER_INVOICES.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Amount due from last invoice if any';
COMMENT ON COLUMN ISSUER_INVOICES.TOTAL_PAYMENT_PAID IS 'Total payment paid for this invoice';
COMMENT ON COLUMN ISSUER_INVOICES.PREMIUMS_THIS_PERIOD IS 'Employers total premium this period for this issuer';
COMMENT ON COLUMN ISSUER_INVOICES.ADJUSTMENTS IS 'Settlement mount adjustment if any';
COMMENT ON COLUMN ISSUER_INVOICES.EXCHANGE_FEES IS 'Total exchange fees on total amount due';
COMMENT ON COLUMN ISSUER_INVOICES.TOTAL_AMOUNT_DUE IS 'Sum of amount due this period';
COMMENT ON COLUMN ISSUER_INVOICES.AMOUNT_RECEIVED_FROM_EMPLOYER IS 'Amount received from employer for this issuer against this period';
COMMENT ON COLUMN ISSUER_INVOICES.PAID_STATUS IS 'Paid Status value can be DUE, PAID or PARTIALLY_PAID';
COMMENT ON COLUMN ISSUER_INVOICES.ECM_DOC_ID IS 'Alfresco ECM doc if for the generated PDF reports';
COMMENT ON COLUMN ISSUER_INVOICES.IS_ACTIVE IS 'Stands for whether this invoice is active or not value can be Y or N';
COMMENT ON COLUMN ISSUER_INVOICES.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements';
COMMENT ON COLUMN ISSUER_INVOICES.AMOUNT_ENCLOSED IS 'The actual amt to be paid to issuer';



COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.ID IS 'Primary Key, associated with Sequence EMPLOYER_LINEITEMS_SEQ';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.EMPLOYER_ID IS 'Employer Id whom employee belongs to';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.POLICY_NUMBER IS 'Policy No of the opted plan';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.EMPLOYEE_ID IS 'Foreign constraint refrence table Employees, for employee id ';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.CARRIER_NAME IS 'Issuer Name whose plan is opted by employee';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PLAN_TYPE IS 'Plan type opted by the employee ffrom the said issuer';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.COVERAGE_TYPE IS 'Plan coverage offered by Issuer';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PERSONS_COVERED IS 'Persons id of thee persons covered under this plan';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.TOTAL_PREMIUM IS 'Total premium of the opted plan';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.EMPLOYEE_CONTRIBUTION IS 'Employee contribution of the piad amount for the lineitem';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.NET_AMOUNT IS 'amount to be paid by employer for this plan selected by employee';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PERIOD_COVERED IS 'Period covered under this plan';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.INVOICE_ID IS 'Foreign constraint reference table Employer_invoices';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.ISSUER_ID IS 'Foreign constraint reference table Issuers';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAYMENT_RECEIVED IS 'Payment received by the employer against the selected plan of Employee';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAID_STATUS IS 'Payment status value can be  DUE,PAID or PARTIALLY_PAID;';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAID_DATE IS 'Date on which premium payment is done';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.PAID_TO_ISSUER IS 'Whether this payment is paid to issuer or not values can be Y or N';
COMMENT ON COLUMN EMPLOYER_INVOICES_LINEITEMS.RETRO_ADJUSTMENTS IS 'It is the adjustment made with respect to last invoice i.e if on last invoice we charge for an employee and he resigned, then we have to deduct that much amount. Those adjustments are stored here';

COMMENT ON COLUMN EMPLOYER_INVOICES.ID IS 'Primary Key, associated with Sequence EMPLOYER_INVOICES_SEQ';
COMMENT ON COLUMN EMPLOYER_INVOICES.EMPLOYER_ID IS 'Id of Employer whose invoice is generated';
COMMENT ON COLUMN EMPLOYER_INVOICES.STATEMENT_DATE IS 'Date on which invoice is generated';
COMMENT ON COLUMN EMPLOYER_INVOICES.INVOICE_NUMBER IS 'Unique no of generated invoice, used to identify invoice';
COMMENT ON COLUMN EMPLOYER_INVOICES.PERIOD_COVERED IS 'Period covered under this active invoice';
COMMENT ON COLUMN EMPLOYER_INVOICES.PAYMENT_DUE_DATE IS 'Last date of premium payment';
COMMENT ON COLUMN EMPLOYER_INVOICES.AMOUNT_DUE_FROM_LAST_INVOICE IS 'Due amount carried from last paid or unpaid invoice';
COMMENT ON COLUMN EMPLOYER_INVOICES.TOTAL_PAYMENT_RECEIVED IS 'Total Payment received from employer this period';
COMMENT ON COLUMN EMPLOYER_INVOICES.PREMIUMS_THIS_PERIOD IS 'Generated premium of invoice this period';
COMMENT ON COLUMN EMPLOYER_INVOICES.ADJUSTMENTS IS 'Adjustment of premium from the previous paid invoice if any';
COMMENT ON COLUMN EMPLOYER_INVOICES.EXCHANGE_FEES IS 'Exchanges fees occured for total amount due ';
COMMENT ON COLUMN EMPLOYER_INVOICES.TOTAL_AMOUNT_DUE IS 'Total amount due from employer invoices';
COMMENT ON COLUMN EMPLOYER_INVOICES.AMOUNT_ENCLOSED IS ', the actual amt paid by employer, that is the amount entered in Payment screen by the user';
COMMENT ON COLUMN EMPLOYER_INVOICES.PAID_STATUS IS 'Status of payment, value can be  DUE,PAID or PARTIALLY_PAID';
COMMENT ON COLUMN EMPLOYER_INVOICES.ECM_DOC_ID IS 'Alfresco ECM Doc if for the generated invoice pdf';
COMMENT ON COLUMN EMPLOYER_INVOICES.INVOICE_TYPE IS 'Type of Generated invoice, value can be BINDER_INVOICE or NORMAL';
COMMENT ON COLUMN EMPLOYER_INVOICES.IS_ACTIVE IS 'For the invoice active or inactive, value can be Y or N';
COMMENT ON COLUMN EMPLOYER_INVOICES.GL_CODE IS 'GL Coding needs to used in the report for accounting system integration requirements';
COMMENT ON COLUMN EMPLOYER_INVOICES.REISSUE_ID IS 'If new reissue invoice for same period is generated then the previous invoice id is set here';

COMMENT ON COLUMN PAYMENT_METHODS.ID IS 'Primary Key, associated with Sequence PAYMENT_METHODS_SEQ';
COMMENT ON COLUMN PAYMENT_METHODS.EMPLOYER_ID IS 'Id of Employer whose Payment this belongs to';
COMMENT ON COLUMN PAYMENT_METHODS.FINANCIAL_INFO_ID IS 'Foreign constraint refrence table FINANCIAL_INFO, For refering Payment details';
COMMENT ON COLUMN PAYMENT_METHODS.PAYMENT_TYPE IS 'Type of payment values can be CREDITCARD, BANK or MANUAL';
COMMENT ON COLUMN PAYMENT_METHODS.STATUS IS 'Status of Payment method values can be Active or InActive';
COMMENT ON COLUMN PAYMENT_METHODS.IS_DEFAULT IS 'Only one payment method can be Defualt for any Employer or issuer';
COMMENT ON COLUMN PAYMENT_METHODS.PAYMENT_METHOD_NAME IS 'Created payment method name';
COMMENT ON COLUMN PAYMENT_METHODS.MODULE_ID IS 'module id refers Issuer id for issuer payment method here employer id will be null';
COMMENT ON COLUMN PAYMENT_METHODS.MODULE_NAME IS 'module name for whose payment method belongs to ex: issuer';

COMMENT ON COLUMN BANK_INFO.ID IS 'Primary Key, associated with Sequence BANK_INFO_SEQ';
COMMENT ON COLUMN BANK_INFO.ACCOUNT_NUMBER IS 'Encrypted account number of the customer';
COMMENT ON COLUMN BANK_INFO.ACCOUNT_TYPE IS 'Type of Bank Account valuve can be C-Checking or S-Saving';
COMMENT ON COLUMN BANK_INFO.BANK_NAME IS 'Bank name in which customer has account';
COMMENT ON COLUMN BANK_INFO.CREATED IS 'Date on which payment method with type Bank is created';
COMMENT ON COLUMN BANK_INFO.ROUTING_NUMBER IS '9 digit ABA routing no, Refer table BANK_ACC_INFO';
COMMENT ON COLUMN BANK_INFO.UPDATED IS 'Last date on which Account details are updated';
COMMENT ON COLUMN BANK_INFO.NAME_ON_ACCOUNT IS 'Customer name as mentioned while creating bank account';


COMMENT ON COLUMN CREDIT_CARD_INFO.ID IS 'Primary Key, associated with Sequence CREDIT_CARD_INFO_SEQ';
COMMENT ON COLUMN CREDIT_CARD_INFO.CARD_NUMBER IS '16 digit encrypted credit card number';
COMMENT ON COLUMN CREDIT_CARD_INFO.CARD_TYPE IS 'Type of card values can be MasterCard, Visa or AmericanExpress';
COMMENT ON COLUMN CREDIT_CARD_INFO.CREATED IS 'Date on which Payment method with type Credit card is created';
COMMENT ON COLUMN CREDIT_CARD_INFO.EXPIRATION_MONTH IS 'Credit card Expiration Month values in the format MM';
COMMENT ON COLUMN CREDIT_CARD_INFO.EXPIRATION_YEAR IS 'Credit card Expiration Year values in the format YYYY';
COMMENT ON COLUMN CREDIT_CARD_INFO.UPDATED IS 'Last date on which Payment details updated';
COMMENT ON COLUMN CREDIT_CARD_INFO.NAME_ON_CARD IS 'Customer name on Credit card';
COMMENT ON COLUMN CREDIT_CARD_INFO.SECURE_TOKEN IS '4 digit security code on the card';

COMMENT ON COLUMN FINANCIAL_INFO.ID IS 'Primary Key, associated with Sequence FINANCIAL_INFO_SEQ';
COMMENT ON COLUMN FINANCIAL_INFO.ADDRESS1 IS 'Address line 1 provided by user through UI';
COMMENT ON COLUMN FINANCIAL_INFO.CITY IS 'users city provided through UI at the time of account creation';
COMMENT ON COLUMN FINANCIAL_INFO.COUNTRY IS 'users country provided through UI at the time of account creation';
COMMENT ON COLUMN FINANCIAL_INFO.CREATED IS 'Date on which Payment method is created';
COMMENT ON COLUMN FINANCIAL_INFO.EMAIL IS 'users Email address provided through UI at the time of account creation';
COMMENT ON COLUMN FINANCIAL_INFO.FIRST_NAME IS 'users first name provided through UI';
COMMENT ON COLUMN FINANCIAL_INFO.LAST_NAME IS 'users last name provided through UI';
COMMENT ON COLUMN FINANCIAL_INFO.PAYMENT_TYPE IS 'user payment type values can be, CREDITCARD, BANK or MANUAL;';
COMMENT ON COLUMN FINANCIAL_INFO.STATE IS 'State name provided by user through UI';
COMMENT ON COLUMN FINANCIAL_INFO.UPDATED IS 'Last date on which payment methods financial info is updated';
COMMENT ON COLUMN FINANCIAL_INFO.ZIPCODE IS '5 digit Zipcode provided by user through UI';
COMMENT ON COLUMN FINANCIAL_INFO.BANK_INFO_ID IS 'If Payment method is Bank then BANK_INFO table entry mapping is done else null is set';
COMMENT ON COLUMN FINANCIAL_INFO.CREDIT_CARD_INFO_ID IS 'If Payment method is credit card then CREDIT_CARD_INFO table entry mapping is refered else null is set';
COMMENT ON COLUMN FINANCIAL_INFO.ADDRESS2 IS 'Address line 1 provided by user through UI';
COMMENT ON COLUMN FINANCIAL_INFO.CONTACT_NUMBER IS 'Users contact number';


COMMENT ON COLUMN REMITTANCE.ID IS 'Rownum associated with view for unique records';
COMMENT ON COLUMN REMITTANCE.INVOICE_NUMBER IS 'Issuer invoice number';
COMMENT ON COLUMN REMITTANCE.STATEMENT_DATE IS 'Date on which invoice is generated';
COMMENT ON COLUMN REMITTANCE.EMPLOYER_NAME IS 'Name of Employer whose employee is opted this issuers plan';
COMMENT ON COLUMN REMITTANCE.EMPLOYEE_NAME IS 'Name of Employer opted for the plan';
COMMENT ON COLUMN REMITTANCE.CARRIER_NAME IS 'name of the issuer';
COMMENT ON COLUMN REMITTANCE.PLAN_TYPE IS 'Plan type provided by issuer';
COMMENT ON COLUMN REMITTANCE.STATUS IS 'Payment to issuer status value can be PAID or PARTIALLY_PAID;';
COMMENT ON COLUMN REMITTANCE.NET_AMOUNT IS 'Net amount to be paid to the issuer';
COMMENT ON COLUMN REMITTANCE.AMOUNT_PAID_TO_ISSUER IS 'Actual paid amount to the issuer';
COMMENT ON COLUMN REMITTANCE.DUE_AMOUNT IS 'Amount difference between NET_AMOUNT and AMOUNT_PAID_TO_ISSUER';
COMMENT ON COLUMN REMITTANCE.EXCHANGE_FEES IS 'Calculated exchanges fees on netAmount';
COMMENT ON COLUMN REMITTANCE.DEDUCTED_EXCHANGE_FEES IS 'Actual deducted fees on paid amount';
COMMENT ON COLUMN REMITTANCE.PAYMENT_DATE IS 'Date on which payment is done';
COMMENT ON COLUMN REMITTANCE.TRANSACTION_ID IS 'Cybersource transaction id through which payment is done to the issuer';
COMMENT ON COLUMN REMITTANCE.PAYMENT_TYPE IS 'Payment mode through which payment is done, for issuer Payment_Type will be Bank only';
COMMENT ON COLUMN REMITTANCE.ACCOUNT_NUMBER IS 'Issuer account number of the account in which the payment is credited';

COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.ID IS 'Rownum associated with view for unique records';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.EMPLOYER_ID IS 'Employer unique id';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.EMPLOYER_NAME IS 'Name of employer';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.INVOICE_NUMBER IS 'Generated invoice number';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.STATEMENT_DATE IS 'Date on which invoice is created';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.TOTAL_AMOUNT_DUE IS 'Total premium amount which is due';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.PAID_AMOUNT IS 'Amount paid by the employer';
COMMENT ON COLUMN ACCOUNT_RECEIVEABLE.PENDING_AMOUNT IS 'Pending amount is the difference amount of TOTAL_AMOUNT_DUE and PAID_AMOUNT';

COMMENT ON COLUMN ACCOUNT_PAYABLE.ID IS 'Rownum associated with view for unique records';
COMMENT ON COLUMN ACCOUNT_PAYABLE.ISSUER_ID IS 'Issuer unique id';
COMMENT ON COLUMN ACCOUNT_PAYABLE.ISSUER_NAME IS 'Name of Issuer';
COMMENT ON COLUMN ACCOUNT_PAYABLE.INVOICE_NUMBER IS 'Generated invoice number';
COMMENT ON COLUMN ACCOUNT_PAYABLE.STATEMENT_DATE IS 'Date on which invoice is generated';
COMMENT ON COLUMN ACCOUNT_PAYABLE.TOTAL_AMOUNT_DUE IS 'Total amount due which need to be paid to the issuer';
COMMENT ON COLUMN ACCOUNT_PAYABLE.PAID_AMOUNT IS 'Total paid amount to the issuer';
COMMENT ON COLUMN ACCOUNT_PAYABLE.BALANCE_AMOUNT IS 'Pending amount to be paid to the issuer, value is difference between TOTAL_AMOUNT_DUE and PAID_AMOUNT';

COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.ROW_NUMBER IS 'Rownum associated with view for unique records';
COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.EMPLOYER_ID IS 'Employer unique id';
COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.EMPLOYER_NAME IS 'Name of Employer';
COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.PLAN_TYPE IS 'Plan opted for';
COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.NET_AMOUNT IS 'Net amount to be paid to the issuer';
COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.PAYMENT_RECEIVED IS 'Payment received from employer';
COMMENT ON COLUMN QHP_ACCOUNT_RECEIVEABLE.PENDING_AMOUNT IS 'Remaining amount to be paid by Employer, value is difference between NET_AMOUNT and PAYMENT_RECEIVED';