/**
 * 
 * @author santanu
 * @version 1.0
 * @since Oct 16, 2015 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class PediatricPlanRequestDTO implements Serializable {
	/* 
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String hiosPlanId;
	private Integer planYear;
	
	/**
	 * @return the hiosPlanId
	 */
	public String getHiosPlanId() {
		return hiosPlanId;
	}
	/**
	 * @param hiosPlanId the hiosPlanId to set
	 */
	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}
	/**
	 * @return the planYear
	 */
	public Integer getPlanYear() {
		return planYear;
	}
	/**
	 * @param planYear the planYear to set
	 */
	public void setPlanYear(Integer planYear) {
		this.planYear = planYear;
	}

}
