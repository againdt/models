set head off
set feedback off
set term off
set trims on
set lines 200
set pages 0
spool grant_and_create_synonyms_PRODNM_READONLY_readonly.sql
---
----
---- grant DMLs 
select '---' from dual;
select '--- grant DMLs ' from dual;
select '---' from dual;
--- 
select 'grant select on PRODNM_OWNER.'||object_name|| ' to PRODNM_READONLY_ROLE;'
from dba_objects
where  OWNER='PRODNM_OWNER'
and object_type='TABLE';

/*
select 'grant select on PRODNM_OWNER.'||object_name|| ' to PRODNM_READONLY_ROLE;'
from dba_objects
where  OWNER='PRODNM_OWNER'
and object_type='SEQUENCE';
*/

/*
ACCOUNT_PAYABLE(VIEW)
issuer_invoices inv, issuer_payment_invoice pay, issuers i
*/


 grant select on PRODNM_OWNER.issuer_invoices to PRODNM_READONLY;
 grant select on PRODNM_OWNER.issuer_payment_invoice  to PRODNM_READONLY;
 grant select on PRODNM_OWNER.issuers to PRODNM_READONLY;

/*
ACCOUNT_RECEIVEABLE(VIEW)
employer_invoices inv, employer_payment_invoice pay, employers e, employer_payments empPay
*/


grant select on PRODNM_OWNER.employer_invoices to PRODNM_READONLY;
grant select on PRODNM_OWNER.employer_payment_invoice to PRODNM_READONLY;
grant select on PRODNM_OWNER.employers to PRODNM_READONLY;
grant select on PRODNM_OWNER.employer_payments to PRODNM_READONLY;

/*
QHP_ACCOUNT_RECEIVEABLE(VIEW)
Employers ; Employer_invoices_lineitems 
*/


grant select on PRODNM_OWNER.Employers to PRODNM_READONLY;
grant select on PRODNM_OWNER.Employer_invoices_lineitems to PRODNM_READONLY;


/*
REMITTANCE (VIEW)
ISSUER_PAYMENTS inv, ISSUER_PAYMENT_DETAIL dtl, PAYMENT_METHODS pay, BANK_INFO bnk, ISSUER_INVOICES_LINEITEMS li, Employers emp, Employees empl
*/

grant select on PRODNM_OWNER.ISSUER_PAYMENTS to PRODNM_READONLY;
grant select on PRODNM_OWNER.ISSUER_PAYMENT_DETAIL to PRODNM_READONLY;
grant select on PRODNM_OWNER.PAYMENT_METHODS to PRODNM_READONLY;
grant select on PRODNM_OWNER.BANK_INFO to PRODNM_READONLY;
grant select on PRODNM_OWNER.ISSUER_INVOICES_LINEITEMS to PRODNM_READONLY;
grant select on PRODNM_OWNER.Employers to PRODNM_READONLY;
grant select on PRODNM_OWNER.Employees to PRODNM_READONLY;

----
---- create synonymns
select '---' from dual;
select '--- create synonymns ' from dual;
select '---' from dual;

select 'create synonym PRODNM_READONLY.'||object_name|| ' for PRODNM_OWNER.'||object_name||';'
from dba_objects
where  OWNER='PRODNM_OWNER'
and object_type='TABLE';

select 'create synonym PRODNM_READONLY.'||object_name|| ' for PRODNM_OWNER.'||object_name||';'
from dba_objects
where  OWNER='PRODNM_OWNER'
and object_type='VIEW';

select 'create synonym PRODNM_READONLY.'||object_name|| ' for PRODNM_OWNER.'||object_name||';'
from dba_objects
where  OWNER='PRODNM_OWNER'
and object_type='SEQUENCE';

spool off
@grant_and_create_synonyms_PRODNM_READONLY_readonly.sql
quit
