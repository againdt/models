package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


/**
 * The persistent class for the employer plans database table.
 * 
 */

@Audited
@Entity
@Table(name = "employer_invoice_lineitems")
public class EmployerInvoiceLineItems implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Status { DUE,PAID,PARTIALLY_PAID,IN_PROCESS,CANCEL; }
	
	//Here P flag is for In_Process Echeck Payment
	public enum PaidToIssuer {Y,N,P;}
	
	public enum IssuerInvoicesGeneratedFlag{Y,N;}
	
	public enum IsActiveEnrollment{Y,N};
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employer_lineitems_seq")
	@SequenceGenerator(name = "employer_lineitems_seq", sequenceName = "employer_lineitems_seq", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "employer_id")
	private Employer employer;

	@Column(name = "policy_number")
	private String policyNumber;
	
	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;
	
	@Column(name = "carrier_name")
	private String carrierName;
	
	@Column(name = "plan_type")
	private String playType;
	
	@Column(name = "coverage_type")
	private String coverageType;
	
	@Column(name = "persons_covered")
	private String personsCovered;
	
	@Column(name = "total_premium")
	private BigDecimal totalPremium;
	
	@Column(name = "employee_contribution")
	private BigDecimal employeeContribution;
	
	@Column(name = "retro_adjustments")
	private BigDecimal retroAdjustments;
	
	@Column(name = "net_amount")
	private BigDecimal netAmount;
	
	@Column(name = "period_covered")
	private String periodCovered;
	
	@ManyToOne
	@JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@ManyToOne
    @JoinColumn(name="invoice_id")
	private EmployerInvoices employerInvoices;
	
	@Column(name = "payment_received")
	private BigDecimal paymentReceived;
	
	@Column(name = "paid_status")
	@Enumerated(EnumType.STRING)
	private Status paidStatus;
	
	@Column(name = "paid_date")
	private Date paidDate;
	
	@Column(name = "paid_to_issuer")
	@Enumerated(EnumType.STRING)
	private PaidToIssuer paidToIssuer;
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="enrollment_id")*/
	@Column(name ="enrollment_id")
	private Integer enrollmentId;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	/**
	 *  Flag to check whether issuer invoice is generated (Y) or not (N)
	 */
	@Column(name = "issuer_invoice_generated", length=1)
	@Enumerated(EnumType.STRING)
	private IssuerInvoicesGeneratedFlag isIssuerInvoiceGenerated;
	
	
	@Column(name = "employer_contribution")
	private BigDecimal employerContribution;
	
	@Column(name = "employer_enrollment_id")
	private Integer employerEnrollmentId;
	
	@Column(name = "isactive_enrollment")
	@Enumerated(EnumType.STRING)
	private IsActiveEnrollment isActiveEnrollment;
	
	@Column(name = "manual_adjustment_amount")
	private BigDecimal manualAdjustmentAmount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "last_updated_by")
	private AccountUser lastUpdatedBy;
	
	@ManyToOne
	@JoinColumn(name = "reason_code_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private AdjustmentReasonCode reasonCodeID;
	
	@Column(name = "comments")
	private String comments;
	
	@Column(name = "manual_employer_contribution")
	private BigDecimal manualEmployerContribution;
	
	@Column(name = "manual_employee_contribution")
	private BigDecimal manualEmployeeContribution;
	
	
	
	/* to autoupdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getPlayType() {
		return playType;
	}

	public void setPlayType(String playType) {
		this.playType = playType;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public String getPersonsCovered() {
		return personsCovered;
	}

	public void setPersonsCovered(String personsCovered) {
		this.personsCovered = personsCovered;
	}

	

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public EmployerInvoices getEmployerInvoices() {
		return employerInvoices;
	}

	public void setEmployerInvoices(EmployerInvoices employerInvoices) {
		this.employerInvoices = employerInvoices;
	}
	
	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Status getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(Status paidStatus) {
		this.paidStatus = paidStatus;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	
	public PaidToIssuer getPaidToIssuer()
	{
		return paidToIssuer;
	}
	
	public void setPaidToIssuer(PaidToIssuer paidToIssuer)
	{
		this.paidToIssuer = paidToIssuer;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigDecimal getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}

	public BigDecimal getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(BigDecimal employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public BigDecimal getRetroAdjustments() {
		return retroAdjustments;
	}

	public void setRetroAdjustments(BigDecimal retroAdjustments) {
		this.retroAdjustments = retroAdjustments;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(BigDecimal paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public IssuerInvoicesGeneratedFlag getIsIssuerInvoiceGenerated() {
		return isIssuerInvoiceGenerated;
	}

	public void setIsIssuerInvoiceGenerated(
			IssuerInvoicesGeneratedFlag isIssuerInvoiceGenerated) {
		this.isIssuerInvoiceGenerated = isIssuerInvoiceGenerated;
	}
	
	public BigDecimal getEmployerContribution() {
		return employerContribution;
	}

	public void setEmployerContribution(BigDecimal employerContribution) {
		this.employerContribution = employerContribution;
	}	
	
	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}

	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}

	public IsActiveEnrollment getIsActiveEnrollment() {
		return isActiveEnrollment;
	}

	public void setIsActiveEnrollment(IsActiveEnrollment isActiveEnrollment) {
		this.isActiveEnrollment = isActiveEnrollment;
	}
	
	public AccountUser getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(AccountUser lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public AdjustmentReasonCode getReasonCodeID() {
		return reasonCodeID;
	}

	public void setReasonCodeID(AdjustmentReasonCode reasonCodeID) {
		this.reasonCodeID = reasonCodeID;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public BigDecimal getManualAdjustmentAmount() {
		return manualAdjustmentAmount;
	}

	public void setManualAdjustmentAmount(BigDecimal manualAdjustmentAmount) {
		this.manualAdjustmentAmount = manualAdjustmentAmount;
	}

	public BigDecimal getManualEmployerContribution() {
		return manualEmployerContribution;
	}

	public void setManualEmployerContribution(BigDecimal manualEmployerContribution) {
		this.manualEmployerContribution = manualEmployerContribution;
	}

	public BigDecimal getManualEmployeeContribution() {
		return manualEmployeeContribution;
	}

	public void setManualEmployeeContribution(BigDecimal manualEmployeeContribution) {
		this.manualEmployeeContribution = manualEmployeeContribution;
	}

	@Override
	public String toString() {
		return "EmployerInvoiceLineItems [id=" + id + ", employer=" + employer
				+ ", policyNumber=" + policyNumber + ", employee=" + employee
				+ ", carrierName=" + carrierName + ", playType=" + playType
				+ ", coverageType=" + coverageType + ", personsCovered="
				+ personsCovered + ", totalPremium=" + totalPremium
				+ ", employeeContribution=" + employeeContribution
				+ ", retroAdjustments=" + retroAdjustments + ", netAmount="
				+ netAmount + ", periodCovered=" + periodCovered + ", issuer="
				+ issuer + ", employerInvoices=" + employerInvoices
				+ ", paymentReceived=" + paymentReceived + ", paidStatus="
				+ paidStatus + ", paidDate=" + paidDate + ", paidToIssuer="
				+ paidToIssuer + ",enrollmentID=" + enrollmentId + "]";
	}
}
