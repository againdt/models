package com.getinsured.hix.model.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.getinsured.hix.model.AccountUser;

/**
 * The persistent class for table EE_ASSISTERS, to save the Assister
 * information.
 */
@Audited
@Entity
@Table(name = "EE_ASSISTERS")
public class Assister implements Serializable {

	private static final long serialVersionUID = 1L;
	public Assister() {
	}
	
	public interface AddAssisterByEntity {
		
	}
	public interface AssisterCerticationNumber {
		
	}

	public interface AssisterInfoGroup {
		
	}
	
	public interface AssisterCertStatusGroup{
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ee_assisters_Seq")
	@SequenceGenerator(name = "ee_assisters_Seq", sequenceName = "ee_assisters_Seq", allocationSize = 1)
	private int id;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "EE_ENTITY_ID")
	private EnrollmentEntity entity;

	@NotEmpty(message="{label.entityValidateFirstName}",groups=Assister.AddAssisterByEntity.class)
	@Size(max=100,groups=Assister.AddAssisterByEntity.class)
	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@NotEmpty(message="{label.entityValidateLastName}",groups=Assister.AddAssisterByEntity.class)
	@Size(max=100,groups=Assister.AddAssisterByEntity.class)
	@Column(name = "LAST_NAME")
	private String lastName;

	@NotEmpty(message="{label.entityValidatePleaseEnterValidEmail}",groups=Assister.AddAssisterByEntity.class)
	@Email(message="{label.validatePleaseEnterValidEmail}",groups=Assister.AddAssisterByEntity.class)
	@Size(max=100,groups=Assister.AddAssisterByEntity.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "EMAIL_ADDRESS")
	private String emailAddress;
	
	@NotEmpty(message="{label.validatePrimaryPhoneNo}",groups=Assister.AddAssisterByEntity.class)
	@Size(min=10,max=10,groups=Assister.AddAssisterByEntity.class)
	@Pattern(regexp="(^[1-9][0-9]{9}$)|(^$)",message="{label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9}",groups=Assister.AddAssisterByEntity.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "PRIMARY_PHONE_NUMBER")
	private String primaryPhoneNumber;

	@Size(max=10,groups=Assister.AddAssisterByEntity.class)
	@Pattern(regexp="(^[1-9][0-9]{9}$)|(^$)",message="{label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9}",groups=Assister.AddAssisterByEntity.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "SECONDARY_PHONE_NUMBER")
	private String secondaryPhoneNumber;

	@NotEmpty(groups=Assister.AssisterInfoGroup.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "COMMUNICATION_PREF")
	private String communicationPreference;

	@Column(name = "STATUS")
	private String status;

	
	@Column(name = "CERTIFICATION_STATUS")
	private String certificationStatus;

	@NotNull(message="{label.validateCertificationNo}",groups=Assister.AssisterCerticationNumber.class)
	@Column(name = "CERTIFICATION_NUMBER")
	private Long certificationNumber;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CERTIFICATION_DATE")
	private Date certificationDate;

	@Future(groups=Assister.AssisterCertStatusGroup.class)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "RECERTIFICATION_DATE")
	private Date reCertificationDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "DECERTIFICAITON_DATE")
	private Date deCertificationDate;

	@Transient
	private String clientCount;
	
	@NotEmpty(groups=Assister.AddAssisterByEntity.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "EDUCATION")
	private String education;

	@Transient
	private transient byte[] photo;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "ASSISTER_NUMBER")
	private Long assisterNumber;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "DOCUMENT_ID")
	private Integer documentId;

	@Column(name = "ACTIVITY_COMMENTS")
	private String activityComments;
	
	@Column(name = "ASSISTER_PHOTO_DOCUMENT_ID")
	private Integer assisterPhotoDocumentId;
	
	// bi-directional one-to-one association to User
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "USER_ID")
	private AccountUser user;

	@NotNull(message="{label.validatePrimaryAssisterSite}",groups=Assister.AssisterInfoGroup.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "PRIMARY_ASSISTER_SITE_ID")
	private Site primaryAssisterSite;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "SECONDARY_ASSISTER_SITE_ID")
	private Site secondaryAssisterSite;

	// bi-directional one-to-one association to Mailing Location
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(cascade = { CascadeType.ALL }) 
	@JoinColumn(name = "MAILING_LOCATION_ID")
	private EntityLocation mailingLocation;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;

	@Column(name = "STATUS_CHANGE_DATE")
	private Date statusChangeDate;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "DELEGATION_CODE")
	private String delegationCode;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "BUSINESS_LEGAL_NAME")
	private String businessLegalName;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "POSTAL_MAIL")
	private String postalMail;
	
	public String getPostalMail() {
		return postalMail;
	}

	public void setPostalMail(String postalMail) {
		this.postalMail = postalMail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EnrollmentEntity getEntity() {
		return entity;
	}
	
	public Integer getAssisterPhotoDocumentId() {
		return assisterPhotoDocumentId;
	}

	public void setAssisterPhotoDocumentId(Integer assisterPhotoDocumentId) {
		this.assisterPhotoDocumentId = assisterPhotoDocumentId;
	}
	
	public void setEntity(EnrollmentEntity entity) {
		this.entity = entity;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public String getCommunicationPreference() {
		return communicationPreference;
	}

	public void setCommunicationPreference(String communicationPreference) {
		this.communicationPreference = communicationPreference;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public Long getCertificationNumber() {
		return certificationNumber;
	}

	public void setCertificationNumber(Long certificationNumber) {
		this.certificationNumber = certificationNumber;
	}

	public Date getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(Date certificationDate) {
		this.certificationDate = certificationDate;
	}

	public Date getReCertificationDate() {
		return reCertificationDate;
	}

	public void setReCertificationDate(Date reCertificationDate) {
		this.reCertificationDate = reCertificationDate;
	}

	public Date getDeCertificationDate() {
		return deCertificationDate;
	}

	public void setDeCertificationDate(Date deCertificationDate) {
		this.deCertificationDate = deCertificationDate;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo1) {
		if (photo1 == null) {
			this.photo = new byte[0];
		} else {
			this.photo = Arrays.copyOf(photo1, photo1.length);
		}
	}

	public Long getAssisterNumber() {
		return assisterNumber;
	}

	public void setAssisterNumber(Long assisterNumber) {
		this.assisterNumber = assisterNumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public Site getPrimaryAssisterSite() {
		return primaryAssisterSite;
	}

	public void setPrimaryAssisterSite(Site primaryAssisterSite) {
		this.primaryAssisterSite = primaryAssisterSite;
	}

	public Site getSecondaryAssisterSite() {
		return secondaryAssisterSite;
	}

	public void setSecondaryAssisterSite(Site secondaryAssisterSite) {
		this.secondaryAssisterSite = secondaryAssisterSite;
	}

	public EntityLocation getMailingLocation() {
		return mailingLocation;
	}

	public void setMailingLocation(EntityLocation mailingLocation) {
		this.mailingLocation = mailingLocation;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getStatusChangeDate() {
		return statusChangeDate;
	}

	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}

	public String getDelegationCode() {
		return delegationCode;
	}

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}
	
	public String getActivityComments() {
		return activityComments;
	}

	public void setActivityComments(String activityComments) {
		this.activityComments = activityComments;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());

	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	@Override
	public String toString() {
		return "Assister Details:"+
		"assisterid= "+id+", "+
		"entityId= "+ (entity != null ? entity.getId() : "")+", "+
		"user ="+(user != null ? user.getId() : "");
	}

	/**
	 * @return the clientCount
	 */
	public String getClientCount() {
		return clientCount;
	}

	/**
	 * @param clientCount the clientCount to set
	 */
	public void setClientCount(String clientCount) {
		this.clientCount = clientCount;
	}

}
