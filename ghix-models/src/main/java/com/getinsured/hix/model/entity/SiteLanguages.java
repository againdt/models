package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for EE_SITE_LANGUAGES table, to save Languages
 * associated with Site.
 * 
 */
@Entity
@Table(name = "EE_SITE_LANGUAGES")
public class SiteLanguages implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_SITE_LANGUAGES_SEQ")
	@SequenceGenerator(name = "EE_SITE_LANGUAGES_SEQ", sequenceName = "EE_SITE_LANGUAGES_SEQ", allocationSize = 1)
	private int id;

	@OneToOne
	@JoinColumn(name = "EE_SITE_ID")
	private Site site;
	
	@NotEmpty(message="{label.validateSpokenLanguage}")
	@Column(name = "SPOKEN_LANGUAGES")
	private String spokenLanguages;

	@NotEmpty(message="{label.validateWrittenLanguage}")
	@Column(name = "WRITTEN_LANGUAGES")
	private String writtenLanguages;

	public SiteLanguages() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public String getSpokenLanguages() {
		return spokenLanguages;
	}

	public void setSpokenLanguages(String spokenLanguages) {
		this.spokenLanguages = spokenLanguages;
	}

	public String getWrittenLanguages() {
		return writtenLanguages;
	}

	public void setWrittenLanguages(String writtenLanguages) {
		this.writtenLanguages = writtenLanguages;
	}

	@Override
	public String toString() {
		return "SiteLanguages details: ID = "+id+", Site: ["+((site!=null)?("Site'ID = "+site.getId()):" ")+"]";
	}

}