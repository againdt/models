/**
 * 
 */
package com.getinsured.hix.model.prescreen;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * This is the model class for PRESCREEN_DATA table
 * @author Nikhil
 * @since 19 April, 2013
 */
@Entity
@Table(name="PRESCREEN_DATA")
@DynamicInsert
@DynamicUpdate
public class PrescreenData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4376872127315053279L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRESCREEN_DATA_SEQ")
	@SequenceGenerator(name = "PRESCREEN_DATA_SEQ", sequenceName = "PRESCREEN_DATA_SEQ", allocationSize = 1)
	private long id;
	
	@Column(name = "SESSION_ID")
	private String sessionId;
	
	@Column(name = "FIRST_NAME")
	private String name;
	
	@Column(name = "ZIPCODE")
	private String zipCode;
	
	@Column(name = "COUNTY")
	private String county;
	
	@Column(name = "IS_MARRIED")
	private String isMarried;
	
	@Column(name = "NO_OF_DEPENDENTS")
	private int noOfDependents;
	
	@Column(name = "HOUSEHOLD_INCOME")
	private double householdIncome;
	
	@Column(name = "CLAIMANT_DOB")
	private Date claimantDob;
	
	@Column(name = "IS_DISABLED")
	private String isDisabled;
	
	@Column(name = "ALIMONY_DEDUCTION")
	private double alimonyDeduction;
	
	@Column(name = "STUDENT_LOAN_DEDUCTION")
	private double studentLoanDeduction;
	
	@Column(name = "RESULTS")
	private String results;
	
	@Column(name = "RESULTS_TYPE")
	private String resultsType;
	
	@Column(name = "PREMIUM_VALUE")
	private double premiumValue;
	
	@Column(name = "FINAL_CREDIT_DISPLAYED")
	private double finalCreditDisplayed;
	
	@Column(name = "FPL")
	private double fpl;
	
	@Column(name = "CSR")
	private String csr;
	
	@Column(name = "FINAL_PLAN_SHOWED")
	private String finalPlanShowed;
	
	@Column(name = "BENCHMARK_STATUS")
	private String benchmarkStatus;
	
	@Column(name = "BENCHMARK_STATUS_DESC")
	private String benchmarkStatusDesc;
	
	@Column(name = "EMAIL_ADDRESS")
	private String email;
	
	@Column(name = "SESSION_DATA_CLOB")
	private String sessionDataClob;
	
	@Column(name = "SCREEN_VISITED")
	private int screenVisisted;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SESSION_START_TS")
	private Date sessionStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SESSION_END_TS")
	private Date sessionEndTime;
	
	@Column(name = "TOTAL_TIME_SPENT")
	private long totalTimeSpent;
	
	@Column(name = "INPUT_PAGE")
	private String inputPage;
	
	@Column(name="SOURCE")
	private String source; 
	
	@Column(name = "CREATED_BY")
	private long createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", updatable=false)
	private Date createdOn;

	@Column(name = "LAST_UPDATED_BY")
	private long updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;

	@Column(name ="STATE_CD")
	private String stateCd ;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getIsMarried() {
		return isMarried;
	}

	public void setIsMarried(String isMarried) {
		this.isMarried = isMarried;
	}

	public int getNoOfDependents() {
		return noOfDependents;
	}

	public void setNoOfDependents(int noOfDependents) {
		this.noOfDependents = noOfDependents;
	}

	public double getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(double householdIncome) {
		this.householdIncome = householdIncome;
	}


	public Date getClaimantDob() {
		return claimantDob;
	}

	public void setClaimantDob(Date claimantDob) {
		this.claimantDob = claimantDob;
	}

	public String getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(String isDisabled) {
		this.isDisabled = isDisabled;
	}

	public double getAlimonyDeduction() {
		return alimonyDeduction;
	}

	public void setAlimonyDeduction(double alimonyDeduction) {
		this.alimonyDeduction = alimonyDeduction;
	}

	public double getStudentLoanDeduction() {
		return studentLoanDeduction;
	}

	public void setStudentLoanDeduction(double studentLoanDeduction) {
		this.studentLoanDeduction = studentLoanDeduction;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

	public double getFinalCreditDisplayed() {
		return finalCreditDisplayed;
	}

	public void setFinalCreditDisplayed(double finalCreditDisplayed) {
		this.finalCreditDisplayed = finalCreditDisplayed;
	}

	public String getResultsType() {
		return resultsType;
	}

	public void setResultsType(String resultsType) {
		this.resultsType = resultsType;
	}

	public double getFpl() {
		return fpl;
	}

	public void setFpl(double fpl) {
		this.fpl = fpl;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFinalPlanShowed() {
		return finalPlanShowed;
	}

	public void setFinalPlanShowed(String finalPlanShowed) {
		this.finalPlanShowed = finalPlanShowed;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public double getPremiumValue() {
		return premiumValue;
	}

	public void setPremiumValue(double premiumValue) {
		this.premiumValue = premiumValue;
	}

	public String getBenchmarkStatus() {
		return benchmarkStatus;
	}

	public void setBenchmarkStatus(String benchmarkStatus) {
		this.benchmarkStatus = benchmarkStatus;
	}

	public String getBenchmarkStatusDesc() {
		return benchmarkStatusDesc;
	}

	public void setBenchmarkStatusDesc(String benchmarkStatusDesc) {
		this.benchmarkStatusDesc = benchmarkStatusDesc;
	}

	public String getSessionDataClob() {
		return sessionDataClob;
	}

	public void setSessionDataClob(String sessionDataClob) {
		this.sessionDataClob = sessionDataClob;
	}

	public int getScreenVisisted() {
		return screenVisisted;
	}

	public void setScreenVisisted(int screenVisisted) {
		this.screenVisisted = screenVisisted;
	}

	public Date getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(Date sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public Date getSessionEndTime() {
		return sessionEndTime;
	}

	public void setSessionEndTime(Date sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}

	public long getTotalTimeSpent() {
		return totalTimeSpent;
	}

	public void setTotalTimeSpent(long totalTimeSpent) {
		this.totalTimeSpent = totalTimeSpent;
	}

	public String getInputPage() {
		return inputPage;
	}

	public void setInputPage(String inputPage) {
		this.inputPage = inputPage;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	} 
	
	public String getStateCd() {
		return stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated date while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}

}
