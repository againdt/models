package com.getinsured.hix.dto.consumer;

import java.io.Serializable;

public class MedicareLeadPipeLineInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  reqEffDate;
	private Integer rank;
	
	public String getReqEffDate() {
		return reqEffDate;
	}
	public void setReqEffDate(String reqEffDate) {
		this.reqEffDate = reqEffDate;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	
}
