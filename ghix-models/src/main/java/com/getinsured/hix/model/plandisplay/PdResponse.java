package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.plandisplay.DrxPrescriptionSearchDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.IndividualPlan;

public class PdResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<IndividualPlan> individualPlanList;
	private String response;
	private java.lang.String nestedStackTrace;
	private List<Long> orderItemIds;
	private PdHouseholdDTO pdHouseholdDTO;
	private Long applicationId;
	private PdPreferencesDTO pdPreferencesDTO;
	private List<DrxPrescriptionSearchDTO> drxDrugList;
	private DrxPrescriptionSearchDTO drxPrescriptionSearchDTO;

	public List<Long> getOrderItemIds() {
		return orderItemIds;
	}

	public void setOrderItemIds(List<Long> orderItemIds) {
		this.orderItemIds = orderItemIds;
	}

	public List<IndividualPlan> getIndividualPlanList() {
		return individualPlanList;
	}

	public void setIndividualPlanList(List<IndividualPlan> individualPlanList) {
		this.individualPlanList = individualPlanList;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public java.lang.String getNestedStackTrace() {
		return nestedStackTrace;
	}

	public void setNestedStackTrace(java.lang.String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}

	public PdHouseholdDTO getPdHouseholdDTO() {
		return pdHouseholdDTO;
	}

	public void setPdHouseholdDTO(PdHouseholdDTO pdHouseholdDTO) {
		this.pdHouseholdDTO = pdHouseholdDTO;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}

	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}	

	public DrxPrescriptionSearchDTO getDrxPrescriptionSearchDTO() {
		return drxPrescriptionSearchDTO;
	}

	public void setDrxPrescriptionSearchDTO(DrxPrescriptionSearchDTO drxPrescriptionSearchDTO) {
		this.drxPrescriptionSearchDTO = drxPrescriptionSearchDTO;
	}	

	public List<DrxPrescriptionSearchDTO> getDrxDrugList() {
		return drxDrugList;
	}

	public void setDrxDrugList(List<DrxPrescriptionSearchDTO> drxDrugList) {
		this.drxDrugList = drxDrugList;
	}
	

}
