package com.getinsured.hix.dto.planmgmt.microservice;

public class UploadFileDTO {

	private String originalFileName;
	private Long fileSize;
	private byte[] fileContent;
	private String contentType;
	
	
	/**
	 * @return the originalFileName
	 */
	public String getOriginalFileName() {
		return originalFileName;
	}
	
	/**
	 * @param originalFileName the originalFileName to set
	 */
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
	
	/**
	 * @return the fileSize
	 */
	public Long getFileSize() {
		return fileSize;
	}
	
	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	
	/**
	 * @return the fileContent
	 */
	public byte[] getFileContent() {
		return fileContent;
	}
	
	/**
	 * @param fileContent the fileContent to set
	 */
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}
	
	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}
	
	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
		
}
