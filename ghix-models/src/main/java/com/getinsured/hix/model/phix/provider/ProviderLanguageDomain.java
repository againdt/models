package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_language_domain")
public class ProviderLanguageDomain  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1570054380590749753L;
	private Integer languageId;
	private String languageIdName;
	private Set<ProviderLanguage> providerLanguages = new HashSet<ProviderLanguage>(0);

	@Id 
	@Column(name="LANGUAGE_ID", unique=true, nullable=false)
	public Integer getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}


	@Column(name="LANGUAGE_ID_NAME", length=100)
	public String getLanguageIdName() {
		return this.languageIdName;
	}

	public void setLanguageIdName(String languageIdName) {
		this.languageIdName = languageIdName;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerLanguageDomain")
	public Set<ProviderLanguage> getProviderLanguages() {
		return this.providerLanguages;
	}

	public void setProviderLanguages(Set<ProviderLanguage> providerLanguages) {
		this.providerLanguages = providerLanguages;
	}

}


