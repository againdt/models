package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;

public class FFMPerson implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String email;	
	private String cityName;
	private String zipCode;
	private String stateCode;
	private String countyCode;
	private String streetName;
	private String homeIndicator;
	private String houseHoldContactEmail;
	private String houseHoldContactMobileNo;
	private String houseHoldContactMobileExt;
	private String houseHoldContactFirstName;
	private String houseHoldContactMiddleName;
	private String houseHoldContactLastName;
	private String houseHoldContactSuffix;
	private String houseHoldContactPersonID;
	
	public String getHomeIndicator() {
		return homeIndicator;
	}
	public void setHomeIndicator(String homeIndicator) {
		this.homeIndicator = homeIndicator;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getHouseHoldContactEmail() {
		return houseHoldContactEmail;
	}
	public void setHouseHoldContactEmail(String houseHoldContactEmail) {
		this.houseHoldContactEmail = houseHoldContactEmail;
	}
	public String getHouseHoldContactMobileNo() {
		return houseHoldContactMobileNo;
	}
	public void setHouseHoldContactMobileNo(String houseHoldContactMobileNo) {
		this.houseHoldContactMobileNo = houseHoldContactMobileNo;
	}
	public String getHouseHoldContactMobileExt() {
		return houseHoldContactMobileExt;
	}
	public void setHouseHoldContactMobileExt(String houseHoldContactMobileExt) {
		this.houseHoldContactMobileExt = houseHoldContactMobileExt;
	}
	public String getHouseHoldContactFirstName() {
		return houseHoldContactFirstName;
	}
	public void setHouseHoldContactFirstName(String houseHoldContactFirstName) {
		this.houseHoldContactFirstName = houseHoldContactFirstName;
	}
	public String getHouseHoldContactMiddleName() {
		return houseHoldContactMiddleName;
	}
	public void setHouseHoldContactMiddleName(String houseHoldContactMiddleName) {
		this.houseHoldContactMiddleName = houseHoldContactMiddleName;
	}
	public String getHouseHoldContactLastName() {
		return houseHoldContactLastName;
	}
	public void setHouseHoldContactLastName(String houseHoldContactLastName) {
		this.houseHoldContactLastName = houseHoldContactLastName;
	}
	public String getHouseHoldContactSuffix() {
		return houseHoldContactSuffix;
	}
	public void setHouseHoldContactSuffix(String houseHoldContactSuffix) {
		this.houseHoldContactSuffix = houseHoldContactSuffix;
	}
	public String getHouseHoldContactPersonID() {
		return houseHoldContactPersonID;
	}
	public void setHouseHoldContactPersonID(String houseHoldContactPersonID) {
		this.houseHoldContactPersonID = houseHoldContactPersonID;
	}	

}
