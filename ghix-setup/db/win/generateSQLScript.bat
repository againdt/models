@ECHO OFF
set /p outputPath="Enter SQL script Output Path : "
echo.

call java -jar ..\lib\ghix-liquibase.jar --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war --changeLogFile=META-INF/master.changelog.xml updateSQL > %outputPath%.sql
pause