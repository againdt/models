package com.getinsured.hix.model.emx;

import com.getinsured.hix.model.Employee.Status;


public class EmxEmployeeDTO {
	private int id;
	private String firstName;
	private String lastName;
	private String encryptedEmployeeId;
	
	public enum Type { EMPLOYEE, COBRA, RETIREE; }
	
	private Status status;
	private Type type;
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
	public String getEncryptedEmployeeId() {
		return encryptedEmployeeId;
	}

	public void setEncryptedEmployeeId(String encryptedEmployeeId) {
		this.encryptedEmployeeId = encryptedEmployeeId;
	}
}
