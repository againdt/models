package com.getinsured.hix.dto.shop.employer.eps;


public class EmployeePlanDTO implements Comparable <EmployeePlanDTO>{
	private Integer employeeId;
	private Float totalPremium=0f;
	private Float individualPremium=0f;
	private Float dependentPremium=0f;
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public Float getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(Float totalPremium) {
		this.totalPremium = totalPremium;
	}
	public Float getIndividualPremium() {
		return individualPremium;
	}
	public void setIndividualPremium(Float individualPremium) {
		this.individualPremium = individualPremium;
	}
	public Float getDependentPremium() {
		return dependentPremium;
	}
	public void setDependentPremium(Float dependentPremium) {
		this.dependentPremium = dependentPremium;
	}
	
	@Override
	public String toString() {
		String NewLine = System.getProperty( "line.separator" );
		String Indent  = "      ";
		StringBuilder strBuilder = new StringBuilder();
		
		strBuilder.append( "EmployeePlanDTO [ " +NewLine );
		strBuilder.append( Indent+"employeeId=" + employeeId +NewLine );
		strBuilder.append( Indent+"totalPremium=" + totalPremium +NewLine );
		strBuilder.append( Indent+"individualPremium=" + individualPremium +NewLine );
		strBuilder.append( Indent+"dependentPremium=" +dependentPremium+ " ] "+NewLine );
		return strBuilder.toString();
	
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((employeeId == null) ? 0 : employeeId.hashCode());
		result = prime
				* result
				+ ((individualPremium == null) ? 0 : individualPremium
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeePlanDTO other = (EmployeePlanDTO) obj;
		if (employeeId == null) {
			if (other.employeeId != null)
				return false;
		} else if (!employeeId.equals(other.employeeId))
			return false;
		if (individualPremium == null) {
			if (other.individualPremium != null)
				return false;
		} else if (!individualPremium.equals(other.individualPremium))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(EmployeePlanDTO o) {
		return individualPremium.compareTo(o.individualPremium);
	}
}