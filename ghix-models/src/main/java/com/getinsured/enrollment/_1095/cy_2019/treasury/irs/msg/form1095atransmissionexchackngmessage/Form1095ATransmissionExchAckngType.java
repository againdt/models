//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.10.15 at 01:56:56 PM PDT 
//


package com.getinsured.enrollment._1095.cy_2019.treasury.irs.msg.form1095atransmissionexchackngmessage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.Form1095AAckStatusGrpType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:msg:form1095atransmissionexchackngmessage" xmlns:airty19a="urn:us:gov:treasury:irs:ext:aca:air:ty19a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Transmission exchange acknowledgement type for 1095A Forms Upstream&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095ATransmissionExchAckngType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095ATransmissionExchAckngType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}Form1095AAckStatusGrp"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095ATransmissionExchAckngType", propOrder = {
    "form1095AAckStatusGrp"
})
public class Form1095ATransmissionExchAckngType {

    @XmlElement(name = "Form1095AAckStatusGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty19a", required = true)
    protected Form1095AAckStatusGrpType form1095AAckStatusGrp;

    /**
     * Gets the value of the form1095AAckStatusGrp property.
     * 
     * @return
     *     possible object is
     *     {@link Form1095AAckStatusGrpType }
     *     
     */
    public Form1095AAckStatusGrpType getForm1095AAckStatusGrp() {
        return form1095AAckStatusGrp;
    }

    /**
     * Sets the value of the form1095AAckStatusGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Form1095AAckStatusGrpType }
     *     
     */
    public void setForm1095AAckStatusGrp(Form1095AAckStatusGrpType value) {
        this.form1095AAckStatusGrp = value;
    }

}
