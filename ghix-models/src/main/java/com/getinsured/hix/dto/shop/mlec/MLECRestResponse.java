package com.getinsured.hix.dto.shop.mlec;

import java.util.Date;
import java.util.List;

public class MLECRestResponse {
	private List<Member> members;
	private PlanLevel tierName;
	private Integer employerId;
	private Date coverageStartDate;
	
	
	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public PlanLevel getTierName() {
		return tierName;
	}

	public void setTierName(PlanLevel tierName) {
		this.tierName = tierName;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}
	
	public static enum PlanLevel {
		PLATINUM, GOLD, SILVER, BRONZE;
	}
}
