package com.getinsured.hix.dto.plandisplay;

public class DrxDosageDTO {

	private String DosageID;
	private String ReferenceNDC;
	private String LabelName;
	private String CommonDaysOfSupply;
	private String GenericDosageID;
	private boolean IsCommonDosage = false;
	public String getDosageID() {
		return DosageID;
	}
	public void setDosageID(String dosageID) {
		DosageID = dosageID;
	}
	public String getReferenceNDC() {
		return ReferenceNDC;
	}
	public void setReferenceNDC(String referenceNDC) {
		ReferenceNDC = referenceNDC;
	}
	public String getLabelName() {
		return LabelName;
	}
	public String getCommonDaysOfSupply() {
		return CommonDaysOfSupply;
	}
	public void setCommonDaysOfSupply(String commonDaysOfSupply) {
		CommonDaysOfSupply = commonDaysOfSupply;
	}
	public void setLabelName(String labelName) {
		LabelName = labelName;
	}
	public String getGenericDosageID() {
		return GenericDosageID;
	}
	public void setGenericDosageID(String genericDosageID) {
		GenericDosageID = genericDosageID;
	}
	public boolean getIsCommonDosage() {
		return IsCommonDosage;
	}
	public void setIsCommonDosage(boolean isCommonDosage) {
		IsCommonDosage = isCommonDosage;
	}
}
