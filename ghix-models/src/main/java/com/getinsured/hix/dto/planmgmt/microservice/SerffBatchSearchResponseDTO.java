package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class SerffBatchSearchResponseDTO extends GhixResponseDTO {

	private Integer totalRecordCount;
	private Integer pageSize;
	private Integer currentRecordCount;
	private List<SerffBatchInfoDTO> serffBatchInfoDTOList;

	public SerffBatchSearchResponseDTO() {
		super();
	}

	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCurrentRecordCount() {
		return currentRecordCount;
	}

	public void setCurrentRecordCount(Integer currentRecordCount) {
		this.currentRecordCount = currentRecordCount;
	}

	public List<SerffBatchInfoDTO> getSerffBatchInfoDTOList() {
		return serffBatchInfoDTOList;
	}

	public void setSerffBatchInfoDTOList(List<SerffBatchInfoDTO> serffBatchInfoDTOList) {
		this.serffBatchInfoDTOList = serffBatchInfoDTOList;
	}

	public void setSerffBatchInfoDTO(SerffBatchInfoDTO serffBatchInfoDTO) {

		if (CollectionUtils.isEmpty(this.serffBatchInfoDTOList)) {
			this.serffBatchInfoDTOList = new ArrayList<SerffBatchInfoDTO>();
		}
		this.serffBatchInfoDTOList.add(serffBatchInfoDTO);
	}
}
