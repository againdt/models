package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.groups.Default;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * The persistent class for the employee applications  database table.
 */
@Audited 
@Entity
@Table(name="employee_applications")
@DynamicInsert
public class EmployeeApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum TypeOfEmployee 	{ FULLTIME, PARTTIME, SEASONAL; }

	public enum EmployeeBooleanFlag { YES, NO;	}
	
	public enum Status 	{ ENROLLED,  WAIVED, NOT_ENROLLED, CANCELED; 	}
	
	public interface EditDependentsGroup extends Default{
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_APPLICATIONS_SEQ")
	@SequenceGenerator(name = "EMPLOYEE_APPLICATIONS_SEQ", sequenceName = "EMPLOYEE_APPLICATIONS_SEQ", allocationSize = 1)
	private int id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)	
	@ManyToOne
	@JoinColumn(name="employer_id")
	private Employer employer;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)	
	@ManyToOne
	@JoinColumn(name="employer_enrollment_id")
	private EmployerEnrollment employerEnrollment;
	
	@Column(name="health_enrollment_id")
	private Integer healthEnrollmentId;
	
	@Column(name="dental_enrollment_id")
	private Integer dentalEnrollmentId;
	
	@Column(name="name")
	private String name;

	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name="status_notes")
	private String statusNotes;
	
	@Column(name="is_married")
	@Enumerated(EnumType.STRING)
	private EmployeeBooleanFlag isMarried;

	@Column(name="child_count")
	private int childCount;
	
	@Column(name="dependent_count")
	private int dependentCount;

	@Column(name="esign_by")
	private String esignBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="esign_date")
	private Date esignDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date created;

	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp")
	private Date updated;
	
	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;

	@ManyToOne
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="EMPLOYEE_COVERAGE_START_DATE")
	private Date employeeCoverageStartDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="EMPLOYEE_COVERAGE_END_DATE")
	private Date employeeCoverageEndDate;

	@NotAudited
	@Column(name = "last_visited")
	private String lastVisited;
	
	@Valid
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "employeeApplication", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EmployeeDetailsApplication> employeeDetailsApplication;
	
	public EmployeeApplication() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public EmployerEnrollment getEmployerEnrollment() {
		return employerEnrollment;
	}

	public void setEmployerEnrollment(EmployerEnrollment employerEnrollment) {
		this.employerEnrollment = employerEnrollment;
	}

	public Integer getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	public void setHealthEnrollmentId(Integer healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}

	public Integer getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(Integer dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getStatusNotes() {
		return statusNotes;
	}

	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}

	public EmployeeBooleanFlag getIsMarried() {
		return isMarried;
	}

	public void setIsMarried(EmployeeBooleanFlag isMarried) {
		this.isMarried = isMarried;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getDependentCount() {
		return dependentCount;
	}

	public void setDependentCount(int dependentCount) {
		this.dependentCount = dependentCount;
	}

	public String getEsignBy() {
		return esignBy;
	}

	public void setEsignBy(String esignBy) {
		this.esignBy = esignBy;
	}
	
	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}
	
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Employee getEmployee() {
		return employee;
	}
	
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public Date getEmployeeCoverageStartDate() {
		return employeeCoverageStartDate;
	}

	public void setEmployeeCoverageStartDate(Date employeeCoverageStartDate) {
		this.employeeCoverageStartDate = employeeCoverageStartDate;
	}
	
	public Date getEmployeeCoverageEndDate() {
		return employeeCoverageEndDate;
	}

	public void setEmployeeCoverageEndDate(Date employeeCoverageEndDate) {
		this.employeeCoverageEndDate = employeeCoverageEndDate;
	}

	public String getLastVisited() {
		return lastVisited;
	}

	public void setLastVisited(String lastVisited) {
		this.lastVisited = lastVisited;
	}

	public List<EmployeeDetailsApplication> getEmployeeDetailsApplication() {
		return employeeDetailsApplication;
	}

	public void setEmployeeDetailsApplication(
			List<EmployeeDetailsApplication> employeeDetailsApplication) {
		this.employeeDetailsApplication = employeeDetailsApplication;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate());
	}
	

}
