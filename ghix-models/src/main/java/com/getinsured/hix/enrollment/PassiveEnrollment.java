package com.getinsured.hix.enrollment;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name="PASSIVE_ENROLLMENT_DATA")
@DynamicInsert
@DynamicUpdate
public class PassiveEnrollment {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PASSIVE_ENROLLMENT_DATA_SEQ")
    @SequenceGenerator(name = "PASSIVE_ENROLLMENT_DATA_SEQ", sequenceName = "PASSIVE_ENROLLMENT_DATA_SEQ", allocationSize = 1)
    private Long id;
    
    @Column(name="BATCH_ID")
    private Long batchId;
    
    @Column(name="FIRST_NAME")
    private String firstName;

    @Column(name="MIDDLE_NAME")
    private String middleName;
    
    @Column(name="LAST_NAME")
    private String lastName;

    @Column(name="DOB")
    private String dob;

    @Column(name="GENDER")
    private String gender;

    @Column(name="SSN")
    private String ssn;

    @Column(name="SUBSCRIBER_INDICATOR")
    private String subscriberIndicator;
    
    @Column(name="RELATIONSHIP_TO_SUB_INDICATOR")
    private String relationToSubscriberInd;

    @Column(name="ISSUER_ASSIGNED_POLICY_ID")
    private String issuerAssignedPolicyId;
    
    @Column(name="RESIDENTIAL_STREET_ADDRESS")
    private String residentialStreetAddress1;

    @Column(name="RESIDENTIAL_STREET_ADDRESS2")
    private String residentialStreetAddress2;
    
    @Column(name="RESIDENTIAL_CITY")
    private String residentialCity;
    
    @Column(name="RESIDENTIAL_STATE")
    private String residentialState;
    
    @Column(name="RESIDENTIAL_ZIP_CODE")
    private String residentialZipCode;
    
    @Column(name="MAILING_STREET_ADDRESS")
    private String mailingStreetAddress1;
    
    @Column(name="MAILING_STREET_ADDRESS2")
    private String mailingStreetAddress2;
    
    @Column(name="MAILING_ADDRESS_CITY")
    private String mailingAddressCity;
    
    @Column(name="MAILING_ADDRESS_STATE")
    private String mailingAddressState;
    
    @Column(name="MAILING_ADDRESS_ZIP_CODE")
    private String mailingAddressZipCode;
    
    @Column(name="RESIDENTIAL_COUNTY_CODE")
    private String residentialCountyCode;
    
    @Column(name="TELEPHONE_NUMBER")
    private String telephoneNumber;
    
    @Column(name="TOBACCO_STATUS")
    private String tobaccoStatus;
    
    @Column(name="QHP_IDENTIFIER")
    private String qhpIdentifier;
    
    @Column(name="SSAP_APPLICATION_ID")
    private Long ssapApplicationId;
    
    @Column(name="RECORD_PROCESSING_STATUS")
    private String recordProcessingStatus;
    
    @Column(name="STATUS_REASON")
    private String statusReason;
    
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="CREATION_TIMESTAMP")
    private Date createdDate;
    
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATE_TIMESTAMP")
    private Date updatedDate;
    
    @Column(name="SUBSCRIBER_EMAIL_ADDRESS")
    private String subscriberEmailAddress; 

    @Transient
    private Integer personId;
    
    @Transient
    private List<String> errors = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getSubscriberIndicator() {
        return subscriberIndicator;
    }

    public void setSubscriberIndicator(String subscriberIndicator) {
        this.subscriberIndicator = subscriberIndicator;
    }

    public String getRelationToSubscriberInd() {
        return relationToSubscriberInd;
    }

    public void setRelationToSubscriberInd(String relationToSubscriberInd) {
        this.relationToSubscriberInd = relationToSubscriberInd;
    }

    public String getIssuerAssignedPolicyId() {
        return issuerAssignedPolicyId;
    }

    public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
        this.issuerAssignedPolicyId = issuerAssignedPolicyId;
    }

    public String getResidentialStreetAddress1() {
        return residentialStreetAddress1;
    }

    public void setResidentialStreetAddress1(String residentialStreetAddress1) {
        this.residentialStreetAddress1 = residentialStreetAddress1;
    }

    public String getResidentialStreetAddress2() {
        return residentialStreetAddress2;
    }

    public void setResidentialStreetAddress2(String residentialStreetAddress2) {
        this.residentialStreetAddress2 = residentialStreetAddress2;
    }

    public String getResidentialCity() {
        return residentialCity;
    }

    public void setResidentialCity(String residentialCity) {
        this.residentialCity = residentialCity;
    }

    public String getResidentialState() {
        return residentialState;
    }

    public void setResidentialState(String residentialState) {
        this.residentialState = residentialState;
    }

    public String getResidentialZipCode() {
        return residentialZipCode;
    }

    public void setResidentialZipCode(String residentialZipCode) {
        this.residentialZipCode = residentialZipCode;
    }

    public String getMailingStreetAddress1() {
        return mailingStreetAddress1;
    }

    public void setMailingStreetAddress1(String mailingStreetAddress1) {
        this.mailingStreetAddress1 = mailingStreetAddress1;
    }

    public String getMailingStreetAddress2() {
        return mailingStreetAddress2;
    }

    public void setMailingStreetAddress2(String mailingStreetAddress2) {
        this.mailingStreetAddress2 = mailingStreetAddress2;
    }

    public String getMailingAddressCity() {
        return mailingAddressCity;
    }

    public void setMailingAddressCity(String mailingAddressCity) {
        this.mailingAddressCity = mailingAddressCity;
    }

    public String getMailingAddressState() {
        return mailingAddressState;
    }

    public void setMailingAddressState(String mailingAddressState) {
        this.mailingAddressState = mailingAddressState;
    }

    public String getMailingAddressZipCode() {
        return mailingAddressZipCode;
    }

    public void setMailingAddressZipCode(String mailingAddressZipCode) {
        this.mailingAddressZipCode = mailingAddressZipCode;
    }

    public String getResidentialCountyCode() {
        return residentialCountyCode;
    }

    public void setResidentialCountyCode(String residentialCountyCode) {
        this.residentialCountyCode = residentialCountyCode;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTobaccoStatus() {
        return tobaccoStatus;
    }

    public void setTobaccoStatus(String tobaccoStatus) {
        this.tobaccoStatus = tobaccoStatus;
    }

    public String getQhpIdentifier() {
        return qhpIdentifier;
    }

    public void setQhpIdentifier(String qhpIdentifier) {
        this.qhpIdentifier = qhpIdentifier;
    }

	public Long getSsapApplicationId() {
        return ssapApplicationId;
    }

    public void setSsapApplicationId(Long ssapApplicationId) {
        this.ssapApplicationId = ssapApplicationId;
    }

    public String getRecordProcessingStatus() {
        return recordProcessingStatus;
    }

    public void setRecordProcessingStatus(String recordProcessingStatus) {
        this.recordProcessingStatus = recordProcessingStatus;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
    
    public String getSubscriberEmailAddress() {
		return subscriberEmailAddress;
	}

	public void setSubscriberEmailAddress(String subscriberEmailAddress) {
		this.subscriberEmailAddress = subscriberEmailAddress;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
    @PrePersist
    public void prePersist()
    {
        this.setCreatedDate(new TSDate());
    }

    /* To AutoUpdate updated dates while updating object */
    @PreUpdate
    public void preUpdate()
    {
        this.setUpdatedDate(new TSDate()); 
    }
    
}
