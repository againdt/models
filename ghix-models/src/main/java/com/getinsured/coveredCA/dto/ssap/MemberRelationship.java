
package com.getinsured.coveredCA.dto.ssap;

import java.util.Comparator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 *
 */
public class MemberRelationship {

    @Expose
    private String individualPersonId;
    @Expose
    private String relatedPersonId;
    @Expose
    private String relation;
    
    public String getIndividualPersonId() {
        return individualPersonId;
    }

    public void setIndividualPersonId(String individualPersonId) {
        this.individualPersonId = individualPersonId;
    }

    public String getRelatedPersonId() {
        return relatedPersonId;
    }

    public void setRelatedPersonId(String relatedPersonId) {
        this.relatedPersonId = relatedPersonId;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

   

    @Override
	public int hashCode() {
		return new HashCodeBuilder()
		.append(individualPersonId)
		.append(relatedPersonId)
		.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MemberRelationship){

			final MemberRelationship other = (MemberRelationship) obj;
			return new EqualsBuilder().append(individualPersonId, other.individualPersonId)
					.append(relatedPersonId, other.relatedPersonId)
					.isEquals();

		} else{
			return false;
		}
	}

	public static MemberRelationship build(String individualPersonId, String relatedPersonId){
		MemberRelationship br = new MemberRelationship();
		br.setIndividualPersonId(individualPersonId);
		br.setRelatedPersonId(relatedPersonId);
		return br;
	}
	
	/**
	 * 
	 * This comparator is used to sort members first by individualPersonId and
	 * then by RelatedPersonId , e.g (1,1),(1,2),(1,3),(2,3)
	 * 
	 * 
	 */

	public static final Comparator<MemberRelationship> MemberRelationshipComparator = new Comparator<MemberRelationship>(){

		@Override
		public int compare(MemberRelationship o1, MemberRelationship o2) {
            int compare = o1.getIndividualPersonId().compareTo(o2.individualPersonId);

			if(compare != 0){
				return compare;
			}

			compare = o1.getRelatedPersonId().compareTo(o2.getRelatedPersonId());

			return compare;
		}
	};

}
