
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;


/**
 * @author gjape
 * 
 */
public class HouseholdContact {

  
    @Expose
	private String phoneNumber;
    @Expose
    private String emailAddress;
    @Expose
    private ContactPreferences contactPreferences=new ContactPreferences();
    @Expose
    private HomeAddress homeAddress=new HomeAddress();
    @Expose
    private MailingAddress mailingAddress=new MailingAddress();
    @Expose
    private Boolean mailingAddressSameAsHomeAddressIndicator=true;
   


    public HomeAddress getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(HomeAddress homeAddress) {
        this.homeAddress = homeAddress;
    }

    
    public ContactPreferences getContactPreferences() {
        return contactPreferences;
    }

    public void setContactPreferences(ContactPreferences contactPreferences) {
        this.contactPreferences = contactPreferences;
    }

    public MailingAddress getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(MailingAddress mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public Boolean getMailingAddressSameAsHomeAddressIndicator() {
        return mailingAddressSameAsHomeAddressIndicator;
    }

    public void setMailingAddressSameAsHomeAddressIndicator(Boolean mailingAddressSameAsHomeAddressIndicator) {
        this.mailingAddressSameAsHomeAddressIndicator = mailingAddressSameAsHomeAddressIndicator;
    }

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

    
}
