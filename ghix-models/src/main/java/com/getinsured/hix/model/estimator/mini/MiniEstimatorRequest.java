/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.List;

import com.getinsured.hix.model.prescreen.calculator.GHIXRequest;

/**
 * Request Object for the Mini Estimator API
 * @author Nikhil Talreja
 * @since 10 July, 2013
 *
 */
public class MiniEstimatorRequest extends GHIXRequest {
	
	public enum EnrollmentYear {
		  CURRENT, NEXT
		};
	private String zipCode;
	
	private String countyCode;
	
	private int familySize;
	
	private double hhIncome = -1.0;
	
	private List<Member> members;
	
	private String docVisitFrequency;
	
	private String noOfPrescriptions;
	
	private String benefits;
	
	private long affiliateId;
	
	private Integer flowId;
	
	private String affiliateHouseholdId;
	
	private String email;
	
	private String phone;
	
	private String apiKey;

	private long leadId;
	
	private long clickId;
	
	private boolean skipAffiliateValidation;
	
	private String showPremiums="N";
	
	private boolean skipEligibility;
	
	private String effectiveStartDate;
	
	private EnrollmentYear enrollmentYear=EnrollmentYear.CURRENT;
	private int eventId;
	
	private boolean skipEligibilityCheck;
	private String isOkToCall;
	
	//HIX-64221 Support for 2016 Issuer Plan Preview
	private boolean planVerification;
	
	private int coverageYear=2016;
	
	private String firstName;
	private String lastName;

	private String csrLevel;

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	//HIX-86405 - data sharing consent should be default to false, if nothing passed.
	private boolean dataSharingConsent;
	
	public boolean getDataSharingConsent() {
		return dataSharingConsent;
	}
	
	public void setDataSharingConsent(boolean dataSharingConsent) {
		this.dataSharingConsent = dataSharingConsent;
	}
	
	public boolean isSkipEligibilityCheck() {
		return skipEligibilityCheck;
	}

	public void setSkipEligibilityCheck(boolean skipEligibilityCheck) {
		this.skipEligibilityCheck = skipEligibilityCheck;
	}

	public long getLeadId() {
		return leadId;
	}

	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public double getHhIncome() {
		return hhIncome;
	}

	public void setHhIncome(double hhIncome) {
		this.hhIncome = hhIncome;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public String getDocVisitFrequency() {
		return docVisitFrequency;
	}

	public void setDocVisitFrequency(String docVisitFrequency) {
		this.docVisitFrequency = docVisitFrequency;
	}

	public String getNoOfPrescriptions() {
		return noOfPrescriptions;
	}

	public void setNoOfPrescriptions(String noOfPrescriptions) {
		this.noOfPrescriptions = noOfPrescriptions;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getFlowId() {

		if(this.flowId == null)
		{
			return new Integer(0);
		}

		return flowId;
	}

	public void setFlowId(Integer flowId) {

		if(this.flowId == null)
		{
			this.flowId = new Integer(0);
		}

		this.flowId = flowId;
	}

	public String getAffiliateHouseholdId() {
		return affiliateHouseholdId;
	}

	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.affiliateHouseholdId = affiliateHouseholdId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public long getClickId() {
		return clickId;
	}

	public void setClickId(long clickId) {
		this.clickId = clickId;
	}

	public boolean isSkipAffiliateValidation() {
		return skipAffiliateValidation;
	}

	public void setSkipAffiliateValidation(boolean skipAffiliateValidation) {
		this.skipAffiliateValidation = skipAffiliateValidation;
	}

	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public EnrollmentYear getEnrollmentYear() {
		return enrollmentYear;
	}
	public void setEnrollmentYear(EnrollmentYear enrollmentYear) {
		this.enrollmentYear = enrollmentYear;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MiniEstimatorRequest [zipCode=");
		builder.append(zipCode);
		builder.append(", countyCode=");
		builder.append(countyCode);
		builder.append(", familySize=");
		builder.append(familySize);
		builder.append(", hhIncome=");
		builder.append(hhIncome);
		builder.append(", docVisitFrequency=");
		builder.append(docVisitFrequency);
		builder.append(", noOfPrescriptions=");
		builder.append(noOfPrescriptions);
		builder.append(", benefits=");
		builder.append(benefits);
		builder.append(", members=");
		builder.append(members);
		builder.append(", affiliateId=");
		builder.append(affiliateId);
		builder.append(", flowId=");
		builder.append(flowId);
		builder.append(", affiliateHouseholdId=");
		builder.append(affiliateHouseholdId);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", apiKey=");
		builder.append(apiKey);
		builder.append(", leadId=");
		builder.append(leadId);
		builder.append(", clickId=");
		builder.append(clickId);
		builder.append(", effectiveStartDate=");
		builder.append(effectiveStartDate);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		final int bits = 32;
		int result = 1;
		result = prime * result
				+ ((affiliateHouseholdId == null) ? 0 : affiliateHouseholdId
						.hashCode());
		result = prime * result + (int) (affiliateId ^ (affiliateId >>> bits));
		result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
		result = prime * result
				+ ((countyCode == null) ? 0 : countyCode.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + familySize;
		long temp;
		temp = Double.doubleToLongBits(hhIncome);
		result = prime * result + (int) (temp ^ (temp >>> bits));
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		MiniEstimatorRequest other = (MiniEstimatorRequest) obj;
		if (affiliateHouseholdId != other.affiliateHouseholdId){
			return false;
		}
		if (affiliateId != other.affiliateId){
			return false;
		}
		if (apiKey == null) {
			if (other.apiKey != null){
				return false;
			}
		} else if (!apiKey.equals(other.apiKey)){
			return false;
		}
		if (countyCode == null) {
			if (other.countyCode != null){
				return false;
			}
		} else if (!countyCode.equals(other.countyCode)){
			return false;
		}
		if (email == null) {
			if (other.email != null){
				return false;
			}
		} else if (!email.equals(other.email)){
			return false;
		}
		if (familySize != other.familySize){
			return false;
		}
		if (Double.doubleToLongBits(hhIncome) != Double
				.doubleToLongBits(other.hhIncome)){
			return false;
		}
		if (members == null) {
			if (other.members != null){
				return false;
			}
		} else if (!members.equals(other.members)){
			return false;
		}
		if (phone == null) {
			if (other.phone != null){
				return false;
			}
		} else if (!phone.equals(other.phone)){
			return false;
		}
		if (zipCode == null) {
			if (other.zipCode != null){
				return false;
			}
		} else if (!zipCode.equals(other.zipCode)){
			return false;
		}
		return true;
	}

	public String getShowPremiums() {
		return showPremiums;
	}

	public void setShowPremiums(String showPremiums) {
		this.showPremiums = showPremiums;
	}
	public boolean isSkipEligibility() {
		return skipEligibility;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public void setSkipEligibility(boolean skipEligibility) {
		this.skipEligibility = skipEligibility;
	}

	public String getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(String isOkToCall) {
		this.isOkToCall = isOkToCall;
	}
	
	public boolean isPlanVerification() {
		return planVerification;
	}

	public void setPlanVerification(boolean planVerification) {
		this.planVerification = planVerification;
	}

	/**
	 * @return the coverageYear
	 */
	public int getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}
	
	
}
