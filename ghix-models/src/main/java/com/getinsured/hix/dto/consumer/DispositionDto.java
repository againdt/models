package com.getinsured.hix.dto.consumer;

import java.io.Serializable;

public class DispositionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer dispositionCode;
	private Integer updatedBy;
	private Integer moduleId;
	private String moduleName;
	private String EffectiveYear;
	
	public Integer getDispositionCode() {
		return dispositionCode;
	}
	public void setDispositionCode(Integer dispositionCode) {
		this.dispositionCode = dispositionCode;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getEffectiveYear() {
		return EffectiveYear;
	}
	public void setEffectiveYear(String effectiveYear) {
		EffectiveYear = effectiveYear;
	}
}
