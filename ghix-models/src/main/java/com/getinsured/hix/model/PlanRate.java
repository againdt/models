package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrePersist;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
/**
 *
 * @author sharma_v
 *	The persistent class for the PM_Plan_Rate database table.
 */
@Audited
@SuppressWarnings("serial")
@Entity
@Table(name = "pm_plan_rate")
public class PlanRate implements Serializable{

	public enum RATE_OPTION {
		// A- Age Option, F-Family Option
		A, F;
	}
	
	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}

	public static enum Relation {
		APPLICANT, SPOUSE, CHILD;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanRate_Seq")
	@SequenceGenerator(name = "PlanRate_Seq", sequenceName = "pm_plan_rate_seq", allocationSize = 1)
	private int id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "plan_id")
	private Plan plan;

	@Column(name = "max_age")
	private int maxAge;

	@Column(name = "min_age")
	private int minAge;

	@Column(name = "tobacco", length = 30)
	private String tobacco;

	@Column(name = "rate")
	private Float rate;

	@Column(name = "relation")
	private String relation;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimestamp;


	@Temporal(value = TemporalType.DATE)
	@Column(name="effective_start_date",nullable=true)
	private Date effectiveStartDate;

	@Temporal(value = TemporalType.DATE)
	@Column(name="effective_end_date",nullable=true)
	private Date effectiveEndDate;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "rating_area_id")
	private RatingArea ratingArea;

	@Column(name = "rate_option")
	@Enumerated(EnumType.STRING)
	private RATE_OPTION rateOption;

	@Column(name = "is_deleted")
	//@Enumerated(EnumType.STRING)
	private String isDeleted;

	@Column(name ="gender")
	private String gender;

	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public String getTobacco() {
		return tobacco;
	}

	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public RatingArea getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(RatingArea ratingArea) {
		this.ratingArea = ratingArea;
	}

	public RATE_OPTION getRateOption() {
		return rateOption;
	}

	public void setRateOption(RATE_OPTION rateOption) {
		this.rateOption = rateOption;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public PlanRate clone() {
		PlanRate rateObj = new PlanRate();
		rateObj.setMaxAge(getMaxAge());
		rateObj.setMinAge(getMinAge());
		rateObj.setRate(getRate());
		rateObj.setTobacco(getTobacco());
		rateObj.setEffectiveEndDate(getEffectiveEndDate());
		rateObj.setEffectiveStartDate(getEffectiveStartDate());
		rateObj.setRatingArea(getRatingArea());
		rateObj.setRateOption(getRateOption());
		rateObj.setIsDeleted(getIsDeleted());
		return rateObj;
	}

	
	public String getIsDeleted() {
		return isDeleted;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
