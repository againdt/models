package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="ENROLLMENT_PLR_EXEC")
public class EnrollmentPLRExecution implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_PLR_EXEC_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_PLR_EXEC_SEQ", sequenceName = "ENROLLMENT_PLR_EXEC_SEQ", allocationSize = 1)
	private Integer id;
	
	//@OneToOne(fetch=FetchType.LAZY)
	@Column(name="ENROLLMENT_1095_ID")
	private Integer enrollment1095;

	@Column(name = "PLR_INBOUND_ACTION")
	private String plrInboundAction;
	
	@Column(name = "PLR_INBOUND_BCC")
	private String plrInboundBatchCategoryCode;
	
	@Column(name = "PLR_SKIPPED_FLAG")
	private String plrSkippedFlag;
	
	@Column(name = "PLR_SKIPPED_MSG")
	private String plrSkippedMsg;
	
	@Column(name = "PLR_XML_FILE_NAME")
	private String plrXmlFileName;
	
	@Column(name = "PLR_XML_GENERATION_TIMESTAMP")
	private Date plrXmlGeneratedOn;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEnrollment1095() {
		return enrollment1095;
	}

	public void setEnrollment1095(Integer enrollment1095) {
		this.enrollment1095 = enrollment1095;
	}

	public String getPlrInboundAction() {
		return plrInboundAction;
	}

	public void setPlrInboundAction(String plrInboundAction) {
		this.plrInboundAction = plrInboundAction;
	}

	public String getPlrInboundBatchCategoryCode() {
		return plrInboundBatchCategoryCode;
	}

	public void setPlrInboundBatchCategoryCode(String plrInboundBatchCategoryCode) {
		this.plrInboundBatchCategoryCode = plrInboundBatchCategoryCode;
	}

	public String getPlrSkippedFlag() {
		return plrSkippedFlag;
	}

	public void setPlrSkippedFlag(String plrSkippedFlag) {
		this.plrSkippedFlag = plrSkippedFlag;
	}

	public String getPlrSkippedMsg() {
		return plrSkippedMsg;
	}

	public void setPlrSkippedMsg(String plrSkippedMsg) {
		this.plrSkippedMsg = plrSkippedMsg;
	}

	public String getPlrXmlFileName() {
		return plrXmlFileName;
	}

	public void setPlrXmlFileName(String plrXmlFileName) {
		this.plrXmlFileName = plrXmlFileName;
	}

	public Date getPlrXmlGeneratedOn() {
		return plrXmlGeneratedOn;
	}

	public void setPlrXmlGeneratedOn(Date plrXmlGeneratedOn) {
		this.plrXmlGeneratedOn = plrXmlGeneratedOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}
}
