package com.getinsured.eligibility.enums;

/**
 * 
 * @author raguram_p
 *
 */

public enum ApplicantStatusEnum {
	NEW,UPDATED,/* TODO: Remove this later- No used */

	/* Financial LCE APPLICANT STATUSES - Starts */
	NO_CHANGE, ADD_NEW, DELETED,
	/**
	 * These are possible values for Financial LCE APPLICANT STATUSES flow. 
	 * Values are determined through automated comparison process when Referral is received.
	 * For Financial, LCE comparison is with enrolled members in previous enrollment.
	 * 
	 * 
	 * LCE APPLICANT STATUSES - when EVENTS are entered by consumer and consumer goes through plan selection
	 * ======================================================================================================
	 * ADD_NEW_ELIGIBILE - New member added and is eligible
	 * ADD_EXISTING_ELIGIBLE - Existing member is now eligible
	 * REMOVE_DELETED_INELIGIBLE - Member deleted (hard delete) from household
	 * REMOVE_NOTDELETED_INELIGIBLE - Member present in household but not eligible (soft delete)
	 * UPDATED_ZIP_COUNTY - Zip-County updated for primary address
	 * CHANGE_IN_ELIGIBILITY - Change in eligibility for a member, (example Previously member was APTC/CSR and now APTC)
	 * 
	 * 
	 * LCE APPLICANT STATUSES - when EVENTS are populated by SYSTEM & KEEP_ONLY set to YES  and consumer goes through plan selection
	 * ==============================================================================================================================
	 * UPDATED_DOB - DoB updated for a member
	 * 
	 * 
	 * LCE APPLICANT STATUSES - when EVENTS are populated by SYSTEM & and consumer DOESNOT goes through plan selection
	 * ================================================================================================================
	 * DEMO_FIRSTNAME, DEMO_MIDDLENAME, DEMO_LASTNAME, DEMO_SSN, DEMO_ADDRESS_LINE1, DEMO_ADDRESS_LINE2, DEMO_PRIMARY_ADDRESS, DEMO_MAILING_ADDRESS, DEMO_OTHER - To capture demo changes for an enrolled member
	 * DEMO_MULTIPLE - More than one demo changes reported for an enrolled member
	 * 
	 * 
	 * NOTE: We are not capturing APPLICANT STATUSES for newly added members who are not eligible.
	 */
	ADD_NEW_ELIGIBILE, ADD_EXISTING_ELIGIBLE, REMOVE_DELETED_INELIGIBLE, REMOVE_NOTDELETED_INELIGIBLE, UPDATED_ZIP_COUNTY, CHANGE_IN_ELIGIBILITY,
	UPDATED_DOB, CHANGE_IN_CSR_LEVEL, CHANGE_IN_APTC_AMOUNT, CHANGE_IN_CITIZENSHIP_STATUS, CHANGE_IN_BLOOD_RELATION,DEMO_NAMESUFFIX,DEMO_PTF,
	DEMO_FIRSTNAME, DEMO_MIDDLENAME, DEMO_LASTNAME, DEMO_SSN, DEMO_ADDRESS_LINE1, DEMO_ADDRESS_LINE2,DEMO_ADDRESS_CITY, DEMO_PRIMARY_ADDRESS, DEMO_MAILING_ADDRESS, DEMO_MULTIPLE, DEMO_OTHER, REMOVE_NOTDELETED_ONLY_QHP_ELIGIBLE,
	DEMO_MARITIAL_STATUS,DEMO_WRITTEN_LANGUAGE,DEMO_SPOKEN_LANGUAGE,
	/* Financial LCE APPLICANT STATUSES - Ends */

	QUALIFYING_EVENT;

	public String value() {
		return name();
	}

	public static ApplicantStatusEnum fromValue(String v) {
		ApplicantStatusEnum data = null;
		for (ApplicantStatusEnum c : ApplicantStatusEnum.class
				.getEnumConstants()) {
			if (c.value().equals(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	@Override
	public String toString() {
		return value();
	}

}


