package com.getinsured.hix.model.plandisplay;

import java.util.List;

public class PlanAvailabilityForDrugRequest {
	private String state;
	private List<String> drugNdcList;
	private List<String> drugRxCodeList;
	private List<String> planHiosList;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<String> getDrugNdcList() {
		return drugNdcList;
	}
	public void setDrugNdcList(List<String> drugNdcList) {
		this.drugNdcList = drugNdcList;
	}
	public List<String> getDrugRxCodeList() {
		return drugRxCodeList;
	}
	public void setDrugRxCodeList(List<String> drugRxCodeList) {
		this.drugRxCodeList = drugRxCodeList;
	}
	public List<String> getPlanHiosList() {
		return planHiosList;
	}
	public void setPlanHiosList(List<String> planHiosList) {
		this.planHiosList = planHiosList;
	}
	
}
