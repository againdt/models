package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

public class EnrolleePaymentDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4352472414920333455L;
	
	private String firstName;
	private String middleName;
	private String lastName;
	private Integer age;
	private String memberId;
	private String relationshipCode;
	private Float totalIndvResponsibilityAmt;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getRelationshipCode() {
		return relationshipCode;
	}
	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}
	public Float getTotalIndvResponsibilityAmt() {
		return totalIndvResponsibilityAmt;
	}
	public void setTotalIndvResponsibilityAmt(Float totalIndvResponsibilityAmt) {
		this.totalIndvResponsibilityAmt = totalIndvResponsibilityAmt;
	}
	
}
