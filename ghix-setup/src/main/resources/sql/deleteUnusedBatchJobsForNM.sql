
delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsDailyEnrollmentReportJob'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsDailyEnrollmentReportJob');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsDailyEnrollmentReportJob')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsDailyEnrollmentReportJob'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsDailyEnrollmentReportJob');
delete from BATCH_JOB_INSTANCE where job_name = 'cmsDailyEnrollmentReportJob';

delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'sendCarrierUpdatedEnrollmentJob'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'sendCarrierUpdatedEnrollmentJob');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'sendCarrierUpdatedEnrollmentJob')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'sendCarrierUpdatedEnrollmentJob'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'sendCarrierUpdatedEnrollmentJob');
delete from BATCH_JOB_INSTANCE where job_name = 'sendCarrierUpdatedEnrollmentJob';

delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentXMLIndividualJob'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentXMLIndividualJob');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentXMLIndividualJob')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentXMLIndividualJob'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentXMLIndividualJob');
delete from BATCH_JOB_INSTANCE where job_name = 'enrollmentXMLIndividualJob';

delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationEnrollmentReportJob'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationEnrollmentReportJob');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationEnrollmentReportJob')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationEnrollmentReportJob'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationEnrollmentReportJob');
delete from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationEnrollmentReportJob';

delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentPendingToCancel'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentPendingToCancel');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentPendingToCancel')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentPendingToCancel'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'enrollmentPendingToCancel');
delete from BATCH_JOB_INSTANCE where job_name = 'enrollmentPendingToCancel';

delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'individualEnrollmentReconciliationJob'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'individualEnrollmentReconciliationJob');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'individualEnrollmentReconciliationJob')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'individualEnrollmentReconciliationJob'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'individualEnrollmentReconciliationJob');
delete from BATCH_JOB_INSTANCE where job_name = 'individualEnrollmentReconciliationJob';

delete from BATCH_JOB_EXECUTION_CONTEXT where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationCancelTermEnrollmentJob'));
delete from BATCH_JOB_PARAMS where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationCancelTermEnrollmentJob');
delete from BATCH_STEP_EXECUTION_CONTEXT where STEP_EXECUTION_ID in (select STEP_EXECUTION_ID from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationCancelTermEnrollmentJob')));
delete from BATCH_STEP_EXECUTION where JOB_EXECUTION_ID in (select JOB_EXECUTION_ID from batch_job_execution  where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationCancelTermEnrollmentJob'));
delete from BATCH_JOB_EXECUTION where job_instance_id in (select job_instance_id from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationCancelTermEnrollmentJob');
delete from BATCH_JOB_INSTANCE where job_name = 'cmsReconciliationCancelTermEnrollmentJob';









