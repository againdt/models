
package com.serff.planmanagementexchangeapi.exchange.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.serff.planmanagementexchangeapi.common.model.pm.ValidationErrors;


/**
 * <p>Java class for TransferPlanResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferPlanResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validationErrors" type="{http://www.serff.com/planManagementExchangeApi/common/model/pm}ValidationErrors"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validationErrors"
})
@XmlRootElement(name="transferPlanResponse", namespace="http://www.serff.com/planManagementExchangeApi/exchange/model/service")
public class TransferPlanResponse {

	public static final String XML_ROOT_ELEMENT = "transferPlanResponse";
	
    @XmlElement(required = true, nillable = true, namespace="http://www.serff.com/planManagementExchangeApi/exchange/model/service")
    protected ValidationErrors validationErrors;

    /**
     * Gets the value of the validationErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationErrors }
     *     
     */
    public ValidationErrors getValidationErrors() {
        return validationErrors;
    }

    /**
     * Sets the value of the validationErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationErrors }
     *     
     */
    public void setValidationErrors(ValidationErrors value) {
        this.validationErrors = value;
    }

}
