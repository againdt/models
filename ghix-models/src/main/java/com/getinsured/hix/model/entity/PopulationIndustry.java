package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * The persistent class for table EE_POPULATION_SERVED, to save entity's
 * population served information.
 * 
 */
@Entity
@Table(name = "EE_POPULATION_INDUSTRIES")
public class PopulationIndustry implements Serializable, PopulationData {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_POPULATION_INDUSTRIES_SEQ")
	@SequenceGenerator(name = "EE_POPULATION_INDUSTRIES_SEQ", sequenceName = "EE_POPULATION_INDUSTRIES_SEQ", allocationSize = 1)
	private int id;

	@OneToOne
	@JoinColumn(name = "EE_ENTITY_ID")
	private EnrollmentEntity entity;

	@Size(max=50)
	@Column(name = "INDUSTRY")
	private String industry;

	@Max(100)
	@Min(1)
	@Column(name = "INDUSTRY_PERCENTAGE")
	private Long industryPercentage;

	@Transient
	private String value;
	
	public PopulationIndustry() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EnrollmentEntity getEntity() {
		return entity;
	}

	public void setEntity(EnrollmentEntity entity) {
		this.entity = entity;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
		this.value = industry;
	}

	public Long getIndustryPercentage() {
		return industryPercentage;
	}

	public void setIndustryPercentage(Long industryPercentage) {
		this.industryPercentage = industryPercentage;
	}

	@Override
	public String toString() {
		return "PopulationIndustry details: ID = "+id+", " +
				"EnrollmentEntity: "+((entity!=null)?("EntityID = "+entity.getId()):"");
	}

	@Override
	public String getValue() {
		return getIndustry();
	}
	
	public void setValue(String value) {
		this.value = value;
		this.industry= value;
	}

}