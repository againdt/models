package com.getinsured.hix.dto.cap.consumerapp;

public class AjaxResponse {
	private boolean status;
	private String message;
	private Object data;
	
	public AjaxResponse(){}	

	
	public AjaxResponse(boolean status, String message)
	{
		this.status = status;
		this.message = message;
	}
	
	public AjaxResponse(boolean status, String message, Object data)
	{
		this.status = status;
		this.message = message;
		this.data = data;
	}
	

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("[Status:%s]", status));
		sb.append(String.format("[Message:%s]", message));
		return String.format("[%s]", sb.toString());
	}
	
	
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

}
