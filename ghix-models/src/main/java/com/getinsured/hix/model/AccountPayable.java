package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

import java.util.Date;

/**
 * The persistent class for the ACCOUNT_PAYABLE oracle view.
 * @author sharma_k
 */

@Entity
@Table(name = "account_payable")
public class AccountPayable implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "issuer_id")
	private int issuerId;
	
	@Column(name = "issuer_name")
	private String issuerName;
	
	@Column(name= "invoice_number")
	private String invoiceNumber;
	
	@Column(name="statement_date")
	private Date statementDate;
	
	@Column(name = "total_amount_due")
	private float totalAmountDue;
	
	@Column(name = "paid_amount")
	private float paidAmount;
	
	@Column(name = "paid_status")
	private String paidStatus;
	
	@Column(name = "balance_amount")
	private float balanceAmount;
	
	
	public int getId()
	{
		return id;
	}
	
	public int getIssuerId()
	{
		return issuerId;
	}
	
	public String getIssuerName()
	{
		return this.issuerName;
	}
	
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}
	
	public Date getStatementDate()
	{
		return statementDate;
	}
	
	public float getTotalAmountDue()
	{
		return totalAmountDue;
	}
	
	public float getPaidAmount()
	{
		return paidAmount;
	}
	
	public float getBalanceAmount()
	{
		return balanceAmount;
	}

	public String getPaidStatus() {
		return paidStatus;
	}
	
	
}


