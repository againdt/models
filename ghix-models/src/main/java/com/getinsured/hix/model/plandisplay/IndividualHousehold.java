package com.getinsured.hix.model.plandisplay;

import java.util.ArrayList;
import java.util.Map;


public class IndividualHousehold{
	private String sadpFlag;
	private long externalHouseholdId;
	private Map<String,String> responsiblePerson;
	private Map<String,String> householdContact;
	private Map<String,String> individual;
	private ArrayList<Map<String,String>>  members;
	private Map<String,String> shop;
	private String userRoleId;
	private String userRoleType;
	private String subsidyFlag;
	
	public String getSubsidyFlag() {
		return subsidyFlag;
	}
	public void setSubsidyFlag(String subsidyFlag) {
		this.subsidyFlag = subsidyFlag;
	}
	public String getSadpFlag() {
		return sadpFlag;
	}
	public void setSadpFlag(String sadpFlag) {
		this.sadpFlag = sadpFlag;
	}
	public String getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getUserRoleType() {
		return userRoleType;
	}
	public void setUserRoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}
	public long getExternalHouseholdId() {
		return externalHouseholdId;
	}
	public void setExternalHouseholdId(long externalHouseholdId) {
		this.externalHouseholdId = externalHouseholdId;
	}
	public Map<String, String> getResponsiblePerson() {
		return responsiblePerson;
	}
	public void setResponsiblePerson(Map<String, String> responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}
	public Map<String, String> getHouseholdContact() {
		return householdContact;
	}
	public void setHouseholdContact(Map<String, String> householdContact) {
		this.householdContact = householdContact;
	}
	public Map<String, String> getIndividual() {
		return individual;
	}
	public void setIndividual(Map<String, String> individual) {
		this.individual = individual;
	}
	public ArrayList<Map<String, String>> getMembers() {
		return members;
	}
	public void setMembers(ArrayList<Map<String, String>> members) {
		this.members = members;
	}
	public Map<String, String> getShop() {
		return shop;
	}
	public void setShop(Map<String, String> shop) {
		this.shop = shop;
	}
	
	
}

