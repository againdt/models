/**
 * 
 */
package com.getinsured.planmgmt.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.getinsured.hix.platform.util.GhixConstants;

/**
 * @author gorai_k
 *
 */
public class GreaterThanCurrentDateValidator implements ConstraintValidator<GreaterThanCurrentDate, Date>{
	private static final Logger LOGGER = LoggerFactory.getLogger(GreaterThanCurrentDateValidator.class);
	public void initialize(GreaterThanCurrentDate object) {}
	
	@Override
	public boolean isValid(Date value, ConstraintValidatorContext validatorContext) {
	if(value != null)
	{
		Date date;
		SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
			Date currentDate = new TSDate();
			if(value.after(currentDate)){
				return true;
			}
		
	return false;
	}
	else
	{
	return true;
	}
	}
}
