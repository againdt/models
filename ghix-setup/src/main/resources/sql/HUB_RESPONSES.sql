-- 
-- TABLE: HUB_RESPONSES 
--

CREATE TABLE HUB_RESPONSES(
    GI_WS_PAYLOAD_ID          NUMBER           NOT NULL,
    HUB_CASE_NUMBER           VARCHAR2(256),
    HUB_SESSION_IDENTIFIER    VARCHAR2(256),
    HUB_RESPONSE_PAYLOAD      CLOB,
    HUB_RESPONSE_CODE         VARCHAR2(256),
    ELAPSE_TIME               NUMBER,
    SSAP_APPLICANT_ID         NUMBER,
    CONSTRAINT PK_HUB_RESPONSES PRIMARY KEY (GI_WS_PAYLOAD_ID)
)
;



COMMENT ON TABLE HUB_RESPONSES IS 'This is to store the HUB response data.  It will be on its own schema, its own data source, its own tablespace.  It will be separated from the user provided data.'
;
COMMENT ON COLUMN HUB_RESPONSES.GI_WS_PAYLOAD_ID IS 'The primary key.  It is from the GI_WS_PAYLOAD table in the schema for the user provided data'
;
COMMENT ON COLUMN HUB_RESPONSES.HUB_CASE_NUMBER IS 'Hub Case Number.  It is from the response of the Hub.'
;
COMMENT ON COLUMN HUB_RESPONSES.HUB_SESSION_IDENTIFIER IS 'Hub session identifier from the HUB'
;
COMMENT ON COLUMN HUB_RESPONSES.HUB_RESPONSE_PAYLOAD IS 'The HUB response in XML/JSON format'
;
COMMENT ON COLUMN HUB_RESPONSES.HUB_RESPONSE_CODE IS 'Hub response code from the HUB'
;
COMMENT ON COLUMN HUB_RESPONSES.ELAPSE_TIME IS 'The time it takes to have the HUB response to the request.'
;
COMMENT ON COLUMN HUB_RESPONSES.SSAP_APPLICANT_ID IS 'SSAP Applicant ID.  Foreign key from SSAP_APPLICANTS table.'
;
-- 
-- INDEX: HUB_RESPONSE_2_IND1_IND1_IND 
--

CREATE INDEX HUB_RESPONSE_2_IND1_IND1_IND ON HUB_RESPONSES(SSAP_APPLICANT_ID)
;
