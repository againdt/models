package com.getinsured.hix.dto.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Jira ID: HIX-55931 Consolidated Employer Payment Past Due notice for Agents
 * @since 
 * @author Sharma_k
 *
 */
public class EmployersDueInvoiceDTO implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String employerName;
	private String firstNameNLastName;
	private String emailID;
	
	private String phoneNumber;
	private Date dueDate;
	private BigDecimal totalAmountDue;
	
	
	/**
	 * @return the employerName
	 */
	public String getEmployerName() {
		return employerName;
	}
	/**
	 * @param employerName the employerName to set
	 */
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	/**
	 * @return the firstNameNLastName
	 */
	public String getFirstNameNLastName() {
		return firstNameNLastName;
	}
	/**
	 * @param firstNameNLastName the firstNameNLastName to set
	 */
	public void setFirstNameNLastName(String firstNameNLastName) {
		this.firstNameNLastName = firstNameNLastName;
	}
	/**
	 * @return the emailID
	 */
	public String getEmailID() {
		return emailID;
	}
	/**
	 * @param emailID the emailID to set
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}
	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	/**
	 * @return the totalAmountDue
	 */
	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}
	/**
	 * @param totalAmountDue the totalAmountDue to set
	 */
	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}
}
