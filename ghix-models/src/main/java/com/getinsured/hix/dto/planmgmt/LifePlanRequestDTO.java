/**
 * 
 * @author santanu
 * @version 1.0
 * @since May 02, 2016 
 * 
 * This DTO to accept request object for Life plan
 */

package com.getinsured.hix.dto.planmgmt;


public class LifePlanRequestDTO {

	private String effectiveDate;
	private Member memberInfo; // member data
	private String tenantCode;
	private String stateCode;
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	/**
	 * @return the memberInfo
	 */
	public Member getMemberInfo() {
		return memberInfo;
	}
	/**
	 * @param memberInfo the memberInfo to set
	 */
	public void setMemberInfo(Member memberInfo) {
		this.memberInfo = memberInfo;
	}
	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}
	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
}
