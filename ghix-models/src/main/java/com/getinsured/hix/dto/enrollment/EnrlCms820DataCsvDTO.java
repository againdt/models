/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @author negi_s
 *
 */
public class EnrlCms820DataCsvDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String hiosIssuerId;
	private String issuerAptcTotal;
	private String issuerCsrTotal;
	private String issuerUfTotal;
	private String lastName;
	private String firstName;
	private String middleName;
	private String namePrefix;
	private String nameSuffix;
	private String exchangeAssignedSubId;
	private String exchangeAssignedQhpId;
	private String exchangeAssignedPolicyId;
	private String issuerAssignedPolicyId;
	private String issuerAssignedSubId;
	private String policyTotalPremiumAmount;
	private String exchangePaymentType;
	private String paymentAmount;
	private String exchangeRelatedReportType;
	private String exchangeReportDocCntrNum;
	private String coveragePeriodStartDate;
	private String coveragePeriodEndDate;

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIssuerAptcTotal() {
		return issuerAptcTotal;
	}

	public void setIssuerAptcTotal(String issuerAptcTotal) {
		this.issuerAptcTotal = issuerAptcTotal;
	}

	public String getIssuerCsrTotal() {
		return issuerCsrTotal;
	}

	public void setIssuerCsrTotal(String issuerCsrTotal) {
		this.issuerCsrTotal = issuerCsrTotal;
	}

	public String getIssuerUfTotal() {
		return issuerUfTotal;
	}

	public void setIssuerUfTotal(String issuerUfTotal) {
		this.issuerUfTotal = issuerUfTotal;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getNamePrefix() {
		return namePrefix;
	}

	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}

	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}

	public String getExchangeAssignedSubId() {
		return exchangeAssignedSubId;
	}

	public void setExchangeAssignedSubId(String exchangeAssignedSubId) {
		this.exchangeAssignedSubId = exchangeAssignedSubId;
	}

	public String getExchangeAssignedQhpId() {
		return exchangeAssignedQhpId;
	}

	public void setExchangeAssignedQhpId(String exchangeAssignedQhpId) {
		this.exchangeAssignedQhpId = exchangeAssignedQhpId;
	}

	public String getExchangeAssignedPolicyId() {
		return exchangeAssignedPolicyId;
	}

	public void setExchangeAssignedPolicyId(String exchangeAssignedPolicyId) {
		this.exchangeAssignedPolicyId = exchangeAssignedPolicyId;
	}

	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}

	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}

	public String getIssuerAssignedSubId() {
		return issuerAssignedSubId;
	}

	public void setIssuerAssignedSubId(String issuerAssignedSubId) {
		this.issuerAssignedSubId = issuerAssignedSubId;
	}

	public String getPolicyTotalPremiumAmount() {
		return policyTotalPremiumAmount;
	}

	public void setPolicyTotalPremiumAmount(String policyTotalPremiumAmount) {
		this.policyTotalPremiumAmount = policyTotalPremiumAmount;
	}

	public String getExchangePaymentType() {
		return exchangePaymentType;
	}

	public void setExchangePaymentType(String exchangePaymentType) {
		this.exchangePaymentType = exchangePaymentType;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getExchangeRelatedReportType() {
		return exchangeRelatedReportType;
	}

	public void setExchangeRelatedReportType(String exchangeRelatedReportType) {
		this.exchangeRelatedReportType = exchangeRelatedReportType;
	}

	public String getExchangeReportDocCntrNum() {
		return exchangeReportDocCntrNum;
	}

	public void setExchangeReportDocCntrNum(String exchangeReportDocCntrNum) {
		this.exchangeReportDocCntrNum = exchangeReportDocCntrNum;
	}

	public String getCoveragePeriodStartDate() {
		return coveragePeriodStartDate;
	}

	public void setCoveragePeriodStartDate(String coveragePeriodStartDate) {
		this.coveragePeriodStartDate = coveragePeriodStartDate;
	}

	public String getCoveragePeriodEndDate() {
		return coveragePeriodEndDate;
	}

	public void setCoveragePeriodEndDate(String coveragePeriodEndDate) {
		this.coveragePeriodEndDate = coveragePeriodEndDate;
	}

	@Override
	public String toString() {
		return "EnrlCms820DataCsvDTO [hiosIssuerId=" + hiosIssuerId + ", issuerAptcTotal=" + issuerAptcTotal
				+ ", issuerCsrTotal=" + issuerCsrTotal + ", issuerUfTotal=" + issuerUfTotal + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", middleName=" + middleName + ", namePrefix=" + namePrefix
				+ ", nameSuffix=" + nameSuffix + ", exchangeAssignedSubId=" + exchangeAssignedSubId
				+ ", exchangeAssignedQhpId=" + exchangeAssignedQhpId + ", exchangeAssignedPolicyId="
				+ exchangeAssignedPolicyId + ", issuerAssignedPolicyId=" + issuerAssignedPolicyId
				+ ", issuerAssignedSubId=" + issuerAssignedSubId + ", policyTotalPremiumAmount="
				+ policyTotalPremiumAmount + ", exchangePaymentType=" + exchangePaymentType + ", paymentAmount="
				+ paymentAmount + ", exchangeRelatedReportType=" + exchangeRelatedReportType
				+ ", exchangeReportDocCntrNum=" + exchangeReportDocCntrNum + ", coveragePeriodStartDate="
				+ coveragePeriodStartDate + ", coveragePeriodEndDate=" + coveragePeriodEndDate + "]";
	}
	
	public String toCsv(char separator) {
		String csv = hiosIssuerId + separator +
				issuerAptcTotal + separator +
				issuerCsrTotal + separator +
				issuerUfTotal + separator +
				lastName + separator +
				firstName + separator +
				middleName + separator +
				namePrefix + separator +
				nameSuffix + separator +
				exchangeAssignedSubId + separator +
				exchangeAssignedQhpId + separator +
				exchangeAssignedPolicyId + separator +
				issuerAssignedPolicyId + separator +
				issuerAssignedSubId + separator +
				policyTotalPremiumAmount + separator +
				exchangePaymentType + separator +
				paymentAmount + separator +
				exchangeRelatedReportType + separator +
				exchangeReportDocCntrNum + separator +
				coveragePeriodStartDate + separator +
				coveragePeriodEndDate;
		return csv.replaceAll("null", "");
	}
}
