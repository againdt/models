package com.getinsured.hix.model.estimator.mini;

import com.getinsured.hix.model.GHIXRequest;
import com.getinsured.hix.model.estimator.mini.EligLead.ExchangeFlowType;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.estimator.mini.EligLead.STATUS;


/**
 * Request Object for updaing ELIG_LEAD table in the Mini Estimator API
 * @author Sunil Desu, Nikhil Talreja
 * @since 14 August, 2013
 *
 */
public class EligLeadRequest extends GHIXRequest{
	
	
	private long id;
	
	private STATUS status; 
	
	private STAGE stage;
	
	private ExchangeFlowType exchangeFlowType;
	
	private String docVisitFrequency;
	
	private String noOfPrescriptions;
	
	private String benefits;
	
	private String name;
	
	private String email;
	
	private String phone;
	
	private String isOkToCall="N";
	
	private int coverageYear;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public STAGE getStage() {
		return stage;
	}

	public void setStage(STAGE stage) {
		this.stage = stage;
	}

	public ExchangeFlowType getExchangeFlowType() {
		return exchangeFlowType;
	}

	public void setExchangeFlowType(ExchangeFlowType exchangeFlowType) {
		this.exchangeFlowType = exchangeFlowType;
	}

	public String getDocVisitFrequency() {
		return docVisitFrequency;
	}

	public void setDocVisitFrequency(String docVisitFrequency) {
		this.docVisitFrequency = docVisitFrequency;
	}

	public String getNoOfPrescriptions() {
		return noOfPrescriptions;
	}

	public void setNoOfPrescriptions(String noOfPrescriptions) {
		this.noOfPrescriptions = noOfPrescriptions;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(String isOkToCall) {
		this.isOkToCall = isOkToCall;
	}

	public int getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}

	
	
}
