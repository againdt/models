insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 1'),
            'Benewah','16009',2018);            
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 1'),
            'Bonner','16017',2018);            
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 1'),
            'Boundary','16021',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 1'),
            'Kootenai','16055',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 1'),
            'Shoshone','16079',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 2'),
            'Clearwater','16053',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 2'),
            'Idaho','16049',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 2'),
            'Latah','16057',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 2'),
            'Lewis','16061',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 2'),
            'Nez Perce','16069',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Ada','16001',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Adams','16003',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Boise','16015',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Canyon','16027',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Elmore','16039',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Gem','16045',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Owyhee','16073',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Payette','16075',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Valley','16085',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 3'),
            'Washington','16087',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Bonneville','16019',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Butte','16023',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Clark','16033',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Custer','16037',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Fremont','16043',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Jefferson','16051',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Lemhi','16059',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Madison','16065',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 4'),
            'Teton','16081',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Blaine','16013',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Camas','16025',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Cassia','16031',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Gooding','16047',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Jerome','16053',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Lincoln','16063',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Minidoka','16067',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 5'),
            'Twin Falls','16083',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Bannock','16005',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Bear Lake','16007',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Bingham','16011',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Caribou','16029',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Franklin','16041',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Oneida','16071',2018);
insert into PM_ZIP_COUNTY_RATING_AREA (ID, RATING_AREA_ID, COUNTY, COUNTY_FIPS, APPLICABLE_YEAR) 
    values (nextval('PM_ZIP_COUNTY_RATING_AREA_SEQ'), 
            (select id from pm_rating_area where state = 'ID' and rating_area = 'Rating Area 6'),
            'Power','16077',2018);
