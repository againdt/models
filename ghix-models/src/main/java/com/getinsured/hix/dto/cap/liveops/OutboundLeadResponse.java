package com.getinsured.hix.dto.cap.liveops;

import com.getinsured.hix.dto.cap.liveops.ReturnObject;

public class OutboundLeadResponse {
	ReturnObject returnObject;
	String request_id; // " : "dba6feee-5317-4769-a9a3-4bc24b99dce4",
    String status; // 200 ,400, 401 ,500 etc
    String error_description;//  
	public ReturnObject getReturnObject() {
		return returnObject;
	}

	public String getError_description() {
		return error_description;
	}

	public void setError_description(String error_description) {
		this.error_description = error_description;
	}

	public void setReturnObject(ReturnObject returnObject) {
		this.returnObject = returnObject;
	}

	public String getRequest_id() {
		return request_id;
	}

	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
