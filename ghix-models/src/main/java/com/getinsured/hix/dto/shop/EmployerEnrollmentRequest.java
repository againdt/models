package com.getinsured.hix.dto.shop;

import java.util.Date;

import com.getinsured.hix.model.EmployerEnrollment.Status;

public class EmployerEnrollmentRequest {

	int employerId;
	private Status status;
	Date coverageDate;
	
	public int getEmployerId() {
		return employerId;
	}
	public void setEmployerId(int employerId) {
		this.employerId = employerId;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Date getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(Date coverageDate) {
		this.coverageDate = coverageDate;
	}
}
