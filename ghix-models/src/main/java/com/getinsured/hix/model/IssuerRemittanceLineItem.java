package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * The persistent class for the ISSUER_INVOICES_LINEITEMS database table.
 * @author sharma_k
 */

@Audited
@Entity
@Table(name = "issuer_remittance_lineitem")
public class IssuerRemittanceLineItem implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "iss_remittance_lineitems_seq")
	@SequenceGenerator(name = "iss_remittance_lineitems_seq", sequenceName = "iss_remittance_lineitems_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "employer_id")
	private Employer employer;
	
	
	@Column(name = "total_premium")
	private BigDecimal totalPremium;
	
	@Column(name = "net_amount")
	private BigDecimal netAmount;
	
	@Column(name = "amount_received_from_employer")
	private BigDecimal amountReceivedFromEmployer;
	
	@ManyToOne
    @JoinColumn(name="remittance_id")
	private IssuerRemittance issuerRemittance;
	
	@Column(name="retro_adjustments")
	private BigDecimal retroAdjustments;
	
	@Column(name="user_fee")
	private BigDecimal userFee;
	
	@Column(name="amount_paid_to_issuer")
	private BigDecimal amtPaidToIssuer;
	
	@OneToOne
	@JoinColumn(name="emp_inv_lineitems_id")
	private EmployerInvoiceLineItems employerInvoiceLineItems; 
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="enrollment_id")*/
	@Column(name ="enrollment_id")
	private Integer enrollmentId;
	
	@Column(name= "external_employee_id")
	private String externalEmployeeId;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}

	public EmployerInvoiceLineItems getEmployerInvoicesLineItems() {
		return employerInvoiceLineItems;
	}

	public void setEmployerInvoicesLineItems(
			EmployerInvoiceLineItems employerInvoiceLineItems) {
		this.employerInvoiceLineItems = employerInvoiceLineItems;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}
	
	/**
	 * @return the issuerRemittance
	 */
	public IssuerRemittance getIssuerRemittance() {
		return issuerRemittance;
	}

	/**
	 * @param issuerRemittance the issuerRemittance to set
	 */
	public void setIssuerRemittance(IssuerRemittance issuerRemittance) {
		this.issuerRemittance = issuerRemittance;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getExternalEmployeeId() {
		return externalEmployeeId;
	}

	public void setExternalEmployeeId(String externalEmployeeId) {
		this.externalEmployeeId = externalEmployeeId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigDecimal getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getAmountReceivedFromEmployer() {
		return amountReceivedFromEmployer;
	}

	public void setAmountReceivedFromEmployer(BigDecimal amountReceivedFromEmployer) {
		this.amountReceivedFromEmployer = amountReceivedFromEmployer;
	}

	public BigDecimal getRetroAdjustments() {
		return retroAdjustments;
	}

	public void setRetroAdjustments(BigDecimal retroAdjustments) {
		this.retroAdjustments = retroAdjustments;
	}

	public BigDecimal getUserFee() {
		return userFee;
	}

	public void setUserFee(BigDecimal userFee) {
		this.userFee = userFee;
	}

	public EmployerInvoiceLineItems getEmployerInvoiceLineItems() {
		return employerInvoiceLineItems;
	}

	public void setEmployerInvoiceLineItems(
			EmployerInvoiceLineItems employerInvoiceLineItems) {
		this.employerInvoiceLineItems = employerInvoiceLineItems;
	}

	public BigDecimal getAmtPaidToIssuer() {
		return amtPaidToIssuer;
	}

	public void setAmtPaidToIssuer(BigDecimal amtPaidToIssuer) {
		this.amtPaidToIssuer = amtPaidToIssuer;
	}

	/*@Override
	public String toString() {
		//return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
		//HIX-25009
		return "IssuerInvoicesLineItems [id=" + id + "]";
	}*/
	
}
