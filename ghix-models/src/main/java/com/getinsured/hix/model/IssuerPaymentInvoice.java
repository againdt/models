package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * The persistent class for the ISSUER_PAYMENT_INVOICE database table.
 * @author kuldeep
 */

@Audited
@Entity
@Table(name = "issuer_payment_invoice")
public class IssuerPaymentInvoice implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issuer_payinvoice_seq")
	@SequenceGenerator(name = "issuer_payinvoice_seq", sequenceName = "issuer_payinvoice_seq", allocationSize = 1)
	private int id;

	@OneToOne
	@JoinColumn(name = "remittance_id")
	private IssuerRemittance issuerRemittance;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_date", nullable = false)
	private Date creationDate;

	@Column(name = "amount")
	private BigDecimal amount;
	
	@OneToOne(mappedBy = "issuerPaymentInvoice", cascade = CascadeType.ALL)
	private IssuerPayments issuerPayments;
	
	@Column(name = "gl_code")
	private Integer glCode;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	
	public IssuerPayments getIssuerPayments() {
		return issuerPayments;
	}

	public void setIssuerPayments(IssuerPayments issuerPayments) {
		this.issuerPayments = issuerPayments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	/**
	 * @return the issuerRemittance
	 */
	public IssuerRemittance getIssuerRemittance() {
		return issuerRemittance;
	}

	/**
	 * @param issuerRemittance the issuerRemittance to set
	 */
	public void setIssuerRemittance(IssuerRemittance issuerRemittance) {
		this.issuerRemittance = issuerRemittance;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationDate(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}
	
	public Integer getGlCode() {
		return glCode;
	}

	public void setGlCode(Integer glCode) {
		this.glCode = glCode;
	}


	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Override
	public String toString() {
		//return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);		
		// Changes for HIX-25009 
		return "IssuerPaymentInvoice [id=" + id + ", creationDate="+ creationDate + ", amount=" + amount + "]";
	}


}
