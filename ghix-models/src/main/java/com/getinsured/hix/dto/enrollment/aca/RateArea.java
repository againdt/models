package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class RateArea{
  @JsonProperty("RateAreaIdentification")
  
  private RateAreaIdentification RateAreaIdentification;
  public void setRateAreaIdentification(RateAreaIdentification RateAreaIdentification){
   this.RateAreaIdentification=RateAreaIdentification;
  }
  public RateAreaIdentification getRateAreaIdentification(){
   return RateAreaIdentification;
  }
}