//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.18 at 02:15:10 PM PDT 
//


package com.getinsured.hix.dto.planmgmt.template.pdben.niem.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.dto.planmgmt.template.pdben.niem.unecerec20misc.TimeCodeType;


/**
 * A data type for a measurement of a quantity of time.
 * 
 * <p>Java class for TimeMeasureType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TimeMeasureType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}MeasureType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}TimeUnitCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeMeasureType", propOrder = {
    "timeUnitCode"
})
public class TimeMeasureType
    extends MeasureType
{

    @XmlElement(name = "TimeUnitCode", required = true, nillable = true)
    protected TimeCodeType timeUnitCode;

    /**
     * Gets the value of the timeUnitCode property.
     * 
     * @return
     *     possible object is
     *     {@link TimeCodeType }
     *     
     */
    public TimeCodeType getTimeUnitCode() {
        return timeUnitCode;
    }

    /**
     * Sets the value of the timeUnitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeCodeType }
     *     
     */
    public void setTimeUnitCode(TimeCodeType value) {
        this.timeUnitCode = value;
    }

}
