package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class EventVerificationDetailsListDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<EventVerificationDetailsDTO> eventVerificationDetailsDTO;
	private Map<String, String> issuers;
	private boolean showOverride;
	
	public List<EventVerificationDetailsDTO> getEventVerificationDetailsDTO() {
		return eventVerificationDetailsDTO;
	}
	public void setEventVerificationDetailsDTO(List<EventVerificationDetailsDTO> eventVerificationDetailsDTO) {
		this.eventVerificationDetailsDTO = eventVerificationDetailsDTO;
	}
	public Map<String, String> getIssuers() {
		return issuers;
	}
	public void setIssuers(Map<String, String> issuers) {
		this.issuers = issuers;
	}
	public boolean isShowOverride() {
		return showOverride;
	}
	public void setShowOverride(boolean showOverride) {
		this.showOverride = showOverride;
	}
	
}
