//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.15 at 12:27:33 PM IST 
//


package us.gov.treasury.irs.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Affordable Care Act (ACA) Response Dependent&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2011-11-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;A group that provides information for the IFSV response dependent&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for IFSVDependentVerificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IFSVDependentVerificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}IFSVApplicantVerification"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IFSVDependentVerificationType", propOrder = {
    "ifsvApplicantVerification"
})
public class IFSVDependentVerificationType {

    @XmlElement(name = "IFSVApplicantVerification", required = true)
    protected IFSVApplicantVerificationType ifsvApplicantVerification;

    /**
     * Gets the value of the ifsvApplicantVerification property.
     * 
     * @return
     *     possible object is
     *     {@link IFSVApplicantVerificationType }
     *     
     */
    public IFSVApplicantVerificationType getIFSVApplicantVerification() {
        return ifsvApplicantVerification;
    }

    /**
     * Sets the value of the ifsvApplicantVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link IFSVApplicantVerificationType }
     *     
     */
    public void setIFSVApplicantVerification(IFSVApplicantVerificationType value) {
        this.ifsvApplicantVerification = value;
    }

}
