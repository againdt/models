package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class AgencyBookOfBusinessExportDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	int id;
	String recordType;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	
}
