package com.getinsured.hix.dto.eapp.grammer;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.eapp.grammer.components.ui.TextComponent;

public abstract class UIComponent {
	protected String label;
	protected UIComponentType type;
	protected List<UIValidationRule> validation;
	protected String model;
	protected UIEntityTypes entity;
	
	
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public UIComponentType getType() {
		return type;
	}
	public void setType(UIComponentType type) {
		this.type = type;
	}
	public List<UIValidationRule> getValidation() {
		return validation;
	}
	public void setValidation(List<UIValidationRule> validation) {
		this.validation = validation;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

	public UIEntityTypes getEntity() {
		return entity;
	}
	public void setEntity(UIEntityTypes entity) {
		this.entity = entity;
	}
	
	public abstract boolean isValid();
	/**
	 * Parses into UIComponent using one of the factory
	 * @param json
	 * @return
	 */
	public abstract UIComponent parse(String json);
	/**
	 * Create a resulting json which is passed to UI as input
	 * @return
	 */
	public abstract String format();
	
	/**
	 * Returns null/empty array if no compilation errors and otherwise
	 * @param json
	 * @return
	 */
	public abstract List<String> compile(String json);
	
	
	public static UIComponent getSampleInstance(){
		UIComponent question = new TextComponent();
		int seed = (int)(Math.random()*8769);
		question.setLabel("Sample question " + seed);
		question.setType(UIComponentType.values()[(int)(Math.random()*4)]);
		question.setModel("household.homeAddress.street1");
		
		List<UIValidationRule> rules = new ArrayList<UIValidationRule>();
		
		rules.add(UIValidationRule.NAME);
		rules.add(UIValidationRule.REQUIRED);
		question.setValidation(rules);
		question.setEntity(UIEntityTypes.values()[(int)(Math.random()*5)]);
		
		List<UIOption> options = new ArrayList<UIOption>();
		options.add(UIOption.getSampleInstance());
		options.add(UIOption.getSampleInstance());
		
		return question;
	}
	@Override
	public String toString() {
		return "UIComponent [label=" + label + ", type=" + type
				+ ", validation=" + validation + ", model=" + model
				+ ", entity=" + entity + "]";
	}
	
	public void compileSummary(List<String> errors, UIValidationRule rule, String field, String value){
		if(!UIValidationRule.valid(rule, value)){
			errors.add(field + " attribute failed " + rule.toString() + "  validation");
		}
	}
	
	public void compileSummary(List<String> errors, UIValidationRule rule, String field, Object value){
		if(!UIValidationRule.valid(rule, value)){ 
			errors.add(field + " attribute failed " + rule.toString() + "  validation");
		}
	}
	
	
	
	
}
