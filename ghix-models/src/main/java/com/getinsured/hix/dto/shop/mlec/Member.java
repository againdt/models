package com.getinsured.hix.dto.shop.mlec;

import java.util.Date;

public class Member {
	private String memberID;
	private Relationship relationship;
	private Date memberDOB;
	private Integer age;
	private Boolean smoker;
	private float benchmarkPremium;
	private Integer dentalContribution;
	
	public static enum Relationship {
		SE, SP, CH, DT;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Relationship getRelationship() {
		return relationship;
	}

	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}

	public Date getMemberDOB() {
		return memberDOB;
	}

	public void setMemberDOB(Date memberDOB) {
		this.memberDOB = memberDOB;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Boolean getSmoker() {
		return smoker;
	}

	public void setSmoker(Boolean smoker) {
		this.smoker = smoker;
	}

	public float getBenchmarkPremium() {
		return benchmarkPremium;
	}

	public void setBenchmarkPremium(float benchmarkPremium) {
		this.benchmarkPremium = benchmarkPremium;
	}

	public Integer getDentalContribution() {
		return dentalContribution;
	}

	public void setDentalContribution(Integer dentalContribution) {
		this.dentalContribution = dentalContribution;
	}

}
