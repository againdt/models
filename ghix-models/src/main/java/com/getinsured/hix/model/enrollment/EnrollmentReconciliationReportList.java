package com.getinsured.hix.model.enrollment;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ajinkya
 * @since 04/02/2015 (DD/MM/YYYY)
 * 
 */
@XmlRootElement(name="enrollmentReconciliationReportList")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnrollmentReconciliationReportList {
	
	@XmlElement(name = "EnrollmentReconciliationReport", required = true)
	private List<EnrollmentOutboundReconciliationReport> enrRecReportList;
	
	@XmlElement(name = "fileCreationDate")
	private String fileCreationDate;

	public List<EnrollmentOutboundReconciliationReport> getEnrRecReportList() {

		if (enrRecReportList == null) {
			enrRecReportList = new ArrayList<EnrollmentOutboundReconciliationReport>();
		}
		return this.enrRecReportList;
	}

	public String getFileCreationDate() {
		return fileCreationDate;
	}

	public void setFileCreationDate(String fileCreationDate) {
		this.fileCreationDate = fileCreationDate;
	}
}
