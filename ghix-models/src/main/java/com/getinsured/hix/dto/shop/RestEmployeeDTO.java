package com.getinsured.hix.dto.shop;

import java.util.ArrayList;
import java.util.Map;

public class RestEmployeeDTO {
	
	private  ArrayList< ArrayList< ArrayList< Map<String,String> > > >  censusList;
	private String coverageStartDate;
	private Integer employerId;
	private Integer employeeId;
	private Integer employeeApplicationId;
	private String empPrimaryZip;
	private String empPrimaryCountyCode;
	private String employerExternalId;
	private String dob;
	private String zip;
	private String countyCode;
	private String tobacco;
	private Double employeeSalary;
	private Map<String,String> otherDetails;
	
	
	public ArrayList<ArrayList<ArrayList<Map<String, String>>>> getCensusList() {
		return censusList;
	}
	public void setCensusList(
			ArrayList<ArrayList<ArrayList<Map<String, String>>>> censusList) {
		this.censusList = censusList;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public Integer getEmployerId() {
		return employerId;
	}
	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}
	public String getEmpPrimaryZip() {
		return empPrimaryZip;
	}
	public void setEmpPrimaryZip(String empPrimaryZip) {
		this.empPrimaryZip = empPrimaryZip;
	}
	public String getEmployerExternalId() {
		return employerExternalId;
	}
	public void setEmployerExternalId(String employerExternalId) {
		this.employerExternalId = employerExternalId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getTobacco() {
		return tobacco;
	}
	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}
	public Double getEmployeeSalary() {
		return employeeSalary;
	}
	public void setEmployeeSalary(Double employeeSalary) {
		this.employeeSalary = employeeSalary;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmpPrimaryCountyCode() {
		return empPrimaryCountyCode;
	}
	public void setEmpPrimaryCountyCode(String empPrimaryCountyCode) {
		this.empPrimaryCountyCode = empPrimaryCountyCode;
	}
	
	public Map<String, String> getOtherDetails() {
		return otherDetails;
	}
	public void setOtherDetails(Map<String, String> otherDetails) {
		this.otherDetails = otherDetails;
	}
	public Integer getEmployeeApplicationId() {
		return employeeApplicationId;
	}
	public void setEmployeeApplicationId(Integer employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}
}
