package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;

public class PlanDisplayEnum implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	public static enum YorN { Y, N; }	
	public static enum EnrollmentType { I, S, A, N; }	
	public static enum CostSharing { CS1, CS2, CS3, CS4, CS5, CS6; }	
	public static enum RequestType { PRESHOPPING, FFM; }
	public static enum ShoppingType { INDIVIDUAL, EMPLOYEE; }	
	public static enum Relationship { SELF, SPOUSE, CHILD, DEPENDENT; }	
	public static enum EnrollmentFlowType { KEEP, NEW; }	
	public static enum PreferencesLevel { LEVEL1, LEVEL2, LEVEL3, LEVEL4; }
	public static enum InsuranceType { HEALTH, DENTAL, STM, VISION, AME, LIFE; }
	public static enum ExchangeType {ON, OFF;}
	public static enum Tenant {GINS;}
	public static enum FlowType { ISSUERVERIFICATION, ANONYMOUSEMPLOYER, EMPLOYERPLANSELECTION, PLANSELECTION, PREELIGIBILITY, ANCILLARY; }
	public static enum GPSVersion {V1, BASELINE;}
	public static enum Gender {M, F}
	public static enum PlanTab {TAX_CREDIT, FULL_PRICE}
	public static enum LogLevel {DEBUG, INFO, WARN, ERROR}
	public static enum ExpenseEstimate {LOW, AVERAGE, PRICEY}
	public static enum MedicalCondition {HIV, DIABETES, HEARTDISEASE}
	public static enum MedicalProcedure {PREGNANCY, FRACTURE}
}
