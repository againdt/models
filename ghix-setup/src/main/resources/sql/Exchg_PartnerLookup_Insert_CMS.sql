insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'CMS',
  'CMS',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  'CMSFFM',
  'T',
  'NM0',
  '',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/NM0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/NM0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '101',
  systimestamp,
  systimestamp  
);

insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, 'CMS', '1' );

insert into EXCHG_FilePattern values ( 126,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_CA_834_RECONSUPPL_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' );

update EXCHG_FilePatternRole set FILEPATTERN_IDS = '101,103,126' where ID = '101';


