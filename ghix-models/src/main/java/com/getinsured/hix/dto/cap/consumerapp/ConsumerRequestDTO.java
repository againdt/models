package com.getinsured.hix.dto.cap.consumerapp;

public class ConsumerRequestDTO {
	
	private int id;
	private String state;
	private String email;
	private String phoneNumber;
	private String mobilePhone;
	private String homePhone;
	private String poaPhone;
	private String status;
	private String leadStatus;
	private String zipCode;
	private String firstName;
	private String lastName;
	private String serviceBy;
	private String preferredContactTime;
	private LocationDto location;
	private String eligLeadId;
	public String affiliateId;
	public String affiliateName;
	public String affiliateFlowId;
	public String flowName;
	public String permissionToCall;
	public String callId;
	public boolean  dataSharingConsent;
	public String  countyCode;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getPoaPhone() {
		return poaPhone;
	}

	public void setPoaPhone(String poaPhone) {
		this.poaPhone = poaPhone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public String getServiceBy() {
		return serviceBy;
	}

	public void setServiceBy(String serviceBy) {
		this.serviceBy = serviceBy;
	}

	public String getPreferredContactTime() {
		return preferredContactTime;
	}

	public void setPreferredContactTime(String preferredContactTime) {
		this.preferredContactTime = preferredContactTime;
	}

	public String getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(String eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public String getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}

	public String getAffiliateName() {
		return affiliateName;
	}

	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	
	public String getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(String affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	public String getPermissionToCall() {
		return permissionToCall;
	}

	public void setPermissionToCall(String permissionToCall) {
		this.permissionToCall = permissionToCall;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public boolean getDataSharingConsent() {
		return dataSharingConsent;
	}

	public void setDataSharingConsent(boolean dataSharingConsent) {
		this.dataSharingConsent = dataSharingConsent;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

}
