package com.getinsured.hix.model.estimator.mini;

import java.io.Serializable;

public class RetireeMedicare implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean exchangeElible;
	private String  hicn;
	private String medicarePartAeffectiveDate;
	private String medicarePartAterminationDate;
	private String medicarePartBeffectiveDate;
	private String medicarePartBterminationDate;
	private String pinCode;
	
	public boolean isExchangeElible() {
		return exchangeElible;
	}
	public void setExchangeElible(boolean exchangeElible) {
		this.exchangeElible = exchangeElible;
	}
	public String getHICN() {
		return hicn;
	}
	public void setHICN(String hicn) {
		this.hicn = hicn;
	}
	public String getMedicarePartAeffectiveDate() {
		return medicarePartAeffectiveDate;
	}
	public void setMeicarePartAeffectiveDate(String partA_EffectiveDate) {
		medicarePartAeffectiveDate = partA_EffectiveDate;
	}
	public String getMedicarePartAterminationDate() {
		return medicarePartAterminationDate;
	}
	public void setMeicarePartAterminationDate(String partA_TerminationDate) {
		medicarePartAterminationDate = partA_TerminationDate;
	}
	public String getMedicarePartBeffectiveDate() {
		return medicarePartBeffectiveDate;
	}
	public void setMedicarePartBeffectiveDate(String partB_EffectiveDate) {
		medicarePartBeffectiveDate = partB_EffectiveDate;
	}
	public String getMedicarePartBterminationDate() {
		return medicarePartBterminationDate;
	}
	public void setMedicarePartBterminationDate(String partB_TerminationDate) {
		medicarePartBterminationDate = partB_TerminationDate;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}


}
