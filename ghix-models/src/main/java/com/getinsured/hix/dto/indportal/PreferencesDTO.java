/**
 * 
 */
package com.getinsured.hix.dto.indportal;

import java.util.Date;

import javax.persistence.Column;

import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.platform.dto.address.LocationDTO;

/**
 * @author vishwanath_s
 * Class to store Contact  Preference details
 */
public class PreferencesDTO {

	private String emailAddress;
	private String phoneNumber;
	private String prefSpokenLang;
	private String prefWrittenLang;
	private GhixNoticeCommunicationMethod prefCommunication;
	private GhixNoticeCommunicationMethod prevPrefCommunication;
	private boolean hasActiveEnrollment;
	private boolean mailingAddressUpdated;
	private boolean contactPreferencesUpdated;
	private LocationDTO locationDto;
	private String dob;
	private boolean editMode;
	private String activeEnrollmentYear;
	private boolean nonFinancial;
	private boolean activeApplication;
	private boolean textMe;
	private boolean mobileNumberVerified;
	private Boolean optInPaperless1095;
	private Integer paperless1095User;
	private Date paperless1095Date;
	private boolean isInsideOEEnrollementWindow;
	
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the prefSpokenLang
	 */
	public String getPrefSpokenLang() {
		return prefSpokenLang;
	}
	/**
	 * @param prefSpokenLang the prefSpokenLang to set
	 */
	public void setPrefSpokenLang(String prefSpokenLang) {
		this.prefSpokenLang = prefSpokenLang;
	}
	/**
	 * @return the prefCommunication
	 */
	public GhixNoticeCommunicationMethod getPrefCommunication() {
		if(prefCommunication == null){
			prefCommunication = GhixNoticeCommunicationMethod.Email;
		}
		return prefCommunication;
	}
	/**
	 * @param prefCommunication the prefCommunication to set
	 */
	public void setPrefCommunication(GhixNoticeCommunicationMethod prefCommunication) {
		this.prefCommunication = prefCommunication;
	}
	/**
	 * @return the prefWrittenLang
	 */
	public String getPrefWrittenLang() {
		return prefWrittenLang;
	}
	/**
	 * @param prefWrittenLang the prefWrittenLang to set
	 */
	public void setPrefWrittenLang(String prefWrittenLang) {
		this.prefWrittenLang = prefWrittenLang;
	}
	
	/**
	 * @return the hasActiveEnrollment
	 */
	public boolean isHasActiveEnrollment() {
		return hasActiveEnrollment;
	}
	/**
	 * @param hasActiveEnrollment the hasActiveEnrollment to set
	 */
	public void setHasActiveEnrollment(boolean hasActiveEnrollment) {
		this.hasActiveEnrollment = hasActiveEnrollment;
	}
	/**
	 * @return the isMailingAddressUpdated
	 */
	public boolean isMailingAddressUpdated() {
		return mailingAddressUpdated;
	}
	/**
	 * @param isMailingAddressUpdated the isMailingAddressUpdated to set
	 */
	public void setMailingAddressUpdated(boolean isMailingAddressUpdated) {
		this.mailingAddressUpdated = isMailingAddressUpdated;
	}
	/**
	 * @return the isContactPreferencesUpdated
	 */
	public boolean isContactPreferencesUpdated() {
		return contactPreferencesUpdated;
	}
	/**
	 * @param isContactPreferencesUpdated the isContactPreferencesUpdated to set
	 */
	public void setContactPreferencesUpdated(boolean isContactPreferencesUpdated) {
		this.contactPreferencesUpdated = isContactPreferencesUpdated;
	}
	/**
	 * @return the locationDto
	 */
	public LocationDTO getLocationDto() {
		return locationDto;
	}
	/**
	 * @param locationDto the locationDto to set
	 */
	public void setLocationDto(LocationDTO locationDto) {
		this.locationDto = locationDto;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	/**
	 * @return the editMode
	 */
	public boolean isEditMode() {
		return editMode;
	}
	/**
	 * @param editMode the editMode to set
	 */
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	/**
	 * @return the activeEnrollmentYear
	 */
	public String getActiveEnrollmentYear() {
		return activeEnrollmentYear;
	}
	/**
	 * @param activeEnrollmentYear the activeEnrollmentYear to set
	 */
	public void setActiveEnrollmentYear(String activeEnrollmentYear) {
		this.activeEnrollmentYear = activeEnrollmentYear;
	}
	public boolean isNonFinancial() {
		return nonFinancial;
	}
	public void setNonFinancial(boolean nonFinancial) {
		this.nonFinancial = nonFinancial;
	}
	/**
	 * @return the activeApplication
	 */
	public boolean isActiveApplication() {
		return activeApplication;
	}
	/**
	 * @param activeApplication the activeApplication to set
	 */
	public void setActiveApplication(boolean activeApplication) {
		this.activeApplication = activeApplication;
	}
	/**
	 * @return the prevPrefCommunication
	 */
	public GhixNoticeCommunicationMethod getPrevPrefCommunication() {
		return prevPrefCommunication;
	}
	/**
	 * @param prevPrefCommunication the prevPrefCommunication to set
	 */
	public void setPrevPrefCommunication(GhixNoticeCommunicationMethod prevPrefCommunication) {
		this.prevPrefCommunication = prevPrefCommunication;
	}
	public boolean getTextMe() {
		return textMe;
	}
	public void setTextMe(boolean textMe) {
		this.textMe = textMe;
	}
	public boolean getMobileNumberVerified() {
		return mobileNumberVerified;
	}
	public void setMobileNumberVerified(boolean mobileNumberVerified) {
		this.mobileNumberVerified = mobileNumberVerified;
	}
	public Boolean getOptInPaperless1095() {
		return optInPaperless1095;
	}
	public void setOptInPaperless1095(Boolean optInPaperless1095) {
		this.optInPaperless1095 = optInPaperless1095;
	}
	public Integer getPaperless1095User() {
		return paperless1095User;
	}
	public void setPaperless1095User(Integer paperless1095User) {
		this.paperless1095User = paperless1095User;
	}
	public Date getPaperless1095Date() {
		return paperless1095Date;
	}
	public void setPaperless1095Date(Date paperless1095Date) {
		this.paperless1095Date = paperless1095Date;
	}
	public boolean isInsideOEEnrollementWindow() {
		return isInsideOEEnrollementWindow;
	}
	public void setInsideOEEnrollementWindow(boolean isInsideOEEnrollementWindow) {
		this.isInsideOEEnrollementWindow = isInsideOEEnrollementWindow;
	}
	
}
