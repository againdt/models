package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * -----------------------------------------------------------------------------
 * HIX-88041 PHIX - Issuer commission schedule data for an issuer.
 * -----------------------------------------------------------------------------
 * 
 * This is POJO class for PM_ISSUER_COMMISSION database table.
 * 
 * @author Bhavin Parmar
 * @since October 03, 2016
 * 
 */
@Audited
@Entity
@Table(name = "PM_ISSUER_COMMISSION")
public class IssuerCommission implements Serializable {

	private static final long serialVersionUID = 1L;

	// Possible values: M-Monthly, Q-Quarterly, Y-Yearly P-PMPM
	public static enum Frequency {
		M, Q, Y, P;
	}
	
	public static enum InsuranceType {
		Health, Dental, Vision, AME, Life, STM, Medicaid, MC_SUP, MC_RX, MC_ADV;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_ISSUER_COMMISSION_SEQ")
	@SequenceGenerator(name = "PM_ISSUER_COMMISSION_SEQ", sequenceName = "PM_ISSUER_COMMISSION_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne(cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
	@JoinColumn(name = "ISSUER_ID", nullable = false)
	private Issuer issuer;

	// Possible values: HEALTH, DENTAL, VISION, MEDICAID, LIFE, AME, STM, etc.
	@Column(name = "INSURANCE_TYPE")
	private String insuranceType;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "SCHEDULE_START_DATE", nullable = false)
	private Date scheduleStartDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "SCHEDULE_END_DATE")
	private Date scheduleEndDate;

	@Column(name = "COMMISSION_DOLLAR_FIRST_YEAR")
	private Double commissionDollarFirstYear;

	@Column(name = "COMMISSION_PCT_FIRST_YEAR")
	private Double commissionPercentageFirstYear;

	@Column(name = "COMMISSION_DOLLAR_SECOND_YEAR")
	private Double commissionDollarSecondYear;

	@Column(name = "COMMISSION_PCT_SECOND_YEAR")
	private Double commissionPercentageSecondYear;

	// Possible values: M-Monthly, Q-Quarterly and Y-Yearly
	@Column(name = "FREQUENCY")
	private String frequency;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public IssuerCommission() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Date getScheduleStartDate() {
		return scheduleStartDate;
	}

	public void setScheduleStartDate(Date scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}

	public Date getScheduleEndDate() {
		return scheduleEndDate;
	}

	public void setScheduleEndDate(Date scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}

	public Double getCommissionDollarFirstYear() {
		return commissionDollarFirstYear;
	}

	public void setCommissionDollarFirstYear(Double commissionDollarFirstYear) {
		this.commissionDollarFirstYear = commissionDollarFirstYear;
	}

	public Double getCommissionPercentageFirstYear() {
		return commissionPercentageFirstYear;
	}

	public void setCommissionPercentageFirstYear(
			Double commissionPercentageFirstYear) {
		this.commissionPercentageFirstYear = commissionPercentageFirstYear;
	}

	public Double getCommissionDollarSecondYear() {
		return commissionDollarSecondYear;
	}

	public void setCommissionDollarSecondYear(Double commissionDollarSecondYear) {
		this.commissionDollarSecondYear = commissionDollarSecondYear;
	}

	public Double getCommissionPercentageSecondYear() {
		return commissionPercentageSecondYear;
	}

	public void setCommissionPercentageSecondYear(
			Double commissionPercentageSecondYear) {
		this.commissionPercentageSecondYear = commissionPercentageSecondYear;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	public Map<InsuranceType, String> getInsuranceTypeList() {
		return IssuerCommission.getInsuranceTypeOptionsList();
	}
	
	public static Map<InsuranceType, String> getInsuranceTypeOptionsList() {

		Map<InsuranceType, String> enumMap = new EnumMap<InsuranceType, String>(InsuranceType.class);
		enumMap.put(InsuranceType.Health, InsuranceType.Health.toString());
		enumMap.put(InsuranceType.Dental, InsuranceType.Dental.toString());
		enumMap.put(InsuranceType.Vision, InsuranceType.Vision.toString());
		enumMap.put(InsuranceType.AME, InsuranceType.AME.toString());
		enumMap.put(InsuranceType.Life, InsuranceType.Life.toString());
		enumMap.put(InsuranceType.STM, InsuranceType.STM.toString());
		enumMap.put(InsuranceType.Medicaid, InsuranceType.Medicaid.toString());
		enumMap.put(InsuranceType.MC_SUP, InsuranceType.MC_SUP.toString());
		enumMap.put(InsuranceType.MC_RX, InsuranceType.MC_RX.toString());
		enumMap.put(InsuranceType.MC_ADV, InsuranceType.MC_ADV.toString());
		return enumMap;
	}
	
	public Map<Frequency, String> getFrequencyList() {
		return IssuerCommission.getFrequencyOptionsList();
	}
	
	public static Map<Frequency, String> getFrequencyOptionsList() {

		Map<Frequency, String> enumMap = new EnumMap<Frequency, String>(Frequency.class);
		enumMap.put(Frequency.M, "Monthly");
		enumMap.put(Frequency.Q, "Quaterly");
		enumMap.put(Frequency.Y, "Yearly");
		enumMap.put(Frequency.P, "PMPM");
		return enumMap;
	}
}
