package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "pm_service_area")
public class ServiceArea implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}

	public static enum EnrollmentAvailStatus {
		AVAILABLE, DEPENDENTSONLY, NOTAVAILABLE;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_SERVICE_AREA_SEQ")
	@SequenceGenerator(name = "PM_SERVICE_AREA_SEQ", sequenceName = "PM_SERVICE_AREA_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "service_area_id")
	private IssuerServiceArea serviceAreaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SERVICE_AREA_EXT_ID")
	private IssuerServiceAreaExt serviceAreaExtId;

	@Column(name = "zip")
	private String zip;

	@Column(name = "county")
	private String county;

	@Column(name = "fips")
	private String fips;

	@Column(name = "state", length = 255)
	private String state;

	@Column(name = "IS_DELETED")
	// @Enumerated(EnumType.STRING)
	private String isDeleted;

	@Column(name = "ENROLLMENT_AVAIL_STATUS")
	private String enrollmentAvailStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public IssuerServiceArea getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(IssuerServiceArea serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	public IssuerServiceAreaExt getServiceAreaExtId() {
		return serviceAreaExtId;
	}

	public void setServiceAreaExtId(IssuerServiceAreaExt serviceAreaExtId) {
		this.serviceAreaExtId = serviceAreaExtId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getFips() {
		return fips;
	}

	public void setFips(String fips) {
		this.fips = fips;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getEnrollmentAvailStatus() {
		return enrollmentAvailStatus;
	}

	public void setEnrollmentAvailStatus(String enrollmentAvailStatus) {
		this.enrollmentAvailStatus = enrollmentAvailStatus;
	}
}
