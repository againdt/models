package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PD_PREFERENCES")
public class PdPreferences implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PD_PREFERENCES_SEQ")
	@SequenceGenerator(name = "PD_PREFERENCES_SEQ", sequenceName = "PD_PREFERENCES_SEQ", allocationSize = 1)
	private Long id;
	
	@Column(name = "PD_HOUSEHOLD_ID")
	private Long pdHouseholdId;
	
	@Column(name = "ELIG_LEAD_ID")
	private Long eligLeadId;
	
	@Column(name = "PREFERENCES")
	private String preferences;
	
	@Column(name = "DOCTORS")
	private String doctors;
	
	@Column(name = "PRESCRIPTIONS")
	private String prescriptions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPdHouseholdId() {
		return pdHouseholdId;
	}

	public void setPdHouseholdId(Long pdHouseholdId) {
		this.pdHouseholdId = pdHouseholdId;
	}

	public Long getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(Long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public String getPreferences() {
		return preferences;
	}

	public void setPreferences(String preferences) {
		this.preferences = preferences;
	}

	public String getDoctors() {
		return doctors;
	}

	public void setDoctors(String doctors) {
		this.doctors = doctors;
	}

	public String getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(String prescriptions) {
		this.prescriptions = prescriptions;
	}
}
