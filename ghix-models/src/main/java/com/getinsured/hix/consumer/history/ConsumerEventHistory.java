package com.getinsured.hix.consumer.history;

import java.util.Date;


public class ConsumerEventHistory extends ConsumerHistoryBase {
	
	private String eventSource; //enrollment, login, logout
	private String eventLevel; //Info
	private Date   datetime ;
	private Integer householdId;
	private String eventDetailJson;
	private Integer createdBy;
	private String description;
	private ConsumerEventMetaData eventMetaData;
	private String createdByName;
	  
	
	public ConsumerEventHistory(){
		
	}
	
	public ConsumerEventHistory(Date dt){
		super(dt);
	}
	
	public String getEventSource() {
		return eventSource;
	}

	public void setEventSource(String eventSource) {
		this.eventSource = eventSource;
	}

	public String getEventLevel() {
		return eventLevel;
	}

	public void setEventLevel(String eventLevel) {
		this.eventLevel = eventLevel;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	
	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

	public String getEventDetailJson() {
		return eventDetailJson;
	}

	public void setEventDetailJson(String eventDetailJson) {
		this.eventDetailJson = eventDetailJson;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	String toJson()
	{
	  return null;
	}

	public ConsumerEventMetaData getEventMetaData() {
		return eventMetaData;
	}

	public void setEventMetaData(ConsumerEventMetaData eventMetaData) {
		this.eventMetaData = eventMetaData;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
}
