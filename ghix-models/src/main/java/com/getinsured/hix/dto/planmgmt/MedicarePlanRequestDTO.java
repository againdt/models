/**
 * 
 * @author santanu
 * @version 1.0
 * @since Feb 3, 2015 
 * 
 * This DTO to accept request object for Medicare plan
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

public class MedicarePlanRequestDTO implements Serializable {
	
	/*
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor.
	 */
	public MedicarePlanRequestDTO() {}

	private String url;
	private String requestMethod;	
	private String effectiveDate;
	private List<Member> memberList; // household member data
	private String insuranceType; // Medicare / Medigap
	private String tenant; 
	private String zipCode;
	private String countyName;
	private String planYear;
	private String planSource;
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}	
	/**
	 * @return the requestMethod
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * @param requestMethod the requestMethod to set
	 */
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}	
	
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	
	/**
	 * @return the memberList
	 */
	public List<Member> getMemberList() {
		return memberList;
	}

	/**
	 * @param memberList the memberList to set
	 */
	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}

	/**
	 * @return the insuranceType
	 */
	public String getInsuranceType() {
		return insuranceType;
	}

	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	/**
	 * @return the tenant
	 */
	public String getTenant() {
		return tenant;
	}

	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the countyName
	 */
	public String getCountyName() {
		return countyName;
	}

	/**
	 * @param countyName the countyName to set
	 */
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
		
	/**
	 * @return the planYear
	 */
	public String getPlanYear() {
		return planYear;
	}

	/**
	 * @param planYear the planYear to set
	 */
	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}
	
	/**
	 * @return the planSource
	 */
	public String getPlanSource() {
		return planSource;
	}

}
