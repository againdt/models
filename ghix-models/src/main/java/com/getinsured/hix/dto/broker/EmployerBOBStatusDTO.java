package com.getinsured.hix.dto.broker;

import java.io.Serializable;

/**
 * DTO for IND49
 *
 */
public class EmployerBOBStatusDTO implements Serializable {
	private static final long serialVersionUID = 1L;
    private String buisnessLegalName;
    private String eligibilityStatus;
    private int employeesTotalCount;
    private long employerID;
    private String enrollmentStatus;
	public String getBuisnessLegalName() {
		return buisnessLegalName;
	}
	public void setBuisnessLegalName(String buisnessLegalName) {
		this.buisnessLegalName = buisnessLegalName;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public int getEmployeesTotalCount() {
		return employeesTotalCount;
	}
	public void setEmployeesTotalCount(int employeesTotalCount) {
		this.employeesTotalCount = employeesTotalCount;
	}
	public long getEmployerID() {
		return employerID;
	}
	public void setEmployerID(long employerID) {
		this.employerID = employerID;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
    
	@Override
	public String toString() {
		return "EmployerBOBStatusDTO details: EmployerID = "+employerID+", BuisnessLegalName = "+buisnessLegalName+", " +
				"EnrollmentStatus = "+enrollmentStatus;
	}
}
