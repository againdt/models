package com.getinsured.hix.model.serff;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * Model class for SERFF_PLAN_MGMT_BATCH table
 * @author Vani Sharma
 * @since 23 Aug, 2013
 */
@Entity
@Table(name="SERFF_PLAN_MGMT_BATCH")
@DynamicInsert
@DynamicUpdate
public class SerffPlanMgmtBatch implements Serializable {

	private static final long serialVersionUID = 9147340232755106762L;
	
	/**
	 *
	 */
	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}
	
	public enum FTP_STATUS {
		IN_PROGRESS, COMPLETED, FAILED;
	}
	
	public enum BATCH_STATUS {
		WAITING, IN_PROGRESS, COMPLETED, FAILED, IN_QUEUE;
	}
	
	public enum EXCHANGE_TYPE {
		ON, OFF, ANY, PUF;
	}
	
	public enum OPERATION_TYPE {
		PLAN, PRESCRIPTION_DRUG, STM, STM_HCC, STM_AREA_FACTOR, AME_FAMILY, AME_GENDER, AME_AGE, PLAN_DOCS, PLAN_MEDICAID, VISION,
		MEDICARE_AD, MEDICARE_SP, MEDICARE_RX, LIFE, DRUGS_JSON, DRUGS_XML;
	}
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERFF_PLAN_MGMT_BATCH_SEQ")
	@SequenceGenerator(name = "SERFF_PLAN_MGMT_BATCH_SEQ", sequenceName = "SERFF_PLAN_MGMT_BATCH_SEQ", allocationSize = 1)
	private long id;
	
	@Column(name = "USER_ID")
	private int userId;

	@Column(name = "FTP_START_TIME")
	private Date ftpStartTime;

	@Column(name = "FTP_END_TIME")
	private Date ftpEndTime;

	@Column(name = "BATCH_STATUS")
	@Enumerated(EnumType.STRING)
	private BATCH_STATUS batchStatus;

	@Column(name = "BATCH_START_TIME")
	private Date batchStartTime;

	@Column(name = "BATCH_END_TIME")
	private Date batchEndTime;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "SERFF_PLAN_MGMT_ID")
	private SerffPlanMgmt serffReqId;

	@Column(name = "IS_DELETED")
	@Enumerated(EnumType.STRING)
	private IS_DELETED isDeleted;

	@Column(name = "FTP_STATUS")
	@Enumerated(EnumType.STRING)
	private FTP_STATUS ftpStatus;

	@Column(name = "ISSUER_ID")
	private String issuerId;

	@Column(name = "CAREER")
	private String career;

	@Column(name = "STATE")
	private String state;
	
	@Column(name = "exchange_type")
	private String exchangeType;	
	
	@Column(name = "default_tenant")
	private String defaultTenant;	
	
	@Column(name = "operation_type")
	private String operationType;	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getFtpStartTime() {
		return ftpStartTime;
	}

	public void setFtpStartTime(Date ftpStartTime) {
		this.ftpStartTime = ftpStartTime;
	}

	public Date getFtpEndTime() {
		return ftpEndTime;
	}

	public void setFtpEndTime(Date ftpEndTime) {
		this.ftpEndTime = ftpEndTime;
	}

	public Date getBatchStartTime() {
		return batchStartTime;
	}

	public void setBatchStartTime(Date batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public Date getBatchEndTime() {
		return batchEndTime;
	}

	public SerffPlanMgmt getSerffReqId() {
		return serffReqId;
	}

	public void setSerffReqId(SerffPlanMgmt serffReqId) {
		this.serffReqId = serffReqId;
	}

	public void setBatchEndTime(Date batchEndTime) {
		this.batchEndTime = batchEndTime;
	}

	
	public IS_DELETED getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(IS_DELETED isDeleted) {
		this.isDeleted = isDeleted;
	}

	public FTP_STATUS getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(FTP_STATUS ftpStatus) {
		this.ftpStatus = ftpStatus;
	}

	public BATCH_STATUS getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(BATCH_STATUS batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getDefaultTenant() {
		return defaultTenant;
	}

	public void setDefaultTenant(String defaultTenant) {
		this.defaultTenant = defaultTenant;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	@Override
	public String toString() {
		return "SerffPlanMgmtBatch [id=" + id + ", userId=" + userId
				+ ", ftpStartTime=" + ftpStartTime + ", ftpEndTime="
				+ ftpEndTime + ", batchStatus=" + batchStatus
				+ ", batchStartTime=" + batchStartTime + ", batchEndTime="
				+ batchEndTime + ", serffReqId=" + serffReqId + ", isDeleted="
				+ isDeleted + ", ftpStatus=" + ftpStatus + ", issuerId="
				+ issuerId + ", career=" + career + ", state=" + state
				+ ", exchangeType=" + exchangeType + ", defaultTenant="
				+ defaultTenant + "]";
	}
		
}
