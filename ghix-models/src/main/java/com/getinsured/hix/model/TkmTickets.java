package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.SortableEntity;

/**
 * 
 * @author hardas_d
 * Ticket entity.  It holds the ticket information
 */
@Audited
@Entity
@Table(name = "tkm_tickets")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
public class TkmTickets implements Serializable,SortableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String DEF_ARCHIVED_VAL = "N";
	
	public enum ticket_status
	{
		Open("Open"), Resolved("Resolved"), UnClaimed("UnClaimed"), Completed("Completed"), Canceled("Canceled"), 
		Reopened("Reopened"), Restarted("Restarted");
		
		private final String ticket_status;

		private ticket_status(String status) {
			ticket_status = status;
		}
		
		public String getStatusCode() {
			return ticket_status;
		}
	}
	
	
	public enum TicketPriority
	 {
	  Critical("Critical", 4), High("High", 3), Medium("Medium", 2), Low("Low", 1);  
	  
	  private final String TicketPriority;
	  
	  public String getTicketPriority() {
		return TicketPriority;
	}

	  private final Integer priorityOrder;

	  private TicketPriority(String priority) {
	   TicketPriority = priority;
	   priorityOrder = 2;
	  }
	  
	  public Integer getPriorityOrder() {
		return priorityOrder;
	}

	private TicketPriority(String priority, Integer order) {
	   TicketPriority = priority;
	   priorityOrder = order;
	  }
	 }
	
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TkmTicket_Seq")
	@SequenceGenerator(name = "TkmTicket_Seq", sequenceName = "tkm_tickets_seq", allocationSize = 1)
	@Column(name = "tkm_ticket_id")
	private Integer id;

	@Column(name = "ticket_number")
	private String number;

	@NotEmpty
	@Size(min=1, max=2000)
	@Column(name = "description")
	private String description;

	@NotEmpty
	@Column(name = "subject")
	private String subject;

	//@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "tkm_workflow_id")
	private TkmWorkflows tkmWorkflows;

	//@NotAudited
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	private AccountUser creator;
	
	//@NotAudited
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "last_updated_by")
	private AccountUser updator;
	
	//@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userrole")
	private Role userRole;
	
	//@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private AccountUser role;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "assigned_queue")
	private TkmQueues tkmQueues;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	@Column(name="status")
	private String status;
	
	@Column(name="closing_comment")
	private String closingComment;
	
	@NotEmpty
	@Column(name="priority")
	private String priority;
	
	
	@Column(name="priority_order")
	private Integer priorityOrder;
	


	@Column(name="module_id")
	private Integer moduleId;
	
	@Column(name = "due_date")
	private Date dueDate;
	
	//merged from PHIX
	@Column(name="submodule_id")
	private Integer subModuleId;
	
	@Column(name = "module_name")
	private String moduleName;
	
	
	@Column(name = "archive_flag")
	private String isArchived;
	
	@Column(name = "archived_by")
	private Integer archivedBy;
	
	@Column(name = "ARCHIVED_ON")
	private Date archivedOn;

	@Audited
	@Fetch(FetchMode.SELECT)
	@OneToMany(mappedBy="ticket", fetch=FetchType.LAZY )
	private List<TkmTicketTasks> tkmtickettasks;

	
	@NotAudited
	@Column(name = "tkm_extracted_batch_flag")
	private String tkmExtractedtBatchFlag;
	
	@Column(name="TENANT_ID")
    private Long tenantId;
		
	@Column(name="submodule_name")
	private String subModuleName;
		
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public TkmWorkflows getTkmWorkflows() {
		return tkmWorkflows;
	}

	public void setTkmWorkflows(TkmWorkflows tkmWorkflows) {
		this.tkmWorkflows = tkmWorkflows;
	}

	public AccountUser getCreator() {
		return creator;
	}

	public void setCreator(AccountUser creator) {
		this.creator = creator;
	}

	public AccountUser getUpdator() {
		return updator;
	}

	public void setUpdator(AccountUser updator) {
		this.updator = updator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	
	public String getTkmExtractedtBatchFlag() {
		return tkmExtractedtBatchFlag;
	}

	public void setTkmExtractedtBatchFlag(String tkmExtractedtBatchFlag) {
		this.tkmExtractedtBatchFlag = tkmExtractedtBatchFlag;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public TkmQueues getTkmQueues() {
		return tkmQueues;
	}

	public void setTkmQueues(TkmQueues tkmQueues) {
		this.tkmQueues = tkmQueues;
	}

	public String getClosingComment() {
		return closingComment;
	}

	public void setClosingComment(String closingComment) {
		this.closingComment = closingComment;
	}

	/**
	 * @return the userRole
	 */
	public Role getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole the userRole to set
	 */
	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the role
	 */
	public AccountUser getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(AccountUser role) {
		this.role = role;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
		
		setPriorityOrder(TicketPriority.valueOf(priority).getPriorityOrder());
	}

	/**
	 * 
	 * @return priorityOrder
	 */
	
	public Integer getPriorityOrder() {
		return priorityOrder;
	}

	/**
	 * 
	 * @param priorityOrder
	 */
	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}
	
	/**
	 * @return the moduleId
	 */
	public Integer getModuleId() {
		return moduleId;
	}
	
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	

	/**
	 * @return the tkmtickettasks
	 */
	public List<TkmTicketTasks> getTkmtickettasks() {
		return tkmtickettasks;
	}

	/**
	 * @param tkmtickettasks the tkmtickettasks to set
	 */
	public void setTkmtickettasks(List<TkmTicketTasks> tkmtickettasks) {
		this.tkmtickettasks = tkmtickettasks;
	}

	public Integer getSubModuleId() {
		return subModuleId;
	}

	public void setSubModuleId(Integer subModuleId) {
		this.subModuleId = subModuleId;
	}
    
	public String getSubModuleName() {
		return subModuleName;
	}

	public void setSubModuleName(String subModuleName) {
		this.subModuleName = subModuleName;
	}

	public String getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(String isArchived) {
		this.isArchived = isArchived;
	}

	public Integer getArchivedBy() {
		return archivedBy;
	}

	public void setArchivedBy(Integer archivedBy) {
		this.archivedBy = archivedBy;
	}

	public Date getArchivedOn() {
		return archivedOn;
	}

	public void setArchivedOn(Date archivedOn) {
		this.archivedOn = archivedOn;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	public Long getTenantId() {
		return tenantId;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
		if(this.isArchived == null || ("").equalsIgnoreCase(this.isArchived) ) {
			this.isArchived = DEF_ARCHIVED_VAL;
		}
		
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        setTenantId(TenantContextHolder.getTenant().getId());
		}
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TkmTickets [id=" + id + ", number=" + number + ", description="
				+ description + ", subject=" + subject + ", tkmWorkflows="
				+ tkmWorkflows + ", creator=" + creator + ", updator="
				+ updator + ", userRole=" + userRole + ", role=" + role
				+ ", tkmQueues=" + tkmQueues + ", created=" + created
				+ ", updated=" + updated + ", status=" + status
				+ ", closingComment=" + closingComment + "]";
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		 Set<String> columnNames = new HashSet<String>();
	        columnNames.add("created");
	        return columnNames;
	}

}
