
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Affordable Insurance Exchange Statement.
 * 					&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095ADetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095ADetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecordSequenceNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}UniqueRecordSequenceNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedRecordSequenceNum" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PayerDLN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PayeeDLN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TaxYr"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}HealthExchangeId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientPolicyDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientSpouseDetail" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CoverageHouseholdGrp" maxOccurs="unbounded"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientPolicyInfoDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095ADetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "recordSequenceNum",
    "uniqueRecordSequenceNum",
    "correctedInd",
    "correctedRecordSequenceNum",
    "payerDLN",
    "payeeDLN",
    "taxYr",
    "healthExchangeId",
    "recipientPolicyDetail",
    "recipientDetail",
    "recipientSpouseDetail",
    "coverageHouseholdGrp",
    "recipientPolicyInfoDetail",
    "errorDetail"
})
public class Form1095ADetailType {

    @XmlElement(name = "RecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String recordSequenceNum;
    @XmlElement(name = "UniqueRecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String uniqueRecordSequenceNum;
    @XmlElement(name = "CorrectedInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String correctedInd;
    @XmlElement(name = "CorrectedRecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String correctedRecordSequenceNum;
    @XmlElement(name = "PayerDLN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String payerDLN;
    @XmlElement(name = "PayeeDLN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String payeeDLN;
    @XmlElement(name = "TaxYr", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String taxYr;
    @XmlElement(name = "HealthExchangeId", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String healthExchangeId;
    @XmlElement(name = "RecipientPolicyDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected RecipientPolicyDetailType recipientPolicyDetail;
    @XmlElement(name = "RecipientDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected RecipientDetailType recipientDetail;
    @XmlElement(name = "RecipientSpouseDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected RecipientSpouseDetailType recipientSpouseDetail;
    @XmlElement(name = "CoverageHouseholdGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected List<CoverageHouseholdGrpType> coverageHouseholdGrp;
    @XmlElement(name = "RecipientPolicyInfoDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected TaxHouseholdPolicyInformationGrpType recipientPolicyInfoDetail;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the recordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordSequenceNum() {
        return recordSequenceNum;
    }

    /**
     * Sets the value of the recordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordSequenceNum(String value) {
        this.recordSequenceNum = value;
    }

    /**
     * Gets the value of the uniqueRecordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueRecordSequenceNum() {
        return uniqueRecordSequenceNum;
    }

    /**
     * Sets the value of the uniqueRecordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueRecordSequenceNum(String value) {
        this.uniqueRecordSequenceNum = value;
    }

    /**
     * Gets the value of the correctedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedInd() {
        return correctedInd;
    }

    /**
     * Sets the value of the correctedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedInd(String value) {
        this.correctedInd = value;
    }

    /**
     * Gets the value of the correctedRecordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedRecordSequenceNum() {
        return correctedRecordSequenceNum;
    }

    /**
     * Sets the value of the correctedRecordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedRecordSequenceNum(String value) {
        this.correctedRecordSequenceNum = value;
    }

    /**
     * Gets the value of the payerDLN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerDLN() {
        return payerDLN;
    }

    /**
     * Sets the value of the payerDLN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerDLN(String value) {
        this.payerDLN = value;
    }

    /**
     * Gets the value of the payeeDLN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayeeDLN() {
        return payeeDLN;
    }

    /**
     * Sets the value of the payeeDLN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayeeDLN(String value) {
        this.payeeDLN = value;
    }

    /**
     * Gets the value of the taxYr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxYr() {
        return taxYr;
    }

    /**
     * Sets the value of the taxYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxYr(String value) {
        this.taxYr = value;
    }

    /**
     * Gets the value of the healthExchangeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthExchangeId() {
        return healthExchangeId;
    }

    /**
     * Sets the value of the healthExchangeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthExchangeId(String value) {
        this.healthExchangeId = value;
    }

    /**
     * Gets the value of the recipientPolicyDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RecipientPolicyDetailType }
     *     
     */
    public RecipientPolicyDetailType getRecipientPolicyDetail() {
        return recipientPolicyDetail;
    }

    /**
     * Sets the value of the recipientPolicyDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecipientPolicyDetailType }
     *     
     */
    public void setRecipientPolicyDetail(RecipientPolicyDetailType value) {
        this.recipientPolicyDetail = value;
    }

    /**
     * Gets the value of the recipientDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RecipientDetailType }
     *     
     */
    public RecipientDetailType getRecipientDetail() {
        return recipientDetail;
    }

    /**
     * Sets the value of the recipientDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecipientDetailType }
     *     
     */
    public void setRecipientDetail(RecipientDetailType value) {
        this.recipientDetail = value;
    }

    /**
     * Gets the value of the recipientSpouseDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RecipientSpouseDetailType }
     *     
     */
    public RecipientSpouseDetailType getRecipientSpouseDetail() {
        return recipientSpouseDetail;
    }

    /**
     * Sets the value of the recipientSpouseDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecipientSpouseDetailType }
     *     
     */
    public void setRecipientSpouseDetail(RecipientSpouseDetailType value) {
        this.recipientSpouseDetail = value;
    }

    /**
     * Gets the value of the coverageHouseholdGrp property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coverageHouseholdGrp property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoverageHouseholdGrp().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoverageHouseholdGrpType }
     * 
     * 
     */
    public List<CoverageHouseholdGrpType> getCoverageHouseholdGrp() {
        if (coverageHouseholdGrp == null) {
            coverageHouseholdGrp = new ArrayList<CoverageHouseholdGrpType>();
        }
        return this.coverageHouseholdGrp;
    }

    /**
     * Gets the value of the recipientPolicyInfoDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TaxHouseholdPolicyInformationGrpType }
     *     
     */
    public TaxHouseholdPolicyInformationGrpType getRecipientPolicyInfoDetail() {
        return recipientPolicyInfoDetail;
    }

    /**
     * Sets the value of the recipientPolicyInfoDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxHouseholdPolicyInformationGrpType }
     *     
     */
    public void setRecipientPolicyInfoDetail(TaxHouseholdPolicyInformationGrpType value) {
        this.recipientPolicyInfoDetail = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
