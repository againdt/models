/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * HIX-70722 Enrollment EnrolleeRelationshipTenantDTO
 * @author negi_s
 * @since 06/07/2015
 *
 */
public class EnrolleeRelationshipTenantDTO implements Serializable{
	
	private String memberId;
	private String relationshipCode;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getRelationshipCode() {
		return relationshipCode;
	}
	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}
	
	@Override
	public String toString() {
		return "EnrolleeRelationshipTenantDTO [memberId=" + memberId
				+ ", relationshipCode=" + relationshipCode + "]";
	}
}
