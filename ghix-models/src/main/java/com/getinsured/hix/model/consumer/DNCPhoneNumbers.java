package com.getinsured.hix.model.consumer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.getinsured.hix.model.TenantIdPrePersistListener;

@Entity
@Table(name = "dnc_number")
@DynamicInsert
@DynamicUpdate
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class DNCPhoneNumbers implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DNCPhoneNumbers_Seq")
	@SequenceGenerator(name = "DNCPhoneNumbers_Seq", sequenceName = "DNCPhoneNumbers_Seq", allocationSize = 1)
	@Column(name = "id")
	private Integer id;
	
	
	@Column(name = "phone_number", nullable = false)
	private String dncNumber;
	
	@Column(name = "status")
	private String status ;
	
	@Column(name="TENANT_ID")
    private Long tenantId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getdncNumber() {
		return dncNumber;
	}
	public void setdncNumber(String dncNumber) {
		this.dncNumber = dncNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Long getTenantId() {
		return tenantId;
	}
	
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
}
	  

