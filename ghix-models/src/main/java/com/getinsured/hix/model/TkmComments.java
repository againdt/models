/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author hardas_d
 *
 */
@Entity
@Table(name="tkm_comments")
public class TkmComments implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4992585286836846684L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TkmComments_Seq")
	@SequenceGenerator(name = "TkmComments_Seq", sequenceName = "tkm_comments_seq", allocationSize = 1)
	private Integer id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ticket_id")
	private TkmTickets ticket;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "commenttarget_id")	
	private CommentTarget commentTarget;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the ticket
	 */
	public TkmTickets getTicket() {
		return ticket;
	}

	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(TkmTickets ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the commentTarget
	 */
	public CommentTarget getCommentTarget() {
		return commentTarget;
	}

	/**
	 * @param commentTarget the commentTarget to set
	 */
	public void setCommentTarget(CommentTarget commentTarget) {
		this.commentTarget = commentTarget;
	}	
}
