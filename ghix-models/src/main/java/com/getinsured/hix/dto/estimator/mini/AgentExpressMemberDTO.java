package com.getinsured.hix.dto.estimator.mini;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class AgentExpressMemberDTO {

	private String firstname;
	private String lastname;
	private String socialSecurityNumber;
	private Boolean smoker;
	private Boolean needsCoverage;
	private Integer eligibility;
	private Integer existingCoverage;
	private Integer chipOptions;
	private Integer medicaidAssistance;
	private Integer taxFilingOptions;
	private Integer incomeOptions;
	private String birthDate;
	private Integer relationship;
	private Integer gender;
	private Boolean reconciledPtc;
	private String extraData;
	private Integer options;

	public AgentExpressMemberDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public Boolean getSmoker() {
		return smoker;
	}

	public void setSmoker(Boolean smoker) {
		this.smoker = smoker;
	}

	public Boolean getNeedsCoverage() {
		return needsCoverage;
	}

	public void setNeedsCoverage(Boolean needsCoverage) {
		this.needsCoverage = needsCoverage;
	}

	public Integer getEligibility() {
		return eligibility;
	}

	public void setEligibility(Integer eligibility) {
		this.eligibility = eligibility;
	}

	public Integer getExistingCoverage() {
		return existingCoverage;
	}

	public void setExistingCoverage(Integer existingCoverage) {
		this.existingCoverage = existingCoverage;
	}

	public Integer getChipOptions() {
		return chipOptions;
	}

	public void setChipOptions(Integer chipOptions) {
		this.chipOptions = chipOptions;
	}

	public Integer getMedicaidAssistance() {
		return medicaidAssistance;
	}

	public void setMedicaidAssistance(Integer medicaidAssistance) {
		this.medicaidAssistance = medicaidAssistance;
	}

	public Integer getTaxFilingOptions() {
		return taxFilingOptions;
	}

	public void setTaxFilingOptions(Integer taxFilingOptions) {
		this.taxFilingOptions = taxFilingOptions;
	}

	public Integer getIncomeOptions() {
		return incomeOptions;
	}

	public void setIncomeOptions(Integer incomeOptions) {
		this.incomeOptions = incomeOptions;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getRelationship() {
		return relationship;
	}

	public void setRelationship(Integer relationship) {
		this.relationship = relationship;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Boolean getReconciledPtc() {
		return reconciledPtc;
	}

	public void setReconciledPtc(Boolean reconciledPtc) {
		this.reconciledPtc = reconciledPtc;
	}

	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}

	public Integer getOptions() {
		return options;
	}

	public void setOptions(Integer options) {
		this.options = options;
	}
}
