package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @since 09th September 2014
 * model class for ADJUSTMENT_REASON_CODE table.
 *
 */
@Entity
@Table(name = "adjustment_reason_code")
public class AdjustmentReasonCode implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4879646907252597641L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "adjustment_reason_code_seq")
	@SequenceGenerator(name = "adjustment_reason_code_seq", sequenceName = "adjustment_reason_code_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "reason_type")
	private String reasonType;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReasonType() {
		return reasonType;
	}

	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}
}
