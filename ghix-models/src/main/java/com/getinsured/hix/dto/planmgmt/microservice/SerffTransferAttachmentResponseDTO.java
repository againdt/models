package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class SerffTransferAttachmentResponseDTO extends GhixResponseDTO {

	private Integer totalNumOfRecords;
	private List<SerffTransferAttachmentInfoDTO> serffTransferAttachmentInfoList;
	
	public SerffTransferAttachmentResponseDTO() {
		super();
	}
	
	public List<SerffTransferAttachmentInfoDTO> getSerffTransferAttachmentInfoList() {
		return serffTransferAttachmentInfoList;
	}

	public void setSerffTransferAttachmentInfo(SerffTransferAttachmentInfoDTO serffTransferAttachmentInfoDTO) {
		if (CollectionUtils.isEmpty(this.serffTransferAttachmentInfoList)) {
			this.serffTransferAttachmentInfoList = new ArrayList<SerffTransferAttachmentInfoDTO>();
		}
		this.serffTransferAttachmentInfoList.add(serffTransferAttachmentInfoDTO);
	}

	public Integer getTotalNumOfRecords() {
		return totalNumOfRecords;
	}

	public void setTotalNumOfRecords(Integer totalNumOfRecords) {
		this.totalNumOfRecords = totalNumOfRecords;
	}
	
}
