package com.getinsured.hix.ui.model.eligibility.prescreen;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * This class encapsulates the eleven income types for a household member. This
 * information corresponds to the income popup that shows up when the "Refine"
 * option is clicked on the Income tab, for any given user.
 * <p>
 * All the income values are default to 0.0.
 * </p>
 * 
 * @author Sunil Desu
 * @since April 16 2013
 */
public class PrescreenIncome {

	private String memberDesc;
	private double jobIncome;
	private double selfEmployment;
	private double socialSecurityBenefits;
	private double unemployment;
	private double retirementPension;
	private double capitalGains;
	private double investmentIncome;
	private double alimonyReceived;
	private double rentalRoyaltyIncome;
	private double farmingFishingIncome;
	private double otherIncome;

	public String getMemberDesc() {
		return memberDesc;
	}

	public void setMemberDesc(String memberDesc) {
		this.memberDesc = memberDesc;
	}

	public double getJobIncome() {
		return jobIncome;
	}

	public void setJobIncome(double jobIncome) {
		this.jobIncome = jobIncome;
	}

	public double getSelfEmployment() {
		return selfEmployment;
	}

	public void setSelfEmployment(double selfEmployment) {
		this.selfEmployment = selfEmployment;
	}

	public double getSocialSecurityBenefits() {
		return socialSecurityBenefits;
	}

	public void setSocialSecurityBenefits(double socialSecurityBenefits) {
		this.socialSecurityBenefits = socialSecurityBenefits;
	}

	public double getUnemployment() {
		return unemployment;
	}

	public void setUnemployment(double unemployment) {
		this.unemployment = unemployment;
	}

	public double getRetirementPension() {
		return retirementPension;
	}

	public void setRetirementPension(double retirementPension) {
		this.retirementPension = retirementPension;
	}

	public double getCapitalGains() {
		return capitalGains;
	}

	public void setCapitalGains(double capitalGains) {
		this.capitalGains = capitalGains;
	}

	public double getInvestmentIncome() {
		return investmentIncome;
	}

	public void setInvestmentIncome(double investmentIncome) {
		this.investmentIncome = investmentIncome;
	}

	public double getAlimonyReceived() {
		return alimonyReceived;
	}

	public void setAlimonyReceived(double alimonyReceived) {
		this.alimonyReceived = alimonyReceived;
	}

	public double getRentalRoyaltyIncome() {
		return rentalRoyaltyIncome;
	}

	public void setRentalRoyaltyIncome(double rentalRoyaltyIncome) {
		this.rentalRoyaltyIncome = rentalRoyaltyIncome;
	}

	public double getFarmingFishingIncome() {
		return farmingFishingIncome;
	}

	public void setFarmingFishingIncome(double farmingFishingIncome) {
		this.farmingFishingIncome = farmingFishingIncome;
	}

	public double getOtherIncome() {
		return otherIncome;
	}

	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}

	@JsonIgnore
	public double getTotalIncome() {
		return this.jobIncome + this.selfEmployment
				+ this.socialSecurityBenefits + this.unemployment
				+ this.retirementPension + this.capitalGains
				+ this.investmentIncome + this.alimonyReceived
				+ this.rentalRoyaltyIncome + this.farmingFishingIncome
				+ this.otherIncome;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrescreenIncome [memberDesc=");
		builder.append(memberDesc);
		builder.append(", jobIncome=");
		builder.append(jobIncome);
		builder.append(", selfEmployment=");
		builder.append(selfEmployment);
		builder.append(", socialSecurityBenefits=");
		builder.append(socialSecurityBenefits);
		builder.append(", unemployment=");
		builder.append(unemployment);
		builder.append(", retirementPension=");
		builder.append(retirementPension);
		builder.append(", capitalGains=");
		builder.append(capitalGains);
		builder.append(", investmentIncome=");
		builder.append(investmentIncome);
		builder.append(", alimonyReceived=");
		builder.append(alimonyReceived);
		builder.append(", rentalRoyaltyIncome=");
		builder.append(rentalRoyaltyIncome);
		builder.append(", farmingFishingIncome=");
		builder.append(farmingFishingIncome);
		builder.append(", otherIncome=");
		builder.append(otherIncome);
		builder.append(", getTotalIncome()=");
		builder.append(getTotalIncome());
		builder.append("]");
		return builder.toString();
	}
}
