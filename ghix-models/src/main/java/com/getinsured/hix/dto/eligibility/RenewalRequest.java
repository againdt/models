package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to send request for renewal status API.
 */
public class RenewalRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String applicationId; 
	private String year; 

	public RenewalRequest() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
}
