package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * The persistent class for the IRS Outbound Transmission database table.
 * 
 */

@Entity
@Table(name="IRS_OUTBOUND_TRANSMISSION")
public class IRSOutboundTransmission implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IRS_OUTBOUND_TRANSMISSION_SEQ")
	@SequenceGenerator(name = "IRS_OUTBOUND_TRANSMISSION_SEQ", sequenceName = "IRS_OUTBOUND_TRANSMISSION_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name="DOCUMENT_FILE_NAME")
	private String documentFileName ;
	
	@Column(name="HOUSEHOLD_CASE_ID")
	private String householdCaseID ;
	
	@Column(name="BATCH_ID")
	private String batchId ;  
	
	@Column(name="SUBMISSION_TYPE")
	private String submissionType ;

	@Column(name="MONTH")
	private String month ;		

	@Column(name="DOCUMENT_FILE_SIZE")
	private String documentFileSize ;

 	@Column(name="IS_ACK_RECEIVED")
    private String isAckReceived ;

 	
 	@Column(name="DOCUMENT_SEQ_ID")
    private String documentSeqId ;
 	
 	@Column(name="TOTAL_HOUSEHOLD_PERXML")
    private Integer totalHouseholdPerXML ;

	@Column(name="ACK_RESP_CODE")
    private String ACKResponseCode ;
 	
	@Column(name="YEAR")
	private String year ;
 	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
 	
 	
	public Integer getTotalHouseholdPerXML() {
		return totalHouseholdPerXML;
	}

	public void setTotalHouseholdPerXML(Integer totalHouseholdPerXML) {
		this.totalHouseholdPerXML = totalHouseholdPerXML;
	}

	public String getDocumentSeqId() {
		return documentSeqId;
	}

	public void setDocumentSeqId(String documentSeqId) {
		this.documentSeqId = documentSeqId;
	}

	public String getIsAckReceived() {
		return isAckReceived;
	}

	public void setIsAckReceived(String isAckReceived) {
		this.isAckReceived = isAckReceived;
	}

	public String getACKResponseCode() {
		return ACKResponseCode;
	}

	public void setACKResponseCode(String aCKResponseCode) {
		ACKResponseCode = aCKResponseCode;
	}

	public String getSubmissionType() {
		return submissionType;
	}

	public void setSubmissionType(String submissionType) {
		this.submissionType = submissionType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	public String getHouseholdCaseID() {
		return householdCaseID;
	}

	public void setHouseholdCaseID(String householdCaseID) {
		this.householdCaseID = householdCaseID;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDocumentFileSize() {
		return documentFileSize;
	}

	public void setDocumentFileSize(String documentFileSize) {
		this.documentFileSize = documentFileSize;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}
}
