/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.util.List;

import com.getinsured.hix.model.GHIXResponse;


/**
 * This is the response object of issuer service
 * 
 * @author gajulapalli_k
 * 
 */
public class IssuerResponse extends GHIXResponse{
		
	private String disclaimerInfo;
	
	private List issuerList;

	public IssuerResponse() {
	}

	/**
	 * @return the disclaimerInfo
	 */
	public String getDisclaimerInfo() {
		return disclaimerInfo;
	}

	/**
	 * @param disclaimerInfo
	 *            the disclaimerInfo to set
	 */
	public void setDisclaimerInfo(String disclaimerInfo) {
		this.disclaimerInfo = disclaimerInfo;
	}

	public List getIssuerList() {
		return issuerList;
	}

	public void setIssuerList(List issuerList) {
		this.issuerList = issuerList;
	}
	


}
