/**
 * PlanRateBenefitResponse.java
 * @author chalse_v
 * @version 1.0
 * @since Mar 20, 2013 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.plandisplay.PlanDentalBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthCostDTO;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.PlanRateBenefit;

/**
 * 
 * @author chalse_v
 * @version 1.0
 * @since Mar 20, 2013 
 *
 */

@Component("planRateBenefitResponse")
public class PlanRateBenefitResponse extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Attribute Map<String,Object> responseData
	 */
	private Map<String, Object> responseData = new HashMap<String, Object>();

	/**
	 * Constructor.
	 */
	public PlanRateBenefitResponse() {
		this.startResponse();
	}
	
	private List<PlanRateBenefit> planRateBenefitList;
	
	
	private List<PlanHealthBenefitDTO> planHealthBenefitDTOList;
	
	
	private List<PlanHealthCostDTO> planHealthCostDTOList;
	
	
	private List<PlanDentalBenefitDTO> planDentalBenefitDTOList;
	
	
	private List<PlanDentalCostDTO> planDentalCostDTOList;
	
	private PlanDTO planInfoDTO; 
	
	private boolean benefitsModified;
	
	private String networkName;
	
	/**
	 * Getter for 'responseData'.
	 *
	 * @return the responseData.
	 */
	public Map<String, Object> getResponseData() {
		return this.responseData;
	}

	/**
	 * Setter for 'responseData'.
	 *
	 * @param responseData the responseData to set.
	 */
	public void setResponseData(Map<String, Object> responseData) {
		this.responseData = responseData;
	}

	/**
	 * Method to add entries into responseData collection.
	 *
	 * @return void
	 */
	public void addResponseData(String key, Object data) {
		this.responseData.put(key, data);
	}

	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse(){
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON. 
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse(){
		String transformedResponse = null;
		
		this.endResponse();
		
		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
		    public HierarchicalStreamWriter createWriter(Writer writer) {
		        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		    }
		});
		
		jsoner.setMode(XStream.NO_REFERENCES);
		
		Validate.notNull(jsoner, "XStream instance improperly confugured.");
		
		transformedResponse = jsoner.toXML(this);
		
		return transformedResponse;
	}
*/
	public List<PlanRateBenefit> getPlanRateBenefitList() {
		return planRateBenefitList;
	}

	public void setPlanRateBenefitList(List<PlanRateBenefit> planRateBenefitList) {
		this.planRateBenefitList = planRateBenefitList;
	}

	public List<PlanHealthBenefitDTO> getPlanHealthBenefitDTOList() {
		return planHealthBenefitDTOList;
	}

	public void setPlanHealthBenefitDTOList(
			List<PlanHealthBenefitDTO> planHealthBenefitDTOList) {
		this.planHealthBenefitDTOList = planHealthBenefitDTOList;
	}

	public List<PlanHealthCostDTO> getPlanHealthCostDTOList() {
		return planHealthCostDTOList;
	}

	public void setPlanHealthCostDTOList(
			List<PlanHealthCostDTO> planHealthCostDTOList) {
		this.planHealthCostDTOList = planHealthCostDTOList;
	}

	public List<PlanDentalBenefitDTO> getPlanDentalBenefitDTOList() {
		return planDentalBenefitDTOList;
	}

	public void setPlanDentalBenefitDTOList(
			List<PlanDentalBenefitDTO> planDentalBenefitDTOList) {
		this.planDentalBenefitDTOList = planDentalBenefitDTOList;
	}

	public List<PlanDentalCostDTO> getPlanDentalCostDTOList() {
		return planDentalCostDTOList;
	}

	public void setPlanDentalCostDTOList(
			List<PlanDentalCostDTO> planDentalCostDTOList) {
		this.planDentalCostDTOList = planDentalCostDTOList;
	}

	/**
	 * @return the planInfoDTO
	 */
	public PlanDTO getPlanInfoDTO() {
		return planInfoDTO;
	}

	/**
	 * @param planInfoDTO the planInfoDTO to set
	 */
	public void setPlanInfoDTO(PlanDTO planInfoDTO) {
		this.planInfoDTO = planInfoDTO;
	}

	/**
	 * @return the benefitsModified
	 */
	public boolean isBenefitsModified() {
		return benefitsModified;
	}

	/**
	 * @param benefitsModified the benefitsModified to set
	 */
	public void setBenefitsModified(boolean benefitsModified) {
		this.benefitsModified = benefitsModified;
	}

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	
	
}
