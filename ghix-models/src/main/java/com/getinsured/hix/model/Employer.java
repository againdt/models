package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for the employers database table.
 *
 */
@Audited
@Entity
@Table(name = "employers")
@XmlAccessorType(XmlAccessType.NONE)
public class Employer implements Serializable {
	private static final long serialVersionUID = 1L;

	public static enum OrgType
	{
		CORPORATION("Corporation"),
		NON_PROFIT("Non-Profit"),
		SOLE_PROPRIETORSHIP("Sole Proprietorship"),
		PARTNERSHIP("Partnership");

		private final String type;
		private OrgType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		@Override
		public String toString() {
			return type;
		}
	}

	public static enum EligibilityStatus{
		NEW("New"),
		APPROVED("Approved"),
		DENIED("Denied"),
		CONDITIONALLY_APPROVED("Conditionally Approved"),
		WITHDRAWN("Withdrawn"),
		EXPIRED("Expired");

		private final String type;
		private EligibilityStatus(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		@Override
		public String toString() {
			return type;
		}
	}


	public interface EmployerCompanyInfoGroup extends Default {
	}

	public interface EmployerContactInfoGroup extends Default {
	}

	public interface EmployerAttestationGroup extends Default {
	}

	public interface EmployerEditContactInfoCRMGroup extends Default {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Employer_Seq")
	@SequenceGenerator(name = "Employer_Seq", sequenceName = "employers_seq", allocationSize = 1)
	@XmlElement(name="id")
	private int id;

	@Column(name = "external_id")
	private String externalId;

	@Column(name = "tax_credit")
	private Float taxCredit;

	@NotNull(message="{label.validateorgtype}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Column(name = "org_type")
	@Enumerated(EnumType.STRING)
	private OrgType orgType;

	@Pattern(regexp="[0-9A-Za-z '.,-]+$",message="{label.validateName}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Size(min=2, max=50,message="{label.validateCompanyNameSize}",groups=Employer.EmployerCompanyInfoGroup.class)
	@NotEmpty(message="{label.validateCompanyNameSize}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Column(name = "name")
	private String name;

	@NotEmpty(message="{label.validateempfirstname}",groups=Employer.EmployerContactInfoGroup.class)
	@Column(name="contact_first_name")
	private String contactFirstName;

	@NotEmpty(message="{label.validateemplastname}",groups=EmployerContactInfoGroup.class)
	@Column(name="contact_last_name")
	private String contactLastName;

	@NotEmpty(message="{label.validateemail}",groups={Employer.EmployerContactInfoGroup.class,Employer.EmployerEditContactInfoCRMGroup.class})
	@Email(message="{label.validateemail}",groups={Employer.EmployerContactInfoGroup.class,Employer.EmployerEditContactInfoCRMGroup.class})
	@Size(max=50,message="{label.validateemaillength}",groups={Employer.EmployerContactInfoGroup.class,Employer.EmployerEditContactInfoCRMGroup.class})
	@Column(name="contact_email")
	private String contactEmail;

	@Pattern(regexp="^(\\([0-9]{3}\\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$",message="{label.validateempvtelephone}",groups=Employer.EmployerCompanyInfoGroup.class)
	@NotEmpty(message="{label.validateempvtelephone}",groups={Employer.EmployerContactInfoGroup.class,Employer.EmployerEditContactInfoCRMGroup.class})
	@Column(name="contact_phone_number")
	private String contactNumber;

	@Column(name="communication_pref")
	private String communicationPref;

	@Pattern(regexp="|[0-9]{9}",message="{label.validateeinnumber}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Column(name = "federal_ein")
	private String federalEIN;

	@Column(name = "state_ein")
	private String stateEIN;

	@Column(name = "total_employees")
	private Integer totalEmployees;

	@NotNull(message="{label.validatefulltimeempnumber}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Min(value=0,message="{label.validatefulltimeempnumber}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Column(name="fulltime_emp_count")
	private Integer fullTimeEmp;

	@Column(name="part_time_emp")
	private Integer partTimeEmp;

	@Column(name="seasonal_emp")
	private Integer seasonalEmp;

	@NotEmpty(message="{label.validateEsignUserName}",groups=Employer.EmployerAttestationGroup.class)
	@Column(name="esign_by")
	private String esignBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="esign_date")
	private Date esignDate;

	@Column(name = "eligibility_status")
	@Enumerated(EnumType.STRING)
	private EligibilityStatus eligibilityStatus;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "eligibility_status_timestamp")
	private Date eligibilityStatusTimestamp;


	@Column(name = "last_visited")
	private String lastVisited;

	@Column(name = "eligibity_decision_factor_note")
	private String eligibityDecisionFactorNote;

	@Valid
	@OneToOne
	@JoinColumn(name="contact_location")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Location contactLocation;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable=false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="last_updated_by")
    private Integer updatedBy;

	@Column(name="auto_pay")
    private String autoPay;

	@Pattern(regexp="^(\\$)?([0-9\\,])+(\\.\\d{0,5})?$",message="{label.validateavgsalaryformat}",groups=Employer.EmployerCompanyInfoGroup.class)
	@Column(name="average_salary")
    private String averageSalary;

	// bi-directional many-to-one association to EmployerDetails

	@Valid
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "employer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<EmployerLocation> locations;
	
	@NotAudited
	@Column(name="AFF_FLOW_ACCESS_ID")
	private Long affFlowAccessID;
	
	@NotAudited
	@Column(name="AFF_AFFILIATE_ID")
	private Long affiliateId;
	
	@NotAudited
	@Column(name="AFF_FLOW_ID")
	private Integer affiliateFlowId;

	public Employer() {
		this.setEligibilityStatus(EligibilityStatus.NEW);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public Float getTaxCredit() {
		return taxCredit;
	}

	public void setTaxCredit(Float taxCredit) {
		this.taxCredit = taxCredit;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public OrgType getOrgType() {
		return orgType;
	}

	public void setOrgType(OrgType orgType) {
		this.orgType = orgType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactEmail() {
		return StringUtils.lowerCase(contactEmail);
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}


	public String getCommunicationPref() {
		return communicationPref;
	}

	public void setCommunicationPref(String communicationPref) {
		this.communicationPref = communicationPref;
	}

	public String getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(String federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getStateEIN() {
		return stateEIN;
	}

	public void setStateEIN(String stateEIN) {
		this.stateEIN = stateEIN;
	}

	public Integer getTotalEmployees() {
		return totalEmployees;
	}

	public void setTotalEmployees(Integer totalEmployees) {
		this.totalEmployees = totalEmployees;
	}

	public Integer getFullTimeEmp() {
		return this.fullTimeEmp;
	}

	public void setFullTimeEmp(Integer fullTimeEmp) {
		this.fullTimeEmp = fullTimeEmp;
	}

	public Integer getPartTimeEmp() {
		return this.partTimeEmp;
	}

	public void setPartTimeEmp(Integer partTimeEmp) {
		this.partTimeEmp = partTimeEmp;
	}

	public Integer getSeasonalEmp() {
		return this.seasonalEmp;
	}

	public void setSeasonalEmp(Integer seasonalEmp) {
		this.seasonalEmp = seasonalEmp;
	}

	public String getEsignBy() {
		return esignBy;
	}

	public void setEsignBy(String esignBy) {
		this.esignBy = esignBy;
	}

	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}

	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public Date getEligibilityStatusTimestamp() {
		return eligibilityStatusTimestamp;
	}

	public void setEligibilityStatusTimestamp(Date eligibilityStatusTimestamp) {
		this.eligibilityStatusTimestamp = eligibilityStatusTimestamp;
	}

	public String getLastVisited() {
		return lastVisited;
	}

	public void setLastVisited(String lastVisited) {
		this.lastVisited = lastVisited;
	}

	public Location getContactLocation() {
		return contactLocation;
	}

	public void setContactLocation(Location contactLocation) {
		this.contactLocation = contactLocation;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public List<EmployerLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<EmployerLocation> locations) {
		this.locations = locations;
	}

	public String getEligibityDecisionFactorNote() {
		return eligibityDecisionFactorNote;
	}

	public void setEligibityDecisionFactorNote(String eligibityDecisionFactorNote) {
		this.eligibityDecisionFactorNote = eligibityDecisionFactorNote;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		if(this.getEligibilityStatus() == null){
			this.setEligibilityStatus(EligibilityStatus.NEW);
			this.setEligibilityStatusTimestamp(new TSDate());
		}
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	public String getAutoPay() {
		return autoPay;
	}

	public void setAutoPay(String autoPay) {
		this.autoPay = autoPay;
	}

	public String getAverageSalary() {
		return averageSalary;
	}

	public void setAverageSalary(String averageSalary) {
		this.averageSalary = averageSalary;
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		if(this.getEligibilityStatus() == null){
			this.setEligibilityStatus(EligibilityStatus.NEW);
		}
		this.setUpdated(new TSDate());
	}

	public Long getAffFlowAccessID() {
		return affFlowAccessID;
	}

	public void setAffFlowAccessID(Long affFlowAccessID) {
		this.affFlowAccessID = affFlowAccessID;
	}
	@Override
	public  String toString() {
        StringBuilder sb = new  StringBuilder();
        sb.append("Employer DB");
        sb.append("id=").append(id).append(", ");
        sb.append("externalId=").append(externalId).append(", ");
        String orgTypeStr = (orgType == null) ? "null" : orgType.getType();
        sb.append("orgType=").append(orgTypeStr).append(", ");
        sb.append("name=").append(name).append(", ");
        sb.append("federalEIN=").append(federalEIN).append(", ");
        sb.append("stateEIN=").append(stateEIN).append(", ");
        sb.append("totalEmployees=").append(totalEmployees).append(", ");
        String locationStr = (locations == null) ? "null" : locations.toString();
        sb.append("locations=").append(locationStr).append(", ");
        sb.append("eligibilityStatus=").append(eligibilityStatus).append(", ");
        sb.append("eligibilityStatusTimestamp=").append(eligibilityStatusTimestamp).append(", ");
        sb.append("lastVisited=").append(lastVisited).append(", ");
        sb.append("created=").append(created).append(", ");
        sb.append("updated=").append(updated).append(", ");
        return  sb.toString();
	}

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	

}
