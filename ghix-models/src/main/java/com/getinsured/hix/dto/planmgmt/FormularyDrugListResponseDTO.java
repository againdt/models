package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class FormularyDrugListResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private String issuerName;
	private String issuerHIOSId;
	private String applicableYear;
	private String formularyId;
	private List<FormularyDrugItemDTO> formularyDrugItemList;
	private List<FormularyPlanDTO> formularyPlanList;
	
	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	public String getIssuerHIOSId() {
		return issuerHIOSId;
	}
	
	public void setIssuerHIOSId(String issuerHIOSId) {
		this.issuerHIOSId = issuerHIOSId;
	}
	
	public String getApplicableYear() {
		return applicableYear;
	}
	
	public void setApplicableYear(String applicableYear) {
		this.applicableYear = applicableYear;
	}
	
	public String getFormularyId() {
		return formularyId;
	}
	
	public void setFormularyId(String formularyId) {
		this.formularyId = formularyId;
	}
	
	public List<FormularyDrugItemDTO> getFormularyDrugItemList() {
		return formularyDrugItemList;
	}
	
	public void addFormularyDrugItemToList(FormularyDrugItemDTO formularyDrugItem) {
		if(null == this.getFormularyDrugItemList()) {
			this.setFormularyDrugItemList(new ArrayList<FormularyDrugItemDTO>());
		}
		this.getFormularyDrugItemList().add(formularyDrugItem);
	}
	
	public void setFormularyDrugItemList(List<FormularyDrugItemDTO> formularyDrugItemList) {
		this.formularyDrugItemList = formularyDrugItemList;
	}
	
	public List<FormularyPlanDTO> getFormularyPlanList() {
		return formularyPlanList;
	}
	
	public void addFormularyPlanToList(FormularyPlanDTO formularyPlan) {
		if(null == this.getFormularyPlanList()) {
			this.setFormularyPlanList(new ArrayList<FormularyPlanDTO>());
		}
		this.getFormularyPlanList().add(formularyPlan);
	}
	
	public void setFormularyPlanList(List<FormularyPlanDTO> formularyPlanList) {
		this.formularyPlanList = formularyPlanList;
	}
}
