
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Coverage Policy Information&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for PolicyDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}QHPPolicyNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}QHPIssuerEIN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}IssuerNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}PolicyCoverageStartDt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}PolicyCoverageEndDt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "qhpPolicyNum",
    "qhpIssuerEIN",
    "issuerNm",
    "policyCoverageStartDt",
    "policyCoverageEndDt"
})
public class PolicyDetailType {

    @XmlElement(name = "QHPPolicyNum", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String qhpPolicyNum;
    @XmlElement(name = "QHPIssuerEIN", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String qhpIssuerEIN;
    @XmlElement(name = "IssuerNm", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String issuerNm;
    @XmlElement(name = "PolicyCoverageStartDt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar policyCoverageStartDt;
    @XmlElement(name = "PolicyCoverageEndDt", namespace = "urn:us:gov:treasury:irs:common")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar policyCoverageEndDt;

    /**
     * Gets the value of the qhpPolicyNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQHPPolicyNum() {
        return qhpPolicyNum;
    }

    /**
     * Sets the value of the qhpPolicyNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQHPPolicyNum(String value) {
        this.qhpPolicyNum = value;
    }

    /**
     * Gets the value of the qhpIssuerEIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQHPIssuerEIN() {
        return qhpIssuerEIN;
    }

    /**
     * Sets the value of the qhpIssuerEIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQHPIssuerEIN(String value) {
        this.qhpIssuerEIN = value;
    }

    /**
     * Gets the value of the issuerNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerNm() {
        return issuerNm;
    }

    /**
     * Sets the value of the issuerNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerNm(String value) {
        this.issuerNm = value;
    }

    /**
     * Gets the value of the policyCoverageStartDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyCoverageStartDt() {
        return policyCoverageStartDt;
    }

    /**
     * Sets the value of the policyCoverageStartDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyCoverageStartDt(XMLGregorianCalendar value) {
        this.policyCoverageStartDt = value;
    }

    /**
     * Gets the value of the policyCoverageEndDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyCoverageEndDt() {
        return policyCoverageEndDt;
    }

    /**
     * Sets the value of the policyCoverageEndDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyCoverageEndDt(XMLGregorianCalendar value) {
        this.policyCoverageEndDt = value;
    }

}
