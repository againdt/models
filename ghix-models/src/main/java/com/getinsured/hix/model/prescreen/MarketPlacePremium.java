package com.getinsured.hix.model.prescreen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.getinsured.hix.model.RatingArea;

@Entity
@Table(name="MARKETPLACE_PREMIUM")
@DynamicInsert
@DynamicUpdate
public class MarketPlacePremium implements Serializable{

	private static final long serialVersionUID = 4376872127315053281L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MARKETPLACE_PREMIUM_SEQ")
	@SequenceGenerator(name = "MARKETPLACE_PREMIUM_SEQ", sequenceName = "MARKETPLACE_PREMIUM_SEQ", allocationSize = 1)
	private long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RATING_AREA_ID")
	private RatingArea ratingArea;
	
	@Column(name="ISSURES_NUM")
	private int numOfIssuers;
	
	@Column(name="QHP_NUM")
	private int numOfQhps;
	
	@Column(name="CAT_QHP_NUM")
	private int numOfCatQhps;
	
	@Column(name="BRONZE_QHP_NUM")
	private int numOfBronzeQhps;
	
	@Column(name="SILVER_QHP_NUM")
	private int numOfSilverQhps;
	
	@Column(name="GOLD_QHP_NUM")
	private int numOfGoldQhps;
	
	@Column(name="PLATINUM_QHP_NUM")
	private int numOfPlatQhps;
	
	@Column(name="CAT_QHP_COST")
	private double catQhpCost;
	
	@Column(name="SILVER_QHP_COST")
	private double silverQhpCost;
	
	@Column(name="BRONZE_QHP_COST")
	private double bronzeQhpCost;
	
	@Column(name="SEC_SILVER_QHP_COST")
	private double secondSilverQhpCost;
	
	@Column(name="GOLD_QHP_COST")
	private double goldQhpCost;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the RatingArea
	 */
	public RatingArea getRatingArea() {
		return ratingArea;
	}

	/**
	 * @param RatingArea the RatingArea to set
	 */
	public void setRatingArea(RatingArea ratingArea) {
		this.ratingArea = ratingArea;
	}

	/**
	 * @return the numOfIssuers
	 */
	public int getNumOfIssuers() {
		return numOfIssuers;
	}

	/**
	 * @param numOfIssuers the numOfIssuers to set
	 */
	public void setNumOfIssuers(int numOfIssuers) {
		this.numOfIssuers = numOfIssuers;
	}

	/**
	 * @return the numOfQhps
	 */
	public int getNumOfQhps() {
		return numOfQhps;
	}

	/**
	 * @param numOfQhps the numOfQhps to set
	 */
	public void setNumOfQhps(int numOfQhps) {
		this.numOfQhps = numOfQhps;
	}

	/**
	 * @return the numOfCatQhps
	 */
	public int getNumOfCatQhps() {
		return numOfCatQhps;
	}

	/**
	 * @param numOfCatQhps the numOfCatQhps to set
	 */
	public void setNumOfCatQhps(int numOfCatQhps) {
		this.numOfCatQhps = numOfCatQhps;
	}

	/**
	 * @return the numOfBronzeQhps
	 */
	public int getNumOfBronzeQhps() {
		return numOfBronzeQhps;
	}

	/**
	 * @param numOfBronzeQhps the numOfBronzeQhps to set
	 */
	public void setNumOfBronzeQhps(int numOfBronzeQhps) {
		this.numOfBronzeQhps = numOfBronzeQhps;
	}

	/**
	 * @return the numOfSilverQhps
	 */
	public int getNumOfSilverQhps() {
		return numOfSilverQhps;
	}

	/**
	 * @param numOfSilverQhps the numOfSilverQhps to set
	 */
	public void setNumOfSilverQhps(int numOfSilverQhps) {
		this.numOfSilverQhps = numOfSilverQhps;
	}

	/**
	 * @return the numOfGoldQhps
	 */
	public int getNumOfGoldQhps() {
		return numOfGoldQhps;
	}

	/**
	 * @param numOfGoldQhps the numOfGoldQhps to set
	 */
	public void setNumOfGoldQhps(int numOfGoldQhps) {
		this.numOfGoldQhps = numOfGoldQhps;
	}

	/**
	 * @return the numOfPlatQhps
	 */
	public int getNumOfPlatQhps() {
		return numOfPlatQhps;
	}

	/**
	 * @param numOfPlatQhps the numOfPlatQhps to set
	 */
	public void setNumOfPlatQhps(int numOfPlatQhps) {
		this.numOfPlatQhps = numOfPlatQhps;
	}

	/**
	 * @return the catQhpCost
	 */
	public double getCatQhpCost() {
		return catQhpCost;
	}

	/**
	 * @param catQhpCost the catQhpCost to set
	 */
	public void setCatQhpCost(double catQhpCost) {
		this.catQhpCost = catQhpCost;
	}

	/**
	 * @return the silverQhpCost
	 */
	public double getSilverQhpCost() {
		return silverQhpCost;
	}

	/**
	 * @param silverQhpCost the silverQhpCost to set
	 */
	public void setSilverQhpCost(double silverQhpCost) {
		this.silverQhpCost = silverQhpCost;
	}

	/**
	 * @return the bronzeQhpCost
	 */
	public double getBronzeQhpCost() {
		return bronzeQhpCost;
	}

	/**
	 * @param bronzeQhpCost the bronzeQhpCost to set
	 */
	public void setBronzeQhpCost(double bronzeQhpCost) {
		this.bronzeQhpCost = bronzeQhpCost;
	}

	/**
	 * @return the secondSilverQhpCost
	 */
	public double getSecondSilverQhpCost() {
		return secondSilverQhpCost;
	}

	/**
	 * @param secondSilverQhpCost the secondSilverQhpCost to set
	 */
	public void setSecondSilverQhpCost(double secondSilverQhpCost) {
		this.secondSilverQhpCost = secondSilverQhpCost;
	}

	/**
	 * @return the goldQhpCost
	 */
	public double getGoldQhpCost() {
		return goldQhpCost;
	}

	/**
	 * @param goldQhpCost the goldQhpCost to set
	 */
	public void setGoldQhpCost(double goldQhpCost) {
		this.goldQhpCost = goldQhpCost;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
