/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.model.GHIXRequest;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * @author Priya C
 *
 */
public class ManualEnrollmentRequest extends GHIXRequest implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer cmrHouseholdID;
	private String issuerName;
	private String exchangeType;
	private String enrollmentModality;
	private String planName;
    private Float premium;
    private String planLevel;
    private String confirmationNumber;
    private Float aptc ;
    private Float netPremium ;
    private String policyNumber;
    private Long applicationId;
    private String carrierAppId; 
    private Integer planId;
    private String  insuranceType;
    private String prefferedTimeToContact;
    private String stateExchangeCode;
    private String subscriberName;
    private String sponsorName;
    private int issuerId;
    private String carrierAppUrl;
    private Integer termLength;
    private Float benefitAmount;
	private String hiosIssuerId;
    
    private List<SsapApplicantDTO> applicantDtoList;
    
    
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public String getEnrollmentModality() {
		return enrollmentModality;
	}
	public void setEnrollmentModality(String enrollmentModality) {
		this.enrollmentModality = enrollmentModality;
	}
	public Float getAptc() {
		return aptc;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public Float getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}
	private Date effectiveDate;

	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public Integer getCmrHouseholdID() {
		return cmrHouseholdID;
	}
	public void setCmrHouseholdID(Integer cmrHouseholdID) {
		this.cmrHouseholdID = cmrHouseholdID;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

    public Long getApplicationId() {
		return applicationId;
	}
   	
    public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
    
    public List<SsapApplicantDTO> getApplicantDtoList() {
		return applicantDtoList;
	}
    
    public void setApplicantDtoList(List<SsapApplicantDTO> applicantDtoList) {
		this.applicantDtoList = applicantDtoList;
	}
	public String getCarrierAppId() {
		return carrierAppId;
	}
	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getPrefferedTimeToContact() {
		return prefferedTimeToContact;
	}
	public void setPrefferedTimeToContact(String prefferedTimeToContact) {
		this.prefferedTimeToContact = prefferedTimeToContact;
	}
	public String getStateExchangeCode() {
		return stateExchangeCode;
	}
	public void setStateExchangeCode(String stateExchangeCode) {
		this.stateExchangeCode = stateExchangeCode;
	}
	public String getSubscriberName() {
		return subscriberName;
	}
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	public String getSponsorName() {
		return sponsorName;
	}
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	public String getCarrierAppUrl() {
		return carrierAppUrl;
	}
	public void setCarrierAppUrl(String carrierAppUrl) {
		this.carrierAppUrl = carrierAppUrl;
	}
	public Integer getTermLength() {
		return termLength;
	}
	public void setTermLength(Integer termLength) {
		this.termLength = termLength;
	}
	public Float getBenefitAmount() {
		return benefitAmount;
	}
	public void setBenefitAmount(Float benefitAmount) {
		this.benefitAmount = benefitAmount;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

}
