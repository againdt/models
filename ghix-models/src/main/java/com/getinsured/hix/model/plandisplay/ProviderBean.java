package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ProviderBean implements Serializable{
	private String id;
	private String name;
	private String networkId;
	private String networkTier;
	private String specialty;
	private String phone;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String locationId;
	private String providerType;
	private List<String> networkTierList;
	private List<String> networkIdList;
	private Map<String, String> identifiers;
	private String currentNetworkStatus;
	private String groupKey;
	
	private static final long serialVersionUID = 1L;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkTier() {
		return networkTier;
	}
	public void setNetworkTier(String networkTier) {
		this.networkTier = networkTier;
	}
	public String getSpecialty() {
		return specialty;
	}
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getProviderType() {
		return providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
		public void setNetworkTierList(List<String> networkTierList) {
		this.networkTierList = networkTierList;
	}
		public void setNetworkIdList(List<String> networkIdList) {
		this.networkIdList = networkIdList;
	}
		
	public Map<String, String> getIdentifiers() {
		return identifiers;
	}
	
	public void setIdentifiers(Map<String, String> identifiers) {
		this.identifiers = identifiers;
	}
	
	public String getCurrentNetworkStatus() {
		return currentNetworkStatus;
	}

	public void setCurrentNetworkStatus(String currentNetworkStatus) {
		this.currentNetworkStatus = currentNetworkStatus;
	}
	
	public  List<String> getNetworkIdList(){
		if(!"".equalsIgnoreCase(this.networkId)){
			String[] networkIdArr = this.networkId.split(",");
			for (int i = 0; i < networkIdArr.length; i++){
				networkIdArr[i] = networkIdArr[i].trim();
			}
			this.networkIdList=Arrays.asList(networkIdArr);
		}
		return this.networkIdList;	
	}
	
	public  List<String> getNetworkTierList(){
		if(networkTierList==null){
			if(this.networkTier!=null &&  this.networkTier.contains("[") && this.networkTier.contains("]")) {
					String temp=this.networkTier.replaceAll("\\[", "").replaceAll("\\]", "");
					String networkTierArr[]=temp.split(",");
					this.networkTierList=Arrays.asList(networkTierArr);
			}
		}
	 return networkTierList;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getLocationId() {
		return locationId;
	}
	
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return name.length()*31;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj!=null && ( obj instanceof ProviderBean)){
			ProviderBean pb=(ProviderBean)	obj;
			{
				return true;
			}
		}
		
		return false;
	}
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	
}
