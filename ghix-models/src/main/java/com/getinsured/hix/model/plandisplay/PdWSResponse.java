package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PlanTab;

public class PdWSResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String hiosId;
	private float estimatedCost;
	private Integer score;
	private List<Provider> providers;
	private List<Drug> drugs;
	private List<Benefit> benefits;	
	private String expenseEstimate;
	private PlanTab planTab;
	
	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public float getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(float estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public List<Provider> getProviders() {
		return providers;
	}

	public void setProviders(List<Provider> providers) {
		this.providers = providers;
	}

	public List<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<Drug> drugs) {
		this.drugs = drugs;
	}

	public List<Benefit> getBenefits() {
		return benefits;
	}

	public void setBenefits(List<Benefit> benefits) {
		this.benefits = benefits;
	}

	public String getExpenseEstimate() {
		return expenseEstimate;
	}

	public void setExpenseEstimate(String expenseEstimate) {
		this.expenseEstimate = expenseEstimate;
	}

	public PlanTab getPlanTab() {
		return planTab;
	}

	public void setPlanTab(PlanTab planTab) {
		this.planTab = planTab;
	}


	public class Provider{
		private String id;
		private String name;
		private String coveredByPlan;
		private String locationId;
		private String providerType;
		private String city;
		private String state;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCoveredByPlan() {
			return coveredByPlan;
		}
		public void setCoveredByPlan(String coveredByPlan) {
			this.coveredByPlan = coveredByPlan;
		}
		public String getLocationId() {
			return locationId;
		}
		public void setLocationId(String locationId) {
			this.locationId = locationId;
		}
		public String getProviderType() {
			return providerType;
		}
		public void setProviderType(String providerType) {
			this.providerType = providerType;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
	}
	
	public class Benefit{
		private String name;
		private String coveredByPlan;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCoveredByPlan() {
			return coveredByPlan;
		}
		public void setCoveredByPlan(String coveredByPlan) {
			this.coveredByPlan = coveredByPlan;
		}
	}
	
	public class Drug{
		private String id;
		private String name;
		private String type;
		private String coveredByPlan;
		private String fairPrice;
		private String dosage;
		private String cost;
		private String genericDosage;
		private String genericFairPrice;
		private String genericCost;
		private String genericCoveredByPlan;
		private String tier;
		private String genericId;
		private String genericTier;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getCoveredByPlan() {
			return coveredByPlan;
		}
		public void setCoveredByPlan(String coveredByPlan) {
			this.coveredByPlan = coveredByPlan;
		}
		public String getFairPrice() {
			return fairPrice;
		}
		public void setFairPrice(String fairPrice) {
			this.fairPrice = fairPrice;
		}
		public String getDosage() {
			return dosage;
		}
		public void setDosage(String dosage) {
			this.dosage = dosage;
		}
		public String getCost() {
			return cost;
		}
		public void setCost(String cost) {
			this.cost = cost;
		}
		public String getGenericDosage() {
			return genericDosage;
		}
		public void setGenericDosage(String genericDosage) {
			this.genericDosage = genericDosage;
		}
		public String getGenericFairPrice() {
			return genericFairPrice;
		}
		public void setGenericFairPrice(String genericFairPrice) {
			this.genericFairPrice = genericFairPrice;
		}
		public String getGenericCost() {
			return genericCost;
		}
		public void setGenericCost(String genericCost) {
			this.genericCost = genericCost;
		}
		public String getGenericCoveredByPlan() {
			return genericCoveredByPlan;
		}
		public void setGenericCoveredByPlan(String genericCoveredByPlan) {
			this.genericCoveredByPlan = genericCoveredByPlan;
		}
		public String getTier() {
			return tier;
		}
		public void setTier(String tier) {
			this.tier = tier;
		}
		public String getGenericId() {
			return genericId;
		}
		public void setGenericId(String genericId) {
			this.genericId = genericId;
		}
		public String getGenericTier() {
			return genericTier;
		}
		public void setGenericTier(String genericTier) {
			this.genericTier = genericTier;
		}
				
	}

}
