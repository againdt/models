package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity 
@Table(name="announcements")
public class Announcement implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public enum Status{
		PENDING, APPROVED, DELETED, ACTIVE, INACTIVE
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Announcements_Seq")
	@SequenceGenerator(name = "Announcements_Seq", sequenceName = "announcements_seq", allocationSize = 1)
	private int id;
	
	@Column(name="name",unique = true,nullable = false)
	private String name;
	
	@Column(name="announcement_text", nullable = false)
	private String announcementText;
	
	@Column(name="module_name", nullable = false)
	private String moduleName;
	
	@Temporal(value = TemporalType.DATE)
    @Column(name="effective_start_date")
	private Date effectiveStartDate;
	
	@Temporal(value = TemporalType.DATE)
    @Column(name="effective_end_date")
	private Date effectiveEndDate;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private Status status;
    
	@Column(name="created_by")
	private int createdBy;
    
	@Column(name="last_updated_by")
    private int updatedBy;
    
	@Temporal( TemporalType.TIMESTAMP)
    @Column(name="creation_timestamp")
	private Date createdDate;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date updatedDate;
    
    @OneToMany(mappedBy="announcement", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<AnnouncementRole> announcementRole;
	
	public Announcement(){
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAnnouncementText() {
		return announcementText;
	}

	public void setAnnouncementText(String announcementText) {
		this.announcementText = announcementText;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<AnnouncementRole> getAnnouncementRole() {
		return announcementRole;
	}

	public void setAnnouncementRole(List<AnnouncementRole> announcementRole) {
		this.announcementRole = announcementRole;
	}
	
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreatedDate(new TSDate());
		this.setUpdatedDate(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdatedDate(new TSDate());
	}
}
