package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the table.
 * 
 */

@Entity
@Table(name="COMMISSION_CONFIG_MAP")
public class CommissionConfigMapping implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMISSION_CONFIG_MAP_SEQ")
	@SequenceGenerator(name = "COMMISSION_CONFIG_MAP_SEQ", sequenceName = "COMMISSION_CONFIG_MAP_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="MAPPING_CONFIG_NAME")
	private String mappingConfigName ;

	@Column(name="MAPPING_JSON_STRING")
	private String mappingJsonString;
	
	@Column(name="STATE")
	private String state ;

	@Column(name="CARRIER")
	private String carrier;
			
	@Column(name="CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMappingConfigName() {
		return mappingConfigName;
	}

	public void setMappingConfigName(String mappingConfigName) {
		this.mappingConfigName = mappingConfigName;
	}

	public String getMappingJsonString() {
		return mappingJsonString;
	}

	public void setMappingJsonString(String mappingJsonString) {
		this.mappingJsonString = mappingJsonString;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}


}
