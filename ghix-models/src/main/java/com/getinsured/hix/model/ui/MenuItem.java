package com.getinsured.hix.model.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.getinsured.hix.model.Permission;

@Entity
@Table(name="MENU_ITEMS")
public class MenuItem implements Serializable {
	
	public enum Flag{
		Y,N
	};
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "menu_item_sequence")
	@SequenceGenerator(name = "menu_item_sequence", allocationSize = 1, sequenceName = "MENU_ITEMS_SEQ")
	private long id;
	
	@Column(name="PARENT_ID")
	private Long parentId;
	
	@Column(name="CAPTION")
	private String caption;
	
	@Column(name="URL")
	private String url;
	
	@Column(name="REDIRECT_URL")
	private String redirecturl;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="CREATION_TIMESTAMP")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date lastUpdatedTimestamp;
	
	
	@Column(name="OPTIONAL")
	private Flag isOptional;
	
	@Column(name="IS_ACTIVE")
	private Flag isActive;
	
	@OneToOne
	@JoinColumn(name="PERMISSION_ID")
	private Permission permission;
	
	@Transient
	private List<MenuItem> subMenus;
	
	
	
	@Transient
	private int order;
	
	public MenuItem(){
		
	}
	
	public MenuItem(String caption, String url, String title,String redirecturl, List<MenuItem> subMenus) {
		this.caption = caption;
		this.url = url;
		this.title = title;
		this.redirecturl = redirecturl;
		this.subMenus = subMenus;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getRedirecturl() {
		return redirecturl;
	}

	public void setRedirecturl(String redirecturl) {
		this.redirecturl = redirecturl;
	}

	public List<MenuItem> getSubMenus() {
		return subMenus;
	}

	public void setSubMenus(List<MenuItem> subMenus) {
		this.subMenus = subMenus;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	
	public Flag getIsOptional() {
		return isOptional;
	}

	public void setIsOptional(Flag isOptional) {
		this.isOptional = isOptional;
	}

	public Flag getIsActive() {
		return isActive;
	}

	public void setIsActive(Flag isActive) {
		this.isActive = isActive;
	}


	public static final Comparator<MenuItem> MENU_ITEMS_COMPARATOR = new Comparator<MenuItem>() {

		@Override
		public int compare(MenuItem o1, MenuItem o2) {
			
			return o1.getOrder() - o2.getOrder();
			
		}
	};
	
	
	public static final Comparator<MenuItem> MENU_ITEMS_SORT_BY_CAPTION = new Comparator<MenuItem>(){

		@Override
		public int compare(MenuItem o1, MenuItem o2) {
			return o1.getCaption().compareTo(o2.getCaption());
		}
		
	};
		
	
	public class MenuTreeView{
		private String text;
		private boolean checked;
		private boolean enabled;
		private List<MenuTreeView> items = new ArrayList<MenuTreeView>();
		
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public boolean getChecked() {
			return checked;
		}
		public void setChecked(boolean checked) {
			this.checked = checked;
		}
		public boolean getEnabled() {
			return enabled;
		}
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
		public List<MenuTreeView> getItems() {
			return items;
		}
		public void setItems(List<MenuTreeView> items) {
			this.items = items;
		}
		
		
		
		
		
		
		
		
		
	}
	
	

}
