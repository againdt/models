package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;




/**
 * 
 * @author rajaramesh_g
 * @since 22/11/2013
 *
 */

public class EnrollmentECommittedDTO implements Serializable{
	
	// Household Contact Information
	private String houseHoldContactFirstName;
	private String houseHoldContactMiddleName;
	private String houseHoldContactLastName;
	private String houseHoldContactType;
	private String houseHoldContactPhoneNumber;
	private String houseHoldContactBestTimeToContact;
	private String houseHoldContactEmail;
	private String houseHoldContactAddressLine1;
	private String houseHoldContactAddressLine2;
	private String houseHoldContactCity;
	private String houseHoldContactState;
	private String houseHoldContactZip;
	private String houseHoldContactCounty;
	private String ticketId;
	private String enrollmentStatus ;
	private String carrierConfirmationNumber;
	private Long ssapApplicationId;
	private String carrierAppId;
	
	// Health Plan
	private String carrierName;
	private String planName;
	private Integer planId;
	private Float monthlyPremium;
	
	private String carrierAppUrl;
	private String producerUserName;
	private String producerPassword;
	
	// Subscriber, Spouse, Dependent
	private List<EnrolleeECommittedDTO>  enrolleeList;

	public String getHouseHoldContactFirstName() {
		return houseHoldContactFirstName;
	}

	public void setHouseHoldContactFirstName(String houseHoldContactFirstName) {
		this.houseHoldContactFirstName = houseHoldContactFirstName;
	}

	public String getHouseHoldContactMiddleName() {
		return houseHoldContactMiddleName;
	}

	public void setHouseHoldContactMiddleName(String houseHoldContactMiddleName) {
		this.houseHoldContactMiddleName = houseHoldContactMiddleName;
	}

	public String getHouseHoldContactLastName() {
		return houseHoldContactLastName;
	}

	public void setHouseHoldContactLastName(String houseHoldContactLastName) {
		this.houseHoldContactLastName = houseHoldContactLastName;
	}

	public String getHouseHoldContactType() {
		return houseHoldContactType;
	}

	public void setHouseHoldContactType(String houseHoldContactType) {
		this.houseHoldContactType = houseHoldContactType;
	}

	public String getHouseHoldContactPhoneNumber() {
		return houseHoldContactPhoneNumber;
	}

	public void setHouseHoldContactPhoneNumber(String houseHoldContactPhoneNumber) {
		this.houseHoldContactPhoneNumber = houseHoldContactPhoneNumber;
	}

	public String getHouseHoldContactBestTimeToContact() {
		return houseHoldContactBestTimeToContact;
	}

	public void setHouseHoldContactBestTimeToContact(
			String houseHoldContactBestTimeToContact) {
		this.houseHoldContactBestTimeToContact = houseHoldContactBestTimeToContact;
	}

	public String getHouseHoldContactEmail() {
		return houseHoldContactEmail;
	}

	public void setHouseHoldContactEmail(String houseHoldContactEmail) {
		this.houseHoldContactEmail = houseHoldContactEmail;
	}

	public String getHouseHoldContactAddressLine1() {
		return houseHoldContactAddressLine1;
	}

	public void setHouseHoldContactAddressLine1(String houseHoldContactAddressLine1) {
		this.houseHoldContactAddressLine1 = houseHoldContactAddressLine1;
	}

	public String getHouseHoldContactAddressLine2() {
		return houseHoldContactAddressLine2;
	}

	public void setHouseHoldContactAddressLine2(String houseHoldContactAddressLine2) {
		this.houseHoldContactAddressLine2 = houseHoldContactAddressLine2;
	}

	public String getHouseHoldContactCity() {
		return houseHoldContactCity;
	}

	public void setHouseHoldContactCity(String houseHoldContactCity) {
		this.houseHoldContactCity = houseHoldContactCity;
	}

	public String getHouseHoldContactState() {
		return houseHoldContactState;
	}

	public void setHouseHoldContactState(String houseHoldContactState) {
		this.houseHoldContactState = houseHoldContactState;
	}

	public String getHouseHoldContactZip() {
		return houseHoldContactZip;
	}

	public void setHouseHoldContactZip(String houseHoldContactZip) {
		this.houseHoldContactZip = houseHoldContactZip;
	}

	public String getHouseHoldContactCounty() {
		return houseHoldContactCounty;
	}

	public void setHouseHoldContactCounty(String houseHoldContactCounty) {
		this.houseHoldContactCounty = houseHoldContactCounty;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Float getMonthlyPremium() {
		return monthlyPremium;
	}

	public void setMonthlyPremium(Float monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}

	public List<EnrolleeECommittedDTO> getEnrolleeList() {
		return enrolleeList;
	}

	public void setEnrolleeList(List<EnrolleeECommittedDTO> enrolleeList) {
		this.enrolleeList = enrolleeList;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getCarrierConfirmationNumber() {
		return carrierConfirmationNumber;
	}

	public void setCarrierConfirmationNumber(String carrierConfirmationNumber) {
		this.carrierConfirmationNumber = carrierConfirmationNumber;
	}

	
	public String getCarrierAppUrl() {
		return carrierAppUrl;
	}

	public void setCarrierAppUrl(String carrierAppUrl) {
		this.carrierAppUrl = carrierAppUrl;
	}
		
	public String getProducerUserName() {
		return producerUserName;
	}

	public void setProducerUserName(String producerUserName) {
		this.producerUserName = producerUserName;
	}

	public String getProducerPassword() {
		return producerPassword;
	}

	public void setProducerPassword(String producerPassword) {
		this.producerPassword = producerPassword;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	
	public String getCarrierAppId() {
		return carrierAppId;
	}

	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}


	@Override
	public String toString() {
		return "EnrollmentECommittedDTO [houseHoldContactFirstName="
				+ houseHoldContactFirstName + ", houseHoldContactMiddleName="
				+ houseHoldContactMiddleName + ", houseHoldContactLastName="
				+ houseHoldContactLastName + ", houseHoldContactType="
				+ houseHoldContactType + ", houseHoldContactPhoneNumber="
				+ houseHoldContactPhoneNumber
				+ ", houseHoldContactBestTimeToContact="
				+ houseHoldContactBestTimeToContact
				+ ", houseHoldContactEmail=" + houseHoldContactEmail
				+ ", houseHoldContactAddressLine1="
				+ houseHoldContactAddressLine1
				+ ", houseHoldContactAddressLine2="
				+ houseHoldContactAddressLine2 + ", houseHoldContactCity="
				+ houseHoldContactCity + ", houseHoldContactState="
				+ houseHoldContactState + ", houseHoldContactZip="
				+ houseHoldContactZip + ", houseHoldContactCounty="
				+ houseHoldContactCounty + ", ticketId=" + ticketId
				+ ", enrollmentStatus=" + enrollmentStatus
				+ ", carrierConfirmationNumber=" + carrierConfirmationNumber
				+ ", carrierName=" + carrierName + ", planName=" + planName
				+ ", planId=" + planId + ", monthlyPremium=" + monthlyPremium
				+ ", carrierAppUrl=" + carrierAppUrl + ", enrolleeList="
				+ enrolleeList + "]";
	}
}