package com.getinsured.hix.dto.directenrollment;

/**
 * DTO for passing credit card info FIXME: this should be encrypted when passed
 * around
 * 
 * @author root
 *
 */
public class CreditCardDTO {
	private String type;
	private String number;
	private String expmonth;
	private String expyear;
	private String cvv;
	private String zip;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExpmonth() {
		return expmonth;
	}

	public void setExpmonth(String expmonth) {
		this.expmonth = expmonth;
	}

	public String getExpyear() {
		return expyear;
	}

	public void setExpyear(String expyear) {
		this.expyear = expyear;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public String toString() {
		return "CreditCardDTO [number=" + number + ", expMonth=" + expmonth + ", expYear=" + expyear + ", cvv=" + cvv
			+ ", zip=" + zip + "]";
	}
}
