/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.hix.dto.cap.consumerapp.CapApplicationDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentApplicationIdDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentConsumerPortalDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationResponse;
import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentECommittedDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentNonEligiblePersonDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPlanResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.MonthlyAPTCAmountDTO;
import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.dto.enrollment.ShopEnrollmentLogDto;
import com.getinsured.hix.dto.ffm.FFMEnrollmentResponse;
import com.getinsured.hix.dto.shop.EmployerCoverageDTO;
import com.getinsured.hix.model.GHIXResponse;

/**
 * @author panda_p
 *
 */

public class EnrollmentResponse extends GHIXResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Enrollment enrollment=null;
	private Enrollee enrollee=null;
	private EnrollmentPayment enrollmentPayment=null;
	private List<Enrollment> enrollmentList=null;
	private List<Map<String, String>> disEnrollmentList=null;
	private Map<String, String> giAndFfmDataList=null;
	private Map<String,List<Map<String, String>>> finalDisEnrollmentMemberMap=null;
	private Map<String, Object> enrollmentsAndRecordCount =null;
	private Map<Integer, String> enrolleeNameMap =null;
	private Integer planId = null;
	private Integer orderItemId = null;
	private List<EnrollmentPlanResponseDTO> enrollmentPlanResponseDTOList =null;
	private List<SendUpdatedEnrolleeResponseDTO> sendUpdatedEnrolleeResponseDTOList =null;
	private List<EnrollmentCountDTO> enrollmentCountDTO = null;
	private boolean isEnrollmentExists = false;
	private EnrollmentRemittanceDTO enrollmentRemittanceDto = null;
	private List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTO=null;
	private EnrollmentPaymentDTO enrollmentPaymentDTO = null;
	private FFMEnrollmentResponse fFMEnrollmentResponse;
	private List<Enrollee> enrolleeList;
	private EnrollmentECommittedDTO enrollmentECommittedDto = null;
	private List<EnrollmentNonEligiblePersonDTO> enrollmentNonEligiblePersonDTO=null;	
	private List<EnrollmentApplicationIdDTO> enrollmentApplicationIdDTOList = null;
	private List<ShopEnrollmentLogDto> shopEnrollmentLogDto;
	private List<Integer> idList;
	private String earliestEffStartDate;
	private EmployerCoverageDTO employerCvgDto;
	private List<EnrollmentShopDTO> enrollmentShopDto;
	private List<Map<String, String>> memberAddedToEnrollment;
	private String cmsPlanId;
	private Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentFinanceMap;
	private List<EnrollmentConsumerPortalDTO> enrollmentConsumerPortalDTOList = null;
	private  Integer enrollmentId;
	private String fFMPolicyId;
	private String subscriberMemberId;
	private EnrollmentCapDto enrollmentCapDto;
	private Map<String, String> renewalStatusMap; //HIX-115619 Add renewal status map
	private Map<String, List<EnrollmentDataDTO>> enrollmentDataDtoListMap;
	private List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList;
	private List<EnrollmentDataDTO> enrollmentDataDTOList;
	private Long enrollmentCount;
	private boolean terminationFlag;
	private boolean dentalTerminationFlag;
	private CapApplicationDTO capApplicatonDto;
	
	private Map<Integer, String> enrollmentIdAndtermDateMap;
	private Map<String,Integer> enrollmentCountMap;
	
	private List<EnrolleeDTO> enrolleeAgeAndId;
	private Float netPremiumAmt ;
	private Float grossPremiumAmt ;
	private Float aptcAmt ;
	private BigDecimal stateSubsidyAmt;
	
	private String enrollmentStatusLabel;
	private String planName;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private Float employerContribution;
	private List<String> memberIds;
	private String subscriberZip;
	private String subscriberCountyCode;
	private List<MonthlyAPTCAmountDTO> monthlyAPTCAmountDTOList;
	private Map<Integer, String> failedEnrollmentMap;
	private boolean enrollmentOverlap;
	private List<EnrollmentCoverageValidationResponse> enrollmentCoverageValidationResponse;
	private List<EnrollmentPremiumDTO> enrollmentPremiumDTOList;
	private EnrollmentDTO enrollmentDTO;
	private List<EnrolleeDTO> enrolleeDTOList;
	private Map<String, List<EnrollmentMemberDataDTO>> enrollmentMemberDataDTOMap;
	private Map<Integer, String> skippedEnrollmentMap;
	
	private String planLevel;
	private String creationDate;
	private Float maxAptc; 
	
	//HIX-110394
	private String benefitEffectiveDate;
	//HIX-112546
	private List<String> hiosIssuerIdList;
	//HIX-112570
	private String changePlanAllowed;
	//HIX-116390
	private List<String> userPermissionList;
	
	public List<String> getUserPermissionList() {
		return userPermissionList;
	}
	public void setUserPermissionList(List<String> userPermissionList) {
		this.userPermissionList = userPermissionList;
	}
	public boolean isTerminationFlag() {
		return terminationFlag;
	}
	public void setTerminationFlag(boolean terminationFlag) {
		this.terminationFlag = terminationFlag;
	}
	public Map<String, Set<EnrollmentCurrentMonthDTO>> getEnrollmentFinanceMap() {
		return enrollmentFinanceMap;
	}
	public void setEnrollmentFinanceMap(
			Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentFinanceMap) {
		this.enrollmentFinanceMap = enrollmentFinanceMap;
	}
	public Map<String, Integer> getEnrollmentCountMap() {
		return enrollmentCountMap;
	}
	public void setEnrollmentCountMap(Map<String, Integer> enrollmentCountMap) {
		this.enrollmentCountMap = enrollmentCountMap;
	}
	public String getCmsPlanId() {
		return cmsPlanId;
	}
	public void setCmsPlanId(String cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}
	public List<Map<String, String>> getMemberAddedToEnrollment() {
		return memberAddedToEnrollment;
	}
	public void setMemberAddedToEnrollment(
			List<Map<String, String>> memberAddedToEnrollment) {
		this.memberAddedToEnrollment = memberAddedToEnrollment;
	}
	
	public List<EnrollmentShopDTO> getEnrollmentShopDto() {
		return enrollmentShopDto;
	}
	public void setEnrollmentShopDto(List<EnrollmentShopDTO> enrollmentShopDto) {
		this.enrollmentShopDto = enrollmentShopDto;
	}
	public EmployerCoverageDTO getEmployerCvgDto() {
		return employerCvgDto;
	}
	public void setEmployerCvgDto(EmployerCoverageDTO employerCvgDto) {
		this.employerCvgDto = employerCvgDto;
	}
	
	public String getEarliestEffStartDate() {
		return earliestEffStartDate;
	}
	public void setEarliestEffStartDate(String latestEffStartDate) {
		this.earliestEffStartDate = latestEffStartDate;
	}
	public Map<Integer, String> getEnrolleeNameMap() {
		return enrolleeNameMap;
	}
	public void setEnrolleeNameMap(Map<Integer, String> enrolleeNameMap) {
		this.enrolleeNameMap = enrolleeNameMap;
	}
	public Enrollment getEnrollment() {
		return enrollment;
	}
	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public Enrollee getEnrollee() {
		return enrollee;
	}
	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	public EnrollmentPayment getEnrollmentPayment() {
		return enrollmentPayment;
	}
	public void setEnrollmentPayment(EnrollmentPayment enrollmentPayment) {
		this.enrollmentPayment = enrollmentPayment;
	}
	
	public List<Enrollment> getEnrollmentList() {
		return enrollmentList;
	}

	public void setEnrollmentList(List<Enrollment> enrollmentList) {
		this.enrollmentList = enrollmentList;
	}

	public Map<String, Object> getEnrollmentsAndRecordCount() {
		return enrollmentsAndRecordCount;
	}

	public void setEnrollmentsAndRecordCount(
			Map<String, Object> enrollmentsAndRecordCount) {
		this.enrollmentsAndRecordCount = enrollmentsAndRecordCount;
	}
	public List<Map<String, String>> getDisEnrollmentList() {
		return disEnrollmentList;
	}
	public void setDisEnrollmentList(List<Map<String, String>> disEnrollmentList) {
		this.disEnrollmentList = disEnrollmentList;
	}
	/**
	 * @return the enrollmentCoverageValidationResponse
	 */
	public List<EnrollmentCoverageValidationResponse> getEnrollmentCoverageValidationResponse() {
		return enrollmentCoverageValidationResponse;
	}
	/**
	 * @param enrollmentCoverageValidationResponse the enrollmentCoverageValidationResponse to set
	 */
	public void setEnrollmentCoverageValidationResponse(
			List<EnrollmentCoverageValidationResponse> enrollmentCoverageValidationResponse) {
		this.enrollmentCoverageValidationResponse = enrollmentCoverageValidationResponse;
	}
	/**
	 * @return the enrollmentOverlap
	 */
	public boolean isEnrollmentOverlap() {
		return enrollmentOverlap;
	}
	/**
	 * @param enrollmentOverlap the enrollmentOverlap to set
	 */
	public void setEnrollmentOverlap(boolean enrollmentOverlap) {
		this.enrollmentOverlap = enrollmentOverlap;
	}
	public Map<String, List<Map<String, String>>> getFinalDisEnrollmentMemberMap() {
		return finalDisEnrollmentMemberMap;
	}
	public void setFinalDisEnrollmentMemberMap(
			Map<String, List<Map<String, String>>> finalDisEnrollmentMemberMap) {
		this.finalDisEnrollmentMemberMap = finalDisEnrollmentMemberMap;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Integer getOrderItemId() {
		return orderItemId;
	}
	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}
	public List<EnrollmentPlanResponseDTO> getEnrollmentPlanResponseDTOList() {
		return enrollmentPlanResponseDTOList;
	}
	public void setEnrollmentPlanResponseDTOList(
			List<EnrollmentPlanResponseDTO> enrollmentPlanResponseDTOList) {
		this.enrollmentPlanResponseDTOList = enrollmentPlanResponseDTOList;
	}
	public List<SendUpdatedEnrolleeResponseDTO> getSendUpdatedEnrolleeResponseDTOList() {
		return sendUpdatedEnrolleeResponseDTOList;
	}
	public void setSendUpdatedEnrolleeResponseDTOList(
			List<SendUpdatedEnrolleeResponseDTO> sendUpdatedEnrolleeResponseDTOList) {
		this.sendUpdatedEnrolleeResponseDTOList = sendUpdatedEnrolleeResponseDTOList;
	}
	public List<EnrollmentCountDTO> getEnrollmentCountDTO() {
		return enrollmentCountDTO;
	}
	public void setEnrollmentCountDTO(List<EnrollmentCountDTO> enrollmentCountDTO) {
		this.enrollmentCountDTO = enrollmentCountDTO;
	}
	
	public boolean getIsEnrollmentExists() {
		return isEnrollmentExists;
	}
	public void setIsEnrollmentExists(boolean isEnrollmentExists) {
		this.isEnrollmentExists = isEnrollmentExists;
	}
	public List<EnrollmentCurrentMonthDTO> getEnrollmentCurrentMonthDTO() {
		return enrollmentCurrentMonthDTO;
	}
	public void setEnrollmentCurrentMonthDTO(
			List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTO) {
		this.enrollmentCurrentMonthDTO = enrollmentCurrentMonthDTO;
	}
	
	public EnrollmentRemittanceDTO getEnrollmentRemittanceDto() {
		return enrollmentRemittanceDto;
	}
	public void setEnrollmentRemittanceDto(
			EnrollmentRemittanceDTO enrollmentRemittanceDto) {
		this.enrollmentRemittanceDto = enrollmentRemittanceDto;
	}
	public FFMEnrollmentResponse getfFMEnrollmentResponse() {
		return fFMEnrollmentResponse;
	}
	public void setfFMEnrollmentResponse(FFMEnrollmentResponse fFMEnrollmentResponse) {
		this.fFMEnrollmentResponse = fFMEnrollmentResponse;
	}
	public List<Enrollee> getEnrolleeList() {
		return enrolleeList;
	}
	public void setEnrolleeList(List<Enrollee> enrolleeList) {
		this.enrolleeList = enrolleeList;
	}
	public List<ShopEnrollmentLogDto> getShopEnrollmentLogDto() {
		return shopEnrollmentLogDto;
	}
	public void setShopEnrollmentLogDto(List<ShopEnrollmentLogDto> shopEnrollmentLogDto) {
		this.shopEnrollmentLogDto = shopEnrollmentLogDto;
	}
	
	public List<Integer> getIdList() {
		return idList;
	}
	public void setIdList(List<Integer> idList) {
		this.idList = idList;
	}
	public Map<String, String> getGiAndFfmDataList() {
		return giAndFfmDataList;
	}
	public void setGiAndFfmDataList(Map<String, String> giAndFfmDataList) {
		this.giAndFfmDataList = giAndFfmDataList;
	}
	public EnrollmentECommittedDTO getEnrollmentECommittedDto() {
		return enrollmentECommittedDto;
	}
	public void setEnrollmentECommittedDto(
			EnrollmentECommittedDTO enrollmentECommittedDto) {
		this.enrollmentECommittedDto = enrollmentECommittedDto;
	}
	public List<EnrollmentNonEligiblePersonDTO> getEnrollmentNonEligiblePersonDTO() {
		return enrollmentNonEligiblePersonDTO;
	}
	public void setEnrollmentNonEligiblePersonDTO(
			List<EnrollmentNonEligiblePersonDTO> enrollmentNonEligiblePersonDTO) {
		this.enrollmentNonEligiblePersonDTO = enrollmentNonEligiblePersonDTO;
	}
	public List<EnrollmentApplicationIdDTO> getEnrollmentApplicationIdDTOList() {
		return enrollmentApplicationIdDTOList;
	}
	public void setEnrollmentApplicationIdDTOList(
			List<EnrollmentApplicationIdDTO> enrollmentApplicationIdDTOList) {
		this.enrollmentApplicationIdDTOList = enrollmentApplicationIdDTOList;
	}
	public List<EnrollmentConsumerPortalDTO> getEnrollmentConsumerPortalDTOList() {
		return enrollmentConsumerPortalDTOList;
	}
	public void setEnrollmentConsumerPortalDTOList(
			List<EnrollmentConsumerPortalDTO> enrollmentConsumerPortalDTOList) {
		this.enrollmentConsumerPortalDTOList = enrollmentConsumerPortalDTOList;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getfFMPolicyId() {
		return fFMPolicyId;
	}
	public void setfFMPolicyId(String fFMPolicyId) {
		this.fFMPolicyId = fFMPolicyId;
	}
	public void setEnrollmentExists(boolean isEnrollmentExists) {
		this.isEnrollmentExists = isEnrollmentExists;
	}
	
	/* CAP: Populating application data for application */
	public CapApplicationDTO getCapApplicatonDto() {
		return capApplicatonDto;
	}
	
	public void setCapApplicatonDto(CapApplicationDTO capApplicatonDto) {
		this.capApplicatonDto = capApplicatonDto;
	}
	/* CAP: Populating application data for enrollment */
	public String getSubscriberMemberId() {
		return subscriberMemberId;
	}
	public void setSubscriberMemberId(String subscriberMemberId) {
		this.subscriberMemberId = subscriberMemberId;
	}
	public EnrollmentPaymentDTO getEnrollmentPaymentDTO() {
		return enrollmentPaymentDTO;
	}
	public void setEnrollmentPaymentDTO(EnrollmentPaymentDTO enrollmentPaymentDTO) {
		this.enrollmentPaymentDTO = enrollmentPaymentDTO;
	}
	public Map<Integer, String> getEnrollmentIdAndtermDateMap() {
		return enrollmentIdAndtermDateMap;
	}
	public void setEnrollmentIdAndtermDateMap(
			Map<Integer, String> enrollmentIdAndtermDateMap) {
		this.enrollmentIdAndtermDateMap = enrollmentIdAndtermDateMap;
	}
	/**
	 * @return the enrollmentCapDto
	 */
	public EnrollmentCapDto getEnrollmentCapDto() {
		return enrollmentCapDto;
	}
	/**
	 * @param enrollmentCapDto the enrollmentCapDto to set
	 */
	public void setEnrollmentCapDto(EnrollmentCapDto enrollmentCapDto) {
		this.enrollmentCapDto = enrollmentCapDto;
	}
	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}
	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}
	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}
	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}
	public Float getAptcAmt() {
		return aptcAmt;
	}
	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}
	public String getEnrollmentStatusLabel() {
		return enrollmentStatusLabel;
	}
	public void setEnrollmentStatusLabel(String enrollmentStatusLabel) {
		this.enrollmentStatusLabel = enrollmentStatusLabel;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getEmployerContribution() {
		return employerContribution;
	}
	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}
	
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	/**
	 * @return the enrollmentDataDtoListMap
	 */
	public Map<String, List<EnrollmentDataDTO>> getEnrollmentDataDtoListMap() {
		return enrollmentDataDtoListMap;
	}
	/**
	 * @param enrollmentDataDtoListMap the enrollmentDataDtoListMap to set
	 */
	public void setEnrollmentDataDtoListMap(
			Map<String, List<EnrollmentDataDTO>> enrollmentDataDtoListMap) {
		this.enrollmentDataDtoListMap = enrollmentDataDtoListMap;
	}
	/**
	 * @return the enrollmentEventHistoryDTOList
	 */
	public List<EnrollmentEventHistoryDTO> getEnrollmentEventHistoryDTOList() {
		return enrollmentEventHistoryDTOList;
	}
	/**
	 * @param enrollmentEventHistoryDTOList the enrollmentEventHistoryDTOList to set
	 */
	public void setEnrollmentEventHistoryDTOList(
			List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList) {
		this.enrollmentEventHistoryDTOList = enrollmentEventHistoryDTOList;
	}
	/**
	 * @return the enrollmentDataDTOList
	 */
	public List<EnrollmentDataDTO> getEnrollmentDataDTOList() {
		return enrollmentDataDTOList;
	}
	/**
	 * @param enrollmentDataDTOList the enrollmentDataDTOList to set
	 */
	public void setEnrollmentDataDTOList(
			List<EnrollmentDataDTO> enrollmentDataDTOList) {
		this.enrollmentDataDTOList = enrollmentDataDTOList;
	}
	public List<String> getMemberIds() {
		return memberIds;
	}
	public void setMemberIds(List<String> memberIds) {
		this.memberIds = memberIds;
	}
	public String getSubscriberZip() {
		return subscriberZip;
	}
	public void setSubscriberZip(String subscriberZip) {
		this.subscriberZip = subscriberZip;
	}
	/**
	 * @return the subscriberCountyCode
	 */
	public String getSubscriberCountyCode() {
		return subscriberCountyCode;
	}
	/**
	 * @param subscriberCountyCode the subscriberCountyCode to set
	 */
	public void setSubscriberCountyCode(String subscriberCountyCode) {
		this.subscriberCountyCode = subscriberCountyCode;
	}
	public boolean isDentalTerminationFlag() {
		return dentalTerminationFlag;
	}
	public void setDentalTerminationFlag(boolean dentalTerminationFlag) {
		this.dentalTerminationFlag = dentalTerminationFlag;
	}
	public Map<Integer, String> getFailedEnrollmentMap() {
		return failedEnrollmentMap;
	}
	public void setFailedEnrollmentMap(Map<Integer, String> failedEnrollmentMap) {
		this.failedEnrollmentMap = failedEnrollmentMap;
	}
	public Long getEnrollmentCount() {
		return enrollmentCount;
	}
	public void setEnrollmentCount(Long enrollmentCount) {
		this.enrollmentCount = enrollmentCount;
	}
	/**
	 * @return the enrollmentPremiumDTOList
	 */
	public List<EnrollmentPremiumDTO> getEnrollmentPremiumDTOList() {
		return enrollmentPremiumDTOList;
	}
	/**
	 * @param enrollmentPremiumDTOList the enrollmentPremiumDTOList to set
	 */
	public void setEnrollmentPremiumDTOList(List<EnrollmentPremiumDTO> enrollmentPremiumDTOList) {
		this.enrollmentPremiumDTOList = enrollmentPremiumDTOList;
	}
	public List<MonthlyAPTCAmountDTO> getMonthlyAPTCAmountDTOList() {
		return monthlyAPTCAmountDTOList;
	}
	public void setMonthlyAPTCAmountDTOList(
			List<MonthlyAPTCAmountDTO> monthlyAPTCAmountDTOList) {
		this.monthlyAPTCAmountDTOList = monthlyAPTCAmountDTOList;
	}
	public EnrollmentDTO getEnrollmentDTO() {
		return enrollmentDTO;
	}
	public void setEnrollmentDTO(EnrollmentDTO enrollmentDTO) {
		this.enrollmentDTO = enrollmentDTO;
	}
	public List<EnrolleeDTO> getEnrolleeDTOList() {
		return enrolleeDTOList;
	}
	public void setEnrolleeDTOList(List<EnrolleeDTO> enrolleeDTOList) {
		this.enrolleeDTOList = enrolleeDTOList;
	}
	public List<EnrolleeDTO> getEnrolleeAgeAndId() {
		return enrolleeAgeAndId;
	}
	public void setEnrolleeAgeAndId(List<EnrolleeDTO> enrolleeAgeAndId) {
		this.enrolleeAgeAndId = enrolleeAgeAndId;
	}
	public Map<String, List<EnrollmentMemberDataDTO>> getEnrollmentMemberDataDTOMap() {
		return enrollmentMemberDataDTOMap;
	}
	public void setEnrollmentMemberDataDTOMap(Map<String, List<EnrollmentMemberDataDTO>> enrollmentMemberDataDTOMap) {
		this.enrollmentMemberDataDTOMap = enrollmentMemberDataDTOMap;
	}
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	public Map<Integer, String> getSkippedEnrollmentMap() {
		return skippedEnrollmentMap;
	}
	public void setSkippedEnrollmentMap(Map<Integer, String> skippedEnrollmentMap) {
		this.skippedEnrollmentMap = skippedEnrollmentMap;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public void setBenefitEffectiveDate(String benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public List<String> getHiosIssuerIdList() {
		return hiosIssuerIdList;
	}
	public void setHiosIssuerIdList(List<String> hiosIssuerIdList) {
		this.hiosIssuerIdList = hiosIssuerIdList;
	}
	public String getChangePlanAllowed() {
		return changePlanAllowed;
	}
	public void setChangePlanAllowed(String changePlanAllowed) {
		this.changePlanAllowed = changePlanAllowed;
	}

	public BigDecimal getStateSubsidyAmt() {
		return stateSubsidyAmt;
	}

	public void setStateSubsidyAmt(BigDecimal stateSubsidyAmt) {
		this.stateSubsidyAmt = stateSubsidyAmt;
	}

	public Map<String, String> getRenewalStatusMap() {
		return renewalStatusMap;
	}
	public void setRenewalStatusMap(Map<String, String> renewalStatusMap) {
		this.renewalStatusMap = renewalStatusMap;
	}
	
	public Float getMaxAptc() {
		return maxAptc;
	}
	public void setMaxAptc(Float maxAptc) {
		this.maxAptc = maxAptc;
	}
	@Override
	public String toString() {
		return "EnrollmentResponse [enrollment=" + enrollment + ", enrollee=" + enrollee + ", enrollmentPayment="
				+ enrollmentPayment + ", enrollmentList=" + enrollmentList + ", disEnrollmentList=" + disEnrollmentList
				+ ", giAndFfmDataList=" + giAndFfmDataList + ", finalDisEnrollmentMemberMap="
				+ finalDisEnrollmentMemberMap + ", enrollmentsAndRecordCount=" + enrollmentsAndRecordCount
				+ ", enrolleeNameMap=" + enrolleeNameMap + ", planId=" + planId + ", orderItemId=" + orderItemId
				+ ", enrollmentPlanResponseDTOList=" + enrollmentPlanResponseDTOList
				+ ", sendUpdatedEnrolleeResponseDTOList=" + sendUpdatedEnrolleeResponseDTOList + ", enrollmentCountDTO="
				+ enrollmentCountDTO + ", isEnrollmentExists=" + isEnrollmentExists + ", enrollmentRemittanceDto="
				+ enrollmentRemittanceDto + ", enrollmentCurrentMonthDTO=" + enrollmentCurrentMonthDTO
				+ ", enrollmentPaymentDTO=" + enrollmentPaymentDTO + ", fFMEnrollmentResponse=" + fFMEnrollmentResponse
				+ ", enrolleeList=" + enrolleeList + ", enrollmentECommittedDto=" + enrollmentECommittedDto
				+ ", enrollmentNonEligiblePersonDTO=" + enrollmentNonEligiblePersonDTO
				+ ", enrollmentApplicationIdDTOList=" + enrollmentApplicationIdDTOList + ", shopEnrollmentLogDto="
				+ shopEnrollmentLogDto + ", idList=" + idList + ", earliestEffStartDate=" + earliestEffStartDate
				+ ", employerCvgDto=" + employerCvgDto + ", enrollmentShopDto=" + enrollmentShopDto
				+ ", memberAddedToEnrollment=" + memberAddedToEnrollment + ", cmsPlanId=" + cmsPlanId
				+ ", enrollmentFinanceMap=" + enrollmentFinanceMap + ", enrollmentConsumerPortalDTOList="
				+ enrollmentConsumerPortalDTOList + ", enrollmentId=" + enrollmentId + ", fFMPolicyId=" + fFMPolicyId
				+ ", subscriberMemberId=" + subscriberMemberId + ", enrollmentCapDto=" + enrollmentCapDto
				+ ", renewalStatusMap=" + renewalStatusMap + ", enrollmentDataDtoListMap=" + enrollmentDataDtoListMap
				+ ", enrollmentEventHistoryDTOList=" + enrollmentEventHistoryDTOList + ", enrollmentDataDTOList="
				+ enrollmentDataDTOList + ", enrollmentCount=" + enrollmentCount + ", terminationFlag="
				+ terminationFlag + ", dentalTerminationFlag=" + dentalTerminationFlag + ", capApplicatonDto="
				+ capApplicatonDto + ", enrollmentIdAndtermDateMap=" + enrollmentIdAndtermDateMap
				+ ", enrollmentCountMap=" + enrollmentCountMap + ", enrolleeAgeAndId=" + enrolleeAgeAndId
				+ ", netPremiumAmt=" + netPremiumAmt + ", grossPremiumAmt=" + grossPremiumAmt + ", aptcAmt=" + aptcAmt
				+ ", stateSubsidyAmt=" + stateSubsidyAmt + ", enrollmentStatusLabel=" + enrollmentStatusLabel
				+ ", planName=" + planName + ", effectiveStartDate=" + effectiveStartDate + ", effectiveEndDate="
				+ effectiveEndDate + ", employerContribution=" + employerContribution + ", memberIds=" + memberIds
				+ ", subscriberZip=" + subscriberZip + ", subscriberCountyCode=" + subscriberCountyCode
				+ ", monthlyAPTCAmountDTOList=" + monthlyAPTCAmountDTOList + ", failedEnrollmentMap="
				+ failedEnrollmentMap + ", enrollmentOverlap=" + enrollmentOverlap
				+ ", enrollmentCoverageValidationResponse=" + enrollmentCoverageValidationResponse
				+ ", enrollmentPremiumDTOList=" + enrollmentPremiumDTOList + ", enrollmentDTO=" + enrollmentDTO
				+ ", enrolleeDTOList=" + enrolleeDTOList + ", enrollmentMemberDataDTOMap=" + enrollmentMemberDataDTOMap
				+ ", skippedEnrollmentMap=" + skippedEnrollmentMap + ", planLevel=" + planLevel + ", creationDate="
				+ creationDate + ", benefitEffectiveDate=" + benefitEffectiveDate + ", hiosIssuerIdList="
				+ hiosIssuerIdList + ", changePlanAllowed=" + changePlanAllowed + ", userPermissionList=" + userPermissionList +"]";
	}
	
}
