package com.getinsured.hix.dto.shop;

import java.util.List;

public class EmployerCoverageDTO {
	private Integer employerId;
	private String coverageStartDate;
	private String coverageEndDate;
	private String planTier;
	private String openEnrollmentStartDate;
	private String openEnrollmentEndDate;
	private List<EmployeeCoverageDTO> employeeCoverage;
	private List<PlanPremiumDTO> planPremiumDTOs;
	private boolean isPlanTierChange;
	
	public Integer getEmployerId() {
		return employerId;
	}
	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getCoverageEndDate() {
		return coverageEndDate;
	}
	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	public String getPlanTier() {
		return planTier;
	}
	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}
	public String getOpenEnrollmentStartDate() {
		return openEnrollmentStartDate;
	}
	public void setOpenEnrollmentStartDate(String openEnrollmentStartDate) {
		this.openEnrollmentStartDate = openEnrollmentStartDate;
	}
	public String getOpenEnrollmentEndDate() {
		return openEnrollmentEndDate;
	}
	public void setOpenEnrollmentEndDate(String openEnrollmentEndDate) {
		this.openEnrollmentEndDate = openEnrollmentEndDate;
	}
	public List<EmployeeCoverageDTO> getEmployeeCoverage() {
		return employeeCoverage;
	}
	public void setEmployeeCoverage(List<EmployeeCoverageDTO> employeeCoverage) {
		this.employeeCoverage = employeeCoverage;
	}
	public List<PlanPremiumDTO> getPlanPremiumDTOs() {
		return planPremiumDTOs;
	}
	public void setPlanPremiumDTOs(List<PlanPremiumDTO> planPremiumDTOs) {
		this.planPremiumDTOs = planPremiumDTOs;
	}
	public boolean isPlanTierChange() {
		return isPlanTierChange;
	}
	public void setPlanTierChange(boolean isPlanTierChange) {
		this.isPlanTierChange = isPlanTierChange;
	}
}