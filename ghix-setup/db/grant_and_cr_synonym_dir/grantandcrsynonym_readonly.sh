#!/bin/bash
#

export PATH=$PATH:$HOME/bin
TMP=/tmp; export TMP
TMPDIR=$TMP; export TMPDIR

# ORACLE_HOSTNAME=ghixdb; export ORACLE_HOSTNAME
export ORACLE_BASE=/u01/app/oracle 
export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/db_1
export ORACLE_SID=ghixdb
export ORACLE_TERM=xterm
export PATH=/usr/sbin:$PATH
export PATH=$ORACLE_HOME/bin:$PATH

export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib

if [ $# -ne 2 ]
then
  echo " "
  echo "Usage $0 <SCHEMA>_USER <SCHEMA>_OWNER"
  echo "Example $0 GHIX1_USER GHIX1_OWNER"
  echo " "
  exit 2
fi
###
cat TEMPLATE_APP_USER_grntcrsyn_readonly.sh |sed "s/TEMPLATE_APP_USER/$1/g" >$1_grant_and_create_syn_readonly.sh
chmod 700 $1_grant_and_create_syn_readonly.sh
###
cat re_cr_TEMPLATE_APP_USER_get_grant_and_create_synonyms_readonly.sql | sed "s/TEMPLATE_APP_USER/$1/g" | sed "s/TEMPLATE_APP_OWNER/$2/g">  re_cr_$1_get_grant_and_create_synonyms_readonly.sql 
##
./$1_grant_and_create_syn_readonly.sh


