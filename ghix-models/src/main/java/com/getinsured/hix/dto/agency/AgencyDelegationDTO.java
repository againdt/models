package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class AgencyDelegationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String brokerId;
	private String  agentFirstName;
	private String  agentLastName;
	
	private String individualId;
	private String individualFirstName;
	private String individualLastName;
	
	private String receivedOn;

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public String getAgentFirstName() {
		return agentFirstName;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public String getIndividualId() {
		return individualId;
	}

	public void setIndividualId(String individualId) {
		this.individualId = individualId;
	}

	public String getIndividualFirstName() {
		return individualFirstName;
	}

	public void setIndividualFirstName(String individualFirstName) {
		this.individualFirstName = individualFirstName;
	}

	public String getIndividualLastName() {
		return individualLastName;
	}

	public void setIndividualLastName(String individualLastName) {
		this.individualLastName = individualLastName;
	}

	public String getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(String receivedOn) {
		this.receivedOn = receivedOn;
	}
	
	
}
