package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employee_qualifying_event")
public class EmployeeQualifyingEvent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_QUALIFYING_EVENT_SEQ")
	@SequenceGenerator(name = "EMPLOYEE_QUALIFYING_EVENT_SEQ", sequenceName = "EMPLOYEE_QUALIFYING_EVENT_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="employee_id")
	private Integer employeeId;
	
	@Column(name="event_type")
	private String eventType;
	
	@Column(name="event_code")
	private String eventCode;
	
	@Column(name="special_event_code")
	private String spEventCode;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "event_date")
	private Date eventDate;
	
	@Column(name="employee_application_id")
    private int employeeApplicationId;
	
	@OneToOne
	@JoinColumn(name="qualify_event_id")
	private EmployeeEvents qualifyEvent;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "sp_open_enrollment_start")
	private Date spOpenEnrollmentStart;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "sp_open_enrollment_end")
	private Date spOpenEnrollmentEnd;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "sp_coverage_start_date")
	private Date spCoverageStartDate;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;
	
	@Column(name="createdby")
	private Integer createdBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	public String getSpEventCode() {
		return spEventCode;
	}

	public void setSpEventCode(String spEventCode) {
		this.spEventCode = spEventCode;
	}


	public EmployeeEvents getQualifyEvent() {
		return qualifyEvent;
	}

	public void setQualifyEvent(EmployeeEvents qualifyEvent) {
		this.qualifyEvent = qualifyEvent;
	}

	public int getEmployeeApplicationId() {
		return employeeApplicationId;
	}

	public void setEmployeeApplicationId(int employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}

	public Date getSpOpenEnrollmentStart() {
		return spOpenEnrollmentStart;
	}

	public void setSpOpenEnrollmentStart(Date spOpenEnrollmentStart) {
		this.spOpenEnrollmentStart = spOpenEnrollmentStart;
	}

	public Date getSpOpenEnrollmentEnd() {
		return spOpenEnrollmentEnd;
	}

	public void setSpOpenEnrollmentEnd(Date spOpenEnrollmentEnd) {
		this.spOpenEnrollmentEnd = spOpenEnrollmentEnd;
	}

	public Date getSpCoverageStartDate() {
		return spCoverageStartDate;
	}

	public void setSpCoverageStartDate(Date spCoverageStartDate) {
		this.spCoverageStartDate = spCoverageStartDate;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
	}
}
