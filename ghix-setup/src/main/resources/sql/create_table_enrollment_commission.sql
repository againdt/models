  CREATE TABLE "ENROLLMENT_COMMISSION" 
   (	"ID" NUMBER(10,0) NOT NULL ENABLE, 
	"CARRIER_NAME" VARCHAR2(60 BYTE), 
	"ISSUER_ID" NUMBER(10,0), 
	"STATE" VARCHAR2(60 BYTE), 
	"ENROLLMENT_ID" NUMBER(10,0), 
	"COMMISSION_AMT" FLOAT(126), 
	"COMMISSION_DATE" DATE, 
	"CREATION_TIMESTAMP" TIMESTAMP (6), 
	"LAST_UPDATE_TIMESTAMP" TIMESTAMP (6), 
	"COMMISSIONABLE_AMOUNT" FLOAT(126) DEFAULT 0, 
	"AMT_PAID_TO_DATE" FLOAT(126) DEFAULT 0 NOT NULL ENABLE, 
	"COMMISSION_MODE" VARCHAR2(20 BYTE) DEFAULT 'AUTO' NOT NULL ENABLE, 
	"PREMIUM_MONTH" DATE, 
	"COMMISSION_TYPE" VARCHAR2(20 CHAR), 
	"CARRIER_FEED_DETAILS_ID" NUMBER, 
	 CONSTRAINT "PK_ENROLLMNT_COMMISION" PRIMARY KEY ("ID")
   );
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."ID" IS 'Primary Key';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."CARRIER_NAME" IS 'generated to store carrier';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."STATE" IS 'generated to store state';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."ENROLLMENT_ID" IS 'generated to store enrollment id';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."COMMISSION_AMT" IS 'generated to store commission amount';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."COMMISSION_DATE" IS 'generated to store Commission Date';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."COMMISSIONABLE_AMOUNT" IS 'Amount of payable commission';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."AMT_PAID_TO_DATE" IS 'Indicates cumulative commission amount paid to date';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."COMMISSION_MODE" IS 'Indicates mode of commission payment.  Possible values: MANUAL, AUTO';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."COMMISSION_TYPE" IS 'Indicates commission type';
   COMMENT ON COLUMN "ENROLLMENT_COMMISSION"."CARRIER_FEED_DETAILS_ID" IS 'Record carrier feed details Id';

  CREATE INDEX "IDX_ENROL_COMM_ENROL_ID" ON "ENROLLMENT_COMMISSION" ("ENROLLMENT_ID");
  