package com.serff.restServices;

//import com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.PlanStatus;

/**
 * UpdateSerffPlanStatusResponse defines the response object obtained by
 * invoking REST call to GHIX-SERFF for plan status updation in SERFF
 * 
 * @author Geetha Chandran
 * @since 11/04/2013
 */
public class UpdateSerffPlanStatusResponse {

	private boolean successful;

	private String planId;

//	private PlanStatus planStatus;

	public boolean isSuccessful() {
		return successful;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	/*public PlanStatus getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(PlanStatus planStatus) {
		this.planStatus = planStatus;
	}*/
}
