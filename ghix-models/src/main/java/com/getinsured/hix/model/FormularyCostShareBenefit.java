/**
 * 
 */
package com.getinsured.hix.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.getinsured.timeshift.TimeShifterUtil;


/**
 * Persistence class 'FormularyCostShare' mapping to table 'FORMULARY_COST_SHARE'.
 *
 * @author chalse_v
 * @version 1.1
 * @since June 13, 2013
 *
 */

@Entity
@Table(name = "FORMULARY_COST_SHARE_BENEFIT")
public class FormularyCostShareBenefit {
	

	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}
	
    @Id
	@SequenceGenerator(name = "seqFormularyCostShareBen", sequenceName = "FORMULARY_COST_SHARE_BEN_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqFormularyCostShareBen")
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name="FORMULARY_ID", referencedColumnName = "ID")
	private Formulary formulary;
    
    @Column(name = "ONE_MON_OUT_NW_RETAIL_OFFERED")
    private Character oneMonthOutNetworkRetailOfferedIndicator;

    @Column(name = "THREE_MON_IN_NW_MAIL_OFFERED")
    private Character  threeMonthInNetworkMailOfferedIndicator;

    @Column(name = "THREE_MON_OUT_NW_MAIL_OFFERED")
    private Character  threeMonthOutNetworkMailOfferedIndicator;
    
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@NotNull
	private Timestamp lastUpdateTimestamp;

	@Column(name = "IS_DELETED")
	//@Enumerated(EnumType.STRING)
	private String isDeleted;

	/**
	 * Default constructor.
	 */
	public FormularyCostShareBenefit() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis())); 
	}

	/**
	 * Getter for 'id'.
	 *
	 * @return the id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Setter for 'id'.
	 *
	 * @param id the 'id' to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Getter for 'formulary'.
	 *
	 * @return the formulary.
	 */
	public Formulary getFormulary() {
		return formulary;
	}

	/**
	 * Setter for 'formulary'.
	 *
	 * @param formulary the 'formulary' to set
	 */
	public void setFormulary(Formulary formulary) {
		this.formulary = formulary;
	}

	/**
	 * Getter for 'oneMonthOutNetworkRetailOfferedIndicator'.
	 *
	 * @return the oneMonthOutNetworkRetailOfferedIndicator.
	 */
	public Character getOneMonthOutNetworkRetailOfferedIndicator() {
		return oneMonthOutNetworkRetailOfferedIndicator;
	}

	/**
	 * Setter for 'oneMonthOutNetworkRetailOfferedIndicator'.
	 *
	 * @param oneMonthOutNetworkRetailOfferedIndicator the 'oneMonthOutNetworkRetailOfferedIndicator' to set
	 */
	public void setOneMonthOutNetworkRetailOfferedIndicator(
			Character oneMonthOutNetworkRetailOfferedIndicator) {
		this.oneMonthOutNetworkRetailOfferedIndicator = oneMonthOutNetworkRetailOfferedIndicator;
	}

	/**
	 * Getter for 'threeMonthInNetworkMailOfferedIndicator'.
	 *
	 * @return the threeMonthInNetworkMailOfferedIndicator.
	 */
	public Character getThreeMonthInNetworkMailOfferedIndicator() {
		return threeMonthInNetworkMailOfferedIndicator;
	}

	/**
	 * Setter for 'threeMonthInNetworkMailOfferedIndicator'.
	 *
	 * @param threeMonthInNetworkMailOfferedIndicator the 'threeMonthInNetworkMailOfferedIndicator' to set
	 */
	public void setThreeMonthInNetworkMailOfferedIndicator(
			Character threeMonthInNetworkMailOfferedIndicator) {
		this.threeMonthInNetworkMailOfferedIndicator = threeMonthInNetworkMailOfferedIndicator;
	}

	/**
	 * Getter for 'threeMonthOutNetworkMailOfferedIndicator'.
	 *
	 * @return the threeMonthOutNetworkMailOfferedIndicator.
	 */
	public Character getThreeMonthOutNetworkMailOfferedIndicator() {
		return threeMonthOutNetworkMailOfferedIndicator;
	}

	/**
	 * Setter for 'threeMonthOutNetworkMailOfferedIndicator'.
	 *
	 * @param threeMonthOutNetworkMailOfferedIndicator the 'threeMonthOutNetworkMailOfferedIndicator' to set
	 */
	public void setThreeMonthOutNetworkMailOfferedIndicator(
			Character threeMonthOutNetworkMailOfferedIndicator) {
		this.threeMonthOutNetworkMailOfferedIndicator = threeMonthOutNetworkMailOfferedIndicator;
	}

	/**
	 * Getter for 'creationTimestamp'.
	 *
	 * @return the creationTimestamp.
	 */
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * Setter for 'creationTimestamp'.
	 *
	 * @param creationTimestamp the 'creationTimestamp' to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * Getter for 'lastUpdateTimestamp'.
	 *
	 * @return the lastUpdateTimestamp.
	 */
	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	/**
	 * Setter for 'lastUpdateTimestamp'.
	 *
	 * @param lastUpdateTimestamp the 'lastUpdateTimestamp' to set
	 */
	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
}
