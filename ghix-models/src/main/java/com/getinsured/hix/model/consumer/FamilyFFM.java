package com.getinsured.hix.model.consumer;

/**
 * 
 * @author bhatt_s
 *
 */
@Deprecated
public class FamilyFFM {
	//ffm_id for this family
	private Integer ffmId;
	
	private Family family;
	
	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public Integer getFfmId() {
		return ffmId;
	}

	public void setFfmId(Integer ffmId) {
		this.ffmId = ffmId;
	}
	
	
}
