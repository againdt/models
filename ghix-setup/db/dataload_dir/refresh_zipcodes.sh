#!/bin/bash
source DB_ENV.sh
DATALOAD_DIR=/home/oracle/dataload_dir
#cd /home/oracle/dataload_dir
  
if [ $# -ne 2 ]
then
  echo " "
  echo "Usage $0 <SCHEMA_OWNER> <PASSWORD>"
  echo "Example: $0 GHIX1_OWNER GHIX1_OWNER"
  echo " "
  exit 2
fi

sqlplus $1/$2@$EZCONNECT @$DATALOAD_DIR/ext_county_zip_l_2load_zipcodes.sql 
wait

