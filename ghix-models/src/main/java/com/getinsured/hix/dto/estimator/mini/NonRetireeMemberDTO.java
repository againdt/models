package com.getinsured.hix.dto.estimator.mini;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * Member DTO of Retiree data.
 */
public class NonRetireeMemberDTO {

	/* Date of Birth of member in MM/DD/YYYY format */
	private String dateOfBirth;
	/* Relationship to the primary applicant SELF / SPOUSE / CHILD */
	private String relationshipToPrimary;
	/* Gender of the member (M/F) */
	private String gender;
	/* Seeking Coverage (Y/N) */
	private String seekingCoverage;
	/* Tobacco Usage (Y/N) */
	private String tobaccoUsage;

	public NonRetireeMemberDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(String relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(String seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
	}

	public String getTobaccoUsage() {
		return tobaccoUsage;
	}

	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}
}
