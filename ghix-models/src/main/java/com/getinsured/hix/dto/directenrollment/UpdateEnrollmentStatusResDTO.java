package com.getinsured.hix.dto.directenrollment;


/**
 * 
 * @author Vijay
 *
 */
public class UpdateEnrollmentStatusResDTO {
	
	public static enum STATUS{
		SUCCESS, FAILURE
	}
	/**
	 * status of this call,
	 * unless specified
	 */	
	private STATUS status = STATUS.SUCCESS;	
	
	private int code;
	
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}		
}