package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

@SuppressWarnings("serial")
public class EnrollmentMajorInfo implements Serializable{

	String txtFullName;
	String txtEmailId;
	String txtAddress;
	String txtPhone;
	String enrolleeId;
	String issuerId;
	String txtInsuranceCompany;
	String txtMetalTier;
	String txtPlanName;
	String txtGrossAmount;
	String txtAPTC;
	String txtExchangeType;
	String txtApplicationId;
	String txtPolicyId;
	String txtConfirmationId;
	String txtEffectiveDate;
	String txtServicedBy;
	String txtSubmitDate;
	String enrollmentId;
	String txtCarrierName;
	String state;
	String productType;
	String enrollmentStatus;
	String txtInsuranceType;
	String txtSsapApplicationid;
	String txtBenifitEndDate;
	

	public String getTxtInsuranceType() {
		return txtInsuranceType;
	}
	public void setTxtInsuranceType(String txtInsuranceType) {
		this.txtInsuranceType = txtInsuranceType;
	}
	public String getTxtFullName() {
		return txtFullName;
	}
	public void setTxtFullName(String txtFullName) {
		this.txtFullName = txtFullName;
	}
	public String getTxtEmailId() {
		return txtEmailId;
	}
	public void setTxtEmailId(String txtEmailId) {
		this.txtEmailId = txtEmailId;
	}
	public String getTxtAddress() {
		return txtAddress;
	}
	public void setTxtAddress(String txtAddress) {
		this.txtAddress = txtAddress;
	}
	public String getTxtPhone() {
		return txtPhone;
	}
	public void setTxtPhone(String txtPhone) {
		this.txtPhone = txtPhone;
	}
	public String getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(String enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	public String getTxtInsuranceCompany() {
		return txtInsuranceCompany;
	}
	public void setTxtInsuranceCompany(String txtInsuranceCompany) {
		this.txtInsuranceCompany = txtInsuranceCompany;
	}
	public String getTxtMetalTier() {
		return txtMetalTier;
	}
	public void setTxtMetalTier(String txtMetalTier) {
		this.txtMetalTier = txtMetalTier;
	}
	public String getTxtPlanName() {
		return txtPlanName;
	}
	public void setTxtPlanName(String txtPlanName) {
		this.txtPlanName = txtPlanName;
	}
	public String getTxtGrossAmount() {
		return txtGrossAmount;
	}
	public void setTxtGrossAmount(String txtGrossAmount) {
		this.txtGrossAmount = txtGrossAmount;
	}
	public String getTxtAPTC() {
		return txtAPTC;
	}
	public void setTxtAPTC(String txtAPTC) {
		this.txtAPTC = txtAPTC;
	}
	public String getTxtExchangeType() {
		return txtExchangeType;
	}
	public void setTxtExchangeType(String txtExchangeType) {
		this.txtExchangeType = txtExchangeType;
	}
	public String getTxtApplicationId() {
		return txtApplicationId;
	}
	public void setTxtApplicationId(String txtApplicationId) {
		this.txtApplicationId = txtApplicationId;
	}
	public String getTxtPolicyId() {
		return txtPolicyId;
	}
	public void setTxtPolicyId(String txtPolicyId) {
		this.txtPolicyId = txtPolicyId;
	}
	public String getTxtConfirmationId() {
		return txtConfirmationId;
	}
	public void setTxtConfirmationId(String txtConfirmationId) {
		this.txtConfirmationId = txtConfirmationId;
	}
	public String getTxtEffectiveDate() {
		return txtEffectiveDate;
	}
	public String getTxtCarrierName() {
		return txtCarrierName;
	}
	public void setTxtCarrierName(String txtCarrierName) {
		this.txtCarrierName = txtCarrierName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public void setTxtEffectiveDate(String txtEffectiveDate) {
		this.txtEffectiveDate = txtEffectiveDate;
	}
	public String getTxtServicedBy() {
		return txtServicedBy;
	}
	public void setTxtServicedBy(String txtServicedBy) {
		this.txtServicedBy = txtServicedBy;
	}
	public String getTxtSubmitDate() {
		return txtSubmitDate;
	}
	public void setTxtSubmitDate(String txtSubmitDate) {
		this.txtSubmitDate = txtSubmitDate;
	}
	public String getTxtSsapApplicationid() {
		return txtSsapApplicationid;
	}
	public void setTxtSsapApplicationid(String txtSsapApplicationid) {
		this.txtSsapApplicationid = txtSsapApplicationid;
	}
	public String getTxtBenifitEndDate() {
		return txtBenifitEndDate;
	}
	public void setTxtBenifitEndDate(String txtBenifitEndDate) {
		this.txtBenifitEndDate = txtBenifitEndDate;
	}
	public String getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	
}
