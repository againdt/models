package com.getinsured.coveredCA.dto.proxy;

import java.io.Serializable;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class Household implements Serializable{
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -3044703729641631847L;
	private String firstName;
	private String lastName;
	private String dob;
	private String homeStreetAddress;
	private String homeCity;
	private String homeZipCode;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getHomeStreetAddress() {
		return homeStreetAddress;
	}
	public void setHomeStreetAddress(String homeStreetAddress) {
		this.homeStreetAddress = homeStreetAddress;
	}
	public String getHomeCity() {
		return homeCity;
	}
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}
	public String getHomeZipCode() {
		return homeZipCode;
	}
	public void setHomeZipCode(String homeZipCode) {
		this.homeZipCode = homeZipCode;
	}
	
	
	

}
