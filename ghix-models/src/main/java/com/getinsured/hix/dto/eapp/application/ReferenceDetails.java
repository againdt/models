package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class ReferenceDetails implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long d2cEnrollmentId;
	private String giAppId;
	/**
	 * following properties are used for 
	 * selecting meta data.
	 */
	private String brandName;
	private String applicationType;
	private String configState;
	private String year;
	
	
	public Long getD2cEnrollmentId() {
		return d2cEnrollmentId;
	}
	public void setD2cEnrollmentId(Long d2cEnrollmentId) {
		this.d2cEnrollmentId = d2cEnrollmentId;
	}
	public String getGiAppId() {
		return giAppId;
	}
	public void setGiAppId(String giAppId) {
		this.giAppId = giAppId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public String getConfigState() {
		return configState;
	}
	public void setConfigState(String configState) {
		this.configState = configState;
	}
	
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public static ReferenceDetails getSampleInstance(){
		ReferenceDetails details = new ReferenceDetails();
		details.setApplicationType("HLT");
		details.setBrandName("Aetna");
		details.setConfigState("FL");
		details.setD2cEnrollmentId(null);
		details.setGiAppId("randomnumber");
		details.setYear("2015");
		
		return details;
	}
	
	public static ReferenceDetails getInstance(String brandName, String state, Long d2cEnrollmentId, String giAppId){
		ReferenceDetails details = new ReferenceDetails();
		details.setApplicationType("HLT");
		details.setBrandName(brandName);
		details.setConfigState(state);
		details.setD2cEnrollmentId(d2cEnrollmentId);
		details.setGiAppId(giAppId);
		details.setYear("2015"); //remove hardcoding
		return details; 
	}
	
	
}
