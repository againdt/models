package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;

import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.hix.platform.util.SortableEntity;


@Entity
@Table(name="issuer_representative")

public class IssuerRepresentative  implements Serializable, SortableEntity {
	
	public enum PrimaryContact { 
		YES, NO; 
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during adding New issuer representative.
	 * @author kunal
	 *
	 */
	public interface AddNewIssuerRepresentative extends Default{
		
	}
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IssuerRepresentative_Seq")
	@SequenceGenerator(name = "IssuerRepresentative_Seq", sequenceName = "issuer_representative_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@OneToOne
    @JoinColumn(name="user_id")
	private AccountUser userRecord;
	
	@OneToOne
	@JoinColumn(name="updatedBy")
	private AccountUser updatedBy;
	
	@Column(name="primary_contact", length=3)
	@Enumerated(EnumType.STRING)
	private PrimaryContact primaryContact;
	
	@Column(name="delegation_code")
	private String delegationCode;
	
	@Column(name="response_code")
	private Integer responseCode;
	
	@Column(name="response_description")
	private String responseDescription;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	private Location location;
	
	@NotEmpty(message="{err.firstName}",groups={IssuerRepresentative.AddNewIssuerRepresentative.class})
	@Column(name = "first_name")
	private String firstName;

	@NotEmpty(message="{err.lastName}",groups={IssuerRepresentative.AddNewIssuerRepresentative.class})
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "title")
	private String title;

	@Pattern(regexp="[0-9]{10}",message="{err.issuerPhoneRequired}",groups={IssuerRepresentative.AddNewIssuerRepresentative.class})
	@Column(name="phone", length=10)
	private String phone;
	
	@NotEmpty(message="{err.email}",groups={IssuerRepresentative.AddNewIssuerRepresentative.class})
	//commenting as per JIRA HIX-67244
	/*@Pattern(regexp="( )*(^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)?",groups={IssuerRepresentative.AddNewIssuerRepresentative.class})*/
	@Pattern(regexp="( )*(^[_A-Za-z0-9._]+([A-Za-z0-9.]+)@[A-Za-z0-9]+([.][a-zA-Z]{2,})+$)?",groups={IssuerRepresentative.AddNewIssuerRepresentative.class})
	@Column(name="email")
	private String email;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp")
	private Date created;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp")
	private Date updated;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "role")
	private String role;
	
	@Column(name="hios_issuer_id")
	private String hiosIssuerId;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public AccountUser getUserRecord() {
		return userRecord;
	}
	
	public void setUserRecord(AccountUser userRecord) {
		this.userRecord = userRecord;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public PrimaryContact getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(PrimaryContact primaryContact) {
		this.primaryContact = primaryContact;
	}
	
	public String getDelegationCode() {
		return delegationCode;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
		columnNames.add("primary_contact");
	 	columnNames.add("updatedby");
	 	columnNames.add("first_name");
	 	columnNames.add("last_name");
	 	columnNames.add("title");
	 	columnNames.add("creation_timestamp");
	 	columnNames.add("last_update_timestamp");
	 	columnNames.add("status");
	 	columnNames.add("hios_issuer_id");
	 	columnNames.add("email");
	 	return columnNames;
	}
}
