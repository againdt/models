package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.TenantIdPrePersistListener;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;

/**
 * The persistent class for the roles database table.
 * 
 */
@Audited
@Entity
@XmlRootElement(name="enrollment")
@XmlAccessorType(XmlAccessType.NONE)
//@XmlType(propOrder = { "actionCode", "sponsorName", "sponsorTaxIdNumber", "sponsorEIN","insurerName","insurerTaxIdNumber","CMSPlanID", "agentBrokerName","brokerFEDTaxPayerId","brokerTPAAccountNumber1","brokerTPAAccountNumber2","issuer", "enrollees", "createdOn", "updatedOn" })
@Table(name="ENROLLMENT")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class Enrollment implements Serializable, Comparable<Enrollment>, SortableEntity {
	public static enum Status{ SOLD, PENDING, ESIG_DONE }
	
/*	public interface EnrollmentBrokerValidator extends Default{
		
	}*/
	
	private static final long serialVersionUID = 1L;
//	private static Validator validator;
	
	public static final String ENROLLMENT_STATUS_CONFIRM = "CONFIRM";
	public static final String ENROLLMENT_STATUS_CANCEL = "CANCEL";
	public static final String ENROLLMENT_STATUS_TERM = "TERM";
	public static final String ENROLLMENT_STATUS_PENDING = "PENDING";
	public static final String ENROLLMENT_STATUS_ABORTED = "ABORTED";
	
	public static final String PERSON_TYPE_ENROLLEE = "ENROLLEE";
	public static final String PERSON_TYPE_SUBSCRIBER = "SUBSCRIBER";
	public static final String PERSON_TYPE_RESPONSIBLE_PERSON = "RESPONSIBLE_PERSON";
	public static final String PERSON_TYPE_CUSTODIAL_PARENT = "CUSTODIAL_PARENT";
	public static final String PERSON_TYPE_HOUSEHOLD_CONTACT = "HOUSEHOLD_CONTACT";
	
	@XmlElement(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Enrollment_Seq")
	@SequenceGenerator(name = "Enrollment_Seq", sequenceName = "ENROLLMENT_SEQ", allocationSize = 1)
	private Integer id;
	
	@XmlElement(name="sponsorName")
	@Column(name="SPONSOR_NAME")
	private String sponsorName ;
	
	@XmlElement(name="sponsorEIN")
	@Column(name="SPONSOR_EIN")
	private String sponsorEIN;

	@XmlElement(name="sponsorTaxIdNumber")
	@Column(name="SPONSOR_TAX_ID_NUMBER")
	private String sponsorTaxIdNumber;
	
	@Column(name = "SUBSCRIBER_NAME")
	private String subscriberName;
	
	@XmlElement(name="enrollmentTypeLkp")
	@OneToOne
	@JoinColumn(name="ENROLLMENT_TYPE_LKP",nullable= true)
    private LookupValue enrollmentTypeLkp ;
	
	@XmlElement(name="insurerName")
	@Column(name="INSURER_NAME")
	private String insurerName ;
	
	@Column(name="CONFIRMATION_NUMBER")
	private String confirmationNumber ;

	
	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	@XmlElement(name="insurerTaxIdNumber")
	@Column(name="INSURER_TAX_ID_NUMBER")
	private String insurerTaxIdNumber ;

	@XmlElement(name="issuerSubscriberIdentifier")
	@Column(name="ISSUER_SUBSCRIBER_IDENTIFIER")
	private String issuerSubscriberIdentifier;

	@XmlElement(name="exchgSubscriberIdentifier")
    @Column(name="EXCHG_SUBSCRIBER_IDENTIFIER")
    private String exchgSubscriberIdentifier;

	@XmlElement(name="insuranceTypeLkp")
	@OneToOne
    @JoinColumn(name="INSURANCE_TYPE_LKP",nullable= true)
	private LookupValue insuranceTypeLkp ;
	
	@XmlElement(name="enrollmentStatus")
	@OneToOne
    @JoinColumn(name="ENROLLMENT_STATUS_LKP",nullable= true)
	private LookupValue enrollmentStatusLkp ;
	
	@XmlElement(name="benefitStartDate")
	@Column(name="BENEFIT_EFFECTIVE_DATE")
	private Date benefitEffectiveDate ;
	
	@XmlElement(name="benefitEndDate")
	@Column(name="BENEFIT_END_DATE")
	private Date benefitEndDate ;
	
	@XmlElement(name="netPremiumAmt")
	@Column(name="NET_PREMIUM_AMT")
	private Float netPremiumAmt ;
	
    @XmlElement(name="healthCoveragePremiumAmt")
	@Column(name="GROSS_PREMIUM_AMT")
	private Float grossPremiumAmt ;
	
	@XmlElement(name="aptcAmt")
	@Column(name="APTC_AMT")
	private Float aptcAmt ;

	@XmlElement(name="stateSubsidy")
	@Column(name="STATE_SUBSIDY_AMT")
	private BigDecimal stateSubsidyAmt;
	
	
	@Column(name="CSR_AMT")
	private Float csrAmt ;
	
	@Column(name="SLCSP_AMT")
	private Float slcspAmt;
	
	@Column(name="CSR_LEVEL")
	private String csrLevel ;
	
	
	@Column(name = "EXCHANGE_ASSIGN_POLICY_NO")
	private String exchangeAssignPolicyNo;
	
	@Column(name = "ISSUER_ASSIGN_POLICY_NO")
	private String issuerAssignPolicyNo;
	
	@XmlElement(name="houseHoldCaseId")
	@Column(name="HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId ;
	
	@XmlElement(name="groupPolicyNumber")
	@Column(name="GROUP_POLICY_NUMBER")
	private String groupPolicyNumber ;
	
	@Column(name="BROKER_TPA_FLAG")
	private String brokerTPAFlag ;
	
	@XmlElement(name="brokerAgentName")
	@Column(name="AGENT_BROKER_NAME")
	private String agentBrokerName ;
	
	@XmlElement(name="insurerCMSPlanID")
	@Column(name="CMS_PLAN_ID")
	private String CMSPlanID ;

//	@Pattern(regexp="[0-9]*", message = "BrokerFedTaxPayerID should be number only.", groups={Enrollment.EnrollmentBrokerValidator.class})
	@XmlElement(name="brokerFEDTaxPayerId")
	@Column(name="BROKER_FED_TAX_PAYER_ID")
	private String brokerFEDTaxPayerId ;

//	@Pattern(regexp="[0-9]*", message = "BrokerTPAAccountNumber1 should be number only.", groups={Enrollment.EnrollmentBrokerValidator.class})
	@XmlElement(name="brokerTPAAccountNumber1")
	@Column(name="BROKER_TPA_ACCOUNT_NUMBER_1")
	private String brokerTPAAccountNumber1 ;
		
	@XmlElement(name="brokerTPAAccountNumber2")
	@Column(name="BROKER_TPA_ACCOUNT_NUMBER_2")
	private String brokerTPAAccountNumber2 ;
	
	@XmlElement(name="createdOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@XmlElement(name="updatedOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="financial_id")
	private FinancialInfo financialInfo;
	
	//@NotAudited 
/*	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ind_order_items_id")
	private PldOrderItem indOrderItem;
*/
	
	@Column(name="ind_order_items_id") 
	private Long pdOrderItemId;
	
	@Column(name="issuer_id") 
	private Integer issuerId;
	
	@Column(name="plan_id") 
	private Integer planId; 
	
	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="employer_id")
	@XmlElement(name="employer")
	private Employer employer;
		
	@Column(name = "PLAN_NAME")
	private String planName;

	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany
    @JoinColumn(name="ESIGNATURE_ID" ,insertable=false , updatable=false)
    private List<EnrollmentEsignature> enrollmentEsignature;

//	@NotAudited 
//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Audited
	@OneToOne
	@JoinColumn(name="created_by")
	private AccountUser createdBy;

	@Transient
	@XmlElement(name="QTYy")
	private int QTYy;

	@Transient
	@XmlElement(name="QTYn")
	private int QTYn;

	@XmlElement(name="QTYt")
	@Transient
	private int QTYt;

//	@NotAudited 
//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Audited
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;

	@OneToMany(mappedBy="enrollment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@XmlElement(name="enrollee")
	private List<Enrollee> enrollees;

	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="WORKSITE_ID")
	private EmployerLocation worksite;

	@Column(name="PLAN_COVERAGE_LEVEL")
	private String planCoverageLevel ;
	 
    @Column(name="FAMILY_COVERAGE_TIER")
	private String familyCoverageTier;
	
    @XmlElement(name="employeeContribution")
    @Column(name="EMPLOYEE_CONTRIBUTION")
     private Float employeeContribution ;
 		
    @XmlElement(name="employerContribution")
    @Column(name="EMPLOYER_CONTRIBUTION")
 	private Float employerContribution ;
	
	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy="enrollment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentEvent> enrollmentEvents;

	@XmlElement(name="paymentTxnId")
	@Column(name="PAYMENT_TXN_ID")
	private String paymentTxnId;
	
	public String getPaymentTxnId() {
		return paymentTxnId;
	}

	public void setPaymentTxnId(String paymentTxnId) {
		this.paymentTxnId = paymentTxnId;
	}

	@Transient
	private String formatUpdatedOn;

	@Transient
	private String formatCreatedOn;

	@Transient
	private String formatBenefitEffectiveDate;

	@Transient
	private String formatBenefitEndDate;
	
	@Transient
	@XmlElement(name="responsiblePerson")
	private Enrollee responsiblePerson;
	
	@Transient
	@XmlElement(name="householdContact")
	private Enrollee householdContact;
	
	@Column(name="SADP")
	private String sadp ;
	
	@Column(name="ASSISTER_BROKER_ID")
	private Integer assisterBrokerId;
	
	@XmlElement(name="brokerRole")
	@Column(name="BROKER_ROLE")
	private String brokerRole;

	@Column(name="PLAN_LEVEL")
	private String planLevel;

	@Transient
	@XmlElement(name="csrAmt")
	private Float totalCSRAmt ;
	
	@Column(name="ENROLLMENT_CONFIRMATION_DATE")
	private Date enrollmentConfirmationDate;
	
	@NotAudited
	@Column(name="CARRIER_SEND_FLAG")
	private String carrierResendFlag;
	
	@Transient
	private String externalEmployerId;
	
	@XmlElement(name="employerCaseId")
	@Column(name="EMPLOYER_CASE_ID", length = 20)
	private String employerCaseId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="ABORT_TIMESTAMP")
	private Date abortTimestamp;	
	
	@Column(name="HIOS_ISSUER_ID", length=10)
	private String hiosIssuerId;
	
	@Transient
	private String giHouseholdId;
	
	@Transient
	private String appEventReason;
	
	
	/**
	 * This is added as Integer as of now, as we don't need CMR_HOUSEHOLD object/data.
	 * 
	 * Used only to return enrollments based on CMR_HOUSEHOLD_ID 
	 */
	@Column(name="CMR_HOUSEHOLD_ID")
	private Integer cmrHouseHoldId;
	
	/**
	 * Comma seperated names of all enrollees.
	 * Used for return enrollments by cmr_household_id api.
	 */
	@Transient
	private String enrolleeNames;
	
	@Transient
	private String enrollmentStatusLabel;
	
	@NotAudited
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="ENROLLMENT_PLAN_ID", referencedColumnName="id" )
	private EnrollmentPlan enrollmentPlan;
	
	@Column(name = "EMPLOYEE_ID")
	private Integer employeeId;

	@Column(name = "STATE_EXCHANGE_CODE")
	private String stateExchangeCode;
	
	@Column(name = "PARTNER_ASSIGNED_CONSUMER_ID")
	private String partnerAssignedConsumerId;
	
	
	@Column(name = "APTC_PERCENTAGE")
	private Float aptcPercentage;
	
	@Column(name = "TICKET_ID")
	private String ticketId;
	
	@OneToOne
    @JoinColumn(name="ENROLLMENTMODALITY",nullable= true)
	private LookupValue enrollmentModalityLkp;
	
	@OneToOne
    @JoinColumn(name="EXCHANGE_TYPE",nullable= true)
	private LookupValue exchangeTypeLkp;
	
	@Column(name = "SSAP_APPLICATION_ID")
	private Long ssapApplicationid;
	
	
	@OneToOne
    @JoinColumn(name="PREFFERED_CONTENT_TIME",nullable= true)
	private LookupValue prefferedContactTime;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	@Column(name="CARRIER_APP_URL")
	private String carrierAppUrl ;

	
	
	@Column(name="D2C_ENROLLMENT_ID")
	private Long d2cEnrollmentId;
	
	@Column(name="CARRIER_APP_ID")
	private String carrierAppId;
	
	@Column(name="CARRIER_APP_UID")
	private String carrierAppUID;
	
	@Column(name="DATE_CLOSED")
	private Date dateClosed ;
	

	@XmlElement(name="revID")
	@Transient
	private int rev;
	
	@XmlElement(name="gS03")
	@Transient
	private String gS03;
	
	@XmlElement(name="gS02")
	@Transient
	private String gS02;
	
	@XmlElement(name="aptcDate")
	@Column(name="APTC_EFF_DATE")
	private Date aptcEffDate;

	@XmlElement(name="stateSubsidyDate")
	@Column(name="STATE_SUBSIDY_EFF_DATE")
	private Date stateSubsidyEffDate;

	@XmlElement(name="csrDate")
	@Column(name="CSR_EFF_DATE")
	private Date csrEffDate;
	
	@XmlElement(name="totalIndivResponsibilityDate")
	@Column(name="TOT_INDV_RESP_EFF_DATE")
	private Date netPremEffDate;
	
	@XmlElement(name="totalEmpResponsibilityDate")
	@Column(name="TOT_EMPLR_RESP_EFF_DATE")
	private Date emplContEffDate;
	
	@XmlElement(name="totalPremiumDate")
	@Column(name="TOT_PREM_EFF_DATE")
	private Date grossPremEffDate;
	
	@Column(name="SLCSP_EFF_DATE")
	private Date slcspEffDate;
	
	@Column(name="EMPLOYEE_APPLICATION_ID")
	private Long employeeAppId;
	
	@Column(name="EMPLOYER_ENROLLMENT_ID")
	private Integer employerEnrollmentId;
	
	@Column(name = "RENEWAL_FLAG")
	private String renewalFlag;
	
	
	@Column(name="EHB_AMT")
	private Float ehbAmt;
	
	@Column(name="STATE_EHB_AMT")
	private Float stateEhbAmt;
	
    @Column(name="DNTL_EHB_AMT")
	private Float dntlEhbAmt;
	
	@Column(name="EHB_EFF_DATE")
	private Date ehbEffDate;
	
	@Column(name="STATE_EHB_EFF_DATE")
	private Date stateEhbEffDate;
	
	@Column(name="DNTL_EHB_EFF_DATE")
	private Date dntlEhbEffDate;
	
	@Column(name="CARRIER_STATUS")
	private String carrierStatus;
	
	@Column(name="FOLLOW_UP_MSG")
	private String followUpMessage;
	
	@Column(name="RATE_UP")
	private String rateUp;
	
	@Column(name="CARRIER_LAST_UPDT_DT")
	private Date carrierLastUpdatedDate ;
	
	@Column(name="CAP_AGENT_ID")
	private Integer capAgentId;
	
	@Column(name="CO_INSURANCE")
	private String coInsurance;
	
	@Column(name="VERIFICATION_REASON")
	private String verificationEvent;

	//HIX-50209
	@Column(name="DUPLICATE_ENROLLMENT_ID")
	private Integer duplicateEnrollmentId;
	//HIX-50209
	
	@Column(name="SUBMITTED_TO_CARRIER_DATE")
	private Date submittedToCarrierDate ;
	
	@Column(name="TERM_LENGTH")
	private Integer termLength;
	
	@Column(name="BENEFIT_AMOUNT")
	private Float benefitAmount;
	
	@Column(name="EHB_PERCENT")
	private Float ehbPercent;
	
	@Column(name="STATE_EHB_PERCENT")
	private Float stateEhbPercent;
	
	@Column(name="DNTL_EHB_PRM_DOLLAR_VAL")
	private Float dntlEssentialHealthBenefitPrmDollarVal;
	
	//HIX-64133
	@Column(name="PD_HOUSEHOLD_ID")
	private Integer pdHouseholdId;
	
	@Column(name="CSR_MULTIPLIER")
	private Float csrMultiplier;
	
	@Column(name="CHANGE_PLAN_ALLOWED")
	private String changePlanAllowed;
		
	@Transient
	private Character enrollmentReason;
	
	@Transient
	private String disableEffectiveDate;
	
	@Transient
	private String readerStatus;
	
	@Transient
	private String readerErrorMsg;	
	
	@Transient
	private String priorEnrollmentTermFlag;
	
	@XmlElement(name="priorEnrollmentId")
	@Column(name="PRIOR_ENROLLMENT_ID")
	private Long priorEnrollmentId;
	
	@XmlElement(name="lastEnrollmentId")
	@Column(name="LAST_ENROLLMENT_ID")
	private Integer lastEnrollmentId;
	
	@Transient
	private String issuerLogo;
	
	@Transient
	private Date ind19CovStDate;
	
	/* 
	 * Jira ID: HIX-77342
	 * Added this column to capture AccountUser who moves the application from Any other status to Pending status 
	 */
	@Audited
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBMITTED_BY")
	private AccountUser submittedBy;
	
	@Column(name="FINANCIAL_EFFECTIVE_DATE")
	private Date financialEffectiveDate;
	
	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}

	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}
	
	@Column(name="LOG_GLOBAL_ID")
	private String logGlobalId;
	
	@OneToMany(mappedBy="enrollment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentPremium> enrollmentPremiums;
	
	@Column(name="GI_WS_PAYLOAD_ID")
	private Integer giWsPayloadId;
	
	@Transient
	private String fileName834;
	
	@Transient
	private boolean showFinancialEffectivedate;
	
	@Transient
	@XmlElement(name="ISA09")
	private String isa09;
	
	@Transient
	@XmlElement(name="ISA10")
	private String isa10;
	
	@Transient
	@XmlElement(name="ISA13")
	private Integer isa13;
	
	@Transient
	@XmlElement(name="GS04")
	private String gs04;
	
	@Transient
	@XmlElement(name="GS05")
	private String gs05;
	
	@Transient
	@XmlElement(name="GS06")
	private Integer gs06;
	
	@Transient
	@XmlElement(name="ST02")
	private String st02;
	
	@Column(name = "RENEWAL_SAME_PLAN")
	private String renewalSamePlan;
	
	@Column(name = "PRIOR_SSAP_APPLICATION_ID")
	private Long priorSsapApplicationid;
	
	@Transient
	@XmlElement(name="catastrophicEligible")
	private String catastrophicEligible;
	
	@Transient
	private Boolean isRenTermEvent = Boolean.FALSE;
	
	@Transient
	private Boolean isPremiumCalculated=Boolean.FALSE;
	
	@Transient
	private Date retroDate;
	
	@Transient
	private Boolean isRatingAreaChanged = Boolean.FALSE;
	
	@Transient
	private String slcspPlanid;
	
	@Transient
	private  boolean isAPTCUpdated=Boolean.FALSE;

	@Transient
	private boolean isStateSubsidyUpdated=Boolean.FALSE;
	
	@Transient
	//@OneToOne(mappedBy="enrollment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EnrollmentIssuerCommision enrollmentIssuerCommision;
	
	@Transient
	private Float maxAPTCAmt;

	@Transient
	private BigDecimal maxStateSubsidyAmt;
	
	@Column(name="EXT_HOUSEHOLD_CASE_ID")
	private String externalHouseHoldCaseId ;
	
	@Column(name="PREM_PAID_TO_DATE_END")
	private Date premiumPaidToDateEnd;
	
	public boolean isShowFinancialEffectivedate() {
		return showFinancialEffectivedate;
	}

	public void setShowFinancialEffectivedate(boolean showFinancialEffectivedate) {
		this.showFinancialEffectivedate = showFinancialEffectivedate;
	}
	
	public String getLogGlobalId() {
		return logGlobalId;
	}

	public void setLogGlobalId(String logGlobalId) {
		this.logGlobalId = logGlobalId;
	}
	
	public String getReaderStatus() {
		return readerStatus;
	}

	public void setReaderStatus(String readerStatus) {
		this.readerStatus = readerStatus;
	}

	public String getReaderErrorMsg() {
		return readerErrorMsg;
	}

	public void setReaderErrorMsg(String readerErrorMsg) {
		this.readerErrorMsg = readerErrorMsg;
	}
	
	public String getDisableEffectiveDate() {
		return disableEffectiveDate;
	}

	public void setDisableEffectiveDate(String disableEffectiveDate) {
		this.disableEffectiveDate = disableEffectiveDate;
	}

	public Date getAptcEffDate() {
		return aptcEffDate;
	}

	public void setAptcEffDate(Date aptcEffDate) {
		this.aptcEffDate = aptcEffDate;
	}

	public Date getCsrEffDate() {
		return csrEffDate;
	}

	public void setCsrEffDate(Date csrEffDate) {
		this.csrEffDate = csrEffDate;
	}

	public Date getNetPremEffDate() {
		return netPremEffDate;
	}

	public void setNetPremEffDate(Date netPremEffDate) {
		this.netPremEffDate = netPremEffDate;
	}

	public Date getEmplContEffDate() {
		return emplContEffDate;
	}

	public void setEmplContEffDate(Date emplContEffDate) {
		this.emplContEffDate = emplContEffDate;
	}

	public Date getGrossPremEffDate() {
		return grossPremEffDate;
	}

	public void setGrossPremEffDate(Date grossPremEffDate) {
		this.grossPremEffDate = grossPremEffDate;
	}

	public String getgS03() {
		return gS03;
	}

	public void setgS03(String gS03) {
		this.gS03 = gS03;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	
	public Enrollment() {}
		
	public String getCarrierResendFlag() {
		return carrierResendFlag;
	}

	public void setCarrierResendFlag(String carrierResendFlag) {
		this.carrierResendFlag = carrierResendFlag;
	}

	public Float getTotalCSRAmt() {
		return totalCSRAmt;
	}

	public void setTotalCSRAmt(Float totalCSRAmt) {
		this.totalCSRAmt = totalCSRAmt;
	}

	public String getAgentBrokerName() {
		return agentBrokerName;
	}

	public Float getAptcAmt() {
		return aptcAmt;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public String getBrokerFEDTaxPayerId() {
		return brokerFEDTaxPayerId;
	}

	public String getBrokerTPAAccountNumber1() {
		return brokerTPAAccountNumber1;
	}
   

	public String getBrokerTPAAccountNumber2() {
		return brokerTPAAccountNumber2;
	}
		
	public String getBrokerTPAFlag() {
		return brokerTPAFlag;
	}
	
	public String getCMSPlanID() {
		return CMSPlanID;
	}
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	

	public Float getCsrAmt() {
		return csrAmt;
	}
	
	public Float getEmployeeContribution() {
		return employeeContribution;
	}

	public Employer getEmployer() {
		return employer;
	}

	public Float getEmployerContribution() {
		return employerContribution;
	}
	
	
	public List<EnrollmentEsignature> getEnrollmentEsignature() {
		return enrollmentEsignature;
	}
	

	public List<EnrollmentEvent> getEnrollmentEvents() {
		return enrollmentEvents;
	}
	
	public LookupValue getEnrollmentStatusLkp() {
		return enrollmentStatusLkp;
	}
	
	public LookupValue getEnrollmentTypeLkp() {
		return enrollmentTypeLkp;
	}
	
	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}
	
	public String getFamilyCoverageTier() {
		return familyCoverageTier;
	}

	public FinancialInfo getFinancialInfo() {
		return financialInfo;
	}

	public String getFormatBenefitEffectiveDate() {
		return getFormatedDate(this.benefitEffectiveDate);
		
	}

	public String getFormatBenefitEndDate() {
		return getFormatedDate(this.benefitEndDate);
	}

	public String getFormatCreatedOn() {
		return getFormatedDate(this.createdOn);
		
	}

	public String getFormatedDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmdd");
		String updatedOn = formatter.format(date);
		return updatedOn;

	}

	public String getFormatUpdatedOn() {
		return getFormatedDate(this.updatedOn);
		
	}

	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	

	
	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}

	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	public Enrollee getHouseholdContact() {
		return householdContact;
	}

	public Integer getId() {
		return id;
	}


	public LookupValue getInsuranceTypeLkp() {
		return insuranceTypeLkp;
	}

	public String getInsurerName() {
		return insurerName;
	}

	public String getInsurerTaxIdNumber() {
		return insurerTaxIdNumber;
	}

	public String getIssuerSubscriberIdentifier() {
		return issuerSubscriberIdentifier;
	}

	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}

	public String getPlanCoverageLevel() {
		return planCoverageLevel;
	}

	
	public String getPlanName() {
		return planName;
	}

	public int getQTYn() {
		return QTYn;
	}

	public int getQTYt() {
		return QTYt;
	}
	
	public int getQTYy() {
		return QTYy;
	}

	
	public String getSadp() {
		return sadp;
	}

	public Enrollee getResponsiblePerson() {
		return responsiblePerson;
	}

	public void setResponsiblePerson(Enrollee responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}

	public String getSponsorEIN() {
		return sponsorEIN;
	}
	public String getSponsorName() {
		return sponsorName;
	}

	public String getSponsorTaxIdNumber() {
		return sponsorTaxIdNumber;
	}

	public int getSubscriberCount() {
		return 1;
	}

	public String getSubscriberName() {
		return subscriberName;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}
	public EmployerLocation getWorksite() {
		return worksite;
	}
	public boolean isPending(){
		if (this.getEnrollmentStatusLkp().getLookupValueCode().equals(ENROLLMENT_STATUS_PENDING)){
			return true;
		}
		else{
			return false;
		}
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
//		runCustomConstraintCheck(Enrollment.EnrollmentBrokerValidator.class);
		runCustomConstraintCheck();
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate());
//		runCustomConstraintCheck(Enrollment.EnrollmentBrokerValidator.class);
		runCustomConstraintCheck();
	}
	
	public void setAgentBrokerName(String agentBrokerName) {
		this.agentBrokerName = agentBrokerName;
	}

	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public void setBrokerFEDTaxPayerId(String brokerFEDTaxPayerId) {
		this.brokerFEDTaxPayerId = brokerFEDTaxPayerId;
	}

	public void setBrokerTPAAccountNumber1(String brokerTPAAccountNumber1) {
		this.brokerTPAAccountNumber1 = brokerTPAAccountNumber1;
	}

	public void setBrokerTPAAccountNumber2(String brokerTPAAccountNumber2) {
		this.brokerTPAAccountNumber2 = brokerTPAAccountNumber2;
	}

	public void setBrokerTPAFlag(String brokerTPAFlag) {
		this.brokerTPAFlag = brokerTPAFlag;
	}

	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setCsrAmt(Float csrAmt) {
		this.csrAmt = csrAmt;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	

	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}

	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}

	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}

	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}

	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}
	public void setEmployer(Employer employer) {
		this.employer = employer;
	}
	
	
	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}

	public void setEnrollees(List<Enrollee> enrollees) {
		this.enrollees = enrollees;
	}
	
	public List<Enrollee> getEnrollees() {
		return enrollees;
	}
	
	
	public void setEnrollmentEsignature(
			List<EnrollmentEsignature> enrollmentEsignature) {
		this.enrollmentEsignature = enrollmentEsignature;
	}
	
	public void setEnrollmentEvents(List<EnrollmentEvent> enrollmentEvents) {
		this.enrollmentEvents = enrollmentEvents;
	}

	/*public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}*/

	public void setEnrollmentStatusLkp(LookupValue enrollmentStatusLkp) {
		this.enrollmentStatusLkp = enrollmentStatusLkp;
	}

	public void setEnrollmentTypeLkp(LookupValue enrollmentTypeLkp) {
		this.enrollmentTypeLkp = enrollmentTypeLkp;
	}

	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}

	public void setFamilyCoverageTier(String familyCoverageTier) {
		this.familyCoverageTier = familyCoverageTier;
	}

	public void setFinancialInfo(FinancialInfo financialInfo) {
		this.financialInfo = financialInfo;
	}

	public void setFormatBenefitEffectiveDate(String formatBenefitEffectiveDate) {
		this.formatBenefitEffectiveDate = formatBenefitEffectiveDate;
	}

	public void setFormatBenefitEndDate(String formatBenefitEndDate) {
		this.formatBenefitEndDate = formatBenefitEndDate;
	}

	public void setFormatCreatedOn(String formatCreatedOn) {
		this.formatCreatedOn = formatCreatedOn;
	}

	public void setFormatUpdatedOn(String formatUpdatedOn) {
		this.formatUpdatedOn = formatUpdatedOn;
	}

	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}

	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	public void setHouseholdContact(Enrollee householdContact) {
		this.householdContact = householdContact;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

	public void setInsuranceTypeLkp(LookupValue insuranceTypeLkp) {
		this.insuranceTypeLkp = insuranceTypeLkp;
	}

	
	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

	public void setInsurerTaxIdNumber(String insurerTaxIdNumber) {
		this.insurerTaxIdNumber = insurerTaxIdNumber;
	}

	public void setIssuerSubscriberIdentifier(String issuerSubscriberIdentifier) {
		this.issuerSubscriberIdentifier = issuerSubscriberIdentifier;
	}

	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}

	public void setPlanCoverageLevel(String planCoverageLevel) {
		this.planCoverageLevel = planCoverageLevel;
	}
	
	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public void setQTYn(int qTYn) {
		QTYn = qTYn;
	}

	public void setQTYt(int qTYt) {
		QTYt = qTYt;
	}

	public void setQTYy(int qTYy) {
		QTYy = qTYy;
	}

	
	public void setSadp(String sadp) {
		this.sadp = sadp;
	}

	public void setSponsorEIN(String sponsorEIN) {
		this.sponsorEIN = sponsorEIN;
	}

	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}

	public void setSponsorTaxIdNumber(String sponsorTaxIdNumber) {
		this.sponsorTaxIdNumber = sponsorTaxIdNumber;
	}

	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public void setWorksite(EmployerLocation worksite) {
		this.worksite = worksite;
	}

	
	public Integer getAssisterBrokerId() {
		return assisterBrokerId;
	}

	public void setAssisterBrokerId(Integer assisterBrokerId) {
		this.assisterBrokerId = assisterBrokerId;
	}

	public String getBrokerRole() {
		return brokerRole;
	}

	public void setBrokerRole(String brokerRole) {
		this.brokerRole = brokerRole;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Date getEnrollmentConfirmationDate() {
		return enrollmentConfirmationDate;
	}

	public void setEnrollmentConfirmationDate(Date enrollmentConfirmationDate) {
		this.enrollmentConfirmationDate = enrollmentConfirmationDate;
	}

	public String getExternalEmployerId() {
		return externalEmployerId;
	}

	public void setExternalEmployerId(String externalEmployerId) {
		this.externalEmployerId = externalEmployerId;
	}

	public String getEmployerCaseId() {
		return employerCaseId;
	}

	public void setEmployerCaseId(String employerCaseId) {
		this.employerCaseId = employerCaseId;
	}

	public Date getAbortTimestamp() {
		return abortTimestamp;
	}

	public void setAbortTimestamp(Date abortTimestamp) {
		this.abortTimestamp = abortTimestamp;
	}

	public Integer getCmrHouseHoldId() {
		return cmrHouseHoldId;
	}

	public void setCmrHouseHoldId(Integer cmrHouseHoldId) {
		this.cmrHouseHoldId = cmrHouseHoldId;
	}

	public String getEnrolleeNames() {
		return enrolleeNames;
	}

	public void setEnrolleeNames(String enrolleeNames) {
		this.enrolleeNames = enrolleeNames;
	}

	public String getEnrollmentStatusLabel() {
		return enrollmentStatusLabel;
	}

	public void setEnrollmentStatusLabel(String enrollmentStatusLabel) {
		this.enrollmentStatusLabel = enrollmentStatusLabel;
	}

	public EnrollmentPlan getEnrollmentPlan() {
		return enrollmentPlan;
	}

	public void setEnrollmentPlan(EnrollmentPlan enrollmentPlan) {
		this.enrollmentPlan = enrollmentPlan;
	}

	public String getStateExchangeCode() {
		return stateExchangeCode;
	}

	public void setStateExchangeCode(String stateExchangeCode) {
		this.stateExchangeCode = stateExchangeCode;
	}

	public String getPartnerAssignedConsumerId() {
		return partnerAssignedConsumerId;
	}

	public void setPartnerAssignedConsumerId(String partnerAssignedConsumerId) {
		this.partnerAssignedConsumerId = partnerAssignedConsumerId;
	}

	public Float getAptcPercentage() {
		return aptcPercentage;
	}

	public void setAptcPercentage(Float aptcPercentage) {
		this.aptcPercentage = aptcPercentage;
	}
	
	public Long getSsapApplicationid() {
		return ssapApplicationid;
	}

	public void setSsapApplicationid(Long ssapApplicationid) {
		this.ssapApplicationid = ssapApplicationid;
	}
	
	public Long getEmployeeAppId() {
		return employeeAppId;
	}

	public void setEmployeeAppId(Long employeeAppId) {
		this.employeeAppId = employeeAppId;
	}

	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}

	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns Enrollee with given member Id
	 * 
	 * @param memberId
	 * @return
	 */
	public Enrollee getEnrolleeByMemberId(String memberId){
		Enrollee enrollee = null;
		
		if(enrollees != null && enrollees.size() > 0){
			for(Enrollee enr : enrollees){
				if(enr.getExchgIndivIdentifier().equalsIgnoreCase(memberId) && 
						(enr.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_ENROLLEE) || enr.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_SUBSCRIBER))){
					enrollee = enr;
					break;
				}
			}
		}
		return enrollee;
	}
	
	public Enrollee getEnrolleeById(Integer enrolleeId){
		Enrollee enrollee = null;
		if(enrollees != null && enrollees.size() > 0){
			for(Enrollee enr : enrollees){
				if(enr.getId() == enrolleeId){
					enrollee = enr;
					break;
				}
			}
		}
		return enrollee;
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns Returns all Active Enrollees for Enrollment.
	 * 
	 * @return
	 */
	public List<Enrollee> getActiveEnrolleesForEnrollment(){
		List<Enrollee> activeEnrolleeList = new ArrayList<Enrollee>();
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_SUBSCRIBER))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_TERM))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_ABORTED))){
					activeEnrolleeList.add(enrollee);
				}
			}
		}
		return activeEnrolleeList;
	}
	
	
	public List<Enrollee> getNonCancelEnrollees(){
		List<Enrollee> activeEnrolleeList = new ArrayList<Enrollee>();
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if((enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_SUBSCRIBER))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_ABORTED))){
					activeEnrolleeList.add(enrollee);
				}
			}
		}
		return activeEnrolleeList;
	}

	public Enrollee getSubscriberEnrollee() {
		if (enrollees != null) {
			for (Enrollee enrollee : enrollees) {
				if (enrollee.getSubscriberNewFlag() != null && enrollee.getSubscriberNewFlag().equals("Y")) {
					return enrollee;
				}
			}
		}
		return null;
	}
	/**
	 * @author Aditya-S
	 * 
	 * Returns Subscriber Person for Enrollment.
	 * 
	 * @return
	 */
	public Enrollee getSubscriberForEnrollment(){
		Enrollee subscriber = null;
		if(enrollees != null && enrollees.size() > 0){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp().getLookupValueCode() != null  && enrollee.getPersonTypeLkp().getLookupValueCode().equals(PERSON_TYPE_SUBSCRIBER)){
					subscriber = enrollee;
					break;
				}
			}
		}
		return subscriber;
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns Responsible Person for Enrollment.
	 * 
	 * @return
	 */
	public Enrollee getResponsiblePersonForEnrollment(){
		Enrollee responsiblePersonEnrollee = null;
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_RESPONSIBLE_PERSON)){
					responsiblePersonEnrollee = enrollee;
					break;
				}
			}
		}
		return responsiblePersonEnrollee;
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns Household Contact Person for Enrollment.
	 * 
	 * @return
	 */
	public Enrollee getHouseholdContactForEnrollment(){
		Enrollee householdEnrollee = null;
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_HOUSEHOLD_CONTACT)){
					householdEnrollee = enrollee;
					break;
				}
			}
		}
		return householdEnrollee;
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns Subscriber and All Enrollees for Enrollment, Returns all Active and Non-Active Enrolles.
	 * 
	 * @return
	 */
	public List<Enrollee> getEnrolleesAndSubscriber(){
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp() != null && (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_SUBSCRIBER))&&
						enrollee.getEnrolleeLkpValue()!=null && !enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_ABORTED)
						){
					enrolleeList.add(enrollee);
				}
			}
		}
		return enrolleeList;
	}
	
	/**
	 * @author Aditya-S
	 * 
	 * Returns count of Active enrollees for enrollment.
	 * 
	 * @return activeEnrolleeCount
	 */
	public int getActiveEnrolleesCount(){
		int activeEnrolleeCount = 0;
		if(enrollees != null){
			for(Enrollee enrollee : enrollees){
				if(enrollee.getPersonTypeLkp()!=null && enrollee.getEnrolleeLkpValue()!=null&& (enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(PERSON_TYPE_SUBSCRIBER))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_TERM))
						&&(!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_ABORTED))){
					activeEnrolleeCount++;
				}
			}
		}
		return activeEnrolleeCount;
	}

	public String getgS02() {
		return gS02;
	}

	public void setgS02(String gS02) {
		this.gS02 = gS02;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getGiHouseholdId() {
		return giHouseholdId;
	}

	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public LookupValue getEnrollmentModalityLkp() {
		return enrollmentModalityLkp;
	}

	public void setEnrollmentModalityLkp(LookupValue enrollmentModalityLkp) {
		this.enrollmentModalityLkp = enrollmentModalityLkp;
	}

	public LookupValue getExchangeTypeLkp() {
		return exchangeTypeLkp;
	}

	public void setExchangeTypeLkp(LookupValue exchangeTypeLkp) {
		this.exchangeTypeLkp = exchangeTypeLkp;
	}

	public LookupValue getPrefferedContactTime() {
		return prefferedContactTime;
	}

	public void setPrefferedContactTime(LookupValue prefferedContactTime) {
		this.prefferedContactTime = prefferedContactTime;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getCarrierAppUrl() {
		return carrierAppUrl;
	}

	public void setCarrierAppUrl(String carrierAppUrl) {
		this.carrierAppUrl = carrierAppUrl;
	}

	public Long getD2cEnrollmentId() {
		return d2cEnrollmentId;
	}

	public void setD2cEnrollmentId(Long d2cEnrollmentId) {
		this.d2cEnrollmentId = d2cEnrollmentId;
	}

	public String getCarrierAppId() {
		return carrierAppId;
	}

	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}

	public String getCarrierAppUID() {
		return carrierAppUID;
	}

	public void setCarrierAppUID(String carrierAppUID) {
		this.carrierAppUID = carrierAppUID;
	}

	public Date getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	public String getCarrierStatus() {
		return carrierStatus;
	}

	public void setCarrierStatus(String carrierStatus) {
		this.carrierStatus = carrierStatus;
	}

	public String getFollowUpMessage() {
		return followUpMessage;
	}

	public void setFollowUpMessage(String followUpMessage) {
		this.followUpMessage = followUpMessage;
	}

	public String getRateUp() {
		return rateUp;
	}

	public void setRateUp(String rateUp) {
		this.rateUp = rateUp;
	}

	public Date getCarrierLastUpdatedDate() {
		return carrierLastUpdatedDate;
	}

	public void setCarrierLastUpdatedDate(Date carrierLastUpdatedDate) {
		this.carrierLastUpdatedDate = carrierLastUpdatedDate;
	}

	public Integer getCapAgentId() {
		return capAgentId;
	}

	public void setCapAgentId(Integer capAgentId) {
		this.capAgentId = capAgentId;
	}
	
	
	//HIX-50209
	public Integer getDuplicateEnrollmentId() {
		return duplicateEnrollmentId;
	}

	public void setDuplicateEnrollmentId(Integer duplicateEnrollmentId) {
		this.duplicateEnrollmentId = duplicateEnrollmentId;
	}
	//HIX-50209
	
	
	
	public Character getEnrollmentReason() {
		return enrollmentReason;
	}

	public String getVerificationEvent() {
		return verificationEvent;
	}

	public void setVerificationEvent(String verificationEvent) {
		this.verificationEvent = verificationEvent;
	}
		
	public void setEnrollmentReason(Character enrollmentReason) {
		this.enrollmentReason = enrollmentReason;
	}

	public String getCoInsurance() {
		return coInsurance;
	}

	public void setCoInsurance(String coInsurance) {
		this.coInsurance = coInsurance;
	}

	public Date getSubmittedToCarrierDate() {
		return submittedToCarrierDate;
	}

	public void setSubmittedToCarrierDate(Date submittedToCarrierDate) {
		this.submittedToCarrierDate = submittedToCarrierDate;
	}

	public Integer getTermLength() {
		return termLength;
	}

	public void setTermLength(Integer termLength) {
		this.termLength = termLength;
	}

	public Float getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(Float benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public Float getDntlEssentialHealthBenefitPrmDollarVal() {
		return dntlEssentialHealthBenefitPrmDollarVal;
	}

	public void setDntlEssentialHealthBenefitPrmDollarVal(
			Float dntlEssentialHealthBenefitPrmDollarVal) {
		this.dntlEssentialHealthBenefitPrmDollarVal = dntlEssentialHealthBenefitPrmDollarVal;
	}

	public Integer getPdHouseholdId() {
		return pdHouseholdId;
	}

	public void setPdHouseholdId(Integer pdHouseholdId) {
		this.pdHouseholdId = pdHouseholdId;
	}
	
	public String getPriorEnrollmentTermFlag() {
		return priorEnrollmentTermFlag;
	}

	public void setPriorEnrollmentTermFlag(String priorEnrollmentTermFlag) {
		this.priorEnrollmentTermFlag = priorEnrollmentTermFlag;
	}
	
	public Long getPriorEnrollmentId() {
		return priorEnrollmentId;
	}

	public void setPriorEnrollmentId(Long priorEnrollmentId) {
		this.priorEnrollmentId = priorEnrollmentId;
	}
	
	public Integer getLastEnrollmentId() {
		return lastEnrollmentId;
	}

	public void setLastEnrollmentId(Integer lastEnrollmentId) {
		this.lastEnrollmentId = lastEnrollmentId;
	}

	public Long getPdOrderItemId() {
		return pdOrderItemId;
	}

	public void setPdOrderItemId(Long pdOrderItemId) {
		this.pdOrderItemId = pdOrderItemId;
	}

	public String getIssuerLogo() {
		return issuerLogo;
	}

	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}

	
	
	public String getAppEventReason() {
		return appEventReason;
	}

	public void setAppEventReason(String appEventReason) {
		this.appEventReason = appEventReason;
	}

	/**
	 * @return the submittedBy
	 */
	public AccountUser getSubmittedBy() {
		return submittedBy;
	}

	/**
	 * @param submittedBy the submittedBy to set
	 */
	public void setSubmittedBy(AccountUser submittedBy) {
		this.submittedBy = submittedBy;
	}

	
	
	public String getFileName834() {
		return fileName834;
	}

	public void setFileName834(String fileName834) {
		this.fileName834 = fileName834;
	}

	public void updateEnrollees(){
		if(enrollees != null && !enrollees.isEmpty()){
			for(Enrollee enrollee : enrollees){
				setExchgSubscriberIdentifier(enrollee.getExchgSubscriberIdentifier());
				setIssuerSubscriberIdentifier(enrollee.getIssuerSubscriberIdentifier());

				if(enrollee.getSubscriberNewFlag().equalsIgnoreCase("Y")){
					LookupValue lookupValue = new LookupValue();
					lookupValue.setLookupValueCode("SUBSCRIBER");
					enrollee.setPersonTypeLkp(lookupValue);
				}

				HealthCoverage healthCoverage = enrollee.getHealthCoverage();
				if( healthCoverage != null){
					this.setHouseHoldCaseId(healthCoverage.getHouseholdOrEmployeeCaseID());
					this.setCMSPlanID(healthCoverage.getClassOfContractCode());
					if(healthCoverage.getExchgAssignedPolicyID()!=null){
						this.setId(Integer.parseInt(healthCoverage.getExchgAssignedPolicyID()));
					}
					enrollee.setEffectiveStartDate(healthCoverage.getEffectiveStartDate());
					enrollee.setEffectiveEndDate(healthCoverage.getEffectiveEndDate());
					enrollee.setLastPremiumPaidDate(healthCoverage.getLastPremiumPaidDate());
					enrollee.setEmployerGroupNo(healthCoverage.getEmployerGroupNo());
					enrollee.setHealthCoveragePolicyNo(healthCoverage.getHealthCoveragePolicyNo());
					if(enrollee.getSubscriberNewFlag().equalsIgnoreCase("Y")) {
						this.setPremiumPaidToDateEnd(healthCoverage.getPremiumPaidToDateEnd());
					}
				}
				if(this.getId()==null && enrollee.getExchgAssignedPolicyID()!=null && !"CA".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))){
					this.setId(Integer.parseInt(enrollee.getExchgAssignedPolicyID()));
				}
				MemberReportingCategory memberReportingCategory = enrollee.getMemberReportingCategory();
				
				if(memberReportingCategory != null){
					AdditionalMaintReason  additionalMaintReason  = memberReportingCategory.getAdditionalMaintReason();
					
					if(additionalMaintReason != null && additionalMaintReason.getLookupValueCode() != null){
						LookupValue lookupValue = new LookupValue();
						lookupValue.setLookupValueCode(additionalMaintReason.getLookupValueCode());
						enrollee.setEnrolleeLkpValue(lookupValue);
						if(lookupValue.getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL) || lookupValue.getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_TERM)){
							enrollee.setDisenrollTimestamp(new TSDate());
						}
					}
				}
					
			}
		}
	}	
	
	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	@Override
	public int compareTo(Enrollment enrollment) {
		if(enrollment != null){
			return this.updatedOn.compareTo(enrollment.getUpdatedOn());
		}
		return 0;
	}

	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public Date getInd19CovStDate() {
		return ind19CovStDate;
	}

	public void setInd19CovStDate(Date ind19CovStDate) {
		this.ind19CovStDate = ind19CovStDate;
	}

	/**
	 * @return the enrollmentPremiums
	 */
	public List<EnrollmentPremium> getEnrollmentPremiums() {
		return enrollmentPremiums;
	}

	/**
	 * @param enrollmentPremiums the enrollmentPremiums to set
	 */
	public void setEnrollmentPremiums(List<EnrollmentPremium> enrollmentPremiums) {
		this.enrollmentPremiums = enrollmentPremiums;
	}

	/**
	 * @return the isa09
	 */
	public String getIsa09() {
		return isa09;
	}

	/**
	 * @param isa09 the isa09 to set
	 */
	public void setIsa09(String isa09) {
		this.isa09 = isa09;
	}

	/**
	 * @return the isa10
	 */
	public String getIsa10() {
		return isa10;
	}

	/**
	 * @param isa10 the isa10 to set
	 */
	public void setIsa10(String isa10) {
		this.isa10 = isa10;
	}

	/**
	 * @return the isa13
	 */
	public Integer getIsa13() {
		return isa13;
	}

	/**
	 * @param isa13 the isa13 to set
	 */
	public void setIsa13(Integer isa13) {
		this.isa13 = isa13;
	}

	/**
	 * @return the gs04
	 */
	public String getGs04() {
		return gs04;
	}

	/**
	 * @param gs04 the gs04 to set
	 */
	public void setGs04(String gs04) {
		this.gs04 = gs04;
	}

	/**
	 * @return the gs05
	 */
	public String getGs05() {
		return gs05;
	}

	/**
	 * @param gs05 the gs05 to set
	 */
	public void setGs05(String gs05) {
		this.gs05 = gs05;
	}

	/**
	 * @return the gs06
	 */
	public Integer getGs06() {
		return gs06;
	}

	/**
	 * @param gs06 the gs06 to set
	 */
	public void setGs06(Integer gs06) {
		this.gs06 = gs06;
	}

	/**
	 * @return the st02
	 */
	public String getSt02() {
		return st02;
	}

	/**
	 * @param st02 the st02 to set
	 */
	public void setSt02(String st02) {
		this.st02 = st02;
	}

	public String getRenewalSamePlan() {
		return renewalSamePlan;
	}

	public void setRenewalSamePlan(String renewalSamePlan) {
		this.renewalSamePlan = renewalSamePlan;
	}

	public String getCatastrophicEligible() {
		return catastrophicEligible;
	}

	public void setCatastrophicEligible(String catastrophicEligible) {
		this.catastrophicEligible = catastrophicEligible;
	}

	/**
	 * @return the isRenTermEvent
	 */
	public Boolean getIsRenTermEvent() {
		return isRenTermEvent;
	}

	/**
	 * @param isRenTermEvent the isRenTermEvent to set
	 */
	public void setIsRenTermEvent(Boolean isRenTermEvent) {
		this.isRenTermEvent = isRenTermEvent;
	}

	public Boolean getIsPremiumCalculated() {
		return isPremiumCalculated;
	}

	public void setIsPremiumCalculated(Boolean isPremiumCalculated) {
		this.isPremiumCalculated = isPremiumCalculated;
	}

	public Date getRetroDate() {
		return retroDate;
	}

	public void setRetroDate(Date retroDate) {
		this.retroDate = retroDate;
	}

	public Float getSlcspAmt() {
		return slcspAmt;
	}

	public void setSlcspAmt(Float slcspAmt) {
		this.slcspAmt = slcspAmt;
	}

	public Date getSlcspEffDate() {
		return slcspEffDate;
	}

	public void setSlcspEffDate(Date slcspEffDate) {
		this.slcspEffDate = slcspEffDate;
	}

	public Float getEhbPercent() {
		return ehbPercent;
	}

	public void setEhbPercent(Float ehbPercent) {
		this.ehbPercent = ehbPercent;
	}

	public Float getEhbAmt() {
		return ehbAmt;
	}

	public void setEhbAmt(Float ehbAmt) {
		this.ehbAmt = ehbAmt;
	}

	public Date getEhbEffDate() {
		return ehbEffDate;
	}

	public void setEhbEffDate(Date ehbEffDate) {
		this.ehbEffDate = ehbEffDate;
	}

	public Float getDntlEhbAmt() {
		return dntlEhbAmt;
	}

	public void setDntlEhbAmt(Float dntlEhbAmt) {
		this.dntlEhbAmt = dntlEhbAmt;
	}

	public Date getDntlEhbEffDate() {
		return dntlEhbEffDate;
	}

	public void setDntlEhbEffDate(Date dntlEhbEffDate) {
		this.dntlEhbEffDate = dntlEhbEffDate;
	}

	public Float getStateEhbPercent() {
		return stateEhbPercent;
	}

	public void setStateEhbPercent(Float stateEhbPercent) {
		this.stateEhbPercent = stateEhbPercent;
	}

	public Date getStateEhbEffDate() {
		return stateEhbEffDate;
	}

	public void setStateEhbEffDate(Date stateEhbEffDate) {
		this.stateEhbEffDate = stateEhbEffDate;
	}

	public Float getStateEhbAmt() {
		return stateEhbAmt;
	}

	public void setStateEhbAmt(Float stateEhbAmt) {
		this.stateEhbAmt = stateEhbAmt;
	}

	/**
	 * @return the csrMultiplier
	 */
	public Float getCsrMultiplier() {
		return csrMultiplier;
	}

	/**
	 * @param csrMultiplier the csrMultiplier to set
	 */
	public void setCsrMultiplier(Float csrMultiplier) {
		this.csrMultiplier = csrMultiplier;
	}
	
	
	public Boolean isRatingAreaChanged() {
		return isRatingAreaChanged;
	}

	public void setIsRatingAreaChanged(Boolean isRatingAreaChanged) {
		this.isRatingAreaChanged = isRatingAreaChanged;
	}

	public Map<Integer, EnrollmentPremium> getEnrollmentPremiumMap() {
		Map<Integer, EnrollmentPremium> premiumMap = new HashMap<Integer, EnrollmentPremium>();
		if(null != this.enrollmentPremiums){
			for(EnrollmentPremium enrollmentPremium : this.enrollmentPremiums){
				premiumMap.put(enrollmentPremium.getMonth(), enrollmentPremium);
			}
		}
		return premiumMap;
	}
	
	public EnrollmentPremium getEnrollmentPremiumForMonth(Integer month) {
		return this.getEnrollmentPremiumMap().get(month);
	}

	/**
	 * @return the giWsPayloadId
	 */
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	/**
	 * @param giWsPayloadId the giWsPayloadId to set
	 */
	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public String getSlcspPlanid() {
		return slcspPlanid;
	}

	public void setSlcspPlanid(String slcspPlanid) {
		this.slcspPlanid = slcspPlanid;
	}

	public boolean isAPTCUpdated() {
		return isAPTCUpdated;
	}

	public void setAPTCUpdated(boolean isAPTCUpdated) {
		this.isAPTCUpdated = isAPTCUpdated;
	}

	public EnrollmentIssuerCommision getEnrollmentIssuerCommision() {
		return enrollmentIssuerCommision;
	}

	public void setEnrollmentIssuerCommision(EnrollmentIssuerCommision enrollmentIssuerCommision) {
		this.enrollmentIssuerCommision = enrollmentIssuerCommision;
	}
	
	public Float getMaxAPTCAmt() {
		return maxAPTCAmt;
	}

	public void setMaxAPTCAmt(Float maxAPTCAmt) {
		this.maxAPTCAmt = maxAPTCAmt;
	}

	public BigDecimal getStateSubsidyAmt() {
		return stateSubsidyAmt;
	}

	public void setStateSubsidyAmt(BigDecimal stateSubsidyAmt) {
		this.stateSubsidyAmt = stateSubsidyAmt;
	}

	public Date getStateSubsidyEffDate() {
		return stateSubsidyEffDate;
	}

	public void setStateSubsidyEffDate(Date stateSubsidyEffDate) {
		this.stateSubsidyEffDate = stateSubsidyEffDate;
	}

	public boolean isStateSubsidyUpdated() {
		return isStateSubsidyUpdated;
	}

	public void setStateSubsidyUpdated(boolean stateSubsidyUpdated) {
		isStateSubsidyUpdated = stateSubsidyUpdated;
	}

	public BigDecimal getMaxStateSubsidyAmt() {
		return maxStateSubsidyAmt;
	}

	public void setMaxStateSubsidyAmt(BigDecimal maxStateSubsidyAmt) {
		this.maxStateSubsidyAmt = maxStateSubsidyAmt;
	}

	/**
	 *  
	 * @param groupClassName
	 */
	//Commenting due to blocker HIX-106628 on Weblogic 
/*	private void runCustomConstraintCheck(Class<?> groupClassName){
		
		if(validator == null){
			ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
			validator = factory.getValidator();
		}
		Set<ConstraintViolation<Enrollment>> constraintViolations =  validator.validate(this,  groupClassName);

		if(constraintViolations != null && !constraintViolations.isEmpty()){
			StringBuilder stringbuilder = new StringBuilder(250);
			for (ConstraintViolation<Enrollment> violation : constraintViolations) {
				 stringbuilder.append("Constraint violation for field: ");
				 stringbuilder.append(violation.getPropertyPath().toString());
				 stringbuilder.append(" Received value: "+violation.getInvalidValue());
				 stringbuilder.append(" Error: "+violation.getMessage());
			 }
			throw new RuntimeException(stringbuilder.toString());
		}
	}*/
	
	private void runCustomConstraintCheck() {
		if(null != this.brokerFEDTaxPayerId && !StringUtils.isAlphanumeric(this.brokerFEDTaxPayerId)) {
			throw new RuntimeException(brokerErrorMessage("brokerFEDTaxPayerId" , this.brokerFEDTaxPayerId));
		}
		if(null != this.brokerTPAAccountNumber1 && !StringUtils.isAlphanumeric(this.brokerTPAAccountNumber1)) {
			throw new RuntimeException(brokerErrorMessage("brokerTPAAccountNumber1" , this.brokerTPAAccountNumber1));
		}
	}

	private String brokerErrorMessage(String field, String value) {
		StringBuilder stringbuilder = new StringBuilder(250);
		stringbuilder.append("Constraint violation for field: ");
		stringbuilder.append(field);
		stringbuilder.append(" Received value: "+ value);
		stringbuilder.append(" Error: Field should be alphanumeric");
		return stringbuilder.toString();
	}

	public Long getPriorSsapApplicationid() {
		return priorSsapApplicationid;
	}

	public void setPriorSsapApplicationid(Long priorSsapApplicationid) {
		this.priorSsapApplicationid = priorSsapApplicationid;
	}

	public String getExternalHouseHoldCaseId() {
		return externalHouseHoldCaseId;
	}

	public void setExternalHouseHoldCaseId(String externalHouseHoldCaseId) {
		this.externalHouseHoldCaseId = externalHouseHoldCaseId;
	}

	public String getChangePlanAllowed() {
		return changePlanAllowed;
	}

	public void setChangePlanAllowed(String changePlanAllowed) {
		this.changePlanAllowed = changePlanAllowed;
	}

	public Date getPremiumPaidToDateEnd() {
		return premiumPaidToDateEnd;
	}

	public void setPremiumPaidToDateEnd(Date premiumPaidToDateEnd) {
		this.premiumPaidToDateEnd = premiumPaidToDateEnd;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
        columnNames.add("id");
        columnNames.add("issuerId");
        columnNames.add("CMSPlanID");
        columnNames.add("planName");
        columnNames.add("planLevel");
        columnNames.add("insurerName");
        columnNames.add("benefitEffectiveDate");
        columnNames.add("benefitEndDate");
        columnNames.add("exchgSubscriberIdentifier");
        columnNames.add("groupPolicyNumber");
        return columnNames;
	}
	
}
