package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

public class TkmTicketTaskResponse extends GHIXResponse implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<TkmTicketTasks> tkmTicketTaskList = null;
	
	private List<TaskFormProperties> tkmTaskProperties = null;

	private boolean bActivitiProcessCompleted;
	
	private List<TkmUserDTO> tkmUserDTO = null;
	public List<TkmTicketTasks> getTkmTicketTaskList() {
		return tkmTicketTaskList;
	}

	public void setTkmTicketTaskList(List<TkmTicketTasks> tkmTicketTaskList) {
		this.tkmTicketTaskList = tkmTicketTaskList;
	}

	/**
	 * @return Returns true if the task process is completed
	 */
	public boolean isActivitiProcessCompleted() {
		return bActivitiProcessCompleted;
	}

	/**
	 * @param Sets the status of the task process completion
	 */
	public void setActivitiProcessCompleted(boolean bActivitiProcessCompleted) {
		this.bActivitiProcessCompleted = bActivitiProcessCompleted;
	}

	public List<TaskFormProperties> getTkmTaskProperties() {
		return tkmTaskProperties;
	}

	public void setTkmTaskProperties(List<TaskFormProperties> tkmTaskProperties) {
		this.tkmTaskProperties = tkmTaskProperties;
	}

	public List<TkmUserDTO> getTkmUserDTO() {
		return tkmUserDTO;
	}
	public void setTkmUserDTO(List<TkmUserDTO> tkmUserDTO) {
		this.tkmUserDTO = tkmUserDTO;
	}
	
	
}
