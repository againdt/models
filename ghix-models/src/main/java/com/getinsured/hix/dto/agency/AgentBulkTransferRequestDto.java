package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

public class AgentBulkTransferRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Integer> agentIds;
	private Integer targetAgentId;
	private String action;
	private List<DelegatedIndividualRequestDto> individualIds;
	
	public List<Integer> getAgentIds() {
		return agentIds;
	}
	public void setAgentIds(List<Integer> agentIds) {
		this.agentIds = agentIds;
	}
	public Integer getTargetAgentId() {
		return targetAgentId;
	}
	public void setTargetAgentId(Integer targetAgentId) {
		this.targetAgentId = targetAgentId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<DelegatedIndividualRequestDto> getIndividualIds() {
		return individualIds;
	}
	public void setIndividualIds(List<DelegatedIndividualRequestDto> individualIds) {
		this.individualIds = individualIds;
	}
	
	

}
