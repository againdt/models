package com.getinsured.hix.dto.plandisplay;

public class DrugDTO implements Cloneable {
	
	private String drugID;
	private String drugName;
	private String drugType;
	private String drugNdc;
	private String drugDosage;
	private String drugFairPrice;
	private String drugCost;
	private String drugTier;
	private String drugRxCode;
	private String isDrugCovered;
	private String authRequired;
	private String stepTherapyRequired;
	private String genericID;
	private String genericName;
	private String genericNdc;
	private String genericDosage;
	private String genericFairPrice;
	private String genericTier;
	private String genericCost;
	private String genericRxCode;
	private String isGenericCovered;
	private String annualRefills;
	private String genericAuthRequired;
	private String genericStepTherapyRequired;
	
	public String getDrugID() {
		return drugID;
	}
	public void setDrugID(String drugID) {
		this.drugID = drugID;
	}
	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public String getDrugType() {
		return drugType;
	}
	public void setDrugType(String drugType) {
		this.drugType = drugType;
	}
	public String getDrugNdc() {
		return drugNdc;
	}
	public void setDrugNdc(String drugNdc) {
		this.drugNdc = drugNdc;
	}
	public String getDrugDosage() {
		return drugDosage;
	}
	public void setDrugDosage(String drugDosage) {
		this.drugDosage = drugDosage;
	}
	public String getDrugFairPrice() {
		return drugFairPrice;
	}
	public void setDrugFairPrice(String drugFairPrice) {
		this.drugFairPrice = drugFairPrice;
	}
	public String getDrugCost() {
		return drugCost;
	}
	public void setDrugCost(String drugCost) {
		this.drugCost = drugCost;
	}
	public String getDrugTier() {
		return drugTier;
	}
	public void setDrugTier(String drugTier) {
		this.drugTier = drugTier;
	}
	public String getAuthRequired() {
		return authRequired;
	}
	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}
	public String getStepTherapyRequired() {
		return stepTherapyRequired;
	}
	public void setStepTherapyRequired(String stepTherapyRequired) {
		this.stepTherapyRequired = stepTherapyRequired;
	}
	public String getGenericID() {
		return genericID;
	}
	public void setGenericID(String genericID) {
		this.genericID = genericID;
	}
	public String getGenericName() {
		return genericName;
	}
	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}
	public String getGenericNdc() {
		return genericNdc;
	}
	public void setGenericNdc(String genericNdc) {
		this.genericNdc = genericNdc;
	}
	public String getGenericDosage() {
		return genericDosage;
	}
	public void setGenericDosage(String genericDosage) {
		this.genericDosage = genericDosage;
	}
	public String getGenericFairPrice() {
		return genericFairPrice;
	}
	public void setGenericFairPrice(String genericFairPrice) {
		this.genericFairPrice = genericFairPrice;
	}
	public String getGenericTier() {
		return genericTier;
	}
	public void setGenericTier(String genericTier) {
		this.genericTier = genericTier;
	}
	public String getGenericCost() {
		return genericCost;
	}
	public void setGenericCost(String genericCost) {
		this.genericCost = genericCost;
	}
	public String getIsDrugCovered() {
		return isDrugCovered;
	}
	public void setIsDrugCovered(String isDrugCovered) {
		this.isDrugCovered = isDrugCovered;
	}
	public String getIsGenericCovered() {
		return isGenericCovered;
	}
	public void setIsGenericCovered(String isGenericCovered) {
		this.isGenericCovered = isGenericCovered;
	}
	public String getAnnualRefills() {
		return annualRefills;
	}
	public void setAnnualRefills(String annualRefills) {
		this.annualRefills = annualRefills;
	}
	@Override
	public DrugDTO clone() {
		try {
			return (DrugDTO)super.clone();
	    }
	    catch(CloneNotSupportedException e) {
	    	throw new AssertionError(e);
	    }
	}
	public String getDrugRxCode() {
		return drugRxCode;
	}
	public void setDrugRxCode(String drugRxCode) {
		this.drugRxCode = drugRxCode;
	}
	public String getGenericRxCode() {
		return genericRxCode;
	}
	public void setGenericRxCode(String genericRxCode) {
		this.genericRxCode = genericRxCode;
	}
	public String getGenericAuthRequired() {
		return genericAuthRequired;
	}
	public void setGenericAuthRequired(String genericAuthRequired) {
		this.genericAuthRequired = genericAuthRequired;
	}
	public String getGenericStepTherapyRequired() {
		return genericStepTherapyRequired;
	}
	public void setGenericStepTherapyRequired(String genericStepTherapyRequired) {
		this.genericStepTherapyRequired = genericStepTherapyRequired;
	}
}
