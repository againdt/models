/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRateBenefit;

/**
 * @author Mishra_m
 *
 */
public class PlanRateBenefitResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<PlanRateBenefit> planRateBenefitList;
	private Plan plan;

	public List<PlanRateBenefit> getPlanRateBenefitList() {
		return planRateBenefitList;
	}

	public void setPlanRateBenefitList(List<PlanRateBenefit> planRateBenefitList) {
		this.planRateBenefitList = planRateBenefitList;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	
	
	
}
