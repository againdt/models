package com.getinsured.hix.dto.eapp.grammer;

import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;


/**
 * UI template cannot have arbitrary shape
 * This class specifies the dimension and scope 
 * along with validation rules
 * @author root
 *
 */
public class UITemplate {

	private Map<String, UISection> sections;
	
	public Map<String, UISection> getSections() {
		return sections;
	}

	public void setSections(Map<String, UISection> sections) {
		this.sections = sections;
	}
	
	public static UITemplate getSampleInstance(){
		UITemplate template = new UITemplate();
		Map<String, UISection> sections = new HashMap<String, UISection>();
		//template.setSections()
		sections.put("basic", UISection.getSampleInstance());
		sections.put("primary", UISection.getSampleInstance());
		template.setSections(sections);
		return template;
	}
	
	public static void main(String args[]){
		UITemplate template = UITemplate.getSampleInstance();
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		System.out.println(gson.toJson(template));
	}
	
}
