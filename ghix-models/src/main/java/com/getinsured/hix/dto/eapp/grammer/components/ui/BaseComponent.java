package com.getinsured.hix.dto.eapp.grammer.components.ui;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.eapp.grammer.UIComponent;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class BaseComponent extends UIComponent { 

	@Override
	public boolean isValid() {
		return false;
	}

	@Override
	public UIComponent parse(String json) {
		UIComponent comp = new BaseComponent();
		System.out.println(json);
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		comp = gson.fromJson(json, BaseComponent.class);
		return comp;
	}

	@Override
	public String format() {
		return null;
	}

	@Override
	public List<String> compile(String json) {
		List<String> errors = new ArrayList<String>();
		errors.add("Are you fresh off the boat? We don't support this component type");
		return errors;
	}

	

}
