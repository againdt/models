--liquibase formatted sql

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='ACCOUNT_SWITCH_EMPLOYEE'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_ADD_BROKER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_ADD_COMMENT'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_ADD_ESIGNATURE'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_ADD_PHOTO'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_APPROVE_BROKER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_DE_DESIGNATE_BROKER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_DESIGNATE_BROKER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_DESIGNATE_EMPLOYER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_EDIT_BROKER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_EDIT_PAYMENTINFO'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_BROKER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_COMMENT'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_CONSUMER'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_EMPLOYEE'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_LANGUAGE'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_NOTIFICATION'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER'), (select id from permissions   where name='BROKER_VIEW_PAYMENTINFO'));
