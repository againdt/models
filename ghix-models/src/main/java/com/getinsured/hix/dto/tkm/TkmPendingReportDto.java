package com.getinsured.hix.dto.tkm;

public class TkmPendingReportDto {
	
	public enum DUEIN
	{
		OVERDUE("Overdue"), TODAY("Today"), TOMORROW("Tomorrow"), ZERO_TO_SEVEN_DAYS("0-7 Days"), EIGHT_TO_FOURTEEN_DAYS("8-14 Days");
		
		@SuppressWarnings("unused")
		private final String dueIn;

		private DUEIN(String dueIn) {
			this.dueIn = dueIn;
		}
	}



	private Integer assigneeId;
	private String assigneeName;
	private String queueName;
	private Long openCount;
	private Long recBetwn_0_7;
	private Long recBetwn_8_14;
	private Long recBetwn_15_30;
	private Long recBetwn_31_45;
	private Long recAbove_45;

	public Integer getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Integer assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Long getRecBetwn_0_7() {
		return recBetwn_0_7;
	}

	public void setRecBetwn_0_7(Long recBetwn_0_7) {
		this.recBetwn_0_7 = recBetwn_0_7;
	}

	public Long getRecBetwn_8_14() {
		return recBetwn_8_14;
	}

	public void setRecBetwn_8_14(Long recBetwn_8_14) {
		this.recBetwn_8_14 = recBetwn_8_14;
	}

	public Long getRecBetwn_15_30() {
		return recBetwn_15_30;
	}

	public void setRecBetwn_15_30(Long recBetwn_15_30) {
		this.recBetwn_15_30 = recBetwn_15_30;
	}

	public Long getRecBetwn_31_45() {
		return recBetwn_31_45;
	}

	public void setRecBetwn_31_45(Long recBetwn_31_45) {
		this.recBetwn_31_45 = recBetwn_31_45;
	}

	public Long getRecAbove_45() {
		return recAbove_45;
	}

	public void setRecAbove_45(Long recAbove_45) {
		this.recAbove_45 = recAbove_45;
	}

	public Long getOpenCount() {
		return openCount;
	}

	public void setOpenCount(Long openCount) {
		this.openCount = openCount;
	}
	
	

	public TkmPendingReportDto(Integer assigneeId, String assigneeName,
			String queueName, Long openCount, Long recBetwn_0_7,
			Long recBetwn_8_14, Long recBetwn_15_30, Long recBetwn_31_45,
			Long recAbove_45) {
		super();
		this.assigneeId = assigneeId;
		this.assigneeName = assigneeName;
		this.queueName = queueName;
		this.openCount = openCount;
		this.recBetwn_0_7 = recBetwn_0_7;
		this.recBetwn_8_14 = recBetwn_8_14;
		this.recBetwn_15_30 = recBetwn_15_30;
		this.recBetwn_31_45 = recBetwn_31_45;
		this.recAbove_45 = recAbove_45;
	}

	public void merge(TkmPendingReportDto other) {
		this.queueName += ","+other.queueName;
		this.openCount += other.openCount;
		this.recBetwn_0_7 += other.recBetwn_0_7;
		this.recBetwn_8_14 += other.recBetwn_8_14;
		this.recBetwn_15_30 += other.recBetwn_15_30;
		this.recBetwn_31_45 += other.recBetwn_31_45;
		this.recAbove_45 += other.recAbove_45;	
		
		
	}
	
}
