package com.getinsured.hix.platform.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Captures all EndPoints details for different GHIX artifacts.
 * Under GHIX Application we have 2 main categories:-
 * 		1. GHIX-WEB URL --> used by
 * 					a. GHIX-WEB to access its own resources like NoticeTypes etc.
 * 					b. To make reverse call to GHIX-WEB from any Service WARS; for example:- GHIX-PLANDISPLAY consumes GHIX-WEB REST CLIENT to get PLAN-MGMT data.
 *
 * 		2. Service WARS --> used by
 * 					a. GHIX-WEB and other modules to invoke REST service exposed by respective Service WARS.
 * 					   example:- GHIX-WEB and GHIX-SHOP consumes PLAN_DISPLAY services.
 * 								 GHIX-SHOP consumes GHIX-AHBX services.
 *
 *
 *
 * Overall structure is to have a serviceURL constant for a module and an inner class to have all services exposed.
 * 			example:- "ghixPlandisplaySvcURL" and an inner class PlandisplayEndpoints which will have all services exposed by PLAN-DISPLAY module
 *
 *
 * PLS NOTE:- configuration.properties will only have serviceURL constant for a module configured and "NO REST service URL mapping".
 * 			  Please add properties under #EndPoints details for different GHIX artifacts - Starts section in configuration.properties files.
 *
 *
 */

@Component
public final class GhixEndPoints {

	public static String UPDATE_PLANSTATUS_URL;
	public static String ENROLLMENT_URL;
	public static String PLANDISPLAY_URL;
	public static String SHOP_URL;
	public static String ELIGIBILITY_URL;
	public static String GHIXWEB_SERVICE_URL;
	public static String GHIX_AHBX_URL;
	public static String TICKETMGMT_URL;
	public static String PLAN_MGMT_URL;
	public static String GHIX_EE_URL;
	public static String PRESCREEN_SVC_URL;
	public static String GHIX_BROKER_URL;
	public static String GHIX_SERFF_SERVICE_URL;
	public static String MS_SCREENER_URL;
	public static String MS_SCREENER_SERVICE_URL;

	public static String HUB_SERVICE_URL;
	public static String FFM_SERVICE_URL;
	public static String AFFILIATE_SERVICE_URL;
	public static String FINANCE_SERVICE_URL;
	public static String CONSUMER_SERVICE_URL;
	public static String APPSERVER_URL;

	public static String GHIX_SSAP_SVC_URL;

	public static String GHIX_SSAP_INTEGRATION_URL;

	public static String GHIX_ELIGIBILITY_SVC_URL;

	public static String GHIX_HUB_INTEGRATION_URL;

	public static String D2C_SERVICE_URL;
	public static String D2C_UHC_URL;
	public static String D2C_HUMANA_URL;
	public static String D2C_AETNA_URL;
	public static String D2C_HCSC_URL;
	public static String D2C_ANTHEM_URL;
	public static String D2C_UHC_STM_URL;
	public static String D2C_ASSURANT_STM_URL;
	public static String GHIX_EAPP_SVC_URL;

	public static String FEEDS_URL;
	
	public static String IDENTITY_SVC_URL;
	
	public static String COVERED_CA_PROXY_URL;
	
	//HIX-105846 - MS STARR URL
	public static String MS_STARR_SVC_URL;
	
	public static String EXTERNAL_ASSISTER_URL;
	
	public static String GHIX_ELIGIBILITY_ENGINE;
	
	public static String GHIXHIX_SERVICE_URL;

	@Value("#{configProp['appServer']}")
	public void setAPPSERVER_URL(String aPPSERVER_URL) {
		APPSERVER_URL = aPPSERVER_URL;
	}

	@Value("#{configProp['ghixEnrollmentServiceURL']}")
	public void setENROLLMENT_URL(String eNROLLMENT_URL) {
		ENROLLMENT_URL = eNROLLMENT_URL;
	}

	@Value("#{configProp['ghixWebServiceUrl']}")
	public void setGhixWebServiceUrl(String ghixWebServiceUrl) {
		GHIXWEB_SERVICE_URL = ghixWebServiceUrl;
	}

	@Value("#{configProp['ghixPlandisplayServiceURL']}")
	public void setGhixPlandisplayServiceURL(String ghixPlandisplayServiceURL) {
		PLANDISPLAY_URL = ghixPlandisplayServiceURL;
	}

	@Value("#{configProp['ghixShopServiceURL']}")
	public void setGhixShopServiceURL(String ghixShopServiceURL) {
		SHOP_URL = ghixShopServiceURL;
	}

	@Value("#{configProp['ghixEligibilityServiceURL']}")
	public void setGhixEligibilityServiceURL(String ghixEligibilityServiceURL) {
		ELIGIBILITY_URL = ghixEligibilityServiceURL;
	}

	@Value("#{configProp['ghixAHBXServiceURL']}")
	public void setGhixAHBXServiceURL(String ghixAHBXServiceURL) {
		GHIX_AHBX_URL = ghixAHBXServiceURL;
	}

	@Value("#{configProp['ghixTicketMgmtServiceURL']}")
	public void setGhixTicketMgmtServiceURL(String ghixTicketMgmtServiceURL) {
		TICKETMGMT_URL = ghixTicketMgmtServiceURL;
	}

	@Value("#{configProp['ghixPlanMgmtServiceURL']}")
	public void setGhixPlanMgmtServiceURL(String ghixPlanMgmtServiceURL) {
		PLAN_MGMT_URL = ghixPlanMgmtServiceURL;
	}

	@Value("#{configProp['ghixEnrollmentEntityServiceURL']}")
	public void setGhixEnrollmentEntityServiceURL(String ghixEnrollmentEntityServiceURL) {
		GHIX_EE_URL = ghixEnrollmentEntityServiceURL;
	}

	@Value("#{configProp['ghixPrescreenServiceURL']}")
	public void setGhixPrescreenServiceURL(String ghixPrescreenServiceURL) {
		PRESCREEN_SVC_URL = ghixPrescreenServiceURL;
	}

	@Value("#{configProp['ghixBrokerServiceURL']}")
	public void setGhixBrokerServiceURL(String ghixBrokerServiceURL) {
		GHIX_BROKER_URL = ghixBrokerServiceURL;
	}

	@Value("#{configProp['ghixSerffServiceURL']}")
	public void setGhixSerffServiceURL(String ghixSerffServiceURL) {
		GHIX_SERFF_SERVICE_URL = ghixSerffServiceURL;
		}

	@Value("#{configProp['ms_screenerURL']}")
	public void setGhixScreenerURL(String ghixScreenerURL) {
		MS_SCREENER_URL = ghixScreenerURL;
	}
	
	@Value("#{configProp['ms_screener.serviceURL']}")
	public void setGhixScreenerServiceURL(String ghixScreenerServiceURL) {
		MS_SCREENER_SERVICE_URL = ghixScreenerServiceURL;
	}

	@Value("#{configProp['ghixAffiliateServiceURL']}")
	public void setGhixAffiliateServiceURL(String ghixAffiliateServiceURL) {
		AFFILIATE_SERVICE_URL = ghixAffiliateServiceURL;
	}

	@Value("#{configProp['ghixFinanceServiceURL']}")
	public void setGhixFinanceServiceURL(String ghixFinanceServiceURL) {
		FINANCE_SERVICE_URL = ghixFinanceServiceURL;
	}

	@Value("#{configProp['ghixConsumerServiceURL']}")
	public void setGhixConsumerServiceURL(String ghixConsumerServiceURL) {
		CONSUMER_SERVICE_URL = ghixConsumerServiceURL;
	}

	@Value("#{configProp['ghixHubServiceURL']}")
	public void setGhixHubServiceURL(String ghixHubServiceURL) {
		HUB_SERVICE_URL = ghixHubServiceURL;
	}

	@Value("#{configProp['ghixFfmServiceURL']}")
	public void setGhixFfmServiceURL(String ghixFfmServiceURL) {
		FFM_SERVICE_URL = ghixFfmServiceURL;
	}

	@Value("#{configProp['ghixD2CServiceURL']}")
	public void setD2cServiceURL(String d2cServiceURL){
		D2C_SERVICE_URL = d2cServiceURL;
	}

	@Value("#{configProp['ghixD2CUHCServiceURL']}")
	public void setGhixD2CUHCServiceUrl(String ghixD2CUHCServiceUrl){
		D2C_UHC_URL = ghixD2CUHCServiceUrl;
	}

	@Value("#{configProp['ghixD2CHumanaServiceURL']}")
	public void setGhixD2CHuanaServiceUrl(String ghixD2CHumanaServiceUrl){
		D2C_HUMANA_URL = ghixD2CHumanaServiceUrl;
	}
	
	@Value("#{configProp['ghixEappServiceURL']}")
	public void setGhixEappServiceUrl(String ghixEappServiceUrl){
		GHIX_EAPP_SVC_URL = ghixEappServiceUrl;
	}

	@Value("#{configProp['ghixD2CAetnaServiceURL']}")
	public void setGhixD2CAetnaServiceUrl(String ghixD2CAetnaServiceUrl){
		D2C_AETNA_URL = ghixD2CAetnaServiceUrl;
	}
	
	@Value("#{configProp['ghixD2CUHCStmServiceUrl']}")
	public void setGhixD2CUHCStmServiceUrl(String ghixD2CUHCStmServiceUrl){
		D2C_UHC_STM_URL = ghixD2CUHCStmServiceUrl;
	}
	
	@Value("#{configProp['ghixD2CAssurantStmServiceUrl']}")
	public void setGhixD2CAssurantStmServiceUrl(String ghixD2CAssurantStmServiceUrl){
		D2C_ASSURANT_STM_URL = ghixD2CAssurantStmServiceUrl;
	}

	@Value("#{configProp['ghixD2CHCSCServiceURL']}")
	public void setGhixD2CHCSCServiceUrl(String ghixD2CHCSCServiceUrl){
		D2C_HCSC_URL = ghixD2CHCSCServiceUrl;
	}
	
	@Value("#{configProp['ghixD2CANTHEMServiceURL']}")
	public void setGhixD2CANTHEMServiceUrl(String ghixD2CANTHEMServiceUrl){
		D2C_ANTHEM_URL = ghixD2CANTHEMServiceUrl;
	}

	@Value("#{configProp['ghixFeedsConfigServiceURL']}")
	public void setGhixFeedConfigUrlServiceUrl(String ghixFeedConfigServiceUrl){
		FEEDS_URL = ghixFeedConfigServiceUrl;
	}


	@Value("#{configProp['ghixSsapSvcURL']}")
	public void setGhixSsapSvcURL(String ghixSsapSvcURL) {
		GHIX_SSAP_SVC_URL = ghixSsapSvcURL;
	}

	@Value("#{configProp['ghixEligibilitySvcURL']}")
	public void setGHIX_ELIGIBILITY_SVC_URL(String gHIX_ELIGIBILITY_SVC_URL) {
		GHIX_ELIGIBILITY_SVC_URL = gHIX_ELIGIBILITY_SVC_URL;
	}

	@Value("#{configProp['ghixHubIntegrationURL']}")
	public void setGhixHubIntegrationURL(String ghixHubIntegrationURL) {
		GHIX_HUB_INTEGRATION_URL = ghixHubIntegrationURL;
	}


	@Value("#{configProp['ghixIdentitySvcUrl']}")
	public void setGhixIdentitySvcURL(String ghixIdentitySvcURL) {
		IDENTITY_SVC_URL = ghixIdentitySvcURL;
	}


	@Value("#{configProp['ghixCoveredCAProxyServiceURL']}")
	public void setGhixCoveredCAProxyServiceURL(String ghixCoveredCAProxyServiceURL) {
		COVERED_CA_PROXY_URL = ghixCoveredCAProxyServiceURL;
	}
	

	@Value("#{configProp['msStarrServiceURL']}")
	public void setMsStarrSvcURL(String msStarrSvcURL) {
		MS_STARR_SVC_URL = msStarrSvcURL;
	}
	
	@Value("#{configProp['ghixExternalAssisterServiceURL']}")
	public void externalAssisterSvcURL(String externalAssisterSvcURL) {
		EXTERNAL_ASSISTER_URL = externalAssisterSvcURL;
	}
	
	@Value("#{configProp['endPointEligibilityDetermination']}")
	public void eligibilityEngineSvcURL(String endPointEligibilityDetermination) {
		GHIX_ELIGIBILITY_ENGINE = endPointEligibilityDetermination;
	}
	
	@Value("#{configProp['ghixHixServiceURL']}")
	public void setGhixHixServiceUrl(String ghixHixServiceURL) {
		GHIXHIX_SERVICE_URL = ghixHixServiceURL;
	}

//	public static class CapFeedEndpoints {
//		public static final String FEEDS_CONFIG_FINDALL_URL 	=  FEEDS_URL + "/config"+ "/findAll";
//		public static final String FEEDS_FINDALLSUMMARY_URL 	=  FEEDS_URL + "/findCarrierSummaries";
//		public static final String FEEDS_SHOWCARRIERDETAILS_URL	=  FEEDS_URL+	"/carrierDetail";
//		public static final String FEEDS_ORIGINALSTATUSFEEDS_URL =  FEEDS_URL+"/feedDetail/{feedDetailsId}/originalFeedInfo";
//		public static final String FEEDS_STATUSFEEDEXCEPTIONS_URL =FEEDS_URL+"/carrierDetail/exception";
//		public static final String FEEDS_COMMISIONFEEDDETIALS_URL =FEEDS_URL+"/commissionDetail";
//		public static final String FEEDS_COMMISIONFEEDEXCEPTIONS_URL= FEEDS_URL+"/commissionException";
//
//	}




	public static class CapFeedEndpoints {
		
		//Getting all carrier names 
		
		public static final String GET_CARRIER_NAMES_URL	= FEEDS_URL + "carrierName";
		public static final String GET_FEED_UPLOAD_USERNAMES_URL	= FEEDS_URL + "getFeedUploadUserNames";
		
		//CarrierConfiguration
		public static final String FEEDS_GET_CARRIER_CONFIG_URL 	= FEEDS_URL + "config"+ "/findAll";
		public static final String FEEDS_ADD_UPDATE_CONFIG_FILE_URL 	=  FEEDS_URL + "config"+  "/upsert";
		public static final String FEEDS_UPDATE_CONFIG_FILE_URLL 	=  FEEDS_URL + "config"+  "/upsert";
		public static final String FEEDS_DOWNLOAD_CONFIG_FILE_URL=FEEDS_URL +"config"+"/download";
		public static final String FEEDS_GET_CARRIER_NAMES_URL=FEEDS_URL +"config"+"/getCarrierNames";
		
		
		//CarrierSummary
		public static final String FEEDS_GET_CARRIER_FEED_SUMMARY_URL 	=  FEEDS_URL + "carrierFeedSummary";
		public static final String GET_CARRIER_FEED_SUMMARY_BY_ID 	=  FEEDS_URL + "getCarrierFeedSummaryById";
		public static final String FEEDS_UPLOAD_CARRIER_FEED_URL =FEEDS_URL +"process/carrierFeedSummary/manualUpload";
		//public static final String FEEDS_UPLOAD_CARRIER_FEED_URL 	=  FEEDS_URL +"carrierFeedSummary/manualUpload";
		public static final String FEEDS_PROCESSALL_CARRIER_FEEDS_URL = FEEDS_URL+"process/carrierFeedSummary/processAll";
		
		//StatusFeedDetails
		public static final String FEEDS_GET_STATUS_FEED_DETAILS_URL=FEEDS_URL+"carrierDetail";
		//public static final String FEEDS_GET_STATUS_FEED_EXCEPTIONS_URL=FEEDS_URL+"carrierDetail/exception";
		public static final String FEEDS_GET_STATUS_FEED_EXCEPTIONS_URL=FEEDS_URL+"policyException";
		public static final String FEEDS_GET_STATUS_FEED_MATCH_ENROLLMENT=FEEDS_URL+"getPossibleMatches";
		public static final String FEEDS_GET_STATUS_FEED_SAVE_MATCHED_ENROLLMENT=FEEDS_URL+"process/carrierFeedSummary/processSingle";
		public static final String FEEDS_STATUS_EXCEPTIONS_MARK_AS_RESOLVED=FEEDS_URL+"markAsResolved";
		
		public static final String FEEDS_SAVE_AS_DUPLICATE_ENROLLMENT=FEEDS_URL+"markAsDuplicate";
		public static final String FEEDS_SAVE_AS_DUPLICATE_ENROLLMENT_POLICY_URL=FEEDS_URL+"markAsDuplicatePolicy";
		
		// Enrollment Renewals
		public static final String FEEDS_GET_ENROLLMENT_RENEWALS_URL =FEEDS_URL+"enrollmentRenewals/getEnrollmentRenewals/";
		public static final String FEEDS_CREATE_UPDATE_ENROLLMENT_RENEWALS_URL =FEEDS_URL+"enrollmentRenewals/createUpdateEnrollmentRenewals";
		
		//CommissionFeedDetails 
		
		public static final String FEEDS_GET_COMMISSION_FEED_DETAILS_URL=FEEDS_URL+"commissionDetail";
		public static final String FEEDS_COMMISSION_ENROLLMENT_UPLOAD_FILE_URL=FEEDS_URL+"";
		public static final String FEEDS_PROCESS_COMMISSION_BACKLOG_URL=FEEDS_URL+"commission/processCommissionBacklog";
		
		
		
		//AgentCommission 
		
		public static final String FEEDS_GET_AGENT_COMMISSION_URL=FEEDS_URL+"";
		
		public static final String FEEDS_GENERATE_COMMISSION_PATTERNS_URL=FEEDS_URL+"";
		
		//TODO-after services are created for the following
		public static final String FEEDS_GET_COMMISSION_FEED_EXCEPTIONS_NOTES_URL=FEEDS_URL+"";
		public static final String FEEDS_SAVE_COMMISSION_FEED_NOTES_URL=FEEDS_URL+"";
		public static final String FEEDS_SAVE_COMMISSION_FEED_EXCEPTIONS_NOTES_URL=FEEDS_URL+"";
	}









	/**
	 * This inner class captures all REST ENDPOINTS exposed by PLAN-DISPALY module
	 *
	 */
	public static class PlandisplayEndpoints {
		//New End Points start
		public static final String FIND_HOUSEHOLD_BY_SHOPPING_ID =  PLANDISPLAY_URL + "plandisplay/findHouseholdByShoppingId";
		public static final String FIND_PESON_BY_HOUSEHOLD_ID =  PLANDISPLAY_URL + "plandisplay/findPersonByHouseholdId";
		public static final String UPDATE_PERSON_SUBSIDY =  PLANDISPLAY_URL + "plandisplay/updatePersonSubsidy";
		public static final String SAVE_PREFERENCES_BY_HOUSEHOLD_ID =  PLANDISPLAY_URL + "plandisplay/savePreferencesByHouseholdId";
		public static final String GET_PLANS = PLANDISPLAY_URL + "plandisplay/getPlans";
		public static final String FIND_CART_ITEMS_BY_HOUSEHOLD_ID = PLANDISPLAY_URL + "plandisplay/findCartItemsByHouseholdId";
		public static final String GET_PLAN_DETAILS = PLANDISPLAY_URL + "plandisplay/getPlanDetails";
		public static final String SAVE_CART_ITEM_URL 			=  PLANDISPLAY_URL + "plandisplay/saveCartItem";
		public static final String DELETE_CARTITEM_URL 			=  PLANDISPLAY_URL + "plandisplay/deleteCartItem";
		public static final String FIND_ORDERITEM_IDS_BY_HOUSEHOLDID  =  PLANDISPLAY_URL + "plandisplay/findOrderItemIdsByHouseholdId";
		public static final String FIND_PLAN_AVAILABILITY =  PLANDISPLAY_URL + "plandisplay/planAvailability";
		public static final String GET_NON_ELIGIBLE_MEMBER_LIST = PLANDISPLAY_URL + "plandisplay/getNonEligibleMemberList";
		public static final String UPDATE_PERSON_TOBACCO =  PLANDISPLAY_URL + "plandisplay/updatePersonTobacco";
		public static final String SET_COVERAGE_DATE =  PLANDISPLAY_URL + "plandisplay/setCoverageDate";
		public static final String GET_HOUSEHOLD_CONTACT =  PLANDISPLAY_URL + "plandisplay/getHouseholdContact";
		public static final String GET_FFM_MEMBER_RESPONSE_LIST = PLANDISPLAY_URL + "plandisplay/getFFMMemberResponseList";
		public static final String UPDATE_ORDER_ITEM_AS_ENROLLED = PLANDISPLAY_URL + "plandisplay/updateOrderItemAsEnrolled";		
		
		//Old End Points Start
		
		public static final String CALCULATE_TAX_CREDIT_URL 	=  PLANDISPLAY_URL + "plandisplay/calculateTaxCredit";
		public static final String FIND_PLD_HOUSEHOLD_BYID_URL 	=  PLANDISPLAY_URL + "plandisplay/findPldHouseholdById";
		public static final String FIND_PLD_HOUSEHOLD_BY_CMR_HOSUSEHOLD_URL 	=  PLANDISPLAY_URL + "plandisplay/findCmrHouseholdId";
		public static final String FIND_PLD_ORDERITEM_BYID_URL 	=  PLANDISPLAY_URL + "plandisplay/findPldOrderItemById";
		public static final String SAVE_HOUSEHOLD_URL 			=  PLANDISPLAY_URL + "plandisplay/saveHousehold";
		public static final String SAVE_PLDGROUP_URL 			=  PLANDISPLAY_URL + "plandisplay/savePldGroup";
		public static final String FIND_PDLGROUP_BYID_URL 		=  PLANDISPLAY_URL + "plandisplay/findPldGroupById";
		public static final String FIND_BYPERSONID_ANDGROUPID_URL 			=  PLANDISPLAY_URL + "plandisplay/findByPersonIdAndGroupId";
		public static final String FIND_BYGROUPID_URL 			=  PLANDISPLAY_URL + "plandisplay/findByGroupId";
		public static final String DELETE_INDIVIDUAL_PERSON_GROUP_URL 		=  PLANDISPLAY_URL + "plandisplay/deleteIndividualPersonGroup";
		public static final String GET_CARTITEM_BYPLAN_URL 		=  PLANDISPLAY_URL + "plandisplay/getCartItemByPlan";
		public static final String DELETE_ORDERITEM_URL 		=  PLANDISPLAY_URL + "plandisplay/deleteOrderItem";
		public static final String GET_CARTITEMS_URL 			=  PLANDISPLAY_URL + "plandisplay/getCartItems";
		public static final String GET_INDIVIDUALPLANS_URL 		=  PLANDISPLAY_URL + "plandisplay/getplans";
		public static final String FIND_PROVIDER_BY_HOUSHOLDID 	=  PLANDISPLAY_URL + "plandisplay/findProviderByHousholdId";
		public static final String FIND_PROVIDER_BY_LEADID 	=  PLANDISPLAY_URL + "plandisplay/findProviderByLeadId";
		public static final String GET_PLAN_INFO_URL 			=  PLANDISPLAY_URL + "plandisplay/getplanInfo";
		public static final String POST_SPLITHOUSEHOLD_URL 		=  PLANDISPLAY_URL + "plandisplay/postSplitHousehold";
		public static final String SHOW_PREFERENCES_URL 		=  PLANDISPLAY_URL + "plandisplay/showPreferences";
		public static final String SHOW_CUSTOMGROUPS_URL 		=  PLANDISPLAY_URL + "plandisplay/showCustomgroups";
		public static final String POST_CUSTOMGROUPS_URL 		=  PLANDISPLAY_URL + "plandisplay/postCustomgroups";
		public static final String CREATE_ANDUPDATE_GROUP_URL	=  PLANDISPLAY_URL + "plandisplay/createAndUpdateGroup";
		public static final String FIND_GROUPS_BY_HOUSEHOLD	=  PLANDISPLAY_URL + "plandisplay/findGroupsByHousehold";
		public static final String SAVE_PREFERENCES_URL 		=  PLANDISPLAY_URL + "plandisplay/savePreferences";
		public static final String SHOWING_CART_URL 			=  PLANDISPLAY_URL + "plandisplay/showingCart";
		public static final String POPULATE_PDLHOUSEHOLD_URL 	=  PLANDISPLAY_URL + "plandisplay/populatepldhousehold";
		public static final String SAVE_CART_ITEMS_URL 			=  PLANDISPLAY_URL + "plandisplay/saveCartItems";
		public static final String FIND_PLDHOUSEHOLD_BYSHOPPINGID_URL 		=  PLANDISPLAY_URL + "plandisplay/findPldHouseholdByShoppingId";
		public static final String GET_HOUSEHOLD_DATA_URL 		=  PLANDISPLAY_URL + "plandisplay/getHouseholdData";
		public static final String FIND_PLDHOUSEHOLDPERSON_BYID_URL 		=  PLANDISPLAY_URL + "plandisplay/findpldHouseholdpersonbyid";
		public static final String INDIVIDUAL_INFORMATION 		= PLANDISPLAY_URL + "plandisplay/saveIndividualInformation";
		public static final String SAVE_APTC_URL 				= PLANDISPLAY_URL + "plandisplay/saveAptc";
		public static final String SAVE_ORDER_ITEM_URL 			= PLANDISPLAY_URL + "plandisplay/saveOrderItem";
		public static final String FORM_GROUP_LIST 		=  PLANDISPLAY_URL + "plandisplay/formGroupList";
		public static final String POST_FIND_ORDER_BY_HOUSEHOLDID 	= PLANDISPLAY_URL + "plandisplay/findOrderByHouseholdId"; //required by shop
		public static final String RESTORE_APTC_VALUE_URL 		=  PLANDISPLAY_URL + "plandisplay/restoreAptcValue";
		public static final String GET_PLAN_DATA                =  PLANDISPLAY_URL + "plandisplay/getPlanData";
		public static final String UPDATE_CONTRIBUTION_PER_PERSON 			=  PLANDISPLAY_URL + "plandisplay/updateContributionPerPerson";
		public static final String UPDATE_HOUSEHOLD_SUBSIDY 			=  PLANDISPLAY_URL + "plandisplay/updateHouseholdSubsidy";
		public static final String UPDATE_SUBSIDY_EXISTING_DENTAL 			=  PLANDISPLAY_URL + "plandisplay/updateSubsidyExistingDental";
		public static final String FIND_CART_ITEM_BY_ID 			=  PLANDISPLAY_URL + "plandisplay/findCartItemById";
		public static final String FIND_PERSHOPPING_CART_ITEMS 			=  PLANDISPLAY_URL + "plandisplay/findPreshoppingCartItems";
		public static final String RE_CALCULATE_SUBSIDY 			=  PLANDISPLAY_URL + "plandisplay/reCalculateSubsidy";
		public static final String DRUG_PROPERTIES_BY_RXCUI 			=  PLANDISPLAY_URL + "api/drugs/properties";
		
		//Added by Enrollment Team
		public static final String FIND_BY_ORDERID 			    = PLANDISPLAY_URL + "plandisplay/findbyorderid/{id}";
		public static final String FIND_BY_HOUSEHOLD_ID 			    = PLANDISPLAY_URL + "plandisplay/findbyhouseholdid";
		public static final String EXTRACT_PERSON_DATA_URL =  PLANDISPLAY_URL + "plandisplay/extractPersonData";
		public static final String SEND_ADMIN_UPDATE_URL 		=  PLANDISPLAY_URL + "plandisplay/sendAdminUpdate";

		public static final String SPECIAL_ENROLLMENT_KEEP_PLAN_URL 	=  PLANDISPLAY_URL + "plandisplay/keepPlan";
		public static final String SPECIAL_ENROLLMENT_PERSONDATA_UPDATE 	=  PLANDISPLAY_URL + "plandisplay/specialEnrollmentPersonDataUpdate";
		public static final String SPECIAL_ENROLLMENT_CHECK_ITEMINCART 	=  PLANDISPLAY_URL + "plandisplay/checkItemInCart";
		public static final String GET_PLDHOUSEHOLD_URL 		=  PLANDISPLAY_URL + "plandisplay/getHousehold";
		public static final String SAVE_INDIVIDUAL_PHIX 	=  PLANDISPLAY_URL + "plandisplay/saveIndividualPHIX";

		public static final String OUR_LOGGEDIN_PAGE = "/account/signup/consumer";
		public static final String GET_PREFERENCES_URL 		=  PLANDISPLAY_URL + "plandisplay/getPreferences";
		public static final String GET_SUBSCRIBER_DATA 		=  PLANDISPLAY_URL + "plandisplay/getSubscriberData";
		public static final String UPDATE_ORDER_STATUS 		=  PLANDISPLAY_URL + "plandisplay/updateOrderStatus";

		public static final String SAVE_HOUSEHOLD_APPLICANT_ELIGIBILITY 	=  PLANDISPLAY_URL + "plandisplay/saveHouseholdForApplicantEligibility";
		public static final String FIND_ORDERID_BY_LEADID = PLANDISPLAY_URL + "plandisplay/findOrderidByLeadid/";
		public static final String FIND_ORDERID_BY_SSAP_APPLICATION_ID = PLANDISPLAY_URL + "plandisplay/findOrderIdBySsapApplicationId/";
		public static final String FIND_LATEST_PLD_HOUSEHOLD_BY_LEADID = PLANDISPLAY_URL + "plandisplay/findLatestPldHouseholdByLeadId";
		public static final String GET_SAML_RESPONSE = PLANDISPLAY_URL + "saml/returnSAMLOutput";
		public static final String GET_SAML_RESPONSE_ATTRIBUTES = PLANDISPLAY_URL + "saml/returnSAMLResponseAttributes";
		public static final String EXTRACT_FFM_HOUSEHOLD_BLOB 	=  PLANDISPLAY_URL + "plandisplay/extractFFMPldHouseholdBlobData";
		public static final String EXTRACT_FFM_PERSON_BLOB 	=  PLANDISPLAY_URL + "plandisplay/extractFFMPldPersonBlobData";
		public static final String SAVE_FFM_ASSIGNED_CONSUMER_ID = PLANDISPLAY_URL + "plandisplay/saveFFMAssignedConsumerId";
		public static final String SAVE_APPLICANT_ELIGIBILITY_URL = PLANDISPLAY_URL + "plandisplay/saveApplicantEligibilityDetails";

		public static final String FIND_FAV_PLAN_BY_ELIG_LEAD_ID = PLANDISPLAY_URL + "plandisplay/findPlanIdByLeadId";
		public static final String FIND_PLAN_BY_APPLICATION_ID = PLANDISPLAY_URL + "plandisplay/findPlanIdByApplicationId";
		public static final String EXTRACT_FFM_PERSON_DATA_URL =  PLANDISPLAY_URL + "plandisplay/extractFFMPersonData";
		public static final String FIND_PLAN_BY_HOUSEHOLDID = PLANDISPLAY_URL + "plandisplay/findPlanIdByHouseholdId";

		public static final String GET_MEMBER_NAMES_FROM_HOUSEHOLD_BLOB = PLANDISPLAY_URL + "plandisplay/getMemberNamesFromHouseholdBlob";
		public static final String UPDATE_PLDPERSON_FOR_TOBACCO_USAGE = PLANDISPLAY_URL + "plandisplay/updatePldPersonForTobaccoUsage";

		public static final String LOGIN_PAGE = "/account/user/login";
		public static final String FIND_PLD_GROUP_BYID_URL 	=  PLANDISPLAY_URL + "plandisplay/findPldGroupById";
		public static final String FIND_OREDERID_BY_HOUSEHOLDID_CARTSTATUS 	=  PLANDISPLAY_URL + "plandisplay/getOrderIdByHIdandStatus";
		public static final String FIND_PLAN_BENEFIT_AND_COST_DATA =  PLANDISPLAY_URL + "plandisplay/getPlanBenefitCostData";
		public static final String GET_ORDER_ID_FOR_AUTO_RENEWAL =  PLANDISPLAY_URL + "plandisplay/findPlanAvailable";
		public static final String UPDATE_SSAP_APPLICATION_ID_FOR_ELIGLEAD_ID =  PLANDISPLAY_URL + "plandisplay/updateSsapApplicationIdForEligLeadId";
		public static final String FIND_PLD_HOUSEHOLD_BY_SSAP_APPLICATION_ID 	=  PLANDISPLAY_URL + "plandisplay/findPldHouseholdBySsapApplicationId";
		public static final String FIND_SSAP_APPLICATION_ID_BY_ORDER_ITEM_ID 	=  PLANDISPLAY_URL + "plandisplay/findSsapApplicationIdByOrderItemId";
		public static final String FIND_PLD_ORDER_ITEM_BY_SSAP_APPLICATION_ID 	=  PLANDISPLAY_URL + "plandisplay/findPldOrderItemBySsapApplicationId";
		public static final String REMOVE_SSAP_APPLICATION_ID_FROM_ORDER_ITEM 	=  PLANDISPLAY_URL + "plandisplay/removeSsapApplicationIdFromOrderItem";
		public static final String FIND_BY_SSAP_APPLICATION_ID 	=  PLANDISPLAY_URL + "plandisplay/findBySsapApplicationId";
		public static final String FIND_PLD_ORDERITEM_BY_GROUP_ID = PLANDISPLAY_URL + "plandisplay/findPldOrderItemByPldGroupId";
		public static final String FIND_PROVIDER_BY_SSAPAPPLICATIONID 	=  PLANDISPLAY_URL + "plandisplay/findProviderBySsapApplicationId";
		public static final String FIND_PLD_HOUSEHOLD_BY_SSAPID_AND_REQESTTYPE 	=  PLANDISPLAY_URL + "plandisplay/findHouseholdBySsapIdAndReqType";
		public static final String FIND_PLD_GROUP_ID_BY_SSAPID 	=  PLANDISPLAY_URL + "plandisplay/findPldGroupIdBySsapApplicationId";
		
		public static final String FIND_RATING_AREA = PLANDISPLAY_URL + "plandisplay/findRatingArea";
		public static final String UPDATE_PLAN_BENEFITS_AND_COST_IN_SOLR = PLANDISPLAY_URL + "plandisplay/updatePlanBenefitsAndCostInSOLR/";
		public static final String EXTRACT_FFM_HOUSEHOLD_DATA_BY_ID 	=  PLANDISPLAY_URL + "plandisplay/extractFFMHouseholdDataById";
		
		public static final String SAVE_PRE_SHOPPING_DATA 	=  PLANDISPLAY_URL + "plandisplay/savePreShoppingData";
		public static final String CREATE_ORDERITEM_FOR_CMS_PLAN_ID 	=  PLANDISPLAY_URL + "plandisplay/createOrderItemForCmsPlanId";
		public static final String UPDATE_APPLICATIONID_IN_ORDERITEM 	=  PLANDISPLAY_URL + "plandisplay/updateApplicationIdInOrderItem";
		public static final String GET_LOWEST_DENTAL_PLAN_URL = PLANDISPLAY_URL + "plandisplay/getlowestdentalplan";
		public static final String GET_MAX_BENEFIT_AME_PLAN_URL = PLANDISPLAY_URL + "plandisplay/getmaxbenefitameplan";
		public static final String GET_LOWEST_VISION_PLAN_URL = PLANDISPLAY_URL + "plandisplay/getlowestvisionplan";		
		public static final String UPDATE_SEEK_COVERAGE_DENTAL 	=  PLANDISPLAY_URL + "plandisplay/updateSeekCoverageDental";
		public static final String EXTRACT_PERSONID_BY_SSAP_ID 			    = PLANDISPLAY_URL + "plandisplay/extractPersonDataByItemId/{ssapId}";
		public static final String GET_CROSSWALK_ISSUER_PLANID 			=  PLANDISPLAY_URL + "plandisplay/getCrossWalkIssuerPlanId";
		public static final String  SEARCH_DRUG_BY_NAME = PLANDISPLAY_URL + "api/drugs/search";
		public static final String  SEARCH_DRUG_BY_ID = PLANDISPLAY_URL + "api/drugs";
		public static final String  GET_FAIR_PRICE_OF_DRUG = PLANDISPLAY_URL + "api/drugs/fairPrice";
		public static final String  GET_PROVIDERS_SEARCH_RESULTS = PLANDISPLAY_URL + "plandisplay/getProvidersSearchResults";
		public static final String GET_RX_CUI_BY_NDC =  PLANDISPLAY_URL + "api/drugs/rxcui/";
		public static final String APPEND_HIOSPLANID_WITH_CSR= PLANDISPLAY_URL + "plandisplay/appendHiosPlanIdWithCSR";
		public static final String  GET_PLAN_SCORE_FOR_PREFERENCESE= PLANDISPLAY_URL + "plandisplay/getPlanScoreForPreferences";
		public static final String  GET_GENERIC_RXCUI_OF_DRUG = PLANDISPLAY_URL + "api/drugs/genericRxcui";
		public static final String  RESTORE_CSR_VALUE= PLANDISPLAY_URL + "plandisplay/restoreCSRValue";
		public static final String  CALCULATE_SLIDER_APTC = PLANDISPLAY_URL + "plandisplay/calculateSliderAptc";
		public static final String  GET_PD_PREFERENCES_BY_LEADID = PLANDISPLAY_URL + "api/getPdPreferencesByLeadId/{eligLeadId}";
		public static final String GET_DRUG_DATA_BY_RXCUI_SEARCH_LIST = PLANDISPLAY_URL + "api/drugs/byRxcuiList";
		
		public static final String SET_LEAD_ID_IN_PDHOUSEHOLD = PLANDISPLAY_URL + "plandisplay/setLeadId";
		public static final String FIND_PLDHOUSEHOLD_BY_LEADID_AND_YEAR 	=  PLANDISPLAY_URL + "plandisplay/findPdHouseholdByLeadIdAndYear";
	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by GHIX-AHBX module
	 *
	 */

	public static class AHBXEndPoints{
		public static final String PLATFORM_VALIDATE_ADDRESS_OEDQ = GHIX_AHBX_URL + "platform/validateaddress/oedq";
		public static final String PLATFORM_ADOBELIVECYCLE = GHIX_AHBX_URL + "platform/createnotice/adobelivecycle";
		public static final String PLATFORM_UCM_CREATECONTENT = GHIX_AHBX_URL + "platform/ecm/createcontent";
		public static final String PLATFORM_UCM_GET_FILE_CONTENT = GHIX_AHBX_URL + "platform/ecm/getfilecontent";

		//Shop urls
		public static final String GET_EMPLOYER_DETAILS  = GHIX_AHBX_URL + "employer/getEmployer";
		public static final String GET_CASE_ORDER_ID     = GHIX_AHBX_URL + "employer/sendOrderId";

		//Delegation urls
		public static String DELEGATIONCODE_RESPONSE = GHIX_AHBX_URL + "delegation/validation/delegationcode";

		//Enrollment Urls
		public static final String ENROLLMENT_IND21_CALL_URL = GHIX_AHBX_URL + "enrollment/sendCarrierUpdatedData";
		public static final String ENROLLMENT_IND20_CALL_URL = GHIX_AHBX_URL + "enrollment/individualplanselection";
		public static final String ENROLLMENT_IND70_CALL_URL= GHIX_AHBX_URL + "enrollment/sendAdminUpdateConfirmation";
		//public static final String ENROLLMENT_IND53_CALL_URL = GHIX_AHBX_URL + "enrollment/verifypin";

		//Agent/Broker Rest URLS (ghix-web to ghix-ahbx)
		public static String SEND_ENTITY_DESIGNATION_DETAIL = GHIX_AHBX_URL + "entity/senddesignationdetail";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_STATUS = GHIX_AHBX_URL + "broker/retrieveindividualBOBStatus";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_DETAIL= GHIX_AHBX_URL + "broker/retrieveindividualbobdetails";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_LIST= GHIX_AHBX_URL + "broker/retrieveindividualbobdetailslist";
		public static String GET_EXTERNAL_EMPLOYER_BOB_DETAIL = GHIX_AHBX_URL + "broker/retrieveemployerbobdetails";
		public static String GET_EXTERNAL_EMPLOYER_BOB_STATUS = GHIX_AHBX_URL + "broker/retrieveemployersbobstatus";
		public static String GET_EXTERNAL_INDIVIDUAL_HOUSEHOLD_ELIGIBILITY_DETAIL = GHIX_AHBX_URL + "broker/retrieveindividualhouseholdeligibilitydetails";

		// sync plan and issuer data url
		public static final String SYNC_PLAN_DATA = GHIX_AHBX_URL + "plan/syncWithAHBX";
		public static final String SYNC_ISSUER_DATA = GHIX_AHBX_URL + "issuer/syncWithAHBX";

		//Agent/Assister/Enrollment Entity Rest URLs (ghix-web to ghix-ahbx and ghix-entity to ghix-ahbx)
		public static String SEND_ENTITY_DETAIL = GHIX_AHBX_URL + "entity/sendentitydetail";
	}

	//Enrollment Entity Rest URLS (ghix-web to ghix-entity)
	public static class EnrollmentEntityEndPoints {
		public static final String GET_ALL_SITE_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getAllSites";
		public static final String GET_SITE_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getsitedetail";
		public static final String SAVE_SITE_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/savesitedetail";
	    public static final String GET_SITE_DETAILS_BY_ID=GHIX_EE_URL + "entity/enrollmentEntity/getsitedetailbyId";
	    public static final String GET_SITE_BY_TYPE=GHIX_EE_URL + "entity/enrollmentEntity/getsitebytype";
	    public static final String GET_ENROLLMENTENTITY_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitydetail";
		public static final String SAVE_ENROLLMENTENTITY_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententitydetail";
		public static final String GET_PAYMENT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getpaymentdetails";
		public static final String GET_PAYMENT_DETAILS_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getpaymentdetailsbyid";
		public static final String SAVE_PAYMENT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/savepaymentdetails";
		public static final String SAVE_ENROLLMENTENTITY_STATUS = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententityStatus";
		public static final String GET_ENTITY_ENROLLMENT_DETAILS_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getentitydetailsbyuserid";
		public static final String GET_ENTITY_ENROLLMENT_HISTORY_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententityhistory";
		public static final String GET_POPULATION_SERVED_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getpopulationserveddetail";
		public static final String SAVE_POPULATION_SERVED_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/savepopulationserveddetail";
		public static final String GET_ENRIOLLMENT_ENTITY_INFORMATION = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitybyid";
		public static final String GET_LIST_OF_ENROLLMENT_ENTITY_SITES = GHIX_EE_URL + "entity/enrollmentEntity/getEnrollmentEntitySites";
		public static final String GET_LIST_OF_ENROLLMENT_ENTITY_SITES_BY_SITE_TYPE = GHIX_EE_URL + "entity/enrollmentEntity/getEnrollmentEntitySitesbysitetype";
		public static final String SAVE_ASSISTER_DETAILS = GHIX_EE_URL + "entity/assister/saveassisterdetails";
		public static final String GET_ASSISTER_DETAIL = GHIX_EE_URL + "entity/assister/geteassisterdetailbyid";
		public static final String GET_LIST_OF_ASSISTERS_BY_EE = GHIX_EE_URL + "entity/assister/getlistofassistersbyenrollmententity";
		public static final String POPULATE_NAVIGATION_MENU_MAP = GHIX_EE_URL + "entity/enrollmentEntity/populatenavigationmenumap";
	    public static final String GET_SITE_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getsitebyid";
	    public static final String GET_ENROLLMENT_ENTITY_CONTACT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitycontactdetail";
	    public static final String SAVE_ENROLLMENT_ENTITY_CONTACT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententitycontactdetail";
	    public static final String GET_ENROLLMENTENTITY_DETAILS_BY_ID = GHIX_EE_URL + "entity/entityadmin/getenrollmententitydetailsbyid";
		public static final String GET_ENTITY_DOCUMENTS_DETAILS_BY_ID = GHIX_EE_URL + "entity/entityadmin/getentitydocumentsdetailsbyid";
		public static final String GET_DOCUMENTS_FROM_ECM_BY_ID = GHIX_EE_URL + "entity/entityadmin/getdocumentsfromecmbyid";
		public static final String SAVE_DOCUMENTS_ON_ECM = GHIX_EE_URL + "entity/entityadmin/savedocumentsonecm";
		public static final String SAVE_DOCUMENTS = GHIX_EE_URL + "entity/entityadmin/savedocuments";
		public static final String GET_ENTITY_ENROLLMENT_HISTORY_FOR_ADMIN_BY_ID = GHIX_EE_URL + "entity/entityadmin/getenrollmententityhistoryforadmin";
		public static final String GET_ENROLLMENTENTITY_MAP = GHIX_EE_URL + "entity/entityadmin/getenrollmententitymap";
		public static final String GET_ASSISTER_DETAIN_BY_USER_ID = GHIX_EE_URL + "entity/assister/getassisterdetailbyuserid";
		public static final String GET_ASSISTER_LIST= GHIX_EE_URL +"entity/assisteradmin/getassisterlist";
		public static final String SAVE_ASSISTER_STATUS= GHIX_EE_URL +"entity/assisteradmin/saveassisterstatus";
		public static final String UPDATE_ASSISTER_DETAIL = GHIX_EE_URL +"entity/assisteradmin/updateassisterdetail";
		public static final String GET_ASSISTER_ADMIN_DETAILS_BY_ID= GHIX_EE_URL +"entity/assisteradmin/getassisteradmindetailsbyid";
		public static final String GET_ASSISTER_LIST_FOR_EXCHANGEADMIN= GHIX_EE_URL +"/entity/entityadmin/getassisterlist";
		public static final String GET_ASSISTER_DETAIL_FOR_EXCHANGEADMIN = GHIX_EE_URL + "entity/entityadmin/getassisterdetailsbyid";
		public static final String SAVE_ASSISTER_STATUS_FOR_EXCHANGEADMIN=GHIX_EE_URL+"entity/entityadmin/saveassisterstatus";
		public static final String UPDATE_ASSISTER_INFORMATION = GHIX_EE_URL +"entity/assister/updateassisterinformation";
		public static final String GET_SEARCH_ASSISTER_LIST_FOR_EXCHANGEADMIN=GHIX_EE_URL +"entity/assisteradmin/getsearchassisterlist";
		public static final String GET_ASSISTER_ADMIN_DOCUMENTS_DETAILS_BY_ID=GHIX_EE_URL+"entity/assisteradmin/getassisteradmindocbyid";
		public static final String GET_ASSISTER_DOCUMENTS_FROM_ECM_BY_ID=GHIX_EE_URL+"entity/assisteradmin/getassisterdocfromecmbyid";
		public static final String GET_ASSISTER_DETAILS_BY_ID=GHIX_EE_URL+"entity/assisteradmin/getassisteradminbyid";
		public static final String SAVE_ASSISTER_DOCUMENTS_ON_ECM=GHIX_EE_URL+"entity/assisteradmin/saveassisterdoconecm";
		public static final String SAVE_ASSISTER_DOCUMENTS=GHIX_EE_URL+"entity/assisteradmin/saveassisterdocuments";
		public static final String GET_SEARCH_LIST_OF_ASSISTERS_BY_EE = GHIX_EE_URL +"entity/entityadmin/getsearchassisterlist";
		public static final String ASSISTER_POPULATE_NAVIGATION_MENU_MAP = GHIX_EE_URL + "entity/assister/populatenavigationmenumap";
		public static final String GET_ASSISTER_ACTIVITY_DETAILS_BY_ID= GHIX_EE_URL +"entity/assisteradmin/getassisterhistorydetailsbyid";
		public static final String SAVE_ENROLLMENTENTITY_WITH_DOCUMENT = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententitywithdocument";
		public static final String GET_ENROLLMENT_DOCUMENT_HISTORY = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitydocumenthistory";
		public static final String DELETE_DOCUMENT =  GHIX_EE_URL + "entity/enrollmentEntity/deletedocument";
		public static final String SEARCH_ENTITIES = GHIX_EE_URL + "entity/assister/searchentities";
		public static final String SEARCH_ASSISTERS = GHIX_EE_URL + "entity/assister/searchassisters";
		public static final String GET_ENROLLMENTENTITY_BY_ID = GHIX_EE_URL + "entity/assister/enrollmententitydetailbyid";
		public static final String GET_ASSISTERS_BY_ENROLLMENTENTITY = GHIX_EE_URL + "entity/assister/getassistersbyenrollmententityid";
		public static final String GET_NO_OF_ASSISTERS_BY_ENROLLMENTENTITY = GHIX_EE_URL + "entity/assister/noofassistersbyenrollmententityandsite";
		public static final String GET_ASSISTER_DETAIL_BY_ID = GHIX_EE_URL + "entity/assister/assisterdetailbyid";
		public static final String DESIGNATE_ASSISTER = GHIX_EE_URL + "entity/assister/designateassister";
		public static final String GET_ASSISTER_DESIGNATION_DETAIL_BY_INDIVIDUAL_ID = GHIX_EE_URL + "entity/assister/getdesignateassisterbyindividualid";
		public static final String GET_INDIVIDUAL_LIST_FOR_ENROLLMENTENTITY = GHIX_EE_URL + "entity/enrollmentEntity/getindividuallistforenrollmententity";
		public static final String GET_ASSISTERS_BY_ENROLLMENTENTITY_AND_SITE_ID = GHIX_EE_URL + "entity/assister/getassistersbyentityandsite";
		public static final String GET_INDIVIDUAL_INFO_BY_ID =GHIX_EE_URL + "entity/assister/getexternalindividualbyid";
		public static final String GET_INDIVIDUAL_LIST_FOR_ASSISTER = GHIX_EE_URL + "entity/assister/getindividuallistforassister";
		public static final String GET_ASSISTER_DESIGNATION_DETAIL_FOR_REQUEST = GHIX_EE_URL + "entity/assister/designateassisterforrequest";
		public static final String SAVE_ASSISTER_DESIGNATION = GHIX_EE_URL + "entity/assister/savedesignateassister";
		public static final String GET_ASSISTER_BY_USER_ID = GHIX_EE_URL + "entity/assister/getassisterbyuserid";
		public static final String GET_ASSISTER_RECORD_BY_USER_ID = GHIX_EE_URL + "entity/assister/getassisterRecordbyuserid";
		public static final String DELETE_PAYMENT_METHOD =  GHIX_EE_URL + "entity/enrollmentEntity/deletepaymentmethod";
		public static final String UPDATE_RECEIVED_PAYMENT =  GHIX_EE_URL + "entity/enrollmentEntity/updateReceivedPayment";
		public static final String GET_LIST_OF_CEC =  GHIX_EE_URL + "entity/enrollmentEntity/getListOfCEC";
		public static final String REASSIGN_INDIVIDUALS =  GHIX_EE_URL + "entity/enrollmentEntity/reassignindividuals";
		public static final String GET_INDIVIDUAL_DESIGNATION =  GHIX_EE_URL + "entity/assister/findIndividualDesignation";
		public static final String GET_ASSISTER_FOR_EMAIL =  GHIX_EE_URL + "entity/assister/checkAssisterForEmail";
		public static final String GET_ASSISTER_EXPORT_LIST =  GHIX_EE_URL + "entity/assisteradmin/getassisterexportlist";
	}

	public static class EnrollmentEndPoints {
	    public static final String LOGOUT_PAGE = "/account/user/logout";
		public static final String FIND_ACTIVE_ENROLLMENTS_FOR_EMPLOYER = ENROLLMENT_URL + "enrollment/findActiveEnrollmentsForEmployer";
		public static final String FIND_ENROLLMENT_PLANLEVEL_COUNT_BY_BROKER = ENROLLMENT_URL + "enrollment/findEnrollmentPlanLevelCountByBroker";
		public static final String CREATE_INDIVIDUAL_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/createIndividualEnrollment";
		public static final String GET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL= ENROLLMENT_URL + "enrollment/getIndividualPlanSelectionDetails";
		public static final String FIND_ENROLLMENTS_BY_ISSUER_URL= ENROLLMENT_URL + "enrollment/findenrollmentsbyissuer";
		public static final String FIND_TAX_FILING_DATE_FOR_YEAR_URL= ENROLLMENT_URL + "enrollment/findtaxfilingdateforyear";
		public static final String FIND_ENROLLMENTS_BY_PLAN_URL= ENROLLMENT_URL + "enrollment/findenrollmentsbyplan";
		public static final String SEARCH_ENROLLEE_URL= ENROLLMENT_URL + "enrollee/searchEnrollee";
		public static final String FIND_ENROLLEE_BY_ID_URL= ENROLLMENT_URL + "enrollee/findbyid/";
		public static final String EMPLOYER_DIS_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/employerdisenrollment";
        public static final String FIND_ECOMMITTED_ENROLLMENT_BY_TICKET_URL= ENROLLMENT_URL + "offexchange/getecommittedenrollment";
		public static final String UPDATE_ECOMMITTED_ENROLLMENT_URL= ENROLLMENT_URL + "offexchange/updateecommittedenrollment";
		public static final String SAVE_ECOMMITTED_ENROLLMENT_URL= ENROLLMENT_URL + "offexchange/saveecommittedenrollment";
		public static final String SAVE_MANUAL_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/private/saveManualEnrollment";
		public static final String DELETE_MANUAL_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/private/deleteManualEnrollment";
		public static final String GET_MANUAL_ENROLLMENT_BY_CMR_HOUSEHOLD_URL= ENROLLMENT_URL + "enrollment/private/getManualEnrollmentByCmrHouseHoldId";
		public static final String UPDATE_ENROLLMENT_WITH_FFM_DATA_URL= ENROLLMENT_URL + "enrollment/private/updateenrollmentwithffmdata";
		public static final String RESEND_ENROLLMENT_TO_FFM_URL= ENROLLMENT_URL + "enrollment/private/resendenrollmenttoffm";
		public static final String FIND_ENROLLMENT_SSAP_APPLICATION_ID= ENROLLMENT_URL + "offexchange/getenrollmentbycmrhouseholdid";
		public static final String FIND_CONSUMER_DETAIL_BY_CMR_HOUSEHOLD_ID = ENROLLMENT_URL + "enrollment/private/getenrollmentdetailsforconsumerportal/";
		public static final String GET_ENROLLMENT_BY_SSAP_APPLICATION_ID = ENROLLMENT_URL + "offexchange/findenrollmentsbyssapid/";
		public static final String ABORT_ENROLLMENT_BY_SSAP_APPLICATION_ID = ENROLLMENT_URL + "offexchange/abortenrollmentbyssapid";
		public static final String CREATE_AUTO_RENEWAL_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/createautorenewal";
		public static final String SET_RESEND_834_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/setResend834";
		public static final String TERMINATE_ENROLLMEN_BY_CMS_PLANID_URL= ENROLLMENT_URL + "enrollment/terminateenrollmentbyplanid";
		public static final String COUNT_ENROLLMEN_BY_CMS_PLANID_URL= ENROLLMENT_URL + "enrollment/countenrollmentbyplanid";

		//PHIX Conflicting Endpoints resolution
		public static final String CREATE_INDIVIDUAL_ENROLLMENT_URL_PRIVATE= ENROLLMENT_URL + "enrollment/private/createIndividualEnrollment";
		public static final String FIND_ENROLLMENTS_BY_CMR_HOUSEHOLD_ID_PRIVATE = ENROLLMENT_URL + "enrollment/private/findEnrollmentsByCmrHouseholdId/";

		public static final String CANCEL_PENDING_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/cancelpendingenrollment";
		public static final String GET_ISSUER_BY_INSURER_TAX_ID_URL= ENROLLMENT_URL + "enrollment/getissuerbyinsurertaxidnumber";
		public static final String ENROLLMENT_XML_URL= ENROLLMENT_URL + "enrollment/enrollmentxml";
		public static final String SEND_CARRIER_UPDATED_ENROLLEE_TO_AHBX_RESPONSE_URL= ENROLLMENT_URL + "enrollee/saveCarrierUpdatedDataResponse";
		public static final String ADMIN_UPDATE_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/adminupdateenrollment";
		public static final String DISENROLL_BY_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/disenrollbyenrollmentid";
		public static final String UPDATE_SHOP_ENROLLMENT_STATUS_URL= ENROLLMENT_URL + "enrollment/updateshopenrollmentstatus";
		public static final String GET_PLANID_AND_ORDERITEMID_BY_ENROLLMENT_ID_URL= ENROLLMENT_URL + "enrollment/getplanidandorderitemidbyenrollmentid";
		public static final String FIND_ENROLLMENTS_BY_CMR_HOUSEHOLD_ID = ENROLLMENT_URL + "enrollment/findEnrollmentsByCmrHouseholdId/";
		public static final String FIND_ENROLLMENT_BY_SSAP_APPLICATION_ID = ENROLLMENT_URL + "enrollment/private/findEnrollmentBySsapApplicationId/";
		public static final String DISENROLL_BY_EMPLOYEE_URL= ENROLLMENT_URL + "enrollment/disenrollbyemployeeID";
		public static final String GROUP_INSTALLATION_URL = ENROLLMENT_URL + "enrollment/createGroupInstallation";
		public static final String DISENROLL_BY_APPLICATION_ID_URL= ENROLLMENT_URL + "enrollment/disenrollbyapplicationID";
		public static final String FIND_ENROLLMENT_BY_ADMIN = ENROLLMENT_URL + "enrollment/private/findenrollmentbyadmin";
		public static final String FIND_ENROLLMENT_COMMISSION = ENROLLMENT_URL + "enrollment/private/findenrollmentcommission";
		public static final String SAVE_ENROLLMENT_COMMISSION = ENROLLMENT_URL + "enrollment/private/saveenrollmentcommission";
		public static final String UPDATE_ENROLLMENT_COMMISSION = ENROLLMENT_URL + "enrollment/private/updateenrollmentcommission";
		public static final String GET_ENROLLMENTLIST_BY_HOUSEHOLD = ENROLLMENT_URL + "enrollment/private/getEnrollmentList";
		public static final String UPDATE_ENROLLMENT_CARRIER_APP_ID_URL = ENROLLMENT_URL + "enrollment/private/updateEnrollmentCarrierAppId/";
		public static final String GET_ENROLLMENT_CAP_INFO = ENROLLMENT_URL + "enrollment/private/getEnrollmentCapInfo/";
		public static final String UPDATE_ENROLLMENT_CMR_HOUSEHOLD_ID = ENROLLMENT_URL + "enrollment/private/updateEnrollmentCmrHouseholdId";
		public static final String FIND_BY_ENROLLMENT_ID = ENROLLMENT_URL + "enrollment/private/findbyenrollmentid/";
		public static final String UPDATE_MAILING_ADDRESS_BY_HOUSEHOLD= ENROLLMENT_URL + "enrollment/enrollmetmailingddressupdate";

		
		//Aetna enrollment application submission
		public static final String AETNA_ENROLLMENT_APPLICATION_SUBMISSION_URL = ENROLLMENT_URL + "aetna/webservice/aetna/applicationsubmission";

		//public static final String GET_CONSUMER_CALL_HISTORY        = CONSUMER_SERVICE_URL+"consumer/getConsumerCallLogHistory";
	    //public static final String SAVE_CONSUMER_CALL_LOG           = CONSUMER_SERVICE_URL+"consumer/saveConsumerCallLog";
	 	//public static final String SAVE_CONSUMER_COMMENT_LOG        = CONSUMER_SERVICE_URL+"consumer/saveConsumerCommentLog";
	 	//public static final String SAVE_CONSUMER_EVENT_LOG          = CONSUMER_SERVICE_URL+"consumer/saveConsumerEventLog";

		//Added by plandisplay team
		public static final String FIND_ENROLLMENT_BY_ID_URL= ENROLLMENT_URL + "enrollment/findbyid/";
		public static final String GET_ENROLLMENT_ATRIBUTE_BY_ID= ENROLLMENT_URL + "enrollment/getenrollmentatributebyid";
		//Added by Kuldeep for Finance Mgmt.
		public static final String FIND_ENROLLMENT_BY_EMPLOYER = ENROLLMENT_URL + "enrollment/findbyemployer";
				public static final String FIND_ENROLLMENT_BY_EMPLOYEE = ENROLLMENT_URL + "enrollment/findbyemployer";
		public static final String SEARCH_ENROLLMENT_BY_CURRENTMONTH = ENROLLMENT_URL + "enrollment/searchenrollmentbycurrentmonth";
		public static final String SEARCH_SUBSCRIBER_BY_ENROLLMENT_ID = ENROLLMENT_URL + "enrollee/findsubscriberbyenrollmentid/";
		public static final String FIND_ENROLLEE_BY_EMPLOYEE = ENROLLMENT_URL + "enrollee/findenrolleebyemployeeapplicationid";
		public static final String FIND_ENROLLEE_BY_APPLICATION_ID = ENROLLMENT_URL + "enrollee/findenrolleebyapplicationid";
		public static final String FIND_ENROLLEE_BY_APPLICATION_ID_JSON = ENROLLMENT_URL + "enrollee/findenrolleebyapplicationidjson";
		public static final String GET_ENROLLEE_COVERAGE_DETAILS_BEFORE_LAST_INVOICE = ENROLLMENT_URL + "enrollee/getEnrolleeCoverageDetailsBeforeLastInvoice";
		
		//Added to verify enrollment exists or not.
		public static final String IS_ENROLLMENT_EXISTS = ENROLLMENT_URL + "enrollment/isenrollmentexists/";
		//Added to fetch Remittance data
		public static final String  GET_REMITTANCEDATABY_ENROLLMENT_ID = ENROLLMENT_URL + "enrollment/getremittancedatabyenrollmentid/";
		//D2c related EndPoints
		public static final String  CREATE_D2C_ENROLLMENT_WITH_ENROLLEES = ENROLLMENT_URL + "enrollment/private/createD2CEnrollmentWithEnrollees";
		public static final String  UPDATE_D2C_ENROLLMENT_STATUS = ENROLLMENT_URL + "enrollment/private/updateD2CEnrollmentStatus";
		public static final String  UPDATE_MANUAL_ENRL_STATUS=ENROLLMENT_URL+"enrollment/private/updatemanualenrollmentstatus";
		public static final String FIND_ENROLLMENT_BY_CARRIER_FEEDDATA = ENROLLMENT_URL + "enrollment/private/findEnrollmentByCarrierFeedDetails";
		public static final String  UPDATE_ENRL_PERSONAL_INFO=ENROLLMENT_URL+"enrollment/private/updateEnrollmentPersonalInfo";
		public static final String  UPDATE_POICY_MAJOR_INFO=ENROLLMENT_URL+"enrollment/private/updateEnrollmentMajorInfo";
		public static final String  UPDATE_POLICY_VERIFICATION_EVENT=ENROLLMENT_URL+"enrollment/private/updateEnrollmentVerificationEvent";

		public static final String  GET_ENROLLEE_COVERAGE_START_DATA_BY_ENROLLMENT_ID = ENROLLMENT_URL + "enrollment/getenrolleecoveragestartdatabyenrollmentid/";
		public static final String  FIND_EMPLOYERS_BY_ENROLLMENT_STARTDATE_AND_ENDDATE_URL= ENROLLMENT_URL + "enrollmentreport/findemployersbyenrollmentstartandenddate";
		public static final String  GET_SHOP_ENROLLMENT_LOG_DETAILS = ENROLLMENT_URL + "enrollmentreport/getShopEnrollmentLogDetails";
		//Reinstatement URL
		public static final String  ENROLLMENT_REINSTATEMENT_URL=ENROLLMENT_URL+"enrollment/reinstateenrollment";
	    public static final String  FIND_EMPLOYEEID_AND_PLANID_BY_EMPLYERID=ENROLLMENT_URL+"enrollment/findEmployeeIdAndPlanIdByEmployerid";
	    public static final String  FIND_ENROLLMENTID_BY_ENROLLMENTID_AND_ENPLOYEEID=ENROLLMENT_URL+"enrollment/findEnrollmentByEmployeeIdAndEnrollmentId";
	    public static final String  FIND_COVERAGE_END_DATE_BY_ENPLOYEE_APP_ID=ENROLLMENT_URL+"enrollment/findCoverageEndDateByEmployeeAppID";
	    //API to check if Issuer has active enrollment

	    public static final String  IS_ISSUER_HAVING_ACTIVE_ENROLLMENT=ENROLLMENT_URL+"enrollment/activeIssuer";
		public static final String  UPDATE_ENROLLMENT_APTC = ENROLLMENT_URL + "enrollment/updateEnrollmentAptc";
		public static final String  IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID = ENROLLMENT_URL + "enrollment/isActiveEnrollmentForApplicationId";
		
		/* CAP: Application  view/edit functionality mappings - Darshan Hardas */
	    public static final String FIND_BY_SSAPAPPLICATONID = ENROLLMENT_URL + "enrollment/private/findBySsapApplicationId/";
	    public static final String SAVE_FFMAPPLICATION_DATA = ENROLLMENT_URL + "enrollment/private/saveFfmAppData/";
	    public static final String SAVE_APPLICATION_ENROLLEE = ENROLLMENT_URL + "enrollment/private/saveEnrollees/";
	    public static final String GET_MANUALENROLLMENT_EXISTS = ENROLLMENT_URL + "enrollment/private/manualEnrollmentExists/";
	    public static final String SAVEENROLLMENTDETAILS = ENROLLMENT_URL + "enrollment/private/saveEnrollmentDetails";
	    /* CAP: Application  view/edit functionality mappings ENDS */
	    public static final String SEND_ENROLLMENT_CONFIRMATION_EMAIL = ENROLLMENT_URL + "offexchange/sendconfirmationemail";
	    /*Enrollment FFM Proxy Endpoint*/
	    public static final String CREATE_FFM_PROXY_ENROLLMENT = ENROLLMENT_URL + "enrlproxy/createFfmProxyEnrollment";
	    
	    /*Enrollment Broker Update Endpoint */
	    public static final String AGENT_BOB_TRANSFER=ENROLLMENT_URL+"enrollment/agentbobtransfer";
	    public static final String UPDATE_ENROLLMENT_BROKER_DETAILS=ENROLLMENT_URL+"enrollment/updateEnrollmentBrokerDetails";
	    public static final String FIND_ENROLLMENT_STATUS_BY_EMPLOYEE_ID= ENROLLMENT_URL+ "enrollment/findEnrollmentStatusByEmployeeId";
	    public static final String GET_ENROLLMENT_DATA_FOR_ASSISTER_BROKER= ENROLLMENT_URL+ "enrollment/getEnrollmentDataForAssisterBroker";
	    
	    
	    public static final String AUTOMATE_SPECIAL_ENROLLMENT_URL= ENROLLMENT_URL+ "enrollment/automateSpecialEnrollment";
	    
	    /* JiraID: HIX-70358  Fetch Enrollment Data from List HouseHoldCaseIds */
	    public static final String GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS = ENROLLMENT_URL + "enrollment/getenrollmentdatabyhouseholdcaseids";
	    
	    /* Jira ID: HIX-72322 Create a new Enrollment Events API for CAP Enrollment History functionality */
	    public static final String GET_ENROLLMENT_EVENT_HISTORY_DATA = ENROLLMENT_URL + "enrollmentreport/getEnrollmentEventHistoryData";
	    
	    /* Jira ID: HIX-72324 Enrollment API for Renewal */
	    public static final String GET_ENROLLMENT_DATA_BY_SSAPAPPLICATIONID = ENROLLMENT_URL + "enrollment/getenrollmentdatabyssapapplicationid";
	    
	    /* Jira ID: HIX-77598  */
	    public static final String GET_ENROLLMENT_DATA_FOR_BROKER_BY_LIST_OF_HOUSEHOLDCASEIDS = ENROLLMENT_URL + "enrollment/getenrollmentdataforbrokerbyhouseholdcaseids";
	    
	    /* Jira ID: HIX-82873 */
	    public static final String ENROLLMENT_UPDATE = ENROLLMENT_URL + "enrollment/updateEnrollment";
	    /* Jira ID: HIX-103358 */
	    public static final String ENROLLMENT_CANCEL = ENROLLMENT_URL + "enrollment/cancelenrollment";
	    /* Jira ID: HIX-103329 */
	    public static final String ENROLLMENT_UPDATE_ADD_TXN = ENROLLMENT_URL + "enrollment/updateenrollmentaddtxn";
	    
	    /* Jira ID: HIX-79659 */
	    public static final String  UPDATE_ENROLLMENT_FOR_BOB_SUCCESS = ENROLLMENT_URL + "enrollment/anonymous/updateenrollmentforbobsuccess";
	    
	    /* Jira ID: HIX-79658 */
	    public static final String GET_ENROLLMENT_BY_BOB_REQUEST = ENROLLMENT_URL + "enrollment/anonymous/getenrollmentbybobrequest";
	    
	    /* Jira Id: HIX-81138 */
	    public static final String GET_ENROLLMENT_SUBSCRIBER_DETAILS = ENROLLMENT_URL + "enrollment/anonymous/getenrollmentandsubscriberdetails";
	    /*Jira Id: HIX-79708*/
	    public static final String FIND_ENROLLMENT_BY_POLICYNUMBER = ENROLLMENT_URL + "enrollment/anonymous/findEnrollmentByPolicyNumber";
	    /*HIX-79398 : API to convert Pending Enrollment to Confirmed*/
	    public static final String UPDATE_ENROLLMENT_STATUS = ENROLLMENT_URL+"enrollment/updateEnrollmentStatus";
	    
	    /*HIX-79701 : API to get BOB Expected Record*/
	    public static final String FIND_BOB_EXPECTED_ENROLLMENT_IDS = ENROLLMENT_URL + "enrollment/anonymous/findbobexpectedenrollmentids";
	    
	    /*HIX-79710 : API for Enrollment Commission*/
	    public static final String SAVE_ENROLLMENT_COMMISSION_ANONYMOUS = ENROLLMENT_URL + "enrollment/anonymous/saveenrollmentcommission";
	    
	    /* HIX-79704*/
	    public static final String ENROLLMENT_RENEWAL = ENROLLMENT_URL + "enrollment/anonymous/enrollmentrenewal";
	    
	    /* HIX-84548 */
	    public static final String UPDATE_PREFERENCES_ENROLLMENT = ENROLLMENT_URL + "enrollment/updateMailByHouseHoldId";
	    
	    /* HIX-85421 */
	    public static final String VALIDATE_ENROLLMENT_COVERAGE_OVERLAP = ENROLLMENT_URL + "enrollment/validateEnrollmentCoverageOverlap";
		
	    /* HIX-87984 */
	    public static final String GET_MONTHLY_APTC_AMOUNT = ENROLLMENT_URL + "enrollment/getmonthlyaptcamount";
	    
	    public static final String CREATE_AUTO_RENEWAL_JSON = ENROLLMENT_URL + "enrollment/createautorenewaljson";
	    public static final String GET_PLANID_ORDERITEM_ID_BY_ENROLLMENT_ID_JSON = ENROLLMENT_URL + "enrollment/getplanidandorderitemidbyenrollmentidjson";
	    public static final String GET_ENROLLMENT_BY_ID_OR_HHCASEIDS = ENROLLMENT_URL + "enrollment/getenrollmentbyidandhousholdcaseids";
	    
	    public static final String GET_POPULATED_CARRIER_AND_MONTH_LIST = ENROLLMENT_URL + "enrollmentrecon/getpopulatedcarrierandmonthlist";
	    public static final String GET_MONTHLY_RECON_ACTIVITY_DATA =  ENROLLMENT_URL + "enrollmentrecon/getmonthlyreconactivitydata";
	    public static final String SEARCH_ENROLLMENT_DISCREPANCY =  ENROLLMENT_URL + "enrollmentrecon/searchenrollmentdiscrepancy";
	    public static final String GET_SEARCH_DISCREPANCY_DROPDOWN_DATA = ENROLLMENT_URL + "enrollmentrecon/getsearchdiscrepancydropdowndata";
	    public static final String GET_ENRL_RECON_DISCREPANCY_DETAIL = ENROLLMENT_URL + "enrollmentrecon/getenrlrecondiscrepancydetail";
	    public static final String GET_ENRL_RECON_COMMENT_DETAIL = ENROLLMENT_URL + "enrollmentrecon/getenrlreconcommentdetail";
	    public static final String ADDEDIT_COMMENT_OR_SNOOZE_DISCREPANCY = ENROLLMENT_URL + "enrollmentrecon/addeditcommentorsnoozediscrepancy";
	    public static final String GET_VISUAL_SUMMARY_DETAIL = ENROLLMENT_URL + "enrollmentrecon/getvisualsummarydetail";
	    public static final String FETCH_ENROLLMENT_SUBSCRIBER_DATA = ENROLLMENT_URL + "enrollee/fetchenrollmentsubscriberdata";
	    
	    public static final String GET_ENROLLMENT_DATA_BY_MEMBERID_LIST = ENROLLMENT_URL + "enrollment/getenrollmentdatabymemberidlist";
	    public static final String PROCESS_CUSTOM_GROUP_ENROLLMENTS = ENROLLMENT_URL + "enrollment/processcustomgroupenrollments";
	    public static final String GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID = ENROLLMENT_URL + "enrollment/getenrollmentcountbyssapid";
	    public static final String ENROLLMENT_CUSTOM_GROUPING = ENROLLMENT_URL + "enrollment/processcustomgroupenrollments";
	    public static final String GET_ENROLLMENT_SUBSCRIBER_EVENTS = ENROLLMENT_URL + "enrollment/getAllSubscriberEvents";
	    public static final String GET_ENROLLMENT_SNAPSHOT = ENROLLMENT_URL + "enrollment/getenrollmentsnapshot";
	    
	    /* JiraID: HIX-109786  Fetch Enrollment Data from List external case IDs */
	    public static final String GET_ENROLLMENT_DATA_BY_LIST_OF_EXTERNAL_CASE_ID = ENROLLMENT_URL + "enrollment/getenrollmentdatabyexternalcaseid";
	    
	    /* JiraID: HIX-110099 Create a new API to updating SSAP Application ID for preAT Enrollments */
	    public static final String UPDATE_ENROLLMENT_SSAP_APPLICATION_ID= ENROLLMENT_URL + "enrollment/updatessapapplicationid";
	    
	    /* JiraID: HIX-110239 Create a new API to fetch creation date of latest enrollment by ssap ID */
	    public static final String GET_LATEST_ENRL_CREATION_DATE_BY_SSAP_ID= ENROLLMENT_URL + "enrollment/getlatestenrollmentcreationtimebyssapid";
	    
	    /* JiraID: HIX-110394 Create a new API to fetch effective date of latest enrollment by ssap ID */
	    public static final String GET_LATEST_ENRL_EFFECTIVE_DATE_BY_SSAP_ID= ENROLLMENT_URL + "enrollment/getlatestenrollmenteffectivedatebyssapid";
	    
	    /* JiraID: HIX-110714 Create a new API to HIOS id for external member id */
	    public static final String GET_PLAN_HIOS_ID_FOR_EXTERNAL_MEMBER_ID= ENROLLMENT_URL + "enrollment/getplanhiosidforexternalmemberid";
	    
	    /* JiraID: HIX-112912 API to return if APP has active enrollments without lookback day logic*/
	    public static final String IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID_NO_LOOKBACK = ENROLLMENT_URL + "enrollment/isactiveenrollmentforapplicationidwithoutlookback";
	    
	    public static final String GET_ENROLLMENT_BY_MEMBER_ID_LIST = ENROLLMENT_URL + "enrollment/enrollmentsByMemberIds";
	    
	    public static final String GET_ENROLLEE_COVERAGE_START_DATA_BY_ENROLLMENT_ID_AND_CHANGE_DATE = ENROLLMENT_URL + "enrollment/getenrolleecoveragestartdatabyenrollmentidandchangedate";
	}

	public static class ShopEndPoints {
		public static final String GET_EMPLOYER_ENROLLMENT 				= SHOP_URL + "getEmployerEnrollment";
		public static final String GET_EMPLOYER_CONTRIBUTION_AMOUNT		= SHOP_URL + "getEmployerContributionAmt";
		public static final String GET_EMPLOYER_DETAILS     			= SHOP_URL + "employer/getEmployer";
		public static final String GET_EMPLOYER_PLANS       			= SHOP_URL + "employerQuoting/getEmployerQuotingData";
		public static final String GET_EMPLOYER_BENCHMARK_PLAN_RATE     = SHOP_URL + "employerQuoting/getEmployerHealthQuotingDataByPlan";
		public static final String GET_EMPLOYEE_PLANS       			= SHOP_URL + "getEmployeePlan";
		public static final String UPDATE_EMPLOYER_ENROLLMENT_STATUS	= SHOP_URL + "updateEmployerEnrollmentStatus";
		public static final String UPDATE_EMPLOYER_ELIGIBILITY_STATUS	= SHOP_URL + "employer/updateEmployerEligibiltyStatus";
		public static final String UPDATE_EMPLOYER_PARTICIPATION_RATE	= SHOP_URL + "updateEmployerParticipationRate";
		public static final String SEND_EMPLOYER_APPEAL_NOTIFICATION	= SHOP_URL + "employer/appeal/sendEmployerAppealNotification";
		public static final String TERMINATE_EMPLOYER_ENROLLMENT		= SHOP_URL + "/terminateEnrollment";
        public static final String GET_EMPLOYEE_AFFORDABILITY_STATUS = SHOP_URL + "getEmployeeAffordabilityStatus";
		public static final String GET_EMPLOYER_REFERENCE_PLANS			= SHOP_URL + "employer/getEmployerReferencePlans";
		public static final String GET_EMPLOYEE_MEMBER_DETAILS     		= SHOP_URL + "employee/getEmployeeDetails";
		public static final String SEND_INDIVIDUAL_INFORMATION 			= SHOP_URL + "employee/sendIndividualInformation";
		public static final String SET_EMPLOYEE_ENROLLED 				= SHOP_URL + "employee/setEmployeeEnrolled";
		public static final String GET_EMPLOYEE_DETAILS     			= SHOP_URL + "employee/getEmployee";
		public static final String DELETE_MEMBER_DETAILS     			= SHOP_URL + "employee/deleteMember";
		public static final String DEACTIVATE_EMPLOYEE     				= SHOP_URL + "employee/deactivateEmployee";
		public static final String GET_EMPLOYER_PLAN_PREMIUMS			= SHOP_URL + "getEmployerPlanPremiums";
		public static final String GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYEE_APP_ID = SHOP_URL + "employeeapp/getEmployerEnrollmentByEmployeeAppId";
		public static final String GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYERID = SHOP_URL + "getEmployerEnrollmentByEmployerId";
		public static final String GET_EMPLOYEE_PLAN_COUNT = SHOP_URL + "employeeQuoting/getEmployeePlanCount";
		public static final String SEND_EMPLOYEE_COVERAGE_TERMINATE_NOTICE	= SHOP_URL + "/sendEmployeeCoverageTerminateNotice";
		public static final String GET_EMPLOYEES_DETAILS     			= SHOP_URL + "employee/getEmployees";
		public static final String CHECK_PLAN_TIER_AVAILABILITY = SHOP_URL + "employerQuoting/checkPlanTierAvailability";
		public static final String GET_ELIGIBLE_EMPLOYEE_DETAILS     			= SHOP_URL + "employee/getEligibleEmployees";
		public static final String GET_EMPLOYER_INFO_IRS     			= SHOP_URL + "employer/getEmployerInfo";
	}

	public static class EligibilityEndPoints {
		public static final String FPL_CALCULATOR_URL 		= SHOP_URL + "eligibility/fplCalculator";
		public static final String APPLICANTS_ELIGIBILITIES = ELIGIBILITY_URL + "eligibility/application/%d";
		public static final String REFERRAL_ACTIVATION = ELIGIBILITY_URL + "referral/activation/{id}";
		public static final String REFERRAL_ACTIVATION_SSAP = ELIGIBILITY_URL + "referral/activation/ssapapplication/{id}";
		public static final String REFERRAL_ACTIVATION_USER_SSAP = ELIGIBILITY_URL + "referral/activation/find/{user}/{ssapapplication}";
		public static final String REFERRAL_ACTIVATION_CMR_SSAP = ELIGIBILITY_URL + "referral/activation/findByCmr/{cmrHousehold}/{ssapapplication}";
		public static final String REFERRAL_ACTIVATION_LINK_CMR = ELIGIBILITY_URL + "referral/activation/linkcmr";
		public static final String VALIDATE_REFERRAL_ANSWERS = ELIGIBILITY_URL + "referral/activation/validate";
		public static final String UPDATE_ACTIVATION_STATUS = ELIGIBILITY_URL + "referral/activation/updateStatus";
		public static final String REFERRAL_COVERAGE_YEAR = ELIGIBILITY_URL + "referral/activation/coverageyear";
		public static final String REFERRAL_ACTIVATION_RIDP = ELIGIBILITY_URL + "referral/activation/processridp";
		public static final String LINK_SSAP_CMRHOUSEHOLD = ELIGIBILITY_URL + "referral/activation/linkssapcmr";
		public static final String REFERRAL_PENDING_CMR = ELIGIBILITY_URL + "referral/pending";
		public static final String LINKING_STATUS_SSAP_CMRHOUSEHOLD= ELIGIBILITY_URL + "referral/csr/linkingstatus";
		public static final String ENROLLED_APP_BY_CMRHOUSEHOLD= ELIGIBILITY_URL + "referral/enrolledapps/%d/%d";
		public static final String SEND_ADDITIONAL_VERIFICATION_DOC_NOTICE=ELIGIBILITY_URL + "referral/activation/noticeToUploadDoc";
		public static final String ELIGIBILITY_NOTIFICATION_URL = ELIGIBILITY_URL + "atresponse/notifications/";
		public static final String ELIGIBILITY_NOTIFICATION_UPDATED_URL=ELIGIBILITY_URL + "atresponse/notifications/updated/";
		public static final String UNIVERSAL_ELIGIBILITY_NOTIFICATION_URL = ELIGIBILITY_URL + "atresponse/notifications/universaleligibility/";
		public static final String ELIGIBILITY_ADD_DOC_REQD_NOTIFICATION_URL = ELIGIBILITY_URL + "ssapintegration/notice/addInfoDoc/";
		public static final String SEND_IRS_HOUSEHOLDINFO_URL = ELIGIBILITY_URL + "irsHouseholdInfoClientController/requestToIrsHouseholdInfo";
		public static final String CHECK_MEC_IN_STATE = ELIGIBILITY_URL + "checkmecinstate?case_number=";
		public static final String RERUN_ELIGIBILITY_URL = ELIGIBILITY_URL + "nonfinancial/eligibility/rerun";
		public static final String PASSIVE_ENROLLMENT_PROCESSOR_URL = ELIGIBILITY_URL + "passive/enrollment/process";
		public static final String FETCH_LCE_APPLICANTS=ELIGIBILITY_URL+"referral/sep/applicant/events";
		public static final String UPDATE_SSAP_SEP_EVENTS=ELIGIBILITY_URL+"referral/sep/update/events";
		public static final String DISPLAY_SEP_QEP_EVENTS=ELIGIBILITY_URL+"referral/sep/events";
		public static final String COUNT_SSAP_CMRHOUSEHOLD= ELIGIBILITY_URL + "referral/activation/activeappcount/{cmrHouseHoldId}/{ssapApplicationId}";
		public static final String BENCHMARK_PREMIUM_SERVICE= ELIGIBILITY_URL + "eligibility/indportal/getBenchMarkPremiumAmount";
		public static final String UPDATE_APTC_URL= ELIGIBILITY_URL + "eligibility/indportal/updateaptc";
		public static final String GET_SSAP_APPLICANT = ELIGIBILITY_URL + "referral/getssapapplicant/";
		public static final String GET_SSAP_APPLICANT_GUID = ELIGIBILITY_URL + "referral/getssapapplicantguid/";
		public static final String GET_SSAP_APPLICANT_FROM_HOME = ELIGIBILITY_URL + "referral/getssapapplicant/home/";
		public static final String UPDATE_REFERRAL_STATUS = ELIGIBILITY_URL + "referral/activation/updateReferralStatus";
		public static final String UPDATE_REFERRAL_COUNT = ELIGIBILITY_URL + "referral/activation/updateReferralCount";
		public static final String UPDATE_REFERRAL_SUCCESS_COUNT = ELIGIBILITY_URL + "referral/activation/updateReferralSuccessCount";		
		public static final String CREATE_NEW_HOUSEHOLD_REFERRAL = ELIGIBILITY_URL + "referral/create/household/{ssapApplicationId}";
		public static final String COMPARE_HOUSEHOLD_PRIMARY_REFERRAL = ELIGIBILITY_URL + "referral/compare/householdPrimary/{ssapApplicationId}/{householdId}";
		public static final String TRIGGER_REFERRAL_NOTICE = ELIGIBILITY_URL + "referral/activation/triggerReferralNotice/{ssapApplicationId}";
		public static final String BATCH_DISENROLL = ELIGIBILITY_URL + "eligibility/batch/disenroll";
		public static final String BATCH_AUTOMATE_CSLEVEL = ELIGIBILITY_URL + "referral/batch/automateCsLevel";
		public static final String BATCH_AUTOMATE_GEOLOCALE = ELIGIBILITY_URL + "referral/batch/automateGeolocale";
		public static final String UPDATE_SSAP_RENEWAL_STATUS = ELIGIBILITY_URL + "application/updateRenewalStatus";
		public static final String CANCEL_APP_AND_EVENTS = ELIGIBILITY_URL + "ssap/application/{case_number}/cancel";
		public static final String CLOSE_APP_AND_EVENTS = ELIGIBILITY_URL + "ssap/application/{case_number}/close";
		public static final String UPDATE_APPLICANT_EVENT = ELIGIBILITY_URL + "ssap/applicantevent/{applicant_event_id}";
		public static final String UPDATE_VALIDATION_STATUS = ELIGIBILITY_URL + "ssap/application/{case_number}/validate?source=";
		public static final String UPDATE_OVERRIDE_SEP_DETAILS = ELIGIBILITY_URL + "eligibility/indportal/saveOverrideSEPDetails";
		public static final String FIND_HH_SSN_DOB = ELIGIBILITY_URL + "referral/find/household/fromSsnDoB/";
		public static final String GET_SSAP_APPLICANTS_BY_SSAP_ID = ELIGIBILITY_URL + "eligibility/indportal/getApplicantsBySsapId";
		public static final String FETCH_SEP_EVENT_DETAILS = ELIGIBILITY_URL + "application/fetchSepEventsDetails";
		public static final String FETCH_LATEST_APTC_AMOUT = ELIGIBILITY_URL + "ssapaptcrecalc/fetchLatestAPTCAmount";
		public static final String FETCH_APTC_RATIO = ELIGIBILITY_URL + "eligibility/api/getAptcRatio";
		public static final String GET_SSAP_APPLICATION_STATUS = ELIGIBILITY_URL + "application/getApplicationStatusById";
		public static final String SET_NOT_TO_CONSIDER_RENEWAL_STATUS = ELIGIBILITY_URL + "renewal/setNotToConsiderRenewalStatus";
		public static final String FETCH_HOUSEHOLD_RENEWAL_STATUS = ELIGIBILITY_URL + "renewal/fetchHouseholdRenewalStatus";
		public static final String INITIATE_ACCOUNT_ACTIVATION_EMAIL = ELIGIBILITY_URL + "atresponse/initiateActivation";
		public static final String HOUSEHOLD_COMPOSITION = GHIX_ELIGIBILITY_ENGINE + "eligibilityEngine/householdComposition";
		public static final String APP_CHANGE_AUTOMATION = ELIGIBILITY_URL + "referral/appChangeAutomation";
		public static final String GENERATE_PUSH_AT= ELIGIBILITY_URL + "/ssapapplication/generateAndPushAT";
		public static final String IS_MEDICAID_CHIP_ELIGIBLE_MEMBER = ELIGIBILITY_URL + "/ssapapplication/hasAssessedMedicaidChipEligibleMember";
		public static final String SSAP_APP_STATUS_UPDATE = ELIGIBILITY_URL + "/application/update";
		public static final String SSAP_APP_CLEANUP_VERIFICATIONS = ELIGIBILITY_URL + "/application/cleanupverifications";
		public static final String SSAP_APPLICANT_APPLICABLE_VERIFICATIONS = ELIGIBILITY_URL + "/application/applicableVerifications";
		public static final String SSAP_APPLICANT_TO_HOUSEHOLD_MEMBER = ELIGIBILITY_URL + "/application/householdMember";
		public static final String SSAP_APPLICANT_VERIFIED = ELIGIBILITY_URL + "/application/applicant/verified";
	}

	public static class PhixEndPoints{
		public static final String ESTIMATOR_API = PRESCREEN_SVC_URL + "eligibility/estimator";
		public static final String LEAD_FOR_OUTBOUND_CALL = PRESCREEN_SVC_URL + "eligibility/leadForOutBoundCall";
		public static final String LEADS_CREATED_FOR_OUTBOUND_CALL = PRESCREEN_SVC_URL + "eligibility/getLeadsCreatedForOutBoundCalls";
		public static final String UPDATE_CONTACT_DETAILS_FOR_LEAD = PRESCREEN_SVC_URL + "eligibility/updateContactDetailsInEligLeadRecord";
		public static final String UPDATE_OTHER_INPUTS_FOR_LEAD = PRESCREEN_SVC_URL + "eligibility/updateOtherInputsInEligLeadRecord";
		public static final String GET_LEAD = PRESCREEN_SVC_URL + "eligibility/getEligLeadRecord";
		public static final String UPDATE_FLOW_FOR_LEAD = PRESCREEN_SVC_URL + "eligibility/updateFlowTypeInEligLeadRecord";
		public static final String SAVE_LEAD = PRESCREEN_SVC_URL + "eligibility/saveEligLeadRecord";
		public static final String UPDATE_APP_STATUS = PRESCREEN_SVC_URL + "eligibility/updateAppStatusInEligLeadRecord";
		public static final String UPDATE_LEAD = PRESCREEN_SVC_URL + "eligibility/updateEligLeadRecord";
		public static final String GET_STATE_CHIP_MEDICAID_RECORD_BY_STATE = PRESCREEN_SVC_URL + "eligibility/getStateChipMedicaidRecordByState";
		public static final String GET_LATEST_LEAD_FOR_PHONE_NUMBER = PRESCREEN_SVC_URL + "eligibility/getLatestEligLeadForPhoneNumber";
		public static final String CENSUS_API = PRESCREEN_SVC_URL + "lead/censusData";
		public static final String CENSUS_API_INDIVIDUAL = PRESCREEN_SVC_URL + "lead/individual/censusData";
		public static final String GET_ELIG_LEAD_BY_EMAIL_AFFID_FLOWID_FOR_INDIVIDUAL = PRESCREEN_SVC_URL + "eligibility/checkEligLeadByEmailAffIdAndFlowIdForIndividual";
		public static final String SAVE_LEAD_EXT = PRESCREEN_SVC_URL + "eligibility/saveEligLeadExtRecord";
	}

	public static class WebServiceEndPoints {
		public static final String GHIX_WEB_PLAN_MGMT_SERVICE = GHIXWEB_SERVICE_URL + "premiumRestService/houseHoldReq";
		public static final String GHIX_WEB_PLAN_MGMT_TRANSFER_PLAN_URL = GHIXWEB_SERVICE_URL + "ghixWebplanMgmtService/tranferPlanDTO";
		public static final String GHIX_WEB_PLAN_MGMT_PROCESS_CSR_URL = GHIXWEB_SERVICE_URL + "ghixWebplanMgmtService/processCSR";
		public static final String GHIX_WEB_PROVIDER_MANAGEMENT_CALHEERS_DATA_UPLOAD_URL = GHIXWEB_SERVICE_URL +"provider/network/filebaseupload";
		public static final String GHIX_WEB_CRM_CONSUMER_GET_ECOMMITMENT_DETAILS_URL = GHIXWEB_SERVICE_URL + "crm/consumer/getECommitmentDetails";
		public static final String GHIX_WEB_CRM_VIEW_CONSUMER_URL  = GHIXWEB_SERVICE_URL + "crm/consumer/viewconsumer";
		public static final String GHIX_WEB_RIDP_WSO2_URL  = GHIXWEB_SERVICE_URL + "ridp/sendRidpVerificationToWso2";
		public static final String GHIX_WEB_CAP_GET_FLOW_CONFIG = GHIXWEB_SERVICE_URL +"cap/flowrouter/getflowconfig";
		public static final String GHIX_WEB_EMX_VIEW_EMPLOYER = GHIXWEB_SERVICE_URL + "emx/employer/view/%d";
		public static final String GHIX_WEB_EMX_CREATE_EMPLOYER = GHIXWEB_SERVICE_URL + "emx/employer/create";
	}

	public static class PlanMgmtEndPoints {
		public static final String GET_EMPLOYER_REFERENCE_PLANS = PLAN_MGMT_URL + "plan/getrefplan";
		//public static final String CHECK_PLAN_AVAILABILITY = PLAN_MGMT_URL + "plan/checkavailability";
		public static final String CHECK_INDV_HEALTH_PLAN_AVAILABILITY = PLAN_MGMT_URL + "plan/checkindvhealthplanavailability";
		public static final String GET_PLAN_DATA_BY_ID_URL = PLAN_MGMT_URL + "plan/getinfo";
		public static final String GET_PLAN_DATA_BY_LIST_OF_IDS_URL = PLAN_MGMT_URL + "plan/getInfoByPlanIds";
		public static final String GET_PLAN_ISSUER_DATA_BY_ID_URL = PLAN_MGMT_URL + "plan/getPlanIssuerInfo";
		public static final String GET_PLANS_BY_ZIP_AND_ISSUERNAME = PLAN_MGMT_URL + "plan/getplansbyissuer";
		public static final String GET_BENCHMARK_PREMIUM_URL = PLAN_MGMT_URL + "premiumRestService/houseHoldReq";
		public static final String GET_EMPLOYER_PLAN_RATE = PLAN_MGMT_URL +  "planratebenefit/getemployerplanrate";
		public static final String GET_LOWEST_PREMIUM_URL = PLAN_MGMT_URL + "planratebenefit/findLowestPremiumPlanRate";
		public static final String MEMBER_LEVEL_EMPLOYEE_RATES = PLAN_MGMT_URL + "planratebenefit/memberlevelemployeerate";

		public static final String GET_INDIVIDUAL_PLAN_BENEFITS_AND_COST_URL = PLAN_MGMT_URL + "planratebenefit/getBenefitsAndCost";
		public static final String GET_INDIVIDUAL_PLAN_BENEFITS_AND_COST_BY_PLANID_URL = PLAN_MGMT_URL + "planratebenefit/getBenefitsAndCostByPlanId";
		
		public static final String GET_HOUSEHOLD_PLAN_RATE_URL = PLAN_MGMT_URL +  "planratebenefit/household";
		public static final String GET_BENCHMARK_PLAN_RATE_URL = PLAN_MGMT_URL +  "planratebenefit/rate/benchmark";
		public static final String PLAN_STATUS_UPDATE_URL = PLAN_MGMT_URL +  "planMgmtService/plan/status/update";
		public static final String GET_PLAN_DETAILS_URL = PLAN_MGMT_URL + "plan/getdetails";
		public static final String UPDATE_PLANSTATUS_URL =  PLAN_MGMT_URL + "plan/updatestatus";
  		public static final String GET_STM_PLAN_RATE_BENEFIT_URL = PLAN_MGMT_URL +  "stmplanratebenefit/stmplan";
		public static final String TEASER_PLAN_API_URL =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/secondPlan";
		public static final String TEASER_ELIGIBILITY_PLANS_API_URL =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/eligiblityRequest";

		public static final String GET_PLAN_BENEFIT_AND_COST_DATA = PLAN_MGMT_URL + "plan/getbenefitcostdata";
		public static final String GET_MEMBER_LEVEL_PLAN_RATE = PLAN_MGMT_URL +  "planratebenefit/memberlevelpremium";
		// SHOP-PM REST CALL
		public static final String GET_PLANAVAILABILITY_FOREMPLOYEES_URL = PLAN_MGMT_URL + "plan/checkPlanAvailabilityForEmployees";

		// PLAN EXISTENCE IN PROVIDER NETWORK
		public static final String GET_PLAN_PROVIDER_NETWORK_URL = PLAN_MGMT_URL + "plan/getprovidernetwork";

		//ISSUERD2C REST CALL
		public static final String GET_ISSUER_D2C_INFO_URL = PLAN_MGMT_URL + "issuerInfo/getIssuerD2CInfo";
		//GET PLANS DATA REST CALL
		public static final String GET_PLANS_DATA_URL = PLAN_MGMT_URL + "plan/getplansdata";

		public static final String DISCLAIMER_INFO = PLAN_MGMT_URL + "issuerInfo/disclaimerInfo";
		public static final String UPDATE_ENROLLMENTAVAILITY_JOB_URL = PLAN_MGMT_URL + "plan/updateEnrollmentAvaility";
		public static final String GET_EMPLOYER_MIN_MAX_RATE = PLAN_MGMT_URL + "planratebenefit/getemployerminmaxrate";
		public static final String GET_ISSUERS_BY_ZIPCODE = PLAN_MGMT_URL + "issuerInfo/byzipcode";
		public static final String GET_ISSUERS_BY_ID = PLAN_MGMT_URL + "issuerInfo/byId";
		public static final String GET_ISSUERS_D2C_YEARS = PLAN_MGMT_URL + "issuerInfo/getIssuerD2CInfo";
		// cross sell REST APIS
		public static final String CROSS_SELL_GET_LOWEST_DENTAL_PREMIUM = PLAN_MGMT_URL + "crosssell/getlowestdentalpremium";
		public static final String CROSS_SELL_GET_DENTAL_PLANS = PLAN_MGMT_URL + "crosssell/getdentalplans";
		public static final String CROSS_SELL_GET_VISION_PLANS = PLAN_MGMT_URL + "crosssell/getvisionplans";
		public static final String CROSS_SELL_GET_LOWEST_VISION_PREMIUM = PLAN_MGMT_URL + "crosssell/getlowestvisionpremium";
		//AME plan REST APIS
		public static final String GET_AME_PLANS = PLAN_MGMT_URL + "ameplan/getameplans";
		public static final String CROSS_SELL_GET_LOWEST_AME_PREMIUM = PLAN_MGMT_URL + "crosssell/getlowestamepremium";
		public static final String CROSS_SELL_GET_AME_PLANS = PLAN_MGMT_URL + "crosssell/getameplans";
		// HIX-57898 get AME plan with highest Max benefit
		public static final String GET_MAX_BENEFIT_AME_PLAN = PLAN_MGMT_URL + "ameplan/getmaxbenefitameplan";
				
		/*	Adding new REST call for HIX-48900	*/
		public static final String GET_PLAN_INFO_BY_ZIP = PLAN_MGMT_URL + "plan/getplaninfobyzip";
		
		public static final String GET_PLANS_FOR_UNIVERSAL_CART_PAGE = PLAN_MGMT_URL + "plan/getPlansForUniversalCart";
		/*	Adding new REST call for HIX-53019*/
		public static final String CHECK_METAL_TIER_AVAILABILITY = PLAN_MGMT_URL + "plan/checkMetalTierAvailability";
		/* HIX-56346 : 	Add new API paymentInfoByID */
		public static final String GET_ISSUER_PAYMENT_INFO_BY_HIOSID = PLAN_MGMT_URL + "issuerInfo/paymentInfoByID";
		// HIX-57897 get cheapest dental plan 
		public static final String GET_LOWEST_DENTAL_PLAN = PLAN_MGMT_URL + "planratebenefit/getlowestdentalplan";
		// HIX-58978 API to return Medicaid plans
		public static final String GET_MEDICAID_PLAN = PLAN_MGMT_URL + "medicaidplan/getmedicaidplans";
		// HIX-59893 API to return Vision plans
		public static final String GET_VISION_PLAN = PLAN_MGMT_URL + "visionplan/getvisionplans";
		// HIX-56452 API to return Medicare plans
		public static final String GET_MEDICARE_PLAN = PLAN_MGMT_URL + "medicare/getmedicareplans";	
		// HIX-62501 API to return lowest Vision plans
		public static final String GET_LOWEST_VISION_PLAN = PLAN_MGMT_URL + "visionplan/getlowestvisionplan";
		// HIX-69548 API to returns issuer list based on tenant code, insurance type and State Code
		public static final String GET_ISSUER_LIST = PLAN_MGMT_URL + "issuerInfo/getissuerlist";
		// HIX-69392 API for crosswalk plan 
		public static final String GET_CROSSWALK_HIOS_PLAN_ID = PLAN_MGMT_URL + "plan/getcrosswalkhiosplanid";
		//HIX-74557 send enrollmentenddateflag value 
		public static final String GET_ENROLLMENT_END_DATE_FLAG = PLAN_MGMT_URL + "issuerInfo/getenrollmentenddateflag";
		public static final String REAPPLY_PLAN_BENEFIT_EDITS = PLAN_MGMT_URL + "plan/reapplyPlanBenefitEdits";
		//HIX-75367 adding end point for generate plan provider network report
		public static final String GENERATE_PROVIDER_PLAN_NETWORK_REPORT = PLAN_MGMT_URL + "providerReport/generateProviderPlanNetworkReport";
		//HIX-78063 : Create a Plan Management API to return a list of IssuerDTO given userid of issuer representative
		public static final String FIND_ISSUER_DETAILS_BY_USER_ID = PLAN_MGMT_URL + "issuerInfo/findIssuerDetailsByUserId/";
		// HIX-78212 :: Create an API to accept Plan ID, Plan Year and return if the plan is Pediatric or Adult or Family Dental plan
		public static final String GET_PEDIATRIC_PLAN_INFO = PLAN_MGMT_URL + "plan/getpediatricplaninfo";
		//HIX-82031 :: Create the new API getPlanInfoByHIOSPlanId
		public static final String GET_PLAN_INFO_BY_HIOS_PLAN_ID = PLAN_MGMT_URL + "plan/getPlanInfoByHiosPlanId";
		// HIX-82024 :: Provide a new API called getRatingArea
		public static final String GET_RATING_AREA = PLAN_MGMT_URL + "plan/getRatingArea";
		//HIX-82954
		public static final String GET_ISSUER_BRANDNAME_BY_ISSUER_ID_LIST = PLAN_MGMT_URL + "issuerInfo/issuerBrandNameByIssuerIdList";
		//HIX-106018
		public static final String GET_ISSUERS_LEGALNAME_BY_ISSUER_ID_LIST = PLAN_MGMT_URL + "issuerInfo/issuersLegalNameByIssuerIdList";
		// HIX-83288 :: publish API to return issuer data by HIOS Id 
		public static final String GET_ISSUER_INFO_BY_HIOS_ID = PLAN_MGMT_URL + "issuerInfo/byHIOSId";
		// HIX-83451 :: publish API to return issuer data by Name
		public static final String GET_ISSUER_INFO_BY_NAME = PLAN_MGMT_URL + "issuerInfo/byName";
		// HIX-84594 :: Publish API for Life Plan quoting
		public static final String GET_LIFE_PLANS = PLAN_MGMT_URL + "life/getPlans";
		//HIX-85945 :: Publish new API to quote household for period loop
		public static final String LIFE_SPAN_ENROLLMENT_QUOTING = PLAN_MGMT_URL + "planratebenefit/lifeSpanEnrollmentQuoting";
		// HIX-88249 :: API to get the Lowest Plan Premium available for household
		public static final String GET_LOWEST_PREMIUM = PLAN_MGMT_URL + "planratebenefit/getLowestPremium";
		
		//Web
		public static final String GET_DISTINCT_PLAN_YEARS_BY_INSURANCE_TYPE = PLAN_MGMT_URL + "plan/getDistinctPlanYearsByInsuranceType";
		public static final String GET_DISTINCT_PLAN_YEARS_BY_INSURANCE_TYPE_AND_ISSUERID = PLAN_MGMT_URL + "plan/getDistinctPlanYearsByInsuranceTypeAndIssuerId";
		public static final String GET_ISSUER_BRAND_NAMES_BY_IS_DELETED = PLAN_MGMT_URL + "issuerBrandName/search/findIssuerBrandNamesByIsDeleted";
		public static final String GET_PLAN_DETAILS_BY_ID = PLAN_MGMT_URL + "plan/";
		public static final String GET_PLAN_CERTIFICATION_HISTORY = PLAN_MGMT_URL + "plan/getPlanCertificationHistory";
		public static final String GET_ISSUER_CERTIFICATION_HISTORY = PLAN_MGMT_URL + "issuers/certificationHistory";
		public static final String UPDATE_PLAN_DETAILS = PLAN_MGMT_URL + "plan/updatePlanDetails";
		public static final String GET_PLANS = PLAN_MGMT_URL + "plan/getPlans";
		public static final String GET_ISSUER_BRAND_NAMES = PLAN_MGMT_URL + "issuerbrandnames";
		public static final String UPDATE_ISSUER_BRAND_NAMES = PLAN_MGMT_URL + "issuerbrandnames/updateIssuerBrandName";
		public static final String CHECK_BRAND_NAME_ID_DUPLICATE = PLAN_MGMT_URL + "issuerbrandnames/checkDuplicate";
		public static final String GET_PLAN_HISTORY = PLAN_MGMT_URL + "plan/getPlanHistory";
		public static final String GET_ISSUER_PLAN_HISTORY = PLAN_MGMT_URL + "plan/getIssuerPlanHistory";
		public static final String GET_SERVICE_AREA_DETAILS = PLAN_MGMT_URL + "plan/getServiceAreaDetails";
		public static final String GET_PLAN_BENEFITS_HISTORY = PLAN_MGMT_URL + "plan/getBenefitsHistory";
		public static final String GET_PLAN_TENANTS = PLAN_MGMT_URL + "plan/getTenants";
		public static final String GET_PLAN_BENEFITS_AND_COST_DATA = PLAN_MGMT_URL + "plan/getBenefitsAndCostData";
		public static final String ADD_TENANTS = PLAN_MGMT_URL + "plan/addTenants";
		public static final String ADD_TENANTS_FOR_SELECTED_PLANS = PLAN_MGMT_URL + "plan/addTenantsForSelectedPlans";
		public static final String GET_PLAN_COUNT_BY_HIOS_ISSUER_ID = PLAN_MGMT_URL + "plan/getPlanCountByHiosIssuerId/";
		public static final String GET_ISSUER_DROP_DOWN_LIST = PLAN_MGMT_URL + "issuers/getIssuerDropDownList";
		public static final String GET_ISSUER_DROP_DOWN_LIST_BY_STATE = PLAN_MGMT_URL + "issuers/getIssuerDropDownListByState/";
		public static final String GET_PLAN_RATE_HISTORY = PLAN_MGMT_URL + "plan/getPlanRateHistory";
		public static final String GET_PLAN_RATE_LIST = PLAN_MGMT_URL + "plan/getPlanRateList";
		public static final String GET_FORMULARY_DRUG_LIST = PLAN_MGMT_URL + "plan/getFormularyDrugList";
		public static final String UPDATE_PLAN_DOCUMENTS_URL = PLAN_MGMT_URL + "plan/updatePlanDocuments";
		public static final String GET_ISSUER_LIST_FOR_DROP_DOWN = PLAN_MGMT_URL + "issuers/getIssuerListForDropDown";
		public static final String GET_ISSUER_LIST_FOR_ISSUER_REP = PLAN_MGMT_URL + "issuers/getIssuerListForIssuerRep/";
		public static final String GET_ISSUER_LIST_FOR_USER = PLAN_MGMT_URL + "issuers/getIssuerListForUser/";
		public static final String GET_ISSUER_FOR_ISSUER_REP = PLAN_MGMT_URL + "issuers/getIssuerForIssuerRep/";
		public static final String GET_MEDICARE_ISSUER_LIST_FOR_DROP_DOWN = PLAN_MGMT_URL + "issuers/getMedicareIssuerListForDropDown";
		public static final String GET_MEDICARE_ISSUER_LIST = PLAN_MGMT_URL + "medicare/issuers";
		public static final String GET_PLAN_ENROLLMENT_HISTORY = PLAN_MGMT_URL + "plan/getEnrollmentHistory";

		// PM - SerffRestController end points
		public static final String CREATE_BATCH_RECORD = PLAN_MGMT_URL + "serfftransfer/createBatchRecord";
		public static final String UPDATE_BATCH_RECORD = PLAN_MGMT_URL + "serfftransfer/updateBatchRecord";
		public static final String SERFF_TRANSFER = PLAN_MGMT_URL + "serfftransfer/";
		public static final String CREATE_PLAN_DOCUMENT_RECORD_URL = PLAN_MGMT_URL + "serfftransfer/createPlanDocumentRecord";
		public static final String GET_PLAN_DOCUMENT_LIST_URL = PLAN_MGMT_URL + "serfftransfer/getPlanDocumentList";
		public static final String GET_SERFF_BATCH_DATA_LIST_URL = PLAN_MGMT_URL + "serfftransfer/batchDataList";

		//HIX-101380 : create a new API which accepts hiosId(s) and return the SBC scenario.
		public static final String GET_SBC_SCENARIO_BY_HIOS_PLAN_ID = PLAN_MGMT_URL + "plan/getSbcScenario";
		public static final String UPDATE_BENEFIT_AND_COST_URL = PLAN_MGMT_URL + "plan/updateBenefitAndCost";
		
		//HIX-102041
		public static final String PRESCRIPTION_SERACH_BY_HIOS_ID_URL = PLAN_MGMT_URL + "prescription/search/byHiosPlanId";
		public static final String REMOVE_TENANTS = PLAN_MGMT_URL + "plan/removeTenants";
		public static final String REMOVE_TENANTS_FOR_SELECTED_PLANS = PLAN_MGMT_URL + "plan/removeTenantsForSelectedPlans";
		public static final String MODIFY_MEDICARE_PLAN_STATUS_URL = PLAN_MGMT_URL + "plan/modifyMedicarePlanStatus";
		public static final String UPDATE_PLANS_STATUS_URL = PLAN_MGMT_URL + "plan/updatePlanStatus";
		public static final String SOFT_DELETE_PLANS_URL = PLAN_MGMT_URL + "plan/bulkSoftdelete";

		public static final String ISSUER_PAYMENT_INFO_URL = PLAN_MGMT_URL + "issuerpayments";
		public static final String GET_ISSUER_PAYMENT_INFO_URL = PLAN_MGMT_URL + "issuerpayments/hiosId/";
		public static final String GET_ISSUER_BY_HIOSID_URL = PLAN_MGMT_URL + "issuers/hiosId/";
		public static final String GET_ISSUER_LIST_URL = PLAN_MGMT_URL + "issuers/";
		public static final String UPDATE_ISSUER_BY_ID_URL = PLAN_MGMT_URL + "issuers/";
		public static final String CREATE_ISSUER_BY_ID_URL = PLAN_MGMT_URL + "issuers";
		public static final String GET_ISSUER_ENROLLMENT_INFO_URL_PART1 = PLAN_MGMT_URL + "issuers/";
		public static final String GET_ISSUER_ENROLLMENT_INFO_URL_PART2 = "/enrollmentInfo";
		public static final String UPDATE_ISSUER_ENROLLMENT_INFO_URL = PLAN_MGMT_URL + "issuers/updateIssuerEnrollmentFlow";
		public static final String GET_ISSUER_COMMISSION_URL = PLAN_MGMT_URL + "issuercommissions/hiosId/";
		public static final String UPDATE_ISSUER_COMMISSION_URL = PLAN_MGMT_URL + "issuercommissions/updateIssuerCommission";
		public static final String GET_ISSUER_HISTORY_URL = PLAN_MGMT_URL + "issuers/history/";

		public static final String GET_PLAN_COUNT_FOR_CSR_VARIATIONS = PLAN_MGMT_URL + "plan/getPlanCountForCsrVariations";
		public static final String BULK_ENABLE_ON_EXCHANGE_PLANS_FOR_OFF_EXCHANGE = PLAN_MGMT_URL + "plan/bulkEnableOnExchangePlansForOffExchange";
		public static final String BULK_UPDATE_PLAN_STATUS = PLAN_MGMT_URL + "plan/bulkUpdatePlanStatus";
		public static final String BULK_ENABLE_SELECTED_ON_EXCHANGE_PLANS_FOR_OFF_EXCHANGE = PLAN_MGMT_URL + "plan/bulkEnableSelectedOnExchangePlansForOffExchange";
		public static final String BULK_UPDATE_PLAN_STATUS_FOR_SELECTED_PLANS = PLAN_MGMT_URL + "plan/bulkUpdatePlanStatusForSelectedPlans";
		public static final String BULK_UPDATE_NON_COMMISSION_FLAG_URL = PLAN_MGMT_URL + "plan/bulkUpdateNonCommissionFlag";

		public static final String CREATE_ISSUER_REPS_URL = PLAN_MGMT_URL + "issuerreps";
		public static final String GET_ISSUER_REPS_URL = PLAN_MGMT_URL + "issuerreps/";
		public static final String UPDATE_ISSUER_REPS_URL = PLAN_MGMT_URL + "issuerreps/";
		public static final String ASSIGN_ISSUER_REPS_MAPPING_URL = PLAN_MGMT_URL + "issuerreps/assignMapping";
		public static final String REMOVE_ISSUER_REPS_MAPPING_URL = PLAN_MGMT_URL + "issuerreps/removeMapping/";
		public static final String GET_ISSUER_REPS_BY_EMAIL_ID_URL = PLAN_MGMT_URL + "issuerreps/email";
		public static final String GET_ISSUER_REPS_LIST_BY_HIOS_ID_URL = PLAN_MGMT_URL + "issuerreps/hiosId/";
		public static final String GET_ISSUER_REPS_LIST_URL = PLAN_MGMT_URL + "issuerreps/list";
	}

	public static class SerffEndPoints {
		public static final String PING_SERFF_MODULE = GHIX_SERFF_SERVICE_URL + "services/Ping";
		public static final String UPDATE_EXCHANGE_WORK_FLOW_STATUS = GHIX_SERFF_SERVICE_URL + "exchangePlanManagementRestService/updateExchangeWorkflowStatus";
	}

	public static class TicketMgmtEndPoints {
		public static final String SEARCHTICKETS 				= TICKETMGMT_URL + "ticketmgmt/searchtickets";
		public static final String GETTICKETTYPE 				= TICKETMGMT_URL + "ticketmgmt/gettickettype";
		public static final String SEARCH_TKMTICKETS_ID 		= TICKETMGMT_URL + "ticketmgmt/findbyid/";
		public static final String TICKETAGEINGREPORT 		    = TICKETMGMT_URL + "ticketmgmt/ageing/report";
		public static final String SEARCH_TKMTICKETS_SUBJECT 	= TICKETMGMT_URL + "ticketmgmt/getticketsubject";
		public static final String GETWORKFLOWLIST				= TICKETMGMT_URL + "ticketmgmt/getworkflowlist";
		public static final String QUEUEURL 					= TICKETMGMT_URL + "ticketmgmt/queueslist";
		public static final String ADDNEWTICKET_URL 			= TICKETMGMT_URL + "ticketmgmt/addnewtickets";
		public static final String EDIT_TICKET_URL 				= TICKETMGMT_URL + "ticketmgmt/editticket";
		//New URL mapping added by Kuldeep
		public static final String GETTICKETTASKLIST			= TICKETMGMT_URL + "ticketmgmt/gettickettasklist";
		//New URL mapping added by Kuldeep for saving Ticket Comments
		public static final String SAVECOMMENTS					= TICKETMGMT_URL + "ticketmgmt/savecomment";

		public static final String USERTICKETLIST				= TICKETMGMT_URL + "ticketmgmt/getUserTicketList";

		public static final String CLAIMUSERTASK				= TICKETMGMT_URL + "ticketmgmt/claimTicketTask";

		public static final String COMPLETEUSERTASK				= TICKETMGMT_URL + "ticketmgmt/completeTicketTask";
		//new URL mapping added for displaying ticket history
		public static final String TICKETHISTORY                = TICKETMGMT_URL + "ticketmgmt/findhistorybyid/";

		public static final String TASKFORMPROPERTIES			= TICKETMGMT_URL + "ticketmgmt/getTaskFormProperties";

		public static final String CREATENEWTICKET				= TICKETMGMT_URL + "ticketmgmt/tickets/ticket/create";
		public static final String CREATEORUPDATETICKET			= TICKETMGMT_URL + "ticketmgmt/tickets/ticket/createOrUpdate";
		public static final String UPDATETICKET					= TICKETMGMT_URL + "ticketmgmt/updateticket";
		public static final String GETTICKETSTATUS				= TICKETMGMT_URL + "ticketmgmt/getticketstatus";
		public static final String CANCELTICKET					= TICKETMGMT_URL + "ticketmgmt/cancelticket";
		public static final String GETTICKETDETAILS				= TICKETMGMT_URL + "ticketmgmt/getticketdetails";
		public static final String GETTICKETLIST				= TICKETMGMT_URL + "ticketmgmt/getticketlist";
		public static final String GETTICKETDOCUMENTPATHLIST	= TICKETMGMT_URL + "ticketmgmt/getticketdocuments";
		public static final String GETTICKETDOCUMENTPATHLISTANDFORMPARAMS	= TICKETMGMT_URL + "ticketmgmt/getTicketDocumentsAndFormParams";
		public static final String GETTICKETACTIVETASKS			= TICKETMGMT_URL + "ticketmgmt/getTicketActiveTask";
		public static final String REASSIGNTICKETACTIVETASKS	= TICKETMGMT_URL + "ticketmgmt/reassignTicketActiveTask";
		public static final String UPDATEQUEUEUSERS				= TICKETMGMT_URL + "ticketmgmt/updatequeueusers";
		public static final String SAVETICKETATTACHMENTS		= TICKETMGMT_URL + "ticketmgmt/saveTicketAttachments";
		public static final String GET_TICKETTASK_BY_ASSIGNEE 	= TICKETMGMT_URL + "ticketmgmt/gettickettaskbyassignee";
		public static final String GET_TASK_USER_INFO_BY_ASSIGNEE  = TICKETMGMT_URL + "ticketmgmt/geTaskUserInfobyAssignee";
		public static final String DELETETICKETATTACHMENTS		= TICKETMGMT_URL + "ticketmgmt/deleteTicketAttachments";
		public static final String GET_TICKET_NUMBERS			= TICKETMGMT_URL + "ticketmgmt/getticketnumbers";
		public static final String GET_REQUESTERS				= TICKETMGMT_URL + "ticketmgmt/getrequesters";
		public static final String GETSIMPLEORDETAILEDHISTORY	= TICKETMGMT_URL + "ticketmgmt/getSimpleOrDetailedHistory";
		public static final String GETTICKETCOUNTFORPIEREPORT 	= TICKETMGMT_URL + "ticketmgmt/getTicketCountForPieReport";
		public static final String SENDSECUREMSG 				= TICKETMGMT_URL + "ticketmgmt/sendsecuremsg";
		public static final String FETCHTARGETTICKETFORMPROERTIES 	= TICKETMGMT_URL + "ticketmgmt/fetchTargetTicketFormProerties";
		public static final String PERFORMRECLASSIFYTICKETWORKFLOW 	= TICKETMGMT_URL + "ticketmgmt/performTicketWorkflowReclassify";
		public static final String GETALLUSERSFORQUEUE 			= TICKETMGMT_URL + "ticketmgmt/getAllUsersForQueue";
		public static final String GETALLUSERSFORQUEUEFORREASSIGN	= TICKETMGMT_URL + "ticketmgmt/getAllUsersForQueueForReassign";
		
		public static final String GETQUEUESFORUSER 			= TICKETMGMT_URL + "ticketmgmt/getQueuesForUser";
		public static final String GETDETAILSFORQUICKACTIONBAR 	= TICKETMGMT_URL + "ticketmgmt/getDetailsForQuickActionBar";
		public static final String SEARCHPENDINGTICKETS 		= TICKETMGMT_URL + "ticketmgmt/getPendingTicketDetails";
		public static final String GET_HOUSEHOLDS_CREATEDFOR 	= TICKETMGMT_URL + "ticketmgmt/getHouseholdNamesForCreatedFor";
		public static final String GET_HOUSEHOLD_BY_USER_ID 	= TICKETMGMT_URL + "ticketmgmt/getHouseholdByuserId";
		public static final String ARCHIVETICKET                = TICKETMGMT_URL + "ticketmgmt/archive";
		public static final String LIST			 				= TICKETMGMT_URL + "ticketmgmt/list";
		public static final String REOPEN			 			= TICKETMGMT_URL + "ticketmgmt/reopen";
		public static final String RESTART			 			= TICKETMGMT_URL + "ticketmgmt/restart";
		public static final String ASSIGNTICKET			 		= TICKETMGMT_URL + "ticketmgmt/assign";
		public static final String ADD_QUEUES_TO_CSR_USER		= TICKETMGMT_URL + "ticketmgmt/addqueuestocsruser";
		public static final String UPDATE_DEFAULT_QUEUES		= TICKETMGMT_URL + "ticketmgmt/updatedefaultqueues";
		public static final String CANCEL_QLE_TICKET            = TICKETMGMT_URL + "ticketmgmt/tickets/ticket/cancel";
		public static final String GETCONSUMERTICKETLIST		= TICKETMGMT_URL + "ticketmgmt/tickets/consumer";
	}

	/**
	 * This class contains the URLs related to the ghix-prescreen-svc module
	 * @author Sunil Desu
	 */
	public static class PrescreenServiceEndPoints{
		public static final String CALCULATE_APTC_URL 				= PRESCREEN_SVC_URL+"eligibility/calculateAptc";
		public static final String GET_APTC_URL 					= PRESCREEN_SVC_URL+"eligibility/getAptc";
		public static final String GET_PRESCREEN_RECORD 			= PRESCREEN_SVC_URL+"eligibility/getPrescreenRecord";
		public static final String GET_PRESCREEN_RECORD_BY_SESSION 	= PRESCREEN_SVC_URL+"eligibility/getPrescreenRecordBySessionId";
		public static final String SAVE_PRESCREEN_RECORD 			= PRESCREEN_SVC_URL+"eligibility/savePrescreenRecord";
		public static final String CALCULATE_CSR_URL 				= PRESCREEN_SVC_URL+"eligibility/computeCSR";
		public static final String OUTBOUND_HOUSEHOLD_INFO_URL		= PRESCREEN_SVC_URL + "api/v1/outbound/householdInfo/";
	}

	/**
	 * This class contains URLS for Rest apis implemented under ghix-broker
	 */
	public static class BrokerServiceEndPoints{
		public static final String GET_BROKER_DETAIL = GHIX_BROKER_URL+"/broker/getBrokerDetail/";
		public static final String GET_ASSISTER_BROKER_DETAIL = GHIX_BROKER_URL+"/broker/getBrokerAssisterDetail/";
		public static final String GHIX_WEB_AGENT_SERVICE = GHIX_BROKER_URL + "/broker/designateEmployer/";
		public static final String GET_AGENT_INFORMATION_BY_EMPLOYERID = GHIX_BROKER_URL + "/broker/brokerinformation";
		public static final String BROKER_DESIGNATE_INDIVIDUAL = GHIX_BROKER_URL + "/broker/designateIndividual";
		public static final String GET_LISTOF_CERTIFIED_BROKERS = GHIX_BROKER_URL + "/broker/getListOfCertifiedBroker";
		public static final String GET_LISTOF_DESIGNATED_BROKERS = GHIX_BROKER_URL + "/broker/listOfDesignatedBrokers";
		public static final String GET_INDIVIDUAL_DESIGNATION = GHIX_BROKER_URL + "/broker/findIndividualDesignation";
		public static final String GET_SET_PARTICIPATION_DETAILS = GHIX_BROKER_URL + "/broker/participationDetails";
		public static final String GET_SET_CONNECT_HOURS = GHIX_BROKER_URL + "/broker/connectHours";
		public static final String SET_BROKER_AVAILABILITY = GHIX_BROKER_URL + "/broker/availability";
		public static final String GET_BROKER_DESIGNATED_NAME = GHIX_BROKER_URL + "/broker/designated/name";
	}

	//Ghix hub URLs
	public static class HubEndPoints {
		public static final String PING_HUB_MODULE = HUB_SERVICE_URL + "services/Ping";
		//public static final String SAVE_APPLICANT_ELIGIBILITY = HUB_SERVICE_URL + "eligibility/applicantEligibility";
		public static final String APPLICANT_ENROLLMENTBB_URL = HUB_SERVICE_URL + "benefitbay/sendenrollmenttobb";
		public static final String APPLICANT_ELIGIBILITY_URL = HUB_SERVICE_URL + "eligibility/applicantEligibility";
		public static final String APPLICANT_ENROLLMENT_URL = HUB_SERVICE_URL + "enrollment/applicantenrollment";

		public static final String BENEFITBAY_URL ="dev.benefitbay.com";
		public static final String JH_AFFILIATE_HOUSEHOLD_DATA = HUB_SERVICE_URL + "jhEligibility/applicantData";
	}

	//Ghix FFM URLs
	public static class FfmEndPoints {
		public static final String FFM_APPLICANT_ELIGIBILITY_URL = FFM_SERVICE_URL + "eligibility/applicantEligibility";
		public static final String APPLICANT_ENROLLMENTBB_URL = FFM_SERVICE_URL + "benefitbay/sendenrollmenttobb";
		public static final String APPLICANT_ENROLLMENT_URL = FFM_SERVICE_URL + "enrollment/applicantenrollment";
	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by AFFILIATE module
	 *
	 */
	public static class AffiliateServiceEndPoints{
		public static final String ADD_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/add";
		public static final String UPDATE_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/edit";
		public static final String SEARCH_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/search";
		public static final String SEARCH_AFFILIATEFLOW = AFFILIATE_SERVICE_URL +"affiliateflow/search";
		public static final String SEARCH_CAMPAIGN = AFFILIATE_SERVICE_URL +"campaign/search";
		public static final String VIEW_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/%d";
		public static final String GET_AFFILIATE = AFFILIATE_SERVICE_URL +"getaffiliate/%d";

		public static final String VIEW_CAMPAIGN = AFFILIATE_SERVICE_URL +"campaign/%d";
		public static final String UPDATE_CAMPAIGN = AFFILIATE_SERVICE_URL +"campaign/edit";

		public static final String VALIDATE_APIKEY = AFFILIATE_SERVICE_URL +"affiliate/validateapikey/%d/%s";
		public static final String GET_AFFILIATE_BY_APIKEY = AFFILIATE_SERVICE_URL +"affiliate/findByApiKey/%s";
		public static final String CHECK_IVRNUMBEREXIST= AFFILIATE_SERVICE_URL +"affiliate/checkIfExistIVRNumber/%d";
		public static final String GET_AFFILIATEFLOW = AFFILIATE_SERVICE_URL +"affiliateflow/%d";
		public static final String AUTO_COMPLETE_MANAGE_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/manageaffiliate";
		public static final String AUTO_COMPLETE_MANAGE_CAMPAIGN = AFFILIATE_SERVICE_URL +"affiliate/managecampaign";
		public static final String AUTO_COMPLETE_MANAGE_AFFILIATEFLOW = AFFILIATE_SERVICE_URL +"affiliate/manageaffiliateflow";
		//public static final String TRACK_CLICKS= AFFILIATE_SERVICE_URL +"affiliate/clicktracker/%d/%d/%s/%s/%s/%s/%d";
		public static final String TRACK_CLICKS= AFFILIATE_SERVICE_URL +"affiliate/clicktracker/add";
		public static final String GET_AFFILIATE_CLICK = AFFILIATE_SERVICE_URL +"affiliate/affiliateclick/%d";

		public static final String ADD_AFFILIATEFILEUPLOAD = AFFILIATE_SERVICE_URL +"affiliateFileUpload/add";
		public static final String GET_AFFILIATEFLOWDETAILS = AFFILIATE_SERVICE_URL +"affiliateFileUpload/getAffFlowDetails";
		public static final String GET_AFFILIATEFLOW_BY_NAME = AFFILIATE_SERVICE_URL +"affiliateFileUpload/getAffFlow";

		public static final String UPDATE_AFFILIATEFILEUPLOAD = AFFILIATE_SERVICE_URL +"affiliateFileUpload/update";
		public static final String SEARCH_AFFILIATEFILEUPLOADLOGS = AFFILIATE_SERVICE_URL +"affiliateFileUpload/searchAffiliateFileUploadLogs";
		public static final String AUTO_COMPLETE_MANAGE_AFFILIATEFILEUPLOADLOGS = AFFILIATE_SERVICE_URL +"affiliateFileUpload/manageaffiliatefileuploadlogs";
		public static final String GET_AFFILIATEFILEUPLOAD = AFFILIATE_SERVICE_URL +"getaffiliateFileUpload/%d";
		public static final String FETCH_AFFILIATEFLOW_IVR = AFFILIATE_SERVICE_URL +"affiliate/fetchIVRNumber";
		public static final String GET_AFFILIATE_BY_IVR= AFFILIATE_SERVICE_URL +"affiliate/getAffiliateDataByIVRNumber/%d";
		public static final String AFFILIATE_DEFAULTFLOW = AFFILIATE_SERVICE_URL +"affiliate/defaultFlow/%d";
	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by FINANCE module
	 */
	public static class FinanceServiceEndPoints
	{
		public static final String SEARCH_NOTPAID_INVOICE = FINANCE_SERVICE_URL + "finance/notpaidemployerinvoices";
		public static final String SEARCH_PAID_INVOICE = FINANCE_SERVICE_URL + "finance/searchpaidemployerinvoices";
		public static final String SEARCH_INVOICE = FINANCE_SERVICE_URL +"finance/searchemployerinvoices";
		public static final String CREATE_PDF = FINANCE_SERVICE_URL +"employerpayment/createpdf/";
		public static final String REISSUE_INVOICE = FINANCE_SERVICE_URL +"employerpayment/reissueinvoice";
		public static final String EMPLOYER_INVOICE = FINANCE_SERVICE_URL +"finance/findemployerinvoicesbyid/";
		public static final String PROCESS_PAYMENT = FINANCE_SERVICE_URL +"employerpayment/processpayment";
		public static final String DUE_EMPLOYER_INVOICES = FINANCE_SERVICE_URL +"finance/finddueemployerinvoices";
		public static final String ISSUER_PAYMENT_PROCESSING = FINANCE_SERVICE_URL + "batch/issuerpaymentprocess";
		public static final String ACTIVE_EMPLOYER_INVOICES = FINANCE_SERVICE_URL + "/finance/findactiveemployerinvoices";
		public static final String EMPLOYER_PAYMENT_INVOICE = FINANCE_SERVICE_URL + "/finance/findemployerpaymentinvoicebyid/";
		public static final String PAYMENT_EVENT_REPORT = FINANCE_SERVICE_URL + "finance/findpaymenteventlogbycriteria";
		public static final String PAYMENTEVENTLOG_BY_MERCHANTREFNO = FINANCE_SERVICE_URL + "finance/findpaymenteventlogbymerchantrefno";
		public static final String PREVIOUSACTIVEDUEINPROCESSINVOICE = FINANCE_SERVICE_URL + "finance/findPreviousActiveDueInProcessInvoice";
		public static final String CANCEL_EMPLOYER_INVOICE = FINANCE_SERVICE_URL + "finance/cancelemployerinvoice";
		public static final String PAYMENT_REDIRECT_DETAILS = FINANCE_SERVICE_URL + "finance/saml/paymentRedirect";
		public static final String DECODE_SAML_RESPONSE = FINANCE_SERVICE_URL + "finance/saml/decodeSAMLOutput";
		//public static final String SEARCH_PAYMENT_HISTORY = FINANCE_SERVICE_URL + "finance/searchpaymenthistory";
		public static final String IS_TERMINATION_ALLOWED = FINANCE_SERVICE_URL + "finance/isterminationallowed";
		public static final String SEARCH_INVOICE_ECMDOC_ID = FINANCE_SERVICE_URL + "finance/searchinvoicebyecmdocid";
		/**
		 * url added for jira ID: HIX-49000
		 */
		public static final String UPDATE_INVOICE_FOR_MANUAL_ADJUSTMENT = FINANCE_SERVICE_URL + "finance/updateinvoiceformanualadjustment";
		public static final String FIND_ALL_ADJUSTMENT_REASONCODE = FINANCE_SERVICE_URL + "finance/findalladjustmentreasoncode";
		
		public static final String SEARCH_ACTIVE_DUE_INVOICES_BY_EMPLOYER = FINANCE_SERVICE_URL + "finance/searchactiveduenormalinvoicesforemployer";
		public static final String SEARCH_ACTIVE_ALL_DUE_INVOICES_BY_EMPLOYER = FINANCE_SERVICE_URL + "finance/searchactivedueinvoicesforemployer";
		public static final String INVOICE_LINE_ITEMS_DTO_BY_INVOICEID = FINANCE_SERVICE_URL + "finance/invoicelineitemsdtobyinvoiceid";
		
		/*
		 * HIX-50488
		 */
		public static final String SEARCH_PAYMENT_METHOD = FINANCE_SERVICE_URL + "paymentmethod/searchpaymentmethod";
		public static final String PAYMENTMETHODS_BY_ID = FINANCE_SERVICE_URL + "paymentmethod/getPaymentMethodById";
		public static final String PAYMENTMETHODS_BY_MODULEID_MODULENAME_PAGE = FINANCE_SERVICE_URL + "paymentmethod/getPagePaymentMethodsByMouduleIdAndModuleNameAndPageable";
		
		public static final String FIND_PAYMENT_METHOD_PCI = FINANCE_SERVICE_URL + "paymentmethod/findpaymentmethodbyPCI/";
		public static final String SAVE_PAYMENT_METHOD = FINANCE_SERVICE_URL + "paymentmethod/savepaymentmethod";
		public static final String MODIFY_PAYMENT_STATUS = FINANCE_SERVICE_URL + "paymentmethod/modifypaymentstatus";
		
		public static final String MIGRATE_PAYMENT_METHOD_PCI = FINANCE_SERVICE_URL + "paymentmethod/migratepaymentmethod";
		public static final String VALIDATE_CREDITCARD = FINANCE_SERVICE_URL + "financialinfo/validatecreditcard";
		public static final String GET_DEFAULT_PAYMENTMETHOD_BY_MODULEID_MODULENAME = FINANCE_SERVICE_URL + "paymentmethod/getDefaultPaymentMethodsByModuleIdAndModuleName";
		public static final String GET_ACTIVE_PAYMENTMETHOD_BY_MODULEID_MODULENAME = FINANCE_SERVICE_URL + "paymentmethod/getActivePaymentMethodsByModuleIdAndModuleName";
		public static final String RETRIEVE_CUSTOMER_PROFILE = FINANCE_SERVICE_URL + "paymentmethod/retrieveCustomerProfile";
		public static final String FIND_PAYMENT_METHOD_BY_ID_MODULEID_MODULENAME = FINANCE_SERVICE_URL + "paymentmethod/findPaymentMethodsByIdModuleIdNModuleName";
		
		/*
		 * HIX-62617
		 */
		public static final String SEARCH_EMPLOYER_PAYMENT_HISTORY = FINANCE_SERVICE_URL + "finance/searchemployerpaymenthistory";
		
		public static final String GET_MAP_JSON = FINANCE_SERVICE_URL + "Ffm/getMapJson";
		public static final String GET_SOURCE_JSON = FINANCE_SERVICE_URL + "Ffm/getSourceJson";
	}

	/**
	 * This class contains the URLs related to the ghix-prescreen-svc module
	 * @author Suhasini Goli
	 *
	 */
	public static class ConsumerServiceEndPoints{
		public static final String SAVE_MEMBER_RECORD 		= CONSUMER_SERVICE_URL+"consumer/saveMember";
		public static final String GET_MEMBER_BY_ID 	    = CONSUMER_SERVICE_URL+"consumer/findMemberById/{id}";
		public static final String SAVE_HOUSEHOLD_RECORD 	= CONSUMER_SERVICE_URL+"consumer/saveHousehold";
		public static final String GET_HOUSEHOLD_BY_ID 	    = CONSUMER_SERVICE_URL+"consumer/findHouseholdById";
		public static final String SEARCH_CONSUMER 	    = CONSUMER_SERVICE_URL+"consumer/manageConsumers";
		public static final String SEARCH_LEADS 	    = CONSUMER_SERVICE_URL+"consumer/manageLeads";
		public static final String GET_HOUSEHOLD_BY_USER_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdByUserId";
		public static final String GET_HOUSEHOLD_BY_CASE_ID  = CONSUMER_SERVICE_URL+"consumer/household/caseId/";
		public static final String GET_HOUSEHOLD_RECORD_BY_USER_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdRecordByUserId";
		public static final String GET_HOUSEHOLD_BY_ELIG_LEAD_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdByEligLeadId";
		public static final String GET_MEMBERS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findMembersByHousehold";
		public static final String SAVE_FAMILY  = CONSUMER_SERVICE_URL+"consumer/saveFamily";
		public static final String GET_HOUSEHOLD_BY_GI_HOUSEHOLD_ID = CONSUMER_SERVICE_URL + "consumer/findHouseholdByGIHouseholdId";
		public static final String SAVE_FFM_RESPONSE  = CONSUMER_SERVICE_URL+"consumer/saveFFMResponse";
		public static final String SAVE_FAMILY_FROM_FFM  = CONSUMER_SERVICE_URL+"consumer/saveFamilyFromFFM";
		public static final String GET_HOUSEHOLD_LIST = CONSUMER_SERVICE_URL + "consumer/getHouseholdList";
		public static final String GET_LINKAPPDATA			= CONSUMER_SERVICE_URL + "consumer/getlinkappdata";
		public static final String SAVE_HOUSEHOLD_INFO  = CONSUMER_SERVICE_URL+"consumer/saveHouseholdInformation";
		public static final String DRUG_SEARCH = CONSUMER_SERVICE_URL+"consumer/drugSearch";
		public static final String DRUG_DETAILS = CONSUMER_SERVICE_URL+"consumer/drugDetails";
		public static final String GET_FAIR_PRICE_OF_DRUG = CONSUMER_SERVICE_URL+"consumer/getFairPriceOfDrug";
		public static final String GET_FAIR_PRICE_OF_DRUG_BY_NDC = CONSUMER_SERVICE_URL+"consumer/getFairPriceOfDrugByNDC";
		public static final String GET_PRESCRIPTION_BY_ID 	       = CONSUMER_SERVICE_URL+"consumer/findPrescriptionById";
		public static final String GET_PRESCRIPTIONS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findPrescriptionsByHousehold";
		public static final String GET_PRESCRIPTIONS_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findPrescriptionsByHouseholdId";
		public static final String SAVE_PRESCRIPTION               = CONSUMER_SERVICE_URL+"consumer/savePrescription";
		public static final String SAVE_PRESCRIPTIONS              = CONSUMER_SERVICE_URL+"consumer/savePrescriptions";
		public static final String GET_PRESCRIPTION_BY_NAME	       = CONSUMER_SERVICE_URL+"consumer/findPrescriptionByName";
		public static final String DELETE_PRESCRIPTION_BY_ID 	   = CONSUMER_SERVICE_URL+"consumer/deletePrescriptionById";
		public static final String DELETE_PRESCRIPTION     	       = CONSUMER_SERVICE_URL+"consumer/deletePrescription";
		public static final String UPDATE_SMOKER_INFO_FOR_MEMBERS  = CONSUMER_SERVICE_URL+"consumer/updateSmokerInfoForMembers";
		public static final String SAVE_LOCATION  = CONSUMER_SERVICE_URL+"consumer/saveLocation";
		public static final String SEARCH_APPLICANTS 		= CONSUMER_SERVICE_URL + "consumer/manageapplicants";
		public static final String SAVE_HOUSEHOLD_ENROLLMENT  = CONSUMER_SERVICE_URL+"consumer/saveHouseholdEnrollment";
		public static final String GET_HOUSEHOLD_ENROLLMENT_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdEnrollmentByHouseholdId";
		public static final String SAVE_STATE_IN_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/saveStateInHousehold";
		public static final String SAVE_CALL_LOGS  = CONSUMER_SERVICE_URL+"consumer/saveCallLogs";
		public static final String SAVE_CALL_LOGS2  = CONSUMER_SERVICE_URL+"consumer/saveCallLogs2";
		public static final String SEARCH_WITH_LEADS  = CONSUMER_SERVICE_URL+"consumer/searchWithLeads";
		public static final String FETCH_CALL_LOG_BY_CALLID = CONSUMER_SERVICE_URL+"consumer/fetchCallLogByCallId";
		public static final String UPDATE_CALL_LOG = CONSUMER_SERVICE_URL+"consumer/updateCallLog";
		public static final String SAVE_STAGE_TRACKING_RECORD  = CONSUMER_SERVICE_URL+"consumer/saveStageTracking";
		public static final String GET_STAGE_TRACKING_BY_ID  = CONSUMER_SERVICE_URL+"consumer/findStageTrackingById";
		public static final String GET_STAGE_TRACKING_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findStageTrackingByHouseholdId";
		public static final String UPDATE_STAGE_TRACKING_STATUS  = CONSUMER_SERVICE_URL+"consumer/updateStageTrackingStatus";
		public static final String GET_STAGE_TRACKING_BY_STATUS  = CONSUMER_SERVICE_URL+"consumer/findStageTrackingByStatus";
		public static final String PROCESS_STAGE_TRACKING_RECORDS  = CONSUMER_SERVICE_URL+"consumer/processStageTrackingRecords";
		public static final String GET_APPLICANTS_BY_APPLICATION_ID  = CONSUMER_SERVICE_URL+"consumer/findApplicantsByApplicationId";
		public static final String CREATE_SSAP_APPLICATION  = CONSUMER_SERVICE_URL+"consumer/createSsapApplication";
		public static final String DELETE_SSAP_APPLICATION  = CONSUMER_SERVICE_URL+"consumer/deleteSsapApplication";
		public static final String GET_APPLICATIONS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findApplicationsByHousehold";
		public static final String GET_APPLICATIONS_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findApplicationsByHouseholdId";
		public static final String GET_SSAP_APPLICATIONS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findSsapApplicationsByHousehold";
		public static final String GET_SSAP_APPLICATIONS_BY_ELIG_LEAD  = CONSUMER_SERVICE_URL+"consumer/findSsapApplicationsByEligLead";
		public static final String GET_ALL_APPLICATIONS_BY_ELIG_LEAD  = CONSUMER_SERVICE_URL+"consumer/findAllApplicationsByEligLead";
		public static final String GET_LATEST_HEALTH_HIOSID_BY_ELIG_LEAD_ID = CONSUMER_SERVICE_URL+"consumer/findLatestHealthHiosIdByEligLeadId";
		public static final String FIND_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/getSsapApplicationById";
		public static final String UPDATE_STAGE_BY_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/updateStageBySsapApplicationId";
		public static final String FIND_SSAP_APPLICATION_BY_CASENUMBER  = CONSUMER_SERVICE_URL+"consumer/findSsapApplicationByCaseNumber";
		public static final String UPDATE_STAGE_AND_STATUS_BY_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/updateStageAndStatusBySsapApplicationId";
		public static final String FIND_STAGE_AND_STATUS_BY_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/findStageAndStatusBySsapApplicationId";
		public static final String UPDATE_CMR_HOUSEHOLD_ID_FOR_SSAP_APPLICATIONS  = CONSUMER_SERVICE_URL+"consumer/updateCmrHouseholdIdForSSAPApplications";
		public static final String GET_CONSUMER_CALL_HISTORY 	    = CONSUMER_SERVICE_URL+"consumer/getConsumerCallLogHistory";
		public static final String SAVE_CONSUMER_CALL_LOG 	    = CONSUMER_SERVICE_URL+"consumer/saveConsumerCallLog";
		public static final String SAVE_CONSUMER_EVENT_LOG 	    = CONSUMER_SERVICE_URL+"consumer/saveConsumerEventLog";
		public static final String SAVE_CONSUMER_COMMENT_LOG 	    = CONSUMER_SERVICE_URL+"consumer/saveConsumerCommentLog";
		public static final String UPDATE_HOUSEHOLD_ID_AND_ELIG_LEAD_ID_BY_ANONYMOUS_ELIG_LEAD_ID  = CONSUMER_SERVICE_URL+"consumer/updateHouseholdIdAndEliLeadIdByAnonymousEligLeadId";
		public static final String RELINK_CMRHOUSEHOLD_TO_USER = CONSUMER_SERVICE_URL+"consumer/relinkHouseholdToUser";
		public static final String CHECKEMAILIFEXIST 	    = CONSUMER_SERVICE_URL+"consumer/checkIfEmailExists";
		public static final String CHECK_EXACT_HOUSEHOLD_EXISTS = CONSUMER_SERVICE_URL+"consumer/checkExactHouseholdExists";
		public static final String GET_LEADS 	    = CONSUMER_SERVICE_URL+"leads/findLeads";
		public static final String MANAGE_LEADS 	    = CONSUMER_SERVICE_URL+"leads/manageLeads";
		public static final String IS_APPLICATION_LOCKED  = CONSUMER_SERVICE_URL+"consumer/isApplicationLocked";
		public static final String GET_APPLICATION_DETAILS = CONSUMER_SERVICE_URL+ "cmrappsvc/getdetails";
		public static final String IMPORT_FAMILY_DETAILS = CONSUMER_SERVICE_URL+ "cmrappsvc/importFamilyDetails";
        public static final String GET_APPLICATIONLIST_BY_HOUSEHOLD = CONSUMER_SERVICE_URL+ "cmrappsvc/getApplicationList";
		public static final String SCREEN_POP_GETINITDATA = CONSUMER_SERVICE_URL+ "cap/screenpopsvc/initdata";	
		public static final String SCREEN_POP_GETINITDATABYHOUSEHOLD = CONSUMER_SERVICE_URL+ "cap/screenpopsvc/initdatabyhousehold";	
		public static final String SCREEN_POP_GETSIDEBARDATABYHOUSEHOLD = CONSUMER_SERVICE_URL+ "cap/screenpopsvc/sidebardatabyhousehold";	
		public static final String SAVEOFFEXCHANGEAPP = CONSUMER_SERVICE_URL+ "cmrappsvc/saveOffExchangeApp";
		public static final String SAVED2CAPP = CONSUMER_SERVICE_URL+ "cmrappsvc/saveD2CApp";
		public static final String SAVEONEXCHANGEFFMAPP = CONSUMER_SERVICE_URL+ "cmrappsvc/saveOnExchangeFFMApp";
		public static final String SAVEONEXCHANGESBEAPP = CONSUMER_SERVICE_URL+ "cmrappsvc/saveOnExchangeSBEApp";
		public static final String SAVEAPPLICATION = CONSUMER_SERVICE_URL+ "cmrappsvc/saveapplication";
		public static final String SAVEADMINISTRATIVEDETAILS = CONSUMER_SERVICE_URL+ "cmrappsvc/saveAdministrativeDetails";
		public static final String SCREEN_POP_GET_HOUSEHOLD_ADRESS = CONSUMER_SERVICE_URL+ "cap/screenpopsvc/household/adress";
		public static final String SCREEN_POP_UPDATE_HOUSEHOLD_ADRESS = CONSUMER_SERVICE_URL+ "cap/screenpopsvc/household/adress/update";
		public static final String CHANGE_LEAD_STATUS 	    = CONSUMER_SERVICE_URL+"leads/changeleadstatus";
		public static final String REASSIGN_LEAD 	    = CONSUMER_SERVICE_URL+"leads/reassignlead";
		public static final String GETHOUSEHOLDACCESSCODES = CONSUMER_SERVICE_URL+"consumer/memeber/accesscode";
		public static final String SEARCH_APP_EVENTS = CONSUMER_SERVICE_URL+"consumer/searchappevents";
		public static final String MANAGE_CONSUMER 	    = CONSUMER_SERVICE_URL+"consumer/manage";
		public static final String CREATE_OR_UPDATE_HOUSEHOLD = CONSUMER_SERVICE_URL+"consumer/createOrUpdateHousehold";
		public static final String UPDATE_EDIT_MEMBER_DETAILS 	= CONSUMER_SERVICE_URL+"consumer/updateEditMemberDetails";
		public static final String APPOINTMENT_API_BASE = CONSUMER_SERVICE_URL + "api/cap/appointment";

		//manual enrollment
		public static final String SAVE_MANUAL_ENROLLMENT = CONSUMER_SERVICE_URL + "cmrappsvc/saveManualEnrollment";
		public static final String UPDATE_CONSUMER_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/updateConsumerApplicationById";
		
		public static final String UPDATE_SSAP_APPLICATION_FOR_PROXY_ENROLLMENT  = CONSUMER_SERVICE_URL+"consumer/updateSsapApplicationExtensionForProxyEnrollment";
		public static final String UPDATE_SSAP_APPLICATION_FOR_PROXY_JOB  = CONSUMER_SERVICE_URL+"consumer/updateSsapApplicationExtensionForProxyJob";
		public static final String FIND_SSAP_APPLICATION_EXT_PHIX_BY_SSAP_ID  = CONSUMER_SERVICE_URL+"consumer/findConsumerApplicationExtPhixBySsapId";
		public static final String ADD_AGENT  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/add";
		public static final String SEARCH_AGENTS  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/manage";
		public static final String VIEW_AGENT  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/viewAgentDetails/";
		public static final String UPDATE_AGENT  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/updateAgentDetails";
		public static final String AGENT_SHIFT_LIST  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/getShifts";
		public static final String UPDATE_AGENT_TEAM  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/updateAgentTeam";
		public static final String UPDATE_AGENT_STATUS  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/updateAgentStatus";
		public static final String UPDATE_STATUS_FOR_AGENTS  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/updateStatusForAgents";
		public static final String FIND_ACTIVE_AGENTS  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/getActiveAgents";
		public static final String FIND_ACTIVE_USER_AGENTS_ROLE  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/getActiveUserAgentsByRole";
		public static final String FIND_ACTIVE_CSR_USERS_BY_NAME = CONSUMER_SERVICE_URL + "/cap/agnetsvc/getActiveCsrUsersByName";
		
		public static final String GET_AGENTS_GOALS  = CONSUMER_SERVICE_URL+"/cap/agnetsvc/goals";
		public static final String UPDATE_AGENTS_GOALS = CONSUMER_SERVICE_URL+"/cap/agnetsvc/updateAgentGoals";
		public static final String GET_HOUSEHOLD_MEDICARE_MEMBER = CONSUMER_SERVICE_URL+"/consumer/household/member/";
		public static final String SAVE_MEMBER_DATA = CONSUMER_SERVICE_URL+"/consumer/household/member/update";
		public static final String SAVE_HOUSEHOLD_MEMBERS 	= CONSUMER_SERVICE_URL+"consumer/household/members";
		public static final String GET_MEMBERS_BY_HOUSEHOLD_ID 	    = CONSUMER_SERVICE_URL+"consumer/membersByHouseholdId/";
		public static final String UPDATE_MEMBERS_FOR_HOUSEHOLD_ID = CONSUMER_SERVICE_URL+"consumer/upsertMembersByHouseholdId/";
		public static final String UPDATE_DISPOSITIONS_CODE = CONSUMER_SERVICE_URL+"/cap/api/disposition";
		public static final String AGENT_LEAD_PIPELINE_INFO = CONSUMER_SERVICE_URL+"cap/agent/leadpipeline/list";
		public static final String AGENT_LEAD_PIPELINE_UPDATE = CONSUMER_SERVICE_URL+"cap/agent/leadpipeline/update";
		public static final String EAPP_MODEL = CONSUMER_SERVICE_URL + "consumer/eapp/model";
		public static final String MEMBER_DISPOSITIONS = CONSUMER_SERVICE_URL + "consumer/member/dispositions";
		public static final String MEMBER_DRX_SESSION_ID = CONSUMER_SERVICE_URL + "consumer/drx/sessionId";
		public static final String GET_CONTRIUTION_FOR_HOUSEHOLD = CONSUMER_SERVICE_URL + "consumer/contribution";
		public static final String UPDATE_HOUSEHOLD_POST_ELIGIBILITY = CONSUMER_SERVICE_URL+"consumer/eligibilitycheck/cmr/update/";
		public static final String GET_SSAP_DOCUMENT =  CONSUMER_SERVICE_URL +"cmrappsvc/application/document/";
	}

	public static class D2CServiceEndpoints{
		public static final String ENROLLMENT_RECORD = D2C_SERVICE_URL+ "enrollment";
		public static final String ENROLLMENTUI_RECORD = D2C_SERVICE_URL+ "enrollmentUI";
		public static final String ENROLLMENTPAYMENT_RECORD = D2C_SERVICE_URL+ "enrollmentPayment";
		public static final String READ_ENROLLMENT_BY_HOUSEHOLD_ID = D2C_SERVICE_URL + "enrollment/householdId";
		public static final String READ_COUNTY_BY_ZIP = D2C_SERVICE_URL + "zipcode";
		public static final String READ_HUMANA_NETWORK_IDENT = D2C_SERVICE_URL + "humanaProductIdentifier";
		public static final String ENROLLMENT_STATUS = D2C_SERVICE_URL + "enrollmentStatus";
		public static final String GI_ENROLLMENT_STATUS = D2C_SERVICE_URL + "giEnrollmentStatus";
		public static final String READ_HUMANA_PAYMENT_OPTIONS = D2C_SERVICE_URL + "humanaPaymentOptions";
		public static final String READ_HCSC_DENTAL_RIDER_COUNTY_PREMIUM = D2C_SERVICE_URL + "hcscDentalRiderCountyPremium";
		public static final String READ_ENROLLMENT_BY_SSAP_ID = D2C_SERVICE_URL + "enrollment/ssapid";
		public static final String READ_ENROLLMENT_BY_GIAPP_ID = D2C_SERVICE_URL + "enrollment/giappid";
		public static final String HCSC_DENTAL_PDF = D2C_HCSC_URL + "directenrollment/dentalpdf/";
		public static final String INBOUND_HCSC_DENTAL_PDF = GHIXWEB_SERVICE_URL + "directenrollment/hcsc/dental/info/";
		public static final String UPDATE_ISSUER_STATUS_BY_GUID = D2C_SERVICE_URL + "enrollmentStatus/guid";
		public static final String REPORT_ENDPOINT = D2C_SERVICE_URL + "enrollment/report";
		public static final String UPDATE_HOUSEHOLD_BY_SSAP = D2C_SERVICE_URL + "enrollment/household/";
		public static final String EAPP_ENTRY_ENDPOINT = GHIX_EAPP_SVC_URL + "eapp";
		public static final String EAPP_SSAP_ENTRY_ENDPOINT = GHIX_EAPP_SVC_URL + "ssap/";

		
		/* CAP: Application  view/edit functionality mappings - Darshan Hardas */
	    /* CAP: Application  view/edit functionality mappings ENDS */
	}
	
	public static class EappServiceEndpoints{
		public static final String ENROLLMENT_RECORD = GHIX_EAPP_SVC_URL+ "service/enrollment";
		public static final String ENROLLMENTUI_RECORD = GHIX_EAPP_SVC_URL+ "service/enrollmentUI";
		public static final String ENROLLMENTPAYMENT_RECORD = GHIX_EAPP_SVC_URL+ "service/enrollmentPayment";
		public static final String READ_ENROLLMENT_BY_HOUSEHOLD_ID = GHIX_EAPP_SVC_URL + "service/enrollment/householdId";
		public static final String ENROLLMENT_STATUS = GHIX_EAPP_SVC_URL + "service/enrollmentStatus";
		public static final String GI_ENROLLMENT_STATUS = GHIX_EAPP_SVC_URL + "service/giEnrollmentStatus";
		public static final String READ_ENROLLMENT_BY_SSAP_ID = GHIX_EAPP_SVC_URL + "service/enrollment/ssapid";
		public static final String READ_ENROLLMENT_BY_GIAPP_ID = GHIX_EAPP_SVC_URL + "service/enrollment/giappid";
		public static final String UPDATE_ISSUER_STATUS_BY_GUID = GHIX_EAPP_SVC_URL + "service/enrollmentStatus/guid";
		public static final String FETCH_DOC_BY_DOCID = GHIX_EAPP_SVC_URL + "service/document";
		public static final String REPORT_ENDPOINT = GHIX_EAPP_SVC_URL + "service/enrollment/report";
		public static final String EAPP_SSAP_ENTRY_ENDPOINT = GHIX_EAPP_SVC_URL + "service/eapp/ssap";
		public static final String EAPP_FETCH_UI_TEMPLATE = GHIX_EAPP_SVC_URL + "service/eapp/uiTemplate";
		public static final String EAPP_FETCH_DISCLAIMER = GHIX_EAPP_SVC_URL + "service/eapp/disclaimer";
		public static final String EAPP_MODEL = GHIX_EAPP_SVC_URL + "service/eapp/model";
		public static final String EAPP_PREVIEW = GHIX_EAPP_SVC_URL + "service/eapp/preview";
		public static final String EAPP_SUBMIT = GHIX_EAPP_SVC_URL + "service/eapp/submit";
		public static final String EAPP_VIEW = GHIX_EAPP_SVC_URL + "service/eapp/view";
		public static final String EAPP_PAYMENT_VALIDATE = GHIX_EAPP_SVC_URL + "service/eapp/payment/validate";
		public static final String EAPP_GET_GI_APP_IDS = GHIX_EAPP_SVC_URL + "service/eapppdf/apps";
		public static final String EAPP_GET_BULK_PDFS = GHIX_EAPP_SVC_URL + "service/eapppdf/bulkpdf";
		public static final String EAPP_PAYMENT_GATEWAY_ACI = GHIX_EAPP_SVC_URL + "service/eapp/payment/gateway/aci";
        public static final String EAPP_WORKFLOW = GHIX_EAPP_SVC_URL + "service/eapp/workflow";

		
		
	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by Agent/Broker module
	 *
	 */
	public static class AgentServiceEndPoints {
		public static final String GET_AGENT_INFORMATION_BY_EMPLOYERID = GHIXWEB_SERVICE_URL + "broker/brokerinformation";
		public static final String GHIX_WEB_AGENT_SERVICE = GHIX_BROKER_URL + "broker/designateEmployer";

	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by ghix_eligibility_svc module for Verification Rules
	 *
	 */
	public static class VerificationRulesServiceEndPoints {
		public static final String VERIFY_RESIDENCY = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/residency";
		public static final String VERIFY_SSN = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/ssn";
		public static final String VERIFY_DEATH = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/death";
		public static final String VERIFY_INCARCERATION = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/incarceration";
		public static final String VERIFY_MEC = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/mec";
		public static final String VERIFY_VLP = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/vlp";
		public static final String VERIFY_CITIZENSHIP = GHIX_ELIGIBILITY_SVC_URL + "/verification/rule/citizenship";
	}

	public static class SsapIntegrationEndpoints {
		public static final String PROCESS_HOUSEHOLD_URL = ELIGIBILITY_URL + "processSSAPHouseHold";
		public static final String PROCESS_INCOME_URL = ELIGIBILITY_URL + "processSSAPIncome";
		public static final String INITIATE_ACCOUNT_TRANSFER_URL = ELIGIBILITY_URL + "initiateAccountTransfer";
		public static final String SSAP_INTEGRATION_URL = ELIGIBILITY_URL + "ssapIntegration";
		public static final String LCE_INTEGRATION_URL = ELIGIBILITY_URL + "lceIntegration";
		public static final String RETRY_SSAP_INTEGRATION_URL = ELIGIBILITY_URL + "retrySsapIntegration";
		public static final String RENEWAL_INTEGRATION_URL = ELIGIBILITY_URL + "renewalIntegration";;

	}

	public static class HubIntegrationEndpoints {
		public static final String RIDP_PRIMARY_URL = GHIX_HUB_INTEGRATION_URL + "invokeRIDPPrimary";
		public static final String RIDP_SECONDARY_URL = GHIX_HUB_INTEGRATION_URL + "invokeRIDPSecondary";
		public static final String FARS_URL = GHIX_HUB_INTEGRATION_URL + "invokeFARS";
		public static final String COI_URL = GHIX_HUB_INTEGRATION_URL + "getCountryOfIssuanceList";
		public static final String VLP_CLOSE_CASE_URL = GHIX_HUB_INTEGRATION_URL + "invokeCloseCaseRequest";
		public static final String INVOKE_APC_URL = GHIX_HUB_INTEGRATION_URL + "invokeAPC";
		public static final String CREATE_RRV_BATCH_ID = GHIX_HUB_INTEGRATION_URL + "createRRVBatch";
		public static final String SUBMIT_RRV_BATCH_RECORDS = GHIX_HUB_INTEGRATION_URL + "submitRRVRecords";
		public static final String GET_VERIFICATION_RESULT = GHIX_HUB_INTEGRATION_URL + "getVerificationResults";

	}

	public static class SsapEndPoints {
		public static final String SSAP_DATA_ACCESS_URL = ELIGIBILITY_URL + "data/";
		public static final String SSAP_APPLICATION_DATA_URL = ELIGIBILITY_URL + "data/SsapApplication/";
		public static final String RELATTIVE_SSAP_APPLICATION_DATA_URL = "/ghix-eligibility/data/SsapApplication/";
		///ghix-eligibility/data/SsapApplication/17050
		public static final String SSAP_APPLICATION_APPLICANTS_DATA_URL = ELIGIBILITY_URL + "data/SsapApplication/{id}/ssapApplicants/";
		public static final String SSAP_APPLICANTS_DATA_URL = ELIGIBILITY_URL + "data/SsapApplicant/";
		public static final String SSAP_APPLICATION_EVENT_DATA_URL = ELIGIBILITY_URL + "data/SsapApplicationEvent/";
		public static final String SSAP_APPLICANT_EVENT_DATA_URL = ELIGIBILITY_URL + "data/SsapApplicantEvent/";
		public static final String SEP_EVENT_DATA_URL = ELIGIBILITY_URL + "data/SepEvents/";
		public static final String SSAP_APPLICANT_EVENT_SEP_EVENT_DATA_URL = ELIGIBILITY_URL + "data/SsapApplicantEvent/{id}/sepEvents/";
		public static final String SSAP_APPLICATION_INTEGRATION_CALLBACK = ELIGIBILITY_URL + "/application/ssapIntegrationCallback";
		public static final String SSAP_GET_MULTIHOUSEHOLD_MEMBERS_URL = ELIGIBILITY_URL + "application/multiHouseholdMembers";
		public static final String LCE_GET_ENROLLED_APPLICANTS = ELIGIBILITY_URL + "iex/lce/getenrolledapplicants";
		public static final String LCE_INVOKE_IND57 = ELIGIBILITY_URL + "iex/lce/invokeind57";
		public static final String LCE_MOVE_ENROLLMENT = ELIGIBILITY_URL + "iex/lce/moveenrollment";
		public static final String LCE_PROCESSOR_ENDPOINT = ELIGIBILITY_URL + "iex/lce/processor";
		public static final String LCE_SAVEAPPLICATION_ENDPOINT = ELIGIBILITY_URL + "iex/lce/savessapapplication";
		public static final String LCE_GET_EVENT_NAMES = ELIGIBILITY_URL + "iex/lce/getsepeventnames";
	}
	
	/**
	 * Endpoints for services hosted by ghix-identity module
	 * 
	 * @author Nikhil Talreja
	 *
	 */
	public static class IdentityServiceEndPoints{
		public static final String WSO2_AVAILABILIITY_CHECK = IDENTITY_SVC_URL + "checkAvailability";
		public static final String WSO2_UPDATE_USER_URL = IDENTITY_SVC_URL + "updateUser";

	}
	
	/**
	 * 
	 * End point for covered ca proxy end points
	 * @author jape_g
	 *
	 */
	public static class CoveredCaProxyEndPoints{
		public static final String SEND_HOUSEHOLD_INFO = COVERED_CA_PROXY_URL + "submitHouseholdInformation";
		public static final String SEND_MEMBER_RELATIONSHIPS = COVERED_CA_PROXY_URL + "submitMemberRelationships";
		public static final String SEND_PERSONAL_DATA = COVERED_CA_PROXY_URL + "submitPersonalData";
		public static final String SEND_INCOME_DATA = COVERED_CA_PROXY_URL + "submitIncomeData";
		public static final String GET_RIDP_QUESTIONS = COVERED_CA_PROXY_URL + "extractRIDPQuestionnaire";
		public static final String GET_RIDP_VERIFICATION_STATUS = COVERED_CA_PROXY_URL + "submitRIDPAnswers";
		public static final String START_CA_APPLICATION = COVERED_CA_PROXY_URL + "startApplication";
		public static final String SUBMIT_CA_APPLICATION = COVERED_CA_PROXY_URL + "getEligibility";
		public static final String GET_PLAN_SELECTION_RESPONSE = COVERED_CA_PROXY_URL + "submitPlan";
		public static final String TERMINATE_APPLICATION = COVERED_CA_PROXY_URL + "terminateApplication";
		
	}

	public static class ScreenerEndPoints {
		public static final String GET_SLSP_RPT_ON_UPDATED_DATE_URL = MS_SCREENER_SERVICE_URL + "/slsp/report/getOnUpdatedDate/";
	}
	
	/**
	 * HIX-105846 MS Starr Endpoints
	 * @author negi_s
	 *
	 */
	public static class MsStarrEndPoints {
		public static final String ENROLLMENT_INTEGRATION = MS_STARR_SVC_URL + "api/v1/enrollments";
	}
	
	/**
	 * HIX-110523 External Broker EndPoints
	 * @author Anoop
	 */
	public static class ExternalAssisterEndPoints {
		public static final String DESIG_REDESIG_DEDESIG_ASSISTER = EXTERNAL_ASSISTER_URL + "externalassister/designateassister";
		public static final String GET_ASSISTER_DETAIL_FOR_HOUSEHOLD = EXTERNAL_ASSISTER_URL + "externalassister/getassisterdetailforhouseholdid";
		public static final String HOUSEHOLDS_FOR_ASSISTER = EXTERNAL_ASSISTER_URL + "externalassister/householdsforbroker";
		public static final String GET_EXTERNAL_ASSISTER_DETAIL = EXTERNAL_ASSISTER_URL + "externalassister/getexternalassisterdetail";
		
	}
}
