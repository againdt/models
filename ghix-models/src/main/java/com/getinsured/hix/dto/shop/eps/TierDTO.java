package com.getinsured.hix.dto.shop.eps;

import java.util.List;
import java.util.Set;

public class TierDTO {
	private String tierName;
	private List<Integer> planIds;
	
	//------calculated fields below
	private Float minIndvPremium = 0f;
	private Float maxIndvPremium = 0f;
	private Float minDependentPremium = 0f;
	private Float maxDependentPremium = 0f;
	private Float minMonthlyEmployerCost;
	private Float maxMonthlyEmployerCost;
	private Float minMonthlyEmployeeCost;
	private Float maxMonthlyEmployeeCost;
	private List<Integer> notAvailabilityList;
	private Set<Integer> empIds;
	
	public String getTierName() {
		return tierName;
	}
	public void setTierName(String tierName) {
		this.tierName = tierName;
	}
	public List<Integer> getPlanIds() {
		return planIds;
	}
	public void setPlanIds(List<Integer> planIds) {
		this.planIds = planIds;
	}
	public Float getMinIndvPremium() {
		return minIndvPremium;
	}
	public void setMinIndvPremium(Float minIndvPremium) {
		this.minIndvPremium = minIndvPremium;
	}
	public Float getMaxIndvPremium() {
		return maxIndvPremium;
	}
	public void setMaxIndvPremium(Float maxIndvPremium) {
		this.maxIndvPremium = maxIndvPremium;
	}
	public Float getMinDependentPremium() {
		return minDependentPremium;
	}
	public void setMinDependentPremium(Float minDependentPremium) {
		this.minDependentPremium = minDependentPremium;
	}
	public Float getMaxDependentPremium() {
		return maxDependentPremium;
	}
	public void setMaxDependentPremium(Float maxDependentPremium) {
		this.maxDependentPremium = maxDependentPremium;
	}
	public Float getMinMonthlyEmployerCost() {
		return minMonthlyEmployerCost;
	}
	public void setMinMonthlyEmployerCost(Float minMonthlyEmployerCost) {
		this.minMonthlyEmployerCost = minMonthlyEmployerCost;
	}
	public Float getMaxMonthlyEmployerCost() {
		return maxMonthlyEmployerCost;
	}
	public void setMaxMonthlyEmployerCost(Float maxMonthlyEmployerCost) {
		this.maxMonthlyEmployerCost = maxMonthlyEmployerCost;
	}
	public Float getMinMonthlyEmployeeCost() {
		return minMonthlyEmployeeCost;
	}
	public void setMinMonthlyEmployeeCost(Float minMonthlyEmployeeCost) {
		this.minMonthlyEmployeeCost = minMonthlyEmployeeCost;
	}
	public Float getMaxMonthlyEmployeeCost() {
		return maxMonthlyEmployeeCost;
	}
	public void setMaxMonthlyEmployeeCost(Float maxMonthlyEmployeeCost) {
		this.maxMonthlyEmployeeCost = maxMonthlyEmployeeCost;
	}
	public List<Integer> getNotAvailabilityList() {
		return notAvailabilityList;
	}
	public void setNotAvailabilityList(List<Integer> notAvailabilityList) {
		this.notAvailabilityList = notAvailabilityList;
	}
	public Set<Integer> getEmpIds() {
		return empIds;
	}
	public void setEmpIds(Set<Integer> empIds) {
		this.empIds = empIds;
	}
	
	public void updatePremium(Float totalPremium,Float indvPremium){
		if(totalPremium != null){
			if((minIndvPremium + minDependentPremium) == 0f || (totalPremium < (minIndvPremium + minDependentPremium))){
				minIndvPremium =  indvPremium;
				minDependentPremium = totalPremium - indvPremium;
			 }
			 if((maxIndvPremium + maxDependentPremium) == 0f || (totalPremium > (maxIndvPremium + maxDependentPremium))){
				maxIndvPremium =  indvPremium;
				maxDependentPremium = totalPremium - indvPremium;
			 }
		}
	}
	
}
