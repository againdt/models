package com.getinsured.hix.dto.plandisplay;

import java.io.Serializable;
import java.util.List;

public class ProviderSearchRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int page;
	String type;
	float radius;
	int per_page;
	String zip_code;	
	String search_term;
	List<String> hios_ids;
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPer_page() {
		return per_page;
	}
	public void setPer_page(int per_page) {
		this.per_page = per_page;
	}
	public String getZip_code() {
		return zip_code;
	}
	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}
	public String getSearch_term() {
		return search_term;
	}
	public void setSearch_term(String search_term) {
		this.search_term = search_term;
	}
	public List<String> getHios_ids() {
		return hios_ids;
	}
	public void setHios_ids(List<String> hios_ids) {
		this.hios_ids = hios_ids;
	}	
	
}
