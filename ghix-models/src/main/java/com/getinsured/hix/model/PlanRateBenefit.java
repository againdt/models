package com.getinsured.hix.model;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.PrescriptionSearchDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;

public class PlanRateBenefit {

	private int id;
	private String name;
	private String level;
	private Float premium;
	private String issuer;
	private String issuerText;
	private String networkType;
	private String ehbCovered;
	private Map <String, String> issuerQualityRating;
	private Map<String, Map<String, String>> planBenefits;
	private Map<String, Map<String, String>> planBaseBenefits;
	private Map<String, Map<String, String>> planCosts;
	private Map<String, Map<String, String>> planBaseCosts;
	private Map<String, Map<String, String>> optionalDeductible;
	private List<Map<String, String>> planDetailsByMember;
	private List<Map<String, String>> providers;
	private String sbcDocUrl;	
	private String planBrochureUrl;
	private String providerLink;
	private String issuerLogo;
	private String disclaimer;
	private Integer issuerId;
	private String maxCoinseForSpecialtyDrugs; 
	private String maxNumDaysForChargingInpatientCopay ; 
	private String primaryCareCostSharingAfterSetNumberVisits; 
	private String primaryCareDeductOrCoinsAfterSetNumberCopays; 
	private String drugName;
	private String drugTier;
	private String drugPrice;
	private Map<String, String> fairCost;
	private String tpId;	
	private String isPuf;
	private String formularyUrl;
	private String hsa;
	/*	New Fields added in SHARED-MODEL */
	private String coinsurance;
	private String policyLength;
	private String oopMax;
	private String guaranteedVsEstimatedRate;
	private String policyLengthUnit;
	private String policyLimit;
	private String outOfNetwkCoverage;
	private String outOfCountyCoverage;
	private String separateDrugDeductible;
	private String oopMaxAttr;
	private String oopMaxFamily;
	private String hiosPlanNumber;
	private String exclusionsAndLimitations;
	private int unifiedPlanRateChangeId;
	private String ehbPercentage;
	private String networkKey;
	private String tier2util;
	private String eocDocUrl;
	private String costSharing;
	private List<PrescriptionSearchDTO> prescriptionSearchDTOList;
	private String networkTransparencyRating;
	private String exchangeType;
	private SbcScenarioDTO sbcScenarioDTO;
	private String networkName;
	
	/**
	 * @return the prescriptionSearchDTOList
	 */
	public List<PrescriptionSearchDTO> getPrescriptionSearchDTOList() {
		return prescriptionSearchDTOList;
	}

	/**
	 * @param prescriptionSearchDTOList the prescriptionSearchDTOList to set
	 */
	public void setPrescriptionSearchDTOList(
			List<PrescriptionSearchDTO> prescriptionSearchDTOList) {
		this.prescriptionSearchDTOList = prescriptionSearchDTOList;
	}

	/**
	 * @return the tier2util
	 */
	public String getTier2util() {
		return tier2util;
	}

	/**
	 * @param tier2util the tier2util to set
	 */
	public void setTier2util(String tier2util) {
		this.tier2util = tier2util;
	}

	public String getNetworkKey() {
		return networkKey;
	}

	public void setNetworkKey(String networkKey) {
		this.networkKey = networkKey;
	}

	public PlanRateBenefit(){
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerText() {
		return issuerText;
	}

	public void setIssuerText(String issuerText) {
		this.issuerText = issuerText;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public Map<String, Map<String, String>> getPlanBenefits() {
		return planBenefits;
	}

	public void setPlanBenefits(
			Map<String, Map<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}

	public Map<String, Map<String, String>> getPlanBaseBenefits() {
		return planBaseBenefits;
	}

	public void setPlanBaseBenefits(Map<String, Map<String, String>> planBaseBenefits) {
		this.planBaseBenefits = planBaseBenefits;
	}

	public String getEhbCovered() {
		return ehbCovered;
	}

	public void setEhbCovered(String ehbCovered) {
		this.ehbCovered = ehbCovered;
	}

	public Map <String, String> getIssuerQualityRating() {
		return issuerQualityRating;
	}

	public void setIssuerQualityRating(Map <String, String> issuerQualityRating) {
		this.issuerQualityRating = issuerQualityRating;
	}

	public List<Map<String, String>> getPlanDetailsByMember() {
		return planDetailsByMember;
	}

	public void setPlanDetailsByMember(List<Map<String, String>> planDetailsByMember) {
		this.planDetailsByMember = planDetailsByMember;
	}
	public List<Map<String, String>> getProviders() {
		return providers;
	}

	public void setProviders(List<Map<String, String>> providers) {
		this.providers = providers;
	}

	public String getPlanBrochureUrl() {
		return planBrochureUrl;
	}

	public void setPlanBrochureUrl(String planBrochureUrl) {
		this.planBrochureUrl = planBrochureUrl;
	}

	@Override
	public String toString() {
		return "Plan [id=" + id + ", name=" + name + ", level=" + level
				+ ", premium=" + premium + ", issuer=" + issuer
				+ ", issuerText=" + issuerText + ", networkType=" + networkType
				+ ", planBenefits=" + planBenefits + "]";
	}

	public Map<String, Map<String, String>> getPlanCosts() {
		return planCosts;
	}

	public void setPlanCosts(Map<String, Map<String, String>> planCosts) {
		this.planCosts = planCosts;
	}
	
	public Map<String, Map<String, String>> getPlanBaseCosts() {
		return planBaseCosts;
	}
	
	public void setPlanBaseCosts(Map<String, Map<String, String>> planCosts) {
		this.planBaseCosts = planCosts;
	}

	public String getProviderLink() {
		return providerLink;
	}

	public void setProviderLink(String providerLink) {
		this.providerLink = providerLink;
	}

	public String getIssuerLogo() {
		return issuerLogo;
	}

	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public String getMaxCoinseForSpecialtyDrugs() {
		return maxCoinseForSpecialtyDrugs;
	}

	public void setMaxCoinseForSpecialtyDrugs(String maxCoinseForSpecialtyDrugs) {
		this.maxCoinseForSpecialtyDrugs = maxCoinseForSpecialtyDrugs;
	}

	public String getMaxNumDaysForChargingInpatientCopay() {
		return maxNumDaysForChargingInpatientCopay;
	}

	public void setMaxNumDaysForChargingInpatientCopay(
			String maxNumDaysForChargingInpatientCopay) {
		this.maxNumDaysForChargingInpatientCopay = maxNumDaysForChargingInpatientCopay;
	}

	public String getPrimaryCareCostSharingAfterSetNumberVisits() {
		return primaryCareCostSharingAfterSetNumberVisits;
	}

	public void setPrimaryCareCostSharingAfterSetNumberVisits(
			String primaryCareCostSharingAfterSetNumberVisits) {
		this.primaryCareCostSharingAfterSetNumberVisits = primaryCareCostSharingAfterSetNumberVisits;
	}

	public String getPrimaryCareDeductOrCoinsAfterSetNumberCopays() {
		return primaryCareDeductOrCoinsAfterSetNumberCopays;
	}

	public void setPrimaryCareDeductOrCoinsAfterSetNumberCopays(
			String primaryCareDeductOrCoinsAfterSetNumberCopays) {
		this.primaryCareDeductOrCoinsAfterSetNumberCopays = primaryCareDeductOrCoinsAfterSetNumberCopays;
	}

	/**
	 * @return the coinsurance
	 */
	public String getCoinsurance() {
		return coinsurance;
	}

	/**
	 * @param coinsurance the coinsurance to set
	 */
	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}

	/**
	 * @return the policyLength
	 */
	public String getPolicyLength() {
		return policyLength;
	}

	/**
	 * @param policyLength the policyLength to set
	 */
	public void setPolicyLength(String policyLength) {
		this.policyLength = policyLength;
	}

	/**
	 * @return the oopMax
	 */
	public String getOopMax() {
		return oopMax;
	}

	/**
	 * @param oopMax the oopMax to set
	 */
	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}

	/**
	 * @return the guaranteedVsEstimatedRate
	 */
	public String getGuaranteedVsEstimatedRate() {
		return guaranteedVsEstimatedRate;
	}

	/**
	 * @param guaranteedVsEstimatedRate the guaranteedVsEstimatedRate to set
	 */
	public void setGuaranteedVsEstimatedRate(String guaranteedVsEstimatedRate) {
		this.guaranteedVsEstimatedRate = guaranteedVsEstimatedRate;
	}

	/**
	 * @return the hsa
	 */
	public String getHsa() {
		return hsa;
	}

	/**
	 * @param hsa the hsa to set
	 */
	public void setHsa(String hsa) {
		this.hsa = hsa;
	}

	/**
	 * @return the formularyUrl
	 */
	public String getFormularyUrl() {
		return formularyUrl;
	}

	/**
	 * @param formularyUrl the formularyUrl to set
	 */
	public void setFormularyUrl(String formularyUrl) {
		this.formularyUrl = formularyUrl;
	}

	/**
	 * @return the fairCost
	 */
	public Map<String, String> getFairCost() {
		return fairCost;
	}

	/**
	 * @param fairCost the fairCost to set
	 */
	public void setFairCost(Map<String, String> fairCost) {
		this.fairCost = fairCost;
	}

	/**
	 * @return the drugTier
	 */
	public String getDrugTier() {
		return drugTier;
	}

	/**
	 * @param drugTier the drugTier to set
	 */
	public void setDrugTier(String drugTier) {
		this.drugTier = drugTier;
	}

	/**
	 * @return the drugPrice
	 */
	public String getDrugPrice() {
		return drugPrice;
	}

	/**
	 * @param drugPrice the drugPrice to set
	 */
	public void setDrugPrice(String drugPrice) {
		this.drugPrice = drugPrice;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getSbcDocUrl() {
		return sbcDocUrl;
	}

	public void setSbcDocUrl(String sbcDocUrl) {
		this.sbcDocUrl = sbcDocUrl;
	}
		
	public String getTpId() {
		return tpId;
	}

	public void setTpId(String tpId) {
		this.tpId = tpId;
	}

	public String getIsPuf() {
		return isPuf;
	}

	public void setIsPuf(String isPuf) {
		this.isPuf = isPuf;
	}	
	
	public String getPolicyLengthUnit() {
		return policyLengthUnit;
	}
	public void setPolicyLengthUnit(String policyLengthUnit) {
		this.policyLengthUnit = policyLengthUnit;
	}
	public String getPolicyLimit() {
		return policyLimit;
	}
	public void setPolicyLimit(String policyLimit) {
		this.policyLimit = policyLimit;
	}
	public String getOutOfNetwkCoverage() {
		return outOfNetwkCoverage;
	}
	public void setOutOfNetwkCoverage(String outOfNetwkCoverage) {
		this.outOfNetwkCoverage = outOfNetwkCoverage;
	}
	public String getOutOfCountyCoverage() {
		return outOfCountyCoverage;
	}
	public void setOutOfCountyCoverage(String outOfCountyCoverage) {
		this.outOfCountyCoverage = outOfCountyCoverage;
	}

	/**
	 * @return the optionalDeductible
	 */
	public Map<String, Map<String, String>> getOptionalDeductible() {
		return optionalDeductible;
	}

	/**
	 * @param optionalDeductible the optionalDeductible to set
	 */
	public void setOptionalDeductible(
			Map<String, Map<String, String>> optionalDeductible) {
		this.optionalDeductible = optionalDeductible;
	}

	public String getSeparateDrugDeductible() {
		return separateDrugDeductible;
	}

	public void setSeparateDrugDeductible(String separateDrugDeductible) {
		this.separateDrugDeductible = separateDrugDeductible;
	}

	public String getOopMaxAttr() {
		return oopMaxAttr;
	}

	public void setOopMaxAttr(String oopMaxAttr) {
		this.oopMaxAttr = oopMaxAttr;
	}

	public String getOopMaxFamily() {
		return oopMaxFamily;
	}

	public void setOopMaxFamily(String oopMaxFamily) {
		this.oopMaxFamily = oopMaxFamily;
	}

	public String getHiosPlanNumber() {
		return hiosPlanNumber;
	}

	public void setHiosPlanNumber(String hiosPlanNumber) {
		this.hiosPlanNumber = hiosPlanNumber;
	}

	public String getExclusionsAndLimitations() {
		return exclusionsAndLimitations;
	}

	public void setExclusionsAndLimitations(String exclusionsAndLimitations) {
		this.exclusionsAndLimitations = exclusionsAndLimitations;
	}

	public int getUnifiedPlanRateChangeId() {
		return unifiedPlanRateChangeId;
	}

	public void setUnifiedPlanRateChangeId(int unifiedPlanRateChangeId) {
		this.unifiedPlanRateChangeId = unifiedPlanRateChangeId;
	}

	public String getEhbPercentage() {
		return ehbPercentage;
	}

	public void setEhbPercentage(String ehbPercentage) {
		this.ehbPercentage = ehbPercentage;
	}

	public String getEocDocUrl() {
		return eocDocUrl;
	}

	public void setEocDocUrl(String eocDocUrl) {
		this.eocDocUrl = eocDocUrl;
	}

	public String getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}

	/**
	 * @return the networkTransparencyRating
	 */
	public String getNetworkTransparencyRating() {
		return networkTransparencyRating;
	}

	/**
	 * @param networkTransparencyRating the networkTransparencyRating to set
	 */
	public void setNetworkTransparencyRating(String networkTransparencyRating) {
		this.networkTransparencyRating = networkTransparencyRating;
	}

	/**
	 * @return the exchangeType
	 */
	public String getExchangeType() {
		return exchangeType;
	}

	/**
	 * @param exchangeType the exchangeType to set
	 */
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public SbcScenarioDTO getSbcScenarioDTO() {
		return sbcScenarioDTO;
	}

	public void setSbcScenarioDTO(SbcScenarioDTO sbcScenarioDTO) {
		this.sbcScenarioDTO = sbcScenarioDTO;
	}

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	
	
	
}
