package com.getinsured.hix.model.plandisplay;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.hix.model.GHIXResponse;

public class PldOrderResponse extends GHIXResponse{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4254859600458728302L;
	private List<PldPlanData> planDataList;
	private Map<String, String> responsiblePerson;
	private Map<String, String> houseHoldContact;
	private String householdCaseId;
	private String sadpFlag;
	private String employerId;
	private Character enrollmentType;
	private String userRoleId;
	private String userRoleType;
	private boolean isSubsidized; 
	private String quotingCounty;
	private String orderId;
	private String shoppingId;
	private String householdId;
	private String employeeId;
	private List<PldMemberData> disenrollMemberInfoList;
	private String planId;
	private Float premium;
	private String orderItemId;
	private String giHouseholdId;
	private Float planPremium;
	private String serc;
	private Long applicationId;
	private String terminationFlag;
	private String DentalTerminationFlag;
	private Character autoRenewal;
	private String requestType;
	private Long ssapApplicationId;
	private Float ehbAmount=null;
	private String nestedStackTrace;
	private String pdHouseholdId;
	private List<String> ind19Disenrolledmembers;
	private String globalId;
	//private Date financialEffectiveDate;
	private Integer giWsPayloadId;
	private String applicationType;
	private Set<String> plans;
	private String marketType;
	private String allowChangePlan;
	
	public String getPdHouseholdId() {
		return pdHouseholdId;
	}
	public void setPdHouseholdId(String pdHouseholdId) {
		this.pdHouseholdId = pdHouseholdId;
	}
	public String getSerc() {
		return serc;
	}
	public void setSerc(String serc) {
		this.serc = serc;
	}
	public Float getPlanPremium() {
		return planPremium;
	}
	public void setPlanPremium(Float planPremium) {
		this.planPremium = planPremium;
	}
	
	private List<Map<String, String>> nonEligiblePersonNameList;
	//added for creating plan url
	//Atul: can you please set it as string the same way as expected in url
	private String orderDate;
	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getQuotingCounty() {
		return quotingCounty;
	}
	public void setQuotingCounty(String quotingCounty) {
		this.quotingCounty = quotingCounty;
	}
	public List<PldPlanData> getPlanDataList() {
		return planDataList;
	}
	public void setPlanDataList(List<PldPlanData> planDataList) {
		this.planDataList = planDataList;
	}
	public Map<String, String> getResponsiblePerson() {
		return responsiblePerson;
	}
	public void setResponsiblePerson(Map<String, String> responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}
	public Map<String, String> getHouseHoldContact() {
		return houseHoldContact;
	}
	public void setHouseHoldContact(Map<String, String> houseHoldContact) {
		this.houseHoldContact = houseHoldContact;
	}
	public String getHouseholdCaseId() {
		return householdCaseId;
	}
	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}
	public String getSadpFlag() {
		return sadpFlag;
	}
	public void setSadpFlag(String sadpFlag) {
		this.sadpFlag = sadpFlag;
	}

	public Character getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(Character enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	public String getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getUserRoleType() {
		return userRoleType;
	}
	public void setUserRoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}
	public String getEmployerId() {
		return employerId;
	}
	public void setEmployerId(String employerId) {
		this.employerId = employerId;
	}
	
	public boolean isSubsidized() {
		return isSubsidized;
	}
	public void setSubsidized(boolean isSubsidized) {
		this.isSubsidized = isSubsidized;
	}

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getShoppingId() {
		return shoppingId; 
	}
	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public List<PldMemberData> getDisenrollMemberInfoList() {
		return disenrollMemberInfoList;
	}
	public void setDisenrollMemberInfoList(
			List<PldMemberData> disenrollMemberInfoList) {
		this.disenrollMemberInfoList = disenrollMemberInfoList;
	}
	
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getOrderItemId() {
		return orderItemId;
	}
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}	
	
	public String getGiHouseholdId() {
		return giHouseholdId;
	}
	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}
	public List<Map<String, String>> getNonEligiblePersonNameList() {
		return nonEligiblePersonNameList;
	}
	public void setNonEligiblePersonNameList(
			List<Map<String, String>> nonEligiblePersonNameList) {
		this.nonEligiblePersonNameList = nonEligiblePersonNameList;
	}
	public String getTerminationFlag() {
		return terminationFlag;
	}
	public void setTerminationFlag(String terminationFlag) {
		this.terminationFlag = terminationFlag;
	}
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	public Character getAutoRenewal() {
		return autoRenewal;
	}
	public void setAutoRenewal(Character autoRenewal) {
		this.autoRenewal = autoRenewal;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public Float getEhbAmount() {
		return ehbAmount;
	}
	public void setEhbAmount(Float ehbAmount) {
		this.ehbAmount = ehbAmount;
	}
	public String getNestedStackTrace() {
		return nestedStackTrace;
	}
	public void setNestedStackTrace(String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}
	
	public static Comparator<PldOrderResponse> PldOrderResponseComparatorByPlanId = new Comparator<PldOrderResponse>(){

		@Override
		public int compare(PldOrderResponse pldObj1, PldOrderResponse pldObj2) {
			
			return pldObj1.getPlanId().compareTo(pldObj2.getPlanId());
			
		}
		
	};

	public String getDentalTerminationFlag() {
		return DentalTerminationFlag;
	}
	public void setDentalTerminationFlag(String dentalTerminationFlag) {
		DentalTerminationFlag = dentalTerminationFlag;
	}
	public List<String> getInd19Disenrolledmembers() {
		return ind19Disenrolledmembers;
	}
	public void setInd19Disenrolledmembers(List<String> ind19Disenrolledmembers) {
		this.ind19Disenrolledmembers = ind19Disenrolledmembers;
	}
	public String getGlobalId() {
		return globalId;
	}
	public void setGlobalId(String globalId) {
		this.globalId = globalId;
	}
	/*public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}
	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}*/
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}
	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public Set<String> getPlans() {
		return plans;
	}
	public void setPlans(Set<String> plans) {
		this.plans = plans;
	}
	public String getMarketType() {
		return marketType;
	}
	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
	public String getAllowChangePlan() {
		return allowChangePlan;
	}
	public void setAllowChangePlan(String allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}
}
