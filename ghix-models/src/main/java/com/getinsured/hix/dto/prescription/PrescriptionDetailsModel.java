package com.getinsured.hix.dto.prescription;

import java.util.Comparator;

/**
 * Model class for Prescription details
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class PrescriptionDetailsModel {

	/**
	 * Definition of various terms in the file
	 * 

	RXCUI	RxNorm Unique identifier for concept (concept ID)
	LAT		Language of Term
	TS		Term status (no value provided)
	LUI		Unique identifier for term (no value provided)
	STT		String type (no value provided)
	SUI		Unique identifier for string (no value provided)
	ISPREF	Atom status - preferred (Y) or not (N) for this string within this concept (no value provided)
	RXAUI	Unique identifier for atom (RxNorm Atom ID)--- primary key to uniquely identify the record
	SAUI	Source asserted atom identifier [optional]
	SCUI	Source asserted concept identifier [optional]
	SDUI	Source asserted descriptor identifier [optional]
	SAB		Source abbreviation
	TTY		Term type in source
	CODE	"Most useful" source asserted identifier (if the source vocabulary has more than one identifier), or a RxNorm-generated source entry identifier (if the source vocabulary has none.)
	STR		String name of the drug. Renamed as drugName in the model class.
	SRL		Source Restriction Level (no value provided)
	SUPPRESS	Suppressible flag. Values = N, O, Y, or E. N - not suppressible. O - Specific individual names (atoms) set as Obsolete because the name is no Stringer provided by the original source. Y - Suppressed by RxNorm editor. E - unquantified, non-prescribable drug with related quantified, prescribable drugs. NLM strongly recommends that users not alter editor-assigned suppressibility.
	CVF			Content view flag. RxNorm includes one value, '4096', to denote inclusion in the Current Prescribable Content subset. All rows with CVF='4096' can be found in the subset.
	 **/

	private String rxcui;
	private String lat;
	private String ts;
	private String lui;
	private String stt;
	private String sui;
	private String ispref;
	private String rxaui;
	private String saui;
	private String scui;
	private String sdui;
	private String sab;
	private String tty;
	private String code;
	private String drugName; // str in the source file. renamed it drugName to make it more readable
	private String srl;
	private String suppress;
	private String cvf;

	public String getRxcui() {
		return rxcui;
	}

	public void setRxcui(String rxcui) {
		this.rxcui = rxcui;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getLui() {
		return lui;
	}

	public void setLui(String lui) {
		this.lui = lui;
	}

	public String getStt() {
		return stt;
	}

	public void setStt(String stt) {
		this.stt = stt;
	}

	public String getSui() {
		return sui;
	}

	public void setSui(String sui) {
		this.sui = sui;
	}

	public String getIspref() {
		return ispref;
	}

	public void setIspref(String ispref) {
		this.ispref = ispref;
	}

	public String getRxaui() {
		return rxaui;
	}

	public void setRxaui(String rxaui) {
		this.rxaui = rxaui;
	}

	public String getSaui() {
		return saui;
	}

	public void setSaui(String saui) {
		this.saui = saui;
	}

	public String getScui() {
		return scui;
	}

	public void setScui(String scui) {
		this.scui = scui;
	}

	public String getSdui() {
		return sdui;
	}

	public void setSdui(String sdui) {
		this.sdui = sdui;
	}

	public String getSab() {
		return sab;
	}

	public void setSab(String sab) {
		this.sab = sab;
	}

	public String getTty() {
		return tty;
	}

	public void setTty(String tty) {
		this.tty = tty;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getSrl() {
		return srl;
	}

	public void setSrl(String srl) {
		this.srl = srl;
	}

	public String getSuppress() {
		return suppress;
	}

	public void setSuppress(String suppress) {
		this.suppress = suppress;
	}

	public String getCvf() {
		return cvf;
	}

	public void setCvf(String cvf) {
		this.cvf = cvf;
	}	

	public static Comparator<PrescriptionDetailsModel> PrescriptionDrugNameComparator = new Comparator<PrescriptionDetailsModel>() {
		public int compare(PrescriptionDetailsModel o1, PrescriptionDetailsModel o2) {
			int obj1Length =  o1.getDrugName() == null ? 0 : o1.getDrugName().length();
			int obj2Length =  o2.getDrugName() == null ? 0 : o2.getDrugName().length();
			return obj1Length - obj2Length;
		}
	};




}
