
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Annual Policy information
 * 
 * <p>Java class for AnnualPolicyInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnnualPolicyInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AnnualPremiumAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AnnualPremiumSLCSPAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AnnualAdvancedPTCAmt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnualPolicyInformationType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "annualPremiumAmt",
    "annualPremiumSLCSPAmt",
    "annualAdvancedPTCAmt"
})
public class AnnualPolicyInformationType {

    @XmlElement(name = "AnnualPremiumAmt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String annualPremiumAmt;
    @XmlElement(name = "AnnualPremiumSLCSPAmt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String annualPremiumSLCSPAmt;
    @XmlElement(name = "AnnualAdvancedPTCAmt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String annualAdvancedPTCAmt;

    /**
     * Gets the value of the annualPremiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualPremiumAmt() {
        return annualPremiumAmt;
    }

    /**
     * Sets the value of the annualPremiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualPremiumAmt(String value) {
        this.annualPremiumAmt = value;
    }

    /**
     * Gets the value of the annualPremiumSLCSPAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualPremiumSLCSPAmt() {
        return annualPremiumSLCSPAmt;
    }

    /**
     * Sets the value of the annualPremiumSLCSPAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualPremiumSLCSPAmt(String value) {
        this.annualPremiumSLCSPAmt = value;
    }

    /**
     * Gets the value of the annualAdvancedPTCAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualAdvancedPTCAmt() {
        return annualAdvancedPTCAmt;
    }

    /**
     * Sets the value of the annualAdvancedPTCAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualAdvancedPTCAmt(String value) {
        this.annualAdvancedPTCAmt = value;
    }

}
