package com.getinsured.hix.dto.phix;

import java.io.Serializable;

/**
 * Holds a blog post
 * @author bhatt
 * @since 14 march 2014
 *
 */
public class GetInsuredBlog implements Serializable{
	
	private static final long serialVersionUID = 1L;
	String title;
	String url;
	String description;
	String imageUrl;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Override
	public String toString() {
		return "GetInsuredBlog [title=" + title + ", url=" + url
				+ ", description=" + description + ", imageUrl=" + imageUrl
				+ "]";
	}
}
