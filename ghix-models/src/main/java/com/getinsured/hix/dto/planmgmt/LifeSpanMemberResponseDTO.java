package com.getinsured.hix.dto.planmgmt;

public class LifeSpanMemberResponseDTO {

	private int id;
	private int age;
	private String daysActive;
	private String memberLevelPreContribution;
	private String ratingArea;
	
	/**
	 * @return the Id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param Id the Id to set
	 */
	public void setId(int Id) {
		this.id = Id;
	}
	
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the daysActive
	 */
	public String getDaysActive() {
		return daysActive;
	}
	/**
	 * @param daysActive the daysActive to set
	 */
	public void setDaysActive(String daysActive) {
		this.daysActive = daysActive;
	}
	/**
	 * @return the memberLevelPreContribution
	 */
	public String getMemberLevelPreContribution() {
		return memberLevelPreContribution;
	}
	/**
	 * @param memberLevelPreContribution the memberLevelPreContribution to set
	 */
	public void setMemberLevelPreContribution(String memberLevelPreContribution) {
		this.memberLevelPreContribution = memberLevelPreContribution;
	}
	/**
	 * @return the ratingArea
	 */
	public String getRatingArea() {
		return ratingArea;
	}
	/**
	 * @param ratingArea the ratingArea to set
	 */
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	
}
