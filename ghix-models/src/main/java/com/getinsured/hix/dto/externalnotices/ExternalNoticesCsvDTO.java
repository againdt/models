package com.getinsured.hix.dto.externalnotices;

public class ExternalNoticesCsvDTO {

	public static final String HEADER = "Notice ID, External Household Case ID, Status";
	
	private Integer noticeId;
	private String externalHouseholdCaseId;
	private String status;
	
	public Integer getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(Integer noticeId) {
		this.noticeId = noticeId;
	}
	public String getExternalHouseholdCaseId() {
		return externalHouseholdCaseId;
	}
	public void setExternalHouseholdCaseId(String externalHouseholdCaseId) {
		this.externalHouseholdCaseId = externalHouseholdCaseId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "ExternalNoticesCsvDTO [noticeId=" + noticeId + ", externalHouseholdCaseId=" + externalHouseholdCaseId
				+ ", status=" + status + "]";
	}
	
	public String toCsv(char separator) {
		String csv = String.valueOf(noticeId) + separator + externalHouseholdCaseId + separator + status;
		return csv.replaceAll("null", "");
	}

}
