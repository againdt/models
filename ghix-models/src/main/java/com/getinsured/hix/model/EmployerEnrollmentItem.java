package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;


/**
 * The persistent class for the employer plans database table.
 * 
 */
@Audited
@Entity
@Table(name = "employer_enrollment_items")
public class EmployerEnrollmentItem implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum PlanLevel {
		PLATINUM, GOLD, SILVER, BRONZE;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EmployerEnrollmentItem_Seq")
	@SequenceGenerator(name = "EmployerEnrollmentItem_Seq", sequenceName = "employer_enrollment_items_seq", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "employer_enrollment_id")
	private EmployerEnrollment employerEnrollment;

	@ManyToOne
	@JoinColumn(name = "BENCHMARK_PLAN_ID")
	private Plan plan;

	@Column(name = "PLAN_TIER", length = 20)
	@Enumerated(EnumType.STRING)
	private PlanLevel planTier;
	
	@Column(name = "premium")
	private Float premium;
	
	@Column(name = "avg_premium")
	private Float avgPremium;
	
	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;
	
	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
	@Column(name="issuer_plan_number")
    private String issuerPlanNumber;
	
	public EmployerEnrollmentItem() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployerEnrollment getEmployerEnrollment() {
		return employerEnrollment;
	}

	public void setEmployerEnrollment(EmployerEnrollment employerEnrollment) {
		this.employerEnrollment = employerEnrollment;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public PlanLevel getPlanTier() {
		return planTier;
	}

	public void setPlanTier(PlanLevel planTier) {
		this.planTier = planTier;
	}

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	public Float getAvgPremium() {
		return avgPremium;
	}

	public void setAvgPremium(Float avgPremium) {
		this.avgPremium = avgPremium;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}
}
