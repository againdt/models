package com.getinsured.hix.model.emx;

import java.io.Serializable;
import java.util.List;



public class EmxEmployeeRequest implements Serializable  {

	
	private static final long serialVersionUID = 1230951042501540394L;
	
	private String firstName;
	private String lastName;
	
	private String street;
	private String city;
	private String stateCode;
	
	private String householdId;
	private String householdPin;
	private String censusType;
	
	private double hraAmount; // Household Level
	private String hraTransitionDate;
	
	private String cobraEndDate;
	private double cobraPremium;
	
	private String qualifyingEventEndDate;
	
    private String zipCode;
	
	private String countyCode;
	
	private int familySize;
	
	private double hhIncome = -1.0;
	
	private long affiliateId;
	
	private long flowId;
	
	//private String affiliateHouseholdId;
	
	private String email;
	
	private String phone;
	

	private long leadId;
	private long employeeId;
	private int eventId;
	
	private String status;
	
	private String acceptedTermsAndConditions;
	
	
	private List<EmxEmployeeMember> members;

	private String employerId;
	private String employerName;
	private String encryptedEmployeeId;

	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getStateCode() {
		return stateCode;
	}


	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}


	/*public String getHouseholdId() {
		return householdId;
	}


	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
*/

	public String getHouseholdPin() {
		return householdPin;
	}


	public void setHouseholdPin(String householdPin) {
		this.householdPin = householdPin;
	}


	public String getCensusType() {
		return censusType;
	}


	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}


	public double getHraAmount() {
		return hraAmount;
	}


	public void setHraAmount(double hraAmount) {
		this.hraAmount = hraAmount;
	}


	public String getHraTransitionDate() {
		return hraTransitionDate;
	}


	public void setHraTransitionDate(String hraTransitionDate) {
		this.hraTransitionDate = hraTransitionDate;
	}


	public String getCobraEndDate() {
		return cobraEndDate;
	}


	public void setCobraEndDate(String cobraEndDate) {
		this.cobraEndDate = cobraEndDate;
	}


	public double getCobraPremium() {
		return cobraPremium;
	}


	public void setCobraPremium(double cobraPremium) {
		this.cobraPremium = cobraPremium;
	}


	public String getQualifyingEventEndDate() {
		return qualifyingEventEndDate;
	}


	public void setQualifyingEventEndDate(String qualifyingEventEndDate) {
		this.qualifyingEventEndDate = qualifyingEventEndDate;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public String getCountyCode() {
		return countyCode;
	}


	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}


	public int getFamilySize() {
		return familySize;
	}


	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}


	public double getHhIncome() {
		return hhIncome;
	}


	public void setHhIncome(double hhIncome) {
		this.hhIncome = hhIncome;
	}


	public long getAffiliateId() {
		return affiliateId;
	}


	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}


	public long getFlowId() {
		return flowId;
	}


	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}


	/*public String getAffiliateHouseholdId() {
		return affiliateHouseholdId;
	}


	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.affiliateHouseholdId = affiliateHouseholdId;
	}
*/

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	

	public long getLeadId() {
		return leadId;
	}


	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}


	public int getEventId() {
		return eventId;
	}


	public void setEventId(int eventId) {
		this.eventId = eventId;
	}


	public String getAcceptedTermsAndConditions() {
		return acceptedTermsAndConditions;
	}


	public void setAcceptedTermsAndConditions(String acceptedTermsAndConditions) {
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
	}


	public List<EmxEmployeeMember> getMembers() {
		return members;
	}


	public void setMembers(List<EmxEmployeeMember> members) {
		this.members = members;
	}


	public long getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getHouseholdId() {
		return householdId;
	}


	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	 
	public void setEmployerId(String employerId) {
		this.employerId = employerId;
	}
	
	public String getEmployerId() {
		return employerId;
	}
	
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	
	public String getEmployerName() {
		return employerName;
	}
	
	public String getEncryptedEmployeeId() {
		return encryptedEmployeeId;
	}


	public void setEncryptedEmployeeId(String encryptedEmployeeId) {
		this.encryptedEmployeeId = encryptedEmployeeId;
	}
}
