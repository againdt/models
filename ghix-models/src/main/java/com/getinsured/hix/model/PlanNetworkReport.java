/**
 * 
 */
package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author shengole_s
 *
 */
@Entity
@Table(name="PM_PLAN_NETWORK_REPORT")
public class PlanNetworkReport {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plan_network_report_seq")
	@SequenceGenerator(name = "plan_network_report_seq", sequenceName = "PM_PLAN_NETWORK_REPORT_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="ECM_DOCUMENT_ID", length=50)
	private String ecmDocumentId;
	
	@Column(name="ECM_DOCUMENT_NAME", length=100)
	private String ecmDocumentName;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date creationTimestamp;
	
	@Column(name="VENDOR_DOCUMENT_NAME", length=100)
	private String vendorDocumentName;

	@Column(name="VENDOR_DOCUMENT_ID", length=50)
	private String vendorDocumentId;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the ecmDocumentId
	 */
	public String getEcmDocumentId() {
		return ecmDocumentId;
	}

	/**
	 * @param ecmDocumentId the ecmDocumentId to set
	 */
	public void setEcmDocumentId(String ecmDocumentId) {
		this.ecmDocumentId = ecmDocumentId;
	}

	/**
	 * @return the ecmDocumentName
	 */
	public String getEcmDocumentName() {
		return ecmDocumentName;
	}

	/**
	 * @param ecmDocumentName the ecmDocumentName to set
	 */
	public void setEcmDocumentName(String ecmDocumentName) {
		this.ecmDocumentName = ecmDocumentName;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	
	/* To AutoUpdate created date while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreationTimestamp(new TSDate());
	}

	/**
	 * @return the vendorDocumentName
	 */
	public String getVendorDocumentName() {
		return vendorDocumentName;
	}

	/**
	 * @param vendorDocumentName the vendorDocumentName to set
	 */
	public void setVendorDocumentName(String vendorDocumentName) {
		this.vendorDocumentName = vendorDocumentName;
	}

	/**
	 * @return the vendorDocumentId
	 */
	public String getVendorDocumentId() {
		return vendorDocumentId;
	}

	/**
	 * @param vendorDocumentId the vendorDocumentId to set
	 */
	public void setVendorDocumentId(String vendorDocumentId) {
		this.vendorDocumentId = vendorDocumentId;
	}
	
	
}
