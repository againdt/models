package com.getinsured.eligibility.ssap.medicaid;

import java.math.BigDecimal;

public class UpdatePremiumRequest {
	
	private Long applicationId;
	private BigDecimal aptcAmount;
	
	
	
	
	
	public BigDecimal getAptcAmount() {
		return aptcAmount;
	}
	public void setAptcAmount(BigDecimal aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	

}
