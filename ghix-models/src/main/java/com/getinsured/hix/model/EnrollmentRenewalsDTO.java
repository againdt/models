package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

public class EnrollmentRenewalsDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private Date benefitStartDate;
	private Date benefitEndDate;
	private Float premium;
	private String enrollmentStatus;
	private String policyId;
	private String productType;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getBenefitStartDate() {
		return benefitStartDate;
	}
	public void setBenefitStartDate(Date benefitStartDate) {
		this.benefitStartDate = benefitStartDate;
	}
	public Date getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
}
