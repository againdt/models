package com.getinsured.hix.dto.agency;

import java.util.List;

public class AgencyAssistantListDto {
	List<AgencyAssistantDetailsDto> agencyAssistants;
	private long count;

	public List<AgencyAssistantDetailsDto> getAgencyAssistants() {
		return agencyAssistants;
	}

	public void setAgencyAssistants(List<AgencyAssistantDetailsDto> agencyAssistants) {
		this.agencyAssistants = agencyAssistants;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
	
}
