package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Map;

public class DiscrepancyPremiumDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer hixEnrollmentId;
	private Integer monthNumber;
	private String monthName;
	private Integer year;
	private Map<String, Object> grossPremiumAmount;
	private Map<String, Object> aptcAmount;
	private Map<String, Object> netPremiumAmount;
	private Map<String, Object> csrAmount;
	private Map<String, Object> isFinancial;
	private Map<String, Object> ratingArea;
	
	
	public Integer getHixEnrollmentId() {
		return hixEnrollmentId;
	}
	public void setHixEnrollmentId(Integer hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}
	public Integer getMonthNumber() {
		return monthNumber;
	}
	public void setMonthNumber(Integer monthNumber) {
		this.monthNumber = monthNumber;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Map<String, Object> getGrossPremiumAmount() {
		return grossPremiumAmount;
	}
	public void setGrossPremiumAmount(Map<String, Object> grossPremiumAmount) {
		this.grossPremiumAmount = grossPremiumAmount;
	}
	public Map<String, Object> getAptcAmount() {
		return aptcAmount;
	}
	public void setAptcAmount(Map<String, Object> aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	public Map<String, Object> getNetPremiumAmount() {
		return netPremiumAmount;
	}
	public void setNetPremiumAmount(Map<String, Object> netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}
	public Map<String, Object> getCsrAmount() {
		return csrAmount;
	}
	public void setCsrAmount(Map<String, Object> csrAmount) {
		this.csrAmount = csrAmount;
	}
	public Map<String, Object> getIsFinancial() {
		return isFinancial;
	}
	public void setIsFinancial(Map<String, Object> isFinancial) {
		this.isFinancial = isFinancial;
	}
	public Map<String, Object> getRatingArea() {
		return ratingArea;
	}
	public void setRatingArea(Map<String, Object> ratingArea) {
		this.ratingArea = ratingArea;
	}
}
