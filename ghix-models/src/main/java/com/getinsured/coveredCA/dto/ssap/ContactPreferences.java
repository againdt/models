
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class ContactPreferences {

    
    @Expose
    private String preferredSpokenLanguage ="English";
    @Expose
    private String preferredContactMethod = "Mail";
    @Expose
    private String preferredWrittenLanguage = "English";
   
    

    

    public String getPreferredSpokenLanguage() {
        return preferredSpokenLanguage;
    }

    public void setPreferredSpokenLanguage(String preferredSpokenLanguage) {
        this.preferredSpokenLanguage = preferredSpokenLanguage;
    }

    public String getPreferredContactMethod() {
        return preferredContactMethod;
    }

    public void setPreferredContactMethod(String preferredContactMethod) {
        this.preferredContactMethod = preferredContactMethod;
    }

    public String getPreferredWrittenLanguage() {
        return preferredWrittenLanguage;
    }

    public void setPreferredWrittenLanguage(String preferredWrittenLanguage) {
        this.preferredWrittenLanguage = preferredWrittenLanguage;
    }

	

    

}
