-- HIX-97960
-- NOTES: UPSERT SCRIPT to insert/update the PUF_SLSP table with 2017 PUF data for the state NE .
-- This Script will insert/update the  PUF_SLSP data for 2017 with the latest data received from FFM state -NE.

INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68814','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68814','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69131','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69131','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69132','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69132','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69133','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69133','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69134','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69134','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69135','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69135','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69138','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69138','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69140','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69140','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69141','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69141','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69142','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69142','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69143','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69143','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69144','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69144','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69145','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69145','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69146','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69146','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69147','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69147','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69148','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69148','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69149','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69149','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69150','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69150','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68815','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68815','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68816','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68816','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68817','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68817','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68818','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68818','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68820','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68820','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68821','Blaine','31009',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68821','Blaine','31009',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68822','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68822','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68823','Garfield','31071',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68823','Garfield','31071',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68824','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68824','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68825','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68825','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69151','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69151','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69152','Hooker','31091',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69152','Hooker','31091',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69153','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69153','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69154','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69154','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69155','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69155','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69156','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69156','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69157','Blaine','31009',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69157','Blaine','31009',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69160','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69160','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69161','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69161','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69162','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69162','Cheyenne','31033',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68826','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68826','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68827','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68827','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68828','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68828','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68831','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68831','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68832','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68832','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68833','Blaine','31009',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68833','Blaine','31009',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68834','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68834','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68835','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68835','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69163','Logan','31113',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69163','Logan','31113',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69165','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69165','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69166','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69166','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69167','Mcpherson','31117',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69167','Mcpherson','31117',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69168','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69168','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69169','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69169','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69170','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69170','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69171','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69171','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69190','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69190','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69201','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69201','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69210','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69210','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69211','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69211','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69212','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69212','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68836','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68836','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68837','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68837','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68838','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68838','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68840','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68840','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68841','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68841','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68842','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68842','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68843','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68843','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68844','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68844','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68845','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68845','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68846','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68846','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68847','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68847','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68848','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68848','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68849','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68849','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68850','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68850','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68852','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68852','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69214','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69214','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69216','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69216','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69217','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69217','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69218','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69218','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69219','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69219','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69220','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69220','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69221','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69221','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69301','Box Butte','31013',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69301','Box Butte','31013',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69331','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69331','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69333','Grant','31075',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69333','Grant','31075',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69334','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69334','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69335','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69335','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69336','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69336','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69337','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69337','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69339','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69339','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68420','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68420','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68421','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68421','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68422','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68422','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68423','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68423','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68424','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68424','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68428','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68428','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68429','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68429','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68430','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68430','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68431','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68431','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68433','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68433','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68434','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68434','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68436','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68436','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68437','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68437','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68438','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68438','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68439','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68439','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68440','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68440','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68853','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68853','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68854','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68854','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68855','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68855','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68856','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68856','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68858','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68858','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68859','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68859','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68860','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68860','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68861','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68861','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69340','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69340','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69341','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69341','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69343','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69343','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69345','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69345','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69346','Sioux','31165',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69346','Sioux','31165',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69347','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69347','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69348','Box Butte','31013',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69348','Box Butte','31013',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69350','Grant','31075',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69350','Grant','31075',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69351','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69351','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69352','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69352','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69353','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69353','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68050','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68050','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68055','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68055','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68056','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68056','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68057','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68057','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68058','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68058','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68059','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68059','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68061','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68061','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68062','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68062','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68063','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68063','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68064','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68064','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68065','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68065','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68066','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68066','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68067','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68067','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68068','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68068','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68069','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68069','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68070','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68070','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68071','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68071','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68072','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68072','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68073','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68073','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68101','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68101','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68102','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68102','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68103','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68103','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68104','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68104','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68105','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68105','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68106','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68106','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68107','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68107','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68108','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68108','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68109','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68109','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68110','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68110','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68111','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68111','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68112','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68112','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68113','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68113','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68114','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68114','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68116','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68116','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68117','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68117','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68118','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68118','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68119','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68119','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68120','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68120','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68122','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68122','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68123','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68123','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68124','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68124','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68127','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68127','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68128','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68128','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68130','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68130','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68131','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68131','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68132','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68132','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68133','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68133','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68134','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68134','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68135','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68135','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68136','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68136','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68137','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68137','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68138','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68138','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68139','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68139','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68142','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68142','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68144','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68144','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68145','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68145','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68147','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68147','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68152','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68152','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68154','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68154','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68155','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68155','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68157','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68157','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68164','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68164','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68172','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68172','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68175','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68175','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68176','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68176','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68178','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68178','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68179','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68179','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68180','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68180','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68182','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68182','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68183','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68183','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68197','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68197','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68198','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68198','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68301','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68301','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68303','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68303','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68304','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68304','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68305','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68305','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68307','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68307','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68309','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68309','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69357','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69357','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69358','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69358','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69360','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69360','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69361','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69361','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69363','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69363','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69365','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69365','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69366','Grant','31075',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69366','Grant','31075',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69367','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69367','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68441','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68441','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68442','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68442','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68443','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68443','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68444','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68444','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68445','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68445','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68310','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68310','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68313','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68313','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68314','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68314','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68315','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68315','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68316','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68316','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68317','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68317','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68318','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68318','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68319','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68319','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68320','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68320','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68321','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68321','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68322','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68322','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68323','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68323','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68324','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68324','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68325','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68325','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68326','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68326','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68327','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68327','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68328','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68328','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68329','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68329','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68330','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68330','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68331','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68331','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68332','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68332','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68333','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68333','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68335','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68335','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68336','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68336','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68337','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68337','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68338','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68338','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68339','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68339','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68446','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68446','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68447','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68447','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68448','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68448','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68450','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68450','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68452','Clay','31035',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68452','Clay','31035',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68453','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68453','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68454','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68454','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68455','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68455','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68456','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68456','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68457','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68457','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68458','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68458','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68862','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68862','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68863','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68863','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68864','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68864','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68865','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68865','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68866','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68866','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68869','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68869','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68870','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68870','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68460','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68460','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68461','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68461','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68462','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68462','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68463','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68463','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68464','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68464','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68465','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68465','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68466','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68466','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68467','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68467','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68501','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68501','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68502','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68502','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68503','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68503','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68504','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68504','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68505','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68505','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68506','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68506','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68871','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68871','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68872','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68872','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68873','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68873','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68874','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68874','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68875','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68875','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68876','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68876','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68878','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68878','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68879','Loup','31115',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68879','Loup','31115',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68507','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68507','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68508','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68508','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68509','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68509','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68510','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68510','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68512','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68512','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68514','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68514','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68516','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68516','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68517','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68517','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68340','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68340','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68341','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68341','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68342','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68342','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68343','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68343','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68344','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68344','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68345','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68345','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68346','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68346','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68347','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68347','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68348','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68348','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68349','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68349','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68350','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68350','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68351','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68351','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68352','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68352','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68001','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68001','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68002','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68002','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68003','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68003','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68004','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68004','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68005','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68005','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68007','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68007','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68008','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68008','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68354','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68354','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68355','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68355','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68357','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68357','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68358','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68358','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68359','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68359','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68360','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68360','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68361','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68361','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68362','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68362','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68364','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68364','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68365','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68365','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68366','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68366','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68367','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68367','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68368','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68368','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68370','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68370','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68371','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68371','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68372','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68372','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68009','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68009','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68010','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68010','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68014','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68014','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68015','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68015','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68016','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68016','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68017','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68017','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68018','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68018','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68019','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68019','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68020','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68020','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68022','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68022','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68023','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68023','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68025','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68025','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68026','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68026','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68028','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68028','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68029','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68029','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68520','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68520','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68521','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68521','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68522','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68522','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68523','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68523','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68524','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68524','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68881','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68881','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68882','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68882','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68883','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68883','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68901','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68901','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68902','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68902','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68526','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68526','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68527','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68527','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68528','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68528','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68529','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68529','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68531','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68531','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68532','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68532','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68542','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68542','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68544','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68544','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68583','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68583','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68588','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68588','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68601','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68601','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68920','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68920','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68922','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68922','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68923','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68923','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68924','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68924','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68925','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68925','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68926','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68926','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68927','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68927','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68928','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68928','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68929','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68929','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68602','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68602','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68620','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68620','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68621','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68621','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68622','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68622','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68623','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68623','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68624','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68624','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68626','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68626','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68627','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68627','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68628','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68628','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68629','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68629','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68631','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68631','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68632','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68632','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68633','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68633','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68634','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68634','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68635','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68635','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68636','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68636','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68637','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68637','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68930','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68930','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68932','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68932','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68375','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68375','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68376','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68376','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68377','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68377','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68378','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68378','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68379','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68379','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68380','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68380','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68381','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68381','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68382','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68382','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68401','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68401','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68402','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68402','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68403','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68403','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68404','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68404','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68030','Dakota','31043',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68030','Dakota','31043',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68031','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68031','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68033','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68033','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68034','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68034','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68036','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68036','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68037','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68037','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68038','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68038','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68039','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68039','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68040','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68040','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68041','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68041','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68042','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68042','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68044','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68044','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68045','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68045','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68046','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68046','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68047','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68047','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68048','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68048','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68405','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68405','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68406','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68406','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68407','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68407','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68409','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68409','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68410','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68410','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68413','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68413','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68414','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68414','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68415','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68415','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68416','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68416','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68417','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68417','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68418','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68418','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68419','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68419','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68933','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68933','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68934','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68934','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68935','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68935','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68936','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68936','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68937','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68937','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68938','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68938','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68939','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68939','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68638','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68638','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68640','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68640','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68641','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68641','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68642','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68642','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68643','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68643','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68644','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68644','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68647','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68647','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68648','Saunders','31155',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68648','Saunders','31155',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68649','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68649','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68651','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68651','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68652','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68652','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68653','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68653','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68654','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68654','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68655','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68655','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68658','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68658','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68659','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68659','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68660','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68660','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68940','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68940','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68941','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68941','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68942','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68942','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68943','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68943','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68944','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68944','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68945','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68945','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68946','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68946','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68947','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68947','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68948','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68948','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68949','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68949','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68661','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68661','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68662','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68662','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68663','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68663','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68664','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68664','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68665','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68665','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68666','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68666','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68667','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68667','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68669','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68669','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68701','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68701','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68702','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68702','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68710','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68710','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68711','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68711','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68713','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68713','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68714','Rock','31149',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68714','Rock','31149',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68715','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68715','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68716','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68716','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68950','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68950','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68952','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68952','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68954','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68954','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68955','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68955','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68956','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68956','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68957','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68957','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68958','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68958','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68959','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68959','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68960','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68960','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68961','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68961','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68717','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68717','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68718','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68718','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68719','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68719','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68720','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68720','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68722','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68722','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68723','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68723','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68724','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68724','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68725','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68725','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68726','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68726','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68727','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68727','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68728','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68728','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68729','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68729','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68730','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68730','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68731','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68731','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68732','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68732','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68733','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68733','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68734','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68734','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68964','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68964','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68966','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68966','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68967','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68967','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68969','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68969','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68970','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68970','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68971','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68971','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68972','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68972','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68973','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68973','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68974','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68974','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68735','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68735','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68736','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68736','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68738','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68738','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68739','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68739','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68740','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68740','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68741','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68741','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68742','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68742','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68743','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68743','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68745','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68745','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68746','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68746','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68747','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68747','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68748','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68748','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68749','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68749','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68751','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68751','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68752','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68752','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68753','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68753','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68755','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68755','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68756','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68756','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68975','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68975','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68976','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68976','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68977','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68977','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68978','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68978','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68979','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68979','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68980','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68980','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68981','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68981','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68982','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68982','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69001','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69001','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68757','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68757','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68758','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68758','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68759','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68759','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68760','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68760','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68761','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68761','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68763','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68763','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68764','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68764','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68765','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68765','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68766','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68766','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68767','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68767','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68768','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68768','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68769','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68769','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69020','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69020','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69021','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69021','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69022','Furnas','31065',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69022','Furnas','31065',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69023','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69023','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69024','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69024','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69025','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69025','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69026','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69026','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69027','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69027','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69028','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69028','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69029','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69029','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69030','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69030','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69032','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69032','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69033','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69033','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69034','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69034','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69036','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69036','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69037','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69037','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69038','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69038','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69039','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69039','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68770','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68770','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68771','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68771','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68773','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68773','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68774','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68774','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68776','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68776','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68777','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68777','Boyd','31015',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68778','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68778','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68779','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68779','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68780','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68780','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68781','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68781','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68783','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68783','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68784','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68784','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68785','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68785','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68786','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68786','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69040','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69040','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69041','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69041','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69042','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69042','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69043','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69043','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69044','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69044','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69045','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69045','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69046','Furnas','31065',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69046','Furnas','31065',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69101','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69101','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69103','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69103','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69120','Custer','31041',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69120','Custer','31041',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69121','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69121','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69122','Deuel','31049',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69122','Deuel','31049',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69123','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69123','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69125','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69125','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69127','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69127','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69128','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69128','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69129','Deuel','31049',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69129','Deuel','31049',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69130','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69130','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68787','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68787','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68788','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68788','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68789','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68789','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68790','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68790','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68791','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68791','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68792','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68792','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68801','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68801','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68802','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68802','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68803','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68803','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68810','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68810','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68812','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68812','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68813','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68813','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68036','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68036','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68038','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68038','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68044','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68044','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68045','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68045','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68045','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68045','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68047','Wayne','31179',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68047','Wayne','31179',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68047','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68047','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68055','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68055','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68064','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68064','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68064','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68064','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68065','Seward','31159',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68065','Seward','31159',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68065','Lancaster','31109',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68065','Lancaster','31109',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68065','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68065','Butler','31023',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68069','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68069','Sarpy','31153',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68112','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68112','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68122','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68122','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68128','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68128','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68138','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68138','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68142','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68142','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68147','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68147','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68152','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68152','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68157','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68157','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68301','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68301','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68301','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68301','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68303','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68303','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68307','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68307','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68316','Polk','31143',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68316','Polk','31143',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68317','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68317','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68322','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68322','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68323','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68323','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68329','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68329','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68332','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68332','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68333','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68333','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68335','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68335','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68339','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68339','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68339','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68339','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68341','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68341','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68342','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68342','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68343','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68343','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68345','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68345','Richardson','31147',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68347','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68347','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68348','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68348','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68351','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68351','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68354','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68354','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68358','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68358','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68359','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68359','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68365','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68365','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68366','Saunders','31155',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68366','Saunders','31155',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68367','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68367','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68367','Polk','31143',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68367','Polk','31143',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68368','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68368','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68371','Hamilton','31081',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68371','Hamilton','31081',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68376','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68376','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68378','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68378','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68381','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68381','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68413','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68413','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68418','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68418','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68421','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68421','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68423','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68423','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68428','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68428','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68443','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68443','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68447','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68447','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68448','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68448','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68452','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68452','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68453','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68453','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68454','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68454','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68455','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68455','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68460','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68460','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68461','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68461','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68462','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68462','Cass','31025',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68464','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68464','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68465','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68465','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68465','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68465','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68601','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68601','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68601','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68601','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68601','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68601','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68623','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68623','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68628','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68628','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68628','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68628','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68629','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68629','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68631','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68631','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68633','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68633','Colfax','31037',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68633','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68633','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68640','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68640','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68640','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68640','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68641','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68641','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68641','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68641','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68642','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68642','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68643','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68643','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68643','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68643','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68644','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68644','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68652','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68652','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68654','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68654','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68654','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68654','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68660','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68660','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68662','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68662','Butler','31023',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68663','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68663','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68669','Seward','31159',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68669','Seward','31159',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68701','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68701','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68714','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68714','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68726','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68726','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68729','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68729','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68730','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68730','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68733','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68733','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68735','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68735','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68735','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68735','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68740','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68740','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68745','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68745','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68745','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68745','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68746','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68746','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68748','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68748','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68752','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68752','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68757','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68757','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68758','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68758','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68758','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68758','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68764','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68764','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68764','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68764','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68768','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68768','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68768','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68768','Cuming','31039',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68769','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68769','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68769','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68769','Knox','31107',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68770','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68770','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68771','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68771','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68771','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68771','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68777','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68777','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68781','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68781','Antelope','31003',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68781','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68781','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68781','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68781','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68784','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68784','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68785','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68785','Dakota','31043',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68786','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68786','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68786','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68786','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68791','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68791','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68801','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68801','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68813','Blaine','31009',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68813','Blaine','31009',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68815','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68815','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68815','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68815','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68817','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68817','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68818','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68818','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68823','Loup','31115',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68823','Loup','31115',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68823','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68823','Holt','31089',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68823','Rock','31149',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68823','Rock','31149',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68823','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68823','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68828','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68828','Valley','31175',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68832','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68832','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68834','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68834','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68835','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68835','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68836','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68836','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68836','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68836','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68840','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68840','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68843','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68843','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68845','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68845','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68856','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68856','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68859','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68859','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68859','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68859','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68860','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68860','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68863','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68863','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68864','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68864','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68864','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68864','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68866','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68866','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68869','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68869','Sherman','31163',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68872','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68872','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68876','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68876','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68878','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68878','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68882','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68882','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68882','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68882','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68901','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68901','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68901','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68901','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68922','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68922','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68924','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68924','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68927','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68927','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68928','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68928','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68930','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68930','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68932','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68932','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68932','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68932','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68932','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68932','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68934','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68934','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68935','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68935','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68936','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68936','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68936','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68936','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68937','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68937','Dawson','31047',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68941','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68941','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68944','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68944','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68947','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68947','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68948','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68948','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68949','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68949','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68956','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68956','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68956','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68956','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68957','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68957','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68960','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68960','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68964','Thayer','31169',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68964','Thayer','31169',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68967','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68967','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68974','Thayer','31169',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68974','Thayer','31169',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68977','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68977','Furnas','31065',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68979','Fillmore','31059',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68979','Fillmore','31059',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68979','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68979','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68980','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68980','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68980','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68980','Hamilton','31081',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68981','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68981','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68982','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68982','Harlan','31083',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68982','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68982','Phelps','31137',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68982','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68982','Franklin','31061',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69001','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69001','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69022','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69022','Red Willow','31145',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69022','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69022','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69024','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69024','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69025','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69025','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69028','Gosper','31073',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69028','Gosper','31073',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69028','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69028','Dawson','31047',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69029','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69029','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69029','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69029','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69030','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69030','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69034','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69034','Frontier','31063',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69038','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69038','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69038','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69038','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69039','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69039','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69040','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69040','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69045','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69045','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69045','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69045','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69045','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69045','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69120','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69120','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69122','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69122','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69122','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69122','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69129','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69129','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69134','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69134','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69134','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69134','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69138','Custer','31041',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69138','Custer','31041',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69138','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69138','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69145','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69145','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69146','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69146','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69147','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69147','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69148','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69148','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69149','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69149','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69153','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69153','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69155','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69155','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69156','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69156','Kimball','31105',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69156','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69156','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69157','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69157','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69157','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69157','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69163','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69163','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69163','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69163','Thomas','31171',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69165','Mcpherson','31117',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69165','Mcpherson','31117',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69165','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69165','Keith','31101',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69166','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69166','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69168','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69168','Chase','31029',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69169','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69169','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69169','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69169','Hayes','31085',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69217','Rock','31149',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69217','Rock','31149',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69301','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69301','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69333','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69333','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69334','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69334','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69335','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69335','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69336','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69336','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69341','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69341','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69343','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69343','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69347','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69347','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69348','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69348','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69350','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69350','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69350','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69350','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69357','Sioux','31165',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69357','Sioux','31165',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69358','Sioux','31165',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69358','Sioux','31165',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69354','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69354','Dawes','31045',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69355','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69355','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69356','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69356','Scotts Bluff','31157',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68335','Nuckolls','31129',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68335','Nuckolls','31129',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68376','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68376','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68379','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68379','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68405','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68405','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68416','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68416','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68423','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68423','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68424','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68424','Gage','31067',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68436','Clay','31035',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68436','Clay','31035',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68437','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68437','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68003','Lancaster','31109',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68003','Lancaster','31109',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68956','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68956','Kearney','31099',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68957','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68957','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68957','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68957','Clay','31035',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68967','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68967','Gosper','31073',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68972','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68972','Webster','31181',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68979','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68979','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68980','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68980','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69001','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69001','Hitchcock','31087',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69027','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69027','Dundy','31057',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69134','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69134','Lincoln','31111',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68882','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68882','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68883','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68883','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69149','Deuel','31049',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69149','Deuel','31049',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69214','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69214','Cherry','31031',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69340','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69340','Garden','31069',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68938','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68938','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68942','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68942','Nuckolls','31129',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68943','Thayer','31169',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68943','Thayer','31169',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68945','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68945','Adams','31001',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68441','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68441','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68442','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68442','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68447','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68447','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68448','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68448','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68453','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68453','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68453','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68453','Thayer','31169',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68458','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68458','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68458','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68458','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68461','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68461','Otoe','31131',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68528','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68528','Seward','31159',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68004','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68004','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68019','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68019','Dodge','31053',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68019','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68019','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68046','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68046','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68057','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68057','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68136','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68136','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68301','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68301','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68313','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68313','York','31185',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68314','Butler','31023',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68314','Butler','31023',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68319','Hamilton','31081',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68319','Hamilton','31081',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68324','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68324','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68329','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68329','Nemaha','31127',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68637','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68637','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68638','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68638','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68638','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68638','Merrick','31121',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68658','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68658','Polk','31143',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68663','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68663','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68663','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68663','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68665','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68665','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68666','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68666','York','31185',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68701','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68701','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68714','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68714','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68729','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68729','Pierce','31139',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68733','Thurston','31173',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68733','Thurston','31173',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68733','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68733','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68739','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68739','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68747','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68747','Cedar','31027',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68748','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68748','Platte','31141',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68755','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68755','Keya Paha','31103',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68767','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68767','Wayne','31179',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68855','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68855','Buffalo','31019',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68858','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68858','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68865','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68865','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68636','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68636','Wheeler','31183',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68714','Brown','31017',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68714','Brown','31017',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68874','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68874','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69022','Gosper','31073',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69022','Gosper','31073',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69101','Mcpherson','31117',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69101','Mcpherson','31117',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69127','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69127','Perkins','31135',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69135','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69135','Brown','31017',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69165','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69165','Arthur','31005',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69301','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69301','Morrill','31123',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69334','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69334','Banner','31007',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('69337','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('69337','Sheridan','31161',2017,'NE',506.28,498,433.16,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68779','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68779','Madison','31119',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68784','Thurston','31173',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68784','Thurston','31173',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68874','Loup','31115',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68874','Loup','31115',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68875','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68875','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68787','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68787','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68788','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68788','Dodge','31053',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68791','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68791','Stanton','31167',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68792','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68792','Dixon','31051',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68803','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68803','Howard','31093',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68823','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68823','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68831','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68831','Hall','31079',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68833','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68833','Custer','31041',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68833','Logan','31113',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68833','Logan','31113',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68626','Saunders','31155',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68626','Saunders','31155',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68627','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68627','Greeley','31077',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68627','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68627','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68628','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68628','Nance','31125',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68636','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68636','Boone','31011',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68637','Garfield','31071',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68637','Garfield','31071',2017,'NE',470.16,449.39,376.99,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68338','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68338','Saline','31151',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68341','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68341','Jefferson','31095',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68347','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68347','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68348','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68348','Pawnee','31133',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68357','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68357','Johnson','31097',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68359','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68359','Fillmore','31059',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68366','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68366','Lancaster','31109',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68367','Butler','31023',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68367','Butler','31023',2017,'NE',374.07,357.55,299.95,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68003','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68003','Cass','31025',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68004','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68004','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68007','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68007','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68014','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68014','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68017','Lancaster','31109',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68017','Lancaster','31109',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68020','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68020','Thurston','31173',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68025','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68025','Saunders','31155',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68028','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68028','Douglas','31055',2017,'NE',288.26,288.23,241.8,25,2,'FFM',405.07);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68029','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68029','Burt','31021',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68031','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68031','Cuming','31039',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);
INSERT INTO puf_slsp (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21)
VALUES('68031','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0)ON CONFLICT ON CONSTRAINT pk_puf_slsp 
DO UPDATE SET (zipcode, county,fips,year,state,slsp21,lsp21,lbp21,plan_count,carrier_count,exchange_type,lgp21) =('68031','Washington','31177',2017,'NE',357.11,341.33,286.34,11,2,'FFM',0);