package com.getinsured.hix.model.planmgmt;

import com.getinsured.hix.model.GHIXResponse;

public class HouseHoldResponse extends GHIXResponse{
	
	private double premium;

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

}
