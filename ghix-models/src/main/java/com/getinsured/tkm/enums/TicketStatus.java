package com.getinsured.tkm.enums;

	public enum TicketStatus {

		UnClaimed("UnClaimed", "Unclaimed"), Resolved("Resolved", "Resolved"), Open(
				"Open", "Open"), Canceled("Canceled", "Canceled"), 
				Reopened("Reopened", "Reopened"), Restarted("Restarted", "Restarted");

		private String ticketStatusCode;

		private String description;

		private TicketStatus(String ticketStatusCode, String description) {
			this.ticketStatusCode = ticketStatusCode;
			this.description = description;
		}

		public String getTicketStatusCode() {
			return ticketStatusCode;
		}

		public String getDescription() {
			return description;
		}
}

