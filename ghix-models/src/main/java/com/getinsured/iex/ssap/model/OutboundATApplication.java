package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.getinsured.timeshift.util.TSDate;

/**
 * The persistent class for the OUTBOUND_AT_APPLICATION database table.
 *
 */
@Entity
@Table(name = "OUTBOUND_AT_APPLICATION")
public class OutboundATApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "OUTBOUND_AT_APPLICATION_ID_GENERATOR", sequenceName = "OUTBOUND_AT_APPLICATION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OUTBOUND_AT_APPLICATION_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 22)
	private long id;

	@Column(name = "SSAP_APPLICATION_ID")
	private long ssapApplicationId;

	@Column(name = "CASE_NUMBER", length = 100)
	private String caseNumber;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "DATE_SENT")
	private Date dateSent;

	@Column(name = "STATUS", length = 100)
	private String status;

	@Column(name = "OUTBOUND_AT_PAYLOAD_ID")
	private Integer outboundATPayloadId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimestamp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Date getDateSent() {
		return dateSent;
	}

	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOutboundATPayload() {
		return outboundATPayloadId;
	}

	public void setOutboundATPayload(Integer outboundATPayload) {
		this.outboundATPayloadId = outboundATPayload;
	}

	public Date getCreatedDate() {
		return creationTimestamp;
	}

	public void setCreatedDate(Date createdDate) {
		this.creationTimestamp = createdDate;
	}

	public Date getUpdatedDate() {
		return lastUpdateTimestamp;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.lastUpdateTimestamp = updatedDate;
	}

	@PrePersist
	public void PrePersist() {
		this.setCreatedDate(new TSDate());
		this.setUpdatedDate(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedDate(new TSDate());
	}
}
