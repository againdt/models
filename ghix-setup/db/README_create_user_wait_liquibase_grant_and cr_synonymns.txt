
Preparation
1. Make sure DB_ENV.sh in create_user_dir and grant_and_cr_synonym_dir are matching to the current Oracle environment
2. Make an Oracle account named DEV_JT with DBA privileges exists.  It can be dropped after the procedure is done.

A Create database users and roles
1	Identify a Schema Prefix using the following pattern:

   GHIX[PROD |QA|DEV]_[DDDD] where DDDD is 0 to 9999

   For PROD
     GHIXPROD_[DDDD] where DDDD is 0 to 9999

   For QA
      GHIXQA_[DDDD] where DDDD is 0 to 9999

   For DEV
     GHIXDEV_[DDDD] where DDDD is 0 to 9999

   For this example, the prefix is: GHIXPROD_0

2	Create tablespace:
create tablespace <Schema Prefix>_DATA
ENCRYPTION USING '3DES168'
DEFAULT STORAGE (ENCRYPT);
		For this example, here is the command to create a new tablespace: 
create tablespace GHIXPROD_0_DATA
ENCRYPTION USING '3DES168'
DEFAULT STORAGE (ENCRYPT);

3	At this point, we have the following:
a.	Schema Prefix: GHIXPROD_0
b.	Tablespace: GHIXPROD_0_DATA

4	To create schema/users and roles
a.	cd /home/oracle/bk_setup/create_users_dir
b.	./cr_USERS_by_prefix.sh <Schema Prefix> <Tablespace Name>
The following will be created:
Schema/users:
<Schema Prefix>_OWNER
<Schema Prefix>_USER
<Schema Prefix>_READONLY

Roles:
	<Schema Prefix>_USER_ROLE
	<Schema Prefix>_USER_READONLY

For this example, here are schema/users and roles will be created:

cd /home/oracle/bk_setup/create_users_dir
./cr_USERS_by_prefix.sh GHIXPROD_0 GHIXPROD_0_DATA

The following will be created:
Schema/users:
GHIXPROD_0_OWNER, its password is: GHIXPROD_0_OWNER
GHIXPROD_0_USER, its password is: GHIXPROD_0_USER
GHIXPROD_0_READONLY, its password is: GHIXPROD_0_READONLY

Roles:
	GHIXPROD_0_USER_ROLE
	GHIXPROD_0_USER_READONLY

B.	Provide the schema/users and their passwords to the deployment team.
The deployment team will use them to create the database objects through .liquibase. procedure

C.	Wait for deployment team to complete the .liquibase. procedure, then 
grant DMLs on OWNER.s objects to USER/READONLY and create respective synonyms on USER/READONLY schema

1.	For USER, run the following command 
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym.sh <SCHEMA PREFIX>_USER <SCHEMA PREFIX>_OWNER

For this example, run the following command:
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym.sh GHIXPROD_0_USER GHIXPROD_0_OWNER

2.	For READONLY, run the following command
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym_readonly.sh <SCHEMA PREFIX>_READONLY <SCHEMA PREFIX>_OWNER

For this example, run the following command:
/home/oracle/bk_setup/grant_and_cr_synonym_dir/grantandcrsynonym_readonly.sh GHIXPROD_0_READONLY GHIXPROD_0_OWNER

