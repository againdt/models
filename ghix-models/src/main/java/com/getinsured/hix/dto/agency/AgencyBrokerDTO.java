package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

/**
 * Broker data transfer class.
 * 
 * @author kanthi_v
 * 
 */
public class AgencyBrokerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String encryptedId;
	private String firstName;
	private String lastName;
	private String status;
	private String licenseNumber;
	private String certificationStatus;
	private int consumersCount;

	public AgencyBrokerDTO() {	
	}

	public String getEncryptedId() {
		return encryptedId;
	}

	public void setEncryptedId(String encryptedId) {
		this.encryptedId = encryptedId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}


	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getConsumersCount() {
		return consumersCount;
	}

	public void setConsumersCount(int consumersCount) {
		this.consumersCount = consumersCount;
	}

	@Override
	public String toString() {
		return "AgencyBrokerDTO details: Id = "+encryptedId;
	}

}
