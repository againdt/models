package com.getinsured.hix.model.prescreen.calculator;

import com.getinsured.hix.model.estimator.mini.MiniEstimatorRequest.EnrollmentYear;
/**
 * This class encapsulates the request parameters that are required for FPL
 * calculation.
 * <p>
 * State, total family income and family size are encapsulated in addition to
 * the inherited fields from GHIXRequest.
 * </p>
 * 
 * @author Sunil Desu
 * @since April 11 2013
 */
public class FplRequest extends GHIXRequest {

	private String state;
	private double familyIncome;
	private int familySize;
	private EnrollmentYear enrollmentYear=EnrollmentYear.CURRENT;
	private long coverageYear;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getFamilyIncome() {
		return familyIncome;
	}

	public void setFamilyIncome(double familyIncome) {
		this.familyIncome = familyIncome;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}
	
	public EnrollmentYear getEnrollmentYear() {
		return enrollmentYear;
	}
	public void setEnrollmentYear(EnrollmentYear enrollmentYear) {
		this.enrollmentYear = enrollmentYear;
	}

	/**
	 * @return the coverageYear
	 */
	public long getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("FplRequest [State=");
		sb.append(state);
		sb.append(", Family Income=");
		sb.append(familyIncome);
		sb.append(", Family Size=");
		sb.append(familySize);
		sb.append(", Enrollment Year=");
		sb.append(enrollmentYear);
		sb.append(", Coverage Year=");
		sb.append(coverageYear);
		sb.append("]");

		return sb.toString();
	}

}
