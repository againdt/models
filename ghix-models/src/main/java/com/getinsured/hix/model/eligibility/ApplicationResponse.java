package com.getinsured.hix.model.eligibility;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

/**
 * This is the Application Response. Each response
 * contains the list of applicantInfo for requested applicationID
 * along with applicationID. 
 * 
 * @author jyotisree
 * @since 7-Feb-2013
 */
public class ApplicationResponse extends GHIXResponse  {

	List<ApplicantInfo> applicants = new ArrayList<ApplicantInfo>();
	long applicationID;
	
	private String costSharing = "CS1";
	private double aptc =30.0f;
	private String enrollmentType = "A"; //I/A/S

	
	public List<ApplicantInfo> getApplicants() {
		return applicants;
	}

	public void setApplicants(List<ApplicantInfo> applicants) {
		this.applicants = applicants;
	}

	public long getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(long applicationID) {
		this.applicationID = applicationID;
	}
	
	public String getCostSharing() {
		return costSharing;
	}
	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}
	public Double getAptc() {
		return aptc;
	}
	public void setAptc(Double aptc) {
		this.aptc = aptc;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	
}
