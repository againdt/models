/**
 * 
 * @author santanu
 * @version 1.0
 * @since July 24, 2014 
 * This DTO will handle all cross sell response object 
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import com.getinsured.hix.model.GHIXResponse;

@Component("CrossSellPlanResponseDTO")
public class CrossSellPlanResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private String lowestPremium;
	
	private List<CrossSellPlan> crossSellPlanList;
	
	private String insuranceType;
	
	/**
	 * Constructor.
	 */
	public CrossSellPlanResponseDTO() {
		this.startResponse();
	}
	
	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse(){
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON. 
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse(){
		String transformedResponse = null;
		
		//this.endResponse();
		
		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
		    public HierarchicalStreamWriter createWriter(Writer writer) {
		        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		    }
		});
		
		Validate.notNull(jsoner, "XStream instance improperly confugured.");
		
		transformedResponse = jsoner.toXML(this);
		
		return transformedResponse;
	}*/

	
	/**
	 * @return the lowestPremium
	 */
	public String getLowestPremium() {
		return lowestPremium;
	}

	/**
	 * @param lowestPremium the lowestPremium to set
	 */
	public void setLowestPremium(String lowestPremium) {
		this.lowestPremium = lowestPremium;
	}

	public List<CrossSellPlan> getCrossSellPlanList() {
		return crossSellPlanList;
	}

	public void setCrossSellPlanList(List<CrossSellPlan> crossSellPlanList) {
		this.crossSellPlanList = crossSellPlanList;
	}
	
	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	
}
