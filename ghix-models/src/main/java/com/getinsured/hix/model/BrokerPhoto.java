package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * 
 * class for the Broker photo.
 * 
 * 
 */

public class BrokerPhoto implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;

	private byte[] photo;
	private Integer imageDocumentId;
	private String mimeType;

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public Integer getImageDocumentId() {
		return imageDocumentId;
	}

	public void setImageDocumentId(Integer imageDocumentId) {
		this.imageDocumentId = imageDocumentId;
	}

	public BrokerPhoto() {

	}

	public int getId() {

		return this.id;

	}

	public void setId(int id) {

		this.id = id;

	}

	public byte[] getPhoto() {

		return photo;

	}

	public void setPhoto(byte[] photoGiven) {

		this.photo = photoGiven.clone();

	}

	@Override
	public String toString() {

		return "Broker Photo Details: brokerId = " + id;

	}

}
