update
	enrollment_premium as epr set
		max_aptc = s.max_aptc
	from
		(
			select ep.id as id,
			pd.max_subsidy as max_subsidy,
			e.ehb_amt as ehb_amt,
			least(pd.max_subsidy,
			e.ehb_amt) as max_aptc
		from
			enrollment e
		join enrollment_premium ep on
			ep.enrollment_id = e.id
		join pd_household pd on
			e.pd_household_id = pd.id
		where
			ep.gross_prem_amt is not null
			and ep.net_prem_amt is not null
			and ep.aptc_amt is not null
			and ep.max_aptc is null
			and e.ehb_amt is not null
			and pd.max_subsidy is not null
			and ep.month between extract(month from e.benefit_effective_date) 
			and extract(month from e.benefit_end_date) ) s
	where
		s.id = epr.id;