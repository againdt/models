package com.getinsured.hix.dto.broker;

import java.io.Serializable;

public class BrokerAvailabilityDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String day;
    private String fromTime;
    private String toTime;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }
}
