/**
 * @author reddy_h
 * DTO class to hold Employer's invoice details
 */
package com.getinsured.hix.dto.finance;

import java.math.BigDecimal;
import java.util.Date;

public class EmployerInvoicesDTO {

	private int employerID;
	private String invoiceNumber;
	private String periodCovered;
	private BigDecimal premiumThisPeriod;
	private BigDecimal adjustments;
	private BigDecimal exchangeFees;
	private BigDecimal totalAmountDue;
	private BigDecimal manualAdjustmentAmt;
	private BigDecimal amountDueFromLastInvoice;
	private BigDecimal totalPaymentReceived;
	private BigDecimal invoice_TotAdj_RegAdj_ManualAdj;
	private Date statementDate;
	private Date paymentDueDate;
	public int getEmployerID() {
		return employerID;
	}
	public void setEmployerID(int employerID) {
		this.employerID = employerID;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getPeriodCovered() {
		return periodCovered;
	}
	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}
	public BigDecimal getPremiumThisPeriod() {
		return premiumThisPeriod;
	}
	public void setPremiumThisPeriod(BigDecimal premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}
	public BigDecimal getAdjustments() {
		return adjustments;
	}
	public void setAdjustments(BigDecimal adjustments) {
		this.adjustments = adjustments;
	}
	public BigDecimal getExchangeFees() {
		return exchangeFees;
	}
	public void setExchangeFees(BigDecimal exchangeFees) {
		this.exchangeFees = exchangeFees;
	}
	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}
	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}
	public BigDecimal getManualAdjustmentAmt() {
		return manualAdjustmentAmt;
	}
	public void setManualAdjustmentAmt(BigDecimal manualAdjustmentAmt) {
		this.manualAdjustmentAmt = manualAdjustmentAmt;
	}
	public BigDecimal getAmountDueFromLastInvoice() {
		return amountDueFromLastInvoice;
	}
	public void setAmountDueFromLastInvoice(BigDecimal amountDueFromLastInvoice) {
		this.amountDueFromLastInvoice = amountDueFromLastInvoice;
	}
	public BigDecimal getTotalPaymentReceived() {
		return totalPaymentReceived;
	}
	public void setTotalPaymentReceived(BigDecimal totalPaymentReceived) {
		this.totalPaymentReceived = totalPaymentReceived;
	}
	public BigDecimal getInvoice_TotAdj_RegAdj_ManualAdj() {
		return invoice_TotAdj_RegAdj_ManualAdj;
	}
	public void setInvoice_TotAdj_RegAdj_ManualAdj(
			BigDecimal invoice_TotAdj_RegAdj_ManualAdj) {
		this.invoice_TotAdj_RegAdj_ManualAdj = invoice_TotAdj_RegAdj_ManualAdj;
	}
	public Date getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	
}
