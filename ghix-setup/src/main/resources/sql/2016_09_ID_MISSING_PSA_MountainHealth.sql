--- Insert Missing Data
call FixServiceAreaForMV('ADA');
call FixServiceAreaForMV('ADAMS');
call FixServiceAreaForMV('BENEWAH');
call FixServiceAreaForMV('BLAINE');
call FixServiceAreaForMV('BOISE');
call FixServiceAreaForMV('BONNER');
call FixServiceAreaForMV('BOUNDARY');
call FixServiceAreaForMV('CAMAS');
call FixServiceAreaForMV('CANYON');
call FixServiceAreaForMV('CUSTER');
call FixServiceAreaForMV('ELMORE');
call FixServiceAreaForMV('GEM');
call FixServiceAreaForMV('GOODING');
call FixServiceAreaForMV('JEROME');
call FixServiceAreaForMV('KOOTENAI');
call FixServiceAreaForMV('LINCOLN');
call FixServiceAreaForMV('MINIDOKA');
call FixServiceAreaForMV('OWYHEE');
call FixServiceAreaForMV('PAYETTE');
call FixServiceAreaForMV('SHOSHONE');
call FixServiceAreaForMV('TWIN FALLS');
call FixServiceAreaForMV('VALLEY');
call FixServiceAreaForMV('WASHINGTON');

--- Update PM_ISSUER_SERVICE_AREA record set complete_state='YES'
update PM_ISSUER_SERVICE_AREA set complete_state = 'YES' where serff_service_area_id='IDS001' and hios_issuer_id='38128' and applicable_year=2017;

----------------Drop Function for DATA INSERT ---------------------------
DROP FUNCTION FixServiceAreaForMV(countyName VARCHAR);
