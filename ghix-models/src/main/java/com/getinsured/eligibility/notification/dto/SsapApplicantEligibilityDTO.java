package com.getinsured.eligibility.notification.dto;

import java.util.Map;
import java.util.Set;

public class SsapApplicantEligibilityDTO {

	private String firstName;

	private String middleName;

	private String lastName;

	private boolean conditional;

	private boolean applyingForCoverage;

	private boolean medicaidAssessed;

	private boolean healthLinkEligible;

	private Map<String, String> verificationMap;

	private Set<EligibilityProgramDTO> eligibilityProgramDTO;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<EligibilityProgramDTO> getEligibilityProgramDTO() {
		return eligibilityProgramDTO;
	}

	public void setEligibilityProgramDTO(Set<EligibilityProgramDTO> eligibilityProgramDTO) {
		this.eligibilityProgramDTO = eligibilityProgramDTO;
	}

	public boolean isConditional() {
		return conditional;
	}

	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	public boolean isApplyingForCoverage() {
		return applyingForCoverage;
	}

	public void setApplyingForCoverage(boolean applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public boolean isMedicaidAssessed() {
		return medicaidAssessed;
	}

	public void setMedicaidAssessed(boolean medicaidAssessed) {
		this.medicaidAssessed = medicaidAssessed;
	}

	public boolean isHealthLinkEligible() {
		return healthLinkEligible;
	}

	public void setHealthLinkEligible(boolean healthLinkEligible) {
		this.healthLinkEligible = healthLinkEligible;
	}

	public Map<String, String> getVerificationMap() {
		return verificationMap;
	}

	public void setVerificationMap(Map<String, String> verificationMap) {
		this.verificationMap = verificationMap;
	}

}
