package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.consumer.StageTracking;

public class ConsumerStageTrackingResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<StageTracking> stageTrackingList;
	
	public List<StageTracking> getStageTrackingList() {
		return stageTrackingList;
	}
	public void setStageTrackingList(List<StageTracking> stageTrackingList) {
		this.stageTrackingList = stageTrackingList;
	}
	
	

}
