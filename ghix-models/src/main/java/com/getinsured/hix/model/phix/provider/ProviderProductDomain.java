package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_product_domain")
public class ProviderProductDomain  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer productId;
	private String productName;
	private Integer siteId;
	private String siteName;
	private Set<ProviderProduct> providerProducts = new HashSet<ProviderProduct>(0);
	private Set<ProviderMapping> providerMappings = new HashSet<ProviderMapping>(0);

	@Id 
	@Column(name="PRODUCT_ID", unique=true, nullable=false)
	public Integer getProductId() {
		return this.productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}


	@Column(name="PRODUCT_NAME", length=100)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


	@Column(name="SITE_ID")
	public Integer getSiteId() {
		return this.siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}


	@Column(name="SITE_NAME", length=100)
	public String getSiteName() {
		return this.siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProductDomain")
	public Set<ProviderProduct> getProviderProducts() {
		return this.providerProducts;
	}

	public void setProviderProducts(Set<ProviderProduct> providerProducts) {
		this.providerProducts = providerProducts;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProductDomain")
	public Set<ProviderMapping> getProviderMappings() {
		return this.providerMappings;
	}

	public void setProviderMappings(Set<ProviderMapping> providerMappings) {
		this.providerMappings = providerMappings;
	}




}


