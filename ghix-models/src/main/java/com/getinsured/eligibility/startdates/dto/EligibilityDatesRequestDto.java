package com.getinsured.eligibility.startdates.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class EligibilityDatesRequestDto {

	@NotNull
	private Long cmrHouseholdId;
	@NotNull
	private Long enAppId;
	@NotNull
	private Long erAppId;
	@NotNull
	private Date enrollmentStartDate;
	@NotNull
	private Date enrollmentEndDate;

	private boolean dentalOnly;

	private Date coverageStartDate;
}
