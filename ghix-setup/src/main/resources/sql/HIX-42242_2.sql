CREATE OR REPLACE
PROCEDURE setDentalEnrollmentID
IS
BEGIN
  FOR EA IN
  ( SELECT ID,EMPLOYEE_ID FROM EMPLOYEE_APPLICATIONS
  )
  LOOP
    DECLARE
      DENTAL_ENRL ENROLLMENT%ROWTYPE;
      HEALTH_ENRL ENROLLMENT%ROWTYPE;
      MAX_DENTAL_ID       NUMBER;
      MAX_HEALTH_ID       NUMBER;
      ACTIVE_HEALTH_COUNT NUMBER;
      ACTIVE_DENTAL_COUNT NUMBER;
    BEGIN
      SELECT MAX(ID)
      INTO MAX_DENTAL_ID
      FROM ENROLLMENT enr
      WHERE enr.EMPLOYEE_ID       = EA.EMPLOYEE_ID
      AND enr.INSURANCE_TYPE_LKP IN
        (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN ('DEN')
        )
      AND enr.ENROLLMENT_TYPE_LKP IN
        (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN ('24')
        );
        
      IF MAX_DENTAL_ID IS NOT NULL THEN
        SELECT * INTO DENTAL_ENRL FROM ENROLLMENT WHERE ID = MAX_DENTAL_ID;
        SELECT COUNT(*)
        INTO ACTIVE_DENTAL_COUNT
        FROM ENROLLMENT
        WHERE ID                   = MAX_DENTAL_ID
        AND ENROLLMENT_STATUS_LKP IN
          (SELECT lookup_value_id
          FROM lookup_value
          WHERE lookup_value_code IN ('CONFIRM','PAYMENT_RECEIVED','PENDING')
          );
          
        IF ACTIVE_DENTAL_COUNT > 0 THEN
          --Updating DENTAL_ENROLLMENT_ID in EMPLOYEE_APPLICATIONS table
          UPDATE EMPLOYEE_APPLICATIONS
          SET DENTAL_ENROLLMENT_ID = DENTAL_ENRL.ID
          WHERE ID                 = EA.ID;
          dbms_output.put_line('DENTAL ACTIVE UPDATE EMPLOYEE APPL. TABLE : ' ||EA.ID|| ' WITH DENTAL ENRL ID :' || DENTAL_ENRL.ID);
        ELSE
          SELECT MAX(ID)
          INTO MAX_HEALTH_ID
          FROM ENROLLMENT enr
          WHERE enr.EMPLOYEE_ID       = EA.EMPLOYEE_ID
          AND enr.INSURANCE_TYPE_LKP IN
            (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN ('HLT')
            )
          AND enr.ENROLLMENT_TYPE_LKP IN
            (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN ('24')
            );
            
          SELECT * INTO HEALTH_ENRL FROM ENROLLMENT WHERE ID = MAX_HEALTH_ID;
          
          SELECT COUNT(*)
          INTO ACTIVE_HEALTH_COUNT
          FROM ENROLLMENT
          WHERE ID                   = MAX_HEALTH_ID
          AND ENROLLMENT_STATUS_LKP IN
            (SELECT lookup_value_id
            FROM lookup_value
            WHERE lookup_value_code IN ('CONFIRM','PAYMENT_RECEIVED','PENDING')
            );
          IF ACTIVE_HEALTH_COUNT > 0 THEN
            --Updating DENTAL_ENROLLMENT_ID in EMPLOYEE_APPLICATIONS table
            UPDATE EMPLOYEE_APPLICATIONS
            SET DENTAL_ENROLLMENT_ID = NULL
            WHERE ID                 = EA.ID;
            dbms_output.put_line('DENTAL TERMINATED AND HEALTH ACTIVE UPDATE EMPLOYEE APPL. TABLE: ' ||EA.ID|| ' WITH DENTAL ENRL ID = NULL' || DENTAL_ENRL.ID);
          ELSE
            DECLARE
              HEALTH_LAST_UPDATE_TIME DATE;
              DENTAL_LAST_UPDATE_TIME DATE;
              TIME_DIFFERENCE         NUMBER;
            BEGIN
              SELECT to_Date(TO_CHAR (HEALTH_ENRL.LAST_UPDATE_TIMESTAMP, 'DD-MON-YYYY HH12:MI:SS'),'DD-MON-YYYY HH12:MI:SS')
              INTO HEALTH_LAST_UPDATE_TIME
              FROM DUAL;
              
              SELECT to_Date(TO_CHAR (DENTAL_ENRL.LAST_UPDATE_TIMESTAMP, 'DD-MON-YYYY HH12:MI:SS'),'DD-MON-YYYY HH12:MI:SS')
              INTO DENTAL_LAST_UPDATE_TIME
              FROM DUAL;
              
              SELECT ABS(((HEALTH_LAST_UPDATE_TIME - DENTAL_LAST_UPDATE_TIME) * 1440))
              INTO TIME_DIFFERENCE
              FROM DUAL;
          
              IF TIME_DIFFERENCE >1 THEN
                --Updating DENTAL_ENROLLMENT_ID in EMPLOYEE_APPLICATIONS table
                UPDATE EMPLOYEE_APPLICATIONS
                SET DENTAL_ENROLLMENT_ID = NULL
                WHERE ID                 = EA.ID;
                dbms_output.put_line('DENTAL TERMINATED AND HEALTH TERMINATED BUT NEW PLAN ONLY HEALTH WITH WAS AGAIN TERM UPDATE EMPLOYEE APPL. TABLE : ' ||EA.ID|| ' WITH DENTAL ENRL ID = NULL' || DENTAL_ENRL.ID);
              ELSE
                --Updating DENTAL_ENROLLMENT_ID in EMPLOYEE_APPLICATIONS table
                UPDATE EMPLOYEE_APPLICATIONS
                SET DENTAL_ENROLLMENT_ID = DENTAL_ENRL.ID
                WHERE ID                 = EA.ID;
                dbms_output.put_line('BOTH DENTAL TERMINATED AND HEALTH TERMINATED UPDATE EMPLOYEE APPL. TABLE : ' ||EA.ID|| ' WITH DENTAL ENRL ID ='|| DENTAL_ENRL.ID);
              END IF;
            END;
          END IF;
        END IF;
      END IF;
    END;
  END LOOP;
END;
/

EXECUTE setDentalEnrollmentID;
