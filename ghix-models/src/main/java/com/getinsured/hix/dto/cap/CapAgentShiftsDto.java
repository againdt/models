package com.getinsured.hix.dto.cap;

/*
 * Created by kaul_s on 2/26/15.
 */
public class CapAgentShiftsDto {

	private Integer shiftId;
	private String shiftName;
	private String shiftDescription;
	private float shiftHours;
	
	
	public Integer getShiftId() {
		return shiftId;
	}
	public void setShiftId(Integer shiftId) {
		this.shiftId = shiftId;
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public String getShiftDescription() {
		return shiftDescription;
	}
	public void setShiftDescription(String shiftDescription) {
		this.shiftDescription = shiftDescription;
	}
	public float getShiftHours() {
		return shiftHours;
	}
	public void setShiftHours(float shiftHours) {
		this.shiftHours = shiftHours;
	}

}
