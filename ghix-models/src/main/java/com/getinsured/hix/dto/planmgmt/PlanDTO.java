package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GHIXResponse;
import com.google.gson.Gson;

//import lombok.Getter;
//import lombok.Setter;

//@Getter
//@Setter
public class PlanDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String planName;
	private String issuerPlanNumber;
	private int issuerId;
	private String issuerName;
	private String issuerLegalName;
	private String issuerLogoUrl;
	private String issuerStatus;
	private boolean available;
	private String planLevel;
	private String planStatus;
	private String planState;
	private Date planStartDate;
	private Date planEndDate;
	private String planDesignType;
	private String serviceAreaId;
	private String formularyId;
	private String issuerVerificationStatus;
	private String planMarket;
	private String networkName;
	private String networkUrl;
	private String enrollAvailability;
	private String enrollAvailEffectiveDate;
	private Map<String, Map<String, String>> planBenefits;	
	private Map<String, Map<String, String>> planCosts;
	private Map<String, Map<String, String>> optionalDeductible;
	private String insuranceType;
	private String hiosIssuerId;
	private String hsa;	
	private String ehbCovered;
	private String formularyUrl;
	private String sbcDocUrl;
	private int planYear;
	private String nonCommissionFlag;
	private String supportFile;
	private String brochureUrl;
	private String exclusionUrl;
	private String networkType;
	private Integer childRecordId;

	//private Float csrAmount;
	//private String costSharing;
	//private String eocDocUrl;
	//private String planRegionName;
	//private String officeVisit;
	//private String genericMedications;
	//private String deductible;
	//private String deductibleFamily;
	//private String oopMax;
	//private String oopMaxFamily;
	//private Float planPremium;
	//private String paymentUrl;
	//private List planList;
	//private String applicationUrl;
	//private String exchangeType;
	//private String issuerLogo;
	//private String genericDrugs;
	//private String officeVisitCost;
	//private String pediatricDentalComponent; 
	//private String stateEhbMandatePercentage;
	
	/*	Adding new fields in Plan response	*/
	//private String coinsurance;
	//private String policyLength;
	//private String guaranteedVsEstimatedRate;
	//private String policyLengthUnit;
	//private String policyLimit;
	//private String outOfNetwkCoverage;
	//private String outOfCountyCoverage;
	//private String separateDrugDeductible;
	//private String oopMaxAttr;
	
	
	//private List<Map<String, String>> providers;
	
	public PlanDTO(){
		 
	}
	
	public int getPlanId() {
		return planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getIssuerLegalName() {
		return issuerLegalName;
	}

	public void setIssuerLegalName(String issuerLegalName) {
		this.issuerLegalName = issuerLegalName;
	}

	public String getIssuerLogoUrl() {
		return issuerLogoUrl;
	}

	public void setIssuerLogoUrl(String issuerLogoUrl) {
		this.issuerLogoUrl = issuerLogoUrl;
	}

	/**
	 * @return the issuerStatus
	 */
	public String getIssuerStatus() {
		return issuerStatus;
	}

	/**
	 * @param issuerStatus the issuerStatus to set
	 */
	public void setIssuerStatus(String issuerStatus) {
		this.issuerStatus = issuerStatus;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

	public String getPlanState() {
		return planState;
	}

	public void setPlanState(String planState) {
		this.planState = planState;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public String getPlanDesignType() {
		return planDesignType;
	}

	public void setPlanDesignType(String planDesignType) {
		this.planDesignType = planDesignType;
	}

	public String getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(String serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	public String getFormularyId() {
		return formularyId;
	}

	public void setFormularyId(String formularyId) {
		this.formularyId = formularyId;
	}

	public String getIssuerVerificationStatus() {
		return issuerVerificationStatus;
	}

	public void setIssuerVerificationStatus(String issuerVerificationStatus) {
		this.issuerVerificationStatus = issuerVerificationStatus;
	}

	public String getPlanMarket() {
		return planMarket;
	}

	public void setPlanMarket(String planMarket) {
		this.planMarket = planMarket;
	}

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getNetworkUrl() {
		return networkUrl;
	}

	public void setNetworkUrl(String networkUrl) {
		this.networkUrl = networkUrl;
	}

	public String getEnrollAvailability() {
		return enrollAvailability;
	}

	public void setEnrollAvailability(String enrollAvailability) {
		this.enrollAvailability = enrollAvailability;
	}

	public String getEnrollAvailEffectiveDate() {
		return enrollAvailEffectiveDate;
	}

	public void setEnrollAvailEffectiveDate(String enrollAvailEffectiveDate) {
		this.enrollAvailEffectiveDate = enrollAvailEffectiveDate;
	}

	public Map<String, Map<String, String>> getPlanBenefits() {
		return planBenefits;
	}

	public void setPlanBenefits(Map<String, Map<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}

	public Map<String, Map<String, String>> getPlanCosts() {
		return planCosts;
	}

	public void setPlanCosts(Map<String, Map<String, String>> planCosts) {
		this.planCosts = planCosts;
	}

	public Map<String, Map<String, String>> getOptionalDeductible() {
		return optionalDeductible;
	}

	public void setOptionalDeductible(Map<String, Map<String, String>> optionalDeductible) {
		this.optionalDeductible = optionalDeductible;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getHsa() {
		return hsa;
	}

	public void setHsa(String hsa) {
		this.hsa = hsa;
	}

	public String getEhbCovered() {
		return ehbCovered;
	}

	public void setEhbCovered(String ehbCovered) {
		this.ehbCovered = ehbCovered;
	}

	public String getFormularyUrl() {
		return formularyUrl;
	}

	public void setFormularyUrl(String formularyUrl) {
		this.formularyUrl = formularyUrl;
	}

	public String getSbcDocUrl() {
		return sbcDocUrl;
	}

	public void setSbcDocUrl(String sbcDocUrl) {
		this.sbcDocUrl = sbcDocUrl;
	}

	public int getPlanYear() {
		return planYear;
	}

	public void setPlanYear(int planYear) {
		this.planYear = planYear;
	}

	public String getNonCommissionFlag() {
		return nonCommissionFlag;
	}

	public void setNonCommissionFlag(String nonCommissionFlag) {
		this.nonCommissionFlag = nonCommissionFlag;
	}

	public static Comparator<PlanDTO> getPlanResponseComparatorByPlanId() {
		return PlanResponseComparatorByPlanId;
	}

	public static void setPlanResponseComparatorByPlanId(Comparator<PlanDTO> planResponseComparatorByPlanId) {
		PlanResponseComparatorByPlanId = planResponseComparatorByPlanId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public String getSupportFile() {
		return supportFile;
	}

	public void setSupportFile(String supportFile) {
		this.supportFile = supportFile;
	}

	public String getBrochureUrl() {
		return brochureUrl;
	}

	public void setBrochureUrl(String brochureUrl) {
		this.brochureUrl = brochureUrl;
	}

	public String getExclusionUrl() {
		return exclusionUrl;
	}

	public void setExclusionUrl(String exclusionUrl) {
		this.exclusionUrl = exclusionUrl;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public Integer getChildRecordId() {
		return childRecordId;
	}

	public void setChildRecordId(Integer childRecordId) {
		this.childRecordId = childRecordId;
	}

	@Override
	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}


	public static Comparator<PlanDTO> PlanResponseComparatorByPlanId = new Comparator<PlanDTO>(){

		@Override
		public int compare(PlanDTO obj1, PlanDTO obj2) {
			Integer obj1PlanId = obj1.getPlanId();
			Integer obj2PlanId = obj2.getPlanId();
			return obj1PlanId.compareTo(obj2PlanId);
		}
	};
}
