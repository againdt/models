package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Date;

import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Tenant;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.platform.enums.OnOffEnum;
import com.getinsured.hix.platform.enums.YesNoEnum;

public class PdQuoteRequest implements Cloneable{
	private PdPreferencesDTO pdPreferencesDTO;
	private PdHouseholdDTO pdHouseholdDTO;
	private List<PdPersonDTO> pdPersonDTOList;
	private Date employerCoverageStartDate;
	private String employerPlanTier;
	private String employerWorksiteZip;
	private String employerWorksiteCounty;
	private InsuranceType insuranceType;
	private ExchangeType exchangeType;
	private EnrollmentFlowType specialEnrollmentFlowType;
	private String issuerId;
	private List<String> planIdList;
	private boolean minimizePlanData;
	private boolean showCatastrophicPlan;
	private String tenant;
	private String initialSubscriberId;
	private String initialCmsPlanId;
	private Long initialPlanIdToEliminate;
	private OnOffEnum affiliateIssuerHiosIdRestricted;
	private YesNoEnum showPufPlans;
	private boolean recommendPlans;
	private String currentPlanId;
	private Boolean showPrevYearEnrlPlan;
	
	@Override
	public PdQuoteRequest clone() {
	    try {
	      return (PdQuoteRequest)super.clone();
	    }
	    catch(CloneNotSupportedException e) {
	      throw new AssertionError(e);
	    }
	}
	
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}
	public PdHouseholdDTO getPdHouseholdDTO() {
		return pdHouseholdDTO;
	}
	public void setPdHouseholdDTO(PdHouseholdDTO pdHouseholdDTO) {
		this.pdHouseholdDTO = pdHouseholdDTO;
	}
	public List<PdPersonDTO> getPdPersonDTOList() {
		return pdPersonDTOList;
	}
	public void setPdPersonDTOList(List<PdPersonDTO> pdPersonDTOList) {
		this.pdPersonDTOList = pdPersonDTOList;
	}
	public Date getEmployerCoverageStartDate() {
		return employerCoverageStartDate;
	}
	public void setEmployerCoverageStartDate(Date employerCoverageStartDate) {
		this.employerCoverageStartDate = employerCoverageStartDate;
	}
	public String getEmployerPlanTier() {
		return employerPlanTier;
	}
	public void setEmployerPlanTier(String employerPlanTier) {
		this.employerPlanTier = employerPlanTier;
	}
	public String getEmployerWorksiteZip() {
		return employerWorksiteZip;
	}
	public void setEmployerWorksiteZip(String employerWorksiteZip) {
		this.employerWorksiteZip = employerWorksiteZip;
	}
	public String getEmployerWorksiteCounty() {
		return employerWorksiteCounty;
	}
	public void setEmployerWorksiteCounty(String employerWorksiteCounty) {
		this.employerWorksiteCounty = employerWorksiteCounty;
	}
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	public ExchangeType getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}
	public EnrollmentFlowType getSpecialEnrollmentFlowType() {
		return specialEnrollmentFlowType;
	}
	public void setSpecialEnrollmentFlowType(EnrollmentFlowType specialEnrollmentFlowType) {
		this.specialEnrollmentFlowType = specialEnrollmentFlowType;
	}
	public String getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	public List<String> getPlanIdList() {
		return planIdList;
	}
	public void setPlanIdList(List<String> planIdList) {
		this.planIdList = planIdList;
	}
	public boolean getMinimizePlanData() {
		return minimizePlanData;
	}
	public void setMinimizePlanData(boolean minimizePlanData) {
		this.minimizePlanData = minimizePlanData;
	}
	public boolean getShowCatastrophicPlan() {
		return showCatastrophicPlan;
	}
	public void setShowCatastrophicPlan(boolean showCatastrophicPlan) {
		this.showCatastrophicPlan = showCatastrophicPlan;
	}
	public String getTenant() {
		return tenant;
	}
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	public Long getInitialPlanIdToEliminate() {
		return initialPlanIdToEliminate;
	}
	public void setInitialPlanIdToEliminate(Long initialPlanIdToEliminate) {
		this.initialPlanIdToEliminate = initialPlanIdToEliminate;
	}
	public String getInitialSubscriberId() {
		return initialSubscriberId;
	}
	public void setInitialSubscriberId(String initialSubscriberId) {
		this.initialSubscriberId = initialSubscriberId;
	}
	public String getInitialCmsPlanId() {
		return initialCmsPlanId;
	}
	public void setInitialCmsPlanId(String initialCmsPlanId) {
		this.initialCmsPlanId = initialCmsPlanId;
	}
	public OnOffEnum getAffiliateIssuerHiosIdRestricted() {
		return affiliateIssuerHiosIdRestricted;
	}
	public void setAffiliateIssuerHiosIdRestricted(
			OnOffEnum affiliateIssuerHiosIdRestricted) {
		this.affiliateIssuerHiosIdRestricted = affiliateIssuerHiosIdRestricted;
	}
	public YesNoEnum getShowPufPlans() {
		return showPufPlans;
	}
	public void setShowPufPlans(YesNoEnum showPufPlans) {
		this.showPufPlans = showPufPlans;
	}
	public boolean getRecommendPlans() {
		return recommendPlans;
	}
	public void setRecommendPlans(boolean recommendPlans) {
		this.recommendPlans = recommendPlans;
	}
	public String getCurrentPlanId() {
		return currentPlanId;
	}
	public void setCurrentPlanId(String currentPlanId) {
		this.currentPlanId = currentPlanId;
	}

	public Boolean getShowPrevYearEnrlPlan() {
		return showPrevYearEnrlPlan;
	}

	public void setShowPrevYearEnrlPlan(Boolean showPrevYearEnrlPlan) {
		this.showPrevYearEnrlPlan = showPrevYearEnrlPlan;
	}
	
}
