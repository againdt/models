/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.util.List;

import com.getinsured.hix.model.GHIXResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is the response object for GoodRxAPI
 * 
 * @author gajulapalli_k
 *
 */
public class GoodRxAPIResponse extends GHIXResponse{
	
	private List brand;
	
	private List generic;
	
	private String display;
	
	private String form;
	
	private String dosage;
	
	private float quantity;
	
	private float price;
	
	private String url;
	
	private String manufacturer;
	
	private String[] errors;
	
	private boolean success;
	
	private GoodRxAPIResponseData data;
	
	/**
	 * @return the data
	 */
	public GoodRxAPIResponseData getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	@JsonProperty("data")
	public void setData(GoodRxAPIResponseData data) {
		this.data = data;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	

	/**
	 * @return the errors
	 */
	public String[] getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}

	/**
	 * @param form the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	

	/**
	 * @return the brand
	 */
	public List getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(List brand) {
		this.brand = brand;
	}

	/**
	 * @return the dosage
	 */
	public String getDosage() {
		return dosage;
	}

	/**
	 * @param dosage the dosage to set
	 */
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	
	/**
	 * @return the display
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * @param manufacturer the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * @return the generic
	 */
	public List getGeneric() {
		return generic;
	}

	/**
	 * @param generic the generic to set
	 */
	public void setGeneric(List generic) {
		this.generic = generic;
	}

	/**
	 * @return the quantity
	 */
	public float getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	
		
}
