package com.getinsured.hix.model;

import java.io.Serializable;

public class TkmQuickActionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1865647539806541411L;

	private String assignee;
	private String status;
	private String currentTask;
	private String taskStatus;

	public TkmQuickActionDto(String assignee, String status, String currentTask, String taskStatus) {
		this.assignee = assignee;
		this.status = status;
		this.currentTask = currentTask;
		this.taskStatus = taskStatus;
	}
	
	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(String currentTask) {
		this.currentTask = currentTask;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

}
