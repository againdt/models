package com.getinsured.hix.dto.estimator.mini;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * Create DTO to send pre-65 Retiree data to AgentExpress.
 */
public class RetireeInfoDTO {

	/* Indicates the Affiliate ID which is unique for an affiliate */
	private long affiliateId;
	/* Indicates the flow ID which is unique for an affiliate */
	private Integer flowId;
	/* Indicates Type of Record */
	private String dataType;
	/* Indicates External partner’s Household ID */
	private String externalId;
	/* Household ID, Will be used for reconciliation of households */
	private String hId;
	/* Indicates Applicant’s First Name */
	private String firstName;
	/* Indicates Applicant’s Last Name */
	private String lastName;
	/* Indicates the household PIN */
	private String pinCode;
	/* Indicates Applicant’s phone number */
	private String phone;
	/* Indicates Applicant’s email ID */
	private String email;

	/* Indicates Applicant’s address line1 */
	private String address1;
	/* Indicates Applicant’s address line2 */
	private String address2;
	/* Indicates Applicant’s city */
	private String city;
	/* Indicates Applicant’s state (Two character state code) */
	private String state;
	/* Indicates the ZIP code for the household. Standard US zipcode (only numbers are allowed in the string and it has to be a 5-digit string) */
	private String zipCode;
	/* Indicates the county code for the household. It is a combination of stateFIPS and countyFIPS */
	private String countyCode;

	/* Annual household income */
	private Double hhIncome;
	/* Number of family members seeking insurance */
	private Integer familySize;
	/* Qualifying Life Event. Possible values are as follows. Default, if nothing is specified: LOST_COVERAGE */
	private Long eventId;
	/* HRA Funding Type of the household (Valid HRA Funding Types are "INDIVIDUAL_MONTHLY_FUNDING", "INDIVIDUAL_ANNUAL_FUNDING", "FAMILY_LUMPSUM_DEPOSIT") */
	private String hraFundingType;
	/* Family HRA Amount of the household */
	private Double familyHraAmount;
	/* HRA subsidy start date in MM/DD/YYYY format */
	private String hraStartDate;
	/* HRA subsidy end date in MM/DD/YYYY format */
	private String hraEndDate;
	/* List of Household Members */
	private List<RetireeMemberDTO> memberList;

	public RetireeInfoDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String gethId() {
		return hId;
	}

	public void sethId(String hId) {
		this.hId = hId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public void setMemberList(List<RetireeMemberDTO> memberList) {
		this.memberList = memberList;
	}

	public Double getHhIncome() {
		return hhIncome;
	}

	public void setHhIncome(Double hhIncome) {
		this.hhIncome = hhIncome;
	}

	public Integer getFamilySize() {
		return familySize;
	}

	public void setFamilySize(Integer familySize) {
		this.familySize = familySize;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getHraFundingType() {
		return hraFundingType;
	}

	public void setHraFundingType(String hraFundingType) {
		this.hraFundingType = hraFundingType;
	}

	public Double getFamilyHraAmount() {
		return familyHraAmount;
	}

	public void setFamilyHraAmount(Double familyHraAmount) {
		this.familyHraAmount = familyHraAmount;
	}

	public String getHraStartDate() {
		return hraStartDate;
	}

	public void setHraStartDate(String hraStartDate) {
		this.hraStartDate = hraStartDate;
	}

	public String getHraEndDate() {
		return hraEndDate;
	}

	public void setHraEndDate(String hraEndDate) {
		this.hraEndDate = hraEndDate;
	}

	public List<RetireeMemberDTO> getMemberList() {
		return memberList;
	}

	/**
	 * set MemberDTO in List
	 */
	public void setMemberInList(RetireeMemberDTO memberDTO) {

		if (null == this.memberList) {
			this.memberList = new ArrayList<RetireeMemberDTO>();
		}
		memberList.add(memberDTO);
	}
}
