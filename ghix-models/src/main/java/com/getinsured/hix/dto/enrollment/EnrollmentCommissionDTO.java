/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import com.getinsured.hix.model.GHIXRequest;

/**
 * @author panda_p
 *
 */
public class EnrollmentCommissionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String carrier;
	
	private String IssuerId ;
	
	private String state ;
	
	private String enrollmentId;	

	private String commissionAmt;

	private String commissionDate ;
		
	private String createdOn;
	
	private String updatedOn;
	
	private String paymentDate;
	
	private Date dueMonthYear;
	
	private String amtPaidToDate;
	
	private String commissionRate;
	
	private String commissionableAmount;
	
	private String mode;
	
	private String commissionType;
	
	private Integer carrierFeedDetailsId;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getIssuerId() {
		return IssuerId;
	}

	public void setIssuerId(String issuerId) {
		IssuerId = issuerId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(String commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public String getCommissionDate() {
		return commissionDate;
	}

	public void setCommissionDate(String commissionDate) {
		this.commissionDate = commissionDate;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getDueMonthYear() {
		return dueMonthYear;
	}

	public void setDueMonthYear(Date dueMonthYear) {
		this.dueMonthYear = dueMonthYear;
	}

	public String getAmtPaidToDate() {
		return amtPaidToDate;
	}

	public void setAmtPaidToDate(String amtPaidToDate) {
		this.amtPaidToDate = amtPaidToDate;
	}

	public String getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(String commissionRate) {
		this.commissionRate = commissionRate;
	}

	public String getCommissionableAmount() {
		return commissionableAmount;
	}

	public void setCommissionableAmount(String commissionableAmount) {
		this.commissionableAmount = commissionableAmount;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}

	public Integer getCarrierFeedDetailsId() {
		return carrierFeedDetailsId;
	}

	public void setCarrierFeedDetailsId(Integer carrierFeedDetailsId) {
		this.carrierFeedDetailsId = carrierFeedDetailsId;
	}
	
	}
