package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;

public class IssuerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String issuerCode;
	private String issuerName;
	public String getIssuerCode() {
		return issuerCode;
	}
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	

}
