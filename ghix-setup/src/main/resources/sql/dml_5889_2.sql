
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ADMIN'), (select id from permissions   where name=UPPER('manage_broker')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='BROKER_ADMIN'), (select id from permissions   where name=UPPER('manage_broker')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Employer_Plan_Selection_wizard')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Plan_Selections')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Employer_Dashboard')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Add_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Edit_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Dependent_Details')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Review_Employee_List')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Manage_Employee_List')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Search_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Available_Plans')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Contribution_Selection')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Renew_Coverage')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Invoices')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Payments')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Participation_Report')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Payment_Report')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Account_Information')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Eligibility_Results')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('View_Authorized_Representatives')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Appeals')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER'), (select id from permissions   where name=UPPER('Complaints')));


INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Delete_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Terminate_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Waive_Coverage')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Terminate_Coverage')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Add_Payment_Method')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Manage_Payment_Methods')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Make_Payment')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Edit_Account_Information')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Edit_Authorized_Representatives')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Terminate_Employer_Account')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Add/Remove_Authorized_Employer_Representatives')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Add_Employees')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Enroll_on_Behalf_of_Employee')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Cancel_Open_Enrollment')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='EMPLOYER_ADMIN'), (select id from permissions   where name=UPPER('Waive_Employee_Coverage')));



INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Individual_Members')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_SHOP_Members')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Qualified_Health_Plans')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Qualified_Dental_Plans')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Qualified_Vision_Plans')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Plan_Details')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Plan_Benefits')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Plan_Rates')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Provider_Network_ID')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Enrollment_Availability')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Certification_Status')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Plan_History')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Plan_Reports')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Issuer_Reports')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER'), (select id from permissions   where name=UPPER('View_Broker_Reports')));


INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Manage_Financial_Information')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Manage_Authorized_Representatives')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Edit_Plan_Details')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Upload_New_Benefits')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Upload_New_Rates')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Update_Provider_Network_ID')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Update_Enrollment_Availability')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));


INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ASSISTER_ADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));


INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='BROKER_ADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));

