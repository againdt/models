package com.getinsured.hix.model.emx;

import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * This Object is used inside EmxEmployeeRequest, 
 * which is to represent the Household members of Employee
 * 
 * @author kancherla_s
 *
 */
public class EmxEmployeeMember {

		
		private String memberId; 
		
		private Double hraAmount; 
		
		private Double employerContributionAmount; 
		
		private String dateOfBirth;
		
		private MemberRelationships relationshipToPrimary;
		
		private String gender;

				public String getMemberId() {
			return memberId;
		}

		public void setMemberId(String memberId) {
			this.memberId = memberId;
		}

		public Double getHraAmount() {
			return hraAmount;
		}

		public void setHraAmount(Double hraAmount) {
			this.hraAmount = hraAmount;
		}

		public Double getEmployerContributionAmount() {
			return employerContributionAmount;
		}

		public void setEmployerContributionAmount(Double employerContributionAmount) {
			this.employerContributionAmount = employerContributionAmount;
		}

		public String getDateOfBirth() {
			return dateOfBirth;
		}

		public void setDateOfBirth(String dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}

		public MemberRelationships getRelationshipToPrimary() {
			return relationshipToPrimary;
		}

		public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
			this.relationshipToPrimary = relationshipToPrimary;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

}
