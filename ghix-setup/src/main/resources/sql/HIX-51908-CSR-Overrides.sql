insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'IND_PORTAL_CSR_OVERRIDES');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'IND_PORTAL_DISENROLL_WITH_DATE');

INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ADMIN'), (select id from permissions where name='IND_PORTAL_CSR_OVERRIDES'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L1_CSR'), (select id from permissions where name='IND_PORTAL_CSR_OVERRIDES'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='IND_PORTAL_CSR_OVERRIDES'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='SUPERVISOR'), (select id from permissions where name='IND_PORTAL_CSR_OVERRIDES'));

INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ADMIN'), (select id from permissions where name='IND_PORTAL_DISENROLL_WITH_DATE'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L1_CSR'), (select id from permissions where name='IND_PORTAL_DISENROLL_WITH_DATE'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='L2_CSR'), (select id from permissions where name='IND_PORTAL_DISENROLL_WITH_DATE'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='SUPERVISOR'), (select id from permissions where name='IND_PORTAL_DISENROLL_WITH_DATE'));
