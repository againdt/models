package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FFMHouseholdResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String aptc;
	private String ffmHouseholdId;
	private String csr; 
	private String countyCode; 
	private String email; 
	private String phoneNumber; 
	private String zipCode; 
    private String state;
    //guid to uniquely identify
    private String giHouseholdId;
    
    
	private String houseHoldpersonId;
    
    private List<FFMMemberResponse> members = new ArrayList<FFMMemberResponse>();
    
    public void addMember(FFMMemberResponse member){
    	members.add(member);
    }
    
	public String getAptc() {
		return aptc;
	}
	public void setAptc(String aptc) {
		this.aptc = aptc;
	}
	public String getFfmHouseholdId() {
		return ffmHouseholdId;
	}
	public void setFfmHouseholdId(String ffmHouseholdId) {
		this.ffmHouseholdId = ffmHouseholdId;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getGiHouseholdId() {
		return giHouseholdId;
	}
	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}
	public List<FFMMemberResponse> getMembers() {
		return members;
	}
	public void setMembers(List<FFMMemberResponse> member) {
		this.members = member;
	}
	
	public String getHouseHoldpersonId() {
		return houseHoldpersonId;
	}

	public void setHouseHoldpersonId(String houseHoldpersonId) {
		this.houseHoldpersonId = houseHoldpersonId;
	}

	
	@Override
	public String toString() {
		return "FFMHouseholdResponse [aptc=" + aptc + ", ffmHouseholdId="
				+ ffmHouseholdId + ", csr=" + csr + ", countyCode="
				+ countyCode + ", email=" + email + ", phoneNumber="
				+ phoneNumber + ", zipCode=" + zipCode + ", state=" + state
				+ ", giHouseholdId=" + giHouseholdId + ", member=" + members
				+ "]";
	}
	
    

}
