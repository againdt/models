
package com.getinsured.coveredCA.dto.ssap;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class HealthCoverage {
	
	  @Expose
	  private Boolean currentlyEnrolledIndicator= false;
	  @Expose
	  private String healthInsuranceType;
	  @Expose
	  private Boolean changesToCoverageIndicator = false;
	  @Expose
	  private Boolean longTermCareIndicator = false;
	  @Expose
	  private Boolean medicareIndicator = false;
	  @Expose
	  private List<CurrentEmployer> currentEmployer = new ArrayList<CurrentEmployer>();
	  


    public Boolean getCurrentlyEnrolledIndicator() {
		return currentlyEnrolledIndicator;
	}

	public void setCurrentlyEnrolledIndicator(Boolean currentlyEnrolledIndicator) {
		this.currentlyEnrolledIndicator = currentlyEnrolledIndicator;
	}

	public String getHealthInsuranceType() {
		return healthInsuranceType;
	}

	public void setHealthInsuranceType(String healthInsuranceType) {
		this.healthInsuranceType = healthInsuranceType;
	}

	public Boolean getChangesToCoverageIndicator() {
		return changesToCoverageIndicator;
	}

	public void setChangesToCoverageIndicator(Boolean changesToCoverageIndicator) {
		this.changesToCoverageIndicator = changesToCoverageIndicator;
	}

	public Boolean getLongTermCareIndicator() {
		return longTermCareIndicator;
	}

	public void setLongTermCareIndicator(Boolean longTermCareIndicator) {
		this.longTermCareIndicator = longTermCareIndicator;
	}

	public Boolean getMedicareIndicator() {
		return medicareIndicator;
	}

	public void setMedicareIndicator(Boolean medicareIndicator) {
		this.medicareIndicator = medicareIndicator;
	}

	public List<CurrentEmployer> getCurrentEmployer() {
        return currentEmployer;
    }

    public void setCurrentEmployer(List<CurrentEmployer> currentEmployer) {
        this.currentEmployer = currentEmployer;
    }

  

}
