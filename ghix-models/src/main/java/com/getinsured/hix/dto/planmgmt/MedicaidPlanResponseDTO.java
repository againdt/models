/**
 * 
 * @author santanu
 * @version 1.0
 * @since Jan 14, 2015 
 *
 * This DTO to Medicaid plan response 
 *
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class MedicaidPlanResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String householdCaseId;
	private List<MedicaidPlan>  medicaidPlanList;

	/**
	 * @return the householdCaseId
	 */
	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	/**
	 * @param householdCaseId the householdCaseId to set
	 */
	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	/**
	 * @return the medicaidPlanList
	 */
	public List<MedicaidPlan> getMedicaidPlanList() {
		return medicaidPlanList;
	}

	/**
	 * @param medicaidPlanList the medicaidPlanList to set
	 */
	public void setMedicaidPlanList(List<MedicaidPlan> medicaidPlanList) {
		this.medicaidPlanList = medicaidPlanList;
	}

	
	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse(){		
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON. 
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse(){
		String transformedResponse = null;
		
		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
		    public HierarchicalStreamWriter createWriter(Writer writer) {
		        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		    }
		});
		
		Validate.notNull(jsoner, "XStream instance improperly confugured.");
		
		transformedResponse = jsoner.toXML(this);
		
		return transformedResponse;
	}*/

}
