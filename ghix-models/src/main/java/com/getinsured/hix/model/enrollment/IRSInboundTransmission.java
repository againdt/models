package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
/**
 * The persistent class for the IRS Outbound Transmission database table.
 * 
 */

@Entity
@Table(name="IRS_INBOUND_TRANSMISSION")
public class IRSInboundTransmission implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IRS_INBOUND_TRANSMISSION_SEQ")
	@SequenceGenerator(name = "IRS_INBOUND_TRANSMISSION_SEQ", sequenceName = "IRS_INBOUND_TRANSMISSION_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name="EFTFileName")
	private String EFTFileName ;	

	@Column(name="HOUSEHOLD_CASE_ID")
	private String householdCaseID ;
	
	@Column(name="BATCH_ID")
	private String batchId ;  

	@Column(name="MONTH")
	private String month ;	

	@Column(name="YEAR")
	private String year ;	

 	@Column(name="IRS_ErrorMessageCd")
    private String errorMessageCd ;

	@Column(name="IRS_ErrorMessageTx")
    private String errorMessageTx ;
	
	@Column(name="IRS_XpathContent")
    private String xPathContent;	
	
 	@Column(name="DOCUMENT_SEQ_ID")
    private String documentSeqId ;
 	
	@Column(name="Response_Type")
    private String responseType ;
	
	@Column(name="Hub_ResponseCode")
    private String hubResponseCode ;
	
	@Column(name="Hub_ResponseDesc")
    private String hubResponseDesc ;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEFTFileName() {
		return EFTFileName;
	}

	public void setEFTFileName(String eFTFileName) {
		EFTFileName = eFTFileName;
	}

	public String getHouseholdCaseID() {
		return householdCaseID;
	}

	public void setHouseholdCaseID(String householdCaseID) {
		this.householdCaseID = householdCaseID;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getErrorMessageCd() {
		return errorMessageCd;
	}

	public void setErrorMessageCd(String errorMessageCd) {
		this.errorMessageCd = errorMessageCd;
	}

	public String getErrorMessageTx() {
		return errorMessageTx;
	}

	public void setErrorMessageTx(String errorMessageTx) {
		this.errorMessageTx = errorMessageTx;
	}

	public String getxPathContent() {
		return xPathContent;
	}

	public void setxPathContent(String xPathContent) {
		this.xPathContent = xPathContent;
	}

	public String getDocumentSeqId() {
		return documentSeqId;
	}

	public void setDocumentSeqId(String documentSeqId) {
		this.documentSeqId = documentSeqId;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getHubResponseCode() {
		return hubResponseCode;
	}

	public void setHubResponseCode(String hubResponseCode) {
		this.hubResponseCode = hubResponseCode;
	}

	public String getHubResponseDesc() {
		return hubResponseDesc;
	}

	public void setHubResponseDesc(String hubResponseDesc) {
		this.hubResponseDesc = hubResponseDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}
}
