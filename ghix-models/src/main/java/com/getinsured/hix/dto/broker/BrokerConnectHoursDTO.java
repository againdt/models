package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

public class BrokerConnectHoursDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean isAvailable;
    private String status;
    private String phoneNumber;
    List<BrokerAvailabilityDTO> brokerAvailabilities;
    private int responseCode;

    public boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean available) {
        isAvailable = available;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<BrokerAvailabilityDTO> getBrokerAvailabilities() {
        return brokerAvailabilities;
    }

    public void setBrokerAvailabilities(List<BrokerAvailabilityDTO> brokerAvailabilities) {
        this.brokerAvailabilities = brokerAvailabilities;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
