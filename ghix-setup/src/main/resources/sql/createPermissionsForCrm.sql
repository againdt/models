
        INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SEARCH_CONSUMERS_CRM');
        INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_CONSUMER_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'ADD_CONSUMER_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SWITCH_TO_CONSUMER_VIEW_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_ECOMMITMENT_DETAILS');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SAVE_ECOMMITMENT_DETAILS');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SEARCH_EMPLOYER_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_EMPLOYER_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SWITCH_TO_EMPLOYER_VIEW_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'RESEND_ACTIVATION_LINK');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SEARCH_EMPLOYEE_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_EMPLOYEE_CRM');
		INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'SWITCH_TO_EMPLOYEE_VIEW_CRM');
		
		
		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SEARCH_CONSUMERS_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='VIEW_CONSUMER_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='ADD_CONSUMER_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SWITCH_TO_CONSUMER_VIEW_CRM'));
    		
       INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='VIEW_ECOMMITMENT_DETAILS'));
    		
      INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SAVE_ECOMMITMENT_DETAILS'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SEARCH_CONSUMERS_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='VIEW_CONSUMER_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='ADD_CONSUMER_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SWITCH_TO_CONSUMER_VIEW_CRM'));

          INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='VIEW_ECOMMITMENT_DETAILS'));
    		
      INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SAVE_ECOMMITMENT_DETAILS'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SEARCH_EMPLOYER_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='VIEW_EMPLOYER_CRM'));


		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SWITCH_TO_EMPLOYER_VIEW_CRM'));
    		
    	INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='RESEND_ACTIVATION_LINK'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SEARCH_EMPLOYER_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='VIEW_EMPLOYER_CRM'));


		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SWITCH_TO_EMPLOYER_VIEW_CRM'));

    	INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='RESEND_ACTIVATION_LINK'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SEARCH_EMPLOYEE_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='VIEW_EMPLOYEE_CRM'));


		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='SWITCH_TO_EMPLOYEE_VIEW_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SEARCH_EMPLOYEE_CRM'));

		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='VIEW_EMPLOYEE_CRM'));


		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'CSR'), (select id from permissions where name='SWITCH_TO_EMPLOYEE_VIEW_CRM'));

