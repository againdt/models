package com.getinsured.hix.consumer.history;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.hix.consumer.history.ConsumerHistoryBase;

@JsonAutoDetect
public class ConsumerHistoryView {
	
	@JsonProperty
	String householdid;
	
	@JsonProperty
	List<ConsumerHistoryBase>  historyList;
	
	public String getHouseholdid() {
		return householdid;
	}
	public void setHouseholdid(String householdid) {
		this.householdid = householdid;
	}
	public List<ConsumerHistoryBase> getHistoryList() {
		return historyList;
	}
	public void setHistoryList(List<ConsumerHistoryBase> historyList) {
		this.historyList = historyList;
	}
	

}