insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/97132/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/57173/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/93091/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/26075/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/52744/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/66440/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/NM0/PASSTHRU',
  '/mnt/FTPnfs-mnt/75605/out',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '115',
  systimestamp,
  systimestamp
);

insert into EXCHG_FilePattern values ( 125, 'O', 'GRP', '', '<HIOS_Issuer_ID>.GROP.D<YYMMDD>.T<HHMMSSt>.T.OUT', 'xml', 'xml', '' );
insert into  EXCHG_FilePatternRole values ( 115, 'Group Installation Out', '125' );
