package com.getinsured.eligibility.referral.ui.dto;



/**
 * 
 * @author raguram_p
 *
 */
public class SepEventDTO {
	
	
	private String name;
	private String label;
	private String applicantGuid;
	private String applicantStatus;
	private Long applicantId;
	private Long ssapApplicationId;
	private String applicantName;
	private String eventDate;
	private String allowFutureEvent;
	private String isGated;
	
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getApplicantGuid() {
		return applicantGuid;
	}
	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}
	public String getApplicantStatus() {
		return applicantStatus;
	}
	public void setApplicantStatus(String applicantStatus) {
		this.applicantStatus = applicantStatus;
	}
	public Long getApplicantId() {
		return applicantId;
	}
	public void setApplicantId(Long applicantId) {
		this.applicantId = applicantId;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	
	public String getAllowFutureEvent() {
		return allowFutureEvent;
	}
	public void setAllowFutureEvent(String allowFutureEvent) {
		this.allowFutureEvent = allowFutureEvent;
	}	
	public String getIsGated() {
		return isGated;
	}
	public void setIsGated(String isGated) {
		this.isGated = isGated;
	}
	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		builder.append("LceActivityDTO [name=").append(name)
				.append(", applicantGuid=").append(applicantGuid)
				.append(", applicantStatus=").append(applicantStatus)
				.append(", applicantId=").append(applicantId)
				.append(", eventDate=").append(eventDate)
				.append(", allowFutureEvent=").append(allowFutureEvent)
				.append(", isGated=").append(isGated)
				.append("]");
		return builder.toString();
	}	
}
