package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.consumer.StageTracking.StageTrackingStatus;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;

public class ConsumerStageTrackingRequestDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer householdId;
	private StageTrackingStatus status;
	private STAGE stage;
	private List<Integer> stageTrackingIdList;
	
	public Integer getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}
	public StageTrackingStatus getStatus() {
		return status;
	}
	public void setStatus(StageTrackingStatus status) {
		this.status = status;
	}
	public STAGE getStage() {
		return stage;
	}
	public void setStage(STAGE stage) {
		this.stage = stage;
	}
	public List<Integer> getStageTrackingIdList() {
		return stageTrackingIdList;
	}
	public void setStageTrackingIdList(List<Integer> stageTrackingIdList) {
		this.stageTrackingIdList = stageTrackingIdList;
	}
		

}
