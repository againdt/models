package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class MemberReportingCategory implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="additionalMaintReason")
	private AdditionalMaintReason additionalMaintReason;

	public AdditionalMaintReason getAdditionalMaintReason() {
		return additionalMaintReason;
	}

	public void setAdditionalMaintReason(AdditionalMaintReason additionalMaintReason) {
		this.additionalMaintReason = additionalMaintReason;
	}

}