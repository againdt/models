
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.service.BasicResponse;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.serff.planmanagementexchangeapi.exchange.model.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidateAndTransformDataTemplate_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/service", "validateAndTransformDataTemplate");
    private final static QName _TransferPlan_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/service", "transferPlan");
    private final static QName _TransferPlanResponse_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/service", "transferPlanResponse");
    private final static QName _ValidateAndTransformDataTemplateResponse_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/service", "validateAndTransformDataTemplateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.serff.planmanagementexchangeapi.exchange.model.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransferPlan }
     * 
     */
    public TransferPlan createTransferPlan() {
        return new TransferPlan();
    }

    /**
     * Create an instance of {@link ValidateAndTransformDataTemplate }
     * 
     */
    public ValidateAndTransformDataTemplate createValidateAndTransformDataTemplate() {
        return new ValidateAndTransformDataTemplate();
    }

    /**
     * Create an instance of {@link TransferPlanResponse }
     * 
     */
    public TransferPlanResponse createTransferPlanResponse() {
        return new TransferPlanResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAndTransformDataTemplate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/service", name = "validateAndTransformDataTemplate")
    public JAXBElement<ValidateAndTransformDataTemplate> createValidateAndTransformDataTemplate(ValidateAndTransformDataTemplate value) {
        return new JAXBElement<ValidateAndTransformDataTemplate>(_ValidateAndTransformDataTemplate_QNAME, ValidateAndTransformDataTemplate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/service", name = "transferPlan")
    public JAXBElement<TransferPlan> createTransferPlan(TransferPlan value) {
        return new JAXBElement<TransferPlan>(_TransferPlan_QNAME, TransferPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferPlanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/service", name = "transferPlanResponse")
    public JAXBElement<TransferPlanResponse> createTransferPlanResponse(TransferPlanResponse value) {
        return new JAXBElement<TransferPlanResponse>(_TransferPlanResponse_QNAME, TransferPlanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BasicResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/service", name = "validateAndTransformDataTemplateResponse")
    public JAXBElement<BasicResponse> createValidateAndTransformDataTemplateResponse(BasicResponse value) {
        return new JAXBElement<BasicResponse>(_ValidateAndTransformDataTemplateResponse_QNAME, BasicResponse.class, null, value);
    }

}
