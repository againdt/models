package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class SelectedInsurancePlan{
  @JsonProperty("InsurancePlanVariant")
  private InsurancePlanVariant InsurancePlanVariant;
  
  @JsonProperty("InsurancePlanStandardComponentIdentification")
  private InsurancePlanStandardComponentIdentification InsurancePlanStandardComponentIdentification;
  @JsonProperty("Issuer")
  
  private Object Issuer;
  public void setInsurancePlanVariant(InsurancePlanVariant InsurancePlanVariant){
   this.InsurancePlanVariant=InsurancePlanVariant;
  }
  public InsurancePlanVariant getInsurancePlanVariant(){
   return InsurancePlanVariant;
  }
  public void setInsurancePlanStandardComponentIdentification(InsurancePlanStandardComponentIdentification InsurancePlanStandardComponentIdentification){
   this.InsurancePlanStandardComponentIdentification=InsurancePlanStandardComponentIdentification;
  }
  public InsurancePlanStandardComponentIdentification getInsurancePlanStandardComponentIdentification(){
   return InsurancePlanStandardComponentIdentification;
  }
  public void setIssuer(Object Issuer){
   this.Issuer=Issuer;
  }
  public Object getIssuer(){
   return Issuer;
  }
}