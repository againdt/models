package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="batchTransmission")
@XmlAccessorType(XmlAccessType.NONE)
public class FilePayloadBatchTransmission implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="batchID")
	private String batchID = null;
	
	@XmlElement(name="originalBatchID")
	private String originalBatchID = null;
	
	public String getOriginalBatchID() {
		return originalBatchID;
	}
	public void setOriginalBatchID(String originalBatchID) {
		this.originalBatchID = originalBatchID;
	}
	@XmlElement(name="reportPeriod")
	private String reportPeriod = null;
	
	@XmlElement(name="batchAttachmentTotalQuantity")
	private Integer batchAttachmentTotalQuantity = null;
	
	public List<Attachment> getAttachment() {
		return attachment;
	}
	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}
	@XmlElement(name="batchCategoryCode")
	private String batchCategoryCode = null;
	
	@XmlElement(name="batchTransmissionQuantity")
	private Integer batchTransmissionQuantity = null;
	
	public Integer getBatchTransmissionQuantity() {
		return batchTransmissionQuantity;
	}
	public void setBatchTransmissionQuantity(Integer batchTransmissionQuantity) {
		this.batchTransmissionQuantity = batchTransmissionQuantity;
	}
	@XmlElement(name="transmissionAttachmentQuantity")	
	private Integer transmissionAttachmentQuantity = 0;
	
	@XmlElement(name="transmissionSequenceID")
	private Integer transmissionSequenceID = 0;	
	
	@XmlElement(name="batchPartnerID")
	private String batchPartnerID = null;
	
	@XmlElement(name="MD5ChecksumText")
	private String MD5ChecksumText = null;
	
	@XmlElement(name="attachment")
	@OneToMany(mappedBy="FilePayloadBatchTransmission", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Attachment> attachment;
	
	@XmlElement(name="binarySizeValue")
	private Integer binarySizeValue = 0;
	
	
	@XmlElement(name="documentSequenceID")
	private Integer DocumentSequenceID = 0;
	
	public Integer getBinarySizeValue() {
		return binarySizeValue;
	}
	public void setBinarySizeValue(Integer binarySizeValue) {
		this.binarySizeValue = binarySizeValue;
	}
	@XmlElement(name="documentFileName")
	private String documentFileName = null; 
	
	public String getBatchID() {
		return batchID;
	}
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	public String getBatchPartnerID() {
		return batchPartnerID;
	}
	public void setBatchPartnerID(String batchPartnerID) {
		this.batchPartnerID = batchPartnerID;
	}
	public Integer getBatchAttachmentTotalQuantity() {
		return batchAttachmentTotalQuantity;
	}
	public void setBatchAttachmentTotalQuantity(Integer batchAttachmentTotalQuantity) {
		this.batchAttachmentTotalQuantity = batchAttachmentTotalQuantity;
	}
	public String getBatchCategoryCode() {
		return batchCategoryCode;
	}
	public void setBatchCategoryCode(String batchCategoryCode) {
		this.batchCategoryCode = batchCategoryCode;
	}
	
	public Integer getTransmissionAttachmentQuantity() {
		return transmissionAttachmentQuantity;
	}
	public void setTransmissionAttachmentQuantity(
			Integer transmissionAttachmentQuantity) {
		this.transmissionAttachmentQuantity = transmissionAttachmentQuantity;
	}	
	public Integer getTransmissionSequenceID() {
		return transmissionSequenceID;
	}
	public void setTransmissionSequenceID(Integer transmissionSequenceID) {
		this.transmissionSequenceID = transmissionSequenceID;
	}
	public String getMD5ChecksumText() {
		return MD5ChecksumText;
	}
	public void setMD5ChecksumText(String mD5ChecksumText) {
		MD5ChecksumText = mD5ChecksumText;
	}
	public String getDocumentFileName() {
		return documentFileName;
	}
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}
	public Integer getDocumentSequenceID() {
		return DocumentSequenceID;
	}
	public void setDocumentSequenceID(Integer documentSequenceID) {
		DocumentSequenceID = documentSequenceID;
	}	
	public String getReportPeriod() {
		return reportPeriod;
	}
	public void setReportPeriod(String reportPeriod) {
		this.reportPeriod = reportPeriod;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
