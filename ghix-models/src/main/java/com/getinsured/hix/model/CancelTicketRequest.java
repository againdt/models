package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

public class CancelTicketRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Integer> subModuleIds;
	private Integer ticketId;
	private String comment;
	private Integer lastUpdatedBy;
	public enum SubModuleName {APPLICANTEVENT};
	
	private SubModuleName subModuleName;

	public enum IndAppealReason
	{
		MedicaidCHP("Medicaid-CHP"), Qhp("QHP"),AptcCsr("APTC-CSR"), AptcAmount("APTC Amount"),
		Flp("FLP"), IncorrectAmount("Incorrect Amount"), FamilyComposition("Family Composition"), Other("Other");
		
		@SuppressWarnings("unused")
		private final String appealReason;

		private IndAppealReason(String reason) {
			appealReason = reason;
		}
	}
	
	public enum Email_Config
	{
		Open("Open"), Create("Create"),Complete("Complete"), Cancel("Cancel"),SecureInbox("SecureInbox");
		
		@SuppressWarnings("unused")
		private final String ticket_status;

		private Email_Config(String status) {
			ticket_status = status;
		}
	}

	public List<Integer> getSubModuleIds() {
		return subModuleIds;
	}

	public void setSubModuleIds(List<Integer> subModuleIds) {
		this.subModuleIds = subModuleIds;
	}

	public Integer getTicketId() {
		if(ticketId == null){
			ticketId=0;
		}
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	public SubModuleName getSubModuleName() {
		return subModuleName;
	}

	public void setSubModuleName(SubModuleName subModuleName) {
		this.subModuleName = subModuleName;
	}

}
