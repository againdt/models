package com.getinsured.hix.dto.directenrollment;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;

import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * This class implements compare function with following requirement
 * 1. primary will always be first in the list (if present)
 * 2. spouse will always be followed by primary(if present) or take first slot
 * 3. Kids will be sorted by oldest first
 * @author root
 *
 */
public class DirectEnrollementDescComparator implements Comparator<DirectEnrollmentMemberDetailsDTO>{

	@Override
	public int compare(DirectEnrollmentMemberDetailsDTO obj1,
			DirectEnrollmentMemberDetailsDTO obj2) {
		
		
		
		/**
		 * SELF always takes precedence
		 */
		if(MemberRelationships.SELF.equals(obj1.getRelationshipToPrimary())){
			return -1;
		}
		
		if(MemberRelationships.SELF.equals(obj2.getRelationshipToPrimary())){
			return 1;
		}
		
		if(MemberRelationships.SPOUSE.equals(obj1.getRelationshipToPrimary())){
			return -1;
		}
		
		if(MemberRelationships.SPOUSE.equals(obj2.getRelationshipToPrimary())){
			return 1;
		}
		
		Date obj1date = null;
		Date obj2date = null;
		try {
			obj1date = DirectEnrollmentMemberDetailsDTO.MMDDYYYY.parse(obj1.getDateOfBirth());
		} catch (ParseException e) {
			return 1;
		};
		try {
			obj2date = DirectEnrollmentMemberDetailsDTO.MMDDYYYY.parse(obj2.getDateOfBirth());
		} catch (ParseException e) {
			return -1;
		};
		
		return (obj1date.compareTo(obj2date));
		
	}

}
