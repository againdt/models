package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class PlanRateListResponseDTO extends GHIXResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<PlanRateDTO> planRateList;
	private Integer totalRecCount;
	private Integer PlanId;

	public List<PlanRateDTO> getPlanRateList() {
		return planRateList;
	}

	public void setPlanRateList(List<PlanRateDTO> planRateList) {
		this.planRateList = planRateList;
	}

	public void addPlanRate(PlanRateDTO planRate) {
		if(null == this.planRateList) {
			this.planRateList = new ArrayList<PlanRateDTO>();
		}
		this.planRateList.add(planRate);
	}

	public Integer getTotalRecCount() {
		return totalRecCount;
	}

	public void setTotalRecCount(Integer totalRecCount) {
		this.totalRecCount = totalRecCount;
	}

	public Integer getPlanId() {
		return PlanId;
	}

	public void setPlanId(Integer planId) {
		PlanId = planId;
	}

}
