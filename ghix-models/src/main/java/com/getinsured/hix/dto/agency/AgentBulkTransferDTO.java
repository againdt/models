package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

public class AgentBulkTransferDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<String> agentIds;
	private String targetAgentId;
	private String action;
	private List<DelegatedIndividualDTO> individualIds;
	
	public List<String> getAgentIds() {
		return agentIds;
	}
	public void setAgentIds(List<String> agentIds) {
		this.agentIds = agentIds;
	}
	public String getTargetAgentId() {
		return targetAgentId;
	}
	public void setTargetAgentId(String targetAgentId) {
		this.targetAgentId = targetAgentId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<DelegatedIndividualDTO> getIndividualIds() {
		return individualIds;
	}
	public void setIndividualIds(List<DelegatedIndividualDTO> individualIds) {
		this.individualIds = individualIds;
	}
	
	
}
