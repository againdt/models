--liquibase formatted sql

-- Delete role permissions for the OPERATIONS (if exists any)

delete from role_permissions where role_id in (select id from roles where name='OPERATIONS');

-- Insert the role permissions for the OPERATIONS

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ACCOUNT_SWITCH_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ACCOUNT_SWITCH_EMPLOYEE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADD_CONSUMER_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADD_EMPLOYEE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADD_NEW_ISSUER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADD_REPRESENTATIVE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADMIN_CREATE_NOTICES')));  
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADMIN_EDIT_NOTICES')));  
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('ADMIN_MANAGE_NOTICES')));  
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('APPEALS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_ADD_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_ADD_COMMENT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_ADD_ESIGNATURE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_ADD_PHOTO')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_ADD_TICKET')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_APPROVE_EMPLOYER_CONSUMER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_DELETE_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_DESIGNATE_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_DESIGNATE_EMPLOYER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_DE_DESIGNATE_EMPLOYER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_EDIT_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_EDIT_PAYMENTINFO')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_COMMENT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_CONSUMER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_EMPLOYEE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_EMPLOYER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_LANGUAGE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('BROKER_VIEW_PAYMENTINFO')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('COMPLAINTS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('CONTRIBUTION_SELECTION')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('DOWNLOAD_ACCOUNT_PAYABLE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('DOWNLOAD_ACCOUNT_RECEIVABLE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('DOWNLOAD_PAYMENT_TRANSACTION')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('DOWNLOAD_QHP_RECEIVABLE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('DOWNLOAD_REMITTENCE_REPORT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EDIT_APP_CONFIG')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EDIT_EMPLOYEE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EDIT_PLAN_BENEFITS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EDIT_PLAN_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EDIT_PLAN_RATES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EDIT_REPRESENTATIVE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EMPLOYER_PLAN_SELECTION_WIZARD')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('EMPLOYER_REGISTRATION_WIZARD')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_ASSISTER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_AUTHORIZED_REPRESENTATIVES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_BROKER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_EMPLOYEE_LIST')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_FINANCIAL_INFORMATION')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_ISSUER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_QHP_PLANS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_SADP_PLANS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_USERS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('MANAGE_USER_OPS')));  
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('PARTICIPATION_REPORT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('PAYMENT_REPORT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('REISSUE_INVOICE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('RENEW_COVERAGE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('RESEND_ACTIVATION_LINK')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('REVIEW_EMPLOYEE_LIST')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SAVE_ECOMMITMENT_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SEARCH_CONSUMERS_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SEARCH_EMPLOYEE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SEARCH_EMPLOYEE_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SEARCH_EMPLOYER_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SWITCH_TO_CONSUMER_VIEW_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SWITCH_TO_EMPLOYEE_VIEW_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('SWITCH_TO_EMPLOYER_VIEW_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_EDIT_QUEUE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_LIST_QUEUE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_LIST_TICKET')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_SAVE_TICKET')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_VIEW_QUEUE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('TKM_VIEW_TICKET')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('UPDATE_CERTIFICATION_STATUS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('UPDATE_ENROLLMENT_AVAILABILITY')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('UPDATE_PROVIDER_NETWORK_ID')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('UPLOAD_NEW_BENEFITS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('UPLOAD_NEW_RATES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ACCOUNT_INFORMATION')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ACCOUNT_PAYABLE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ACCOUNT_RECEIVABLE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ACTIVE_EMP_INVOICE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_AUTHORIZED_REPRESENTATIVES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_AVAILABLE_PLANS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_CERTIFICATION_STATUS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_CONSUMER_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_DEPENDENT_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ECOMMITMENT_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ELIGIBILITY_RESULTS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_EMPLOYEE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_EMPLOYEE_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_EMPLOYER_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_EMPLOYER_DASHBOARD')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ENROLLMENT_AVAILABILITY')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_ENROLLMENT_REPORT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_INVOICES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_INVOICES_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PAYMENTS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PAYMENT_MONITOR_REPORT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PLAN_BENEFITS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PLAN_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PLAN_HISTORY')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PLAN_RATES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PLAN_SELECTIONS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_PROVIDER_NETWORK_ID')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_QHP_ACCOUNT_RECEIVABLE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions   where name=UPPER('VIEW_REMITTENCE_REPORT'))); 
 
 
