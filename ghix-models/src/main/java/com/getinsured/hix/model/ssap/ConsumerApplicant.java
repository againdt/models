package com.getinsured.hix.model.ssap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.eligibility.ConsumerEligibilityProgram;


/**
 * The persistent class for the SSAP_APPLICANTS database table.
 *
 */
@Entity
@Table(name="SSAP_APPLICANTS")
@NamedQuery(name="ConsumerApplicant.findAll", query="SELECT s FROM ConsumerApplicant s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsumerApplicant implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public enum BooleanFlag	{ YES, NO;}
	public enum SmokerFlag {Y, N};
   	public enum EligibilityProgramValues { MEDICAID, MEDICARE, CHIP, APTC, QHP, BLANK, CSR ;}
	public enum EligibilityStateValues { INITIAL_ELIGIBILITY_NEEDED , INITIAL_ELIGIBILITY_COMPLETE , FINAL_ELIGIBILITY_COMPLETE;}
	public enum Gender{ M, F;	}
	

	@Id
	@SequenceGenerator(name="SSAP_APPLICANTS_ID_GENERATOR", sequenceName="SSAP_APPLICANTS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICANTS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="APPLYING_FOR_COVERAGE", length=1)
	private String applyingForCoverage;

	@Temporal(TemporalType.DATE)
	@Column(name="BIRTH_DATE")
	private Date birthDate;

	@Column(name="CITIZENSHIP_IMMIGRATION_STATUS", length=50)
	private String citizenshipImmigrationStatus;

	@Column(name="CITIZENSHIP_IMMIGRATION_VID", precision=22)
	private BigDecimal citizenshipImmigrationVid;

	@Column(name="CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@Column(name="DEATH_STATUS", length=10)
	private String deathStatus;

	@Column(name="DEATH_VERIFICATION_VID", precision=22)
	private BigDecimal deathVerificationVid;

	@Column(name="EXTERNAL_APPLICANT_ID", length=100)
	private String externalApplicantId;

	@Column(name="FIRST_NAME", length=100)
	private String firstName;
	
	@Column(name="SUFFIX", length=100)
	private String suffix;

	@Column(length=10)
	private String gender;

	@Column(name="HOUSEHOLD_CONTACT_FLAG", length=5)
	private String householdContactFlag;

	//@Column(name="HOUSEHOLD_CONTACT_VID", precision=22)
	//private BigDecimal householdContactVid;

	@Column(name="INCARCERATION_STATUS", length=50)
	private String incarcerationStatus;

	@Column(name="INCARCERATION_VID", precision=22)
	private BigDecimal incarcerationVid;

	@Column(name="INCOME_VERIFICATION_STATUS", length=50)
	private String incomeVerificationStatus;

	@Column(name="INCOME_VERIFICATION_VID", precision=22)
	private BigDecimal incomeVerificationVid;

	@Column(name="LAST_NAME", length=100)
	private String lastName;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;
/*
	@Column(name="MAILIING_LOCATION_ID", precision=22)
	private BigDecimal mailiingLocationId;*/
	
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name="MAILIING_LOCATION_ID")
	private Location mailingLocation;
	
	@Column(length=10)
	private String married;

	@Column(name="MEC_VERIFICATION_STATUS", length=50)
	private String mecVerificationStatus;

	@Column(name="MEC_VERIFICATION_VID", precision=22)
	private BigDecimal mecVerificationVid;

	@Column(name="MIDDLE_NAME", length=100)
	private String middleName;
/*
	@Column(name="OTHER_LOCATION_ID", precision=22)
	private BigDecimal otherLocationId;*/
	
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name="PRIMARY_LOC_ID")
	private Location otherLocation;

	@Column(name="RESIDENCY_STATUS", length=50)
	private String residencyStatus;

	@Column(name="RESIDENCY_VID", precision=22)
	private BigDecimal residencyVid;

	@Column(name="SSN_VERIFICATION_STATUS", length=50)
	private String ssnVerificationStatus;

	@Column(name="SSN_VERIFICATION_VID", precision=22)
	private BigDecimal ssnVerificationVid;

	@Column(length=1)
	private String tobaccouser;

	@Column(name="PERSON_ID", precision=22)
	private Long personId;
	
	//bi-directional many-to-one association to SsapApplication
	//@ManyToOne(fetch=FetchType.LAZY)
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SSAP_APPLICATION_ID", nullable=false)
	private ConsumerApplication ssapApplication;
	
	@OneToMany(mappedBy="ssapApplicant", cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	private List<ConsumerEligibilityProgram> eligibilityPrograms;

	@Column(name="SSN")
	private String ssn;
	
	@Column(name="PHONE_NUMBER")
    private String phoneNumber;
	
	@Column(name="EMAIL_ADDRESS")
    private String email;
	
	@Column(name="CSR_LEVEL")
    private String csrLevel;
	
	@Column(name="MONTHLY_APTC_AMOUNT")
    private Integer monthlyAptcAmount;
	
	@Column(name="ELIGIBILITY_STATUS")
	private String eligibilityStatus;
	
	@Column(name="FULL_TIME_STUDENT")
	@Enumerated(EnumType.STRING)
    private BooleanFlag fullTimeStudent;


	@Column(name = "RELATIONSHIP")
	private String relationship;
	
	@Column(name = "PREFERRED_TIME_TO_CONTACT")
	private String preferredTimeToContact;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
	@Column(name = "FFM_HOUSEHOLD_RESPONSE")
	private String fFMHouseholdResponse;

	public ConsumerApplicant() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplyingForCoverage() {
		return this.applyingForCoverage;
	}

	public void setApplyingForCoverage(String applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCitizenshipImmigrationStatus() {
		return this.citizenshipImmigrationStatus;
	}

	public void setCitizenshipImmigrationStatus(String citizenshipImmigrationStatus) {
		this.citizenshipImmigrationStatus = citizenshipImmigrationStatus;
	}

	public BigDecimal getCitizenshipImmigrationVid() {
		return this.citizenshipImmigrationVid;
	}

	public void setCitizenshipImmigrationVid(BigDecimal citizenshipImmigrationVid) {
		this.citizenshipImmigrationVid = citizenshipImmigrationVid;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getDeathStatus() {
		return this.deathStatus;
	}

	public void setDeathStatus(String deathStatus) {
		this.deathStatus = deathStatus;
	}

	public BigDecimal getDeathVerificationVid() {
		return this.deathVerificationVid;
	}

	public void setDeathVerificationVid(BigDecimal deathVerificationVid) {
		this.deathVerificationVid = deathVerificationVid;
	}

	public String getExternalApplicantId() {
		return this.externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHouseholdContactFlag() {
		return this.householdContactFlag;
	}

	public void setHouseholdContactFlag(String householdContactFlag) {
		this.householdContactFlag = householdContactFlag;
	}

	/*public BigDecimal getHouseholdContactVid() {
		return this.householdContactVid;
	}

	public void setHouseholdContactVid(BigDecimal householdContactVid) {
		this.householdContactVid = householdContactVid;
	}*/

	public String getIncarcerationStatus() {
		return this.incarcerationStatus;
	}

	public void setIncarcerationStatus(String incarcerationStatus) {
		this.incarcerationStatus = incarcerationStatus;
	}

	public BigDecimal getIncarcerationVid() {
		return this.incarcerationVid;
	}

	public void setIncarcerationVid(BigDecimal incarcerationVid) {
		this.incarcerationVid = incarcerationVid;
	}

	public String getIncomeVerificationStatus() {
		return this.incomeVerificationStatus;
	}

	public void setIncomeVerificationStatus(String incomeVerificationStatus) {
		this.incomeVerificationStatus = incomeVerificationStatus;
	}

	public BigDecimal getIncomeVerificationVid() {
		return this.incomeVerificationVid;
	}

	public void setIncomeVerificationVid(BigDecimal incomeVerificationVid) {
		this.incomeVerificationVid = incomeVerificationVid;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	/*public BigDecimal getMailiingLocationId() {
		return this.mailiingLocationId;
	}

	public void setMailiingLocationId(BigDecimal mailiingLocationId) {
		this.mailiingLocationId = mailiingLocationId;
	}*/

	public String getMarried() {
		return this.married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public String getMecVerificationStatus() {
		return this.mecVerificationStatus;
	}

	public void setMecVerificationStatus(String mecVerificationStatus) {
		this.mecVerificationStatus = mecVerificationStatus;
	}

	public BigDecimal getMecVerificationVid() {
		return this.mecVerificationVid;
	}

	public void setMecVerificationVid(BigDecimal mecVerificationVid) {
		this.mecVerificationVid = mecVerificationVid;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/*public BigDecimal getOtherLocationId() {
		return this.otherLocationId;
	}

	public void setOtherLocationId(BigDecimal otherLocationId) {
		this.otherLocationId = otherLocationId;
	}*/

	public String getResidencyStatus() {
		return this.residencyStatus;
	}

	public void setResidencyStatus(String residencyStatus) {
		this.residencyStatus = residencyStatus;
	}

	public BigDecimal getResidencyVid() {
		return this.residencyVid;
	}

	public void setResidencyVid(BigDecimal residencyVid) {
		this.residencyVid = residencyVid;
	}

	public String getSsnVerificationStatus() {
		return this.ssnVerificationStatus;
	}

	public void setSsnVerificationStatus(String ssnVerificationStatus) {
		this.ssnVerificationStatus = ssnVerificationStatus;
	}

	public BigDecimal getSsnVerificationVid() {
		return this.ssnVerificationVid;
	}

	public void setSsnVerificationVid(BigDecimal ssnVerificationVid) {
		this.ssnVerificationVid = ssnVerificationVid;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	/*public String getVlpVerificationStatus() {
		return vlpVerificationStatus;
	}

	public void setVlpVerificationStatus(String vlpVerificationStatus) {
		this.vlpVerificationStatus = vlpVerificationStatus;
	}*/

	public String getTobaccouser() {
		return this.tobaccouser;
	}

	public void setTobaccouser(String tobaccouser) {
		this.tobaccouser = tobaccouser;
	}

	public ConsumerApplication getSsapApplication() {
		return this.ssapApplication;
	}

	public void setSsapApplication(ConsumerApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public Integer getMonthlyAptcAmount() {
		return monthlyAptcAmount;
	}

	public void setMonthlyAptcAmount(Integer monthlyAptcAmount) {
		this.monthlyAptcAmount = monthlyAptcAmount;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public Location getMailingLocation() {
		return mailingLocation;
	}

	public void setMailingLocation(Location mailingLocation) {
		this.mailingLocation = mailingLocation;
	}

	public Location getOtherLocation() {
		return otherLocation;
	}

	public void setOtherLocation(Location otherLocation) {
		this.otherLocation = otherLocation;
	}

	public List<ConsumerEligibilityProgram> getEligibilityPrograms() {
		return eligibilityPrograms;
	}

	public void setEligibilityPrograms(List<ConsumerEligibilityProgram> eligibilityPrograms) {
		this.eligibilityPrograms = eligibilityPrograms;
	}

	public BooleanFlag getFullTimeStudent() {
		return fullTimeStudent;
	}

	public void setFullTimeStudent(BooleanFlag fullTimeStudent) {
		this.fullTimeStudent = fullTimeStudent;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getPreferredTimeToContact() {
		return preferredTimeToContact;
	}

	public void setPreferredTimeToContact(String preferredTimeToContact) {
		this.preferredTimeToContact = preferredTimeToContact;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(new TSDate().getTime()));
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public String getfFMHouseholdResponse() {
		return fFMHouseholdResponse;
	}

	public void setfFMHouseholdResponse(String fFMHouseholdResponse) {
		this.fFMHouseholdResponse = fFMHouseholdResponse;
	}

}
