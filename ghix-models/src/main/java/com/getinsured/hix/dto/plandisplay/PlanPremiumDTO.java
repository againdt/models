package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PlanTab;

public class PlanPremiumDTO {
	String hiosId;
	Float netPremium;
	private PlanTab planTab;
	
	public String getHiosId() {
		return hiosId;
	}
	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}
	public Float getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}
	public PlanTab getPlanTab() {
		return planTab;
	}
	public void setPlanTab(PlanTab planTab) {
		this.planTab = planTab;
	}
	
}
