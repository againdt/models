package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class PlanSearchResponseDTO  extends GHIXResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<PlanResponseDTO> planResponseDTOList = new ArrayList<PlanResponseDTO>();
	int totalNumOfPlans;
	Long csrPlanCount;
	int updatedCount;
	List<PlanLightDTO> updatedPlans = new ArrayList<PlanLightDTO>();
	List<PlanLightDTO> failedPlans  = new ArrayList<PlanLightDTO>();
	String issuerLogoURL;
  	
	public List<PlanResponseDTO> getPlanResponseDTOList() {
		return planResponseDTOList;
	}
	public void setPlanResponseDTOList(List<PlanResponseDTO> planResponseDTOList) {
		this.planResponseDTOList = planResponseDTOList;
	}
	public int getTotalNumOfPlans() {
		return totalNumOfPlans;
	}
	public void setTotalNumOfPlans(int totalNumOfPlans) {
		this.totalNumOfPlans = totalNumOfPlans;
	}
	public Long getCsrPlanCount() {
		return csrPlanCount;
	}
	public void setCsrPlanCount(Long csrPlanCount) {
		this.csrPlanCount = csrPlanCount;
	}
	public int getUpdatedCount() {
		return updatedCount;
	}
	public void setUpdatedCount(int updatedCount) {
		this.updatedCount = updatedCount;
	}
	public List<PlanLightDTO> getUpdatedPlans() {
		return updatedPlans;
	}
	public void setUpdatedPlans(List<PlanLightDTO> updatedPlans) {
		this.updatedPlans = updatedPlans;
	}
	public List<PlanLightDTO> getFailedPlans() {
		return failedPlans;
	}
	public void setFailedPlans(List<PlanLightDTO> failedPlans) {
		this.failedPlans = failedPlans;
	}
	public String getIssuerLogoURL() {
		return issuerLogoURL;
	}
	public void setIssuerLogoURL(String issuerLogoURL) {
		this.issuerLogoURL = issuerLogoURL;
	}
	
	
	

}
