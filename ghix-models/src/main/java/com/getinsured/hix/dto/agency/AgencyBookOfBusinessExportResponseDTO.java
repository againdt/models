package com.getinsured.hix.dto.agency;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgencyBookOfBusinessExportResponseDTO {
	int responseCode;
	String status;
	byte[] byteStream;
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public byte[] getByteStream() {
		return byteStream;
	}
	public void setByteStream(byte[] byteStream) {
		this.byteStream = byteStream;
	}
	
	
}
