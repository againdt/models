package com.getinsured.hix.dto.planmgmt.microservice;

public class SearchDTO {

	private String sortBy;
	private String sortOrder;
	private String changeOrder;
	private String pageNumber;
	private Integer startRecord;

	public SearchDTO() {
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getChangeOrder() {
		return changeOrder;
	}

	public void setChangeOrder(String changeOrder) {
		this.changeOrder = changeOrder;
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getStartRecord() {
		return startRecord;
	}

	public void setStartRecord(Integer startRecord) {
		this.startRecord = startRecord;
	}
}
