CREATE OR REPLACE PROCEDURE SETEFFECTIVEDATESCANCELTERM
		IS
		BEGIN
		FOR enroll IN (SELECT ID FROM ENROLLMENT WHERE TOT_INDV_RESP_EFF_DATE IS NULL AND TOT_EMPLR_RESP_EFF_DATE IS NULL AND TOT_PREM_EFF_DATE IS NULL AND  ENROLLMENT_STATUS_LKP
		IN(SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE
		IN('CANCEL','TERM')) ORDER BY ID)
		LOOP
		DECLARE
		enrollmentID NUMBER ;
		BEGIN
		enrollmentID:=enroll.ID;
		dbms_output.put_line('Start Updating for Enrollment = ' || enrollmentID);
		--update Enrollee and Enrollee_Aud HEALTH_COV_PREM_EFF_DATE
		UPDATE ENROLLEE SET HEALTH_COV_PREM_EFF_DATE=EFFECTIVE_START_DATE where
		enrollment_id=enrollmentID and PERSON_TYPE_LKP NOT IN(SELECT
		LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE
		IN('CUSTODIAL_PARENT','RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT'));
		UPDATE ENROLLEE_AUD SET HEALTH_COV_PREM_EFF_DATE=EFFECTIVE_START_DATE
		where enrollment_id=enrollmentID and PERSON_TYPE_LKP NOT IN(SELECT
		LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE
		IN('CUSTODIAL_PARENT','RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT'));
		DECLARE
		lastEffectiveDate TIMESTAMP;
		lastRev NUMBER :=0;
		BEGIN
		--Loop through revision for the enrollment
		FOR revision IN
		(SELECT REV
		FROM ENROLLMENT_AUD WHERE ID=enrollmentID ORDER BY REV ASC)
		LOOP
		--check if it had any special enrollment
		IF lastRev>0 THEN
		DECLARE
		eventID NUMBER;
		recordCount NUMBER;
		BEGIN
		SELECT COUNT(*) INTO recordCount FROM ENROLLEE_AUD ene, ENROLLMENT_EVENT eve
		where ene.last_event_id=eve.id and ene.ENROLLMENT_REASON='S' and
		eve.EVENT_TYPE_LKP in (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE where
		lookup_value_code ='021') and ene.PERSON_TYPE_LKP NOT IN(SELECT
		LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE
		IN('CUSTODIAL_PARENT','RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT')) and
		eve.SPCL_ENROLLMENT_REASON_LKP IS NOT NULL and
		ene.enrollment_id=enrollmentID and ene.rev>lastRev and
		ene.rev<=revision.rev;

		IF recordCount>0 THEN
		SELECT MAX(eve.ID) INTO eventID FROM ENROLLEE_AUD ene, ENROLLMENT_EVENT eve
		where ene.last_event_id=eve.id and ene.ENROLLMENT_REASON='S' and
		eve.EVENT_TYPE_LKP in (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE where
		lookup_value_code ='021') and
		ene.PERSON_TYPE_LKP NOT IN(SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE
		LOOKUP_VALUE_CODE
		IN('CUSTODIAL_PARENT','RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT')) and
		eve.SPCL_ENROLLMENT_REASON_LKP IS NOT NULL and
		ene.enrollment_id=enrollmentID and ene.rev>lastRev and
		ene.rev<=revision.rev;

		--Find startdate of Enrollee for which the Addition event occured
		SELECT EFFECTIVE_START_DATE INTO lastEffectiveDate FROM ENROLLEE WHERE ID =
		(SELECT ENROLLEE_ID FROM ENROLLMENT_EVENT WHERE ID=eventID);
		UPDATE ENROLLMENT_AUD SET TOT_INDV_RESP_EFF_DATE=lastEffectiveDate,
		TOT_EMPLR_RESP_EFF_DATE=lastEffectiveDate,
		TOT_PREM_EFF_DATE=lastEffectiveDate WHERE ID=enrollmentID AND
		REV=revision.rev;

		ELSE
		SELECT COUNT(*) INTO recordCount FROM ENROLLEE_AUD ene, ENROLLMENT_EVENT eve
		where ene.last_event_id=eve.id and ene.ENROLLMENT_REASON='S' and
		eve.EVENT_TYPE_LKP in (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE where
		lookup_value_code ='024') and ene.ENROLLEE_STATUS_LKP IN (SELECT
		LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE IN('TERM'))
		and
		ene.PERSON_TYPE_LKP NOT IN(SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE
		LOOKUP_VALUE_CODE
		IN('CUSTODIAL_PARENT','RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT')) and
		eve.SPCL_ENROLLMENT_REASON_LKP IS NOT NULL and
		ene.enrollment_id=enrollmentID and ene.rev>lastRev and
		ene.rev<=revision.rev;
		IF recordCount>0 THEN
		SELECT MAX(eve.ID) INTO eventID FROM ENROLLEE_AUD ene, ENROLLMENT_EVENT eve
		where ene.last_event_id=eve.id and ene.ENROLLMENT_REASON='S' and
		eve.EVENT_TYPE_LKP in (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE where
		lookup_value_code ='024') and ene.ENROLLEE_STATUS_LKP IN (SELECT
		LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE IN('TERM'))
		and
		ene.PERSON_TYPE_LKP NOT IN(SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE
		LOOKUP_VALUE_CODE
		IN('CUSTODIAL_PARENT','RESPONSIBLE_PERSON','HOUSEHOLD_CONTACT')) and
		eve.SPCL_ENROLLMENT_REASON_LKP IS NOT NULL and
		ene.enrollment_id=enrollmentID and ene.rev>lastRev and
		ene.rev<=revision.rev;

		--Find the next date of effective end date of the Enrollee for which termination event occurred
		SELECT EFFECTIVE_END_DATE+1 INTO lastEffectiveDate FROM ENROLLEE WHERE ID =
		(SELECT ENROLLEE_ID FROM ENROLLMENT_EVENT WHERE ID=eventID);

		UPDATE ENROLLMENT_AUD SET TOT_INDV_RESP_EFF_DATE=lastEffectiveDate,
		TOT_EMPLR_RESP_EFF_DATE=lastEffectiveDate,
		TOT_PREM_EFF_DATE=lastEffectiveDate WHERE ID=enrollmentID AND
		REV=revision.rev;
		ELSE
		UPDATE ENROLLMENT_AUD SET TOT_INDV_RESP_EFF_DATE=lastEffectiveDate,
		TOT_EMPLR_RESP_EFF_DATE=lastEffectiveDate,
		TOT_PREM_EFF_DATE=lastEffectiveDate WHERE ID=enrollmentID AND
		REV=revision.rev;
		END IF;
		END IF;
		END;
		ELSE
		SELECT BENEFIT_EFFECTIVE_DATE INTO lastEffectiveDate FROM ENROLLMENT WHERE
		ID=enrollmentID;
		UPDATE ENROLLMENT_AUD SET TOT_INDV_RESP_EFF_DATE=lastEffectiveDate,
		TOT_EMPLR_RESP_EFF_DATE=lastEffectiveDate,
		TOT_PREM_EFF_DATE=lastEffectiveDate WHERE ID=enrollmentID AND
		REV=revision.rev;
		END IF;
		lastRev :=revision.rev;
		END LOOP;
		--Update the date in enrollment Table
		UPDATE ENROLLMENT SET TOT_INDV_RESP_EFF_DATE=lastEffectiveDate,
		TOT_EMPLR_RESP_EFF_DATE=lastEffectiveDate,
		TOT_PREM_EFF_DATE=lastEffectiveDate WHERE ID=enrollmentID;
		END;
		dbms_output.put_line('End Updating for Enrollment = ' || enrollmentID);
		END;

		END LOOP;
		END;