package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.getinsured.timeshift.TimeShifterUtil;

/**
 * 
 * Entity class for PM_DRUG table.
 * @author vardekar_s
 *
 */

@Entity
@Table(name = "PM_DRUG")
@DynamicInsert
@DynamicUpdate
public class Drug implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_DRUG_SEQ")
	@SequenceGenerator(name = "PM_DRUG_SEQ", sequenceName = "PM_DRUG_SEQ", allocationSize = 1)
	private long id;	
	
	@Column(name = "RXCUI")
	private String rxcui;
	
	@Column(name = "AUTH_REQUIRED")
	private String authRequired;
	
	@Column(name = "STEP_THERAPY_REQUIRED")
	private String stepTherapyRequired;	
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "DRUG_TIER_ID", referencedColumnName = "ID")
	private DrugTier drugTier;
	
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@NotNull
	private Timestamp lastUpdateTimestamp;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis())); 
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRxcui() {
		return rxcui;
	}

	public void setRxcui(String rxcui) {
		this.rxcui = rxcui;
	}

	public String getAuthRequired() {
		return authRequired;
	}

	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}

	public String getStepTherapyRequired() {
		return stepTherapyRequired;
	}

	public void setStepTherapyRequired(String stepTherapyRequired) {
		this.stepTherapyRequired = stepTherapyRequired;
	}

	public DrugTier getDrugTier() {
		return drugTier;
	}

	public void setDrugTier(DrugTier drugTier) {
		this.drugTier = drugTier;
	}	

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
}
