package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class PersonMarriedIndicator{
  @JsonProperty("Value")
  
  private Boolean Value;
  public void setValue(Boolean Value){
   this.Value=Value;
  }
  public Boolean getValue(){
   return Value;
  }
}