package com.getinsured.hix.model.enrollment;

public enum EnrollmentAttributeEnum {
	
	id("id"),
	sponsorName("sponsorName"),
	sponsorEIN("sponsorEIN"),
	sponsorTaxIdNumber("sponsorTaxIdNumber"),
	subscriberName("subscriberName"),
	enrollmentStatusCode("enrollmentStatusLkp.lookupValueCode"),
	enrollmentStatusLabel("enrollmentStatusLkp.lookupValueLabel"),	
	benefitEffectiveDate("benefitEffectiveDate"),
	benefitEndDate("benefitEndDate"),
	grossPremiumAmt("grossPremiumAmt"),
	aptcAmt("aptcAmt"),
	slcspAmt("slcspAmt"),
	netPremiumAmt("netPremiumAmt"),
	insurerName("insurerName"), 
	carrierAppId("carrierAppId");

	
    private final String name;       

    private EnrollmentAttributeEnum(String s) {
        name = s;
    }
    
    public String toString() {
	       return this.name;
	}

}
