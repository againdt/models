package com.getinsured.hix.dto.planmgmt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class PlanBulkUpdateRequestDTO {

	private List<Integer> planIdList;
	private PlanSearchRequestDTO planSearchRequestDTO;
	private String selectedPlanStatus;
	private String selectedIssuerVerificationStatus;
	private String selectedEnrollAvailability;
	private List<String> selectedTenantList;
	private int lastUpdatedBy;

	public PlanBulkUpdateRequestDTO() {
	}

	public List<Integer> getPlanIdList() {
		return planIdList;
	}

	public void setPlanIdInList(Integer planID) {

		if (CollectionUtils.isEmpty(this.planIdList)) {
			this.planIdList = new ArrayList<Integer>();
		}
		this.planIdList.add(planID);
	}

	public PlanSearchRequestDTO getPlanSearchRequestDTO() {
		return planSearchRequestDTO;
	}

	public void setPlanSearchRequestDTO(PlanSearchRequestDTO planSearchRequestDTO) {
		this.planSearchRequestDTO = planSearchRequestDTO;
	}

	public String getSelectedPlanStatus() {
		return selectedPlanStatus;
	}

	public void setSelectedPlanStatus(String selectedPlanStatus) {
		this.selectedPlanStatus = selectedPlanStatus;
	}

	public String getSelectedIssuerVerificationStatus() {
		return selectedIssuerVerificationStatus;
	}

	public void setSelectedIssuerVerificationStatus(String selectedIssuerVerificationStatus) {
		this.selectedIssuerVerificationStatus = selectedIssuerVerificationStatus;
	}

	public String getSelectedEnrollAvailability() {
		return selectedEnrollAvailability;
	}

	public void setSelectedEnrollAvailability(String selectedEnrollAvailability) {
		this.selectedEnrollAvailability = selectedEnrollAvailability;
	}

	public List<String> getSelectedTenantList() {
		return selectedTenantList;
	}

	public void setTenantInList(String tenantCode) {

		if (CollectionUtils.isEmpty(this.selectedTenantList)) {
			this.selectedTenantList = new ArrayList<String>();
		}
		this.selectedTenantList.add(tenantCode);
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
