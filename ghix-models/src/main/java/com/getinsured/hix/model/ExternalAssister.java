package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name = "external_assister")
public class ExternalAssister implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXTERNAL_ASSISTER_SEQ")
	@SequenceGenerator(name = "EXTERNAL_ASSISTER_SEQ", sequenceName = "EXTERNAL_ASSISTER_SEQ", allocationSize = 1)
	@Column(name = "id")
	private int id;

	@Column(name ="name")
	private String name;
	
	@Column(name ="type")
	private String type;
	
	@Column(name ="federal_tax_id")
	private String federalTaxId;
	
	@Column(name ="external_assister_id")
	private String externalAssisterId;
	
	@Column(name ="agent_npn")
	private String agentNpn;
	
	@Column(name ="user_id")
	private Integer userId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFederalTaxId() {
		return federalTaxId;
	}

	public void setFederalTaxId(String federalTaxId) {
		this.federalTaxId = federalTaxId;
	}

	public String getExternalAssisterId() {
		return externalAssisterId;
	}

	public void setExternalAssisterId(String externalAssisterId) {
		this.externalAssisterId = externalAssisterId;
	}

	public String getAgentNpn() {
		return agentNpn;
	}

	public void setAgentNpn(String agentNpn) {
		this.agentNpn = agentNpn;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}
	
}


