package com.getinsured.hix.dto.planmgmt;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;

public class PlanSearchRequestDTO {
	
	String insuranceType;
	String selectedPlanLevel;
	String tenant;
	String planYear;
	String issuerName;
	String issuerPlanNumber;
	String market;
	String status;
	String issuerVerificationStatus;
	String state;
	String exchangeType;
	String issuerBrandNameId;
	String csrVariation;
	String enableForOffExchangeFlow;
	String nonCommissionFlag;
	String selectedPlanName;
	String sortBy;
	SortOrder sortOrder;
	String selectedTenants;
	String csrOffExchangeFlag;
	
	String issuerVerificationStatusForUpdate;
	String planStatusForUpdate;
	String enrollmentAvailForUpdate;
	
	int lastUpdatedBy;
	int startRecord;
	int pageSize;
	int issuerId;
	int resultCount;
	long tenantId;
	
	List<String> selectedCsrVariationList;
	List<Integer> selectedPlanIds;

	public PlanSearchRequestDTO(){
		 
	}
	
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getSelectedPlanLevel() {
		return selectedPlanLevel;
	}
	public void setSelectedPlanLevel(String selectedPlanLevel) {
		this.selectedPlanLevel = selectedPlanLevel;
	}
	public String getTenant() {
		return tenant;
	}
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	public String getPlanYear() {
		return planYear;
	}
	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIssuerVerificationStatus() {
		return issuerVerificationStatus;
	}
	public void setIssuerVerificationStatus(String issuerVerificationStatus) {
		this.issuerVerificationStatus = issuerVerificationStatus;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public String getIssuerBrandNameId() {
		return issuerBrandNameId;
	}
	public void setIssuerBrandNameId(String issuerBrandNameId) {
		this.issuerBrandNameId = issuerBrandNameId;
	}
	public String getCsrVariation() {
		return csrVariation;
	}
	public void setCsrVariation(String csrVariation) {
		this.csrVariation = csrVariation;
	}
	public String getEnableForOffExchangeFlow() {
		return enableForOffExchangeFlow;
	}
	public void setEnableForOffExchangeFlow(String enableForOffExchangeFlow) {
		this.enableForOffExchangeFlow = enableForOffExchangeFlow;
	}
	public String getNonCommissionFlag() {
		return nonCommissionFlag;
	}
	public void setNonCommissionFlag(String nonCommissionFlag) {
		this.nonCommissionFlag = nonCommissionFlag;
	}
	public String getSelectedPlanName() {
		return selectedPlanName;
	}
	public void setSelectedPlanName(String selectedPlanName) {
		this.selectedPlanName = selectedPlanName;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	public int getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(int startRecord) {
		this.startRecord = startRecord;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	public int getResultCount() {
		return resultCount;
	}
	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSelectedTenants() {
		return selectedTenants;
	}

	public void setSelectedTenants(String selectedTenants) {
		this.selectedTenants = selectedTenants;
	}

	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(int lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getCsrOffExchangeFlag() {
		return csrOffExchangeFlag;
	}

	public void setCsrOffExchangeFlag(String csrOffExchangeFlag) {
		this.csrOffExchangeFlag = csrOffExchangeFlag;
	}

	public List<String> getSelectedCsrVariationList() {
		return selectedCsrVariationList;
	}

	public void setSelectedCsrVariationList(List<String> selectedCsrVariationList) {
		this.selectedCsrVariationList = selectedCsrVariationList;
	}

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	public String getIssuerVerificationStatusForUpdate() {
		return issuerVerificationStatusForUpdate;
	}

	public void setIssuerVerificationStatusForUpdate(String issuerVerificationStatusForUpdate) {
		this.issuerVerificationStatusForUpdate = issuerVerificationStatusForUpdate;
	}

	public String getPlanStatusForUpdate() {
		return planStatusForUpdate;
	}

	public void setPlanStatusForUpdate(String planStatusForUpdate) {
		this.planStatusForUpdate = planStatusForUpdate;
	}

	public String getEnrollmentAvailForUpdate() {
		return enrollmentAvailForUpdate;
	}

	public void setEnrollmentAvailForUpdate(String enrollmentAvailForUpdate) {
		this.enrollmentAvailForUpdate = enrollmentAvailForUpdate;
	}

	public List<Integer> getSelectedPlanIds() {
		return selectedPlanIds;
	}

	public void setSelectedPlanIds(List<Integer> selectedPlanIds) {
		this.selectedPlanIds = selectedPlanIds;
	}

	

}
