
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Augmented File Deail Type&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for AugmentedFileDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AugmentedFileDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AugmentedFileName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AIRTransmittedRecordsCnt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IRDBReceivedStatusInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IRDBProcessedStatusInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IRDBProcessedRecordsCnt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AugmentedFileDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "augmentedFileName",
    "airTransmittedRecordsCnt",
    "irdbReceivedStatusInd",
    "irdbProcessedStatusInd",
    "irdbProcessedRecordsCnt"
})
public class AugmentedFileDetailType {

    @XmlElement(name = "AugmentedFileName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String augmentedFileName;
    @XmlElement(name = "AIRTransmittedRecordsCnt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String airTransmittedRecordsCnt;
    @XmlElement(name = "IRDBReceivedStatusInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    @XmlSchemaType(name = "string")
    protected YOrNIndType irdbReceivedStatusInd;
    @XmlElement(name = "IRDBProcessedStatusInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    @XmlSchemaType(name = "string")
    protected IRDBProcessedStatusIndType irdbProcessedStatusInd;
    @XmlElement(name = "IRDBProcessedRecordsCnt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String irdbProcessedRecordsCnt;

    /**
     * Gets the value of the augmentedFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAugmentedFileName() {
        return augmentedFileName;
    }

    /**
     * Sets the value of the augmentedFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAugmentedFileName(String value) {
        this.augmentedFileName = value;
    }

    /**
     * Gets the value of the airTransmittedRecordsCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAIRTransmittedRecordsCnt() {
        return airTransmittedRecordsCnt;
    }

    /**
     * Sets the value of the airTransmittedRecordsCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAIRTransmittedRecordsCnt(String value) {
        this.airTransmittedRecordsCnt = value;
    }

    /**
     * Gets the value of the irdbReceivedStatusInd property.
     * 
     * @return
     *     possible object is
     *     {@link YOrNIndType }
     *     
     */
    public YOrNIndType getIRDBReceivedStatusInd() {
        return irdbReceivedStatusInd;
    }

    /**
     * Sets the value of the irdbReceivedStatusInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link YOrNIndType }
     *     
     */
    public void setIRDBReceivedStatusInd(YOrNIndType value) {
        this.irdbReceivedStatusInd = value;
    }

    /**
     * Gets the value of the irdbProcessedStatusInd property.
     * 
     * @return
     *     possible object is
     *     {@link IRDBProcessedStatusIndType }
     *     
     */
    public IRDBProcessedStatusIndType getIRDBProcessedStatusInd() {
        return irdbProcessedStatusInd;
    }

    /**
     * Sets the value of the irdbProcessedStatusInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link IRDBProcessedStatusIndType }
     *     
     */
    public void setIRDBProcessedStatusInd(IRDBProcessedStatusIndType value) {
        this.irdbProcessedStatusInd = value;
    }

    /**
     * Gets the value of the irdbProcessedRecordsCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIRDBProcessedRecordsCnt() {
        return irdbProcessedRecordsCnt;
    }

    /**
     * Sets the value of the irdbProcessedRecordsCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIRDBProcessedRecordsCnt(String value) {
        this.irdbProcessedRecordsCnt = value;
    }

}
