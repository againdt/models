package com.getinsured.hix.model.planmgmt;

import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

public class TeaserPlanResponse extends GHIXResponse {
	
	private double benchmarkPremium;
	private String benchmarkPlanId;
	private String planName;
	private String issuerName;
	private Map<String, HashMap<String, String>> benefits;
	
	private int totalPlans;

	public int getTotalPlans() {
		return totalPlans;
	}

	public void setTotalPlans(int totalPlans) {
		this.totalPlans = totalPlans;
	}

	public double getBenchmarkPremium() {
		return benchmarkPremium;
	}

	public void setBenchmarkPremium(double benchmarkPremium) {
		this.benchmarkPremium = benchmarkPremium;
	}

	public String getBenchmarkPlanId() {
		return benchmarkPlanId;
	}

	public void setBenchmarkPlanId(String benchmarkPlanId) {
		this.benchmarkPlanId = benchmarkPlanId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public Map<String, HashMap<String, String>> getBenefits() {
		return benefits;
	}

	public void setBenefits(Map<String, HashMap<String, String>> benefits) {
		this.benefits = benefits;
	}

}
