/**
 * EmployerRequest.java
 * @author chalse_v
 * @version 1.0
 * @since Mar 22, 2013 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author chalse_v
 * @version 1.0
 * @since Mar 22, 2013 
 *
 */
public class EmployerRequest implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private  ArrayList< ArrayList< ArrayList< Map<String,String> > > >  employeeList;
	private String insType;
	private String effectiveDate;
	private String planIdStr;
	private String empPrimaryZip;
	private String empCountyCode;
	private String ehbCovered;
	private String planLevel;	
	private List<Integer> employeeIdList;	
	/**
	 * Constructor.
	 */
	public EmployerRequest() {
	}

	/**
	 * Getter for 'employeeList'.
	 *
	 * @return the employeeList.
	 */
	public  ArrayList< ArrayList< ArrayList< Map<String,String> > > >  getEmployeeList() {
		return this.employeeList;
	}

	/**
	 * Setter for 'employeeList'.
	 *
	 * @param employeeList the employeeList to set.
	 */
	public void setEmployeeList(ArrayList< ArrayList< ArrayList< Map<String,String> > > >  employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * Getter for 'insType'.
	 *
	 * @return the insType.
	 */
	public String getInsType() {
		return this.insType;
	}

	/**
	 * Setter for 'insType'.
	 *
	 * @param insType the insType to set.
	 */
	public void setInsType(String insType) {
		this.insType = insType;
	}

	/**
	 * Getter for 'effectiveDate'.
	 *
	 * @return the effectiveDate.
	 */
	public String getEffectiveDate() {
		return this.effectiveDate;
	}

	/**
	 * Setter for 'effectiveDate'.
	 *
	 * @param effectiveDate the effectiveDate to set.
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * Getter for 'planIdStr'.
	 *
	 * @return the planIdStr.
	 */
	public String getPlanIdStr() {
		return this.planIdStr;
	}

	/**
	 * Setter for 'planIdStr'.
	 *
	 * @param planIdStr the planIdStr to set.
	 */
	public void setPlanIdStr(String planIdStr) {
		this.planIdStr = planIdStr;
	}

	/**
	 * @return the empPrimaryZip
	 */
	public String getEmpPrimaryZip() {
		return empPrimaryZip;
	}

	/**
	 * @param empPrimaryZip the empPrimaryZip to set
	 */
	public void setEmpPrimaryZip(String empPrimaryZip) {
		this.empPrimaryZip = empPrimaryZip;
	}

	public String getEmpCountyCode() {
		return empCountyCode;
	}

	public void setEmpCountyCode(String empCountyCode) {
		this.empCountyCode = empCountyCode;
	}

	/**
	 * @return the ehbCovered
	 */
	public String getEhbCovered() {
		return ehbCovered;
	}

	/**
	 * @param ehbCovered the ehbCovered to set
	 */
	public void setEhbCovered(String ehbCovered) {
		this.ehbCovered = ehbCovered;
	}

	/**
	 * @return the planLevel
	 */
	public String getPlanLevel() {
		return planLevel;
	}

	/**
	 * @param planLevel the planLevel to set
	 */
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	/**
	 * @return the employeeIdList
	 */
	public List<Integer> getEmployeeIdList() {
		return employeeIdList;
	}

	/**
	 * @param employeeIdList the employeeIdList to set
	 */
	public void setEmployeeIdList(List<Integer> employeeIdList) {
		this.employeeIdList = employeeIdList;
	}

	

	
}
