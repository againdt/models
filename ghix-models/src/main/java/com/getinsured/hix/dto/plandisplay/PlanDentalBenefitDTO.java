package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.PlanDentalBenefit;

public class PlanDentalBenefitDTO extends PlanDentalBenefit{

	private static final long serialVersionUID = 1L;

	private int planDentalId;
	
	private int planId;

	private String effectiveStartDate;
	
	private String effectiveEndDate;
	
	
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public int getPlanDentalId() {
		return planDentalId;
	}

	public void setPlanDentalId(int planDentalId) {
		this.planDentalId = planDentalId;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}
}
