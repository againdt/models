package com.getinsured.hix.dto.planmgmt;

import java.util.Date;
/**
 * DTO for Plan Tenant request
 */
public class PlanTenantLightDTO {
	private long id;
	private int planId;
	private String tenantCode;
	private Date tenantCreatedDate;
	private String tenantName;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getTenantCreatedDate() {
		return tenantCreatedDate;
	}
	public void setTenantCreatedDate(Date tenantCreatedDate) {
		this.tenantCreatedDate = tenantCreatedDate;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}

}
