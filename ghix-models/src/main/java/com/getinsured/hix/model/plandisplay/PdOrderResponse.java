package com.getinsured.hix.model.plandisplay;

import java.util.List;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;

public class PdOrderResponse extends GHIXResponse
{	
	private List<PdOrderItemDTO> pdOrderItemDTOList;
	private Long giWsPayloadId;

	private String globalId;
	
	public List<PdOrderItemDTO> getPdOrderItemDTOList() {
		return pdOrderItemDTOList;
	}
	public void setPdOrderItemDTOList(List<PdOrderItemDTO> pdOrderItemDTOList) {
		this.pdOrderItemDTOList = pdOrderItemDTOList;
	}
	
	public Long getGiWsPayloadId() {
		return giWsPayloadId;
	}
	public void setGiWsPayloadId(Long giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}
	public String getGlobalId() {
		return globalId;
	}
	public void setGlobalId(String globalId) {
		this.globalId = globalId;
	}
	
	
}
