package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerResponseDTO extends GhixResponseDTO {

	private IssuerDTO issuerDTO;

	public IssuerResponseDTO() {
		super();
	}

	/**
	 * @return the issuerDTO
	 */
	public IssuerDTO getIssuerDTO() {
		return issuerDTO;
	}

	/**
	 * @param issuerDTO the issuerDTO to set
	 */
	public void setIssuerDTO(IssuerDTO issuerDTO) {
		this.issuerDTO = issuerDTO;
	}
	
}
