package com.getinsured.hix.model;

import java.io.Serializable;

public class PermissionsDto implements Serializable {
	
	private int id;
	private String name;
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	


}
