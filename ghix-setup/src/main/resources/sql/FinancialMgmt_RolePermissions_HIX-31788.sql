 Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'FINANCE_EDIT_AUTOPAY');
 
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='ADMIN'), (select id from permissions where name=UPPER('MAKE_PAYMENT')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions where name=UPPER('MAKE_PAYMENT')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='EMPLOYER'), (select id from permissions where name=UPPER('MAKE_PAYMENT')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='CSR'), (select id from permissions where name=UPPER('MAKE_PAYMENT')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='BROKER'), (select id from permissions where name=UPPER('MAKE_PAYMENT')));
 
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='ADMIN'), (select id from permissions where name=UPPER('ADD_PAYMENT_METHOD')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions where name=UPPER('ADD_PAYMENT_METHOD')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='EMPLOYER'), (select id from permissions where name=UPPER('ADD_PAYMENT_METHOD')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='CSR'), (select id from permissions where name=UPPER('ADD_PAYMENT_METHOD')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='BROKER'), (select id from permissions where name=UPPER('ADD_PAYMENT_METHOD')));
 
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='ADMIN'), (select id from permissions where name=UPPER('MANAGE_PAYMENT_METHODS')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions where name=UPPER('MANAGE_PAYMENT_METHODS')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='EMPLOYER'), (select id from permissions where name=UPPER('MANAGE_PAYMENT_METHODS')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='CSR'), (select id from permissions where name=UPPER('MANAGE_PAYMENT_METHODS')));
  INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='BROKER'), (select id from permissions where name=UPPER('MANAGE_PAYMENT_METHODS')));
 
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='ADMIN'), (select id from permissions where name=UPPER('FINANCE_EDIT_AUTOPAY')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='OPERATIONS'), (select id from permissions where name=UPPER('FINANCE_EDIT_AUTOPAY')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='EMPLOYER'), (select id from permissions where name=UPPER('FINANCE_EDIT_AUTOPAY')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='CSR'), (select id from permissions where name=UPPER('FINANCE_EDIT_AUTOPAY')));
 INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
 VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
 (select id from roles where name='BROKER'), (select id from permissions where name=UPPER('FINANCE_EDIT_AUTOPAY')));