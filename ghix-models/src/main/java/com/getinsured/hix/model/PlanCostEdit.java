package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Audited
@Entity
@Table(name = "PLAN_COST_EDITS")
public class PlanCostEdit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_COST_EDITS_SEQ")
	@SequenceGenerator(name = "PLAN_COST_EDITS_SEQ", sequenceName = "PLAN_COST_EDITS_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;
	
	@Column(name = "ISSUER_PLAN_NUMBER")
	private String issuerPlanNumber;
	
	@Column(name = "PLAN_COST_ID")
	private Integer planCostId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "APPLICABLE_YEAR")
	private String applicableYear;
	
	@Column(name = "INSURANCE_TYPE")
	private String insuranceType;
	
	@Column(name = "IN_NETWORK_IND")
	private Double inNetworkInd;
	
	@Column(name = "IN_NETWORK_FLY")
	private Double inNetworkFamily;
	
	@Column(name = "OUT_NETWORK_IND")
	private Double outOfNetworkInd;
	
	@Column(name = "OUT_NETWORK_FLY")
	private Double outOfNetworkFamily;
	
	@Column(name = "LIMIT_EXCEP_DISPLAY")
	private String limitExceptionValue;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name = "LAST_UPDATED_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	public PlanCostEdit(){
		
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public Integer getPlanCostId() {
		return planCostId;
	}

	public void setPlanCostId(Integer planCostId) {
		this.planCostId = planCostId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(String applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}


	public Double getInNetworkInd() {
		return inNetworkInd;
	}

	public Double getInNetworkFamily() {
		return inNetworkFamily;
	}

	public Double getOutOfNetworkInd() {
		return outOfNetworkInd;
	}

	public Double getOutOfNetworkFamily() {
		return outOfNetworkFamily;
	}

	public String getLimitExceptionValue() {
		return limitExceptionValue;
	}

	public void setInNetworkInd(Double inNetworkInd) {
		this.inNetworkInd = inNetworkInd;
	}

	public void setInNetworkFamily(Double inNetworkFamily) {
		this.inNetworkFamily = inNetworkFamily;
	}

	public void setOutOfNetworkInd(Double outOfNetworkInd) {
		this.outOfNetworkInd = outOfNetworkInd;
	}

	public void setOutOfNetworkFamily(Double outOfNetworkFamily) {
		this.outOfNetworkFamily = outOfNetworkFamily;
	}

	public void setLimitExceptionValue(String limitExceptionValue) {
		this.limitExceptionValue = limitExceptionValue;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
}
