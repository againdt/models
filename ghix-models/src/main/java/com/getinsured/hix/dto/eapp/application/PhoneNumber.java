package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class PhoneNumber implements Serializable{
	private static final long serialVersionUID = 1L;
	private String number;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	public static PhoneNumber getInstance(String number){
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setNumber(number);
		return phoneNumber;
	}
	
	public static PhoneNumber getSampleInstance(){
		PhoneNumber num = new PhoneNumber();
		num.setNumber("4088008000");
		return num;
	}
	
	
	
}
