package com.getinsured.hix.dto.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.AdjustmentReasonCode;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerPaymentInvoice;
import com.getinsured.hix.model.GHIXResponse;


/**
 * @since 26th June, 2013
 * @author Kuldeep
 *
 */
public class FinanceResponse extends GHIXResponse implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> employerInvoicesMap = null;

	private String reissueInvoiceResponse = null;
	
	private EmployerInvoices employerInvoices = null;
	
	private String processPaymentResposne = null;
	
	private List<EmployerInvoices> employerInvoicesList = null;
	
	private String batchResponse = null;
	
	private int paymentId=0;
	
	private EmployerPaymentInvoice employerPaymentInvoice = null;
	
	private Map<String, Object> dataObject = null;
	
	private BigDecimal totalPremiumAmount = null;
	
	private BigDecimal averageMonthlyPremium = null;
	
	private boolean isTerminationAllowed = Boolean.FALSE;
	
	private List<AdjustmentReasonCode> adjustmentReasonCodeList;
	
	private Integer employerEnrollmentID = null;
	
	private Date coverageStartDate;
	
	public EmployerPaymentInvoice getEmployerPaymentInvoice() {
		return employerPaymentInvoice;
	}

	public void setEmployerPaymentInvoice(
			EmployerPaymentInvoice employerPaymentInvoice) {
		this.employerPaymentInvoice = employerPaymentInvoice;
	}

	public Map<String, Object> getEmployerInvoicesMap() {
		return employerInvoicesMap;
	}

	public void setEmployerInvoicesMap(Map<String, Object> employerInvoicesMap) {
		this.employerInvoicesMap = employerInvoicesMap;
	}

	public String getReissueInvoiceResponse() {
		return reissueInvoiceResponse;
	}

	public void setReissueInvoiceResponse(String reissueInvoiceResponse) {
		this.reissueInvoiceResponse = reissueInvoiceResponse;
	}

	public EmployerInvoices getEmployerInvoices() {
		return employerInvoices;
	}

	public void setEmployerInvoices(EmployerInvoices employerInvoices) {
		this.employerInvoices = employerInvoices;
	}

	public String getProcessPaymentResposne() {
		return processPaymentResposne;
	}

	public void setProcessPaymentResposne(String processPaymentResposne) {
		this.processPaymentResposne = processPaymentResposne;
	}

	public List<EmployerInvoices> getEmployerInvoicesList() {
		return employerInvoicesList;
	}

	public void setEmployerInvoicesList(List<EmployerInvoices> employerInvoicesList) {
		this.employerInvoicesList = employerInvoicesList;
	}

	public String getBatchResponse() {
		return batchResponse;
	}

	public void setBatchResponse(String batchResponse) {
		this.batchResponse = batchResponse;
	}

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	
	public Map<String, Object> getDataObject() {
		return dataObject;
	}

	public void setDataObject(Map<String, Object> dataObject) {
		this.dataObject = dataObject;
	}

	public BigDecimal getTotalPremiumAmount() {
		return totalPremiumAmount;
	}

	public void setTotalPremiumAmount(BigDecimal totalPremiumAmount) {
		this.totalPremiumAmount = totalPremiumAmount;
	}

	public BigDecimal getAverageMonthlyPremium() {
		return averageMonthlyPremium;
	}

	public void setAverageMonthlyPremium(BigDecimal averageMonthlyPremium) {
		this.averageMonthlyPremium = averageMonthlyPremium;
	}

	/**
	 * @return the isTerminationAllowed
	 */
	public boolean isTerminationAllowed() {
		return isTerminationAllowed;
	}

	/**
	 * @param isTerminationAllowed the isTerminationAllowed to set
	 */
	public void setTerminationAllowed(boolean isTerminationAllowed) {
		this.isTerminationAllowed = isTerminationAllowed;
	}

	/**
	 * @return the adjustmentReasonCodeList
	 */
	public List<AdjustmentReasonCode> getAdjustmentReasonCodeList() {
		return adjustmentReasonCodeList;
	}

	/**
	 * @param adjustmentReasonCodeList the adjustmentReasonCodeList to set
	 */
	public void setAdjustmentReasonCodeList(
			List<AdjustmentReasonCode> adjustmentReasonCodeList) {
		this.adjustmentReasonCodeList = adjustmentReasonCodeList;
	}

	/**
	 * @return the employerEnrollmentID
	 */
	public Integer getEmployerEnrollmentID() {
		return employerEnrollmentID;
	}

	/**
	 * @param employerEnrollmentID the employerEnrollmentID to set
	 */
	public void setEmployerEnrollmentID(Integer employerEnrollmentID) {
		this.employerEnrollmentID = employerEnrollmentID;
	}

	/**
	 * @return the coverageStartDate
	 */
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	/**
	 * @param coverageStartDate the coverageStartDate to set
	 */
	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
}
