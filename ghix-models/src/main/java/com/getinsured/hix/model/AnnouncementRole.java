package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity 
@Table(name="announcement_roles")
public class AnnouncementRole implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Announcement_Roles_Seq")
	@SequenceGenerator(name = "Announcement_Roles_Seq", sequenceName = "announcement_roles_seq", allocationSize = 1)
	private int id;
	
	//uni-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name="announcement_id", insertable=true,updatable=true)
	private Announcement announcement;
    
    //uni-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name="role_id")
	private Role role;
	
    public AnnouncementRole(){
    	
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Announcement getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(Announcement announcement) {
		this.announcement = announcement;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
