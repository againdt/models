package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class DelegatedIndividualRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer agentId;
	private Integer individualId;
	private String designationStatus;
	
	
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public Integer getIndividualId() {
		return individualId;
	}
	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}
	public String getDesignationStatus() {
		return designationStatus;
	}
	public void setDesignationStatus(String designationStatus) {
		this.designationStatus = designationStatus;
	}
	
	
}
