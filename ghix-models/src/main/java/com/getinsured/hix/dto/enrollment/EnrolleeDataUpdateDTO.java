package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.platform.util.GhixConstants;

public class EnrolleeDataUpdateDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public static enum TRANSACTION_TYPE{
		ADD, TERM, CHANGE, CANCEL
	}
	
	private String exchgIndivIdentifier;
	private Date   effectiveStartDate;
	private Date   effectiveEndDate;
	private String effectiveStartDateStr;
	private String effectiveEndDateStr;
	private String firstName;
	private String lastName;
	private String middleName;
	private String ssn;
	private String gender;
	private Integer enrolleeId;
	private TRANSACTION_TYPE txnType;
	private String txnReason;
	
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	private void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}
	private void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the effectiveStartDateStr
	 */
	public String getEffectiveStartDateStr() {
		return effectiveStartDateStr;
	}
	/**
	 * @param effectiveStartDateStr the effectiveStartDateStr to set
	 */
	public void setEffectiveStartDateStr(String effectiveStartDateStr)throws ParseException {
		this.effectiveStartDateStr = effectiveStartDateStr;
		if(StringUtils.isNotBlank(effectiveStartDateStr)){
			setEffectiveStartDate(StringToDate(effectiveStartDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
		}
	}
	/**
	 * @return the effectiveEndDateStr
	 */
	public String getEffectiveEndDateStr() {
		return effectiveEndDateStr;
	}
	/**
	 * @param effectiveEndDateStr the effectiveEndDateStr to set
	 */
	public void setEffectiveEndDateStr(String effectiveEndDateStr)throws ParseException {
		this.effectiveEndDateStr = effectiveEndDateStr;
		if(StringUtils.isNotBlank(effectiveEndDateStr)){
			setEffectiveEndDate(StringToDate(effectiveEndDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
		}
	}
	
	public Integer getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	public TRANSACTION_TYPE getTxnType() {
		return txnType;
	}
	public void setTxnType(TRANSACTION_TYPE txnType) {
		this.txnType = txnType;
	}
	public String getTxnReason() {
		return txnReason;
	}
	public void setTxnReason(String txnReason) {
		this.txnReason = txnReason;
	}
	private Date StringToDate(String dateStr, String format) throws ParseException{
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    return dateFormat.parse(dateStr);
	}
}
