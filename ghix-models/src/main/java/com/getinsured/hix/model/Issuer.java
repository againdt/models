package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.hix.platform.config.FileBasedConfiguration;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.planmgmt.validation.GreaterThanZero;

/**
 * The persistent class for the roles database table.
 */
@Audited
@Entity
@Table(name="issuers")
@XmlRootElement(name="issuer")
@XmlAccessorType(XmlAccessType.NONE)
public class Issuer implements Serializable, SortableEntity {
	private static final long serialVersionUID = 1L;
	
	public static enum certification_status {
		CERTIFIED, DECERTIFIED, PENDING, REGISTERED;
	}
	
	public static enum license_status { 
		ACTIVE, INACTIVE, VOLUNTARY_SURRENDER; 
	}
	public enum IssuerAccreditation { 
		YES, NO,EXCELLENT; 
	}
	
	private static List<String> sortableColumnsList = new ArrayList<String>(Arrays.asList(
			"certificationStatus", 
			"creationTimestamp", 
			"lastUpdateTimestamp", 
			"lastUpdatedBy", 
			"effectiveEndDate", 
			"effectiveStartDate", 
			"decertifiedOn", 
			"certifiedOn", 
			"name", 
			"companyLegalName", 
			"shortName", 
			"marketingName", 
			"state", 
			"stateOfDomicile", 
			"companyState", 
			"zip", 
			"hiosIssuerId", 
			"loginRequired")); 

	/**
	 * This validation group provides groupings for fields  {@link #licenseNumber}, 
	 *
	 * during editing issuer details.
	 * @author kunal
	 *
	 */
	public interface EditIssuerDetails extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during adding New issuer .
	 * @author kunal
	 *
	 */
	public interface AddNewIssuer extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #siteUrl}, 
	 *{@link #companyLegalName},{@link #companyZip},{@link #paymentUrl},{@link #companySiteUrl}
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditCompanyProfile extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditShopProfile extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #indvCustServicePhone}, 
	 * {@link #indvCustServicePhoneExt}, {@link #indvCustServiceTollFree}, {@link #indvCustServiceTTY} 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditIndividualProfile extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditAccreditationDocuments extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditCertificationStatus extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditCompanyProfileIssuer extends Default{
		
	}
	
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during adding New issuer .
	 * @author Hari
	 *
	 */
	public interface PhixAddIssuer extends Default{
		
	}
	
	public interface PhixEditIssuerDetails extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #name}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditCompanyProfileIssuerPHIX extends Default{
		
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Issuer_Seq")
	@SequenceGenerator(name = "Issuer_Seq", sequenceName = "issuers_seq", allocationSize = 1)
	private int id;
	
	@NotEmpty(message="{err.issuerName}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class })
	@XmlElement(name="name")
	@Column(name="name", length=200)
	private String name;
	
	@Size(max=9,message="{label.federalEmployerId}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	@Pattern(regexp="( )*([0-9]{9})?",message="{err.federalEmployerId}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	@Column(name="FEDERAL_EIN", length=9)
	private String federalEin;
	
	@Pattern(regexp="( )*([0-9a-zA-Z])*",message="{err.naicCompanyCode}",groups={Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	@NotEmpty(message="{err.naicCompanyCode}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class})
	@Column(name="naic_company_code", length=25)
	private String naicCompanyCode;
	
	@Pattern(regexp="( )*([0-9a-zA-Z])*",message="{err.naicGroupCode}",groups={Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	@NotEmpty(message="{err.naicGroupCode}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class})
	@Column(name="naic_group_code", length=25)
	private String naicGroupCode;
	
	@NotEmpty(message="{err.hiosIssuerId}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class,Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class})
	@Size(min=5,max=5,message="{err.hiosIdLength}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class})
	@Pattern(regexp="[0-9]{5}",message="{err.hiosIssuerId}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class})
	@GreaterThanZero(groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class})
	@Column(name="hios_issuer_id", length=5)
	private String hiosIssuerId;
	
	@Column(name="issuer_accreditation", length=10)
	@Enumerated(EnumType.STRING)
	private IssuerAccreditation issuerAccreditation;
	
	@Size(max=50,message="{err.validateAccreditingEntity}",groups={Issuer.EditAccreditationDocuments.class})
	@Column(name="accrediting_entity", length=50)
	private String accreditingEntity;
	
	//@NotEmpty(message="{err.issuerLicenseNumber}")
	@Column(name="license_number", length=30)
	private String licenseNumber;
	
	@Column(name="license_status", length=30)
	private String licenseStatus;
	
	@NotEmpty(message="{err.selectCertStatus}",groups={Issuer.EditCertificationStatus.class})
	@Pattern(regexp="^CERTIFIED|DECERTIFIED|PENDING|REGISTERED$",message="{err.selectCorrectCertStatus}",groups={Issuer.EditCertificationStatus.class})
	@Column(name="certification_status", length=20)
	private String certificationStatus;
	
	@NotEmpty(message="{err.address}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class,Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class})
	@Pattern(regexp="([A-Za-z0-9-#,'. ]{0,50})?",message="{err.address}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class,Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class,Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class})
	@Column(name="address_line1", length=200)
	private String addressLine1;
	
	@Pattern(regexp="([A-Za-z0-9-#,'. ]{0,50})?",message="{err.address}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class,Issuer.PhixAddIssuer.class,Issuer.PhixEditIssuerDetails.class,Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class})
	@Column(name="address_line2", length=200)
	private String addressLine2;
	
	@NotEmpty(message="{err.issuerCity}",groups={Issuer.AddNewIssuer.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class,Issuer.EditIssuerDetails.class})
	@Pattern(regexp="([A-Za-z0-9-#,'. ]{0,50})?",message="{err.issuerCity}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class})
	@Column(name="city", length=30)
	private String city;
	
	@NotEmpty(message="{err.issuerState}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	@Pattern(regexp="( )*([A-Z]{2})?",message="{err.state}",groups={Issuer.AddNewIssuer.class, Issuer.EditIssuerDetails.class})
	@Column(name="state", length=2)
	private String state;
	
	@NotEmpty(message="{err.issuerZip}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	//@Size(min=5,max=5,message="{err.zipLenght}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class})
	@Pattern(regexp="( )*([0-9]{5})?",message="{err.zipOnlyDigits}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	//@Pattern(regexp="[0-9]{5}",message="{err.zipOnlyDigits}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class})
	@Column(name="zip", length=5)
	private String zip;
	
	@Column(name="PHONE_NUMBER", length=10)
	private String phoneNumber;
	
	/*adding server side validation for issuers email HIX-67244*/
	@Pattern(regexp="^[_A-Za-z0-9._]+([A-Za-z0-9.]+)@[A-Za-z0-9]+([.][a-zA-Z]{2,})+$",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class, Issuer.PhixAddIssuer.class, Issuer.PhixEditIssuerDetails.class})
	@Column(name="EMAIL_ADDRESS", length=100)
	private String emailAddress;
	
	@Column(name="enrollment_url", length=200)
	private String enrollmentUrl;
	
	@Column(name="contact_person", length=50)
	private String contactPerson;
	
	@Column(name="initial_payment", length=200)
	private String initialPayment;
	
	@Column(name="recurring_payment", length=200)
	private String recurringPayment;

	//commenting due to HIX-64562 @Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[	 ]{0})+)?",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="site_url", length=200)
	private String siteUrl;
	
	//@NotEmpty(message="{err.companyLegalName}",groups={Issuer.EditCompanyProfileIssuer.class})
	// HIX-89473 - remove pattern. we don't have client side validation in any profile 
	//@Pattern(regexp="([A-Za-z0-9-_,()'. ])*",message="{err.companyLegalName}",groups={Issuer.EditCompanyProfile.class, Issuer.EditCompanyProfileIssuer.class})
	@Column(name="company_legal_name", length=100)
	private String companyLegalName;
	
	@Column(name="company_logo", length=100)
	private String companyLogo;
	
	@NotAudited
	//@Lob
	@Type(type="org.hibernate.type.BinaryType")
	@Basic(fetch = FetchType.LAZY)
	@Column(name="LOGO", length=10000)
	private  byte[] logo;
	
	@NotAudited
	@Column(name="LOGO_URL")
	private String logoURL;	
	
	@Column(name="state_of_domicile", length=2)
	private String stateOfDomicile;
	
	@Pattern(regexp="([A-Za-z0-9-#,'. ]{0,50})?",message="{err.address}",groups={Issuer.EditCompanyProfile.class, Issuer.EditCompanyProfileIssuer.class})
	@Column(name="company_address_line1", length=200)
	private String companyAddressLine1;
	
	@Pattern(regexp="([A-Za-z0-9-#,'. ]{0,50})?",message="{err.address}",groups={Issuer.EditCompanyProfile.class, Issuer.EditCompanyProfileIssuer.class})	
	@Column(name="company_address_line2", length=200)
	private String companyAddressLine2;
	
	@Pattern(regexp="([A-Za-z0-9-#,'. ]{0,50})?",message="{err.issuerCity}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class})
	@Column(name="company_city", length=30)
	private String companyCity;
	
	@Pattern(regexp="( )*([A-Z]{2})?",message="{err.companyState}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="company_state", length=2)
	private String companyState;
	
	@Pattern(regexp="( )*([0-9]{5})?",message="{err.zipOnlyDigits}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="company_zip", length=5)
	private String companyZip;
	
	//@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="company_site_url", length=200)
	private String companySiteUrl;
	
	@NotEmpty(message="{err.customerServicePhone}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="indv_cust_service_phone", length=1024)
	private String indvCustServicePhone;
	
	@Pattern(regexp="[0-9]*",message="{err.customerServiceExt}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="indv_cust_service_phone_ext", length=1024)
	private String indvCustServicePhoneExt;
	
	@NotEmpty(message="{err.issuerPhoneInvalid}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="indv_cust_service_toll_free", length=1024)
	private String indvCustServiceTollFree;
	
	@NotEmpty(message="{err.issuerPhoneInvalid}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="indv_cust_service_tty", length=1024)
	private String indvCustServiceTTY;
	
	//@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validfacingWebSite}",groups={Issuer.EditIndividualProfile.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="indv_site_url", length=200)
	private String indvSiteUrl;
	
	@NotEmpty(message="{err.customerServicePhone}",groups={Issuer.EditShopProfile.class})
	@Column(name="shop_cust_service_phone", length=1024)
	private String shopCustServicePhone;
	
	@Pattern(regexp="[0-9]*",message="{err.customerServiceExt}",groups={Issuer.EditShopProfile.class})
	@Column(name="shop_cust_service_phone_ext", length=1024)
	private String shopCustServicePhoneExt;
	
	@NotEmpty(message="{err.issuerPhoneInvalid}",groups={Issuer.EditShopProfile.class})
	@Column(name="shop_cust_service_toll_free", length=1024)
	private String shopCustServiceTollFree;
	
	@NotEmpty(message="{err.issuerPhoneInvalid}",groups={Issuer.EditShopProfile.class})
	@Column(name="shop_cust_service_tty", length=1024)
	private String shopCustServiceTTY;
	
	//@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validfacingWebSite}",groups={Issuer.EditShopProfile.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditShopProfile.class})
	@Column(name="shop_site_url", length=200)
	private String shopSiteUrl;
	
	@Column(name="effective_start_date",nullable=true)
	private Date effectiveStartDate;
	
	@Column(name="effective_end_date",nullable=true)
	private Date effectiveEndDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	 
	@Column(name="certification_doc", length=100)
	private String certificationDoc;
	
	@NotEmpty(message="{err.issuerShortName}",groups={Issuer.AddNewIssuer.class,Issuer.EditIssuerDetails.class})
	@Column(name="short_name")
	private String shortName;
		
	@Column(name="txn_834_Version")
	private String txn834Version;
		
	@Column(name="txn_820_Version")
	private String txn820Version;
	
	@Column(name="accreditation_exp_date")
	private Date accreditationExpDate;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "issuer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<IssuerDocument> issuerDocuments;
	
	@Column(name="comment_id")
	private Integer commentId;
	
	@Column(name="marketing_name",nullable=true)
	private String marketingName;
	
//	@Column(name = "org_chart",nullable=true) // Changes are done for JIRA PERF-81
	//@Lob
	@Type(type="org.hibernate.type.BinaryType")
	@Column(length = 100000, name = "org_chart", nullable=true) 
	@Basic(fetch = FetchType.LAZY)
	//private Blob orgChart;
	private  byte[] orgChart;
	
	@Column(name="last_updated_by")
	private Integer lastUpdatedBy;
	
	@Column(name="national_producer_number")
	private String nationalProducerNumber;
		
	@Column(name="agent_first_name")
	private String agentFirstName;
	
	@Column(name="agent_last_name")
	private String agentLastName;
	
	//@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?)?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuer.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="payment_url")
	private String paymentUrl;
	
	@Column(name="ON_EXCHANGE_DISCLAIMER")
	private String onExchangeDisclaimer;
	
	@Column(name="OFF_EXCHANGE_DISCLAIMER")
	private String offExchangeDisclaimer;
	
	//@Pattern(regexp="( )*(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2})){1}([a-zA-Z0-9@#$%%-_+=&?]*[ ]{0})+(\\.){1}([a-zA-Z0-9@#$%%-_+=&?\\.]+[ ]{0})+)?",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?)?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="producer_url", length=200)
	private String producerUrl;
	
	@Column(name="producer_uname",unique = true)
	private String producerUserName;

	@Column(name="Broker_ID")
	private String brokerId;
	
	@Column(name="Writing_Agent")
	private String writingAgent;
	
	@Pattern(regexp="( )*([0-9]{10})*",message="{err.customerServiceExt}",groups={Issuer.EditCompanyProfile.class})
	@Column(name="Broker_Phone")
	private String brokerPhone;
	
	@Column(name="Broker_Fax")
	private String brokerFax;
	
	/*modifying server side validation for issuers email HIX-67244*/
	//@Pattern(regexp="( )*(^[_A-Za-z0-9-\\+!#$%^&*-=+|']+(\\.[_A-Za-z0-9-\\+!#$%^&*-=+|']+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)?",groups={Issuer.EditCompanyProfile.class})
	@Pattern(regexp="( )*(^[_A-Za-z0-9._]+([A-Za-z0-9.]+)@[A-Za-z0-9]+([.][a-zA-Z]{2,})+$)?",groups={Issuer.EditCompanyProfile.class})
	@Column(name="Broker_Email")
	private String brokerEmail;

	@Pattern(regexp="([a-zA-Z0-9!@#$%^&*()_=\\-\\+]{0,100})?",message="{label.validatePasswordLength}",groups={Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="producer_password", length = 2000)
	private String producerPassword;
	
	//@Pattern(regexp="(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2}([w]{3}[\\.]){0,1})|(^[w]{3}[\\.]))?(.)*)",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?)?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditCompanyProfile.class,Issuer.EditCompanyProfileIssuerPHIX.class})
	@Column(name="application_url", length=200)
	private String applicationUrl;
	
	@Column(name="D2C")
	private String d2C;
	
	/*@Column(name="BRAND_NAME_ID")
	private Integer brandNameId;*/

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)	
	@ManyToOne(cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
	@JoinColumn(name = "BRAND_NAME_ID")
	private IssuerBrandName issuerBrandName;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "issuer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private IssuerExt issuerExt;
	
	@Pattern(regexp="^YES|NO$",groups={Issuer.EditCompanyProfile.class})
	@Column(name="LOGIN_REQUIRED", length=3)
	//@Enumerated(EnumType.STRING)
	private String loginRequired;
	
	/* adding new columns for HIX-66733*/
	@Pattern(regexp="( )*([0-9]{10})*",message="{err.appStatusDeptPhoneNumber}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="APP_STATUS_DEPT_PHONE_NUMBER", length=200)
	private String appStatusDeptPhoneNumber;
	
	@Column(name="COMMISSIONS_CONTACT_NAME", length=200)
	private String commissionsContactName;

	@Pattern(regexp="( )*([0-9]{10})*",message="{err.commissionsPhoneNumber}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="COMMISSIONS_PHONE_NUMBER", length=10)
	private String commissionsPhoneNumber;
	
	/*modifying server side validation for issuers email HIX-67244*/
	//@Pattern(regexp="( )*(^[_A-Za-z0-9-\\+!#$%^&*-=+|']+(\\.[_A-Za-z0-9-\\+!#$%^&*-=+|']+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)?",groups={Issuer.EditIndividualProfile.class})
	@Pattern(regexp="( )*(^[_A-Za-z0-9._]+([A-Za-z0-9.]+)@[A-Za-z0-9]+([.][a-zA-Z]{2,})+$)?",groups={Issuer.EditIndividualProfile.class})
	@Column(name="COMMISSIONS_EMAIL_ID", length=100)
	private String commissionsEmailId;

	@Pattern(regexp="( )*([0-9]{10})*",message="{err.pcdPhoneNumber}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="PCD_PHONE_NUMBER", length=10)
	private String pcdPhoneNumber;

	@Pattern(regexp="( )*([0-9]{10})*",message="{err.pcdFaxNumber}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="PCD_FAX_NUMBER", length=10)
	private String pcdFaxNumber;

	/*modifying server side validation for issuers email HIX-67244*/
	//@Pattern(regexp="( )*(^[_A-Za-z0-9-\\+!#$%^&*-=+|']+(\\.[_A-Za-z0-9-\\+!#$%^&*-=+|']+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)?",groups={Issuer.EditIndividualProfile.class})
	@Pattern(regexp="( )*(^[_A-Za-z0-9._]+([A-Za-z0-9.]+)@[A-Za-z0-9]+([.][a-zA-Z]{2,})+$)?",groups={Issuer.EditIndividualProfile.class})
	@Column(name="PCD_EMAIL_ID", length=100)
	private String pcdEmailId;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "PCD_LOCATION_ID")
	private Location location;
	
	@Column(name="BROKER_CONTACT_NAME", length=200)
	private String brokerContactName;

	@Pattern(regexp="( )*([0-9]{10})*",message="{err.commissionsPhoneNumber}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="BROKER_PHONE_NUMBER", length=10)
	private String brokerPhoneNumber;
	
	/*modifying server side validation for issuers email HIX-67244*/
	//@Pattern(regexp="( )*(^[_A-Za-z0-9-\\+!#$%^&*-=+|']+(\\.[_A-Za-z0-9-\\+!#$%^&*-=+|']+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)?",groups={Issuer.EditIndividualProfile.class})
	@Pattern(regexp="( )*(^[_A-Za-z0-9._]+([A-Za-z0-9.]+)@[A-Za-z0-9]+([.][a-zA-Z]{2,})+$)?",groups={Issuer.EditIndividualProfile.class})
	@Column(name="BROKER_EMAIL_ID", length=100)
	private String brokerEmailId;

	@Transient
	private String shortTenantList;
	
	@Transient
	private String longTenantList;
	
	@Pattern(regexp="^true|false$",groups={Issuer.EditCompanyProfile.class})
	@Column(name="SEND_ENROLLMENT_END_DATE_FLAG", length=5)
	private String sendEnrollmentEndDateFlag;
	
	@Pattern(regexp="[0-9]*",message="{err.commissionsPhoneNumberExt}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="COMMISSIONS_PHONE_NUMBER_EXT", length=15)
	private String commissionsPhoneNumberExt;
	
	@Pattern(regexp="[0-9]*",message="{err.pcdPhoneNumberExt}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="PCD_PHONE_NUMBER_EXT", length=15)
	private String pcdPhoneNumberExt;
	
	@Pattern(regexp="[0-9]*",message="{err.brokerPhoneNumberExt}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="BROKER_PHONE_NUMBER_EXT", length=15)
	private String brokerPhoneNumberExt;
	
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?)?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="COMMISSION_PORTAL_URL", length=200)
	private String commissionPortalUrl;
	
	@Column(name="COMMISSION_PORTAL_USERNAME",unique = true)
	private String commissionPortalUserName;
	
	@Pattern(regexp="([a-zA-Z0-9!@#$%^&*()_=\\-\\+]{0,100})?",message="{label.validatePasswordLength}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="COMMISSION_PORTAL_PASSWORD", length = 2000)
	private String commissionPortalPassword;
	
	@Pattern(regexp="(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?)?]+)+$)|^$",message="{err.validfacingWebSite}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="AGENCY_REPO_PORTAL_URL", length=200)
	private String agencyRepoPortalUrl;
	
	@Column(name="AGENCY_REPO_PORTAL_USERNAME",unique = true)
	private String agencyRepoPortalUserName;
	
	@Pattern(regexp="([a-zA-Z0-9!@#$%^&*()_=\\-\\+]{0,100})?",message="{label.validatePasswordLength}",groups={Issuer.EditIndividualProfile.class})
	@Column(name="AGENCY_REPO_PORTAL_PASSWORD", length = 2000)
	private String agencyRepoPortalPassword;
	
	
	public Issuer() {
	}
	
	public Issuer(int id, String name, String hiosIssuerId, String shortName) {
		this.id = id;
		this.name = name;
		this.hiosIssuerId = hiosIssuerId;
		this.shortName = shortName;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
	public String getLicenseStatus() {
		return licenseStatus;
	}

	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}	

	public String getEnrollmentUrl() {
		return enrollmentUrl;
	}

	public void setEnrollmentUrl(String enrollmentUrl) {
		this.enrollmentUrl = enrollmentUrl;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}
	

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phone) {
		this.phoneNumber = phone;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getInitialPayment() {
		return initialPayment;
	}

	public void setInitialPayment(String initialPayment) {
		this.initialPayment = initialPayment;
	}

	public String getRecurringPayment() {
		return recurringPayment;
	}

	public void setRecurringPayment(String recurringPayment) {
		this.recurringPayment = recurringPayment;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="certified_on")
	private Date certifiedOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="decertified_on")
	private Date decertifiedOn;

	
	private static FileBasedConfiguration config = FileBasedConfiguration.getConfiguration();

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public Date getCertifiedOn() {
		return certifiedOn;
	}

	public void setCertifiedOn(Date certifiedOn) {
		this.certifiedOn = certifiedOn;
	}

	public Date getDecertifiedOn() {
		return decertifiedOn;
	}

	public void setDecertifiedOn(Date decertifiedOn) {
		this.decertifiedOn = decertifiedOn;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		updateBlanksToNull();
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		updateBlanksToNull();
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	/**
	 * HIX-87408: POSTGRES - Federal EIN is saved empty while creating carrier
	 */
	private void updateBlanksToNull() {

		if (StringUtils.isBlank(this.getState())) {
			this.setState(null);
		}

		if (StringUtils.isBlank(this.getFederalEin())) {
			this.setFederalEin(null);
		}

		if (StringUtils.isBlank(this.getNaicCompanyCode())) {
			this.setNaicCompanyCode(null);
		}

		if (StringUtils.isBlank(this.getNaicGroupCode())) {
			this.setNaicGroupCode(null);
		}

		if (StringUtils.isBlank(this.getStateOfDomicile())) {
			this.setStateOfDomicile(null);
		}

		if (StringUtils.isBlank(this.getCompanyState())) {
			this.setCompanyState(null);
		}

		if (StringUtils.isBlank(this.getLogoURL())) {
			this.setLogoURL(null);
		}
	}

	public String getCertificationDoc() {
		return certificationDoc;
	}

	public void setCertificationDoc(String certificationDoc) {
		this.certificationDoc = certificationDoc;
	}

	public String getFederalEin() {
		return federalEin;
	}

	public void setFederalEin(String federalEmployerId) {
		this.federalEin = federalEmployerId;
	}

	public String getNaicCompanyCode() {
		return naicCompanyCode;
	}

	public void setNaicCompanyCode(String naicCompanyCode) {
		this.naicCompanyCode = naicCompanyCode;
	}

	public String getNaicGroupCode() {
		return naicGroupCode;
	}

	public void setNaicGroupCode(String naicGroupCode) {
		this.naicGroupCode = naicGroupCode;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		if(null == hiosIssuerId) {
			throw new IllegalArgumentException("Issuer's HIOS idenifier must be specified.");
		}
		else {
			this.hiosIssuerId = hiosIssuerId;
		}
	}

	public IssuerAccreditation getIssuerAccreditation() {
		return issuerAccreditation;
	}

	public void setIssuerAccreditation(IssuerAccreditation issuerAccreditation) {
		this.issuerAccreditation = issuerAccreditation;
	}

	public String getAccreditingEntity() {
		return accreditingEntity;
	}

	public void setAccreditingEntity(String accreditingEntity) {
		this.accreditingEntity = accreditingEntity;
	}

	public String getCompanyLegalName() {
		return companyLegalName;
	}

	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public String getStateOfDomicile() {
		return stateOfDomicile;
	}

	public void setStateOfDomicile(String stateOfDomicile) {
		this.stateOfDomicile = stateOfDomicile;
	}

	public String getCompanyAddressLine1() {
		return companyAddressLine1;
	}

	public void setCompanyAddressLine1(String companyAddressLine1) {
		this.companyAddressLine1 = companyAddressLine1;
	}

	public String getCompanyAddressLine2() {
		return companyAddressLine2;
	}

	public void setCompanyAddressLine2(String companyAddressLine2) {
		this.companyAddressLine2 = companyAddressLine2;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	public String getCompanyState() {
		return companyState;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public String getCompanyZip() {
		return companyZip;
	}

	public void setCompanyZip(String companyZip) {
		this.companyZip = companyZip;
	}

	public String getCompanySiteUrl() {
		return companySiteUrl;
	}

	public void setCompanySiteUrl(String companySiteUrl) {
		this.companySiteUrl = companySiteUrl;
	}

	public String getIndvCustServicePhone() {
		return indvCustServicePhone;
	}

	public void setIndvCustServicePhone(String indvCustServicePhone) {
		this.indvCustServicePhone = indvCustServicePhone;
	}

	public String getIndvCustServicePhoneExt() {
		return indvCustServicePhoneExt;
	}

	public void setIndvCustServicePhoneExt(String indvCustServicePhoneExt) {
		this.indvCustServicePhoneExt = indvCustServicePhoneExt;
	}

	public String getIndvCustServiceTollFree() {
		return indvCustServiceTollFree;
	}

	public void setIndvCustServiceTollFree(String indvCustServiceTollFree) {
		this.indvCustServiceTollFree = indvCustServiceTollFree;
	}

	public String getIndvCustServiceTTY() {
		return indvCustServiceTTY;
	}

	public void setIndvCustServiceTTY(String indvCustServiceTTY) {
		this.indvCustServiceTTY = indvCustServiceTTY;
	}

	public String getIndvSiteUrl() {
		return indvSiteUrl;
	}

	public void setIndvSiteUrl(String indvSiteUrl) {
		this.indvSiteUrl = indvSiteUrl;
	}

	public String getShopCustServicePhone() {
		return shopCustServicePhone;
	}

	public void setShopCustServicePhone(String shopCustServicePhone) {
		this.shopCustServicePhone = shopCustServicePhone;
	}

	public String getShopCustServicePhoneExt() {
		return shopCustServicePhoneExt;
	}

	public void setShopCustServicePhoneExt(String shopCustServicePhoneExt) {
		this.shopCustServicePhoneExt = shopCustServicePhoneExt;
	}

	public String getShopCustServiceTollFree() {
		return shopCustServiceTollFree;
	}

	public void setShopCustServiceTollFree(String shopCustServiceTollFree) {
		this.shopCustServiceTollFree = shopCustServiceTollFree;
	}

	public String getShopCustServiceTTY() {
		return shopCustServiceTTY;
	}

	public void setShopCustServiceTTY(String shopCustServiceTTY) {
		this.shopCustServiceTTY = shopCustServiceTTY;
	}

	public String getShopSiteUrl() {
		return shopSiteUrl;
	}

	public void setShopSiteUrl(String shopSiteUrl) {
		this.shopSiteUrl = shopSiteUrl;
	}

	public List<IssuerDocument> getIssuerDocuments() {
		return issuerDocuments;
	}

	public void setIssuerDocuments(List<IssuerDocument> issuerDocuments) {
		this.issuerDocuments = issuerDocuments;
	}
		
	/**
	 * @return the commissionsPhoneNumberExt
	 */
	public String getCommissionsPhoneNumberExt() {
		return commissionsPhoneNumberExt;
	}

	/**
	 * @param commissionsPhoneNumberExt the commissionsPhoneNumberExt to set
	 */
	public void setCommissionsPhoneNumberExt(String commissionsPhoneNumberExt) {
		this.commissionsPhoneNumberExt = commissionsPhoneNumberExt;
	}

	/**
	 * @return the pcdPhoneNumberExt
	 */
	public String getPcdPhoneNumberExt() {
		return pcdPhoneNumberExt;
	}

	/**
	 * @param pcdPhoneNumberExt the pcdPhoneNumberExt to set
	 */
	public void setPcdPhoneNumberExt(String pcdPhoneNumberExt) {
		this.pcdPhoneNumberExt = pcdPhoneNumberExt;
	}

	/**
	 * @return the brokerPhoneNumberExt
	 */
	public String getBrokerPhoneNumberExt() {
		return brokerPhoneNumberExt;
	}

	/**
	 * @param brokerPhoneNumberExt the brokerPhoneNumberExt to set
	 */
	public void setBrokerPhoneNumberExt(String brokerPhoneNumberExt) {
		this.brokerPhoneNumberExt = brokerPhoneNumberExt;
	}

	public Map<license_status,String> getIssuerLicStatuses(){		
		Map<license_status,String> enumMap = new EnumMap<license_status,String>(license_status.class);
	    enumMap.put(license_status.ACTIVE, "Active");
	    enumMap.put(license_status.INACTIVE, "Inactive");
	    enumMap.put(license_status.VOLUNTARY_SURRENDER, "Voluntry Surrender");  	      		
		return enumMap;
	}
	
	public Map<certification_status,String> getIssuerStatuses(){		
		Map<certification_status,String> enumMap = new EnumMap<certification_status,String>(certification_status.class);
	    enumMap.put(certification_status.CERTIFIED, "Certified");
	    enumMap.put(certification_status.DECERTIFIED, "De-certified");
	    enumMap.put(certification_status.PENDING, "Pending");  	   
	    enumMap.put(certification_status.REGISTERED, "Registered");
		return enumMap;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getTxn834Version() {
		return txn834Version;
	}

	public void setTxn834Version(String txn834Version) {
		this.txn834Version = txn834Version;
	}

	public String getTxn820Version() {
		return txn820Version;
	}

	public void setTxn820Version(String txn820Version) {
		this.txn820Version = txn820Version;
	}

	public Date getAccreditationExpDate() {
		return accreditationExpDate;
	}

	public void setAccreditationExpDate(Date accreditationExpDate) {
		this.accreditationExpDate = accreditationExpDate;
	}
	
	public String getMarketingName() {
		return marketingName;
	}

	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}
	
 /**
   * https://hibernate.atlassian.net/browse/HHH-5255
   * Byte array marked with lazy load are not lazy loaded due to the above bug which is still an issue in ORM 4.
   * Hence Byte array has to be wrapped by an object of type java.sql.blob 
   * @return
   * @throws GIException
   */
	/*public byte[] getOrgChart()  {
		byte[] byteArr = null;
		try {
			if(orgChart!=null && orgChart.length()>0){
				int length = new Long(orgChart.length()).intValue();
			
				byteArr = orgChart.getBytes(Long.valueOf(1), length );
			}
		}catch (SQLException e) {				   
			   // In this situation if the Blob is null in DB , there should be no exception. So not throwing error.
			   // throw new GIException(e.getMessage());
		}
		
		return byteArr;
	}*/
 /**
   * https://hibernate.atlassian.net/browse/HHH-5255
   * Byte array marked with lazy load are not lazy loaded due to the above bug which is still an issue in ORM 4.
   * Hence Byte array has to be wrapped by an object of type java.sql.blob 
   * @return
   * @throws GIException
   */
	/*public void setOrgChart(byte[] orgChart) throws GIException   {
		try {
			if(orgChart!=null && orgChart.length>0){
				
				this.orgChart = new SerialBlob(orgChart) ;
			}else{
				this.orgChart = null;
			}
		}catch (SQLException e) {
			throw new GIException(e.getMessage());
		}
	}*/

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getNationalProducerNumber() {
		return nationalProducerNumber;
	}

	public void setNationalProducerNumber(String nationalProducerNumber) {
		this.nationalProducerNumber = nationalProducerNumber;
	}

	public String getAgentFirstName() {
		return agentFirstName;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}
	
	public String getOnExchangeDisclaimer() {
		return onExchangeDisclaimer;
	}

	public void setOnExchangeDisclaimer(String onExchangeDisclaimer) {
		this.onExchangeDisclaimer = onExchangeDisclaimer;
	}

	public String getDisclaimer() {
		return onExchangeDisclaimer;
	}

	public void setDisclaimer(String onExchangeDisclaimer) {
		this.onExchangeDisclaimer = onExchangeDisclaimer;
	}

	public String getOffExchangeDisclaimer() {
		return offExchangeDisclaimer;
	}

	public void setOffExchangeDisclaimer(String offExchangeDisclaimer) {
		this.offExchangeDisclaimer = offExchangeDisclaimer;
	}

	public String getProducerUrl() {
		return producerUrl;
	}

	public void setProducerUrl(String producerUrl) {
		this.producerUrl = producerUrl;
	}

	public String getProducerUserName() {
		return producerUserName;
	}

	public void setProducerUserName(String producerUserName) {
		this.producerUserName = producerUserName;
	}

	public String getProducerPassword() {
		return producerPassword;
	}

	public void setProducerPassword(String producerPassword) {
		this.producerPassword = producerPassword;
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public String getD2C() {
		return d2C;
	}

	public void setD2C(String d2c) {
		d2C = d2c;
	}

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public String getWritingAgent() {
		return writingAgent;
	}

	public void setWritingAgent(String writingAgent) {
		this.writingAgent = writingAgent;
	}

	public String getBrokerPhone() {
		return brokerPhone;
	}

	public void setBrokerPhone(String brokerPhone) {
		this.brokerPhone = brokerPhone;
	}

	public String getBrokerFax() {
		return brokerFax;
	}

	public void setBrokerFax(String brokerFax) {
		this.brokerFax = brokerFax;
	}

	public String getBrokerEmail() {
		return brokerEmail;
	}

	public void setBrokerEmail(String brokerEmail) {
		this.brokerEmail = brokerEmail;
	}

	/**
	 * @return the issuerBrandName
	 */
	public IssuerBrandName getIssuerBrandName() {
		return issuerBrandName;
	}

	/**
	 * @param issuerBrandName the issuerBrandName to set
	 */
	public void setIssuerBrandName(IssuerBrandName issuerBrandName) {
		this.issuerBrandName = issuerBrandName;
	}

	public IssuerExt getIssuerExt() {
		return issuerExt;
	}

	public void setIssuerExt(IssuerExt issuerExt) {
		this.issuerExt = issuerExt;
	}

	public String getLoginRequired() {
		return loginRequired;
	}

	public void setLoginRequired(String loginRequired) {
		this.loginRequired = loginRequired;
	}

	/**
	 * @return the appStatusDeptPhoneNumber
	 */
	public String getAppStatusDeptPhoneNumber() {
		return appStatusDeptPhoneNumber;
	}

	/**
	 * @param appStatusDeptPhoneNumber the appStatusDeptPhoneNumber to set
	 */
	public void setAppStatusDeptPhoneNumber(String appStatusDeptPhoneNumber) {
		this.appStatusDeptPhoneNumber = appStatusDeptPhoneNumber;
	}

	/**
	 * @return the commissionsContactName
	 */
	public String getCommissionsContactName() {
		return commissionsContactName;
	}

	/**
	 * @param commissionsContactName the commissionsContactName to set
	 */
	public void setCommissionsContactName(String commissionsContactName) {
		this.commissionsContactName = commissionsContactName;
	}

	/**
	 * @return the commissionsPhoneNumber
	 */
	public String getCommissionsPhoneNumber() {
		return commissionsPhoneNumber;
	}

	/**
	 * @param commissionsPhoneNumber the commissionsPhoneNumber to set
	 */
	public void setCommissionsPhoneNumber(String commissionsPhoneNumber) {
		this.commissionsPhoneNumber = commissionsPhoneNumber;
	}

	/**
	 * @return the commissionsEmailId
	 */
	public String getCommissionsEmailId() {
		return commissionsEmailId;
	}

	/**
	 * @param commissionsEmailId the commissionsEmailId to set
	 */
	public void setCommissionsEmailId(String commissionsEmailId) {
		this.commissionsEmailId = commissionsEmailId;
	}

	/**
	 * @return the pcdPhoneNumber
	 */
	public String getPcdPhoneNumber() {
		return pcdPhoneNumber;
	}

	/**
	 * @param pcdPhoneNumber the pcdPhoneNumber to set
	 */
	public void setPcdPhoneNumber(String pcdPhoneNumber) {
		this.pcdPhoneNumber = pcdPhoneNumber;
	}

	/**
	 * @return the pcdFaxNumber
	 */
	public String getPcdFaxNumber() {
		return pcdFaxNumber;
	}

	/**
	 * @param pcdFaxNumber the pcdFaxNumber to set
	 */
	public void setPcdFaxNumber(String pcdFaxNumber) {
		this.pcdFaxNumber = pcdFaxNumber;
	}

	/**
	 * @return the pcdEmailId
	 */
	public String getPcdEmailId() {
		return pcdEmailId;
	}

	/**
	 * @param pcdEmailId the pcdEmailId to set
	 */
	public void setPcdEmailId(String pcdEmailId) {
		this.pcdEmailId = pcdEmailId;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @return the brokerContactName
	 */
	public String getBrokerContactName() {
		return brokerContactName;
	}

	/**
	 * @param brokerContactName the brokerContactName to set
	 */
	public void setBrokerContactName(String brokerContactName) {
		this.brokerContactName = brokerContactName;
	}

	/**
	 * @return the brokerPhoneNumber
	 */
	public String getBrokerPhoneNumber() {
		return brokerPhoneNumber;
	}

	/**
	 * @param brokerPhoneNumber the brokerPhoneNumber to set
	 */
	public void setBrokerPhoneNumber(String brokerPhoneNumber) {
		this.brokerPhoneNumber = brokerPhoneNumber;
	}

	/**
	 * @return the brokerEmailId
	 */
	public String getBrokerEmailId() {
		return brokerEmailId;
	}

	/**
	 * @param brokerEmailId the brokerEmailId to set
	 */
	public void setBrokerEmailId(String brokerEmailId) {
		this.brokerEmailId = brokerEmailId;
	}

	public String getShortTenantList() {
		return shortTenantList;
	}

	public void setShortTenantList(String shortTenantList) {
		this.shortTenantList = shortTenantList;
	}

	public String getLongTenantList() {
		return longTenantList;
	}

	public void setLongTenantList(String longTenantList) {
		this.longTenantList = longTenantList;
	}
	
	public String getSendEnrollmentEndDateFlag(){
		return sendEnrollmentEndDateFlag;
	}
	
	public void setSendEnrollmentEndDateFlag(String sendEnrollmentEndDateFlag){
		this.sendEnrollmentEndDateFlag = sendEnrollmentEndDateFlag;
	}
	
	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
	
	public String getLogoURL() {
		return logoURL;
	}

	public void setLogoURL(String logoURL) {
		this.logoURL = logoURL;
	}

	/**
	 * @return the commissionPortalUrl
	 */
	public String getCommissionPortalUrl() {
		return commissionPortalUrl;
	}

	/**
	 * @param commissionPortalUrl the commissionPortalUrl to set
	 */
	public void setCommissionPortalUrl(String commissionPortalUrl) {
		this.commissionPortalUrl = commissionPortalUrl;
	}

	/**
	 * @return the commissionPortalUserName
	 */
	public String getCommissionPortalUserName() {
		return commissionPortalUserName;
	}

	/**
	 * @param commissionPortalUserName the commissionPortalUserName to set
	 */
	public void setCommissionPortalUserName(String commissionPortalUserName) {
		this.commissionPortalUserName = commissionPortalUserName;
	}

	/**
	 * @return the commissionPortalPassword
	 */
	public String getCommissionPortalPassword() {
		return commissionPortalPassword;
	}

	/**
	 * @param commissionPortalPassword the commissionPortalPassword to set
	 */
	public void setCommissionPortalPassword(String commissionPortalPassword) {
		this.commissionPortalPassword = commissionPortalPassword;
	}

	/**
	 * @return the agencyRepoPortalUrl
	 */
	public String getAgencyRepoPortalUrl() {
		return agencyRepoPortalUrl;
	}

	/**
	 * @param agencyRepoPortalUrl the agencyRepoPortalUrl to set
	 */
	public void setAgencyRepoPortalUrl(String agencyRepoPortalUrl) {
		this.agencyRepoPortalUrl = agencyRepoPortalUrl;
	}

	/**
	 * @return the agencyRepoPortalUserName
	 */
	public String getAgencyRepoPortalUserName() {
		return agencyRepoPortalUserName;
	}

	/**
	 * @param agencyRepoPortalUserName the agencyRepoPortalUserName to set
	 */
	public void setAgencyRepoPortalUserName(String agencyRepoPortalUserName) {
		this.agencyRepoPortalUserName = agencyRepoPortalUserName;
	}

	/**
	 * @return the agencyRepoPortalPassword
	 */
	public String getAgencyRepoPortalPassword() {
		return agencyRepoPortalPassword;
	}

	/**
	 * @param agencyRepoPortalPassword the agencyRepoPortalPassword to set
	 */
	public void setAgencyRepoPortalPassword(String agencyRepoPortalPassword) {
		this.agencyRepoPortalPassword = agencyRepoPortalPassword;
	}

	public byte[] getOrgChart() {
		return orgChart;
	}

	public void setOrgChart(byte[] orgChart) {
		this.orgChart = orgChart;
	}

	public static List<String> getSortableColumnsList() {
		return sortableColumnsList;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>(getSortableColumnsList());
	 	return columnNames;
	}

	/** Eventually these should go to DB with UI support from Issuer Payment Info screen
	 * 
	 * @return
	 */
	public String getPaymentProviderEntityId() {
		String entityId = config.getProperty("issuer.payment."+this.hiosIssuerId+".entityId");
		if(entityId == null) {
			return GhixPlatformConstants.PLATFORM_KEY_ALIAS;
		}
		return entityId;
	}
}
