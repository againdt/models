package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

/**
 * The persistent class for the ISSUER_PAYMENTS database table.
 * @author kuldeep
 */

@Audited
@Entity
@XmlRootElement(name="enrollment")
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "issuer_payments")
public class IssuerPayments implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issuer_payments_seq")
	@SequenceGenerator(name = "issuer_payments_seq", sequenceName = "issuer_payments_seq", allocationSize = 1)
	private int id;

	@Column(name = "case_id")
	private String caseId;
	
	@XmlElement(name="statementDate")
	@Column(name = "statement_date")
	private Date statementDate;
	
	@XmlElement(name="invoiceNumber")
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@XmlElement(name="periodCovered")
	@Column(name = "period_covered")
	private String periodCovered;
	
	@XmlElement(name="paymentDueDate")
	@Column(name = "payment_due_date")
	private Date paymentDueDate;
	
	@XmlElement(name="amountDueFromLastInvoice")
	@Column(name = "amount_due_from_last_invoice")
	private BigDecimal amountDueFromLastInvoice;
	
	@XmlElement(name="totalPaymentReceived")
	@Column(name = "total_payment_received")
	private BigDecimal totalPaymentReceived;
	
	@XmlElement(name="premiumThisPeriod")
	@Column(name = "premiums_this_period")
	private BigDecimal premiumThisPeriod;
	
	@XmlElement(name="adjustments")
	@Column(name = "adjustments")
	private BigDecimal adjustments;
	
	@XmlElement(name="exchangeFees")
	@Column(name = "exchange_fees")
	private BigDecimal exchangeFees;
	
	@XmlElement(name="totalAmountDue")
	@Column(name = "total_amount_due")
	private BigDecimal totalAmountDue;
	
	@XmlElement(name="amountEnclosed")
	@Column(name = "amount_enclosed")
	private BigDecimal amountEnclosed;
	
	@OneToOne
	@JoinColumn(name = "issuer_payment_invoice_id")
	private IssuerPaymentInvoice issuerPaymentInvoice;

	@XmlElement(name="status")
	@Column(name = "status")
	private String status;
	
	@Column(name = "is_refund", columnDefinition = "char(1)")
	private Character isRefund;
	
	@Column(name = "is_partial_payment", columnDefinition = "char(1)")
	private Character isPartialPayment;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public BigDecimal getAmountDueFromLastInvoice() {
		return amountDueFromLastInvoice;
	}

	public void setAmountDueFromLastInvoice(BigDecimal amountDueFromLastInvoice) {
		this.amountDueFromLastInvoice = amountDueFromLastInvoice;
	}

	public BigDecimal getTotalPaymentReceived() {
		return totalPaymentReceived;
	}

	public void setTotalPaymentReceived(BigDecimal totalPaymentReceived) {
		this.totalPaymentReceived = totalPaymentReceived;
	}

	public BigDecimal getPremiumThisPeriod() {
		return premiumThisPeriod;
	}

	public void setPremiumThisPeriod(BigDecimal premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}

	public BigDecimal getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(BigDecimal adjustments) {
		this.adjustments = adjustments;
	}

	public BigDecimal getExchangeFees() {
		return exchangeFees;
	}

	public void setExchangeFees(BigDecimal exchangeFees) {
		this.exchangeFees = exchangeFees;
	}

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getAmountEnclosed() {
		return amountEnclosed;
	}

	public void setAmountEnclosed(BigDecimal amountEnclosed) {
		this.amountEnclosed = amountEnclosed;
	}

	public IssuerPaymentInvoice getIssuerPaymentInvoice() {
		return issuerPaymentInvoice;
	}

	public void setIssuerPaymentInvoice(
			IssuerPaymentInvoice issuerPaymentInvoice) {
		this.issuerPaymentInvoice = issuerPaymentInvoice;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Character getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(Character isRefund) {
		this.isRefund = isRefund;
	}

	public Character getIsPartialPayment() {
		return isPartialPayment;
	}

	public void setIsPartialPayment(Character isPartialPayment) {
		this.isPartialPayment = isPartialPayment;
	}
	
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		//return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
		// Changes for HIX-25009 
		return "IssuerPayments [id=" + id + ", caseId=" + caseId
				+ ", statementDate=" + statementDate + ", invoiceNumber="
				+ invoiceNumber + ", periodCovered=" + periodCovered
				+ ", paymentDueDate=" + paymentDueDate + ", totalAmountDue="
				+ totalAmountDue + "]";
	}

}
