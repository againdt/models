package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author  sharma_k
 * @since 08th January 2015
 * Model class for EMPLOYER_EXCHANGE_FEES table 
 * 
 */
@Entity
@Table(name = "employer_exchange_fees")
public class EmployerExchangeFees implements Serializable
{
	private static final long serialVersionUID = 1L;
	public enum ExchangeFeeTypeCode{NSF_FLAT_FEE, EMPLOYER_USER_FEE};
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employer_exchange_fees_seq")
	@SequenceGenerator(name = "employer_exchange_fees_seq", sequenceName = "employer_exchange_fees_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "fee_type_code")
	@Enumerated(EnumType.STRING)
	private ExchangeFeeTypeCode feeTypeCode;

	@ManyToOne
	@JoinColumn(name = "invoice_id")
	private EmployerInvoices employerInvoices;
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date updatedOn;
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the feeTypeCode
	 */
	public ExchangeFeeTypeCode getFeeTypeCode() {
		return feeTypeCode;
	}

	/**
	 * @param feeTypeCode the feeTypeCode to set
	 */
	public void setFeeTypeCode(ExchangeFeeTypeCode feeTypeCode) {
		this.feeTypeCode = feeTypeCode;
	}

	/**
	 * @return the employerInvoices
	 */
	public EmployerInvoices getEmployerInvoices() {
		return employerInvoices;
	}

	/**
	 * @param employerInvoices the employerInvoices to set
	 */
	public void setEmployerInvoices(EmployerInvoices employerInvoices) {
		this.employerInvoices = employerInvoices;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployerExchangeFees [id=");
		builder.append(id);
		builder.append(", feeTypeCode=");
		builder.append(feeTypeCode);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", createdOn=");
		builder.append(createdOn);
		builder.append("]");
		return builder.toString();
	}
}
