
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TransferDataTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferDataTemplate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataTemplateId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataTemplateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}scheduleItemStatus"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}scheduleItemStatusChangeDate"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dateSubmitted" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="insurerAttachment" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}InsurerAttachment" minOccurs="0"/>
 *         &lt;element name="exchangeAttachment" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}ExchangeAttachment" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataTemplateId",
    "dataTemplateType",
    "scheduleItemStatus",
    "scheduleItemStatusChangeDate",
    "comment",
    "dateSubmitted",
    "insurerAttachment",
    "exchangeAttachment"
})
@XmlRootElement(name="transferDataTemplate")
public class TransferDataTemplate {

    @XmlElement(required = true)
    protected String dataTemplateId;
    @XmlElement(required = true)
    protected String dataTemplateType;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true, nillable = true)
    protected String scheduleItemStatus;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scheduleItemStatusChangeDate;
    @XmlElement(required = true, nillable = true)
    protected String comment;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateSubmitted;
    protected InsurerAttachment insurerAttachment;
    protected ExchangeAttachment exchangeAttachment;

    /**
     * Gets the value of the dataTemplateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplateId() {
        return dataTemplateId;
    }

    /**
     * Sets the value of the dataTemplateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplateId(String value) {
        this.dataTemplateId = value;
    }

    /**
     * Gets the value of the dataTemplateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplateType() {
        return dataTemplateType;
    }

    /**
     * Sets the value of the dataTemplateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplateType(String value) {
        this.dataTemplateType = value;
    }

    /**
     * Gets the value of the scheduleItemStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleItemStatus() {
        return scheduleItemStatus;
    }

    /**
     * Sets the value of the scheduleItemStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleItemStatus(String value) {
        this.scheduleItemStatus = value;
    }

    /**
     * Gets the value of the scheduleItemStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScheduleItemStatusChangeDate() {
        return scheduleItemStatusChangeDate;
    }

    /**
     * Sets the value of the scheduleItemStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScheduleItemStatusChangeDate(XMLGregorianCalendar value) {
        this.scheduleItemStatusChangeDate = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the dateSubmitted property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateSubmitted() {
        return dateSubmitted;
    }

    /**
     * Sets the value of the dateSubmitted property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateSubmitted(XMLGregorianCalendar value) {
        this.dateSubmitted = value;
    }

    /**
     * Gets the value of the insurerAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link InsurerAttachment }
     *     
     */
    public InsurerAttachment getInsurerAttachment() {
        return insurerAttachment;
    }

    /**
     * Sets the value of the insurerAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsurerAttachment }
     *     
     */
    public void setInsurerAttachment(InsurerAttachment value) {
        this.insurerAttachment = value;
    }

    /**
     * Gets the value of the exchangeAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeAttachment }
     *     
     */
    public ExchangeAttachment getExchangeAttachment() {
        return exchangeAttachment;
    }

    /**
     * Sets the value of the exchangeAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeAttachment }
     *     
     */
    public void setExchangeAttachment(ExchangeAttachment value) {
        this.exchangeAttachment = value;
    }

}
