/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author pratap
 *
 */
public class EnrollmentOffExchangeDTO implements Serializable{

	/**
	 * 
	 */
	public EnrollmentOffExchangeDTO() {
		
	}
	
	
	private Integer planId;
	private Integer employerId; 
	private Integer employeeId; 
	private Integer userRoleId;   
	private Integer orderItemId;   
	
	private String csrLevel;
	
	private String planType;             
	private Float grossPremiumAmt;      
	private Float netPremiumAmt;        
	private String marketType;           
	private Float dependentContribution;
	private Date coverageStartDate;    
	private Float employeeContribution; 
	private Boolean sadpFlag;           
	private String enrollmentType;     
	private String userRoleType;       
	private Boolean isSubsidized;
	private String esignature;
	private String preferdTimeOfContact;


	private String houseHoldContactId;
	private String householdCaseId;
	private String houseHoldContactFirstName;     
	private String houseHoldContactMiddleName;
	private String houseHoldContactLastName;
	private String houseHoldContactHomeZip;       
	private String houseHoldContactPreferredEmail;
	private String houseHoldContactHomeCity;      
	private String houseHoldContactHomeState;     
	private String houseHoldContactHomeAddress1;
	private String houseHoldContactHomeAddress2;
	private String houseHoldContactCounty;
	private String houseHoldContactHomePhoneNumber;
	private String houseHoldContactHomeStateName;
	

	private String responsiblePersonId;         
	private String responsiblePersonFirstName;
	private String responsiblePersonMiddleName;
	private String responsiblePersonLastName;
	private String responsiblePersonPreferredEmail; 
	private String responsiblePersonSsn;            
	private String responsiblePersonPrimaryPhone;   
	private String responsiblePersonType;           
	private String responsiblePersonHomeCity;       
	private String responsiblePersonHomeZip;        
	private String responsiblePersonHomeState;      
	private String responsiblePersonHomeAddress1;   
	private String responsiblePersonHomeAddress2; 
	private String responsiblePersonHomeCounty;
	
	private List<EnrolleeOffExchangeDTO> enrolles = new ArrayList<EnrolleeOffExchangeDTO>();
	
	private String cmrHouseholdId;
	private Float aptcAmt ;
	private Long ssapApplicationId;
	private String exchangeType;

	
	
	public Float getAptcAmt() {
		return aptcAmt;
	}

	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public String getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(String cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}

	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}

	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}

	public Integer getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Float getDependentContribution() {
		return dependentContribution;
	}

	public void setDependentContribution(Float dependentContribution) {
		this.dependentContribution = dependentContribution;
	}

	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Float getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	public Boolean getSadpFlag() {
		return sadpFlag;
	}

	public void setSadpFlag(Boolean sadpFlag) {
		this.sadpFlag = sadpFlag;
	}

	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public String getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public Integer getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	public String getUserRoleType() {
		return userRoleType;
	}

	public void setUserRoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}

	public Boolean getIsSubsidized() {
		return isSubsidized;
	}

	public void setIsSubsidized(Boolean isSubsidized) {
		this.isSubsidized = isSubsidized;
	}

	public String getEsignature() {
		return esignature;
	}

	public void setEsignature(String esignature) {
		this.esignature = esignature;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getPreferdTimeOfContact() {
		return preferdTimeOfContact;
	}

	public void setPreferdTimeOfContact(String preferdTimeOfContact) {
		this.preferdTimeOfContact = preferdTimeOfContact;
	}

	public String getHouseHoldContactId() {
		return houseHoldContactId;
	}

	public void setHouseHoldContactId(String houseHoldContactId) {
		this.houseHoldContactId = houseHoldContactId;
	}

	public String getHouseHoldContactFirstName() {
		return houseHoldContactFirstName;
	}

	public void setHouseHoldContactFirstName(String houseHoldContactFirstName) {
		this.houseHoldContactFirstName = houseHoldContactFirstName;
	}

	public String getHouseHoldContactMiddleName() {
		return houseHoldContactMiddleName;
	}

	public void setHouseHoldContactMiddleName(String houseHoldContactMiddleName) {
		this.houseHoldContactMiddleName = houseHoldContactMiddleName;
	}

	public String getHouseHoldContactLastName() {
		return houseHoldContactLastName;
	}

	public void setHouseHoldContactLastName(String houseHoldContactLastName) {
		this.houseHoldContactLastName = houseHoldContactLastName;
	}

	public String getHouseHoldContactHomeZip() {
		return houseHoldContactHomeZip;
	}

	public void setHouseHoldContactHomeZip(String houseHoldContactHomeZip) {
		this.houseHoldContactHomeZip = houseHoldContactHomeZip;
	}

	public String getHouseHoldContactPreferredEmail() {
		return houseHoldContactPreferredEmail;
	}

	public void setHouseHoldContactPreferredEmail(
			String houseHoldContactPreferredEmail) {
		this.houseHoldContactPreferredEmail = houseHoldContactPreferredEmail;
	}

	public String getHouseHoldContactHomeCity() {
		return houseHoldContactHomeCity;
	}

	public void setHouseHoldContactHomeCity(String houseHoldContactHomeCity) {
		this.houseHoldContactHomeCity = houseHoldContactHomeCity;
	}

	public String getHouseHoldContactHomeState() {
		return houseHoldContactHomeState;
	}

	public void setHouseHoldContactHomeState(String houseHoldContactHomeState) {
		this.houseHoldContactHomeState = houseHoldContactHomeState;
	}

	public String getHouseHoldContactHomeAddress1() {
		return houseHoldContactHomeAddress1;
	}

	public void setHouseHoldContactHomeAddress1(String houseHoldContactHomeAddress1) {
		this.houseHoldContactHomeAddress1 = houseHoldContactHomeAddress1;
	}

	public String getHouseHoldContactHomeAddress2() {
		return houseHoldContactHomeAddress2;
	}

	public void setHouseHoldContactHomeAddress2(String houseHoldContactHomeAddress2) {
		this.houseHoldContactHomeAddress2 = houseHoldContactHomeAddress2;
	}

	public String getResponsiblePersonId() {
		return responsiblePersonId;
	}

	public void setResponsiblePersonId(String responsiblePersonId) {
		this.responsiblePersonId = responsiblePersonId;
	}

	public String getResponsiblePersonFirstName() {
		return responsiblePersonFirstName;
	}

	public void setResponsiblePersonFirstName(String responsiblePersonFirstName) {
		this.responsiblePersonFirstName = responsiblePersonFirstName;
	}

	public String getResponsiblePersonMiddleName() {
		return responsiblePersonMiddleName;
	}

	public void setResponsiblePersonMiddleName(String responsiblePersonMiddleName) {
		this.responsiblePersonMiddleName = responsiblePersonMiddleName;
	}

	public String getResponsiblePersonLastName() {
		return responsiblePersonLastName;
	}

	public void setResponsiblePersonLastName(String responsiblePersonLastName) {
		this.responsiblePersonLastName = responsiblePersonLastName;
	}

	public String getResponsiblePersonPreferredEmail() {
		return responsiblePersonPreferredEmail;
	}

	public void setResponsiblePersonPreferredEmail(
			String responsiblePersonPreferredEmail) {
		this.responsiblePersonPreferredEmail = responsiblePersonPreferredEmail;
	}

	public String getResponsiblePersonSsn() {
		return responsiblePersonSsn;
	}

	public void setResponsiblePersonSsn(String responsiblePersonSsn) {
		this.responsiblePersonSsn = responsiblePersonSsn;
	}

	public String getResponsiblePersonPrimaryPhone() {
		return responsiblePersonPrimaryPhone;
	}

	public void setResponsiblePersonPrimaryPhone(
			String responsiblePersonPrimaryPhone) {
		this.responsiblePersonPrimaryPhone = responsiblePersonPrimaryPhone;
	}

	public String getResponsiblePersonType() {
		return responsiblePersonType;
	}

	public void setResponsiblePersonType(String responsiblePersonType) {
		this.responsiblePersonType = responsiblePersonType;
	}

	public String getResponsiblePersonHomeCity() {
		return responsiblePersonHomeCity;
	}

	public void setResponsiblePersonHomeCity(String responsiblePersonHomeCity) {
		this.responsiblePersonHomeCity = responsiblePersonHomeCity;
	}

	public String getResponsiblePersonHomeZip() {
		return responsiblePersonHomeZip;
	}

	public void setResponsiblePersonHomeZip(String responsiblePersonHomeZip) {
		this.responsiblePersonHomeZip = responsiblePersonHomeZip;
	}

	public String getResponsiblePersonHomeState() {
		return responsiblePersonHomeState;
	}

	public void setResponsiblePersonHomeState(String responsiblePersonHomeState) {
		this.responsiblePersonHomeState = responsiblePersonHomeState;
	}

	public String getResponsiblePersonHomeAddress1() {
		return responsiblePersonHomeAddress1;
	}

	public void setResponsiblePersonHomeAddress1(
			String responsiblePersonHomeAddress1) {
		this.responsiblePersonHomeAddress1 = responsiblePersonHomeAddress1;
	}

	public String getResponsiblePersonHomeAddress2() {
		return responsiblePersonHomeAddress2;
	}

	public void setResponsiblePersonHomeAddress2(
			String responsiblePersonHomeAddress2) {
		this.responsiblePersonHomeAddress2 = responsiblePersonHomeAddress2;
	}

	public String getResponsiblePersonHomeCounty() {
		return responsiblePersonHomeCounty;
	}

	public void setResponsiblePersonHomeCounty(String responsiblePersonHomeCounty) {
		this.responsiblePersonHomeCounty = responsiblePersonHomeCounty;
	}

	public String getHouseHoldContactHomePhoneNumber() {
		return houseHoldContactHomePhoneNumber;
	}

	public void setHouseHoldContactHomePhoneNumber(
			String houseHoldContactHomePhoneNumber) {
		this.houseHoldContactHomePhoneNumber = houseHoldContactHomePhoneNumber;
	}

	public String getHouseHoldContactCounty() {
		return houseHoldContactCounty;
	}

	public void setHouseHoldContactCounty(String houseHoldContactCounty) {
		this.houseHoldContactCounty = houseHoldContactCounty;
	}

	public List<EnrolleeOffExchangeDTO> getEnrolles() {
		return enrolles;
	}

	public void setEnrolles(List<EnrolleeOffExchangeDTO> enrolles) {
		this.enrolles = enrolles;
	}
	
	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getHouseHoldContactHomeStateName() {
		return houseHoldContactHomeStateName;
	}

	public void setHouseHoldContactHomeStateName(
			String houseHoldContactHomeStateName) {
		this.houseHoldContactHomeStateName = houseHoldContactHomeStateName;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	@Override
	public String toString() {
		return "EnrollmentOffExchangeDTO [planId=" + planId + ", employerId="
				+ employerId + ", employeeId=" + employeeId + ", userRoleId="
				+ userRoleId + ", orderItemId=" + orderItemId + ", csrLevel="
				+ csrLevel + ", planType=" + planType + ", grossPremiumAmt="
				+ grossPremiumAmt + ", netPremiumAmt=" + netPremiumAmt
				+ ", marketType=" + marketType + ", dependentContribution="
				+ dependentContribution + ", coverageStartDate="
				+ coverageStartDate + ", employeeContribution="
				+ employeeContribution + ", sadpFlag=" + sadpFlag
				+ ", enrollmentType=" + enrollmentType + ", userRoleType="
				+ userRoleType + ", isSubsidized=" + isSubsidized
				+ ", esignature=" + esignature + ", preferdTimeOfContact="
				+ preferdTimeOfContact + ", houseHoldContactId="
				+ houseHoldContactId + ", householdCaseId=" + householdCaseId
				+ ", houseHoldContactFirstName=" + houseHoldContactFirstName
				+ ", houseHoldContactMiddleName=" + houseHoldContactMiddleName
				+ ", houseHoldContactLastName=" + houseHoldContactLastName
				+ ", houseHoldContactHomeZip=" + houseHoldContactHomeZip
				+ ", houseHoldContactPreferredEmail="
				+ houseHoldContactPreferredEmail
				+ ", houseHoldContactHomeCity=" + houseHoldContactHomeCity
				+ ", houseHoldContactHomeState=" + houseHoldContactHomeState
				+ ", houseHoldContactHomeAddress1="
				+ houseHoldContactHomeAddress1
				+ ", houseHoldContactHomeAddress2="
				+ houseHoldContactHomeAddress2 + ", houseHoldContactCounty="
				+ houseHoldContactCounty + ", houseHoldContactHomePhoneNumber="
				+ houseHoldContactHomePhoneNumber
				+ ", houseHoldContactHomeStateName="
				+ houseHoldContactHomeStateName + ", responsiblePersonId="
				+ responsiblePersonId + ", responsiblePersonFirstName="
				+ responsiblePersonFirstName + ", responsiblePersonMiddleName="
				+ responsiblePersonMiddleName + ", responsiblePersonLastName="
				+ responsiblePersonLastName
				+ ", responsiblePersonPreferredEmail="
				+ responsiblePersonPreferredEmail + ", responsiblePersonSsn="
				+ responsiblePersonSsn + ", responsiblePersonPrimaryPhone="
				+ responsiblePersonPrimaryPhone + ", responsiblePersonType="
				+ responsiblePersonType + ", responsiblePersonHomeCity="
				+ responsiblePersonHomeCity + ", responsiblePersonHomeZip="
				+ responsiblePersonHomeZip + ", responsiblePersonHomeState="
				+ responsiblePersonHomeState
				+ ", responsiblePersonHomeAddress1="
				+ responsiblePersonHomeAddress1
				+ ", responsiblePersonHomeAddress2="
				+ responsiblePersonHomeAddress2
				+ ", responsiblePersonHomeCounty="
				+ responsiblePersonHomeCounty + ", enrolles=" + enrolles
				+ ", cmrHouseholdId=" + cmrHouseholdId + ", aptcAmt=" + aptcAmt
				+ ", ssapApplicationId=" + ssapApplicationId
				+ ", exchangeType=" + exchangeType + "]";
	}



}
