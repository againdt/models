package com.getinsured.hix.consumer.history;

import java.util.Date;



public class ConsumerEnrollEventHistory extends ConsumerEventMetaData {
	
	private Integer enrollmentId;
	private Integer policyId;
	private String insureanceCompany;
	private String planName;
	private String policyNumber;
	private Date effectiveDate;
	private String enrollmentStatus;
	private String eventName;
	private String planType;
	private String monthlyPremium;
	private String aptc;
	private String previousEnrollmentStatus;
	private String currentEnrollmentStatus;
	private String exchangeType;
	private Integer updatedBy;
	private Date updatedOn;
	private Date benefitEndDate;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Integer getPolicyId() {
		return policyId;
	}
	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}
	public String getInsureanceCompany() {
		return insureanceCompany;
	}
	public void setInsureanceCompany(String insureanceCompany) {
		this.insureanceCompany = insureanceCompany;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(String monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	public String getAptc() {
		return aptc;
	}
	public void setAptc(String aptc) {
		this.aptc = aptc;
	}
	public String getPreviousEnrollmentStatus() {
		return previousEnrollmentStatus;
	}
	public void setPreviousEnrollmentStatus(String previousEnrollmentStatus) {
		this.previousEnrollmentStatus = previousEnrollmentStatus;
	}
	public String getCurrentEnrollmentStatus() {
		return currentEnrollmentStatus;
	}
	public void setCurrentEnrollmentStatus(String currentEnrollmentStatus) {
		this.currentEnrollmentStatus = currentEnrollmentStatus;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Date getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	
	public String toJson() {
		return " {\"enrollmentId\":" + enrollmentId
				+ ", \"policyId\":" + policyId + ", \"insureanceCompany\":\""
				+ insureanceCompany + "\", \"planName\":\"" + planName
				+ "\", \"policyNumber\":\"" + policyNumber + "\", effectiveDate':"
				+ effectiveDate + "}";
	}
	
	
	
	
}
