package com.getinsured.hix.model;

import java.util.List;
import java.util.Map;

public class GroupList {

	private int id;
	private String name;
	private float monthlyAptc;
	private float yearlyAptc;
	private List<Map<String, String>> personList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public float getMonthlyAptc() {
		return monthlyAptc;
	}

	public void setMonthlyAptc(float monthlyAptc) {
		this.monthlyAptc = monthlyAptc;
	}

	public float getYearlyAptc() {
		return yearlyAptc;
	}

	public void setYearlyAptc(float yearlyAptc) {
		this.yearlyAptc = yearlyAptc;
	}

	public List<Map<String, String>> getPersonList() {
		return personList;
	}

	public void setPersonList(List<Map<String, String>> personList) {
		this.personList = personList;
	}
	
}
