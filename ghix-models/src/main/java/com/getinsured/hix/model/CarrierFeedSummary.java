package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="CARRIER_FEED_SUMMARY")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class CarrierFeedSummary 
{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARRIER_FEED_SUMMARY_SEQ")
	@SequenceGenerator(name = "CARRIER_FEED_SUMMARY_SEQ", sequenceName = "CARRIER_FEED_SUMMARY_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "FILE_NAME", length = 1000)
	private String fileName;
	
	@Column(name = "CARRIER_STATUS", length = 50)
	private String carrierStatus;
	
	@Column(name = "CARRIER_NAME", length = 1000)
	private String carrierName;
	
	@Column(name="ISSUER_ID")
	private Integer issuerId;
	
	@Column(name="FEED_TYPE")
	private String feedType;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;
	
	/*
	@OneToOne(targetEntity = AccountUser.class, optional=true, cascade = {CascadeType.ALL})
	@JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", insertable = false, updatable = false)
	private AccountUser createdBy;
	*/
	
	@Column(name="CREATED_BY")
	private Integer createdUserId;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;

	public String getFeedType() {
		return feedType;
	}

	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;

    @Formula("(select count(s.id) from CARRIER_FEED_DETAILS s where s.CARRIER_FEED_SUMMARY_ID = id)")
    private long totalRecordsProcessed;

    @Formula("(select count(s.id) from CARRIER_FEED_DETAILS s where s.CARRIER_FEED_SUMMARY_ID = id and s.STATUS like 'FAIL%')")
    private long failRecordCount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCarrierStatus() {
		return carrierStatus;
	}

	public void setCarrierStatus(String carrierStatus) {
		this.carrierStatus = carrierStatus;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

    public long getTotalRecordsProcessed() {
        return totalRecordsProcessed;
    }

    public long getFailRecordCount() {
        return failRecordCount;
    }
    
    public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
    /*public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}*/
	
	public Integer getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(Integer createdUserId) {
		this.createdUserId = createdUserId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
