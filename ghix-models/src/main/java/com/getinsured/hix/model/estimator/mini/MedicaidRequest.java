package com.getinsured.hix.model.estimator.mini;

import java.util.List;

import com.getinsured.hix.model.prescreen.calculator.GHIXRequest;

/**
 * Request model for the new Medicaid/CHIP calculator
 * of the mini-estimator
 * 
 * @author Nikhil Talreja
 * @since 08 July, 2013
 *
 */

public class MedicaidRequest extends GHIXRequest{
	
	private String stateCode;
	
	private double fplPercentage;
	
	private List<FamilyMember> members;
	private boolean householdParent;
	private int householdChildCount;
	
	public String effectiveStartDate;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public double getFplPercentage() {
		return fplPercentage;
	}

	public void setFplPercentage(double fplPercentage) {
		this.fplPercentage = fplPercentage;
	}

	public List<FamilyMember> getMembers() {
		return members;
	}

	public void setMembers(List<FamilyMember> members) {
		this.members = members;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MedicaidRequest [stateCode=");
		builder.append(stateCode);
		builder.append(", fplPercentage=");
		builder.append(fplPercentage);
		builder.append(", members=");
		builder.append(members);
		return builder.toString();
	}
	public boolean isHouseholdParent() {
		return householdParent;
	}
	public void setHouseholdParent(boolean householdParent) {
		this.householdParent = householdParent;
	}
	public int getHouseholdChildCount() {
		return householdChildCount;
	}
	public void setHouseholdChildCount(int householdChildCount) {
		this.householdChildCount = householdChildCount;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	
}
