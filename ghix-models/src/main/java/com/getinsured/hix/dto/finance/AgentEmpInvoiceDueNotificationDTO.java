package com.getinsured.hix.dto.finance;

import java.io.Serializable;
import java.util.List;

/**
 * Jira ID: HIX-55931 Consolidated Employer Payment Past Due notice for Agents
 * @since 
 * @author Sharma_k
 *
 */
public class AgentEmpInvoiceDueNotificationDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String brokerFirstName;
	private String brokerLastName;
	private Integer brokerID;
	private List<EmployersDueInvoiceDTO> EmployersDueInvoiceDTOList;
	
	/**
	 * @return the brokerFirstName
	 */
	public String getBrokerFirstName() {
		return brokerFirstName;
	}
	/**
	 * @param brokerFirstName the brokerFirstName to set
	 */
	public void setBrokerFirstName(String brokerFirstName) {
		this.brokerFirstName = brokerFirstName;
	}
	/**
	 * @return the brokerLastName
	 */
	public String getBrokerLastName() {
		return brokerLastName;
	}
	/**
	 * @param brokerLastName the brokerLastName to set
	 */
	public void setBrokerLastName(String brokerLastName) {
		this.brokerLastName = brokerLastName;
	}
	/**
	 * @return the brokerID
	 */
	public Integer getBrokerID() {
		return brokerID;
	}
	/**
	 * @param brokerID the brokerID to set
	 */
	public void setBrokerID(Integer brokerID) {
		this.brokerID = brokerID;
	}
	/**
	 * @return the employersDueInvoiceDTOList
	 */
	public List<EmployersDueInvoiceDTO> getEmployersDueInvoiceDTOList() {
		return EmployersDueInvoiceDTOList;
	}
	/**
	 * @param employersDueInvoiceDTOList the employersDueInvoiceDTOList to set
	 */
	public void setEmployersDueInvoiceDTOList(
			List<EmployersDueInvoiceDTO> employersDueInvoiceDTOList) {
		EmployersDueInvoiceDTOList = employersDueInvoiceDTOList;
	}
}
