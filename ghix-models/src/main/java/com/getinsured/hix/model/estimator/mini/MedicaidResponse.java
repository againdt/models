/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.List;

import com.getinsured.hix.model.prescreen.calculator.GHIXResponse;

/**
 * Response model for the new Medicaid/CHIP calculator
 * of the mini-estimator
 * 
 * @author Nikhil Talreja
 * @since 08 July, 2013
 *
 */

public class MedicaidResponse extends GHIXResponse{
	
	private boolean isHouseholdMedicaidEligible;
	
	private boolean isHouseholdCHIPEligible;
	
	private boolean isHouseholdMedicareEligible;
	
	private boolean isNativeAmericanHousehold;
	private List<FamilyMember> members;
	
	private String sbeWebsite;
	
	private String sbePhone;
	
	private String medicaidExpansion;
	
	public String getMedicaidExpansion() {
		return medicaidExpansion;
	}


	public void setMedicaidExpansion(String medicaidExpansion) {
		this.medicaidExpansion = medicaidExpansion;
	}


	public boolean isHouseholdMedicaidEligible() {
		return isHouseholdMedicaidEligible;
	}


	public void setHouseholdMedicaidEligible(boolean isHouseholdMedicaidEligible) {
		this.isHouseholdMedicaidEligible = isHouseholdMedicaidEligible;
	}


	public boolean isHouseholdCHIPEligible() {
		return isHouseholdCHIPEligible;
	}


	public void setHouseholdCHIPEligible(boolean isHouseholdCHIPEligible) {
		this.isHouseholdCHIPEligible = isHouseholdCHIPEligible;
	}


	public boolean isHouseholdMedicareEligible() {
		return isHouseholdMedicareEligible;
	}


	public void setHouseholdMedicareEligible(boolean isHouseholdMedicareEligible) {
		this.isHouseholdMedicareEligible = isHouseholdMedicareEligible;
	}


	public List<FamilyMember> getMembers() {
		return members;
	}


	public void setMembers(List<FamilyMember> members) {
		this.members = members;
	}


	public String getSbeWebsite() {
		return sbeWebsite;
	}


	public void setSbeWebsite(String sbeWebsite) {
		this.sbeWebsite = sbeWebsite;
	}


	public String getSbePhone() {
		return sbePhone;
	}


	public void setSbePhone(String sbePhone) {
		this.sbePhone = sbePhone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MedicaidResponse [isHouseholdMedicaidEligible=");
		builder.append(isHouseholdMedicaidEligible);
		builder.append(", isHouseholdCHIPEligible=");
		builder.append(isHouseholdCHIPEligible);
		builder.append(", isHouseholdMedicareEligible=");
		builder.append(isHouseholdMedicareEligible);
		builder.append(", members=");
		builder.append(members);
		builder.append(", sbeWebsite=");
		builder.append(sbeWebsite);
		builder.append(", sbePhone=");
		builder.append(sbePhone);
		builder.append(", medicaidExpansion=");
		builder.append(medicaidExpansion);
		builder.append(", errCode=");
		builder.append(getErrCode());
		builder.append(", errMsg=");
		builder.append(getErrMsg());
		builder.append(", execDuration=");
		builder.append(getExecDuration());
		builder.append("]");
		return builder.toString();
	}
	public boolean isNativeAmericanHousehold() {
		return isNativeAmericanHousehold;
	}
	public void setNativeAmericanHousehold(boolean isNativeAmericanHousehold) {
		this.isNativeAmericanHousehold = isNativeAmericanHousehold;
	}
}
