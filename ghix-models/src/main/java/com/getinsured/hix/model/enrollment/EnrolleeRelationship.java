/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;


/**
 * @author panda_p
 *
 */
@Audited
@Entity
@Table(name="ENROLLEE_RELATIONSHIP")
public class EnrolleeRelationship implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLEE_RELATIONSHIP_SEQ")
	@SequenceGenerator(name = "ENROLLEE_RELATIONSHIP_SEQ", sequenceName = "ENROLLEE_RELATIONSHIP_SEQ", allocationSize = 1)
	private Integer id;
	
	
	//@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(cascade= {CascadeType.ALL})
    @JoinColumn(name="SOURCE_ENROLEE_ID")
	private Enrollee sourceEnrollee;
	
	@ManyToOne(cascade= {CascadeType.ALL})
    @JoinColumn(name="TARGET_ENROLEE_ID")
	private Enrollee targetEnrollee;

//	@NotAudited 
//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "RELATIONSHIP_LKP")
	private LookupValue relationshipLkp;	
	

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private AccountUser createdBy;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Enrollee getTargetEnrollee() {
		return targetEnrollee;
	}

	public void setTargetEnrollee(Enrollee targetEnrollee) {
		this.targetEnrollee = targetEnrollee;
	}

	public Enrollee getSourceEnrollee() {
		return sourceEnrollee;
	}

	public void setSourceEnrollee(Enrollee sourceEnrollee) {
		this.sourceEnrollee = sourceEnrollee;
	}

	public LookupValue getRelationshipLkp() {
		return relationshipLkp;
	}

	public void setRelationshipLkp(LookupValue relationshipLkp) {
		this.relationshipLkp = relationshipLkp;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}

	
	
}
