package com.getinsured.hix.dto.planmgmt.microservice;

public class AuditDTO {

	private String createBy;
	private String lastUpdatedBy;

	public AuditDTO() {
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
