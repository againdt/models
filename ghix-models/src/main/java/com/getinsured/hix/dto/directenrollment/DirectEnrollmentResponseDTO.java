package com.getinsured.hix.dto.directenrollment;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getinsured.hix.dto.directenrollment.DirectEnrollmentStatus.SendStatus;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;


public class DirectEnrollmentResponseDTO<T> {

	public static enum STATUS{
		SUCCESS, FAILURE
	}
	/**
	 * status of this call,
	 * unless specified
	 */
	private STATUS status = STATUS.SUCCESS;
	
	/**
	 * execution time 
	 */
	private long execDuration;
	
	/**
	 * List of error codes and messages
	 */
	private Map<Integer, String> errors = new HashMap<Integer, String>();
	
	/**
	 * actual object
	 */
	private T object;
	
	/**
	 * variable to keep track of start time
	 */
	private transient long startTime;

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public long getExecDuration() {
		return execDuration;
	}

	public void setExecDuration(long execDuration) {
		this.execDuration = execDuration;
	}

	public Map<Integer, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<Integer, String> errors) {
		this.errors = errors;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}
	
	public void startResponse() {
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		this.execDuration = endTime - this.startTime;
	}
	
	public void addError(Integer id, String error){
		this.errors.put(id, error);
	}
	
	public void setFailure(){
		this.status = STATUS.FAILURE;
	}
	
	@JsonIgnore
	public void setSuccess(){
		this.status = STATUS.SUCCESS;
	}
	
	public void setSuccess(boolean flag){
		this.status = STATUS.SUCCESS;
	}
	
	@JsonIgnore
	public boolean isSuccess(){
		
		if(this.status==null){
			return false;
		}
		
		if(STATUS.SUCCESS.equals(this.status)){
			return true;
		}
		
		return false;
	}
	
	public static void main(String args[]){
		DirectEnrollmentResponseDTO<SendApplicationResponseDTO> dto = new DirectEnrollmentResponseDTO<SendApplicationResponseDTO>();
		Map<Integer, String> errors = new HashMap<Integer, String>();
		errors.put(0, "schema validation error");
		errors.put(1, "The elements in namespace has incomplete content");
		errors.put(2, "Some more bad things");
		dto.setErrors(errors);
		dto.setStatus(STATUS.FAILURE);
		SendApplicationResponseDTO resp = new SendApplicationResponseDTO();
		resp.setStatus(SendStatus.REJECTED);
		resp.setWebserviceResponse("webservice response");
		dto.setObject(resp);
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		System.out.println(gson.toJson(dto));
		
	}
	
}
