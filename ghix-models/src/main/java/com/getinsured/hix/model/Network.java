package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

/**
 * The persistent class for the network database table.
 */
@Audited
@Entity
@Table(name="network")
public class Network implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static enum CERTIFIED {
		YES, NO;
	}
	public static enum VERIFIED {
		YES, NO;
	}
	
	public enum NetworkType{ 
		PPO, HMO; 
	}
	
	public static enum PROVIDER_DATA {
		YES, NO;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Network_Seq")
	@SequenceGenerator(name = "Network_Seq", sequenceName = "network_seq", allocationSize = 1)
	private int id;
	
	//@ManyToOne(cascade = CascadeType.ALL)
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@Column(name="name", length=200)
	private String name;
	
	@Column(name="gi_network_type", length=5)
	@Enumerated(EnumType.STRING)
	private NetworkType type;
	
	@Column(name="effective_start_date")
	private Date effectiveStartDate;
	
	@Column(name="effective_end_date")
	private Date effectiveEndDate;
	
	@NotAudited
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "network", fetch = FetchType.EAGER)
	private Set<ProviderNetwork> providerNetwork = new HashSet<ProviderNetwork>(0);
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	@Column(name="NETWORK_ID", length=6)
	private String networkID;
	
	@Column(name="NETWORK_URL")
	private String networkURL;
	
	@Column(name = "APPLICABLE_YEAR")
    private Integer applicableYear;
	
	@Column(name = "NETWORK_KEY")
	private String networkKey;	
	
	@Column(name = "certified", length = 5)
	@Enumerated(EnumType.STRING)
	private CERTIFIED certified;
	
	@Column(name = "verified", length = 5)
	@Enumerated(EnumType.STRING)
	private VERIFIED verified;
	
	@Column(name = "has_provider_data", length = 5)
	@Enumerated(EnumType.STRING)
	private PROVIDER_DATA hasProviderData;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NetworkType getType() {
		return type;
	}

	public void setType(NetworkType type) {
		this.type = type;
	}
	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Set<ProviderNetwork> getProviderNetwork() {
		return providerNetwork;
	}

	public void setProviderNetwork(Set<ProviderNetwork> providerNetwork) {
		this.providerNetwork = providerNetwork;
	}
	
	/**
	 * Getter for 'creationTimestamp'
	 * 
	 * @return the creationTimestamp
	 */
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * Setter for 'creationTimestamp'
	 * 
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	/**
	 * Getter for 'networkID'.
	 *
	 * @return the networkID.
	 */
	public String getNetworkID() {
		return this.networkID;
	}

	/**
	 * Setter for 'networkID'.
	 *
	 * @param networkID the networkID to set.
	 */
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}

	/** 
	 * Getter for 'networkURL'.
	 *
	 * @return the networkURL.
	 */
	public String getNetworkURL() {
		return this.networkURL;
	}

	/** 
	 * Setter for 'networkURL'.
	 *
	 * @param networkURL the networkURL to set.
	 */
	public void setNetworkURL(String networkURL) {
		this.networkURL = networkURL;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}
	
	public String getNetworkKey() {
		return networkKey;
	}

	public void setNetworkKey(String networkKey) {
		this.networkKey = networkKey;
	}
	
	public Map<CERTIFIED, String> getCertifiedList() {
		Map<CERTIFIED, String> enumMap = new EnumMap<CERTIFIED, String>(
				CERTIFIED.class);
		enumMap.put(CERTIFIED.YES, "Yes");
		enumMap.put(CERTIFIED.NO, "No");
		return enumMap;
	}
	
	public Map<VERIFIED, String> getVerifiedList() {
		Map<VERIFIED, String> enumMap = new EnumMap<VERIFIED, String>(
				VERIFIED.class);
		enumMap.put(VERIFIED.YES, "Yes");
		enumMap.put(VERIFIED.NO, "No");
		return enumMap;
	}

	public Map<PROVIDER_DATA, String> getHasProviderDataList() {
		Map<PROVIDER_DATA, String> enumMap = new EnumMap<PROVIDER_DATA, String>(PROVIDER_DATA.class);
		enumMap.put(PROVIDER_DATA.YES, "Yes");
		enumMap.put(PROVIDER_DATA.NO, "No");
		return enumMap;
	}	
	
	public void setCertified(CERTIFIED certified) {
		this.certified = certified;
	}

	public void setVerified(VERIFIED verified) {
		this.verified = verified;
	}

	public CERTIFIED getCertified() {
		return certified;
	}

	public VERIFIED getVerified() {
		return verified;
	}

	public PROVIDER_DATA getHasProviderData() {
		return hasProviderData;
	}

	public void setHasProviderData(PROVIDER_DATA hasProviderData) {
		this.hasProviderData = hasProviderData;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
