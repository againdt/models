--liquibase formatted sql

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('ADD_ACCREDITATION_DOCUMENTS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('ADD_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('ADD_REPRESENTATIVE')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_ACCREDITATION_DOCUMENTS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_COMPNAY_PROFILE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_INDIVIDUAL_MARKET_PROFILE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_PLAN_BENEFITS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_PLAN_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_PLAN_RATES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_REPRESENTATIVE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('EDIT_SHOP_MARKET_PROFILE')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('MANAGE_QHP_PLANS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('MANAGE_REPRESENTATIVE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('MANAGE_SADP_PLANS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('UPDATE_CERTIFICATION_STATUS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('UPDATE_ENROLLMENT_AVAILABILITY')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('UPLOAD_NEW_BENEFITS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('UPLOAD_NEW_RATES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('UPDATE_PROVIDER_NETWORK_ID')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_ACCREDITATION_DOCUMENTS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_CERTIFICATION_STATUS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_COMPNAY_PROFILE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_ENROLLMENT_AVAILABILITY')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_FINANCIAL_LIST_INFO')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_INDIVIDUAL_MARKET_PROFILE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_ISSUER_PROFILE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VERIFY_PLAN')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_PLAN_BENEFITS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_PLAN_DETAILS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_PLAN_HISTORY')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_PLAN_RATES')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_PROVIDER_NETWORK_ID')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='ISSUER_REPRESENTATIVE'), (select id from permissions   where name=UPPER('VIEW_SHOP_MARKET_PROFILE')));








