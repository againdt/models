package com.getinsured.hix.model.emx;

import java.io.Serializable;
import java.util.List;



public class EmxMemberVerificationRequest implements Serializable  {


	private static final long serialVersionUID = 1230951042501540394L;

	private String firstName;
	private String lastName;
	private String lastFourSsn; //aka last 4 SSN

	private String accessPin;


	private String censusType;

	private long affiliateId;

	private int flowId;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastFourSsn() {
		return lastFourSsn;
	}

	public void setLastFourSsn(String lastFourSsn) {
		this.lastFourSsn = lastFourSsn;
	}

	public String getAccessPin() {
		return accessPin;
	}

	public void setAccessPin(String accessPin) {
		this.accessPin = accessPin;
	}

	public String getCensusType() {
		return censusType;
	}

	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}

	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public int getFlowId() {
		return flowId;
	}

	public void setFlowId(int flowId) {
		this.flowId = flowId;
	}

	}
