package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.model.Comment;

@Entity
@Table(name="ENRL_RECON_COMMENT")
public class EnrlReconComment  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_comment_seq")
	@SequenceGenerator(name = "enrl_recon_comment_seq", sequenceName = "ENRL_RECON_COMMENT_SEQ", allocationSize = 1)
	private Integer id;
	
	@JoinColumn(name="DISCREPANCY_ID")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EnrlReconDiscrepancy discrepancy;
	
	@JoinColumn(name="COMMENT_ID")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Comment comment;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public EnrlReconDiscrepancy getDiscrepancy() {
		return discrepancy;
	}
	public void setDiscrepancy(EnrlReconDiscrepancy discrepancy) {
		this.discrepancy = discrepancy;
	}
	public Comment getComment() {
		return comment;
	}
	public void setComment(Comment comment) {
		this.comment = comment;
	}
}
