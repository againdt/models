package com.getinsured.hix.dto.broker;

import java.io.Serializable;

/**
 * Implemented for IND-47
 * 
 * @author kanthi_v
 * 
 */
public class DesignateBrokerDTO implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int brokerId;
	private Integer employerId;
	private Integer individualId;
	private String status;
	private String role;

	public DesignateBrokerDTO() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(int brokerId) {
		this.brokerId = brokerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public Integer getEmployerId() {
		return this.employerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getIndividualId() {
		return individualId;
	}

	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}

	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "DesignateBrokerDTO details: Id = "+id+", BrokerId = "+brokerId+", " +
				"EmployerId = "+employerId+", IndividualId = "+individualId+", Status = "+status+", Role = "+role;
	}
}