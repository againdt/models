
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Month Safe Harbor Indicator Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-05-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;A group that contains monthly safe harbor indicators.&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for MonthSafeHarborIndicatorDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MonthSafeHarborIndicatorDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MonthNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}SafeHarborInd"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthSafeHarborIndicatorDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "monthNum",
    "safeHarborInd"
})
public class MonthSafeHarborIndicatorDetailType {

    @XmlElement(name = "MonthNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected int monthNum;
    @XmlElement(name = "SafeHarborInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String safeHarborInd;

    /**
     * Gets the value of the monthNum property.
     * 
     */
    public int getMonthNum() {
        return monthNum;
    }

    /**
     * Sets the value of the monthNum property.
     * 
     */
    public void setMonthNum(int value) {
        this.monthNum = value;
    }

    /**
     * Gets the value of the safeHarborInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSafeHarborInd() {
        return safeHarborInd;
    }

    /**
     * Sets the value of the safeHarborInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSafeHarborInd(String value) {
        this.safeHarborInd = value;
    }

}
