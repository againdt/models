package com.getinsured.hix.dto.enrollment;

import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;

public class EnrollmentByMemberListRequestDTO {

	public EnrollmentByMemberListRequestDTO() {

	}

	private List<EnrollmentMembersDTO> members;

	private String householdCaseId;

	private String coverageYear;

	/*
	 * TDB: If we want to get enrollments based on enrollment status then how we are
	 * going to handle future terminations ??
	 */
	private List<EnrollmentStatus> enrollmentStatus;

	public List<EnrollmentMembersDTO> getMembers() {
		return members;
	}

	public void setMembers(List<EnrollmentMembersDTO> members) {
		this.members = members;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public List<EnrollmentStatus> getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatusList(List<EnrollmentStatus> enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

}
