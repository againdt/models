package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EnrollmentGroup{
  @JsonProperty("SelectedInsurancePlan")
  
  private SelectedInsurancePlan SelectedInsurancePlan;
  @JsonProperty("InsurancePolicyEffectiveDate")
  
  private Object InsurancePolicyEffectiveDate;
  @JsonProperty("InsuranceApplicant")
  
  private Object InsuranceApplicant;
  public void setSelectedInsurancePlan(SelectedInsurancePlan SelectedInsurancePlan){
   this.SelectedInsurancePlan=SelectedInsurancePlan;
  }
  public SelectedInsurancePlan getSelectedInsurancePlan(){
   return SelectedInsurancePlan;
  }
  public void setInsurancePolicyEffectiveDate(Object InsurancePolicyEffectiveDate){
   this.InsurancePolicyEffectiveDate=InsurancePolicyEffectiveDate;
  }
  public Object getInsurancePolicyEffectiveDate(){
   return InsurancePolicyEffectiveDate;
  }
  public void setInsuranceApplicant(Object InsuranceApplicant){
   this.InsuranceApplicant=InsuranceApplicant;
  }
  public Object getInsuranceApplicant(){
   return InsuranceApplicant;
  }
}