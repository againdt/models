package com.getinsured.hix.dto.directenrollment;

import java.util.Map;

import com.getinsured.hix.dto.directenrollment.DirectEnrollmentStatus.SubmitStatus;
import com.getinsured.hix.dto.directenrollment.payment.PaymentConstants.PaymentError;

public class SubmitUIResponseDTO {
	private DirectEnrollmentStatus.SubmitStatus status;
	private Map<PaymentError, String> paymentError;
	
	public static SubmitUIResponseDTO getObject(SubmitStatus status){
		SubmitUIResponseDTO dto = new SubmitUIResponseDTO();
		dto.setStatus(status);
		return dto;
	}
	
	public DirectEnrollmentStatus.SubmitStatus getStatus() {
		return status;
	}
	public void setStatus(DirectEnrollmentStatus.SubmitStatus status) {
		this.status = status;
	}
	public Map<PaymentError, String> getPaymentError() {
		return paymentError;
	}
	public void setPaymentError(Map<PaymentError, String> paymentError) {
		this.paymentError = paymentError;
	}
	
//	public static void main(String args[]){
//		SubmitUIResponseDTO response = new SubmitUIResponseDTO();
//		response.setStatus(SubmitStatus.SUCCESS);
//		Map<PaymentError, String> paymentError = new HashMap<PaymentError, String>();
//		paymentError.put(PaymentError.BANK_ERROR, "Unknown error validating bank data");
//		response.setPaymentError(paymentError);
//		Gson gson = new Gson();
//		System.out.println(gson.toJson(response));
//	}
	

}
