package com.getinsured.hix.dto.directenrollment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * Holds information about information collected
 * about family members during pre-eligibility calculation
 * @author sachin
 *
 */
public class DirectEnrollmentMemberDetailsDTO extends Member{
	public static final SimpleDateFormat MMDDYYYY = new SimpleDateFormat(
			"MM/dd/yyyy", Locale.ENGLISH);
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DirectEnrollmentMemberDetailsDTO.class);
	/**
	 * calculated to make it easy on front end
	 */
	private int age;
	/**
	 * if age<18
	 */
	private Boolean isMinor=false;
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Boolean getIsMinor() {
		return isMinor;
	}
	public void setIsMinor(Boolean isMinor) {
		this.isMinor = isMinor;
	}
	
	public DirectEnrollmentMemberDetailsDTO(){
		
	}
	
	public DirectEnrollmentMemberDetailsDTO(Member member){
		setDateOfBirth(member.getDateOfBirth());
		setGender(member.getGender());
		setIsNativeAmerican(member.getIsNativeAmerican());
		setIsTobaccoUser(member.getIsTobaccoUser());
		setMemberEligibility(member.getMemberEligibility());
		setMemberNumber(member.getMemberNumber());
		setRelationshipToPrimary(member.getRelationshipToPrimary());
	}
	
	public void calculateAge(String dob, Date effectiveStartDate){
		if(dob==null || StringUtils.isEmpty(dob)){
			return;
		}
		
		Date date = null;
		try {
			date = MMDDYYYY.parse(dob);
		} catch (ParseException e) {
			LOGGER.error("error parsing date:" + dob,e);
			return;
		}
		LocalDate birthDate = new LocalDate(date);
		Years age = Years.yearsBetween(birthDate, new LocalDate(effectiveStartDate));
		this.age = age.getYears();
	}
	
	/**
	 * HIX-61530
	 * Feb 18 2015
	 * minor is always less than 18
	 */
	public void computeMinor(int minorAge){
		if(this.age<minorAge){	// HIX-56090 Per Jenny, members under 19 are considered as children
			this.isMinor=true;
		}
	}

	public static void main(String args[]) throws ParseException{
		List<DirectEnrollmentMemberDetailsDTO> members = new ArrayList<DirectEnrollmentMemberDetailsDTO>();
		DirectEnrollmentMemberDetailsDTO dto = null;
		boolean spousePresent = Math.random()>0.5?true:false;
		boolean primaryPresent = Math.random()>0.5?true:false;
		int primaryLocation = (int)Math.floor(Math.random()*5);
		
		
		for(int i=0;i<5;i++){
			int base = 1970 + (int)Math.floor(Math.random()*45);
			dto = new DirectEnrollmentMemberDetailsDTO();
			dto.setDateOfBirth("01/01/" + base);
			dto.calculateAge("01/01/" + base, new TSDate());
			dto.setRelationshipToPrimary(MemberRelationships.CHILD);
			if(primaryPresent && i==primaryLocation){
				dto.setRelationshipToPrimary(MemberRelationships.SELF);
				primaryPresent = false;
			}
			
			if(spousePresent && i==(primaryLocation+1)){
				dto.setRelationshipToPrimary(MemberRelationships.SPOUSE);
				spousePresent = false;
			}
			
			members.add(dto);
			System.out.println(dto);
		}
		DirectEnrollementMemberComparator comp = new DirectEnrollementMemberComparator();
		Collections.sort(members, comp);
		
		System.out.println("\n\n\n");
		for(DirectEnrollmentMemberDetailsDTO member : members){
			System.out.println(member);
		}
		
//		int youngest = -1;
//		Date youngestAge = MMDDYYYY.parse("09/01/1870");
//		Date memberAge = null;
//		DirectEnrollmentMemberDetailsDTO member;
//		for(int i=0;i<members.size();i++){
//			member = members.get(i);
//			if(MemberRelationships.CHILD.equals(member.getRelationshipToPrimary())){
//				try{
//					memberAge = MMDDYYYY.parse(member.getDateOfBirth());
//				}catch (ParseException e) {
//					continue;
//				}
//				
//				if(memberAge.compareTo(youngestAge)>0){
//					youngest = i;
//					youngestAge = (Date)memberAge.clone();
//				}
//			}
//		}
		
		//System.out.println("youngest " + youngest);
		
		Date first = MMDDYYYY.parse("09/01/1870");
		Date second = MMDDYYYY.parse("09/01/1980");
		System.out.println(second.compareTo(first));
		
		System.out.println(dto.age + " minor:" + dto.isMinor);
	}
	
	
	
}
