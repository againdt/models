/* ENROLLMENT TABLE COMMENTS*/
COMMENT ON TABLE ENROLLMENT IS 'This entity contains the enrollment information including the order items, employer, lookupvalues information'; 
COMMENT ON COLUMN ENROLLMENT.RESPONSIBLE_PERSON_ID IS 'Foreign Key of Enrollee table ID';
COMMENT ON COLUMN ENROLLMENT.HOUSEHOLD_CONTACT_ID IS 'Foreign Key of Enrollee table ID';
COMMENT ON COLUMN ENROLLMENT.ESIGNATURE_ID IS 'Foreign Key of Enrollment esignature table ID';
/* ENROLLEE TABLE COMMENTS*/
COMMENT ON TABLE ENROLLEE IS 'This entity contains the enrollee information and the lookupvalues, enrollment events information'; 
COMMENT ON COLUMN ENROLLEE.RELATIONSHIP_HCP_LKP IS 'Foreign Key of LookupValue table ID';
/* ENROLLMENT_EVENT TABLE COMMENTS*/
COMMENT ON TABLE ENROLLMENT_EVENT IS 'This entity contains the enrollment event like event type, event reason lookup information'; 
COMMENT ON COLUMN ENROLLMENT_EVENT.CREATED_BY IS 'this is to store the enrollment event created by user';
COMMENT ON COLUMN ENROLLMENT_EVENT.UPDATED_BY IS 'this is to store the enrollment event  updated by the user';
/* ENROLLEE_RELATIONSHIP TABLE COMMENTS*/
COMMENT ON TABLE ENROLLEE_RELATIONSHIP IS 'This entity contains the enrollee and relationship lookup information'; 
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.ID IS 'primary key of the table';
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.SOURCE_ENROLEE_ID IS 'Foreign key of Enrollee table ID';
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.TARGET_ENROLEE_ID IS 'Foreign key of Enrollee table ID';
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.RELATIONSHIP_LKP IS 'this is to store the lookupValue of relationship';
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.CREATED_ON IS 'the enrollee relationship is created on particular date';
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.UPDATED_ON IS 'the enrollee relationship is updated on particular date';
COMMENT ON COLUMN ENROLLEE_RELATIONSHIP.RELATIONSHIP_HCP_LKP IS 'Foreign Key of LookupValue table ID';
/* ENROLLEE_RACE TABLE COMMENTS*/
COMMENT ON TABLE ENROLLEE_RACE IS 'This entity contains the enrollee and race information'; 
COMMENT ON COLUMN ENROLLEE_RACE.ID IS 'primary key of the table';
COMMENT ON COLUMN ENROLLEE_RACE.ENROLEE_ID IS 'Foreign key of Enrollee table ID';
COMMENT ON COLUMN ENROLLEE_RACE.RACE_ETHNICITY_LKP IS 'Foreign key of LookupValue table ID';
COMMENT ON COLUMN ENROLLEE_RACE.CREATED_ON IS 'the enrollee race created on particular date';
COMMENT ON COLUMN ENROLLEE_RACE.UPDATED_ON IS 'the enrollee race uodated on particualr date';
COMMENT ON COLUMN ENROLLEE_RACE.RACE_DESCRIPTION IS 'this is to store the description of the enrolle race';
/* LOOKUP_TYPE TABLE COMMENTS*/
COMMENT ON TABLE LOOKUP_TYPE IS 'This entity contains the look up types like language, relationship'; 
COMMENT ON COLUMN LOOKUP_TYPE.LOOKUP_TYPE_ID IS 'primary key of the lookup type table';
COMMENT ON COLUMN LOOKUP_TYPE.NAME IS 'name of the lookup type';
COMMENT ON COLUMN LOOKUP_TYPE.DATA_TYPE IS 'data type of the lookup type';
COMMENT ON COLUMN LOOKUP_TYPE.DESCRIPTION IS 'description of the lookup type';
/* LOOKUP_VALUE TABLE COMMENTS*/
COMMENT ON TABLE LOOKUP_VALUE IS 'This entity contains the look up values like language, relationship'; 
COMMENT ON COLUMN LOOKUP_VALUE.LOOKUP_VALUE_ID IS 'primary key of the lookup value table';
COMMENT ON COLUMN LOOKUP_VALUE.LOOKUP_TYPE_ID IS 'Foreign key of the lookup type';
COMMENT ON COLUMN LOOKUP_VALUE.LOOKUP_VALUE_CODE IS 'code of the lookup value';
COMMENT ON COLUMN LOOKUP_VALUE.LOOKUP_VALUE_LABEL IS 'label of the lookup value';
COMMENT ON COLUMN LOOKUP_VALUE.PARENT_LOOKUP_VALUE_ID IS 'parent lookup value id';
COMMENT ON COLUMN LOOKUP_VALUE.DESCRIPTION IS 'description of the lookup value';
/* ENROLLMENT_ESIGNATURE TABLE COMMENTS*/
COMMENT ON TABLE ENROLLMENT_ESIGNATURE IS 'This entity contains the joining information tables of enrollment and esignature';
COMMENT ON COLUMN ENROLLMENT_ESIGNATURE.ID IS 'primary key of the table';
COMMENT ON COLUMN ENROLLMENT_ESIGNATURE.ENROLLMENT_ID IS 'Foreign Key of of enrollment table id';
COMMENT ON COLUMN ENROLLMENT_ESIGNATURE.ESIGNATURE_ID IS 'Foreign Key of of esignature table id';
COMMIT;