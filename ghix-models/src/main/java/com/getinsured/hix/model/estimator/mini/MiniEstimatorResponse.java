/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Response Object for the Mini Estimator API
 * @author Nikhil Talreja
 * @since 10 July, 2013
 *
 */
public class MiniEstimatorResponse{
	
	private static final String NOT_APPLICABLE = "N/A";
	
	private static final String NO = "N";
	
	private static final String YES = "Y";

	//Reflected back from request
	private long affiliateId;
	
	//Reflected back from request
	private long flowId;
	//Reflected back from request. Holds household Id used in the affiliate's system
	private String affiliateHouseholdId;
	
	//Overall status of the API call
	private String status;
	
	//Possible values are Y,N
	private String allPlansAvailable = NO;
	
	//Possible values are Y,N
	private String subsidyEligible = NO;
	
	//Mandate Penalty amount. N/A by default
	private String mandatePenaltyAmount = NOT_APPLICABLE;
	
	private String expectedMonthlyPremium = NOT_APPLICABLE;
	
	//APTC eligibility. N/A by default. Other possible values are Y,N
	private String aptcEligible = NO;
	
	//APTC amount. N/A by default.
	private String aptcAmount = NOT_APPLICABLE;
	
	//CSR eligibility. N/A by default. Other possible values are Y,N
	private String csrEligible = NO;
	
	private String csrLevelSilver= NOT_APPLICABLE;
	
	private List<Member> members;
	
	//Possible values are Y,N
	private String medicaidHousehold = NO;
	
	//Flag to indicate if a state has elected not to expand Medicaid to 138% of FPL
	private String medicaidExpansion = NO;
	
	//Possible values are Y,N
	private String chipHousehold = NO;
	
	//Flag to indicate if all members of a household are eligible for medicare. Possible values are Y,N
	private String medicareHousehold = NO;
	//Flag to indicate if all members of a household are NativeAmericans . Possible values are Y,N
		private String nativeAmericanHousehold = NO;
	
	private int noOfBronzePlansAvailable = 0;
	
	private int noOfSilverPlansAvailable = 0;
	
	private String planInfoAvailable = YES;
	
	//Second Lowest Silver Plans details - START
	private String slspName = NOT_APPLICABLE;

	private String slspDeductible = NOT_APPLICABLE;

	private String slspMaxOutOfPocket = NOT_APPLICABLE;

	private String slspDoctorVisits = NOT_APPLICABLE;

	private String slspGenericDrugs = NOT_APPLICABLE;

	private String slspMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

	private String slspMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Second Lowest Silver Plans details - END
	
	//Lowest Silver Plans details - START
	private String lspName = NOT_APPLICABLE;

	private String lspDeductible = NOT_APPLICABLE;

	private String lspMaxOutOfPocket = NOT_APPLICABLE;

	private String lspDoctorVisits = NOT_APPLICABLE;

	private String lspGenericDrugs = NOT_APPLICABLE;

	private String lspMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;
	
	private String lspMonthlyPremiumBeforeAPTCForTenant = NOT_APPLICABLE;
	
	private String lbpMonthlyPremiumBeforeAPTCForTenant = NOT_APPLICABLE;

	private String lspMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Lowest Silver Plans details - END
	
	//Lowest Bronze Plans details - START
	private String lbpName = NOT_APPLICABLE;

	private String lbpDeductible = NOT_APPLICABLE;

	private String lbpMaxOutOfPocket = NOT_APPLICABLE;

	private String lbpDoctorVisits = NOT_APPLICABLE;

	private String lbpGenericDrugs = NOT_APPLICABLE;

	private String lbpMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

	private String lbpMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Lowest Bronze Plans details - END
	
	//Get Insured Lowest Silver Plans details - START
	private String giAvailableLspName = NOT_APPLICABLE;

	private String giAvailableLspDeductible = NOT_APPLICABLE;

	private String giAvailableLspMaxOutOfPocket = NOT_APPLICABLE;

	private String giAvailableLspDoctorVisits = NOT_APPLICABLE;

	private String giAvailableLspGenericDrugs = NOT_APPLICABLE;

	private String giAvailableLspMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

	private String giAvailableLspMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Get Insured Lowest Silver Plans details - END
	
	//Get Insured Lowest Bronze Plans details - START
	private String giAvailableLbpName = NOT_APPLICABLE;

	private String giAvailableLbpDeductible = NOT_APPLICABLE;

	private String giAvailableLbpMaxOutOfPocket = NOT_APPLICABLE;

	private String giAvailableLbpDoctorVisits = NOT_APPLICABLE;

	private String giAvailableLbpGenericDrugs = NOT_APPLICABLE;

	private String giAvailableLbpMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

	private String giAvailableLbpMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Get Insured Lowest Bronze Plans details - END
	
	//Get Insured Lowest Gold Plans details - START
	private String giAvailableLgpName = NOT_APPLICABLE;

	private String giAvailableLgpDeductible = NOT_APPLICABLE;

	private String giAvailableLgpMaxOutOfPocket = NOT_APPLICABLE;

	private String giAvailableLgpDoctorVisits = NOT_APPLICABLE;

	private String giAvailableLgpGenericDrugs = NOT_APPLICABLE;

	private String giAvailableLgpMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

	private String giAvailableLgpMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Get Insured Lowest Gold Plans details - END
		
	//Get Insured Lowest Platinum Plans details - START
	private String giAvailableLppName = NOT_APPLICABLE;
	
	private String giAvailableLppDeductible = NOT_APPLICABLE;
	
	private String giAvailableLppMaxOutOfPocket = NOT_APPLICABLE;
	
	private String giAvailableLppDoctorVisits = NOT_APPLICABLE;
	
	private String giAvailableLppGenericDrugs = NOT_APPLICABLE;
	
	private String giAvailableLppMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;
	
	private String giAvailableLppMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
	//Get Insured Lowest Platinum Plans details - END

	
	//Get Insured Higest Silver Plans details - START
		private String giAvailableHspName = NOT_APPLICABLE;

		private String giAvailableHspDeductible = NOT_APPLICABLE;

		private String giAvailableHspMaxOutOfPocket = NOT_APPLICABLE;

		private String giAvailableHspDoctorVisits = NOT_APPLICABLE;

		private String giAvailableHspGenericDrugs = NOT_APPLICABLE;

		private String giAvailableHspMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

		private String giAvailableHspMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
		//Get Insured Higest Silver Plans details - END
		
		//Get Insured Higest Bronze Plans details - START
		private String giAvailableHbpName = NOT_APPLICABLE;

		private String giAvailableHbpDeductible = NOT_APPLICABLE;

		private String giAvailableHbpMaxOutOfPocket = NOT_APPLICABLE;

		private String giAvailableHbpDoctorVisits = NOT_APPLICABLE;

		private String giAvailableHbpGenericDrugs = NOT_APPLICABLE;

		private String giAvailableHbpMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

		private String giAvailableHbpMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
		//Get Insured Higest Bronze Plans details - END
		
		//Get Insured Higest Gold Plans details - START
		private String giAvailableHgpName = NOT_APPLICABLE;

		private String giAvailableHgpDeductible = NOT_APPLICABLE;

		private String giAvailableHgpMaxOutOfPocket = NOT_APPLICABLE;

		private String giAvailableHgpDoctorVisits = NOT_APPLICABLE;

		private String giAvailableHgpGenericDrugs = NOT_APPLICABLE;

		private String giAvailableHgpMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;

		private String giAvailableHgpMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
		//Get Insured Higest Gold Plans details - END
			
		//Get Insured Higest Platinum Plans details - START
		private String giAvailableHppName = NOT_APPLICABLE;
		
		private String giAvailableHppDeductible = NOT_APPLICABLE;
		
		private String giAvailableHppMaxOutOfPocket = NOT_APPLICABLE;
		
		private String giAvailableHppDoctorVisits = NOT_APPLICABLE;
		
		private String giAvailableHppGenericDrugs = NOT_APPLICABLE;
		
		private String giAvailableHppMonthlyPremiumBeforeAPTC = NOT_APPLICABLE;
		
		private String giAvailableHppMonthlyPremiumAfterAPTC = NOT_APPLICABLE;
		//Get Insured Higest Platinum Plans details - END
	
	//State Based Exchange Details - START
	private String hixName = NOT_APPLICABLE;
	
	private String hixURL = NOT_APPLICABLE;
	
	private String hixPhoneNumber = NOT_APPLICABLE;
	//State Based Exchange Details - END
	
	//Website for Medicare. N/A by default. Populated only if household has a member eligible for Medicare
	private String medicareURL = NOT_APPLICABLE;
	
	private String medicarePhoneNumber = NOT_APPLICABLE;
	
	private long startTime;
	
	private long execDuration;
	
	private Map<Integer,String> errors;
	
	private long leadId;
	
	private EligLead eligLead;
	
	
	private String giOnExchangePlansAvailable=NOT_APPLICABLE;
	private String giOffExchangePlansAvailable=NOT_APPLICABLE;
	
	
	private String giOnExchangeCheapestPremium=NOT_APPLICABLE;
	private String giOffExchangeCheapestPremium=NOT_APPLICABLE;
	
	
	public String getGiOnExchangeCheapestPremium() {
		return giOnExchangeCheapestPremium;
	}

	public void setGiOnExchangeCheapestPremium(String giOnExchangeCheapestPremium) {
		this.giOnExchangeCheapestPremium = giOnExchangeCheapestPremium;
	}

	public String getGiOffExchangeCheapestPremium() {
		return giOffExchangeCheapestPremium;
	}

	public void setGiOffExchangeCheapestPremium(String giOffExchangeCheapestPremium) {
		this.giOffExchangeCheapestPremium = giOffExchangeCheapestPremium;
	}

	public long getLeadId() {
		return leadId;
	}

	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}

	public void startResponse() {
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		execDuration = endTime - startTime;
	}
	
	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	

	public String getAffiliateHouseholdId() {
		return affiliateHouseholdId;
	}

	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.affiliateHouseholdId = affiliateHouseholdId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAllPlansAvailable() {
		return allPlansAvailable;
	}

	public void setAllPlansAvailable(String allPlansAvailable) {
		this.allPlansAvailable = allPlansAvailable;
	}

	public String getSubsidyEligible() {
		return subsidyEligible;
	}

	public void setSubsidyEligible(String subsidyEligible) {
		this.subsidyEligible = subsidyEligible;
	}

	public String getMandatePenaltyAmount() {
		return mandatePenaltyAmount;
	}

	public void setMandatePenaltyAmount(String mandatePenaltyAmount) {
		this.mandatePenaltyAmount = mandatePenaltyAmount;
	}

	public String getExpectedMonthlyPremium() {
		return expectedMonthlyPremium;
	}

	public void setExpectedMonthlyPremium(String expectedMonthlyPremium) {
		this.expectedMonthlyPremium = expectedMonthlyPremium;
	}

	public String getAptcEligible() {
		return aptcEligible;
	}

	public void setAptcEligible(String aptcEligible) {
		this.aptcEligible = aptcEligible;
	}

	public String getAptcAmount() {
		return aptcAmount;
	}

	public void setAptcAmount(String aptcAmount) {
		this.aptcAmount = aptcAmount;
	}

	public String getCsrEligible() {
		return csrEligible;
	}

	public void setCsrEligible(String csrEligible) {
		this.csrEligible = csrEligible;
	}

	public String getCsrLevelSilver() {
		return csrLevelSilver;
	}

	public void setCsrLevelSilver(String csrLevelSilver) {
		this.csrLevelSilver = csrLevelSilver;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public String getMedicaidHousehold() {
		return medicaidHousehold;
	}

	public void setMedicaidHousehold(String medicaidHousehold) {
		this.medicaidHousehold = medicaidHousehold;
	}

	public String getChipHousehold() {
		return chipHousehold;
	}

	public void setChipHousehold(String chipHousehold) {
		this.chipHousehold = chipHousehold;
	}

	public String getMedicareHousehold() {
		return medicareHousehold;
	}

	public void setMedicareHousehold(String medicareHousehold) {
		this.medicareHousehold = medicareHousehold;
	}

	public int getNoOfBronzePlansAvailable() {
		return noOfBronzePlansAvailable;
	}

	public void setNoOfBronzePlansAvailable(int noOfBronzePlansAvailable) {
		this.noOfBronzePlansAvailable = noOfBronzePlansAvailable;
	}

	public int getNoOfSilverPlansAvailable() {
		return noOfSilverPlansAvailable;
	}

	public void setNoOfSilverPlansAvailable(int noOfSilverPlansAvailable) {
		this.noOfSilverPlansAvailable = noOfSilverPlansAvailable;
	}

	public String getPlanInfoAvailable() {
		return planInfoAvailable;
	}

	public void setPlanInfoAvailable(String planInfoAvailable) {
		this.planInfoAvailable = planInfoAvailable;
	}

	public String getSlspName() {
		return slspName;
	}

	public void setSlspName(String slspName) {
		this.slspName = slspName;
	}

	public String getSlspDeductible() {
		return slspDeductible;
	}

	public void setSlspDeductible(String slspDeductible) {
		this.slspDeductible = slspDeductible;
	}

	public String getSlspMaxOutOfPocket() {
		return slspMaxOutOfPocket;
	}

	public void setSlspMaxOutOfPocket(String slspMaxOutOfPocket) {
		this.slspMaxOutOfPocket = slspMaxOutOfPocket;
	}

	public String getSlspDoctorVisits() {
		return slspDoctorVisits;
	}

	public void setSlspDoctorVisits(String slspDoctorVisits) {
		this.slspDoctorVisits = slspDoctorVisits;
	}

	public String getSlspGenericDrugs() {
		return slspGenericDrugs;
	}

	public void setSlspGenericDrugs(String slspGenericDrugs) {
		this.slspGenericDrugs = slspGenericDrugs;
	}

	public String getSlspMonthlyPremiumBeforeAPTC() {
		return slspMonthlyPremiumBeforeAPTC;
	}

	public void setSlspMonthlyPremiumBeforeAPTC(String slspMonthlyPremiumBeforeAPTC) {
		this.slspMonthlyPremiumBeforeAPTC = slspMonthlyPremiumBeforeAPTC;
	}

	public String getSlspMonthlyPremiumAfterAPTC() {
		return slspMonthlyPremiumAfterAPTC;
	}

	public void setSlspMonthlyPremiumAfterAPTC(String slspMonthlyPremiumAfterAPTC) {
		this.slspMonthlyPremiumAfterAPTC = slspMonthlyPremiumAfterAPTC;
	}

	public String getLspName() {
		return lspName;
	}

	public void setLspName(String lspName) {
		this.lspName = lspName;
	}

	public String getLspDeductible() {
		return lspDeductible;
	}

	public void setLspDeductible(String lspDeductible) {
		this.lspDeductible = lspDeductible;
	}

	public String getLspMaxOutOfPocket() {
		return lspMaxOutOfPocket;
	}

	public void setLspMaxOutOfPocket(String lspMaxOutOfPocket) {
		this.lspMaxOutOfPocket = lspMaxOutOfPocket;
	}

	public String getLspDoctorVisits() {
		return lspDoctorVisits;
	}

	public void setLspDoctorVisits(String lspDoctorVisits) {
		this.lspDoctorVisits = lspDoctorVisits;
	}

	public String getLspGenericDrugs() {
		return lspGenericDrugs;
	}

	public void setLspGenericDrugs(String lspGenericDrugs) {
		this.lspGenericDrugs = lspGenericDrugs;
	}

	public String getLspMonthlyPremiumBeforeAPTC() {
		return lspMonthlyPremiumBeforeAPTC;
	}

	public void setLspMonthlyPremiumBeforeAPTC(String lspMonthlyPremiumBeforeAPTC) {
		this.lspMonthlyPremiumBeforeAPTC = lspMonthlyPremiumBeforeAPTC;
	}

	public String getLspMonthlyPremiumAfterAPTC() {
		return lspMonthlyPremiumAfterAPTC;
	}

	public void setLspMonthlyPremiumAfterAPTC(String lspMonthlyPremiumAfterAPTC) {
		this.lspMonthlyPremiumAfterAPTC = lspMonthlyPremiumAfterAPTC;
	}

	public String getLbpName() {
		return lbpName;
	}

	public void setLbpName(String lbpName) {
		this.lbpName = lbpName;
	}

	public String getLbpDeductible() {
		return lbpDeductible;
	}

	public void setLbpDeductible(String lbpDeductible) {
		this.lbpDeductible = lbpDeductible;
	}

	public String getLbpMaxOutOfPocket() {
		return lbpMaxOutOfPocket;
	}

	public void setLbpMaxOutOfPocket(String lbpMaxOutOfPocket) {
		this.lbpMaxOutOfPocket = lbpMaxOutOfPocket;
	}

	public String getLbpDoctorVisits() {
		return lbpDoctorVisits;
	}

	public void setLbpDoctorVisits(String lbpDoctorVisits) {
		this.lbpDoctorVisits = lbpDoctorVisits;
	}

	public String getLbpGenericDrugs() {
		return lbpGenericDrugs;
	}

	public void setLbpGenericDrugs(String lbpGenericDrugs) {
		this.lbpGenericDrugs = lbpGenericDrugs;
	}

	public String getLbpMonthlyPremiumBeforeAPTC() {
		return lbpMonthlyPremiumBeforeAPTC;
	}

	public void setLbpMonthlyPremiumBeforeAPTC(String lbpMonthlyPremiumBeforeAPTC) {
		this.lbpMonthlyPremiumBeforeAPTC = lbpMonthlyPremiumBeforeAPTC;
	}

	public String getLbpMonthlyPremiumAfterAPTC() {
		return lbpMonthlyPremiumAfterAPTC;
	}

	public void setLbpMonthlyPremiumAfterAPTC(String lbpMonthlyPremiumAfterAPTC) {
		this.lbpMonthlyPremiumAfterAPTC = lbpMonthlyPremiumAfterAPTC;
	}

	public String getGiAvailableLspName() {
		return giAvailableLspName;
	}

	public void setGiAvailableLspName(String giAvailableLspName) {
		this.giAvailableLspName = giAvailableLspName;
	}

	public String getGiAvailableLspDeductible() {
		return giAvailableLspDeductible;
	}

	public void setGiAvailableLspDeductible(String giAvailableLspDeductible) {
		this.giAvailableLspDeductible = giAvailableLspDeductible;
	}

	public String getGiAvailableLspMaxOutOfPocket() {
		return giAvailableLspMaxOutOfPocket;
	}

	public void setGiAvailableLspMaxOutOfPocket(String giAvailableLspMaxOutOfPocket) {
		this.giAvailableLspMaxOutOfPocket = giAvailableLspMaxOutOfPocket;
	}

	public String getGiAvailableLspDoctorVisits() {
		return giAvailableLspDoctorVisits;
	}

	public void setGiAvailableLspDoctorVisits(String giAvailableLspDoctorVisits) {
		this.giAvailableLspDoctorVisits = giAvailableLspDoctorVisits;
	}

	public String getGiAvailableLspGenericDrugs() {
		return giAvailableLspGenericDrugs;
	}

	public void setGiAvailableLspGenericDrugs(String giAvailableLspGenericDrugs) {
		this.giAvailableLspGenericDrugs = giAvailableLspGenericDrugs;
	}

	public String getGiAvailableLspMonthlyPremiumBeforeAPTC() {
		return giAvailableLspMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableLspMonthlyPremiumBeforeAPTC(
			String giAvailableLspMonthlyPremiumBeforeAPTC) {
		this.giAvailableLspMonthlyPremiumBeforeAPTC = giAvailableLspMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableLspMonthlyPremiumAfterAPTC() {
		return giAvailableLspMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableLspMonthlyPremiumAfterAPTC(
			String giAvailableLspMonthlyPremiumAfterAPTC) {
		this.giAvailableLspMonthlyPremiumAfterAPTC = giAvailableLspMonthlyPremiumAfterAPTC;
	}

	public String getGiAvailableLbpName() {
		return giAvailableLbpName;
	}

	public void setGiAvailableLbpName(String giAvailableLbpName) {
		this.giAvailableLbpName = giAvailableLbpName;
	}

	public String getGiAvailableLbpDeductible() {
		return giAvailableLbpDeductible;
	}

	public void setGiAvailableLbpDeductible(String giAvailableLbpDeductible) {
		this.giAvailableLbpDeductible = giAvailableLbpDeductible;
	}

	public String getGiAvailableLbpMaxOutOfPocket() {
		return giAvailableLbpMaxOutOfPocket;
	}

	public void setGiAvailableLbpMaxOutOfPocket(String giAvailableLbpMaxOutOfPocket) {
		this.giAvailableLbpMaxOutOfPocket = giAvailableLbpMaxOutOfPocket;
	}

	public String getGiAvailableLbpDoctorVisits() {
		return giAvailableLbpDoctorVisits;
	}

	public void setGiAvailableLbpDoctorVisits(String giAvailableLbpDoctorVisits) {
		this.giAvailableLbpDoctorVisits = giAvailableLbpDoctorVisits;
	}

	public String getGiAvailableLbpGenericDrugs() {
		return giAvailableLbpGenericDrugs;
	}

	public void setGiAvailableLbpGenericDrugs(String giAvailableLbpGenericDrugs) {
		this.giAvailableLbpGenericDrugs = giAvailableLbpGenericDrugs;
	}

	public String getGiAvailableLbpMonthlyPremiumBeforeAPTC() {
		return giAvailableLbpMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableLbpMonthlyPremiumBeforeAPTC(
			String giAvailableLbpMonthlyPremiumBeforeAPTC) {
		this.giAvailableLbpMonthlyPremiumBeforeAPTC = giAvailableLbpMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableLbpMonthlyPremiumAfterAPTC() {
		return giAvailableLbpMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableLbpMonthlyPremiumAfterAPTC(
			String giAvailableLbpMonthlyPremiumAfterAPTC) {
		this.giAvailableLbpMonthlyPremiumAfterAPTC = giAvailableLbpMonthlyPremiumAfterAPTC;
	}

	public String getHixName() {
		return hixName;
	}

	public void setHixName(String hixName) {
		this.hixName = hixName;
	}

	public String getHixURL() {
		return hixURL;
	}

	public void setHixURL(String hixURL) {
		this.hixURL = hixURL;
	}

	public String getHixPhoneNumber() {
		return hixPhoneNumber;
	}

	public void setHixPhoneNumber(String hixPhoneNumber) {
		this.hixPhoneNumber = hixPhoneNumber;
	}

	public String getMedicareURL() {
		return medicareURL;
	}

	public void setMedicareURL(String medicareURL) {
		this.medicareURL = medicareURL;
	}

	public String getMedicarePhoneNumber() {
		return medicarePhoneNumber;
	}

	public void setMedicarePhoneNumber(String medicarePhoneNumber) {
		this.medicarePhoneNumber = medicarePhoneNumber;
	}

	public long getExecDuration() {
		return execDuration;
	}

	public void setExecDuration(long execDuration) {
		this.execDuration = execDuration;
	}

	public Map<Integer, String> getErrors() {
		
		if(errors == null){
			errors = new HashMap<Integer, String>();
		}
		
		return errors;
	}

	public void setErrors(Map<Integer, String> errors) {
		this.errors = errors;
	}

	public String getMedicaidExpansion() {
		return medicaidExpansion;
	}

	public void setMedicaidExpansion(String medicaidExpansion) {
		this.medicaidExpansion = medicaidExpansion;
	}
	public long getFlowId() {
		return flowId;
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}
	public String getNativeAmericanHousehold() {
		return nativeAmericanHousehold;
	}
	public void setNativeAmericanHousehold(String nativeAmericanHousehold) {
		this.nativeAmericanHousehold = nativeAmericanHousehold;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MiniEstimatorResponse [affiliateId=").append(affiliateId);
		builder.append(", flowId=").append(flowId);
		builder.append(", affiliateHouseholdId=").append(affiliateHouseholdId);
		builder.append(", status=").append(status);
		builder.append(", allPlansAvailable=").append(allPlansAvailable);
		builder.append(", subsidyEligible=").append(subsidyEligible);
		builder.append(", mandatePenaltyAmount=").append(mandatePenaltyAmount);
		builder.append(", expectedMonthlyPremium=").append(expectedMonthlyPremium);
		builder.append(", aptcEligible=").append(aptcEligible);
		builder.append(", aptcAmount=").append(aptcAmount);
		builder.append(", nativeAmericanHousehold=").append(nativeAmericanHousehold);
		builder.append(", csrEligible=").append(csrEligible);
		builder.append(", csrLevelSilver=").append(csrLevelSilver);
		builder.append(", members=").append(members);
		builder.append(", medicaidHousehold=").append(medicaidHousehold);
		builder.append(", medicaidExpansion=").append(medicaidExpansion);
		builder.append(", chipHousehold=").append(chipHousehold);
		builder.append(", medicareHousehold=").append(medicareHousehold);
		appendPlanData(builder);
		builder.append(", hixName=").append(hixName);
		builder.append(", hixURL=").append(hixURL);
		builder.append(", hixPhoneNumber=").append(hixPhoneNumber);
		builder.append(", medicareURL=").append(medicareURL);
		builder.append(", medicarePhoneNumber=").append(medicarePhoneNumber);
		builder.append(", startTime=").append(startTime);
		builder.append(", execDuration=").append(execDuration);
		builder.append(", errors=").append(errors);
		builder.append(", leadId=").append(leadId);
		builder.append("]");
		return builder.toString();
	}
	
	public String getGiAvailableLgpName() {
		return giAvailableLgpName;
	}

	public void setGiAvailableLgpName(String giAvailableLgpName) {
		this.giAvailableLgpName = giAvailableLgpName;
	}

	public String getGiAvailableLgpDeductible() {
		return giAvailableLgpDeductible;
	}

	public void setGiAvailableLgpDeductible(String giAvailableLgpDeductible) {
		this.giAvailableLgpDeductible = giAvailableLgpDeductible;
	}

	public String getGiAvailableLgpMaxOutOfPocket() {
		return giAvailableLgpMaxOutOfPocket;
	}

	public void setGiAvailableLgpMaxOutOfPocket(String giAvailableLgpMaxOutOfPocket) {
		this.giAvailableLgpMaxOutOfPocket = giAvailableLgpMaxOutOfPocket;
	}

	public String getGiAvailableLgpDoctorVisits() {
		return giAvailableLgpDoctorVisits;
	}

	public void setGiAvailableLgpDoctorVisits(String giAvailableLgpDoctorVisits) {
		this.giAvailableLgpDoctorVisits = giAvailableLgpDoctorVisits;
	}

	public String getGiAvailableLgpGenericDrugs() {
		return giAvailableLgpGenericDrugs;
	}

	public void setGiAvailableLgpGenericDrugs(String giAvailableLgpGenericDrugs) {
		this.giAvailableLgpGenericDrugs = giAvailableLgpGenericDrugs;
	}

	public String getGiAvailableLgpMonthlyPremiumBeforeAPTC() {
		return giAvailableLgpMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableLgpMonthlyPremiumBeforeAPTC(
			String giAvailableLgpMonthlyPremiumBeforeAPTC) {
		this.giAvailableLgpMonthlyPremiumBeforeAPTC = giAvailableLgpMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableLgpMonthlyPremiumAfterAPTC() {
		return giAvailableLgpMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableLgpMonthlyPremiumAfterAPTC(
			String giAvailableLgpMonthlyPremiumAfterAPTC) {
		this.giAvailableLgpMonthlyPremiumAfterAPTC = giAvailableLgpMonthlyPremiumAfterAPTC;
	}

	public String getGiAvailableLppName() {
		return giAvailableLppName;
	}

	public void setGiAvailableLppName(String giAvailableLppName) {
		this.giAvailableLppName = giAvailableLppName;
	}

	public String getGiAvailableLppDeductible() {
		return giAvailableLppDeductible;
	}

	public void setGiAvailableLppDeductible(String giAvailableLppDeductible) {
		this.giAvailableLppDeductible = giAvailableLppDeductible;
	}

	public String getGiAvailableLppMaxOutOfPocket() {
		return giAvailableLppMaxOutOfPocket;
	}

	public void setGiAvailableLppMaxOutOfPocket(String giAvailableLppMaxOutOfPocket) {
		this.giAvailableLppMaxOutOfPocket = giAvailableLppMaxOutOfPocket;
	}

	public String getGiAvailableLppDoctorVisits() {
		return giAvailableLppDoctorVisits;
	}

	public void setGiAvailableLppDoctorVisits(String giAvailableLppDoctorVisits) {
		this.giAvailableLppDoctorVisits = giAvailableLppDoctorVisits;
	}

	public String getGiAvailableLppGenericDrugs() {
		return giAvailableLppGenericDrugs;
	}

	public void setGiAvailableLppGenericDrugs(String giAvailableLppGenericDrugs) {
		this.giAvailableLppGenericDrugs = giAvailableLppGenericDrugs;
	}

	public String getGiAvailableLppMonthlyPremiumBeforeAPTC() {
		return giAvailableLppMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableLppMonthlyPremiumBeforeAPTC(
			String giAvailableLppMonthlyPremiumBeforeAPTC) {
		this.giAvailableLppMonthlyPremiumBeforeAPTC = giAvailableLppMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableLppMonthlyPremiumAfterAPTC() {
		return giAvailableLppMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableLppMonthlyPremiumAfterAPTC(
			String giAvailableLppMonthlyPremiumAfterAPTC) {
		this.giAvailableLppMonthlyPremiumAfterAPTC = giAvailableLppMonthlyPremiumAfterAPTC;
	}
	
	
	public String getGiAvailableHspName() {
		return giAvailableHspName;
	}

	public void setGiAvailableHspName(String giAvailableHspName) {
		this.giAvailableHspName = giAvailableHspName;
	}

	public String getGiAvailableHspDeductible() {
		return giAvailableHspDeductible;
	}

	public void setGiAvailableHspDeductible(String giAvailableHspDeductible) {
		this.giAvailableHspDeductible = giAvailableHspDeductible;
	}

	public String getGiAvailableHspMaxOutOfPocket() {
		return giAvailableHspMaxOutOfPocket;
	}

	public void setGiAvailableHspMaxOutOfPocket(String giAvailableHspMaxOutOfPocket) {
		this.giAvailableHspMaxOutOfPocket = giAvailableHspMaxOutOfPocket;
	}

	public String getGiAvailableHspDoctorVisits() {
		return giAvailableHspDoctorVisits;
	}

	public void setGiAvailableHspDoctorVisits(String giAvailableHspDoctorVisits) {
		this.giAvailableHspDoctorVisits = giAvailableHspDoctorVisits;
	}

	public String getGiAvailableHspGenericDrugs() {
		return giAvailableHspGenericDrugs;
	}

	public void setGiAvailableHspGenericDrugs(String giAvailableHspGenericDrugs) {
		this.giAvailableHspGenericDrugs = giAvailableHspGenericDrugs;
	}

	public String getGiAvailableHspMonthlyPremiumBeforeAPTC() {
		return giAvailableHspMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableHspMonthlyPremiumBeforeAPTC(
			String giAvailableHspMonthlyPremiumBeforeAPTC) {
		this.giAvailableHspMonthlyPremiumBeforeAPTC = giAvailableHspMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableHspMonthlyPremiumAfterAPTC() {
		return giAvailableHspMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableHspMonthlyPremiumAfterAPTC(
			String giAvailableHspMonthlyPremiumAfterAPTC) {
		this.giAvailableHspMonthlyPremiumAfterAPTC = giAvailableHspMonthlyPremiumAfterAPTC;
	}

	public String getGiAvailableHbpName() {
		return giAvailableHbpName;
	}

	public void setGiAvailableHbpName(String giAvailableHbpName) {
		this.giAvailableHbpName = giAvailableHbpName;
	}

	public String getGiAvailableHbpDeductible() {
		return giAvailableHbpDeductible;
	}

	public void setGiAvailableHbpDeductible(String giAvailableHbpDeductible) {
		this.giAvailableHbpDeductible = giAvailableHbpDeductible;
	}

	public String getGiAvailableHbpMaxOutOfPocket() {
		return giAvailableHbpMaxOutOfPocket;
	}

	public void setGiAvailableHbpMaxOutOfPocket(String giAvailableHbpMaxOutOfPocket) {
		this.giAvailableHbpMaxOutOfPocket = giAvailableHbpMaxOutOfPocket;
	}

	public String getGiAvailableHbpDoctorVisits() {
		return giAvailableHbpDoctorVisits;
	}

	public void setGiAvailableHbpDoctorVisits(String giAvailableHbpDoctorVisits) {
		this.giAvailableHbpDoctorVisits = giAvailableHbpDoctorVisits;
	}

	public String getGiAvailableHbpGenericDrugs() {
		return giAvailableHbpGenericDrugs;
	}

	public void setGiAvailableHbpGenericDrugs(String giAvailableHbpGenericDrugs) {
		this.giAvailableHbpGenericDrugs = giAvailableHbpGenericDrugs;
	}

	public String getGiAvailableHbpMonthlyPremiumBeforeAPTC() {
		return giAvailableHbpMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableHbpMonthlyPremiumBeforeAPTC(
			String giAvailableHbpMonthlyPremiumBeforeAPTC) {
		this.giAvailableHbpMonthlyPremiumBeforeAPTC = giAvailableHbpMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableHbpMonthlyPremiumAfterAPTC() {
		return giAvailableHbpMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableHbpMonthlyPremiumAfterAPTC(
			String giAvailableHbpMonthlyPremiumAfterAPTC) {
		this.giAvailableHbpMonthlyPremiumAfterAPTC = giAvailableHbpMonthlyPremiumAfterAPTC;
	}

	public String getGiAvailableHgpName() {
		return giAvailableHgpName;
	}

	public void setGiAvailableHgpName(String giAvailableHgpName) {
		this.giAvailableHgpName = giAvailableHgpName;
	}

	public String getGiAvailableHgpDeductible() {
		return giAvailableHgpDeductible;
	}

	public void setGiAvailableHgpDeductible(String giAvailableHgpDeductible) {
		this.giAvailableHgpDeductible = giAvailableHgpDeductible;
	}

	public String getGiAvailableHgpMaxOutOfPocket() {
		return giAvailableHgpMaxOutOfPocket;
	}

	public void setGiAvailableHgpMaxOutOfPocket(String giAvailableHgpMaxOutOfPocket) {
		this.giAvailableHgpMaxOutOfPocket = giAvailableHgpMaxOutOfPocket;
	}

	public String getGiAvailableHgpDoctorVisits() {
		return giAvailableHgpDoctorVisits;
	}

	public void setGiAvailableHgpDoctorVisits(String giAvailableHgpDoctorVisits) {
		this.giAvailableHgpDoctorVisits = giAvailableHgpDoctorVisits;
	}

	public String getGiAvailableHgpGenericDrugs() {
		return giAvailableHgpGenericDrugs;
	}

	public void setGiAvailableHgpGenericDrugs(String giAvailableHgpGenericDrugs) {
		this.giAvailableHgpGenericDrugs = giAvailableHgpGenericDrugs;
	}

	public String getGiAvailableHgpMonthlyPremiumBeforeAPTC() {
		return giAvailableHgpMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableHgpMonthlyPremiumBeforeAPTC(
			String giAvailableHgpMonthlyPremiumBeforeAPTC) {
		this.giAvailableHgpMonthlyPremiumBeforeAPTC = giAvailableHgpMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableHgpMonthlyPremiumAfterAPTC() {
		return giAvailableHgpMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableHgpMonthlyPremiumAfterAPTC(
			String giAvailableHgpMonthlyPremiumAfterAPTC) {
		this.giAvailableHgpMonthlyPremiumAfterAPTC = giAvailableHgpMonthlyPremiumAfterAPTC;
	}

	public String getGiAvailableHppName() {
		return giAvailableHppName;
	}

	public void setGiAvailableHppName(String giAvailableHppName) {
		this.giAvailableHppName = giAvailableHppName;
	}

	public String getGiAvailableHppDeductible() {
		return giAvailableHppDeductible;
	}

	public void setGiAvailableHppDeductible(String giAvailableHppDeductible) {
		this.giAvailableHppDeductible = giAvailableHppDeductible;
	}

	public String getGiAvailableHppMaxOutOfPocket() {
		return giAvailableHppMaxOutOfPocket;
	}

	public void setGiAvailableHppMaxOutOfPocket(String giAvailableHppMaxOutOfPocket) {
		this.giAvailableHppMaxOutOfPocket = giAvailableHppMaxOutOfPocket;
	}

	public String getGiAvailableHppDoctorVisits() {
		return giAvailableHppDoctorVisits;
	}

	public void setGiAvailableHppDoctorVisits(String giAvailableHppDoctorVisits) {
		this.giAvailableHppDoctorVisits = giAvailableHppDoctorVisits;
	}

	public String getGiAvailableHppGenericDrugs() {
		return giAvailableHppGenericDrugs;
	}

	public void setGiAvailableHppGenericDrugs(String giAvailableHppGenericDrugs) {
		this.giAvailableHppGenericDrugs = giAvailableHppGenericDrugs;
	}

	public String getGiAvailableHppMonthlyPremiumBeforeAPTC() {
		return giAvailableHppMonthlyPremiumBeforeAPTC;
	}

	public void setGiAvailableHppMonthlyPremiumBeforeAPTC(
			String giAvailableHppMonthlyPremiumBeforeAPTC) {
		this.giAvailableHppMonthlyPremiumBeforeAPTC = giAvailableHppMonthlyPremiumBeforeAPTC;
	}

	public String getGiAvailableHppMonthlyPremiumAfterAPTC() {
		return giAvailableHppMonthlyPremiumAfterAPTC;
	}

	public void setGiAvailableHppMonthlyPremiumAfterAPTC(
			String giAvailableHppMonthlyPremiumAfterAPTC) {
		this.giAvailableHppMonthlyPremiumAfterAPTC = giAvailableHppMonthlyPremiumAfterAPTC;
	}
	
	public EligLead getEligLead() {
		return eligLead;
	}

	public void setEligLead(EligLead eligLead) {
		this.eligLead = eligLead;
	}

	/**
	 * Appends plan data to toString method()
	 * 
	 * @author Nikhil Talreja
	 */
	private void appendPlanData(StringBuilder builder){
		
		builder.append(", noOfBronzePlansAvailable=").append(noOfBronzePlansAvailable);
		builder.append(", noOfSilverPlansAvailable=").append(noOfSilverPlansAvailable);
		builder.append(", slspName=").append(slspName);
		builder.append(", slspDeductible=").append(slspDeductible);
		builder.append(", slspMaxOutOfPocket=").append(slspMaxOutOfPocket);
		builder.append(", slspDoctorVisits=").append(slspDoctorVisits);
		builder.append(", slspGenericDrugs=").append(slspGenericDrugs);
		builder.append(", slspMonthlyPremiumBeforeAPTC=").append(slspMonthlyPremiumBeforeAPTC);
		builder.append(", slspMonthlyPremiumAfterAPTC=").append(slspMonthlyPremiumAfterAPTC);
		builder.append(", lspName=").append(lspName);
		builder.append(", lspDeductible=").append(lspDeductible);
		builder.append(", lspMaxOutOfPocket=").append(lspMaxOutOfPocket);
		builder.append(", lspDoctorVisits=").append(lspDoctorVisits);
		builder.append(", lspGenericDrugs=").append(lspGenericDrugs);
		builder.append(", lspMonthlyPremiumBeforeAPTC=").append(lspMonthlyPremiumBeforeAPTC);
		builder.append(", lspMonthlyPremiumAfterAPTC=").append(lspMonthlyPremiumAfterAPTC);
		builder.append(", lbpName=").append(lbpName);
		builder.append(", lbpDeductible=").append(lbpDeductible);
		builder.append(", lbpMaxOutOfPocket=").append(lbpMaxOutOfPocket);
		builder.append(", lbpDoctorVisits=").append(lbpDoctorVisits);
		builder.append(", lbpGenericDrugs=").append(lbpGenericDrugs);
		builder.append(", lbpMonthlyPremiumBeforeAPTC=").append(lbpMonthlyPremiumBeforeAPTC);
		builder.append(", lbpMonthlyPremiumAfterAPTC=").append(lbpMonthlyPremiumAfterAPTC);
		builder.append(", giAvailableLspName=").append(giAvailableLspName);
		builder.append(", giAvailableLspDeductible=").append(giAvailableLspDeductible);
		builder.append(", giAvailableLspMaxOutOfPocket=").append(giAvailableLspMaxOutOfPocket);
		builder.append(", giAvailableLspDoctorVisits=").append(giAvailableLspDoctorVisits);
		builder.append(", giAvailableLspGenericDrugs=").append(giAvailableLspGenericDrugs);
		builder.append(", giAvailableLspMonthlyPremiumBeforeAPTC=").append(giAvailableLspMonthlyPremiumBeforeAPTC);
		builder.append(", giAvailableLspMonthlyPremiumAfterAPTC=").append(giAvailableLspMonthlyPremiumAfterAPTC);
		builder.append(", giAvailableLbpName=").append(giAvailableLbpName);
		builder.append(", giAvailableLbpDeductible=").append(giAvailableLbpDeductible);
		builder.append(", giAvailableLbpMaxOutOfPocket=").append(giAvailableLbpMaxOutOfPocket);
		builder.append(", giAvailableLbpDoctorVisits=").append(giAvailableLbpDoctorVisits);
		builder.append(", giAvailableLbpGenericDrugs=").append(giAvailableLbpGenericDrugs);
		builder.append(", giAvailableLbpMonthlyPremiumBeforeAPTC=").append(giAvailableLbpMonthlyPremiumBeforeAPTC);
		builder.append(", giAvailableLbpMonthlyPremiumAfterAPTC=").append(giAvailableLbpMonthlyPremiumAfterAPTC);	
	}

	public String getLspMonthlyPremiumBeforeAPTCForTenant() {
		return lspMonthlyPremiumBeforeAPTCForTenant;
	}

	public void setLspMonthlyPremiumBeforeAPTCForTenant(String lspMonthlyPremiumBeforeAPTCForTenant) {
		this.lspMonthlyPremiumBeforeAPTCForTenant = lspMonthlyPremiumBeforeAPTCForTenant;
	}

	public String getLbpMonthlyPremiumBeforeAPTCForTenant() {
		return lbpMonthlyPremiumBeforeAPTCForTenant;
	}

	public void setLbpMonthlyPremiumBeforeAPTCForTenant(String lbpMonthlyPremiumBeforeAPTCForTenant) {
		this.lbpMonthlyPremiumBeforeAPTCForTenant = lbpMonthlyPremiumBeforeAPTCForTenant;
	}
	
	public String getGiOnExchangePlansAvailable() {
		return giOnExchangePlansAvailable;
	}

	public void setGiOnExchangePlansAvailable(String giOnExchangePlansAvailable) {
		this.giOnExchangePlansAvailable = giOnExchangePlansAvailable;
	}

	public String getGiOffExchangePlansAvailable() {
		return giOffExchangePlansAvailable;
	}

	public void setGiOffExchangePlansAvailable(String giOffExchangePlansAvailable) {
		this.giOffExchangePlansAvailable = giOffExchangePlansAvailable;
	}
}
