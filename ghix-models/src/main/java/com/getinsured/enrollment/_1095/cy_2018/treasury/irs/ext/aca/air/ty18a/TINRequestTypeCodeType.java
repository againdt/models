
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TINRequestTypeCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TINRequestTypeCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INDIVIDUAL_TIN"/>
 *     &lt;enumeration value="BUSINESS_TIN"/>
 *     &lt;enumeration value="UNKNOWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TINRequestTypeCodeType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
@XmlEnum
public enum TINRequestTypeCodeType {

    INDIVIDUAL_TIN,
    BUSINESS_TIN,
    UNKNOWN;

    public String value() {
        return name();
    }

    public static TINRequestTypeCodeType fromValue(String v) {
        return valueOf(v);
    }

}
