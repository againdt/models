Insert into NOTICE_TYPES (ID,BCC_ADDRESS,CATEGORY,CC_ADDRESS,CREATED,DESCRIPTION,EMAIL_CLASS,FROM_ADDRESS,MODULE_NAME,REPLY_TO,RET_ADDRESS,SUBJECT,TITLE,TYPE,UNSUBSCRIBE_COUNT,UPDATED) values (notice_types_seq.NEXTVAL,'vimo@xoriant.com','ENDUSER','vimo@xoriant.com',SYSTIMESTAMP,'Account activation at getinsured','AccountActivationEmail','vimo@xoriant.com','ADMIN','vimo@xoriant.com','vimo@xoriant.com','A record has beedn created for you on the ${exchangename} Exchange','Account Activation Email','PLAIN_EMAIL',null,SYSTIMESTAMP);

update NOTICE_TYPES
set TEMPLATE='"<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<title th:remove=\"all\"></title>\r\n</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\" />\r\n</p>\r\n<p>\r\n	Welcome <span>${name}</span> ,</p>\r\n<p>\r\n	A record has been created for you on the ${exchangename} Exchange.\r\n Please click on below link or copy and paste it in your browser.\r\n ${activationUrl} </p>\r\n This is a one-time login, so it can be used only once. It expires after one day.<p>\r\n	<p>\r\n	Thank you!</p>\r\n"'
where EMAIL_CLASS='AccountActivationEmail';


update ROLES
set LANDING_PAGE='/shop/employee/start'
where NAME='EMPLOYEE';