//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 02:42:17 PM IST 
//


package com.serff.template.svcarea.hix.pm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.serff.template.svcarea.niem.core.IdentificationType;
import com.serff.template.svcarea.niem.core.ProperNameTextType;
import com.serff.template.svcarea.niem.core.TextType;
import com.serff.template.svcarea.niem.proxy.xsd.Boolean;
import com.serff.template.svcarea.niem.proxy.xsd.String;
import com.serff.template.svcarea.niem.usps.states.USStateCodeType;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.hix._0_1.hix_pm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GeographicAreaPartialCountyIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "GeographicAreaPartialCountyIndicator");
    private final static QName _IssuerIdentification_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "IssuerIdentification");
    private final static QName _ServiceAreaName_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "ServiceAreaName");
    private final static QName _ServiceAreaIdentification_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "ServiceAreaIdentification");
    private final static QName _GeographicAreaEntireStateIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "GeographicAreaEntireStateIndicator");
    private final static QName _ServiceArea_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "ServiceArea");
    private final static QName _GeographicAreaPartialCountyJustificationText_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "GeographicAreaPartialCountyJustificationText");
    private final static QName _IssuerStateCode_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "IssuerStateCode");
    private final static QName _GeographicArea_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "GeographicArea");
    private final static QName _LocationPostalCode_QNAME = new QName("http://hix.cms.gov/0.1/hix-pm", "LocationPostalCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.hix._0_1.hix_pm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceAreaType }
     * 
     */
    public ServiceAreaType createServiceAreaType() {
        return new ServiceAreaType();
    }

    /**
     * Create an instance of {@link GeographicAreaType }
     * 
     */
    public GeographicAreaType createGeographicAreaType() {
        return new GeographicAreaType();
    }

    /**
     * Create an instance of {@link IssuerType }
     * 
     */
    public IssuerType createIssuerType() {
        return new IssuerType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "GeographicAreaPartialCountyIndicator")
    public JAXBElement<Boolean> createGeographicAreaPartialCountyIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_GeographicAreaPartialCountyIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "IssuerIdentification")
    public JAXBElement<IdentificationType> createIssuerIdentification(IdentificationType value) {
        return new JAXBElement<IdentificationType>(_IssuerIdentification_QNAME, IdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProperNameTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "ServiceAreaName")
    public JAXBElement<ProperNameTextType> createServiceAreaName(ProperNameTextType value) {
        return new JAXBElement<ProperNameTextType>(_ServiceAreaName_QNAME, ProperNameTextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "ServiceAreaIdentification")
    public JAXBElement<IdentificationType> createServiceAreaIdentification(IdentificationType value) {
        return new JAXBElement<IdentificationType>(_ServiceAreaIdentification_QNAME, IdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "GeographicAreaEntireStateIndicator")
    public JAXBElement<Boolean> createGeographicAreaEntireStateIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_GeographicAreaEntireStateIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceAreaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "ServiceArea")
    public JAXBElement<ServiceAreaType> createServiceArea(ServiceAreaType value) {
        return new JAXBElement<ServiceAreaType>(_ServiceArea_QNAME, ServiceAreaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "GeographicAreaPartialCountyJustificationText")
    public JAXBElement<TextType> createGeographicAreaPartialCountyJustificationText(TextType value) {
        return new JAXBElement<TextType>(_GeographicAreaPartialCountyJustificationText_QNAME, TextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link USStateCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "IssuerStateCode")
    public JAXBElement<USStateCodeType> createIssuerStateCode(USStateCodeType value) {
        return new JAXBElement<USStateCodeType>(_IssuerStateCode_QNAME, USStateCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeographicAreaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "GeographicArea")
    public JAXBElement<GeographicAreaType> createGeographicArea(GeographicAreaType value) {
        return new JAXBElement<GeographicAreaType>(_GeographicArea_QNAME, GeographicAreaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-pm", name = "LocationPostalCode")
    public JAXBElement<String> createLocationPostalCode(String value) {
        return new JAXBElement<String>(_LocationPostalCode_QNAME, String.class, null, value);
    }

}
