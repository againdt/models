package com.getinsured.hix.dto.shop;

public class EmployeeCoverageDTO {
	private Integer employeeId;
	private String Availability; 
	private String healthAvailability;
	private String dentalAvailability;
	private String zip;
	private String county;
	private String countyCode;
	private String hiosPlanNumber;
	private String healthHiosPlanNumber;
	private String dentalHiosPlanNumber;
	
	public String getHiosPlanNumber() {
		return hiosPlanNumber;
	}
	public void setHiosPlanNumber(String hiosPlanNumber) {
		this.hiosPlanNumber = hiosPlanNumber;
	}
	
	public String getHealthHiosPlanNumber() {
		return healthHiosPlanNumber;
	}
	public void setHealthHiosPlanNumber(String healthHiosPlanNumber) {
		this.healthHiosPlanNumber = healthHiosPlanNumber;
	}
	public String getDentalHiosPlanNumber() {
		return dentalHiosPlanNumber;
	}
	public void setDentalHiosPlanNumber(String dentalHiosPlanNumber) {
		this.dentalHiosPlanNumber = dentalHiosPlanNumber;
	}
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	
	
	public String getAvailability() {
		return Availability;
	}
	public void setAvailability(String availability) {
		Availability = availability;
	}
	/*new*/
	public String getHealthAvailability() {
		return healthAvailability;
	}
	public void setHealthAvailability(String healthAvailability) {
		this.healthAvailability = healthAvailability;
	}
	public String getDentalAvailability() {
		return dentalAvailability;
	}
	public void setDentalAvailability(String dentalAvailability) {
		this.dentalAvailability = dentalAvailability;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
}