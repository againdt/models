package com.getinsured.hix.dto.entity;

import java.io.Serializable;

/**
 * Used for holding Enrollment Entity Search parameters.
 */
public class EnrollmentEntitySearchParameters implements Serializable {
	private static final long serialVersionUID = 1L;

	private String entityName;
	private String entityNumber;
	private String status;
	private String organizationType;
	private String language;
	private String zipCode;
	private String fromDate;
	private String toDate;
	private String countyServed;
	private String receivedPayments;

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	
	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCountyServed() {
		return countyServed;
	}

	public void setCountyServed(String countyServed) {
		this.countyServed = countyServed;
	}

	public String getReceivedPayments() {
		return receivedPayments;
	}

	public void setReceivedPayments(String receivedPayments) {
		this.receivedPayments = receivedPayments;
	}

	@Override
	public String toString() {
		return "EnrollmentEntitySearchParameters details: EntityName = "+entityName+", Status = "+status+", " +
				"OrganizationType = "+organizationType+", CountyServed = "+countyServed+", ReceivedPayments = "+receivedPayments+", Language = "+language;
	}
}
