/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author hardas_d
 *
 */
@Entity
@Table(name="tkm_documents")
public class TkmDocuments implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5045121359678714490L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TkmDocuments_Seq")
	@SequenceGenerator(name = "TkmDocuments_Seq", sequenceName = "tkm_documents_seq", allocationSize = 1)
	private Integer id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ticket_id")
	private TkmTickets ticket;

	@Column(name = "document_path")	
	private String documentPath;
	
	@Column(name="user_id")
	private Integer userId;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the ticket
	 */
	public TkmTickets getTicket() {
		return ticket;
	}

	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(TkmTickets ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the documentPath
	 */
	public String getDocumentPath() {
		return documentPath;
	}

	/**
	 * @param documentPath the documentPath to set
	 */
	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}
	
	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
