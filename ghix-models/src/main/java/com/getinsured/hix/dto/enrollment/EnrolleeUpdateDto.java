package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;


public class EnrolleeUpdateDto implements Serializable{
	private String exchgIndivIdentifier;
	private String issuerIndivIdentifier;
	private String effectiveStartDate;
	private String effectiveEndDate;
	private String lastPremiumPaidDate;
	
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public String getIssuerIndivIdentifier() {
		return issuerIndivIdentifier;
	}
	public void setIssuerIndivIdentifier(String issuerIndivIdentifier) {
		this.issuerIndivIdentifier = issuerIndivIdentifier;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}
	public void setLastPremiumPaidDate(String lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}
}
