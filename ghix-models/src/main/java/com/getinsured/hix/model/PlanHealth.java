package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.platform.util.exception.GIException;

@Audited
@Entity
@Table(name = "plan_health")
public class PlanHealth implements Serializable {
	private static final long serialVersionUID = 1L;

	public static enum EHBCovered {
		COMPLETE, PARTIAL;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanHealth_Seq")
	@SequenceGenerator(name = "PlanHealth_Seq", sequenceName = "plan_health_seq", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "plan_id")
	private Plan plan;

	@Column(name = "parent_plan_id")
	private int parentPlanId;

	@Column(name = "plan_level", length = 10)
	private String planLevel;

	@Column(name = "cost_sharing", length = 5)
	private String costSharing;

	@Column(name = "status", length = 10)
	private String status;

	/*@Column(name = "eb_benchmark", length = 3)
	private String ebBenchmark;

	@Column(name = "eb_ambulatorty_patient", length = 3)
	private String ebAmbulatortyPatient;

	@Column(name = "eb_emergency", length = 3)
	private String ebEmergency;

	@Column(name = "eb_hospitalization", length = 3)
	private String ebHospitalization;

	@Column(name = "eb_maternity", length = 10)
	private String ebMaternity;

	@Column(name = "eb_Mental_Health", length = 10)
	private String ebMentalHealth;

	@Column(name = "eb_Prescription", length = 10)
	private String ebPrescription;

	@Column(name = "eb_rehabilitative", length = 10)
	private String ebRehabilitative;

	@Column(name = "eb_laboratory", length = 10)
	private String ebLaboratory;

	@Column(name = "eb_preventive_wellness", length = 10)
	private String ebPreventiveWellness;

	@Column(name = "eb_pediatric", length = 10)
	private String ebPediatric;
*/
	@Column(name = "acturial_value_certificate", length = 100)
	private String acturialValueCertificate;

	@Column(name = "benefit_file", length = 100)
	private String benefitFile;

	@Column(name = "rate_file", length = 100)
	private String rateFile;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "rate_effective_date")
	private Date rateEffectiveDate;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "benefit_effective_date")
	private Date benefitEffectiveDate;

	@Column(name = "ehb_covered")
	private String ehbCovered;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "created_by", length = 10)
	private String createdBy;

	@Column(name = "last_updated_by", length = 10)
	private Integer lastUpdatedBy;

	/*@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "effective_start_date", nullable = true)
	private Date effectiveStartDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "effective_end_date", nullable = true)
	private Date effectiveEndDate;*/

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "comment_id")
	private Integer commentId;

	@Column(name = "csr_adv_pay_amt")
	private Float csrAdvPayAmt;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "planHealth", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@MapKey(name = "name")
	private Map<String, PlanHealthCost> planHealthCosts = new HashMap<String, PlanHealthCost>();

	@NotAudited
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name ="plan_sbc_scenario_id")
	private PlanSbcScenario planSbcScenarioId ;
	
	public Float getCsrAdvPayAmt() {
		return csrAdvPayAmt;
	}

	public void setCsrAdvPayAmt(Float csrAdvPayAmt) {
		this.csrAdvPayAmt = csrAdvPayAmt;
	}

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@MapKey(name = "name")
	private Map<String, PlanHealthBenefit> planHealthBenefits = new HashMap<String, PlanHealthBenefit>();

	/*	commenting the code for SERFF changes
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "planHealth", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private  List<PlanHealthServiceRegion> planHealthServiceRegion;*/

	// Fields Added on 8May-2013 Start
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PlanHealthBenefit> planHealthBenefitVO;

	public List<PlanHealthBenefit> getPlanHealthBenefitVO() {
		return planHealthBenefitVO;
	}

	public void setPlanHealthBenefitVO(List<PlanHealthBenefit> planHealthBenefitVO) {
		this.planHealthBenefitVO = planHealthBenefitVO;
	}

	@Column(name = "design_type")
	private String planDesignType;

	@Column(name = "unique_plan_design")
	private String uniquePlanDesign;

	@Column(name = "is_qhp")
	private String isQHP;

	@Column(name = "pregnancy_notice")
	private String pregnancyNotice;

	@Column(name = "specialist_referal")
	private String specialistReferal;

	@Column(name = "plan_level_exclusions")
	private String planLevelExclusions;

	@Column(name = "child_only_offering")
	private String childOnlyOffering;

	@Column(name = "wellness_program_offered")
	private String wellnessProgramOffered;

	@Column(name = "disease_mgmt_program_offered")
	private String diseaseMgmtProgramOffered;

	@Column(name = "benefits_url")
	private String benefitsUrl;

	@Column(name = "enrollment_url")
	private String enrollmentUrl;

	@Column(name = "av_calc_output_number")
	private String avCalcOutputNumber;

	@Column(name = "multiple_network_tiers")
	private String multipleNetworkTiers;

	@Column(name = "tier1_util")
	private String tier1Util;

	@Column(name = "tier2_util")
	private String tier2Util;

	// Fields Added on 8May-2013 Ends  // Following changes are done for JIRA - PERF-81
//	@Column(name = "plan_attributes",nullable=true)
	@Basic(fetch = FetchType.LAZY) 
	@Type(type="org.hibernate.type.BinaryType")
	@Column(length = 100000, name = "plan_attributes", nullable=true) 
	private byte[] planAttributes;

	//HIX-9662-Start: Added new Columns
	@Column(name = "sbc_doc_name")
	private String sbcDocName;
	
	@Column(name = "sbc_ucm_id")
	private String sbcUcmId;
	
	//HIX-9662 -End
	
	@Column(name = "is_med_drug_deduct_integrated")
	private String isMedDrugDeductibleIntegrated;
	
	@Column(name = "is_med_drug_max_op_integrated")
	private String isMedDrugMaxOutPocketIntegrated;
	
	//Start of new columns added for HIX-8556
	@Column(name = "out_of_country_coverage", length = 5)
	private String outOfCountryCoverage;

	@Column(name = "out_of_country_coverage_desc", length = 100)
	private String outOfCountryCoverageDesc;

	@Column(name = "out_of_svc_area_coverage", length = 5)
	private String outOfServiceAreaCoverage;

	@Column(name = "out_of_svc_area_coverage_desc", length = 100)
	private String outOfServiceAreaCoverageDesc;

	@Column(name = "national_network", length = 5)
	private String nationalNetwork;
	//End of new columns added for HIX-8556

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "planHealth", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PlanHealthCost> planHealthCostsList;
					
	@Column(name = "max_coinsurance_for_sp_drug")
	private Long maxCoinsuranceForSPDrug;
	
	@Column(name = "max_chrging_inpatient_cpy")
	private Long maxDaysForChargingInpatientCopay;

	@Column(name = "pmry_care_cs_aftr_no_visits")
	private Long beginPmryCareCsAfterNumOfVisits;

	@Column(name = "pmry_care_dedtcoins_aftr_cpy")
	private Long beginPmrycareDedtOrCoinsAfterNumOfCopays;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getParentPlanId() {
		return parentPlanId;
	}

	public void setParentPlanId(int parentPlanId) {
		this.parentPlanId = parentPlanId;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}

	/*public String getEbBenchmark() {
		return ebBenchmark;
	}

	public void setEbBenchmark(String ebBenchmark) {
		this.ebBenchmark = ebBenchmark;
	}

	public String getEbAmbulatortyPatient() {
		return ebAmbulatortyPatient;
	}

	public void setEbAmbulatortyPatient(String ebAmbulatortyPatient) {
		this.ebAmbulatortyPatient = ebAmbulatortyPatient;
	}

	public String getEbEmergency() {
		return ebEmergency;
	}

	public void setEbEmergency(String ebEmergency) {
		this.ebEmergency = ebEmergency;
	}

	public String getEbHospitalization() {
		return ebHospitalization;
	}

	public void setEbHospitalization(String ebHospitalization) {
		this.ebHospitalization = ebHospitalization;
	}

	public String getEbMaternity() {
		return ebMaternity;
	}

	public void setEbMaternity(String ebMaternity) {
		this.ebMaternity = ebMaternity;
	}

	public String getEbMentalHealth() {
		return ebMentalHealth;
	}

	public void setEbMentalHealth(String ebMentalHealth) {
		this.ebMentalHealth = ebMentalHealth;
	}

	public String getEbPrescription() {
		return ebPrescription;
	}

	public void setEbPrescription(String ebPrescription) {
		this.ebPrescription = ebPrescription;
	}

	public String getEbRehabilitative() {
		return ebRehabilitative;
	}

	public void setEbRehabilitative(String ebRehabilitative) {
		this.ebRehabilitative = ebRehabilitative;
	}

	public String getEbLaboratory() {
		return ebLaboratory;
	}

	public void setEbLaboratory(String ebLaboratory) {
		this.ebLaboratory = ebLaboratory;
	}

	public String getEbPreventiveWellness() {
		return ebPreventiveWellness;
	}

	public void setEbPreventiveWellness(String ebPreventiveWellness) {
		this.ebPreventiveWellness = ebPreventiveWellness;
	}

	public String getEbPediatric() {
		return ebPediatric;
	}

	public void setEbPediatric(String ebPediatric) {
		this.ebPediatric = ebPediatric;
	}

*/	public Map<String, PlanHealthBenefit> getPlanHealthBenefits() {
		return planHealthBenefits;
	}

	public void setPlanHealthBenefits(
			Map<String, PlanHealthBenefit> planHealthBenefits) {
		this.planHealthBenefits = planHealthBenefits;
	}

	public String getCreated_by() {
		return createdBy;
	}

	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public void setCreated_by(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer updatedBy) {
		this.lastUpdatedBy = updatedBy;
	}

	/*public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}*/

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}

	public String getActurialValueCertificate() {
		return acturialValueCertificate;
	}

	public void setActurialValueCertificate(String acturialValueCertificate) {
		this.acturialValueCertificate = acturialValueCertificate;
	}

	/*	commenting the code for SERFF changes
	public List<PlanHealthServiceRegion> getPlanHealthServiceRegion() {
		return planHealthServiceRegion;
	}

	public void setPlanHealthServiceRegion(
			List<PlanHealthServiceRegion> planHealthServiceRegion) {
		this.planHealthServiceRegion = planHealthServiceRegion;
	}*/

	public String getBenefitFile() {
		return benefitFile;
	}

	public void setBenefitFile(String benefitFile) {
		this.benefitFile = benefitFile;
	}

	public String getRateFile() {
		return rateFile;
	}

	public void setRateFile(String rateFile) {
		this.rateFile = rateFile;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public Date getRateEffectiveDate() {
		return rateEffectiveDate;
	}

	public void setRateEffectiveDate(Date rateEffectiveDate) {
		this.rateEffectiveDate = rateEffectiveDate;
	}

	public String getEhbCovered() {
		return ehbCovered;
	}

	public void setEhbCovered(String ehbCovered) {
		this.ehbCovered = ehbCovered;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public Map<EHBCovered, String> getEHBCoveredList() {
		Map<EHBCovered, String> enumMap = new EnumMap<EHBCovered, String>(
				EHBCovered.class);
		enumMap.put(EHBCovered.COMPLETE, "Complete");
		enumMap.put(EHBCovered.PARTIAL, "Partial");
		return enumMap;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getPlanDesignType() {
		return planDesignType;
	}

	public void setPlanDesignType(String planDesignType) {
		this.planDesignType = planDesignType;
	}

	public String getUniquePlanDesign() {
		return uniquePlanDesign;
	}

	public void setUniquePlanDesign(String uniquePlanDesign) {
		this.uniquePlanDesign = uniquePlanDesign;
	}

	public String getIsQHP() {
		return isQHP;
	}

	public void setIsQHP(String isQHP) {
		this.isQHP = isQHP;
	}

	public String getPregnancyNotice() {
		return pregnancyNotice;
	}

	public void setPregnancyNotice(String pregnancyNotice) {
		this.pregnancyNotice = pregnancyNotice;
	}

	public String getSpecialistReferal() {
		return specialistReferal;
	}

	public void setSpecialistReferal(String specialistReferal) {
		this.specialistReferal = specialistReferal;
	}

	public String getPlanLevelExclusions() {
		return planLevelExclusions;
	}

	public void setPlanLevelExclusions(String planLevelExclusions) {
		this.planLevelExclusions = planLevelExclusions;
	}

	public String getChildOnlyOffering() {
		return childOnlyOffering;
	}

	public void setChildOnlyOffering(String childOnlyOffering) {
		this.childOnlyOffering = childOnlyOffering;
	}

	public String getWellnessProgramOffered() {
		return wellnessProgramOffered;
	}

	public void setWellnessProgramOffered(String wellnessProgramOffered) {
		this.wellnessProgramOffered = wellnessProgramOffered;
	}

	public String getDiseaseMgmtProgramOffered() {
		return diseaseMgmtProgramOffered;
	}

	public void setDiseaseMgmtProgramOffered(String diseaseMgmtProgramOffered) {
		this.diseaseMgmtProgramOffered = diseaseMgmtProgramOffered;
	}

	public String getBenefitsUrl() {
		return benefitsUrl;
	}

	public void setBenefitsUrl(String benefitsUrl) {
		this.benefitsUrl = benefitsUrl;
	}

	public String getEnrollmentUrl() {
		return enrollmentUrl;
	}

	public void setEnrollmentUrl(String enrollmentUrl) {
		this.enrollmentUrl = enrollmentUrl;
	}

	public String getAvCalcOutputNumber() {
		return avCalcOutputNumber;
	}

	public void setAvCalcOutputNumber(String avCalcOutputNumber) {
		this.avCalcOutputNumber = avCalcOutputNumber;
	}

	public String getMultipleNetworkTiers() {
		return multipleNetworkTiers;
	}

	public void setMultipleNetworkTiers(String multipleNetworkTiers) {
		this.multipleNetworkTiers = multipleNetworkTiers;
	}

	public String getTier1Util() {
		return tier1Util;
	}

	public void setTier1Util(String tier1Util) {
		this.tier1Util = tier1Util;
	}

	public String getTier2Util() {
		return tier2Util;
	}

	public void setTier2Util(String tier2Util) {
		this.tier2Util = tier2Util;
	}
		
	public byte[] getPlanAttributes()  {
		return this.planAttributes;
	}
				
	public void setPlanAttributes(byte[] planAttributes) {
		this.planAttributes = planAttributes;
	}
	public String getSbcDocName() {
		return sbcDocName;
	}

	public void setSbcDocName(String sbcDocName) {
		this.sbcDocName = sbcDocName;
	}

	public String getSbcUcmId() {
		return sbcUcmId;
	}

	public void setSbcUcmId(String sbcUcmId) {
		this.sbcUcmId = sbcUcmId;
	}
	
	public String getIsMedDrugDeductibleIntegrated() {
		 return isMedDrugDeductibleIntegrated;
	}

	public void setIsMedDrugDeductibleIntegrated(
			String isMedDrugDeductibleIntegrated) {
		this.isMedDrugDeductibleIntegrated = isMedDrugDeductibleIntegrated;
	}

	public String getIsMedDrugMaxOutPocketIntegrated() {
		return isMedDrugMaxOutPocketIntegrated;
	}

	public void setIsMedDrugMaxOutPocketIntegrated(
			String isMedDrugMaxOutPocketIntegrated) {
		this.isMedDrugMaxOutPocketIntegrated = isMedDrugMaxOutPocketIntegrated;
	}

	public Map<String, PlanHealthCost> getPlanHealthCosts() {
				return planHealthCosts;
	}

	public void setPlanHealthCosts(Map<String, PlanHealthCost> planHealthCosts) {
		this.planHealthCosts = planHealthCosts;
	}

	public String getOutOfCountryCoverage() {
		return outOfCountryCoverage;
	}

	public void setOutOfCountryCoverage(String outOfCountryCoverage) {
		this.outOfCountryCoverage = outOfCountryCoverage;
	}

	public String getOutOfCountryCoverageDesc() {
		return outOfCountryCoverageDesc;
	}

	public void setOutOfCountryCoverageDesc(String outOfCountryCoverageDesc) {
		this.outOfCountryCoverageDesc = outOfCountryCoverageDesc;
	}

	public String getOutOfServiceAreaCoverage() {
		return outOfServiceAreaCoverage;
	}

	public void setOutOfServiceAreaCoverage(String outOfServiceAreaCoverage) {
		this.outOfServiceAreaCoverage = outOfServiceAreaCoverage;
	}

	public String getOutOfServiceAreaCoverageDesc() {
		return outOfServiceAreaCoverageDesc;
	}

	public void setOutOfServiceAreaCoverageDesc(String outOfServiceAreaCoverageDesc) {
		this.outOfServiceAreaCoverageDesc = outOfServiceAreaCoverageDesc;
	}

	public String getNationalNetwork() {
		return nationalNetwork;
	}

	public void setNationalNetwork(String nationalNetwork) {
		this.nationalNetwork = nationalNetwork;
	}
	
	public List<PlanHealthCost> getPlanHealthCostsList() {
		return planHealthCostsList;
	}

	public void setPlanHealthCostsList(List<PlanHealthCost> planHealthCostsList) {
		this.planHealthCostsList = planHealthCostsList;
	}

	public PlanSbcScenario getPlanSbcScenario() {
		return planSbcScenarioId;
	}

	public void setPlanSbcScenarioId(PlanSbcScenario planSbcScenarioId) {
		this.planSbcScenarioId = planSbcScenarioId;
	}

	public Long getMaxCoinsuranceForSPDrug() {
		return maxCoinsuranceForSPDrug;
	}

	public void setMaxCoinsuranceForSPDrug(Long maxCoinsuranceForSPDrug) {
		this.maxCoinsuranceForSPDrug = maxCoinsuranceForSPDrug;
	}

	public Long getMaxDaysForChargingInpatientCopay() {
		return maxDaysForChargingInpatientCopay;
	}

	public void setMaxDaysForChargingInpatientCopay(
			Long maxDaysForChargingInpatientCopay) {
		this.maxDaysForChargingInpatientCopay = maxDaysForChargingInpatientCopay;
	}

	public Long getBeginPmryCareCsAfterNumOfVisits() {
		return beginPmryCareCsAfterNumOfVisits;
	}

	public void setBeginPmryCareCsAfterNumOfVisits(
			Long beginPmryCareCsAfterNumOfVisits) {
		this.beginPmryCareCsAfterNumOfVisits = beginPmryCareCsAfterNumOfVisits;
	}

	public Long getBeginPmrycareDedtOrCoinsAfterNumOfCopays() {
		return beginPmrycareDedtOrCoinsAfterNumOfCopays;
	}

	public void setBeginPmrycareDedtOrCoinsAfterNumOfCopays(
			Long beginPmrycareDedtOrCoinsAfterNumOfCopays) {
		this.beginPmrycareDedtOrCoinsAfterNumOfCopays = beginPmrycareDedtOrCoinsAfterNumOfCopays;
	}
}
