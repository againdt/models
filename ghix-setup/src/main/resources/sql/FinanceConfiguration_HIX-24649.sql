insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.IsBinderInvoice','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ElectronicPaymentOnly','NO','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.BinderGraceDays','15','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ProRatioSupported','NO','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.PartialPaymentSupported','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.MinimumPaymentThreshold','80','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ParitialPaymentAllocationType','ratio','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.MaximumPaymentThreshold','200','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ExchangeDeductRemittence','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.AcceptCreditCard','NO','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.PaymentGracePeriod','30','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.IsPaymentGateway','TRUE','',null,current_timestamp,null,current_timestamp);