/**
 * 
 * @author santanu
 * @version 1.0
 * @since May 30, 2017 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class PlanBenefitAndCostRequestDTO implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;	
	private Integer planId;
	private List<Map<String, String>> planBenefitAndCostList;
	private Integer updatedBy;
	
	/**
	 * @return the planId
	 */
	public Integer getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	/**
	 * @return the planBenefitAndCostList
	 */
	public List<Map<String, String>> getPlanBenefitAndCostList() {
		return planBenefitAndCostList;
	}
	/**
	 * @param planBenefitAndCOstList the planBenefitAndCostList to set
	 */
	public void setPlanBenefitAndCOstList(List<Map<String, String>> planBenefitAndCostList) {
		this.planBenefitAndCostList = planBenefitAndCostList;
	}
	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
		
}