
package com.serff.planmanagementserffapi.serff.service;



/**
 * This class was generated by Apache CXF 2.7.3
 * 2013-04-26T12:04:02.187+05:30
 * Generated source version: 2.7.3
 */

public class ServerException extends Exception {
    
    private com.serff.planmanagementserffapi.common.model.service.ServerException serverException;
    public ServerException() {
        super();
    }
    
    public ServerException(String message) {
        super(message);
    }
    
    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServerException(String message, com.serff.planmanagementserffapi.common.model.service.ServerException serverException) {
        super(message);
        this.serverException = serverException;
    }

    public ServerException(String message, com.serff.planmanagementserffapi.common.model.service.ServerException serverException, Throwable cause) {
        super(message, cause);
        this.serverException = serverException;
    }

    public com.serff.planmanagementserffapi.common.model.service.ServerException getFaultInfo() {
        return this.serverException;
    }
}
