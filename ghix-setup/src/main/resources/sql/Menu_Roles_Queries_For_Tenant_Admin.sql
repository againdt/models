------------------------------------------------------------------------------------------------------------
------- Script to add menus and submenus to MENU_ROLES table for TENANT_ADMIN role ----------
------------------------------------------------------------------------------------------------------------

DELETE FROM MENU_ROLES WHERE ROLE_ID=(SELECT ID FROM ROLES WHERE NAME='TENANT_ADMIN');

-----------------------------------
---------- ISSUERS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Issuers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Add New Issuer'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Issuers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-----------------------------------
---------- PLANS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View QHPs'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View SADPs'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Add Plans'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plan Status'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans Util'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans PDF Util'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans PDF Status'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where title ='Plan Reports'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View STMs'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View AMEs'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View VISIONs'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View Medicares'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Ancillaries'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- AFFILIATES -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Affiliates'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where title ='Add Affiliate'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Affiliates') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Affiliate'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Affiliates') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Flow'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Affiliates') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Campaign'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Affiliates') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Census Files'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Affiliates') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

--------------------------------------------------
---------- APPLICATION CONFIGURATION -------------
--------------------------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Application Configuration'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Global Config  '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='UI Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='PHIX Flow Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Shop Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Enrollment Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plan Selection Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plan Management Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Financial Management Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Serff Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Security Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Cap Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='FFM Config '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Provision Config  '),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='STARR Config'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Application Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

---------------------------------------------
---------- TENANT CONFIGURATION -------------
---------------------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Tenant Configuration'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Branding'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tenant Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Global Config'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tenant Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Permissions'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tenant Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Roles'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tenant Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Cross Tenant Linking'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tenant Configuration') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- PROVIDERS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Providers'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
60,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Mapping Files'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Providers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Provider Networks'),(select id from roles where lower(name) =lower('TENANT_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Providers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
