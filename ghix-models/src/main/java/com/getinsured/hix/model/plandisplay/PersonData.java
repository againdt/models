package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Krishna_prasad
 *
 */

public class PersonData implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	private String personId;
	private String externalPersonId;
	private String existingMedicalEnrollmentID;
	private String existingSADPEnrollmentID;
	private String firstname;
	private String lastname;
	private String dob;	
	private String smoker;	
	private String dentalEligible;
	private String relationship;
	private String coverageStartDate;
	private String newPersonFLAG;
	private String maintenanceReasonCode;
	private Double employerContribution;
	private Double employerDentalContribution;
	private String gender;
	private String seekingCoverageDental;
	private int age;
	
	public static List<PersonData> clone(List<PersonData> personDataList)
	{
		List<PersonData> personDataCopyList = new ArrayList<PersonData>();
		for(PersonData personData : personDataList)
		{
			PersonData personDataCopy = new PersonData();
			personDataCopy.setPersonId(personData.getPersonId());
			personDataCopy.setExternalPersonId(personData.getExternalPersonId());
			personDataCopy.setExistingMedicalEnrollmentID(personData.getExistingMedicalEnrollmentID());
			personDataCopy.setExistingSADPEnrollmentID(personData.getExistingSADPEnrollmentID());
			personDataCopy.setFirstname(personData.getFirstname());
			personDataCopy.setLastname(personData.getLastname());
			personDataCopy.setDob(personData.getDob());
			personDataCopy.setSmoker(personData.getSmoker());
			personDataCopy.setDentalEligible(personData.getDentalEligible());
			personDataCopy.setRelationship(personData.getRelationship());
			personDataCopy.setCoverageStartDate(personData.getCoverageStartDate());
			personDataCopy.setNewPersonFLAG(personData.getNewPersonFLAG());
			personDataCopy.setMaintenanceReasonCode(personData.getMaintenanceReasonCode());
			personDataCopy.setEmployerContribution(personData.getEmployerContribution());
			personDataCopy.setEmployerDentalContribution(personData.getEmployerDentalContribution());
			personDataCopyList.add(personDataCopy);
		}		
		return personDataCopyList;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getExternalPersonId() {
		return externalPersonId;
	}
	public void setExternalPersonId(String externalPersonId) {
		this.externalPersonId = externalPersonId;
	}	
	public String getExistingMedicalEnrollmentID() {
		return existingMedicalEnrollmentID;
	}
	public void setExistingMedicalEnrollmentID(String existingMedicalEnrollmentID) {
		this.existingMedicalEnrollmentID = existingMedicalEnrollmentID;
	}
	public String getExistingSADPEnrollmentID() {
		return existingSADPEnrollmentID;
	}
	public void setExistingSADPEnrollmentID(String existingSADPEnrollmentID) {
		this.existingSADPEnrollmentID = existingSADPEnrollmentID;
	}
	public String getSmoker() {
		return smoker;
	}
	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}	
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}	
	public String getNewPersonFLAG() {
		return newPersonFLAG;
	}
	public void setNewPersonFLAG(String newPersonFLAG) {
		this.newPersonFLAG = newPersonFLAG;
	}
	public String getMaintenanceReasonCode() {
		return maintenanceReasonCode;
	}
	public void setMaintenanceReasonCode(String maintenanceReasonCode) {
		this.maintenanceReasonCode = maintenanceReasonCode;
	}
	public String getDentalEligible() {
		return dentalEligible;
	}
	public void setDentalEligible(String dentalEligible) {
		this.dentalEligible = dentalEligible;
	}
	public Double getEmployerContribution() {
		return employerContribution;
	}
	public void setEmployerContribution(Double employerContribution) {
		this.employerContribution = employerContribution;
	}

	public Double getEmployerDentalContribution() {
		return employerDentalContribution;
	}

	public void setEmployerDentalContribution(Double employerDentalContribution) {
		this.employerDentalContribution = employerDentalContribution;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getSeekingCoverageDental() {
		return seekingCoverageDental;
	}

	public void setSeekingCoverageDental(String seekingCoverageDental) {
		this.seekingCoverageDental = seekingCoverageDental;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public enum SeekingCoverage
	{
		Y,N;
	}
}
