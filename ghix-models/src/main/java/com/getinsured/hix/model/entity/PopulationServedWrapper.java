package com.getinsured.hix.model.entity;

import java.io.Serializable;
import java.util.Map;

public class PopulationServedWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Map<String, PopulationLanguage> populationLanguages = null;
	private Map<String, PopulationEthnicity> populationEthnicities = null;
	private Map<String, PopulationIndustry> populationIndustries = null;

	public PopulationServedWrapper() {
	}

	public Map<String, PopulationLanguage> getPopulationLanguages() {
		return populationLanguages;
	}

	public void setPopulationLanguages(
			Map<String, PopulationLanguage> populationLanguages) {
		this.populationLanguages = populationLanguages;
	}

	public Map<String, PopulationEthnicity> getPopulationEthnicities() {
		return populationEthnicities;
	}

	public void setPopulationEthnicities(
			Map<String, PopulationEthnicity> populationEthnicities) {
		this.populationEthnicities = populationEthnicities;
	}

	public Map<String, PopulationIndustry> getPopulationIndustries() {
		return populationIndustries;
	}

	public void setPopulationIndustries(
			Map<String, PopulationIndustry> populationIndustries) {
		this.populationIndustries = populationIndustries;
	}

	@Override
	public String toString() {
		return "PopulationServedWrapper details: PopulationLanguages = ["+populationLanguages+"]";
	}
}
