package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_PLR_IN")
public class EnrollmentPLRIn implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_PLR_IN_SEQ")
	@SequenceGenerator(name = "ENRL_PLR_IN_SEQ", sequenceName = "ENRL_PLR_IN_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name = "DOMAIN_NAME")
	private String domainName;
	
	@Column(name = "SUB_DOMAIN_NAME")
	private String subDomainName;
	
	@Column(name = "XML_FILE_NAME")
	private String plrXmlFileName;
	
	@Column(name = "PACKAGE_NAME")
	private String plrPackageName;
	
	@Column(name = "POLICY_NO")
	private Integer policyNumber;
	
	@Column(name = "ERR_CODE_IDENTIFIER")
	private String errorCodeIdentifier;
	
	@Column(name = "FILE_VALIDATION_COMMENT_DTL")
	private String fileValidationCommentDtl;
		
	@Column(name = "ETL_LOAD_DATE")
	private Date etlLoadDate;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATED_TIMESTAMP", nullable = false)
	private Date updatedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return the subDomainName
	 */
	public String getSubDomainName() {
		return subDomainName;
	}

	/**
	 * @param subDomainName the subDomainName to set
	 */
	public void setSubDomainName(String subDomainName) {
		this.subDomainName = subDomainName;
	}

	/**
	 * @return the plrXmlFileName
	 */
	public String getPlrXmlFileName() {
		return plrXmlFileName;
	}

	/**
	 * @param plrXmlFileName the plrXmlFileName to set
	 */
	public void setPlrXmlFileName(String plrXmlFileName) {
		this.plrXmlFileName = plrXmlFileName;
	}

	/**
	 * @return the plrPackageName
	 */
	public String getPlrPackageName() {
		return plrPackageName;
	}

	/**
	 * @param plrPackageName the plrPackageName to set
	 */
	public void setPlrPackageName(String plrPackageName) {
		this.plrPackageName = plrPackageName;
	}

	/**
	 * @return the errorCodeIdentifier
	 */
	public String getErrorCodeIdentifier() {
		return errorCodeIdentifier;
	}

	/**
	 * @param errorCodeIdentifier the errorCodeIdentifier to set
	 */
	public void setErrorCodeIdentifier(String errorCodeIdentifier) {
		this.errorCodeIdentifier = errorCodeIdentifier;
	}

	/**
	 * @return the fileValidationCommentDtl
	 */
	public String getFileValidationCommentDtl() {
		return fileValidationCommentDtl;
	}

	/**
	 * @param fileValidationCommentDtl the fileValidationCommentDtl to set
	 */
	public void setFileValidationCommentDtl(String fileValidationCommentDtl) {
		this.fileValidationCommentDtl = fileValidationCommentDtl;
	}

	/**
	 * @return the policyNumber
	 */
	public Integer getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * @param policyNumber the policyNumber to set
	 */
	public void setPolicyNumber(Integer policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * @return the etlLoadDate
	 */
	public Date getEtlLoadDate() {
		return etlLoadDate;
	}

	/**
	 * @param etlLoadDate the etlLoadDate to set
	 */
	public void setEtlLoadDate(Date etlLoadDate) {
		this.etlLoadDate = etlLoadDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EnrollmentPLRIn [id=" + id + ", domainName=" + domainName + ", subDomainName=" + subDomainName
				+ ", plrXmlFileName=" + plrXmlFileName + ", plrPackageName=" + plrPackageName + ", errorCodeIdentifier="
				+ errorCodeIdentifier + ", fileValidationCommentDtl=" + fileValidationCommentDtl + ", policyNumber="
				+ policyNumber + ", etlLoadDate=" + etlLoadDate + ", createdOn=" + createdOn + ", updatedOn="
				+ updatedOn + "]";
	}
}
