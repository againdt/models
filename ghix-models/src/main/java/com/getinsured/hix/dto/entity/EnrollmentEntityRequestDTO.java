package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.PopulationServedWrapper;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.getinsured.hix.model.PaymentMethods;

/**
 * Used to wrap data received from UI and send it to ghix-entity for save
 */
public class EnrollmentEntityRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private EnrollmentEntity enrollmentEntity;
	private Site site;
	private SiteLanguages siteLanguages;
	private SiteLocationHours siteLocationHours;
	private List<SiteLocationHours> siteLocationHoursList;
	private PaymentMethods paymentMethods;
	private Integer moduleId;
	private String entityType;
	private PopulationServedWrapper populationServedWrapper;
	private Integer userId;
	private String siteType;
	private EntityDocuments entityDocuments;
	private byte[] attachment;
	private Integer pageSize;
	private Integer startRecord;
	private String zipCode;
	private String recordId;
	private String fromDate;
	private String language;
	private String receivedPayments;
	private String toDate;
	private String countyServed;
	private String entityName;
	private String entityNumber;
	private String status;
	private String organizationType;
	private Map<String, Object> searchCriteria;
	private String desigStatus;
	private Integer distance;
	private String sortBy;
	private String sortOrder;
	
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	
	public String getCountyServed() {
		return countyServed;
	}

	public void setCountyServed(String countyServed) {
		this.countyServed = countyServed;
	}

	public String getReceivedPayments() {
		return receivedPayments;
	}

	public void setReceivedPayments(String receivedPayments) {
		this.receivedPayments = receivedPayments;
	}
	
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getStartRecord() {
		return startRecord;
	}

	public void setStartRecord(Integer startRecord) {
		this.startRecord = startRecord;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public EnrollmentEntity getEnrollmentEntity() {
		return enrollmentEntity;
	}

	public void setEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		this.enrollmentEntity = enrollmentEntity;
	}

	public SiteLocationHours getSiteLocationHours() {
		return siteLocationHours;
	}

	public void setSiteLocationHours(SiteLocationHours siteLocationHours) {
		this.siteLocationHours = siteLocationHours;
	}

	public List<SiteLocationHours> getSiteLocationHoursList() {
		return siteLocationHoursList;
	}

	public void setSiteLocationHoursList(List<SiteLocationHours> siteLocationHoursList) {
		this.siteLocationHoursList = siteLocationHoursList;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public SiteLanguages getSiteLanguages() {
		return siteLanguages;
	}

	public void setSiteLanguages(SiteLanguages siteLanguages) {
		this.siteLanguages = siteLanguages;
	}

	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public PopulationServedWrapper getPopulationServedWrapper() {
		return populationServedWrapper;
	}

	public void setPopulationServedWrapper(PopulationServedWrapper populationServedWrapper) {
		this.populationServedWrapper = populationServedWrapper;
	}

	public EntityDocuments getEntityDocuments() {
		return entityDocuments;
	}

	public void setEntityDocuments(EntityDocuments entityDocuments) {
		this.entityDocuments = entityDocuments;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	public void setAttachment(byte[] attachment1) {
		if (attachment1 == null) {
			this.attachment = new byte[0];
		} else {
			this.attachment = Arrays.copyOf(attachment1, attachment1.length);
		}
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public Map<String, Object> getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(Map<String, Object> searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public String getDesigStatus() {
		return desigStatus;
	}

	public void setDesigStatus(String desigStatus) {
		this.desigStatus = desigStatus;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}
	
}
