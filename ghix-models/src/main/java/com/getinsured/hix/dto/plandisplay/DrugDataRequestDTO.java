package com.getinsured.hix.dto.plandisplay;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;

/**
 * DTO class is used to provide Request for Drug Data List.
 * 
 * @since October 23, 2018
 */
public class DrugDataRequestDTO {

	private List<String> rxcuiList;

	private int pageNumber;
	private int pageSize = GhixConstants.PAGE_SIZE; // Set Default value.
	private int startRecord;
	private String sortBy;
	private String sortOrder;

	public DrugDataRequestDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public List<String> getRxcuiList() {
		return rxcuiList;
	}

	public void addRxcuiToList(String rxcui) {

		if (rxcuiList == null) {
			rxcuiList = new ArrayList<String>();
		}
		rxcuiList.add(rxcui);
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStartRecord() {

		if (pageNumber <= 0) {
			pageNumber = 1;
		}
		startRecord = (pageNumber - 1) * this.pageSize;
		return startRecord;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
}
