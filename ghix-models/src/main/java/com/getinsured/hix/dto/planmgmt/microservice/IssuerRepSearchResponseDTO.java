package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerRepSearchResponseDTO extends GhixResponseDTO {

	private String totalNumOfRecords;
	private String issuerName;
	private String companyLogo;
	private List<IssuerRepDTO> issuerRepDTOList;

	public IssuerRepSearchResponseDTO() {
		super();
	}

	public String getTotalNumOfRecords() {
		return totalNumOfRecords;
	}

	public void setTotalNumOfRecords(String totalNumOfRecords) {
		this.totalNumOfRecords = totalNumOfRecords;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public List<IssuerRepDTO> getIssuerRepDTOList() {
		return issuerRepDTOList;
	}

	public void setIssuerRepDTO(IssuerRepDTO issuerRepDTO) {

		if (CollectionUtils.isEmpty(this.issuerRepDTOList)) {
			this.issuerRepDTOList = new ArrayList<IssuerRepDTO>();
		}
		this.issuerRepDTOList.add(issuerRepDTO);
	}
}
