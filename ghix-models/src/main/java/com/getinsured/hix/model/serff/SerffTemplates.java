package com.getinsured.hix.model.serff;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

/**
 * Model class for SERFF_TEMPLATES table
 * @author Geetha Chandran
 * @since 1 Aug, 2013
 */
@Audited
@Entity
@Table(name="SERFF_TEMPLATES")
@DynamicInsert
@DynamicUpdate
public class SerffTemplates {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERFF_TEMPLATES_SEQ")
	@SequenceGenerator(name = "SERFF_TEMPLATES_SEQ", sequenceName = "SERFF_TEMPLATES_SEQ", allocationSize = 1)
	private Long id;

	@Column(name="PLAN_ID")
	private String planId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="ADMIN_LMD")
	private Date adminLastModifiedDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="NETWORK_LMD")
	private Date networkLastModifiedDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="SVCAREA_LMD")
	private Date svcAreaLastModifiedDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="PLAN_LMD")
	private Date planLastModifiedDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="RATES_LMD")
	private Date ratesLastModifiedDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="PRES_DRUG_LMD")
	private Date presDrugLastModifiedDate;
	
	@Column(name = "APPLICABLE_YEAR")
    private Integer applicableYear;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="ADMIN_LAST_UPDATED")
	private Date adminLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="NETWORK_LAST_UPDATED")
	private Date networkLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="SERVICE_AREA_LAST_UPDATED")
	private Date serviceAreaLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="PLAN_LAST_UPDATED")
	private Date planLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="RATES_LAST_UPDATED")
	private Date ratesLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="PRES_DRUG_LAST_UPDATED")
	private Date presDrugLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="BUSINESS_RULES_LAST_UPDATED")
	private Date businessRuleLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="URAC_LAST_UPDATED")
	private Date uracLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="NCQA_LAST_UPDATED")
	private Date ncqaLastUpdated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="URRT_LAST_UPDATED")
	private Date urrtLastUpdated;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Date getAdminLastModifiedDate() {
		return adminLastModifiedDate;
	}

	public void setAdminLastModifiedDate(Date adminLastModifiedDate) {
		this.adminLastModifiedDate = adminLastModifiedDate;
	}

	public Date getNetworkLastModifiedDate() {
		return networkLastModifiedDate;
	}

	public void setNetworkLastModifiedDate(Date networkLastModifiedDate) {
		this.networkLastModifiedDate = networkLastModifiedDate;
	}

	public Date getSvcAreaLastModifiedDate() {
		return svcAreaLastModifiedDate;
	}

	public void setSvcAreaLastModifiedDate(Date svcAreaLastModifiedDate) {
		this.svcAreaLastModifiedDate = svcAreaLastModifiedDate;
	}

	public Date getPlanLastModifiedDate() {
		return planLastModifiedDate;
	}

	public void setPlanLastModifiedDate(Date planLastModifiedDate) {
		this.planLastModifiedDate = planLastModifiedDate;
	}

	public Date getRatesLastModifiedDate() {
		return ratesLastModifiedDate;
	}

	public void setRatesLastModifiedDate(Date ratesLastModifiedDate) {
		this.ratesLastModifiedDate = ratesLastModifiedDate;
	}

	public Date getPresDrugLastModifiedDate() {
		return presDrugLastModifiedDate;
	}

	public void setPresDrugLastModifiedDate(Date presDrugLastModifiedDate) {
		this.presDrugLastModifiedDate = presDrugLastModifiedDate;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public Date getAdminLastUpdated() {
		return adminLastUpdated;
	}

	public void setAdminLastUpdated(Date adminLastUpdated) {
		this.adminLastUpdated = adminLastUpdated;
	}

	public Date getNetworkLastUpdated() {
		return networkLastUpdated;
	}

	public void setNetworkLastUpdated(Date networkLastUpdated) {
		this.networkLastUpdated = networkLastUpdated;
	}

	public Date getServiceAreaLastUpdated() {
		return serviceAreaLastUpdated;
	}

	public void setServiceAreaLastUpdated(Date serviceAreaLastUpdated) {
		this.serviceAreaLastUpdated = serviceAreaLastUpdated;
	}

	public Date getPlanLastUpdated() {
		return planLastUpdated;
	}

	public void setPlanLastUpdated(Date planLastUpdated) {
		this.planLastUpdated = planLastUpdated;
	}

	public Date getRatesLastUpdated() {
		return ratesLastUpdated;
	}

	public void setRatesLastUpdated(Date ratesLastUpdated) {
		this.ratesLastUpdated = ratesLastUpdated;
	}

	public Date getPresDrugLastUpdated() {
		return presDrugLastUpdated;
	}

	public void setPresDrugLastUpdated(Date presDrugLastUpdated) {
		this.presDrugLastUpdated = presDrugLastUpdated;
	}

	public Date getBusinessRuleLastUpdated() {
		return businessRuleLastUpdated;
	}

	public void setBusinessRuleLastUpdated(Date businessRuleLastUpdated) {
		this.businessRuleLastUpdated = businessRuleLastUpdated;
	}

	public Date getUracLastUpdated() {
		return uracLastUpdated;
	}

	public void setUracLastUpdated(Date uracLastUpdated) {
		this.uracLastUpdated = uracLastUpdated;
	}

	public Date getNcqaLastUpdated() {
		return ncqaLastUpdated;
	}

	public void setNcqaLastUpdated(Date ncqaLastUpdated) {
		this.ncqaLastUpdated = ncqaLastUpdated;
	}

	public Date getUrrtLastUpdated() {
		return urrtLastUpdated;
	}

	public void setUrrtLastUpdated(Date urrtLastUpdated) {
		this.urrtLastUpdated = urrtLastUpdated;
	}
}
