/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;


/**
 * @author panda_p
 *
 */
@Audited
@Entity
@XmlAccessorType(XmlAccessType.NONE)
@Table(name="ENROLLEE_RACE")
public class EnrolleeRace implements Serializable {
	
	@XmlElement(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLEE_RACE_SEQ")
	@SequenceGenerator(name = "ENROLLEE_RACE_SEQ", sequenceName = "ENROLLEE_RACE_SEQ", allocationSize = 1)
	private Integer id;
	
		
//	@NotAudited 
//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@XmlElement(name="raceEthnicityLkp")
	@OneToOne
	@JoinColumn(name = "RACE_ETHNICITY_LKP")
	private LookupValue raceEthnicityLkp;
	

	@ManyToOne//(cascade= {CascadeType.ALL})
    @JoinColumn(name="ENROLEE_ID")
	private Enrollee enrollee;
	
	@Column(name="RACE_DESCRIPTION",nullable=true)
	private String raceDescription;
	
		
	public LookupValue getRaceEthnicityLkp() {
		return raceEthnicityLkp;
	}

	public void setRaceEthnicityLkp(LookupValue raceEthnicityLkp) {
		this.raceEthnicityLkp = raceEthnicityLkp;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private AccountUser createdBy;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Enrollee getEnrollee() {
		return enrollee;
	}

	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}

	public String getRaceDescription() {
		return raceDescription;
	}

	public void setRaceDescription(String raceDescription) {
		this.raceDescription = raceDescription;
	}
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
