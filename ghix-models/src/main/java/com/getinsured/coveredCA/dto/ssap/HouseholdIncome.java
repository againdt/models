package com.getinsured.coveredCA.dto.ssap;

/**
 * @author gjape
 *
 */
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class HouseholdIncome {
	
	
	@Expose
	private List<EmploymentIncome> employmentIncome = new ArrayList<EmploymentIncome>();
	@Expose
	private List<SelfEmploymentIncome> selfEmploymentIncome = new ArrayList<SelfEmploymentIncome>();
	@Expose
	private List<OtherIncome> otherIncome = new ArrayList<OtherIncome>();
	@Expose
	private List<Deductions> deductions = new ArrayList<Deductions>();
	
	@Expose
	private String calculatedYearlyIncome;
	@Expose
	private String expectedYearlyIncome;
	@Expose
	private Boolean expectedYearlyIncomeIndicator;
	
	
	
	
	
	
	public List<Deductions> getDeductions() {
		return deductions;
	}
	public void setDeductions(List<Deductions> deductions) {
		this.deductions = deductions;
	}

	public String getExpectedYearlyIncome() {
		return expectedYearlyIncome;
	}
	public void setExpectedYearlyIncome(String expectedYearlyIncome) {
		this.expectedYearlyIncome = expectedYearlyIncome;
	}
	public List<EmploymentIncome> getEmploymentIncome() {
		return employmentIncome;
	}
	public void setEmploymentIncome(List<EmploymentIncome> employmentIncome) {
		this.employmentIncome = employmentIncome;
	}
	public List<SelfEmploymentIncome> getSelfEmploymentIncome() {
		return selfEmploymentIncome;
	}
	public void setSelfEmploymentIncome(
			List<SelfEmploymentIncome> selfEmploymentIncome) {
		this.selfEmploymentIncome = selfEmploymentIncome;
	}
	public List<OtherIncome> getOtherIncome() {
		return otherIncome;
	}
	public void setOtherIncome(List<OtherIncome> otherIncome) {
		this.otherIncome = otherIncome;
	}
	public String getCalculatedYearlyIncome() {
		return calculatedYearlyIncome;
	}
	public void setCalculatedYearlyIncome(String calculatedYearlyIncome) {
		this.calculatedYearlyIncome = calculatedYearlyIncome;
	}
	public Boolean getExpectedYearlyIncomeIndicator() {
		return expectedYearlyIncomeIndicator;
	}
	public void setExpectedYearlyIncomeIndicator(
			Boolean expectedYearlyIncomeIndicator) {
		this.expectedYearlyIncomeIndicator = expectedYearlyIncomeIndicator;
	}
	
	

}
