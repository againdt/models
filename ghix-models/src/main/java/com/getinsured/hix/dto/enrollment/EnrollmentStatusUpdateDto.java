package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.AccountUser;

public class EnrollmentStatusUpdateDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer enrollmentId;
	private String issuerAssignPolicyNo;
	private List<EnrolleeUpdateDto> enrolleeUpdateDtoList;
	private AccountUser updatedByUser;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}
	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}
	public List<EnrolleeUpdateDto> getEnrolleeUpdateDtoList() {
		return enrolleeUpdateDtoList;
	}
	public void setEnrolleeUpdateDtoList(
			List<EnrolleeUpdateDto> enrolleeUpdateDtoList) {
		this.enrolleeUpdateDtoList = enrolleeUpdateDtoList;
	}
	public AccountUser getUpdatedByUser() {
		return updatedByUser;
	}
	public void setUpdatedByUser(AccountUser updatedByUser) {
		this.updatedByUser = updatedByUser;
	}
}
