/**
 * 
 */
package com.getinsured.hix.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


/**
 * Persistence class 'DrugCodeTierCodeType' mapping to table 'DRUG_CODE_TIER_CODE_TYPE'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 15, 2013 4:21:04 PM 
 *
 */
@Entity
@Table(name = "DRUG_CODE_TIER_CODE_TYPE")
public class DrugCodeTierCodeType {
	@Id
	@SequenceGenerator(name = "DrugCodeTierCodeType_Seq", sequenceName = "DRUG_CODE_TIER_CODE_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DrugCodeTierCodeType_Seq")
	private int id;
	
	@Column(name = "NAME")
	@Length(max = 200)
	@NotNull
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "DCS_TIER_TYPE_ID", referencedColumnName = "ID")
    private DCSTierType dcsTierTypeId;
	
	@NotNull
	@Column(name="CREATION_TIMESTAMP")
	private Timestamp created;
	
	@NotNull
	@Column(name="LAST_UPDATED_TIMESTAMP")
	private Timestamp updated;
	
	/**
	 * Default constructor.
	 */
	public DrugCodeTierCodeType() {
	}

	/**
	 * Getter for 'id'.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter for 'id'.
	 *
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter for 'name'.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for 'name'.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for 'dcsTierTypeId'.
	 *
	 * @return the dcsTierTypeId
	 */
	public DCSTierType getDcsTierTypeId() {
		return dcsTierTypeId;
	}

	/**
	 * Setter for 'dcsTierTypeId'.
	 *
	 * @param dcsTierTypeId the dcsTierTypeId to set
	 */
	public void setDcsTierTypeId(DCSTierType dcsTierTypeId) {
		this.dcsTierTypeId = dcsTierTypeId;
	}

	/**
	 * Getter for 'created'.
	 *
	 * @return the created
	 */
	public Timestamp getCreated() {
		return created;
	}

	/**
	 * Setter for 'created'.
	 *
	 * @param created the created to set
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}

	/**
	 * Getter for 'updated'.
	 *
	 * @return the updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}

	/**
	 * Setter for 'updated'.
	 *
	 * @param updated the updated to set
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

}
