package com.getinsured.eligibility.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the PROGRAM_ELIG_GIWSPAYLOAD database table.
 *
 */
@Entity
@Table(name="PROGRAM_ELIG_GIWSPAYLOAD")
@DynamicInsert
@DynamicUpdate
public class EligibilityProgramGiwspayload {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROGRAM_ELIG_GIWSPAYLOAD_SEQ")
	@SequenceGenerator(name = "PROGRAM_ELIG_GIWSPAYLOAD_SEQ", sequenceName = "PROGRAM_ELIG_GIWSPAYLOAD_SEQ", allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name="GI_WS_PAYLOAD_ID", nullable=false)
	private Integer giWsPayloadId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROGRAM_ELIG_ID", nullable=false)
	private EligibilityProgram eligibilityProgram;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public EligibilityProgram getEligibilityProgram() {
		return eligibilityProgram;
	}

	public void setEligibilityProgram(EligibilityProgram eligibilityProgram) {
		this.eligibilityProgram = eligibilityProgram;
	}

}