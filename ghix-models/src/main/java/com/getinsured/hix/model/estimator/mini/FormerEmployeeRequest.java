/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.List;

import com.getinsured.hix.model.prescreen.calculator.GHIXRequest;


public class FormerEmployeeRequest extends GHIXRequest {
	
	
	private long affiliateId;
	private Integer flowId;
	private String apiKey;
	private String dataType;
	
	// Household Level Data
		private String externalId;// Household External ID
		private String firstName;
		private String lastName;
		private String email;
		private String phone;
		
		private String address1;
		private String address2;
		private String city;
		private String state;
		
		private String zipCode;
		private String countyCode;
		
		private int familySize;
		private int eventId;
		
		private double hhIncome = -1.0;
		
		private String pinCode; 
		
		private String eventDate;
		private String hraFundingType; // Possible values are IndividualAnnual, IndividualMonthly,FamilyLumpSum
		private double familyHraAmount=-1.0; // Household Level
		private List<EmployeeMember> members;
		
		private boolean skipAffiliateValidation;
		private String isOkToCall;
		//HIX-86405 - data sharing consent should be default to false, if nothing passed.
		private boolean dataSharingConsent;
	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getHraFundingType() {
		return hraFundingType;
	}

	public void setHraFundingType(String hraFundingType) {
		this.hraFundingType = hraFundingType;
	}

	public double getFamilyHraAmount() {
		return familyHraAmount;
	}

	public void setFamilyHraAmount(double familyHraAmount) {
		this.familyHraAmount = familyHraAmount;
	}

	
	
	
	public boolean getDataSharingConsent() {
		return dataSharingConsent;
	}
	
	public void setDataSharingConsent(boolean dataSharingConsent) {
		this.dataSharingConsent = dataSharingConsent;
	}
	

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public double getHhIncome() {
		return hhIncome;
	}

	public void setHhIncome(double hhIncome) {
		this.hhIncome = hhIncome;
	}

	



	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	public String getAffiliateHouseholdId() {
		return externalId;
	}

	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.externalId = affiliateHouseholdId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	

	public boolean isSkipAffiliateValidation() {
		return skipAffiliateValidation;
	}

	public void setSkipAffiliateValidation(boolean skipAffiliateValidation) {
		this.skipAffiliateValidation = skipAffiliateValidation;
	}

	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MiniEstimatorRequest [zipCode=");
		builder.append(zipCode);
		builder.append(", countyCode=");
		builder.append(countyCode);
		builder.append(", familySize=");
		builder.append(familySize);
		builder.append(", hhIncome=");
		builder.append(hhIncome);
		builder.append(", docVisitFrequency=");
	
		builder.append(", members=");
		builder.append(members);
		builder.append(", affiliateId=");
		builder.append(affiliateId);
		builder.append(", flowId=");
		builder.append(flowId);
		builder.append(", affiliateHouseholdId=");
		builder.append(externalId);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", apiKey=");
		builder.append(apiKey);
		builder.append(", leadId=");
		
		builder.append(", clickId=");
	
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		final int bits = 32;
		int result = 1;
		result = prime * result
				+ ((externalId == null) ? 0 : externalId
						.hashCode());
		result = prime * result + (int) (affiliateId ^ (affiliateId >>> bits));
		result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
		result = prime * result
				+ ((countyCode == null) ? 0 : countyCode.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + familySize;
		long temp;
		temp = Double.doubleToLongBits(hhIncome);
		result = prime * result + (int) (temp ^ (temp >>> bits));
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		FormerEmployeeRequest other = (FormerEmployeeRequest) obj;
		if (externalId != other.externalId){
			return false;
		}
		if (affiliateId != other.affiliateId){
			return false;
		}
		if (apiKey == null) {
			if (other.apiKey != null){
				return false;
			}
		} else if (!apiKey.equals(other.apiKey)){
			return false;
		}
		if (countyCode == null) {
			if (other.countyCode != null){
				return false;
			}
		} else if (!countyCode.equals(other.countyCode)){
			return false;
		}
		if (email == null) {
			if (other.email != null){
				return false;
			}
		} else if (!email.equals(other.email)){
			return false;
		}
		if (familySize != other.familySize){
			return false;
		}
		if (Double.doubleToLongBits(hhIncome) != Double
				.doubleToLongBits(other.hhIncome)){
			return false;
		}
		if (members == null) {
			if (other.members != null){
				return false;
			}
		} else if (!members.equals(other.members)){
			return false;
		}
		if (phone == null) {
			if (other.phone != null){
				return false;
			}
		} else if (!phone.equals(other.phone)){
			return false;
		}
		if (zipCode == null) {
			if (other.zipCode != null){
				return false;
			}
		} else if (!zipCode.equals(other.zipCode)){
			return false;
		}
		return true;
	}

	

	public List<EmployeeMember> getMembers() {
		return members;
	}

	public void setMembers(List<EmployeeMember> members) {
		this.members = members;
	}

	public String getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(String isOkToCall) {
		this.isOkToCall = isOkToCall;
	}
	
	
}
