package com.getinsured.hix.dto.entity;

import java.io.Serializable;

import com.getinsured.hix.model.entity.EnrollmentEntity;

public class EnrollmentEntityDTO implements Serializable {

	private EnrollmentEntity enrollmentEntity;

	public EnrollmentEntity getEnrollmentEntity() {
		return enrollmentEntity;
	}

	public void setEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		this.enrollmentEntity = enrollmentEntity;
	}
	
}
