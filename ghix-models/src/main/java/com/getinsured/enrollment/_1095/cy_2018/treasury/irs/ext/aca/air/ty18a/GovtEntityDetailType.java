
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Govt Entity Detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for GovtEntityDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GovtEntityDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityAddress"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AlternativeFilerEIN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityContactName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityContactPhoneNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityFaxNum" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityEmailTxt" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GovtEntityDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "govtEntityNm",
    "govtEntityAddress",
    "alternativeFilerEIN",
    "govtEntityContactName",
    "govtEntityContactPhoneNum",
    "govtEntityFaxNum",
    "govtEntityEmailTxt",
    "errorDetail"
})
public class GovtEntityDetailType {

    @XmlElement(name = "GovtEntityNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String govtEntityNm;
    @XmlElement(name = "GovtEntityAddress", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MailingAddressGrpType govtEntityAddress;
    @XmlElement(name = "AlternativeFilerEIN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String alternativeFilerEIN;
    @XmlElement(name = "GovtEntityContactName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CompletePersonNameDetailType govtEntityContactName;
    @XmlElement(name = "GovtEntityContactPhoneNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String govtEntityContactPhoneNum;
    @XmlElement(name = "GovtEntityFaxNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String govtEntityFaxNum;
    @XmlElement(name = "GovtEntityEmailTxt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String govtEntityEmailTxt;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the govtEntityNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityNm() {
        return govtEntityNm;
    }

    /**
     * Sets the value of the govtEntityNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityNm(String value) {
        this.govtEntityNm = value;
    }

    /**
     * Gets the value of the govtEntityAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public MailingAddressGrpType getGovtEntityAddress() {
        return govtEntityAddress;
    }

    /**
     * Sets the value of the govtEntityAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public void setGovtEntityAddress(MailingAddressGrpType value) {
        this.govtEntityAddress = value;
    }

    /**
     * Gets the value of the alternativeFilerEIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternativeFilerEIN() {
        return alternativeFilerEIN;
    }

    /**
     * Sets the value of the alternativeFilerEIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternativeFilerEIN(String value) {
        this.alternativeFilerEIN = value;
    }

    /**
     * Gets the value of the govtEntityContactName property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public CompletePersonNameDetailType getGovtEntityContactName() {
        return govtEntityContactName;
    }

    /**
     * Sets the value of the govtEntityContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public void setGovtEntityContactName(CompletePersonNameDetailType value) {
        this.govtEntityContactName = value;
    }

    /**
     * Gets the value of the govtEntityContactPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityContactPhoneNum() {
        return govtEntityContactPhoneNum;
    }

    /**
     * Sets the value of the govtEntityContactPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityContactPhoneNum(String value) {
        this.govtEntityContactPhoneNum = value;
    }

    /**
     * Gets the value of the govtEntityFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityFaxNum() {
        return govtEntityFaxNum;
    }

    /**
     * Sets the value of the govtEntityFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityFaxNum(String value) {
        this.govtEntityFaxNum = value;
    }

    /**
     * Gets the value of the govtEntityEmailTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityEmailTxt() {
        return govtEntityEmailTxt;
    }

    /**
     * Sets the value of the govtEntityEmailTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityEmailTxt(String value) {
        this.govtEntityEmailTxt = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
