package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * This object contains essential information we use
 * while persisting enrollment meta record.
 * @author root
 *
 */
public class DirectEnrollmentMetaDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5384856313999276111L;
	private Long enrollmentMetaId;
	private String carrierName;
	private String carrierId;
	private String state;
	/**
	 * additional check to mark this record inactive if needed
	 */
	private Boolean active;
	private Date startDate;
	private Date endDate;
	private String schema;
	
	public Long getEnrollmentMetaId() {
		return enrollmentMetaId;
	}
	public void setEnrollmentMetaId(Long enrollmentMetaId) {
		this.enrollmentMetaId = enrollmentMetaId;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public String getCarrierId() {
		return carrierId;
	}
	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}	

	public static void main(String args[]){
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		DirectEnrollmentMetaDTO dto = new DirectEnrollmentMetaDTO();
		dto.setCarrierId("someId");
		dto.setEnrollmentMetaId(5L);
		dto.setState("FL");
		dto.setSchema("dummy:json{}data");
		System.out.println(gson.toJson(dto));
		
	}
	
	
}
