package com.getinsured.hix.dto.externalassister;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class DesignateAssisterDTO extends GHIXResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<DesignateAssisterResponse> designateAssisterResponse;

	public List<DesignateAssisterResponse> getDesignateAssisterResponse() {
		return designateAssisterResponse;
	}

	public void setDesignateAssisterResponse(List<DesignateAssisterResponse> designateAssisterResponse) {
		this.designateAssisterResponse = designateAssisterResponse;
	}

}
