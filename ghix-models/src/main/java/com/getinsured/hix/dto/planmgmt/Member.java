package com.getinsured.hix.dto.planmgmt;

public class Member {

	private int id;
	private String relation;
	private String dob;
	private String tobacco;
	private String gender;
	private int age;
	private String zipCode;
	private String countyCode;

	/**
	 * @return the Id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param Id the Id to set
	 */
	public void setId(int Id) {
		this.id = Id;
	}
	/**
	 * @return the relation
	 */
	public String getRelation() {
		return relation;
	}
	/**
	 * @param relation the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}
	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	/**
	 * @return the tobacco
	 */
	public String getTobacco() {
		return tobacco;
	}
	/**
	 * @param tobacco the tobacco to set
	 */
	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the countyCode
	 */
	public String getCountyCode() {
		return countyCode;
	}
	/**
	 * @param countyCode the countyCode to set
	 */
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	
	
}
