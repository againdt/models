insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (1,'Triage Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (3,'Appeal Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (4,'Technical Issues Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (6,'Billing Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (7,'General Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (9,'Feedback Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (10,'Complaint Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (11,'Document Verification Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (14,'Appeal Review Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (16,'Referral Identity Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (21,'Expedited Appeals Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
insert into TKM_QUEUES (TKM_QUEUE_ID,QUEUE_NAME,ROLE_ID,CREATION_TIMESTAMP,CREATED_BY,LAST_UPDATE_TIMESTAMP,LAST_UPDATED_BY,IS_DEFAULT) values (41,'Issuer Workgroup',Null,SYSTIMESTAMP,1,SYSTIMESTAMP,1,'Y');
 commit;
DROP SEQUENCE TKM_QUEUES_SEQ;
CREATE SEQUENCE TKM_QUEUES_SEQ START WITH 1041;

