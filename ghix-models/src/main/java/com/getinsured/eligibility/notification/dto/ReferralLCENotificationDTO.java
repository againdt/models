package com.getinsured.eligibility.notification.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;


public class ReferralLCENotificationDTO implements JSONAware{
	
	private static final String NO = "No";
	
	private List<String> ssapApplicants;
	
	private String userName;
	
	private String primaryApplicantName;
	
	private String caseNumber;
	
	private String keepOnly;
	
	private Date enrollmentEndDate;
	
	private String spanishEnrollmentDate;
	
	private String dependentName;
	
	private String eventType;
	
	private String currentPlanAvailable = NO;
	
	private String currentPlanPremiumChange = NO;
	
	private String newPremium;
	
	private String isHealthDisEnrolled;
	
	private String isDentalDisEnrolled;
	
	private Date terminationDate;
	
	private String newHealthPremiumBeforeCredit;
	
	private String newDentalPremiumBeforeCredit;
	
	private List<String> healthEnrollees;
	
	private List<String> dentalEnrollees;
	
	private List<String> addedDependents;
	
	private List<String> removedDependents;
	
	private String changePlan;
	
	private String applicationValidationStatus;
	
	private Set<String> gatedEventLabels = new HashSet<>();
	
	private String meta_external_household_case_id;
	
	private String coverageStartDate;
	
	private String planName;
	
	private List<Map<String, String>> memberNames = new ArrayList<Map<String, String>>();
	
	private long ssapApplicationId;
	
	//  this flag is added for QEP confirm life event as 2 notices are triggered for document Required notice
	private boolean triggerDocumentRequiredNoticeFlag = true;	
	
	public boolean isTriggerDocumentRequiredNoticeFlag() {
		return triggerDocumentRequiredNoticeFlag;
	}

	public void setTriggerDocumentRequiredNoticeFlag(boolean triggerDocumentRequiredNoticeFlag) {
		this.triggerDocumentRequiredNoticeFlag = triggerDocumentRequiredNoticeFlag;
	}

	public Set<String> getGatedEventLabels() {
		return gatedEventLabels;
	}

	public String getCurrentPlanAvailable() {
		return currentPlanAvailable;
	}

	public void setCurrentPlanAvailable(String currentPlanAvailable) {
		this.currentPlanAvailable = currentPlanAvailable;
	}

	public String getCurrentPlanPremiumChange() {
		return currentPlanPremiumChange;
	}

	public void setCurrentPlanPremiumChange(String currentPlanPremiumChange) {
		this.currentPlanPremiumChange = currentPlanPremiumChange;
	}

	public String getNewPremium() {
		return newPremium;
	}

	public void setNewPremium(String newPremium) {
		this.newPremium = newPremium;
	}
	
	public String getDependentName() {
		return dependentName;
	}

	public void setDependentName(String dependentName) {
		this.dependentName = dependentName;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public List<String> getSsapApplicants() {
		return ssapApplicants;
	}

	public void setSsapApplicants(List<String> ssapApplicants) {
		this.ssapApplicants = ssapApplicants;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPrimaryApplicantName() {
		return primaryApplicantName;
	}

	public void setPrimaryApplicantName(String primaryApplicantName) {
		this.primaryApplicantName = primaryApplicantName;
	}

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}

	public Date getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Date enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public String getSpanishEnrollmentDate() {
		return spanishEnrollmentDate;
	}

	public void setSpanishEnrollmentDate(String spanishEnrollmentDate) {
		this.spanishEnrollmentDate = spanishEnrollmentDate;
	}

	public String getIsHealthDisEnrolled() {
		return isHealthDisEnrolled;
	}

	public void setIsHealthDisEnrolled(String isHealthDisEnrolled) {
		this.isHealthDisEnrolled = isHealthDisEnrolled;
	}

	public String getIsDentalDisEnrolled() {
		return isDentalDisEnrolled;
	}

	public void setIsDentalDisEnrolled(String isDentalDisEnrolled) {
		this.isDentalDisEnrolled = isDentalDisEnrolled;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	
	public String getNewHealthPremiumBeforeCredit() {
		return newHealthPremiumBeforeCredit;
	}

	public void setNewHealthPremiumBeforeCredit(String newHealthPremiumBeforeCredit) {
		this.newHealthPremiumBeforeCredit = newHealthPremiumBeforeCredit;
	}

	public String getNewDentalPremiumBeforeCredit() {
		return newDentalPremiumBeforeCredit;
	}

	public void setNewDentalPremiumBeforeCredit(String newDentalPremiumBeforeCredit) {
		this.newDentalPremiumBeforeCredit = newDentalPremiumBeforeCredit;
	}

	public List<String> getHealthEnrollees() {
		return healthEnrollees;
	}

	public void setHealthEnrollees(List<String> healthEnrollees) {
		this.healthEnrollees = healthEnrollees;
	}

	public List<String> getDentalEnrollees() {
		return dentalEnrollees;
	}

	public void setDentalEnrollees(List<String> dentalEnrollees) {
		this.dentalEnrollees = dentalEnrollees;
	}

	public List<String> getAddedDependents() {
		return addedDependents;
	}

	public void setAddedDependents(List<String> addedDependents) {
		this.addedDependents = addedDependents;
	}

	public List<String> getRemovedDependents() {
		return removedDependents;
	}

	public void setRemovedDependents(List<String> removedDependents) {
		this.removedDependents = removedDependents;
	}

	public String getChangePlan() {
		return changePlan;
	}

	public void setChangePlan(String changePlan) {
		this.changePlan = changePlan;
	}

	public String getApplicationValidationStatus() {
		return applicationValidationStatus;
	}

	public void setApplicationValidationStatus(String applicationValidationStatus) {
		this.applicationValidationStatus = applicationValidationStatus;
	}

	public void addGatedEventLabel(String gatedEventLabel) {
		this.gatedEventLabels.add(gatedEventLabel);
	}

	public String getMeta_external_household_case_id() {
		return meta_external_household_case_id;
	}

	public void setMeta_external_household_case_id(String meta_external_household_case_id) {
		this.meta_external_household_case_id = meta_external_household_case_id;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	
	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}
	
	public List<Map<String, String>> getMemberNames() {
		return memberNames;
	}

	public void setMemberNames(List<Map<String, String>> memberNames) {
		this.memberNames = memberNames;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject jsonObj = new JSONObject();
		
		jsonObj.put("ssapApplicants", ssapApplicants);	
		jsonObj.put("userName", userName);	
		jsonObj.put("primaryApplicantName", primaryApplicantName);	
		jsonObj.put("caseNumber", caseNumber);	
		jsonObj.put("keepOnly", keepOnly);	
		jsonObj.put("enrollmentEndDate", enrollmentEndDate.toString());	
		jsonObj.put("spanishEnrollmentDate", spanishEnrollmentDate);	
		jsonObj.put("dependentName", dependentName);	
		jsonObj.put("eventType", eventType);	
		jsonObj.put("currentPlanAvailable", currentPlanAvailable);	
		jsonObj.put("currentPlanPremiumChange", currentPlanPremiumChange);	
		jsonObj.put("newPremium", newPremium);	
		jsonObj.put("isHealthDisEnrolled", isHealthDisEnrolled);	
		jsonObj.put("isDentalDisEnrolled", isDentalDisEnrolled);	
		jsonObj.put("terminationDate", terminationDate);	
		jsonObj.put("newHealthPremiumBeforeCredit", newHealthPremiumBeforeCredit);	
		jsonObj.put("newDentalPremiumBeforeCredit", newDentalPremiumBeforeCredit);	
		jsonObj.put("healthEnrollees", healthEnrollees);	
		jsonObj.put("dentalEnrollees", dentalEnrollees);	
		jsonObj.put("addedDependents", addedDependents);	
		jsonObj.put("removedDependents", removedDependents);	
		jsonObj.put("changePlan", changePlan);	
		jsonObj.put("applicationValidationStatus", applicationValidationStatus);	
		jsonObj.put("gatedEventLabels", gatedEventLabels);	
		jsonObj.put("meta_external_household_case_id", meta_external_household_case_id);
		jsonObj.put("coverageStartDate", coverageStartDate);
		jsonObj.put("planName", planName);
		jsonObj.put("memberNames", memberNames);
		
		return jsonObj.toJSONString();
	}
	
}
