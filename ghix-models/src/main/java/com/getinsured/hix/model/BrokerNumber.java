package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for generating the unique Broker Number for Broker
 * 
 */
@Entity
@Table(name = "BROKER_NUMBER")
public class BrokerNumber implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BROKER_NUMBER_SEQ")
	@SequenceGenerator(name = "BROKER_NUMBER_SEQ", sequenceName = "BROKER_NUMBER_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "BROKER_ID")
	private Integer brokerId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	@Override
	public String toString() {
		return "BrokerNumber details: ID = "+id+", BrokerId = "+brokerId;
	}
}
