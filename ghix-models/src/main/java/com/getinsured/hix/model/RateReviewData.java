package com.getinsured.hix.model;
import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="rate_review_data")

public class RateReviewData  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RateReviewData_Seq")
	@SequenceGenerator(name = "RateReviewData_Seq", sequenceName = "rate_review_data_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="plan_id")
	private Plan plan;
	
	@ManyToOne
    @JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@Column(name="rate_change_requested ", length=10)
	private String rateChangeRequested;
	
	@Temporal(value = TemporalType.DATE)	
	@Column(name="rate_change_submission_date")
	private Date rateChangeSubmissionDate;
	
	@Temporal(value = TemporalType.DATE)	
	@Column(name="rate_change_effective_date")
	private Date rateChangeEffectiveDate;
	
	@Column(name="rate_increase_percentage ", length=10)
	private String rateIncreasePercentage;
	
	/*@Column(name="justification_information", columnDefinition="blob")	
    private Blob justificationInformation;*/
	
	@Column(name="medical_loss_ratio", length=10)
	private String medicalLossRatio;
	
	@Column(name="rate_review_status", length=10)
	private String rateReviewStatus;
	
	@Column(name="serff_id", length=20)
	private String serffId;
	
	@Column(name="number_of_applications")
	private int numberOfApplications;
	
	@Column(name="number_of_denials")
	private int numberOfDenials;
	
	@Column(name="number_of_affected_insured")
	private int numberOfAffectedInsured;
	
	/*@Column(name="rate_increase_details")	
    private Blob rateIncreaseDetails;*/

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public String getRateChangeRequested() {
		return rateChangeRequested;
	}

	public void setRateChangeRequested(String rateChangeRequested) {
		this.rateChangeRequested = rateChangeRequested;
	}

	public Date getRateChangeSubmissionDate() {
		return rateChangeSubmissionDate;
	}

	public void setRateChangeSubmissionDate(Date rateChangeSubmissionDate) {
		this.rateChangeSubmissionDate = rateChangeSubmissionDate;
	}

	public Date getRateChangeEffectiveDate() {
		return rateChangeEffectiveDate;
	}

	public void setRateChangeEffectiveDate(Date rateChangeEffectiveDate) {
		this.rateChangeEffectiveDate = rateChangeEffectiveDate;
	}

	public String getRateIncreasePercentage() {
		return rateIncreasePercentage;
	}

	public void setRateIncreasePercentage(String rateIncreasePercentage) {
		this.rateIncreasePercentage = rateIncreasePercentage;
	}

	/*public Blob getJustificationInformation() {
		return justificationInformation;
	}

	public void setJustificationInformation(Blob justificationInformation) {
		this.justificationInformation = justificationInformation;
	}*/

	public String getMedicalLossRatio() {
		return medicalLossRatio;
	}

	public void setMedicalLossRatio(String medicalLossRatio) {
		this.medicalLossRatio = medicalLossRatio;
	}

	public String getRateReviewStatus() {
		return rateReviewStatus;
	}

	public void setRateReviewStatus(String rateReviewStatus) {
		this.rateReviewStatus = rateReviewStatus;
	}

	public String getSerffId() {
		return serffId;
	}

	public void setSerffId(String serffId) {
		this.serffId = serffId;
	}

	public int getNumberOfApplications() {
		return numberOfApplications;
	}

	public void setNumberOfApplications(int numberOfApplications) {
		this.numberOfApplications = numberOfApplications;
	}

	public int getNumberOfDenials() {
		return numberOfDenials;
	}

	public void setNumberOfDenials(int numberOfDenials) {
		this.numberOfDenials = numberOfDenials;
	}

	public int getNumberOfAffectedInsured() {
		return numberOfAffectedInsured;
	}

	public void setNumberOfAffectedInsured(int numberOfAffectedInsured) {
		this.numberOfAffectedInsured = numberOfAffectedInsured;
	}

	/*public Blob getRateIncreaseDetails() {
		return rateIncreaseDetails;
	}

	public void setRateIncreaseDetails(Blob rateIncreaseDetails) {
		this.rateIncreaseDetails = rateIncreaseDetails;
	}*/

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}
}	
