package com.getinsured.hix.model.phix.provider;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_product")
public class ProviderProduct  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer providerProductId;
	private ProviderPracticeStatusDomain providerPracticeStatusDom;
	private ProviderNamePhix providerNamePhix;
	private ProviderProductDomain providerProductDomain;
	private ProviderAddressPhix providerAddressPhix;
	private Integer siteProductGenerationId;
	private Integer strenuusId;
	private Integer strenuusLocationId;
	private String gender;
	private Integer npi;
	private String phone;
	private String degree;
	private String officeHours;
	private String webSite;
	private String addressConfidence;
	private Set<ProviderGroupAffil> providerGroupAffils = new HashSet<ProviderGroupAffil>(0);
	private Set<ProviderEducation> providerEducations = new HashSet<ProviderEducation>(0);
	private Set<ProviderSpecialty> providerSpecialties = new HashSet<ProviderSpecialty>(0);
	private Set<ProviderLanguage> providerLanguages = new HashSet<ProviderLanguage>(0);
	private Set<ProviderHospitalAffil> providerHospitalAffils = new HashSet<ProviderHospitalAffil>(0);
	private Set<ProviderIdentifier> providerIdentifiers = new HashSet<ProviderIdentifier>(0);
	private Set<ProviderNetworkMapping> providerNetworkMappings = new HashSet<ProviderNetworkMapping>(0);

	@Id     
	@Column(name="PROVIDER_PRODUCT_ID", unique=true, nullable=false)
	public Integer getProviderProductId() {
		return this.providerProductId;
	}

	public void setProviderProductId(Integer providerProductId) {
		this.providerProductId = providerProductId;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PRACITCE_STATUS_ID")
	public ProviderPracticeStatusDomain getProviderPracticeStatusDom() {
		return this.providerPracticeStatusDom;
	}

	public void setProviderPracticeStatusDom(ProviderPracticeStatusDomain providerPracticeStatusDom) {
		this.providerPracticeStatusDom = providerPracticeStatusDom;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="NAME_ID")
	public ProviderNamePhix getProviderNamePhix() {
		return this.providerNamePhix;
	}

	public void setProviderNamePhix(ProviderNamePhix providerNamePhix) {
		this.providerNamePhix = providerNamePhix;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PRODUCT_ID")
	public ProviderProductDomain getProviderProductDomain() {
		return this.providerProductDomain;
	}

	public void setProviderProductDomain(ProviderProductDomain providerProductDomain) {
		this.providerProductDomain = providerProductDomain;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ADDRESS_ID")
	public ProviderAddressPhix getProviderAddressPhix() {
		return this.providerAddressPhix;
	}

	public void setProviderAddressPhix(ProviderAddressPhix providerAddressPhix) {
		this.providerAddressPhix = providerAddressPhix;
	}


	@Column(name="SITE_PRODUCT_GENERATION_ID")
	public Integer getSiteProductGenerationId() {
		return this.siteProductGenerationId;
	}

	public void setSiteProductGenerationId(Integer siteProductGenerationId) {
		this.siteProductGenerationId = siteProductGenerationId;
	}


	@Column(name="STRENUUS_ID")
	public Integer getStrenuusId() {
		return this.strenuusId;
	}

	public void setStrenuusId(Integer strenuusId) {
		this.strenuusId = strenuusId;
	}


	@Column(name="STRENUUS_LOCATION_ID")
	public Integer getStrenuusLocationId() {
		return this.strenuusLocationId;
	}

	public void setStrenuusLocationId(Integer strenuusLocationId) {
		this.strenuusLocationId = strenuusLocationId;
	}


	@Column(name="GENDER", length=10)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	@Column(name="NPI")
	public Integer getNpi() {
		return this.npi;
	}

	public void setNpi(Integer npi) {
		this.npi = npi;
	}


	@Column(name="PHONE", length=50)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	@Column(name="DEGREE", length=100)
	public String getDegree() {
		return this.degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}


	@Column(name="OFFICE_HOURS", length=500)
	public String getOfficeHours() {
		return this.officeHours;
	}

	public void setOfficeHours(String officeHours) {
		this.officeHours = officeHours;
	}


	@Column(name="WEB_SITE", length=500)
	public String getWebSite() {
		return this.webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}


	@Column(name="ADDRESS_CONFIDENCE", length = 5)
	public String getAddressConfidence() {
		return this.addressConfidence;
	}

	public void setAddressConfidence(String addressConfidence) {
		this.addressConfidence = addressConfidence;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProduct")
	public Set<ProviderGroupAffil> getProviderGroupAffils() {
		return this.providerGroupAffils;
	}

	public void setProviderGroupAffils(Set<ProviderGroupAffil> providerGroupAffils) {
		this.providerGroupAffils = providerGroupAffils;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProduct")
	public Set<ProviderEducation> getProviderEducations() {
		return this.providerEducations;
	}

	public void setProviderEducations(Set<ProviderEducation> providerEducations) {
		this.providerEducations = providerEducations;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProduct")
	public Set<ProviderSpecialty> getProviderSpecialties() {
		return this.providerSpecialties;
	}

	public void setProviderSpecialties(Set<ProviderSpecialty> providerSpecialties) {
		this.providerSpecialties = providerSpecialties;
	}

	@OneToMany(fetch=FetchType.EAGER, mappedBy="providerProduct")
	public Set<ProviderLanguage> getProviderLanguages() {
		return this.providerLanguages;
	}

	public void setProviderLanguages(Set<ProviderLanguage> providerLanguages) {
		this.providerLanguages = providerLanguages;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProduct")
	public Set<ProviderHospitalAffil> getProviderHospitalAffils() {
		return this.providerHospitalAffils;
	}

	public void setProviderHospitalAffils(Set<ProviderHospitalAffil> providerHospitalAffils) {
		this.providerHospitalAffils = providerHospitalAffils;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProduct")
	public Set<ProviderIdentifier> getProviderIdentifiers() {
		return this.providerIdentifiers;
	}

	public void setProviderIdentifiers(Set<ProviderIdentifier> providerIdentifiers) {
		this.providerIdentifiers = providerIdentifiers;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerProduct")
	public Set<ProviderNetworkMapping> getProviderNetworkMappings() {
		return this.providerNetworkMappings;
	}

	public void setProviderNetworkMappings(Set<ProviderNetworkMapping> providerNetworkMappings) {
		this.providerNetworkMappings = providerNetworkMappings;
	}




}


