package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class ApplicantExternalEnrollmentResponse  extends GHIXResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	
	private String memberId ;
	private List<ExternalEnrollmentDTO> enrollments;
	
	public String getApplicantId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public List<ExternalEnrollmentDTO> getEnrollments() {
		return enrollments;
	}
	public void setEnrollments(List<ExternalEnrollmentDTO> enrollments) {
		this.enrollments = enrollments;
	}
	
	
	
		
}
