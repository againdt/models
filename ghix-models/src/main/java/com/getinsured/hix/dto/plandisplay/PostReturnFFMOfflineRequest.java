/**
 * Created: Oct 16, 2014
 */
package com.getinsured.hix.dto.plandisplay;

/**
 * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
 * POJO to store response from FFM. To be used in offline mode,
 */
public class PostReturnFFMOfflineRequest {
	
	
	private String samlResponse ;
	private Long  ssapApplicationId ;
	private Integer agentUserId;
	
	public String getSamlResponse() {
		return samlResponse;
	}
	public void setSamlResponse(String samlResponse) {
		this.samlResponse = samlResponse;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public Integer getAgentUserId() {
		return agentUserId;
	}
	public void setAgentUserId(Integer agentUserId) {
		this.agentUserId = agentUserId;
	}
	
	
	
	
	

}
