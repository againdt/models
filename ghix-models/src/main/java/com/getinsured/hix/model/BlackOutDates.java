package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

@Entity
@Table(name="BLACKOUT_DATES")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class BlackOutDates implements Serializable {

	private static final long serialVersionUID = -7931938613832018443L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLACKOUT_DATES_SEQ")
	@SequenceGenerator(name = "BLACKOUT_DATES_SEQ", sequenceName = "BLACKOUT_DATES_SEQ", allocationSize = 1)
	@Column(name="ID")
	private Integer id;
 
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="BLACKOUT_DATES_TIMESTAMP")
	private Date blackOutDate;
   
    @Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updated;
	
	@Column(name="TENANT_ID")
    private Long tenantId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Long getTenantId() {
		return tenantId;
	}
	
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	public void setBlackOutDate(Date blackOutDate) {
		this.blackOutDate = blackOutDate;
	}
	
	public Date getBlackOutDate() {
		return blackOutDate;
	}
	
    @PrePersist
	public void prePersist() {
		if (null == this.created) {
			this.setCreated(new TSDate());
		}
		
		this.setUpdated(new TSDate());
		if (TenantContextHolder.getTenant() != null	&& TenantContextHolder.getTenant().getId() != null) {
			setTenantId(TenantContextHolder.getTenant().getId());
		}
	}

}
