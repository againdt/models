package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityLocation;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.getinsured.hix.model.AccountUser;

/**
 * Used to wrap data fetched from database in ghix-entity and send it to
 * ghix-web
 * 
 */
public class EnrollmentEntityAssisterSearchResult implements Serializable {
	private static final long serialVersionUID = 1L;

	private EnrollmentEntity enrollmentEntity;
	private Site site;
	private EntityLocation entityLocation;
	private AccountUser user;
	private SiteLanguages siteLanguages;
	private SiteLocationHours siteLocationHours;
	private List<SiteLocationHours> listOfSiteLocationHours;
	private double distance;

	public List<SiteLocationHours> getListOfSiteLocationHours() {
		return listOfSiteLocationHours;
	}

	public void setListOfSiteLocationHours(List<SiteLocationHours> listOfSiteLocationHours) {
		this.listOfSiteLocationHours = listOfSiteLocationHours;
	}

	public SiteLocationHours getSiteLocationHours() {
		return siteLocationHours;
	}

	public void setSiteLocationHours(SiteLocationHours siteLocationHours) {
		this.siteLocationHours = siteLocationHours;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public EnrollmentEntity getEnrollmentEntity() {
		return enrollmentEntity;
	}

	public void setEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		this.enrollmentEntity = enrollmentEntity;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public EntityLocation getEntityLocation() {
		return entityLocation;
	}

	public void setEntityLocation(EntityLocation entityLocation) {
		this.entityLocation = entityLocation;
	}

	public SiteLanguages getSiteLanguages() {
		return siteLanguages;
	}

	public void setSiteLanguages(SiteLanguages siteLanguages) {
		this.siteLanguages = siteLanguages;
	}

	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "EnrollmentEntityAssisterSearchResult details: EnrollmentEntity = ["+enrollmentEntity+"], UserID = "+(user != null ? user.getId(): "");
	}
}
