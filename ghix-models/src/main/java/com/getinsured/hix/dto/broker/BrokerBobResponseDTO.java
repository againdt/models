package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;


public class BrokerBobResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private int count;
	private List<BrokerBOBDetailsDTO> brokerBobDetailsDTO;
	List<String> QHPIssuerNames;
	List<String> QDPIssuerNames;
	List<String> bothQHPAndQDPIssuerNames;
	private Integer responseCode;
    private String responseDesc;
    private HouseholdEligibilityInformationDTO householdEligibilityInformationDTO;
	
	private List<Long> individualsId;
	
	
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDesc() {
		return responseDesc;
	}
	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
	public List<String> getQHPIssuerNames() {
		return QHPIssuerNames;
	}
	public void setQHPIssuerNames(List<String> qHPIssuerNames) {
		QHPIssuerNames = qHPIssuerNames;
	}
	public List<String> getQDPIssuerNames() {
		return QDPIssuerNames;
	}
	public void setQDPIssuerNames(List<String> qDPIssuerNames) {
		QDPIssuerNames = qDPIssuerNames;
	}
	public List<String> getBothQHPAndQDPIssuerNames() {
		return bothQHPAndQDPIssuerNames;
	}
	public void setBothQHPAndQDPIssuerNames(List<String> bothQHPAndQDPIssuerNames) {
		this.bothQHPAndQDPIssuerNames = bothQHPAndQDPIssuerNames;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<BrokerBOBDetailsDTO> getBrokerBobDetailsDTO() {
		return brokerBobDetailsDTO;
	}
	public void setBrokerBobDetailsDTO(List<BrokerBOBDetailsDTO> brokerBobDetailsDTO) {
		this.brokerBobDetailsDTO = brokerBobDetailsDTO;
	}
	public List<Long> getIndividualsId() {
		return individualsId;
	}
	public void setIndividualsId(List<Long> individualsId) {
		this.individualsId = individualsId;
	}
	
	public HouseholdEligibilityInformationDTO getHouseholdEligibilityInformationDTO() {
		return householdEligibilityInformationDTO;
	}
	public void setHouseholdEligibilityInformationDTO(
			HouseholdEligibilityInformationDTO householdEligibilityInformationDTO) {
		this.householdEligibilityInformationDTO = householdEligibilityInformationDTO;
	}

}
