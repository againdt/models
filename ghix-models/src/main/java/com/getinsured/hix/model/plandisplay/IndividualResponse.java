package com.getinsured.hix.model.plandisplay;

public class IndividualResponse {
   
	protected String giHouseholdId;
   
    protected String responseCode;
   
    public String getGiHouseholdId() {
		return giHouseholdId;
	}

	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	protected String responseDescription;
}
