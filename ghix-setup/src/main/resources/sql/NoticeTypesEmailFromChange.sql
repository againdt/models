update notice_types set EMAIL_FROM ='noreply@ms-marketplace.com'
where template_location in ('notificationTemplate/prelaunchSignupConfirmationEmail.html',
'notificationTemplate/EmployerPassedDueDateNotice.html', 
'notificationTemplate/TestNoticeTemplate.html',
'notificationTemplate/nmBrokerCertificationNotification.html',
'notificationTemplate/EmployerPassedEndDueDateNotice.html', 
'notificationTemplate/EmployerPassedMiddleDueDateNotice.html',
'notificationTemplate/EmployerInvoiceReadyNotification.html',
'notificationTemplate/EmployerInvoices.html ',
'notificationTemplate/EmployerBinderInvoiceDueNotification.html',
'notificationTemplate/EmployerPaymentFailureNotice.html', 
'notificationTemplate/EmployerBinderInvoiceReadyNotification.html',
'notificationTemplate/BatchMonitoringNotify.html', 
'notificationTemplate/EnrollmentBatchFailureEmailNotification.html',
'notificationTemplate/EnrollmentStatusCancelTermNotification.html',
'notificationTemplate/EmployerAccountActivationEmail.html');