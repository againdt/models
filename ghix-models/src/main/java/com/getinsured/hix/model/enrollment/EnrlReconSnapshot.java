package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name="ENRL_RECON_SNAPSHOT")
public class EnrlReconSnapshot  implements Serializable, SortableEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_snapshot_seq")
	@SequenceGenerator(name = "enrl_recon_snapshot_seq", sequenceName = "ENRL_RECON_SNAPSHOT_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="FILE_ID")
	private Integer fileId;
	
	@Column(name="HIX_ENROLLMENT_ID")
	private Long hixEnrollmentId;
	
	@Column(name="HIX_SUBSCRIBER_ID")
	private Long hixSubscriberId;
	
	@Column(name="ISSUER_ASSIGNED_POLICY_ID")
	private String issuerAssignedPolicyId;
	
	@Column(name="HIX_DATA")
	private String hixData;
	
	@Column(name="ISSUER_DATA")
	private String issuerData;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;

	@Column(name="COMPARISON_STATUS")
	private String comparisonStatus;
	
	@Column(name="ERROR_MESSAGE")
	private String errorMessage;
	
	@Column(name="COVERAGE_YEAR")
	private String coverageYear;
	
	@Column(name="SUBSCRIBER_NAME")
	private String subscriberName;
	
	@Column(name="BENEFIT_EFFECTIVE_DATE")
	private Date benefitEffectiveDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Long getHixEnrollmentId() {
		return hixEnrollmentId;
	}

	public void setHixEnrollmentId(Long hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}

	public Long getHixSubscriberId() {
		return hixSubscriberId;
	}

	public void setHixSubscriberId(Long hixSubscriberId) {
		this.hixSubscriberId = hixSubscriberId;
	}

	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}

	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}

	public String getHixData() {
		return hixData;
	}

	public void setHixData(String hixData) {
		this.hixData = hixData;
	}

	public String getIssuerData() {
		return issuerData;
	}

	public void setIssuerData(String issuerData) {
		this.issuerData = issuerData;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
	}

	public String getComparisonStatus() {
		return comparisonStatus;
	}

	public void setComparisonStatus(String comparisonStatus) {
		this.comparisonStatus = comparisonStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getSubscriberName() {
		return subscriberName;
	}

	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
        columnNames.add("id");
        columnNames.add("hixEnrollmentId");
        columnNames.add("issuerAssignedPolicyId");
        columnNames.add("hixSubscriberId");
        columnNames.add("subscriberName");
        columnNames.add("benefitEffectiveDate");
        return columnNames;
	}
}
