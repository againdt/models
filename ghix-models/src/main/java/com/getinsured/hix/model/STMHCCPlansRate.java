/**
 * 
 */
package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * POJO class for STM HCC Plan rates
 * @author sharma_va
 *
 */

@Audited
@Entity
@Table(name = "PM_STM_HCC_RATE")
public class STMHCCPlansRate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STM_HCC_RATE_SEQ")
	@SequenceGenerator(name = "STM_HCC_RATE_SEQ", sequenceName = "STM_HCC_RATE_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "state")
	private String state;
	
	@Column(name = "plan_id")
	private Integer planId;
	
	@Column(name = "min_age")
	private Integer minAge;
	
	@Column(name = "max_age")
	private Integer maxAge;
	
	@Column(name = "male_rate")
	private Double maleRate;
	
	@Column(name = "female_rate")
	private Double femaleRate;
	
	@Column(name = "child_rate")
	private Double childRate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp" , nullable = false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp" , nullable = false)
	private Date lastUpdateTimestamp;
	
	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	private Date startDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;
	
	public STMHCCPlansRate(){
		
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public Double getMaleRate() {
		return maleRate;
	}

	public void setMaleRate(Double maleRate) {
		this.maleRate = maleRate;
	}

	public Double getFemaleRate() {
		return femaleRate;
	}

	public void setFemaleRate(Double femaleRate) {
		this.femaleRate = femaleRate;
	}

	public Double getChildRate() {
		return childRate;
	}

	public void setChildRate(Double childRate) {
		this.childRate = childRate;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
