package com.getinsured.hix.model.emx;

import com.getinsured.hix.model.Location;

public class EmxEmployerDTO{
	private String userName;
	private String password;
	private String name;
	private int id;
	private String contactFirstName;
	private String contactLastName;
	private String contactEmail;
	private String contactNumber;
	private Location contactLocation;
	private Integer createdBy;
    private Integer updatedBy;
	private Long affiliateFlowAccessId;
	private Long affiliateId;
	private Integer affiliateFlowId;
	private Long employeeCount;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Location getContactLocation() {
		return contactLocation;
	}

	public void setContactLocation(Location contactLocation) {
		this.contactLocation = contactLocation;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getAffiliateFlowAccessId() {
		return affiliateFlowAccessId;
	}

	public void setAffiliateFlowAccessId(Long affiliateFlowAccessId) {
		this.affiliateFlowAccessId = affiliateFlowAccessId;
	}

	public Long getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(Long employeeCount) {
		this.employeeCount = employeeCount;
	}

	public Long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}
	
}
