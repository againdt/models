package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerPaymentInfoDTO extends AuditDTO {

	private Integer id;
	private String keystoreFileLocation;
	private String privateKeyName;
	private String password;
	private String passwordSecuredKey;
	private String securityDnsName;
	private String securityAddress;
	private String securityKeyInfo;
	private String issuerAuthUrl;
	private String hiosId;
	private String securityCertName;

	public IssuerPaymentInfoDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKeystoreFileLocation() {
		return keystoreFileLocation;
	}

	public void setKeystoreFileLocation(String keystoreFileLocation) {
		this.keystoreFileLocation = keystoreFileLocation;
	}

	public String getPrivateKeyName() {
		return privateKeyName;
	}

	public void setPrivateKeyName(String privateKeyName) {
		this.privateKeyName = privateKeyName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordSecuredKey() {
		return passwordSecuredKey;
	}

	public void setPasswordSecuredKey(String passwordSecuredKey) {
		this.passwordSecuredKey = passwordSecuredKey;
	}

	public String getSecurityDnsName() {
		return securityDnsName;
	}

	public void setSecurityDnsName(String securityDnsName) {
		this.securityDnsName = securityDnsName;
	}

	public String getSecurityAddress() {
		return securityAddress;
	}

	public void setSecurityAddress(String securityAddress) {
		this.securityAddress = securityAddress;
	}

	public String getSecurityKeyInfo() {
		return securityKeyInfo;
	}

	public void setSecurityKeyInfo(String securityKeyInfo) {
		this.securityKeyInfo = securityKeyInfo;
	}

	public String getIssuerAuthUrl() {
		return issuerAuthUrl;
	}

	public void setIssuerAuthUrl(String issuerAuthUrl) {
		this.issuerAuthUrl = issuerAuthUrl;
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getSecurityCertName() {
		return securityCertName;
	}

	public void setSecurityCertName(String securityCertName) {
		this.securityCertName = securityCertName;
	}
}
