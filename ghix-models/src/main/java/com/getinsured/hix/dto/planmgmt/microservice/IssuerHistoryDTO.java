package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerHistoryDTO {

	private String fieldUpdated;
	private String newValue;
	private String commentId;
	private String documentId;
	private String lastUpdatedBy;
	private String lastUpdatedOn;
	private String companyLogo;

	public IssuerHistoryDTO() {
		super();
	}

	public String getFieldUpdated() {
		return fieldUpdated;
	}

	public void setFieldUpdated(String fieldUpdated) {
		this.fieldUpdated = fieldUpdated;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(String lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
}
