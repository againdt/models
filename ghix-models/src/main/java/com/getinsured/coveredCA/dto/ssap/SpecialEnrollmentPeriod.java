
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class SpecialEnrollmentPeriod {

	
	@Expose
	private String qualifiedLifeEventsReason;
	@Expose 
	private String reasonForOther;
	@Expose
	private String reasonForOther1;
	@Expose
	private String qualifiedForSpecialEnrollment = "Yes, this household qualifies for Special Enrollment";
	@Expose
	private String coverageDateCategory;
	@Expose
	private String qualifiedEventDate;
	@Expose
	private String ExpirationDate;
	
	
	
	
	public String getQualifiedLifeEventsReason() {
		return qualifiedLifeEventsReason;
	}
	public void setQualifiedLifeEventsReason(String qualifiedLifeEventsReason) {
		this.qualifiedLifeEventsReason = qualifiedLifeEventsReason;
	}
	public String getReasonForOther() {
		return reasonForOther;
	}
	public void setReasonForOther(String reasonForOther) {
		this.reasonForOther = reasonForOther;
	}
	public String getReasonForOther1() {
		return reasonForOther1;
	}
	public void setReasonForOther1(String reasonForOther1) {
		this.reasonForOther1 = reasonForOther1;
	}
	public String getQualifiedForSpecialEnrollment() {
		return qualifiedForSpecialEnrollment;
	}
	public void setQualifiedForSpecialEnrollment(
			String qualifiedForSpecialEnrollment) {
		this.qualifiedForSpecialEnrollment = qualifiedForSpecialEnrollment;
	}
	public String getCoverageDateCategory() {
		return coverageDateCategory;
	}
	public void setCoverageDateCategory(String coverageDateCategory) {
		this.coverageDateCategory = coverageDateCategory;
	}
	public String getQualifiedEventDate() {
		return qualifiedEventDate;
	}
	public void setQualifiedEventDate(String qualifiedEventDate) {
		this.qualifiedEventDate = qualifiedEventDate;
	}
	public String getExpirationDate() {
		return ExpirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		ExpirationDate = expirationDate;
	}
	
	
	
   
    
   
    
   

    
}
