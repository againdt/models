package com.getinsured.hix.cap.drx;

public class  ArrayOfSessionRequest {
	
	private Session Session;

	public Session getSession() {
		return Session;
	}

	public void setSession(Session session) {
		this.Session = session;
	}

	public static class Session {
		private String Birthdate;
		private Profile Profile;

		public String getBirthdate() {
			return Birthdate;
		}

		public void setBirthdate(String birthdate) {
			Birthdate = birthdate;
		}

		public Profile getProfile() {
			return Profile;
		}

		public void setProfile(Profile profile) {
			this.Profile = profile;
		}

	public	static class Profile {
			private String Birthdate;

			public String getBirthdate() {
				return Birthdate;
			}

			public void setBirthdate(String birthdate) {
				Birthdate = birthdate;
			}

			public String getFirstName() {
				return FirstName;
			}

			public void setFirstName(String firstName) {
				FirstName = firstName;
			}

			public String getLastName() {
				return LastName;
			}

			public void setLastName(String lastName) {
				LastName = lastName;
			}

			private String FirstName;
			private String LastName;
			private String WorkPhone;
			private String CellPhone;
			private String HomePhone;

			public String getWorkPhone() {
				return WorkPhone;
			}

			public void setWorkPhone(String workPhone) {
				WorkPhone = workPhone;
			}

			public String getCellPhone() {
				return CellPhone;
			}

			public void setCellPhone(String cellPhone) {
				CellPhone = cellPhone;
			}

			public String getHomePhone() {
				return HomePhone;
			}

			public void setHomePhone(String homePhone) {
				HomePhone = homePhone;
			}
			
			
		}
	}
}