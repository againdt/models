package com.getinsured.iex.request.ssap;

public class SsapApplicationResponse {
	
	private int count;
	
	private String errorCode;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
