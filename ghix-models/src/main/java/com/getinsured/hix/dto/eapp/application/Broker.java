package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.util.Map;



/**
 * Broker is an entity submitting 
 * an insurance quote to carriers. 
 * 
 * In case of multi-tenant environment, this value would
 * depend on the consumer of this service. 
 * @author root
 *
 */
public class Broker implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private Address address;
	private String npn;
	private String userId;
	private PhoneNumber phoneNumber;
	private PhoneNumber faxNumber;
	private String emailAddress;
	private Map<String, Object> other;
	
	public Map<String, Object> getOther() {
		return other;
	}
	public void setOther(Map<String, Object> other) {
		this.other = other;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getNpn() {
		return npn;
	}
	public void setNpn(String npn) {
		this.npn = npn;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public PhoneNumber getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(PhoneNumber phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public PhoneNumber getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(PhoneNumber faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public static Broker getSampleInstance(){
		Broker broker = new Broker();
		broker.setEmailAddress("support@getinsured.com");
		broker.setId("A001" + (int)(1908*Math.random()));
		broker.setName("Vimo");
		broker.setNpn("100NPN");
		broker.setUserId("VIM001");
		broker.setPhoneNumber(PhoneNumber.getSampleInstance());
		broker.setFaxNumber(PhoneNumber.getSampleInstance());
		broker.setName("Humana Arizona");
		broker.setAddress(Address.getSampleInstance());
		return broker;
	}
	
}
