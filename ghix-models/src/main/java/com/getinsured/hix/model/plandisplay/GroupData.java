package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;

/**
 * 
 * @author Krishna_prasad
 *
 */

public class GroupData implements Serializable 
{
	
	private static final long serialVersionUID = 1L;
	private Integer groupId;
	private Float aptc;
	private BigDecimal stateSubsidy;
	private BigDecimal remainingStateSubsidy;
	private Float remainingAptc;
	private String csr;
	private String zipcode;
	private String countycode;	
	private Float employeeContribution;
	private Float dependentContribution;
	private List<PersonData> personDataList;
	private Map<String,Object> preferences;
	private List<PrescriptionSearchRequest> prescriptionRequestList;
	
	public static GroupData clone(GroupData groupData)
	{
		GroupData groupDataCopy = new GroupData();
		groupDataCopy.setGroupId(groupData.getGroupId());
		groupDataCopy.setAptc(groupData.getAptc());
		groupDataCopy.setRemainingAptc(groupData.getRemainingAptc());
		groupDataCopy.setCsr(groupData.getCsr());
		groupDataCopy.setZipcode(groupData.getZipcode());
		groupDataCopy.setCountycode(groupData.getCountycode());
		groupDataCopy.setEmployeeContribution(groupData.getEmployeeContribution());
		groupDataCopy.setDependentContribution(groupData.getDependentContribution());
		groupDataCopy.setPersonDataList(PersonData.clone(groupData.getPersonDataList()));
		groupDataCopy.setPreferences(groupData.getPreferences());
		groupDataCopy.setPrescriptionRequestList(groupData.getPrescriptionRequestList());
		groupDataCopy.setStateSubsidy(groupData.getStateSubsidy());
		groupDataCopy.setRemainingStateSubsidy(groupData.getRemainingStateSubsidy());
		
		return groupDataCopy;
	}
	
	
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public Float getAptc() {
		return aptc;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public Float getRemainingAptc() {
		return remainingAptc;
	}
	public void setRemainingAptc(Float remainingAptc) {
		this.remainingAptc = remainingAptc;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountycode() {
		return countycode;
	}
	public void setCountycode(String countycode) {
		this.countycode = countycode;
	}	
	public Float getEmployeeContribution() {
		return employeeContribution;
	}
	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}
	public Float getDependentContribution() {
		return dependentContribution;
	}
	public void setDependentContribution(Float dependentContribution) {
		this.dependentContribution = dependentContribution;
	}
	public List<PersonData> getPersonDataList() {
		return personDataList;
	}
	public void setPersonDataList(List<PersonData> personDataList) {
		this.personDataList = personDataList;
	}
	public Map<String,Object> getPreferences() {
		return preferences;
	}
	public void setPreferences(Map<String,Object> preferences) {
		this.preferences = preferences;
	}


	public List<PrescriptionSearchRequest> getPrescriptionRequestList() {
		return prescriptionRequestList;
	}

	public BigDecimal getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(BigDecimal stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}

	public BigDecimal getRemainingStateSubsidy() {
		return remainingStateSubsidy;
	}

	public void setRemainingStateSubsidy(BigDecimal remainingStateSubsidy) {
		this.remainingStateSubsidy = remainingStateSubsidy;
	}

	public void setPrescriptionRequestList(
			List<PrescriptionSearchRequest> prescriptionRequestList) {
		this.prescriptionRequestList = prescriptionRequestList;
	}


	
}
