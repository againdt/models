package com.getinsured.hix.dto.eapp;

import java.util.HashMap;
import java.util.Map;

/**
 * Possible set of documents/configs
 * 
 * @author root
 *
 */
public enum DocumentTypeEnum {
	UI_TEMPLATE("uitemplate"), 
	PDF_MAP("pdfmap"), 
	XML_MAP("xmlmap"), 
	XML_FUNC("xmlfunc"), 
	WORKFLOW("workflow"),
	PDF("carrierpdf"), 
	DISCLAIMER("disclaimer"), 
	PLAN_DATA("PLAN_DATA"),
	OTHER("other");

	private static final Map<String, DocumentTypeEnum> DocTypeEnumMap = new HashMap<String, DocumentTypeEnum>();

	static {
		for (DocumentTypeEnum type : DocumentTypeEnum.values()) {
			DocTypeEnumMap.put(type.getDocumentType(), type);
		}
	}

	private String documentType;

	private DocumentTypeEnum(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public static DocumentTypeEnum getEnumFromValue(String in) {
		DocumentTypeEnum type = DocTypeEnumMap.get(in);
		if (type == null)
			return DocumentTypeEnum.OTHER;
		return type;
	}

}
