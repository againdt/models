package com.getinsured.hix.dto.planmgmt.microservice;

public class SerffBatchSearchRequestDTO extends SearchDTO {

	private String hiosIssuerID;
	private String uploadDate;
	private String status;
	private boolean includeRequestXml;

	public SerffBatchSearchRequestDTO() {
		super();
		includeRequestXml = false;
	}

	public String getHiosIssuerID() {
		return hiosIssuerID;
	}

	public void setHiosIssuerID(String hiosIssuerID) {
		this.hiosIssuerID = hiosIssuerID;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean getIncludeRequestXml() {
		return includeRequestXml;
	}

	public void setIncludeRequestXml(boolean includeRequestXml) {
		this.includeRequestXml = includeRequestXml;
	}
}
