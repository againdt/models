package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="FEED_EXCEPTION_MATRIX")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class FeedExceptionMatrix {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FEED_EXCEPTION_MATRIX_SEQ")
	@SequenceGenerator(name = "FEED_EXCEPTION_MATRIX_SEQ", sequenceName = "FEED_EXCEPTION_MATRIX_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="CARRIER_STATUS_LKP_ID")
	private Integer carrierStatusLkpId;
	
	@Column(name="GI_STATUS_LKP_ID")
	private Integer giStatusLkpId;
	
	@Column(name = "EXCEPTION_CODE")
	private String exceptionCode;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Integer getCarrierStatusLkpId() {
		return carrierStatusLkpId;
	}

	public void setCarrierStatusLkpId(Integer carrierStatusLkpId) {
		this.carrierStatusLkpId = carrierStatusLkpId;
	}

	public Integer getGiStatusLkpId() {
		return giStatusLkpId;
	}

	public void setGiStatusLkpId(Integer giStatusLkpId) {
		this.giStatusLkpId = giStatusLkpId;
	}

	public String getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
}
