package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * -----------------------------------------------------------------------------
 * QhpReportDTO class is used to populates Enrollments data by Coverage Year
 * -----------------------------------------------------------------------------
 * 
 * @since May 23, 2019
 */
public class QhpReportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id; // ID that uniquely identifies QHP Report data
	private String externalCaseId; // The External(AHBX) case ID that is tied to an AT and the enrollment.
	private String hiosPlanId; // The 16 alphanumeric plan ID
	private String planType; // Plan Type (Possible values: HEALTH, DENTAL)
	private Integer yearOfEnrollment; // The Enrollment Year before Renewal Year
	private Integer enrollmentId; // ID that uniquely identifies an enrollment in the system
	private String enrollmentStatus; // Enrollment Status (Possible values: CONFIRM/CANCEL/TERM/PENDING/ABORTED)
	private String planName; // Carrier plan name
	private String planLevel; // Plan Level (Possible values: PLATINUM, GOLD, SILVER, BRONZE, EXPANDEDBRONZE, CATASTROPHIC)
	private String issuerName; // Carrier name
	private Integer brokerAssisterId; // Broker/Assister ID
	private String delegationType; // Delegation Type (Possible values: AGENT(Broker)/ASSISTER Role)
	private String memberId; // External Member ID
	private Date memberBenefitStartDate; // Benefit start date of member
	private Date memberBenefitEndDate; // Benefit end date of member
	private String memberStatus; // Status of the Member(Enrollee).
	private Boolean subscriberFlag = Boolean.FALSE; // Subscriber flag (Possible values: TRUE/FALSE)
	private Integer batchJobExecutionId; // Store ID from BATCH_JOB_EXECUTION table for QHP Report JOB

	public QhpReportDTO() {
	}

	public QhpReportDTO(String externalCaseId, String hiosPlanId, String planType, Integer yearOfEnrollment,
			Integer enrollmentId, String enrollmentStatus, String planName, String planLevel, String issuerName,
			Integer brokerAssisterId, String delegationType, String memberId, Date memberBenefitStartDate,
			Date memberBenefitEndDate, String memberStatus, String subscriberFlag) {
		this.externalCaseId = externalCaseId;
		this.hiosPlanId = hiosPlanId;
		this.planType = planType;
		this.yearOfEnrollment = yearOfEnrollment;
		this.enrollmentId = enrollmentId;
		this.enrollmentStatus = enrollmentStatus;
		this.planName = planName;
		this.planLevel = planLevel;
		this.issuerName = issuerName;
		this.brokerAssisterId = brokerAssisterId;
		this.delegationType = delegationType;
		this.memberId = memberId;
		this.memberBenefitStartDate = memberBenefitStartDate;
		this.memberBenefitEndDate = memberBenefitEndDate;
		this.memberStatus = memberStatus;

		if ("SUBSCRIBER".equalsIgnoreCase(subscriberFlag)) {
			this.subscriberFlag = Boolean.TRUE;
		}
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExternalCaseId() {
		return externalCaseId;
	}

	public void setExternalCaseId(String externalCaseId) {
		this.externalCaseId = externalCaseId;
	}

	public String getHiosPlanId() {
		return hiosPlanId;
	}

	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public Integer getYearOfEnrollment() {
		return yearOfEnrollment;
	}

	public void setYearOfEnrollment(Integer yearOfEnrollment) {
		this.yearOfEnrollment = yearOfEnrollment;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public Integer getBrokerAssisterId() {
		return brokerAssisterId;
	}

	public void setBrokerAssisterId(Integer brokerAssisterId) {
		this.brokerAssisterId = brokerAssisterId;
	}

	public String getDelegationType() {
		return delegationType;
	}

	public void setDelegationType(String delegationType) {
		this.delegationType = delegationType;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getMemberBenefitStartDate() {
		return memberBenefitStartDate;
	}

	public void setMemberBenefitStartDate(Date memberBenefitStartDate) {
		this.memberBenefitStartDate = memberBenefitStartDate;
	}

	public Date getMemberBenefitEndDate() {
		return memberBenefitEndDate;
	}

	public void setMemberBenefitEndDate(Date memberBenefitEndDate) {
		this.memberBenefitEndDate = memberBenefitEndDate;
	}

	public String getMemberStatus() {
		return memberStatus;
	}

	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}

	public Boolean getSubscriberFlag() {
		return subscriberFlag;
	}

	public void setSubscriberFlag(Boolean subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}

	public Integer getBatchJobExecutionId() {
		return batchJobExecutionId;
	}

	public void setBatchJobExecutionId(Integer batchJobExecutionId) {
		this.batchJobExecutionId = batchJobExecutionId;
	}
}
