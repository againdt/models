CREATE OR REPLACE PROCEDURE ENABLE_PROVIDER_CON
IS
	err_msg VARCHAR2(250);
	tmp_table_name VARCHAR2(100);
  	tmp_constraint_name VARCHAR2(100);
  	
BEGIN

for cur in 
    (
    select 
      constraint_name, 
      table_name 
    from user_constraints
    where  
          constraint_type='R' and
          (
             (TABLE_NAME = 'PROVIDER_LANGUAGE_DOMAIN') or
             (TABLE_NAME = 'PROVIDER_PRODUCT_DOMAIN') or
             (TABLE_NAME = 'PROVIDER_SPECIALTY_DOMAIN') or
          	 (TABLE_NAME = 'PROVIDER_BOARD_CERT_DOMAIN') or
             (TABLE_NAME = 'PROVIDER_TYPE_DOMAIN') or
             (TABLE_NAME = 'PROVIDER_PRACTICE_STATUS_DOM') or
             (TABLE_NAME = 'PROVIDER_ADDRESS_PHIX')or
             (TABLE_NAME = 'PROVIDER_NAME_PHIX') or
             (TABLE_NAME = 'PROVIDER_PRODUCT') or
             (TABLE_NAME = 'PROVIDER_HOSPITAL_AFFIL') or
             (TABLE_NAME = 'PROVIDER_EDUCATION') or
             (TABLE_NAME = 'PROVIDER_IDENTIFIER') or
             (TABLE_NAME = 'PROVIDER_GROUP_AFFIL') or
             (TABLE_NAME = 'PROVIDER_LANGUAGE') or
             (TABLE_NAME = 'PROVIDER_SPECIALTY') or
             (TABLE_NAME = 'PROVIDER_MAPPING') or
             --(TABLE_NAME = 'PROVIDER_MAPPING') or
             (TABLE_NAME = 'PROVIDER_SPECIALTY_SCORE')
             
          )
      ) loop
      
      tmp_table_name := cur.table_name;
      tmp_constraint_name := cur.constraint_name;
      --DBMS_OUTPUT.PUT_LINE( 'Eisabling Constarints for: ' ||tmp_table_name || ', Constarint name: ' || tmp_constraint_name);
      
      execute immediate 'ALTER TABLE '||cur.table_name||' enable novalidate  constraint '||cur.constraint_name;
  end loop;
  
EXCEPTION
  WHEN OTHERS THEN
    err_msg := 'Exception while enabling Constarints for: ' ||tmp_table_name || ', Constarint name: ' || tmp_constraint_name ||', '|| SQLERRM;
    DBMS_OUTPUT.PUT_LINE(err_msg);
  
END;
