-- SQL Script to update new TEMPLATE_LOCATION and set TERMINATION_DATE as OPEN_ENDED date for all templates 
update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/PasswordRecoveryEmail.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='PasswordRecoveryEmail';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/AccountActivationEmail.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='AccountActivationEmail';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/TestNoticeTemplate.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='TestNoticeTemplate';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/BrokerConfirmationEmail.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='BrokerConfirmationEmail';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/BrokerProfileUpdateEmail.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='BrokerProfileUpdateEmail';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/EmployerConfirmationEmail.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='EmployerConfirmationEmail';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/IssuerConfirmationEmail.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='IssuerConfirmationEmail';

update NOTICE_TYPES
set TEMPLATE_LOCATION = '/resources/notificationTemplate/EmployerPassedDueDateNotice.html'
, TERMINATION_DATE = to_date('31-MAR-2099','DD-MON-RR')
where EMAIL_CLASS='EmployerPassedDueDateNotice';
