package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.platform.util.DateUtil;

public class EnrollmentCSVDto implements Serializable{
	
	private String enrollmentStatus;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private Float aptc;
	private Integer enrollmentIDPhix;
	private String capAgentNPN;
	private String emailAddress;
	private String insurerName;
	
	private StringBuilder errorMessage;
	private StringBuilder errorCSV;
	
	public static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	
	public EnrollmentCSVDto(String enrollmentStatus, Date effectiveStartDate, Date effectiveEndDate, Float aptc,
			Integer enrollmentIDPhix, String capAgentNPN, String emailAddress, String insurerName) {
		super();
		this.enrollmentStatus = enrollmentStatus;
		this.effectiveStartDate = effectiveStartDate;
		this.effectiveEndDate = effectiveEndDate;
		this.aptc = aptc;
		this.enrollmentIDPhix = enrollmentIDPhix;
		this.capAgentNPN = capAgentNPN;
		this.emailAddress = emailAddress;
		this.insurerName = insurerName;
	}
	
	public EnrollmentCSVDto(StringBuilder errorMessage, StringBuilder errorCSV) {
		super();
		this.errorMessage = errorMessage;
		this.errorCSV = errorCSV;
	}
	
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public Float getAptc() {
		return aptc;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public Integer getEnrollmentIDPhix() {
		return enrollmentIDPhix;
	}
	public void setEnrollmentIDPhix(Integer enrollmentIDPhix) {
		this.enrollmentIDPhix = enrollmentIDPhix;
	}
	public String getCapAgentNPN() {
		return capAgentNPN;
	}
	public void setCapAgentNPN(String capAgentNPN) {
		this.capAgentNPN = capAgentNPN;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getInsurerName() {
		return insurerName;
	}
	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}
	
	public StringBuilder getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(StringBuilder errorMessage) {
		this.errorMessage = errorMessage;
	}
	public StringBuilder getErrorCSV() {
		return errorCSV;
	}
	public void setErrorCSV(StringBuilder errorCSV) {
		this.errorCSV = errorCSV;
	}
	
	@Override
	public String toString() {
		return "EnrollmentCSVDto [enrollmentStatus=" + enrollmentStatus + ", effectiveStartDate=" + effectiveStartDate
				+ ", effectiveEndDate=" + effectiveEndDate + ", aptc=" + aptc + ", enrollmentIDPhix=" + enrollmentIDPhix
				+ ", capAgentNPN=" + capAgentNPN + ", emailAddress=" + emailAddress + ", insurerName=" + insurerName
				+ "]";
	}
	
	public StringBuilder toStringBuilder() {
		return new StringBuilder(enrollmentStatus + "," + DateUtil.dateToString(effectiveStartDate, DATE_FORMAT_MM_DD_YYYY)
				+ "," + DateUtil.dateToString(effectiveEndDate, DATE_FORMAT_MM_DD_YYYY) + "," + String.format("%.2f", aptc) + "," + enrollmentIDPhix
				+ "," + capAgentNPN + "," + emailAddress + ","+ insurerName);
	}
	

}
