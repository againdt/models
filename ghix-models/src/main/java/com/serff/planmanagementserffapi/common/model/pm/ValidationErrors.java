
package com.serff.planmanagementserffapi.common.model.pm;

import java.util.ArrayList;
import java.util.List;



public class ValidationErrors {

    protected List<ValidationError> errors;

    /**
     * Gets the value of the errors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidationError }
     * 
     * 
     */
    public List<ValidationError> getErrors() {
        if (errors == null) {
            errors = new ArrayList<ValidationError>();
        }
        return this.errors;
    }

}
