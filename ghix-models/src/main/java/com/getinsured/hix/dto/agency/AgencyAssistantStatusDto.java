package com.getinsured.hix.dto.agency;


import java.util.List;


public class AgencyAssistantStatusDto {

	String id;
	String approvalStatus;
	String status;
	String comments;
	List<String> assitantStatusList;
	List<AgencyAssistantStatusHistory> statusHistory;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<String> getAssitantStatusList() {
		return assitantStatusList;
	}
	public void setAssitantStatusList(List<String> assitantStatusList) {
		this.assitantStatusList = assitantStatusList;
	}
	public List<AgencyAssistantStatusHistory> getStatusHistory() {
		return statusHistory;
	}
	public void setStatusHistory(List<AgencyAssistantStatusHistory> statusHistory) {
		this.statusHistory = statusHistory;
	}
	
	
	
}
