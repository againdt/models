//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.07 at 05:31:40 PM IST 
//


package com.getinsured.enrollment._1095.cy_2018.manifest.cms.dsh.bdshfferesp.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.enrollment._1095.cy_2018.manifest.niem.niem.niem_core._2.NonNegativeDecimalType;


/**
 * <p>Java class for RestrictedNonNegativeDecimalMinimumOneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestrictedNonNegativeDecimalMinimumOneType">
 *   &lt;simpleContent>
 *     &lt;restriction base="&lt;http://niem.gov/niem/niem-core/2.0>NonNegativeDecimalType">
 *       &lt;attGroup ref="{http://niem.gov/niem/structures/2.0}SimpleObjectAttributeGroup"/>
 *     &lt;/restriction>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestrictedNonNegativeDecimalMinimumOneType", namespace = "http://bdshfferesp.dsh.cms.gov/extension/1.0")
public class RestrictedNonNegativeDecimalMinimumOneType
    extends NonNegativeDecimalType
{


}
