package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerHistoryResponseDTO extends GhixResponseDTO {

	private String hiosIssuerId;
	private String issuerName;
	private String companyLogo;
	private List<IssuerHistoryDTO> issuerHistoryDTOList;

	public IssuerHistoryResponseDTO() {
		super();
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public List<IssuerHistoryDTO> getIssuerHistoryDTOList() {
		return issuerHistoryDTOList;
	}

	public void setIssuerHistoryDTO(IssuerHistoryDTO issuerHistoryDTO) {

		if (CollectionUtils.isEmpty(this.issuerHistoryDTOList)) {
			this.issuerHistoryDTOList = new ArrayList<IssuerHistoryDTO>();
		}
		this.issuerHistoryDTOList.add(issuerHistoryDTO);
	}
}
