
package com.serff.planmanagementserffapi.serff.model.service;

import com.serff.planmanagementserffapi.common.model.pm.ValidationErrors;



public class DataTemplateOutcome {

    protected String dataTemplateId;
    protected ExchangeAttachment exchangeAttachment;
    protected ValidationErrors validationErrors;

    /**
     * Gets the value of the dataTemplateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplateId() {
        return dataTemplateId;
    }

    /**
     * Sets the value of the dataTemplateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplateId(String value) {
        this.dataTemplateId = value;
    }

    /**
     * Gets the value of the exchangeAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeAttachment }
     *     
     */
    public ExchangeAttachment getExchangeAttachment() {
        return exchangeAttachment;
    }

    /**
     * Sets the value of the exchangeAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeAttachment }
     *     
     */
    public void setExchangeAttachment(ExchangeAttachment value) {
        this.exchangeAttachment = value;
    }

    /**
     * Gets the value of the validationErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationErrors }
     *     
     */
    public ValidationErrors getValidationErrors() {
        return validationErrors;
    }

    /**
     * Sets the value of the validationErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationErrors }
     *     
     */
    public void setValidationErrors(ValidationErrors value) {
        this.validationErrors = value;
    }

}
