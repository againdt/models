package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TkmWorkflowsResponse extends GHIXResponse implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TkmWorkflows tkmWorkflows = null;
	
	private List<TkmWorkflows> tkmWorkflowsList = null;
	
	private Map<String, Object> tkmTicketsMap = null;

	private List<Role> tkmRoleList = null;
	
	private List<String> ticketType = null;
	
	private List<AccountUser> tkmUserList = null;
	
	public TkmWorkflows getTkmWorkflows() {
		return tkmWorkflows;
	}

	public void setTkmWorkflows(TkmWorkflows tkmWorkflows) {
		this.tkmWorkflows = tkmWorkflows;
	}

	public List<TkmWorkflows> getTkmWorkflowsList() {
		return tkmWorkflowsList;
	}

	public void setTkmWorkflowsList(List<TkmWorkflows> tkmWorkflowsList) {
		this.tkmWorkflowsList = tkmWorkflowsList;
	}

	public Map<String, Object> getTkmTicketsMap() {
		return tkmTicketsMap;
	}

	public void setTkmTicketsMap(Map<String, Object> tkmTicketsMap) {
		this.tkmTicketsMap = tkmTicketsMap;
	}

	public List<Role> getTkmRoleList() {
		return tkmRoleList;
	}

	public void setTkmRoleList(List<Role> tkmRoleList) {
		this.tkmRoleList = tkmRoleList;
	}

	public List<String> getTicketType() {
		return ticketType;
	}

	public void setTicketType(List<String> ticketType) {
		this.ticketType = ticketType;
	}
	
	public List<AccountUser> getTkmUserList() {
		return tkmUserList;
	}

	public void setTkmUserList(List<AccountUser> tkmUserList) {
		this.tkmUserList = tkmUserList;
	}

	public TkmWorkflows getTicketWorkflows(String category, String type) {
		// TODO Auto-generated method stub
		return null;
	}
}