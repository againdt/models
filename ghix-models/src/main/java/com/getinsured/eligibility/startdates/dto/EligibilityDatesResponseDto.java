package com.getinsured.eligibility.startdates.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;

@JsonDeserialize(builder = EligibilityDatesResponseDto.EligibilityDatesResponseDtoBuilder.class)
@Data
@Builder
public class EligibilityDatesResponseDto {

	public enum Status {REQUIRED, NOTREQUIRED, OVERRIDDEN, VALID};
	public enum SubStatus {NOTREQUIRED, VALID, OLDER_60_DAYS, GREATER_THAN_ENROLLMENT_END_DATE};

	private Status status;
	private SubStatus subStatus;
	private Date financialEffectiveDate;
	@JsonProperty
	private boolean isUpdated;
	@JsonProperty
	private boolean isNF2Fcase;
	
	@JsonPOJOBuilder(withPrefix = "")
    public static final class EligibilityDatesResponseDtoBuilder {
    }

}
