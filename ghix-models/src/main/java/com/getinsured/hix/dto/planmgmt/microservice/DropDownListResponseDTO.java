package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO class is used to get/set data for Drop-Down List.
 */
public class DropDownListResponseDTO extends GhixResponseDTO {

	private Map<String, String> data;

	public DropDownListResponseDTO() {
		super();
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(String key, String value) {

		if (null == data) {
			data = new HashMap<String, String>();
		}
		this.data.put(key, value);
	}
}
