package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * HIX-60431
 * This is pojo class for PLAN_MC_BENEFIT database table
 * @author sharma_va
 */
@Audited
@Entity
@Table(name = "PLAN_MC_BENEFIT")
public class PlanMedicareBenefit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_MC_BENEFIT_SEQ")
	@SequenceGenerator(name = "PLAN_MC_BENEFIT_SEQ", sequenceName = "PLAN_MC_BENEFIT_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_MEDICARE_ID")
	private PlanMedicare planMedicare;

	@Column(name = "CATEGORY")
	private String category;

	@Column(name = "CATEGORY_ENUM")
	private String categoryEnum;

	@Column(name = "CATEGORY_NAME")
	private String categoryName;

	@Column(name = "SORT_ORDER")
	private String sortOrder;

	@Column(name = "FULL_VALUE")
	private String fullName;

	@Column(name = "TINY_VALUE")
	private String tinyValue;

	@Column(name = "FLAG_VALUE")
	private Integer flagValue;

	@Column(name = "COPAYMENT_AMOUNT")
	private Integer copaymentAmount;

	@Column(name = "COINSURANCE_AMOUNT")
	private Integer coinsuranceAmount;

	@Column(name = "NAME")
	private String name;

	@Column(name = "SHORT_NAME")
	private String shortName;

	@Column(name = "TINY_NAME")
	private String tinyName;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name = "FULL_VALUE_EXT")
	private String fullValueExt ;
	
	public PlanMedicareBenefit() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanMedicare getPlanMedicare() {
		return planMedicare;
	}

	public void setPlanMedicare(PlanMedicare planMedicare) {
		this.planMedicare = planMedicare;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryEnum() {
		return categoryEnum;
	}

	public void setCategoryEnum(String categoryEnum) {
		this.categoryEnum = categoryEnum;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getTinyValue() {
		return tinyValue;
	}

	public void setTinyValue(String tinyValue) {
		this.tinyValue = tinyValue;
	}

	public Integer getFlagValue() {
		return flagValue;
	}

	public void setFlagValue(Integer flagValue) {
		this.flagValue = flagValue;
	}

	public Integer getCopaymentAmount() {
		return copaymentAmount;
	}

	public void setCopaymentAmount(Integer copaymentAmount) {
		this.copaymentAmount = copaymentAmount;
	}

	public Integer getCoinsuranceAmount() {
		return coinsuranceAmount;
	}

	public void setCoinsuranceAmount(Integer coinsuranceAmount) {
		this.coinsuranceAmount = coinsuranceAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getTinyName() {
		return tinyName;
	}

	public void setTinyName(String tinyName) {
		this.tinyName = tinyName;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getFullValueExt() {
		return fullValueExt;
	}

	public void setFullValueExt(String fullValueExt) {
		this.fullValueExt = fullValueExt;
	}
}
