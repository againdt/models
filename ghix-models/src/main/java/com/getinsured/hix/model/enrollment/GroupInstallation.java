package com.getinsured.hix.model.enrollment;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import com.getinsured.hix.model.LookupValue;

/**
 * 
 * @author meher_a
 *
 */
@Audited
@Entity
@Table(name="ENRL_GROUP_INSTALLATION")
public class GroupInstallation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUP_INSTALLATION_SEQ")
	@SequenceGenerator(name = "GROUP_INSTALLATION_SEQ", sequenceName = "GROUP_INSTALLATION_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="EMPLOYER_ID")
	private Integer employerId ;
	
	@Column(name="ISSUER_ID")
	private Integer issuerId ;
	
	@Column(name="BROKER_ID")
	private Integer brokerId ;
	
	@Column(name="GROUP_DATA")
	private String groupData ;
	
	@Column(name="IS_CHANGED")
	private String isChanged;
	
	@OneToMany(mappedBy="groupInstallation", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<GroupPlan> groupPlan;
	
	@OneToOne
	@JoinColumn(name="ENROLLMENT_ACTION_LKP",nullable= true)
    private LookupValue enrollmentActionLkp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="UPDATED_ON",nullable=false)
	private Date updatedOn;

	@Column(name="TERMINATION_DATE")
	private Date terminatedOn;
	
	
	@Column(name="EMPLOYER_ENROLLMENT_ID")
	private Integer employerEnrollmentId;
	
	
	@Column(name="FILE_NAME")
	private String fileName;
		
	
	public Integer getEmployerEnrollmentId() {
		return employerEnrollmentId;
	}

	public void setEmployerEnrollmentId(Integer employerEnrollmentId) {
		this.employerEnrollmentId = employerEnrollmentId;
	}

	public Date getTerminatedOn() {
		return terminatedOn;
	}

	public void setTerminatedOn(Date terminatedOn) {
		this.terminatedOn = terminatedOn;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public String getGroupData() {
		return groupData;
	}

	public void setGroupData(String groupData) {
		this.groupData = groupData;
	}

	public String getIsChanged() {
		return isChanged;
	}

	public void setIsChanged(String isChanged) {
		this.isChanged = isChanged;
	}

	public List<GroupPlan> getGroupPlan() {
		return groupPlan;
	}

	public void setGroupPlan(List<GroupPlan> groupPlan) {
		this.groupPlan = groupPlan;
	}

	public LookupValue getEnrollmentActionLkp() {
		return enrollmentActionLkp;
	}

	public void setEnrollmentActionLkp(LookupValue enrollmentActionLkp) {
		this.enrollmentActionLkp = enrollmentActionLkp;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		GroupInstallation other = (GroupInstallation) obj;
		if (id == null) {
			if (other.id != null){
				return false;
			}
		} else if (!id.equals(other.id)){
			return false;
		}
		return true;
	}
	
	
	
}
