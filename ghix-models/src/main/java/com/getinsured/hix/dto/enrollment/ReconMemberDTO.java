package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class ReconMemberDTO implements Serializable, Comparable<ReconMemberDTO> {
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDate;
	private String gender;
	private String ssn;
	private String subscriberIndicator;
	private String individualRelationshipCode;
	private String exchangeAssignedSubscriberId;
	private String exchangeAssignedMemberId;
	private String issuerAssignedSubscriberId;
	private String issuerAssignedMemberId;
	private String homeAddress1;
	private String homeAddress2;
	private String homeCity;
	private String homeState;
	private String homeZip;
	private String homeCountyCode;
	private String mailingAddress1;
	private String mailingAddress2;
	private String mailingCity;
	private String mailingState;
	private String mailingZip;
	private String telephoneNumber;
	private String tobacco;
	//Only Hix side
	private Integer enrolleeId;
	private Float individualPremium;
	private String individualPremEffectiveDate;
	private String enrolleeStatus;
	@JsonIgnore
	private Date memberStartDate;
	
	private String memberBeginDate;
	private String memberEndDate;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = returnNullIfEmpty(firstName);
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = returnNullIfEmpty(middleName);
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = returnNullIfEmpty(lastName);
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = returnNullIfEmpty(birthDate);
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = returnNullIfEmpty(gender);
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = returnNullIfEmpty(ssn);
	}
	public String getSubscriberIndicator() {
		return subscriberIndicator;
	}
	public void setSubscriberIndicator(String subscriberIndicator) {
		this.subscriberIndicator = returnNullIfEmpty(subscriberIndicator);
	}
	public String getIndividualRelationshipCode() {
		return individualRelationshipCode;
	}
	public void setIndividualRelationshipCode(String individualRelationshipCode) {
		this.individualRelationshipCode = returnNullIfEmpty(individualRelationshipCode);
	}
	public String getExchangeAssignedSubscriberId() {
		return exchangeAssignedSubscriberId;
	}
	public void setExchangeAssignedSubscriberId(String exchangeAssignedSubscriberId) {
		this.exchangeAssignedSubscriberId = returnNullIfEmpty(exchangeAssignedSubscriberId);
	}
	public String getExchangeAssignedMemberId() {
		return exchangeAssignedMemberId;
	}
	public void setExchangeAssignedMemberId(String exchangeAssignedMemberId) {
		this.exchangeAssignedMemberId = returnNullIfEmpty(exchangeAssignedMemberId);
	}
	public String getIssuerAssignedSubscriberId() {
		return issuerAssignedSubscriberId;
	}
	public void setIssuerAssignedSubscriberId(String issuerAssignedSubscriberId) {
		this.issuerAssignedSubscriberId = returnNullIfEmpty(issuerAssignedSubscriberId);
	}
	public String getIssuerAssignedMemberId() {
		return issuerAssignedMemberId;
	}
	public void setIssuerAssignedMemberId(String issuerAssignedMemberId) {
		this.issuerAssignedMemberId = returnNullIfEmpty(issuerAssignedMemberId);
	}
	public String getHomeAddress1() {
		return homeAddress1;
	}
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = returnNullIfEmpty(homeAddress1);
	}
	public String getHomeAddress2() {
		return homeAddress2;
	}
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = returnNullIfEmpty(homeAddress2);
	}
	public String getHomeCity() {
		return homeCity;
	}
	public void setHomeCity(String homeCity) {
		this.homeCity = returnNullIfEmpty(homeCity);
	}
	public String getHomeState() {
		return homeState;
	}
	public void setHomeState(String homeState) {
		this.homeState = returnNullIfEmpty(homeState);
	}
	public String getHomeZip() {
		return homeZip;
	}
	public void setHomeZip(String homeZip) {
		this.homeZip = returnNullIfEmpty(homeZip);
	}
	public String getHomeCountyCode() {
		return homeCountyCode;
	}
	public void setHomeCountyCode(String homeCountyCode) {
		this.homeCountyCode = returnNullIfEmpty(homeCountyCode);
	}
	public String getMailingAddress1() {
		return mailingAddress1;
	}
	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = returnNullIfEmpty(mailingAddress1);
	}
	public String getMailingAddress2() {
		return mailingAddress2;
	}
	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = returnNullIfEmpty(mailingAddress2);
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = returnNullIfEmpty(mailingCity);
	}
	public String getMailingState() {
		return mailingState;
	}
	public void setMailingState(String mailingState) {
		this.mailingState = returnNullIfEmpty(mailingState);
	}
	public String getMailingZip() {
		return mailingZip;
	}
	public void setMailingZip(String mailingZip) {
		this.mailingZip = returnNullIfEmpty(mailingZip);
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = returnNullIfEmpty(telephoneNumber);
	}
	public String getTobacco() {
		return tobacco;
	}
	public void setTobacco(String tobacco) {
		this.tobacco = returnNullIfEmpty(tobacco);
	}
	
	public Integer getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	public Float getIndividualPremium() {
		return individualPremium;
	}
	public void setIndividualPremium(Float individualPremium) {
		this.individualPremium = individualPremium;
	}
	public String getIndividualPremEffectiveDate() {
		return individualPremEffectiveDate;
	}
	public void setIndividualPremEffectiveDate(String individualPremEffectiveDate) {
		this.individualPremEffectiveDate = returnNullIfEmpty(individualPremEffectiveDate);
	}
	@Override
	public int compareTo(ReconMemberDTO member) {
		int compareValue=0;
		boolean isNumber = member!=null && null != member.getExchangeAssignedMemberId() && null != this.exchangeAssignedMemberId
				&& NumberUtils.isNumber(member.getExchangeAssignedMemberId().trim())
				&& NumberUtils.isNumber(this.exchangeAssignedMemberId.trim());
		if(isNumber){
			compareValue= (Integer.compare(Integer.parseInt(member.getExchangeAssignedMemberId().trim()), Integer.parseInt(this.exchangeAssignedMemberId.trim())));
			if(compareValue==0){
				if(this.getMemberStartDate()!=null && member.memberStartDate!=null){
					compareValue= member.getMemberStartDate().compareTo(this.getMemberStartDate());
				}
			}
		}
		return compareValue;
	}
	
	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	public String getEnrolleeStatus() {
		return enrolleeStatus;
	}
	public void setEnrolleeStatus(String enrolleeStatus) {
		this.enrolleeStatus = returnNullIfEmpty(enrolleeStatus);
	}
	public Date getMemberStartDate() {
		return memberStartDate;
	}
	public void setMemberStartDate(Date memberStartDate) {
		this.memberStartDate = memberStartDate;
	}
	public String getMemberBeginDate() {
		return memberBeginDate;
	}
	public void setMemberBeginDate(String memberBeginDate) {
		this.memberBeginDate = returnNullIfEmpty(memberBeginDate);
	}
	public String getMemberEndDate() {
		return memberEndDate;
	}
	public void setMemberEndDate(String memberEndDate) {
		this.memberEndDate = returnNullIfEmpty(memberEndDate);
	}
	
	private String returnNullIfEmpty(String value){
		if(null != value && !value.trim().isEmpty()){
			return value.trim();
		}else{
			return null;
		}
	}
}
