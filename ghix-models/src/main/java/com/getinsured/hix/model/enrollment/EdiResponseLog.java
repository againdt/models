package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EDI_RESPONSE_LOG")
public class EdiResponseLog implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EdiResponseLog_Seq")
	@SequenceGenerator(name = "EdiResponseLog_Seq", sequenceName = "EDI_RESPONSE_LOG_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="RESPONSE_CODE",nullable= true)
    private int responseCode;
	
	@Column(name="HIOS_ISSUER_ID",nullable= true)
    private String hiosIssuerId;
	
	@Column(name="JOB_EXECUTION_ID",nullable= true)
    private Integer jobExecutionId;
	
	@Column(name="JOB_INSTANCE_ID",nullable= true)
    private Integer jobInstanceId;
	
	@Column(name="STEP_EXECUTION_ID",nullable= true)
	private Integer stepExecutionId;
	
	@Column(name="JOB_STEP_NAME",nullable= true)
	private String jobStepName;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATED_ON",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="UPDATED_ON",nullable=false)
	private Date updatedOn;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public Integer getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(Integer jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	public Integer getJobInstanceId() {
		return jobInstanceId;
	}

	public void setJobInstanceId(Integer jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}

	public Integer getStepExecutionId() {
		return stepExecutionId;
	}

	public void setStepExecutionId(Integer stepExecutionId) {
		this.stepExecutionId = stepExecutionId;
	}

	public String getJobStepName() {
		return jobStepName;
	}

	public void setJobStepName(String jobStepName) {
		this.jobStepName = jobStepName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdatedOn(new TSDate()); 
	}
}
