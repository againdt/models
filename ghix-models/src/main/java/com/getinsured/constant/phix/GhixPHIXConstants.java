/**
 * 
 */
package com.getinsured.constant.phix;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author panda_p In this class we can define constants for GHIX application. These Constants can
 *         be either direct (define a constant of type psf & directly assign any value to it) or it
 *         can from config file. (define a constant of type psf & populate value from config file by
 *         using 'Value' attribute of annotation)
 */

@Component
public final class GhixPHIXConstants {

	private GhixPHIXConstants() {
	}

	/***************************************************************************************/
	/********************** Common configuration properties *********************************/
	/***************************************************************************************/

	public static String MY_ACCOUNT_URL;
	
	public static String APP_URL;
	public static String PREVIOUS_PAGE_URL = "previousPageURL" ;
	
	public static String[] helathBenefitList = {"PRIMARY_VISIT", "SPECIAL_VISIT", "OTHER_PRACTITIONER_VISIT", "PREVENT_SCREEN_IMMU",
        "LABORATORY_SERVICES", "IMAGING_XRAY", "IMAGING_SCAN",
        "GENERIC", "PREFERRED_BRAND", "NON_PREFERRED_BRAND", "SPECIALTY_DRUGS",
        "OUTPATIENT_FACILITY_FEE", "OUTPATIENT_SURGERY_SERVICES",
        "EMERGENCY_SERVICES", "EMERGENCY_TRANSPORTATION", "URGENT_CARE",
        "MENTAL_HEALTH_OUT", "MENTAL_HEALTH_IN", "SUBSTANCE_OUTPATIENT_USE", "SUBSTANCE_INPATIENT_USE",
        "PRENATAL_POSTNATAL", "INPATIENT_HOSPITAL_SERVICE", "INPATIENT_PHY_SURGICAL_SERVICE",
        "HOME_HEALTH_SERVICES", "OUTPATIENT_REHAB_SERVICES", "HABILITATION", "SKILLED_NURSING_FACILITY", "DURABLE_MEDICAL_EQUIP", "HOSPICE_SERVICE", 
        "RTN_EYE_EXAM_CHILDREN", "GLASSES_CHILDREN",
        "ACCIDENTAL_DENTAL", "BASIC_DENTAL_CARE_ADULT", "BASIC_DENTAL_CARE_CHILD", "DENTAL_CHECKUP_CHILDREN", "MAJOR_DENTAL_CARE_CHILD", 
       "MAJOR_DENTAL_CARE_ADULT", "ORTHODONTIA_ADULT", "ORTHODONTIA_CHILD", "RTN_DENTAL_ADULT", 
        "ACUPUNCTURE", "CHIROPRACTIC", "REHABILITATIVE_PHYSICAL_THERAPY", "HEARING_AIDS", "NUTRITIONAL_COUNSELING", "WEIGHT_LOSS", "DELIVERY_IMP_MATERNITY_SERVICES"};
       // "MAJOR_DENTAL_CARE_ADULT", "ORTHODONTIA_ADULT", "ORTHODONTIA_CHILD", "RTN_DENTAL_ADULT"};

	public static String[] dentalBenefitList = {"ACCIDENTAL_DENTAL", "BASIC_DENTAL_CARE_ADULT", "BASIC_DENTAL_CARE_CHILD", "DENTAL_CHECKUP_CHILDREN", "MAJOR_DENTAL_CARE_CHILD", 
        "MAJOR_DENTAL_CARE_ADULT", "ORTHODONTIA_ADULT", "ORTHODONTIA_CHILD", "RTN_DENTAL_ADULT", "TEETH_CLEANING", "X_RAY", "TOPICAL_FLOURIDE", "FILLINGS", "EXTRACTIONS", "ORAL_SURGERY", "CROWN", "ROOT_CANAL", "DENTURES", "ENDODONTICS"};
	
	@Value("#{configProp['appUrl']}")
	public void setAPP_URL(String appURL) {
		APP_URL = appURL;
	}

	@Value("#{configProp['security.myAccountUrl']}")
	public void setMY_ACCOUNT_URL(String myAccountUrl) {
		MY_ACCOUNT_URL = myAccountUrl;
	}

	public static String MY_INBOX_URL;

	@Value("#{configProp['security.myInboxUrl']}")
	public void setMY_INBOX_URL(String myInboxUrl) {
		MY_INBOX_URL = myInboxUrl;
	}

	public static String LOGIN_PAGE_URL;

	@Value("#{configProp['security.loginPageUrl']}")
	public void setLOGIN_PAGE_URL(String loginPageUrl) {
		LOGIN_PAGE_URL = loginPageUrl;
	}
	
	public static String HELP_PAGE_URL;	
	@Value("#{configProp['masthead.help']}")
	public void setHELP_PAGE_URL(String helpPageUrl) {
		HELP_PAGE_URL = helpPageUrl;
	}
	
	public static String CHAT_URL;	
	@Value("#{configProp['masthead.chatUrl']}")
	public void setCHAT_URL(String chatUrl) {
		CHAT_URL = chatUrl;
	}
	
	public static String CUSTOMER_SERVICE_PHONE;	
	@Value("#{configProp['masthead.customerServicePhone']}")
	public void setCUSTOMER_SERVICE_PHONE(String customerServicePhone) {
		CUSTOMER_SERVICE_PHONE = customerServicePhone;
	}
	
	public static String REDIRECT_ENROLLMENT_TO_BB_URL;	
	@Value("#{configProp['enrollment.redirectEnrollmentToBbUrl']}")
	public void setREDIRECT_ENROLLMENT_TO_BB_URL(String rEDIRECT_ENROLLMENT_TO_BB_URL) {
		REDIRECT_ENROLLMENT_TO_BB_URL = rEDIRECT_ENROLLMENT_TO_BB_URL;
	}
	
	/***************************************************************************************/
	/********************** Common configuration properties *********************************/
	/***************************************************************************************/
	public static String UPLOAD_PATH;
	public static String ISSUER_DOC_PATH;
	public static String EXCHANGE_NAME;
	public static String PLAN_DOC_PATH;
	public static String PROVIDER_DOC_PATH;
	public static String BROKER_DOC_PATH;
	public static String ENVIRONMENT;
	public static String GOOGLE_ANALYTICS_CODE;
	
	@Value("#{configProp.uploadPath}")
	public void setUPLOAD_PATH(String uploadPath) {
		UPLOAD_PATH = uploadPath;
	}

	@Value("#{configProp.issuer_doc_path}")
	public void setISSUER_DOC_PATH(String issuerDocPath) {
		ISSUER_DOC_PATH = issuerDocPath;
	}

	@Value("#{configProp.exchangename}")
	public void setEXCHANGE_NAME(String eXCHANGE_NAME) {
		EXCHANGE_NAME = eXCHANGE_NAME;
	}
	
	@Value("#{configProp.ghixEnvironment}")
	public void setENVIRONMENT(String eNVIRONMENT) {
		ENVIRONMENT = eNVIRONMENT;
	}
	
	@Value("#{configProp.googleAnalyticsTrackingCode}")
	public void setGOOGLE_ANALYTICS_CODE(String tRACKING_CODE) {
		GOOGLE_ANALYTICS_CODE = tRACKING_CODE;
	}

	/***************************************************************************************/
	/*********************** Broker configuration properties *****************************/
	/***************************************************************************************/
	@Value("#{configProp.broker_doc_path}")
	public void setBROKER_DOC_PATH(String brokerDocPath) {
		BROKER_DOC_PATH = brokerDocPath;
	}

	public static String SWITCH_URL_IND66;

	@Value("#{configProp['giToAhbxAccountSwitchUrl']}")
	public void setSWITCH_URL_IND66(String sWITCH_URL_IND66) {
		SWITCH_URL_IND66 = sWITCH_URL_IND66;
	}

	// for build detail configuration
	public static String BUILD_NUMBER;
	public static String BUILD_DATE;
    public static String BUILD_BRANCH_NAME;
	
	@Value("#{buildProp['build.number']}")
	public void setBUILD_NUMBER(String bUILD_NUMBER) {
		BUILD_NUMBER = bUILD_NUMBER;
	}

	@Value("#{buildProp['build.timestamp']}")
	public void setBUILD_DATE(String bUILD_DATE) {
		BUILD_DATE = bUILD_DATE;
	}

	@Value("#{buildProp['branch.name']}")
	public void setBUILD_BRANCH_NAME(String bUILD_BRANCH_NAME) {
		BUILD_BRANCH_NAME = bUILD_BRANCH_NAME;
	}


	/***************************************************************************************/
	/*********************** Provider/Plan configuration properties *****************************/
	/***************************************************************************************/
	public static String PROVIDER_UPLOADFILEPATH;
	public static String PROVIDER_PHYSICIAN_DOWNLOADFILEPATH;
	public static String PROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH;
	public static String PROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH;
	public static String PROVIDER_FACILITIES_DOWNLOADFILEPATH;
	public static String PROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH;
	public static String PROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH;
	public static final String ACKNOWLEDGE_200_OK = "200 OK";
	public static final String ENROLLMENT_ENTITY_DOC_PATH = "Enrollment Entity Documents";

	@Value("#{configProp['provider.uploadFilePath']}")
	public void setPROVIDER_UPLOADFILEPATH(String pROVIDER_UPLOADFILEPATH) {
		PROVIDER_UPLOADFILEPATH = pROVIDER_UPLOADFILEPATH;
	}

	@Value("#{configProp['provider.physicians.downloadFilePath']}")
	public void setPROVIDER_PHYSICIAN_DOWNLOADFILEPATH(String pROVIDER_PHYSICIAN_DOWNLOADFILEPATH) {
		PROVIDER_PHYSICIAN_DOWNLOADFILEPATH = pROVIDER_PHYSICIAN_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.physiciansAddress.downloadFilePath']}")
	public void setPROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH(String pROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH) {
		PROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH = pROVIDER_PHYSICIAN_ADDR_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.physiciansSpecialty.downloadFilePath']}")
	public void setPROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH(String pROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH) {
		PROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH = pROVIDER_PHYSICIAN_SPCLTY_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.facilities.downloadFilePath']}")
	public void setPROVIDER_FACILITIES_DOWNLOADFILEPATH(String pROVIDER_FACILITIES_DOWNLOADFILEPATH) {
		PROVIDER_FACILITIES_DOWNLOADFILEPATH = pROVIDER_FACILITIES_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.facilitiesAddress.downloadFilePath']}")
	public void setPROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH(String pROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH) {
		PROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH = pROVIDER_FACILITIES_ADDR_DOWNLOADFILEPATH;
	}

	@Value("#{configProp['provider.facilitiesSpecialty.downloadFilePath']}")
	public void setPROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH(String pROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH) {
		PROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH = pROVIDER_FACILITIES_SPCLTY_DOWNLOADFILEPATH;
	}

	@Value("#{configProp.plan_doc_path}")
	public void setPLAN_DOC_PATH(String planDocPath) {
		PLAN_DOC_PATH = planDocPath;
	}

	@Value("#{configProp.provider_doc_path}")
	public void setPROVIDER_DOC_PATH(String providerDocPath) {
		PROVIDER_DOC_PATH = providerDocPath;
	}
	

	/*****************************************************************************************************/
	/*********************** Employer SHOP Web Service configuration properties *************************/
	/*****************************************************************************************************/
	public static String TNS_GHIX_EMP_PLAN_SELECTION;
	public static String WSDL_GHIX_EMP_PLAN_SELECTION;
	public static String TNS_ACN_ORDER_ID_UPDATE;
	public static String WSDL_ACN_ORDER_ID_UPDATE;
	public static String TNS_ACN_EMP_DETAILS;
	public static String WSDL_ACN_EMP_DETAILS;

	/*@Value("#{configProp.tnsGhixEmpPlanSelection}")
	public void setTNS_GHIX_EMP_PLAN_SELECTION(String tNS_GHIX_EMP_PLAN_SELECTION) {
		TNS_GHIX_EMP_PLAN_SELECTION = tNS_GHIX_EMP_PLAN_SELECTION;
	}

	@Value("#{configProp.wsdlGhixEmpPlanSelection}")
	public void setWSDL_GHIX_EMP_PLAN_SELECTION(String wSDL_GHIX_EMP_PLAN_SELECTION) {
		WSDL_GHIX_EMP_PLAN_SELECTION = wSDL_GHIX_EMP_PLAN_SELECTION;
	}
*/
	/*@Value("#{configProp.tnsAcnOrderIdUpdate}")
	public void setTNS_ACN_ORDER_ID_UPDATE(String tNS_ACN_ORDER_ID_UPDATE) {
		TNS_ACN_ORDER_ID_UPDATE = tNS_ACN_ORDER_ID_UPDATE;
	}*/

	/*@Value("#{configProp.wsdlAcnOrderIdUpdate}")
	public void setWSDL_ACN_ORDER_ID_UPDATE(String wSDL_ACN_ORDER_ID_UPDATE) {
		WSDL_ACN_ORDER_ID_UPDATE = wSDL_ACN_ORDER_ID_UPDATE;
	}
*/
	/*@Value("#{configProp.tnsAcnEmployerDetails}")
	public void setTNS_ACN_EMP_DETAILS(String tNS_ACN_EMP_DETAILS) {
		TNS_ACN_EMP_DETAILS = tNS_ACN_EMP_DETAILS;
	}

	@Value("#{configProp.wsdlAcnEmployerDetails}")
	public void setWSDL_ACN_EMP_DETAILS(String wSDL_ACN_EMP_DETAILS) {
		WSDL_ACN_EMP_DETAILS = wSDL_ACN_EMP_DETAILS;
	}

	public static String AFTER_SHOP_EXTERNAL_REDIRECT_LINK;

	@Value("#{configProp.AfterShopExternalRedirectLink}")
	public void setACN_AFTER_SHOP_REDIRECT_LINK(String aFTER_SHOP_EXTERNAL_REDIRECT_LINK) {
		AFTER_SHOP_EXTERNAL_REDIRECT_LINK = aFTER_SHOP_EXTERNAL_REDIRECT_LINK;
	}
*/
	public static String AFTER_ENROLLMENT_ACN_REDIRECT_LINK;

	@Value("#{configProp['enrollment.afterEnrollmentAcnRedirectLink']}")
	public void setAFTER_ENROLLMENT_ACN_REDIRECT_LINK(String aFTER_ENROLLMENT_ACN_REDIRECT_LINK) {
		AFTER_ENROLLMENT_ACN_REDIRECT_LINK = aFTER_ENROLLMENT_ACN_REDIRECT_LINK;
	}
	
	public static String ADULT_DENTAL_PLAN_LINK;
	@Value("#{configProp['enrollment.findAdultDentalPlanLink']}")
	public void setADULT_DENTAL_PLAN_LINK(String aDULT_DENTAL_PLAN_LINK) {
		ADULT_DENTAL_PLAN_LINK = aDULT_DENTAL_PLAN_LINK;
	}

	/* Web service configuration for Eligibility module begins */
	public static String GET_INCOME_FOR_ANONYMOUS_PRESCREEN_URL="#";
	public static String GET_USER_APPLICATION="#";
	public static String SAVE_USER_APPLICATION="#";

	/* Web service configuration for Eligibility module ends */

	/*****************************************************************************************************/
	/*********************** Enrollment Web Service configuration properties *****************************/
	/*****************************************************************************************************/

	public static boolean ENRL_AHBX_WSDL_CALL_ENABLE;

	@Value("#{configProp['enrollment.ahbxWsdlCallEnable']}")
	public void setENRL_AHBX_WSDL_CALL_ENABLE(boolean eNRL_AHBX_WSDL_CALL_ENABLE) {
		ENRL_AHBX_WSDL_CALL_ENABLE = eNRL_AHBX_WSDL_CALL_ENABLE;
	}
	
	public static String ISA05;

	@Value("#{configProp['enrollment.ISA05']}")
	public void setISA05(String strISA05) {
		ISA05 = strISA05;
	}

	public static String ISA06;

	@Value("#{configProp['enrollment.ISA06']}")
	public void setISA06(String strISA06) {
		ISA06 = strISA06;
	}
	public static String GS08;
	@Value("#{configProp['enrollment.GS08']}")
	public void setGS08(String strGS08) {
		GS08 = strGS08;
	}
	/*
	 * public static String CREATE_INDIVIDUAL_ENROLLMENT_URL;
	 * @Value("#{configProp['enrollment.createIndividualEnrollmentUrl']}") public void
	 * setCREATE_INDIVIDUAL_ENROLLMENT_URL(String cREATE_INDIVIDUAL_ENROLLMENT_URL) {
	 * CREATE_INDIVIDUAL_ENROLLMENT_URL = cREATE_INDIVIDUAL_ENROLLMENT_URL; } public static String
	 * GET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL;
	 * @Value("#{configProp['enrollment.getIndividualPlanSelectionDetailsUrl']}") public void
	 * setGETINDIVIDUAL_PLAN_SELECTION_DETAILS_URL(String gET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL)
	 * { GET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL = gET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL; }
	 * public static String GET_INDIVIDUAL_PLAN_SELECTION_URL;
	 * @Value("#{configProp['enrollment.getIndividualPlanSelectionUrl']}") public void
	 * setGETINDIVIDUAL_PLAN_SELECTION_URL(String gET_INDIVIDUAL_PLAN_SELECTION_URL) {
	 * GET_INDIVIDUAL_PLAN_SELECTION_URL = gET_INDIVIDUAL_PLAN_SELECTION_URL; } public static String
	 * GET_VERIFY_PIN_URL;
	 * @Value("#{configProp['enrollment.getVerifyUrl']}") public void setGET_VERIFY_PIN_URL(String
	 * gET_VERIFY_PIN_URL) { GET_VERIFY_PIN_URL = gET_VERIFY_PIN_URL; } public static String
	 * FIND_BY_ORDERID_URL;
	 * @Value("#{configProp['enrollment.findbyorderidUrl']}") public void
	 * setFIND_BY_ORDERID_URL(String fIND_BY_ORDERID_URL) { FIND_BY_ORDERID_URL =
	 * fIND_BY_ORDERID_URL; } public static String SEARCH_ENROLLEE_URL;
	 * @Value("#{configProp['enrollment.searchEnrolleeUrl']}") public void
	 * setSEARCH_ENROLLEE_URL(String sEARCH_ENROLLEE_URL) { SEARCH_ENROLLEE_URL =
	 * sEARCH_ENROLLEE_URL; } public static String FIND_ENROLLEE_BY_ID_URL;
	 * @Value("#{configProp['enrollment.findEnrolleeByIdUrl']}") public void
	 * setFIND_ENROLLEE_BY_ID_URL(String fIND_ENROLLEE_BY_ID_URL) { FIND_ENROLLEE_BY_ID_URL =
	 * fIND_ENROLLEE_BY_ID_URL; }
	 */

	/*
	 * public static String FIND_ENROLLMENTS_BY_ISSUER_URL;
	 * @Value("#{configProp['enrollment.findEnrollmentsByIssuerUrl']}") public void
	 * setFIND_ENROLLMENTS_BY_ISSUER_URL(String fIND_ENROLLMENTS_BY_ISSUER_URL) {
	 * FIND_ENROLLMENTS_BY_ISSUER_URL = fIND_ENROLLMENTS_BY_ISSUER_URL; } public static String
	 * FIND_TAX_FILING_DATE_FOR_YEAR_URL;
	 * @Value("#{configProp['enrollment.findTaxFilingDateForYearUrl']}") public void
	 * setFIND_TAX_FILING_DATE_FOR_YEAR_URL(String fIND_TAX_FILING_DATE_FOR_YEAR_URL) {
	 * FIND_TAX_FILING_DATE_FOR_YEAR_URL = fIND_TAX_FILING_DATE_FOR_YEAR_URL; }
	 */

	/*
	 * public static boolean DISPLAY_SIGNATURE_PIN;
	 * @Value("#{configProp['enrollment.displaySignaturePIN']}") public void
	 * setDISPLAYSIGNATUREPIN(boolean DISPLAYSIGNATUREPIN) { DISPLAY_SIGNATURE_PIN =
	 * DISPLAYSIGNATUREPIN; }
	 */
	/*
	 * public static boolean DISPLAY_FILE_TAX_RETURN;
	 * @Value("#{configProp['enrollment.displayFileTaxReturn']}") public void
	 * setDISPLAYFILETAXRETURN(boolean DISPLAYFILETAXRETURN) { DISPLAY_FILE_TAX_RETURN =
	 * DISPLAYFILETAXRETURN; }
	 */

	/*
	 * public static boolean DISPLAY_CONFIRMATION_DISCLAIMERS;
	 * @Value("#{configProp['enrollment.displayConfirmationDisclaimers']}") public void
	 * setDISPLAYCONFIRMATIONDISCLAIMERS(boolean DISPLAYCONFIRMATIONDISCLAIMERS) {
	 * DISPLAY_CONFIRMATION_DISCLAIMERS = DISPLAYCONFIRMATIONDISCLAIMERS; }
	 */
	/*
	 * public static boolean DISPLAY_CONFIRMATION_MAKING_CHANGES_TO_YOUR_PLANS;
	 * @Value("#{configProp['enrollment.displayConfirmationMakingChangesToYourPlans']}") public void
	 * setDISPLAYCONFIRMATIONMAKINGCHANGESTOYOURPLANS(boolean
	 * DISPLAYCONFIRMATIONMAKINGCHANGESTOYOURPLANS) {
	 * DISPLAY_CONFIRMATION_MAKING_CHANGES_TO_YOUR_PLANS =
	 * DISPLAYCONFIRMATIONMAKINGCHANGESTOYOURPLANS; }
	 */

	public static String OUTPUTPATH;

	@Value("#{configProp.outputpath}")
	public void setOUTPUTPATH(String oUTPUTPATH) {
		OUTPUTPATH = oUTPUTPATH;
	}

	/*****************************************************************************************************/
	/*********************** Enrollment configuration properties *****************************************/
	/*****************************************************************************************************/

	/*****************************************************************************************************/
	/*********************** GHIX SERFF configuration properties *************************/
	/*****************************************************************************************************/
	
	public static String SERFF_CSR_HOME_PATH;
	
	@Value("#{configProp.SERFF_CSR_homePath}")
	public void setSerffCsrHomePath(String serfCsrHomePath) {
		SERFF_CSR_HOME_PATH = serfCsrHomePath;
	}
	
	public static String FTP_SERVER_UPLOAD_SUBPATH;
	
	@Value("#{configProp['Serff_Ftp_Server_Subpath']}")
	public void setFTPServerSubpath(String ftpServerPath) {
		FTP_SERVER_UPLOAD_SUBPATH = ftpServerPath;
	}
	
	public static String SERVICE_SERVER_LIST;

	@Value("#{configProp['SERFF_ServiceServerList']}")
	public void setServiceServerList(String serverList) {
		SERVICE_SERVER_LIST = serverList;
	}

	/*****************************************************************************************************/
	/*********************** Common GHix Application String Constants ************************************/
	/*****************************************************************************************************/
	public static String MAXUPLOADSIZEEXCEEDED_EXCEPTION = "Maxuploadsizeexceededexception";
	public static final String ANONYMOUS_PERSON_LIST = "ANONYMOUS_PERSON_LIST";
	public static final String INDIVIDUAL_PERSON_LIST = "INDIVIDUAL_PERSON_LIST";
	public static final String ACN_INDIV_PERSON_LIST = "ACN_INDIV_PERSON_LIST";
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	public static final String DISPLAY_DATE_FORMAT = "MMM dd, yyyy";
	public static final String DISPLAY_DATE_FORMAT1 = "yyyy-mm-dd hh:mm:ss";
	public static final String FILENAME_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String GROUP_ENRL_DATE_FORMAT = "yyMMdd";
	public static final String GROUP_ENRL_TIME_FORMAT = "HHmmss";
	public static final String EMP_ENRL_DATE_FORMAT = "yyyymmdd";
	public static final String ANONYMOUS_INDIVIDUAL_TYPE = "ANONYMOUS_INDIVIDUAL_TYPE";
	public static final String HOUSEHOLD_ID = "HOUSEHOLD_ID";
	public static final String SYS_DATE = "SYS_DATE";
	public static final String SHOP_EMPLOYEE_CONTRIBUTION = "EMPLOYEE_CONTRIBUTION";
	public static final String SHOP_DEPENDENT_CONTRIBUTION = "DEPENDENT_CONTRIBUTION";

	public static final String INDIVIDUAL_ORDER_ID = "INDIVIDUAL_ORDER_ID";
	public static final String COVERAGE_START = "COVERAGE_START";
	public static final String COVERAGE_END = "COVERAGE_END";
	public static final String HOUSEHOLD_PREFERENCES = "HOUSEHOLD_PREFERENCES";
	public static final String MEDICAL_CONDITIONS = "MEDICAL_CONDITIONS";

	public static final String HOUSEHOLD = "HOUSEHOLD";
	public static final String HLD_ENROLLMENT_TYPE = "ENROLLMENT_TYPE";
	public static final String HLD_APTC = "APTC";
	public static final String HLD_CSR = "CSR";
	public static final String HLD_PRGM_TYPE = "ProgramType";

	public static final String CURRENT_GROUP_ID = "CURRENT_GROUP_ID";
	public static final String GROUP_LIST = "GROUP_LIST";

	public static final Integer PAGE_SIZE = 10;

	public static final String UNDERSCORE = "_";
	public static final String DOT = ".";
	public static final String FRONT_SLASH = "/";
	public static final String ISSUER_CERTI_FOLDER = "certificates";
	public static final String ISSUER_SIGN_APPLICATION_STATEMENT = "I have reviewed the issuer application approve of it to be listed on the exchange.";

	public static final String TOTAL_TAX_CREDIT = "TOTAL_TAX_CREDIT";

	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final String INCORRECT_TYPE = "INCORRECT TYPE";

	public static final String CERTI_FOLDER = "certificates";
	public static final String BENEFITS_FOLDER = "benefits";
	public static final String RATES_FOLDER = "rates";
	public static final String AUTHORITY_FOLDER = "authority";
	public static final String DISCLOSURES_FOLDER = "disclosures";
	public static final String MARKETING_FOLDER = "marketing";
	public static final String ACCREDITATION_FOLDER = "accreditation";
	public static final String ADD_INFO_FOLDER = "additional_info";
	public static final String BROCHURE_FOLDER = "brochures";
	public static final String PLAN_SUPPORT_DOC_FOLDER = "plan_support";
	public static final String ADD_SUPPORT_FOLDER = "support";
	public static final String ORGANIZATION_FOLDER = "organization";

	public static final String ENROLLMENT_TYPE_SHOP = "24";
	public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	public static final String USER_NAME_CARRIER = "CARRIER";
	public static final String USER_NAME_EXCHANGE = "EXCHANGE";
	public static final String CA_834_INDV = "CA_834_INDV";
	public static final String CA_834_SHOP = "CA_834_SHOP";
	public static final String NM_834_INDV = "NM_834_INDV";
	public static final String NM_834_SHOP = "NM_834_SHOP";
	public static final String CA_834_RECONSHOP = "CA_834_RECONSHOP";
	public static final String CA_834_RECONINDIV = "CA_834_RECONINDV";
	public static final String NM_834_RECONSHOP = "NM_834_RECONSHOP";
	public static final String NM_834_RECONINDIV = "NM_834_RECONINDV";
	public static final String CA_820_SHOP = "CA_820_SHOP";
	public static final String CA_820_INDV = "CA_820_INDV";
	public static final String INDIVIDUAL = "Individual";
	public static final String INBOUND = "Inbound";
	public static final String OUTBOUND = "Outbound";
	public static final String SHOP = "SHOP";
	public static final String SSAP_APPLICATION_ID = "ssapApplicationId";

	public static String BRANCH_NAME;

	public static enum FileType {
		BENEFITS_FILE, RATES_FILE, ACTUARIAL_CERTIFICATES, AUTHORITY, MARKETING, DISCLOSURES, ACCREDITATION, ADDITIONAL_INFO, ADDITIONAL_SUPPORT, ORGANIZATION, CERTIFICATES, BROCHURE, PROVIDER
		// to add provider filetypes
	}

	public static String TARGET_SERVER_URL;

	@Value("#{configProp.targetServerUrl}")
	public void setTARGET_SERVER_URL(String target_server_url) {
		TARGET_SERVER_URL = target_server_url;
	}

	public static String SMARTYSTREET_HTML_TOKEN;

	@Value("#{configProp['smartystreet.htmltoken']}")
	public void setSMARTYSTREET_HTML_TOKEN(String smartystreet_html_token) {
		SMARTYSTREET_HTML_TOKEN = smartystreet_html_token;
	}

	@Value("#{configProp.BRANCH_NAME}")
	public void setBranchName(String branchName) {
		BRANCH_NAME = branchName;
	}

	/**
	 * SERFF Exception/Error Constants.
	 */
	public static final String DATA_ADMIN_DATA = "DATA_ADMIN_DATA";
	public static final String DATA_ECP_DATA = "DATA_ECP_DATA";
	public static final String DATA_BENEFITS = "DATA_BENEFITS";
	public static final String DATA_PRESCRIPTION_DRUG = "DATA_PRESCRIPTION_DRUG";
	public static final String DATA_SERVICE_AREA = "DATA_SERVICE_AREA";
	public static final String DATA_RATING_TABLE = "DATA_RATING_TABLE";
	public static final String DATA_RATING_RULES = "DATA_RATING_RULES";
	public static final String DATA_NETWORK_ID = "DATA_NETWORK_ID";
	public static final String DATA_DENTAL_BENEFITS = "DATA_DENTAL_BENEFITS";
	public static final String DATA_SBC = "sbc";
	public static final String PRESCRIPTION_DRUG_FILENAME_SUFFIX = "drug.xml";
	public static final String NETWORK_ID_FILENAME_SUFFIX = "network.xml";
	public static final String URAC_FILENAME_SUFFIX = "urac.xml";
	public static final String NCQA_FILENAME_SUFFIX = "ncqa.xml";
	public static final String BENEFITS_FILENAME_SUFFIX = "plans.xml";
	public static final String RATING_TABLE_FILENAME_SUFFIX = "rate.xml";
	public static final String SERVICE_AREA_FILENAME_SUFFIX = "servicearea.xml";
	public static final String UNIFIED_RATE_FILENAME_SUFFIX = "urrt.xml";
	public static final long FTP_UPLOAD_MAX_SIZE = 314572800;
	public static final String FTP_UPLOAD_PLAN_PATH = "/uploadPlans/";
	public static final String FTP_BROCHURE_PATH = "dropbox";
	public static final String FTP_BROCHURE_SUCCESS_PATH = "success/ID_";
	public static final String FTP_BROCHURE_ERROR_PATH = "error/ID_";

	public static String getFTPUploadPlanPath() {
		return FTP_SERVER_UPLOAD_SUBPATH + FTP_UPLOAD_PLAN_PATH;
	}
	
	/** Client Exceptions */
	public static final Integer EXCEPTION_REQUIRED_FIELDS_MISSING_CODE = 1001;
	public static final String EXCEPTION_REQUIRED_FIELDS_MISSING_DESC = "Required fields missing.";

	public static final Integer EXCEPTION_INVALID_PARAMETER_CODE = 1002;
	public static final String EXCEPTION_INVALID_PARAMETER_DESC = "Invalid parameter.";

	public static final Integer EXCEPTION_NO_OBJECTS_WERE_FOUND_CODE = 1003;
	public static final String EXCEPTION_NO_OBJECTS_WERE_FOUND_DESC = "No objects were found.";

	public static final Integer EXCEPTION_FIELDS_CONTAIN_NO_DATA_CODE = 1004;
	public static final String EXCEPTION_FIELDS_CONTAIN_NO_DATA_DESC = "The fields contain no data.";

	public static final Integer EXCEPTION_OPERATION_NOT_SUPPORTED_CODE = 1005;
	public static final String EXCEPTION_OPERATION_NOT_SUPPORTED_DESC = "The operation is not implemented and should not be called.";

	/** Server Error */
	public static final Integer EXCEPTION_INTERNAL_DATABASE_ERROR_CODE = 2001;
	public static final String EXCEPTION_INTERNAL_DATABASE_ERROR_DESC = "Internal database error.";

	public static final Integer EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE = 2002;
	public static final String EXCEPTION_INTERNAL_APPLICATION_ERROR_DESC = "Internal application error.";

	public static final Integer EXCEPTION_IO_ERROR_CODE = 2003;
	public static final String EXCEPTION_IO_ERROR_DESC = "IO error occurred.";

	public static final Integer EXCEPTION_NULL_POINTER_EXCEPTION_CODE = 2004;
	public static final String EXCEPTION_NULL_POINTER_EXCEPTION_DESC = "Null pointer exception.";

	/**********************************/

	public static String PLAN_STATUS_LOADED = "LOADED";

	public static String PRESCREEN_FB_SHARE_URL="#";
	public static String PRESCREEN_TWITTER_SHARE_URL="#";
	public static String PRESCREEN_GPLUS_SHARE_URL="#";

	/*****************************************************************************************************/
	/*********************** GHIX SERFF Web Service configuration properties *************************/
	/*****************************************************************************************************/

	public static String GHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL;

	@Value("#{configProp['ghix_serff.plan.status.update']}")
	public void setGHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL(String gHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL) {
		GHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL = gHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL;
	}

	public static final Integer PLAN_NOT_FOUND_EHEALTH_ERROR_CODE = 4001;
	public static final String PLAN_NOT_FOUND_EHEALTH_DESC = "No Plans found for eHealth";
	public static final Integer EHEALTH_SERVICE_DOWN = 4002;
	public static final Integer EHEALTH_INTERNAL_ERROR = 4003;

	public static final Integer PLAN_NOT_FOUND_QUOTIT_ERROR_CODE = 4004;
	public static final String PLAN_NOT_FOUND_QUOTIT_DESC = "No Plans found for quotit";
	public static final Integer QUOTIT_SERVICE_DOWN = 4005;
	public static final Integer QUOTIT_INTERNAL_ERROR = 4006;

	public static final Integer PLAN_NOT_FOUND_UHC_ERROR_CODE = 4007;
	public static final String PLAN_NOT_FOUND_UHC_DESC = "No Plans found for UHC";
	public static final Integer UHC_SERVICE_DOWN = 4008;
	public static final Integer UHC_INTERNAL_ERROR = 4009;

	public static final Integer INVALID_STATE = 4010;
	public static final Integer INVALID_ZIPCODE = 4011;
	public static final Integer SQL_EXCEPTION = 4012;

	public static final Integer FAULT_EHEALTH = 4013;
	public static final Integer FAULT_UHC = 4014;
	public static final Integer FAULT_QUOTIT = 4015;
	
	public static final Integer INVALID_ISSUER = 4020;

	public static final Integer TEASER_PLAN_EXCEPTION_CODE = 5001;
	public static final String TEASER_PLAN_EXCEPTION_DESC = "Error occured while retrieving second lowest silver non tobacco plan";
	public static final Integer PLAN_NOT_FOUND = 5002;
	public static final String PLAN_NOT_FOUND_DESC = "No Silver Plan found from Teaser API";
		
	public static final String PHIX = "PHIX";
	public static final String CA = "STATE";
	public static final String NM = "NM";

	public static final Integer TEASER_PLAN_SLSNTP_ERRORCODE = 5001;
	public static final Integer TEASER_PLAN_SLSTP_ERRORCODE = 5003;
	public static final Integer TEASER_PLAN_LSP_ERRORCODE = 5004;
	public static final Integer TEASER_PLAN_LBP_ERRORCODE = 5005;

	public static final String TEASER_PLAN_SLSNTP_ERRORCODE_DESC = "Error occured while retrieving second lowest silver non tobacco plan";

	public static final String TEASER_PLAN_SLSTP_ERRORCODE_DESC = "Error occured while retrieving second lowest silver tobacco plan";

	public static final String TEASER_PLAN_LSP_ERRORCODE_DESC = "Error occured while retrieving lowest silver tobacco plan";

	public static final String TEASER_PLAN_LBP_ERRORCODE_DESC = "Error occured while retrieving lowest bronze plan";


	/***************************************************************************************/
	/********************** Plan Display properties *********************************/
	/***************************************************************************************/
	public static final String SUBSCRIBER_DATA = "subscriberData";
	public static final String SUBSCRIBER_MEMBER_ID = "subscriberMemberId";
	public static final String SUBSCRIBER_MEMBER_COUNTY = "subscriberMemberCounty";
	public static final String SUBSCRIBER_MEMBER_ZIP = "subscriberMemberZip";

	public static final String STATE_CODE = "stateCode";
	public static final String EXCHANGE_TYPE = "exchangeType";
	
	public static final String MIN_PREMIUM_PER_MEMBER = "planSelection.MinPremiumPerMember";
	

	public static final Integer ELIGIBILITY_RESPONSE_ERRORCODE = 6001;

	public static final String ELIGIBILITY_RESPONSE_ERRORCODE_DESC = "Error occured while retrieving Eligibility Response";
	
	public static String ANON_APPLY_URL;
	
	public static final String V_HIGH_MEDICAL_VAL = "vHighMedicalVal";

	public static final String HIGH_MEDICAL_VAL = "highMedicalVal";

	public static final String MODERATE_MEDICAL_VAL = "moderateMedicalVal";

	public static final String LOW_MEDICAL_VAL = "lowMedicalVal";

	public static final String V_HIGH_DRUG_USE_VAL = "vHighDrugUseVal";

	public static final String HIGH_DRUG_USE_VAL = "highDrugUseVal";

	public static final String MODERATE_DRUG_USE_VAL = "moderateDrugUseVal";

	public static final String LOW_DRUG_USE_VAL = "lowDrugUseVal";
	
	public static final String PROVIDER_VAL = "providerVal";

	public static final String ASSISTER_IMAGE_PATH = null;
	
	@Value("#{configProp['plandisplay.anonymousApplyURL']}")
	public void setANON_APPLY_URL(String aNON_APPLY_URL) {
		ANON_APPLY_URL = aNON_APPLY_URL;
	}
	
	// two factor authentication properties and keys
	public static Boolean IS_ENABLE2FACT_AUTH;	
	public static String TWO_FACTOR_AUTHENTICATION_ROLES;
	public static String DUO_SECRET_KEY;
	public static String DUO_INTEGRATION_KEY;
	public static String DUO_APPLICATION_SECRET_KEY;
	
	@Value("#{configProp['isEnable2FactAuth']}")
	public void setIS_ENABLE2FACT_AUTH(Boolean isEnable2FactAuth) {
		IS_ENABLE2FACT_AUTH = isEnable2FactAuth;
	}
	
	@Value("#{configProp['twoFactorAuthenticationRoles:']}")
	public void setTWO_FACTOR_AUTHENTICATION_ROLES(String two_Factor_Authenticaton_Roles) {
		TWO_FACTOR_AUTHENTICATION_ROLES = two_Factor_Authenticaton_Roles;
	}
	
	@Value("#{configProp['duoSecretKeyForTwoFactAuth:']}")
	public void setDUO_SECRET_KEY(String duo_Secret_Key) {
		DUO_SECRET_KEY = duo_Secret_Key;
	}
	
	@Value("#{configProp['duoIntegrationKeyForTwoFactAuth:']}")
	public void setDUO_INTEGRATION_KEY(String duo_Integration_Key) {
		DUO_INTEGRATION_KEY = duo_Integration_Key;
	}
	
	@Value("#{configProp['duoApplicationSecretKeyForTwoFactAuth']}")
	public void setDUO_APPLICATION_SECRET_KEY(String duo_Application_Secret_Key) {
		DUO_APPLICATION_SECRET_KEY = duo_Application_Secret_Key;
	}

	// FFM Integration properties
	//public static String DEFAULT_NPN;
	/*
	 * Get the default NPN for getinsured.com to be used with FFM integration
	 */
	/*@Value("#{configProp['defaultNPN']}")
	public void setDEFAULT_NPN(String defaultNPN) {
		DEFAULT_NPN = defaultNPN;
	}*/
	
	public static String D2C_KAISER_SFTP_FILEPATH;
	
	@Value("#{configProp['D2C.kaiser.sftp.filepath']}")
	public void setD2C_KAISER_SFTP_FILEPATH(String d2c_KAISER_SFTP_FILEPATH) {
		D2C_KAISER_SFTP_FILEPATH = d2c_KAISER_SFTP_FILEPATH;
	}

	//HOME PAGE Youtube URL
	public static String YOUTUBE_VIDEO_URL;
	
	@Value("#{configProp['homepage.youtube.video']}")
	public void setVideoUrl(String videoUrl) {
		YOUTUBE_VIDEO_URL = videoUrl;
	}
	
	/***************************************************************************************/
	/*********************** Strennus Provider configuration properties *****************************/
	/***************************************************************************************/
	public static String STRENNUS_DATA_DIR;
	public static String STRENNUS_JOB_LOG_DIR;
	
	@Value("#{configProp.strennus_data_dir}")
	public void setSTRENNUS_DATA_DIR(String strennusDataDir) {
		STRENNUS_DATA_DIR = System.getProperty("GHIX_HOME")+strennusDataDir;
	}

	@Value("#{configProp.strennus_job_log_dir}")
	public void setSTRENNUS_JOB_LOG_DIR(String strennusJobLogDir){
		STRENNUS_JOB_LOG_DIR = strennusJobLogDir;
	}
	
}
