package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.*;

@Entity
@Table(name="CARRIER_FEED_DETAILS")
public class CarrierFeedDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARRIER_FEED_DETAILS_SEQ")
	@SequenceGenerator(name = "CARRIER_FEED_DETAILS_SEQ", sequenceName = "CARRIER_FEED_DETAILS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "APP_ID", length = 100)
	private String appId;
	
	@Column(name = "CARRIER_STATUS", length = 50)
	private String carrierStatus;
	
	@Column(name = "GI_STATUS", length = 50)
	private String giStatus;
	
	@Column(name = "STATUS", length = 50)
	private String status;
	
	@Column(name = "REASON", length = 512)
	private String reason;
	
	@Column(name = "FEED_RECORD_JSON", length = 4000)
	private String feedRecordJson;
	
	@Column(name = "NOTES", length = 4000)
	private String notes;
	
	@Column(name="CARRIER_FEED_SUMMARY_ID")
	private Integer carrierFeedSummaryId;

    @ManyToOne
    @JoinColumn(name="CARRIER_FEED_SUMMARY_ID", insertable = false, updatable = false)
    private CarrierFeedSummary carrierFeedSummary;
	
	@Column(name="ENROLLMENT_ID")
	private Integer enrollmentId;
	
	@Column(name="OLD_GI_STATUS")
	private String oldGiStatus;
	
	@Column(name="CARRIER_LAST_UPDT_DT")
	private Date carrierLastUpdatedDate ;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;
	
    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private AccountUser createdBy;

    @ManyToOne
    @JoinColumn(name = "LAST_UPDATED_BY")
    private AccountUser lastUpdatedBy;

    @Column(name = "COMMISSION_AMT")
    private Float commissionAmt;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "COMMISSION_DATE")
    private Date commissionDate;
    
    @Column(name="POLICY_ID")
	private String policyId;
    
    @Column(name="NAME")
	private String name;
	    
    @OneToOne(targetEntity = LookupValue.class, optional=true, cascade = {CascadeType.DETACH})
   	@JoinColumn(name = "PRODUCT_TYPE_LKP_ID", referencedColumnName = "lookup_value_id")
   	private LookupValue productTypeLkp;
    
    @Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;
    
    @Column(name="TERMINATION_DATE")
	private Date terminationDate;
    
    @Column(name="IS_RENEWAL")
   	private String isRenewal;
    
    @Column(name = "RENEWAL_FEED_JSON", length = 2000)
	private String renewalFeedJson;
	
	@Column(name="CARRIER_UID")
	private String carrierUID;
	
	@Column(name="AGENT_OF_RECORD")
	private String agentOfRecord;
	
	@Column(name="PLAN_NAME")
	private String planName;
	
	@Column(name="EFFECTIVE_ENDDATE")
	private Date effectiveEndDate;
	
	@Column(name="LATEST_EFFECTIVE_DATE")
	private Date latestEffectiveDate;
	
	@Column(name="LATEST_EFFECTIVE_ENDDATE")
	private Date latestEffectiveEndDate;
	
	@Column(name="MATCH_TYPE")
	private String matchType;
	
	@Column(name="ENROLLMENT_STATUS")
	private String enrollmentStatus;
	
	@Column(name="PAID_MONTH_YEAR")
	private Date paidMonthYear;
	
	@Column(name="COMMISSION_TYPE")
	private String commissionType;
	
	@Column(name="TERM_REASON")
	private String termReason;
	
	@Column(name="UPDATE_TYPE")
	private String updateType;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="NET_PREMIUM_AMOUNT")
	private String netPremiumAmount;
	
	@Column(name="TOTAL_PREMIUM_AMOUNT")
	private Float totalPremiumAmount;
	
	@Column(name="RESOLVE_CATEGORY")
	private String resolveCategory;
	
	@Transient
	private Boolean isBacklogProcess;
	
	public String getOldGiStatus() {
		return oldGiStatus;
	}

	public void setOldGiStatus(String oldGiStatus) {
		this.oldGiStatus = oldGiStatus;
	}

	public Date getCarrierLastUpdatedDate() {
		return carrierLastUpdatedDate;
	}

	public void setCarrierLastUpdatedDate(Date carrierLastUpdatedDate) {
		this.carrierLastUpdatedDate = carrierLastUpdatedDate;
	}

	
    
    public String getNetPremiumAmount() {
		return netPremiumAmount;
	}

	public void setNetPremiumAmount(String netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}

	public Float getTotalPremiumAmount() {
		return totalPremiumAmount;
	}

	public void setTotalPremiumAmount(Float totalPremiumAmount) {
		this.totalPremiumAmount = totalPremiumAmount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getCarrierStatus() {
		return carrierStatus;
	}

	public void setCarrierStatus(String carrierStatus) {
		this.carrierStatus = carrierStatus;
	}

	public String getGiStatus() {
		return giStatus;
	}

	public void setGiStatus(String giStatus) {
		this.giStatus = giStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getFeedRecordJson() {
		return feedRecordJson;
	}

	public void setFeedRecordJson(String feedRecordJson) {
		this.feedRecordJson = feedRecordJson;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getCarrierFeedSummaryId() {
		return carrierFeedSummaryId;
	}

	public void setCarrierFeedSummaryId(Integer carrierFeedSummaryId) {
		this.carrierFeedSummaryId = carrierFeedSummaryId;
	}

    public CarrierFeedSummary getCarrierFeedSummary() {
        return carrierFeedSummary;
    }

    public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

    public Float getCommissionAmt() {
        return commissionAmt;
    }

    public void setCommissionAmt(Float commissionAmt) {
        this.commissionAmt = commissionAmt;
    }

    public Date getCommissionDate() {
        return commissionDate;
    }

    public void setCommissionDate(Date commissionDate) {
        this.commissionDate = commissionDate;
    }

    public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	
	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public LookupValue getProductTypeLkp() {
		return productTypeLkp;
	}

	public void setProductTypeLkp(LookupValue productTypeLkp) {
		this.productTypeLkp = productTypeLkp;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	
	public String getIsRenewal() {
		return isRenewal;
	}

	public void setIsRenewal(String isRenewal) {
		this.isRenewal = isRenewal;
	}

	public String getRenewalFeedJson() {
		return renewalFeedJson;
	}

	public void setRenewalFeedJson(String renewalFeedJson) {
		this.renewalFeedJson = renewalFeedJson;
	}
	
	public String getCarrierUID() {
		return carrierUID;
	}

	public void setCarrierUID(String carrierUID) {
		this.carrierUID = carrierUID;
	}

	public String getAgentOfRecord() {
		return agentOfRecord;
	}

	public void setAgentOfRecord(String agentOfRecord) {
		this.agentOfRecord = agentOfRecord;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Date getLatestEffectiveDate() {
		return latestEffectiveDate;
	}

	public void setLatestEffectiveDate(Date latestEffectiveDate) {
		this.latestEffectiveDate = latestEffectiveDate;
	}

	public Date getLatestEffectiveEndDate() {
		return latestEffectiveEndDate;
	}

	public void setLatestEffectiveEndDate(Date latestEffectiveEndDate) {
		this.latestEffectiveEndDate = latestEffectiveEndDate;
	}

	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public Date getPaidMonthYear() {
		return paidMonthYear;
	}

	public void setPaidMonthYear(Date paidMonthYear) {
		this.paidMonthYear = paidMonthYear;
	}

	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}

	public String getTermReason() {
		return termReason;
	}

	public void setTermReason(String termReason) {
		this.termReason = termReason;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public void setCarrierFeedSummary(CarrierFeedSummary carrierFeedSummary) {
		this.carrierFeedSummary = carrierFeedSummary;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getIsBacklogProcess() {
		if(isBacklogProcess != null){
			return isBacklogProcess;
		}else{
			return Boolean.FALSE;
		}
	}

	public void setIsBacklogProcess(Boolean isBacklogProcess) {
		this.isBacklogProcess = isBacklogProcess;
	}
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(AccountUser lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getResolveCategory() {
		return resolveCategory;
	}

	public void setResolveCategory(String resolveCategory) {
		this.resolveCategory = resolveCategory;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
