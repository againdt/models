INSERT INTO permissions (ID,NAME) VALUES (PERMISSIONS_SEQ.NEXTVAL,'MENU_PERMISSION');
INSERT INTO permissions (ID,NAME) VALUES (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_TENANT');
INSERT INTO permissions (ID,NAME) VALUES (PERMISSIONS_SEQ.NEXTVAL,'VIEW_PNL_REPORT');
INSERT INTO permissions (ID,NAME) VALUES (PERMISSIONS_SEQ.NEXTVAL,'MANAGE_AFFILIATES');

UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='MENU_PERMISSION' ) WHERE m.parent_id IS NULL;  


UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='MANAGE_TENANT' ) WHERE m.parent_id =( select id from menu_items where caption ='Tenant Configuration');  

UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='VIEW_PNL_REPORT' ) WHERE m.parent_id =(select id from menu_items where caption ='PnL Reports');


UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='MANAGE_AFFILIATES' ) WHERE m.parent_id = (select id from menu_items where caption ='Affiliates');  


UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='ADD_CONSUMER_CRM' ) WHERE m.caption ='Create New Consumer';


UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='EDIT_APP_CONFIG' ) WHERE m.caption ='STARR Config';


UPDATE menu_items m
SET m.permission_id = ( SELECT p.id FROM permissions p WHERE p.name ='VIEW_CONSUMER_CRM' ) WHERE m.caption ='DNC Tool';

INSERT
INTO TENANT_PERMISSIONS
  (
    ID,
    PERMISSION_ID,
    TENANT_ID,
    CREATION_TIMESTAMP,
    LAST_UPDATE_TIMESTAMP
  )
  (SELECT
    TENANT_PERMISSIONS_SEQ.NEXTVAL,
   p.id,t.id,systimestamp,systimestamp FROM tenant t, permissions p
   where t.name='GetInsured'
   and p.id not in (select tp.permission_id from tenant_permissions tp)
  );