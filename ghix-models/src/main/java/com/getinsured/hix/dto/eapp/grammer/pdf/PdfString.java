package com.getinsured.hix.dto.eapp.grammer.pdf;

import java.util.List;

import com.getinsured.hix.dto.eapp.application.EappApplication;
import com.getinsured.hix.dto.eapp.grammer.Template;

public class PdfString implements Template{
	
	public static final String MODEL_RESERVED_CHAR = "$";
	
	private String value;

	@Override
	public boolean isValid() {
		if(value.startsWith(MODEL_RESERVED_CHAR)){
			return EappApplication.isValidExpr(value);
		}else{
			/**
			 * check if legitimate text content is present
			 * like the length check, so that our code runs safely
			 */
		}
		
		return true;
		
	}

	@Override
	public List<String> compile() {
		return null;
	}

	
}
