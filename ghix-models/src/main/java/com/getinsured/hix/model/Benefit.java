package com.getinsured.hix.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Benefit implements Serializable 
{
	private String deductible;
	private String deductibleOut;
	private String officeVisit;
	private String officeVisitOut;
	private String coinsurance;
	private String coinsuranceOut;
	private String emegency;
	private String emegencyOut;
	private String annualOutOfPocketLmt;
	private String annualOutOfPocketLmtOut;
	private String lifeTimeMax;
	private String lifeTimeMaxOut;
	private String rxCard;
	private String rxCardOut;
	private String maternity;
	private String maternityOut;
	private String mentalHealth;
	private String mentalHealthOut;
	
	public String getMentalHealthOut() {
		return mentalHealthOut;
	}
	public void setMentalHealthOut(String mentalHealthOut) {
		this.mentalHealthOut = mentalHealthOut;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getDeductibleOut() {
		return deductibleOut;
	}
	public void setDeductibleOut(String deductibleOut) {
		this.deductibleOut = deductibleOut;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public String getOfficeVisitOut() {
		return officeVisitOut;
	}
	public void setOfficeVisitOut(String officeVisitOut) {
		this.officeVisitOut = officeVisitOut;
	}
	public String getCoinsurance() {
		return coinsurance;
	}
	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}
	public String getCoinsuranceOut() {
		return coinsuranceOut;
	}
	public void setCoinsuranceOut(String coinsuranceOut) {
		this.coinsuranceOut = coinsuranceOut;
	}
	public String getEmegency() {
		return emegency;
	}
	public void setEmegency(String emegency) {
		this.emegency = emegency;
	}
	public String getEmegencyOut() {
		return emegencyOut;
	}
	public void setEmegencyOut(String emegencyOut) {
		this.emegencyOut = emegencyOut;
	}
	public String getAnnualOutOfPocketLmt() {
		return annualOutOfPocketLmt;
	}
	public void setAnnualOutOfPocketLmt(String annualOutOfPocketLmt) {
		this.annualOutOfPocketLmt = annualOutOfPocketLmt;
	}
	public String getAnnualOutOfPocketLmtOut() {
		return annualOutOfPocketLmtOut;
	}
	public void setAnnualOutOfPocketLmtOut(String annualOutOfPocketLmtOut) {
		this.annualOutOfPocketLmtOut = annualOutOfPocketLmtOut;
	}
	public String getLifeTimeMax() {
		return lifeTimeMax;
	}
	public void setLifeTimeMax(String lifeTimeMax) {
		this.lifeTimeMax = lifeTimeMax;
	}
	public String getLifeTimeMaxOut() {
		return lifeTimeMaxOut;
	}
	public void setLifeTimeMaxOut(String lifeTimeMaxOut) {
		this.lifeTimeMaxOut = lifeTimeMaxOut;
	}
	public String getRxCard() {
		return rxCard;
	}
	public void setRxCard(String rxCard) {
		this.rxCard = rxCard;
	}
	public String getRxCardOut() {
		return rxCardOut;
	}
	public void setRxCardOut(String rxCardOut) {
		this.rxCardOut = rxCardOut;
	}
	public String getMaternity() {
		return maternity;
	}
	public void setMaternity(String maternity) {
		this.maternity = maternity;
	}
	public String getMaternityOut() {
		return maternityOut;
	}
	public void setMaternityOut(String maternityOut) {
		this.maternityOut = maternityOut;
	}
	public String getMentalHealth() {
		return mentalHealth;
	}
	public void setMentalHealth(String mentalHealth) {
		this.mentalHealth = mentalHealth;
	}
	
	
}

