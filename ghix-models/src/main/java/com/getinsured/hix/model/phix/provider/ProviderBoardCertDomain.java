package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_board_cert_domain")
public class ProviderBoardCertDomain  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer boardCertId;
	private String boardCertName;
	private Set<ProviderSpecialty> providerSpecialties = new HashSet<ProviderSpecialty>(0);

	@Id 
	@Column(name="BOARD_CERT_ID", unique=true, nullable=false)
	public Integer getBoardCertId() {
		return this.boardCertId;
	}

	public void setBoardCertId(Integer boardCertId) {
		this.boardCertId = boardCertId;
	}


	@Column(name="BOARD_CERT_NAME", length=50)
	public String getBoardCertName() {
		return this.boardCertName;
	}

	public void setBoardCertName(String boardCertName) {
		this.boardCertName = boardCertName;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerBoardCertDomain")
	public Set<ProviderSpecialty> getProviderSpecialties() {
		return this.providerSpecialties;
	}

	public void setProviderSpecialties(Set<ProviderSpecialty> providerSpecialties) {
		this.providerSpecialties = providerSpecialties;
	}


}


