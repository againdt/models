package com.getinsured.hix.dto.directenrollment;

/**
 * Holds the payment data
 * 
 * @author root
 *
 */
public class PaymentDTO {
	protected BankDTO bank;
	protected CreditCardDTO creditcard;
	protected PayeeDTO payee;
	protected String payeeCheck;

	public BankDTO getBank() {
		return bank;
	}

	public void setBank(BankDTO bank) {
		this.bank = bank;
	}

	public CreditCardDTO getCreditcard() {
		return creditcard;
	}

	public void setCreditcard(CreditCardDTO creditcard) {
		this.creditcard = creditcard;
	}

	public PayeeDTO getPayee() {
		return payee;
	}

	public void setPayee(PayeeDTO payee) {
		this.payee = payee;
	}

	public String getPayeeCheck() {
		return payeeCheck;
	}

	public void setPayeeCheck(String payeeCheck) {
		this.payeeCheck = payeeCheck;
	}

	@Override
	public String toString() {
		return "PaymentDTO [bank=" + bank + ", creditCard=" + creditcard + ", payee=" + payee + ", payeeCheck="
			+ payeeCheck + "]";
	}
}