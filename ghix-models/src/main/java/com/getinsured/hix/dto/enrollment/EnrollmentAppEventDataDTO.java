package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @since 07th August 2015.
 * @author Sharma_k
 * @version 1.1 initial release.
 * Jira Id: HIX-69207
 * Description Customer History: Enrollment Events
 */
public class EnrollmentAppEventDataDTO implements Serializable
{
	private Integer enrollmentId;
	private String insurerName;
	private String planName;
	private String enrollmentStatus;
	//We will not be sending EnrolleeAppEventDTO, Same is updated in Story HIX-69207
	private List<EnrolleeAppEventDTO> enrolleeAppEventDTOList;
	
	
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the insurerName
	 */
	public String getInsurerName() {
		return insurerName;
	}
	/**
	 * @param insurerName the insurerName to set
	 */
	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the enrollmentStatus
	 */
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	/**
	 * @return the enrolleeAppEventDTOList
	 */
	public List<EnrolleeAppEventDTO> getEnrolleeAppEventDTOList() {
		return enrolleeAppEventDTOList;
	}
	/**
	 * @param enrolleeAppEventDTOList the enrolleeAppEventDTOList to set
	 */
	public void setEnrolleeAppEventDTOList(
			List<EnrolleeAppEventDTO> enrolleeAppEventDTOList) {
		this.enrolleeAppEventDTOList = enrolleeAppEventDTOList;
	}
}
