package com.getinsured.hix.model;

import java.math.BigDecimal;

public class EmployerInvoiceLineItemsDetail
{
	private int id;
	private String policyNumber;
	private String name;
	private String carrierName;
	private String planType;
	private String coverageType;
	private String personsCovered;
	private BigDecimal totalPremium;
	private BigDecimal employeeContribution;
	private BigDecimal retroAdjustments;
	private BigDecimal netAmount;
	private int quantityCovered;
	private BigDecimal adjustment;
	private int employeeId;
	private BigDecimal manualAdjAmount;
	private BigDecimal manualEmployeeContriAdj;
	private BigDecimal manualEmployerContriAdj;
	private String comments;
	private AdjustmentReasonCode adjReasonCode;
	private String employeeName;
	private BigDecimal totAdj_RetroAdj_ManualAdj;
	private BigDecimal totEmployeeContri_RegContri_ManualContri;
	private BigDecimal employerContribution;
	private BigDecimal invoiceTotalAmountDue;
	
	
	public BigDecimal getInvoiceTotalAmountDue() {
		return invoiceTotalAmountDue;
	}

	public void setInvoiceTotalAmountDue(BigDecimal invoiceTotalAmountDue) {
		this.invoiceTotalAmountDue = invoiceTotalAmountDue;
	}

	public BigDecimal getEmployerContribution() {
		return employerContribution;
	}

	public void setEmployerContribution(BigDecimal employerContribution) {
		this.employerContribution = employerContribution;
	}

	public AdjustmentReasonCode getAdjReasonCode() {
		return adjReasonCode;
	}

	public void setAdjReasonCode(AdjustmentReasonCode adjReasonCode) {
		this.adjReasonCode = adjReasonCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public BigDecimal getTotAdj_RetroAdj_ManualAdj() {
		return totAdj_RetroAdj_ManualAdj;
	}

	public void setTotAdj_RetroAdj_ManualAdj(BigDecimal totAdj_RetroAdj_ManualAdj) {
		this.totAdj_RetroAdj_ManualAdj = totAdj_RetroAdj_ManualAdj;
	}

	public BigDecimal getTotEmployeeContri_RegContri_ManualContri() {
		return totEmployeeContri_RegContri_ManualContri;
	}

	public void setTotEmployeeContri_RegContri_ManualContri(
			BigDecimal totEmployeeContri_RegContri_ManualContri) {
		this.totEmployeeContri_RegContri_ManualContri = totEmployeeContri_RegContri_ManualContri;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public AdjustmentReasonCode getReasonCode() {
		return adjReasonCode;
	}

	public void setReasonCode(AdjustmentReasonCode adjReasonCode) {
		this.adjReasonCode = adjReasonCode;
	}

	public BigDecimal getManualEmployeeContriAdj() {
		return manualEmployeeContriAdj;
	}

	public void setManualEmployeeContriAdj(BigDecimal manualEmployeeContriAdj) {
		this.manualEmployeeContriAdj = manualEmployeeContriAdj;
	}

	public BigDecimal getManualEmployerContriAdj() {
		return manualEmployerContriAdj;
	}

	public void setManualEmployerContriAdj(BigDecimal manualEmployerContriAdj) {
		this.manualEmployerContriAdj = manualEmployerContriAdj;
	}

	public BigDecimal getManualAdjAmount() {
		return manualAdjAmount;
	}

	public void setManualAdjAmount(BigDecimal manualAdjAmount) {
		this.manualAdjAmount = manualAdjAmount;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getQuantityCovered()
	{
		return quantityCovered;
	}
	
	public void setQuantityCovered(int quantityCovered)
	{
		this.quantityCovered = quantityCovered;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public String getPersonsCovered() {
		return personsCovered;
	}

	public void setPersonsCovered(String personsCovered) {
		this.personsCovered = personsCovered;
	}

	public BigDecimal getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}

	public BigDecimal getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(BigDecimal employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public BigDecimal getRetroAdjustments() {
		return retroAdjustments;
	}

	public void setRetroAdjustments(BigDecimal retroAdjustments) {
		this.retroAdjustments = retroAdjustments;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(BigDecimal adjustment) {
		this.adjustment = adjustment;
	}
}
