package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.TenantIdPrePersistListener;

/**
 * @since 20th July 2015
 * @author Sharma_k
 *
 */
@Entity
@IdClass(AudId.class)
@Table(name="ENROLLMENT_EVENT_AUD")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class EnrollmentEventAud implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Id
	@Column(name = "REV")
	private int rev;
	
	@Column(name = "REVTYPE")
	private int revType;
	
	@ManyToOne
    @JoinColumn(name="ENROLLEE_ID")
	private Enrollee enrollee;
	
	@ManyToOne
    @JoinColumn(name="ENROLLMENT_ID")
	private Enrollment enrollment;
	
	@OneToOne
    @JoinColumn(name="EVENT_TYPE_LKP")
	private LookupValue eventTypeLkp;
	
	@OneToOne
    @JoinColumn(name="EVENT_REASON_LKP")
	private LookupValue eventReasonLkp;
	
	@OneToOne
    @JoinColumn(name="SPCL_ENROLLMENT_REASON_LKP")
	private LookupValue spclEnrollmentReasonLkp;
	
	@OneToOne
    @JoinColumn(name="created_by", nullable= true)
	private AccountUser createdBy;
	
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY", nullable= true)
    private AccountUser updatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	    
	@Column(name="SEND_TO_CARRIER_FLAG")
	private String sendToCarrierFlag;
	
	@Column(name="EXTRACTION_STATUS")
	private String extractionStatus;
	
	@Column(name="SEND_RENEWAL_TAG")
	private String sendRenewalTag;
	
	@Column(name="TXN_IDENTIFIER")
	private String txnIdentifier;
	
	@Column(name="QLE_IDENTIFIER")
	private String qleIdentifier;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the rev
	 */
	public int getRev() {
		return rev;
	}

	/**
	 * @param rev the rev to set
	 */
	public void setRev(int rev) {
		this.rev = rev;
	}

	/**
	 * @return the revType
	 */
	public int getRevType() {
		return revType;
	}

	/**
	 * @param revType the revType to set
	 */
	public void setRevType(int revType) {
		this.revType = revType;
	}

	/**
	 * @return the enrollee
	 */
	public Enrollee getEnrollee() {
		return enrollee;
	}

	/**
	 * @param enrollee the enrollee to set
	 */
	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	/**
	 * @return the enrollment
	 */
	public Enrollment getEnrollment() {
		return enrollment;
	}

	/**
	 * @param enrollment the enrollment to set
	 */
	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	/**
	 * @return the eventTypeLkp
	 */
	public LookupValue getEventTypeLkp() {
		return eventTypeLkp;
	}

	/**
	 * @param eventTypeLkp the eventTypeLkp to set
	 */
	public void setEventTypeLkp(LookupValue eventTypeLkp) {
		this.eventTypeLkp = eventTypeLkp;
	}

	/**
	 * @return the eventReasonLkp
	 */
	public LookupValue getEventReasonLkp() {
		return eventReasonLkp;
	}

	/**
	 * @param eventReasonLkp the eventReasonLkp to set
	 */
	public void setEventReasonLkp(LookupValue eventReasonLkp) {
		this.eventReasonLkp = eventReasonLkp;
	}

	/**
	 * @return the spclEnrollmentReasonLkp
	 */
	public LookupValue getSpclEnrollmentReasonLkp() {
		return spclEnrollmentReasonLkp;
	}

	/**
	 * @param spclEnrollmentReasonLkp the spclEnrollmentReasonLkp to set
	 */
	public void setSpclEnrollmentReasonLkp(LookupValue spclEnrollmentReasonLkp) {
		this.spclEnrollmentReasonLkp = spclEnrollmentReasonLkp;
	}

	/**
	 * @return the createdBy
	 */
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the sendToCarrierFlag
	 */
	public String getSendToCarrierFlag() {
		return sendToCarrierFlag;
	}

	/**
	 * @param sendToCarrierFlag the sendToCarrierFlag to set
	 */
	public void setSendToCarrierFlag(String sendToCarrierFlag) {
		this.sendToCarrierFlag = sendToCarrierFlag;
	}

	/**
	 * @return the extractionStatus
	 */
	public String getExtractionStatus() {
		return extractionStatus;
	}

	/**
	 * @param extractionStatus the extractionStatus to set
	 */
	public void setExtractionStatus(String extractionStatus) {
		this.extractionStatus = extractionStatus;
	}

	/**
	 * @return the sendRenewalTag
	 */
	public String getSendRenewalTag() {
		return sendRenewalTag;
	}

	/**
	 * @param sendRenewalTag the sendRenewalTag to set
	 */
	public void setSendRenewalTag(String sendRenewalTag) {
		this.sendRenewalTag = sendRenewalTag;
	}

	public String getTxnIdentifier() {
		return txnIdentifier;
	}

	public void setTxnIdentifier(String txnIdentifier) {
		this.txnIdentifier = txnIdentifier;
	}

	public String getQleIdentifier() {
		return qleIdentifier;
	}

	public void setQleIdentifier(String qleIdentifier) {
		this.qleIdentifier = qleIdentifier;
	}
	
}
