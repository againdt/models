package com.getinsured.hix.model.enrollment;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.getinsured.hix.model.AccountUser;

@Entity
@Table(name="ENRL_AGENT_BOB_TRNSFR")
public class EnrlAgentBOBTransfer {
	
	public enum BATCH_PROCESSING_STATUS{
		  PROCESSED, FAILED, PARTIALLY_PROCESSED, REPROCESS;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AGENT_BOB_TRNSFR_SEQ")
	@SequenceGenerator(name = "AGENT_BOB_TRNSFR_SEQ", sequenceName = "ENRL_AGENT_BOB_TRNSFR_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="REQUEST")
	private String request;
	
	@Column(name="RESPONSE")
	private String response;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="SKIP_ENROLLMENT_ID")
	private String skipEnrollmentId;
	
	@Column(name="SKIP_ERROR")
	private String skipError;
	
	@Column(name="BATCH_EXEC_ID")
	private Integer batchExecId;
	
	@Column(name="BATCH_PROCESSED_STATUS")
	private String batchProcessedStatus;
	
	@Column(name="CREATION_TIME_STAMP")
	private Date creationTimeStamp;

	@Transient
	private AccountUser user;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSkipEnrollmentId() {
		return skipEnrollmentId;
	}

	public void setSkipEnrollmentId(String skipEnrollmentId) {
		this.skipEnrollmentId = skipEnrollmentId;
	}

	public String getSkipError() {
		return skipError;
	}

	public void setSkipError(String skipError) {
		this.skipError = skipError;
	}

	public Integer getBatchExecId() {
		return batchExecId;
	}

	public void setBatchExecId(Integer batchExecId) {
		this.batchExecId = batchExecId;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	public String getBatchProcessedStatus() {
		return batchProcessedStatus;
	}

	public void setBatchProcessedStatus(String batchProcessedStatus) {
		this.batchProcessedStatus = batchProcessedStatus;
	}

		
	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimeStamp(new TSDate());
	}
}
