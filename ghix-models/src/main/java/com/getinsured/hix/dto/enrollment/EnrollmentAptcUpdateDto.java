package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class EnrollmentAptcUpdateDto implements Serializable{
	private Integer enrollmentId;
	private Float aptcAmt;
	private Date aptcEffectiveDate;
	private Long ssapApplicationId;
	
	/**
	 * Refer Jira ID: HIX-77253 && HIX-77255
	 * we may expect TRUE | FALSE values as well for this flag, Default value as FALSE
	 * TRUE - set APTC_amt and APTC_EFF_Date as NULL
	 * FALSE - Expect APTC_AMT in Request either $0 or amt > $0 else throw exception
	 */
	private boolean isConvertedToNonFinancialFlag = Boolean.FALSE;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Float getAptcAmt() {
		return aptcAmt;
	}
	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public Date getAptcEffectiveDate() {
		return aptcEffectiveDate;
	}
	public void setAptcEffectiveDate(Date aptcEffectiveDate) {
		this.aptcEffectiveDate = aptcEffectiveDate;
	}
	/**
	 * @return the isConvertedToNonFinancialFlag
	 */
	public boolean isConvertedToNonFinancialFlag() {
		return isConvertedToNonFinancialFlag;
	}
	/**
	 * @param isConvertedToNonFinancialFlag the isConvertedToNonFinancialFlag to set
	 */
	public void setConvertedToNonFinancialFlag(boolean isConvertedToNonFinancialFlag) {
		this.isConvertedToNonFinancialFlag = isConvertedToNonFinancialFlag;
	}
	
}
