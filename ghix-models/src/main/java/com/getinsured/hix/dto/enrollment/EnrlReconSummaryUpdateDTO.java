package com.getinsured.hix.dto.enrollment;

public class EnrlReconSummaryUpdateDTO {
	
	private String issuerName;
	private String fileName; 
	private String dateReceived;
	private String status;
	private String discrepancyFileName; 
	private String discrepancySentDate;
	
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDateReceived() {
		return dateReceived;
	}
	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDiscrepancyFileName() {
		return discrepancyFileName;
	}
	public void setDiscrepancyFileName(String discrepancyFileName) {
		this.discrepancyFileName = discrepancyFileName;
	}
	public String getDiscrepancySentDate() {
		return discrepancySentDate;
	}
	public void setDiscrepancySentDate(String discrepancySentDate) {
		this.discrepancySentDate = discrepancySentDate;
	}
}
