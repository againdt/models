package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.PlanDentalCost;

public class PlanDentalCostDTO extends PlanDentalCost {
	private static final long serialVersionUID = 1L;
	
	private int planDentalId;
	
	private int planId;


	public int getPlanDentalId() {
		return planDentalId;
	}

	public void setPlanDentalId(int planDentalId) {
		this.planDentalId = planDentalId;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}
}
