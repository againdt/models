/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ganatra_h
 *
 */
@Entity
@Table(name="employer_documents")
public class EmployerDocuments implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYER_DOCUMENTS_SEQ")
	@SequenceGenerator(name = "EMPLOYER_DOCUMENTS_SEQ", sequenceName = "EMPLOYER_DOCUMENTS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="employer_id")
    private int employerId;
	
	@Column(name="doc_id")
	private String docId;
	
	@Column(name="file_name")
	private String fileName;

	@Column(name="is_deleted")
	private String isDeleted;

	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="uploaded_date")
	private Date uploadedDate;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmployerId() {
		return employerId;
	}

	public void setEmployerId(int employerId) {
		this.employerId = employerId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}
}
