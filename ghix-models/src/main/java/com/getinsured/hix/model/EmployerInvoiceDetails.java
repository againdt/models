package com.getinsured.hix.model;

import java.util.Date;
import java.util.List;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;



public class EmployerInvoiceDetails
{
	
	private int id;

	private Employer employer;

	private String caseId;
	
	private Date statementDate;
	
	private String invoiceNumber;
	
	private String periodCovered;
	
	private Date paymentDueDate;
	
	private float amountDueFromLastInvoice;
	
	private float totalPaymentReceived;
	
	private float premiumThisPeriod;
	
	private float adjustments;
	
	private float exchangeFees;
	
	private float totalAmountDue;
	
	private float amountEnclosed;
	
	private PaidStatus paidStatus;
	
	private String name;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private int zip;
	
	private String address1;
	
	private String address2;
	
	
	private List<EmployerInvoiceLineItemsDetail> employerInvoiceLineItemsDetail;
	
	private List<EmployerInvoiceLineItems> employerInvoiceLineItems;
	
	private List<EmployerInvoiceLineItemsDetail> employerInvoiceCarrierBreakDown;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public float getAmountDueFromLastInvoice() {
		return amountDueFromLastInvoice;
	}

	public void setAmountDueFromLastInvoice(float amountDueFromLastInvoice) {
		this.amountDueFromLastInvoice = amountDueFromLastInvoice;
	}

	public float getTotalPaymentReceived() {
		return totalPaymentReceived;
	}

	public void setTotalPaymentReceived(float totalPaymentReceived) {
		this.totalPaymentReceived = totalPaymentReceived;
	}

	public float getPremiumThisPeriod() {
		return premiumThisPeriod;
	}

	public void setPremiumThisPeriod(float premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}

	public float getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(float adjustments) {
		this.adjustments = adjustments;
	}

	public float getExchangeFees() {
		return exchangeFees;
	}

	public void setExchangeFees(float exchangeFees) {
		this.exchangeFees = exchangeFees;
	}

	public float getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(float totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public float getAmountEnclosed() {
		return amountEnclosed;
	}

	public void setAmountEnclosed(float amountEnclosed) {
		this.amountEnclosed = amountEnclosed;
	}

	public List<EmployerInvoiceLineItemsDetail> getEmployerInvoiceLineItemsDetail() {
		return employerInvoiceLineItemsDetail;
	}

	public void setEmployerInvoiceLineItemsDetail(
			List<EmployerInvoiceLineItemsDetail> employerInvoiceLineItemsDetail) {
		this.employerInvoiceLineItemsDetail = employerInvoiceLineItemsDetail;
	}

	public PaidStatus getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(PaidStatus paidStatus) {
		this.paidStatus = paidStatus;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/*public List<EmployerLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<EmployerLocation> locations) {
		this.locations = locations;
	}*/
	
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getZip() {
		return this.zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public List<EmployerInvoiceLineItems> getEmployerInvoiceLineItems() {
		return employerInvoiceLineItems;
	}

	public void setEmployerInvoiceLineItems(
			List<EmployerInvoiceLineItems> employerInvoiceLineItems) {
		this.employerInvoiceLineItems = employerInvoiceLineItems;
	}
	
	public List<EmployerInvoiceLineItemsDetail> getEmployerInvoiceCarrierBreakDown()
	{
		return employerInvoiceCarrierBreakDown;
	}
	
	public void setEmployerInvoiceCarrierBreakDown(List<EmployerInvoiceLineItemsDetail> employerInvoiceCarrierBreakDown)
	{
		this.employerInvoiceCarrierBreakDown = employerInvoiceCarrierBreakDown;
	}

}
