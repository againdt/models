package com.getinsured.hix.dto.crm;

import java.io.Serializable;

/**
 * DTO for displaying manual enrollment data 
 * on CRM screen
 * @author root
 *
 */
public class ConsumerHistoryRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	private int agentId ;
	private String agentName;
	private int householdId ;
	private String logData;
	private String type;
	
	
	public int getAgentId() {
		return agentId;
	}
	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public int getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}
	public String getLogData() {
		return logData;
	}
	public void setLogData(String logData) {
		this.logData = logData;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
