/**
 * AHBXSyncRequest.java
 * @author chalse_v
 * @version 1.0
 * @since Apr 10, 2013 
 */
package com.getinsured.hix.dto.planmgmt.ahbx;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


import com.getinsured.hix.model.GHIXRequest;

/**
 * The AHBX synchronization ReST request class.
 * 
 * @author chalse_v
 * @version 1.0
 * @since Apr 10, 2013 
 *
 */
public class AHBXSyncRequest  extends GHIXRequest implements Serializable {
	
	/**
	 * Attribute long serialVersionUID
	 */	
	private static final long serialVersionUID = 5303756694068788307L;
	
	/**
	 * Attribute Logger LOGGER
	 */	
	public static enum RequestMethod {GET, POST}
	
	/**
	 * Attribute String url
	 */
	private String url;
	
	/**
	 * Attribute RequestMethod requestMethod
	 */
	private RequestMethod requestMethod;
	
	/**
	 * Attribute Map<String,String> requestParameters
	 */
	private Map<String, Object> requestParameters = new HashMap<String, Object>();
	
	
	/**
	 * Constructor.
	 */
	public AHBXSyncRequest() {
	}
	
	
	/**
	 * Getter for 'requestMethod'.
	 *
	 * @return the requestMethod.
	 */
	public RequestMethod getRequestMethod() {
		return this.requestMethod;
	}

	/**
	 * Setter for 'requestMethod'.
	 *
	 * @param requestMethod the requestMethod to set.
	 */
	public void setRequestMethod(RequestMethod requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * Getter for 'url'.
	 *
	 * @return the url.
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Setter for 'url'.
	 *
	 * @param url the url to set.
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Getter for 'requestParameters'.
	 *
	 * @return the requestParameters.
	 */
	public Map<String, Object> getRequestParameters() {
		return this.requestParameters;
	}

	/**
	 * Setter for 'requestParameters'.
	 *
	 * @param requestParameters the requestParameters to set.
	 */
	public void setRequestParameters(Map<String, Object> requestParameters) {
		this.requestParameters = requestParameters;
	}
	
	/**
	 * Add for 'requestParameters'.
	 *
	 * @param requestParameters the requestParameters to set.
	 */
	public void addRequestParameters(String key, Object value) {
		if(null == key || null == value || key.isEmpty()) {
			throw new IllegalArgumentException();
		}
		else {
			this.requestParameters.put(key, value);
		}
	}
	
	/**
	 * AddAll for 'requestParameters'.
	 *
	 * @param requestParameters the requestParameters to set.
	 */
	public void addAllRequestParameters(Map<String, Object> requestParameters) {
		if(null == requestParameters || requestParameters.isEmpty()) {
			throw new IllegalArgumentException();
		}
		else {
			this.requestParameters.putAll(requestParameters);
		}
	}

}
