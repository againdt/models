package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.getinsured.hix.dto.eapp.ProductTypeEnum;


/**
 * this can represent any insurance plan
 * 
 * @author root
 *
 */
public class Plan implements Serializable{
	private static final long serialVersionUID = 1L;
	private ProductTypeEnum insuranceType;
	private String id;
	private String planName; 
	private BigDecimal planCost;
	private String issuerPlanId;
	private String networkType;
	private Boolean hsaEligible = false;
	private String coInsurance;
	private String exchangeType;
	private String hiosPlanId;
    private String planInfoUrl;
	
	private Map<String, Object> other;
	
	public ProductTypeEnum getInsuranceType() {
		return insuranceType;
	}
	public Map<String, Object> getOther() {
		return other;
	}
	public void setOther(Map<String, Object> other) {
		this.other = other;
	}
	public void setInsuranceType(ProductTypeEnum insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public BigDecimal getPlanCost() {
		return planCost;
	}
	public void setPlanCost(BigDecimal planCost) {
		this.planCost = planCost;
	}
	
	public String getIssuerPlanId() {
		return issuerPlanId;
	}
	public void setIssuerPlanId(String issuerPlanId) {
		this.issuerPlanId = issuerPlanId;
	}
	
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	
	public Boolean getHsaEligible() {
		return hsaEligible;
	}
	public void setHsaEligible(Boolean hsaEligible) {
		this.hsaEligible = hsaEligible;
	}
	
	public String getCoInsurance() {
		return coInsurance;
	}
	public void setCoInsurance(String coInsurance) {
		this.coInsurance = coInsurance;
	}
	
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	
	public String getHiosPlanId() {
		return hiosPlanId;
	}
	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}
    public String getPlanInfoUrl() {
        return planInfoUrl;
    }
    public void setPlanInfoUrl(String planInfoUrl) {
        this.planInfoUrl = planInfoUrl;
    }

	public static Plan getSampleInstance(){
		Plan plan = new Plan();
		plan.setId("845A" + (int)34563*Math.random());
		plan.setInsuranceType(ProductTypeEnum.HLT);
		plan.setPlanCost(new BigDecimal(345.34));
		plan.setPlanName("Silver Plan PPO");
		plan.setHiosPlanId("123456CA23434");
		plan.setHsaEligible(false);
		return plan;
	}
}
