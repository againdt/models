
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;ACA Form Submission Status Detail Request Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2012-09-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;Description&gt;A group that provides ACA Form Submission Status Detail request message related information&lt;/Description&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for ACABulkRequestTransmitterStatusRequestGrpDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ACABulkRequestTransmitterStatusRequestGrpDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}ReceiptId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TransmittalControlCd"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ACABulkRequestTransmitterStatusRequestGrpDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "receiptId",
    "transmittalControlCd"
})
public class ACABulkRequestTransmitterStatusRequestGrpDetailType {

    @XmlElement(name = "ReceiptId", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String receiptId;
    @XmlElement(name = "TransmittalControlCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String transmittalControlCd;

    /**
     * Gets the value of the receiptId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * Sets the value of the receiptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptId(String value) {
        this.receiptId = value;
    }

    /**
     * Gets the value of the transmittalControlCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmittalControlCd() {
        return transmittalControlCd;
    }

    /**
     * Sets the value of the transmittalControlCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmittalControlCd(String value) {
        this.transmittalControlCd = value;
    }

}
