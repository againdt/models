package com.getinsured.hix.dto.agency;


import java.util.List;

import com.getinsured.hix.model.agency.assistant.AgencyAssistant.ApprovalStatus;

public class AgencyAssistantInfoDto {

	String id;
	String firstName;
	String lastName;
	String primaryContactNumber;
	String businessContactNumber;
	String personalEmailAddress;
	String businessEmailAddress;
	String communicationPref;
	String businessLegalName;
	AgencyLocationDto businessAddress;
	AgencyLocationDto correspondenceAddress;
	String assistantRole;
	ApprovalStatus approvalStatus;
	String status;
	String agencyId;
	List<AgencySiteLocationDto> agencySites;
	String siteId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPrimaryContactNumber() {
		return primaryContactNumber;
	}
	public void setPrimaryContactNumber(String primaryContactNumber) {
		this.primaryContactNumber = primaryContactNumber;
	}
	public String getBusinessContactNumber() {
		return businessContactNumber;
	}
	public void setBusinessContactNumber(String businessContactNumber) {
		this.businessContactNumber = businessContactNumber;
	}
	public String getPersonalEmailAddress() {
		return personalEmailAddress;
	}
	public void setPersonalEmailAddress(String personalEmailAddress) {
		this.personalEmailAddress = personalEmailAddress;
	}
	public String getBusinessEmailAddress() {
		return businessEmailAddress;
	}
	public void setBusinessEmailAddress(String businessEmailAddress) {
		this.businessEmailAddress = businessEmailAddress;
	}
	public String getBusinessLegalName() {
		return businessLegalName;
	}
	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}
	public AgencyLocationDto getBusinessAddress() {
		return businessAddress;
	}
	public void setBusinessAddress(AgencyLocationDto businessAddress) {
		this.businessAddress = businessAddress;
	}
	public AgencyLocationDto getCorrespondenceAddress() {
		return correspondenceAddress;
	}
	public void setCorrespondenceAddress(AgencyLocationDto correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}
	public String getAssistantRole() {
		return assistantRole;
	}
	public void setAssistantRole(String assistantRole) {
		this.assistantRole = assistantRole;
	}
	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCommunicationPref() {
		return communicationPref;
	}
	public void setCommunicationPref(String communicationPref) {
		this.communicationPref = communicationPref;
	}
	public String getAgencyId() {
		return agencyId;
	}
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}
	public List<AgencySiteLocationDto> getAgencySites() {
		return agencySites;
	}
	public void setAgencySites(List<AgencySiteLocationDto> agencySites) {
		this.agencySites = agencySites;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	
}
