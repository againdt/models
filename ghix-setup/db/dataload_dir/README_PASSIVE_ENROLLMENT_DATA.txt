## To Setup

1. copy the following new files to /home/oracle/dataload_dir

setup_load4PASSIVE_ENROLLMENT_DATA.sh
refresh_PASSIVE_ENROLLMENT_DATA.sh
PASSIVE_ENROLLMENT_DATA.CSV
EXT_PASSIVE_ENROLLMENT_DATA.sql
EXT_2load_PASSIVE_ENROLLMENT_DATA.sql

2.
 DB_ENV.sh should be already exists.
 Double check if it configures appropriately.

3.
cd /home/oracle/dataload_dir
./setup_load4PASSIVE_ENROLLMENT_DATA.sh <schema_owner> <password>
Example
./setup_load4PASSIVE_ENROLLMENT_DATA.sh iexdev iexdevpassword

## To Run
/home/oracle/dataload_dir
1. After receive <new file>.csv, cp it to /home/oracle/dataload_dir

2. cp <new file>.csv PASSIVE_ENROLLMENT_DATA.CSV
   Example
      cp Deidentified_DATA_TEST_JT.csv PASSIVE_ENROLLMENT_DATA.CSV

      3. (Optional)
      cd /home/oracle/dataload_dir
      ./setup_load4PASSIVE_ENROLLMENT_DATA.sh <schema_owner> <password>
      Example
      ./setup_load4PASSIVE_ENROLLMENT_DATA.sh iexdev iexdevpassword

      4.
      cd /home/oracle/dataload_dir
      ./refresh_PASSIVE_ENROLLMENT_DATA.sh <schema_owner> <password>
      Example
      ./refresh_PASSIVE_ENROLLMENT_DATA.sh iexdev iexdevpassword



Remarks
The following DB objects will be created through liquibase
 SEQUENCE PASSIVE_ENROLLMENT_DATA_SEQ
 SEQUENCE PASSIVE_ENROLLMENT_BATCH_SEQ
 TABLE PASSIVE_ENROLLMENT_DATA_BATCH 
 TABLE PASSIVE_ENROLLMENT_DATA

