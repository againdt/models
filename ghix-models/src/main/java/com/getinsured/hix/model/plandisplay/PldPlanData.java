package com.getinsured.hix.model.plandisplay;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.model.GHIXResponse;

public class PldPlanData extends GHIXResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6521857876170611201L;
	private Map<String, String> plan;
	private List<PldMemberData> memberInfoList;
	private String existingEnrollmentId;
	private List<PldMemberData> newMemberInfoList;
	private List<PldMemberData> existingMemberInfoList;
	private String createNewEnrollment;
	private String newEnrollmentReason;
	private PlanResponse planResponse;
	private String qleReason;
	private boolean terminationFlag;
	private Date financialEffectiveDate;
	public Map<String, String> getPlan() {
		return plan;
	}
	public void setPlan(Map<String, String> plan) {
		this.plan = plan;
	}
	public List<PldMemberData> getMemberInfoList() {
		return memberInfoList;
	}
	public void setMemberInfoList(List<PldMemberData> memberInfoList) {
		this.memberInfoList = memberInfoList;
	}
	public List<PldMemberData> getNewMemberInfoList() {
		return newMemberInfoList;
	}
	public void setNewMemberInfoList(List<PldMemberData> newMemberInfoList) {
		this.newMemberInfoList = newMemberInfoList;
	}
	public List<PldMemberData> getExistingMemberInfoList() {
		return existingMemberInfoList;
	}
	public void setExistingMemberInfoList(List<PldMemberData> existingMemberInfoList) {
		this.existingMemberInfoList = existingMemberInfoList;
	}
	public String getExistingEnrollmentId() {
		return existingEnrollmentId;
	}
	public void setExistingEnrollmentId(String existingEnrollmentId) {
		this.existingEnrollmentId = existingEnrollmentId;
	}
	public String getCreateNewEnrollment() {
		return createNewEnrollment;
	}
	public void setCreateNewEnrollment(String createNewEnrollment) {
		this.createNewEnrollment = createNewEnrollment;
	}
	public String getNewEnrollmentReason() {
		return newEnrollmentReason;
	}
	public void setNewEnrollmentReason(String newEnrollmentReason) {
		this.newEnrollmentReason = newEnrollmentReason;
	}
	public PlanResponse getPlanResponse() {
		return planResponse;
	}
	public void setPlanResponse(PlanResponse planResponse) {
		this.planResponse = planResponse;
	}
	public String getQleReason() {
		return qleReason;
	}
	public void setQleReason(String qleReason) {
		this.qleReason = qleReason;
	}
	public boolean isTerminationFlag() {
		return terminationFlag;
	}
	public void setTerminationFlag(boolean terminationFlag) {
		this.terminationFlag = terminationFlag;
	}
	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}
	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}
	
}
