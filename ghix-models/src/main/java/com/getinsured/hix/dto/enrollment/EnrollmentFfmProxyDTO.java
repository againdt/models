/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author negi_s
 *
 */
public class EnrollmentFfmProxyDTO implements Serializable{
	
	//HIX-69022 enum to distinguish the different proxy flows
	public static enum Flow{ FFM_PROXY, CA_PROXY }

	// Partner/ Consumer Identification
	private String informationExchangeSystemId;
	private String userType;
	private String stateExchangeCode;
	private String partnerAssignedConsumerId;
	
	// User Identity Assertion
	private String ffeAssignedConsumerId;
	private String ffeUserId;
	private String uiaFirstName;
	private String uiaMiddleName;
	private String uiaLastName;
	
	// Agent/Broker Information
	private String agentOrBrokerIndicator;
	private String agentOrBrokerNationalProducerNumber;
	private String agentOrBrokerFirstName;
	private String agentOrBrokerMiddleName;
	private String agentOrBrokerLastName;
	private String agentOrBrokerNameSuffix;

	// APTC Attestation Information
	private String aptcAttestationFirstName;
	private String aptcAttestationMiddleName;
	private String aptcAttestationLastName;
	private String aptcAttestationSuffixName;
	private String aptcAttestationId;
	private String aptcAttestation;
	private String aptcAttestationSsn;
	private String aptcAttestationDob;
	private String aptcAttestationGender;

	// Enrollment Group
	private String enrollmentGroupFFEAssignedPolicyNumber;
	private String enrollmentGroupIssuerPolicyNumber;
	private String enrollmentGroupStartDate;
	private String enrollmentGroupEndDate;
	private String enrollmentGroupIssuerId;
	private String enrollmentGroupAssignedPlanId;
	private String enrollmentGroupTransactionType;

	// Member Actions 
	private String memberFFEAssignedApplicantId;
	private String memberIssuerAssignedMemberId;
	private String memberFFEAssignedMemberId;
	private String memberSubscriberIndicator;
	private String memberRelationshipToSubscriber;
	private String memberTobaccoUseIndicator;
	private String memberLastDateOfTobaccoUse;
	private String memberEnrollmentPeriodType;
	private String memberTypeCode;
	private String memberReasonCode;
	private String memberActionEffectiveDate;

	// Premium Information
	private String premiumRatingArea;
	private Float premiumTotalPremiumAmount;
	private Float premiumAPTCElectedPercentage;
	private Float premiumAPTCAppliedAmount;
	private Float premiumTotalIndividualResponsibilityAmount;
	private String premiumCSRLevelApplicable;
	
	private Integer cmrHouseholdId;
	private Long ssapApplicationId;
	private Integer planId;
	private String issuerId;
	
	private String securablesApplicationData; //securables.getApplicationData()
	private String targetId; //securables.getSecurableTarget.getTargetId(); Same as the GI user object id of the agent
	
	private Flow flow;
	
	
	List<EnrolleeFfmProxyDTO> enrolleeList = new ArrayList<EnrolleeFfmProxyDTO>();

	public String getInformationExchangeSystemId() {
		return informationExchangeSystemId;
	}

	public void setInformationExchangeSystemId(String informationExchangeSystemId) {
		this.informationExchangeSystemId = informationExchangeSystemId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getStateExchangeCode() {
		return stateExchangeCode;
	}

	public void setStateExchangeCode(String stateExchangeCode) {
		this.stateExchangeCode = stateExchangeCode;
	}

	public String getPartnerAssignedConsumerId() {
		return partnerAssignedConsumerId;
	}

	public void setPartnerAssignedConsumerId(String partnerAssignedConsumerId) {
		this.partnerAssignedConsumerId = partnerAssignedConsumerId;
	}

	public String getFfeAssignedConsumerId() {
		return ffeAssignedConsumerId;
	}

	public void setFfeAssignedConsumerId(String ffeAssignedConsumerId) {
		this.ffeAssignedConsumerId = ffeAssignedConsumerId;
	}

	public String getFfeUserId() {
		return ffeUserId;
	}

	public void setFfeUserId(String ffeUserId) {
		this.ffeUserId = ffeUserId;
	}

	public String getUiaFirstName() {
		return uiaFirstName;
	}

	public void setUiaFirstName(String uiaFirstName) {
		this.uiaFirstName = uiaFirstName;
	}

	public String getUiaMiddleName() {
		return uiaMiddleName;
	}

	public void setUiaMiddleName(String uiaMiddleName) {
		this.uiaMiddleName = uiaMiddleName;
	}

	public String getUiaLastName() {
		return uiaLastName;
	}

	public void setUiaLastName(String uiaLastName) {
		this.uiaLastName = uiaLastName;
	}

	public String getAgentOrBrokerIndicator() {
		return agentOrBrokerIndicator;
	}

	public void setAgentOrBrokerIndicator(String agentOrBrokerIndicator) {
		this.agentOrBrokerIndicator = agentOrBrokerIndicator;
	}

	public String getAgentOrBrokerNationalProducerNumber() {
		return agentOrBrokerNationalProducerNumber;
	}

	public void setAgentOrBrokerNationalProducerNumber(
			String agentOrBrokerNationalProducerNumber) {
		this.agentOrBrokerNationalProducerNumber = agentOrBrokerNationalProducerNumber;
	}

	public String getAgentOrBrokerFirstName() {
		return agentOrBrokerFirstName;
	}

	public void setAgentOrBrokerFirstName(String agentOrBrokerFirstName) {
		this.agentOrBrokerFirstName = agentOrBrokerFirstName;
	}

	public String getAgentOrBrokerMiddleName() {
		return agentOrBrokerMiddleName;
	}

	public void setAgentOrBrokerMiddleName(String agentOrBrokerMiddleName) {
		this.agentOrBrokerMiddleName = agentOrBrokerMiddleName;
	}

	public String getAgentOrBrokerLastName() {
		return agentOrBrokerLastName;
	}

	public void setAgentOrBrokerLastName(String agentOrBrokerLastName) {
		this.agentOrBrokerLastName = agentOrBrokerLastName;
	}

	public String getAgentOrBrokerNameSuffix() {
		return agentOrBrokerNameSuffix;
	}

	public void setAgentOrBrokerNameSuffix(String agentOrBrokerNameSuffix) {
		this.agentOrBrokerNameSuffix = agentOrBrokerNameSuffix;
	}

	public String getAptcAttestationFirstName() {
		return aptcAttestationFirstName;
	}

	public void setAptcAttestationFirstName(String aptcAttestationFirstName) {
		this.aptcAttestationFirstName = aptcAttestationFirstName;
	}

	public String getAptcAttestationMiddleName() {
		return aptcAttestationMiddleName;
	}

	public void setAptcAttestationMiddleName(String aptcAttestationMiddleName) {
		this.aptcAttestationMiddleName = aptcAttestationMiddleName;
	}

	public String getAptcAttestationLastName() {
		return aptcAttestationLastName;
	}

	public void setAptcAttestationLastName(String aptcAttestationLastName) {
		this.aptcAttestationLastName = aptcAttestationLastName;
	}

	public String getAptcAttestationSuffixName() {
		return aptcAttestationSuffixName;
	}

	public void setAptcAttestationSuffixName(String aptcAttestationSuffixName) {
		this.aptcAttestationSuffixName = aptcAttestationSuffixName;
	}

	public String getAptcAttestationId() {
		return aptcAttestationId;
	}

	public void setAptcAttestationId(String aptcAttestationId) {
		this.aptcAttestationId = aptcAttestationId;
	}

	public String getAptcAttestation() {
		return aptcAttestation;
	}

	public void setAptcAttestation(String aptcAttestation) {
		this.aptcAttestation = aptcAttestation;
	}

	public String getAptcAttestationSsn() {
		return aptcAttestationSsn;
	}

	public void setAptcAttestationSsn(String aptcAttestationSsn) {
		this.aptcAttestationSsn = aptcAttestationSsn;
	}

	public String getAptcAttestationDob() {
		return aptcAttestationDob;
	}

	public void setAptcAttestationDob(String aptcAttestationDob) {
		this.aptcAttestationDob = aptcAttestationDob;
	}

	public String getAptcAttestationGender() {
		return aptcAttestationGender;
	}

	public void setAptcAttestationGender(String aptcAttestationGender) {
		this.aptcAttestationGender = aptcAttestationGender;
	}

	public String getEnrollmentGroupFFEAssignedPolicyNumber() {
		return enrollmentGroupFFEAssignedPolicyNumber;
	}

	public void setEnrollmentGroupFFEAssignedPolicyNumber(
			String enrollmentGroupFFEAssignedPolicyNumber) {
		this.enrollmentGroupFFEAssignedPolicyNumber = enrollmentGroupFFEAssignedPolicyNumber;
	}

	public String getEnrollmentGroupIssuerPolicyNumber() {
		return enrollmentGroupIssuerPolicyNumber;
	}

	public void setEnrollmentGroupIssuerPolicyNumber(
			String enrollmentGroupIssuerPolicyNumber) {
		this.enrollmentGroupIssuerPolicyNumber = enrollmentGroupIssuerPolicyNumber;
	}

	public String getEnrollmentGroupStartDate() {
		return enrollmentGroupStartDate;
	}

	public void setEnrollmentGroupStartDate(String enrollmentGroupStartDate) {
		this.enrollmentGroupStartDate = enrollmentGroupStartDate;
	}

	public String getEnrollmentGroupEndDate() {
		return enrollmentGroupEndDate;
	}

	public void setEnrollmentGroupEndDate(String enrollmentGroupEndDate) {
		this.enrollmentGroupEndDate = enrollmentGroupEndDate;
	}

	public String getEnrollmentGroupIssuerId() {
		return enrollmentGroupIssuerId;
	}

	public void setEnrollmentGroupIssuerId(String enrollmentGroupIssuerId) {
		this.enrollmentGroupIssuerId = enrollmentGroupIssuerId;
	}

	public String getEnrollmentGroupAssignedPlanId() {
		return enrollmentGroupAssignedPlanId;
	}

	public void setEnrollmentGroupAssignedPlanId(
			String enrollmentGroupAssignedPlanId) {
		this.enrollmentGroupAssignedPlanId = enrollmentGroupAssignedPlanId;
	}

	public String getEnrollmentGroupTransactionType() {
		return enrollmentGroupTransactionType;
	}

	public void setEnrollmentGroupTransactionType(
			String enrollmentGroupTransactionType) {
		this.enrollmentGroupTransactionType = enrollmentGroupTransactionType;
	}

	public String getMemberFFEAssignedApplicantId() {
		return memberFFEAssignedApplicantId;
	}

	public void setMemberFFEAssignedApplicantId(String memberFFEAssignedApplicantId) {
		this.memberFFEAssignedApplicantId = memberFFEAssignedApplicantId;
	}

	public String getMemberIssuerAssignedMemberId() {
		return memberIssuerAssignedMemberId;
	}

	public void setMemberIssuerAssignedMemberId(String memberIssuerAssignedMemberId) {
		this.memberIssuerAssignedMemberId = memberIssuerAssignedMemberId;
	}

	public String getMemberFFEAssignedMemberId() {
		return memberFFEAssignedMemberId;
	}

	public void setMemberFFEAssignedMemberId(String memberFFEAssignedMemberId) {
		this.memberFFEAssignedMemberId = memberFFEAssignedMemberId;
	}

	public String getMemberSubscriberIndicator() {
		return memberSubscriberIndicator;
	}

	public void setMemberSubscriberIndicator(String memberSubscriberIndicator) {
		this.memberSubscriberIndicator = memberSubscriberIndicator;
	}

	public String getMemberRelationshipToSubscriber() {
		return memberRelationshipToSubscriber;
	}

	public void setMemberRelationshipToSubscriber(
			String memberRelationshipToSubscriber) {
		this.memberRelationshipToSubscriber = memberRelationshipToSubscriber;
	}

	public String getMemberTobaccoUseIndicator() {
		return memberTobaccoUseIndicator;
	}

	public void setMemberTobaccoUseIndicator(String memberTobaccoUseIndicator) {
		this.memberTobaccoUseIndicator = memberTobaccoUseIndicator;
	}

	public String getMemberLastDateOfTobaccoUse() {
		return memberLastDateOfTobaccoUse;
	}

	public void setMemberLastDateOfTobaccoUse(String memberLastDateOfTobaccoUse) {
		this.memberLastDateOfTobaccoUse = memberLastDateOfTobaccoUse;
	}

	public String getMemberEnrollmentPeriodType() {
		return memberEnrollmentPeriodType;
	}

	public void setMemberEnrollmentPeriodType(String memberEnrollmentPeriodType) {
		this.memberEnrollmentPeriodType = memberEnrollmentPeriodType;
	}

	public String getMemberTypeCode() {
		return memberTypeCode;
	}

	public void setMemberTypeCode(String memberTypeCode) {
		this.memberTypeCode = memberTypeCode;
	}

	public String getMemberReasonCode() {
		return memberReasonCode;
	}

	public void setMemberReasonCode(String memberReasonCode) {
		this.memberReasonCode = memberReasonCode;
	}

	public String getMemberActionEffectiveDate() {
		return memberActionEffectiveDate;
	}

	public void setMemberActionEffectiveDate(String memberActionEffectiveDate) {
		this.memberActionEffectiveDate = memberActionEffectiveDate;
	}

	public String getPremiumRatingArea() {
		return premiumRatingArea;
	}

	public void setPremiumRatingArea(String premiumRatingArea) {
		this.premiumRatingArea = premiumRatingArea;
	}

	public Float getPremiumTotalPremiumAmount() {
		return premiumTotalPremiumAmount;
	}

	public void setPremiumTotalPremiumAmount(Float premiumTotalPremiumAmount) {
		this.premiumTotalPremiumAmount = premiumTotalPremiumAmount;
	}

	public Float getPremiumAPTCElectedPercentage() {
		return premiumAPTCElectedPercentage;
	}

	public void setPremiumAPTCElectedPercentage(Float premiumAPTCElectedPercentage) {
		this.premiumAPTCElectedPercentage = premiumAPTCElectedPercentage;
	}

	public Float getPremiumAPTCAppliedAmount() {
		return premiumAPTCAppliedAmount;
	}

	public void setPremiumAPTCAppliedAmount(Float premiumAPTCAppliedAmount) {
		this.premiumAPTCAppliedAmount = premiumAPTCAppliedAmount;
	}

	public Float getPremiumTotalIndividualResponsibilityAmount() {
		return premiumTotalIndividualResponsibilityAmount;
	}

	public void setPremiumTotalIndividualResponsibilityAmount(
			Float premiumTotalIndividualResponsibilityAmount) {
		this.premiumTotalIndividualResponsibilityAmount = premiumTotalIndividualResponsibilityAmount;
	}

	public String getPremiumCSRLevelApplicable() {
		return premiumCSRLevelApplicable;
	}

	public void setPremiumCSRLevelApplicable(String premiumCSRLevelApplicable) {
		this.premiumCSRLevelApplicable = premiumCSRLevelApplicable;
	}

	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getSecurablesApplicationData() {
		return securablesApplicationData;
	}

	public void setSecurablesApplicationData(String securablesApplicationData) {
		this.securablesApplicationData = securablesApplicationData;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public List<EnrolleeFfmProxyDTO> getEnrolleeList() {
		return enrolleeList;
	}

	public void setEnrolleeList(List<EnrolleeFfmProxyDTO> enrolleeList) {
		this.enrolleeList = enrolleeList;
	}

	public Flow getFlow() {
		return flow;
	}

	public void setFlow(Flow flow) {
		this.flow = flow;
	}

	@Override
	public String toString() {
		return "EnrollmentFfmProxyDTO [informationExchangeSystemId="
				+ informationExchangeSystemId + ", userType=" + userType
				+ ", stateExchangeCode=" + stateExchangeCode
				+ ", partnerAssignedConsumerId=" + partnerAssignedConsumerId
				+ ", ffeAssignedConsumerId=" + ffeAssignedConsumerId
				+ ", ffeUserId=" + ffeUserId + ", uiaFirstName=" + uiaFirstName
				+ ", uiaMiddleName=" + uiaMiddleName + ", uiaLastName="
				+ uiaLastName + ", agentOrBrokerIndicator="
				+ agentOrBrokerIndicator
				+ ", agentOrBrokerNationalProducerNumber="
				+ agentOrBrokerNationalProducerNumber
				+ ", agentOrBrokerFirstName=" + agentOrBrokerFirstName
				+ ", agentOrBrokerMiddleName=" + agentOrBrokerMiddleName
				+ ", agentOrBrokerLastName=" + agentOrBrokerLastName
				+ ", agentOrBrokerNameSuffix=" + agentOrBrokerNameSuffix
				+ ", aptcAttestationFirstName=" + aptcAttestationFirstName
				+ ", aptcAttestationMiddleName=" + aptcAttestationMiddleName
				+ ", aptcAttestationLastName=" + aptcAttestationLastName
				+ ", aptcAttestationSuffixName=" + aptcAttestationSuffixName
				+ ", aptcAttestationId=" + aptcAttestationId
				+ ", aptcAttestation=" + aptcAttestation
				+ ", aptcAttestationSsn=" + aptcAttestationSsn
				+ ", aptcAttestationDob=" + aptcAttestationDob
				+ ", aptcAttestationGender=" + aptcAttestationGender
				+ ", enrollmentGroupFFEAssignedPolicyNumber="
				+ enrollmentGroupFFEAssignedPolicyNumber
				+ ", enrollmentGroupIssuerPolicyNumber="
				+ enrollmentGroupIssuerPolicyNumber
				+ ", enrollmentGroupStartDate=" + enrollmentGroupStartDate
				+ ", enrollmentGroupEndDate=" + enrollmentGroupEndDate
				+ ", enrollmentGroupIssuerId=" + enrollmentGroupIssuerId
				+ ", enrollmentGroupAssignedPlanId="
				+ enrollmentGroupAssignedPlanId
				+ ", enrollmentGroupTransactionType="
				+ enrollmentGroupTransactionType
				+ ", memberFFEAssignedApplicantId="
				+ memberFFEAssignedApplicantId
				+ ", memberIssuerAssignedMemberId="
				+ memberIssuerAssignedMemberId + ", memberFFEAssignedMemberId="
				+ memberFFEAssignedMemberId + ", memberSubscriberIndicator="
				+ memberSubscriberIndicator
				+ ", memberRelationshipToSubscriber="
				+ memberRelationshipToSubscriber
				+ ", memberTobaccoUseIndicator=" + memberTobaccoUseIndicator
				+ ", memberLastDateOfTobaccoUse=" + memberLastDateOfTobaccoUse
				+ ", memberEnrollmentPeriodType=" + memberEnrollmentPeriodType
				+ ", memberTypeCode=" + memberTypeCode + ", memberReasonCode="
				+ memberReasonCode + ", memberActionEffectiveDate="
				+ memberActionEffectiveDate + ", premiumRatingArea="
				+ premiumRatingArea + ", premiumTotalPremiumAmount="
				+ premiumTotalPremiumAmount + ", premiumAPTCElectedPercentage="
				+ premiumAPTCElectedPercentage + ", premiumAPTCAppliedAmount="
				+ premiumAPTCAppliedAmount
				+ ", premiumTotalIndividualResponsibilityAmount="
				+ premiumTotalIndividualResponsibilityAmount
				+ ", premiumCSRLevelApplicable=" + premiumCSRLevelApplicable
				+ ", cmrHouseholdId=" + cmrHouseholdId + ", ssapApplicationId="
				+ ssapApplicationId + ", planId=" + planId + ", issuerId="
				+ issuerId + ", securablesApplicationData="
				+ securablesApplicationData + ", targetId=" + targetId
				+ ", flow=" + flow + ", enrolleeList=" + enrolleeList + "]";
	}
}
