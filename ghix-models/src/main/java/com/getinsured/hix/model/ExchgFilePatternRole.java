package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the EXCHG_FilePatternRole database table.
 * 
 */
@Entity
@Table(name="EXCHG_FilePatternRole")
public class ExchgFilePatternRole implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exchg_filepattern_Seq")
	@SequenceGenerator(name = "exchg_filepattern_Seq", sequenceName = "EXCHG_FILEPATTERN_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="FilePattern_IDs")
	private String filePatternIds;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFilePatternIds() {
		return filePatternIds;
	}

	public void setFilePatternIds(String filePatternIds) {
		this.filePatternIds = filePatternIds;
	}
	
}
