package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.PdOrderItemPerson;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;

public class PdOrderItemPersonDTO extends PdOrderItemPerson {

	private static final long serialVersionUID = 1L;
	private String externalId;
	private Relationship relationship;
	private Long pdPersonId;
	
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	public Relationship getRelationship() {
		return relationship;
	}

	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}

	public Long getPdPersonId() {
		return pdPersonId;
	}

	public void setPdPersonId(Long pdPersonId) {
		this.pdPersonId = pdPersonId;
	}	
	
}