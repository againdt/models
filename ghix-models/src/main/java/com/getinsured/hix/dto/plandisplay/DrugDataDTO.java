package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to retrieve data from DRUGDATA table using DrugData Model.
 * 
 * @since October 23, 2018
 */
public class DrugDataDTO {

	private String rxcui;
	private String name;
	private String strength;
	private String route;
	private String fullName;
	private String rxTermDosageForm;
	private String rxNormDosageForm;
	private String type;
	private String genericName;
	private String genericRxcui;
	private String status;
	private String remappedTo;

	public DrugDataDTO() {
	}

	public DrugDataDTO(String rxcui, String name, String strength, String route, String fullName,
			String rxTermDosageForm, String rxNormDosageForm, String type, String genericName, String genericRxcui,
			String status, String remappedTo) {
		this.rxcui = rxcui;
		this.name = name;
		this.strength = strength;
		this.route = route;
		this.fullName = fullName;
		this.rxTermDosageForm = rxTermDosageForm;
		this.rxNormDosageForm = rxNormDosageForm;
		this.type = type;
		this.genericName = genericName;
		this.genericRxcui = genericRxcui;
		this.status = status;
		this.remappedTo = remappedTo;
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getRxcui() {
		return rxcui;
	}

	public void setRxcui(String rxcui) {
		this.rxcui = rxcui;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getRxTermDosageForm() {
		return rxTermDosageForm;
	}

	public void setRxTermDosageForm(String rxTermDosageForm) {
		this.rxTermDosageForm = rxTermDosageForm;
	}

	public String getRxNormDosageForm() {
		return rxNormDosageForm;
	}

	public void setRxNormDosageForm(String rxNormDosageForm) {
		this.rxNormDosageForm = rxNormDosageForm;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGenericName() {
		return genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	public String getGenericRxcui() {
		return genericRxcui;
	}

	public void setGenericRxcui(String genericRxcui) {
		this.genericRxcui = genericRxcui;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemappedTo() {
		return remappedTo;
	}

	public void setRemappedTo(String remappedTo) {
		this.remappedTo = remappedTo;
	}
}
