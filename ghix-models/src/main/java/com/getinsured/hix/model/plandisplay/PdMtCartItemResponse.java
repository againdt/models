package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;

public class PdMtCartItemResponse {
	
	public enum ErrorCodes{
		SUCCESS(200, "SUCCESS"),
		VALIDATION_ERROR(400, "VALIDATION_ERROR"),
		NOT_FOUND(404, "NOT_FOUND"),
		SYSTEM_ERROR(500, "SYSTEM_ERROR");
		
		
		private int id;
		private String message;
		private ErrorCodes(int id,String message){
			this.id=id;
			this.message=message;
		}
		
		public int getId() {
			return id;
		}
		
		public String getMessage() {
			return message;
		}
	}
	public static enum ApplicationStatus { ACTIVE, DELETED; }	
	public static enum YesOrNo { YES, NO; }	
	public static enum MaleOrFemale { MALE, FEMALE; }	
	public static enum ExpenseEstimate { LOW, AVERAGE, PRICEY; }	
	
	private Integer status;
	private String message;
	private List<Map<String,String>> errors;
	private String timeStamp;
	private String leadId;
	private List<PdMtCartItem> cartItemList;
	private PdPreferencesDTO pdPreferencesDTO;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String meassage) {
		this.message = meassage;
	}
	public List<Map<String, String>> getErrors() {
		return errors;
	}
	public void setErrors(List<Map<String, String>> errors) {
		this.errors = errors;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public List<PdMtCartItem> getCartItemList() {
		return cartItemList;
	}
	public void setCartItemList(List<PdMtCartItem> cartItemList) {
		this.cartItemList = cartItemList;
	}
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}	
}
