package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CMR_DOCUMENTS")
public class ConsumerDocument implements Serializable {

	/**
	 * Model class representing the consumer documents.
	 */
	private static final long serialVersionUID = 4381495603088374952L;

	@Id
	@SequenceGenerator(name="CMR_DOCUMENTS_ID_GENERATOR", sequenceName="CMR_DOCUMENTS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CMR_DOCUMENTS_ID_GENERATOR")
	@Column(unique=true, nullable = false, precision=22)
	private Long id;
	
	@Column(name = "ECM_DOCUMENT_ID")
	private String ecmDocumentId;

	@Column(name = "DOCUMENT_NAME")
	private String documentName;

	@Column(name = "DOCUMENT_CATEGORY")
	private String documentCategory;
	
	@Column(name = "DOCUMENT_TYPE")
	private String documentType;
	
	@Column(name = "TARGET_ID")
	private Long targetId;

	@Column(name = "TARGET_NAME")
	private String targetName;
	
	@Column(name = "ACCEPTED")
	private String accepted;

	@Column(name = "COMMENTS")
	private String comments;
	
	@Column(name="CREATED_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "CREATED_BY")
	private Long createdBy;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEcmDocumentId() {
		return ecmDocumentId;
	}

	public void setEcmDocumentId(String ecmDocumentId) {
		this.ecmDocumentId = ecmDocumentId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getAccepted() {
		return accepted;
	}

	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreatedDate(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setCreatedDate(new TSDate()); 
	}
}
