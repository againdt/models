package com.getinsured.hix.dto.finance;

import java.math.BigDecimal;
import java.util.Date;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AdjustmentReasonCode;
import com.getinsured.hix.model.PaymentMethods;

/**
 * @since 26th June, 2013
 * @author Kuldeep
 *
 */
public class FinanceRequest
{
	private int id;
	private float paidAmt;
	private PaymentMethods paymentMethods;
	private String isActiveKey;
	private Date paymentDueDate;
	
	/**
	 * New fields added for Jira ID HIX-49000
	 */
	private int employer_invoiceID;
	private int employer_Invoice_lineitemID;
	private BigDecimal manual_adjustment_amount;
	private BigDecimal manual_employer_contribution;
	private BigDecimal manual_employee_contribution;
	private AdjustmentReasonCode adjustmentReasonCode;
	private String comments;
	private AccountUser accountUser;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the paidamt
	 */
	public float getPaidAmt() {
		return paidAmt;
	}
	/**
	 * @param paidamt the paidamt to set
	 */
	public void setPaidAmt(float paidAmt) {
		this.paidAmt = paidAmt;
	}
	/**
	 * @return the paymentMethods
	 */
	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}
	/**
	 * @param paymentMethods the paymentMethods to set
	 */
	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getIsActiveKey() {
		return isActiveKey;
	}
	
	/**
	 * 
	 * @param isActiveKey
	 */
	public void setIsActiveKey(String isActiveKey) {
		this.isActiveKey = isActiveKey;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getPaymentDueDate() {
		return paymentDueDate;
	}
	
	/**
	 * 
	 * @param paymentDueDate
	 */
	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	/**
	 * @return the employer_invoiceID
	 */
	public int getEmployer_invoiceID() {
		return employer_invoiceID;
	}
	/**
	 * @param employer_invoiceID the employer_invoiceID to set
	 */
	public void setEmployer_invoiceID(int employer_invoiceID) {
		this.employer_invoiceID = employer_invoiceID;
	}
	/**
	 * @return the employer_Invoice_lineitemID
	 */
	public int getEmployer_Invoice_lineitemID() {
		return employer_Invoice_lineitemID;
	}
	/**
	 * @param employer_Invoice_lineitemID the employer_Invoice_lineitemID to set
	 */
	public void setEmployer_Invoice_lineitemID(int employer_Invoice_lineitemID) {
		this.employer_Invoice_lineitemID = employer_Invoice_lineitemID;
	}
	/**
	 * @return the manual_adjustment_amount
	 */
	public BigDecimal getManual_adjustment_amount() {
		return manual_adjustment_amount;
	}
	/**
	 * @param manual_adjustment_amount the manual_adjustment_amount to set
	 */
	public void setManual_adjustment_amount(BigDecimal manual_adjustment_amount) {
		this.manual_adjustment_amount = manual_adjustment_amount;
	}
	/**
	 * @return the manual_employer_contribution
	 */
	public BigDecimal getManual_employer_contribution() {
		return manual_employer_contribution;
	}
	/**
	 * @param manual_employer_contribution the manual_employer_contribution to set
	 */
	public void setManual_employer_contribution(
			BigDecimal manual_employer_contribution) {
		this.manual_employer_contribution = manual_employer_contribution;
	}
	/**
	 * @return the manual_employee_contribution
	 */
	public BigDecimal getManual_employee_contribution() {
		return manual_employee_contribution;
	}
	/**
	 * @param manual_employee_contribution the manual_employee_contribution to set
	 */
	public void setManual_employee_contribution(
			BigDecimal manual_employee_contribution) {
		this.manual_employee_contribution = manual_employee_contribution;
	}
	/**
	 * @return the adjustmentReasonCode
	 */
	public AdjustmentReasonCode getAdjustmentReasonCode() {
		return adjustmentReasonCode;
	}
	/**
	 * @param adjustmentReasonCode the adjustmentReasonCode to set
	 */
	public void setAdjustmentReasonCode(AdjustmentReasonCode adjustmentReasonCode) {
		this.adjustmentReasonCode = adjustmentReasonCode;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the accountUser
	 */
	public AccountUser getAccountUser() {
		return accountUser;
	}
	/**
	 * @param accountUser the accountUser to set
	 */
	public void setAccountUser(AccountUser accountUser) {
		this.accountUser = accountUser;
	}
}
