
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class CitizenshipDocument {

	@Expose
	private String alienNumber;
    @Expose
    private String sevisId;
    @Expose
    private String passportOrDocumentNumber;
    @Expose
    private String passportCountryOfIssuance;
    @Expose
    private String cardNumber;
    @Expose
    private String documentExpirationDate;
    @Expose
    private String visaNumber;
	@Expose
    private String i94Number;
    @Expose
    private String documentDescription;
    
    
  
    

    
    public String getPassportOrDocumentNumber() {
		return passportOrDocumentNumber;
	}

	public void setPassportOrDocumentNumber(String passportOrDocumentNumber) {
		this.passportOrDocumentNumber = passportOrDocumentNumber;
	}

	public String getPassportCountryOfIssuance() {
		return passportCountryOfIssuance;
	}

	public void setPassportCountryOfIssuance(String passportCountryOfIssuance) {
		this.passportCountryOfIssuance = passportCountryOfIssuance;
	}

	

    public String getSevisId() {
		return sevisId;
	}

	public void setSevisId(String sevisId) {
		this.sevisId = sevisId;
	}

	public String getAlienNumber() {
        return alienNumber;
    }

    public void setAlienNumber(String alienNumber) {
        this.alienNumber = alienNumber;
    }

    public String getDocumentDescription() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription = documentDescription;
    }

   
    public String getVisaNumber() {
        return visaNumber;
    }

    public void setVisaNumber(String visaNumber) {
        this.visaNumber = visaNumber;
    }

    public String getDocumentExpirationDate() {
        return documentExpirationDate;
    }

    public void setDocumentExpirationDate(String documentExpirationDate) {
        this.documentExpirationDate = documentExpirationDate;
    }

    public String getI94Number() {
        return i94Number;
    }

    public void setI94Number(String i94Number) {
        this.i94Number = i94Number;
    }

    

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    

}
