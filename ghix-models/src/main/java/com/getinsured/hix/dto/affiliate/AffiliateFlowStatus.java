package com.getinsured.hix.dto.affiliate;

/**
 * Enum to represent Flow Status.
 * 
 * @author Ekram Ali Kazi
 */
public enum AffiliateFlowStatus
{
	/**
	 * REGISTERED
	 *
	REGISTERED("Registered"),

	/**
	 * ACTIVE
	 */
	ACTIVE("Active"),

	/**
	 * INACTIVE
	 */
	INACTIVE("Inactive"),

	/**
	 * SUSPENDED
	 */
	SUSPENDED("Suspended");

	private String	description;

	/**
	 * @param description
	 */
	private AffiliateFlowStatus(String description)
	{
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

}