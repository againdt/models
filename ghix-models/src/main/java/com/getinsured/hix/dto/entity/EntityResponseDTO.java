package com.getinsured.hix.dto.entity;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

/**
 * Encapsulates the response received from AHBX webservices.
 */
public class EntityResponseDTO extends GHIXResponse implements Serializable
{
	private static final long serialVersionUID = 1L;

	private int responseCode;

	private long brokerId;

	private String responseDescription;
	private String brokerDelegationCode;

	// Added for Enrollment Rest URL support
	private String brokerName;
	private String stateLicenseNumber;
	private String stateEINNumber;
	
	private String emailId;//Added as part of HIX-27433
	private String certificationStatus;//Added as part of HIX-27433
	private int count;
	private String role;
	private String agentNPN;
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getBrokerName()
	{
		return brokerName;
	}

	public void setBrokerName(String brokerName)
	{
		this.brokerName = brokerName;
	}

	public String getStateLicenseNumber()
	{
		return stateLicenseNumber;
	}

	public void setStateLicenseNumber(String stateLicenseNumber)
	{
		this.stateLicenseNumber = stateLicenseNumber;
	}

	public String getStateEINNumber()
	{
		return stateEINNumber;
	}

	public void setStateEINNumber(String stateEINNumber)
	{
		this.stateEINNumber = stateEINNumber;
	}

	public void setResponseCode(int responseCode)
	{
		this.responseCode = responseCode;
	}

	public void setResponseDescription(String responseDescription)
	{
		this.responseDescription = responseDescription;
	}

	public int getResponseCode()
	{
		return responseCode;
	}

	public String getResponseDescription()
	{
		return responseDescription;
	}

	public long getBrokerId()
	{
		return brokerId;
	}

	public void setBrokerId(long brokerId)
	{
		this.brokerId = brokerId;
	}

	public String getBrokerDelegationCode()
	{
		return brokerDelegationCode;
	}

	public void setBrokerDelegationCode(String brokerDelegationCode)
	{
		this.brokerDelegationCode = brokerDelegationCode;
	}
	

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAgentNPN() {
		return agentNPN;
	}

	public void setAgentNPN(String agentNPN) {
		this.agentNPN = agentNPN;
	}

	@Override
	public String toString()
	{
		return "EntityResponseDTO details: ResponseCode = "+responseCode+", BrokerId = "+brokerId+
				", ResponseDescription = "+responseDescription+", StateLicenseNumber = "+stateLicenseNumber;
	}
}
