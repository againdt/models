
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.enrollment._1095.cy_2018.treasury.irs.common.CoverageHouseholdType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Form 1095A Affordable Insurance Exchange Group Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-08-21&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;Transmission of 1095A Forms for Up stream&lt;/DescriptionTxt&gt;
 * 					&lt;DataElementID/&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095AAffordableInsExchGrpType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095AAffordableInsExchGrpType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecordSequenceNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TaxYr"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedRecordSequenceNum" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}HealthExchangeId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientPolicyCoverageDtl"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}Recipient"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientSpouse" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}CoverageHousehold" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientMonthlyPolicyInfo" maxOccurs="12"/>
 *       &lt;/sequence>
 *       &lt;attribute name="recordType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="lineNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095AAffordableInsExchGrpType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "recordSequenceNum",
    "taxYr",
    "correctedInd",
    "correctedRecordSequenceNum",
    "healthExchangeId",
    "recipientPolicyCoverageDtl",
    "recipient",
    "recipientSpouse",
    "coverageHousehold",
    "recipientMonthlyPolicyInfo"
})
public class Form1095AAffordableInsExchGrpType {

    @XmlElement(name = "RecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String recordSequenceNum;
    @XmlElement(name = "TaxYr", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String taxYr;
    @XmlElement(name = "CorrectedInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String correctedInd;
    @XmlElement(name = "CorrectedRecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String correctedRecordSequenceNum;
    @XmlElement(name = "HealthExchangeId", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String healthExchangeId;
    @XmlElement(name = "RecipientPolicyCoverageDtl", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected RecipientPolicyCoverageDtlType recipientPolicyCoverageDtl;
    @XmlElement(name = "Recipient", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected PersonInformationType recipient;
    @XmlElement(name = "RecipientSpouse", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected EOYPersonType recipientSpouse;
    @XmlElement(name = "CoverageHousehold", namespace = "urn:us:gov:treasury:irs:common")
    protected List<CoverageHouseholdType> coverageHousehold;
    @XmlElement(name = "RecipientMonthlyPolicyInfo", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected List<RecipientMonthlyPolicyInfoType> recipientMonthlyPolicyInfo;
    @XmlAttribute(name = "recordType")
    protected String recordType;
    @XmlAttribute(name = "lineNum")
    protected BigInteger lineNum;

    /**
     * Gets the value of the recordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordSequenceNum() {
        return recordSequenceNum;
    }

    /**
     * Sets the value of the recordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordSequenceNum(String value) {
        this.recordSequenceNum = value;
    }

    /**
     * Gets the value of the taxYr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxYr() {
        return taxYr;
    }

    /**
     * Sets the value of the taxYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxYr(String value) {
        this.taxYr = value;
    }

    /**
     * Gets the value of the correctedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedInd() {
        return correctedInd;
    }

    /**
     * Sets the value of the correctedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedInd(String value) {
        this.correctedInd = value;
    }

    /**
     * Gets the value of the correctedRecordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedRecordSequenceNum() {
        return correctedRecordSequenceNum;
    }

    /**
     * Sets the value of the correctedRecordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedRecordSequenceNum(String value) {
        this.correctedRecordSequenceNum = value;
    }

    /**
     * Gets the value of the healthExchangeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthExchangeId() {
        return healthExchangeId;
    }

    /**
     * Sets the value of the healthExchangeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthExchangeId(String value) {
        this.healthExchangeId = value;
    }

    /**
     * Gets the value of the recipientPolicyCoverageDtl property.
     * 
     * @return
     *     possible object is
     *     {@link RecipientPolicyCoverageDtlType }
     *     
     */
    public RecipientPolicyCoverageDtlType getRecipientPolicyCoverageDtl() {
        return recipientPolicyCoverageDtl;
    }

    /**
     * Sets the value of the recipientPolicyCoverageDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecipientPolicyCoverageDtlType }
     *     
     */
    public void setRecipientPolicyCoverageDtl(RecipientPolicyCoverageDtlType value) {
        this.recipientPolicyCoverageDtl = value;
    }

    /**
     * Gets the value of the recipient property.
     * 
     * @return
     *     possible object is
     *     {@link PersonInformationType }
     *     
     */
    public PersonInformationType getRecipient() {
        return recipient;
    }

    /**
     * Sets the value of the recipient property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonInformationType }
     *     
     */
    public void setRecipient(PersonInformationType value) {
        this.recipient = value;
    }

    /**
     * Gets the value of the recipientSpouse property.
     * 
     * @return
     *     possible object is
     *     {@link EOYPersonType }
     *     
     */
    public EOYPersonType getRecipientSpouse() {
        return recipientSpouse;
    }

    /**
     * Sets the value of the recipientSpouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link EOYPersonType }
     *     
     */
    public void setRecipientSpouse(EOYPersonType value) {
        this.recipientSpouse = value;
    }

    /**
     * Gets the value of the coverageHousehold property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coverageHousehold property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoverageHousehold().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoverageHouseholdType }
     * 
     * 
     */
    public List<CoverageHouseholdType> getCoverageHousehold() {
        if (coverageHousehold == null) {
            coverageHousehold = new ArrayList<CoverageHouseholdType>();
        }
        return this.coverageHousehold;
    }

    /**
     * Gets the value of the recipientMonthlyPolicyInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recipientMonthlyPolicyInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecipientMonthlyPolicyInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecipientMonthlyPolicyInfoType }
     * 
     * 
     */
    public List<RecipientMonthlyPolicyInfoType> getRecipientMonthlyPolicyInfo() {
        if (recipientMonthlyPolicyInfo == null) {
            recipientMonthlyPolicyInfo = new ArrayList<RecipientMonthlyPolicyInfoType>();
        }
        return this.recipientMonthlyPolicyInfo;
    }

    /**
     * Gets the value of the recordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Sets the value of the recordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordType(String value) {
        this.recordType = value;
    }

    /**
     * Gets the value of the lineNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLineNum() {
        return lineNum;
    }

    /**
     * Sets the value of the lineNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLineNum(BigInteger value) {
        this.lineNum = value;
    }

}
