package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

public class Member implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  id;	
	private String  name;	
	private Integer dispositionCode;	
	private String  dispositionLabel;	
	private Integer rank;
	private String  reqEffectiveDate;
	private String gender;
	private String dob;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getDispositionCode() {
		return dispositionCode;
	}
	public void setDispositionCode(Integer dispositionCode) {
		this.dispositionCode = dispositionCode;
	}
	public String getDispositionLabel() {
		return dispositionLabel;
	}
	public void setDispositionLabel(String dispositionLabel) {
		this.dispositionLabel = dispositionLabel;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getReqEffectiveDate() {
		return reqEffectiveDate;
	}
	public void setReqEffectiveDate(String reqEffectiveDate) {
		this.reqEffectiveDate = reqEffectiveDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}	
	
}
