package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 *  The Pojo claas for Issuer Payment Information
 *
 */
@Audited
@Entity
@Table(name="ISSUER_PAYMENT_INFO")
public class IssuerPaymentInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ISSUER_PAYMENT_INFO_SEQ")
	@SequenceGenerator(name = "ISSUER_PAYMENT_INFO_SEQ", sequenceName = "ISSUER_PAYMENT_INFO_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
    @JoinColumn(name="ISSUER_ID")
	private Issuer issuer;
	
	@Column(name = "KEY_STORE_FILE_LOC")
	private String keyStoreFileLocation;
	
	@Column(name = "PRIVATE_KEY_NAME")
	private String privateKeyName;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "PASSWORD_SEC_KEY")
	private String passwordSecuredKey;
	
	@Column(name = "SEC_DNS_NAME")
	private String securityDnsName;
	
	@Column(name = "SEC_ADDRESS")
	private String securityAddress;
	
	@Column(name = "SEC_KEY_INFO")
	private String securityKeyInfo;
	
	@Column(name = "ISSUER_AUTH_URL")
	private String issuerAuthURL;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name = "SECURITY_CERT_NAME")
	private String securityCertName ;
	
	
	public IssuerPaymentInformation(){
		
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public String getKeyStoreFileLocation() {
		return keyStoreFileLocation;
	}

	public void setKeyStoreFileLocation(String keyStoreFileLocation) {
		this.keyStoreFileLocation = keyStoreFileLocation;
	}

	public String getPrivateKeyName() {
		return privateKeyName;
	}

	public void setPrivateKeyName(String privateKeyName) {
		this.privateKeyName = privateKeyName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordSecuredKey() {
		return passwordSecuredKey;
	}

	public void setPasswordSecuredKey(String passwordSecuredKey) {
		this.passwordSecuredKey = passwordSecuredKey;
	}

	public String getSecurityDnsName() {
		return securityDnsName;
	}

	public void setSecurityDnsName(String securityDnsName) {
		this.securityDnsName = securityDnsName;
	}

	public String getSecurityAddress() {
		return securityAddress;
	}

	public void setSecurityAddress(String securityAddress) {
		this.securityAddress = securityAddress;
	}

	public String getSecurityKeyInfo() {
		return securityKeyInfo;
	}

	public void setSecurityKeyInfo(String securityKeyInfo) {
		this.securityKeyInfo = securityKeyInfo;
	}

	public String getIssuerAuthURL() {
		return issuerAuthURL;
	}

	public void setIssuerAuthURL(String issuerAuthURL) {
		this.issuerAuthURL = issuerAuthURL;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getSecurityCertName() {
		return securityCertName;
	}

	public void setSecurityCertName(String securityCertName) {
		this.securityCertName = securityCertName;
	}
	
}
