package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
/**
 * HIX-60431
 * This is pojo class for PLAN_MEDICARE database table
 * @author sharma_va
 *
 */
@Audited
@Entity
@Table(name = "PLAN_MEDICARE")
public class PlanMedicare implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_MEDICARE_SEQ")
	@SequenceGenerator(name = "PLAN_MEDICARE_SEQ", sequenceName = "PLAN_MEDICARE_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;

	@Column(name = "CMS_CONTRACT_ID")
	private String cmsContractId;

	@Column(name = "CMS_PLAN_ID")
	private String cmsPlanId;

	@Column(name = "CMS_INCLUDE_RX")
	private String cmsIncludeRx;

	@Column(name = "CMS_PLAN_NAME")
	private String cmsPlanName;

	@Column(name = "CMS_SEGMENT_ID")
	private String cmsSegmentId;

	@Column(name = "CMS_IS_SNP")
	private String cmsIsSnp;

	@Column(name = "IS_MEDICAID")
	private String isMedicaid;

	@Column(name = "BENEFITS_URL")
	private String benefitURL;
	
	@Column(name = "SELECT_NETWORK_ONLY")
	private String selectNetowrkOnly;
	
	@Column(name = "RATE_TYPE")
	private String rateType;
	
	@Column(name = "DATA_SOURCE")
	private String dataSource;
	
	@Column(name = "SNP_TYPE")
	private String snpType;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "planMedicare", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PlanMedicareBenefit> planMedicareBenefitList;

	public PlanMedicare() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getCmsContractId() {
		return cmsContractId;
	}

	public void setCmsContractId(String cmsContractId) {
		this.cmsContractId = cmsContractId;
	}

	public String getCmsPlanId() {
		return cmsPlanId;
	}

	public void setCmsPlanId(String cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}

	public String getCmsIncludeRx() {
		return cmsIncludeRx;
	}

	public void setCmsIncludeRx(String cmsIncludeRx) {
		this.cmsIncludeRx = cmsIncludeRx;
	}

	public String getCmsPlanName() {
		return cmsPlanName;
	}

	public void setCmsPlanName(String cmsPlanName) {
		this.cmsPlanName = cmsPlanName;
	}

	public String getCmsSegmentId() {
		return cmsSegmentId;
	}

	public void setCmsSegmentId(String cmsSegmentId) {
		this.cmsSegmentId = cmsSegmentId;
	}

	public String getCmsIsSnp() {
		return cmsIsSnp;
	}

	public void setCmsIsSnp(String cmsIsSnp) {
		this.cmsIsSnp = cmsIsSnp;
	}

	public String getIsMedicaid() {
		return isMedicaid;
	}

	public void setIsMedicaid(String isMedicaid) {
		this.isMedicaid = isMedicaid;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public List<PlanMedicareBenefit> getPlanMedicareBenefitList() {
		return planMedicareBenefitList;
	}

	public void setPlanMedicareBenefitList(
			List<PlanMedicareBenefit> planMedicareBenefitList) {
		this.planMedicareBenefitList = planMedicareBenefitList;
	}

	public String getBenefitURL() {
		return benefitURL;
	}

	public void setBenefitURL(String benefitURL) {
		this.benefitURL = benefitURL;
	}

	public String getSelectNetowrkOnly() {
		return selectNetowrkOnly;
	}

	public void setSelectNetowrkOnly(String selectNetowrkOnly) {
		this.selectNetowrkOnly = selectNetowrkOnly;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getSnpType() {
		return snpType;
	}

	public void setSnpType(String snpType) {
		this.snpType = snpType;
	}
}
