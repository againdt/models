package com.getinsured.hix.dto.eapp.workflow;

import java.util.Map;

public class EmailWorkflowArgs {
	private String to;
	private String subject;
	private String body;
	private String from;
	public EmailWorkflowArgs() {
		super();
	}
	public EmailWorkflowArgs(String toEmail, String subject, String body) {
		super();
		this.to = toEmail;
		this.subject = subject;
		this.body = body;
	}
	
	public EmailWorkflowArgs(Map<String,String> map){
		super();
		this.to = (String)map.get("to");
		this.subject = (String)map.get("subject");
		this.body = (String)map.get("body");
		this.from = (String)map.get("from");
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	
	

}
