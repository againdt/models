package com.getinsured.hix.dto.cap.consumerapp;

public class ScreenPopUpDTO {
	
	private HouseholdDTO householdDTO;
	private EnrollmentDTO enrollmentDTO;
	private CapApplicationDTO applicationDTO;
	private AffiliateFlowDTO affiliateFlowDTO;
	public HouseholdDTO getHouseholdDTO() {
		return householdDTO;
	}
	public void setHouseholdDTO(HouseholdDTO householdDTO) {
		this.householdDTO = householdDTO;
	}
	public EnrollmentDTO getEnrollmentDTO() {
		return enrollmentDTO;
	}
	public void setEnrollmentDTO(EnrollmentDTO enrollmentDTO) {
		this.enrollmentDTO = enrollmentDTO;
	}
	public CapApplicationDTO getApplicationDTO() {
		return applicationDTO;
	}
	public void setApplicationDTO(CapApplicationDTO applicationDTO) {
		this.applicationDTO = applicationDTO;
	}
	public AffiliateFlowDTO getAffiliateFlowDTO() {
		return affiliateFlowDTO;
	}
	public void setAffiliateFlowDTO(AffiliateFlowDTO affiliateFlowDTO) {
		this.affiliateFlowDTO = affiliateFlowDTO;
	}

	
}
