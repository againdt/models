package com.getinsured.hix.model.estimator.mini;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

/**
 * This is the model class for ELIG_LEAD table
 * @author Nikhil, Sunil Desu
 * @since 13 August, 2013
 */
@Audited
@Entity
@Table(name="ELIG_LEAD")
@DynamicUpdate
@DynamicInsert
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
public class EligLead implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4145411109923886242L;
	
	public enum STATUS{
		/**
		   	1)	Redirected - Default, upon redirection to GI
			2)	Not Interested – After a GI agent called the customer and verified no interest in GI products
			3)	Closed Online – Online enrollment
			4)	Closed by Agent – Sale of a health plan was performed by an Agent
		 */
		REDIRECTED, NOT_INTERESTED, CLOSED_ONLINE, CLOSED_BY_AGENT, ECOMMITTED, CALLED_ONCE, CALLED_TWICE, CALLED_THRICE, SWITCHED_TO_A_DIFFERENT_PLAN, PENDING, OTHER, NO_QUALIFYING_LIFE_EVENT,FAKE_TEST_LEAD, REDIRECT_AGENT_EXPRESS_ID;
	}
	public enum STAGE{
		/**
		 * 
		  	1)	Welcome - GI welcome page
			2)	Pre Eligibility - GI Pre eligibility calculator
			3)	Pre Shopping - GI Pre shopping module
			4)	Registered - Registration at GI
			5)  Pre Shopping Complete
			6)  SCREENER_PAGE : Screener page - The user continued after the sign up page to the screener 
			7)  SCREENER_PASS : Screener Pass - The user passed the screener and started the SSAP 
			8)  SCREENER_FAILED : Screener Failed - The user didn't pass the screener and was redirected to Healthcare.gov/e-commit 
			9)  TRANSITION_PAGE : Transition Page - The customer arrived to the transition page prior to being redirected to Healthcare.gov 
			10) SSAP_ON_EXCHANGE_START_YOUR_APPLICATION - SSAP On Exchange Start your application 
			11) SSAP_ON_EXCHANGE_FAMILY_AND_HOUSEHOLD - SSAP On Exchange Family & Household 
			12) SSAP_ON_EXCHANGE_INCOME - SSAP On Exchange Income 
			13) SSAP_ON_EXCHANGE_ADDITIONAL_QUESTIONS - SSAP On Exchange Additional Questions 
			14) SSAP_ON_EXCHANGE_REVIEW_AND_SIGN - SSAP On Exchange Review & Sign 
			15) PROXY_READY_TO_SUBMIT : The user completed the application and received the "thank you page" 
			16) PROXY_SUBMIT_IN_PROGRESS : The application was picked by the batch job and will be sent to the proxy 
			17) PROXY_SUBMIT_FAILED : The status that was received from the proxy service is failure. If the number of attempts is lower than X, additional attempts will be made. 
			18) PROXY_FAILED : We tried max number of attempts and still failed 
			19) PROXY_INCOMPLETE : We were able to process some of the application but weren't able to complete the proxy 
			20) PROXY_SUCCESS : Proxy was completed successfully 
			21) HCGOV_SUBMIT_IN_PROGRESS : We lock this app and we check if APTC>aptcAmount in ELIG_LEAD 
			22) REDIRECTED_TO_GI_HCGOV_ELIG_MISMATCH : The tax credit result from Healthcare.gov is lower than the aptcAmount in ELIG_LEAD 
			23) APP_SUBMIT_HCGOV_FAILED : Failed attempt to submit enrollment to Healthcare.gov 
			24) APP_HCGOV_FAILED : Failed multiple times to submit application to HCGOV 
			25)	Redirected to the FFM/SBE - The customer was redirected to the FFM/SBE for eligibility determination
			26)	Redirected Back to Getinsured
			27) POST_FFM_ELIGIBILITY_RECEIVED : Post FFM eligibility received
			28) POST_FFM_PLAN_UNAVAILABLE
			29) POST_FFM_PLAN_AVAILABLE
			30) POST_FFM_SHOPPING
			31) POST_FFM_REVIEW
			32) POST_FFM_ESIGNATURE
			33)	Shopping - GI smart choice plan selection 
			34) Payable Event
			35)	Enrollment - GI Enrollment module
			36)	Enrollment completed - The customer submitted/gave his payment details. Enrollment was submitted to the FFM/SBE/Carriers
			37)	Issuer confirmed enrollment – (Enrollment Status = Confirmed)
			38)	Commission received from carrier
		*/

		WELCOME,
		PRE_ELIGIBILITY,
		PRE_SHOPPING,
		REGISTERED,
		PRE_SHOPPING_COMPLETE,
		SCREENER_PAGE,
		SCREENER_PASS,
		SCREENER_FAILED,
		TRANSITION_PAGE, 
		SSAP_ON_EXCHANGE_START_YOUR_APPLICATION, 
		SSAP_ON_EXCHANGE_FAMILY_AND_HOUSEHOLD,
		SSAP_ON_EXCHANGE_INCOME,
		SSAP_ON_EXCHANGE_ADDITIONAL_QUESTIONS,
		SSAP_ON_EXCHANGE_REVIEW_AND_SIGN, 
		PROXY_READY_TO_SUBMIT, 
		PROXY_SUBMIT_IN_PROGRESS,
		PROXY_SUBMIT_FAILED,
		PROXY_FAILED,
		PROXY_INCOMPLETE,
		PROXY_SUCCESS,
		HCGOV_SUBMIT_IN_PROGRESS, 
		APP_SUBMIT_HCGOV_FAILED, 
		APP_HCGOV_FAILED,
		REDIRECTED_TO_THE_FFM_SBE, 
		REDIRECTED_BACK_TO_GETINSURED, 
		POST_FFM_ELIGIBILITY_RECEIVED,
		REDIRECTED_TO_GI_HCGOV_ELIG_MISMATCH,
		POST_FFM_PLAN_UNAVAILABLE,
		POST_FFM_PLAN_AVAILABLE,
		POST_FFM_SHOPPING,
		POST_FFM_REVIEW,
		POST_FFM_ESIGNATURE, 
		SHOPPING,
		PAYABLE_EVENT,
		ENROLLMENT,
		ENROLLMENT_COMPLETED,
		ISSUER_CONFIRMED_ENROLLMENT,
		COMMISSION_RECEIVED_FROM_CARRIER,
		
		//Following stages will be used by coveredCA proxy only.
		
		SSAP_CA_START_YOUR_APPLICATION_SECTION ,
		SSAP_CA_HOUSEHOLD_SECTION,
		SSAP_CA_INCOME_SECTION,
		SSAP_CA_PERSONAL_DATA_SECTION,
		SSAP_CA_ELIGIBILITY_SECTION,
		SSAP_CA_ENROLLMENT_COMPLETED
		
		}
	
	public enum LEAD_TYPE{
		
		/**
		 * 1. Web - Default.
		 * 2. Call - When phix home page is accessed using member/portal
		 */
		
		WEB, CALL
	}
	
	/**
	 * 
	 * @author Sunil Desu
	 * @since November 25 2013
	 * 
	 * 	Assaf O Definition On/Off Exchange: 
     *
	 *	Off Exchange = 
	 *	1. aptcEligible=N & medicaidHousehold=N & medicareHousehold=N & chipHousehold=N 
	 *	2. aptcEligible=Y & aptcAmount=$0 
	 *	
	 *	On Exchange = aptcEligible=Y & aptcAmount > $0 or n/a 
	 *	
	 *	Note: In the case of medicaidHousehold=Y or medicareHousehold=Y or chipHousehold=Y, display public program message.
	 *
	 */
	public enum ExchangeFlowType{
		ON, OFF,PUBLIC;
	}
	
	
	/**
	 * 
	 * @author Nikhil Talreja
	 * @since 09 December, 2013
	 * 
	 * HIX-24712 App Status updates for Jackson Hewitt customers
	 *
	 */
	public enum AppStatus{
		/** 
		1 App. Start 
		2 FFM Complete 
		3 Final Transaction 
		4 No Answer 1 
		5 No Answer 2 
		6 Discrepancies 
		7 Not Interested 
		8 Other 
		*/
		APP_START,
		FFM_COMPLETE,
		FINAL_TRANSACTION,
		NO_ANSWER_1,
		NO_ANSWER_2,
		DISCREPANCIES,
		NOT_INTERESTED,
		OTHER
	}
	public enum EligibilityResult
	{
		
		ON_EXCHANGE,
		OFF_EXCHANGE,
		MEDCAID,
		MEDICARE,
		CHIP,
		NOT_ELIGIBLE,
		STM,
		VISION,
		DENTAL,
		AME,
		LIFE
		
	}
	public enum YesNo
	{
		YES,
		NO
	}
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ELIG_LEAD_SEQ")
	@SequenceGenerator(name = "ELIG_LEAD_SEQ", sequenceName = "ELIG_LEAD_SEQ", allocationSize = 1)
	private long id;
	
	@NotAudited
	@Column(name = "NAME")
	private String name;
	
	@NotAudited
	@Column(name = "ZIPCODE")
	private String zipCode;
	
	@NotAudited
	@Column(name = "COUNTY_CODE")
	private String countyCode;
	
	@NotAudited
	@Column(name = "FAMILY_SIZE")
	private int familySize;
	
	@NotAudited
	@Column(name = "NO_OF_APPLICANTS")
	private int noOfApplicants;
	
	//HIX-91458 : Application treats -1 as Income not entered. 
	@NotAudited
	@Column(name = "HOUSEHOLD_INCOME")
	private double householdIncome=-1; 
	
	@NotAudited
	@Column(name = "EMAIL_ADDRESS")
	private String emailAddress;
	
	@NotAudited
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	
	@NotAudited
	@Column(name = "IS_OK_TO_CALL")
	private String isOkToCall="Y";
	
	@NotAudited
//	@Lob
	@Column(name = "MEMBER_DATA")
	private String memberData;
	
	@NotAudited
	@Column(name = "APTC")
	private String aptc;
	
	@NotAudited
	@Column(name = "PREMIUM")
	private String premium;
	
	@NotAudited
	@Column(name = "CSR")
	private String csr;
	
	@NotAudited
	@Column(name = "DOC_VISIT_FREQUENCY")
	private String docVisitFrequency;
	
	@NotAudited
	@Column(name = "NO_OF_PRESCRIPTIONS")
	private String noOfPrescriptions;
	
	@NotAudited
	@Column(name = "BENEFITS")
	private String benefits;
	
	@NotAudited
	@Column(name = "AFFILIATE_ID")
	private long affiliateId;
	
	@NotAudited
	@Column(name = "EXT_AFF_HOUSEHOLD_ID")
	private String extAffHouseholdId;
	
	@NotAudited
	@Column(name = "FLOW_ID")
	private Integer flowId;
	
	@NotAudited
	@Column(name = "CLICK_ID")
	private long clickId;
	
	@NotAudited
//	@Lob
	@Column(name = "API_OUTPUT")
	private String apiOutput;
	
	@NotAudited
	@Column(name = "OVERALL_API_STATUS")
	private String overallApiStatus;
	
	@NotAudited
	@Column(name = "GI_EXECUTION_DURATION")
	private Long giExecutionDuration;
	
	@NotAudited
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)	
	private STATUS status;
	
	@Column(name = "STAGE")
	@Enumerated(EnumType.STRING)	
	private STAGE stage;
	
	@NotAudited
	@Column(name="LEAD_TYPE")
	@Enumerated(EnumType.STRING)
	private LEAD_TYPE leadType = LEAD_TYPE.WEB;
	
	@NotAudited
	@Column(name = "ERROR_CODE")
	private int errorCode;
	
	@NotAudited
	@Column(name = "ERROR_MSG")
	private String errorMessage;
	
	@NotAudited
	@Column(name = "CREATED_BY")
	private long createdBy;
	
	@NotAudited
	@Column(name = "LAST_UPDATED_BY")
	private long lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date creationTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	@NotAudited
	@Column(name = "GI_EXCHANGE_FLOW_TYPE")
	@Enumerated(EnumType.STRING)
	public ExchangeFlowType exchangeFlowType;
	
	@Column(name="GI_APP_STATUS")
	@Enumerated(EnumType.STRING)
	private AppStatus appStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="GI_APP_STATUS_UPDATE_TMSTP")
	private Date appStatusUpdateTimeStamp;
	
	
	@Column(name="BATCH_PROCESSED")
	private Long batchProcessed;
	
	
	@Column(name="CREATED_BY_API")
	private String createdByAPI;
	
	@NotAudited
	@Column(name = "EVENT_ID")
	private Long eventId;
	
	@NotAudited
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "COVERAGE_START_DATE")
	private Date coverageStartDate;
	
	@NotAudited
	@Column(name = "TCPA_COMPLIANCE_URL")
	private String tcpaComplianceURL;
	
	@NotAudited
	@Column(name = "AFF_FILE_UPLOAD_LOG_ID")
	private Long affFileUploadLogId;
	
	@NotAudited
	@Column(name="ELIGIBILITY_RESULT")
	@Enumerated(EnumType.STRING)
	private EligibilityResult eligibilityResult;
	
	@NotAudited
	@Column(name="ELIGIBILITY_CHECKED")
	@Enumerated(EnumType.STRING)
	private YesNo eligibilityCalculated;
	
	@NotAudited
	@Column(name="HAS_CHIP_MEMBERS")
	@Enumerated(EnumType.STRING)
	private YesNo hasChipMembers;
	
	@NotAudited
	@Column(name="HAS_MEDICAID_CHILDREN")
	@Enumerated(EnumType.STRING)
	private YesNo hasMedicaidChildren;
	
	@NotAudited
	@Column(name="HAS_MEDICAID_MEMBERS")
	@Enumerated(EnumType.STRING)
	private YesNo hasMedicaidMembers;
	
	@NotAudited
	@Column(name="HAS_MEDICARE_MEMBERS")
	@Enumerated(EnumType.STRING)
	private YesNo hasMedicareMembers;
	
	@NotAudited
	@OneToMany(mappedBy="leadId", cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	private List<PreEligibilityResults> preEligibilityResults;
	
	@Column(name="TENANT_ID")
    private Long tenantId;
	
	@Column(name="DATA_SHARING_CONSENT")
	private Integer dataSharingConsent;
	
	@Column(name="REDIRECT_ID")
	private String redirectId;
	
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public int getNoOfApplicants() {
		return noOfApplicants;
	}

	public void setNoOfApplicants(int noOfApplicants) {
		this.noOfApplicants = noOfApplicants;
	}

	public double getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(double householdIncome) {
		this.householdIncome = householdIncome;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(String isOkToCall) {
		this.isOkToCall = isOkToCall;
	}

	public String getMemberData() {
		return memberData;
	}

	public void setMemberData(String memberData) {
		this.memberData = memberData;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getDocVisitFrequency() {
		return docVisitFrequency;
	}

	public void setDocVisitFrequency(String docVisitFrequency) {
		this.docVisitFrequency = docVisitFrequency;
	}

	public String getNoOfPrescriptions() {
		return noOfPrescriptions;
	}

	public void setNoOfPrescriptions(String noOfPrescriptions) {
		this.noOfPrescriptions = noOfPrescriptions;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public String getExtAffHouseholdId() {
		return extAffHouseholdId;
	}

	public void setExtAffHouseholdId(String extAffHouseholdId) {
		this.extAffHouseholdId = extAffHouseholdId;
	}

	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	public long getClickId() {
		return clickId;
	}

	public void setClickId(long clickId) {
		this.clickId = clickId;
	}

	public String getApiOutput() {
		return apiOutput;
	}

	public void setApiOutput(String apiOutput) {
		this.apiOutput = apiOutput;
	}

	public String getOverallApiStatus() {
		return overallApiStatus;
	}

	public void setOverallApiStatus(String overallApiStatus) {
		this.overallApiStatus = overallApiStatus;
	}

	public Long getGiExecutionDuration() {
		return giExecutionDuration;
	}

	public void setGiExecutionDuration(Long giExecutionDuration) {
		this.giExecutionDuration = giExecutionDuration;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public STAGE getStage() {
		return stage;
	}

	public void setStage(STAGE stage) {
		this.stage = stage;
	}

	public LEAD_TYPE getLeadType() {
		return leadType;
	}

	public void setLeadType(LEAD_TYPE leadType) {
		this.leadType = leadType;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
	public ExchangeFlowType getExchangeFlowType() {
		return exchangeFlowType;
	}

	public void setExchangeFlowType(ExchangeFlowType exchangeFlowType) {
		this.exchangeFlowType = exchangeFlowType;
	}

	public AppStatus getAppStatus() {
		return appStatus;
	}

	public void setAppStatus(AppStatus appStatus) {
		this.appStatus = appStatus;
	}

	public Date getAppStatusUpdateTimeStamp() {
		return appStatusUpdateTimeStamp;
	}

	public void setAppStatusUpdateTimeStamp(Date appStatusUpdateTimeStamp) {
		this.appStatusUpdateTimeStamp = appStatusUpdateTimeStamp;
	}
	
	public Long getBatchProcessed() {
		return batchProcessed;
	}

	public void setBatchProcessed(Long batchProcessed) {
		this.batchProcessed = batchProcessed;
	}

	public String getCreatedByAPI() {
		return createdByAPI;
	}

	public void setCreatedByAPI(String createdByAPI) {
		this.createdByAPI = createdByAPI;
	}

	@PrePersist
	public void prePersist(){
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
		setTenantIdIfApplicable();
	}
	
	@PreUpdate
	public void preUpdate(){
		this.setLastUpdateTimestamp(new TSDate());
		setTenantIdIfApplicable();
	}

	private void setTenantIdIfApplicable() {
		if (TenantContextHolder.getTenant() != null
				&& TenantContextHolder.getTenant().getId() != null
				&& this.tenantId == null) {
	        setTenantId(TenantContextHolder.getTenant().getId());
		}
	}
	
	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getTcpaComplianceURL() {
		return tcpaComplianceURL;
	}

	public void setTcpaComplianceURL(String tcpaComplianceURL) {
		this.tcpaComplianceURL = tcpaComplianceURL;
	}
	
	public Long getAffFileUploadLogId() {
		return affFileUploadLogId;
	}

	public void setAffFileUploadLogId(Long affFileUploadLogId) {
		this.affFileUploadLogId = affFileUploadLogId;
	}

	public Map<String, String> fetchBenefitsMap(){
		Map<String, String> benefitsMapKey = new HashMap<String, String>();
		benefitsMapKey.put("Adult dental", "ADULT_DENTAL");
		benefitsMapKey.put("Pediatric dental", "PEDIATRIC_DENTAL");
		benefitsMapKey.put("Acupuncture", "ACUPUNCTURE");
		benefitsMapKey.put("Chiropractic care", "CHIROPRACTIC_CARE");
		benefitsMapKey.put("Occupational and physical therapy", "OCCUPATIONAL_AND_PHYSICAL_THERAPY");
		benefitsMapKey.put("Hearing aids", "HEARING_AIDS");
		benefitsMapKey.put("Nutritional counseling", "NUTRITIONAL_COUNSELING");
		
		Map<String, String> benefitsMap = new HashMap<String, String>();		
		benefitsMap.put("ADULT_DENTAL", "N");
		benefitsMap.put("PEDIATRIC_DENTAL", "N");
		benefitsMap.put("ACUPUNCTURE", "N");
		benefitsMap.put("CHIROPRACTIC_CARE", "N");
		benefitsMap.put("OCCUPATIONAL_AND_PHYSICAL_THERAPY", "N");
		benefitsMap.put("HEARING_AIDS", "N");
		benefitsMap.put("NUTRITIONAL_COUNSELING", "N");
		if(this.getBenefits()!=null && this.getBenefits().trim().length()>0){
			Set<String> benefitsStored = new HashSet<String>(Arrays.asList(this.getBenefits().split(",")));
			for(String benefit:benefitsStored){
				benefitsMap.put(benefitsMapKey.get(benefit), "Y");
			}
		}
		return benefitsMap;
		
	}

	public EligibilityResult getEligibilityResult() {
		return eligibilityResult;
	}

	public void setEligibilityResult(EligibilityResult eligibilityResult) {
		this.eligibilityResult = eligibilityResult;
	}

	public YesNo getHasChipMembers() {
		return hasChipMembers;
	}

	public void setHasChipMembers(YesNo hasChipMembers) {
		this.hasChipMembers = hasChipMembers;
	}

	public YesNo getHasMedicaidChildren() {
		return hasMedicaidChildren;
	}

	public void setHasMedicaidChildren(YesNo hasMedicaidChildren) {
		this.hasMedicaidChildren = hasMedicaidChildren;
	}

	public YesNo getHasMedicaidMembers() {
		return hasMedicaidMembers;
	}

	public void setHasMedicaidMembers(YesNo hasMedicaidMembers) {
		this.hasMedicaidMembers = hasMedicaidMembers;
	}

	public YesNo getHasMedicareMembers() {
		return hasMedicareMembers;
	}

	public void setHasMedicareMembers(YesNo hasMedicareMembers) {
		this.hasMedicareMembers = hasMedicareMembers;
	}

	public List<PreEligibilityResults> getPreEligibilityResults() {
		return preEligibilityResults;
	}

	public void setPreEligibilityResults(
			List<PreEligibilityResults> preEligibilityResults) {
		this.preEligibilityResults = preEligibilityResults;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Boolean getDataSharingConsent() {
		if (dataSharingConsent != null) {
			if (dataSharingConsent == 1) {
				return true;
			} else if (dataSharingConsent == 0) {
				return false;
			}
		}
		return null;
	}

	public void setDataSharingConsent(Boolean dataSharingConsent) {
		if (dataSharingConsent != null) {
			if (dataSharingConsent == Boolean.TRUE) {
				this.dataSharingConsent = 1;
			}
			else if(dataSharingConsent == Boolean.FALSE){
				this.dataSharingConsent = 0;
			}
		}
		else{
			this.dataSharingConsent = null;
		}
	}

	public YesNo getEligibilityCalculated() {
		return eligibilityCalculated;
	}

	public void setEligibilityCalculated(YesNo eligibilityCalculated) {
		this.eligibilityCalculated = eligibilityCalculated;
	}

	public String getRedirectId() {
		return redirectId;
	}

	public void setRedirectId(String redirectId) {
		this.redirectId = redirectId;
	}
}
