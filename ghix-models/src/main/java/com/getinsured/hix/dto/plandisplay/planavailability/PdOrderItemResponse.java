package com.getinsured.hix.dto.plandisplay.planavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public class PdOrderItemResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PdOrderItemPersonResponse> pdOrderItemPersonResponseList;
	private Long planId;
	private Float premiumBeforeCredit;
	private Float premiumAfterCredit;
	private Float monthlySubsidy;
	private BigDecimal monthlyStateSubsidy;
	private InsuranceType insuranceType;
	
	public List<PdOrderItemPersonResponse> getPdOrderItemPersonResponseList() {
		return pdOrderItemPersonResponseList;
	}
	
	public void setPdOrderItemPersonResponseList(List<PdOrderItemPersonResponse> pdOrderItemPersonResponseList) {
		this.pdOrderItemPersonResponseList = pdOrderItemPersonResponseList;
	}
	
	public Long getPlanId() {
		return planId;
	}
	
	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	
	public Float getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}
	
	public void setPremiumBeforeCredit(Float premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}
	
	public Float getPremiumAfterCredit() {
		return premiumAfterCredit;
	}
	
	public void setPremiumAfterCredit(Float premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}
	
	public Float getMonthlySubsidy() {
		return monthlySubsidy;
	}
	
	public void setMonthlySubsidy(Float monthlySubsidy) {
		this.monthlySubsidy = monthlySubsidy;
	}
	
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

	public BigDecimal getMonthlyStateSubsidy() {
		return monthlyStateSubsidy;
	}

	public void setMonthlyStateSubsidy(BigDecimal monthlyStateSubsidy) {
		this.monthlyStateSubsidy = monthlyStateSubsidy;
	}
}
