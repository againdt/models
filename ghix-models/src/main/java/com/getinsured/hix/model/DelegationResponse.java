package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * Contains the response sent by AHBX for IND54
 */
public class DelegationResponse extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String delegationCode;
	private String recordType;
	private String responseDescription;

	private long recordId;
	private int responseCode;

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public String getDelegationCode() {
		return delegationCode;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	@Override
	public String toString() {
		return "DelegationResponse details: DelegationCode = "+delegationCode+", RecordType = "+recordType+", RecordId = "+recordId+
				", ResponseDescription = "+responseDescription+", ResponseCode = "+responseCode;
	}
}
