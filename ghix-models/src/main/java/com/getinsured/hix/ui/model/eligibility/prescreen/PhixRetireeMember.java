package com.getinsured.hix.ui.model.eligibility.prescreen;

import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * This class encapsulates the Retiree member information related to the
 * home page of eligibility section of the private exchange.
 * 
 * 
 * @author Suresh
 * @since Sep 16, 2015
 * 
 */
public class PhixRetireeMember {
	
	
	private MemberRelationships relationshipToPrimary;
	
	private String memberEligibility = "NA";
	
	private String medicaidChild = "N";
	
	private String hraContributionAmount="";
	
	public PhixRetireeMember(){
		
	}

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

	public String getMemberEligibility() {
		return memberEligibility;
	}

	public void setMemberEligibility(String memberEligibility) {
		this.memberEligibility = memberEligibility;
	}

	public String getMedicaidChild() {
		return medicaidChild;
	}

	public void setMedicaidChild(String medicaidChild) {
		this.medicaidChild = medicaidChild;
	}

	public String getHraContributionAmount() {
		return hraContributionAmount;
	}

	public void setHraContributionAmount(String hraContributionAmount) {
		this.hraContributionAmount = hraContributionAmount;
	}
	
		
}
