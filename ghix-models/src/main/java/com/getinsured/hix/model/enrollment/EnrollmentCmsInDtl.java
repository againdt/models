package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ENRL_CMS_IN_DTL")
public class EnrollmentCmsInDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_IN_DTL_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_IN_DTL_SEQ", sequenceName = "ENRL_CMS_IN_DTL_SEQ", allocationSize = 1)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ENRL_CMS_IN_ID")
	private EnrollmentCmsIn enrollmentCmsIn;

	@Column(name = "QHP_ID")
	private String qhpId;

	@Column(name = "EXCHG_ASSIGN_POLICY_ID")
	private Integer exchAssignPolicyId;

	@Column(name = "ISSUER_ASSIGN_SUBSCRIBER_ID")
	private Integer issuerAssignSubsriberId;

	@Column(name = "ERROR_CODE")
	private String errorCode;

	@Column(name = "ERROR_DESC")
	private String errorDesc;

	@Column(name = "ELEMENT_IN_ERROR")
	private String elementInError;

	@Column(name = "ADDITIONAL_ERROR_INFO")
	private String additionalErrorInfo;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the enrollmentCmsIn
	 */
	public EnrollmentCmsIn getEnrollmentCmsIn() {
		return enrollmentCmsIn;
	}

	/**
	 * @param enrollmentCmsIn
	 *            the enrollmentCmsIn to set
	 */
	public void setEnrollmentCmsIn(EnrollmentCmsIn enrollmentCmsIn) {
		this.enrollmentCmsIn = enrollmentCmsIn;
	}

	/**
	 * @return the qhpId
	 */
	public String getQhpId() {
		return qhpId;
	}

	/**
	 * @param qhpId
	 *            the qhpId to set
	 */
	public void setQhpId(String qhpId) {
		this.qhpId = qhpId;
	}

	/**
	 * @return the exchAssignPolicyId
	 */
	public Integer getExchAssignPolicyId() {
		return exchAssignPolicyId;
	}

	/**
	 * @param exchAssignPolicyId
	 *            the exchAssignPolicyId to set
	 */
	public void setExchAssignPolicyId(Integer exchAssignPolicyId) {
		this.exchAssignPolicyId = exchAssignPolicyId;
	}

	/**
	 * @return the issuerAssignSubsriberId
	 */
	public Integer getIssuerAssignSubsriberId() {
		return issuerAssignSubsriberId;
	}

	/**
	 * @param issuerAssignSubsriberId
	 *            the issuerAssignSubsriberId to set
	 */
	public void setIssuerAssignSubsriberId(Integer issuerAssignSubsriberId) {
		this.issuerAssignSubsriberId = issuerAssignSubsriberId;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorDesc
	 */
	public String getErrorDesc() {
		return errorDesc;
	}

	/**
	 * @param errorDesc
	 *            the errorDesc to set
	 */
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	/**
	 * @return the elementInError
	 */
	public String getElementInError() {
		return elementInError;
	}

	/**
	 * @param elementInError
	 *            the elementInError to set
	 */
	public void setElementInError(String elementInError) {
		this.elementInError = elementInError;
	}

	/**
	 * @return the additionalErrorInfo
	 */
	public String getAdditionalErrorInfo() {
		return additionalErrorInfo;
	}

	/**
	 * @param additionalErrorInfo
	 *            the additionalErrorInfo to set
	 */
	public void setAdditionalErrorInfo(String additionalErrorInfo) {
		this.additionalErrorInfo = additionalErrorInfo;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
	}
}
