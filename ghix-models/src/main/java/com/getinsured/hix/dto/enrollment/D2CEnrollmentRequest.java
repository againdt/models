package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class D2CEnrollmentRequest implements Serializable{

	private Integer enrollmentId;
	private Long d2cEnrollmentId;
	private Date benefitEffectiveDate;
	private Date benefitEndDate;
	private Float aptcAmt;
	private Float csrAmt;
	private Integer planId;
	private Integer issuerId;
	private Integer employerId;
	private String insuranceType;
	private String enrollmentStatus;
	private String enrollmentType;
	private Float grossPremiumAmt;
	private Float netPremiumAmt;
	private Integer assisterBrokerId;
	private Integer cmrHouseholdId;
	private Integer employeeId;
	private String ticketId;
	private Long ssapApplicationId;
	private Integer indOrderItemsId;
	private String exchangeType;
	private String groupPolicyNumber;
	private String planName;
	private String exchangeAssignPolicyNo;
	private String issuerAssignPolicyNo;
	private String confirmationNo;
	private String carrierAppId;
	private String carrierAppUid;
	private List<D2CEnrolleeDTO> d2CEnrolleeDTOList;
	private boolean existSubscriber;
	private Integer capAgentId;
	
	public Integer getCapAgentId() {
		return capAgentId;
	}

	public void setCapAgentId(Integer capAgentId) {
		this.capAgentId = capAgentId;
	}

	public boolean isExistSubscriber() {
		return existSubscriber;
	}

	public void setExistSubscriber(boolean existSubscriber) {
		this.existSubscriber = existSubscriber;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Long getD2cEnrollmentId() {
		return d2cEnrollmentId;
	}

	public void setD2cEnrollmentId(Long d2cEnrollmentId) {
		this.d2cEnrollmentId = d2cEnrollmentId;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public Float getAptcAmt() {
		return aptcAmt;
	}

	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public Float getCsrAmt() {
		return csrAmt;
	}

	public void setCsrAmt(Float csrAmt) {
		this.csrAmt = csrAmt;
	}

	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}

	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}

	public Integer getAssisterBrokerId() {
		return assisterBrokerId;
	}

	public void setAssisterBrokerId(Integer assisterBrokerId) {
		this.assisterBrokerId = assisterBrokerId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getIndOrderItemsId() {
		return indOrderItemsId;
	}

	public void setIndOrderItemsId(Integer indOrderItemsId) {
		this.indOrderItemsId = indOrderItemsId;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}

	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}

	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}

	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}

	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}

	public String getConfirmationNo() {
		return confirmationNo;
	}

	public void setConfirmationNo(String confirmationNo) {
		this.confirmationNo = confirmationNo;
	}

	public String getCarrierAppId() {
		return carrierAppId;
	}

	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}

	public String getCarrierAppUid() {
		return carrierAppUid;
	}

	public void setCarrierAppUid(String carrierAppUid) {
		this.carrierAppUid = carrierAppUid;
	}

	public List<D2CEnrolleeDTO> getD2CEnrolleeDTOList() {
		return d2CEnrolleeDTOList;
	}

	public void setD2CEnrolleeDTOList(List<D2CEnrolleeDTO> d2cEnrolleeDTOList) {
		d2CEnrolleeDTOList = d2cEnrolleeDTOList;
	}
	
	public static void main(String args[]){
		D2CEnrollmentRequest request = new D2CEnrollmentRequest();
		request.setAptcAmt(new Float(23.80));
		request.setCapAgentId(1);
		request.setCmrHouseholdId(200);
		request.setPlanName("Delta dental");
		request.setConfirmationNo("23434");
		request.setEnrollmentStatus("PENDING");
		request.setIssuerId(2);
		request.setPlanId(2);
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		System.out.println(gson.toJson(request));
	}
}
