package com.getinsured.hix.dto.directenrollment.zip;

/**
 * Uniquely identifies a county.
 * 
 * @author root
 *
 */
public class DirectEnrollmentCountyDTO {
	private String countyName;
	private String countyFips;
	private String countyState;
	
	
	public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public String getCountyFips() {
		return countyFips;
	}
	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}
	public String getCountyState() {
		return countyState;
	}
	public void setCountyState(String countyState) {
		this.countyState = countyState;
	}

}
