package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the facility_address database table.
 * 
 */

@Entity
@Table(name="facility_address")
public class FacilityAddress implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Status{
		 ACTIVE,INACTIVE;
	 }
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FacilityAddress_Seq")
	@SequenceGenerator(name = "FacilityAddress_Seq", sequenceName = "facility_address_seq", allocationSize = 1)
	private int id;

	@ManyToOne
    @JoinColumn(name="facility_id")
	private Facility facility;
	
	@Transient
	private String datasourceRefId;
	
	@Column(name="address1")
	private String address1;
	
	@Column(name="address2")
	private String address2;
	
	@Column(name="state")
	private String state;
	
	@Column(name="city")
	private String city;
	
	@Column(name="zip")
	private String zip;
	
	@Column(name="extended_zipcode")
	private String extendedZipcode;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="fax")
	private String fax;
	
	@Column(name="county")
	private String county;
	
	@Column(name="lattitude")
	private Float lattitude;
	
	@Column(name="longitude")
	private Float longitude;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address) {
		this.address1 = address;
	}
	
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phone) {
		this.phoneNumber = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Float getLattitude() {
		return lattitude;
	}

	public void setLattitude(Float lattitude) {
		this.lattitude = lattitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getExtendedZipcode() {
		return extendedZipcode;
	}

	public void setExtendedZipcode(String extzip) {
		this.extendedZipcode = extzip;
	}

	public String getDatasourceRefId() {
		return datasourceRefId;
	}

	public void setDatasourceRefId(String datasourceRefId) {
		this.datasourceRefId = datasourceRefId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}
	
		
}
