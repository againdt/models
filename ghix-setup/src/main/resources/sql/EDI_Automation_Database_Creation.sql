create sequence EXCHG_PrtLkp_seq
  minvalue 1
  maxvalue 9999999999
  start with 1
  increment by 1
  nocycle;

create table EXCHG_PartnerLookup
(
  ID                        number        not null,
  InsurerName               varchar2(50)  not null,
  HIOS_Issuer_ID            varchar2(5)   not null,
  Market                    varchar2(10)  not null,
  Direction                 varchar2(8)   not null,
  ISA05                     varchar2(2)   not null,
  ISA06                     varchar2(15)  not null,
  ISA07                     varchar2(2)   not null,
  ISA08                     varchar2(15)  not null,
  ISA15                     varchar2(1)   not null,
  GS02                      varchar2(15),
  GS03                      varchar2(15),
  GS08                      varchar2(12),
  ST01                      varchar2(3)   not null,
  Source_Dir                varchar2(255) not null,
  Target_Dir                varchar2(255),
  InProcess_Dir             varchar2(255) not null,
  Archive_Dir               varchar2(255) not null,
  Map_Name                  varchar2(50),
  Validation_Standard       varchar2(50),
  APF                       varchar2(50),
  Role_ID                   number        not null,
  Creation_Timestamp        timestamp(6)  not null,
  Last_Update_Timestamp     timestamp(6)  not null
);

COMMENT ON TABLE  EXCHG_PartnerLookup IS 'A table holds partner records';
COMMENT ON COLUMN EXCHG_PartnerLookup."ID" IS 'A sequence for this record';
COMMENT ON COLUMN EXCHG_PartnerLookup."INSURERNAME" IS 'Name of the Insurance Carrier';
COMMENT ON COLUMN EXCHG_PartnerLookup."HIOS_ISSUER_ID" IS 'HIOS ID for the Issuer';
COMMENT ON COLUMN EXCHG_PartnerLookup."MARKET" IS 'Market is "FI and 24" (for SHOP and Individual), "FI", "24"';
COMMENT ON COLUMN EXCHG_PartnerLookup."DIRECTION" IS 'Direction of the transaction';
COMMENT ON COLUMN EXCHG_PartnerLookup."ISA05" IS 'ISA05 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."ISA06" IS 'ISA06 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."ISA07" IS 'ISA07 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."ISA08" IS 'ISA08 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."ISA15" IS 'ISA15 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."GS02" IS 'GS02 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."GS03" IS 'GS03 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."GS08" IS 'GS08 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."ST01" IS 'ST01 element in EDI';
COMMENT ON COLUMN EXCHG_PartnerLookup."SOURCE_DIR" IS 'Originating directory for file pickup so processing can start.  Outbound file is XML; inbound file is EDI.';
COMMENT ON COLUMN EXCHG_PartnerLookup."TARGET_DIR" IS 'Final directory for file dropoff after processing has been completed.  Outbound file is EDI; inbound file is XML.';
COMMENT ON COLUMN EXCHG_PartnerLookup."INPROCESS_DIR" IS 'Directory for file processing';
COMMENT ON COLUMN EXCHG_PartnerLookup."ARCHIVE_DIR" IS 'Directory for EDI archiving';
COMMENT ON COLUMN EXCHG_PartnerLookup."MAP_NAME" IS 'Map to use for file translation';
COMMENT ON COLUMN EXCHG_PartnerLookup."VALIDATION_STANDARD" IS 'TIBCO-supplied validation standard to perform EDI validation';
COMMENT ON COLUMN EXCHG_PartnerLookup."APF" IS 'Validator profile file containing configuration information for TIBCO Instream to determine what is checked or not checked.';
COMMENT ON COLUMN EXCHG_PartnerLookup."ROLE_ID" IS 'Cross-reference to EXCHG_FilePatternRole.ID';
COMMENT ON COLUMN EXCHG_PartnerLookup."CREATION_TIMESTAMP" IS 'Timestamp when partner record was created';
COMMENT ON COLUMN EXCHG_PartnerLookup."LAST_UPDATE_TIMESTAMP" IS 'Timestamp when partner record was last updated';

create sequence EXCHG_ControlNum_seq
  minvalue 1
  maxvalue 9999999999
  start with 1
  increment by 1
  nocycle;

create table EXCHG_ControlNum
(
  ID		  number      not null,
  HIOS_Issuer_ID  varchar2(5) not null,
  Control_Num     number(9,0) not null,
  CONSTRAINT uk_EXCHG_ControlNum_alt UNIQUE  (HIOS_Issuer_ID)
);

COMMENT ON TABLE  EXCHG_ControlNum IS 'A table holds outbound partner control numbers for EDI';
COMMENT ON COLUMN EXCHG_ControlNum."HIOS_ISSUER_ID" IS 'HIOS ID for the Issuer';
COMMENT ON COLUMN EXCHG_ControlNum."CONTROL_NUM" IS 'Control number used for ISA13 and GS06 in EDI';


create table EXCHG_FilePattern
(
  ID                   number        not null,
  Direction            varchar2(1)   not null,
  FileType             varchar2(3)   not null,
  GS08                 varchar2(12),
  FilePattern          varchar2(80)  not null,
  FilePattern_Ext_Pre  varchar2(20),
  FilePattern_Ext_Post varchar2(20),
  Xref_IDs             varchar2(20)
);

COMMENT ON TABLE  EXCHG_FilePattern IS 'A table holds outbound partner file patterns';
COMMENT ON COLUMN EXCHG_FilePattern."ID" IS 'A unique identifier for this record';
COMMENT ON COLUMN EXCHG_FilePattern."DIRECTION" IS 'O for outbound, I for inbound';
COMMENT ON COLUMN EXCHG_FilePattern."FILETYPE" IS 'Transaction type';
COMMENT ON COLUMN EXCHG_FilePattern."GS08" IS 'GS08 element in EDI';
COMMENT ON COLUMN EXCHG_FilePattern."FILEPATTERN" IS 'File pattern to be used to search directories for files to pick up and process';
COMMENT ON COLUMN EXCHG_FilePattern."FILEPATTERN_EXT_PRE" IS 'Source file suffix';
COMMENT ON COLUMN EXCHG_FilePattern."FILEPATTERN_EXT_POST" IS 'Target file suffix';
COMMENT ON COLUMN EXCHG_FilePattern."XREF_IDS" IS 'Cross-references to EXCHG_FilePattern.ID to indicate TA1 and 999 transactions that need to be generated';

create table EXCHG_FilePatternRole
(
  ID              number         not null,
  Name            varchar2(120)  not null,
  FilePattern_IDs varchar2(1024) not null
);

COMMENT ON TABLE  EXCHG_FilePatternRole IS 'A table holds outbound partner file patterns';
COMMENT ON COLUMN EXCHG_FilePatternRole."ID" IS 'A unique identifier for this record';
COMMENT ON COLUMN EXCHG_FilePatternRole."NAME" IS 'Name of role';
COMMENT ON COLUMN EXCHG_FilePatternRole."FILEPATTERN_IDS" IS 'Cross reference(s) to EXCHG_FilePattern.ID';

create table EXCHG_ReturnCode
(
  Tool        varchar2(15)  not null,
  ReturnCode  varchar2(3)   not null,
  Description varchar2(400) not null
);

COMMENT ON TABLE  EXCHG_ReturnCode IS 'A table holds TIBCO return codes for their command line tools';
COMMENT ON COLUMN EXCHG_ReturnCode."TOOL" IS 'Name of the TIBCO tool';
COMMENT ON COLUMN EXCHG_ReturnCode."RETURNCODE" IS 'Returncode of the error';
COMMENT ON COLUMN EXCHG_ReturnCode."DESCRIPTION" IS 'Description of the error';

