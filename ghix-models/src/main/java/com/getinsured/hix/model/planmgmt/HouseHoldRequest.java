package com.getinsured.hix.model.planmgmt;

import java.util.Calendar;
import java.util.List;

import com.getinsured.hix.model.GHIXRequest;

public class HouseHoldRequest extends GHIXRequest {

	private List<Member> members;
	
	private String state;
	private String zipCode;
	private String county;
	private Calendar effectiveDate;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HouseHoldRequest [members=");
		builder.append(members.toString());
		builder.append(", state=");
		builder.append(state);
		builder.append(", zipCode=");
		builder.append(zipCode);
		builder.append(", county=");
		builder.append(county);
		builder.append(", effectiveDate=");
		builder.append(effectiveDate);
		builder.append("]");
		return builder.toString();
	}

	public Calendar getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Calendar effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}


	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	
}
