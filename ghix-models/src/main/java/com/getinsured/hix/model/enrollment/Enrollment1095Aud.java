package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.annotations.Type;

import com.getinsured.hix.model.AudId;

@Entity
@IdClass(AudId.class)
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "ENROLLMENT_1095_AUD")
public class Enrollment1095Aud implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@XmlElement(name="id")
	private int id;
	
	@Id
	@Column(name = "REV")
	private int rev;
	
	@Column(name = "REVTYPE")
	private int revType;
	
	@Column(name = "EXCHG_ASSIGNED_POLICY_ID")
	private Integer exchgAsignedPolicyId;
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId;
	
	@Column(name = "COVERAGE_YEAR")
	private Integer coverageYear;
	
	@Column(name = "CORRECTED_CHECKBOX_INDICATOR", length = 1)
	private String correctedCheckBoxIndicator;
	
	@Column(name = "VOID_CHECKBOX_INDICATOR", length = 1)
	private String voidCheckboxindicator;
	
	@Column(name = "POLICY_ISSUER_NAME")
	private String policyIssuerName; 
	
	@Column(name = "POLICY_START_DATE")
	private Date policyStartDate;
	
	@Column(name ="POLICY_END_DATE")
	private Date policyEndDate;
	
	@Column(name ="RESEND_PDF_FLAG", length = 1)
	private String resendPdfFlag;
	
	@Column(name="CORRECTION_SOURCE")
	private String correctionSource;
	
	@Column(name ="EDIT_TOOL_OVERWRITTEN")
	private String editToolOverwritten;
	
	@Column(name ="CORRECTION_INDICATOR_PDF")
	private String correctionIndicatorPdf;
	
	@Column(name ="CORRECTION_INDICATOR_XML")
	private String correctionIndicatorXml;
		
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Column(name="LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Column(name = "CORRECTED_RECORD_SEQ_NUM")
	private String correctedRecordSeqNum;
	
	@Column(name = "ECM_DOC_ID")
	private String ecmDocId;
	
	@Column(name = "PDF_GENERATION_TIMESTAMP")
	private Date pdfGeneratedOn;
	
	@Column(name = "CMS_XML_FILE_NAME")
	private String cmsXmlFileName;
	
	@Column(name = "CMS_XML_GENERATION_TIMESTAMP")
	private Date cmsXmlGeneratedOn;
	
	@Column(name = "UPDATE_NOTES")
	private String updateNotes;
	
	@Column(name = "PDF_SKIPPED_FLAG")
	private String pdfSkippedFlag;
	
	@Column(name = "PDF_SKIPPED_MSG")
	private String pdfSkippedMsg;
	
	@Column(name = "XML_SKIPPED_FLAG")
	private String xmlSkippedFlag;
	
	@Column(name = "XML_SKIPPED_MSG")
	private String xmlSkippedMsg;
	
	@Column(name="NOTICE_ID")
	private Integer notice;
	
	@Column(name = "IS_ACTIVE")
	private String isActive = "Y";
	
	@Column(name = "ORIGINAL_BATCH_ID")
	private String originalBatchId;
	
	@Column(name = "PDF_FILE_NAME")
	private String pdfFileName;
	
	@Column(name = "BATCH_CATEGORY_CODE")
	private String batchCategoryCode;
	
	@Column(name = "INBOUND_BCC")
	private String inboundBatchCategoryCode;
	
	@Column(name = "INBOUND_ACTION")
	private String inboundAction;
	
	@Column(name = "ENROLLMENT_IN_1095_ID")
	private Integer enrollmentIn1095Id;
	
	@Column(name = "RESUB_CRRCTN_IND")
	private String resubCorrectionInd;
	
	@Column(name = "IS_OBSOLETE")
	private String isObsolete = "N";
	
	@Transient
	private Boolean isOverwritten;
	
	@Type(type = "numeric_boolean")
    @Column(name="IS_FINANCIAL")
    private boolean isFinancial;
	
	@Type(type = "numeric_boolean")
    @Column(name="IS_TERM_BY_NON_PAYMENT")
    private boolean isTermByNonPayment;
	
	@Column(name = "PREM_PAID_TO_DATE_END")
	private Date premiumPaidToDateEnd;


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the exchgAsignedPolicyId
	 */
	public Integer getExchgAsignedPolicyId() {
		return exchgAsignedPolicyId;
	}

	/**
	 * @param exchgAsignedPolicyId the exchgAsignedPolicyId to set
	 */
	public void setExchgAsignedPolicyId(Integer exchgAsignedPolicyId) {
		this.exchgAsignedPolicyId = exchgAsignedPolicyId;
	}

	/**
	 * @return the houseHoldCaseId
	 */
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	/**
	 * @param houseHoldCaseId the houseHoldCaseId to set
	 */
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	/**
	 * @return the coverageYear
	 */
	public Integer getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	/**
	 * @return the correctedCheckBoxIndicator
	 */
	public String getCorrectedCheckBoxIndicator() {
		return correctedCheckBoxIndicator;
	}

	/**
	 * @param correctedCheckBoxIndicator the correctedCheckBoxIndicator to set
	 */
	public void setCorrectedCheckBoxIndicator(String correctedCheckBoxIndicator) {
		this.correctedCheckBoxIndicator = correctedCheckBoxIndicator;
	}

	/**
	 * @return the policyIssuerName
	 */
	public String getPolicyIssuerName() {
		return policyIssuerName;
	}

	/**
	 * @param policyIssuerName the policyIssuerName to set
	 */
	public void setPolicyIssuerName(String policyIssuerName) {
		this.policyIssuerName = policyIssuerName;
	}

	/**
	 * @return the policyStartDate
	 */
	public Date getPolicyStartDate() {
		return policyStartDate;
	}

	/**
	 * @param policyStartDate the policyStartDate to set
	 */
	public void setPolicyStartDate(Date policyStartDate) {
		this.policyStartDate = policyStartDate;
	}

	/**
	 * @return the policyEndDate
	 */
	public Date getPolicyEndDate() {
		return policyEndDate;
	}

	/**
	 * @param policyEndDate the policyEndDate to set
	 */
	public void setPolicyEndDate(Date policyEndDate) {
		this.policyEndDate = policyEndDate;
	}

	/**
	 * @return the resendPdfFlag
	 */
	public String getResendPdfFlag() {
		return resendPdfFlag;
	}

	/**
	 * @param resendPdfFlag the resendPdfFlag to set
	 */
	public void setResendPdfFlag(String resendPdfFlag) {
		this.resendPdfFlag = resendPdfFlag;
	}

	/**
	 * @return the correctionSource
	 */
	public String getCorrectionSource() {
		return correctionSource;
	}

	/**
	 * @param correctionSource the correctionSource to set
	 */
	public void setCorrectionSource(String correctionSource) {
		this.correctionSource = correctionSource;
	}

	/**
	 * @return the editToolOverwritten
	 */
	public String getEditToolOverwritten() {
		return editToolOverwritten;
	}

	/**
	 * @param editToolOverwritten the editToolOverwritten to set
	 */
	public void setEditToolOverwritten(String editToolOverwritten) {
		this.editToolOverwritten = editToolOverwritten;
	}

	/**
	 * @return the correctionIndicatorPdf
	 */
	public String getCorrectionIndicatorPdf() {
		return correctionIndicatorPdf;
	}

	/**
	 * @param correctionIndicatorPdf the correctionIndicatorPdf to set
	 */
	public void setCorrectionIndicatorPdf(String correctionIndicatorPdf) {
		this.correctionIndicatorPdf = correctionIndicatorPdf;
	}

	/**
	 * @return the correctionIndicatorXml
	 */
	public String getCorrectionIndicatorXml() {
		return correctionIndicatorXml;
	}

	/**
	 * @param correctionIndicatorXml the correctionIndicatorXml to set
	 */
	public void setCorrectionIndicatorXml(String correctionIndicatorXml) {
		this.correctionIndicatorXml = correctionIndicatorXml;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the correctedRecordSeqNum
	 */
	public String getCorrectedRecordSeqNum() {
		return correctedRecordSeqNum;
	}

	/**
	 * @param correctedRecordSeqNum the correctedRecordSeqNum to set
	 */
	public void setCorrectedRecordSeqNum(String correctedRecordSeqNum) {
		this.correctedRecordSeqNum = correctedRecordSeqNum;
	}

	/**
	 * @return the ecmDocId
	 */
	public String getEcmDocId() {
		return ecmDocId;
	}

	/**
	 * @param ecmDocId the ecmDocId to set
	 */
	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}

	/**
	 * @return the pdfGeneratedOn
	 */
	public Date getPdfGeneratedOn() {
		return pdfGeneratedOn;
	}

	/**
	 * @param pdfGeneratedOn the pdfGeneratedOn to set
	 */
	public void setPdfGeneratedOn(Date pdfGeneratedOn) {
		this.pdfGeneratedOn = pdfGeneratedOn;
	}

	/**
	 * @return the cmsXmlFileName
	 */
	public String getCmsXmlFileName() {
		return cmsXmlFileName;
	}

	/**
	 * @param cmsXmlFileName the cmsXmlFileName to set
	 */
	public void setCmsXmlFileName(String cmsXmlFileName) {
		this.cmsXmlFileName = cmsXmlFileName;
	}

	/**
	 * @return the cmsXmlGeneratedOn
	 */
	public Date getCmsXmlGeneratedOn() {
		return cmsXmlGeneratedOn;
	}

	/**
	 * @param cmsXmlGeneratedOn the cmsXmlGeneratedOn to set
	 */
	public void setCmsXmlGeneratedOn(Date cmsXmlGeneratedOn) {
		this.cmsXmlGeneratedOn = cmsXmlGeneratedOn;
	}

	/**
	 * @return the updateNotes
	 */
	public String getUpdateNotes() {
		return updateNotes;
	}

	/**
	 * @param updateNotes the updateNotes to set
	 */
	public void setUpdateNotes(String updateNotes) {
		this.updateNotes = updateNotes;
	}

	/**
	 * @return the pdfSkippedFlag
	 */
	public String getPdfSkippedFlag() {
		return pdfSkippedFlag;
	}

	/**
	 * @param pdfSkippedFlag the pdfSkippedFlag to set
	 */
	public void setPdfSkippedFlag(String pdfSkippedFlag) {
		this.pdfSkippedFlag = pdfSkippedFlag;
	}

	/**
	 * @return the pdfSkippedMsg
	 */
	public String getPdfSkippedMsg() {
		return pdfSkippedMsg;
	}

	/**
	 * @param pdfSkippedMsg the pdfSkippedMsg to set
	 */
	public void setPdfSkippedMsg(String pdfSkippedMsg) {
		this.pdfSkippedMsg = pdfSkippedMsg;
	}

	/**
	 * @return the xmlSkippedFlag
	 */
	public String getXmlSkippedFlag() {
		return xmlSkippedFlag;
	}

	/**
	 * @param xmlSkippedFlag the xmlSkippedFlag to set
	 */
	public void setXmlSkippedFlag(String xmlSkippedFlag) {
		this.xmlSkippedFlag = xmlSkippedFlag;
	}

	/**
	 * @return the xmlSkippedMsg
	 */
	public String getXmlSkippedMsg() {
		return xmlSkippedMsg;
	}

	/**
	 * @param xmlSkippedMsg the xmlSkippedMsg to set
	 */
	public void setXmlSkippedMsg(String xmlSkippedMsg) {
		this.xmlSkippedMsg = xmlSkippedMsg;
	}

	/**
	 * @return the notice
	 */
	public Integer getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(Integer notice) {
		this.notice = notice;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the originalBatchId
	 */
	public String getOriginalBatchId() {
		return originalBatchId;
	}

	/**
	 * @param originalBatchId the originalBatchId to set
	 */
	public void setOriginalBatchId(String originalBatchId) {
		this.originalBatchId = originalBatchId;
	}
	
	public String getVoidCheckboxindicator() {
		return voidCheckboxindicator;
	}

	public void setVoidCheckboxindicator(String voidCheckboxindicator) {
		this.voidCheckboxindicator = voidCheckboxindicator;
	}

	/**
	 * @return the pdfFileName
	 */
	public String getPdfFileName() {
		return pdfFileName;
	}

	/**
	 * @param pdfFileName the pdfFileName to set
	 */
	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}

	public String getBatchCategoryCode() {
		return batchCategoryCode;
	}

	public void setBatchCategoryCode(String batchCategoryCode) {
		this.batchCategoryCode = batchCategoryCode;
	}

	public String getInboundBatchCategoryCode() {
		return inboundBatchCategoryCode;
	}

	public void setInboundBatchCategoryCode(String inboundBatchCategoryCode) {
		this.inboundBatchCategoryCode = inboundBatchCategoryCode;
	}

	public String getInboundAction() {
		return inboundAction;
	}

	public void setInboundAction(String inboundAction) {
		this.inboundAction = inboundAction;
	}

	public Integer getEnrollmentIn1095Id() {
		return enrollmentIn1095Id;
	}

	public void setEnrollmentIn1095Id(Integer enrollmentIn1095Id) {
		this.enrollmentIn1095Id = enrollmentIn1095Id;
	}

	public String getResubCorrectionInd() {
		return resubCorrectionInd;
	}

	public void setResubCorrectionInd(String resubCorrectionInd) {
		this.resubCorrectionInd = resubCorrectionInd;
	}

	public Boolean getIsOverwritten() {
		return isOverwritten;
	}

	public void setIsOverwritten(Boolean isOverwritten) {
		this.isOverwritten = isOverwritten;
	}

	public String getIsObsolete() {
		return isObsolete;
	}

	public void setIsObsolete(String isObsolete) {
		this.isObsolete = isObsolete;
	}

	public boolean isFinancial() {
		return isFinancial;
	}

	public void setFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}

	public boolean isTermByNonPayment() {
		return isTermByNonPayment;
	}

	public void setTermByNonPayment(boolean isTermByNonPayment) {
		this.isTermByNonPayment = isTermByNonPayment;
	}

	public Date getPremiumPaidToDateEnd() {
		return premiumPaidToDateEnd;
	}

	public void setPremiumPaidToDateEnd(Date premiumPaidToDateEnd) {
		this.premiumPaidToDateEnd = premiumPaidToDateEnd;
	}

}
