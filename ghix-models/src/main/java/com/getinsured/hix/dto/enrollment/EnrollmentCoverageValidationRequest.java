/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

/**
 * @author negi_s
 *
 */
public class EnrollmentCoverageValidationRequest implements Serializable{

	public enum InsuranceType {
		HLT, DEN;
	}

	private String coverageStartDate;
	private String coverageEndDate;
	private String householdCaseId;
	private Integer enrollmentId;
	private Long employeeApplicationId;
	private InsuranceType insuranceType;
	private List<String> memberIdList;
	private List<EnrollmentMemberCoverageDTO> memberList;

	/**
	 * @return the coverageStartDate
	 */
	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	/**
	 * @param coverageStartDate the coverageStartDate to set
	 */
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	/**
	 * @return the coverageEndDate
	 */
	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	/**
	 * @param coverageEndDate the coverageEndDate to set
	 */
	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	/**
	 * @return the householdCaseId
	 */
	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	/**
	 * @param householdCaseId
	 *            the householdCaseId to set
	 */
	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	/**
	 * @return the employeeApplicationId
	 */
	public Long getEmployeeApplicationId() {
		return employeeApplicationId;
	}

	/**
	 * @param employeeApplicationId
	 *            the employeeApplicationId to set
	 */
	public void setEmployeeApplicationId(Long employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}

	/**
	 * @return the insuranceType
	 */
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}

	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

	/**
	 * @return the memberIdList
	 */
	public List<String> getMemberIdList() {
		return memberIdList;
	}

	/**
	 * @param memberIdList the memberIdList to set
	 */
	public void setMemberIdList(List<String> memberIdList) {
		this.memberIdList = memberIdList;
	}

	/**
	 * @return the memberList
	 */
	public List<EnrollmentMemberCoverageDTO> getMemberList() {
		return memberList;
	}

	/**
	 * @param memberList the memberList to set
	 */
	public void setMemberList(List<EnrollmentMemberCoverageDTO> memberList) {
		this.memberList = memberList;
	}

	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EnrollmentCoverageValidationRequest [coverageStartDate=" + coverageStartDate + ", coverageEndDate="
				+ coverageEndDate + ", householdCaseId=" + householdCaseId + ", enrollmentId=" + enrollmentId
				+ ", employeeApplicationId=" + employeeApplicationId + ", insuranceType=" + insuranceType
				+ ", memberIdList=" + memberIdList + ", memberList=" + memberList + "]";
	}

}