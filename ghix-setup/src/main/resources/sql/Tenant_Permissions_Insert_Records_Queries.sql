INSERT
INTO TENANT_PERMISSIONS
  (
    ID,
    PERMISSION_ID,
    TENANT_ID,
    CREATION_TIMESTAMP,
    LAST_UPDATE_TIMESTAMP
  )
  (SELECT
    TENANT_PERMISSIONS_SEQ.NEXTVAL,
   p.id,t.id,systimestamp,systimestamp FROM tenant t, permissions p
   where t.name='GetInsured'
  );