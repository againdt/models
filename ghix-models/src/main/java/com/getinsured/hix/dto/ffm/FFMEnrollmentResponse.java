package com.getinsured.hix.dto.ffm;

import java.util.Map;

public class FFMEnrollmentResponse {

	// Partner/ Consumer Identification
		private String informationExchangeSystemId;
		private String userType;
		private String stateExchangeCode;
		private String partnerAssignedConsumerId;
		
		// User Identity Assertion
		private String ffeAssignedConsumerId;
		
		// Enrollment Group
		private String enrollmentGroupFFEAssignedPolicyNumber;
		private String enrollmentGroupIssuerPolicyNumber;
		private String enrollmentGroupStartDate;
		private String enrollmentGroupEndDate;
		private String enrollmentGroupIssuerId;
		private String enrollmentGroupAssignedPlanId;
		
		// Transaction Information
		private String transactionType;
		private String transactionResponseCode;
		private String paymentTransactionId;
		
		// Premium Information
		private String premiumRatingArea;
		private String premiumTotalPremiumAmount;
		private String premiumAPTCAppliedAmount;
		private String premiumTotalIndividualResponsibilityAmount;
		private String premiumCSRLevelApplicable;
		
		// Member Actions 
		private String memberFFEAssignedApplicantId;
		private String memberIssuerAssignedMemberId;
		private String memberFFEAssignedMemberId;
		private String memberTypeCode;
		private String memberReasonCode;
		private String memberActionEffectiveDate;
		private String responseCode;
		
		private Integer enrollmentId;
		private String ffeAssignedPolNo;
		private String issuerAssignedPolNo;
		
		private Map<String, String> giAndFfmDataList=null;
		
		public String getInformationExchangeSystemId() {
			return informationExchangeSystemId;
		}
		public void setInformationExchangeSystemId(String informationExchangeSystemId) {
			this.informationExchangeSystemId = informationExchangeSystemId;
		}
		public String getUserType() {
			return userType;
		}
		public void setUserType(String userType) {
			this.userType = userType;
		}
		public String getStateExchangeCode() {
			return stateExchangeCode;
		}
		public void setStateExchangeCode(String stateExchangeCode) {
			this.stateExchangeCode = stateExchangeCode;
		}
		public String getPartnerAssignedConsumerId() {
			return partnerAssignedConsumerId;
		}
		public void setPartnerAssignedConsumerId(String partnerAssignedConsumerId) {
			this.partnerAssignedConsumerId = partnerAssignedConsumerId;
		}
		public String getFfeAssignedConsumerId() {
			return ffeAssignedConsumerId;
		}
		public void setFfeAssignedConsumerId(String ffeAssignedConsumerId) {
			this.ffeAssignedConsumerId = ffeAssignedConsumerId;
		}
		public String getEnrollmentGroupFFEAssignedPolicyNumber() {
			return enrollmentGroupFFEAssignedPolicyNumber;
		}
		public void setEnrollmentGroupFFEAssignedPolicyNumber(
				String enrollmentGroupFFEAssignedPolicyNumber) {
			this.enrollmentGroupFFEAssignedPolicyNumber = enrollmentGroupFFEAssignedPolicyNumber;
		}
		public String getEnrollmentGroupIssuerPolicyNumber() {
			return enrollmentGroupIssuerPolicyNumber;
		}
		public void setEnrollmentGroupIssuerPolicyNumber(
				String enrollmentGroupIssuerPolicyNumber) {
			this.enrollmentGroupIssuerPolicyNumber = enrollmentGroupIssuerPolicyNumber;
		}
		public String getEnrollmentGroupStartDate() {
			return enrollmentGroupStartDate;
		}
		public void setEnrollmentGroupStartDate(String enrollmentGroupStartDate) {
			this.enrollmentGroupStartDate = enrollmentGroupStartDate;
		}
		public String getEnrollmentGroupEndDate() {
			return enrollmentGroupEndDate;
		}
		public void setEnrollmentGroupEndDate(String enrollmentGroupEndDate) {
			this.enrollmentGroupEndDate = enrollmentGroupEndDate;
		}
		public String getEnrollmentGroupIssuerId() {
			return enrollmentGroupIssuerId;
		}
		public void setEnrollmentGroupIssuerId(String enrollmentGroupIssuerId) {
			this.enrollmentGroupIssuerId = enrollmentGroupIssuerId;
		}
		public String getEnrollmentGroupAssignedPlanId() {
			return enrollmentGroupAssignedPlanId;
		}
		public void setEnrollmentGroupAssignedPlanId(
				String enrollmentGroupAssignedPlanId) {
			this.enrollmentGroupAssignedPlanId = enrollmentGroupAssignedPlanId;
		}
		public String getTransactionType() {
			return transactionType;
		}
		public void setTransactionType(String transactionType) {
			this.transactionType = transactionType;
		}
		public String getTransactionResponseCode() {
			return transactionResponseCode;
		}
		public void setTransactionResponseCode(String transactionResponseCode) {
			this.transactionResponseCode = transactionResponseCode;
		}
		public String getPaymentTransactionId() {
			return paymentTransactionId;
		}
		public void setPaymentTransactionId(String paymentTransactionId) {
			this.paymentTransactionId = paymentTransactionId;
		}
		public String getPremiumRatingArea() {
			return premiumRatingArea;
		}
		public void setPremiumRatingArea(String premiumRatingArea) {
			this.premiumRatingArea = premiumRatingArea;
		}
		public String getPremiumTotalPremiumAmount() {
			return premiumTotalPremiumAmount;
		}
		public void setPremiumTotalPremiumAmount(String premiumTotalPremiumAmount) {
			this.premiumTotalPremiumAmount = premiumTotalPremiumAmount;
		}
		public String getPremiumAPTCAppliedAmount() {
			return premiumAPTCAppliedAmount;
		}
		public void setPremiumAPTCAppliedAmount(String premiumAPTCAppliedAmount) {
			this.premiumAPTCAppliedAmount = premiumAPTCAppliedAmount;
		}
		public String getPremiumTotalIndividualResponsibilityAmount() {
			return premiumTotalIndividualResponsibilityAmount;
		}
		public void setPremiumTotalIndividualResponsibilityAmount(
				String premiumTotalIndividualResponsibilityAmount) {
			this.premiumTotalIndividualResponsibilityAmount = premiumTotalIndividualResponsibilityAmount;
		}
		public String getPremiumCSRLevelApplicable() {
			return premiumCSRLevelApplicable;
		}
		public void setPremiumCSRLevelApplicable(String premiumCSRLevelApplicable) {
			this.premiumCSRLevelApplicable = premiumCSRLevelApplicable;
		}
		public String getMemberFFEAssignedApplicantId() {
			return memberFFEAssignedApplicantId;
		}
		public void setMemberFFEAssignedApplicantId(String memberFFEAssignedApplicantId) {
			this.memberFFEAssignedApplicantId = memberFFEAssignedApplicantId;
		}
		public String getMemberIssuerAssignedMemberId() {
			return memberIssuerAssignedMemberId;
		}
		public void setMemberIssuerAssignedMemberId(String memberIssuerAssignedMemberId) {
			this.memberIssuerAssignedMemberId = memberIssuerAssignedMemberId;
		}
		public String getMemberFFEAssignedMemberId() {
			return memberFFEAssignedMemberId;
		}
		public void setMemberFFEAssignedMemberId(String memberFFEAssignedMemberId) {
			this.memberFFEAssignedMemberId = memberFFEAssignedMemberId;
		}
		public String getMemberTypeCode() {
			return memberTypeCode;
		}
		public void setMemberTypeCode(String memberTypeCode) {
			this.memberTypeCode = memberTypeCode;
		}
		public String getMemberReasonCode() {
			return memberReasonCode;
		}
		public void setMemberReasonCode(String memberReasonCode) {
			this.memberReasonCode = memberReasonCode;
		}
		public String getMemberActionEffectiveDate() {
			return memberActionEffectiveDate;
		}
		public void setMemberActionEffectiveDate(String memberActionEffectiveDate) {
			this.memberActionEffectiveDate = memberActionEffectiveDate;
		}
		public String getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}
		public Integer getEnrollmentId() {
			return enrollmentId;
		}
		public void setEnrollmentId(Integer enrollmentId) {
			this.enrollmentId = enrollmentId;
		}
		public String getFfeAssignedPolNo() {
			return ffeAssignedPolNo;
		}
		public void setFfeAssignedPolNo(String ffeAssignedPolNo) {
			this.ffeAssignedPolNo = ffeAssignedPolNo;
		}
		public String getIssuerAssignedPolNo() {
			return issuerAssignedPolNo;
		}
		public void setIssuerAssignedPolNo(String issuerAssignedPolNo) {
			this.issuerAssignedPolNo = issuerAssignedPolNo;
		}
		public Map<String, String> getGiAndFfmDataList() {
			return giAndFfmDataList;
		}
		public void setGiAndFfmDataList(Map<String, String> giAndFfmDataList) {
			this.giAndFfmDataList = giAndFfmDataList;
		}
		
		
}
