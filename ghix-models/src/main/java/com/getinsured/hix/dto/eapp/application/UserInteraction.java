package com.getinsured.hix.dto.eapp.application;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserInteraction implements Serializable{
	private static final long serialVersionUID = 1L;
	private String lastPageVisited;
	/**
	 * format mm/dd/yyyy HH:MM:SS AM/PM
	 */
	private String whenLastPageVisited;
	/**
	 * append sections saved in the order
	 */
	private List<String> interactionStack;
	private Integer paymentCallCount;
	/**
	 * Date and time
	 * format MM/dd/yyyy HH:MM:SS AM/PM
	 */
	private List<String> accessStack;
	/**
	 * If we have either a logged in user/household has been
	 * created and is in session
	 */
	private Boolean householdInSession = Boolean.FALSE;
	/**
	 * If this is a logged in request or anonymous flow
	 */
	private Boolean userInSession = Boolean.FALSE;
	/**
	 * As part of the last 
	 */
	private Boolean isNewHousehold = Boolean.FALSE;
    private String submitIpAddress;
    private String userAgent;
	
	public String getLastPageVisited() {
		return lastPageVisited;
	}
	public void setLastPageVisited(String lastPageVisited) {
		this.lastPageVisited = lastPageVisited;
	}
	public String getWhenLastPageVisited() {
		return whenLastPageVisited;
	}
	public void setWhenLastPageVisited(String whenLastPageVisited) {
		this.whenLastPageVisited = whenLastPageVisited;
	}
	public List<String> getInteractionStack() {
		return interactionStack;
	}
	public void setInteractionStack(List<String> interactionStack) {
		this.interactionStack = interactionStack;
	}
	public List<String> getAccessStack() {
		return accessStack;
	}
	public void setAccessStack(List<String> accessStack) {
		this.accessStack = accessStack;
	}
	public Integer getPaymentCallCount() {
		return paymentCallCount;
	}
	public void setPaymentCallCount(Integer paymentCallCount) {
		this.paymentCallCount = paymentCallCount;
	}
	public Boolean getHouseholdInSession() {
		return householdInSession;
	}
	public void setHouseholdInSession(Boolean householdInSession) {
		this.householdInSession = householdInSession;
	}
	public Boolean getUserInSession() {
		return userInSession;
	}
	public void setUserInSession(Boolean userInSession) {
		this.userInSession = userInSession;
	}
	public Boolean getIsNewHousehold() {
		return isNewHousehold;
	}
	public void setIsNewHousehold(Boolean isNewHousehold) {
		this.isNewHousehold = isNewHousehold;
	}
    public void setSubmitIpAddress(String submitIpAddress) {
        this.submitIpAddress = submitIpAddress;
    }
    public String getSubmitIpAddress() {
        return submitIpAddress;
    }
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

	public static UserInteraction getSampleInstance(){
		UserInteraction interaction = new UserInteraction();
		interaction.accessStack = new ArrayList<String>();
		interaction.interactionStack = new ArrayList<String>();
		interaction.lastPageVisited = "1.Eligiblity";
		interaction.interactionStack.add("1:Eligiblity");
		interaction.interactionStack.add("2:Payment");
		interaction.accessStack.add("06/08/2015 10:15:23 AM");
		interaction.whenLastPageVisited = "06/08/2015 10:15:23 AM";
		interaction.paymentCallCount = 0;
		
		return interaction;
	}

}
