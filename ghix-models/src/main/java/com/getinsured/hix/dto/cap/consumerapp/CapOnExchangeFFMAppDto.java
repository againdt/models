package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.Date;

public class CapOnExchangeFFMAppDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long ssapAppId;
	private String enrollmentAppId;
	private String enrollmentConfirmationId;
	private String effectiveStartDate;
	private String applicationStatus ;

	public long getSsapAppId() {
		return ssapAppId;
	}

	public void setSsapAppId(long ssapAppId) {
		this.ssapAppId = ssapAppId;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getEnrollmentAppId() {
		return enrollmentAppId;
	}

	public void setEnrollmentAppId(String enrollmentAppId) {
		this.enrollmentAppId = enrollmentAppId;
	}

	public String getEnrollmentConfirmationId() {
		return enrollmentConfirmationId;
	}

	public void setEnrollmentConfirmationId(String enrollmentConfirmationId) {
		this.enrollmentConfirmationId = enrollmentConfirmationId;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

}
