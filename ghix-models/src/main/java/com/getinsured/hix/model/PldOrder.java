package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

public class PldOrder {

	public enum Status {
		CART, ORDER;
	}
	
	public enum Flag {
		YES, NO;
	}
	private int id;
	
	private String quality_rating;
	
	/*@ManyToOne
	@JoinColumn(name = "pld_household_id")
	private PldHousehold pldHousehold;*/
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Enumerated(EnumType.STRING)
	private Flag sadpFlag;
	
	private Date created;

	private Date updated;
	
	private Integer createdBy;

	private Integer updatedBy;
	
	/*@Fetch(FetchMode.SELECT)
	@OneToMany(mappedBy="pldOrder", fetch=FetchType.EAGER )
	private List<PldOrderItem> pldOrderItems;*/
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuality_rating() {
		return quality_rating;
	}

	public void setQuality_rating(String quality_rating) {
		this.quality_rating = quality_rating;
	}

	/*public PldHousehold getPldHousehold() {
		return pldHousehold;
	}

	public void setPldHousehold(PldHousehold pldHousehold) {
		this.pldHousehold = pldHousehold;
	}*/

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Flag getSadpFlag() {
		return sadpFlag;
	}

	public void setSadpFlag(Flag sadpFlag) {
		this.sadpFlag = sadpFlag;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}

	
}
