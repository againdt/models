package com.getinsured.hix.model.agency;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the agency_documents database table.
 * 
 */
@Entity
@Table(name="agency_documents")
@NamedQuery(name="AgencyDocument.findAll", query="SELECT a FROM AgencyDocument a")
public class AgencyDocument implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum DocumentType {
		REGISTRATION("Registration"),
		CONTRACT("Contract"),
		SUPPORTING("Supporting"),
		DECLARATION("E&O Declaration");
		
		String value = null;
		
		DocumentType(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
	}

	@Id
	@SequenceGenerator(name="AGENCY_DOCUMENTS_ID_GENERATOR", sequenceName="AGENCY_DOCUMENTS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AGENCY_DOCUMENTS_ID_GENERATOR")
	private Long id;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="document_mime")
	private String documentMime;

	@Column(name="document_name")
	private String documentName;

	@Column(name="document_type")
	private String documentType;

	@Column(name="ecm_document_id")
	private String ecmDocumentId;

	@Column(name="active_flag")
	private String activeFlag;
	
	//uni-directional many-to-one association to Agency
	@ManyToOne
	@JoinColumn(name="agency_id")
	private Agency agency;

	public AgencyDocument() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getDocumentMime() {
		return this.documentMime;
	}

	public void setDocumentMime(String documentMime) {
		this.documentMime = documentMime;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getEcmDocumentId() {
		return this.ecmDocumentId;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public void setEcmDocumentId(String ecmDocumentId) {
		this.ecmDocumentId = ecmDocumentId;
	}

	public Agency getAgency() {
		return this.agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

}