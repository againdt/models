package com.getinsured.hix.dto.consumer;

import java.io.Serializable;

public class HouseholdExtensionDto implements Serializable {

	
	private static final long serialVersionUID = 1L;

        private String dataType="";
	    // Household Level Data
		private String externalId;// Household External ID
		private String pinCode;  // Household pincode to access HRA flows
		private String hraFundingType; // Possible values are configured at flow level
		private double familyHraAmount=-1.0; // Household Level
		//private String ssn; // Household SSN
		private String hraStartDate;
		private String hraEndDate;
		private String hId; // Used for reconciliation of Household
		private String emailOptOut; // Email opt out flag
		private String languagePreference;
		
		private String eventId; // Qualifying Event Id
		private String eventDate; // Qualifying Event Date
		private String edeTransactionId;
		private String edeSsapId; // SSAP Id associated with EDE application.
		
		
		
		public String getDataType() {
			return dataType;
		}
		public void setDataType(String dataType) {
			this.dataType = dataType;
		}
		public String getExternalId() {
			return externalId;
		}
		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}
		public String getPinCode() {
			return pinCode;
		}
		public void setPinCode(String pinCode) {
			this.pinCode = pinCode;
		}
		public String getHraFundingType() {
			return hraFundingType;
		}
		public void setHraFundingType(String hraFundingType) {
			this.hraFundingType = hraFundingType;
		}
		public double getFamilyHraAmount() {
			return familyHraAmount;
		}
		public void setFamilyHraAmount(double familyHraAmount) {
			this.familyHraAmount = familyHraAmount;
		}
		
		public String getHraStartDate() {
			return hraStartDate;
		}
		public void setHraStartDate(String hraStartDate) {
			this.hraStartDate = hraStartDate;
		}
		public String getHraEndDate() {
			return hraEndDate;
		}
		public void setHraEndDate(String hraEndDate) {
			this.hraEndDate = hraEndDate;
		}
		public String gethId() {
			return hId;
		}
		public void sethId(String hId) {
			this.hId = hId;
		}
		public String getEmailOptOut() {
			return emailOptOut;
		}
		public void setEmailOptOut(String emailOptOut) {
			this.emailOptOut = emailOptOut;
		}
		public String getLanguagePreference() {
			return languagePreference;
		}
		public void setLanguagePreference(String languagePreference) {
			this.languagePreference = languagePreference;
		} 
		
		public String getEventId() {
			return eventId;
		}
		public void setEventId(String eventId) {
			this.eventId = eventId;
		}
		public String getEventDate() {
			return eventDate;
		}
		public void setEventDate(String eventDate) {
			this.eventDate = eventDate;
		}
		public String getEdeTransactionId() {
			return edeTransactionId;
		}
		public void setEdeTransactionId(String transactionId) {
			this.edeTransactionId = transactionId;
		}

        public String getEdeSsapId() {
            return edeSsapId;
        }

        public void setEdeSsapId(String edeSsapId) {
            this.edeSsapId = edeSsapId;
        }
}
