delete from NOTICE_TYPES where EMAIL_CLASS='TestNoticeTemplate';

Insert into NOTICE_TYPES 
(ID,BCC_ADDRESS,CATEGORY,CC_ADDRESS,CREATED,DESCRIPTION,EMAIL_CLASS,FROM_ADDRESS,MODULE_NAME,REPLY_TO,RET_ADDRESS,SUBJECT,TITLE,TYPE,UNSUBSCRIBE_COUNT,UPDATED,LANGUAGE) 
values 
(NOTICE_TYPES_SEQ.NEXTVAL,'vimo@xoriant.com','ENDUSER','vimo@xoriant.com',SYSTIMESTAMP,'test notice template','TestNoticeTemplate','vimo@xoriant.com','notices','vimo@xoriant.com','vimo@xoriant.com','Template','Template Notice','HTML_EMAIL',null,SYSTIMESTAMP,'US_EN');


update NOTICE_TYPES
set TEMPLATE='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Getinsured Invoice</title>
<link href="{host}/resources/css/testnotificationtemplate.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="topContainer">
  <div class="row">
    <table>
	<tr>
		<td style="padding: 20px 0 30px 0;">
	              <img src="{host}/resources/images/ms_exchange_image.jpg" width="500" height="66" alt="Mississippi Health Exchange" />
	       </td>
	</tr>
	<tr>
		<td colspan="3" style="border-bottom: 1px #C7E0B6 solid;"><img src="{host}/resources/images/Get_Insured_Logo.gif"
		width="125" alt="Getinsured Logo" /></td>
	</tr>

        <tr>

        <td colspan="1" style="text-align: left; font-size: 11px; padding-left: 
        0px;  padding-bottom: 20px;">Get Health Insurance Inc Ltd<br />

          P/O Box 97696 California, USA </td>

        <td colspan="2" class="smallRight" style="vertical-align: top; padding-right: 0px;  padding-bottom: 20px;">For more information, please 
        call us at (888) 888-8888 or visit us at www.exchange-name.com </td>

        </tr>
    </table>
  </div>
  <div class="row">
    <table>
      <tr>
        <td colspan="1" style="vertical-align: top;">
        <table class="yellowBox" style="width:200px; ">
            <!--Invoice name table-->

            <tr class="green">

              <td style=" padding: 10px; font-size: 13px;" 
              colspan="1"><strong>Invoice Name</strong></td>

            </tr>

            <!--end of invoice name-->

            <tr>

              <td><font style="font-size: 10px; text-transform: uppercase;">Bill 
              to</font><br />

                <strong>${employerInvoice.employerName}</strong><br />

                ${employerInvoice.addressLine1}<br />

                ${employerInvoice.addressLine2} </td>

            </tr>

            <!--Table within Invoice Name: Case ID, Invoice #, and Payment 
            Due-->

            <tr>

              <td><table style="width:200px;  border-top: 1px #C7E0B6 solid;">

                  <tr>

                    <td  style="text-align: right; line-height: 18px; white-space:nowrap;"> Case ID<br />

                      Invoice Number<br />

                      Payment Date<br />

                      Statement Date </td>

                    <td style="line-height: 18px; white-space:nowrap;"><strong> 
                    # ${employerInvoice.caseId} <br />

                      # ${employerInvoice.invoiceNumber}<br />

                      ${employerInvoice.paymentDate?date}<br />

                      ${employerInvoice.statementDate?date}</strong></td>

                  </tr>

                </table></td>

            </tr>

          </table>
	  </td>
       </tr>
    </table>
  </div>
</div>
<div>
<table style="width: 440px;  border: 1px solid #C7E0B6; ">
<#list bookList as bk>
<tr>
<td>
 ${bk.title} </td>
</tr>
  <#list bk.chapters as chapter>
           <tr>        
           <td>${chapter.title}</td>
            <td>${chapter.page}</td>
            </tr>
  </#list>
</#list>
</table>
</div>
</body>
</html>'
where EMAIL_CLASS='TestNoticeTemplate';