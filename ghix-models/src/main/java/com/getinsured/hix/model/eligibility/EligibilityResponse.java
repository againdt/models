package com.getinsured.hix.model.eligibility;

import com.getinsured.hix.model.GHIXResponse;

/**
 * All Eligibility response must extend this.
 * 
 * @author polimetla_b
 * @since 20-Feb-2013
 */
public class EligibilityResponse extends GHIXResponse{
	
	/**
	 * This transfers required fields to response.
	 * 
	 * @param req
	 */
	public EligibilityResponse(EligibilityRequest req)
	{	
		this.setApplicationID(req.getApplicationID());
		super.setAppID(req.getAppID());
	}
	
	private long applicationID;

	public long getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(long applicationID) {
		this.applicationID = applicationID;
	}
	
	

}
