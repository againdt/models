package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ENRL_CMS_OUT_SKIPPED_POLICY")
public class EnrollmentCmsOutSkippedPolicy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_OUT_SKIP_POL_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_OUT_SKIP_POL_SEQ", sequenceName = "ENRL_CMS_OUT_SKIP_POL_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name="EXCHANGE_ASSIGNED_POLICY_ID")
	private String exchangeAssignedPolicyId;
	
	@Column(name="ERROR_DESCRIPTION")
	private String errorDescription;
	
	@Column(name="ERROR_CODE")
	private String errorCode;
	
	@ManyToOne
	@JoinColumn(name="ENRL_CMS_OUT_ID")
	private EnrollmentCmsOut enrlCmsOut;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExchangeAssignedPolicyId() {
		return exchangeAssignedPolicyId;
	}

	public void setExchangeAssignedPolicyId(String exchangeAssignedPolicyId) {
		this.exchangeAssignedPolicyId = exchangeAssignedPolicyId;
	}

	public String getDescription() {
		return errorDescription;
	}

	public void setDescription(String errorDescription) {
		this.errorDescription = errorDescription.substring(0, errorDescription.length() > 1000 ? 1000 : errorDescription.length());
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public EnrollmentCmsOut getEnrlCmsOut() {
		return enrlCmsOut;
	}

	public void setEnrlCmsOut(EnrollmentCmsOut enrlCmsOut) {
		this.enrlCmsOut = enrlCmsOut;
	}
	
}
