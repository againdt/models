/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.getinsured.hix.model.DateAdapter;

/**
 * @author negi_s
 * @since 02/02/2015 
 * 		
 * HIX-60318:: This table stores the Enrollment ID, Member ID,
 * Event ID, Batch Execution Id, XML File Name, XML File Creation Date
 * and HIOS Issuer ID from the Outbound XML files
 * 
 */
@Entity
@XmlAccessorType(XmlAccessType.NONE)
@Table(name="ENRL_OUT_RECON_REPORT")
public class EnrollmentOutboundReconciliationReport implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_RECON_REPORT_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_RECON_REPORT_SEQ", sequenceName = "ENROLLMENT_RECON_REPORT_SEQ", allocationSize = 1)
	private int id;

	@XmlElement(name="enrollmentId")
	@Column(name="ENROLLMENT_ID")
	private Integer enrollmentId;
	
	@XmlElement(name="subscriberMemberId")
	@Column(name="SUBSCRIBER_MEMBER_ID")
	private String subscriberMemberId;

	@XmlElement(name="subscriberEventId")
	@Column(name="SUBSCRIBER_EVENT_ID")
	private Integer subscriberEventId;  
	
	@XmlElement(name="batchExecutionId")
	@Column(name="BATCH_EXECUTION_ID")
	private Long batchExecutionId;

	@Column(name="XML_FILE_NAME")
	private String xmlFileName;
	
	@Column(name="XML_FILE_CREATION_DATE")
	private Date xmlFileCreationDate;
	
	@XmlElement(name="hiosIssuerId")
	@Column(name="HIOS_ISSUER_ID")
	private String hiosIssuerId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;
	
	@Column(name="EDI_STATUS")
	private String ediStatus;
	
	@Column(name="ACK_TA1_STATUS")
	private String ackTa1Status;
	
	@Column(name = "ACK_TA1_REVD_DATE")
	private Date ackTa1ReceivedDate;
	
	@Column(name = "SUBSCRIBER_LINENO")
	private String subscriberLineNo;
	
	@Column(name = "INS03_EVENTTYPECODE")
	private String ins03EventTypeCode;
	
	@Column(name = "INS04_EVENTREASONCODE")
	private String ins04EventReasonCode;
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId;
	
	@Column(name = "ACK_999_STATUS")
	private String ack999Status;
	
	@Column(name = "ACK_999_REVD_DATE")
	private Date ack999ReceivedDate;
	
	@Column(name = "ISA09")
	private String isa09;
	
	@Column(name = "ISA10")
	private String isa10;
	
	@Column(name = "GS04")
	private String gs04;
	
	@Column(name = "GS05")
	private String gs05;
	
	@Column(name="EXCHG_RECON_ID")
	private Integer exchReconId;
	
	@Column(name = "ISA13")
	private Integer isa13;
	
	@Column(name = "ST02")
	private String st02;
	
	@Column(name = "GS06")
	private Integer gs06;
	
	@XmlElement(name="maintenanceType")
	@Column(name = "MAINTENANCE_TYPE")
	private String maintenanceType;
	
	@XmlElement(name="maintenanceReasonCode")
	@Column(name = "MAINTENANCE_REASON")
	private String maintenanceReasonCode; 
	
	@XmlElement(name="renpFlag")
	@Column(name = "RENPFLAG")
	private String rENPFlag;
	
	@XmlElement(name="coverageStartDate")
	@Column(name = "COVERAGE_STARTDATE")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date coverageStartDate;

	@XmlElement(name="coverageEndDate")
	@Column(name = "COVERAGE_ENDDATE")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date coverageEndDate;
	
	@Column(name = "TOTAL_QTY")
	private Integer totalQTY;
	
	@XmlElement(name="agentName")
	@Column(name = "AGENT_NAME")
	private String agentName;
	
	@XmlElement(name="agentID")
	@Column(name = "AGENT_ID")
	private String agentID;
	
	@Transient
	private String householdOrEmployeeCaseID;
	
	@Transient
	private String insurerName;
	

	
	public Integer getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Integer getEnrollmentId() {
		return enrollmentId;
	}


	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Long getBatchExecutionId() {
		return batchExecutionId;
	}


	public void setBatchExecutionId(Long batchExecutionId) {
		this.batchExecutionId = batchExecutionId;
	}


	public String getXmlFileName() {
		return xmlFileName;
	}


	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public Date getXmlFileCreationDate() {
		return xmlFileCreationDate;
	}


	public void setXmlFileCreationDate(Date xmlFileCreationDate) {
		this.xmlFileCreationDate = xmlFileCreationDate;
	}


	public String getHiosIssuerId() {
		return hiosIssuerId;
	}


	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
	}


	public String getHouseholdOrEmployeeCaseID() {
		return householdOrEmployeeCaseID;
	}


	public void setHouseholdOrEmployeeCaseID(String householdOrEmployeeCaseID) {
		this.householdOrEmployeeCaseID = householdOrEmployeeCaseID;
	}


	public String getInsurerName() {
		return insurerName;
	}


	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}


	


	public String getEdiStatus() {
		return ediStatus;
	}


	public void setEdiStatus(String ediStatus) {
		this.ediStatus = ediStatus;
	}


	/**
	 * @return the subscriberMemberId
	 */
	public String getSubscriberMemberId() {
		return subscriberMemberId;
	}


	/**
	 * @param subscriberMemberId the subscriberMemberId to set
	 */
	public void setSubscriberMemberId(String subscriberMemberId) {
		this.subscriberMemberId = subscriberMemberId;
	}


	/**
	 * @return the subscriberEventId
	 */
	public Integer getSubscriberEventId() {
		return subscriberEventId;
	}


	/**
	 * @param subscriberEventId the subscriberEventId to set
	 */
	public void setSubscriberEventId(Integer subscriberEventId) {
		this.subscriberEventId = subscriberEventId;
	}


	/**
	 * @return the ackTa1Status
	 */
	public String getAckTa1Status() {
		return ackTa1Status;
	}


	/**
	 * @param ackTa1Status the ackTa1Status to set
	 */
	public void setAckTa1Status(String ackTa1Status) {
		this.ackTa1Status = ackTa1Status;
	}


	/**
	 * @return the ackTa1ReceivedDate
	 */
	public Date getAckTa1ReceivedDate() {
		return ackTa1ReceivedDate;
	}


	/**
	 * @param ackTa1ReceivedDate the ackTa1ReceivedDate to set
	 */
	public void setAckTa1ReceivedDate(Date ackTa1ReceivedDate) {
		this.ackTa1ReceivedDate = ackTa1ReceivedDate;
	}



	public Integer getGs06() {
		return gs06;
	}


	public void setGs06(Integer gs06) {
		this.gs06 = gs06;
	}


	public String getSt02() {
		return st02;
	}


	public void setSt02(String st02) {
		this.st02 = st02;
	}


	/**
	 * @return the subscriberLineNo
	 */
	public String getSubscriberLineNo() {
		return subscriberLineNo;
	}


	/**
	 * @param subscriberLineNo the subscriberLineNo to set
	 */
	public void setSubscriberLineNo(String subscriberLineNo) {
		this.subscriberLineNo = subscriberLineNo;
	}


	/**
	 * @return the ins03EventTypeCode
	 */
	public String getIns03EventTypeCode() {
		return ins03EventTypeCode;
	}


	/**
	 * @param ins03EventTypeCode the ins03EventTypeCode to set
	 */
	public void setIns03EventTypeCode(String ins03EventTypeCode) {
		this.ins03EventTypeCode = ins03EventTypeCode;
	}


	/**
	 * @return the ins04EventReasonCode
	 */
	public String getIns04EventReasonCode() {
		return ins04EventReasonCode;
	}


	/**
	 * @param ins04EventReasonCode the ins04EventReasonCode to set
	 */
	public void setIns04EventReasonCode(String ins04EventReasonCode) {
		this.ins04EventReasonCode = ins04EventReasonCode;
	}


	/**
	 * @return the houseHoldCaseId
	 */
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}


	/**
	 * @param houseHoldCaseId the houseHoldCaseId to set
	 */
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}


	/**
	 * @return the ack999Status
	 */
	public String getAck999Status() {
		return ack999Status;
	}


	/**
	 * @param ack999Status the ack999Status to set
	 */
	public void setAck999Status(String ack999Status) {
		this.ack999Status = ack999Status;
	}


	/**
	 * @return the ack999ReceivedDate
	 */
	public Date getAck999ReceivedDate() {
		return ack999ReceivedDate;
	}


	/**
	 * @param ack999ReceivedDate the ack999ReceivedDate to set
	 */
	public void setAck999ReceivedDate(Date ack999ReceivedDate) {
		this.ack999ReceivedDate = ack999ReceivedDate;
	}

	public String getIsa09() {
		return isa09;
	}


	public void setIsa09(String isa09) {
		this.isa09 = isa09;
	}


	public String getIsa10() {
		return isa10;
	}


	public void setIsa10(String isa10) {
		this.isa10 = isa10;
	}


	public String getGs04() {
		return gs04;
	}


	public void setGs04(String gs04) {
		this.gs04 = gs04;
	}


	public String getGs05() {
		return gs05;
	}


	public void setGs05(String gs05) {
		this.gs05 = gs05;
	}


	/**
	 * @return the exchReconId
	 */
	public Integer getExchReconId() {
		return exchReconId;
	}


	/**
	 * @param exchReconId the exchReconId to set
	 */
	public void setExchReconId(Integer exchReconId) {
		this.exchReconId = exchReconId;
	}


	/**
	 * @return the isa13
	 */
	public Integer getIsa13() {
		return isa13;
	}


	/**
	 * @param isa13 the isa13 to set
	 */
	public void setIsa13(Integer isa13) {
		this.isa13 = isa13;
	}


	public String getMaintenanceType() {
		return maintenanceType;
	}


	public void setMaintenanceType(String maintenanceType) {
		this.maintenanceType = maintenanceType;
	}


	public String getMaintenanceReasonCode() {
		return maintenanceReasonCode;
	}


	public void setMaintenanceReasonCode(String maintenanceReasonCode) {
		this.maintenanceReasonCode = maintenanceReasonCode;
	}


	public String getrENPFlag() {
		return rENPFlag;
	}


	public void setrENPFlag(String rENPFlag) {
		this.rENPFlag = rENPFlag;
	}


	public Date getCoverageStartDate() {
		return coverageStartDate;
	}


	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}


	public Date getCoverageEndDate() {
		return coverageEndDate;
	}


	public void setCoverageEndDate(Date coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}


	public Integer getTotalQTY() {
		return totalQTY;
	}


	public void setTotalQTY(Integer totalQTY) {
		this.totalQTY = totalQTY;
	}


	public String getAgentName() {
		return agentName;
	}


	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}


	public String getAgentID() {
		return agentID;
	}


	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}
}
