DELETE FROM ROLE_PERMISSIONS WHERE role_id IN (SELECT id FROM roles WHERE name NOT IN('ADMIN', 'OPERATIONS'))
                           AND permission_id IN (SELECT id FROM permissions  
                           WHERE name = UPPER('TKM_LIST_QUEUE'));
