
package com.serff.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Insurer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Insurer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="companyInfo" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}CompanyInfo"/>
 *         &lt;element name="accreditationInfo" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}AccreditationInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issuerId",
    "companyInfo",
    "accreditationInfo"
})
@XmlRootElement(name="insurer")
public class Insurer {

    protected int issuerId;
    @XmlElement(required = true)
    protected CompanyInfo companyInfo;
    @XmlElement(required = true)
    protected AccreditationInfo accreditationInfo;

    /**
     * Gets the value of the issuerId property.
     * 
     */
    public int getIssuerId() {
        return issuerId;
    }

    /**
     * Sets the value of the issuerId property.
     * 
     */
    public void setIssuerId(int value) {
        this.issuerId = value;
    }

    /**
     * Gets the value of the companyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyInfo }
     *     
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * Sets the value of the companyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyInfo }
     *     
     */
    public void setCompanyInfo(CompanyInfo value) {
        this.companyInfo = value;
    }

    /**
     * Gets the value of the accreditationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccreditationInfo }
     *     
     */
    public AccreditationInfo getAccreditationInfo() {
        return accreditationInfo;
    }

    /**
     * Sets the value of the accreditationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccreditationInfo }
     *     
     */
    public void setAccreditationInfo(AccreditationInfo value) {
        this.accreditationInfo = value;
    }

}
