package com.getinsured.hix.dto.agency;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class AgencySiteLocationDto {
	private String id;
	@NotBlank
	@Size(max=50)
	private String siteLocationName;
	@NotNull
	@Valid
	private AgencyLocationDto location;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSiteLocationName() {
		return siteLocationName;
	}
	public void setSiteLocationName(String siteLocationName) {
		this.siteLocationName = siteLocationName;
	}
	public AgencyLocationDto getLocation() {
		return location;
	}
	public void setLocation(AgencyLocationDto location) {
		this.location = location;
	}
	
	
}
