package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="ENRL_RECON_ERROR")
public class EnrlReconError  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_error_seq")
	@SequenceGenerator(name = "enrl_recon_error_seq", sequenceName = "ENRL_RECON_ERROR_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="FILE_ID")
	private Integer fileId;
	
	@Column(name="IN_DATA")
	private String inData;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Column(name="ENROLLMENT_ID")
	private Long enrollmentId;
	
	@Column(name="SUBSCRIBER_ID")
	private Integer subscriberId;
	
	@Column(name="ERROR_MESSAGE")
	private String errorMessage;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public String getInData() {
		return inData;
	}

	public void setInData(String inData) {
		this.inData = inData;
	}

/*	public Integer getRecordCode() {
		return recordCode;
	}

	public void setRecordCode(Integer recordCode) {
		this.recordCode = recordCode;
	}*/

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Long getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Integer getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(Integer subscriberId) {
		this.subscriberId = subscriberId;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
	}

}
