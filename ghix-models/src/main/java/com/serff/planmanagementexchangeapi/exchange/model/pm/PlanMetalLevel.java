
package com.serff.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanMetalLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlanMetalLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Platinum"/>
 *     &lt;enumeration value="Gold"/>
 *     &lt;enumeration value="Silver"/>
 *     &lt;enumeration value="Bronze"/>
 *     &lt;enumeration value="Catastrophic"/>
 *     &lt;enumeration value="High"/>
 *     &lt;enumeration value="Low"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "")
@XmlEnum
@XmlRootElement(name="planMetalLevel")
public enum PlanMetalLevel {

    @XmlEnumValue("Platinum")
    PLATINUM("Platinum"),
    @XmlEnumValue("Gold")
    GOLD("Gold"),
    @XmlEnumValue("Silver")
    SILVER("Silver"),
    @XmlEnumValue("Bronze")
    BRONZE("Bronze"),
    @XmlEnumValue("Catastrophic")
    CATASTROPHIC("Catastrophic"),
    @XmlEnumValue("High")
    HIGH("High"),
    @XmlEnumValue("Low")
    LOW("Low");
    private final String value;

    PlanMetalLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlanMetalLevel fromValue(String v) {
        for (PlanMetalLevel c: PlanMetalLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
