package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.tkm.HistoryDto;
import com.getinsured.hix.dto.tkm.ReportDto;
import com.getinsured.hix.model.consumer.Household;

public class TkmTicketsResponse extends GHIXResponse implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TkmTickets tkmTickets = null;
	
	private List<TkmTickets> tkmTicketsList = null;
	
	private Map<String, Object> tkmTicketsMap = null;
	
	private List<String> tkmTicketsSubjectList = null;
	
	private List<HistoryDto> tkmTicketHistory = null;

	private List<TkmTicketTasks> taskList = null;
	
	private List<Comment> commentList = null;
	
	private List<String> documentPathList = null;
	
	private List<TkmDocuments> documentList = null;
	
	private List<ReportDto> ticketReportDtoList = null;
	
	private List<Object[]> tkmQueueUserDtoList = null;
	
	private String ticketStatus;
	
	private List<String> ticketNumbers;
	
	private List<AccountUser> requesters;
	
	private Map<String,Integer> ticketNumbersByPriorityOrType;
	
	private List<TaskFormProperties> tkmTicketFormProperties = null;
	
	private List<AccountUser> queueUsers;
	
	private List<Integer> userQueues;
	
	private TkmQuickActionDto tkmQuickActionDto;
	
	private String createdForName;
	
	private List<Household> householdList;
	
	public TkmTickets getTkmTickets() {
		return tkmTickets;
	}

	public void setTkmTickets(TkmTickets tkmTickets) {
		this.tkmTickets = tkmTickets;
	}

	public List<TkmTickets> getTkmTicketsList() {
		return tkmTicketsList;
	}

	public void setTkmTicketsList(List<TkmTickets> tkmTicketsList) {
		this.tkmTicketsList = tkmTicketsList;
	}

	public Map<String, Object> getTkmTicketsMap() {
		return tkmTicketsMap;
	}

	public void setTkmTicketsMap(Map<String, Object> tkmTicketsMap) {
		this.tkmTicketsMap = tkmTicketsMap;
	}

	public List<String> getTkmTicketsSubjectList() {
		return tkmTicketsSubjectList;
	}
	
	public List<ReportDto> getTicketReportDtoList() {
		return ticketReportDtoList;
	}

	public void setTicketReportDtoList(List<ReportDto> ticketReportDtoList) {
		this.ticketReportDtoList = ticketReportDtoList;
	}

	public void setTkmTicketsSubjectList(List<String> tkmTicketsSubjectList) {
		this.tkmTicketsSubjectList = tkmTicketsSubjectList;
	}
	public void setTkmTicketHistory(List<HistoryDto> tkmTicketHistory) {
		this.tkmTicketHistory = tkmTicketHistory;
	}
		public List<HistoryDto> getTkmTicketHistory() {
			return tkmTicketHistory;
				}

	/**
	 * @return the taskList
	 */
	public List<TkmTicketTasks> getTaskList() {
		return taskList;
	}

	/**
	 * @param taskList the taskList to set
	 */
	public void setTaskList(List<TkmTicketTasks> taskList) {
		this.taskList = taskList;
	}

	/**
	 * @return the commentList
	 */
	public List<Comment> getCommentList() {
		return commentList;
	}

	/**
	 * @param commentList the commentList to set
	 */
	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	/**
	 * @return the documentPathList
	 */
	public List<String> getDocumentPathList() {
		return documentPathList;
	}

	/**
	 * @param documentPathList the documentPathList to set
	 */
	public void setDocumentPathList(List<String> documentList) {
		this.documentPathList = documentList;
	}

	public List<TkmDocuments> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<TkmDocuments> documentList) {
		this.documentList = documentList;
	}

	/**
	 * @return the ticketStatus
	 */
	public String getTicketStatus() {
		return ticketStatus;
	}

	/**
	 * @param ticketStatus the ticketStatus to set
	 */
	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	/**
	 * @return the ticketNumbers
	 */
	public List<String> getTicketNumbers() {
		return ticketNumbers;
	}
	
	/**
	 * @param ticketNumbers the ticketNumbers to set
	 */
	public void setTicketNumbers(List<String> ticketNumbers) {
		this.ticketNumbers = ticketNumbers;
	}

	/**
	 * @return the requesters
	 */
	public List<AccountUser> getRequesters() {
		return requesters;
	}
	
	/**
	 * @param requesters the requesters to set
	 */
	public void setRequesters(List<AccountUser> requesters) {
		this.requesters = requesters;
	}

	public Map<String, Integer> getTicketNumbersByPriorityOrType() {
		return ticketNumbersByPriorityOrType;
	}

	public void setTicketNumbersByPriorityOrType(
			Map<String, Integer> ticketNumbersByPriorityOrType) {
		this.ticketNumbersByPriorityOrType = ticketNumbersByPriorityOrType;
	}

	public List<TaskFormProperties> getTkmTicketFomProperties() {
		return tkmTicketFormProperties;
	}

	public void setTkmTicketFomProperties(List<TaskFormProperties> tkmTicketFormProperties) {
		this.tkmTicketFormProperties = tkmTicketFormProperties;
	}

	public List<AccountUser> getQueueUsers() {
		return queueUsers;
	}

	public void setQueueUsers(List<AccountUser> queueUsers) {
		this.queueUsers = queueUsers;
	}

	public List<Integer> getUserQueues() {
		return userQueues;
	}

	public void setUserQueues(List<Integer> userQueues) {
		this.userQueues = userQueues;
	}

	public TkmQuickActionDto getTkmQuickActionDto() {
		return tkmQuickActionDto;
	}

	public void setTkmQuickActionDto(TkmQuickActionDto tkmQuickActionDto) {
		this.tkmQuickActionDto = tkmQuickActionDto;
	}
	
	public String getCreatedForName() {
		return createdForName;
	}

	public void setCreatedForName(String createdForName) {
		this.createdForName = createdForName;
	}

	public List<Household> getHouseholdList() {
		return householdList;
	}

	public void setHouseholdList(List<Household> householdList) {
		this.householdList = householdList;
	}
	
	public List<Object[]> getTkmQueueUserDtoList() {
		return tkmQueueUserDtoList;
	}

	public void setTkmQueueUserDtoList(List<Object[]> userList) {
		this.tkmQueueUserDtoList = userList;
	}

}