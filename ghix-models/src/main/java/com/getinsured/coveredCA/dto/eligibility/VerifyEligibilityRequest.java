package com.getinsured.coveredCA.dto.eligibility;

public class VerifyEligibilityRequest {
	
	private Long applicationId;
	private Double householdIncome;
	
	
	
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	public Double getHouseholdIncome() {
		return householdIncome;
	}
	public void setHouseholdIncome(Double householdIncome) {
		this.householdIncome = householdIncome;
	}
	
	

}
