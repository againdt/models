package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

/**
 * This class contains redundant information
 * to be mostly used by UI module to show information 
 * in a particular way
 * @author root
 *
 */
public class UIHelper implements Serializable{
	private static final long serialVersionUID = 1L;
	private boolean hasSpouse;
	private boolean hasDependent;
	private boolean childOnlyApp;
	private boolean oep;
	private int applicantCount;
	private boolean agent;
	private int initiatingAgentId = -1;
	private int capAgentId = -1;
    private String anyTobaccoUsage = "N";
    public String getAnyTobaccoUsage() {
        return anyTobaccoUsage;
    }
    public void setAnyTobaccoUsage(String anyTobaccoUsage) {
        this.anyTobaccoUsage = anyTobaccoUsage;
    }
    public boolean isHasSpouse() {
		return hasSpouse;
	}
	public void setHasSpouse(boolean hasSpouse) {
		this.hasSpouse = hasSpouse;
	}
	public boolean isHasDependent() {
		return hasDependent;
	}
	public void setHasDependent(boolean hasDependent) {
		this.hasDependent = hasDependent;
	}
	
	public boolean isChildOnlyApp() {
		return childOnlyApp;
	}
	public void setChildOnlyApp(boolean childOnlyApp) {
		this.childOnlyApp = childOnlyApp;
	}
	public boolean isOep() {
		return oep;
	}
	public void setOep(boolean oep) {
		this.oep = oep;
	}
	public int getApplicantCount() {
		return applicantCount;
	}
	public void setApplicantCount(int applicantCount) {
		this.applicantCount = applicantCount;
	}
	public boolean isAgent() {
		return agent;
	}
	public void setAgent(boolean agent) {
		this.agent = agent;
	}
	public int getInitiatingAgentId() {
		return initiatingAgentId;
	}
	public void setInitiatingAgentId(int initiatingAgentId) {
		this.initiatingAgentId = initiatingAgentId;
	}
    public int getCapAgentId() {
        return capAgentId;
    }
    public void setCapAgentId(int capAgentId) {
        this.capAgentId = capAgentId;
    }

    public static UIHelper getSampleInstance(){
		UIHelper helper = new UIHelper();
		helper.setHasDependent(true);
		helper.setHasSpouse(true);
		helper.setChildOnlyApp(false);
		helper.setOep(false);
		helper.setApplicantCount(1);
		helper.setAgent(false);
        helper.setAnyTobaccoUsage("N");
		return helper;
	}
	
}
