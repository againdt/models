DELETE FROM LOOKUP_VALUE WHERE LOOKUP_TYPE_ID=(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='PLAN_LEVEL');
DELETE FROM LOOKUP_TYPE WHERE NAME='PLAN_LEVEL';

INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'QDP_PLAN_LEVEL', 'string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'GOLD','Gold','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'SILVER','Silver','');
