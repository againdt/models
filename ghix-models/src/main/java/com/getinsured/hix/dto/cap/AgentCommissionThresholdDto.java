package com.getinsured.hix.dto.cap;

public class AgentCommissionThresholdDto {
	
	private String meetingThresholdActual;
	private String actualESR="0";
	private String loginHoursWeek1="0";
	private String ajdOccupancyWeek1="0";
	private String loginHoursWeek2="0";
	private String ajdOccupancyWeek2="0";
	
	private String meetingThresholdGoal="-";
	private String esrGoal="20%";
	private String shiftHours;
	private String adjOccupancy="70%";
	public String getMeetingThresholdActual() {
		return meetingThresholdActual;
	}
	public void setMeetingThresholdActual(String meetingThresholdActual) {
		this.meetingThresholdActual = meetingThresholdActual;
	}
	public String getActualESR() {
		return actualESR;
	}
	public void setActualESR(String actualESR) {
		this.actualESR = actualESR;
	}
	public String getLoginHoursWeek1() {
		return loginHoursWeek1;
	}
	public void setLoginHoursWeek1(String loginHoursWeek1) {
		this.loginHoursWeek1 = loginHoursWeek1;
	}
	public String getAjdOccupancyWeek1() {
		return ajdOccupancyWeek1;
	}
	public void setAjdOccupancyWeek1(String ajdOccupancyWeek1) {
		this.ajdOccupancyWeek1 = ajdOccupancyWeek1;
	}
	public String getLoginHoursWeek2() {
		return loginHoursWeek2;
	}
	public void setLoginHoursWeek2(String loginHoursWeek2) {
		this.loginHoursWeek2 = loginHoursWeek2;
	}
	public String getAjdOccupancyWeek2() {
		return ajdOccupancyWeek2;
	}
	public void setAjdOccupancyWeek2(String ajdOccupancyWeek2) {
		this.ajdOccupancyWeek2 = ajdOccupancyWeek2;
	}
	public String getMeetingThresholdGoal() {
		return meetingThresholdGoal;
	}
	public void setMeetingThresholdGoal(String meetingThresholdGoal) {
		this.meetingThresholdGoal = meetingThresholdGoal;
	}
	public String getEsrGoal() {
		return esrGoal;
	}
	public void setEsrGoal(String esrGoal) {
		this.esrGoal = esrGoal;
	}
	public String getShiftHours() {
		return shiftHours;
	}
	public void setShiftHours(String shiftHours) {
		this.shiftHours = shiftHours;
	}
	public String getAdjOccupancy() {
		return adjOccupancy;
	}
	public void setAdjOccupancy(String adjOccupancy) {
		this.adjOccupancy = adjOccupancy;
	}
	
	
	

}
