
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Month by month Policy inforation 
 * 
 * <p>Java class for MonthlyPolicyInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MonthlyPolicyInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MonthlyPremiumAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MonthlyPremiumSLCSPAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MonthlyAdvancedPTCAmt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthlyPolicyInformationType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "monthlyPremiumAmt",
    "monthlyPremiumSLCSPAmt",
    "monthlyAdvancedPTCAmt"
})
public class MonthlyPolicyInformationType {

    @XmlElement(name = "MonthlyPremiumAmt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String monthlyPremiumAmt;
    @XmlElement(name = "MonthlyPremiumSLCSPAmt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String monthlyPremiumSLCSPAmt;
    @XmlElement(name = "MonthlyAdvancedPTCAmt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String monthlyAdvancedPTCAmt;

    /**
     * Gets the value of the monthlyPremiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyPremiumAmt() {
        return monthlyPremiumAmt;
    }

    /**
     * Sets the value of the monthlyPremiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyPremiumAmt(String value) {
        this.monthlyPremiumAmt = value;
    }

    /**
     * Gets the value of the monthlyPremiumSLCSPAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyPremiumSLCSPAmt() {
        return monthlyPremiumSLCSPAmt;
    }

    /**
     * Sets the value of the monthlyPremiumSLCSPAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyPremiumSLCSPAmt(String value) {
        this.monthlyPremiumSLCSPAmt = value;
    }

    /**
     * Gets the value of the monthlyAdvancedPTCAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyAdvancedPTCAmt() {
        return monthlyAdvancedPTCAmt;
    }

    /**
     * Sets the value of the monthlyAdvancedPTCAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyAdvancedPTCAmt(String value) {
        this.monthlyAdvancedPTCAmt = value;
    }

}
