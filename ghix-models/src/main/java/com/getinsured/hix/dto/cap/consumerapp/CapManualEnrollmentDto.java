package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.timeshift.TimeShifterUtil;

public class CapManualEnrollmentDto implements Serializable {

	private static final long serialVersionUID = -4026130182440044058L;

	private Integer enrollmentId;
	private String insuranceCompany;
	private String metalTier;
	private String planName;
	private String planID;
	private Float premium;
	private String effectiveDate;
	private String exchangeType;
	private Float aptc;
	private String confirmationNumber;
	private Float netPremium;
	private String policyNumber;
	// if this enrollment is manual or automatic
	private boolean manualModality;
	private String enrollmentStatus;
	
	private String subscriberUserName;
	private String subscriberPassword;
	
	private HouseholdDTO householdDTO;
	private String prefferedTimeToContact;
	private String insuranceType;
	private SsapApplicationDTO applicationDto;
	private List<SsapApplicantDTO> applicantDtoList;
	private ConsumerDocumentDTO consumerDocumentDto;
	private String loggedInUserName;// this is for accessing enrollmentEndPoints to set the real logged in user (set the cap agent id in case of csr is logged in).
	private EffectiveApplicationDTO effectiveApplicationDTO;
	private boolean ffmAgentSubmit;
	private String termLength; // HIX-63636
	private String benefitAmt;
	private String issuerId;
	private String issuerName;
	private String hiosIssuerId;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getMetalTier() {
		return metalTier;
	}

	public void setMetalTier(String metalTier) {
		this.metalTier = metalTier;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}
	
	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public Float getAptc() {
		return aptc;
	}

	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	public Float getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public boolean isManualModality() {
		return manualModality;
	}

	public void setManualModality(boolean manualModality) {
		this.manualModality = manualModality;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	public String getSubscriberUserName() {
		return subscriberUserName;
	}
	public void setSubscriberUserName(String subscriberUserName) {
		this.subscriberUserName = subscriberUserName;
	}
	public String getSubscriberPassword() {
		return subscriberPassword;
	}
	public void setSubscriberPassword(String subscriberPassword) {
		this.subscriberPassword = subscriberPassword;
	}
	
	public HouseholdDTO getHouseholdDTO() {
		return householdDTO;
	}
	
	public void setHouseholdDTO(HouseholdDTO householdDTO) {
		this.householdDTO = householdDTO;
	}
	
	public SsapApplicationDTO getApplicationDto() {
		return applicationDto;
	}
	
	public void setApplicationDto(SsapApplicationDTO applicationDto) {
		this.applicationDto = applicationDto;
	}
	
	public List<SsapApplicantDTO> getApplicantDtoList() {
		return applicantDtoList;
	}
	
	public void setApplicantDtoList(List<SsapApplicantDTO> applicantDtoList) {
		this.applicantDtoList = applicantDtoList;
	}
	
	public ConsumerDocumentDTO getConsumerDocumentDto() {
		return consumerDocumentDto;
	}
	
	public void setConsumerDocumentDto(ConsumerDocumentDTO consumerDocumentDto) {
		this.consumerDocumentDto = consumerDocumentDto;
	}
	
	public void setExchgIndivIdentifierForApplicantsIfNecessary() {
		int count=0;// needed to add  count as mili seconds are same for the execution time of the for loop
		for (SsapApplicantDTO applicant : applicantDtoList) {
			if (StringUtils.isEmpty(applicant.getExchgIndivIdentifier())) {
				applicant.setExchgIndivIdentifier("CAP_"
						+ TimeShifterUtil.currentTimeMillis()+count);
				count++;
			}
		}
	}
	
	public String getLoggedInUserName() {
		return loggedInUserName;
	}

	public void setLoggedInUserName(String loggedInUserName) {
		this.loggedInUserName = loggedInUserName;
	}

	public EffectiveApplicationDTO getEffectiveApplicationDTO() {
		return effectiveApplicationDTO;
	}

	public void setEffectiveApplicationDTO(
			EffectiveApplicationDTO effectiveApplicationDTO) {
		this.effectiveApplicationDTO = effectiveApplicationDTO;
	}
	
	public String getPrefferedTimeToContact() {
		return prefferedTimeToContact;
	}
	public void setPrefferedTimeToContact(String prefferedTimeToContact) {
		this.prefferedTimeToContact = prefferedTimeToContact;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public boolean isFfmAgentSubmit() {
		return ffmAgentSubmit;
	}
	
	public void setFfmAgentSubmit(boolean ffmAgentSubmit) {
		this.ffmAgentSubmit = ffmAgentSubmit;
	}

	public String getTermLength() {
		return termLength;
	}

	public void setTermLength(String termLength) {
		this.termLength = termLength;
	}

	public String getBenefitAmt() {
		return benefitAmt;
	}

	public void setBenefitAmt(String benefitAmt) {
		this.benefitAmt = benefitAmt;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
}
