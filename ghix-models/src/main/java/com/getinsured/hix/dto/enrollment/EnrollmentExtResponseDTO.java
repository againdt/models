/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * Enrollment ACA response DTO
 * @author negi_s
 * @since 13/09/2017
 */
public class EnrollmentExtResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String status;
	private Integer responseCode;
	private String responseMsg;
	private String url;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		return "EnrollmentExtResponseDTO [status=" + status + ", responseCode=" + responseCode + ", responseMsg="
				+ responseMsg + ", url=" + url + "]";
	}

}
