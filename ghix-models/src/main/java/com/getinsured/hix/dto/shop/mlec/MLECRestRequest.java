package com.getinsured.hix.dto.shop.mlec;

import java.util.Date;
import java.util.List;

public class MLECRestRequest {
	private Integer employerId;
	private String employerQuotingZip;
	private String employerQuotingCountyCode;
	private String employeeQuotingZip;
	private String employeeQuotingCountyCode;
	private Date effectiveDate;
	private Integer benchmarkPlanId;
	private List<Member> members;
	private Integer employeeApplicationId;
	
	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public String getEmployerQuotingZip() {
		return employerQuotingZip;
	}

	public void setEmployerQuotingZip(String employerQuotingZip) {
		this.employerQuotingZip = employerQuotingZip;
	}

	public String getEmployerQuotingCountyCode() {
		return employerQuotingCountyCode;
	}

	public void setEmployerQuotingCountyCode(String employerQuotingCountyCode) {
		this.employerQuotingCountyCode = employerQuotingCountyCode;
	}

	public String getEmployeeQuotingZip() {
		return employeeQuotingZip;
	}

	public void setEmployeeQuotingZip(String employeeQuotingZip) {
		this.employeeQuotingZip = employeeQuotingZip;
	}

	public String getEmployeeQuotingCountyCode() {
		return employeeQuotingCountyCode;
	}

	public void setEmployeeQuotingCountyCode(String employeeQuotingCountyCode) {
		this.employeeQuotingCountyCode = employeeQuotingCountyCode;
	}


	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Integer getBenchmarkPlanId() {
		return benchmarkPlanId;
	}

	public void setBenchmarkPlanId(Integer benchmarkPlanId) {
		this.benchmarkPlanId = benchmarkPlanId;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public Integer getEmployeeApplicationId() {
		return employeeApplicationId;
	}

	public void setEmployeeApplicationId(Integer employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}
}
