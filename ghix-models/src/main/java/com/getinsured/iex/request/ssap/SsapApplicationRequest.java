package com.getinsured.iex.request.ssap;

import java.math.BigDecimal;
import java.util.List;

public class SsapApplicationRequest {
	
	private String caseNumber;
	private String applicationStatus;
	private long ssapApplicationId;
	private List<String> exchgIndivIdList;
	private String effectiveDate;
	private BigDecimal userId;
	private String applicantGuidToRemove;
	private String applicationDentalStatus;
	private String enrlRenewalFlag;
	private boolean isApplicationDentalStatusUpdate = false;
	private boolean isAppUpdateByReInstatement = false;
	private boolean isEnrollmentCanceledOrTerminated = false;
	
	public String getCaseNumber() {
		return caseNumber;
	}
	
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public String getApplicationStatus() {
		return applicationStatus;
	}
	
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	
	public long getSsapApplicationId() {
		return ssapApplicationId;
	}
	
	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getEnrlRenewalFlag() {
		return enrlRenewalFlag;
	}
	public void setEnrlRenewalFlag(String enrlRenewalFlag) {
		this.enrlRenewalFlag = enrlRenewalFlag;
	}
	
	public String getApplicantGuidToRemove() {
		return applicantGuidToRemove;
	}

	public void setApplicantGuidToRemove(String applicantGuidToRemove) {
		this.applicantGuidToRemove = applicantGuidToRemove;
	}
	
	public List<String> getExchgIndivIdList() {
		return exchgIndivIdList;
	}

	public void setExchgIndivIdList(List<String> exchgIndivIdList) {
		this.exchgIndivIdList = exchgIndivIdList;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}	
	
	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	
	public String getApplicationDentalStatus() {
		return applicationDentalStatus;
	}
	
	public void setApplicationDentalStatus(String applicationDentalStatus) {
		this.applicationDentalStatus = applicationDentalStatus;
	}
	
	public boolean isApplicationDentalStatusUpdate() {
		return isApplicationDentalStatusUpdate;
	}

	public void setApplicationDentalStatusUpdate(boolean isApplicationDentalStatusUpdate) {
		this.isApplicationDentalStatusUpdate = isApplicationDentalStatusUpdate;
	}
	
	public boolean isAppUpdateByReInstatement() {
		return isAppUpdateByReInstatement;
	}
	
	public void setAppUpdateByReInstatement(boolean isAppUpdateByReInstatement) {
		this.isAppUpdateByReInstatement = isAppUpdateByReInstatement;
	}
	
	public boolean isEnrollmentCanceledOrTerminated() {
		return isEnrollmentCanceledOrTerminated;
	}
	
	public void setEnrollmentCanceledOrTerminated(boolean isEnrollmentCanceledOrTerminated) {
		this.isEnrollmentCanceledOrTerminated = isEnrollmentCanceledOrTerminated;
	}

	@Override
	public String toString() {
		return "SsapApplicationRequest [caseNumber=" + caseNumber + ", applicationStatus=" + applicationStatus
				+ ", ssapApplicationId=" + ssapApplicationId + ", exchgIndivIdList=" + exchgIndivIdList
				+ ", effectiveDate=" + effectiveDate + ", userId=" + userId + ", applicantGuidToRemove="
				+ applicantGuidToRemove + ", applicationDentalStatus=" + applicationDentalStatus + ", enrlRenewalFlag="
				+ enrlRenewalFlag + ", isApplicationDentalStatusUpdate=" + isApplicationDentalStatusUpdate
				+ ", isAppUpdateByReInstatement=" + isAppUpdateByReInstatement + ", isEnrollmentCanceledOrTerminated="
				+ isEnrollmentCanceledOrTerminated + "]";
	}

}
