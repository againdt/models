package com.getinsured.hix.model.prescreen;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class PlanDataKey implements Serializable
{


  private static final long serialVersionUID = 4376872127315053281L;

  @Column(name = "ZIPCODE", nullable = false)
  private String zipCode;

  @Column(name = "FIPS")
  private String countyCode;

  @Column(name = "STATE")
  private String state;

  @Column(name = "YEAR")
  private int year;

  public String getZipCode()
  {
    return zipCode;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }

  public String getCountyCode()
  {
    return countyCode;
  }

  public void setCountyCode(String countyCode)
  {
    this.countyCode = countyCode;
  }

  public String getState()
  {
    return state;
  }

  public void setState(String state)
  {
    this.state = state;
  }

  public int getYear()
  {
    return year;
  }

  public void setYear(int year)
  {
    this.year = year;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    PlanDataKey that = (PlanDataKey) o;

    if (year != that.year) return false;
    if (!zipCode.equals(that.zipCode)) return false;
    if (!countyCode.equals(that.countyCode)) return false;
    return state.equals(that.state);

  }

  @Override
  public int hashCode()
  {
    int result = zipCode.hashCode();
    result = 31 * result + countyCode.hashCode();
    result = 31 * result + state.hashCode();
    result = 31 * result + year;
    return result;
  }
}

