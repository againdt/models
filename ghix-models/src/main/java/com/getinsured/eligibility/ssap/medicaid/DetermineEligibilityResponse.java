package com.getinsured.eligibility.ssap.medicaid;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.model.estimator.mini.Member;

/*
 * @author jape_g
 * 
 */


public class DetermineEligibilityResponse {
	
		private String aptcAmount;
	    private List<Member> Members;
	    private int planId;
	    private String planName;
		private BigDecimal premiumAfterCredit;
		private BigDecimal premiumBeforeCredit;
		private PlanAvailabilityStatus planAvailabilityStatus;
		private String exchangeType; 
		
		
	    
	    public PlanAvailabilityStatus getPlanAvailabilityStatus() {
			return planAvailabilityStatus;
		}
		public void setPlanAvailabilityStatus(
				PlanAvailabilityStatus planAvailabilityStatus) {
			this.planAvailabilityStatus = planAvailabilityStatus;
		}
		public String getAptcAmount() {
			return aptcAmount;
		}
		public void setAptcAmount(String aptcAmount) {
			this.aptcAmount = aptcAmount;
		}
		public List<Member> getMembers() {
			return Members;
		}
		public void setMembers(List<Member> members) {
			Members = members;
		}
		public int getPlanId() {
			return planId;
		}
		public void setPlanId(int planId) {
			this.planId = planId;
		}
		public BigDecimal getPremiumAfterCredit() {
			return premiumAfterCredit;
		}
		public void setPremiumAfterCredit(BigDecimal premiumAfterCredit) {
			this.premiumAfterCredit = premiumAfterCredit;
		}
		public BigDecimal getPremiumBeforeCredit() {
			return premiumBeforeCredit;
		}
		public void setPremiumBeforeCredit(BigDecimal premiumBeforeCredit) {
			this.premiumBeforeCredit = premiumBeforeCredit;
		}
		public String getPlanName() {
			return planName;
		}
		public void setPlanName(String planName) {
			this.planName = planName;
		}
		public String getExchangeType() {
			return exchangeType;
		}
		public void setExchangeType(String exchangeType) {
			this.exchangeType = exchangeType;
		}
	}


