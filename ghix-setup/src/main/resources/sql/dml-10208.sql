UPDATE region set region_name='Region 3' where region_name='Region 2' and county ='YOLO';
UPDATE region set region_name='Region 3' where region_name='Region 2' and county ='SACRAMENTO';
UPDATE region set region_name='Region 3' where region_name='Region 2' and county ='PLACER';
UPDATE region set region_name='Region 3' where region_name='Region 2' and county ='EL DORADO';

insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '93623', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95306', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95311', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95318', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95325', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95338', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95345', 'Region 10');
insert into region values (REGION_SEQ.NEXTVAL, 'MARIPOSA', '95389', 'Region 10'); 

insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93201', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93207', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93208', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93218', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93219', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93221', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93223', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93227', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93235', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93237', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93244', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93247', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93256', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93257', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93258', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93260', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93261', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93262', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93265', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93267', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93270', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93271', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93272', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93274', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93275', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93277', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93278', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93279', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93282', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93286', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93290', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93291', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93292', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93293', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93603', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93615', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93618', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93633', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93647', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93666', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93670', 'Region 10'); 
insert into region values (REGION_SEQ.NEXTVAL, 'TULARE', '93673', 'Region 10'); 