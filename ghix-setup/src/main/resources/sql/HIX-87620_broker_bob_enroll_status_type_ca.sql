INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE, DESCRIPTION)
			VALUES (LOOKUPTYPE_SEQ.nextval,'ENROLLMENT_STATUS_TYPE','string','Enrollment Status Type');

			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'ENROLLMENT_STATUS_TYPE')
			,'PENDING', 'Pending', NULL, 'N', 'Pending', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'ENROLLMENT_STATUS_TYPE')
			,'ENROLLED', 'Enrolled', NULL, 'N', 'Enrolled', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'ENROLLMENT_STATUS_TYPE')
			,'TERMINATED', 'Terminated', NULL, 'N', 'Terminated', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));
			INSERT INTO LOOKUP_VALUE (LOOKUP_VALUE_ID, LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL,
						  PARENT_LOOKUP_VALUE_ID, ISOBSOLETE, DESCRIPTION, LOOKUP_LOCALE_ID)
					VALUES (LOOKUPVALUE_SEQ.nextval, (SELECT lookup_type_id FROM LOOKUP_TYPE WHERE name = 'ENROLLMENT_STATUS_TYPE')
			,'CANCELLED', 'Cancelled', NULL, 'N', 'Cancelled', (SELECT id FROM lookup_locale WHERE locale_language = 'ENGLISH'));