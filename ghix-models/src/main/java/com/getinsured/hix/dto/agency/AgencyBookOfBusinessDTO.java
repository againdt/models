package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

public class AgencyBookOfBusinessDTO implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private long count;
	private List<IndividualDetailsDTO> listOfIndividuals;
	private int pageNumber;
	private String sortBy;
	private String sortOrder;
	private String agencyManagerCertificationStatus;
	
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public List<IndividualDetailsDTO> getListOfIndividuals() {
		return listOfIndividuals;
	}
	public void setListOfIndividuals(List<IndividualDetailsDTO> listOfIndividuals) {
		this.listOfIndividuals = listOfIndividuals;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getAgencyManagerCertificationStatus() {
		return agencyManagerCertificationStatus;
	}
	public void setAgencyManagerCertificationStatus(String agencyManagerCertificationStatus) {
		this.agencyManagerCertificationStatus = agencyManagerCertificationStatus;
	}

	
}
