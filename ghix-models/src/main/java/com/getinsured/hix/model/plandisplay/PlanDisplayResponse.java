/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.GroupList;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.PldHousehold;
import com.getinsured.hix.model.PldOrder;

/**
 * @author Mishra_m
 *
 */
public class PlanDisplayResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private PldHousehold pldHousehold;
	private PldOrder pldOrder;
	/*private PldPerson pldPerson;
	private PldGroup pldGroup;
	private List<PldGroup> pldGroupList;
	private PldGroupPerson pldGroupPerson;
	private List<PldGroupPerson> pldGroupPersonList;
	private PldOrderItem pldOrderItem;
	private PldHouseholdPerson pldHouseholdPerson;*/
	private List<IndividualPlan> individualPlanList;
	
	private Map<String, Object> outputData;
	
	private List<GroupList> groupListArr;
	private String response;
	//private List<PldHouseholdPerson> pldHouseholdPersonList;
	private Map householdData;
	private String sadpFlag;
	private List<Integer> planIds;
	private List<Map<String, String>> personDataMapList;
	private List<GroupData> groupDataList;
	private List<ProviderBean> providersList;
	private List<PldOrderResponse> pldOrderResponse;
	private java.lang.String nestedStackTrace;
	private String marketType;

	public Map getHouseholdData() {
		return householdData;
	}

	public void setHouseholdData(Map householdData) {
		this.householdData = householdData;
	}

	public PldHousehold getPldHousehold() {
		return pldHousehold;
	}

	public void setPldHousehold(PldHousehold pldHousehold) {
		this.pldHousehold = pldHousehold;
	}

	public PldOrder getPldOrder() {
		return pldOrder;
	}

	public void setPldOrder(PldOrder pldOrder) {
		this.pldOrder = pldOrder;
	}
	
	/*public PldPerson getPldPerson() {
		return pldPerson;
	}

	public void setPldPerson(PldPerson pldPerson) {
		this.pldPerson = pldPerson;
	}

	public PldGroup getPldGroup() {
		return pldGroup;
	}

	public void setPldGroup(PldGroup pldGroup) {
		this.pldGroup = pldGroup;
	}

	public List<PldGroup> getPldGroupList() {
		return pldGroupList;
	}

	public void setPldGroupList(List<PldGroup> pldGroupList) {
		this.pldGroupList = pldGroupList;
	}

	public PldOrder getPldOrder() {
		return pldOrder;
	}

	public void setPldOrder(PldOrder pldOrder) {
		this.pldOrder = pldOrder;
	}

	public PldGroupPerson getPldGroupPerson() {
		return pldGroupPerson;
	}

	public void setPldGroupPerson(PldGroupPerson pldGroupPerson) {
		this.pldGroupPerson = pldGroupPerson;
	}

	public List<PldGroupPerson> getPldGroupPersonList() {
		return pldGroupPersonList;
	}

	public void setPldGroupPersonList(List<PldGroupPerson> pldGroupPersonList) {
		this.pldGroupPersonList = pldGroupPersonList;
	}

	public PldOrderItem getPldOrderItem() {
		return pldOrderItem;
	}

	public void setPldOrderItem(PldOrderItem pldOrderItem) {
		this.pldOrderItem = pldOrderItem;
	}*/

	public List<IndividualPlan> getIndividualPlanList() {
		return individualPlanList;
	}

	public void setIndividualPlanList(List<IndividualPlan> individualPlanList) {
		this.individualPlanList = individualPlanList;
	}

	public Map<String, Object> getOutputData() {
		return outputData;
	}

	public void setOutputData(Map<String, Object> outputData) {
		this.outputData = outputData;
	}

	public List<GroupList> getGroupListArr() {
		return groupListArr;
	}

	public void setGroupListArr(List<GroupList> groupListArr) {
		this.groupListArr = groupListArr;
	}

	/*public PldHouseholdPerson getPldHouseholdPerson() {
		return pldHouseholdPerson;
	}

	public void setPldHouseholdPerson(PldHouseholdPerson pldHouseholdPerson) {
		this.pldHouseholdPerson = pldHouseholdPerson;
	}*/

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	/*public List<PldHouseholdPerson> getPldHouseholdPersonList() {
		return pldHouseholdPersonList;
	}

	public void setPldHouseholdPersonList(
			List<PldHouseholdPerson> pldHouseholdPersonList) {
		this.pldHouseholdPersonList = pldHouseholdPersonList;
	}*/

	public String getSadpFlag() {
		return sadpFlag;
	}

	public void setSadpFlag(String sadpFlag) {
		this.sadpFlag = sadpFlag;
	}

	public List<Integer> getPlanIds() {
		return planIds;
	}

	public void setPlanIds(List<Integer> planIds) {
		this.planIds = planIds;
	}

	public List<Map<String, String>> getPersonDataMapList() {
		return personDataMapList;
	}

	public void setPersonDataMapList(List<Map<String, String>> personDataMapList) {
		this.personDataMapList = personDataMapList;
	}

	public List<GroupData> getGroupDataList() {
		return groupDataList;
	}

	public void setGroupDataList(List<GroupData> groupDataList) {
		this.groupDataList = groupDataList;
	}

	public List<ProviderBean> getProvidersList() {
		return providersList;
	}

	public void setProvidersList(List<ProviderBean> providersList) {
		this.providersList = providersList;
	}

	public List<PldOrderResponse> getPldOrderResponse() {
		return pldOrderResponse;
	}

	public void setPldOrderResponse(List<PldOrderResponse> pldOrderResponse) {
		this.pldOrderResponse = pldOrderResponse;
	}
	
	public java.lang.String getNestedStackTrace() {
		return nestedStackTrace;
	}

	public void setNestedStackTrace(java.lang.String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}	
	
	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
}
