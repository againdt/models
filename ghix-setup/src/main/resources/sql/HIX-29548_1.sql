insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.isActive','Y','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordHistoryLimit','6','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordMaxAge','60','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordMinAge','15','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordComplexity.isActive','Y','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordComplexity.Regex','((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordHistoryLimitActive','Y','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordMaxAgePolicyActive','Y','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'security.PasswordPolicy.PasswordMinAgePolicyActive','Y','',null,current_timestamp,null,current_timestamp);














