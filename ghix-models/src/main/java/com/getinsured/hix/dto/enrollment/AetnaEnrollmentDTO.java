package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AetnaEnrollmentDTO implements Serializable{

	private String enrollmentFormId;
	private String requestedEffectiveDate;
	private String applicationId;
	private String applicationStatus;
	private String creditCardAccountNumber;
	private String cardType;
	private String electronicFundAccountNumber;
	private String routingNumber;
	
	private List<AetnaEnrollment> aetnaEnrollmentList = new ArrayList<AetnaEnrollment>();

	public String getEnrollmentFormId() {
		return enrollmentFormId;
	}
	public void setEnrollmentFormId(String enrollmentFormId) {
		this.enrollmentFormId = enrollmentFormId;
	}
	
	public String getRequestedEffectiveDate() {
		return requestedEffectiveDate;
	}
	public void setRequestedEffectiveDate(String requestedEffectiveDate) {
		this.requestedEffectiveDate = requestedEffectiveDate;
	}
	
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getCreditCardAccountNumber() {
		return creditCardAccountNumber;
	}
	public void setCreditCardAccountNumber(String creditCardAccountNumber) {
		this.creditCardAccountNumber = creditCardAccountNumber;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getElectronicFundAccountNumber() {
		return electronicFundAccountNumber;
	}
	public void setElectronicFundAccountNumber(String electronicFundAccountNumber) {
		this.electronicFundAccountNumber = electronicFundAccountNumber;
	}
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public List<AetnaEnrollment> getAetnaEnrollmentList() {
		return aetnaEnrollmentList;
	}

	public void setAetnaEnrollmentList(List<AetnaEnrollment> aetnaEnrollmentList) {
		this.aetnaEnrollmentList = aetnaEnrollmentList;
	}

	
}
