-- SQL Script to update USER_TYPE for all templates wth ADMIN type
update NOTICE_TYPES
set USER_TYPE = 'BROKER'
where EMAIL_CLASS='PasswordRecoveryEmail';

update NOTICE_TYPES
set USER_TYPE = 'BROKER'
where EMAIL_CLASS='AccountActivationEmail';

update NOTICE_TYPES
set USER_TYPE = 'BROKER'
where EMAIL_CLASS='TestNoticeTemplate';
