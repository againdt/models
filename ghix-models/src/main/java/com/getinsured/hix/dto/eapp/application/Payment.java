package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

import com.getinsured.hix.dto.directenrollment.BankDTO;
import com.getinsured.hix.dto.directenrollment.PaymentVerificationDTO;

public class Payment implements Serializable{
	private static final long serialVersionUID = 1L;
	private String initialPaymentMode;
	private String recurringPaymentMode;
	private CreditCard creditCard;
	private BankDetails bankDetails;
	private String token;
    private String recurringToken;
	
	public String getInitialPaymentMode() {
		return initialPaymentMode;
	}
	public void setInitialPaymentMode(String initialPaymentMode) {
		this.initialPaymentMode = initialPaymentMode;
	}
	public String getRecurringPaymentMode() {
		return recurringPaymentMode;
	}
	public void setRecurringPaymentMode(String recurringPaymentMode) {
		this.recurringPaymentMode = recurringPaymentMode;
	}
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	public BankDetails getBankDetails() {
		return bankDetails;
	}
	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
    public String getRecurringToken() {
        return recurringToken;
    }
    public void setRecurringToken(String recurringToken) {
        this.recurringToken = recurringToken;
    }

	public static Payment getSampleInstance(){
		Payment payment = new Payment();
		payment.setBankDetails(BankDetails.getSampleInstance());
		payment.setCreditCard(CreditCard.getSampleInstance());
		payment.setInitialPaymentMode("creditcard");
		payment.setRecurringPaymentMode("bank");
		return payment;
	}
	
	/**
	 * this will be passed to payment validation service
	 * @return
	 */
	public PaymentVerificationDTO createValidationObject(){
		PaymentVerificationDTO dto = new PaymentVerificationDTO();
		if(this.bankDetails!=null){
			BankDTO bankDetails = new BankDTO();
			bankDetails.setAccount(this.bankDetails.getAccountNumber());
			bankDetails.setRouting(this.bankDetails.getRoutingNumber());
			bankDetails.setType(this.bankDetails.getAccountType());
			dto.setBank(bankDetails);
		}
		
		if(this.creditCard!=null){
			com.getinsured.hix.dto.directenrollment.CreditCardDTO creditCardDTO = new com.getinsured.hix.dto.directenrollment.CreditCardDTO();
			creditCardDTO.setNumber(this.creditCard.getNumber());
			creditCardDTO.setType(this.creditCard.getType());
			creditCardDTO.setZip(this.creditCard.getZip());
			creditCardDTO.setCvv(this.creditCard.getCvv());
			creditCardDTO.setExpmonth(this.creditCard.getExpirationMonth());
			creditCardDTO.setExpyear(this.creditCard.getExpirationYear()); 
			dto.setCreditcard(creditCardDTO);
		}
		
		return dto;
	}

}
