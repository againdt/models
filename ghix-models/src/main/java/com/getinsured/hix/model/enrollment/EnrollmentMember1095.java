package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="ENROLLMENT_MEMBER_1095")
public class EnrollmentMember1095 implements Serializable{
	
private static final long serialVersionUID = 1L;

	public enum MemberType{
		RECEPIENT,SPOUSE,MEMBER;
	}
	public enum YorNFlagIndicator{
		Y ,N;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_MEMBER_1095_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_MEMBER_1095_SEQ", sequenceName = "ENROLLMENT_MEMBER_1095_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="ENROLLMENT_1095_ID")
	private Enrollment1095 enrollment1095;
	
	@Column(name = "MEMBER_ID")
	private Integer memberId;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "MIDDLE_NAME")
	private String middleName;
	
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name = "NAME_SUFFIX")
	private String nameSuffix;
	
	@Column(name = "SSN")
	private String ssn;
	
	@Column(name = "BIRTH_DATE")
	private Date birthDate;
	
	@Column(name = "COVERAGE_START_DATE")
	private Date coverageStartDate;
	
	@Column(name = "COVERAGE_END_DATE")
	private Date coverageEndDate;
	
	@Column(name = "MEMBER_TYPE")
	//@Enumerated(EnumType.STRING)
	private String memberType;
	
	@Column(name = "ADDRESS1")
	private String address1;
	
	@Column(name = "ADDRESS2")
	private String address2;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "STATE")
	private String state;
	
	@Column(name = "ZIP")
	private String zip;
	
	@Column(name = "COUNTY")
	private String county;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	//@Audited
	//@OneToOne
	//@JoinColumn(name="CREATED_BY")
	@Column(name="CREATED_BY")
	private Integer createdBy;

	/*@Audited
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")*/
	@Column(name="LAST_UPDATED_BY")
	private Integer updatedBy;

	@Column(name = "UPDATE_NOTES")
	private String updateNotes;
	
	@Column(name = "IS_ACTIVE")
	//@Enumerated(EnumType.STRING)
	private String isActive = "Y";
	
	@Column(name = "ENROLLEE_ID")
	private Integer enrolleeId;

	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the enrollment1095
	 */
	public Enrollment1095 getEnrollment1095() {
		return enrollment1095;
	}

	/**
	 * @param enrollment1095 the enrollment1095 to set
	 */
	public void setEnrollment1095(Enrollment1095 enrollment1095) {
		this.enrollment1095 = enrollment1095;
	}

	/**
	 * @return the memberId
	 */
	public Integer getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the nameSuffix
	 */
	public String getNameSuffix() {
		return nameSuffix;
	}

	/**
	 * @param nameSuffix the nameSuffix to set
	 */
	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}

	/**
	 * @return the ssn
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * @param ssn the ssn to set
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the coverageStartDate
	 */
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	/**
	 * @param coverageStartDate the coverageStartDate to set
	 */
	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	/**
	 * @return the coverageEndDate
	 */
	public Date getCoverageEndDate() {
		return coverageEndDate;
	}

	/**
	 * @param coverageEndDate the coverageEndDate to set
	 */
	public void setCoverageEndDate(Date coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	/**
	 * @return the memberType
	 */
	public String getMemberType() {
		return memberType;
	}

	/**
	 * @param memberType the memberType to set
	 */
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state != null ? state.toUpperCase().trim() : null;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state != null ? state.toUpperCase().trim() : null;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * @param county the county to set
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updateNotes
	 */
	public String getUpdateNotes() {
		return updateNotes;
	}

	/**
	 * @param updateNotes the updateNotes to set
	 */
	public void setUpdateNotes(String updateNotes) {
		this.updateNotes = updateNotes;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the enrolleeId
	 */
	public Integer getEnrolleeId() {
		return enrolleeId;
	}

	/**
	 * @param enrolleeId the enrolleeId to set
	 */
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((coverageEndDate == null) ? 0 : coverageEndDate.hashCode());
		result = prime * result + ((coverageStartDate == null) ? 0 : coverageStartDate.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((memberId == null) ? 0 : memberId.hashCode());
		result = prime * result + ((memberType == null) ? 0 : memberType.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((nameSuffix == null) ? 0 : nameSuffix.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EnrollmentMember1095 other = (EnrollmentMember1095) obj;
		if (birthDate == null) {
			if (other.birthDate != null) {
				return false;
			}
		} else if (!(birthDate.compareTo(other.birthDate) == 0)) {
			return false;
		}
		if (coverageEndDate == null && other.coverageEndDate != null) {
			return false;
		} else if (coverageEndDate != null && other.coverageEndDate == null) {
			return false;
		} else if (coverageEndDate != null && other.coverageEndDate != null && !(coverageEndDate.compareTo(other.coverageEndDate) == 0)) {
			return false;
		}
		if (coverageStartDate == null && other.coverageStartDate != null) {
			return false;
		} else if (coverageStartDate != null && other.coverageStartDate == null) {
			return false;
		} else if (coverageStartDate != null && other.coverageStartDate != null && !(coverageStartDate.compareTo(other.coverageStartDate) == 0)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null && !other.firstName.trim().isEmpty()) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null && !other.lastName.trim().isEmpty()) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (memberId == null) {
			if (other.memberId != null) {
				return false;
			}
		} else if (!memberId.equals(other.memberId)) {
			return false;
		}
		if (memberType == null) {
			if (other.memberType != null && !other.memberType.trim().isEmpty()) {
				return false;
			}
		} else if (!memberType.equals(other.memberType)) {
			return false;
		}
		if (middleName == null) {
			if (other.middleName != null && !other.middleName.trim().isEmpty()) {
				return false;
			}
		} else if (!middleName.equals(other.middleName)) {
			return false;
		}
		if (nameSuffix == null) {
			if (other.nameSuffix != null && !other.nameSuffix.trim().isEmpty()) {
				return false;
			}
		} else if (!nameSuffix.equals(other.nameSuffix)) {
			return false;
		}
		if (ssn == null) {
			if (other.ssn != null && !other.ssn.trim().isEmpty()) {
				return false;
			}
		} else if (!ssn.equals(other.ssn)) {
			return false;
		}
		return true;
	}

}
