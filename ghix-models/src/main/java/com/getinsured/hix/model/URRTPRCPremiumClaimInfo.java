package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "pm_urrt_prc_prm_clm_info")
@XmlAccessorType(XmlAccessType.NONE)
public class URRTPRCPremiumClaimInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_urrt_prc_prm_clm_info_seq")
	@SequenceGenerator(name = "pm_urrt_prc_prm_clm_info_seq", sequenceName = "pm_urrt_prc_prm_clm_info_seq", allocationSize = 1)
	private int id;

	// foreign key.
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pm_urrt_plan_rate_change_id")
	private URRTInsurancePlanRateChange unifiedRatePlanRateChange;

	@Column(name = "pi_ar_pmpm_qty")
	private double averageRatePMPMQuantity;

	@Column(name = "pi_mbr_mths_qty")
	private int memberMonthsQuantity;

	@Column(name = "pi_tot_prm_amt")
	private double totalPremiumAmount;

	@Column(name = "pi_fpoebo_tp_qty")
	private double fullPortionOrEhbBasisOfTPQuantity;
	//smbp : state mandate benefits portion.
	@Column(name = "pi_smbptaot_ehb_qty")
	private double smbpThatAreOtherThanEHBQuantity;

	@Column(name = "pi_obpo_tp_qty")
	private double otherBenefitsPortionOfTPQuantity;

	@Column(name = "ci_ta_clm_amt")
	private double totalAllowedClaimsAmount;

	@Column(name = "ci_fpoebo_tac_qty")
	private double fullPortionOrEhbBasisOfTACQuantity;
    //smbp : state mandate benefits portion.
	@Column(name = "ci_smbpo_tac_taot_ehb_qty")
	private double smbpOfTACThatAreOtherThanEHBQuantity;

	@Column(name = "ci_obpo_tac_qty")
	private double otherBenefitsPortionOfTACQuantity;

	@Column(name = "ci_acnio_amt")
	private double allowedClaimsNotIssuersObligationAmount;
	//poac : portion of allowed claims.
	@Column(name = "ci_poacpb_hhs_foboip_amt")
	private double poacPayableByHHSFundsOnBehalfOfInsuredPersonAmount;
	//poac : portion of allowed claims.
	@Column(name = "ci_poacpb_hhs_oboip_qty")
	private double poacPayableByHHSOnBehalfOfInsuredPersonQuantity;

	@Column(name = "ci_ticpwif_amt")
	private double ticAmount;

	@Column(name = "ci_net_rein_amt")
	private double netReinAmount;

	@Column(name = "ci_net_risk_adj_amt")
	private double netRiskAdjustmentAmount;

	@Column(name = "ic_pmpm_qty")
	private double incurredClaimsPMPMQuantity;

	@Column(name = "ac_pmpm_qty")
	private double allowedClaimsPMPMQuantity;
   
	//poac : portion of allowed claims.
	@Column(name = "ehb_poac_pmpm_qty")
	private double ehbpoacPMPMQuantity;

	@Column(name = "dircp_type")
	private String definingInsuranceRateCostPeriodType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public URRTInsurancePlanRateChange getUnifiedRatePlanRateChange() {
		return unifiedRatePlanRateChange;
	}

	public void setUnifiedRatePlanRateChange(
			URRTInsurancePlanRateChange unifiedRatePlanRateChange) {
		this.unifiedRatePlanRateChange = unifiedRatePlanRateChange;
	}

	public double getAverageRatePMPMQuantity() {
		return averageRatePMPMQuantity;
	}

	public void setAverageRatePMPMQuantity(double averageRatePMPMQuantity) {
		this.averageRatePMPMQuantity = averageRatePMPMQuantity;
	}

	public int getMemberMonthsQuantity() {
		return memberMonthsQuantity;
	}

	public void setMemberMonthsQuantity(int memberMonthsQuantity) {
		this.memberMonthsQuantity = memberMonthsQuantity;
	}

	public double getTotalPremiumAmount() {
		return totalPremiumAmount;
	}

	public void setTotalPremiumAmount(double totalPremiumAmount) {
		this.totalPremiumAmount = totalPremiumAmount;
	}

	public double getFullPortionOrEhbBasisOfTPQuantity() {
		return fullPortionOrEhbBasisOfTPQuantity;
	}

	public void setFullPortionOrEhbBasisOfTPQuantity(
			double fullPortionOrEhbBasisOfTPQuantity) {
		this.fullPortionOrEhbBasisOfTPQuantity = fullPortionOrEhbBasisOfTPQuantity;
	}

	public double getOtherBenefitsPortionOfTPQuantity() {
		return otherBenefitsPortionOfTPQuantity;
	}

	public void setOtherBenefitsPortionOfTPQuantity(
			double otherBenefitsPortionOfTPQuantity) {
		this.otherBenefitsPortionOfTPQuantity = otherBenefitsPortionOfTPQuantity;
	}

	public double getTotalAllowedClaimsAmount() {
		return totalAllowedClaimsAmount;
	}

	public void setTotalAllowedClaimsAmount(double totalAllowedClaimsAmount) {
		this.totalAllowedClaimsAmount = totalAllowedClaimsAmount;
	}

	public double getFullPortionOrEhbBasisOfTACQuantity() {
		return fullPortionOrEhbBasisOfTACQuantity;
	}

	public void setFullPortionOrEhbBasisOfTACQuantity(
			double fullPortionOrEhbBasisOfTACQuantity) {
		this.fullPortionOrEhbBasisOfTACQuantity = fullPortionOrEhbBasisOfTACQuantity;
	}

	public double getOtherBenefitsPortionOfTACQuantity() {
		return otherBenefitsPortionOfTACQuantity;
	}

	public void setOtherBenefitsPortionOfTACQuantity(
			double otherBenefitsPortionOfTACQuantity) {
		this.otherBenefitsPortionOfTACQuantity = otherBenefitsPortionOfTACQuantity;
	}

	public double getAllowedClaimsNotIssuersObligationAmount() {
		return allowedClaimsNotIssuersObligationAmount;
	}

	public void setAllowedClaimsNotIssuersObligationAmount(
			double allowedClaimsNotIssuersObligationAmount) {
		this.allowedClaimsNotIssuersObligationAmount = allowedClaimsNotIssuersObligationAmount;
	}
	
	public double getPoacPayableByHHSFundsOnBehalfOfInsuredPersonAmount() {
		return poacPayableByHHSFundsOnBehalfOfInsuredPersonAmount;
	}

	public void setPoacPayableByHHSFundsOnBehalfOfInsuredPersonAmount(
			double poacPayableByHHSFundsOnBehalfOfInsuredPersonAmount) {
		this.poacPayableByHHSFundsOnBehalfOfInsuredPersonAmount = poacPayableByHHSFundsOnBehalfOfInsuredPersonAmount;
	}

	public double getPoacPayableByHHSOnBehalfOfInsuredPersonQuantity() {
		return poacPayableByHHSOnBehalfOfInsuredPersonQuantity;
	}

	public void setPoacPayableByHHSOnBehalfOfInsuredPersonQuantity(
			double poacPayableByHHSOnBehalfOfInsuredPersonQuantity) {
		this.poacPayableByHHSOnBehalfOfInsuredPersonQuantity = poacPayableByHHSOnBehalfOfInsuredPersonQuantity;
	}

	public double getEhbpoacPMPMQuantity() {
		return ehbpoacPMPMQuantity;
	}

	public void setEhbpoacPMPMQuantity(double ehbpoacPMPMQuantity) {
		this.ehbpoacPMPMQuantity = ehbpoacPMPMQuantity;
	}

	public double getTicAmount() {
		return ticAmount;
	}

	public void setTicAmount(double ticAmount) {
		this.ticAmount = ticAmount;
	}

	public double getNetReinAmount() {
		return netReinAmount;
	}

	public void setNetReinAmount(double netReinAmount) {
		this.netReinAmount = netReinAmount;
	}

	public double getNetRiskAdjustmentAmount() {
		return netRiskAdjustmentAmount;
	}

	public void setNetRiskAdjustmentAmount(double netRiskAdjustmentAmount) {
		this.netRiskAdjustmentAmount = netRiskAdjustmentAmount;
	}

	public double getIncurredClaimsPMPMQuantity() {
		return incurredClaimsPMPMQuantity;
	}

	public void setIncurredClaimsPMPMQuantity(double incurredClaimsPMPMQuantity) {
		this.incurredClaimsPMPMQuantity = incurredClaimsPMPMQuantity;
	}

	public double getAllowedClaimsPMPMQuantity() {
		return allowedClaimsPMPMQuantity;
	}

	public void setAllowedClaimsPMPMQuantity(double allowedClaimsPMPMQuantity) {
		this.allowedClaimsPMPMQuantity = allowedClaimsPMPMQuantity;
	}
   
	public String getDefiningInsuranceRateCostPeriodType() {
		return definingInsuranceRateCostPeriodType;
	}

	public void setDefiningInsuranceRateCostPeriodType(
			String definingInsuranceRateCostPeriodType) {
		this.definingInsuranceRateCostPeriodType = definingInsuranceRateCostPeriodType;
	}

	public double getSmbpThatAreOtherThanEHBQuantity() {
		return smbpThatAreOtherThanEHBQuantity;
	}

	public void setSmbpThatAreOtherThanEHBQuantity(
			double smbpThatAreOtherThanEHBQuantity) {
		this.smbpThatAreOtherThanEHBQuantity = smbpThatAreOtherThanEHBQuantity;
	}

	public double getSmbpOfTACThatAreOtherThanEHBQuantity() {
		return smbpOfTACThatAreOtherThanEHBQuantity;
	}

	public void setSmbpOfTACThatAreOtherThanEHBQuantity(
			double smbpOfTACThatAreOtherThanEHBQuantity) {
		this.smbpOfTACThatAreOtherThanEHBQuantity = smbpOfTACThatAreOtherThanEHBQuantity;
	}

}
