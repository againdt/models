package com.getinsured.hix.dto.agency;

import java.util.List;

public class AgencyCertificationHistory {
	String id;
	String certificationDate;
	String previousStatus;
	String newStatus;
	String comment;
	String updatedBy;
	List<AgencyDocumentDto> documents;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCertificationDate() {
		return certificationDate;
	}
	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}
	public String getPreviousStatus() {
		return previousStatus;
	}
	public void setPreviousStatus(String previousStatus) {
		this.previousStatus = previousStatus;
	}
	public String getNewStatus() {
		return newStatus;
	}
	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public List<AgencyDocumentDto> getDocuments() {
		return documents;
	}
	public void setDocuments(List<AgencyDocumentDto> documents) {
		this.documents = documents;
	}

}
