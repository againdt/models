package com.getinsured.hix.model.serff;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * Model class for SERFF_PLAN_MGMT table
 * @author Nikhil Talreja
 * @since 05 March, 2013
 */
@Entity
@Table(name="SERFF_PLAN_MGMT")
@DynamicInsert
@DynamicUpdate
public class SerffPlanMgmt implements Serializable {

	private static final long serialVersionUID = 9147340232755106762L;
	private static final int MAX_LEN_FOR_DESC = 1000;

//	S-SERFF, T-SOAP UI / JMeter, U-UI, O-Others
	public enum CALLER_TYPE {
		S, T, U, O;
	}
	
//	S-Success, F-Failure P- In Progress
	public enum REQUEST_STATUS {
		S, F, P;
	}
	
//	B-Begin, E-End, P-Progress, W-Wait
	public enum REQUEST_STATE {
		B, E, P, W;
	}
	
//	PROD, DEV, QA, PILOT,...etc
	public enum ENV {
		PROD, DEV, QA, PILOT;
	}
	
	@Id
	@Column(name = "SERFF_REQ_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERFF_PLAN_MGMT_SEQ")
	@SequenceGenerator(name = "SERFF_PLAN_MGMT_SEQ", sequenceName = "SERFF_PLAN_MGMT_SEQ", allocationSize = 1)
	private long serffReqId;
	
	@Column(name="CALLER_IP")
	private String callerIp;
	
	@Column(name="CALLER_TYPE")
	@Enumerated(EnumType.STRING)
	private CALLER_TYPE callerType;
	
	@Column(name="SERFF_OPERATION")
	private String operation;
	
	@Column(name="PROCESS_IP")
	private String processIp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="START_TIME", updatable=false)
	private Date startTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Date endTime;
	
	@Column(name="REQUEST_XML")
	private String requestXml;
	
	@Column(name="RESPONSE_XML")
	private String responseXml;
	
	@Column(name="PM_RESPONSE_XML")
	private String pmResponseXml;
	
	@Column(name="REQUEST_STATUS")
	@Enumerated(EnumType.STRING)
	private REQUEST_STATUS requestStatus;
	
	@Column(name="REMARKS")
	private String remarks;
	
	@Column(name="ATTACHMENTS_LIST")
	private String attachmentsList;
	
	@Column(name="REQUEST_STATE")
	@Enumerated(EnumType.STRING)
	private REQUEST_STATE requestState;
	
	@Column(name="REQUEST_STATE_DESC")
	private String requestStateDesc;
	
	@Column(name="ENV")
	@Enumerated(EnumType.STRING)
	private ENV env;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP", updatable=false)
	private Date createdOn;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Column(name="ISSUER_ID")
	private String issuerId;
	
	@Column(name="ISSUER_NAME")
	private String issuerName;
	
	@Column(name="HIOS_PRODUCT_ID")
	private String hiosProductId;
	
	@Column(name="PLAN_ID")
	private String planId;
	
	@Column(name="PLAN_NAME")
	private String planName;
	
	@Column(name="SERFF_TRACK_NUM")
	private String serffTrackNum;
	
	@Column(name="STATE_TRACK_NUM")
	private String stateTrackNum;
	
	@Column(name="PLAN_STATS")
	private String planStats;
	
	//One to many association with serff documents
	//To test using JUnit, make fetch type as EAGER
	@OneToMany(mappedBy = "serffReqId", cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	private Set<SerffDocument> serffDocuments;
	
	public long getSerffReqId() {
		return serffReqId;
	}

	public void setSerffReqId(long serffReqId) {
		this.serffReqId = serffReqId;
	}

	public String getCallerIp() {
		return callerIp;
	}

	public void setCallerIp(String callerIp) {
		this.callerIp = callerIp;
	}

	public CALLER_TYPE getCallerType() {
		return callerType;
	}

	public void setCallerType(CALLER_TYPE callerType) {
		this.callerType = callerType;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getProcessIp() {
		return processIp;
	}

	public void setProcessIp(String processIp) {
		this.processIp = processIp;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getRequestXml() {
		return requestXml;
	}

	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}
	
	public void setPmResponseXml(String pmResponseXml) {
		this.pmResponseXml = pmResponseXml;
	}

	public String getPmResponseXml() {
		return pmResponseXml;
	}

	public REQUEST_STATUS getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(REQUEST_STATUS requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAttachmentsList() {
		return attachmentsList;
	}

	public void setAttachmentsList(String attachmentsList) {
		this.attachmentsList = attachmentsList;
	}

	public REQUEST_STATE getRequestState() {
		return requestState;
	}

	public void setRequestState(REQUEST_STATE requestState) {
		this.requestState = requestState;
	}

	public String getRequestStateDesc() {
		return requestStateDesc;
	}

	public void setRequestStateDesc(String requestStateDesc) {

		if (null != requestStateDesc && MAX_LEN_FOR_DESC < requestStateDesc.length()) {
			this.requestStateDesc = requestStateDesc.substring(0, MAX_LEN_FOR_DESC);
		}
		else {
			this.requestStateDesc = requestStateDesc;
		}
	}

	public ENV getEnv() {
		return env;
	}

	public void setEnv(ENV env) {
		this.env = env;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosProductId() {
		return hiosProductId;
	}

	public void setHiosProductId(String hiosProductId) {
		this.hiosProductId = hiosProductId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getSerffTrackNum() {
		return serffTrackNum;
	}

	public void setSerffTrackNum(String serffTrackNum) {
		this.serffTrackNum = serffTrackNum;
	}

	public String getStateTrackNum() {
		return stateTrackNum;
	}

	public void setStateTrackNum(String stateTrackNum) {
		this.stateTrackNum = stateTrackNum;
	}
	
	public Set<SerffDocument> getSerffDocuments() {
		return serffDocuments;
	}

	public void setSerffDocuments(Set<SerffDocument> serffDocuments) {
		this.serffDocuments = serffDocuments;
	}
	
	public String getPlanStats() {
		return planStats;
	}
	
	public void setPlanStats(String planStats) {
		this.planStats = planStats;
	}

	@javax.persistence.PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}
	
}
