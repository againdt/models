
package com.serff.planmanagementserffapi.serff.model.service;

import com.serff.planmanagementserffapi.common.model.pm.ValidationErrors;


public class TransferPlanReply {

    protected String requestId;
    protected String exchangeWorkflowStatus;
    protected ValidationErrors validationErrors;

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the exchangeWorkflowStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeWorkflowStatus() {
        return exchangeWorkflowStatus;
    }

    /**
     * Sets the value of the exchangeWorkflowStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeWorkflowStatus(String value) {
        this.exchangeWorkflowStatus = value;
    }

    /**
     * Gets the value of the validationErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationErrors }
     *     
     */
    public ValidationErrors getValidationErrors() {
        return validationErrors;
    }

    /**
     * Sets the value of the validationErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationErrors }
     *     
     */
    public void setValidationErrors(ValidationErrors value) {
        this.validationErrors = value;
    }

}
