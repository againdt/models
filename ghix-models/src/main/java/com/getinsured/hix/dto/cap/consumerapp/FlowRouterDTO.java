/**
 * Created: Oct 22, 2014
 */
package com.getinsured.hix.dto.cap.consumerapp;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
 * 
 */
public class FlowRouterDTO extends GHIXResponse {

	private String buttonText;
	private boolean showButton;
	private String nextActionUrl;
	private List<String> nextStepsList = new ArrayList<String>();
	private String reasonToCall;
	private String greetingMessage;
	private String viewConsumerAccountUrl;
	private Long ssapApplicationId;
	private String applicationStatus;
	private String flowConfig;
	private boolean newTabRequired;
	private String carrierSiteUrl;
	private String phixFlowForState;
	private boolean d2cApplication;
	private String redirectUrl;

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public boolean isShowButton() {
		return showButton;
	}

	public void setShowButton(boolean showButton) {
		this.showButton = showButton;
	}

	public String getNextActionUrl() {
		return nextActionUrl;
	}

	public void setNextActionUrl(String nextActionUrl) {
		this.nextActionUrl = nextActionUrl;
	}

	public List<String> getNextStepsList() {
		return nextStepsList;
	}

	public void setNextStepsList(List<String> nextStepsList) {
		this.nextStepsList = nextStepsList;
	}

	public void addNextSteps(String nextStep) {
		if (this.nextStepsList != null) {
			this.nextStepsList.add(nextStep);
		} else {
			this.nextStepsList = new ArrayList<String>();
			this.nextStepsList.add(nextStep);
		}

	}

	public String getReasonToCall() {
		return reasonToCall;
	}

	public void setReasonToCall(String reasonToCall) {
		this.reasonToCall = reasonToCall;
	}

	public String getGreetingMessage() {
		return greetingMessage;
	}

	public void setGreetingMessage(String greetingMessage) {
		this.greetingMessage = greetingMessage;
	}

	public String getViewConsumerAccountUrl() {
		return viewConsumerAccountUrl;
	}

	public void setViewConsumerAccountUrl(String viewConsumerAccountUrl) {
		this.viewConsumerAccountUrl = viewConsumerAccountUrl;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getFlowConfig() {
		return flowConfig;
	}

	public void setFlowConfig(String flowConfig) {
		this.flowConfig = flowConfig;
	}

	public boolean isNewTabRequired() {
		return newTabRequired;
	}

	public void setNewTabRequired(boolean newTabRequired) {
		this.newTabRequired = newTabRequired;
	}

	public String getCarrierSiteUrl() {
		return carrierSiteUrl;
	}

	public void setCarrierSiteUrl(String carrierSiteUrl) {
		this.carrierSiteUrl = carrierSiteUrl;
	}
	
	public String getPhixFlowForState() {
		return phixFlowForState;
	}
	
	public void setPhixFlowForState(String phixFlowForState) {
		this.phixFlowForState = phixFlowForState;
	}

	public boolean isD2cApplication() {
		return d2cApplication;
	}

	public void setD2cApplication(boolean d2cApplication) {
		this.d2cApplication = d2cApplication;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

}
