package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

/**
 * 
 * @author Samir Gundawar
 *
 */

@Entity
@Table(name = "COMMISSION_BACKLOG_STATUS")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class CommissionBackLogStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMISSION_BACKLOG_STATUS_SEQ")
	@SequenceGenerator(name = "COMMISSION_BACKLOG_STATUS_SEQ", sequenceName = "COMMISSION_BACKLOG_STATUS_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "SEARCH_FILTER")
	private String searchFilter;

	@Column(name = "TOTAL_RECORDS")
	private Integer totalRecords;
	
	@Column(name = "SUCCESS_RECORDS")
	private Integer successRecords;
	
	@Column(name = "FAIL_RECORDS")
	private Integer failRecords;

	@Column(name = "TOTAL_COMMISSION_AMT")
	private Float totalCommissionAmt;
	
	@Column(name = "SUCCESS_COMMISSION_AMT")
	private Float successCommissionAmt;
	
	@Column(name = "FAIL_COMMISSION_AMT")
	private Float failCommissionAmt;
	
	@Column(name = "STATUS")
	private String status;

	@Column(name = "REASON")
	private String reason;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	public CommissionBackLogStatus() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}

	public Integer getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Integer getSuccessRecords() {
		return successRecords;
	}

	public void setSuccessRecords(Integer successRecords) {
		this.successRecords = successRecords;
	}

	public Integer getFailRecords() {
		return failRecords;
	}

	public void setFailRecords(Integer failRecords) {
		this.failRecords = failRecords;
	}

	public Float getTotalCommissionAmt() {
		return totalCommissionAmt;
	}

	public void setTotalCommissionAmt(Float totalCommissionAmt) {
		this.totalCommissionAmt = totalCommissionAmt;
	}

	public Float getSuccessCommissionAmt() {
		return successCommissionAmt;
	}

	public void setSuccessCommissionAmt(Float successCommissionAmt) {
		this.successCommissionAmt = successCommissionAmt;
	}

	public Float getFailCommissionAmt() {
		return failCommissionAmt;
	}

	public void setFailCommissionAmt(Float failCommissionAmt) {
		this.failCommissionAmt = failCommissionAmt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	public Long getTenantId() {
			return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
}
