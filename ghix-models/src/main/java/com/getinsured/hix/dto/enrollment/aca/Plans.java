package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class Plans{
  @JsonProperty("amount")
  
  private Integer amount;
  @JsonProperty("refId")
  
  private String refId;
  public void setAmount(Integer amount){
   this.amount=amount;
  }
  public Integer getAmount(){
   return amount;
  }
  public void setRefId(String refId){
   this.refId=refId;
  }
  public String getRefId(){
   return refId;
  }
}