package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerEnrollmentFlowRequestDTO extends EnrollmentFlowDTO {

	private List<String> hiosIssuerIdList;

	public IssuerEnrollmentFlowRequestDTO() {
		super();
	}

	public List<String> getHiosIssuerIdList() {
		return hiosIssuerIdList;
	}

	public void setHiosIssuerId(String hiosIssuerId) {

		if (CollectionUtils.isEmpty(this.hiosIssuerIdList)) {
			this.hiosIssuerIdList = new ArrayList<String>();
		}
		this.hiosIssuerIdList.add(hiosIssuerId);
	}
}
