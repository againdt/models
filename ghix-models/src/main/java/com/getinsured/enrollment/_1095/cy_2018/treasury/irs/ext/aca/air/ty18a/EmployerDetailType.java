
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Employer Record Detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for EmployerDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployerDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}EmployerEIN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TINTypeCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerTINValidateNameResponseDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerMailingAddress"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerContactName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerContactPhoneNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployerDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "employerNm",
    "employerEIN",
    "tinTypeCd",
    "employerTINValidateNameResponseDetail",
    "employerMailingAddress",
    "employerContactName",
    "employerContactPhoneNum",
    "errorDetail"
})
public class EmployerDetailType {

    @XmlElement(name = "EmployerNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String employerNm;
    @XmlElement(name = "EmployerEIN", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String employerEIN;
    @XmlElement(name = "TINTypeCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String tinTypeCd;
    @XmlElement(name = "EmployerTINValidateNameResponseDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected TINValidateNameResponseDetailType employerTINValidateNameResponseDetail;
    @XmlElement(name = "EmployerMailingAddress", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MailingAddressGrpType employerMailingAddress;
    @XmlElement(name = "EmployerContactName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CompletePersonNameDetailType employerContactName;
    @XmlElement(name = "EmployerContactPhoneNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String employerContactPhoneNum;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the employerNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerNm() {
        return employerNm;
    }

    /**
     * Sets the value of the employerNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerNm(String value) {
        this.employerNm = value;
    }

    /**
     * Gets the value of the employerEIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerEIN() {
        return employerEIN;
    }

    /**
     * Sets the value of the employerEIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerEIN(String value) {
        this.employerEIN = value;
    }

    /**
     * Gets the value of the tinTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTINTypeCd() {
        return tinTypeCd;
    }

    /**
     * Sets the value of the tinTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTINTypeCd(String value) {
        this.tinTypeCd = value;
    }

    /**
     * Gets the value of the employerTINValidateNameResponseDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public TINValidateNameResponseDetailType getEmployerTINValidateNameResponseDetail() {
        return employerTINValidateNameResponseDetail;
    }

    /**
     * Sets the value of the employerTINValidateNameResponseDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public void setEmployerTINValidateNameResponseDetail(TINValidateNameResponseDetailType value) {
        this.employerTINValidateNameResponseDetail = value;
    }

    /**
     * Gets the value of the employerMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public MailingAddressGrpType getEmployerMailingAddress() {
        return employerMailingAddress;
    }

    /**
     * Sets the value of the employerMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public void setEmployerMailingAddress(MailingAddressGrpType value) {
        this.employerMailingAddress = value;
    }

    /**
     * Gets the value of the employerContactName property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public CompletePersonNameDetailType getEmployerContactName() {
        return employerContactName;
    }

    /**
     * Sets the value of the employerContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public void setEmployerContactName(CompletePersonNameDetailType value) {
        this.employerContactName = value;
    }

    /**
     * Gets the value of the employerContactPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerContactPhoneNum() {
        return employerContactPhoneNum;
    }

    /**
     * Sets the value of the employerContactPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerContactPhoneNum(String value) {
        this.employerContactPhoneNum = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
