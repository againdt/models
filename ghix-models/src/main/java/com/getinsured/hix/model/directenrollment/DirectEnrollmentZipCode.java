package com.getinsured.hix.model.directenrollment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "D2C_ZIPCOUNTY")
public class DirectEnrollmentZipCode {

	/**
	 * We are never going to update a zip code using this JPA, so no point
	 * putting sequence generator information here
	 */
	@Id
	private Long id;

	@Column(name = "ZIP")
	private String zip;

	@Column(name = "STATE")
	private String state;

	@Column(name = "COUNTY")
	private String county;

	@Column(name = "ISSUER_BRAND_NAME")
	private String issuerBrandName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getIssuerBrandName() {
		return issuerBrandName;
	}

	public void setIssuerBrandName(String issuerBrandName) {
		this.issuerBrandName = issuerBrandName;
	}

}
