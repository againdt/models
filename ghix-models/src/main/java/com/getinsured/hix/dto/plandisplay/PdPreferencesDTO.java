package com.getinsured.hix.dto.plandisplay;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalCondition;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalProcedure;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PreferencesLevel;


public class PdPreferencesDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private PreferencesLevel medicalUse;
	private PreferencesLevel prescriptionUse;
	private List<String> benefits;
	private String providers;
	private String prescriptions;
	private Long pdHouseholdId;
	private Long eligLeadId;
	private MedicalCondition medicalCondition;
	private MedicalProcedure medicalProcedure;
	
	public PreferencesLevel getMedicalUse() {
		return medicalUse;
	}

	public void setMedicalUse(PreferencesLevel medicalUse) {
		this.medicalUse = medicalUse;
	}
	
	public PreferencesLevel getPrescriptionUse() {
		return prescriptionUse;
	}

	public void setPrescriptionUse(PreferencesLevel prescriptionUse) {
		this.prescriptionUse = prescriptionUse;
	}
	
	public List<String> getBenefits() {
		return benefits;
	}

	public void setBenefits(List<String> benefits) {
		this.benefits = benefits;
	}

	public String getProviders() {
		return providers;
	}

	public void setProviders(String providers) {
		this.providers = providers;
	}

	public String getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(String prescriptions) {
		this.prescriptions = prescriptions;
	}

	public Long getPdHouseholdId() {
		return pdHouseholdId;
	}

	public void setPdHouseholdId(Long pdHouseholdId) {
		this.pdHouseholdId = pdHouseholdId;
	}

	public Long getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(Long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	
	public MedicalCondition getMedicalCondition() {
		return medicalCondition;
	}
	 
	public void setMedicalCondition(MedicalCondition medicalCondition) {
		this.medicalCondition = medicalCondition;
	}

	public MedicalProcedure getMedicalProcedure() {
		return medicalProcedure;
	}

	public void setMedicalProcedure(MedicalProcedure medicalProcedure) {
		this.medicalProcedure = medicalProcedure;
	}
	
}