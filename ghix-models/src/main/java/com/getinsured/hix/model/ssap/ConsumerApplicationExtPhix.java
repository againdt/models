package com.getinsured.hix.model.ssap;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the SSAP_APPLICATIONS_EXT_PHIX database table.
 *
 */

@Entity
@Table(name="SSAP_APPLICATIONS_EXT_PHIX")
@NamedQuery(name="ConsumerApplicationExtPhix.findAll", query="SELECT s FROM ConsumerApplicationExtPhix s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsumerApplicationExtPhix implements Serializable {
	
	private static final long serialVersionUID = 1L;	
		
	@GenericGenerator(name = "generator", strategy = "foreign",	parameters = @Parameter(name = "property", value = "consumerApplication"))	
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name="SSAP_APPLICATION_ID", unique = true, nullable = true)
	private long ssapApplicationId;

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private ConsumerApplication consumerApplication;

	@Column(name="PROXY_ID", length=100)
	private long proxyId;

	@Column(name="NUM_OF_PROXY_ATTEMPTS", precision=10)
	private int numOfProxyAttempts;

	@Column(name="NUM_OF_HCGOV_ATTEMPTS")
	private int numOfHcgovAttempts;

	@Column(name="HCGOV_ELIG_DOC_URL")
	private String hcgovEligDocUrl;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ENROLLMENT_SUBMIT_TIMESTAMP")
	private Date enrollmentSubmitTimestamp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date lastUpdatedTimestamp;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PROXY_SUBMIT_TIMESTAMP")
	private Date proxySubmitTimestamp;
	
	
	public ConsumerApplicationExtPhix() {
	}	

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdatedTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdatedTimestamp(new TSDate());
	}

	public long getProxyId() {
		return proxyId;
	}


	public void setProxyId(long proxyId) {
		this.proxyId = proxyId;
	}


	public int getNumOfProxyAttempts() {
		return numOfProxyAttempts;
	}


	public void setNumOfProxyAttempts(int numOfProxyAttempts) {
		this.numOfProxyAttempts = numOfProxyAttempts;
	}


	public int getNumOfHcgovAttempts() {
		return numOfHcgovAttempts;
	}


	public void setNumOfHcgovAttempts(int numOfHcgovAttempts) {
		this.numOfHcgovAttempts = numOfHcgovAttempts;
	}


	public String getHcgovEligDocUrl() {
		return hcgovEligDocUrl;
	}


	public void setHcgovEligDocUrl(String hcgovEligDocUrl) {
		this.hcgovEligDocUrl = hcgovEligDocUrl;
	}

	public ConsumerApplication getConsumerApplication() {
		return consumerApplication;
	}

	public void setConsumerApplication(ConsumerApplication consumerApplication) {
		this.consumerApplication = consumerApplication;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Date getEnrollmentSubmitTimestamp() {
		return enrollmentSubmitTimestamp;
	}

	public void setEnrollmentSubmitTimestamp(Date enrollmentSubmitTimestamp) {
		this.enrollmentSubmitTimestamp = enrollmentSubmitTimestamp;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public Date getProxySubmitTimestamp() {
		return proxySubmitTimestamp;
	}

	public void setProxySubmitTimestamp(Date proxySubmitTimestamp) {
		this.proxySubmitTimestamp = proxySubmitTimestamp;
	}


}
