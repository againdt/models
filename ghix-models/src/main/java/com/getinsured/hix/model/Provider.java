package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the provider database table.
 * 
 */

@Entity
@Table(name="provider")
public class Provider implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum ProviderType{
		 FACILITY,PHYSICIAN;
	}
	
	public enum ProviderStatus{
		 ACTIVE,INACTIVE,RETIRED,DECEASED;
	}
	public enum SanctionStatus{
		Y,N;
	}
	public enum AccessibilityCode{
		P,B,W,E,T,R;
	}
	public enum FSSProvider{
		Y,N;
	}
	public enum LanguagesLocation{
		L,P,B;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Provider_Seq")
	@SequenceGenerator(name = "Provider_Seq", sequenceName = "provider_seq", allocationSize = 1)
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="gi_type")
	@Enumerated(EnumType.STRING)
	private ProviderType type;
		
	@Column(name="npi_number")
	private String npiNumber;
	
	@Column(name="upi_number")
	private String upiNumber;
	
	@Column(name="license_number")
	private String licenseNumber;
	
	@Column(name="medicare_provider_id")
	private String medicareProviderId;
	
	@Column(name="tax_id")
	private String taxId;
	
	@Column(name="office_hours")
	private String officeHours;
	
	@Column(name="datasource")
	private String datasource;
	
	@Column(name="datasource_ref_id")
	private String datasourceRefId;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private ProviderStatus status;

	@Temporal(TemporalType.DATE)
	@Column(name="last_update_date")
	private Date lastUpdateDate;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "provider")
	private Set<ProviderNetwork> providerNetwork = new HashSet<ProviderNetwork>(0);
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	@Column(name="taxonomy_code")
	private String taxonomy_code;
	
	@Column(name="licensing_state")
	private String licensing_state;
	
	@Column(name="UPIN")
	private String UPIN;
	
	@Column(name="SSN")
	private Integer SSN;
	
	@Column(name="medical_provider_id")
	private String medical_provider_id;
	
	@Column(name="ffs_provider_country")
	private String ffs_provider_country;
	
	@Column(name="languages_location")
	@Enumerated(EnumType.STRING)
	private LanguagesLocation languages_location;
	
	@Column(name="ffs_provider")
	@Enumerated(EnumType.STRING)
	private FSSProvider ffs_provider;
	
	@Column(name="accessibility_code")
	@Enumerated(EnumType.STRING)
	private AccessibilityCode accessibility_code;
	
	@Column(name="sanction_status")
	@Enumerated(EnumType.STRING)
	private SanctionStatus sanction_status;
	
	@Column(name="location_id")
	private String location_id;
	
	@Column(name="pcp_id")
	private String pcp_id;
	
	@Column(name="group_key")
	private String groupKey;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public ProviderType getType() {
		return type;
	}

	public void setType(ProviderType type) {
		this.type = type;
	}

	public ProviderStatus getStatus() {
		return status;
	}

	public void setStatus(ProviderStatus status) {
		this.status = status;
	}
	
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdated) {
		this.lastUpdateDate = lastUpdated;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
	public void setNpiNumber(String npiNumber) {
		this.npiNumber = npiNumber;
	}

	public String getUpiNumber() {
		return upiNumber;
	}

	public void setUpiNumber(String upiNumber) {
		this.upiNumber = upiNumber;
	}

	public String getNpiNumber() {
		return npiNumber;
	}

	public String getMedicareProviderId() {
		return medicareProviderId;
	}

	public void setMedicareProviderId(String medicareProviderId) {
		this.medicareProviderId = medicareProviderId;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getOfficeHours() {
		return officeHours;
	}

	public void setOfficeHours(String officeHours) {
		this.officeHours = officeHours;
	}
	
	public String getDatasource() {
		return datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public String getDatasourceRefId() {
		return datasourceRefId;
	}

	public void setDatasourceRefId(String datasourceRefId) {
		this.datasourceRefId = datasourceRefId;
	}

	public Set<ProviderNetwork> getProviderNetwork() {
		return providerNetwork;
	}

	public void setProviderNetwork(Set<ProviderNetwork> providerNetwork) {
		this.providerNetwork = providerNetwork;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	public String getTaxonomy_code() {
		return taxonomy_code;
	}

	public void setTaxonomy_code(String taxonomy_code) {
		this.taxonomy_code = taxonomy_code;
	}

	public String getLicensing_state() {
		return licensing_state;
	}

	public void setLicensing_state(String licensing_state) {
		this.licensing_state = licensing_state;
	}

	public String getUPIN() {
		return UPIN;
	}

	public void setUPIN(String uPIN) {
		UPIN = uPIN;
	}

	public Integer getSSN() {
		return SSN;
	}

	public void setSSN(Integer sSN) {
		SSN = sSN;
	}

	public String getMedical_provider_id() {
		return medical_provider_id;
	}

	public void setMedical_provider_id(String medical_provider_id) {
		this.medical_provider_id = medical_provider_id;
	}

	public String getFfs_provider_country() {
		return ffs_provider_country;
	}

	public void setFfs_provider_country(String ffs_provider_country) {
		this.ffs_provider_country = ffs_provider_country;
	}

	
	public String getLocation_id() {
		return location_id;
	}

	public void setLocation_id(String location_id) {
		this.location_id = location_id;
	}

	public String getPcp_id() {
		return pcp_id;
	}

	public void setPcp_id(String pcp_id) {
		this.pcp_id = pcp_id;
	}

	public LanguagesLocation getLanguages_location() {
		return languages_location;
	}

	public void setLanguages_location(LanguagesLocation languages_location) {
		this.languages_location = languages_location;
	}

	public FSSProvider getFfs_provider() {
		return ffs_provider;
	}

	public void setFfs_provider(FSSProvider ffs_provider) {
		this.ffs_provider = ffs_provider;
	}

	public AccessibilityCode getAccessibility_code() {
		return accessibility_code;
	}

	public void setAccessibility_code(AccessibilityCode accessibility_code) {
		this.accessibility_code = accessibility_code;
	}

	public SanctionStatus getSanction_status() {
		return sanction_status;
	}

	public void setSanction_status(SanctionStatus sanction_status) {
		this.sanction_status = sanction_status;
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	
	
}
