package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.List;

public class HouseholdRequestDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<Integer> householdIdList;

	public List<Integer> getHouseholdIdList() {
		return householdIdList;
	}

	public void setHouseholdIdList(List<Integer> householdIdList) {
		this.householdIdList = householdIdList;
	}
	
	

}
