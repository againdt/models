package com.getinsured.coveredCA.dto.ssap;

import com.getinsured.coveredCA.dto.eligibility.ExchangePlanAvailabilityStatus;
import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class PlanDetails {
	
	@Expose
	private String planName;
	@Expose
	private Integer planId;
	@Expose
	private String premiumAfterCredit;
	@Expose
	private String premiumBeforeCredit;
	@Expose
	private String hiosId;
	@Expose
	private ExchangePlanAvailabilityStatus exchangePlanAvailabilityStatus;
	@Expose
	private String planLevel;
	
	@Expose
	private boolean isPlanAvailableOnCA;
	
	public ExchangePlanAvailabilityStatus getExchangePlanAvailabilityStatus() {
		return exchangePlanAvailabilityStatus;
	}
	public void setExchangePlanAvailabilityStatus(
			ExchangePlanAvailabilityStatus exchangePlanAvailabilityStatus) {
		this.exchangePlanAvailabilityStatus = exchangePlanAvailabilityStatus;
	}
	public String getHiosId() {
		return hiosId;
	}
	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPremiumAfterCredit() {
		return premiumAfterCredit;
	}
	public void setPremiumAfterCredit(String premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}
	public String getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}
	public void setPremiumBeforeCredit(String premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	
	public boolean isPlanAvailableOnCA() {
		return isPlanAvailableOnCA;
	}
	public void setPlanAvailableOnCA(boolean isPlanAvailableOnCA) {
		this.isPlanAvailableOnCA = isPlanAvailableOnCA;
	}

	

}
