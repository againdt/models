package com.getinsured.hix.model.phix.provider;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="provider_education")
public class ProviderEducation  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private ProviderProduct providerProduct;
	private String educationType;
	private String educationValue;
	private String educationYear;

	@Id 
	@SequenceGenerator(name="PROVIDER_EDUCATION_SEQ", sequenceName="PROVIDER_EDUCATION_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="PROVIDER_EDUCATION_SEQ")    
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROVIDER_PRODUCT_ID")
	public ProviderProduct getProviderProduct() {
		return this.providerProduct;
	}

	public void setProviderProduct(ProviderProduct providerProduct) {
		this.providerProduct = providerProduct;
	}


	@Column(name="EDUCATION_TYPE", length=20)
	public String getEducationType() {
		return this.educationType;
	}

	public void setEducationType(String educationType) {
		this.educationType = educationType;
	}


	@Column(name="EDUCATION_VALUE", length=100)
	public String getEducationValue() {
		return this.educationValue;
	}

	public void setEducationValue(String educationValue) {
		this.educationValue = educationValue;
	}


	@Column(name="EDUCATION_YEAR", length=10)
	public String getEducationYear() {
		return this.educationYear;
	}

	public void setEducationYear(String educationYear) {
		this.educationYear = educationYear;
	}




}


