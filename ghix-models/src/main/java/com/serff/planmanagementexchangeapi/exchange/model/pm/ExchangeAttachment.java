
package com.serff.planmanagementexchangeapi.exchange.model.pm;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ExchangeAttachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExchangeAttachment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}fileName"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}dateLastModified"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}exchangeSuppliedData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fileName",
    "dateLastModified",
    "exchangeSuppliedData"
})
@XmlRootElement(name="exchangeAttachment")
public class ExchangeAttachment {

    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    protected String fileName;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateLastModified;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true, nillable = true)
    @XmlMimeType("application/octet-stream")
    protected DataHandler exchangeSuppliedData;

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the dateLastModified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateLastModified() {
        return dateLastModified;
    }

    /**
     * Sets the value of the dateLastModified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateLastModified(XMLGregorianCalendar value) {
        this.dateLastModified = value;
    }

    /**
     * Gets the value of the exchangeSuppliedData property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getExchangeSuppliedData() {
        return exchangeSuppliedData;
    }

    /**
     * Sets the value of the exchangeSuppliedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setExchangeSuppliedData(DataHandler value) {
        this.exchangeSuppliedData = value;
    }

}
