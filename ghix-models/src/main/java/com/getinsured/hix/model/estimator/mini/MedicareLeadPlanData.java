package com.getinsured.hix.model.estimator.mini;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * MedicareLeadPlanData
 */

public class MedicareLeadPlanData   {
  @JsonProperty("planId")
  private String planId = null;

  @JsonProperty("insuranceType")
  private String insuranceType = null;

  @JsonProperty("plantype")
  private String plantype = null;

  @JsonProperty("planName")
  private String planName = null;

  @JsonProperty("planHiosId")
  private String planHiosId = null;

  @JsonProperty("premium")
  private String premium = null;

  @JsonProperty("carrierName")
  private String carrierName = null;

  @JsonProperty("planSource")
  private String planSource = null;

  @JsonProperty("coverageDate")
  private String coverageDate = null;

  public MedicareLeadPlanData planId(String planId) {
    this.planId = planId;
    return this;
  }

   /**
   * Get planId
   * @return planId
  **/
  @NotNull
  public String getPlanId() {
    return planId;
  }

  public void setPlanId(String planId) {
    this.planId = planId;
  }

  public MedicareLeadPlanData insuranceType(String insuranceType) {
    this.insuranceType = insuranceType;
    return this;
  }

   /**
   * Get insuranceType
   * @return insuranceType
  **/
  @NotNull
  public String getInsuranceType() {
    return insuranceType;
  }

  public void setInsuranceType(String insuranceType) {
    this.insuranceType = insuranceType;
  }

  public MedicareLeadPlanData plantype(String plantype) {
    this.plantype = plantype;
    return this;
  }

   /**
   * Get plantype
   * @return plantype
  **/
  @NotNull
  public String getPlantype() {
    return plantype;
  }

  public void setPlantype(String plantype) {
    this.plantype = plantype;
  }

  public MedicareLeadPlanData planName(String planName) {
    this.planName = planName;
    return this;
  }

   /**
   * Get planName
   * @return planName
  **/
  public String getPlanName() {
    return planName;
  }

  public void setPlanName(String planName) {
    this.planName = planName;
  }

  public MedicareLeadPlanData planHiosId(String planHiosId) {
    this.planHiosId = planHiosId;
    return this;
  }

   /**
   * Get planHiosId
   * @return planHiosId
  **/
  @NotNull
  public String getPlanHiosId() {
    return planHiosId;
  }

  public void setPlanHiosId(String planHiosId) {
    this.planHiosId = planHiosId;
  }

  public MedicareLeadPlanData premium(String premium) {
    this.premium = premium;
    return this;
  }

   /**
   * Get premium
   * @return premium
  **/
  public String getPremium() {
    return premium;
  }

  public void setPremium(String premium) {
    this.premium = premium;
  }

  public MedicareLeadPlanData carrierName(String carrierName) {
    this.carrierName = carrierName;
    return this;
  }

   /**
   * Get carrierName
   * @return carrierName
  **/
  public String getCarrierName() {
    return carrierName;
  }

  public void setCarrierName(String carrierName) {
    this.carrierName = carrierName;
  }

  public MedicareLeadPlanData planSource(String planSource) {
    this.planSource = planSource;
    return this;
  }

   /**
   * Get planSource
   * @return planSource
  **/
  @NotNull
  public String getPlanSource() {
    return planSource;
  }

  public void setPlanSource(String planSource) {
    this.planSource = planSource;
  }

  public MedicareLeadPlanData coverageDate(String coverageDate) {
    this.coverageDate = coverageDate;
    return this;
  }

   /**
   * Get coverageDate
   * @return coverageDate
  **/
  @NotNull
  public String getCoverageDate() {
    return coverageDate;
  }

  public void setCoverageDate(String coverageDate) {
    this.coverageDate = coverageDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicareLeadPlanData medicareleadPlanData = (MedicareLeadPlanData) o;
    return Objects.equals(this.planId, medicareleadPlanData.planId) &&
        Objects.equals(this.insuranceType, medicareleadPlanData.insuranceType) &&
        Objects.equals(this.plantype, medicareleadPlanData.plantype) &&
        Objects.equals(this.planName, medicareleadPlanData.planName) &&
        Objects.equals(this.planHiosId, medicareleadPlanData.planHiosId) &&
        Objects.equals(this.premium, medicareleadPlanData.premium) &&
        Objects.equals(this.carrierName, medicareleadPlanData.carrierName) &&
        Objects.equals(this.planSource, medicareleadPlanData.planSource) &&
        Objects.equals(this.coverageDate, medicareleadPlanData.coverageDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(planId, insuranceType, plantype, planName, planHiosId, premium, carrierName, planSource, coverageDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MedicareLeadPlanData {\n");
    
    sb.append("    planId: ").append(toIndentedString(planId)).append("\n");
    sb.append("    insuranceType: ").append(toIndentedString(insuranceType)).append("\n");
    sb.append("    plantype: ").append(toIndentedString(plantype)).append("\n");
    sb.append("    planName: ").append(toIndentedString(planName)).append("\n");
    sb.append("    planHiosId: ").append(toIndentedString(planHiosId)).append("\n");
    sb.append("    premium: ").append(toIndentedString(premium)).append("\n");
    sb.append("    carrierName: ").append(toIndentedString(carrierName)).append("\n");
    sb.append("    planSource: ").append(toIndentedString(planSource)).append("\n");
    sb.append("    coverageDate: ").append(toIndentedString(coverageDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

