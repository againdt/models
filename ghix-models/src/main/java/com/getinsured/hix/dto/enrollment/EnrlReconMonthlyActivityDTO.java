package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class EnrlReconMonthlyActivityDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<IssuerDetails> issuerDetailsList;
	private Map<Integer, String> monthDetailsMap;
	private List<Integer> yearList;
	
	private List<HighLevelSummary> highLevelSummaryList;
	private List<IssuerLevelSummary> issuerLevelSummaryList;
	private List<FileLevelSummary> fileLevelSummaryList;
	private Long totalDiscrepancyCount;
	private List<TopDiscrepancy> topDiscrepancyList;
	private List<HighLevelSummary> lastTwelveMonthSummaryList;
	
	public Long getTotalDiscrepancyCount() {
		return totalDiscrepancyCount;
	}

	public void setTotalDiscrepancyCount(Long totalDiscrepancyCount) {
		this.totalDiscrepancyCount = totalDiscrepancyCount;
	}

	public List<IssuerDetails> getIssuerDetailsList() {
		return issuerDetailsList;
	}

	public List<HighLevelSummary> getHighLevelSummaryList() {
		return highLevelSummaryList;
	}

	public void setHighLevelSummaryList(List<HighLevelSummary> highLevelSummaryList) {
		this.highLevelSummaryList = highLevelSummaryList;
	}
	
	public List<IssuerLevelSummary> getIssuerLevelSummaryList() {
		return issuerLevelSummaryList;
	}

	public void setIssuerLevelSummaryList(List<IssuerLevelSummary> issuerLevelSummaryList) {
		this.issuerLevelSummaryList = issuerLevelSummaryList;
	}
	
	public Map<Integer, String> getMonthDetailsMap() {
		return monthDetailsMap;
	}

	public void setMonthDetailsMap(Map<Integer, String> monthDetailsMap) {
		this.monthDetailsMap = monthDetailsMap;
	}

	public void setIssuerDetailsList(List<IssuerDetails> issuerDetailsList) {
		this.issuerDetailsList = issuerDetailsList;
	}

	public List<Integer> getYearList() {
		return yearList;
	}

	public void setYearList(List<Integer> yearList) {
		this.yearList = yearList;
	}

	public List<FileLevelSummary> getFileLevelSummaryList() {
		return fileLevelSummaryList;
	}

	public void setFileLevelSummaryList(List<FileLevelSummary> fileLevelSummaryList) {
		this.fileLevelSummaryList = fileLevelSummaryList;
	}

	public List<TopDiscrepancy> getTopDiscrepancyList() {
		return topDiscrepancyList;
	}

	public void setTopDiscrepancyList(List<TopDiscrepancy> topDiscrepancyList) {
		this.topDiscrepancyList = topDiscrepancyList;
	}

	public List<HighLevelSummary> getLastTwelveMonthSummaryList() {
		return lastTwelveMonthSummaryList;
	}

	public void setLastTwelveMonthSummaryList(List<HighLevelSummary> lastTwelveMonthSummaryList) {
		this.lastTwelveMonthSummaryList = lastTwelveMonthSummaryList;
	}

	public static class IssuerDetails{
		private Integer issuerId;
		private String hiosIssuerId;
		private String issuerName;
		public Integer getIssuerId() {
			return issuerId;
		}
		public void setIssuerId(Integer issuerId) {
			this.issuerId = issuerId;
		}
		public String getHiosIssuerId() {
			return hiosIssuerId;
		}
		public void setHiosIssuerId(String hiosIssuerId) {
			this.hiosIssuerId = hiosIssuerId;
		}
		public String getIssuerName() {
			return issuerName;
		}
		public void setIssuerName(String issuerName) {
			this.issuerName = issuerName;
		}
	}
	
//	public static class MonthDetails{
//		private Integer monthNum;
//		private String monthName;
//		public Integer getMonthNum() {
//			return monthNum;
//		}
//		public void setMonthNum(Integer monthNum) {
//			this.monthNum = monthNum;
//		}
//		public String getMonthName() {
//			return monthName;
//		}
//		public void setMonthName(String monthName) {
//			this.monthName = monthName;
//		}
//		
//		public String getMonthName(Integer monthNum){
//			
//		}
//	}
	
	public static class TopDiscrepancy{
		private Long discrepancyCount;
		private String discrepancyTypeCode;
		private String discrepancyTypeLabel;
		private String discrepancyCategory;
		private Float discrepancyPercent;
		public Long getDiscrepancyCount() {
			return discrepancyCount;
		}
		public void setDiscrepancyCount(Long discrepancyCount) {
			this.discrepancyCount = discrepancyCount;
		}
		public String getDiscrepancyTypeCode() {
			return discrepancyTypeCode;
		}
		public void setDiscrepancyTypeCode(String discrepancyTypeCode) {
			this.discrepancyTypeCode = discrepancyTypeCode;
		}
		public String getDiscrepancyTypeLabel() {
			return discrepancyTypeLabel;
		}
		public void setDiscrepancyTypeLabel(String discrepancyTypeLabel) {
			this.discrepancyTypeLabel = discrepancyTypeLabel;
		}
		public String getDiscrepancyCategory() {
			return discrepancyCategory;
		}
		public void setDiscrepancyCategory(String discrepancyCategory) {
			this.discrepancyCategory = discrepancyCategory;
		}
		public Float getDiscrepancyPercent() {
			return discrepancyPercent;
		}
		public void setDiscrepancyPercent(Float discrepancyPercent) {
			this.discrepancyPercent = discrepancyPercent;
		}
		
	}
	
	public static class IssuerLevelSummary{
		private String hiosIssuerId;
		private String issuerName;
		private Long totalEnrlCntInFile;
		private Long totalEnrlInHix;
		private Long totalNumberOfFiles;
		private String lastReconDate;
		
		private Long enrlMissinginHixCount;
		private Long enrlMissingInFileCount;
		private Long enrlSuccessInHixCount;
		private Long enrollmentFailedCount;
		private Long totalDiscrepancyCount;
		private Long totalEnrlDiscrepancyCount;
		private Long totalEnrollmentMissingInFile;
		
		private Float enrlMissingInHixPercent;
		private Float enrlMissingInFilePercent;
		private Float enrlSuccessInHixPercent;
		private Float enrollmentFailedPercent;
		private Float totalDiscrepancyPercent;
		private Float totalEnrlDiscrepancyPercent;
		
		
		public String getHiosIssuerId() {
			return hiosIssuerId;
		}
		public void setHiosIssuerId(String hiosIssuerId) {
			this.hiosIssuerId = hiosIssuerId;
		}
		public String getIssuerName() {
			return issuerName;
		}
		public void setIssuerName(String issuerName) {
			this.issuerName = issuerName;
		}
		public Long getTotalEnrlCntInFile() {
			return totalEnrlCntInFile;
		}
		public void setTotalEnrlCntInFile(Long totalEnrlCntInFile) {
			this.totalEnrlCntInFile = totalEnrlCntInFile;
		}
		public Long getTotalEnrlInHix() {
			return totalEnrlInHix;
		}
		public void setTotalEnrlInHix(Long totalEnrlInHix) {
			this.totalEnrlInHix = totalEnrlInHix;
		}
		public Long getTotalNumberOfFiles() {
			return totalNumberOfFiles;
		}
		public void setTotalNumberOfFiles(Long totalNumberOfFiles) {
			this.totalNumberOfFiles = totalNumberOfFiles;
		}
		public String getLastReconDate() {
			return lastReconDate;
		}
		public void setLastReconDate(String lastReconDate) {
			this.lastReconDate = lastReconDate;
		}
		public Float getEnrlMissingInHixPercent() {
			return enrlMissingInHixPercent;
		}
		public void setEnrlMissingInHixPercent(Float enrlMissingInHixPercent) {
			this.enrlMissingInHixPercent = enrlMissingInHixPercent;
		}
		public Long getEnrlMissinginHixCount() {
			return enrlMissinginHixCount;
		}
		public void setEnrlMissinginHixCount(Long enrlMissinginHixCount) {
			this.enrlMissinginHixCount = enrlMissinginHixCount;
		}
		public Float getEnrlSuccessInHixPercent() {
			return enrlSuccessInHixPercent;
		}
		public void setEnrlSuccessInHixPercent(Float enrlSuccessInHixPercent) {
			this.enrlSuccessInHixPercent = enrlSuccessInHixPercent;
		}
		public Long getEnrlSuccessInHixCount() {
			return enrlSuccessInHixCount;
		}
		public void setEnrlSuccessInHixCount(Long enrlSuccessInHixCount) {
			this.enrlSuccessInHixCount = enrlSuccessInHixCount;
		}
		public Float getEnrollmentFailedPercent() {
			return enrollmentFailedPercent;
		}
		public void setEnrollmentFailedPercent(Float enrollmentFailedPercent) {
			this.enrollmentFailedPercent = enrollmentFailedPercent;
		}
		public Long getEnrollmentFailedCount() {
			return enrollmentFailedCount;
		}
		public void setEnrollmentFailedCount(Long enrollmentFailedCount) {
			this.enrollmentFailedCount = enrollmentFailedCount;
		}
		public Long getTotalDiscrepancyCount() {
			return totalDiscrepancyCount;
		}
		public void setTotalDiscrepancyCount(Long totalDiscrepancyCount) {
			this.totalDiscrepancyCount = totalDiscrepancyCount;
		}
		public Long getTotalEnrlDiscrepancyCount() {
			return totalEnrlDiscrepancyCount;
		}
		public void setTotalEnrlDiscrepancyCount(Long totalEnrlDiscrepancyCount) {
			this.totalEnrlDiscrepancyCount = totalEnrlDiscrepancyCount;
		}
		public Long getTotalEnrollmentMissingInFile() {
			return totalEnrollmentMissingInFile;
		}
		public void setTotalEnrollmentMissingInFile(Long totalEnrollmentMissingInFile) {
			this.totalEnrollmentMissingInFile = totalEnrollmentMissingInFile;
		}
		public Float getTotalDiscrepancyPercent() {
			return totalDiscrepancyPercent;
		}
		public void setTotalDiscrepancyPercent(Float totalDiscrepancyPercent) {
			this.totalDiscrepancyPercent = totalDiscrepancyPercent;
		}
		public Float getTotalEnrlDiscrepancyPercent() {
			return totalEnrlDiscrepancyPercent;
		}
		public void setTotalEnrlDiscrepancyPercent(Float totalEnrlDiscrepancyPercent) {
			this.totalEnrlDiscrepancyPercent = totalEnrlDiscrepancyPercent;
		}
		public Long getEnrlMissingInFileCount() {
			return enrlMissingInFileCount;
		}
		public void setEnrlMissingInFileCount(Long enrlMissingInFileCount) {
			this.enrlMissingInFileCount = enrlMissingInFileCount;
		}
		public Float getEnrlMissingInFilePercent() {
			return enrlMissingInFilePercent;
		}
		public void setEnrlMissingInFilePercent(Float enrlMissingInFilePercent) {
			this.enrlMissingInFilePercent = enrlMissingInFilePercent;
		}
	}
	
	public static class HighLevelSummary{
		private Integer month;
		private Integer year;
		private Long totalEnrlCntInFile;
		private Long totalEnrlInHix;
		private Long totalMissingInFile;
		private Long totalMissingInHix;
		private Long totalSuccess;
		private Long totalFailure;
		private Long totalDiscrepancyCount;
		private Long totalEnrlDiscrepancyCount;
		
		private Float totalMissingInFilePercent;
		private Float totalMissingInHixPercent;
		private Float totalSuccessPercent;
		private Float totalFailurePercent;
		private Float totalDiscrepancyPercent;
		private Float totalEnrlDiscrepancyPercent;
		private Float totalEnrlCntInFilePercent;
		
		public Integer getMonth() {
			return month;
		}
		public void setMonth(Integer month) {
			this.month = month;
		}
		public Integer getYear() {
			return year;
		}
		public void setYear(Integer year) {
			this.year = year;
		}
		public Long getTotalEnrlCntInFile() {
			return totalEnrlCntInFile;
		}
		public void setTotalEnrlCntInFile(Long totalEnrlCntInFile) {
			this.totalEnrlCntInFile = totalEnrlCntInFile;
		}
		public Long getTotalEnrlInHix() {
			return totalEnrlInHix;
		}
		public void setTotalEnrlInHix(Long totalEnrlInHix) {
			this.totalEnrlInHix = totalEnrlInHix;
		}
		public Long getTotalSuccess() {
			return totalSuccess;
		}
		public void setTotalSuccess(Long totalSuccess) {
			this.totalSuccess = totalSuccess;
		}
		public Long getTotalFailure() {
			return totalFailure;
		}
		public void setTotalFailure(Long totalFailure) {
			this.totalFailure = totalFailure;
		}
		public Long getTotalDiscrepancyCount() {
			return totalDiscrepancyCount;
		}
		public void setTotalDiscrepancyCount(Long totalDiscrepancyCount) {
			this.totalDiscrepancyCount = totalDiscrepancyCount;
		}
		public Long getTotalEnrlDiscrepancyCount() {
			return totalEnrlDiscrepancyCount;
		}
		public void setTotalEnrlDiscrepancyCount(Long totalEnrlDiscrepancyCount) {
			this.totalEnrlDiscrepancyCount = totalEnrlDiscrepancyCount;
		}
		public Float getTotalSuccessPercent() {
			return totalSuccessPercent;
		}
		public void setTotalSuccessPercent(Float totalSuccessPercent) {
			this.totalSuccessPercent = totalSuccessPercent;
		}
		public Float getTotalFailurePercent() {
			return totalFailurePercent;
		}
		public void setTotalFailurePercent(Float totalFailurePercent) {
			this.totalFailurePercent = totalFailurePercent;
		}
		public Float getTotalDiscrepancyPercent() {
			return totalDiscrepancyPercent;
		}
		public void setTotalDiscrepancyPercent(Float totalDiscrepancyPercent) {
			this.totalDiscrepancyPercent = totalDiscrepancyPercent;
		}
		public Float getTotalEnrlDiscrepancyPercent() {
			return totalEnrlDiscrepancyPercent;
		}
		public void setTotalEnrlDiscrepancyPercent(Float totalEnrlDiscrepancyPercent) {
			this.totalEnrlDiscrepancyPercent = totalEnrlDiscrepancyPercent;
		}
		public Float getTotalEnrlCntInFilePercent() {
			return totalEnrlCntInFilePercent;
		}
		public void setTotalEnrlCntInFilePercent(Float totalEnrlCntInFilePercent) {
			this.totalEnrlCntInFilePercent = totalEnrlCntInFilePercent;
		}
		public Long getTotalMissingInFile() {
			return totalMissingInFile;
		}
		public void setTotalMissingInFile(Long totalMissingInFile) {
			this.totalMissingInFile = totalMissingInFile;
		}
		public Long getTotalMissingInHix() {
			return totalMissingInHix;
		}
		public void setTotalMissingInHix(Long totalMissingInHix) {
			this.totalMissingInHix = totalMissingInHix;
		}
		public Float getTotalMissingInFilePercent() {
			return totalMissingInFilePercent;
		}
		public void setTotalMissingInFilePercent(Float totalMissingInFilePercent) {
			this.totalMissingInFilePercent = totalMissingInFilePercent;
		}
		public Float getTotalMissingInHixPercent() {
			return totalMissingInHixPercent;
		}
		public void setTotalMissingInHixPercent(Float totalMissingInHixPercent) {
			this.totalMissingInHixPercent = totalMissingInHixPercent;
		}
	}
	
	public static class FileLevelSummary{
		private Integer fileId;
		private Integer month;
		private Integer year;
		private String fileName;
		private String dateReceived;
		private String status;
		private Integer totalEnrlCntInFile;
		private Integer totalEnrlInHix;
		private Integer totalSuccess;
		private Integer totalFailure;
		private Integer totalEnrlMissingInHix;
		private Integer totalEnrlMissingInFile;
		private Integer totalDiscrepancyCount;
		private Integer totalEnrlDiscrepancyCount;
		private String discrepancyReportName;
		private String discrepancyReportCreationTimestamp;
		private IssuerDetails issuerDetails;
		
		
		public IssuerDetails getIssuerDetails() {
			return issuerDetails;
		}
		public void setIssuerDetails(IssuerDetails issuerDetails) {
			this.issuerDetails = issuerDetails;
		}
		public Integer getFileId() {
			return fileId;
		}
		public void setFileId(Integer fileId) {
			this.fileId = fileId;
		}
		public Integer getMonth() {
			return month;
		}
		public void setMonth(Integer month) {
			this.month = month;
		}
		public Integer getYear() {
			return year;
		}
		public void setYear(Integer year) {
			this.year = year;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getDateReceived() {
			return dateReceived;
		}
		public void setDateReceived(String dateReceived) {
			this.dateReceived = dateReceived;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public Integer getTotalEnrlCntInFile() {
			return totalEnrlCntInFile;
		}
		public void setTotalEnrlCntInFile(Integer totalEnrlCntInFile) {
			this.totalEnrlCntInFile = totalEnrlCntInFile;
		}
		public Integer getTotalEnrlInHix() {
			return totalEnrlInHix;
		}
		public void setTotalEnrlInHix(Integer totalEnrlInHix) {
			this.totalEnrlInHix = totalEnrlInHix;
		}
		public Integer getTotalSuccess() {
			return totalSuccess;
		}
		public void setTotalSuccess(Integer totalSuccess) {
			this.totalSuccess = totalSuccess;
		}
		public Integer getTotalFailure() {
			return totalFailure;
		}
		public void setTotalFailure(Integer totalFailure) {
			this.totalFailure = totalFailure;
		}
		public Integer getTotalDiscrepancyCount() {
			return totalDiscrepancyCount;
		}
		public void setTotalDiscrepancyCount(Integer totalDiscrepancyCount) {
			this.totalDiscrepancyCount = totalDiscrepancyCount;
		}
		public Integer getTotalEnrlDiscrepancyCount() {
			return totalEnrlDiscrepancyCount;
		}
		public void setTotalEnrlDiscrepancyCount(Integer totalEnrlDiscrepancyCount) {
			this.totalEnrlDiscrepancyCount = totalEnrlDiscrepancyCount;
		}
		public Integer getTotalEnrlMissingInHix() {
			return totalEnrlMissingInHix;
		}
		public void setTotalEnrlMissingInHix(Integer totalEnrlMissingInHix) {
			this.totalEnrlMissingInHix = totalEnrlMissingInHix;
		}
		public Integer getTotalEnrlMissingInFile() {
			return totalEnrlMissingInFile;
		}
		public void setTotalEnrlMissingInFile(Integer totalEnrlMissingInFile) {
			this.totalEnrlMissingInFile = totalEnrlMissingInFile;
		}
		public String getDiscrepancyReportName() {
			return discrepancyReportName;
		}
		public void setDiscrepancyReportName(String discrepancyReportName) {
			this.discrepancyReportName = discrepancyReportName;
		}
		public String getDiscrepancyReportCreationTimestamp() {
			return discrepancyReportCreationTimestamp;
		}
		public void setDiscrepancyReportCreationTimestamp(String discrepancyReportCreationTimestamp) {
			this.discrepancyReportCreationTimestamp = discrepancyReportCreationTimestamp;
		}
	}

}
