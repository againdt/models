package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrollmentXMLValidationDetails implements Serializable{
	public enum ValidationStatus{VALID, INVALID};
	
	private String XMLFileName;
	private String errorMessage;
	private ValidationStatus validationStatus;
	/**
	 * @return the xMLFileName
	 */
	public String getXMLFileName() {
		return XMLFileName;
	}
	/**
	 * @param xMLFileName the xMLFileName to set
	 */
	public void setXMLFileName(String xMLFileName) {
		XMLFileName = xMLFileName;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return the validationStatus
	 */
	public ValidationStatus getValidationStatus() {
		return validationStatus;
	}
	/**
	 * @param validationStatus the validationStatus to set
	 */
	public void setValidationStatus(ValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}
}
