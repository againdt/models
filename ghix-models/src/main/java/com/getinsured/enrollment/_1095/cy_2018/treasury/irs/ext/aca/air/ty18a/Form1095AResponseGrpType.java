
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Form 1095A Response Group Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-08-20&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;1095A affordable Insurance Exchange statement file from IRS to exchange&lt;/DescriptionTxt&gt;
 * 					&lt;DataElementID/&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095AResponseGrpType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095AResponseGrpType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}UniqueRecordSequenceNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}ErrorMessageCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}ErrorMessageTxt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}XpathContent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095AResponseGrpType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "uniqueRecordSequenceNum",
    "errorMessageCd",
    "errorMessageTxt",
    "xpathContent"
})
public class Form1095AResponseGrpType {

    @XmlElement(name = "UniqueRecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String uniqueRecordSequenceNum;
    @XmlElement(name = "ErrorMessageCd", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String errorMessageCd;
    @XmlElement(name = "ErrorMessageTxt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String errorMessageTxt;
    @XmlElement(name = "XpathContent", namespace = "urn:us:gov:treasury:irs:common")
    protected String xpathContent;

    /**
     * Gets the value of the uniqueRecordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueRecordSequenceNum() {
        return uniqueRecordSequenceNum;
    }

    /**
     * Sets the value of the uniqueRecordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueRecordSequenceNum(String value) {
        this.uniqueRecordSequenceNum = value;
    }

    /**
     * Gets the value of the errorMessageCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessageCd() {
        return errorMessageCd;
    }

    /**
     * Sets the value of the errorMessageCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessageCd(String value) {
        this.errorMessageCd = value;
    }

    /**
     * Gets the value of the errorMessageTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessageTxt() {
        return errorMessageTxt;
    }

    /**
     * Sets the value of the errorMessageTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessageTxt(String value) {
        this.errorMessageTxt = value;
    }

    /**
     * Gets the value of the xpathContent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXpathContent() {
        return xpathContent;
    }

    /**
     * Sets the value of the xpathContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXpathContent(String value) {
        this.xpathContent = value;
    }

}
