/**
 * AHBXSyncResponse.java
 * @author chalse_v
 * @version 1.0
 * @since Apr 10, 2013 
 */
package com.getinsured.hix.dto.planmgmt.ahbx;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.getinsured.hix.model.GHIXResponse;

/**
 * The AHBX synchronization ReST response class.
 * 
 * @author chalse_v
 * @version 1.0
 * @since Apr 10, 2013 
 *
 */
public class AHBXSyncResponse extends GHIXResponse implements Serializable{
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Attribute Logger LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger(AHBXSyncResponse.class);
	
	/**
	 * Attribute Map<String,Object> responseData
	 */
	private Map<String, Object> responseData = new HashMap<String, Object>();
	
	/**
	 * Constructor.
	 */
	public AHBXSyncResponse() {
		this.startResponse();
	}
	
	/**
	 * Getter for 'responseData'.
	 *
	 * @return the responseData.
	 */
	public Map<String, Object> getResponseData() {
		return this.responseData;
	}
	
	/**
	 * Setter for 'responseData'.
	 *
	 * @param responseData the responseData to set.
	 */
	public void setResponseData(Map<String, Object> responseData) {
		this.responseData = responseData;
	}
	
	/**
	 * Method to add entries into responseData collection.
	 *
	 * @return void
	 */
	public void addResponseData(String key, Object data) {
		this.responseData.put(key, data);
	}
	
	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse(){
		LOGGER.info("Sending rest response.");
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON. 
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse(){
		String transformedResponse = null;
		
		this.endResponse();
		LOGGER.info("Beginning to create JSON response.");
		
		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
		    public HierarchicalStreamWriter createWriter(Writer writer) {
		        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		    }
		});
		
		jsoner.setMode(XStream.NO_REFERENCES);
		
		Validate.notNull(jsoner, "XStream instance improperly confugured.");
		
		transformedResponse = jsoner.toXML(this);
		
		LOGGER.info("Created JSON response.");
		return transformedResponse;
	}	*/
}
