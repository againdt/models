MERGE INTO
	enrollment_premium epr
		USING(
	SELECT
		ep.id AS id,
		pd.max_subsidy AS max_subsidy,
		e.ehb_amt AS ehb_amt,
		LEAST(pd.max_subsidy, e.ehb_amt) AS max_aptc
	FROM
		enrollment e
	JOIN enrollment_premium ep ON
		ep.enrollment_id = e.id
	JOIN pd_household pd ON
		e.pd_household_id = pd.id
	WHERE
		ep.gross_prem_amt IS NOT NULL
		AND ep.net_prem_amt IS NOT NULL
		AND ep.aptc_amt IS NOT NULL
		AND ep.max_aptc IS NULL
		AND e.ehb_amt IS NOT NULL
		AND pd.max_subsidy IS NOT NULL
		AND ep.month BETWEEN EXTRACT(MONTH FROM e.benefit_effective_date) AND EXTRACT(MONTH FROM e.benefit_end_date) ) s ON
	(epr.id = s.id)
	WHEN MATCHED THEN UPDATE
		SET
			epr.max_aptc = s.max_aptc;