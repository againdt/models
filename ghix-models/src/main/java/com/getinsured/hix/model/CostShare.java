/**
 * File 'CostShare.java' for persisting 'COST_SHARE' table records.
 *
 * @author chalse_v
 * @since Jun 13, 2013
 *
 */
package com.getinsured.hix.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.getinsured.timeshift.TimeShifterUtil;

/**
 * Persistence class 'CostShare' mapping to table 'FORMULARY_COST_SHARE'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 13, 2013
 *
 */

@Entity
@Table(name="COST_SHARE")
public class CostShare {
    @Id
	@SequenceGenerator(name = "seqCostShare", sequenceName = "COST_SHARE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCostShare")
    private Integer id;

    @ManyToOne
    @JoinColumn(name="FORMULARY_ID", referencedColumnName = "ID")
	private Formulary formulary;

    @Column(name = "COST_SHARE_TYPE")
	@NotNull
    protected String costSharingType;
    
    @Column(name = "COPAYMENT_AMOUNT")
	@NotNull
    protected BigDecimal coPaymentAmount;
    
    @Column(name = "COINSURANCE_PERCENT")   
	@NotNull 
    protected BigDecimal coInsurancePercent;
    
    @Column(name = "NETWORK_COST_TYPE")
	@NotNull
    protected String networkCostType;
    
    @Column(name = "DRUG_PRESCRIPTION_PERIOD")
	@NotNull
    protected Integer drugPrescriptionPeriod;
    
    @Column(name = "DRUG_PRESCRIPTION_PERIOD_UNIT")
	@NotNull
    protected String drugPrescriptionPeriodUnit;
    
    @Column(name = "PHARMACY_CODE")
	@NotNull
    protected String pharmacyCode;
    
    @Column(name = "DRUG_TIER_LEVEL")
    private Integer drugTierLevel;    
    
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@NotNull
	private Timestamp lastUpdateTimestamp;
	
	@Column(name="IS_DELETED")
	private String isDeleted;	
	
	@Column(name="COPAYMENT_ATTR")
	private String copaymentAttribute;	
	
	@Column(name="COINSURANCE_ATTR")
	private String coinsuranceAttribute;	
	/**
	 * Default constructor.
	 */
	public CostShare() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis())); 
	}

	/**
	 * Getter for 'id'.
	 *
	 * @return the id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Setter for 'id'.
	 *
	 * @param id the 'id' to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Getter for 'formulary'.
	 *
	 * @return the formulary.
	 */
	public Formulary getFormulary() {
		return formulary;
	}

	/**
	 * Setter for 'formulary'.
	 *
	 * @param formulary the 'formulary' to set
	 */
	public void setFormulary(Formulary formulary) {
		this.formulary = formulary;
	}

	/**
	 * Getter for 'networkCostType'.
	 *
	 * @return the networkCostType.
	 */
	public String getNetworkCostType() {
		return networkCostType;
	}

	/**
	 * Setter for 'networkCostType'.
	 *
	 * @param networkCostType the 'networkCostType' to set
	 */
	public void setNetworkCostType(String networkCostType) {
		this.networkCostType = networkCostType;
	}

	/**
	 * Getter for 'drugPrescriptionPeriod'.
	 *
	 * @return the drugPrescriptionPeriod.
	 */
	public Integer getDrugPrescriptionPeriod() {
		return drugPrescriptionPeriod;
	}

	/**
	 * Setter for 'drugPrescriptionPeriod'.
	 *
	 * @param drugPrescriptionPeriod the 'drugPrescriptionPeriod' to set
	 */
	public void setDrugPrescriptionPeriod(Integer drugPrescriptionPeriod) {
		this.drugPrescriptionPeriod = drugPrescriptionPeriod;
	}

	/**
	 * Getter for 'drugPrescriptionPeriodUnit'.
	 *
	 * @return the drugPrescriptionPeriodUnit.
	 */
	public String getDrugPrescriptionPeriodUnit() {
		return drugPrescriptionPeriodUnit;
	}

	/**
	 * Setter for 'drugPrescriptionPeriodUnit'.
	 *
	 * @param drugPrescriptionPeriodUnit the 'drugPrescriptionPeriodUnit' to set
	 */
	public void setDrugPrescriptionPeriodUnit(String drugPrescriptionPeriodUnit) {
		this.drugPrescriptionPeriodUnit = drugPrescriptionPeriodUnit;
	}

	/**
	 * Getter for 'pharmacyCode'.
	 *
	 * @return the pharmacyCode.
	 */
	public String getPharmacyCode() {
		return pharmacyCode;
	}

	/**
	 * Setter for 'pharmacyCode'.
	 *
	 * @param pharmacyCode the 'pharmacyCode' to set
	 */
	public void setPharmacyCode(String pharmacyCode) {
		this.pharmacyCode = pharmacyCode;
	}

	/**
	 * Getter for 'costSharingType'.
	 *
	 * @return the costSharingType.
	 */
	public String getCostSharingType() {
		return costSharingType;
	}

	/**
	 * Setter for 'costSharingType'.
	 *
	 * @param costSharingType the 'costSharingType' to set
	 */
	public void setCostSharingType(String costSharingType) {
		this.costSharingType = costSharingType;
	}

	/**
	 * Getter for 'coPaymentAmount'.
	 *
	 * @return the coPaymentAmount.
	 */
	public BigDecimal getCoPaymentAmount() {
		return coPaymentAmount;
	}

	/**
	 * Setter for 'coPaymentAmount'.
	 *
	 * @param coPaymentAmount the 'coPaymentAmount' to set
	 */
	public void setCoPaymentAmount(BigDecimal coPaymentAmount) {
		this.coPaymentAmount = coPaymentAmount;
	}

	/**
	 * Getter for 'coInsurancePercent'.
	 *
	 * @return the coInsurancePercent.
	 */
	public BigDecimal getCoInsurancePercent() {
		return coInsurancePercent;
	}

	/**
	 * Setter for 'coInsurancePercent'.
	 *
	 * @param coInsurancePercent the 'coInsurancePercent' to set
	 */
	public void setCoInsurancePercent(BigDecimal coInsurancePercent) {
		this.coInsurancePercent = coInsurancePercent;
	}

	/**
	 * Getter for 'creationTimestamp'.
	 *
	 * @return the creationTimestamp.
	 */
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * Setter for 'creationTimestamp'.
	 *
	 * @param creationTimestamp the 'creationTimestamp' to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * Getter for 'lastUpdateTimestamp'.
	 *
	 * @return the lastUpdateTimestamp.
	 */
	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	/**
	 * Setter for 'lastUpdateTimestamp'.
	 *
	 * @param lastUpdateTimestamp the 'lastUpdateTimestamp' to set
	 */
	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}


	/**
	 * Getter for 'drugTierLevel'
	 * 
	 * @return drugTierLevel
	 */
	public Integer getDrugTierLevel() {
		return drugTierLevel;
	}

	/**
	 * Setter for 'drugTierLevel'
	 * 
	 * @param drugTierLevel
	 */
	public void setDrugTierLevel(Integer drugTierLevel) {
		this.drugTierLevel = drugTierLevel;
	}
	
	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCopaymentAttribute() {
		return copaymentAttribute;
	}

	public void setCopaymentAttribute(String copaymentAttribute) {
		this.copaymentAttribute = copaymentAttribute;
	}

	public String getCoinsuranceAttribute() {
		return coinsuranceAttribute;
	}

	public void setCoinsuranceAttribute(String coinsuranceAttribute) {
		this.coinsuranceAttribute = coinsuranceAttribute;
	}
	
}
