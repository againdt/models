package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.enrollment.EnrollmentMemberDTO;

public class EnrollmentWithMemberDataDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String enrollmentID;
	private String enrollmentStatus;
	private String insuranceType;
	private String planID;
	private List<EnrollmentMembersDTO> members;
	
	
	public EnrollmentWithMemberDataDTO() {
		
	}


	public String getPlanID() {
		return planID;
	}


	public void setPlanID(String planID) {
		this.planID = planID;
	}


	public List<EnrollmentMembersDTO> getMembers() {
		return members;
	}


	public void setMembers(List<EnrollmentMembersDTO> members) {
		this.members = members;
	}


	public String getEnrollmentID() {
		return enrollmentID;
	}

	public void setEnrollmentID(String enrollmentID) {
		this.enrollmentID = enrollmentID;
	}

	
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	
	

	
	
	
}
