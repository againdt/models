package com.getinsured.hix.model.eligibility;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.ssap.ConsumerApplicant;

/**
 * The persistent class for the PROGRAM_ELIGIBILITIES database table.
 *
 */

@Entity
@Table(name = "PROGRAM_ELIGIBILITIES")
@DynamicInsert
@DynamicUpdate
public class ConsumerEligibilityProgram {

	public enum EligibilityProgramValues{
		APTC,
		QHP,
		MEDICAID,
		CSR,
		CHIP,
		BLANK
	}

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROGRAM_ELIGIBILITIES_SEQ")
	@SequenceGenerator(name = "PROGRAM_ELIGIBILITIES_SEQ", sequenceName = "PROGRAM_ELIGIBILITIES_SEQ", allocationSize = 1)
	private Long id;

	@Column(name = "ELIGIBILITY_TYPE")
	private String eligibilityType;

	@Column(name = "ELIGIBILITY_INDICATOR")
	private String eligibilityIndicator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_START_DATE")
	private Date eligibilityStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_END_DATE")
	private Date eligibilityEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_DETERMINATION_DATE")
	private Date eligibilityDeterminationDate;

	@Column(name = "INELIGIBLE_REASON")
	private String ineligibleReason;

	@Column(name = "ESTABLISHING_CATEGORY_CODE")
	private String establishingCategoryCode;

	@Column(name = "ESTABLISHING_STATE_CODE")
	private String establishingStateCode;

	@Column(name = "ESTABLISHING_COUNTY_NAME")
	private String establishingCountyName;
/*
	@Column(name = "SSAP_APPLICANT_ID")
	private Long ssapApplicantId;
	*/
	
	@ManyToOne
	@JoinColumn(name="SSAP_APPLICANT_ID")
	private ConsumerApplicant ssapApplicant; 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEligibilityType() {
		return eligibilityType;
	}

	public void setEligibilityType(String eligibilityType) {
		this.eligibilityType = eligibilityType;
	}

	public String getEligibilityIndicator() {
		return eligibilityIndicator;
	}

	public void setEligibilityIndicator(String eligibilityIndicator) {
		this.eligibilityIndicator = eligibilityIndicator;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	public Date getEligibilityDeterminationDate() {
		return eligibilityDeterminationDate;
	}

	public void setEligibilityDeterminationDate(Date eligibilityDeterminationDate) {
		this.eligibilityDeterminationDate = eligibilityDeterminationDate;
	}

	public String getIneligibleReason() {
		return ineligibleReason;
	}

	public void setIneligibleReason(String ineligibleReason) {
		this.ineligibleReason = ineligibleReason;
	}

	public String getEstablishingCategoryCode() {
		return establishingCategoryCode;
	}

	public void setEstablishingCategoryCode(String establishingCategoryCode) {
		this.establishingCategoryCode = establishingCategoryCode;
	}

	public String getEstablishingStateCode() {
		return establishingStateCode;
	}

	public void setEstablishingStateCode(String establishingStateCode) {
		this.establishingStateCode = establishingStateCode;
	}

	public String getEstablishingCountyName() {
		return establishingCountyName;
	}

	public void setEstablishingCountyName(String establishingCountyName) {
		this.establishingCountyName = establishingCountyName;
	}


	public ConsumerApplicant getSsapApplicant() {
		return ssapApplicant;
	}

	public void setSsapApplicant(ConsumerApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}
	
}
