//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2013.03.12 at 12:03:43 PM IST
//


package com.getinsured.hix.dto.planmgmt.template.plan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CostShareVarianceVO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CostShareVarianceVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="planId" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="planMarketingName" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="metalLevel" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="csrVariationType" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="issuerActuarialValue" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="avCalculatorOutputNumber" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="medicalAndDrugDeductiblesIntegrated" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="medicalAndDrugMaxOutOfPocketIntegrated" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="defaultCopayInNetwork" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="defaultCopayOutOfNetwork" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="defaultCoInsuranceInNetwork" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="defaultCoInsuranceOutOfNetwork" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="multipleProviderTiers" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="firstTierUtilization" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="secondTierUtilization" type="{http://vo.ffe.cms.hhs.gov}ExcelCellVO"/>
 *         &lt;element name="sbc" type="{http://vo.ffe.cms.hhs.gov}SBCVO"/>
 *         &lt;element name="planDeductibleList" type="{http://vo.ffe.cms.hhs.gov}PlanDeductibleVO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="serviceVisitList" type="{http://vo.ffe.cms.hhs.gov}ServiceVisitVO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="moopList" type="{http://vo.ffe.cms.hhs.gov}MoopVO" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "planId",
    "planMarketingName",
    "metalLevel",
    "csrVariationType",
    "issuerActuarialValue",
    "avCalculatorOutputNumber",
    "medicalAndDrugDeductiblesIntegrated",
    "medicalAndDrugMaxOutOfPocketIntegrated",
    "defaultCopayInNetwork",
    "defaultCopayOutOfNetwork",
    "defaultCoInsuranceInNetwork",
    "defaultCoInsuranceOutOfNetwork",
    "multipleProviderTiers",
    "firstTierUtilization",
    "secondTierUtilization",
    "sbc",
    "planDeductibleList",
    "serviceVisitList",
    "moopList"
})
@XmlRootElement(name="costShareVarianceVO")
public class CostShareVarianceVO {

    @XmlElement(required = true)
    protected ExcelCellVO planId;
    @XmlElement(required = true)
    protected ExcelCellVO planMarketingName;
    @XmlElement(required = true)
    protected ExcelCellVO metalLevel;
    @XmlElement(required = true)
    protected ExcelCellVO csrVariationType;
    @XmlElement(required = true)
    protected ExcelCellVO issuerActuarialValue;
    @XmlElement(required = true)
    protected ExcelCellVO avCalculatorOutputNumber;
    @XmlElement(required = true)
    protected ExcelCellVO medicalAndDrugDeductiblesIntegrated;
    @XmlElement(required = true)
    protected ExcelCellVO medicalAndDrugMaxOutOfPocketIntegrated;
    @XmlElement(required = true)
    protected ExcelCellVO defaultCopayInNetwork;
    @XmlElement(required = true)
    protected ExcelCellVO defaultCopayOutOfNetwork;
    @XmlElement(required = true)
    protected ExcelCellVO defaultCoInsuranceInNetwork;
    @XmlElement(required = true)
    protected ExcelCellVO defaultCoInsuranceOutOfNetwork;
    @XmlElement(required = true)
    protected ExcelCellVO multipleProviderTiers;
    @XmlElement(required = true)
    protected ExcelCellVO firstTierUtilization;
    @XmlElement(required = true)
    protected ExcelCellVO secondTierUtilization;
    @XmlElement(required = true)
    protected SBCVO sbc;
    protected List<PlanDeductibleVO> planDeductibleList;
    protected List<ServiceVisitVO> serviceVisitList;
    @XmlElement(required = true)
    protected List<MoopVO> moopList;

    /**
     * Gets the value of the planId property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setPlanId(ExcelCellVO value) {
        this.planId = value;
    }

    /**
     * Gets the value of the planMarketingName property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getPlanMarketingName() {
        return planMarketingName;
    }

    /**
     * Sets the value of the planMarketingName property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setPlanMarketingName(ExcelCellVO value) {
        this.planMarketingName = value;
    }

    /**
     * Gets the value of the metalLevel property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getMetalLevel() {
        return metalLevel;
    }

    /**
     * Sets the value of the metalLevel property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setMetalLevel(ExcelCellVO value) {
        this.metalLevel = value;
    }

    /**
     * Gets the value of the csrVariationType property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getCsrVariationType() {
        return csrVariationType;
    }

    /**
     * Sets the value of the csrVariationType property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setCsrVariationType(ExcelCellVO value) {
        this.csrVariationType = value;
    }

    /**
     * Gets the value of the issuerActuarialValue property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getIssuerActuarialValue() {
        return issuerActuarialValue;
    }

    /**
     * Sets the value of the issuerActuarialValue property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setIssuerActuarialValue(ExcelCellVO value) {
        this.issuerActuarialValue = value;
    }

    /**
     * Gets the value of the avCalculatorOutputNumber property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getAvCalculatorOutputNumber() {
        return avCalculatorOutputNumber;
    }

    /**
     * Sets the value of the avCalculatorOutputNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setAvCalculatorOutputNumber(ExcelCellVO value) {
        this.avCalculatorOutputNumber = value;
    }

    /**
     * Gets the value of the medicalAndDrugDeductiblesIntegrated property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getMedicalAndDrugDeductiblesIntegrated() {
        return medicalAndDrugDeductiblesIntegrated;
    }

    /**
     * Sets the value of the medicalAndDrugDeductiblesIntegrated property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setMedicalAndDrugDeductiblesIntegrated(ExcelCellVO value) {
        this.medicalAndDrugDeductiblesIntegrated = value;
    }

    /**
     * Gets the value of the medicalAndDrugMaxOutOfPocketIntegrated property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getMedicalAndDrugMaxOutOfPocketIntegrated() {
        return medicalAndDrugMaxOutOfPocketIntegrated;
    }

    /**
     * Sets the value of the medicalAndDrugMaxOutOfPocketIntegrated property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setMedicalAndDrugMaxOutOfPocketIntegrated(ExcelCellVO value) {
        this.medicalAndDrugMaxOutOfPocketIntegrated = value;
    }

    /**
     * Gets the value of the defaultCopayInNetwork property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getDefaultCopayInNetwork() {
        return defaultCopayInNetwork;
    }

    /**
     * Sets the value of the defaultCopayInNetwork property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setDefaultCopayInNetwork(ExcelCellVO value) {
        this.defaultCopayInNetwork = value;
    }

    /**
     * Gets the value of the defaultCopayOutOfNetwork property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getDefaultCopayOutOfNetwork() {
        return defaultCopayOutOfNetwork;
    }

    /**
     * Sets the value of the defaultCopayOutOfNetwork property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setDefaultCopayOutOfNetwork(ExcelCellVO value) {
        this.defaultCopayOutOfNetwork = value;
    }

    /**
     * Gets the value of the defaultCoInsuranceInNetwork property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getDefaultCoInsuranceInNetwork() {
        return defaultCoInsuranceInNetwork;
    }

    /**
     * Sets the value of the defaultCoInsuranceInNetwork property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setDefaultCoInsuranceInNetwork(ExcelCellVO value) {
        this.defaultCoInsuranceInNetwork = value;
    }

    /**
     * Gets the value of the defaultCoInsuranceOutOfNetwork property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getDefaultCoInsuranceOutOfNetwork() {
        return defaultCoInsuranceOutOfNetwork;
    }

    /**
     * Sets the value of the defaultCoInsuranceOutOfNetwork property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setDefaultCoInsuranceOutOfNetwork(ExcelCellVO value) {
        this.defaultCoInsuranceOutOfNetwork = value;
    }

    /**
     * Gets the value of the multipleProviderTiers property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getMultipleProviderTiers() {
        return multipleProviderTiers;
    }

    /**
     * Sets the value of the multipleProviderTiers property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setMultipleProviderTiers(ExcelCellVO value) {
        this.multipleProviderTiers = value;
    }

    /**
     * Gets the value of the firstTierUtilization property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getFirstTierUtilization() {
        return firstTierUtilization;
    }

    /**
     * Sets the value of the firstTierUtilization property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setFirstTierUtilization(ExcelCellVO value) {
        this.firstTierUtilization = value;
    }

    /**
     * Gets the value of the secondTierUtilization property.
     *
     * @return
     *     possible object is
     *     {@link ExcelCellVO }
     *
     */
    public ExcelCellVO getSecondTierUtilization() {
        return secondTierUtilization;
    }

    /**
     * Sets the value of the secondTierUtilization property.
     *
     * @param value
     *     allowed object is
     *     {@link ExcelCellVO }
     *
     */
    public void setSecondTierUtilization(ExcelCellVO value) {
        this.secondTierUtilization = value;
    }

    /**
     * Gets the value of the sbc property.
     *
     * @return
     *     possible object is
     *     {@link SBCVO }
     *
     */
    public SBCVO getSbc() {
        return sbc;
    }

    /**
     * Sets the value of the sbc property.
     *
     * @param value
     *     allowed object is
     *     {@link SBCVO }
     *
     */
    public void setSbc(SBCVO value) {
        this.sbc = value;
    }

    /**
     * Gets the value of the planDeductibleList property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the planDeductibleList property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlanDeductibleList().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlanDeductibleVO }
     *
     *
     */
    public List<PlanDeductibleVO> getPlanDeductibleList() {
        if (planDeductibleList == null) {
            planDeductibleList = new ArrayList<PlanDeductibleVO>();
        }
        return this.planDeductibleList;
    }

    /**
     * Gets the value of the serviceVisitList property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceVisitList property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceVisitList().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceVisitVO }
     *
     *
     */
    public List<ServiceVisitVO> getServiceVisitList() {
        if (serviceVisitList == null) {
            serviceVisitList = new ArrayList<ServiceVisitVO>();
        }
        return this.serviceVisitList;
    }

    /**
     * Gets the value of the moopList property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moopList property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoopList().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoopVO }
     *
     *
     */
    public List<MoopVO> getMoopList() {
        if (moopList == null) {
            moopList = new ArrayList<MoopVO>();
        }
        return this.moopList;
    }

}
