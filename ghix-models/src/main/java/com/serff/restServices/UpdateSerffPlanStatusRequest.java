package com.serff.restServices;

//import com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.PlanStatus;

/**
 * This is UpdateSerffPlanStatusRequest object. Used to update plan Status in 
 * in SERFF by making REST call to GHIX-SERFF 
 * 
 * @author Geetha Chandran
 * @since 11/04/2013
 */
public class UpdateSerffPlanStatusRequest {
	
	private int issuerId;
	
	private String planId;
	
//	private PlanStatus planStatus;

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	/*public PlanStatus getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(PlanStatus planStatus) {
		this.planStatus = planStatus;
	}*/
}
