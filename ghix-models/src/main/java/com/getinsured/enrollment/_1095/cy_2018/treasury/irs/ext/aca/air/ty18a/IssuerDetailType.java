
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Issuer Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-04-09&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial Version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;All the elements associated with an Issuer on
 * 						exchange generated monthly report
 * 					&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for IssuerDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IssuerDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerEmployerEIN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TINTypeCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerTINValidateNameResponseDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerMailingAddress"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerContactName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerContactPhoneNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerContactFaxNum" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerContactEmailTxt" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerNameControl" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IssuerDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "issuerName",
    "issuerEmployerEIN",
    "tinTypeCd",
    "issuerTINValidateNameResponseDetail",
    "issuerMailingAddress",
    "issuerContactName",
    "issuerContactPhoneNum",
    "issuerContactFaxNum",
    "issuerContactEmailTxt",
    "issuerNameControl",
    "errorDetail"
})
public class IssuerDetailType {

    @XmlElement(name = "IssuerName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String issuerName;
    @XmlElement(name = "IssuerEmployerEIN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String issuerEmployerEIN;
    @XmlElement(name = "TINTypeCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String tinTypeCd;
    @XmlElement(name = "IssuerTINValidateNameResponseDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected TINValidateNameResponseDetailType issuerTINValidateNameResponseDetail;
    @XmlElement(name = "IssuerMailingAddress", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MailingAddressGrpType issuerMailingAddress;
    @XmlElement(name = "IssuerContactName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CompletePersonNameDetailType issuerContactName;
    @XmlElement(name = "IssuerContactPhoneNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String issuerContactPhoneNum;
    @XmlElement(name = "IssuerContactFaxNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String issuerContactFaxNum;
    @XmlElement(name = "IssuerContactEmailTxt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String issuerContactEmailTxt;
    @XmlElement(name = "IssuerNameControl", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String issuerNameControl;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the issuerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerName() {
        return issuerName;
    }

    /**
     * Sets the value of the issuerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerName(String value) {
        this.issuerName = value;
    }

    /**
     * Gets the value of the issuerEmployerEIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerEmployerEIN() {
        return issuerEmployerEIN;
    }

    /**
     * Sets the value of the issuerEmployerEIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerEmployerEIN(String value) {
        this.issuerEmployerEIN = value;
    }

    /**
     * Gets the value of the tinTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTINTypeCd() {
        return tinTypeCd;
    }

    /**
     * Sets the value of the tinTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTINTypeCd(String value) {
        this.tinTypeCd = value;
    }

    /**
     * Gets the value of the issuerTINValidateNameResponseDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public TINValidateNameResponseDetailType getIssuerTINValidateNameResponseDetail() {
        return issuerTINValidateNameResponseDetail;
    }

    /**
     * Sets the value of the issuerTINValidateNameResponseDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public void setIssuerTINValidateNameResponseDetail(TINValidateNameResponseDetailType value) {
        this.issuerTINValidateNameResponseDetail = value;
    }

    /**
     * Gets the value of the issuerMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public MailingAddressGrpType getIssuerMailingAddress() {
        return issuerMailingAddress;
    }

    /**
     * Sets the value of the issuerMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public void setIssuerMailingAddress(MailingAddressGrpType value) {
        this.issuerMailingAddress = value;
    }

    /**
     * Gets the value of the issuerContactName property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public CompletePersonNameDetailType getIssuerContactName() {
        return issuerContactName;
    }

    /**
     * Sets the value of the issuerContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public void setIssuerContactName(CompletePersonNameDetailType value) {
        this.issuerContactName = value;
    }

    /**
     * Gets the value of the issuerContactPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerContactPhoneNum() {
        return issuerContactPhoneNum;
    }

    /**
     * Sets the value of the issuerContactPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerContactPhoneNum(String value) {
        this.issuerContactPhoneNum = value;
    }

    /**
     * Gets the value of the issuerContactFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerContactFaxNum() {
        return issuerContactFaxNum;
    }

    /**
     * Sets the value of the issuerContactFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerContactFaxNum(String value) {
        this.issuerContactFaxNum = value;
    }

    /**
     * Gets the value of the issuerContactEmailTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerContactEmailTxt() {
        return issuerContactEmailTxt;
    }

    /**
     * Sets the value of the issuerContactEmailTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerContactEmailTxt(String value) {
        this.issuerContactEmailTxt = value;
    }

    /**
     * Gets the value of the issuerNameControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerNameControl() {
        return issuerNameControl;
    }

    /**
     * Sets the value of the issuerNameControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerNameControl(String value) {
        this.issuerNameControl = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
