package com.getinsured.eligibility.householdinfo.irs;

import java.util.List;


public class IRSHouseholdClientRequest {
	private Long householdCaseId;
	private String month;
	private String year;
	private String benefitEffectiveDate;
	private List<Long> ssapApplicationList;

	public Long getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(Long householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(String benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	public List<Long> getSsapApplicationList() {
		return ssapApplicationList;
	}

	public void setSsapApplicationList(List<Long> ssapApplicationList) {
		this.ssapApplicationList = ssapApplicationList;
	}

	@Override
	public String toString() {
		return "IRSHouseholdClientRequest [householdCaseId=" + householdCaseId
				+ ", month=" + month + ", year=" + year
				+ ", benefitEffectiveDate=" + benefitEffectiveDate
				+ ", ssapApplicationList=" + ssapApplicationList + "]";
	}
}
