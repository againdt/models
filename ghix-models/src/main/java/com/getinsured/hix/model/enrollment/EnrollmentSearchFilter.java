package com.getinsured.hix.model.enrollment;

public class EnrollmentSearchFilter {
	

	private String firstName;
	private String lastName;
	private String carrier;
	private String uniqueId;
	private String enrollmentStatus;
	private String lastUpdatedFrom;
	private String lastUpdatedTo;
	private String policyEffectiveFrom;
	private String policyEffectiveTo;
	private String state;
	private String servicedBy;
	private String servicedByName;
	public String getServicedByName() {
		return servicedByName;
	}
	public void setServicedByName(String servicedByName) {
		this.servicedByName = servicedByName;
	}
	private String policySubmitFrom;
	private String policySubmitTo;
	private String productType;
	private String exchangeType;
	private String partner;
	private String flowName;
	private String insuranceType;
	private String enrollmentID;
	private String policyNo;
	private String sortColumn;
	private boolean sortDirection;
	private int startRecord;
	private int pageSize;
	private String verificationEvent;
	private String renewal;
	private boolean enableMedicare;
	private String hicn;
	/**
	 * @return the renewal
	 */
	public String getRenewal() {
		return renewal;
	}
	/**
	 * @param renewal the renewal to set
	 */
	public void setRenewal(String renewal) {
		this.renewal = renewal;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getEnrollmentID() {
		return enrollmentID;
	}
	public void setEnrollmentID(String enrollmentID) {
		this.enrollmentID = enrollmentID;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getServicedBy() {
		return servicedBy;
	}
	public void setServicedBy(String servicedBy) {
		this.servicedBy = servicedBy;
	}
	public String getLastUpdatedFrom() {
		return lastUpdatedFrom;
	}
	public void setLastUpdatedFrom(String lastUpdatedFrom) {
		this.lastUpdatedFrom = lastUpdatedFrom;
	}
	public String getLastUpdatedTo() {
		return lastUpdatedTo;
	}
	public void setLastUpdatedTo(String lastUpdatedTo) {
		this.lastUpdatedTo = lastUpdatedTo;
	}
	public String getPolicyEffectiveFrom() {
		return policyEffectiveFrom;
	}
	public void setPolicyEffectiveFrom(String policyEffectiveFrom) {
		this.policyEffectiveFrom = policyEffectiveFrom;
	}
	public String getPolicyEffectiveTo() {
		return policyEffectiveTo;
	}
	public void setPolicyEffectiveTo(String policyEffectiveTo) {
		this.policyEffectiveTo = policyEffectiveTo;
	}
	public String getPolicySubmitFrom() {
		return policySubmitFrom;
	}
	public void setPolicySubmitFrom(String policySubmitFrom) {
		this.policySubmitFrom = policySubmitFrom;
	}
	public String getPolicySubmitTo() {
		return policySubmitTo;
	}
	public void setPolicySubmitTo(String policySubmitTo) {
		this.policySubmitTo = policySubmitTo;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public boolean isSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(boolean sortDirection) {
		this.sortDirection = sortDirection;
	}
	public int getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(int startRecord) {
		this.startRecord = startRecord;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getVerificationEvent() {
		return verificationEvent;
	}
	public void setVerificationEvent(String verificationEvent) {
		this.verificationEvent = verificationEvent;
	}

	public boolean isEnableMedicare() {
		return enableMedicare;
	}
	public void setEnableMedicare(boolean enableMedicare) {
		this.enableMedicare = enableMedicare;
	}
	public String getHicn() {
		return hicn;
	}
	public void setHicn(String hicn) {
		this.hicn = hicn;
	}
	@Override
	public String toString() {
		return "EnrollmentSearchFilter [firstName=" + firstName + ", lastName="
				+ lastName + ", carrier=" + carrier + ", uniqueId=" + uniqueId
				+ ", enrollmentStatus=" + enrollmentStatus
				+ ", lastUpdatedFrom=" + lastUpdatedFrom + ", lastUpdatedTo="
				+ lastUpdatedTo + ", policyEffectiveFrom="
				+ policyEffectiveFrom + ", policyEffectiveTo="
				+ policyEffectiveTo + ", state=" + state + ", servicedBy="
				+ servicedBy + ", policySubmitFrom=" + policySubmitFrom
				+ ", policySubmitTo=" + policySubmitTo + ", productType="
				+ productType + ", exchangeType=" + exchangeType + ", partner="
				+ partner + ", flowName=" + flowName + ", renewal="+renewal +", sortColumn="
				+ sortColumn + ", sortDirection=" + sortDirection
				+ ", startRecord=" + startRecord + ", pageSize=" + pageSize
				+ ", hicn=" + hicn
				+ "]";
	}
	
	
	

}
