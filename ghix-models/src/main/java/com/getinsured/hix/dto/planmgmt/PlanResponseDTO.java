package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Date;

public class PlanResponseDTO extends PlanDTO implements Serializable {
	//This DTO is being added for ManageQHP


	private Date lastUpdateTimestamp;

	private String networkType;
	private Date enrollmentAvailEffDate;

	private int stmPlanId;
	private int amePlanId;
	private int visionPlanId;
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PlanResponseDTO() {
		super();
	}




	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}


	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}



	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	
	public int getStmPlanId() {
		return stmPlanId;
	}

	public void setStmPlanId(int stmPlanId) {
		this.stmPlanId = stmPlanId;
	}

	public int getAmePlanId() {
		return amePlanId;
	}

	public void setAmePlanId(int amePlanId) {
		this.amePlanId = amePlanId;
	}

	public int getVisionPlanId() {
		return visionPlanId;
	}

	public void setVisionPlanId(int visionPlanId) {
		this.visionPlanId = visionPlanId;
	}




	public Date getEnrollmentAvailEffDate() {
		return enrollmentAvailEffDate;
	}




	public void setEnrollmentAvailEffDate(Date enrollmentAvailEffDate) {
		this.enrollmentAvailEffDate = enrollmentAvailEffDate;
	}
	
	
	
	
	
	

}
