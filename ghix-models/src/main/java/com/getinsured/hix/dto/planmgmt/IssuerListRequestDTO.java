/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

/**
 * @author Krishna Gajulapalli
 *
 */
public class IssuerListRequestDTO implements Serializable{

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Integer> issuerIds;

	/**
	 * @return the issuerIds
	 */
	public List<Integer> getIssuerIds() {
		return issuerIds;
	}

	/**
	 * @param issuerIds the issuerIds to set
	 */
	public void setIssuerIds(List<Integer> issuerIds) {
		this.issuerIds = issuerIds;
	}

	

}
