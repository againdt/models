package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;
import java.util.Date;

import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.ApplicationStatus;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.ExpenseEstimate;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.YesOrNo;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public class PdMtCartItem {
	
	private String id;
	private ApplicationStatus status;
	private String planId;
	private String planName;
	private String carrierName;
	private InsuranceType insuranceType;
	private Float grossPremium;
	private Float aptc;
	private CostSharing csr;
	private Float netPremium;
	private String coverageStartDate;
	private ExchangeType exchangeType;
	private String primaryCareVisit;
	private String genericDrug;
	private Integer deductible;
	private ExpenseEstimate expenseEstimate;
	private PlanLevel metalTier;
	private String productType;
	private YesOrNo hsaCompatible;
	private Map<String,String> preferences;
	private List<PdMtCartItemMember> memberList;
	private String createdTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ApplicationStatus getStatus() {
		return status;
	}
	public void setStatus(ApplicationStatus status) {
		this.status = status;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	public Float getGrossPremium() {
		return grossPremium;
	}
	public void setGrossPremium(Float grossPremium) {
		this.grossPremium = grossPremium;
	}
	public Float getAptc() {
		return aptc;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public CostSharing getCsr() {
		return csr;
	}
	public void setCsr(CostSharing csr) {
		this.csr = csr;
	}
	public Float getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public ExchangeType getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}
	public String getPrimaryCareVisit() {
		return primaryCareVisit;
	}
	public void setPrimaryCareVisit(String primaryCareVisit) {
		this.primaryCareVisit = primaryCareVisit;
	}
	public String getGenericDrug() {
		return genericDrug;
	}
	public void setGenericDrug(String genericDrug) {
		this.genericDrug = genericDrug;
	}
	public Integer getDeductible() {
		return deductible;
	}
	public void setDeductible(Integer deductible) {
		this.deductible = deductible;
	}
	public ExpenseEstimate getExpenseEstimate() {
		return expenseEstimate;
	}
	public void setExpenseEstimate(ExpenseEstimate expenseEstimate) {
		this.expenseEstimate = expenseEstimate;
	}
	public PlanLevel getMetalTier() {
		return metalTier;
	}
	public void setMetalTier(PlanLevel metalTier) {
		this.metalTier = metalTier;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public YesOrNo getHsaCompatible() {
		return hsaCompatible;
	}
	public void setHsaCompatible(YesOrNo hsaCompatible) {
		this.hsaCompatible = hsaCompatible;
	}
	public Map<String, String> getPreferences() {
		return preferences;
	}
	public void setPreferences(Map<String, String> preferences) {
		this.preferences = preferences;
	}
	public List<PdMtCartItemMember> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<PdMtCartItemMember> memberList) {
		this.memberList = memberList;
	}
	
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	
}
