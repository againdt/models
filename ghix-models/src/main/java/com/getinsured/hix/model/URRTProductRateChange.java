package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "pm_urrt_prod_rate_change")
@XmlAccessorType(XmlAccessType.NONE)
public class URRTProductRateChange implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
		@XmlElement(name = "id")
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_urrt_prod_rate_change_seq")
		@SequenceGenerator(name = "pm_urrt_prod_rate_change_seq", sequenceName = "pm_urrt_prod_rate_change_seq", allocationSize = 1)
		private int id;
	
		//foreign key.
		@ManyToOne(cascade = CascadeType.ALL)
		@JoinColumn(name = "pm_urrt_id")
		private UnifiedRate unifiedRate;
	    		
		@NotAudited
		@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
		@OneToMany(mappedBy = "unifiedProductRateChange", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		private List<URRTInsurancePlanRateChange> plaRateChangeList;
		
		@Column(name = "hios_prod_id") 
		private String hiosInsuranceProductIdentifier;
		
		@Column(name = "prod_name") 
		private String productName;
		
		@Column(name = "waprc_pcnt_qty") 
		private int weightedAverageProductRateChangePercentQuantity;
		
		@Column(name = "rc_pcnt_qty_year0") 
		private double rateChangePercentQuantityYear0;
		
		@Column(name = "rc_pcnt_qty_year1") 
		private double rateChangePercentQuantityYear1;
		
		@Column(name = "rc_pcnt_qty_year2") 
		private double rateChangePercentQuantityYear2;
		
		@Column(name = "is_obsolete") 
		private String isObsolete;
		
		@Temporal(value = TemporalType.TIMESTAMP)
	 	@Column(name = "created_timestamp", nullable = false)
	 	private Date creationTimestamp;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		
		public UnifiedRate getUnifiedRate() {
			return unifiedRate;
		}

		public void setUnifiedRate(UnifiedRate unifiedRate) {
			this.unifiedRate = unifiedRate;
		}

		public String getHiosInsuranceProductIdentifier() {
			return hiosInsuranceProductIdentifier;
		}

		public void setHiosInsuranceProductIdentifier(
				String hiosInsuranceProductIdentifier) {
			this.hiosInsuranceProductIdentifier = hiosInsuranceProductIdentifier;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public int getWeightedAverageProductRateChangePercentQuantity() {
			return weightedAverageProductRateChangePercentQuantity;
		}

		public void setWeightedAverageProductRateChangePercentQuantity(
				int weightedAverageProductRateChangePercentQuantity) {
			this.weightedAverageProductRateChangePercentQuantity = weightedAverageProductRateChangePercentQuantity;
		}

		public double getRateChangePercentQuantityYear0() {
			return rateChangePercentQuantityYear0;
		}

		public void setRateChangePercentQuantityYear0(
				double rateChangePercentQuantityYear0) {
			this.rateChangePercentQuantityYear0 = rateChangePercentQuantityYear0;
		}

		public double getRateChangePercentQuantityYear1() {
			return rateChangePercentQuantityYear1;
		}

		public void setRateChangePercentQuantityYear1(
				double rateChangePercentQuantityYear1) {
			this.rateChangePercentQuantityYear1 = rateChangePercentQuantityYear1;
		}

		public double getRateChangePercentQuantityYear2() {
			return rateChangePercentQuantityYear2;
		}

		public void setRateChangePercentQuantityYear2(
				double rateChangePercentQuantityYear2) {
			this.rateChangePercentQuantityYear2 = rateChangePercentQuantityYear2;
		}

		public String getIsObsolete() {
			return isObsolete;
		}

		public void setIsObsolete(String isObsolete) {
			this.isObsolete = isObsolete;
		}

		public Date getCreationTimestamp() {
			return creationTimestamp;
		}

		public void setCreationTimestamp(Date creationTimestamp) {
			this.creationTimestamp = creationTimestamp;
		}

		public List<URRTInsurancePlanRateChange> getPlaRateChangeList() {
			return plaRateChangeList;
		}

		public void setPlaRateChangeList(
				List<URRTInsurancePlanRateChange> plaRateChangeList) {
			this.plaRateChangeList = plaRateChangeList;
		}

		
}
		
		
		
		
		
