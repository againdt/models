package com.getinsured.hix.dto.cap.liveops;

public class ReturnObject {
	String id;
    String requestedCallTime;
    String campaignId;
    String timezone; 
    String expires;
    String createdTime;
    String embargoUntil;
    String params;
    String phonenum;
    String outboundBatchId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRequestedCallTime() {
		return requestedCallTime;
	}
	public void setRequestedCallTime(String requestedCallTime) {
		this.requestedCallTime = requestedCallTime;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getExpires() {
		return expires;
	}
	public void setExpires(String expires) {
		this.expires = expires;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getEmbargoUntil() {
		return embargoUntil;
	}
	public void setEmbargoUntil(String embargoUntil) {
		this.embargoUntil = embargoUntil;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getPhonenum() {
		return phonenum;
	}
	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}
	public String getOutboundBatchId() {
		return outboundBatchId;
	}
	public void setOutboundBatchId(String outboundBatchId) {
		this.outboundBatchId = outboundBatchId;
	}
	

}

