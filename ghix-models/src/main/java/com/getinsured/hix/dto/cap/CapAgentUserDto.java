package com.getinsured.hix.dto.cap;

/*
 * Created by kaul_s on 2/26/15.
 */
public class CapAgentUserDto {

	
	String firstName;
	String lastName;
	String fullName;
	String phoneNumber;
	String email;
	int userId;
	String NPN;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNPN() {
		return NPN;
	}

	public void setNPN(String nPN) {
		NPN = nPN;
	}

}
