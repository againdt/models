package com.getinsured.hix.dto.agency;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class AgencySiteDto {
	private String id;
	@NotBlank
	private String agencyId;
	private String certificationStatus;
	@NotBlank
	@Size(max=50)
	private String siteLocationName;
	@Email
	private String emailAddress;
	@Size(max=10)
	@Pattern(regexp="^[0-9]{10}|^$")
	private String phoneNumber;
	@NotNull
	@Valid
	private AgencyLocationDto location;
	@NotNull
	@Valid
	@NotEmpty
	private List<AgencySiteHourDto> siteLocationHours;
	@NotBlank
	String siteType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAgencyId() {
		return agencyId;
	}
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}
	public String getSiteLocationName() {
		return siteLocationName;
	}
	public void setSiteLocationName(String siteLocationName) {
		this.siteLocationName = siteLocationName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public AgencyLocationDto getLocation() {
		return location;
	}
	public void setLocation(AgencyLocationDto location) {
		this.location = location;
	}
	public List<AgencySiteHourDto> getSiteLocationHours() {
		return siteLocationHours;
	}
	public void setSiteLocationHours(List<AgencySiteHourDto> siteLocationHours) {
		this.siteLocationHours = siteLocationHours;
	}
	public String getSiteType() {
		return siteType;
	}
	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}
	public String getCertificationStatus() {
		return certificationStatus;
	}
	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	
}
