package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.EmployeeApplication.TypeOfEmployee;


/**
 * The persistent class for the employers database table.
 * 
 */
@Audited
@Entity
@Table(name="employees")
@org.hibernate.annotations.DynamicInsert
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum EmployeeBooleanFlag { YES, NO;	}
	
	public enum Status 	{  TERMINATED,  NOT_ELIGIBLE, DELETED, ELIGIBLE,ACTIVE,INACTIVE; 	}
	
	public interface EmployeeInfoGroup extends Default {
	
	}
	
    public interface EditDependentsGroup extends Default{
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Employee_Seq")
	@SequenceGenerator(name = "Employee_Seq", sequenceName = "employees_seq", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name="employer_id")
	private Employer employer;

	@Valid
	@NotNull(message="Please select worksite.",groups=Employee.EmployeeInfoGroup.class)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name="employer_location_id")
	private EmployerLocation employerLocation;
	
	@Column(name="name")
	private String name;

	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name="status_notes")
	private String statusNotes;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="employment_date")
	private Date employmentDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="termination_date")
	private Date terminationDate;

	@Column(name="is_married")
	@Enumerated(EnumType.STRING)
	private EmployeeBooleanFlag isMarried;

	@Column(name="child_count")
	@Min(value=0, message="{label.validatechild}",groups=Employee.EmployeeInfoGroup.class)
	@Max(value=99, message="{label.validatechild}",groups=Employee.EmployeeInfoGroup.class)
	private int childCount;
	
	@Column(name="dependent_count")
	private int dependentCount;

	@Column(name="esign_by")
	private String esignBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="esign_date")
	private Date esignDate;
	
	@Column(name = "last_visited")
	private String lastVisited;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date created;

	@NotAudited
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp")
	private Date updated;
	
	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;

	// bi-directional one-to-many association to EmployeeDetails
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)	
	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy("dob ASC")
	@Valid
	@NotNull
	private List<EmployeeDetails> employeeDetails;

	@NotAudited
	@Column(name="ELIG_LEAD_ID")
	private Long leadId;
	

	public Employee() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public EmployerLocation getEmployerLocation() {
		return employerLocation;
	}

	public void setEmployerLocation(EmployerLocation employerLocation) {
		this.employerLocation = employerLocation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getStatusNotes() {
		return statusNotes;
	}

	public void setStatusNotes(String statusNotes) {
		this.statusNotes = statusNotes;
	}

	public Date getEmploymentDate() {
		return employmentDate;
	}

	public void setEmploymentDate(Date employmentDate) {
		this.employmentDate = employmentDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public EmployeeBooleanFlag getIsMarried() {
		return isMarried;
	}

	public void setIsMarried(EmployeeBooleanFlag isMarried) {
		this.isMarried = isMarried;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getDependentCount() {
		return dependentCount;
	}

	public void setDependentCount(int dependentCount) {
		this.dependentCount = dependentCount;
	}

	public String getEsignBy() {
		return esignBy;
	}

	public void setEsignBy(String esignBy) {
		this.esignBy = esignBy;
	}
	
	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}
	
	public String getLastVisited() {
		return lastVisited;
	}

	public void setLastVisited(String lastVisited) {
		this.lastVisited = lastVisited;
	}

	public List<EmployeeDetails> getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(List<EmployeeDetails> employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate());
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

}
