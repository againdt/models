create function change_seq(tabname text, tabseqname text) returns integer as $$
DECLARE
    v bigint :=1;
BEGIN
      EXECUTE 'select max(id) from '||tabname INTO v;
	  v:=v+1000;
      EXECUTE format('alter sequence '||tabseqname||' increment by '||v);
      EXECUTE 'select nextval('||''''||tabseqname||''''||')';	 
      EXECUTE 'alter sequence '||tabseqname||' increment by 1'; 
	  EXECUTE 'select nextval(' ||''''||tabseqname||''''||')' INTO v;
	  return v;
END;
$$ LANGUAGE plpgsql;