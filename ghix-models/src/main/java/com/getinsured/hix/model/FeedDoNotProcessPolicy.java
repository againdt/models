package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.*;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="FEED_DO_NOT_PROCESS_POLICY")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class FeedDoNotProcessPolicy {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FEED_DO_NOT_PROCESS_POLICY_SEQ")
	@SequenceGenerator(name = "FEED_DO_NOT_PROCESS_POLICY_SEQ", sequenceName = "FEED_DO_NOT_PROCESS_POLICY_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "CARRIER_NAME", length = 1000)
	private String carrierName;

    @Column(name="POLICY_ID")
	private String policyId;

    @OneToOne(targetEntity = LookupValue.class, optional=true)
	@JoinColumn(name = "PRODUCT_TYPE_LKP_ID", referencedColumnName = "lookup_value_id")
	private LookupValue productTypeLkp;

	@OneToOne(targetEntity = AccountUser.class, optional=true)
	@JoinColumn(name = "ADDED_BY", referencedColumnName = "ID", insertable = false, updatable = false)
	private AccountUser addedByUser;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "ADDED_TIMESTAMP", nullable = false)
	private Date addedTimestamp;

	@Column(name="ADDED_BY")
	private Integer addedByUserId;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setAddedTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the carrierName
	 */
	public String getCarrierName() {
		return carrierName;
	}

	/**
	 * @param carrierName the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	/**
	 * @return the policyId
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return the productTypeLkp
	 */
	public LookupValue getProductTypeLkp() {
		return productTypeLkp;
	}

	/**
	 * @param productTypeLkp the productTypeLkp to set
	 */
	public void setProductTypeLkp(LookupValue productTypeLkp) {
		this.productTypeLkp = productTypeLkp;
	}

	/**
	 * @return the addedByUser
	 */
	public AccountUser getAddedByUser() {
		return addedByUser;
	}

	/**
	 * @param addedByUser the addedByUser to set
	 */
	public void setAddedByUser(AccountUser addedByUser) {
		this.addedByUser = addedByUser;
	}

	/**
	 * @return the addedTimestamp
	 */
	public Date getAddedTimestamp() {
		return addedTimestamp;
	}

	/**
	 * @param addedTimestamp the addedTimestamp to set
	 */
	public void setAddedTimestamp(Date addedTimestamp) {
		this.addedTimestamp = addedTimestamp;
	}

	/**
	 * @return the addedByUserId
	 */
	public Integer getAddedByUserId() {
		return addedByUserId;
	}

	/**
	 * @param addedByUserId the addedByUserId to set
	 */
	public void setAddedByUserId(Integer addedByUserId) {
		this.addedByUserId = addedByUserId;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
}
