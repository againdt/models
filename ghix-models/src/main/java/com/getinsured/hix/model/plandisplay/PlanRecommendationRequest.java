package com.getinsured.hix.model.plandisplay;

import java.util.List;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public class PlanRecommendationRequest {
	private Float maxSubsidy;
	private PlanDisplayEnum.CostSharing costSharing;
	private String coverageStartDate;
	private String zipCode;
	private String countyCode;
	private InsuranceType insuranceType;
	private ExchangeType exchangeType;
	
	private PdPreferencesDTO pdPreferencesDTO;
	
	private List<PdMtCartItemMember> pdMtCartItemMemberList;
	
	private boolean minimizePlanData;
	private boolean showCatastrophicPlan;
	private String currentPlanId;
	private String tenantCode;
	
	public Float getMaxSubsidy() {
		return maxSubsidy;
	}
	public void setMaxSubsidy(Float maxSubsidy) {
		this.maxSubsidy = maxSubsidy;
	}
	public PlanDisplayEnum.CostSharing getCostSharing() {
		return costSharing;
	}
	public void setCostSharing(PlanDisplayEnum.CostSharing costSharing) {
		this.costSharing = costSharing;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	public ExchangeType getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}
	public List<PdMtCartItemMember> getPdMtCartItemMemberList() {
		return pdMtCartItemMemberList;
	}
	public void setPdMtCartItemMemberList(
			List<PdMtCartItemMember> pdMtCartItemMemberList) {
		this.pdMtCartItemMemberList = pdMtCartItemMemberList;
	}
	public boolean isMinimizePlanData() {
		return minimizePlanData;
	}
	public void setMinimizePlanData(boolean minimizePlanData) {
		this.minimizePlanData = minimizePlanData;
	}
	public boolean isShowCatastrophicPlan() {
		return showCatastrophicPlan;
	}
	public void setShowCatastrophicPlan(boolean showCatastrophicPlan) {
		this.showCatastrophicPlan = showCatastrophicPlan;
	}
	public String getCurrentPlanId() {
		return currentPlanId;
	}
	public void setCurrentPlanId(String currentPlanId) {
		this.currentPlanId = currentPlanId;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	
}
