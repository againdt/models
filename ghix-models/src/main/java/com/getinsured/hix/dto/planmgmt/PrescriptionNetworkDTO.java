/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

/**
 * @author Krishna Gajulapalli
 *
 */
public class PrescriptionNetworkDTO {
	private int id;
	private String state;
	private String hiosIssuerId;
	private String formularyId;
	private String formularyUrl;
	private String issuerName;
	private int applicableYear;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the hIOSIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	/**
	 * @param hIOSIssuerId
	 *            the hIOSIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	/**
	 * @return the formularyId
	 */
	public String getFormularyId() {
		return formularyId;
	}

	/**
	 * @param formularyId
	 *            the formularyId to set
	 */
	public void setFormularyId(String formularyId) {
		this.formularyId = formularyId;
	}

	/**
	 * @return the formularyUrl
	 */
	public String getFormularyUrl() {
		return formularyUrl;
	}

	/**
	 * @param formularyUrl
	 *            the formularyUrl to set
	 */
	public void setFormularyUrl(String formularyUrl) {
		this.formularyUrl = formularyUrl;
	}

	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * @param issuerName
	 *            the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	/**
	 * @return the applicableYear
	 */
	public int getApplicableYear() {
		return applicableYear;
	}

	/**
	 * @param applicableYear the applicableYear to set
	 */
	public void setApplicableYear(int applicableYear) {
		this.applicableYear = applicableYear;
	}

	
}
