
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Withdrawn-Assister Request' WHERE LOOKUP_VALUE_LABEL = 'Withdrawn - Assister Request';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Denied-Not Eligible For Training' WHERE LOOKUP_VALUE_LABEL = 'Denied - Not Eligible For Training';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Denied-Did Not Complete Required Training' WHERE LOOKUP_VALUE_LABEL = 'Denied - Did Not Complete Required Training';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Denied-Background Check' WHERE LOOKUP_VALUE_LABEL = 'Denied - Background Check';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Denied-Conflict of Interest' WHERE LOOKUP_VALUE_LABEL = 'Denied - Conflict of Interest';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Denied-Unable to Satisfy Program Requirements' WHERE LOOKUP_VALUE_LABEL = 'Denied - Unable to Satisfy Program Requirements';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Decertified-Misconduct' WHERE LOOKUP_VALUE_LABEL = 'Decertified - Misconduct';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Decertified-Assister Request' WHERE LOOKUP_VALUE_LABEL = 'Decertified - Assister Request';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Entity Deregistered-Failed to Renew' WHERE LOOKUP_VALUE_LABEL = 'Entity Deregistered-� Failed to Renew';
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_LABEL = 'Entity Deregistered-Misconduct' WHERE LOOKUP_VALUE_LABEL = 'Entity Deregistered - Misconduct';
