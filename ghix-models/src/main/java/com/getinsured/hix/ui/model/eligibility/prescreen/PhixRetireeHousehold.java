package com.getinsured.hix.ui.model.eligibility.prescreen;

import java.util.List;

import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * This class encapsulates the household member information  for a Retiree type .
 * 
 * 
 * 
 * @author Suresh Kancherla
 * @since Sep 16, 2016
 * 
 */
public class PhixRetireeHousehold {
	

	private List<PhixRetireeMember> members;

	private String HraContributionType="";
	
	private String HraContributionAmount="";
	
	public PhixRetireeHousehold(){
		
	}

	public List<PhixRetireeMember> getMembers() {
		return members;
	}

	public void setMembers(List<PhixRetireeMember> members) {
		this.members = members;
	}

	public String getHraContributionType() {
		return HraContributionType;
	}

	public void setHraContributionType(String hraContributionType) {
		HraContributionType = hraContributionType;
	}

	public String getHraContributionAmount() {
		return HraContributionAmount;
	}

	public void setHraContributionAmount(String hraContributionAmount) {
		HraContributionAmount = hraContributionAmount;
	}
	
}
