package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class SpecialEnrollment implements Serializable{
	private static final long serialVersionUID = 1L;
	private String qualifyingLifeEvent;
	private String eventDate;
	public String getQualifyingLifeEvent() {
		return qualifyingLifeEvent;
	}
	public void setQualifyingLifeEvent(String qualifyingLifeEvent) {
		this.qualifyingLifeEvent = qualifyingLifeEvent;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	public static SpecialEnrollment getSampleInstance(){
		SpecialEnrollment sep = new SpecialEnrollment();
		sep.setEventDate("07/08/2015");
		sep.setQualifyingLifeEvent("OTHER");
		return sep;
	}

}
