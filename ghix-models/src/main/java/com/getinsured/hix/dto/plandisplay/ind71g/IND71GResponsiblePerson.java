package com.getinsured.hix.dto.plandisplay.ind71g;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class IND71GResponsiblePerson {
	
	@Size(min=IND71Enums.RESPONSIBLE_PERSON_MIN_LENGTH, max=IND71Enums.RESPONSIBLE_PERSON_MAX_LENGTH,message="responsiblePersonId" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.RESPONSIBLE_PERSON_REGEX,message="responsiblePersonId" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonId;

    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="responsiblePersonFirstName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="responsiblePersonFirstName" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonFirstName;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="responsiblePersonMiddleName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="responsiblePersonMiddleName" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonMiddleName;

    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="responsiblePersonLastName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="responsiblePersonLastName" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonLastName;
    
    @Size(min=IND71Enums.SUFFIX_MIN_LENGTH, max=IND71Enums.SUFFIX_MAX_LENGTH,message="responsiblePersonSuffix" + IND71Enums.LENGTH_ERROR )
    private String responsiblePersonSuffix;

    @Size(min=IND71Enums.SSN_MIN_LENGTH, max=IND71Enums.SSN_MAX_LENGTH,message="responsiblePersonSsn" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.SSN_REGEX,message="responsiblePersonSsn" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonSsn;
    
    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="responsiblePersonPrimaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="responsiblePersonPrimaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonPrimaryPhone;
    
    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="responsiblePersonSecondaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="responsiblePersonSecondaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonSecondaryPhone;
    
    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="responsiblePersonPreferredPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="responsiblePersonPreferredPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonPreferredPhone;
    
    @Pattern(regexp=IND71Enums.EMAIL_REGEX,message="responsiblePersonPreferredEmail" + IND71Enums.INVALID_EMAIL_ERROR)
    private String responsiblePersonPreferredEmail;
    
    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="responsiblePersonHomeAddress1" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="responsiblePersonHomeAddress1" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonHomeAddress1;
    
    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="responsiblePersonHomeAddress2" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="responsiblePersonHomeAddress2" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonHomeAddress2;
    
    @Size(min=IND71Enums.CITY_MIN_LENGTH, max=IND71Enums.CITY_MAX_LENGTH,message="responsiblePersonHomeCity" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.CITY_REGEX,message="responsiblePersonHomeCity" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonHomeCity;
    
    @Size(min=IND71Enums.STATE_MIN_LENGTH, max=IND71Enums.STATE_MAX_LENGTH,message="responsiblePersonHomeState" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.STATE_REGEX,message="responsiblePersonHomeState" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonHomeState;
    
    @Size(min=IND71Enums.ZIP_MIN_LENGTH, max=IND71Enums.ZIP_MAX_LENGTH,message="responsiblePersonHomeZip" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ZIP_REGEX,message="responsiblePersonHomeZip" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonHomeZip;
    
    @Pattern(regexp=IND71Enums.RESPONSIBLE_PERSON_TYPE,message="responsiblePersonType" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonType;

    public String getResponsiblePersonId() {
        return responsiblePersonId;
    }

    public void setResponsiblePersonId(String responsiblePersonId) {
        this.responsiblePersonId = responsiblePersonId;
    }

    public String getResponsiblePersonFirstName() {
        return responsiblePersonFirstName;
    }

    public void setResponsiblePersonFirstName(String responsiblePersonFirstName) {
        this.responsiblePersonFirstName = responsiblePersonFirstName;
    }

    public String getResponsiblePersonMiddleName() {
        return responsiblePersonMiddleName;
    }

    public void setResponsiblePersonMiddleName(String responsiblePersonMiddleName) {
        this.responsiblePersonMiddleName = responsiblePersonMiddleName;
    }

    public String getResponsiblePersonLastName() {
        return responsiblePersonLastName;
    }

    public void setResponsiblePersonLastName(String responsiblePersonLastName) {
        this.responsiblePersonLastName = responsiblePersonLastName;
    }

    public String getResponsiblePersonSuffix() {
        return responsiblePersonSuffix;
    }

    public void setResponsiblePersonSuffix(String responsiblePersonSuffix) {
        this.responsiblePersonSuffix = responsiblePersonSuffix;
    }

    public String getResponsiblePersonSsn() {
        return responsiblePersonSsn;
    }

    public void setResponsiblePersonSsn(String responsiblePersonSsn) {
        this.responsiblePersonSsn = responsiblePersonSsn;
    }

    public String getResponsiblePersonPrimaryPhone() {
        return responsiblePersonPrimaryPhone;
    }

    public void setResponsiblePersonPrimaryPhone(String responsiblePersonPrimaryPhone) {
        this.responsiblePersonPrimaryPhone = responsiblePersonPrimaryPhone;
    }

    public String getResponsiblePersonSecondaryPhone() {
        return responsiblePersonSecondaryPhone;
    }

    public void setResponsiblePersonSecondaryPhone(String responsiblePersonSecondaryPhone) {
        this.responsiblePersonSecondaryPhone = responsiblePersonSecondaryPhone;
    }

    public String getResponsiblePersonPreferredPhone() {
        return responsiblePersonPreferredPhone;
    }

    public void setResponsiblePersonPreferredPhone(String responsiblePersonPreferredPhone) {
        this.responsiblePersonPreferredPhone = responsiblePersonPreferredPhone;
    }

    public String getResponsiblePersonPreferredEmail() {
        return responsiblePersonPreferredEmail;
    }

    public void setResponsiblePersonPreferredEmail(String responsiblePersonPreferredEmail) {
        this.responsiblePersonPreferredEmail = responsiblePersonPreferredEmail;
    }

    public String getResponsiblePersonHomeAddress1() {
        return responsiblePersonHomeAddress1;
    }

    public void setResponsiblePersonHomeAddress1(String responsiblePersonHomeAddress1) {
        this.responsiblePersonHomeAddress1 = responsiblePersonHomeAddress1;
    }

    public String getResponsiblePersonHomeAddress2() {
        return responsiblePersonHomeAddress2;
    }

    public void setResponsiblePersonHomeAddress2(String responsiblePersonHomeAddress2) {
        this.responsiblePersonHomeAddress2 = responsiblePersonHomeAddress2;
    }

    public String getResponsiblePersonHomeCity() {
        return responsiblePersonHomeCity;
    }

    public void setResponsiblePersonHomeCity(String responsiblePersonHomeCity) {
        this.responsiblePersonHomeCity = responsiblePersonHomeCity;
    }

    public String getResponsiblePersonHomeState() {
        return responsiblePersonHomeState;
    }

    public void setResponsiblePersonHomeState(String responsiblePersonHomeState) {
        this.responsiblePersonHomeState = responsiblePersonHomeState;
    }

    public String getResponsiblePersonHomeZip() {
        return responsiblePersonHomeZip;
    }

    public void setResponsiblePersonHomeZip(String responsiblePersonHomeZip) {
        this.responsiblePersonHomeZip = responsiblePersonHomeZip;
    }

    public String getResponsiblePersonType() {
        return responsiblePersonType;
    }

    public void setResponsiblePersonType(String responsiblePersonType) {
        this.responsiblePersonType = responsiblePersonType;
    }
}
