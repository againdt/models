package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.PlanHealthCost;

public class PlanHealthCostDTO extends PlanHealthCost {
	private static final long serialVersionUID = 1L;
	
	private int planHealthId;
	
	private int planId;

	

	public int getPlanHealthId() {
		return planHealthId;
	}

	public void setPlanHealthId(int planHealthId) {
		this.planHealthId = planHealthId;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}
}
