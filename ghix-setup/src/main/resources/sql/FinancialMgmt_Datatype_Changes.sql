
delete from exchange_fees;
delete from issuer_payment_detail;
delete from issuer_payments;
delete from issuer_payment_invoice;
delete from issuer_invoices_lineitems;
delete from issuer_invoices;
delete from employer_payments;
delete from employer_payment_invoice;
delete from employer_invoices_lineitems;
delete from employer_notification;
delete from employer_invoices;

commit;

alter table EMPLOYER_INVOICES modify (AMOUNT_DUE_FROM_LAST_INVOICE number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES modify (TOTAL_PAYMENT_RECEIVED number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES modify (PREMIUMS_THIS_PERIOD number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES modify (ADJUSTMENTS number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES modify (EXCHANGE_FEES number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES modify (TOTAL_AMOUNT_DUE number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES modify (AMOUNT_ENCLOSED number(20,2) DEFAULT 0);

alter table EMPLOYER_INVOICES_LINEITEMS modify (TOTAL_PREMIUM number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES_LINEITEMS modify (EMPLOYEE_CONTRIBUTION number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES_LINEITEMS modify (RETRO_ADJUSTMENTS number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES_LINEITEMS modify (NET_AMOUNT number(20,2) DEFAULT 0);
alter table EMPLOYER_INVOICES_LINEITEMS modify (PAYMENT_RECEIVED number(20,2) DEFAULT 0);

alter table EMPLOYER_PAYMENTS modify (AMOUNT_DUE_FROM_LAST_INVOICE number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (TOTAL_PAYMENT_RECEIVED number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (ADJUSTMENTS number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (EXCHANGE_FEES number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (TOTAL_AMOUNT_DUE number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (AMOUNT_ENCLOSED number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (REFUND_AMT number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENTS modify (PREMIUMS_THIS_PERIOD number(20,2) DEFAULT 0);

alter table EMPLOYER_PAYMENT_INVOICE modify (AMOUNT number(20,2) DEFAULT 0);
alter table EMPLOYER_PAYMENT_INVOICE modify (EXCESS_AMOUNT number(20,2) DEFAULT 0);

alter table ISSUER_INVOICES modify (AMOUNT_DUE_FROM_LAST_INVOICE number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (TOTAL_PAYMENT_PAID number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (PREMIUMS_THIS_PERIOD number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (EXCHANGE_FEES number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (ADJUSTMENTS number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (TOTAL_AMOUNT_DUE number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (AMOUNT_ENCLOSED number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES modify (AMOUNT_RECEIVED_FROM_EMPLOYER number(20,2) DEFAULT 0);

alter table ISSUER_INVOICES_LINEITEMS modify (TOTAL_PREMIUM number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES_LINEITEMS modify (NET_AMOUNT number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES_LINEITEMS modify (AMOUNT_RECEIVED_FROM_EMPLOYER number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES_LINEITEMS modify (RETRO_ADJUSTMENTS number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES_LINEITEMS modify (USER_FEE number(20,2) DEFAULT 0);
alter table ISSUER_INVOICES_LINEITEMS modify (AMOUNT_PAID_TO_ISSUER number(20,2) DEFAULT 0);

alter table ISSUER_PAYMENTS modify (AMOUNT_DUE_FROM_LAST_INVOICE number(20,2) DEFAULT 0);
alter table ISSUER_PAYMENTS modify (TOTAL_PAYMENT_RECEIVED number(20,2) DEFAULT 0);
alter table ISSUER_PAYMENTS modify (PREMIUMS_THIS_PERIOD number(20,2) DEFAULT 0);
alter table ISSUER_PAYMENTS modify (ADJUSTMENTS number(20,2) DEFAULT 0);
alter table ISSUER_PAYMENTS modify (EXCHANGE_FEES number(20,2) DEFAULT 0);
alter table ISSUER_PAYMENTS modify (TOTAL_AMOUNT_DUE number(20,2) DEFAULT 0);
alter table ISSUER_PAYMENTS modify (AMOUNT_ENCLOSED number(20,2) DEFAULT 0);

alter table ISSUER_PAYMENT_INVOICE modify (AMOUNT number(20,2) DEFAULT 0);

alter table ISSUER_PAYMENT_DETAIL modify (AMOUNT_PAID number(20,2) DEFAULT 0);

alter table EXCHANGE_FEES modify (TOTAL_FEE_BALANCE_DUE number(20,2) DEFAULT 0);
alter table EXCHANGE_FEES modify (PAYMENT_RECEIVED number(20,2) DEFAULT 0);
alter table EXCHANGE_FEES modify (TOTAL_PAYMENT number(20,2) DEFAULT 0);
alter table EXCHANGE_FEES modify (TOTAL_REFUND number(20,2) DEFAULT 0);

