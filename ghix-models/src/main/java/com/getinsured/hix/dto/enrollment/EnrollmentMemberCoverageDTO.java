/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

/**
 * @author negi_s
 *
 */
public class EnrollmentMemberCoverageDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer enrolleeId;
	private String memberId;
	private String memberStartDate;
	private String memberEndDate;
	private boolean newMember;
	private boolean isPriorDisEnrollmentExists;
	private List<Integer> priorDisEnrollmentList;
	
	
	/**
	 * @return the enrolleeId
	 */
	public Integer getEnrolleeId() {
		return enrolleeId;
	}
	/**
	 * @param enrolleeId the enrolleeId to set
	 */
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	
	/**
	 * @return the memberId
	 */
	public String getMemberId() {
		return memberId;
	}
	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	/**
	 * @return the memberStartDate
	 */
	public String getMemberStartDate() {
		return memberStartDate;
	}
	/**
	 * @param memberStartDate the memberStartDate to set
	 */
	public void setMemberStartDate(String memberStartDate) {
		this.memberStartDate = memberStartDate;
	}
	/**
	 * @return the memberEndDate
	 */
	public String getMemberEndDate() {
		return memberEndDate;
	}
	/**
	 * @param memberEndDate the memberEndDate to set
	 */
	public void setMemberEndDate(String memberEndDate) {
		this.memberEndDate = memberEndDate;
	}
	public boolean isPriorDisEnrollmentExists() {
		return isPriorDisEnrollmentExists;
	}
	public void setPriorDisEnrollmentExists(boolean isPriorDisEnrollmentExists) {
		this.isPriorDisEnrollmentExists = isPriorDisEnrollmentExists;
	}
	public List<Integer> getPriorDisEnrollmentList() {
		return priorDisEnrollmentList;
	}
	public void setPriorDisEnrollmentList(List<Integer> priorDisEnrollmentList) {
		this.priorDisEnrollmentList = priorDisEnrollmentList;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EnrollmentMemberCoverageDTO [enrolleeId=" + enrolleeId + ", memberId=" + memberId + ", memberStartDate="
				+ memberStartDate + ", memberEndDate=" + memberEndDate + "]";
	}
	public boolean isNewMember() {
		return newMember;
	}
	public void setNewMember(boolean newMember) {
		this.newMember = newMember;
	}
}
