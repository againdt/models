package com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws;

import java.util.Map;

import com.getinsured.hix.model.GHIXRequest;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Insurer;
import com.serff.planmanagementexchangeapi.exchange.model.service.RetrievePlanStatus;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.planmanagementexchangeapi.exchange.model.service.UpdatePlanStatus;
import com.serff.template.plan.PlanBenefitTemplateVO;

/**
 * 
 * @author Bhavani Polimetla, Sunil Desu
 * @since March 14 2013
 */
public class PlanMgmtRequest extends GHIXRequest {


	// Add list of attachments here....they are in main template...we need to
	// pass their content management IDs.	
	
	private RetrievePlanStatus retrievePlanStatus;
	private UpdatePlanStatus updatePlanStatus;
	private TransferPlan transferPlan;
	private Insurer insurer;
	private PlanBenefitTemplateVO planBenefit;
	private com.serff.template.admin.extension.PayloadType adminTemplate;
	private com.serff.template.plan.QhpApplicationEcpVO essentialCommProv;
	private com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrug;
	private com.serff.template.rbrules.extension.PayloadType rateBussinessRules;
	private com.serff.template.urrt.InsuranceRateForecastTempVO unifiedRate;
	private com.serff.template.plan.QhpApplicationRateGroupVO rates;
	private com.serff.template.svcarea.extension.PayloadType serviceArea;
	private com.serff.template.networkProv.extension.PayloadType networkProvider;
	private com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType csrTemplate;
	private com.serff.template.urac.IssuerType uracTemplate;
	private com.serff.template.ncqa.IssuerType ncqaTemplate;
	private Map<String, String> supportingDocuments;
	
	public RetrievePlanStatus getRetrievePlanStatus() {
		return retrievePlanStatus;
	}

	public void setRetrievePlanStatus(RetrievePlanStatus retrievePlanStatus) {
		this.retrievePlanStatus = retrievePlanStatus;
	}

	public UpdatePlanStatus getUpdatePlanStatus() {
		return updatePlanStatus;
	}

	public void setUpdatePlanStatus(UpdatePlanStatus updatePlanStatus) {
		this.updatePlanStatus = updatePlanStatus;
	}

	/**
	 * @return the transferPlan
	 */
	public TransferPlan getTransferPlan() {
		return transferPlan;
	}

	/**
	 * @param transferPlan the transferPlan to set
	 */
	public void setTransferPlan(TransferPlan transferPlan) {
		this.transferPlan = transferPlan;
	}

	/**
	 * @return the insurer
	 */
	public Insurer getInsurer() {
		return insurer;
	}

	/**
	 * @param insurer the insurer to set
	 */
	public void setInsurer(Insurer insurer) {
		this.insurer = insurer;
	}

	/**
	 * @return the planBenefit
	 */
	public PlanBenefitTemplateVO getPlanBenefit() {
		return planBenefit;
	}

	/**
	 * @param planBenefit the planBenefit to set
	 */
	public void setPlanBenefit(PlanBenefitTemplateVO planBenefit) {
		this.planBenefit = planBenefit;
	}
	
	public com.serff.template.admin.extension.PayloadType getAdminTemplate() {
		return adminTemplate;
	}

	public void setAdminTemplate(
			com.serff.template.admin.extension.PayloadType adminTemplate) {
		this.adminTemplate = adminTemplate;
	}

	public com.serff.template.plan.QhpApplicationEcpVO getEssentialCommProv() {
		return essentialCommProv;
	}

	public void setEssentialCommProv(
			com.serff.template.plan.QhpApplicationEcpVO essentialCommProv) {
		this.essentialCommProv = essentialCommProv;
	}

	public com.serff.template.plan.PrescriptionDrugTemplateVO getPrescriptionDrug() {
		return prescriptionDrug;
	}

	public void setPrescriptionDrug(
			com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrug) {
		this.prescriptionDrug = prescriptionDrug;
	}

	public com.serff.template.rbrules.extension.PayloadType getRateBussinessRules() {
		return rateBussinessRules;
	}

	public void setRateBussinessRules(
			com.serff.template.rbrules.extension.PayloadType rateBussinessRules) {
		this.rateBussinessRules = rateBussinessRules;
	}

	public com.serff.template.plan.QhpApplicationRateGroupVO getRates() {
		return rates;
	}

	public void setRates(
			com.serff.template.plan.QhpApplicationRateGroupVO rates) {
		this.rates = rates;
	}

	public com.serff.template.svcarea.extension.PayloadType getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(
			com.serff.template.svcarea.extension.PayloadType serviceArea) {
		this.serviceArea = serviceArea;
	}

	@Override
	public String toString() {
		return "PlanMgmtRequest [retrievePlanStatus=" + retrievePlanStatus
				+ ", updatePlanStatus=" + updatePlanStatus + "]";
	}

	public com.serff.template.networkProv.extension.PayloadType getNetworkProvider() {
		return networkProvider;
	}

	public void setNetworkProvider(
			com.serff.template.networkProv.extension.PayloadType networkProvider) {
		this.networkProvider = networkProvider;
	}

	public com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType getCsrTemplate() {
		return csrTemplate;
	}

	public void setCsrTemplate(
			com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType csrTemplate) {
		this.csrTemplate = csrTemplate;
	}

	public Map<String, String> getSupportingDocuments() {
		return supportingDocuments;
	}

	public void setSupportingDocuments(Map<String, String> supportingDocuments) {
		this.supportingDocuments = supportingDocuments;
	}

	public com.serff.template.urrt.InsuranceRateForecastTempVO getUnifiedRate() {
		return unifiedRate;
	}

	public void setUnifiedRate(
			com.serff.template.urrt.InsuranceRateForecastTempVO unifiedRate) {
		this.unifiedRate = unifiedRate;
	}

	public com.serff.template.urac.IssuerType getUracTemplate() {
		return uracTemplate;
	}

	public void setUracTemplate(com.serff.template.urac.IssuerType uracTemplate) {
		this.uracTemplate = uracTemplate;
	}

	public com.serff.template.ncqa.IssuerType getNcqaTemplate() {
		return ncqaTemplate;
	}

	public void setNcqaTemplate(com.serff.template.ncqa.IssuerType ncqaTemplate) {
		this.ncqaTemplate = ncqaTemplate;
	}
	
}
