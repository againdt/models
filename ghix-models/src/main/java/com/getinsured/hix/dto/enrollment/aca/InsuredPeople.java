package com.getinsured.hix.dto.enrollment.aca;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Awesome Pojo Generator
 */
public class InsuredPeople {
	@JsonProperty("PersonName")
	
	private PersonName PersonName;
	@JsonProperty("PersonAugmentation")
	
	private PersonAugmentation PersonAugmentation;
	@JsonProperty("PersonMarriedIndicator")
	
	private PersonMarriedIndicator PersonMarriedIndicator;
	@JsonProperty("PersonSubscriberIndicator")
	
	private PersonSubscriberIndicator PersonSubscriberIndicator;
	@JsonProperty("PersonSSNIdentification")
	
	private PersonSSNIdentification PersonSSNIdentification;
	@JsonProperty("PersonBirthDate")
	
	private PersonBirthDate PersonBirthDate;
	@JsonProperty("PersonSexCode")
	
	private PersonSexCode PersonSexCode;
	@JsonProperty("id")
	
	private String id;

	public void setPersonName(PersonName PersonName) {
		this.PersonName = PersonName;
	}

	public PersonName getPersonName() {
		return PersonName;
	}

	public void setPersonAugmentation(PersonAugmentation PersonAugmentation) {
		this.PersonAugmentation = PersonAugmentation;
	}

	public PersonAugmentation getPersonAugmentation() {
		return PersonAugmentation;
	}

	public void setPersonMarriedIndicator(PersonMarriedIndicator PersonMarriedIndicator) {
		this.PersonMarriedIndicator = PersonMarriedIndicator;
	}

	public PersonMarriedIndicator getPersonMarriedIndicator() {
		return PersonMarriedIndicator;
	}

	public void setPersonSSNIdentification(PersonSSNIdentification PersonSSNIdentification) {
		this.PersonSSNIdentification = PersonSSNIdentification;
	}

	public PersonSSNIdentification getPersonSSNIdentification() {
		return PersonSSNIdentification;
	}

	public void setPersonBirthDate(PersonBirthDate PersonBirthDate) {
		this.PersonBirthDate = PersonBirthDate;
	}

	public PersonBirthDate getPersonBirthDate() {
		return PersonBirthDate;
	}

	public void setPersonSexCode(PersonSexCode PersonSexCode) {
		this.PersonSexCode = PersonSexCode;
	}

	public PersonSexCode getPersonSexCode() {
		return PersonSexCode;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	public PersonSubscriberIndicator getPersonSubscriberIndicator() {
		return PersonSubscriberIndicator;
	}

	public void setPersonSubscriberIndicator(PersonSubscriberIndicator personSubscriberIndicator) {
		PersonSubscriberIndicator = personSubscriberIndicator;
	}
}