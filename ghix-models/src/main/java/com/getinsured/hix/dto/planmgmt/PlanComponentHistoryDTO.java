package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GHIXResponse;
import com.google.gson.Gson;

public class PlanComponentHistoryDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String planName;
	private String issuerPlanNumber;
	private int issuerId;
	private String issuerName;
	private String issuerLogoUrl;
	private String planStatus;
	private List<Map<String, Object>> historyList;	
	private String insuranceType;
	private String hiosIssuerId;
	private int planYear;
	private Date planStartDate;
	private Date planEndDate;

	public PlanComponentHistoryDTO(){
		 
	}
	
	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getIssuerLogoUrl() {
		return issuerLogoUrl;
	}

	public void setIssuerLogoUrl(String issuerLogoUrl) {
		this.issuerLogoUrl = issuerLogoUrl;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

	public List<Map<String, Object>> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(List<Map<String, Object>> historyList) {
		this.historyList = historyList;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public int getPlanYear() {
		return planYear;
	}

	public void setPlanYear(int planYear) {
		this.planYear = planYear;
	}

	public static Comparator<PlanComponentHistoryDTO> getPlanResponseComparatorByPlanId() {
		return PlanResponseComparatorByPlanId;
	}

	public static void setPlanResponseComparatorByPlanId(Comparator<PlanComponentHistoryDTO> planResponseComparatorByPlanId) {
		PlanResponseComparatorByPlanId = planResponseComparatorByPlanId;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}


	public static Comparator<PlanComponentHistoryDTO> PlanResponseComparatorByPlanId = new Comparator<PlanComponentHistoryDTO>(){

		@Override
		public int compare(PlanComponentHistoryDTO obj1, PlanComponentHistoryDTO obj2) {
			Integer obj1PlanId = obj1.getPlanId();
			Integer obj2PlanId = obj2.getPlanId();
			return obj1PlanId.compareTo(obj2PlanId);
		}
	};
}
