package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_specialty_domain")
public class ProviderSpecialtyDomain  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer specialtyId;
	private String specialtyName;
	private Set<ProviderSpecialty> providerSpecialties = new HashSet<ProviderSpecialty>(0);

	@Id 
	@Column(name="SPECIALTY_ID", unique=true, nullable=false)
	public Integer getSpecialtyId() {
		return this.specialtyId;
	}

	public void setSpecialtyId(Integer specialtyId) {
		this.specialtyId = specialtyId;
	}


	@Column(name="SPECIALTY_NAME", length=100)
	public String getSpecialtyName() {
		return this.specialtyName;
	}

	public void setSpecialtyName(String specialtyName) {
		this.specialtyName = specialtyName;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerSpecialtyDomain")
	public Set<ProviderSpecialty> getProviderSpecialties() {
		return this.providerSpecialties;
	}

	public void setProviderSpecialties(Set<ProviderSpecialty> providerSpecialties) {
		this.providerSpecialties = providerSpecialties;
	}


}


