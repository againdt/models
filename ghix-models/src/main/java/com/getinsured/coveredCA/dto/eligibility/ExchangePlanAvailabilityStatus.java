package com.getinsured.coveredCA.dto.eligibility;

public class ExchangePlanAvailabilityStatus {

	
	private Boolean planAvailability;
	private Boolean isPremiumSame;
	private String newPremiumAmount;
	private String message;
	
	
	
	public Boolean getPlanAvailability() {
		return planAvailability;
	}
	public void setPlanAvailability(Boolean planAvailability) {
		this.planAvailability = planAvailability;
	}
	public Boolean getIsPremiumSame() {
		return isPremiumSame;
	}
	public void setIsPremiumSame(Boolean isPremiumSame) {
		this.isPremiumSame = isPremiumSame;
	}
	public String getNewPremiumAmount() {
		return newPremiumAmount;
	}
	public void setNewPremiumAmount(String newPremiumAmount) {
		this.newPremiumAmount = newPremiumAmount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
