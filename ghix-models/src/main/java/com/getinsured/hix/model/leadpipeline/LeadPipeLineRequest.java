package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

public class LeadPipeLineRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6174013298480847525L;
	
	private Integer pageNumber;
	
	private Integer pageSize;
	
	private String agentId;
	
	private String sortByField;
	
	private String sortOrder;
	
	private String timeZone;

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getSortByField() {
		return sortByField;
	}

	public void setSortByField(String sortByField) {
		this.sortByField = sortByField;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
}