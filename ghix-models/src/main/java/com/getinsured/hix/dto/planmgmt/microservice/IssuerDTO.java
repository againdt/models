package com.getinsured.hix.dto.planmgmt.microservice;

import java.io.Serializable;
import java.util.Date;

public class IssuerDTO extends AuditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String agentFaxNumber;
	private boolean agentFaxNumberApplyToParentBand;
	private String agentFirstName;
	private boolean agentFirstNameApplyToParentBand;
	private String agentLastName;
	private boolean agentLastNameApplyToParentBand;
	private String agentPhoneNumber;
	private boolean agentPhoneNumberApplyToParentBand;
	private String agentEmail;
	private boolean agentEmailApplyToParentBand;
	private String brokerId;
	private boolean brokerIdApplyToParentBand;
	private String carrierApplicationUrl;
	private boolean carrierApplicationUrlApplyToParentBand;
	private String city;
	private String companyAddress1;
	private boolean companyAddress1ApplyToParentBand;
	private String companyAddress2;
	private boolean companyAddress2ApplyToParentBand;
	private String companyCity;
	private boolean companyCityApplyToParentBand;
	private String companyLegalName;
	private String companyLogo;
	private UploadFileDTO uploadCompanyLogo;
	private String companyState;
	private boolean companyStateApplyToParentBand;
	private String companyZip;
	private boolean companyZipApplyToParentBand;
	private String customerWebsiteUrl;
	private boolean customerWebsiteUrlApplyToParentBand;
	private String federalEmployeeId;
	private String hiosIssuerId;
	private String indvMktAppStatusDeptPhoneNumber;
	private boolean indvMktAppStatusDeptPhoneNumberApplyToParentBand;
	private String indvMktBrokerEmailId;
	private boolean indvMktBrokerEmailIdApplyToParentBand;
	private String indvMktBrokerName;
	private boolean indvMktBrokerNameApplyToParentBand;
	private String indvMktBrokerPhoneNumber;
	private boolean indvMktBrokerPhoneNumberApplyToParentBand;
	private String indvMktBrokerPhoneNumberExt;
	private String indvMktBrokerReportingPortalPwd;
	private boolean indvMktBrokerReportingPortalPwdApplyToParentBand;
	private String indvMktBrokerReportingPortalUname;
	private boolean indvMktBrokerReportingPortalUnameApplyToParentBand;
	private String indvMktAgencyRepoPortalURL;
	private boolean indvMktAgencyRepoPortalURLApplyToParentBand;
	private String indvMktCommContactName;
	private boolean indvMktCommContactNameApplyToParentBand;
	private String indvMktCommEmail;
	private boolean indvMktCommEmailApplyToParentBand;
	private String indvMktCommPhoneNumber;
	private boolean indvMktCommPhoneNumberApplyToParentBand;
	private String indvMktCommPhoneNumberExt;
	private String indvMktCommPortalPwd;
	private boolean indvMktCommPortalPwdApplyToParentBand;
	private String indvMktCommPortalUname;
	private boolean indvMktCommPortalUnameApplyToParentBand;
	private String indvMktCommPortalUrl;
	private boolean indvMktCommPortalUrlApplyToParentBand;
	private String indvMktCustServicePhoneNumber;
	private boolean indvMktCustServicePhoneNumberApplyToParentBand;
	private String indvMktCustServicePhoneNumberExt;
	private String indvMktCustServiceTollFreePhoneNumber;
	private boolean indvMktCustServiceTollFreePhoneNumberApplyToParentBand;
	private String indvMktCustServiceTTY;
	private boolean indvMktCustServiceTTYApplyToParentBand;
	private String indvMktCustWebsiteUrl;
	private boolean indvMktCustWebsiteUrlApplyToParentBand;
	private String indvMktPcdEmail;
	private boolean indvMktPcdEmailApplyToParentBand;
	private String indvMktPcdPhoneNumber;
	private boolean indvMktPcdPhoneNumberApplyToParentBand;
	private String indvMktPcdPhoneNumberExt;
	private String indvMktPcdFAX;
	private boolean indvMktPcdFAXApplyToParentBand;
	//private Integer indvMktPcdLocation;
	//private String indvMktPcdCity;
	private String indvMktPcdState;
	private boolean indvMktPcdStateApplyToParentBand;
	private String indvMktPcdStreetAddress1;
	private boolean indvMktPcdStreetAddress1ApplyToParentBand;
	private String indvMktPcdStreetAddress2;
	private boolean indvMktPcdStreetAddress2ApplyToParentBand;
	private String indvMktPcdZip;
	private boolean indvMktPcdZipApplyToParentBand;
	private String isLoginInRequired;
	private boolean isLoginInRequiredApplyToParentBand;
	private String issuerState;
	private String naicCompanyCode;
	private String naicGroupCode;
	private String nationalProducerNum;
	private boolean nationalProducerNumApplyToParentBand;
	private String offExchangeDisclaimer;
	private boolean offExchangeDisclaimerApplyToParentBand;
	private String onExchangeDisclaimer;
	private boolean onExchangeDisclaimerApplyToParentBand;
	private String parentBrandName;
	private Integer parentBrandId;
	private String paymentUrl;
	private boolean paymentUrlApplyToParentBand;
	private String producerPortalPwd;
	private boolean producerPortalPwdApplyToParentBand;
	private String producerPortalUname;
	private boolean producerPortalUnameApplyToParentBand;
	private String producerPortalUrl;
	private boolean producerPortalUrlApplyToParentBand;
	private String shopMktCustServicePhoneNumber;
	private String shopMktCustServicePhoneNumberExt;
	private String shopMktCustServiceTollFreePhoneNumber;
	private String shopMktCustServiceTTY;
	private String shopMktCustWebsiteUrl;
	private String shortName;
	private String state;
	private String status;
	private String streetAddress1;
	private String streetAddress2;
	private String websiteUrl;
	private boolean websiteUrlApplyToParentBand;
	private String writingAgent;
	private boolean writingAgentApplyToParentBand;
	private String zip;
	private IssuerRepDTO primaryContact;
	private Integer commentId;
	private String certificationStatus;
	private String certificationDoc;
	private boolean updateCertStatus;
	private boolean updateIndvMktProfile;
	private boolean updateCompMktProfile;
	private boolean updateIssuerDetails;

	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	
	public IssuerDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAgentEmail() {
		return agentEmail;
	}

	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}

	public String getAgentFaxNumber() {
		return agentFaxNumber;
	}

	public void setAgentFaxNumber(String agentFaxNumber) {
		this.agentFaxNumber = agentFaxNumber;
	}

	public String getAgentFirstName() {
		return agentFirstName;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public String getAgentPhoneNumber() {
		return agentPhoneNumber;
	}

	public void setAgentPhoneNumber(String agentPhoneNumber) {
		this.agentPhoneNumber = agentPhoneNumber;
	}

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public String getCarrierApplicationUrl() {
		return carrierApplicationUrl;
	}

	public void setCarrierApplicationUrl(String carrierApplicationUrl) {
		this.carrierApplicationUrl = carrierApplicationUrl;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCompanyAddress1() {
		return companyAddress1;
	}

	public void setCompanyAddress1(String companyAddress1) {
		this.companyAddress1 = companyAddress1;
	}

	public String getCompanyAddress2() {
		return companyAddress2;
	}

	public void setCompanyAddress2(String companyAddress2) {
		this.companyAddress2 = companyAddress2;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	public String getCompanyLegalName() {
		return companyLegalName;
	}

	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
	
	/**
	 * @return the uploadCompanyLogo
	 */
	public UploadFileDTO getUploadCompanyLogo() {
		return uploadCompanyLogo;
	}

	/**
	 * @param uploadCompanyLogo the uploadCompanyLogo to set
	 */
	public void setUploadCompanyLogo(UploadFileDTO uploadCompanyLogo) {
		this.uploadCompanyLogo = uploadCompanyLogo;
	}

	public String getCompanyState() {
		return companyState;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public String getCompanyZip() {
		return companyZip;
	}

	public void setCompanyZip(String companyZip) {
		this.companyZip = companyZip;
	}

	public String getCustomerWebsiteUrl() {
		return customerWebsiteUrl;
	}

	public void setCustomerWebsiteUrl(String customerWebsiteUrl) {
		this.customerWebsiteUrl = customerWebsiteUrl;
	}

	public String getFederalEmployeeId() {
		return federalEmployeeId;
	}

	public void setFederalEmployeeId(String federalEmployeeId) {
		this.federalEmployeeId = federalEmployeeId;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIndvMktAppStatusDeptPhoneNumber() {
		return indvMktAppStatusDeptPhoneNumber;
	}

	public void setIndvMktAppStatusDeptPhoneNumber(
			String indvMktAppStatusDeptPhoneNumber) {
		this.indvMktAppStatusDeptPhoneNumber = indvMktAppStatusDeptPhoneNumber;
	}

	public String getIndvMktBrokerEmailId() {
		return indvMktBrokerEmailId;
	}

	public void setIndvMktBrokerEmailId(String indvMktBrokerEmailId) {
		this.indvMktBrokerEmailId = indvMktBrokerEmailId;
	}

	public String getIndvMktBrokerName() {
		return indvMktBrokerName;
	}

	public void setIndvMktBrokerName(String indvMktBrokerName) {
		this.indvMktBrokerName = indvMktBrokerName;
	}

	public String getIndvMktBrokerPhoneNumber() {
		return indvMktBrokerPhoneNumber;
	}

	public void setIndvMktBrokerPhoneNumber(String indvMktBrokerPhoneNumber) {
		this.indvMktBrokerPhoneNumber = indvMktBrokerPhoneNumber;
	}

	public String getIndvMktBrokerPhoneNumberExt() {
		return indvMktBrokerPhoneNumberExt;
	}

	public void setIndvMktBrokerPhoneNumberExt(
			String indvMktBrokerPhoneNumberExt) {
		this.indvMktBrokerPhoneNumberExt = indvMktBrokerPhoneNumberExt;
	}

	public String getIndvMktBrokerReportingPortalPwd() {
		return indvMktBrokerReportingPortalPwd;
	}

	public void setIndvMktBrokerReportingPortalPwd(
			String indvMktBrokerReportingPortalPwd) {
		this.indvMktBrokerReportingPortalPwd = indvMktBrokerReportingPortalPwd;
	}

	public String getIndvMktBrokerReportingPortalUname() {
		return indvMktBrokerReportingPortalUname;
	}

	public void setIndvMktBrokerReportingPortalUname(
			String indvMktBrokerReportingPortalUname) {
		this.indvMktBrokerReportingPortalUname = indvMktBrokerReportingPortalUname;
	}

	public String getIndvMktCommContactName() {
		return indvMktCommContactName;
	}

	public void setIndvMktCommContactName(String indvMktCommContactName) {
		this.indvMktCommContactName = indvMktCommContactName;
	}

	public String getIndvMktCommEmail() {
		return indvMktCommEmail;
	}

	public void setIndvMktCommEmail(String indvMktCommEmail) {
		this.indvMktCommEmail = indvMktCommEmail;
	}

	public String getIndvMktCommPhoneNumber() {
		return indvMktCommPhoneNumber;
	}

	public void setIndvMktCommPhoneNumber(String indvMktCommPhoneNumber) {
		this.indvMktCommPhoneNumber = indvMktCommPhoneNumber;
	}

	public String getIndvMktCommPhoneNumberExt() {
		return indvMktCommPhoneNumberExt;
	}

	public void setIndvMktCommPhoneNumberExt(String indvMktCommPhoneNumberExt) {
		this.indvMktCommPhoneNumberExt = indvMktCommPhoneNumberExt;
	}

	public String getIndvMktCommPortalPwd() {
		return indvMktCommPortalPwd;
	}

	public void setIndvMktCommPortalPwd(String indvMktCommPortalPwd) {
		this.indvMktCommPortalPwd = indvMktCommPortalPwd;
	}

	public String getIndvMktCommPortalUname() {
		return indvMktCommPortalUname;
	}

	public void setIndvMktCommPortalUname(String indvMktCommPortalUname) {
		this.indvMktCommPortalUname = indvMktCommPortalUname;
	}

	public String getIndvMktCommPortalUrl() {
		return indvMktCommPortalUrl;
	}

	public void setIndvMktCommPortalUrl(String indvMktCommPortalUrl) {
		this.indvMktCommPortalUrl = indvMktCommPortalUrl;
	}

	public String getIndvMktCustServicePhoneNumber() {
		return indvMktCustServicePhoneNumber;
	}

	public void setIndvMktCustServicePhoneNumber(
			String indvMktCustServicePhoneNumber) {
		this.indvMktCustServicePhoneNumber = indvMktCustServicePhoneNumber;
	}

	public String getIndvMktCustServicePhoneNumberExt() {
		return indvMktCustServicePhoneNumberExt;
	}

	public void setIndvMktCustServicePhoneNumberExt(
			String indvMktCustServicePhoneNumberExt) {
		this.indvMktCustServicePhoneNumberExt = indvMktCustServicePhoneNumberExt;
	}

	public String getIndvMktCustServiceTollFreePhoneNumber() {
		return indvMktCustServiceTollFreePhoneNumber;
	}

	public void setIndvMktCustServiceTollFreePhoneNumber(
			String indvMktCustServiceTollFreePhoneNumber) {
		this.indvMktCustServiceTollFreePhoneNumber = indvMktCustServiceTollFreePhoneNumber;
	}

	public String getIndvMktCustServiceTTY() {
		return indvMktCustServiceTTY;
	}

	public void setIndvMktCustServiceTTY(String indvMktCustServiceTTY) {
		this.indvMktCustServiceTTY = indvMktCustServiceTTY;
	}

	public String getIndvMktCustWebsiteUrl() {
		return indvMktCustWebsiteUrl;
	}

	public void setIndvMktCustWebsiteUrl(String indvMktCustWebsiteUrl) {
		this.indvMktCustWebsiteUrl = indvMktCustWebsiteUrl;
	}

	public String getIndvMktPcdFAX() {
		return indvMktPcdFAX;
	}

	public void setIndvMktPcdFAX(String indvMktPcdFAX) {
		this.indvMktPcdFAX = indvMktPcdFAX;
	}

	/**
	 * @return the indvMktPcdState
	 */
	public String getIndvMktPcdState() {
		return indvMktPcdState;
	}

	/**
	 * @param indvMktPcdState the indvMktPcdState to set
	 */
	public void setIndvMktPcdState(String indvMktPcdState) {
		this.indvMktPcdState = indvMktPcdState;
	}

	/**
	 * @return the indvMktPcdStreetAddress1
	 */
	public String getIndvMktPcdStreetAddress1() {
		return indvMktPcdStreetAddress1;
	}

	/**
	 * @param indvMktPcdStreetAddress1 the indvMktPcdStreetAddress1 to set
	 */
	public void setIndvMktPcdStreetAddress1(String indvMktPcdStreetAddress1) {
		this.indvMktPcdStreetAddress1 = indvMktPcdStreetAddress1;
	}

	/**
	 * @return the indvMktPcdStreetAddress2
	 */
	public String getIndvMktPcdStreetAddress2() {
		return indvMktPcdStreetAddress2;
	}

	/**
	 * @param indvMktPcdStreetAddress2 the indvMktPcdStreetAddress2 to set
	 */
	public void setIndvMktPcdStreetAddress2(String indvMktPcdStreetAddress2) {
		this.indvMktPcdStreetAddress2 = indvMktPcdStreetAddress2;
	}

	/**
	 * @return the indvMktPcdZip
	 */
	public String getIndvMktPcdZip() {
		return indvMktPcdZip;
	}

	/**
	 * @param indvMktPcdZip the indvMktPcdZip to set
	 */
	public void setIndvMktPcdZip(String indvMktPcdZip) {
		this.indvMktPcdZip = indvMktPcdZip;
	}

	public String getIndvMktPcdEmail() {
		return indvMktPcdEmail;
	}

	public void setIndvMktPcdEmail(String indvMktPcdEmail) {
		this.indvMktPcdEmail = indvMktPcdEmail;
	}

	public String getIndvMktPcdPhoneNumber() {
		return indvMktPcdPhoneNumber;
	}

	public void setIndvMktPcdPhoneNumber(String indvMktPcdPhoneNumber) {
		this.indvMktPcdPhoneNumber = indvMktPcdPhoneNumber;
	}

	public String getIndvMktPcdPhoneNumberExt() {
		return indvMktPcdPhoneNumberExt;
	}

	public void setIndvMktPcdPhoneNumberExt(String indvMktPcdPhoneNumberExt) {
		this.indvMktPcdPhoneNumberExt = indvMktPcdPhoneNumberExt;
	}

	/*public Integer getIndvMktPcdLocation() {
		return indvMktPcdLocation;
	}

	public void setIndvMktPcdLocation(Integer indvMktPcdLocation) {
		this.indvMktPcdLocation = indvMktPcdLocation;
	}*/

	public String getIsLoginInRequired() {
		return isLoginInRequired;
	}

	public void setIsLoginInRequired(String isLoginInRequired) {
		this.isLoginInRequired = isLoginInRequired;
	}

	public String getIssuerState() {
		return issuerState;
	}

	public void setIssuerState(String issuerState) {
		this.issuerState = issuerState;
	}

	public String getNaicCompanyCode() {
		return naicCompanyCode;
	}

	public void setNaicCompanyCode(String naicCompanyCode) {
		this.naicCompanyCode = naicCompanyCode;
	}

	public String getNaicGroupCode() {
		return naicGroupCode;
	}

	public void setNaicGroupCode(String naicGroupCode) {
		this.naicGroupCode = naicGroupCode;
	}
	
	public String getNationalProducerNum() {
		return nationalProducerNum;
	}

	public void setNationalProducerNum(String nationalProducerNum) {
		this.nationalProducerNum = nationalProducerNum;
	}

	public String getOffExchangeDisclaimer() {
		return offExchangeDisclaimer;
	}

	public void setOffExchangeDisclaimer(String offExchangeDisclaimer) {
		this.offExchangeDisclaimer = offExchangeDisclaimer;
	}

	public String getOnExchangeDisclaimer() {
		return onExchangeDisclaimer;
	}

	public void setOnExchangeDisclaimer(String onExchangeDisclaimer) {
		this.onExchangeDisclaimer = onExchangeDisclaimer;
	}

	public String getParentBrandName() {
		return parentBrandName;
	}

	public void setParentBrandName(String parentBrandName) {
		this.parentBrandName = parentBrandName;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getProducerPortalPwd() {
		return producerPortalPwd;
	}

	public void setProducerPortalPwd(String producerPortalPwd) {
		this.producerPortalPwd = producerPortalPwd;
	}

	public String getProducerPortalUname() {
		return producerPortalUname;
	}

	public void setProducerPortalUname(String producerPortalUname) {
		this.producerPortalUname = producerPortalUname;
	}

	public String getProducerPortalUrl() {
		return producerPortalUrl;
	}

	public void setProducerPortalUrl(String producerPortalUrl) {
		this.producerPortalUrl = producerPortalUrl;
	}

	public String getShopMktCustServicePhoneNumber() {
		return shopMktCustServicePhoneNumber;
	}

	public void setShopMktCustServicePhoneNumber(
			String shopMktCustServicePhoneNumber) {
		this.shopMktCustServicePhoneNumber = shopMktCustServicePhoneNumber;
	}

	public String getShopMktCustServicePhoneNumberExt() {
		return shopMktCustServicePhoneNumberExt;
	}

	public void setShopMktCustServicePhoneNumberExt(
			String shopMktCustServicePhoneNumberExt) {
		this.shopMktCustServicePhoneNumberExt = shopMktCustServicePhoneNumberExt;
	}

	public String getShopMktCustServiceTollFreePhoneNumber() {
		return shopMktCustServiceTollFreePhoneNumber;
	}

	public void setShopMktCustServiceTollFreePhoneNumber(
			String shopMktCustServiceTollFreePhoneNumber) {
		this.shopMktCustServiceTollFreePhoneNumber = shopMktCustServiceTollFreePhoneNumber;
	}

	public String getShopMktCustServiceTTY() {
		return shopMktCustServiceTTY;
	}

	public void setShopMktCustServiceTTY(String shopMktCustServiceTTY) {
		this.shopMktCustServiceTTY = shopMktCustServiceTTY;
	}

	public String getShopMktCustWebsiteUrl() {
		return shopMktCustWebsiteUrl;
	}

	public void setShopMktCustWebsiteUrl(String shopMktCustWebsiteUrl) {
		this.shopMktCustWebsiteUrl = shopMktCustWebsiteUrl;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getWritingAgent() {
		return writingAgent;
	}

	public void setWritingAgent(String writingAgent) {
		this.writingAgent = writingAgent;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public IssuerRepDTO getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(IssuerRepDTO primaryContact) {
		this.primaryContact = primaryContact;
	}
	
	public String getIndvMktAgencyRepoPortalURL() {
		return indvMktAgencyRepoPortalURL;
	}

	public void setIndvMktAgencyRepoPortalURL(String indvMktAgencyRepoPortalURL) {
		this.indvMktAgencyRepoPortalURL = indvMktAgencyRepoPortalURL;
	}
	
	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	
	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	public String getCertificationDoc() {
		return certificationDoc;
	}

	public void setCertificationDoc(String certificationDoc) {
		this.certificationDoc = certificationDoc;
	}
	
	public boolean isUpdateCertStatus() {
		return updateCertStatus;
	}

	public void setUpdateCertStatus(boolean updateCertStatus) {
		this.updateCertStatus = updateCertStatus;
	}
	
	public boolean isUpdateIndvMktProfile() {
		return updateIndvMktProfile;
	}

	public void setUpdateIndvMktProfile(boolean updateIndvMktProfile) {
		this.updateIndvMktProfile = updateIndvMktProfile;
	}

	public boolean isUpdateCompMktProfile() {
		return updateCompMktProfile;
	}

	public void setUpdateCompMktProfile(boolean updateCompMktProfile) {
		this.updateCompMktProfile = updateCompMktProfile;
	}

	public boolean isUpdateIssuerDetails() {
		return updateIssuerDetails;
	}

	public void setUpdateIssuerDetails(boolean updateIssuerDetails) {
		this.updateIssuerDetails = updateIssuerDetails;
	}

	/**
	 * @return the indvMktAppStatusDeptPhoneNumberApplyToParentBand
	 */
	public boolean isIndvMktAppStatusDeptPhoneNumberApplyToParentBand() {
		return indvMktAppStatusDeptPhoneNumberApplyToParentBand;
	}

	/**
	 * @param indvMktAppStatusDeptPhoneNumberApplyToParentBand the indvMktAppStatusDeptPhoneNumberApplyToParentBand to set
	 */
	public void setIndvMktAppStatusDeptPhoneNumberApplyToParentBand(
			boolean indvMktAppStatusDeptPhoneNumberApplyToParentBand) {
		this.indvMktAppStatusDeptPhoneNumberApplyToParentBand = indvMktAppStatusDeptPhoneNumberApplyToParentBand;
	}

	/**
	 * @return the indvMktBrokerEmailIdApplyToParentBand
	 */
	public boolean isIndvMktBrokerEmailIdApplyToParentBand() {
		return indvMktBrokerEmailIdApplyToParentBand;
	}

	/**
	 * @param indvMktBrokerEmailIdApplyToParentBand the indvMktBrokerEmailIdApplyToParentBand to set
	 */
	public void setIndvMktBrokerEmailIdApplyToParentBand(
			boolean indvMktBrokerEmailIdApplyToParentBand) {
		this.indvMktBrokerEmailIdApplyToParentBand = indvMktBrokerEmailIdApplyToParentBand;
	}

	/**
	 * @return the indvMktBrokerNameApplyToParentBand
	 */
	public boolean isIndvMktBrokerNameApplyToParentBand() {
		return indvMktBrokerNameApplyToParentBand;
	}

	/**
	 * @param indvMktBrokerNameApplyToParentBand the indvMktBrokerNameApplyToParentBand to set
	 */
	public void setIndvMktBrokerNameApplyToParentBand(
			boolean indvMktBrokerNameApplyToParentBand) {
		this.indvMktBrokerNameApplyToParentBand = indvMktBrokerNameApplyToParentBand;
	}

	/**
	 * @return the indvMktBrokerPhoneNumberApplyToParentBand
	 */
	public boolean isIndvMktBrokerPhoneNumberApplyToParentBand() {
		return indvMktBrokerPhoneNumberApplyToParentBand;
	}

	/**
	 * @param indvMktBrokerPhoneNumberApplyToParentBand the indvMktBrokerPhoneNumberApplyToParentBand to set
	 */
	public void setIndvMktBrokerPhoneNumberApplyToParentBand(
			boolean indvMktBrokerPhoneNumberApplyToParentBand) {
		this.indvMktBrokerPhoneNumberApplyToParentBand = indvMktBrokerPhoneNumberApplyToParentBand;
	}

	/**
	 * @return the indvMktBrokerReportingPortalPwdApplyToParentBand
	 */
	public boolean isIndvMktBrokerReportingPortalPwdApplyToParentBand() {
		return indvMktBrokerReportingPortalPwdApplyToParentBand;
	}

	/**
	 * @param indvMktBrokerReportingPortalPwdApplyToParentBand the indvMktBrokerReportingPortalPwdApplyToParentBand to set
	 */
	public void setIndvMktBrokerReportingPortalPwdApplyToParentBand(
			boolean indvMktBrokerReportingPortalPwdApplyToParentBand) {
		this.indvMktBrokerReportingPortalPwdApplyToParentBand = indvMktBrokerReportingPortalPwdApplyToParentBand;
	}

	/**
	 * @return the indvMktBrokerReportingPortalUnameApplyToParentBand
	 */
	public boolean isIndvMktBrokerReportingPortalUnameApplyToParentBand() {
		return indvMktBrokerReportingPortalUnameApplyToParentBand;
	}

	/**
	 * @param indvMktBrokerReportingPortalUnameApplyToParentBand the indvMktBrokerReportingPortalUnameApplyToParentBand to set
	 */
	public void setIndvMktBrokerReportingPortalUnameApplyToParentBand(
			boolean indvMktBrokerReportingPortalUnameApplyToParentBand) {
		this.indvMktBrokerReportingPortalUnameApplyToParentBand = indvMktBrokerReportingPortalUnameApplyToParentBand;
	}

	/**
	 * @return the indvMktAgencyRepoPortalURLApplyToParentBand
	 */
	public boolean isIndvMktAgencyRepoPortalURLApplyToParentBand() {
		return indvMktAgencyRepoPortalURLApplyToParentBand;
	}

	/**
	 * @param indvMktAgencyRepoPortalURLApplyToParentBand the indvMktAgencyRepoPortalURLApplyToParentBand to set
	 */
	public void setIndvMktAgencyRepoPortalURLApplyToParentBand(
			boolean indvMktAgencyRepoPortalURLApplyToParentBand) {
		this.indvMktAgencyRepoPortalURLApplyToParentBand = indvMktAgencyRepoPortalURLApplyToParentBand;
	}

	/**
	 * @return the indvMktCommContactNameApplyToParentBand
	 */
	public boolean isIndvMktCommContactNameApplyToParentBand() {
		return indvMktCommContactNameApplyToParentBand;
	}

	/**
	 * @param indvMktCommContactNameApplyToParentBand the indvMktCommContactNameApplyToParentBand to set
	 */
	public void setIndvMktCommContactNameApplyToParentBand(
			boolean indvMktCommContactNameApplyToParentBand) {
		this.indvMktCommContactNameApplyToParentBand = indvMktCommContactNameApplyToParentBand;
	}

	/**
	 * @return the indvMktCommEmailApplyToParentBand
	 */
	public boolean isIndvMktCommEmailApplyToParentBand() {
		return indvMktCommEmailApplyToParentBand;
	}

	/**
	 * @param indvMktCommEmailApplyToParentBand the indvMktCommEmailApplyToParentBand to set
	 */
	public void setIndvMktCommEmailApplyToParentBand(
			boolean indvMktCommEmailApplyToParentBand) {
		this.indvMktCommEmailApplyToParentBand = indvMktCommEmailApplyToParentBand;
	}

	/**
	 * @return the indvMktCommPhoneNumberApplyToParentBand
	 */
	public boolean isIndvMktCommPhoneNumberApplyToParentBand() {
		return indvMktCommPhoneNumberApplyToParentBand;
	}

	/**
	 * @param indvMktCommPhoneNumberApplyToParentBand the indvMktCommPhoneNumberApplyToParentBand to set
	 */
	public void setIndvMktCommPhoneNumberApplyToParentBand(
			boolean indvMktCommPhoneNumberApplyToParentBand) {
		this.indvMktCommPhoneNumberApplyToParentBand = indvMktCommPhoneNumberApplyToParentBand;
	}

	/**
	 * @return the indvMktCommPortalPwdApplyToParentBand
	 */
	public boolean isIndvMktCommPortalPwdApplyToParentBand() {
		return indvMktCommPortalPwdApplyToParentBand;
	}

	/**
	 * @param indvMktCommPortalPwdApplyToParentBand the indvMktCommPortalPwdApplyToParentBand to set
	 */
	public void setIndvMktCommPortalPwdApplyToParentBand(
			boolean indvMktCommPortalPwdApplyToParentBand) {
		this.indvMktCommPortalPwdApplyToParentBand = indvMktCommPortalPwdApplyToParentBand;
	}

	/**
	 * @return the indvMktCommPortalUnameApplyToParentBand
	 */
	public boolean isIndvMktCommPortalUnameApplyToParentBand() {
		return indvMktCommPortalUnameApplyToParentBand;
	}

	/**
	 * @param indvMktCommPortalUnameApplyToParentBand the indvMktCommPortalUnameApplyToParentBand to set
	 */
	public void setIndvMktCommPortalUnameApplyToParentBand(
			boolean indvMktCommPortalUnameApplyToParentBand) {
		this.indvMktCommPortalUnameApplyToParentBand = indvMktCommPortalUnameApplyToParentBand;
	}

	/**
	 * @return the indvMktCommPortalUrlApplyToParentBand
	 */
	public boolean isIndvMktCommPortalUrlApplyToParentBand() {
		return indvMktCommPortalUrlApplyToParentBand;
	}

	/**
	 * @param indvMktCommPortalUrlApplyToParentBand the indvMktCommPortalUrlApplyToParentBand to set
	 */
	public void setIndvMktCommPortalUrlApplyToParentBand(
			boolean indvMktCommPortalUrlApplyToParentBand) {
		this.indvMktCommPortalUrlApplyToParentBand = indvMktCommPortalUrlApplyToParentBand;
	}

	/**
	 * @return the indvMktCustServicePhoneNumberApplyToParentBand
	 */
	public boolean isIndvMktCustServicePhoneNumberApplyToParentBand() {
		return indvMktCustServicePhoneNumberApplyToParentBand;
	}

	/**
	 * @param indvMktCustServicePhoneNumberApplyToParentBand the indvMktCustServicePhoneNumberApplyToParentBand to set
	 */
	public void setIndvMktCustServicePhoneNumberApplyToParentBand(
			boolean indvMktCustServicePhoneNumberApplyToParentBand) {
		this.indvMktCustServicePhoneNumberApplyToParentBand = indvMktCustServicePhoneNumberApplyToParentBand;
	}

	/**
	 * @return the indvMktCustServiceTollFreePhoneNumberApplyToParentBand
	 */
	public boolean isIndvMktCustServiceTollFreePhoneNumberApplyToParentBand() {
		return indvMktCustServiceTollFreePhoneNumberApplyToParentBand;
	}

	/**
	 * @param indvMktCustServiceTollFreePhoneNumberApplyToParentBand the indvMktCustServiceTollFreePhoneNumberApplyToParentBand to set
	 */
	public void setIndvMktCustServiceTollFreePhoneNumberApplyToParentBand(
			boolean indvMktCustServiceTollFreePhoneNumberApplyToParentBand) {
		this.indvMktCustServiceTollFreePhoneNumberApplyToParentBand = indvMktCustServiceTollFreePhoneNumberApplyToParentBand;
	}

	/**
	 * @return the indvMktCustServiceTTYApplyToParentBand
	 */
	public boolean isIndvMktCustServiceTTYApplyToParentBand() {
		return indvMktCustServiceTTYApplyToParentBand;
	}

	/**
	 * @param indvMktCustServiceTTYApplyToParentBand the indvMktCustServiceTTYApplyToParentBand to set
	 */
	public void setIndvMktCustServiceTTYApplyToParentBand(
			boolean indvMktCustServiceTTYApplyToParentBand) {
		this.indvMktCustServiceTTYApplyToParentBand = indvMktCustServiceTTYApplyToParentBand;
	}

	/**
	 * @return the indvMktCustWebsiteUrlApplyToParentBand
	 */
	public boolean isIndvMktCustWebsiteUrlApplyToParentBand() {
		return indvMktCustWebsiteUrlApplyToParentBand;
	}

	/**
	 * @param indvMktCustWebsiteUrlApplyToParentBand the indvMktCustWebsiteUrlApplyToParentBand to set
	 */
	public void setIndvMktCustWebsiteUrlApplyToParentBand(
			boolean indvMktCustWebsiteUrlApplyToParentBand) {
		this.indvMktCustWebsiteUrlApplyToParentBand = indvMktCustWebsiteUrlApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdEmailApplyToParentBand
	 */
	public boolean isIndvMktPcdEmailApplyToParentBand() {
		return indvMktPcdEmailApplyToParentBand;
	}

	/**
	 * @param indvMktPcdEmailApplyToParentBand the indvMktPcdEmailApplyToParentBand to set
	 */
	public void setIndvMktPcdEmailApplyToParentBand(
			boolean indvMktPcdEmailApplyToParentBand) {
		this.indvMktPcdEmailApplyToParentBand = indvMktPcdEmailApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdPhoneNumberApplyToParentBand
	 */
	public boolean isIndvMktPcdPhoneNumberApplyToParentBand() {
		return indvMktPcdPhoneNumberApplyToParentBand;
	}

	/**
	 * @param indvMktPcdPhoneNumberApplyToParentBand the indvMktPcdPhoneNumberApplyToParentBand to set
	 */
	public void setIndvMktPcdPhoneNumberApplyToParentBand(
			boolean indvMktPcdPhoneNumberApplyToParentBand) {
		this.indvMktPcdPhoneNumberApplyToParentBand = indvMktPcdPhoneNumberApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdFAXApplyToParentBand
	 */
	public boolean isIndvMktPcdFAXApplyToParentBand() {
		return indvMktPcdFAXApplyToParentBand;
	}

	/**
	 * @param indvMktPcdFAXApplyToParentBand the indvMktPcdFAXApplyToParentBand to set
	 */
	public void setIndvMktPcdFAXApplyToParentBand(
			boolean indvMktPcdFAXApplyToParentBand) {
		this.indvMktPcdFAXApplyToParentBand = indvMktPcdFAXApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdStateApplyToParentBand
	 */
	public boolean isIndvMktPcdStateApplyToParentBand() {
		return indvMktPcdStateApplyToParentBand;
	}

	/**
	 * @param indvMktPcdStateApplyToParentBand the indvMktPcdStateApplyToParentBand to set
	 */
	public void setIndvMktPcdStateApplyToParentBand(
			boolean indvMktPcdStateApplyToParentBand) {
		this.indvMktPcdStateApplyToParentBand = indvMktPcdStateApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdStreetAddress1ApplyToParentBand
	 */
	public boolean isIndvMktPcdStreetAddress1ApplyToParentBand() {
		return indvMktPcdStreetAddress1ApplyToParentBand;
	}

	/**
	 * @param indvMktPcdStreetAddress1ApplyToParentBand the indvMktPcdStreetAddress1ApplyToParentBand to set
	 */
	public void setIndvMktPcdStreetAddress1ApplyToParentBand(
			boolean indvMktPcdStreetAddress1ApplyToParentBand) {
		this.indvMktPcdStreetAddress1ApplyToParentBand = indvMktPcdStreetAddress1ApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdStreetAddress2ApplyToParentBand
	 */
	public boolean isIndvMktPcdStreetAddress2ApplyToParentBand() {
		return indvMktPcdStreetAddress2ApplyToParentBand;
	}

	/**
	 * @param indvMktPcdStreetAddress2ApplyToParentBand the indvMktPcdStreetAddress2ApplyToParentBand to set
	 */
	public void setIndvMktPcdStreetAddress2ApplyToParentBand(
			boolean indvMktPcdStreetAddress2ApplyToParentBand) {
		this.indvMktPcdStreetAddress2ApplyToParentBand = indvMktPcdStreetAddress2ApplyToParentBand;
	}

	/**
	 * @return the indvMktPcdZipApplyToParentBand
	 */
	public boolean isIndvMktPcdZipApplyToParentBand() {
		return indvMktPcdZipApplyToParentBand;
	}

	/**
	 * @param indvMktPcdZipApplyToParentBand the indvMktPcdZipApplyToParentBand to set
	 */
	public void setIndvMktPcdZipApplyToParentBand(
			boolean indvMktPcdZipApplyToParentBand) {
		this.indvMktPcdZipApplyToParentBand = indvMktPcdZipApplyToParentBand;
	}

	/**
	 * @return the agentFaxNumberApplyToParentBand
	 */
	public boolean isAgentFaxNumberApplyToParentBand() {
		return agentFaxNumberApplyToParentBand;
	}

	/**
	 * @param agentFaxNumberApplyToParentBand the agentFaxNumberApplyToParentBand to set
	 */
	public void setAgentFaxNumberApplyToParentBand(
			boolean agentFaxNumberApplyToParentBand) {
		this.agentFaxNumberApplyToParentBand = agentFaxNumberApplyToParentBand;
	}

	/**
	 * @return the agentFirstNameApplyToParentBand
	 */
	public boolean isAgentFirstNameApplyToParentBand() {
		return agentFirstNameApplyToParentBand;
	}

	/**
	 * @param agentFirstNameApplyToParentBand the agentFirstNameApplyToParentBand to set
	 */
	public void setAgentFirstNameApplyToParentBand(
			boolean agentFirstNameApplyToParentBand) {
		this.agentFirstNameApplyToParentBand = agentFirstNameApplyToParentBand;
	}

	/**
	 * @return the agentLastNameApplyToParentBand
	 */
	public boolean isAgentLastNameApplyToParentBand() {
		return agentLastNameApplyToParentBand;
	}

	/**
	 * @param agentLastNameApplyToParentBand the agentLastNameApplyToParentBand to set
	 */
	public void setAgentLastNameApplyToParentBand(
			boolean agentLastNameApplyToParentBand) {
		this.agentLastNameApplyToParentBand = agentLastNameApplyToParentBand;
	}

	/**
	 * @return the agentPhoneNumberApplyToParentBand
	 */
	public boolean isAgentPhoneNumberApplyToParentBand() {
		return agentPhoneNumberApplyToParentBand;
	}

	/**
	 * @param agentPhoneNumberApplyToParentBand the agentPhoneNumberApplyToParentBand to set
	 */
	public void setAgentPhoneNumberApplyToParentBand(
			boolean agentPhoneNumberApplyToParentBand) {
		this.agentPhoneNumberApplyToParentBand = agentPhoneNumberApplyToParentBand;
	}

	/**
	 * @return the agentEmailApplyToParentBand
	 */
	public boolean isAgentEmailApplyToParentBand() {
		return agentEmailApplyToParentBand;
	}

	/**
	 * @param agentEmailApplyToParentBand the agentEmailApplyToParentBand to set
	 */
	public void setAgentEmailApplyToParentBand(boolean agentEmailApplyToParentBand) {
		this.agentEmailApplyToParentBand = agentEmailApplyToParentBand;
	}

	/**
	 * @return the brokerIdApplyToParentBand
	 */
	public boolean isBrokerIdApplyToParentBand() {
		return brokerIdApplyToParentBand;
	}

	/**
	 * @param brokerIdApplyToParentBand the brokerIdApplyToParentBand to set
	 */
	public void setBrokerIdApplyToParentBand(boolean brokerIdApplyToParentBand) {
		this.brokerIdApplyToParentBand = brokerIdApplyToParentBand;
	}

	/**
	 * @return the carrierApplicationUrlApplyToParentBand
	 */
	public boolean isCarrierApplicationUrlApplyToParentBand() {
		return carrierApplicationUrlApplyToParentBand;
	}

	/**
	 * @param carrierApplicationUrlApplyToParentBand the carrierApplicationUrlApplyToParentBand to set
	 */
	public void setCarrierApplicationUrlApplyToParentBand(
			boolean carrierApplicationUrlApplyToParentBand) {
		this.carrierApplicationUrlApplyToParentBand = carrierApplicationUrlApplyToParentBand;
	}

	/**
	 * @return the companyAddress1ApplyToParentBand
	 */
	public boolean isCompanyAddress1ApplyToParentBand() {
		return companyAddress1ApplyToParentBand;
	}

	/**
	 * @param companyAddress1ApplyToParentBand the companyAddress1ApplyToParentBand to set
	 */
	public void setCompanyAddress1ApplyToParentBand(
			boolean companyAddress1ApplyToParentBand) {
		this.companyAddress1ApplyToParentBand = companyAddress1ApplyToParentBand;
	}

	/**
	 * @return the companyCityApplyToParentBand
	 */
	public boolean isCompanyCityApplyToParentBand() {
		return companyCityApplyToParentBand;
	}

	/**
	 * @param companyCityApplyToParentBand the companyCityApplyToParentBand to set
	 */
	public void setCompanyCityApplyToParentBand(boolean companyCityApplyToParentBand) {
		this.companyCityApplyToParentBand = companyCityApplyToParentBand;
	}

	/**
	 * @return the companyAddress2ApplyToParentBand
	 */
	public boolean isCompanyAddress2ApplyToParentBand() {
		return companyAddress2ApplyToParentBand;
	}

	/**
	 * @param companyAddress2ApplyToParentBand the companyAddress2ApplyToParentBand to set
	 */
	public void setCompanyAddress2ApplyToParentBand(
			boolean companyAddress2ApplyToParentBand) {
		this.companyAddress2ApplyToParentBand = companyAddress2ApplyToParentBand;
	}

	/**
	 * @return the companyStateApplyToParentBand
	 */
	public boolean isCompanyStateApplyToParentBand() {
		return companyStateApplyToParentBand;
	}

	/**
	 * @param companyStateApplyToParentBand the companyStateApplyToParentBand to set
	 */
	public void setCompanyStateApplyToParentBand(
			boolean companyStateApplyToParentBand) {
		this.companyStateApplyToParentBand = companyStateApplyToParentBand;
	}

	/**
	 * @return the companyZipApplyToParentBand
	 */
	public boolean isCompanyZipApplyToParentBand() {
		return companyZipApplyToParentBand;
	}

	/**
	 * @param companyZipApplyToParentBand the companyZipApplyToParentBand to set
	 */
	public void setCompanyZipApplyToParentBand(boolean companyZipApplyToParentBand) {
		this.companyZipApplyToParentBand = companyZipApplyToParentBand;
	}

	/**
	 * @return the customerWebsiteUrlApplyToParentBand
	 */
	public boolean isCustomerWebsiteUrlApplyToParentBand() {
		return customerWebsiteUrlApplyToParentBand;
	}

	/**
	 * @param customerWebsiteUrlApplyToParentBand the customerWebsiteUrlApplyToParentBand to set
	 */
	public void setCustomerWebsiteUrlApplyToParentBand(
			boolean customerWebsiteUrlApplyToParentBand) {
		this.customerWebsiteUrlApplyToParentBand = customerWebsiteUrlApplyToParentBand;
	}

	/**
	 * @return the isLoginInRequiredApplyToParentBand
	 */
	public boolean isLoginInRequiredApplyToParentBand() {
		return isLoginInRequiredApplyToParentBand;
	}

	/**
	 * @param isLoginInRequiredApplyToParentBand the isLoginInRequiredApplyToParentBand to set
	 */
	public void setLoginInRequiredApplyToParentBand(
			boolean isLoginInRequiredApplyToParentBand) {
		this.isLoginInRequiredApplyToParentBand = isLoginInRequiredApplyToParentBand;
	}

	/**
	 * @return the nationalProducerNumApplyToParentBand
	 */
	public boolean isNationalProducerNumApplyToParentBand() {
		return nationalProducerNumApplyToParentBand;
	}

	/**
	 * @param nationalProducerNumApplyToParentBand the nationalProducerNumApplyToParentBand to set
	 */
	public void setNationalProducerNumApplyToParentBand(
			boolean nationalProducerNumApplyToParentBand) {
		this.nationalProducerNumApplyToParentBand = nationalProducerNumApplyToParentBand;
	}

	/**
	 * @return the offExchangeDisclaimerApplyToParentBand
	 */
	public boolean isOffExchangeDisclaimerApplyToParentBand() {
		return offExchangeDisclaimerApplyToParentBand;
	}

	/**
	 * @param offExchangeDisclaimerApplyToParentBand the offExchangeDisclaimerApplyToParentBand to set
	 */
	public void setOffExchangeDisclaimerApplyToParentBand(
			boolean offExchangeDisclaimerApplyToParentBand) {
		this.offExchangeDisclaimerApplyToParentBand = offExchangeDisclaimerApplyToParentBand;
	}

	/**
	 * @return the onExchangeDisclaimerApplyToParentBand
	 */
	public boolean isOnExchangeDisclaimerApplyToParentBand() {
		return onExchangeDisclaimerApplyToParentBand;
	}

	/**
	 * @param onExchangeDisclaimerApplyToParentBand the onExchangeDisclaimerApplyToParentBand to set
	 */
	public void setOnExchangeDisclaimerApplyToParentBand(
			boolean onExchangeDisclaimerApplyToParentBand) {
		this.onExchangeDisclaimerApplyToParentBand = onExchangeDisclaimerApplyToParentBand;
	}

	/**
	 * @return the paymentUrlApplyToParentBand
	 */
	public boolean isPaymentUrlApplyToParentBand() {
		return paymentUrlApplyToParentBand;
	}

	/**
	 * @param paymentUrlApplyToParentBand the paymentUrlApplyToParentBand to set
	 */
	public void setPaymentUrlApplyToParentBand(boolean paymentUrlApplyToParentBand) {
		this.paymentUrlApplyToParentBand = paymentUrlApplyToParentBand;
	}

	/**
	 * @return the producerPortalPwdApplyToParentBand
	 */
	public boolean isProducerPortalPwdApplyToParentBand() {
		return producerPortalPwdApplyToParentBand;
	}

	/**
	 * @param producerPortalPwdApplyToParentBand the producerPortalPwdApplyToParentBand to set
	 */
	public void setProducerPortalPwdApplyToParentBand(
			boolean producerPortalPwdApplyToParentBand) {
		this.producerPortalPwdApplyToParentBand = producerPortalPwdApplyToParentBand;
	}

	/**
	 * @return the producerPortalUnameApplyToParentBand
	 */
	public boolean isProducerPortalUnameApplyToParentBand() {
		return producerPortalUnameApplyToParentBand;
	}

	/**
	 * @param producerPortalUnameApplyToParentBand the producerPortalUnameApplyToParentBand to set
	 */
	public void setProducerPortalUnameApplyToParentBand(
			boolean producerPortalUnameApplyToParentBand) {
		this.producerPortalUnameApplyToParentBand = producerPortalUnameApplyToParentBand;
	}

	/**
	 * @return the producerPortalUrlApplyToParentBand
	 */
	public boolean isProducerPortalUrlApplyToParentBand() {
		return producerPortalUrlApplyToParentBand;
	}

	/**
	 * @param producerPortalUrlApplyToParentBand the producerPortalUrlApplyToParentBand to set
	 */
	public void setProducerPortalUrlApplyToParentBand(
			boolean producerPortalUrlApplyToParentBand) {
		this.producerPortalUrlApplyToParentBand = producerPortalUrlApplyToParentBand;
	}

	/**
	 * @return the websiteUrlApplyToParentBand
	 */
	public boolean isWebsiteUrlApplyToParentBand() {
		return websiteUrlApplyToParentBand;
	}

	/**
	 * @param websiteUrlApplyToParentBand the websiteUrlApplyToParentBand to set
	 */
	public void setWebsiteUrlApplyToParentBand(boolean websiteUrlApplyToParentBand) {
		this.websiteUrlApplyToParentBand = websiteUrlApplyToParentBand;
	}

	/**
	 * @return the writingAgentApplyToParentBand
	 */
	public boolean isWritingAgentApplyToParentBand() {
		return writingAgentApplyToParentBand;
	}

	/**
	 * @param writingAgentApplyToParentBand the writingAgentApplyToParentBand to set
	 */
	public void setWritingAgentApplyToParentBand(
			boolean writingAgentApplyToParentBand) {
		this.writingAgentApplyToParentBand = writingAgentApplyToParentBand;
	}

	public Integer getParentBrandId() {
		return parentBrandId;
	}

	public void setParentBrandId(Integer parentBrandId) {
		this.parentBrandId = parentBrandId;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
	
	
}
