package com.getinsured.hix.dto.planmgmt;

import com.getinsured.hix.model.GHIXRequest;

public class IssuerEnrollmentEndDateRequestDTO  extends GHIXRequest {
	private Integer issuerId;

	/**
	 * @return the issuerId
	 */
	public Integer getIssuerId() {
		return issuerId;
	}

	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

}
