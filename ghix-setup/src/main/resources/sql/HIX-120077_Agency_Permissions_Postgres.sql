INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENCY_INFORMATION',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'EDIT_AGENCY_INFORMATION',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENCY_SITE',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'EDIT_AGENCY_SITE',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENCY_SITE_LIST',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'UPLOAD_AGENCY_DOCUMENT',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENCY_DOCUMENT',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENCY_DOC_LIST',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'REMOVE_AGENCY_DOCUMENT',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'EDIT_AGENCY_CERT_STATUS',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENCY_CERT_HISTORY',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'VIEW_AGENT_ROLE',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'MANAGE_AGENCY',null, null);
INSERT INTO permissions (ID,NAME, description, is_default) VALUES (nextval('permissions_seq'),'AGENCY_MANAGE_AGENTS',null, null);