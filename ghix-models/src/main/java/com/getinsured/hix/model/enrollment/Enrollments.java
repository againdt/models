package com.getinsured.hix.model.enrollment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "enrollments")
@XmlAccessorType(XmlAccessType.NONE)
// @XmlType(propOrder = { "txnCreateDateTime", "enrollment" })
public class Enrollments {

	@XmlElement(name = "enrollment")
	private List<Enrollment> enrollmentList;

	@XmlElement(name = "hiosIssuerID")
	private String hiosIssuerId;

	@XmlElement(name = "ISA05")
	private String ISA05;

	@XmlElement(name = "ISA06")
	private String ISA06;

	@XmlElement(name = "ISA07")
	private String ISA07;

	@XmlElement(name = "ISA08")
	private String ISA08;

	@XmlElement(name = "ISA13")
	private Integer ISA13;

	@XmlElement(name = "ISA15")
	private String ISA15;

	@XmlElement(name = "GS02")
	private String GS02;

	@XmlElement(name = "GS03")
	private String GS03;

	@XmlElement(name = "sourceExchgID")
	private String sourceExchgId;

	@XmlElement(name = "jobExecutionId")
	private long jobExecutionId;

	@XmlElement(name = "sendBenefitEndDate")
	private String sendBenefitEndDate;

	@XmlElement(name = "stateCode")
	private String stateCode;

	private Boolean isRenEnrollments = Boolean.FALSE;

    @XmlElement(name = "stateSubsidyEnabled")
	private Boolean isStateSubsidyEnabled = Boolean.FALSE;

	public Integer getISA13() {
		return ISA13;
	}

	public void setISA13(Integer iSA13) {
		ISA13 = iSA13;
	}

	public String getISA05() {
		return ISA05;
	}

	public void setISA05(String iSA05) {
		ISA05 = iSA05;
	}

	public String getISA06() {
		return ISA06;
	}

	public void setISA06(String iSA06) {
		ISA06 = iSA06;
	}

	public String getISA07() {
		return ISA07;
	}

	public void setISA07(String iSA07) {
		ISA07 = iSA07;
	}

	public String getISA08() {
		return ISA08;
	}

	public void setISA08(String iSA08) {
		ISA08 = iSA08;
	}

	public String getISA15() {
		return ISA15;
	}

	public void setISA15(String iSA15) {
		ISA15 = iSA15;
	}

	public String getGS02() {
		return GS02;
	}

	public void setGS02(String gS02) {
		GS02 = gS02;
	}

	public String getGS03() {
		return GS03;
	}

	public void setGS03(String gS03) {
		GS03 = gS03;
	}

	@XmlElement(name = "txnCreateDateTime")
	public String getTxnCreateDateTime() {
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		return (formatter.format(new TSDate()));
	}

	@XmlElement(name = "txnCreateDateTimeZone")
	public String getTxnCreateDateTimeZone() {
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(new TSDate());
		TimeZone timeZone = calendar.getTimeZone();

		String timeZone_Code = timeZone.getDisplayName(false, TimeZone.SHORT);

		if (timeZone_Code.equalsIgnoreCase("PST")) {
			return "PT";
		} else if (timeZone_Code.equalsIgnoreCase("MST")) {
			return "MT";
		} else if (timeZone_Code.equalsIgnoreCase("CST")) {
			return "CT";
		} else if (timeZone_Code.equalsIgnoreCase("EST")) {
			return "ET";
		}

		return timeZone_Code;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public List<Enrollment> getEnrollmentList() {
		return enrollmentList;
	}

	public void setEnrollmentList(List<Enrollment> enrollmentList) {
		this.enrollmentList = enrollmentList;
	}

	public String getSourceExchgId() {
		return sourceExchgId;
	}

	public void setSourceExchgId(String sourceExchgId) {
		this.sourceExchgId = sourceExchgId;
	}

	public long getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	public String getSendBenefitEndDate() {
		return sendBenefitEndDate;
	}

	public void setSendBenefitEndDate(String sendBenefitEndDate) {
		this.sendBenefitEndDate = sendBenefitEndDate;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @return the isRenEnrollments
	 */
	public Boolean getIsRenEnrollments() {
		return isRenEnrollments;
	}

	/**
	 * @param isRenEnrollments the isRenEnrollments to set
	 */
	public void setIsRenEnrollments(Boolean isRenEnrollments) {
		this.isRenEnrollments = isRenEnrollments;
	}

	public Boolean getStateSubsidyEnabled() {
		return isStateSubsidyEnabled;
	}

	public void setStateSubsidyEnabled(Boolean stateSubsidyEnabled) {
		isStateSubsidyEnabled = stateSubsidyEnabled;
	}
}
