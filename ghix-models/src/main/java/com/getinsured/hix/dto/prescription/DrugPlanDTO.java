package com.getinsured.hix.dto.prescription;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DrugPlanDTO {
	@SerializedName("step_therapy")
	public Boolean stepTherapy;
	@SerializedName("quantity_limit")
	public Boolean quantityLimit;
	@SerializedName("prior_authorization")
	public Boolean priorAuthorization;
	@SerializedName("plan_id")
	public String planId;
	@SerializedName("years")
	public List<Integer> years;
	@SerializedName("drug_tier")
	public String drugTier;
	public String rxnormId;
	
	public Boolean getStepTherapy() {
		return stepTherapy;
	}
	public void setStepTherapy(Boolean stepTherapy) {
		this.stepTherapy = stepTherapy;
	}
	public Boolean getQuantityLimit() {
		return quantityLimit;
	}
	public void setQuantityLimit(Boolean quantityLimit) {
		this.quantityLimit = quantityLimit;
	}
	public Boolean getPriorAuthorization() {
		return priorAuthorization;
	}
	public void setPriorAuthorization(Boolean priorAuthorization) {
		this.priorAuthorization = priorAuthorization;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public List<Integer> getYears() {
		return years;
	}
	public void setYears(List<Integer> years) {
		this.years = years;
	}
	public String getDrugTier() {
		return drugTier;
	}
	public void setDrugTier(String drugTier) {
		this.drugTier = drugTier;
	}
	public String getKey() {
		StringBuffer sb = new StringBuffer();
		sb.append(stepTherapy ? "Y" : "N");
		sb.append(priorAuthorization ? "Y" : "N");
		sb.append((years != null) ? years.get(0) : "0000");
		sb.append(drugTier);
		return sb.toString(); 
	}
	public String getRxnormId() {
		return rxnormId;
	}
	public void setRxnormId(String rxnormId) {
		this.rxnormId = rxnormId;
	}
}
