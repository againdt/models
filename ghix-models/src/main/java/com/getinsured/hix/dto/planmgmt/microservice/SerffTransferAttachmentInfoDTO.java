package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.Date;

import com.getinsured.hix.platform.util.DateUtil;

public class SerffTransferAttachmentInfoDTO {

	private Long attachmentID;
	private String documentId;
	private String documentName;
	private String documentType;
	private Long documentSize;
	private String createdOn;
	private boolean downloadable;
	
	public Long getAttachmentID() {
		return attachmentID;
	}
	public void setAttachmentID(Long attachmentID) {
		this.attachmentID = attachmentID;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public Long getDocumentSize() {
		return documentSize;
	}
	public void setDocumentSize(Long documentSize) {
		this.documentSize = documentSize;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		if (null != createdOn) {
			this.createdOn = DateUtil.dateToString(createdOn, "MMM dd, yyyy");
		}
	}
	public boolean isDownloadable() {
		return downloadable;
	}
	public void setDownloadable(boolean downloadable) {
		this.downloadable = downloadable;
	}
	
}
