INSERT INTO ROLES (ID,CREATED,DESCRIPTION,LABEL,LANDING_PAGE,MAX_RETRY_COUNT,NAME,UPDATED,POST_REG_URL,PRIVILEGED) 
			values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ASSISTERENROLLMENTENTITYADMIN','Entity Administrator','/entity/entityadmin/managelist',3,'ASSISTERENROLLMENTENTITYADMIN',SYSTIMESTAMP,'/entity/entityadmin/managelist',1);
INSERT INTO ROLES (ID,CREATED,DESCRIPTION,LABEL,LANDING_PAGE,MAX_RETRY_COUNT,NAME,UPDATED,POST_REG_URL,PRIVILEGED) 
			values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ASSISTERENROLLMENTENTITY','Entity','/entity/enrollmententity/entityinformation',3,'ASSISTERENROLLMENTENTITY',SYSTIMESTAMP,'/enrollmententity/registration',0);
			
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='EEADMIN_MANAGE_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='EEADMIN_MANAGE_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='EEADMIN_VIEW_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='EEADMIN_EDIT_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='EEADMIN_VIEW_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='EEADMIN_EDIT_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ENTITY_MANAGE_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ENTITY_MANAGE_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ENTITY_VIEW_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ENTITY_EDIT_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ENTITY_VIEW_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ENTITY_EDIT_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ASSISTER_MANAGE_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ASSISTER_EDIT_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name='ASSISTER_VIEW_ASSISTER'));


INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='EEADMIN_MANAGE_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='EEADMIN_VIEW_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='EEADMIN_EDIT_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_MANAGE_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_MANAGE_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_VIEW_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_EDIT_ENTITY'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_VIEW_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_EDIT_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_VIEW_INDIVIDUAL'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_EDIT_INDIVIDUAL'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ASSISTER_MANAGE_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ASSISTER_EDIT_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ASSISTER_VIEW_ASSISTER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_ASSISTER_APPROVE_USER'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITY'), (select id from permissions   where name='ENTITY_ASSISTER_DECLINE_USER'));

