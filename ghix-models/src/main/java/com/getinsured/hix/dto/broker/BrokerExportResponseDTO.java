package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;


public class BrokerExportResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<BrokerExportDTO> brokerExportDTO;
	private Integer responseCode;
    private String responseDesc;
    
	public List<BrokerExportDTO> getBrokerExportDTO() {
		return brokerExportDTO;
	}
	public void setBrokerExportDTO(List<BrokerExportDTO> brokerExportDTO) {
		this.brokerExportDTO = brokerExportDTO;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDesc() {
		return responseDesc;
	}
	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

}
