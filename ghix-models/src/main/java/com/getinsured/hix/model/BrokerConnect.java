package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.getinsured.timeshift.sql.TSTimestamp;
import org.hibernate.annotations.Type;

/**
 * The persistent class for the Broker database table.
 *
 */
@Entity
@Table(name = "broker_connect")
public class BrokerConnect implements Serializable {
    public enum Status {
        INACTIVE, ACTIVE
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BROKER_CONNECT_SEQ")
    @SequenceGenerator(name = "BROKER_CONNECT_SEQ", sequenceName = "BROKER_CONNECT_SEQ", allocationSize = 1)
    private int id;

    @OneToOne
    @JoinColumn(name="BROKER_ID", nullable=false)
    private Broker brokerId;

    @Column(name="PHONE_NUMBER")
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(name="STATUS")
    private Status status;

    @Type(type = "numeric_boolean")
    @Column(name="IS_AVAILABLE")
    private boolean isAvailable;

    @Column(name="CREATION_TIMESTAMP")
    private Timestamp creationTimestamp;

    @Column(name="LAST_UPDATE_TIMESTAMP")
    private Timestamp lastUpdateTimestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Broker getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(Broker brokerId) {
        this.brokerId = brokerId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Timestamp getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    /* To AutoUpdate created and updated dates while persisting object */
    @PrePersist
    public void PrePersist() {
        this.setCreationTimestamp(new TSTimestamp());
        this.setLastUpdateTimestamp(new TSTimestamp());
    }

    /* To AutoUpdate updated dates while updating object */
    @PreUpdate
    public void PreUpdate() {
        this.setLastUpdateTimestamp(new TSTimestamp());
    }
}
