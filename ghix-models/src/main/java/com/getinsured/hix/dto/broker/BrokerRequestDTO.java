package com.getinsured.hix.dto.broker;

import com.getinsured.hix.model.DesignateBroker.Status;

public class BrokerRequestDTO {

	private Integer id;
	private Integer individualId;
	private Status desigStatus;
	private String esignName;
	private Integer userId;

	public Integer getIndividualId() {
		return individualId;
	}

	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Status getDesigStatus() {
		return desigStatus;
	}

	public void setDesigStatus(Status desigStatus) {
		this.desigStatus = desigStatus;
	}

	public String getEsignName() {
		return esignName;
	}

	public void setEsignName(String esignName) {
		this.esignName = esignName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
