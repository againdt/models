package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.consumer.Household;

public class HouseholdResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<Household> householdList = new ArrayList<Household>();

	public List<Household> getHouseholdList() {
		return householdList;
	}

	public void setHouseholdList(List<Household> householdList) {
		this.householdList = householdList;
	}
	

}
