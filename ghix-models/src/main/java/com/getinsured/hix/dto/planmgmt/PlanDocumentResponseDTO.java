package com.getinsured.hix.dto.planmgmt;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO class is used to get response of Plan Documents.
 */
public class PlanDocumentResponseDTO {

	private Map<String, String> failedPlanIdsAndErrorMap;

	public PlanDocumentResponseDTO() {
	}

	public Map<String, String> getFailedPlanIdsAndErrorMap() {
		return failedPlanIdsAndErrorMap;
	}

	public void setFailedPlanIdsAndError(String key, String value) {

		if (null == failedPlanIdsAndErrorMap) {
			failedPlanIdsAndErrorMap = new HashMap<String, String>();
		}
		failedPlanIdsAndErrorMap.put(key, value);
	}
}
