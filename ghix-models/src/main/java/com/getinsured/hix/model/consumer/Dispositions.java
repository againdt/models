package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

import com.getinsured.hix.model.LookupValue;


/**
 * The persistent class for the CMR_HOUSEHOLD database table.
 * 
 */

@Audited
@Entity
@Table(name="Dispositions")
@DynamicUpdate
@DynamicInsert
public class Dispositions implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DISPOSITIONS_SEQ")
	@SequenceGenerator(name = "DISPOSITIONS_SEQ", sequenceName = "DISPOSITIONS_SEQ", allocationSize = 1)
	private int id;
	

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable=false)
	private Date created;
	
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;

	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "effective_year")
	private String effectiveYear;
	
	@Column(name="module_id")
	private Integer moduleId;
	
	@Column(name = "module_name")
	private String moduleName;
	
	@OneToOne
    @JoinColumn(name="lookup_value_id",nullable= true)
	private LookupValue codeTypeLkp;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEffectiveYear() {
		return effectiveYear;
	}

	public void setEffectiveYear(String effectiveYear) {
		this.effectiveYear = effectiveYear;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public LookupValue getCodeTypeLkp() {
		return codeTypeLkp;
	}

	public void setCodeTypeLkp(LookupValue codeTypeLkp) {
		this.codeTypeLkp = codeTypeLkp;
	}

}
