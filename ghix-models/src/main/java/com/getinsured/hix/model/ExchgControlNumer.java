package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the EXCHG_FilePatternRole database table.
 * 
 */
@Entity
@Table(name="EXCHG_ControlNum")
public class ExchgControlNumer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXCHG_ControlNum_seq")
	@SequenceGenerator(name = "EXCHG_ControlNum_seq", sequenceName = "EXCHG_ControlNum_seq", allocationSize = 1)
	private int id;

	@Column(name="HIOS_Issuer_ID")
	private String hiosIssuerId;

	@Column(name="Control_Num")
	private Integer controlnumber;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public Integer getControlnumber() {
		return controlnumber;
	}

	public void setControlnumber(Integer controlnumber) {
		this.controlnumber = controlnumber;
	}


}