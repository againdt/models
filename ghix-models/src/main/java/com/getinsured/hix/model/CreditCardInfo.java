package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.envers.Audited;


/**
 * The persistent class for the credit_card_info database table.
 */

@Audited
@TypeDef(name = "vimoEncryptedString", typeClass = VimoEncryptedStringType.class)
@Entity
@Table(name = "credit_card_info")
public class CreditCardInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum cardTypes {
        mastercard(002), visa(001), americanexpress(003);
        private int code;

        private cardTypes(int c) {
            code = c;
        }

        public int getCode() {
            return code;
        }
    };

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CreditCardInfo_Seq")
    @SequenceGenerator(name = "CreditCardInfo_Seq", sequenceName = "credit_card_info_seq", allocationSize = 1)
    private int id;
    
    @Column(name = "name_on_card")
    private String nameOnCard;

    @Column(name = "card_number")
    @Type(type = "vimoEncryptedString")
    private String cardNumber;

    @Column(name = "card_type")
    private String cardType;

    @Column(name = "expiration_month")
    private String expirationMonth;

    @Column(name = "expiration_year")
    private String expirationYear;
    
    /*Code commented please refer Jira HIX-8499 */
    /*@Column(name = "secure_token")
    private String secureToken;*/

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATION_TIMESTAMP")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATE_TIMESTAMP")
    private Date updated;
    
    @Column(name = "last_updated_by")
    private Integer lastUpdatedBy;

    public CreditCardInfo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    /*public String getSecureToken() {
		return secureToken;
	}

	public void setSecureToken(String secureToken) {
		this.secureToken = secureToken;
	}*/

	public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /* To AutoUpdate created and updated dates while persisting object */
    @PrePersist
    public void PrePersist() {
        this.setCreated(new TSDate());
        this.setUpdated(new TSDate());
    }

    /* To AutoUpdate updated dates while updating object */
    @PreUpdate
    public void PreUpdate() {
        this.setUpdated(new TSDate());
    }

    /**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("cardType=").append(cardType).append(", \n");
        sb.append("nameOnCard=").append(nameOnCard).append(", \n");
        sb.append("cardNumber=").append(cardNumber).append(", \n");
        sb.append("expirationMonth=").append(expirationMonth).append(", \n");
        sb.append("expirationYear=").append(expirationYear).append(", \n");
        return sb.toString();
    }

}
