package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class People{
  @JsonProperty("incomes")
  
  private List<Incomes> incomes;
  @JsonProperty("firstName")
  
  private String firstName;
  @JsonProperty("lastName")
  
  private String lastName;
  @JsonProperty("smoker")
  
  private Boolean smoker;
  @JsonProperty("gender")
  
  private Integer gender;
  @JsonProperty("socialSecurityNumber")
  
  private String socialSecurityNumber;
  @JsonProperty("needsCoverage")
  
  private Boolean needsCoverage;
  @JsonProperty("relationship")
  
  private Integer relationship;
  @JsonProperty("birthDate")
  
  private String birthDate;
  public void setIncomes(List<Incomes> incomes){
   this.incomes=incomes;
  }
  public List<Incomes> getIncomes(){
   return incomes;
  }
  public void setFirstName(String firstName){
   this.firstName=firstName;
  }
  public String getFirstName(){
   return firstName;
  }
  public void setLastName(String lastName){
   this.lastName=lastName;
  }
  public String getLastName(){
   return lastName;
  }
  public void setSmoker(Boolean smoker){
   this.smoker=smoker;
  }
  public Boolean getSmoker(){
   return smoker;
  }
  public void setGender(Integer gender){
   this.gender=gender;
  }
  public Integer getGender(){
   return gender;
  }
  public void setSocialSecurityNumber(String socialSecurityNumber){
   this.socialSecurityNumber=socialSecurityNumber;
  }
  public String getSocialSecurityNumber(){
   return socialSecurityNumber;
  }
  public void setNeedsCoverage(Boolean needsCoverage){
   this.needsCoverage=needsCoverage;
  }
  public Boolean getNeedsCoverage(){
   return needsCoverage;
  }
  public void setRelationship(Integer relationship){
   this.relationship=relationship;
  }
  public Integer getRelationship(){
   return relationship;
  }
  public void setBirthDate(String birthDate){
   this.birthDate=birthDate;
  }
  public String getBirthDate(){
   return birthDate;
  }
}