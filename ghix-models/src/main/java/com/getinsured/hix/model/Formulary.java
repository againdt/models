/**
 * 
 */
package com.getinsured.hix.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.getinsured.timeshift.TimeShifterUtil;

/**
 * Persistence class 'Formulary' mapping to table 'FORMULARY'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 15, 2013 3:59:41 PM 
 */
@Entity
@Table(name = "FORMULARY")
public class Formulary {
	
    @Id
	@SequenceGenerator(name = "Formulary_Seq", sequenceName = "FORMULARY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Formulary_Seq")
    private int id;
    
    @Column(name = "FORMULARY_ID")
    @Length(max = 200)
	@NotNull
    private String formularyId;
    
    @Column(name="FORMULARY_URL")
    @Length(max = 800)
	@NotNull
    private String formularyUrl;
    
    @ManyToOne
    @JoinColumn(name="ISSUER_ID", referencedColumnName = "ID")
	private Issuer issuer;
    
    @ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "DRUG_LIST_ID", referencedColumnName = "ID")
	private DrugList drugList;

    @Column(name="NUMBER_OF_TIERS")
    private Integer noOfTiers;    
    
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@NotNull
	private Timestamp lastUpdateTimestamp;
	
	@Column(name = "APPLICABLE_YEAR")
	private Integer applicableYear;
	
	/**
	 * Default constructor.
	 */
	public Formulary() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis())); 
	}

	/**
	 * Getter for 'id'.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter for 'id'.
	 *
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter for 'formularyId'.
	 *
	 * @return the formularyId
	 */
	public String getFormularyId() {
		return formularyId;
	}

	/**
	 * Setter for 'formularyId'.
	 *
	 * @param formularyId the formularyId to set
	 */
	public void setFormularyId(String formularyId) {
		this.formularyId = formularyId;
	}

	/**
	 * Getter for 'formularyUrl'.
	 *
	 * @return the formularyUrl
	 */
	public String getFormularyUrl() {
		return formularyUrl;
	}

	/**
	 * Setter for 'formularyUrl'.
	 *
	 * @param formularyUrl the formularyUrl to set
	 */
	public void setFormularyUrl(String formualryUrl) {
		this.formularyUrl = formualryUrl;
	}

	/**
	 * Getter for 'issuer'.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Setter for 'issuer'.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}	
	

	/**
	 * Getter for 'creationTimestamp'
	 * 
	 * @return the creationTimestamp
	 */
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * Setter for 'creationTimestamp'
	 * 
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * Getter for 'lastUpdateTimestamp
	 * 
	 * @return the lastUpdateTimestamp
	 */
	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	/**
	 * Setter for 'lastUpdateTimestamp'
	 * 
	 * @param lastUpdateTimestamp the lastUpdateTimestamp to set
	 */
	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	/**
	 * 
	 * Getter for 'drugList'
	 * 
	 * @return drugList
	 */
	public DrugList getDrugList() {
		return drugList;
	}

	/**
	 * Setter for 'drugList'
	 * @param drugList drugList to set.
	 */
	public void setDrugList(DrugList drugList) {
		this.drugList = drugList;
	}

	/**
	 * 
	 * Getter for 'noOfTiers'
	 *  
	 * @return noOfTiers
	 */
	public Integer getNoOfTiers() {
		return noOfTiers;
	}

	/**
	 * Setter for 'noOfTiers'
	 * 
	 * @param noOfTiers no of tiers to set.
	 */
	public void setNoOfTiers(Integer noOfTiers) {
		this.noOfTiers = noOfTiers;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}
}
