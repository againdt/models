
package com.serff.planmanagementserffapi.serff.model.service;

import com.getinsured.hix.model.GHIXRequest;


public class UpdateExchangeWorkflowStatus extends GHIXRequest {
	protected String issuerId; 	/** HIOS Issuer Identifier*/
    protected String exchangePlanId;
    protected String exchangeWorkflowStatus;

    /**
     * Gets the value of the 'issuerId' property.
     * 
     * @return issuerId
     */
	public String getIssuerId() {
		return issuerId;
	}

    /**
     * Sets the value of the 'issuerId' property.
     * 
     * @param issuerId The HIOS Issuer ID
     */
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

    /**
     * Gets the value of the exchangePlanId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangePlanId() {
        return exchangePlanId;
    }

    /**
     * Sets the value of the exchangePlanId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangePlanId(String value) {
        this.exchangePlanId = value;
    }

    /**
     * Gets the value of the exchangeWorkflowStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeWorkflowStatus() {
        return exchangeWorkflowStatus;
    }

    /**
     * Sets the value of the exchangeWorkflowStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeWorkflowStatus(String value) {
        this.exchangeWorkflowStatus = value;
    }

}
