package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.DateAdapter;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.TenantIdPrePersistListener;
import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;

/**
 * The persistent class for the roles database table.
 * 
 */
@Audited
@Entity
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "ENROLLEE")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class Enrollee implements Serializable, SortableEntity {
	private static final long serialVersionUID = 1L;

	@XmlElement(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Enrollee_Seq")
	@SequenceGenerator(name = "Enrollee_Seq", sequenceName = "ENROLLEE_SEQ", allocationSize = 1)
	private int id;
	
	@Transient
	@XmlElement(name="healthCoverage")
	private HealthCoverage healthCoverage;

	@Transient
	@XmlElement(name="memberReportingCategory")
	private MemberReportingCategory memberReportingCategory;

	@ManyToOne
	@JoinColumn(name = "ENROLLMENT_ID")
	private Enrollment enrollment;

	@XmlElement(name="SSN")
	@Column(name = "TAX_ID_NUMBER")
	private String taxIdNumber;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "LAST_EVENT_ID")
	private EnrollmentEvent lastEventId;

	@Transient
	private String formatUpdatedOn;
	@Transient
	private String formatCreatedOn;
	@Transient
	private String formatBirthDate;
	@Transient
	private String formatEffectiveDate;

	@Transient
	private String formatEffectiveEndDate;

	@Transient
	private String formatmaintenanceEffectiveDate;

	@Transient
	private String formatlastPremiumPaidDate;

	@Transient
	@XmlElement(name="subscriberFlag")
	private String subscriberNewFlag;

	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "MEMBER_EMPLOYER_ID", nullable = true)
	private Employer employer;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="relationshipLkp")
	@OneToOne
	@JoinColumn(name = "RELATIONSHIP_HCP_LKP")
	private LookupValue relationshipToHCPLkp;

	@Transient
	@XmlElement(name="relationshipToSubscriber")
	private LookupValue relationshipToSubscriberLkp;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="enrolleeLkpValue")
	@OneToOne
	@JoinColumn(name = "ENROLLEE_STATUS_LKP")
	private LookupValue enrolleeLkpValue;

	@XmlElement(name="exchgIndivIdentifier")
	@Column(name = "EXCHG_INDIV_IDENTIFIER")
	private String exchgIndivIdentifier;

	@XmlElement(name="issuerIndivIdentifier")
	@Column(name = "ISSUER_INDIV_IDENTIFIER")
	private String issuerIndivIdentifier;

	@XmlElement(name="healthCoveragePolicyNo")
	@Column(name = "HEALTH_COVERAGE_POLICY_NO")
	private String healthCoveragePolicyNo;

	@XmlElement(name="benefitEffectiveBeginDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	@Column(name = "EFFECTIVE_START_DATE")
	private Date effectiveStartDate;

	@XmlElement(name="benefitEffectiveEndDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	@Column(name = "EFFECTIVE_END_DATE")
	private Date effectiveEndDate;

	@XmlElement(name="lastName")
	@Column(name = "LAST_NAME")
	private String lastName;

	@XmlElement(name="firstName")
	@Column(name = "FIRST_NAME")
	private String firstName;

	@XmlElement(name="middleName")
	@Column(name = "MIDDLE_NAME")
	private String middleName;

	@XmlElement(name="nameSuffix")
	@Column(name = "SUFFIX")
	private String suffix;

	@XmlElement(name="nationalIndivID")
	@Column(name = "NATIONAL_INDIV_IDENTIFIER")
	private String nationalIndividualIdentifier;

	@XmlElement(name="primaryPhoneNo")
	@Column(name = "PRIMARY_PHONE_NO")
	private String primaryPhoneNo;

	@XmlElement(name="secondaryPhoneNo")
	@Column(name = "SECONDARY_PHONE_NO")
	private String secondaryPhoneNo;


	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="languageSpokenLkp")
	@OneToOne
	@JoinColumn(name = "LANGUAGE_SPOKEN_LKP")
	private LookupValue languageLkp;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="languageWrittenLkp")
	@OneToOne
	@JoinColumn(name = "LANGUAGE_WRITTEN_LKP")
	private LookupValue languageWrittenLkp;

	@XmlElement(name="preferredSMS")
	@Column(name = "PREF_SMS")
	private String preferredSMS;

	@XmlElement(name="preferredEmail")
	@Column(name = "PREF_EMAIL")
	private String preferredEmail;

	@XmlElement(name="homeAddress")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "HOME_ADDRESS_ID")
	private Location homeAddressid;


	@XmlElement(name="birthDate")
	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@XmlElement(name="totalIndivResponsibilityAmt")
	@Column(name = "TOTAL_INDV_RESPONSIBILITY_AMT")
	private Float totalIndvResponsibilityAmt;

	@XmlElement(name="healthCoveragePremiumDate")
	@Column(name="HEALTH_COV_PREM_EFF_DATE")
	private Date totIndvRespEffDate;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="genderLkp")
	@OneToOne
	@JoinColumn(name = "GENDER_LKP")
	private LookupValue genderLkp;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="maritalStatusLkp")
	@OneToOne
	@JoinColumn(name = "MARITAL_STATUS_LKP")
	private LookupValue maritalStatusLkp;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="citizenshipStatusLkp")
	@OneToOne
	@JoinColumn(name = "CITIZENSHIP_STATUS_LKP")
	private LookupValue citizenshipStatusLkp;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="tobaccoUsageLkp")
	@OneToOne 
	@JoinColumn(name = "TOBACCO_USAGE_LKP")
	private LookupValue tobaccoUsageLkp; 

	@XmlElement(name="mailingAddress")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MAILING_ADDRESS_ID")
	private Location mailingAddressId;

	@XmlElement(name="maintenanceEffectiveDate")
	@Column(name = "MAINTENANCE_EFFECTIVE_DATE")
	private Date maintenanceEffectiveDate;

	@XmlElement(name="ratingArea")
	@Column(name = "RATING_AREA")
	private String ratingArea;

	@XmlElement(name="lastPremiumPaidDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	@Column(name = "LAST_PREMIUM_PAID_DATE")
	private Date lastPremiumPaidDate;

	@XmlElement(name="TotEmpResponsibilityAmt")
	@Column(name = "TOT_EMP_RESPONSIBILITY_AMT")
	private Float TotEmpResponsibilityAmt;

	@XmlElement(name="idCode")
	@Column(name = "RESP_PERSON_ID_CODE")
	private String respPersonIdCode;

	@XmlElement(name="custodialParent")
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "CUSTODIAL_PARENT_ID")
	private Enrollee custodialParent;

	@NotAudited 
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@XmlElement(name="enrollmentEvents")
	@OneToMany(mappedBy="enrollee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentEvent> enrollmentEvents;

	@XmlElement(name="raceEthnicity")
	@OneToMany(mappedBy="enrollee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrolleeRace> enrolleeRace;

	@OneToMany(mappedBy="sourceEnrollee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrolleeRelationship> enrolleeRelationship;

	@XmlElement(name="createdOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	@XmlElement(name="personType")
	@OneToOne
	@JoinColumn(name = "PERSON_TYPE_LKP")
	private LookupValue personTypeLkp;

//	@NotAudited 
//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Audited
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private AccountUser createdBy;

//	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Audited
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;
	
	@Column(name = "TERMINATION_FLAG")
	private String terminationFlag;

	@XmlElement(name="memberIndivDeathDate")
	@Column(name = "DEATH_DATE")
	private Date deathDate;
	
	@Column(name = "AGE")
	private Integer age;

	@Transient
	@XmlElement(name="formatSignatureDate")
	private Date formatSignatureDate;

	@Transient
	@XmlElement(name="showMailingAddress")
	private String showMailingAddress;

	@Transient
	@XmlElement(name="incorrectMemberFirstName")
	private String incorrectMemberFirstName;

	@Transient
	@XmlElement(name="incorrectMemberLastName")
	private String incorrectMemberLastName;

	@Transient
	@XmlElement(name="incorrectMemberMiddleName")
	private String incorrectMemberMiddleName;

	@Transient
	@XmlElement(name="incorrectMemberNameSuffix")
	private String incorrectMemberNameSuffix;

	@Transient
	@XmlElement(name="incorrectMemberSSN")
	private String incorrectMemberSSN;

	@Transient
	@XmlElement(name="incorrectMemberNationalIndivID")
	private String incorrectMemberNationalIndivID;

	@Transient
	@XmlElement(name="incorrectMemberBirthDate")
	private Date incorrectMemberBirthDate;

	@Transient
	@XmlElement(name="employerGroupNo")
	private String employerGroupNo;

	@Column(name = "LAST_TOBACCO_USE_DATE")
	private Date lastTobaccoUseDate;
	
	@Column(name = "FFM_INSURANCE_APPLICANT_ID")
	private String insuranceAplicantId;
	
	@Column(name = "FFM_PERSON_ID")
	private String ffmPersonId ;
	
	@Column(name = "EMPLOYEE_ID")
	private Integer employeeId;
	
	@Transient
	@XmlElement(name="employeeContribution")
	private Float employeeContribution ;

	@Transient
	@XmlElement(name="aptcAmt")
	private Float aptcAmt ;

	@Transient
	@XmlElement(name="csrAmt")
	private Float csrAmt ;

	@Transient
	@XmlElement(name="healthCoveragePremiumAmt")
	private Float grossPremiumAmt ;

	@Transient
	@XmlElement(name="netPremiumAmt")
	private Float netPremiumAmt ;

	@Transient
	@XmlElement(name="employerContribution")
 	private Float employerContribution ;
	
	@Transient
	@XmlElement(name="aptcDate")
	private Date aptcEffDate;
	
	@Transient
	@XmlElement(name="csrDate")
	private Date csrEffDate;
	
	@Transient
	@XmlElement(name="totalIndivResponsibilityDate")
	private Date netPremEffDate;
	
	@Transient
	@XmlElement(name="totalEmpResponsibilityDate")
	private Date emplContEffDate;
	
	@Transient
	@XmlElement(name="totalPremiumDate")
	private Date grossPremEffDate;
	
	@Transient
	private String maintainenceReasonCode;
	
	@XmlElement(name="exchgSubscriberIdentifier")
	@Transient
	private String exchgSubscriberIdentifier;
	
	@XmlElement(name="memberEntityIdentifierCode")
	@Transient
	private String memberEntityIdentifierCode;
	
	@Column(name = "HEIGHT")
	private Float height;
	
	@Column(name = "WEIGHT")
	private Float weight;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	@XmlElement(name="ratingAreaEffDate")
	@Column(name = "RATING_AREA_EFF_DATE")
	private Date ratingAreaEffDate;
	
	@Transient
	private Long existingHealthEnrollmentId;

	@Transient
	private Long existingDentalEnrollmentId;
	
	@Transient
	private Float lastSliceTotalIndvRespAmt;
	
	@Transient
	private boolean ratingAreaChanged;
	
	@XmlElement(name="premium")
	@Transient
	private List<EnrollmentPremium> enrollmentPremiums;
	
	@Transient
	@XmlElement(name="exchgAssignedPolicyID")
	private String exchgAssignedPolicyID;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "DISENROLL_TIMESTAMP")
	private Date disenrollTimestamp;
	
	@Transient
	private  boolean newMember=false;
	
	@Transient
	private boolean eventCreated=false;
	
	@Transient
	private boolean ageUpdated=false;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "QUOTING_DATE")
	private Date quotingDate;
	
	@Column(name = "EXTERNAL_INDIV_IDENTIFIER")
	private String externalIndivId ;
	
	public void setAptcEffDate(Date aptcEffDate) {
		this.aptcEffDate = aptcEffDate;
	}

	public Date getCsrEffDate() {
		return csrEffDate;
	}

	public void setCsrEffDate(Date csrEffDate) {
		this.csrEffDate = csrEffDate;
	}

	public String getTerminationFlag() {
		return terminationFlag;
	}

	public void setTerminationFlag(String terminationFlag) {
		this.terminationFlag = terminationFlag;
	}

	public Date getNetPremEffDate() {
		return netPremEffDate;
	}

	public void setNetPremEffDate(Date netPremEffDate) {
		this.netPremEffDate = netPremEffDate;
	}

	public Date getEmplContEffDate() {
		return emplContEffDate;
	}

	public void setEmplContEffDate(Date emplContEffDate) {
		this.emplContEffDate = emplContEffDate;
	}

	public Date getGrossPremEffDate() {
		return grossPremEffDate;
	}

	public void setGrossPremEffDate(Date grossPremEffDate) {
		this.grossPremEffDate = grossPremEffDate;
	}

	public String getEmployerGroupNo() {
		return employerGroupNo;
	}
	
	public void setEmployerGroupNo(String employerGroupNo) {
		this.employerGroupNo = employerGroupNo;
	}

	@Column(name = "ENROLLMENT_REASON")
	private Character enrollmentReason;

	
	
	public Date getTotIndvRespEffDate() {
		return totIndvRespEffDate;
	}

	public void setTotIndvRespEffDate(Date totIndvRespEffDate) {
		this.totIndvRespEffDate = totIndvRespEffDate;
	}

	public String getIncorrectMemberFirstName() {
		return incorrectMemberFirstName;
	}

	public void setIncorrectMemberFirstName(String incorrectMemberFirstName) {
		this.incorrectMemberFirstName = incorrectMemberFirstName;
	}

	public String getIncorrectMemberLastName() {
		return incorrectMemberLastName;
	}

	public void setIncorrectMemberLastName(String incorrectMemberLastName) {
		this.incorrectMemberLastName = incorrectMemberLastName;
	}

	public String getIncorrectMemberMiddleName() {
		return incorrectMemberMiddleName;
	}

	public void setIncorrectMemberMiddleName(String incorrectMemberMiddleName) {
		this.incorrectMemberMiddleName = incorrectMemberMiddleName;
	}

	public String getIncorrectMemberNameSuffix() {
		return incorrectMemberNameSuffix;
	}

	public void setIncorrectMemberNameSuffix(String incorrectMemberNameSuffix) {
		this.incorrectMemberNameSuffix = incorrectMemberNameSuffix;
	}

	public String getIncorrectMemberSSN() {
		return incorrectMemberSSN;
	}

	public void setIncorrectMemberSSN(String incorrectMemberSSN) {
		this.incorrectMemberSSN = incorrectMemberSSN;
	}

	public String getIncorrectMemberNationalIndivID() {
		return incorrectMemberNationalIndivID;
	}

	public void setIncorrectMemberNationalIndivID(
			String incorrectMemberNationalIndivID) {
		this.incorrectMemberNationalIndivID = incorrectMemberNationalIndivID;
	}

	public Date getIncorrectMemberBirthDate() {
		return incorrectMemberBirthDate;
	}

	public void setIncorrectMemberBirthDate(Date incorrectMemberBirthDate) {
		this.incorrectMemberBirthDate = incorrectMemberBirthDate;
	}

	public LookupValue getIncorrectMemberGenderLkp() {
		return incorrectMemberGenderLkp;
	}

	public void setIncorrectMemberGenderLkp(LookupValue incorrectMemberGenderLkp) {
		this.incorrectMemberGenderLkp = incorrectMemberGenderLkp;
	}

	public LookupValue getIncorrectMemberMaritalStatusLkp() {
		return incorrectMemberMaritalStatusLkp;
	}

	public void setIncorrectMemberMaritalStatusLkp(
			LookupValue incorrectMemberMaritalStatusLkp) {
		this.incorrectMemberMaritalStatusLkp = incorrectMemberMaritalStatusLkp;
	}

	public List<EnrolleeRace> getIncorrectMemberRaceEthnicityLkp() {
		return incorrectMemberRaceEthnicityLkp;
	}

	public void setIncorrectMemberRaceEthnicityLkp(
			List<EnrolleeRace> incorrectMemberRaceEthnicityLkp) {
		this.incorrectMemberRaceEthnicityLkp = incorrectMemberRaceEthnicityLkp;
	}

	public LookupValue getIncorrectMemberCitizenshipStatusLkp() {
		return incorrectMemberCitizenshipStatusLkp;
	}

	public void setIncorrectMemberCitizenshipStatusLkp(
			LookupValue incorrectMemberCitizenshipStatusLkp) {
		this.incorrectMemberCitizenshipStatusLkp = incorrectMemberCitizenshipStatusLkp;
	}

	@Transient
	@XmlElement(name="incorrectMemberGenderLkp")
	private LookupValue incorrectMemberGenderLkp;

	@Transient
	@XmlElement(name="incorrectMemberMaritalStatusLkp")
	private LookupValue incorrectMemberMaritalStatusLkp;


	@Transient
	@XmlElement(name="incorrectMemberRaceEthnicityLkp")
	private List<EnrolleeRace> incorrectMemberRaceEthnicityLkp;

	@Transient
	@XmlElement(name="incorrectMemberCitizenshipStatusLkp")
	private LookupValue incorrectMemberCitizenshipStatusLkp;

	@Transient
	private int rev;
	
	@XmlElement(name="renewalStatus")
	@Transient
	private String renewalStatus;
	
	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public String getShowMailingAddress() {
		return showMailingAddress;
	}

	public void setShowMailingAddress(String showMailingAddress) {
		this.showMailingAddress = showMailingAddress;
	}

	public String getFormatSignatureDate() {
		return getFormatedDate(formatSignatureDate);
	}

	public void setFormatSignatureDate(Date formatSignatureDate) {
		this.formatSignatureDate = formatSignatureDate;
	}

	@XmlElement(name="issuerSubscriberIdentifier")
	@Transient
	private String issuerSubscriberIdentifier;

	public List<EnrolleeRelationship> getEnrolleeRelationship() {
		return enrolleeRelationship;
	}

	public void setEnrolleeRelationship(
			List<EnrolleeRelationship> enrolleeRelationship) {
		this.enrolleeRelationship = enrolleeRelationship;
	}

	public LookupValue getPersonTypeLkp() {
		return personTypeLkp;
	}

	public void setPersonTypeLkp(LookupValue personTypeLkp) {
		this.personTypeLkp = personTypeLkp;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}
	public String getFormatEffectiveEndDate() {
		return getFormatedDate(this.effectiveEndDate);
	}

	public void setFormatEffectiveEndDate(String formatEffectiveEndDate) {
		this.formatEffectiveEndDate = formatEffectiveEndDate;
	}
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public LookupValue getRelationshipToHCPLkp() {
		return relationshipToHCPLkp;
	}

	public void setRelationshipToHCPLkp(LookupValue relationshipToHCPLkp) {
		this.relationshipToHCPLkp = relationshipToHCPLkp;
	}

	public Float getTotalIndvResponsibilityAmt() {
		return totalIndvResponsibilityAmt;
	}

	public void setTotalIndvResponsibilityAmt(Float totalIndvResponsibilityAmt) {
		this.totalIndvResponsibilityAmt = totalIndvResponsibilityAmt;
	}


	public List<EnrolleeRace> getEnrolleeRace() {
		return enrolleeRace;
	}

	public void setEnrolleeRace(List<EnrolleeRace> enrolleeRace) {
		this.enrolleeRace = enrolleeRace;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getMaintenanceEffectiveDate() {
		return maintenanceEffectiveDate;
	}

	public void setMaintenanceEffectiveDate(Date maintenanceEffectiveDate) {
		this.maintenanceEffectiveDate = maintenanceEffectiveDate;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public Date getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}

	public void setLastPremiumPaidDate(Date lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}

	public Float getTotEmpResponsibilityAmt() {
		return TotEmpResponsibilityAmt;
	}

	public void setTotEmpResponsibilityAmt(Float totEmpResponsibilityAmt) {
		TotEmpResponsibilityAmt = totEmpResponsibilityAmt;
	}

	public String getRespPersonIdCode() {
		return respPersonIdCode;
	}
	public String getNationalIndividualIdentifier() {
		return nationalIndividualIdentifier;
	}

	public void setNationalIndividualIdentifier(String nationalIndividualIdentifier) {
		this.nationalIndividualIdentifier = nationalIndividualIdentifier;
	}

	public void setRespPersonIdCode(String respPersonIdCode) {
		this.respPersonIdCode = respPersonIdCode;
	}

	public Enrollee getCustodialParent() {
		return custodialParent;
	}

	public void setCustodialParent(Enrollee custodialParent) {
		this.custodialParent = custodialParent;
	}


	public List<EnrollmentEvent> getEnrollmentEvents() {
		return enrollmentEvents;
	}

	public void setEnrollmentEvents(List<EnrollmentEvent> enrollmentEvents) {
		this.enrollmentEvents = enrollmentEvents;
	}


	public Enrollee() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}
	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public char getSubscriberFlag() {
		char subscriberFlag;
		if (getPersonTypeLkp().getLookupValueCode().equals("SUBSCRIBER")){
			subscriberFlag = 'Y';
		}
		else{
			subscriberFlag = 'N';
		}
		return subscriberFlag;
	}

	public LookupValue getEnrolleeLkpValue() {
		return enrolleeLkpValue;
	}

	public void setEnrolleeLkpValue(LookupValue enrolleeLkpValue) {
		this.enrolleeLkpValue = enrolleeLkpValue;
	}

	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivDepIdentifier) {
		this.exchgIndivIdentifier = exchgIndivDepIdentifier;
	}

	public String getIssuerIndivIdentifier() {
		return issuerIndivIdentifier;
	}

	public void setIssuerIndivIdentifier(String issuerIndivDepIdentifier) {
		this.issuerIndivIdentifier = issuerIndivDepIdentifier;
	}

	public String getHealthCoveragePolicyNo() {
		return healthCoveragePolicyNo;
	}

	public void setHealthCoveragePolicyNo(String healthCoveragePolicyNo) {
		this.healthCoveragePolicyNo = healthCoveragePolicyNo;
	}
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if(StringUtils.isNotEmpty(lastName)){
			lastName = lastName.trim();
		}
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if(StringUtils.isNotEmpty(firstName)){
			firstName = firstName.trim();
		}
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		if(StringUtils.isNotEmpty(middleName)){
			middleName = middleName.trim();
		}
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getPrimaryPhoneNo() {
		return primaryPhoneNo;
	}

	public void setPrimaryPhoneNo(String primaryPhoneNo) {
		this.primaryPhoneNo = primaryPhoneNo;
	}

	public String getSecondaryPhoneNo() {
		return secondaryPhoneNo;
	}

	public void setSecondaryPhoneNo(String secondaryPhoneNo) {
		this.secondaryPhoneNo = secondaryPhoneNo;
	}

	/*public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}*/

	public Location getHomeAddressid() {
		return homeAddressid;
	}

	public void setHomeAddressid(Location homeAddressid) {
		this.homeAddressid = homeAddressid;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public LookupValue getGenderLkp() {
		return genderLkp;
	}

	public void setGenderLkp(LookupValue genderLkp) {
		this.genderLkp = genderLkp;
	}

	public LookupValue getMaritalStatusLkp() {
		return maritalStatusLkp;
	}

	public void setMaritalStatusLkp(LookupValue maritalStatusLkp) {
		this.maritalStatusLkp = maritalStatusLkp;
	}

	public LookupValue getCitizenshipStatusLkp() {
		return citizenshipStatusLkp;
	}

	public void setCitizenshipStatusLkp(LookupValue citizenshipStatusLkp) {
		this.citizenshipStatusLkp = citizenshipStatusLkp;
	}

	public LookupValue getTobaccoUsageLkp() {
		return tobaccoUsageLkp;
	}

	public void setTobaccoUsageLkp(LookupValue tobaccoUsageLkp) {
		this.tobaccoUsageLkp = tobaccoUsageLkp;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Location getMailingAddressId() {
		return mailingAddressId;
	}

	public void setMailingAddressId(Location mailingAddressId) {
		this.mailingAddressId = mailingAddressId;
	}

	public String getPreferredSMS() {
		return preferredSMS;
	}

	public LookupValue getLanguageLkp() {
		return languageLkp;
	}

	public void setLanguageLkp(LookupValue languageLkp) {
		this.languageLkp = languageLkp;
	}

	public LookupValue getLanguageWrittenLkp() {
		return languageWrittenLkp;
	}

	public void setLanguageWrittenLkp(LookupValue languageWrittenLkp) {
		this.languageWrittenLkp = languageWrittenLkp;
	}

	public void setPreferredSMS(String preferredSMS) {
		this.preferredSMS = preferredSMS;
	}

	public String getPreferredEmail() {
		return preferredEmail;
	}

	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	public String getFormatUpdatedOn() {
		return getFormatedDate(this.updatedOn);
	}

	public void setFormatUpdatedOn(String formatUpdatedOn) {
		this.formatUpdatedOn = formatUpdatedOn;
	}

	public String getFormatCreatedOn() {
		return getFormatedDate(this.createdOn);
	}

	public void setFormatCreatedOn(String formatCreatedOn) {
		this.formatCreatedOn = formatCreatedOn;
	}

	public String getFormatBirthDate() {
		return getFormatedDate(this.birthDate);
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public void setFormatBirthDate(String formatBirthDate) {
		this.formatBirthDate = formatBirthDate;
	}

	public String getFormatEffectiveDate() {
		return getFormatedDate(this.effectiveStartDate);
	}

	public void setFormatEffectiveDate(String formatEffectiveDate) {
		this.formatEffectiveDate = formatEffectiveDate;
	}

	public String getFormatmaintenanceEffectiveDate() {
		return getFormatedDate(this.maintenanceEffectiveDate);
	}

	public void setFormatmaintenanceEffectiveDate(
			String formatmaintenanceEffectiveDate) {
		this.formatmaintenanceEffectiveDate = formatmaintenanceEffectiveDate;
	}


	public String getFormatlastPremiumPaidDate() {
		return getFormatedDate(this.lastPremiumPaidDate);
	}

	public void setFormatlastPremiumPaidDate(String formatlastPremiumPaidDate) {
		this.formatlastPremiumPaidDate = formatlastPremiumPaidDate;
	}

	public String getFormatedDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmdd");
		String updatedOn = formatter.format(date);
		return updatedOn;

	}

	public String getIssuerSubscriberIdentifier() {
		return issuerSubscriberIdentifier;
	}

	public void setIssuerSubscriberIdentifier(String issuerSubscriberIdentifier) {
		this.issuerSubscriberIdentifier = issuerSubscriberIdentifier;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public Character getEnrollmentReason() {
		return enrollmentReason;
	}

	public void setEnrollmentReason(Character enrollmentReason) {
		this.enrollmentReason = enrollmentReason;
	}

	public EnrollmentEvent getLastEventId() {
		return lastEventId;
	}

	public void setLastEventId(EnrollmentEvent lastEventId) {
		this.lastEventId = lastEventId;
	}

	public LookupValue getRelationshipToSubscriberLkp() {
		return relationshipToSubscriberLkp;
	}

	public void setRelationshipToSubscriberLkp(
			LookupValue relationshipToSubscriberLkp) {
		this.relationshipToSubscriberLkp = relationshipToSubscriberLkp;
	}
	public Date getLastTobaccoUseDate() {
		return lastTobaccoUseDate;
	}

	public void setLastTobaccoUseDate(Date lastTobaccoUseDate) {
		this.lastTobaccoUseDate = lastTobaccoUseDate;
	}

	public Float getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public Float getAptcAmt() {
		return aptcAmt;
	}

	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public Float getCsrAmt() {
		return csrAmt;
	}

	public void setCsrAmt(Float csrAmt) {
		this.csrAmt = csrAmt;
	}

	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}

	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}

	public Float getEmployerContribution() {
		return employerContribution;
	}

	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}

	public String getMaintainenceReasonCode() {
		return maintainenceReasonCode;
	}

	public void setMaintainenceReasonCode(String maintainenceReasonCode) {
		this.maintainenceReasonCode = maintainenceReasonCode;
	}

	public String getInsuranceAplicantId() {
		return insuranceAplicantId;
	}

	public void setInsuranceAplicantId(String insuranceAplicantId) {
		this.insuranceAplicantId = insuranceAplicantId;
	}

	public String getFfmPersonId() {
		return ffmPersonId;
	}

	public void setFfmPersonId(String ffmPersonId) {
		this.ffmPersonId = ffmPersonId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getMemberEntityIdentifierCode() {
		return memberEntityIdentifierCode;
	}

	public void setMemberEntityIdentifierCode(String memberEntityIdentifierCode) {
		this.memberEntityIdentifierCode = memberEntityIdentifierCode;
	}
	
	public String getSubscriberNewFlag() {
		return subscriberNewFlag;
	}

	public void setSubscriberNewFlag(String subscriberNewFlag) {
		this.subscriberNewFlag = subscriberNewFlag;
	}

	public HealthCoverage getHealthCoverage() {
		return healthCoverage;
	}

	public void setHealthCoverage(HealthCoverage healthCoverage) {
		this.healthCoverage = healthCoverage;
	}
	
	public MemberReportingCategory getMemberReportingCategory() {
		return memberReportingCategory;
	}

	public void setMemberReportingCategory(MemberReportingCategory memberReportingCategory) {
		this.memberReportingCategory = memberReportingCategory;
	}

	/**
	 * @return the exchgSubscriberIdentifier
	 */
	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}

	/**
	 * @param exchgSubscriberIdentifier the exchgSubscriberIdentifier to set
	 */
	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getExistingHealthEnrollmentId() {
		return existingHealthEnrollmentId;
	}

	public void setExistingHealthEnrollmentId(Long existingHealthEnrollmentId) {
		this.existingHealthEnrollmentId = existingHealthEnrollmentId;
	}

	
	public Long getExistingDentalEnrollmentId() {
		return existingDentalEnrollmentId;
	}

	public void setExistingDentalEnrollmentId(Long existingDentalEnrollmentId) {
		this.existingDentalEnrollmentId = existingDentalEnrollmentId;
	}

	public String getRenewalStatus() {
		return renewalStatus;
	}

	public void setRenewalStatus(String renewalStatus) {
		this.renewalStatus = renewalStatus;
	}


	public Float getLastSliceTotalIndvRespAmt() {
		return lastSliceTotalIndvRespAmt;
	}

	public void setLastSliceTotalIndvRespAmt(Float lastSliceTotalIndvRespAmt) {
		this.lastSliceTotalIndvRespAmt = lastSliceTotalIndvRespAmt;
	}

	/**
	 * @return the enrollmentPremiums
	 */
	public List<EnrollmentPremium> getEnrollmentPremiums() {
		return enrollmentPremiums;
	}

	/**
	 * @param enrollmentPremiums the enrollmentPremiums to set
	 */
	public void setEnrollmentPremiums(List<EnrollmentPremium> enrollmentPremiums) {
		this.enrollmentPremiums = enrollmentPremiums;
	}

	public Date getDisenrollTimestamp() {
		return disenrollTimestamp;
	}

	public void setDisenrollTimestamp(Date disenrollTimestamp) {
		this.disenrollTimestamp = disenrollTimestamp;
	}

	/**
	 * @return the ratingAreaEffDate
	 */
	public Date getRatingAreaEffDate() {
		return ratingAreaEffDate;
	}

	/**
	 * @param ratingAreaEffDate the ratingAreaEffDate to set
	 */
	public void setRatingAreaEffDate(Date ratingAreaEffDate) {
		this.ratingAreaEffDate = ratingAreaEffDate;
	}

	public boolean isRatingAreaChanged() {
		return ratingAreaChanged;
	}

	public void setRatingAreaChanged(boolean ratingAreaChanged) {
		this.ratingAreaChanged = ratingAreaChanged;
	}

	public boolean isNewMember() {
		return newMember;
	}

	public void setNewMember(boolean newMember) {
		this.newMember = newMember;
	}

	public boolean isEventCreated() {
		return eventCreated;
	}

	public void setEventCreated(boolean eventCreated) {
		this.eventCreated = eventCreated;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public boolean isAgeUpdated() {
		return ageUpdated;
	}

	public void setAgeUpdated(boolean ageUpdated) {
		this.ageUpdated = ageUpdated;
	}

	public String getExchgAssignedPolicyID() {
		return exchgAssignedPolicyID;
	}

	public void setExchgAssignedPolicyID(String exchgAssignedPolicyID) {
		this.exchgAssignedPolicyID = exchgAssignedPolicyID;
	}

	public Date getQuotingDate() {
		return quotingDate;
	}

	public void setQuotingDate(Date quotingDate) {
		this.quotingDate = quotingDate;
	}

	public String getExternalIndivId() {
		return externalIndivId;
	}

	public void setExternalIndivId(String externalIndivId) {
		this.externalIndivId = externalIndivId;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
        columnNames.add("firstName");
        columnNames.add("middleName");
        columnNames.add("lastName");
        columnNames.add("birthDate");
        columnNames.add("taxIdNumber");
        return columnNames;
	}
	
}
