package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.getinsured.timeshift.sql.TSTimestamp;

/**
 * The persistent class for the Broker database table.
 *
 */
@Entity
@Table(name = "broker_connect_hours")
public class BrokerConnectHours implements Serializable {
    public enum Days {
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BROKER_CONNECT_HOURS_SEQ")
    @SequenceGenerator(name = "BROKER_CONNECT_HOURS_SEQ", sequenceName = "BROKER_CONNECT_HOURS_SEQ", allocationSize = 1)
    private int id;

    @ManyToOne
    @JoinColumn(name="BROKER_CONNECT_ID", nullable=false)
    private BrokerConnect brokerConnectId;

    @Enumerated(EnumType.ORDINAL)
    @Column(name="DAY")
    private Days day;

    @Column(name="FROM_TIME")
    private String fromTime;

    @Column(name="TO_TIME")
    private String toTime;

    @Column(name="CREATION_TIMESTAMP")
    private Timestamp creationTimestamp;

    @Column(name="LAST_UPDATE_TIMESTAMP")
    private Timestamp lastUpdateTimestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BrokerConnect getBrokerConnectId() {
        return brokerConnectId;
    }

    public void setBrokerConnectId(BrokerConnect brokerConnectId) {
        this.brokerConnectId = brokerConnectId;
    }

    public Days getDay() {
        return day;
    }

    public void setDay(Days day) {
        this.day = day;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Timestamp getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    /* To AutoUpdate created and updated dates while persisting object */
    @PrePersist
    public void PrePersist() {
        this.setCreationTimestamp(new TSTimestamp());
        this.setLastUpdateTimestamp(new TSTimestamp());
    }

    /* To AutoUpdate updated dates while updating object */
    @PreUpdate
    public void PreUpdate() {
        this.setLastUpdateTimestamp(new TSTimestamp());
    }
}
