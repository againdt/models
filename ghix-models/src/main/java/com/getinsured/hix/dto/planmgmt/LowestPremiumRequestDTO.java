/**
 * 
 * @author santanu
 * @version 1.0
 * @since Aug 22, 2016 
 * 
 * This DTO to accept request object for Lowest Premium Quoting
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.List;
import  com.getinsured.hix.dto.planmgmt.Member;

public class LowestPremiumRequestDTO {

	private String effectiveDate;
	private List<Member> memberList; // member data of household
	private String tenantCode;
	private String insuranceType;
	private String exchangeType;
	private boolean showCatastrophicPlan = false ; 
	private String restrictHiosIds;
	
	
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the memberList
	 */
	public List<Member> getMemberList() {
		return memberList;
	}
	/**
	 * @param memberList the memberList to set
	 */
	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}
	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}
	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	/**
	 * @return the insuranceType
	 */
	public String getInsuranceType() {
		return insuranceType;
	}
	/**
	 * @param insuranceType the insuranceType to set
	 */
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	/**
	 * @return the exchangeType
	 */
	public String getExchangeType() {
		return exchangeType;
	}
	/**
	 * @param exchangeType the exchangeType to set
	 */
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	/**
	 * @return the showCatastrophicPlan
	 */
	public boolean isShowCatastrophicPlan() {
		return showCatastrophicPlan;
	}
	/**
	 * @param showCatastrophicPlan the showCatastrophicPlan to set
	 */
	public void setShowCatastrophicPlan(boolean showCatastrophicPlan) {
		this.showCatastrophicPlan = showCatastrophicPlan;
	}
	
	public String getRestrictHiosIds() {
		return restrictHiosIds;
	}
	public void setRestrictHiosIds(String restrictHiosIds) {
		this.restrictHiosIds = restrictHiosIds;
	}
	
}
