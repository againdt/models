package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to send request for PersonType API.
 */
public class PersonTypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String personId; 
	private boolean isApplyingForCoverage; 

	public PersonTypeDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public boolean isApplyingForCoverage() {
		return isApplyingForCoverage;
	}

	public void setApplyingForCoverage(boolean isApplyingForCoverage) {
		this.isApplyingForCoverage = isApplyingForCoverage;
	}

}
