delete from EXCHG_PartnerLookup;

drop sequence EXCHG_PrtLkp_seq;
create sequence EXCHG_PrtLkp_seq;

delete from EXCHG_ControlNum;
drop sequence EXCHG_ControlNum_seq;
create sequence EXCHG_ControlNum_seq;

delete from EXCHG_FilePattern;
delete from EXCHG_FilePatternRole;

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  'ID0',
  '451294707',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '301',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  'ID0',
  '451294707',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '302',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '451294707',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '303',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '451294707',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '304',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  'ID0',
  '451294707',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X306',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '305',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '451294707',
  'ID0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '306',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '307',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '308',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '309',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '310',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  'ID0',
  '451294707',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '311',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294707',
  'T',
  'ID0',
  '451294707',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '312',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  'FI',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '451294707',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '313',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Inbound',
  'ZZ',
  '451294707',
  'ZZ',
  'ID0',
  'T',
  '451294707',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '314',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross of Idaho Health Service, Inc',
  '61589',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/ID0/PASSTHRU',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '315',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  'ID0',
  '451294709',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '301',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  'ID0',
  '451294709',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '302',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '451294709',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '303',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '451294709',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '304',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  'ID0',
  '451294709',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X306',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '305',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '451294709',
  'ID0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '306',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '307',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '308',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '309',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '310',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  'ID0',
  '451294709',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '311',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294709',
  'T',
  'ID0',
  '451294709',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '312',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '451294709',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '313',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'ID0',
  'T',
  '451294709',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '314',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'PacificSource',
  '60597',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/PASSTHRU',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '315',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  'ID0',
  '943037165',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '301',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  'ID0',
  '943037165',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '302',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '943037165',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '303',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '943037165',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '304',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  'ID0',
  '943037165',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X306',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '305',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '943037165',
  'ID0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '306',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '307',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '308',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '309',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '310',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  'ID0',
  '943037165',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '311',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '943037165',
  'T',
  'ID0',
  '943037165',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '312',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '943037165',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '313',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'ID0',
  'T',
  '943037165',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '314',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'SelectHealth',
  '26002',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/PASSTHRU',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '315',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  'ID0',
  '451294708',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '301',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  'ID0',
  '451294708',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X220A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '302',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '451294708',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '303',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '451294708',
  'ID0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '304',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  'ID0',
  '451294708',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X306',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '305',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '451294708',
  'ID0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '306',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '307',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/TA1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '308',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '309',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '310',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  'ID0',
  '451294708',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '311',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Outbound',
  'ZZ',
  'ID0',
  'ZZ',
  '451294708',
  'T',
  'ID0',
  '451294708',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/X231A1',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '312',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  'FI',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '451294708',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '313',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Inbound',
  'ZZ',
  '451294708',
  'ZZ',
  'ID0',
  'T',
  '451294708',
  'ID0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromMFT/ID0_Carrier_Test',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '314',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BridgeSpan Health Company',
  '59765',
  '24',
  'Outbound',
  '',
  '',
  '',
  '',
  'T',
  '',
  '',
  '',
  'GRP',
  '/home/tibcouser/data/fromGHIX/ID0_Carrier_Test/PASSTHRU',
  '/home/tibcouser/data/toMFT/ID0_Carrier_Test',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive/ID0_Carrier_Test',
  '',
  '',
  '',
  '315',
  systimestamp,
  systimestamp  
);




insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '61589', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '60597', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '26002', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '59765', '1' );


insert all
into EXCHG_FilePattern values ( 301,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_MS_834_INDV_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 302,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_MS_834_SHOP_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 303,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_MS_834_RECONINDV_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 304,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_MS_834_RECONSHOP_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 305,  'O', '820', '005010X306', 'to_<HIOS_Issuer_ID>_MS_820_SHOP_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 306,  'O', 'TA1', '', 'to_<HIOS_Issuer_ID>_MS_TA1_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 307,  'O', 'TA1', '', 'to_<HIOS_Issuer_ID>_MS_TA1_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 308,  'O', 'TA1', '', 'to_<HIOS_Issuer_ID>_MS_TA1_820_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 309,  'O', '999', '005010X231A1', 'to_<HIOS_Issuer_ID>_MS_999_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 310, 'O', '999', '005010X231A1', 'to_<HIOS_Issuer_ID>_MS_999_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 311, 'O', '999', '005010X231A1', 'to_<HIOS_Issuer_ID>_MS_999_820_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 312, 'I', '834', '005010X220A1', 'from_<HIOS_Issuer_ID>_MS_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'xml', '206,209' )
into EXCHG_FilePattern values ( 313, 'I', '834', '005010X220A1', 'from_<HIOS_Issuer_ID>_MS_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'xml', '207,210' )
into EXCHG_FilePattern values ( 314, 'I', '820', '005010X306', 'from_<HIOS_Issuer_ID>_MS_820_INDV_<YYYYMMDDHHMMSS>', 'edi', 'xml', '208,211' )
into EXCHG_FilePattern values ( 315, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_MS_TA1_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 316, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_MS_TA1_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 317, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_MS_TA1_834_RECONINDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 318, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_MS_TA1_834_RECONSHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 319, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_MS_TA1_820_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 320, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_MS_999_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 321, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_MS_999_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 322, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_MS_999_834_RECONINDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 323, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_MS_999_834_RECONSHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 324, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_MS_999_820_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 325, 'O', 'GRP', '', '<HIOS_Issuer_ID>.GROP.D<YYMMDD>.T<HHMMSSt>.T.OUT', 'xml', 'xml', '' )
into EXCHG_FilePattern values ( 326, 'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_CA_834_RECONSUPPL_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 327, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_CA_TA1_834_RECONSUPPL_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 328, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_CA_999_834_RECONSUPPL_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
SELECT * FROM dual;



insert all
into  EXCHG_FilePatternRole values ( 301, '5010 Individual 834out', '301,303,326' )
into  EXCHG_FilePatternRole values ( 302, '5010 SHOP 834out', '302,304' )
into  EXCHG_FilePatternRole values ( 303, '5010 Individual 834in', '312' )
into  EXCHG_FilePatternRole values ( 304, '5010 SHOP 834in', '313' )
into  EXCHG_FilePatternRole values ( 305, '5010 SHOP 820out', '305' )
into  EXCHG_FilePatternRole values ( 306, '5010 Individual 820in', '314' )
into  EXCHG_FilePatternRole values ( 307, '5010 Individual TA1out', '306,308' )
into  EXCHG_FilePatternRole values ( 308, '5010 SHOP TA1out', '307' )
into  EXCHG_FilePatternRole values ( 309, '5010 Individual TA1in', '315,317,327' )
into  EXCHG_FilePatternRole values ( 310, '5010 SHOP TA1in', '316,318,319' )
into  EXCHG_FilePatternRole values ( 311, '5010 Individual 999out', '309,311' )
into  EXCHG_FilePatternRole values ( 312, '5010 SHOP 999out', '310' )
into  EXCHG_FilePatternRole values ( 313, '5010 Individual 999in', '320,322,328' )
into  EXCHG_FilePatternRole values ( 314, '5010 SHOP 999in', '321,323,324' )
into  EXCHG_FilePatternRole values ( 315, 'Group Installation Out', '325' )
SELECT * FROM dual;


commit;

