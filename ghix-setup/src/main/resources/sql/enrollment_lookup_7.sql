
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_CODE = 'HLT' WHERE LOOKUP_VALUE_CODE = 'HEALTH' AND  LOOKUP_TYPE_ID IN(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='INSURANCE_TYPE');
UPDATE LOOKUP_VALUE SET LOOKUP_VALUE_CODE = 'DEN' WHERE LOOKUP_VALUE_CODE = 'DENTAL' AND  LOOKUP_TYPE_ID IN(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='INSURANCE_TYPE');