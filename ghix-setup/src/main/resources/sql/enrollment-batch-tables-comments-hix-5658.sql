/* BATCH_JOB_EXECUTION TABLE COMMENTS*/
COMMENT ON TABLE BATCH_JOB_EXECUTION IS 'This table holds all information relevant to the JobExecution object. Every time a Job is run there will always be a new JobExecution, and a new row in this table'; 
COMMENT ON COLUMN BATCH_JOB_EXECUTION.JOB_EXECUTION_ID IS 'Primary key that uniquely identifies this execution. The value of this column is obtainable by calling the getId method of the JobExecution object';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.VERSION IS 'This column specify an optimistic locking strategy when dealing with updates to the database';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.JOB_INSTANCE_ID IS 'Foreign key from the BATCH_JOB_INSTANCE table indicating the instance to which this execution belongs. There may be more than one execution per instance';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.CREATE_TIME IS 'Timestamp representing the time that the execution was created';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.START_TIME IS 'Timestamp representing the time the execution was started';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.END_TIME IS 'Timestamp representing the time the execution was finished, regardless of success or failure. An empty value in this column even though the job is not currently running indicates that there has been some type of error and the framework was unable to perform a last save before failing';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.STATUS IS 'Character string representing the status of the execution. This may be COMPLETED, STARTED, etc. The object representation of this column is the BatchStatus enumeration';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.EXIT_CODE IS 'Character string representing the exit code of the execution. In the case of a command line job, this may be converted into a number';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.EXIT_MESSAGE IS 'Character string representing a more detailed description of how the job exited. In the case of failure, this might include as much of the stack trace as is possible';
COMMENT ON COLUMN BATCH_JOB_EXECUTION.LAST_UPDATED IS 'Timestamp representing the last time this execution was persisted';
/* BATCH_JOB_EXECUTION_CONTEXT TABLE COMMENTS*/
COMMENT ON TABLE BATCH_JOB_EXECUTION_CONTEXT IS 'This table holds all information relevant to an Jobs ExecutionContext. There is exactly one Job ExecutionContext per JobExecution, and it contains all of the job-level data that is needed for a particular job execution. This data typically represents the state that must be retrieved after a failure so that a JobInstance can start from where it left off'; 
COMMENT ON COLUMN BATCH_JOB_EXECUTION_CONTEXT.JOB_EXECUTION_ID IS 'Foreign key representing the JobExecution to which the context belongs. There may be more than one row associated to a given execution';
COMMENT ON COLUMN BATCH_JOB_EXECUTION_CONTEXT.SHORT_CONTEXT IS 'A string version of the SERIALIZED_CONTEXT';
COMMENT ON COLUMN BATCH_JOB_EXECUTION_CONTEXT.SERIALIZED_CONTEXT IS 'The entire context, serialized';
/* BATCH_JOB_INSTANCE TABLE COMMENTS*/
COMMENT ON TABLE BATCH_JOB_INSTANCE IS 'This table holds all information relevant to a JobInstance, and serves as the top of the overall hierarchy'; 
COMMENT ON COLUMN BATCH_JOB_INSTANCE.JOB_INSTANCE_ID IS 'The unique id that will identify the instance, which is also the primary key. The value of this column should be obtainable by calling the getId method on JobInstance';
COMMENT ON COLUMN BATCH_JOB_INSTANCE.VERSION IS 'This column specify an optimistic locking strategy when dealing with updates to the database';
COMMENT ON COLUMN BATCH_JOB_INSTANCE.JOB_NAME IS 'Name of the job obtained from the Job object. Because it is required to identify the instance, it must not be null';
COMMENT ON COLUMN BATCH_JOB_INSTANCE.JOB_KEY IS 'A serialization of the JobParameters that uniquely identifies separate instances of the same job from one another. (JobInstances with the same job name must have different JobParameters, and thus, different JOB_KEY values)';
/* BATCH_JOB_PARAMS TABLE COMMENTS*/
COMMENT ON TABLE BATCH_JOB_PARAMS IS 'This table holds all information relevant to the JobParameters object. It contains 0 or more key/value pairs that together uniquely identify a JobInstance and serve as a record of the parameters a job was run with. It should be noted that the table has been denormalized. Rather than creating a separate table for each type, there is one table with a column indicating the type'; 
COMMENT ON COLUMN BATCH_JOB_PARAMS.JOB_INSTANCE_ID IS 'Foreign Key from the BATCH_JOB_INSTANCE table that indicates the job instance the parameter entry belongs to. It should be noted that multiple rows (i.e key/value pairs) may exist for each instance';
COMMENT ON COLUMN BATCH_JOB_PARAMS.TYPE_CD IS 'String representation of the type of value stored, which can be either a string, date, long, or double. Because the type must be known, it cannot be null';
COMMENT ON COLUMN BATCH_JOB_PARAMS.KEY_NAME IS 'The parameter key';
COMMENT ON COLUMN BATCH_JOB_PARAMS.STRING_VAL IS 'Parameter value, if the type is string';
COMMENT ON COLUMN BATCH_JOB_PARAMS.DATE_VAL IS 'Parameter value, if the type is date';
COMMENT ON COLUMN BATCH_JOB_PARAMS.LONG_VAL IS 'Parameter value, if the type is a long';
COMMENT ON COLUMN BATCH_JOB_PARAMS.DOUBLE_VAL IS 'Parameter value, if the type is double';
/* BATCH_STEP_EXECUTION TABLE COMMENTS*/
COMMENT ON TABLE BATCH_STEP_EXECUTION IS 'This table holds all information relevant to the StepExecution object. This table is very similar in many ways to the BATCH_JOB_EXECUTION table and there will always be at least one entry per Step for each JobExecution created'; 
COMMENT ON COLUMN BATCH_STEP_EXECUTION.STEP_EXECUTION_ID IS 'Primary key that uniquely identifies this execution. The value of this column should be obtainable by calling the getId method of the StepExecution object';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.VERSION IS 'This column specify an optimistic locking strategy when dealing with updates to the database';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.STEP_NAME IS 'The name of the step to which this execution belongs';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.JOB_EXECUTION_ID IS 'Foreign key from the BATCH_JOB_EXECUTION table indicating the JobExecution to which this StepExecution belongs. There may be only one StepExecution for a given JobExecution for a given Step name';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.START_TIME IS 'Timestamp representing the time the execution was started';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.END_TIME IS 'Timestamp representing the time the execution was finished, regardless of success or failure. An empty value in this column even though the job is not currently running indicates that there has been some type of error and the framework was unable to perform a last save before failing';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.STATUS IS 'Character string representing the status of the execution. This may be COMPLETED, STARTED, etc. The object representation of this column is the BatchStatus enumeration';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.COMMIT_COUNT IS 'The number of times in which the step has committed a transaction during this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.READ_COUNT IS 'The number of items read during this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.FILTER_COUNT IS 'The number of items filtered out of this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.WRITE_COUNT IS 'The number of items written and committed during this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.READ_SKIP_COUNT IS 'The number of items skipped on read during this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.WRITE_SKIP_COUNT IS 'The number of items skipped on write during this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.PROCESS_SKIP_COUNT IS 'The number of items skipped during processing during this execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.ROLLBACK_COUNT IS 'The number of rollbacks during this execution. Note that this count includes each time rollback occurs, including rollbacks for retry and those in the skip recovery procedure';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.EXIT_CODE IS 'Character string representing the exit code of the execution. In the case of a command line job, this may be converted into a number';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.EXIT_MESSAGE IS 'Character string representing a more detailed description of how the job exited. In the case of failure, this might include as much of the stack trace as is possible';
COMMENT ON COLUMN BATCH_STEP_EXECUTION.LAST_UPDATED IS 'Timestamp representing the last time this execution was persisted';
/* BATCH_STEP_EXECUTION_CONTEXT TABLE COMMENTS*/
COMMENT ON TABLE BATCH_STEP_EXECUTION_CONTEXT IS 'The BATCH_STEP_EXECUTION_CONTEXT table holds all information relevant to an Steps ExecutionContext. There is exactly one ExecutionContext per StepExecution, and it contains all of the data that needs to persisted for a particular step execution. This data typically represents the state that must be retrieved after a failure so that a JobInstance can start from where it left off'; 
COMMENT ON COLUMN BATCH_STEP_EXECUTION_CONTEXT.STEP_EXECUTION_ID IS 'Foreign key representing the StepExecution to which the context belongs. There may be more than one row associated to a given execution';
COMMENT ON COLUMN BATCH_STEP_EXECUTION_CONTEXT.SHORT_CONTEXT IS 'A string version of the SERIALIZED_CONTEXT';
COMMENT ON COLUMN BATCH_STEP_EXECUTION_CONTEXT.SERIALIZED_CONTEXT IS 'The entire context, serialized';
COMMIT;