package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * The persistent class for the ISSUER_ACCREDITATION table.
 */
@Audited
@Entity
@Table(name="issuer_accreditation")
public class IssuerAccreditation  implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public static enum accreditation_status {
		APPROVED, PENDING, DISAPPROVED;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IssuerAccrediation_Seq")
	@SequenceGenerator(name = "IssuerAccrediation_Seq", sequenceName = "issuer_accrediation_seq", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "issuer_id")
	private Issuer issuer;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "expiration_date")
	private Date expirationDate;

	@Column(name = "accreditation_status", length = 25)
	private String accreditationStatus;

	@Column(name = "ncqa_org_id")
	private String ncqaOrgId;

	@Column(name = "ncqa_sub_id")
	private String ncqaSubId;

	@Column(name = "urac_app_num", length = 25)
	private String uracAppNum;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date craetionTimeStamp;

	@Column(name = "created_by")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdatedTimeStamp;

	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

	@Column(name = "product_id")
	private String productId;

	@Column(name = "market_type")
	private String marketType;

	@Column(name = "product_category")
	private String productCategory;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCraetionTimeStamp(new TSDate());
		this.setLastUpdatedTimeStamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdatedTimeStamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getAccreditationStatus() {
		return accreditationStatus;
	}

	public void setAccreditationStatus(String accreditationStatus) {
		this.accreditationStatus = accreditationStatus;
	}

	public String getNcqaOrgId() {
		return ncqaOrgId;
	}

	public void setNcqaOrgId(String ncqaOrgId) {
		this.ncqaOrgId = ncqaOrgId;
	}

	public String getNcqaSubId() {
		return ncqaSubId;
	}

	public void setNcqaSubId(String ncqaSubId) {
		this.ncqaSubId = ncqaSubId;
	}

	public String getUracAppNum() {
		return uracAppNum;
	}

	public void setUracAppNum(String uracAppNum) {
		this.uracAppNum = uracAppNum;
	}

	public Date getCraetionTimeStamp() {
		return craetionTimeStamp;
	}

	public void setCraetionTimeStamp(Date craetionTimeStamp) {
		this.craetionTimeStamp = craetionTimeStamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedTimeStamp() {
		return lastUpdatedTimeStamp;
	}

	public void setLastUpdatedTimeStamp(Date lastUpdatedTimeStamp) {
		this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
}
