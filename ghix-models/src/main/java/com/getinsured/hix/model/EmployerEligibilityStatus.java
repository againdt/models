package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.Employer.EligibilityStatus;

@Entity
@Table(name = "employer_eligibility_status")
public class EmployerEligibilityStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Employer_elgity_stat_seq")
	@SequenceGenerator(name = "Employer_elgity_stat_seq", sequenceName = "employer_elgity_stat_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "eligibility_status")
	@Enumerated(EnumType.STRING)
	private EligibilityStatus eligibilityStatus;
	
	@ManyToOne
	@JoinColumn(name="employer_id")
	private Employer employer;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date createdTimestamp;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name = "eligibity_decision_factor_note")
	private String eligibityDecisionFactorNote;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getEligibityDecisionFactorNote() {
		return eligibityDecisionFactorNote;
	}

	public void setEligibityDecisionFactorNote(String eligibityDecisionFactorNote) {
		this.eligibityDecisionFactorNote = eligibityDecisionFactorNote;
	}
	


}
