package com.getinsured.hix.model.plandisplay;

import java.util.HashMap;
import java.util.Map;

public class EmployerDTO {
	
	private String coverageDate;
	private String tierName;
	private Map<Integer, PlanPremium> planData;
	public String getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(String coverageDate) {
		this.coverageDate = coverageDate;
	}
	public String getTierName() {
		return tierName;
	}
	public void setTierName(String tierName) {
		this.tierName = tierName;
	}
	public Map<Integer, PlanPremium> getPlanData() {
		return planData;
	}
	public void setPlanData(Map<Integer, PlanPremium> planData) {
		this.planData = planData;
	}
	
	public static EmployerDTO clone(EmployerDTO employerDTO) {
		EmployerDTO employerDTOClone = new EmployerDTO();
		employerDTOClone.setCoverageDate(employerDTO.getCoverageDate());
		Map<Integer, PlanPremium> clonedMap = new HashMap<Integer, PlanPremium>();
		for(Map.Entry<Integer, PlanPremium> entry : employerDTO.getPlanData().entrySet())
		{
			clonedMap.put(entry.getKey(), PlanPremium.clone(entry.getValue()));
		}
		employerDTOClone.setPlanData(clonedMap);
		employerDTOClone.setTierName(employerDTO.getTierName());
		return employerDTOClone;
	}
	
		
}