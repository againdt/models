package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrolleeAppEventDTO implements Serializable
{
	private String firstName;
	private String lastName;
	private String exchgIndivIdentifier;
	private String eventReasonLookupValue;
	
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the exchgIndivIdentifier
	 */
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	/**
	 * @param exchgIndivIdentifier the exchgIndivIdentifier to set
	 */
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	/**
	 * @return the eventReasonLookupValue
	 */
	public String getEventReasonLookupValue() {
		return eventReasonLookupValue;
	}
	/**
	 * @param eventReasonLookupValue the eventReasonLookupValue to set
	 */
	public void setEventReasonLookupValue(String eventReasonLookupValue) {
		this.eventReasonLookupValue = eventReasonLookupValue;
	}

}
