package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TkmTicketsRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String description;
	private String subject;
	private String category;
	private String type;
	private Integer requester;
	private Integer assignee;
	private Integer ticketId;
	private String comment;
	private Integer createdBy;
	private Integer lastUpdatedBy;
	private List<Integer> queue;
	private Integer userRoleId;
	private List<String> documentLinks;
	private String ticketStatus;
	private String sortBy;
	private String sortOrder;
	private Integer startRecord;
	private Integer pageSize ;
	private String priority;
	private Integer priorityOrder;
	private Integer moduleId;
	private Integer subModuleId; 
	private boolean isDetailedHistoryView;
	private Map<String,String> ticketFormPropertiesMap=new HashMap<String,String>();
	private String moduleName;
	private Date submittedBefore;
	private Date submittedAfter;
	
	private String caseNumber;
	private String redirectUrl;
	
	private String isArchived;
	private Integer archivedBy;
	private String indAppealReason;

	public enum IndAppealReason
	{
		MedicaidCHP("Medicaid-CHP"), Qhp("QHP"),AptcCsr("APTC-CSR"), AptcAmount("APTC Amount"),
		Flp("FLP"), IncorrectAmount("Incorrect Amount"), FamilyComposition("Family Composition"), Other("Other");
		
		@SuppressWarnings("unused")
		private final String appealReason;

		private IndAppealReason(String reason) {
			appealReason = reason;
		}
	}
	
	public enum Email_Config
	{
		Open("Open"), Create("Create"),Complete("Complete"), Cancel("Cancel"),SecureInbox("SecureInbox");
		
		@SuppressWarnings("unused")
		private final String ticket_status;

		private Email_Config(String status) {
			ticket_status = status;
		}
	}

	public List<Integer> getQueue() {
		return queue;
	}

	public void setQueue(List<Integer> queue) {
		this.queue = queue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getRequester() {
		return requester;
	}

	public void setRequester(Integer requester) {
		this.requester = requester;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getAssignee() {
		return assignee;
	}

	public void setAssignee(Integer assignee) {
		this.assignee = assignee;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return the userRoleId
	 */
	public Integer getUserRoleId() {
		return userRoleId;
	}

	/**
	 * @param userRoleId the userRoleId to set
	 */
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 * @return the documentLinks
	 */
	public List<String> getDocumentLinks() {
		return documentLinks;
	}

	/**
	 * @param documentLinks the documentLinks to set
	 */
	public void setDocumentLinks(List<String> documentLinks) {
		this.documentLinks = documentLinks;
	}

	/**
	 * @return the ticketStatus
	 */
	public String getTicketStatus() {
		return ticketStatus;
	}

	/**
	 * @param ticketStatus the ticketStatus to set
	 */
	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	/**
	 * @return the sortBy
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * @param sortBy the sortBy to set
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the startRecord
	 */
	public Integer getStartRecord() {
		return startRecord;
	}

	/**
	 * @param startRecord the startRecord to set
	 */
	public void setStartRecord(Integer startRecord) {
		this.startRecord = startRecord;
	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	/**
	 * 
	 * @return priorityOrder
	 */
	public Integer getPriorityOrder() {
		return priorityOrder;
	}

	/**
	 * 
	 * @param priorityOrder
	 */
	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}
	
	
	/**
	 * @return the moduleId
	 */
	public Integer getModuleId() {
		return moduleId;
	}
	
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the subModuleId
	 */
	public Integer getSubModuleId() {
		return subModuleId;
	}

	/**
	 * @param subModuleId the subModuleId to set
	 */
	public void setSubModuleId(Integer subModuleId) {
		this.subModuleId = subModuleId;
	}

	/**
	 * @return the isDetailedHistoryView
	 */
	public boolean isDetailedHistoryView() {
		return isDetailedHistoryView;
	}

	/**
	 * @param isDetailedHistoryView the isDetailedHistoryView to set
	 */
	public void setDetailedHistoryView(boolean isDetailedHistoryView) {
		this.isDetailedHistoryView = isDetailedHistoryView;
	}

	public Map<String, String> getTicketFormPropertiesMap() {
		return ticketFormPropertiesMap;
	}

	public void setTicketFormPropertiesMap(
			Map<String, String> ticketFormPropertiesMap) {
		this.ticketFormPropertiesMap = ticketFormPropertiesMap;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	
	public Date getSubmittedBefore() {
		return submittedBefore;
	}

	public void setSubmittedBefore(Date submittedBefore) {
		this.submittedBefore = submittedBefore;
	}

	public Date getSubmittedAfter() {
		return submittedAfter;
	}

	public void setSubmittedAfter(Date submittedAfter) {
		this.submittedAfter = submittedAfter;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	
	public String getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(String isArchived) {
		this.isArchived = isArchived;
	}

	public Integer getArchivedBy() {
		return archivedBy;
	}

	public void setArchivedBy(Integer archivedBy) {
		this.archivedBy = archivedBy;
	}
	public String getIndAppealReason() {
		return indAppealReason;
	}

	public void setIndAppealReason(String indAppealReason) {
		this.indAppealReason = indAppealReason;
	}
	
	// Creates exception when serialized.
//	public void setIndAppealReason(IndAppealReason indAppealReason) {
//		this.indAppealReason = indAppealReason.toString();
//	}
}
