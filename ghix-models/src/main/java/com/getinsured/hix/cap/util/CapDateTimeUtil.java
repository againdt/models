package com.getinsured.hix.cap.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;


/**
 * Jing Chen
 */
public class CapDateTimeUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CapDateTimeUtil.class);
    public static final Integer SLOT_DURATION = 60; // 1 hr in minutes

    // TimeZone
    public static final String TZ_PACIFIC = "US/Pacific";
    public static final String TZ_EASTERN = "US/Eastern";
    public static final String TZ_UTC = "UTC";
    // Date time format pattern
    public static final String APPT_DATE_HOUR_MINUTE_FORMAT = "MM/dd/yyyy HH:mm";
    public static final String MONTH_DAY_YEAR_FORMAT = "MM/dd/yyyy";
    public static final String APPT_DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";
    public static final String APPT_DATE_HOUR_FORMAT = "MM/dd/yyyy HH";
    // DateFormat instance
    public static final DateFormat MONTH_DAY_YEAR_FORMATTER = new SimpleDateFormat("MM/dd/yyyy");
    public static final DateFormat MONTH_YEAR_FORMATTER = new SimpleDateFormat("MM/yyyy");
    public static final DateFormat HOUR_MINUTE_FORMATTER = new SimpleDateFormat("HH:mm");
    public static final DateFormat DATE_HOUR_MINUTE_FORMATTER = new SimpleDateFormat("MM/dd/yyyy HH:mm");
    public static final DateFormat MONTH_DAY_YEAR_HOUR_FORMATTER = new SimpleDateFormat("MM/dd/yyyy HH");
    public static final DateFormat YEAR_MONTH_DAY_DF = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat TWENTY_FOUR_HOUR_MINUTE_FORMATTER = new SimpleDateFormat("Hmm");

    public static Date convertToTimeZone(Date date, String tzFrom, String tzTo) {
        return Date.from(LocalDateTime.ofInstant(date.toInstant(), ZoneId.of(tzTo))
                .atZone(ZoneId.of(tzFrom)).toInstant());
    }

    /**
     * @param strDate: MM/dd/yyyy HH:mm
     */
    public static String convertToTimeZone(String strDate, String tzFrom, String tzTo) throws ParseException {
        Date fromDate = DATE_HOUR_MINUTE_FORMATTER.parse(strDate);
        Date toDate = Date.from(LocalDateTime.ofInstant(fromDate.toInstant(), ZoneId.of(tzTo))
                .atZone(ZoneId.of(tzFrom)).toInstant());
        return DATE_HOUR_MINUTE_FORMATTER.format(toDate);
    }

    public static Date convertToTimeZone(Date fromDate, int minutesToAdd, String tzFrom, String tzTo) throws ParseException {
        //Date fromDate = DATE_HOUR_MINUTE_FORMATTER.parse(strDate);
    	ZonedDateTime zdt = LocalDateTime.ofInstant(fromDate.toInstant(), ZoneId.of(tzTo))
                .atZone(ZoneId.of(tzFrom)).plusMinutes(minutesToAdd);
                
        Date toDate = Date.from(zdt.toInstant());
        return toDate;
    }
    /**
     * @param strDate: MM/dd/yyyy
     */
    public static ZonedDateTime convertStrDate2ZonedDateTime(String strDate) {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MONTH_DAY_YEAR_FORMAT);
        return ZonedDateTime.parse(strDate, formatter);
    }

    /**
     * @param hourMinTz In: 9:30 AM  - Out: 12:00 PM
     * @hour hour Without Leading 0
     */
    public static String convertHourMinSwitchTimeZone_hourWithoutLeading0(String hourMinTz, String tzFrom, String tzTo) {
        String[] hourMinTzArr = hourMinTz.split(":");
        if (hourMinTzArr[0].length() == 1) {
            // add leading 0
            hourMinTz = "0".concat(hourMinTz);
        }
        ZoneId zoneIdFrom = ZoneId.of(tzFrom);
        String localStrDateTime = "2015-01-05 ".concat(hourMinTz);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a");
        LocalDateTime localtDateTime = LocalDateTime.parse(localStrDateTime, formatter);
        ZonedDateTime zonedDateTimeFrom = ZonedDateTime.of(localtDateTime, zoneIdFrom);
        ZonedDateTime zonedDateTimeTo = zonedDateTimeFrom.withZoneSameInstant(ZoneId.of(tzTo));
        String[] arr = zonedDateTimeTo.format(formatter).split(" ");
        if (arr[1].startsWith("0")) {
            return arr[1].replaceFirst("0", "") + " " + arr[2];
        }
        return arr[1] + " " + arr[2];
    }

    /**
     * @param hourMinTz In: 09:30 AM  - Out: 12:00 PM
     * @hour hour With Leading 0
     */
    public static String convertHourMinSwitchTimeZone_hourWithLeading0(String hourMinTz, String tzFrom, String tzTo) {
        ZoneId zoneIdFrom = ZoneId.of(tzFrom);
        String localStrDateTime = "2015-01-05 ".concat(hourMinTz);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a");
        LocalDateTime localtDateTime = LocalDateTime.parse(localStrDateTime, formatter);
        ZonedDateTime zonedDateTimeFrom = ZonedDateTime.of(localtDateTime, zoneIdFrom);
        ZonedDateTime zonedDateTimeTo = zonedDateTimeFrom.withZoneSameInstant(ZoneId.of(tzTo));
        String[] arr = zonedDateTimeTo.format(formatter).split(" ");
        return arr[1] + " " + arr[2];
    }

    public static Date convertUTCDateToTimeZone(Date date, TimeZone tzTo) {
        return Date.from(LocalDateTime.ofInstant(date.toInstant(), ZoneId.of(tzTo.getID()))
                .atZone(ZoneOffset.UTC).toInstant());
    }

    /*-----------------------------------------------------------*

      UTC (String) Date -> Local (String) Date

      Local (String) Date -> UTC (String) Date

    *-----------------------------------------------------------*/

    /**
     * @param localStrDate format is MM/dd/yyyy HH:mm
     */
    public static Date localStrDate2UTCDate(String localStrDate, TimeZone localZone)
            throws ParseException {

        Date localDate = DATE_HOUR_MINUTE_FORMATTER.parse(localStrDate);

        return convertToTimeZone(localDate, localZone.getID(), TZ_UTC);
    }

    public static Date utcDate2LocalDate(Date utcDate, TimeZone localZone) {
        return convertToTimeZone(utcDate, TZ_UTC, localZone.getID());
    }

    public static Date utcDate2LocalDate(Date utcDate, ZoneId localZone) {
        return convertToTimeZone(utcDate, TZ_UTC, localZone.getId());
    }

    public static String utcDate2LocalStrDate(Date utcDate, TimeZone localZone) {
        Date localDate = convertToTimeZone(utcDate, TZ_UTC, localZone.getID());
        return DATE_HOUR_MINUTE_FORMATTER.format(localDate);
    }

    // verify if currentDate is in [startDate, endDate)
    public static boolean isTimeBetweenIncludingStartTime(Date currentDate, Date startDate, Date endDate) {

        LocalTime curr = convertDate2LocalTime(currentDate);
        LocalTime start = convertDate2LocalTime(startDate);
        LocalTime end = convertDate2LocalTime(endDate);

        return curr.equals(start) || (curr.isAfter(start) && curr.isBefore(end)) ;
    }

    // verify if currentDate is in (startDate, endDate)
    public static boolean isTimeBetween(Date currentDate, Date startDate, Date endDate) {

        LocalTime curr = convertDate2LocalTime(currentDate);
        LocalTime start = convertDate2LocalTime(startDate);
        LocalTime end = convertDate2LocalTime(endDate);

        return curr.equals(end) || (curr.isAfter(start) && curr.isBefore(end));
    }

    /********  For future use ********/
//    public static boolean isTimeBetween(LocalDate currentDate, LocalDate startDate, LocalDate endDate) {
//        return currentDate.equals(startDate) || (currentDate.isAfter(startDate) && currentDate.isBefore(endDate));
//    }
//
//    public static boolean isTimeBetween(LocalDateTime currentDate, LocalDateTime startDate, LocalDateTime endDate) {
//        return currentDate.equals(startDate) || (currentDate.isAfter(startDate) && currentDate.isBefore(endDate));
//    }

    /**
     * @param currentTime format: XX:XX AM/PM
     */
    public static boolean isWithinTimeSlot(String currentTime, String timeSlot, int slotDurationInMinute) throws ParseException {
        // XX:XX AM/PM to 24 Hour Clock
        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

        // create time as HH:mm:ss
        String current = date24Format.format(date12Format.parse(currentTime)) + ":00";
        String start = date24Format.format(date12Format.parse(timeSlot)) + ":00";
        String[] arr = start.split(":");
        String end = String.valueOf(Integer.parseInt(arr[0]) + (slotDurationInMinute / 60)) + ":" + arr[1] + ":00";

        LocalTime currLocalTime = LocalTime.parse(current);
        LocalTime startLocalTime = LocalTime.parse(start);
        LocalTime endLocalTime = LocalTime.parse(end);

        return currLocalTime.compareTo(startLocalTime) >= 0 && currLocalTime.compareTo(endLocalTime) < 0;
    }

    public static Date convertLocalDate2Date(LocalDate localDate, TimeZone localZone) {
        return Date.from(localDate.atStartOfDay(ZoneId.of(localZone.getID())).toInstant());
    }

    public static Date convertLocalDateTime2Date(LocalDateTime localDateTime, TimeZone localZone) {
        ZoneId zoneId = ZoneId.of(localZone.getID());
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        return Date.from(zonedDateTime.toInstant());
    }

    public static String convertLocalDateTime2StrDate(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MONTH_DAY_YEAR_FORMAT);
        return localDateTime.format(formatter); // MM/dd/yyyy
    }

    public static LocalDateTime convertDate2LocalDateTime(Date date, TimeZone localZone) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.of(localZone.getID()));
    }

    /**
     * @detail convert date to localtime ignore TimeZone
     */
    public static LocalTime convertDate2LocalTime(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime();
    }

    /**
     * @param strDate MM/dd/yyyy
     */
    public static String getDayOfWeek(String strDate) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate dateTime = LocalDate.parse(strDate, formatter);
        String dayOfWeek = dateTime.getDayOfWeek().name();
        LOGGER.info("Day Of Week for " + strDate + ": " + dayOfWeek);
        return dayOfWeek;

    }

    public static int getDayOfWeek(Date date, TimeZone localZone) {
        LocalDateTime localDateTime = convertDate2LocalDateTime(date, localZone);
        return localDateTime.getDayOfWeek().getValue();
    }

    // computer hour offset between local and target timeZone
    public static int computeLocalOffsetHours(String timeZone) {
        Instant instant = Instant.now(); // can be LocalDateTime
        ZoneId systemZone = ZoneId.of(timeZone); // target timezone
        ZoneOffset currentOffsetForTargetZone = systemZone.getRules().getOffset(instant);
        return currentOffsetForTargetZone.getTotalSeconds() / 3600;
    }

    // computer hour offset between from timeZone and to timeZone
    public static int computeOffsetHours(String fromTz, String toTz) {
        return computeLocalOffsetHours(toTz) - computeLocalOffsetHours(fromTz);
    }

    /**
     * Convert local day to UTC dates
     */
    public static String[] convertLocalDay2UTCDates(String dt, String format, TimeZone tz) {
        ZonedDateTime localDate = ZonedDateTime.of(LocalDateTime.parse(dt, DateTimeFormatter.ofPattern(format)), tz.toZoneId());
        ZonedDateTime utcDate = ZonedDateTime.ofInstant(localDate.toInstant(), ZoneOffset.UTC);

        String[] retVal = {
                utcDate.format(DateTimeFormatter.ofPattern(format)),
                (utcDate.plusDays(1)).format(DateTimeFormatter.ofPattern(format))
        };
        return retVal;
    }

    public static String[] convertLocalDay2UTCDates(String dt, String format, ZoneId zoneId) {
        ZonedDateTime localDate = ZonedDateTime.of(LocalDateTime.parse(dt, DateTimeFormatter.ofPattern(format)), zoneId);
        ZonedDateTime utcDate = ZonedDateTime.ofInstant(localDate.toInstant(), ZoneOffset.UTC);

        String[] retVal = {
                utcDate.format(DateTimeFormatter.ofPattern(format)),
                (utcDate.plusDays(1)).format(DateTimeFormatter.ofPattern(format))
        };
        return retVal;
    }

    public static String randDateTimeGenerator() {

        long offset = Timestamp.valueOf("2012-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2017-12-30 00:00:00").getTime();
        long diff = end - offset + 1;
        Timestamp randTimestamp = new Timestamp(offset + (long) (Math.random() * diff));

        LocalDateTime randlocalDateTime = randTimestamp.toLocalDateTime();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(APPT_DATE_HOUR_MINUTE_FORMAT);

        return formatter.format(randlocalDateTime);
    }

    public static String makeMonthOrDay2Digits(String monthOrDay) {
        if (StringUtils.isEmpty(monthOrDay)) {
            return null;
        }
        return monthOrDay.length() < 2 ? "0".concat(monthOrDay) : monthOrDay;
    }

    public static String makeMonthOrDay2Digits(int monthOrDay) {
        if (monthOrDay <= 0) {
            return null;
        }
        return monthOrDay < 10 ? "0".concat(String.valueOf(monthOrDay)) : String.valueOf(monthOrDay);
    }

    public static Integer removeLeading0ForMonthOrDay(String monthOrDay) {
        if (StringUtils.isEmpty(monthOrDay)) {
            return null;
        }
        return monthOrDay.startsWith("0") ? Integer.parseInt(String.valueOf(monthOrDay.charAt(1))) : Integer.parseInt(monthOrDay);
    }

    // Calculate time slot base on appointment date time and duration without leading 0
    // strApptDate format: MM/dd/yyyy HH:mm
    public static String getTimeSlotText(String strApptDate, Integer duration, TimeZone timeZone) {
        DateTimeFormatter dtf12Hour = DateTimeFormatter.ofPattern("hh:mm a");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(APPT_DATE_HOUR_MINUTE_FORMAT);

        //String hourMinInUTC = strApptDate.split("\\s+")[1]; //  HH:mm
        // HIX-93871 Appointments Should Adjust with Bi-Annual Time Changes
        String localStrDateTimeInUTC = strApptDate;
        LocalDateTime localDateTimeInUTC = LocalDateTime.parse(localStrDateTimeInUTC, formatter);
        ZonedDateTime zonedDateTimeFrom = ZonedDateTime.of(localDateTimeInUTC, ZoneOffset.UTC);
        ZonedDateTime zonedDateTimeTo = zonedDateTimeFrom.withZoneSameInstant(ZoneId.of(timeZone.getID()));

        LocalTime start = LocalTime.of(zonedDateTimeTo.getHour(), zonedDateTimeTo.getMinute()); // hh:mm a

        LocalTime end = start.plusMinutes(duration); // hh(+1):mm a

        String timeSlot = dtf12Hour.format(start) + " - " + dtf12Hour.format(end);

        // HIX-88225: Remove beginning 0 in timeSlots which have a single digit hour
        if (timeSlot != null && timeSlot.length() > 3) {
            String[] timeSlotArr = timeSlot.split(" - ");
            String fromTime = timeSlotArr[0].trim();
            String toTime = timeSlotArr[1].trim();
            if (fromTime.startsWith("0")) {
                fromTime = fromTime.replaceFirst("0", "");
            }
            if (toTime.startsWith("0")) {
                toTime = toTime.replaceFirst("0", "");
            }
            timeSlot = fromTime + " - " + toTime;
        }
        return timeSlot;
    }
}