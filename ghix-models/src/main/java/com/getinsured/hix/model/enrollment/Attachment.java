package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="attachment")
@XmlAccessorType(XmlAccessType.NONE)
public class Attachment implements Serializable
{
	private static final long serialVersionUID = 1L;
		
	@XmlElement(name="MD5ChecksumText")
	private String MD5ChecksumText = null;
		
	@XmlElement(name="binarySizeValue")
	private long binarySizeValue = 0;
	
	@XmlElement(name="documentSequenceID")
	private String documentSequenceID;
	
    @XmlElement(name="documentFileName")
	private String documentFileName = null; 
	
	@XmlElement(name="SHA256HashValueText")
	private String sha256HashValueText = null;
	
	public String getMD5ChecksumText() {
		return MD5ChecksumText;
	}
	public long getBinarySizeValue() {
		return binarySizeValue;
	}
	public void setBinarySizeValue(long binarySizeValue) {
		this.binarySizeValue = binarySizeValue;
	}
	public void setMD5ChecksumText(String mD5ChecksumText) {
		MD5ChecksumText = mD5ChecksumText;
	}
	public String getDocumentSequenceID() {
		return documentSequenceID;
	}
	public void setDocumentSequenceID(String documentSequenceID) {
		this.documentSequenceID = documentSequenceID;
	}
	public String getDocumentFileName() {
		return documentFileName;
	}
	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}
	/**
	 * @return the sha256HashValueText
	 */
	public String getSha256HashValueText() {
		return sha256HashValueText;
	}
	/**
	 * @param sha256HashValueText the sha256HashValueText to set
	 */
	public void setSha256HashValueText(String sha256HashValueText) {
		this.sha256HashValueText = sha256HashValueText;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
