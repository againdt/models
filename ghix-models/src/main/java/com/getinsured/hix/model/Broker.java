package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for the Broker database table.
 * 
 */
@Audited
@Entity
@Table(name = "brokers")
public class Broker implements Serializable, Cloneable ,SortableEntity {

	private static final long serialVersionUID = 1L;

	public enum certification_status {
		Pending("Pending"), Withdrawn("Withdrawn"), Certified("Certified"), Eligible("Eligible"), Denied("Denied"), TerminatedVested(
				"Terminated-Vested"), TerminatedForCause("Terminated-For-Cause"), Deceased("Deceased"), Incomplete(
						"Incomplete"), Suspended("Suspended");
		private String certi_status;

		private certification_status(String strStatus) {
			setCerti_status(strStatus);
		}
		// Pending,Certified,Suspended,NonCertified,DeCertified,,

		public String getCerti_status() {
			return certi_status;
		}

		public void setCerti_status(String certi_status) {
			this.certi_status = certi_status;
		}

	}
	/**
	 * This validation group provides groupings for fields  {@link #licenseNumber}, 
	 * {@link #licenseRenewalDate}, {@link #contactNumber}, {@link #communicationPreference}, {@link #companyName}(businessName)
	 * {@link #federalEIN}(employerIdentificationNumber),  
	 *  
	 * during registration of broker.
	 * @author duraphe_a
	 *
	 */
	public interface BrokerRegistration extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields   {@link #businessContactPhoneNumber}
	 * during registration of broker.
	 * @author duraphe_a
	 *
	 */
	public interface BrokerBusinessContact extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields   {@link #alternatePhoneNumber},
	 * during registration of broker.
	 * @author duraphe_a
	 *
	 */
	public interface BrokerAlternateContact extends Default{
		
	}
	/**
	 * This validation group provides groupings for fields  {@link #faxNumber}
	 * during registration of broker.
	 * @author duraphe_a
	 *
	 */
	public interface BrokerFaxContact extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #yourWebSite}, 
	 * {@link #yourPublicEmail}, 
	 * during registration of broker.
	 * @author duraphe_a
	 *
	 */
	public interface BrokerProfile extends Default{

	}
	/**This validation group validates {@link #certificationStatus}.
	 * It should not be empty.
	 * @author duraphe_a
	 */
	public interface BrokerCerificationChange extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields {@link #certificationDate}
	 * and {@link #reCertificationDate}. This gets trigger only when admin changes status to "Certified". 
	 * @author duraphe_a
	 *
	 */
	public interface BrokerCertifiedStatus extends Default{
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Brokers_Seq")
	@SequenceGenerator(name = "Brokers_Seq", sequenceName = "brokers_Seq", allocationSize = 1)
	private int id;

	@NotEmpty(message="{label.broker.validateBusinessName}",groups=Broker.BrokerRegistration.class)
	@Column(name = "company_name")
	private String companyName;

	@NotEmpty(message="{label.validateLicenseNumber}",groups=Broker.BrokerRegistration.class)
	@Pattern(regexp="^[a-zA-Z0-9]{1,10}$",message="{label.validateLicenseNumber}",groups=Broker.BrokerRegistration.class)
	@Column(name = "license_number", unique = true, nullable = false)
	private String licenseNumber;

	@Pattern(regexp="^\\d{9}$",message="{label.validateFederalEIN}",groups=Broker.BrokerRegistration.class)
	@NotEmpty(message="{label.validateFederalEIN}",groups=Broker.BrokerRegistration.class)
	@Size(max=9,message="{label.validateFederalEIN}",groups=Broker.BrokerRegistration.class)
	@Column(name = "federal_ein")
	private String federalEIN;

	
	@NotEmpty(message="{label.broker.validatePhoneNo}",groups=Broker.BrokerRegistration.class)
	@Pattern(regexp="^[1-9][0-9]{2}[-][0-9]{3}[-][0-9]{4}$",message="{label.broker.validatePhoneNo}",groups=Broker.BrokerRegistration.class)
	@Column(name = "contactNumber")
	private String contactNumber;

	@Column(name = "BROKER_SUPPORTING_DOCUMENT_ID")
	private Integer supportingDocumentId;
	
	@Column(name = "BROKER_CONTRACT_DOCUMENT_ID")
	private Integer contractDocumentId;
	
	@Column(name = "BROKER_E_O_DECLR_DOCUMENT_ID")
	private Integer eoDeclarationDocumentId;

	@Column(name = "broker_image_document_id")
	private Integer imageDocumentId;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "clients_served")
	private String clientsServed;

	@NotNull(message="{label.validateCertificationStatus}",groups=Broker.BrokerCerificationChange.class)
	@NotEmpty(message="{label.validateCertificationStatus}",groups=Broker.BrokerCerificationChange.class)
	@Pattern(regexp="^Pending|Withdrawn|Certified|Eligible|Denied|Terminated-Vested|Terminated-For-Cause|Deceased|Incomplete|Suspended$",message="{label.validateCertificationStatus}",groups=Broker.BrokerCerificationChange.class)
	@Column(name = "certification_status")
	private String certificationStatus;

	@Transient
	private String distance;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "POSTAL_MAIL")
	private String postalMailEnabled;

	public String getPostalMailEnabled() {
		return postalMailEnabled;
	}

	public void setPostalMailEnabled(String postalMailEnabled) {
		this.postalMailEnabled = postalMailEnabled;
	}

	// bi-directional one-to-one association to User
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "userid")
	private AccountUser user;

	// bi-directional one-to-one association to Location
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	private Location location;

	// bi-directional one-to-one association to Mailing Location
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "mailing_locaton_id")
	private Location mailingLocation;

	@Column(name = "area_served")
	private String areaServed;

	@Column(name = "languages_spoken")
	private String languagesSpoken;

	@Column(name = "product_expertise")
	private String productExpertise;

	@Column(name = "education")
	private String education;

	@Column(name = "training")
	private String training;

	@Column(name = "esignature")
	private String esignature;

	@Column(name = "comments")
	private String comments;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "application_date", nullable = true)
	private Date applicationDate;

	@NotNull(groups=Broker.BrokerCertifiedStatus.class,message="{label.validateCertDate}") 
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "certification_date", nullable = true)
	private Date certificationDate;

	
	@NotNull(groups=Broker.BrokerCertifiedStatus.class,message="{label.validateCertDate}") 
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "recertify_date", nullable = true)
	private Date reCertificationDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "decertify_date", nullable = true)
	private Date deCertificationDate;

	
	@Transient
	private transient byte[] photo;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;

	// @Column(name="doing_business_as", nullable=true)
	// private String doingBusinessAs;

	@Column(name = "about_me", nullable = true)
	private String aboutMe;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "delegation_code")
	private String delegationCode;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "response_code")
	private Integer responseCode;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "response_description")
	private String responseDescription;

	@Column(name = "status_change_date")
	private Date statusChangeDate;

	@Column(name = "certification_number")
	private Long certificationNumber;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "broker_number")
	private Long brokerNumber;

	@Pattern(regexp="(^[1-9][0-9]{2}[-][0-9]{3}[-][0-9]{4}$)",message="{label.broker.validatePhoneNo}",groups=Broker.BrokerBusinessContact.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "business_contact_phone_number")
	private String businessContactPhoneNumber;

	@Pattern(regexp="([1-9][0-9][0-9][-][0-9][0-9][0-9][-][0-9][0-9][0-9][0-9])",message="{label.broker.validatePhoneNo}",groups=Broker.BrokerAlternateContact.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "alternate_phone_number")
	private String alternatePhoneNumber;

	
	@Pattern(regexp="(^[1-9][0-9]{2}[-][0-9]{3}[-][0-9]{4}$)",message="{label.bkr.validateFaxNo}",groups=Broker.BrokerFaxContact.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "fax_number")
	private String faxNumber;
	
	@NotEmpty(message="{error.validateCommunicationPreferenceInvalid}",groups=Broker.BrokerRegistration.class)
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "COMMUNICATION_PREF")
	private String communicationPreference;


	public String getCommunicationPreference() {
		return communicationPreference;
	}

	public void setCommunicationPreference(String communicationPreference) {
		this.communicationPreference = communicationPreference;
	}

	//@Future(message="{err.validateLicrenewaldatePast}",groups=Broker.BrokerRegistration.class)
	@NotNull(groups=Broker.BrokerRegistration.class) 
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "license_renewal_date", nullable = true)
	private Date licenseRenewalDate;

	@Pattern(regexp="(((^[hH][tT]{2}[pP][sS]{0,1}[:][/]{2}([w]{3}[\\.]){0,1})|(^[w]{3}[\\.]))[a-zA-Z0-9_~-]+([\\.][a-zA-Z0-9_~-]+)+(.)*$)|^$",message="{err.validateYourWebSite}",groups=Broker.BrokerProfile.class)
	@NotAudited
	@Column(name = "your_web_site")
	private String yourWebSite;
	
	@Column(name = "state_ein")
	private String stateEIN;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Pattern(regexp="^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$|^$",groups=Broker.BrokerProfile.class,message="{label.validatePublicEmail}")
	@Email(message="{label.validatePublicEmail}",groups=Broker.BrokerProfile.class)
	@Column(name = "your_public_email")
	private String yourPublicEmail;
	
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	
	@Column(name = "AGENCY_ID")
	private Long agencyId;
	
	@Column(name = "STATUS")
	private String status;
	
	@NotAudited
	@Column(name = "FIRST_NAME")
	private String firstName;

	@NotAudited
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name="comments_id")
	private Long commentsId;
	
	@Transient
	String agencyName;
	
	@Transient
	String changeType;
	
	@Column(name="personal_email_address")
	private String personalEmailAddress;
	
	@NotAudited
	@Column(name="npn")
	private String npn;
	
	public Broker() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getStateEIN() {
		return stateEIN;
	}

	public void setStateEIN(String stateEIN) {
		this.stateEIN = stateEIN;
	}

	public String getCompanyName() {
		return this.companyName;
	}
	
	public void setYourPublicEmail(String publicEmail) {
		this.yourPublicEmail = publicEmail;
	}

	public String getYourPublicEmail() {
		return this.yourPublicEmail;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCertificationStatus() {
		return this.certificationStatus;
	}

	public void setCertificationStatus(String certificationstatus) {
		this.certificationStatus = certificationstatus;
	}

	public String getClientsServed() {
		return this.clientsServed;
	}

	public void setClientsServed(String clientsServed) {
		this.clientsServed = clientsServed;
	}

	public String getLicenseNumber() {
		return this.licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Location getMailingLocation() {
		return mailingLocation;
	}

	public void setMailingLocation(Location mailingLocation) {
		this.mailingLocation = mailingLocation;
	}

	public AccountUser getUser() {
		return this.user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public String getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAreaServed() {
		return this.areaServed;
	}

	public void setAreaServed(String areaServed) {
		this.areaServed = areaServed;
	}

	public String getLanguagesSpoken() {
		return this.languagesSpoken;
	}

	public void setLanguagesSpoken(String languagesSpoken) {
		this.languagesSpoken = languagesSpoken;
	}

	public String getProductExpertise() {
		return this.productExpertise;
	}

	public void setProductExpertise(String productExpertise) {
		this.productExpertise = productExpertise;
	}

	public String getEducation() {
		return this.education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getTraining() {
		return this.training;
	}

	public void setTraining(String training) {
		this.training = training;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getApplicationDate() {
		return this.applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getCertificationDate() {
		return this.certificationDate;
	}

	public void setCertificationDate(Date certificationDate) {
		this.certificationDate = certificationDate;
	}

	public Date getReCertificationDate() {
		return reCertificationDate;
	}

	public void setReCertificationDate(Date reCertificationDate) {
		this.reCertificationDate = reCertificationDate;
	}

	public Date getDeCertificationDate() {
		return deCertificationDate;
	}

	public void setDeCertificationDate(Date deCertificationDate) {
		this.deCertificationDate = deCertificationDate;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photoGiven) {
		this.photo = photoGiven.clone();
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/*
	 * public String getDoingBusinessAs() { return doingBusinessAs; }
	 * 
	 * public void setDoingBusinessAs(String doingBusinessAs) {
	 * this.doingBusinessAs = doingBusinessAs; }
	 */

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public String getDelegationCode() {
		return delegationCode;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public Integer getSupportingDocumentId() {
		return supportingDocumentId;
	}

	public void setSupportingDocumentId(Integer supportingDocumentId) {
		this.supportingDocumentId = supportingDocumentId;
	}

	/**
	 * Getter for attribute 'contractDocumentId'.
	 *
	 * @return the contractDocumentId.
	 */
	public Integer getContractDocumentId() {
		return contractDocumentId;
	}

	/**
	 * Setter for attribute 'contractDocumentId'.
	 *
	 * @param contractDocumentId the contractDocumentId to set.
	 */
	public void setContractDocumentId(Integer contractDocumentId) {
		this.contractDocumentId = contractDocumentId;
	}

	/**
	 * Getter for attribute 'eoDeclarationDocumentId'.
	 *
	 * @return the eoDeclarationDocumentId.
	 */
	public Integer getEoDeclarationDocumentId() {
		return eoDeclarationDocumentId;
	}

	/**
	 * Setter for attribute 'eoDeclarationDocumentId'.
	 *
	 * @param eoDeclarationDocumentId the eoDeclarationDocumentId to set.
	 */
	public void setEoDeclarationDocumentId(Integer eoDeclarationDocumentId) {
		this.eoDeclarationDocumentId = eoDeclarationDocumentId;
	}

	public String getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(String federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getEsignature() {
		return esignature;
	}

	public void setEsignature(String esignature) {
		this.esignature = esignature;
	}

	public Date getStatusChangeDate() {
		return statusChangeDate;
	}

	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}

	public Long getCertificationNumber() {
		return certificationNumber;
	}

	public void setCertificationNumber(Long certificationNumber) {
		this.certificationNumber = certificationNumber;
	}

	public Long getBrokerNumber() {
		return brokerNumber;
	}

	public void setBrokerNumber(Long brokerNumber) {
		this.brokerNumber = brokerNumber;
	}

	public String getBusinessContactPhoneNumber() {
		return businessContactPhoneNumber;
	}

	public void setBusinessContactPhoneNumber(String businessContactPhoneNumber) {
		this.businessContactPhoneNumber = businessContactPhoneNumber;
	}

	public String getAlternatePhoneNumber() {
		return alternatePhoneNumber;
	}

	public void setAlternatePhoneNumber(String alternatePhoneNumber) {
		this.alternatePhoneNumber = alternatePhoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Date getLicenseRenewalDate() {
		return licenseRenewalDate;
	}

	public void setLicenseRenewalDate(Date licenseRenewalDate) {
		this.licenseRenewalDate = licenseRenewalDate;
	}

	public String getYourWebSite() {
		return yourWebSite;
	}

	public void setYourWebSite(String yourWebSite) {
		this.yourWebSite = yourWebSite;
	}
	
	/**
	 * @return the distance
	 */
	public String getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(String distance) {
		this.distance = distance;
	}

	
	public Integer getImageDocumentId() {
		return imageDocumentId;
	}

	public void setImageDocumentId(Integer imageDocumentId) {
		this.imageDocumentId = imageDocumentId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());

	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getCommentsId() {
		return commentsId;
	}

	public void setCommentsId(Long commentsId) {
		this.commentsId = commentsId;
	}
	
	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getPersonalEmailAddress() {
		return personalEmailAddress;
	}

	public void setPersonalEmailAddress(String personalEmailAddress) {
		this.personalEmailAddress = personalEmailAddress;
	}

	@Override
	public Object clone() throws CloneNotSupportedException{
		Broker brokerObj = new Broker();
		brokerObj.setId(getId());
		brokerObj.setCompanyName(getCompanyName());
		brokerObj.setEoDeclarationDocumentId(getEoDeclarationDocumentId());
		brokerObj.setContractDocumentId(getContractDocumentId());
		brokerObj.setSupportingDocumentId(getSupportingDocumentId());
		brokerObj.setContactNumber(getContactNumber());
		brokerObj.setLicenseNumber(getLicenseNumber());
		brokerObj.setCertificationStatus(getCertificationStatus());
		brokerObj.setLocation(getLocation());
		brokerObj.setMailingLocation(getMailingLocation());
		brokerObj.setUser(getUser());
		brokerObj.setAreaServed(getAreaServed());
		brokerObj.setLanguagesSpoken(getLanguagesSpoken());
		brokerObj.setProductExpertise(getProductExpertise());
		brokerObj.setClientsServed(getClientsServed());
		brokerObj.setEducation(getEducation());
		brokerObj.setTraining(getTraining());
		brokerObj.setEsignature(getEsignature());
		brokerObj.setApplicationDate(getApplicationDate());
		brokerObj.setCertificationDate(getCertificationDate());
		brokerObj.setReCertificationDate(getCertificationDate());
		brokerObj.setDeCertificationDate(getDeCertificationDate());
		brokerObj.setComments(getComments());
		// brokerObj.setDoingBusinessAs(getDoingBusinessAs());
		brokerObj.setAboutMe(getAboutMe());
		brokerObj.setFederalEIN(getFederalEIN());
		brokerObj.setStatusChangeDate(getStatusChangeDate());
		brokerObj.setCertificationNumber(getCertificationNumber());
		brokerObj.setBrokerNumber(getBrokerNumber());
		brokerObj.setAlternatePhoneNumber(getAlternatePhoneNumber());
		brokerObj.setFaxNumber(getFaxNumber());
		brokerObj.setBusinessContactPhoneNumber(getBusinessContactPhoneNumber());
		brokerObj.setLicenseRenewalDate(getLicenseRenewalDate());
		brokerObj.setYourWebSite(getYourWebSite());
		brokerObj.setYourPublicEmail(getYourPublicEmail());
		brokerObj.setCommunicationPreference(getCommunicationPreference());
		brokerObj.setImageDocumentId(getImageDocumentId());
		brokerObj.setPostalMailEnabled(getPostalMailEnabled());
		brokerObj.setPersonalEmailAddress(getPersonalEmailAddress());
		brokerObj.setNpn(getNpn());
		return brokerObj;
	}

	@Override
	public String toString() {
		return "Broker Details: brokerId = " + id + ", userId = " + ( user != null ? user.getId() : "" );
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}
	
	private static List<String> sortableColumnsList = new ArrayList<String>(Arrays.asList(
			"agentLicense", 
			"agentName",
			"user.firstName", 
			"user.lastName", 
			"firstName", 
			"lastName", 
			"companyName", 
			"licenseNumber", 
			"applicationDate", 
			"certificationStatus",
			"certificationDate")); 

	public static List<String> getSortableColumnsList() {
		return sortableColumnsList;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>(getSortableColumnsList());
	 	return columnNames;
	}
}
