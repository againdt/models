package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for table EE_POPULATION_SERVED, to save entity's
 * population served information.
 * 
 */
@Entity
@Table(name = "EE_POPULATION_LANGUAGES")
public class PopulationLanguage implements Serializable, Comparable<PopulationLanguage>, PopulationData {
	private static final long serialVersionUID = 1L;
	private static final int PRIME = 31;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_POPULATION_LANGUAGES_SEQ")
	@SequenceGenerator(name = "EE_POPULATION_LANGUAGES_SEQ", sequenceName = "EE_POPULATION_LANGUAGES_SEQ", allocationSize = 1)
	private int id;

	@OneToOne
	@JoinColumn(name = "EE_ENTITY_ID")
	private EnrollmentEntity entity;

	@NotEmpty
	@Size(max=20)
	@Column(name = "GI_LANGUAGE")
	private String language;
		
	@Transient
	private String value;
		
	@Column(name = "LANGUAGE_PERCENTAGE")
	private Long languagePercentage;
	
	@Column(name = "NO_OF_STAFF_SPEAK_LANGUAGE")
	private Integer noOfStaffSpeakLanguage;

	public PopulationLanguage() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EnrollmentEntity getEntity() {
		return entity;
	}

	public void setEntity(EnrollmentEntity entity) {
		this.entity = entity;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
		this.value= language;
	}

	public Long getLanguagePercentage() {
		return languagePercentage;
	}

	public void setLanguagePercentage(Long languagePercentage) {
		this.languagePercentage = languagePercentage;
	}

	public Integer getNoOfStaffSpeakLanguage() {
		return noOfStaffSpeakLanguage;
	}

	public void setNoOfStaffSpeakLanguage(Integer noOfStaffSpeakLanguage) {
		this.noOfStaffSpeakLanguage = noOfStaffSpeakLanguage;
	}

	@Override
	public String toString() {
		return "PopulationLanguage details: ID = "+id+", EnrollmentEntity: "+((entity!=null)?("EntityID = "+entity.getId()):"");
	}

	@Override
	public int compareTo(PopulationLanguage obj) {
		return obj.getLanguage().compareTo(this.language);
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 1;
		result = PRIME * result + ((entity == null) ? 0 : entity.hashCode());
		result = PRIME * result + id;
		result = PRIME * result
				+ ((language == null) ? 0 : language.hashCode());
		result = PRIME
				* result
				+ ((languagePercentage == null) ? 0 : languagePercentage
						.hashCode());
		result = PRIME
				* result
				+ ((noOfStaffSpeakLanguage == null) ? 0
						: noOfStaffSpeakLanguage.hashCode());
		return result;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PopulationLanguage)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		PopulationLanguage other = (PopulationLanguage) obj;
		return checkPopulationLanguageData(other);
	}

	/**
	 * Method to check current object's population language data for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPopulationLanguageData(PopulationLanguage other) {
		return (checkEntity(other) && checkId(other) && checkLanguageData(other));
	}

	/**
	 * Method to check current object's attribute 'entity' for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntity(PopulationLanguage other) {
		if (entity == null) {
			if (other.entity != null) {
				return false;
			}
		} else if (!entity.equals(other.entity)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'id' for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkId(PopulationLanguage other) {
		if (id != other.id) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's language data for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLanguageData(PopulationLanguage other) {
		return (checkLanguage(other) && checkLanguagePercentage(other) && checkNoOfStaffSpeakLanguage(other));
	}

	/**
	 * Method to check current object's attribute 'noOfStaffSpeakLanguage' for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkNoOfStaffSpeakLanguage(PopulationLanguage other) {
		if (noOfStaffSpeakLanguage == null) {
			if (other.noOfStaffSpeakLanguage != null) {
				return false;
			}
		} else if (!noOfStaffSpeakLanguage.equals(other.noOfStaffSpeakLanguage)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'languagePercentage' for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLanguagePercentage(PopulationLanguage other) {
		if (languagePercentage == null) {
			if (other.languagePercentage != null) {
				return false;
			}
		} else if (!languagePercentage.equals(other.languagePercentage)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'language' for equality with given object.
	 * 
	 * @param other The other PopulationLanguage instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLanguage(PopulationLanguage other) {
		if (language == null) {
			if (other.language != null) {
				return false;
			}
		} else if (!language.equals(other.language)) {
			return false;
		}
		return true;
	}

	@Override
	public String getValue() {
		return getLanguage();
	}

	public void setValue(String value) {
		this.value = value;
		this.language= value;
	}
}