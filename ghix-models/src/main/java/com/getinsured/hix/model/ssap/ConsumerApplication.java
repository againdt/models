package com.getinsured.hix.model.ssap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;


/**
 * The persistent class for the SSAP_APPLICATIONS database table.
 *
 */
@Audited
@Entity
@Table(name="SSAP_APPLICATIONS")
@NamedQuery(name="ConsumerApplication.findAll", query="SELECT s FROM ConsumerApplication s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConsumerApplication implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum STATUS {
		/**
		   	1)	Redirected - Default, upon redirection to GI
			2)	Not Interested ï¿½ After a GI agent called the customer and verified no interest in GI products
			3)	Closed Online ï¿½ Online enrollment
			4)	Closed by Agent ï¿½ Sale of a health plan was performed by an Agent

		 */
		FAKE_TEST_LEAD, REDIRECTED, NOT_INTERESTED, CLOSED_ONLINE, CLOSED_BY_AGENT, ECOMMITTED, CALLED_ONCE, CALLED_TWICE, CALLED_THRICE, SWITCHED_TO_A_DIFFERENT_PLAN, PENDING, OTHER,
		FFM_PROXY_COMPLETED, FFM_PROXY_FAILED, FFM_PROXY_INPROGRESS, FFM_REDIRECT, OP, SG, SU, ER, EN, CC, CL, DELETED
	}
	


	@Id
	@SequenceGenerator(name="SSAP_APPLICATIONS_ID_GENERATOR", sequenceName="SSAP_APPLICATIONS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICATIONS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;


	@Column(name="APPLICATION_DATA")
	private String applicationData;

	@NotAudited
	@Column(name="PHIX_APP_STATUS", length=50)
	@Enumerated(EnumType.STRING)
	private STATUS status;
	
	
	@Column(name="APPLICATION_STATUS", length=50)
	private String applicationStatus = "OP";


	@NotAudited
	@Column(name="PRODUCT_TYPE", length=100)
	private String applicationType;

/*	@Column(name="CMR_HOUSEOLD_ID", precision=22)
	private BigDecimal cmrHouseoldId;
*/
	@NotAudited
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CMR_HOUSEOLD_ID", nullable=true)
	private Household household;
	
	@NotAudited
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ELIG_LEAD_ID", nullable=true)
	private EligLead eligLead;

	@NotAudited
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PLAN_ID", nullable=true)
	private Plan plan;

	@Column(name="CREATED_BY", precision=10)
	private BigDecimal createdBy;

	@Column(name="CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@NotAudited
	@Column(name="ESIGN_DATE")
	private Timestamp esignDate;

	@NotAudited
	@Column(name="ESIGN_FIRST_NAME", length=100)
	private String esignFirstName;

	@NotAudited
	@Column(name="ESIGN_LAST_NAME", length=100)
	private String esignLastName;

	@NotAudited
	@Column(name="ESIGN_MIDDLE_NAME", length=100)
	private String esignMiddleName;

	@NotAudited
	@Column(name="CASE_NUMBER", length=100)
	private String caseNumber;

	@NotAudited
	@Column(name="LAST_NOTICE_ID", precision=22)
	private BigDecimal lastNoticeId;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@NotAudited
	@Column(length=100)
	private String source;

	@NotAudited
	@Column(name="SSAP_APPLICATION_SECTION_ID", precision=22)
	private BigDecimal ssapApplicationSectionId;

	@NotAudited
	@Column(name="CURRENT_PAGE_ID", length=100)
	private String currentPageId;

	@NotAudited
	@Column(name="COVERAGE_YEAR")
	private Integer coverageYear;

	
	@Column(name="ELIGIBILITY_STATUS", length=50)
	private String eligibilityStatus;

	
	@Column(name="EXCHANGE_ELIGIBILITY_STATUS", length=10)
	private String exchangeEligibilityStatus;

	@NotAudited
	@Column(name="CSR_LEVEL", length=10)
	private String csrLevel;

	@NotAudited
	@Column(name="ENABLE_ENROLLMENT", length=1)
	private String allowEnrollment;

	@NotAudited
	@Column(name="ENROLLMENT_STATUS", length=50)
	private String enrollmentStatus;

	@NotAudited
	@Column(name = "MAXIMUM_APTC")
	private BigDecimal maximumAPTC;

	@NotAudited
	@Column(name = "ELECTED_APTC")
	private BigDecimal electedAPTC;
	
	@NotAudited
	@Column(name="STATE")
    private String state;
	
	@NotAudited
	@Column(name = "ZIPCODE")
	private String zipCode; 
	
	@NotAudited
	@Column(name = "COUNTY_CODE")
	private String countyCode; 
	
	@NotAudited
	@Column(name = "EXTERNAL_APPLICATION_ID")
	private String externalApplicationId;
	
	
	
	@Column(name = "STAGE")
	@Enumerated(EnumType.STRING)
	private STAGE stage; 
	
	@NotAudited
	@Column(name = "MEMBER_DATA")
	private String memberData;
	
	@NotAudited
	@Column(name = "EXCHANGE_TYPE")
	private String exchangeType;
	
	@Column(name="last_updated_by")
    private Integer updatedBy;

	@NotAudited
	@Column(name="PREMIUM_BEFORE_CREDIT")
	private BigDecimal premiumBeforeCredit;
	
	@NotAudited
	@Column(name="PREMIUM_AFTER_CREDIT")
	private BigDecimal premiumAfterCredit;
 
	@NotAudited
	@Column(name="APP_LEAD_STATUS")
	private String appleadStatus;
	    
	@NotAudited
	@Column(name="START_DATE")
	private Timestamp startDate;
	
	@NotAudited
	@Column(name="APPLICATION_TYPE", length=100)
	private String ssapApplicationType;


	//bi-directional many-to-one association to SsapApplicant
	//@JsonManagedReference
	@NotAudited
	@OneToMany(mappedBy="ssapApplication", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	private List<ConsumerApplicant> ssapApplicants;
	
	@NotAudited
	@Column(name = "ACA_TRANSACTION_URL")
	private String acaTransactionURL;
	
	@Transient
	private String issuerName;

	public ConsumerApplication() {
	}

	public String getSsapApplicationType() {
		return ssapApplicationType;
	}

	public void setSsapAppicationType(String ssapApplicationType) {
		this.ssapApplicationType = ssapApplicationType;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplicationData() {
		return this.applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public STATUS getStatus() {
		return this.status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public String getApplicationType() {
		return this.applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	/*public BigDecimal getCmrHouseoldId() {
		return this.cmrHouseoldId;
	}

	public void setCmrHouseoldId(BigDecimal cmrHouseoldId) {
		this.cmrHouseoldId = cmrHouseoldId;
	}*/

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEsignDate() {
		return this.esignDate;
	}

	public void setEsignDate(Timestamp esignDate) {
		this.esignDate = esignDate;
	}

	public String getEsignFirstName() {
		return this.esignFirstName;
	}

	public void setEsignFirstName(String esignFirstName) {
		this.esignFirstName = esignFirstName;
	}

	public String getEsignLastName() {
		return this.esignLastName;
	}

	public void setEsignLastName(String esignLastName) {
		this.esignLastName = esignLastName;
	}

	public String getEsignMiddleName() {
		return this.esignMiddleName;
	}

	public void setEsignMiddleName(String esignMiddleName) {
		this.esignMiddleName = esignMiddleName;
	}

	public String getCaseNumber() {
		return this.caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public BigDecimal getLastNoticeId() {
		return this.lastNoticeId;
	}

	public void setLastNoticeId(BigDecimal lastNoticeId) {
		this.lastNoticeId = lastNoticeId;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public BigDecimal getSsapApplicationSectionId() {
		return this.ssapApplicationSectionId;
	}

	public void setSsapApplicationSectionId(BigDecimal ssapApplicationSectionId) {
		this.ssapApplicationSectionId = ssapApplicationSectionId;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public List<ConsumerApplicant> getSsapApplicants() {
		return this.ssapApplicants;
	}

	public void setSsapApplicants(List<ConsumerApplicant> ssapApplicants) {
		this.ssapApplicants = ssapApplicants;
	}

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public ConsumerApplicant addSsapApplicant(ConsumerApplicant ssapApplicant) {
		getSsapApplicants().add(ssapApplicant);
		ssapApplicant.setSsapApplication(this);

		return ssapApplicant;
	}

	public ConsumerApplicant removeSsapApplicant(ConsumerApplicant ssapApplicant) {
		getSsapApplicants().remove(ssapApplicant);
		ssapApplicant.setSsapApplication(null);

		return ssapApplicant;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}

	public void setExchangeEligibilityStatus(String exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getAllowEnrollment() {
		return allowEnrollment;
	}

	public void setAllowEnrollment(String allowEnrollment) {
		this.allowEnrollment = allowEnrollment;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public BigDecimal getMaximumAPTC() {
		return maximumAPTC;
	}

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
		this.maximumAPTC = maximumAPTC;
	}

	public BigDecimal getElectedAPTC() {
		return electedAPTC;
	}

	public void setElectedAPTC(BigDecimal electedAPTC) {
		this.electedAPTC = electedAPTC;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

	public STAGE getStage() {
		return stage;
	}

	public void setStage(STAGE stage) {
		this.stage = stage;
	}

	public Household getHousehold() {
		return household;
	}

	public void setHousehold(Household household) {
		this.household = household;
	}
	
	

	public EligLead getEligLead() {
		return eligLead;
	}

	public void setEligLead(EligLead eligLead) {
		this.eligLead = eligLead;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getMemberData() {
		return memberData;
	}

	public void setMemberData(String memberData) {
		this.memberData = memberData;
	}
	
	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
		
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "consumerApplication", cascade = CascadeType.ALL)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ConsumerApplicationExtPhix ssapApplicationExtPhix;	
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSTimestamp());
		this.setLastUpdateTimestamp(new TSTimestamp());
		
		StringBuffer sb = new StringBuffer(UUID.randomUUID().toString());
		sb.deleteCharAt(8);
		sb.deleteCharAt(12);
		sb.deleteCharAt(16);
		sb.deleteCharAt(20);
		this.setCaseNumber(sb.toString());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	public BigDecimal getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}

	public void setPremiumBeforeCredit(BigDecimal premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}

	public BigDecimal getPremiumAfterCredit() {
		return premiumAfterCredit;
	}

	public void setPremiumAfterCredit(BigDecimal premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}

	public ConsumerApplicationExtPhix getSsapApplicationExtPhix() {
		return ssapApplicationExtPhix;
	}

	public void setSsapApplicationExtPhix(
			ConsumerApplicationExtPhix ssapApplicationExtPhix) {
		this.ssapApplicationExtPhix = ssapApplicationExtPhix;
	}
	public String getAppleadStatus() {
		return appleadStatus;
	}
	public void setAppleadStatus(String appleadStatus) {
		this.appleadStatus = appleadStatus;
	}

	public String getCurrentPageId() {
		return currentPageId;
	}

	public void setCurrentPageId(String currentPageId) {
		this.currentPageId = currentPageId;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getAcaTransactionURL() {
		return acaTransactionURL;
	}

	public void setAcaTransactionURL(String acaTransactionURL) {
		this.acaTransactionURL = acaTransactionURL;
	}

		
}

