package com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws;

import com.serff.planmanagementexchangeapi.common.model.pm.PlanStatus;
import com.serff.planmanagementexchangeapi.exchange.model.service.UpdatePlanStatusResponse;
import com.getinsured.hix.model.GHIXResponse;

public class PlanMgmtResponse extends GHIXResponse {

	private PlanStatus planStatus;
	private UpdatePlanStatusResponse updatePlanStatusResponse;

	public UpdatePlanStatusResponse getUpdatePlanStatusResponse() {
		return updatePlanStatusResponse;
	}

	public void setUpdatePlanStatusResponse(
			UpdatePlanStatusResponse updatePlanStatusResponse) {
		this.updatePlanStatusResponse = updatePlanStatusResponse;
	}

	public PlanStatus getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(PlanStatus planStatus) {
		this.planStatus = planStatus;
	}

	@Override
	public String toString() {
		return "PlanMgmtResponse [planStatus=" + planStatus
				+ ", updatePlanStatusResponse=" + updatePlanStatusResponse
				+ "]";
	}
	
}
