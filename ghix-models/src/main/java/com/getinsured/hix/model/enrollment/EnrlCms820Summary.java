package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_CMS_820_SUMMARY")
public class EnrlCms820Summary  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public enum SummaryStatus {
		RECEIVED("Received"),
		LOADED("Loaded"),
		LOADING_ERROR("Loading Error"),
		PROCESSING("Processing"),
		COMPLETED("Completed");
		
		private final String stage;       

	    private SummaryStatus(String s) {
	        stage = s;
	    }
	    
	    public String toString() {
		       return this.stage;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_820_SUMMARY_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_820_SUMMARY_SEQ", sequenceName = "ENRL_CMS_820_SUMMARY_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="HIOS_ISSUER_ID")
	private String hiosIssuerId;
	
	@Column(name="FILE_NAME", nullable=false)
	private String fileName;
	
	@Column(name="COVERAGE_YEAR")
	private Integer coverageYear;
	
	@Column(name="PROCESSING_MONTH")
	private Integer processingMonth;
	
	@Column(name="TOTAL_ISSUER_APTC")
	private BigDecimal totalIssuerAptc;
	
	@Column(name="TOTAL_ISSUER_CSR")
	private BigDecimal totalIssuerCsr;
	
	@Column(name="TOTAL_ISSUER_UF")
	private BigDecimal totalIssuerUf;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimeStamp;
	
	@Column(name="ERROR_DATA")
	private String errorData;
	
	@Column(name="TRANSITION_YEAR_MONTH")
	private String transitionYearMonth;
	
	@Column(name="TOTAL_PAYMENT")
	private BigDecimal totalPayment;
	
	@Column(name="PAYMENT_METHOD_CODE")
	private String paymentMethodCode;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="TXN_CONTROL_NUMBER")
	private String txnControlNumber;
	
	@Column(name="RPT_GENERATION_DATE")
	private Date rptGenerationDate;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public Integer getProcessingMonth() {
		return processingMonth;
	}

	public void setProcessingMonth(Integer processingMonth) {
		this.processingMonth = processingMonth;
	}

	public BigDecimal getTotalIssuerAptc() {
		return totalIssuerAptc;
	}

	public void setTotalIssuerAptc(BigDecimal totalIssuerAptc) {
		this.totalIssuerAptc = totalIssuerAptc;
	}

	public BigDecimal getTotalIssuerCsr() {
		return totalIssuerCsr;
	}

	public void setTotalIssuerCsr(BigDecimal totalIssuerCsr) {
		this.totalIssuerCsr = totalIssuerCsr;
	}

	public BigDecimal getTotalIssuerUf() {
		return totalIssuerUf;
	}

	public void setTotalIssuerUf(BigDecimal totalIssuerUf) {
		this.totalIssuerUf = totalIssuerUf;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public String getErrorData() {
		return errorData;
	}

	public void setErrorData(String errorData) {
		this.errorData = errorData;
	}

	public String getTransitionYearMonth() {
		return transitionYearMonth;
	}

	public void setTransitionYearMonth(String transitionYearMonth) {
		this.transitionYearMonth = transitionYearMonth;
	}

	public BigDecimal getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}

	public String getPaymentMethodCode() {
		return paymentMethodCode;
	}

	public void setPaymentMethodCode(String paymentMethodCode) {
		this.paymentMethodCode = paymentMethodCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTxnControlNumber() {
		return txnControlNumber;
	}

	public void setTxnControlNumber(String txnControlNumber) {
		this.txnControlNumber = txnControlNumber;
	}

	public Date getRptGenerationDate() {
		return rptGenerationDate;
	}

	public void setRptGenerationDate(Date rptGenerationDate) {
		this.rptGenerationDate = rptGenerationDate;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimeStamp(new TSDate());
		this.setCreationTimeStamp(new TSDate()); 
	}
	
	@PreUpdate
	public void PreUpdate()
	{
		this.setCreationTimeStamp(new TSDate()); 
	}
}
