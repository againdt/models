package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class EnrollmentIrsPolicyInfoDTO implements Serializable{
	
	    private String qhpPolicyNum;
	    private String qhpIssuerEIN;
	    private Boolean pediatricDentalPlanPremiumInd;
	    private Float slcspAdjMonthlyPremiumAmt;
	    private Float householdAPTCAmt;
	    private Float totalHsldMonthlyPremiumAmt;
	    private String issuerNm;
	    private Date policyCoverageStartDt;
	    private Date policyCoverageEndDt;
	    private Float totalQHPMonthlyPremiumAmt;
	    private Float aptcPaymentAmt;
	    private Boolean isActive;
	    private Date lastUpdate;
	    private Boolean isFinancialFlow;
	    private Integer month;
	    private List<EnrollmentIrsCoveredIndividualDTO> coveredIndividualList;
	    
		public String getQhpPolicyNum() {
			return qhpPolicyNum;
		}
		public void setQhpPolicyNum(String qhpPolicyNum) {
			this.qhpPolicyNum = qhpPolicyNum;
		}
		public String getQhpIssuerEIN() {
			return qhpIssuerEIN;
		}
		public void setQhpIssuerEIN(String qhpIssuerEIN) {
			this.qhpIssuerEIN = qhpIssuerEIN;
		}
		public Boolean getPediatricDentalPlanPremiumInd() {
			return pediatricDentalPlanPremiumInd;
		}
		public void setPediatricDentalPlanPremiumInd(
				Boolean pediatricDentalPlanPremiumInd) {
			this.pediatricDentalPlanPremiumInd = pediatricDentalPlanPremiumInd;
		}
		public Float getSlcspAdjMonthlyPremiumAmt() {
			return slcspAdjMonthlyPremiumAmt;
		}
		public void setSlcspAdjMonthlyPremiumAmt(Float slcspAdjMonthlyPremiumAmt) {
			this.slcspAdjMonthlyPremiumAmt = slcspAdjMonthlyPremiumAmt;
		}
		public Float getHouseholdAPTCAmt() {
			return householdAPTCAmt;
		}
		public void setHouseholdAPTCAmt(Float householdAPTCAmt) {
			this.householdAPTCAmt = householdAPTCAmt;
		}
		public Float getTotalHsldMonthlyPremiumAmt() {
			return totalHsldMonthlyPremiumAmt;
		}
		public void setTotalHsldMonthlyPremiumAmt(Float totalHsldMonthlyPremiumAmt) {
			this.totalHsldMonthlyPremiumAmt = totalHsldMonthlyPremiumAmt;
		}
		public String getIssuerNm() {
			return issuerNm;
		}
		public void setIssuerNm(String issuerNm) {
			this.issuerNm = issuerNm;
		}
		public Date getPolicyCoverageStartDt() {
			return policyCoverageStartDt;
		}
		public void setPolicyCoverageStartDt(Date policyCoverageStartDt) {
			this.policyCoverageStartDt = policyCoverageStartDt;
		}
		public Date getPolicyCoverageEndDt() {
			return policyCoverageEndDt;
		}
		public void setPolicyCoverageEndDt(Date policyCoverageEndDt) {
			this.policyCoverageEndDt = policyCoverageEndDt;
		}
		public Float getTotalQHPMonthlyPremiumAmt() {
			return totalQHPMonthlyPremiumAmt;
		}
		public void setTotalQHPMonthlyPremiumAmt(Float totalQHPMonthlyPremiumAmt) {
			this.totalQHPMonthlyPremiumAmt = totalQHPMonthlyPremiumAmt;
		}
		public Float getAptcPaymentAmt() {
			return aptcPaymentAmt;
		}
		public void setAptcPaymentAmt(Float aptcPaymentAmt) {
			this.aptcPaymentAmt = aptcPaymentAmt;
		}
		public List<EnrollmentIrsCoveredIndividualDTO> getCoveredIndividualList() {
			return coveredIndividualList;
		}
		public void setCoveredIndividualList(
				List<EnrollmentIrsCoveredIndividualDTO> coveredIndividualList) {
			this.coveredIndividualList = coveredIndividualList;
		}
		
		/**
		 * @return the isActive
		 */
		public Boolean getIsActive() {
			return isActive;
		}
		/**
		 * @param isActive the isActive to set
		 */
		public void setIsActive(Boolean isActive) {
			this.isActive = isActive;
		}
		/**
		 * @return the lastUpdate
		 */
		public Date getLastUpdate() {
			return lastUpdate;
		}
		/**
		 * @param lastUpdate the lastUpdate to set
		 */
		public void setLastUpdate(Date lastUpdate) {
			this.lastUpdate = lastUpdate;
		}
		public Boolean getIsFinancialFlow() {
			return isFinancialFlow;
		}
		public void setIsFinancialFlow(Boolean isFinancialFlow) {
			this.isFinancialFlow = isFinancialFlow;
		}
		public Integer getMonth() {
			return month;
		}
		public void setMonth(Integer month) {
			this.month = month;
		}
		@Override
		public String toString() {
			return "EnrollmentIrsPolicyInfoDTO [qhpPolicyNum=" + qhpPolicyNum
					+ ", qhpIssuerEIN=" + qhpIssuerEIN
					+ ", pediatricDentalPlanPremiumInd="
					+ pediatricDentalPlanPremiumInd
					+ ", slcspAdjMonthlyPremiumAmt="
					+ slcspAdjMonthlyPremiumAmt + ", householdAPTCAmt="
					+ householdAPTCAmt + ", totalHsldMonthlyPremiumAmt="
					+ totalHsldMonthlyPremiumAmt + ", issuerNm=" + issuerNm
					+ ", policyCoverageStartDt=" + policyCoverageStartDt
					+ ", policyCoverageEndDt=" + policyCoverageEndDt
					+ ", totalQHPMonthlyPremiumAmt="
					+ totalQHPMonthlyPremiumAmt + ", aptcPaymentAmt="
					+ aptcPaymentAmt + ", isActive=" + isActive
					+ ", lastUpdate=" + lastUpdate + ", isFinancialFlow="
					+ isFinancialFlow + ", month=" + month
					+ ", coveredIndividualList=" + coveredIndividualList + "]";
		}

}
