package com.getinsured.hix.model.consumer;

import java.math.BigDecimal;

/**
 * 
 * @author hardas_d
 * This DTO will hold the data to be needed for Manage Consumer page.
 */
public class ManageApplicantsDto {

	private BigDecimal householdId;
	private String firstName;
	private String lastName;
	private String ssn;
	private long personId;
	private long ssapApplicationId;
	private String externalApplicantId;
	private String externalApplicationId;
	private String phoneNumber;
	private String source;
	private String primaryContactName;
	private String caseNumber;
	private String applicationStatus;
	private String applicationYear;
	private String applicantGuidCode;
	private String financialAssistanceFlag;
	private String appStatusDescription;
	private String authRepName;

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public BigDecimal getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(BigDecimal householdId) {
		this.householdId = householdId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getExternalApplicantId() {
		return externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPrimaryContactName() {
		return primaryContactName;
	}

	public void setPrimaryContactName(String primaryContactName) {
		this.primaryContactName = primaryContactName;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getApplicationYear() {
		return applicationYear;
	}

	public void setApplicationYear(String applicationYear) {
		this.applicationYear = applicationYear;
	}

	public String getApplicantGuidCode() {
		return applicantGuidCode;
	}

	public void setApplicantGuidCode(String applicantGuidCode) {
		this.applicantGuidCode = applicantGuidCode;
	}

	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}
	
	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}

	public String getAppStatusDescription() {
		return appStatusDescription;
	}

	public void setAppStatusDescription(String appStatusDescription) {
		this.appStatusDescription = appStatusDescription;
	}

	public String getAuthRepName() {
		return authRepName;
	}

	public void setAuthRepName(String authRepName) {
		this.authRepName = authRepName;
	}
}
