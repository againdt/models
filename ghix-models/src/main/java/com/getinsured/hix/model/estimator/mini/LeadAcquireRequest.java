package com.getinsured.hix.model.estimator.mini;

/**
 * This object represents the new fields we want to add
 * as part of outbound call tool
 * Please refer to HIX-29754 for more details
 * 
 * In the older version of api we do not have rich details about our
 * consumers from affiliates. This request object will add those missing attributes 
 * for our agents to call them. 
 * @author bhatt
 *
 */
public class LeadAcquireRequest extends MiniEstimatorRequest {
	//private String firstName;// Moved to MiniEstimator Request	
	//private String lastName;// Moved to MiniEstimator Request
	//private String isOkToCall; Moved this attribute to Mini Estimator Request
	private String tcpaComplianceLink;
	
	private String dn; // DNIS / Dailed Number from liveops integration partner
	
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	/*public String getIsOkToCall() {
		return isOkToCall;
	}
	public void setIsOkToCall(String isOkToCall) {
		this.isOkToCall = isOkToCall;
	}*/
	public String getTcpaComplianceLink() {
		return tcpaComplianceLink;
	}
	public void setTcpaComplianceLink(String tcpaComplianceLink) {
		this.tcpaComplianceLink = tcpaComplianceLink;
	}
	

}
