package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class Enrollment{
  @JsonProperty("coverageAmount")
  
  private Float coverageAmount;
  @JsonProperty("notes")
  
  private String notes;
  @JsonProperty("created")
  
  private String created;
  @JsonProperty("planName")
  
  private String planName;
  @JsonProperty("policyNo")
  
  private Integer policyNo;
  @JsonProperty("responseData")
  
  private String responseData;
  @JsonProperty("type")
  
  private String type;
  @JsonProperty("carrier")
  
  private String carrier;
  @JsonProperty("premium")
  
  private Float premium;
  @JsonProperty("id")
  
  private String id;
  @JsonProperty("requestData")
  
  private String requestData;
  @JsonProperty("updated")
  
  private String updated;
  @JsonProperty("effectiveDate")
  
  private String effectiveDate;
  public void setCoverageAmount(Float coverageAmount){
   this.coverageAmount=coverageAmount;
  }
  public Float getCoverageAmount(){
   return coverageAmount;
  }
  public void setNotes(String notes){
   this.notes=notes;
  }
  public String getNotes(){
   return notes;
  }
  public void setCreated(String created){
   this.created=created;
  }
  public String getCreated(){
   return created;
  }
  public void setPlanName(String planName){
   this.planName=planName;
  }
  public String getPlanName(){
   return planName;
  }
  public void setPolicyNo(Integer policyNo){
   this.policyNo=policyNo;
  }
  public Integer getPolicyNo(){
   return policyNo;
  }
  public void setResponseData(String responseData){
   this.responseData=responseData;
  }
  public String getResponseData(){
   return responseData;
  }
  public void setType(String type){
   this.type=type;
  }
  public String getType(){
   return type;
  }
  public void setCarrier(String carrier){
   this.carrier=carrier;
  }
  public String getCarrier(){
   return carrier;
  }
  public void setPremium(Float premium){
   this.premium=premium;
  }
  public Float getPremium(){
   return premium;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setRequestData(String requestData){
   this.requestData=requestData;
  }
  public String getRequestData(){
   return requestData;
  }
  public void setUpdated(String updated){
   this.updated=updated;
  }
  public String getUpdated(){
   return updated;
  }
  public void setEffectiveDate(String effectiveDate){
   this.effectiveDate=effectiveDate;
  }
  public String getEffectiveDate(){
   return effectiveDate;
  }
}