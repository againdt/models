package com.getinsured.hix.dto.directenrollment.payment;

import java.util.HashMap;

/**
 * Maps CyberSource response codes to GetInsured codes.
 * 
 * @see com.getinsured.directenrollment.model.trustcommerce.TrustCommercePaymentResponse
 * @author richard@igroupcreative.com
 *
 */
public class CyberSourceResponseDTO {
	public enum ResponseStatus {
		INTERNATIONAL, ERROR, DECLINE, DECLINE_AVS, DECLINE_CVV, BADDATA, APPROVED
	}

	private String requestId;
	private int reasonCode;
	private String decision;
	private ResponseStatus status;
	private HashMap<String, String> response;

	private static HashMap<Integer, ResponseStatus> statusMap;

	static {
		statusMap = new HashMap<>();
		statusMap.put(100, ResponseStatus.APPROVED);
		statusMap.put(101, ResponseStatus.BADDATA);
		statusMap.put(102, ResponseStatus.BADDATA);
		statusMap.put(200, ResponseStatus.DECLINE_AVS);
		statusMap.put(202, ResponseStatus.BADDATA);
		statusMap.put(204, ResponseStatus.DECLINE);
		statusMap.put(209, ResponseStatus.DECLINE_CVV);
		statusMap.put(210, ResponseStatus.DECLINE);
		statusMap.put(211, ResponseStatus.DECLINE_CVV);
		statusMap.put(220, ResponseStatus.DECLINE);
		statusMap.put(222, ResponseStatus.DECLINE);
		statusMap.put(223, ResponseStatus.DECLINE);
		statusMap.put(230, ResponseStatus.DECLINE_CVV);
		statusMap.put(231, ResponseStatus.BADDATA);
		statusMap.put(388, ResponseStatus.BADDATA);
	}

	public CyberSourceResponseDTO(HashMap<String, String> response) {
		if (response == null) {
			this.decision = "null response";
			this.status = ResponseStatus.ERROR;
		} else {
			this.requestId = response.get("requestID");
			this.reasonCode = Integer.parseInt(response.get("reasonCode"));
			this.decision = response.get("decision");
			this.status = statusMap.get(reasonCode);
			if (this.status == null) {
				this.status = ResponseStatus.ERROR;
			}
			this.response = response;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{");

		sb.append("\"requestId\":\"" + requestId + "\"");
		sb.append(",");
		sb.append("\"reasonCode\":\"" + reasonCode + "\"");
		sb.append(",");
		sb.append("\"decision\":\"" + decision + "\"");
		sb.append(",");
		sb.append("\"status\":\"" + status + "\"");
		sb.append(",");
		sb.append("\"response\":{");
		boolean first = true;
		if (response != null) {
			for (String key : response.keySet()) {
				if (!first) {
					sb.append(",");
				}
				first = false;
				sb.append("\"" + key + "\":\"" + response.get(key) + "\"");
			}
		}
		sb.append("}");

		return sb.toString();
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public int getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public HashMap<String, String> getResponse() {
		return response;
	}

	public void setResponse(HashMap<String, String> response) {
		this.response = response;
	}
}