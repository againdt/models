/** @author santanu
 * @version 1.0
 * @since May 26, 2017 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

public class SbcScenarioResponseDTO  extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Map<String, SbcScenarioDTO> sbcScenario;

	/**
	 * @return the sbcScenario
	 */
	public Map<String, SbcScenarioDTO> getSbcScenario() {
		return sbcScenario;
	}

	/**
	 * @param sbcScenario the sbcScenario to set
	 */
	public void setSbcScenario(Map<String, SbcScenarioDTO> sbcScenario) {
		this.sbcScenario = sbcScenario;
	}

	

}
