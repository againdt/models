package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_CMS_820_DATA")
public class EnrlCms820Data  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static DateFormat formatter = new SimpleDateFormat("yyyyMMdd");

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_820_DATA_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_820_DATA_SEQ", sequenceName = "ENRL_CMS_820_DATA_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="SUMMARY_ID")
	private Integer summaryId;
	
	@Column(name="HIOS_ISSUER_ID")
	private String hiosIssuerId;
	
	@Column(name="ISSUER_APTC_TOTAL")
	private BigDecimal issuerAptcTotal;
	
	@Column(name="ISSUER_CSR_TOTAL")
	private BigDecimal issuerCsrTotal;
	
	@Column(name="ISSUER_UF_TOTAL")
	private BigDecimal issuerUfTotal;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="NAME_PREFIX")
	private String namePrefix;
	
	@Column(name="NAME_SUFFIX")
	private String nameSuffix;
	
	@Column(name="EXCHG_ASSIGNED_SUBSCRIBER_ID")
	private String exchangeAssignedSubId;
	
	@Column(name="EXCHANGE_ASSIGNED_QHP_ID")
	private String exchangeAssignedQhpId;
	
	@Column(name="EXCHANGE_ASSIGNED_POLICY_ID")
	private Long exchangeAssignedPolicyId;
	
	@Column(name="ISSUER_ASSIGNED_POLICY_ID")
	private String issuerAssignedPolicyId;
	
	@Column(name="ISSUER_ASSIGNED_SUBSCRIBER_ID")
	private String issuerAssignedSubId;
	
	@Column(name="POLICY_TOTAL_PREMIUM_AMOUNT")
	private BigDecimal policyTotalPremiumAmount;
	
	@Column(name="EXCHANGE_PAYMENT_TYPE")
	private String exchangePaymentType;
	
	@Column(name="PAYMENT_AMOUNT")
	private BigDecimal paymentAmount;
	
	@Column(name="EXCHANGE_RELATED_REPORT_TYPE")
	private String exchangeRelatedReportType;
	
	@Column(name="EXCHANGE_REPORT_DOC_CNTRL_NUM")
	private String exchangeReportDocCntrNum;
	
	@Column(name="COVERAGE_PERIOD_START_DATE")
	private Date coveragePeriodStartDate;
	
	@Column(name="COVERAGE_PERIOD_END_DATE")
	private Date coveragePeriodEndDate;
	
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimeStamp;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSummaryId() {
		return summaryId;
	}

	public void setSummaryId(Integer summaryId) {
		this.summaryId = summaryId;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public BigDecimal getIssuerAptcTotal() {
		return issuerAptcTotal;
	}

	public void setIssuerAptcTotal(BigDecimal issuerAptcTotal) {
		this.issuerAptcTotal = issuerAptcTotal;
	}

	public BigDecimal getIssuerCsrTotal() {
		return issuerCsrTotal;
	}

	public void setIssuerCsrTotal(BigDecimal issuerCsrTotal) {
		this.issuerCsrTotal = issuerCsrTotal;
	}

	public BigDecimal getIssuerUfTotal() {
		return issuerUfTotal;
	}

	public void setIssuerUfTotal(BigDecimal issuerUfTotal) {
		this.issuerUfTotal = issuerUfTotal;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getNamePrefix() {
		return namePrefix;
	}

	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}

	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}

	public String getExchangeAssignedSubId() {
		return exchangeAssignedSubId;
	}

	public void setExchangeAssignedSubId(String exchangeAssignedSubId) {
		this.exchangeAssignedSubId = exchangeAssignedSubId;
	}

	public String getExchangeAssignedQhpId() {
		return exchangeAssignedQhpId;
	}

	public void setExchangeAssignedQhpId(String exchangeAssignedQhpId) {
		this.exchangeAssignedQhpId = exchangeAssignedQhpId;
	}

	public Long getExchangeAssignedPolicyId() {
		return exchangeAssignedPolicyId;
	}

	public void setExchangeAssignedPolicyId(Long exchangeAssignedPolicyId) {
		this.exchangeAssignedPolicyId = exchangeAssignedPolicyId;
	}

	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}

	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}

	public String getIssuerAssignedSubId() {
		return issuerAssignedSubId;
	}

	public void setIssuerAssignedSubId(String issuerAssignedSubId) {
		this.issuerAssignedSubId = issuerAssignedSubId;
	}

	public BigDecimal getPolicyTotalPremiumAmount() {
		return policyTotalPremiumAmount;
	}

	public void setPolicyTotalPremiumAmount(BigDecimal policyTotalPremiumAmount) {
		this.policyTotalPremiumAmount = policyTotalPremiumAmount;
	}

	public String getExchangePaymentType() {
		return exchangePaymentType;
	}

	public void setExchangePaymentType(String exchangePaymentType) {
		this.exchangePaymentType = exchangePaymentType;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getExchangeRelatedReportType() {
		return exchangeRelatedReportType;
	}

	public void setExchangeRelatedReportType(String exchangeRelatedReportType) {
		this.exchangeRelatedReportType = exchangeRelatedReportType;
	}

	public String getExchangeReportDocCntrNum() {
		return exchangeReportDocCntrNum;
	}

	public void setExchangeReportDocCntrNum(String exchangeReportDocCntrNum) {
		this.exchangeReportDocCntrNum = exchangeReportDocCntrNum;
	}

	public Date getCoveragePeriodStartDate() {
		return coveragePeriodStartDate;
	}

	public void setCoveragePeriodStartDate(Date coveragePeriodStartDate) {
		this.coveragePeriodStartDate = coveragePeriodStartDate;
	}

	public Date getCoveragePeriodEndDate() {
		return coveragePeriodEndDate;
	}

	public void setCoveragePeriodEndDate(Date coveragePeriodEndDate) {
		this.coveragePeriodEndDate = coveragePeriodEndDate;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimeStamp(new TSDate());
		this.setCreationTimeStamp(new TSDate()); 
	}
	
	@PreUpdate
	public void PreUpdate()
	{
		this.setCreationTimeStamp(new TSDate()); 
	}

	@Override
	public String toString() {
		return "EnrlCms820Data [hiosIssuerId=" + hiosIssuerId + ", issuerAptcTotal=" + issuerAptcTotal
				+ ", issuerCsrTotal=" + issuerCsrTotal + ", issuerUfTotal=" + issuerUfTotal + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", middleName=" + middleName + ", namePrefix=" + namePrefix
				+ ", nameSuffix=" + nameSuffix + ", exchangeAssignedSubId=" + exchangeAssignedSubId
				+ ", exchangeAssignedQhpId=" + exchangeAssignedQhpId + ", exchangeAssignedPolicyId="
				+ exchangeAssignedPolicyId + ", issuerAssignedPolicyId=" + issuerAssignedPolicyId
				+ ", issuerAssignedSubId=" + issuerAssignedSubId + ", policyTotalPremiumAmount="
				+ policyTotalPremiumAmount + ", exchangePaymentType=" + exchangePaymentType + ", paymentAmount="
				+ paymentAmount + ", exchangeRelatedReportType=" + exchangeRelatedReportType
				+ ", exchangeReportDocCntrNum=" + exchangeReportDocCntrNum + ", coveragePeriodStartDate="
				+ coveragePeriodStartDate + ", coveragePeriodEndDate=" + coveragePeriodEndDate + "]";
	}
	
	public String toCsv(char separator) {
		String csv = hiosIssuerId + separator +
				issuerAptcTotal + separator +
				issuerCsrTotal + separator +
				issuerUfTotal + separator +
				lastName + separator +
				firstName + separator +
				middleName + separator +
				namePrefix + separator +
				nameSuffix + separator +
				exchangeAssignedSubId + separator +
				exchangeAssignedQhpId + separator +
				exchangeAssignedPolicyId + separator +
				issuerAssignedPolicyId + separator +
				issuerAssignedSubId + separator +
				policyTotalPremiumAmount + separator +
				exchangePaymentType + separator +
				paymentAmount + separator +
				exchangeRelatedReportType + separator +
				exchangeReportDocCntrNum + separator +
				formatter.format(coveragePeriodStartDate) + separator +
				formatter.format(coveragePeriodEndDate);
		return csv.replaceAll("null", "");
	}
}
