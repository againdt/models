/**
 * 
 */
package com.getinsured.planmgmt.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author gorai_k
 *
 */
public class GreaterThanZeroValidator implements ConstraintValidator<GreaterThanZero, String>{
	
	public void initialize(GreaterThanZero object) {}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext validatorContext) {
	if(Integer.parseInt(value) > 0)
	{
	return true;
	}
	else
	{
	return false;
	}
	}

}
