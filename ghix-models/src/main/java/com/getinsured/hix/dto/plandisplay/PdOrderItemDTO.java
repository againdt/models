package com.getinsured.hix.dto.plandisplay;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.model.PdOrderItem;
import com.getinsured.hix.dto.plandisplay.PdOrderItemPersonDTO;

public class PdOrderItemDTO extends PdOrderItem {

	private static final long serialVersionUID = 1L;
	private List<PdOrderItemPersonDTO> pdOrderItemPersonDTOList;
	private BigDecimal aggrAptc;
	private String previousEnrollmentId;
	private boolean disenrollAllMembers;
	private BigDecimal maxStateSubsidy;

	public List<PdOrderItemPersonDTO> getPdOrderItemPersonDTOList() {
		return pdOrderItemPersonDTOList;
	}
	
	public void setPdOrderItemPersonDTOList(List<PdOrderItemPersonDTO> pdOrderItemPersonDTOList) {
		this.pdOrderItemPersonDTOList = pdOrderItemPersonDTOList;
	}

	public BigDecimal getAggrAptc() {
		return aggrAptc;
	}

	public void setAggrAptc(BigDecimal aggrAptc) {
		this.aggrAptc = aggrAptc;
	}

	public String getPreviousEnrollmentId() {
		return previousEnrollmentId;
	}

	public void setPreviousEnrollmentId(String previousEnrollmentId) {
		this.previousEnrollmentId = previousEnrollmentId;
	}

	public boolean getDisenrollAllMembers() {
		return disenrollAllMembers;
	}

	public void setDisenrollAllMembers(boolean disenrollAllMembers) {
		this.disenrollAllMembers = disenrollAllMembers;
	}

	public BigDecimal getMaxStateSubsidy() {
		return maxStateSubsidy;
	}

	public void setMaxStateSubsidy(BigDecimal maxStateSubsidy) {
		this.maxStateSubsidy = maxStateSubsidy;
	}
}