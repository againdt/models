package com.getinsured.eligibility.enums;

public enum AccountTransferQueueStatus {
	
	QUEUED("Queued"), DE_QUEUED("De-queued"), PROCESSED_REAL_TIME("Processed Real Time"), PROCESSED_BATCH("Processed Batch"), CLOSED("Closed");

	private String	description;

	private AccountTransferQueueStatus(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}