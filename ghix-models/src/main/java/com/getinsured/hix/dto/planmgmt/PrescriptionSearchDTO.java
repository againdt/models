/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

/**
 * @author shengole_s
 *
 */
public class PrescriptionSearchDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String drugName;
	//private String drugPrice;
	private String fairCost;
	private String coinsuranceAmount;
	private String copayAmount;
	private String isSupportPlan;
	private String rxCuiCode;
	private String drugTier;
	private String drugNameForTooltip;
	private String drugDosage;
	private String coinsurancePercentage;
	private String showBrandOrGenericName;
	private List<PrescriptionSearchDTO> associatedDrugs;	
	
	public List<PrescriptionSearchDTO> getAssociatedDrugs() {
		return associatedDrugs;
	}
	public void setAssociatedDrugs(List<PrescriptionSearchDTO> associatedDrugs) {
		this.associatedDrugs = associatedDrugs;
	}
	/**
	 * @return the coinsurancePercentage
	 */
	public String getCoinsurancePercentage() {
		return coinsurancePercentage;
	}
	/**
	 * @param coinsurancePercentage the coinsurancePercentage to set
	 */
	public void setCoinsurancePercentage(String coinsurancePercentage) {
		this.coinsurancePercentage = coinsurancePercentage;
	}
	/**
	 * @return the drugName
	 */
	public String getDrugName() {
		return drugName;
	}
	/**
	 * @param drugName the drugName to set
	 */
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	/**
	 * @return the fairCost
	 */
	public String getFairCost() {
		return fairCost;
	}
	/**
	 * @param fairCost the fairCost to set
	 */
	public void setFairCost(String fairCost) {
		this.fairCost = fairCost;
	}
	/**
	 * @return the coinsuranceAmount
	 */
	public String getCoinsuranceAmount() {
		return coinsuranceAmount;
	}
	/**
	 * @param coinsuranceAmount the coinsuranceAmount to set
	 */
	public void setCoinsuranceAmount(String coinsuranceAmount) {
		this.coinsuranceAmount = coinsuranceAmount;
	}
	/**
	 * @return the copayAmount
	 */
	public String getCopayAmount() {
		return copayAmount;
	}
	/**
	 * @param copayAmount the copayAmount to set
	 */
	public void setCopayAmount(String copayAmount) {
		this.copayAmount = copayAmount;
	}
	/**
	 * @return the isSupportPlan
	 */
	public String getIsSupportPlan() {
		return isSupportPlan;
	}
	/**
	 * @param isSupportPlan the isSupportPlan to set
	 */
	public void setIsSupportPlan(String isSupportPlan) {
		this.isSupportPlan = isSupportPlan;
	}
	/**
	 * @return the rxCuiCode
	 */
	public String getRxCuiCode() {
		return rxCuiCode;
	}
	/**
	 * @param rxCuiCode the rxCuiCode to set
	 */
	public void setRxCuiCode(String rxCuiCode) {
		this.rxCuiCode = rxCuiCode;
	}
	/**
	 * @return the drugTier
	 */
	public String getDrugTier() {
		return drugTier;
	}
	/**
	 * @param drugTier the drugTier to set
	 */
	public void setDrugTier(String drugTier) {
		this.drugTier = drugTier;
	}
	public String getDrugNameForTooltip() {
		return drugNameForTooltip;
	}
	public void setDrugNameForTooltip(String drugNameForTooltip) {
		this.drugNameForTooltip = drugNameForTooltip;
	}
	public String getDrugDosage() {
		return drugDosage;
	}
	public void setDrugDosage(String drugDosage) {
		this.drugDosage = drugDosage;
	}
	public String getShowBrandOrGenericName() {
		return showBrandOrGenericName;
	}
	public void setShowBrandOrGenericName(String showBrandOrGenericName) {
		this.showBrandOrGenericName = showBrandOrGenericName;
	}
	
}
