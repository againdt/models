package com.getinsured.hix.model;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The persistent class for the financial_info database table.
 * 
 */

@Audited
@Entity
@Table(name="financial_info")
public class FinancialInfo implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(FinancialInfo.class);
	
	public enum PaymentType 	{
		CREDITCARD, BANK, MANUAL,CHECK,EFT, ACH;
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FinancialInfo_Seq")
	@SequenceGenerator(name = "FinancialInfo_Seq", sequenceName = "financial_info_seq", allocationSize = 1)
	private int id;
	
	/*@ManyToOne
    @JoinColumn(name="person_id")
	private Person person;*/

	@Valid
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="bank_info_id")
	private BankInfo bankInfo;
	
	@Valid
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="credit_card_info_id")
	private CreditCardInfo creditCardInfo;
	
	@Column(name="first_name", length=60)
	private String firstName;

	@Column(name="last_name", length=60)
	private String lastName;
	
	@Column(name="email", length=100)
	private String email;
	
	@Column(name="address1")
	private String address1;
	
	@Column(name="address2")
	private String address2;

	@Column(name="city", length=60)
	private String city;

	@Column(name="state", length=2)
	private String state;
		
	@Column(name="zipcode")
	private String zipcode;
	
	@Column(name="country", length=20)
	private String country;
	
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "CREATION_TIMESTAMP")
	private Date created;

	@Column(name="payment_type", length=20)
	@Enumerated(EnumType.STRING)  
	private PaymentType paymentType;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updated;
    
    @Column(name="contact_number")
	private String contactNumber;
    
    @Valid
    @OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "location_id")
	private Location location;
    
    @Column(name = "last_updated_by")
   	private Integer lastUpdatedBy;

    public FinancialInfo() {
    }
    
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the person
	 *//*
	public Person getPerson() {
		return person;
	}

	*//**
	 * @param person the person to set
	 *//*
	public void setPerson(Person person) {
		this.person = person;
	}*/

	/**
	 * @return the bankInfo
	 */
	public BankInfo getBankInfo() {
		return bankInfo;
	}

	/**
	 * @param bankInfo the bankInfo to set
	 */
	public void setBankInfo(BankInfo bankInfo) {
		this.bankInfo = bankInfo;
	}

	/**
	 * @return the creditCardInfo
	 */
	public CreditCardInfo getCreditCardInfo() {
		return creditCardInfo;
	}

	/**
	 * @param creditCardInfo the creditCardInfo to set
	 */
	public void setCreditCardInfo(CreditCardInfo creditCardInfo) {
		this.creditCardInfo = creditCardInfo;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param string the country to set
	 */
	public void setCountry(String string) {
		this.country = string;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the paymentType
	 */
	public PaymentType getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
	
	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("firstName=").append(firstName).append(", \n");
        sb.append("lastName=").append(lastName).append(", \n");
        sb.append("address1=").append(address1).append(", \n");
        sb.append("address2=").append(address2).append(", \n");
        sb.append("email=").append(email).append(", \n");
        sb.append("city=").append(city).append(", \n");
        sb.append("state=").append(state).append(", \n");
        sb.append("zip=").append(zipcode).append(", \n");
        if(bankInfo != null){
        	sb.append("bankinfo=").append(bankInfo.toString()).append(", \n");
        }
        if(creditCardInfo != null){
        	sb.append("CreditCardInfo=").append(creditCardInfo.toString()).append(", \n");
        }
        return sb.toString();
    }
	
	public JSONObject toJson() {
		
		//StringBuilder sb = new StringBuilder();
		JSONObject JasonObj = new JSONObject();
		//String jsonText = "";
		Map<String,String> bankData = new HashMap<String,String>();
		try {
	        if(bankInfo != null){
	        	
	        	String accountNumber = "0000000000000000" + ( bankInfo.getAccountNumber().substring(bankInfo.getAccountNumber().length() - 4, bankInfo.getAccountNumber().length()) );
				
				bankData.put("accountNumber", accountNumber);
				bankData.put("bankName", bankInfo.getBankName());
				bankData.put("accountType", bankInfo.getAccountType());
				bankData.put("routingNumber", bankInfo.getRoutingNumber());
				
				JasonObj.put("bankData", bankData);
	        }
	        if(creditCardInfo != null){
	        	
	        	Map<String,String> creditCardData = new HashMap<String,String>();
				
				String cardNumber = "000000000000" + ( creditCardInfo.getCardNumber().substring(creditCardInfo.getCardNumber().length() - 4, creditCardInfo.getCardNumber().length()) ); 
				
				creditCardData.put("cardName", firstName + " " + lastName);
				creditCardData.put("cardNumber", cardNumber);
				creditCardData.put("cardType", creditCardInfo.getCardType());
				creditCardData.put("expirationMonth", creditCardInfo.getExpirationMonth());
				creditCardData.put("expirationYear", creditCardInfo.getExpirationYear());
				
				JasonObj.put("creditCardData", creditCardData);
	        }
	        
	        StringWriter out = new StringWriter();
	        JasonObj.write(out);
			
		}catch (JSONException e) {
			//e.printStackTrace();
			LOGGER.error("" , e);
		}
		return JasonObj;
	}
	
	@Override
	public Object clone(){
		FinancialInfo financialObj = new FinancialInfo();
		
		financialObj.setFirstName(getFirstName());
		financialObj.setLastName(getLastName());
		financialObj.setEmail(getEmail());
		financialObj.setAddress1(getAddress1());
		financialObj.setAddress2(getAddress1());
		financialObj.setCity(getCity());
		financialObj.setState(getState());
		financialObj.setZipcode(getZipcode());
		financialObj.setCountry(getCountry());
		
		return financialObj;
	}

	
}

