/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

/**
 * @author Srikanth Nakka
 *
 */
public class EnrollmentIds {

	private String issuerAssignPolicyNo;
	private String carrierAppId;
	private String exchangeAssignPolicyNo;
	
	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}
	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}
	public String getCarrierAppId() {
		return carrierAppId;
	}
	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}
	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}
	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}
}
