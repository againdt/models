package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.TenantIdPrePersistListener;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@XmlRootElement(name="enrollment")
@XmlAccessorType(XmlAccessType.NONE)
@IdClass(AudId.class)
@Table(name="ENROLLMENT_AUD")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class EnrollmentAud implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Id
	@Column(name = "REV")
	private int rev;

	@XmlElement(name="sponsorName")
	@Column(name="SPONSOR_NAME")
	private String sponsorName ;

	@XmlElement(name="sponsorEIN")
	@Column(name="SPONSOR_EIN")
	private String sponsorEIN;

	@XmlElement(name="sponsorTaxIdNumber")
	@Column(name="SPONSOR_TAX_ID_NUMBER")
	private String sponsorTaxIdNumber;

	@Column(name = "SUBSCRIBER_NAME")
	private String subscriberName;

	@XmlElement(name="enrollmentTypeLkp")
	@OneToOne
	@JoinColumn(name="ENROLLMENT_TYPE_LKP",nullable= true)
	private LookupValue enrollmentTypeLkp ;

	@XmlElement(name="insurerName")
	@Column(name="INSURER_NAME")
	private String insurerName ;

	@XmlElement(name="insurerTaxIdNumber")
	@Column(name="INSURER_TAX_ID_NUMBER")
	private String insurerTaxIdNumber ;

	@XmlElement(name="issuerSubscriberIdentifier")
	@Column(name="ISSUER_SUBSCRIBER_IDENTIFIER")
	private String issuerSubscriberIdentifier;

	@XmlElement(name="exchgSubscriberIdentifier")
	@Column(name="EXCHG_SUBSCRIBER_IDENTIFIER")
	private String exchgSubscriberIdentifier;

	@XmlElement(name="insuranceTypeLkp")
	@OneToOne
	@JoinColumn(name="INSURANCE_TYPE_LKP",nullable= true)
	private LookupValue insuranceTypeLkp ;

	@OneToOne
	@JoinColumn(name="ENROLLMENT_STATUS_LKP",nullable= true)

	private LookupValue enrollmentStatusLkp ;

	@XmlElement(name="benefitStartDate")
	@Column(name="BENEFIT_EFFECTIVE_DATE")
	private Date benefitEffectiveDate ;

	@XmlElement(name="benefitEndDate")
	@Column(name="BENEFIT_END_DATE")
	private Date benefitEndDate ;

	@XmlElement(name="netPremiumAmt")
	@Column(name="NET_PREMIUM_AMT")
	private Float netPremiumAmt ;

	@XmlElement(name="healthCoveragePremiumAmt")
	@Column(name="GROSS_PREMIUM_AMT")
	private Float grossPremiumAmt ;

	@XmlElement(name="aptcAmt")
	@Column(name="APTC_AMT")
	private Float aptcAmt ;

	@XmlElement(name="stateSubsidy")
	@Column(name="STATE_SUBSIDY_AMT")
	private BigDecimal stateSubsidyAmt;

	@Column(name="CSR_AMT")
	private Float csrAmt ;

	@XmlElement(name="houseHoldCaseId")
	@Column(name="HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId ;

	@XmlElement(name="groupPolicyNumber")
	@Column(name="GROUP_POLICY_NUMBER")
	private String groupPolicyNumber ;

	@Column(name="BROKER_TPA_FLAG")
	private String brokerTPAFlag ;

	@XmlElement(name="brokerAgentName")
	@Column(name="AGENT_BROKER_NAME")
	private String agentBrokerName ;

	@XmlElement(name="insurerCMSPlanID")
	@Column(name="CMS_PLAN_ID")
	private String CMSPlanID ;

	@XmlElement(name="brokerFEDTaxPayerId")
	@Column(name="BROKER_FED_TAX_PAYER_ID")
	private String brokerFEDTaxPayerId ;

	@XmlElement(name="brokerTPAAccountNumber1")
	@Column(name="BROKER_TPA_ACCOUNT_NUMBER_1")
	private String brokerTPAAccountNumber1 ;

	@XmlElement(name="brokerTPAAccountNumber2")
	@Column(name="BROKER_TPA_ACCOUNT_NUMBER_2")
	private String brokerTPAAccountNumber2 ;

	@XmlElement(name="createdOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;

	@XmlElement(name="updatedOn")
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;

	@Column(name="ind_order_items_id", insertable=false, updatable=false) 
	private Integer indOrderItemId;

	@Column(name="issuer_id") 
	private Integer issuerId;
	
	@Column(name="plan_id") 
	private Integer planId; 

	@Column(name = "PLAN_NAME")
	private String planName;

	@Column(name="PLAN_COVERAGE_LEVEL")
	private String planCoverageLevel ;


	@Column(name="FAMILY_COVERAGE_TIER")
	private String familyCoverageTier;

	@XmlElement(name="employeeContribution")
	@Column(name="EMPLOYEE_CONTRIBUTION")
	private Float employeeContribution ;

	@XmlElement(name="employerContribution")
	@Column(name="EMPLOYER_CONTRIBUTION")
	private Float employerContribution ;

	@Column(name="SADP")
	private String sadp ;

	@Column(name="ASSISTER_BROKER_ID")
	private Integer assisterBrokerId;

	@XmlElement(name="brokerRole")
	@Column(name="BROKER_ROLE")
	private String brokerRole;

	@Column(name="PLAN_LEVEL")
	private String planLevel;

	@Column(name="ENROLLMENT_CONFIRMATION_DATE")
	private Date enrollmentConfirmationDate;

	@XmlElement(name="employerCaseId")
	@Column(name="EMPLOYER_CASE_ID", length = 20)
	private String employerCaseId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="ABORT_TIMESTAMP")
	private Date abortTimestamp;	

	@Column(name="APTC_EFF_DATE")
	private Date aptcEffDate;

	@Column(name="STATE_SUBSIDY_EFF_DATE")
	private Date stateSubsidyEffDate;
	
	@Column(name="CSR_EFF_DATE")
	private Date csrEffDate;
	
	@Column(name="TOT_INDV_RESP_EFF_DATE")
	private Date netPremEffDate;
	
	@Column(name="TOT_EMPLR_RESP_EFF_DATE")
	private Date emplContEffDate;
	
	@Column(name="TOT_PREM_EFF_DATE")
	private Date grossPremEffDate;
	
	@Column(name="PAYMENT_TXN_ID")
	private String paymentTxnId;
	
	@Column(name="EHB_PERCENT")
	private Float ehbPercent;
	
	@Column(name="STATE_EHB_PERCENT")
	private Float stateEhbPercent;
	
	@Column(name="DNTL_EHB_PRM_DOLLAR_VAL")
	private Float dntlEssentialHealthBenefitPrmDollarVal; 
	
	@Column(name="SUBMITTED_TO_CARRIER_DATE")
	private Date submittedToCarrierDate ;
	
	@Column(name = "SSAP_APPLICATION_ID")
	private Long ssapApplicationid;
	
	@Column(name="SLCSP_AMT")
	private Float slcspAmt;
	
	@Column(name="SLCSP_EFF_DATE")
	private Date slcspEffDate;
	
	//HIX-64133
	@Column(name="PD_HOUSEHOLD_ID")
	private Integer pdHouseholdId;
	

	@Column(name="LAST_UPDATED_BY")
	private Integer updatedBy;
	

	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name="REVTYPE")
	private int revtype;
	
	@Column(name="LOG_GLOBAL_ID")
	private String logGlobalId;
	
	@Column(name="PRIOR_ENROLLMENT_ID")
	private Long priorEnrollmentId;
	
	@Column(name = "RENEWAL_FLAG")
	private String renewalFlag;
	
	@Column(name="LAST_ENROLLMENT_ID")
	private Integer lastEnrollmentId;
	
	@Column(name="DNTL_EHB_EFF_DATE")
	private Date dntlEhbEffDate;
	
	@Column(name="STATE_EHB_EFF_DATE")
	private Date stateEhbEffDate;
	
	@Column(name="EHB_EFF_DATE")
	private Date ehbEffDate;
	
	@Column(name="DNTL_EHB_AMT")
	private Float dntlEhbAmt;
	
	@Column(name="STATE_EHB_AMT")
	private Float stateEhbAmt;
	
	@Column(name="HIOS_ISSUER_ID", length=10)
	private String hiosIssuerId;
	
	@Column(name = "PRIOR_SSAP_APPLICATION_ID")
	private Long priorSsapApplicationid;
	
	@Column(name="EXT_HOUSEHOLD_CASE_ID")
	private String externalHouseHoldCaseId ;
	
	@Column(name="CHANGE_PLAN_ALLOWED")
	private String changePlanAllowed;
	
	@Column(name="PREM_PAID_TO_DATE_END")
	private Date premiumPaidToDateEnd;

	public Integer getLastEnrollmentId() {
		return lastEnrollmentId;
	}

	public void setLastEnrollmentId(Integer lastEnrollmentId) {
		this.lastEnrollmentId = lastEnrollmentId;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	public Long getPriorEnrollmentId() {
		return priorEnrollmentId;
	}

	public void setPriorEnrollmentId(Long priorEnrollmentId) {
		this.priorEnrollmentId = priorEnrollmentId;
	}

	public String getLogGlobalId() {
		return logGlobalId;
	}

	public void setLogGlobalId(String logGlobalId) {
		this.logGlobalId = logGlobalId;
	}
	
	public Date getSubmittedToCarrierDate() {
		return submittedToCarrierDate;
	}
	
	@Column(name="FINANCIAL_EFFECTIVE_DATE")
	private Date financialEffectiveDate;
	
	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}

	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}

	public void setSubmittedToCarrierDate(Date submittedToCarrierDate) {
		this.submittedToCarrierDate = submittedToCarrierDate;
	}

	public String getPaymentTxnId() {
		return paymentTxnId;
	}

	public void setPaymentTxnId(String paymentTxnId) {
		this.paymentTxnId = paymentTxnId;
	}
	
	public EnrollmentAud() {
		// TODO Auto-generated constructor stub
	}

	public Date getAptcEffDate() {
		return aptcEffDate;
	}

	public void setAptcEffDate(Date aptcEffDate) {
		this.aptcEffDate = aptcEffDate;
	}

	public Date getCsrEffDate() {
		return csrEffDate;
	}

	public void setCsrEffDate(Date csrEffDate) {
		this.csrEffDate = csrEffDate;
	}


	public Date getNetPremEffDate() {
		return netPremEffDate;
	}


	public void setNetPremEffDate(Date netPremEffDate) {
		this.netPremEffDate = netPremEffDate;
	}


	public Date getEmplContEffDate() {
		return emplContEffDate;
	}


	public void setEmplContEffDate(Date emplContEffDate) {
		this.emplContEffDate = emplContEffDate;
	}


	public Date getGrossPremEffDate() {
		return grossPremEffDate;
	}


	public void setGrossPremEffDate(Date grossPremEffDate) {
		this.grossPremEffDate = grossPremEffDate;
	}


	public String getAgentBrokerName() {
		return agentBrokerName;
	}

	public Float getAptcAmt() {
		return aptcAmt;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public String getBrokerFEDTaxPayerId() {
		return brokerFEDTaxPayerId;
	}

	public String getBrokerTPAAccountNumber1() {
		return brokerTPAAccountNumber1;
	}


	public String getBrokerTPAAccountNumber2() {
		return brokerTPAAccountNumber2;
	}

	public String getBrokerTPAFlag() {
		return brokerTPAFlag;
	}

	public String getCMSPlanID() {
		return CMSPlanID;
	}

	public Date getCreatedOn() {
		return createdOn;
	}


	public Float getCsrAmt() {
		return csrAmt;
	}

	public Float getEmployeeContribution() {
		return employeeContribution;
	}

	public Float getEmployerContribution() {
		return employerContribution;
	}

	public LookupValue getEnrollmentStatusLkp() {
		return enrollmentStatusLkp;
	}

	public LookupValue getEnrollmentTypeLkp() {
		return enrollmentTypeLkp;
	}

	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}

	public String getFamilyCoverageTier() {
		return familyCoverageTier;
	}

	public String getFormatBenefitEffectiveDate() {
		return getFormatedDate(this.benefitEffectiveDate);

	}

	public String getFormatBenefitEndDate() {
		return getFormatedDate(this.benefitEndDate);
	}

	public String getFormatCreatedOn() {
		return getFormatedDate(this.createdOn);

	}

	public String getFormatedDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyymmdd");
		String updatedOn = formatter.format(date);
		return updatedOn;

	}

	public String getFormatUpdatedOn() {
		return getFormatedDate(this.updatedOn);

	}

	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}


	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}

	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	public Integer getId() {
		return id;
	}

	public LookupValue getInsuranceTypeLkp() {
		return insuranceTypeLkp;
	}

	public String getInsurerName() {
		return insurerName;
	}

	public String getInsurerTaxIdNumber() {
		return insurerTaxIdNumber;
	}

	public String getIssuerSubscriberIdentifier() {
		return issuerSubscriberIdentifier;
	}

	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}

	public String getPlanCoverageLevel() {
		return planCoverageLevel;
	}


	public String getPlanName() {
		return planName;
	}

	public String getSadp() {
		return sadp;
	}

	public String getSponsorEIN() {
		return sponsorEIN;
	}
	public String getSponsorName() {
		return sponsorName;
	}

	public String getSponsorTaxIdNumber() {
		return sponsorTaxIdNumber;
	}

	public int getSubscriberCount() {
		return 1;
	}

	public String getSubscriberName() {
		return subscriberName;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	public void setAgentBrokerName(String agentBrokerName) {
		this.agentBrokerName = agentBrokerName;
	}

	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public void setBrokerFEDTaxPayerId(String brokerFEDTaxPayerId) {
		this.brokerFEDTaxPayerId = brokerFEDTaxPayerId;
	}

	public void setBrokerTPAAccountNumber1(String brokerTPAAccountNumber1) {
		this.brokerTPAAccountNumber1 = brokerTPAAccountNumber1;
	}

	public void setBrokerTPAAccountNumber2(String brokerTPAAccountNumber2) {
		this.brokerTPAAccountNumber2 = brokerTPAAccountNumber2;
	}

	public void setBrokerTPAFlag(String brokerTPAFlag) {
		this.brokerTPAFlag = brokerTPAFlag;
	}

	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setCsrAmt(Float csrAmt) {
		this.csrAmt = csrAmt;
	}

	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}

	public void setEnrollmentStatusLkp(LookupValue enrollmentStatusLkp) {
		this.enrollmentStatusLkp = enrollmentStatusLkp;
	}

	public void setEnrollmentTypeLkp(LookupValue enrollmentTypeLkp) {
		this.enrollmentTypeLkp = enrollmentTypeLkp;
	}

	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}

	public void setFamilyCoverageTier(String familyCoverageTier) {
		this.familyCoverageTier = familyCoverageTier;
	}

	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}

	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setInsuranceTypeLkp(LookupValue insuranceTypeLkp) {
		this.insuranceTypeLkp = insuranceTypeLkp;
	}

	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

	public void setInsurerTaxIdNumber(String insurerTaxIdNumber) {
		this.insurerTaxIdNumber = insurerTaxIdNumber;
	}

	public void setIssuerSubscriberIdentifier(String issuerSubscriberIdentifier) {
		this.issuerSubscriberIdentifier = issuerSubscriberIdentifier;
	}

	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}

	public void setPlanCoverageLevel(String planCoverageLevel) {
		this.planCoverageLevel = planCoverageLevel;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public void setSadp(String sadp) {
		this.sadp = sadp;
	}

	public void setSponsorEIN(String sponsorEIN) {
		this.sponsorEIN = sponsorEIN;
	}

	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}

	public void setSponsorTaxIdNumber(String sponsorTaxIdNumber) {
		this.sponsorTaxIdNumber = sponsorTaxIdNumber;
	}

	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getAssisterBrokerId() {
		return assisterBrokerId;
	}

	public void setAssisterBrokerId(Integer assisterBrokerId) {
		this.assisterBrokerId = assisterBrokerId;
	}

	public String getBrokerRole() {
		return brokerRole;
	}

	public void setBrokerRole(String brokerRole) {
		this.brokerRole = brokerRole;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Integer getIndOrderItemId() {
		return indOrderItemId;
	}

	public void setIndOrderItemId(Integer indOrderItemId) {
		this.indOrderItemId = indOrderItemId;
	}

	public Date getEnrollmentConfirmationDate() {
		return enrollmentConfirmationDate;
	}

	public Float getDntlEssentialHealthBenefitPrmDollarVal() {
		return dntlEssentialHealthBenefitPrmDollarVal;
	}

	public void setDntlEssentialHealthBenefitPrmDollarVal(
			Float dntlEssentialHealthBenefitPrmDollarVal) {
		this.dntlEssentialHealthBenefitPrmDollarVal = dntlEssentialHealthBenefitPrmDollarVal;
	}

	public void setEnrollmentConfirmationDate(Date enrollmentConfirmationDate) {
		this.enrollmentConfirmationDate = enrollmentConfirmationDate;
	}

	public String getEmployerCaseId() {
		return employerCaseId;
	}

	public void setEmployerCaseId(String employerCaseId) {
		this.employerCaseId = employerCaseId;
	}

	public Date getAbortTimestamp() {
		return abortTimestamp;
	}

	public void setAbortTimestamp(Date abortTimestamp) {
		this.abortTimestamp = abortTimestamp;
	}

	public Long getSsapApplicationid() {
		return ssapApplicationid;
	}

	public void setSsapApplicationid(Long ssapApplicationid) {
		this.ssapApplicationid = ssapApplicationid;
	}

	public Integer getPdHouseholdId() {
		return pdHouseholdId;
	}

	public void setPdHouseholdId(Integer pdHouseholdId) {
		this.pdHouseholdId = pdHouseholdId;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public int getRevtype() {
		return revtype;
	}

	public void setRevtype(int revtype) {
		this.revtype = revtype;
	}

	public Float getSlcspAmt() {
		return slcspAmt;
	}

	public void setSlcspAmt(Float slcspAmt) {
		this.slcspAmt = slcspAmt;
	}

	public Date getSlcspEffDate() {
		return slcspEffDate;
	}

	public void setSlcspEffDate(Date slcspEffDate) {
		this.slcspEffDate = slcspEffDate;
	}

	public Float getEhbPercent() {
		return ehbPercent;
	}

	public void setEhbPercent(Float ehbPercent) {
		this.ehbPercent = ehbPercent;
	}

	public Date getEhbEffDate() {
		return ehbEffDate;
	}

	public void setEhbEffDate(Date ehbEffDate) {
		this.ehbEffDate = ehbEffDate;
	}

	public Date getDntlEhbEffDate() {
		return dntlEhbEffDate;
	}

	public void setDntlEhbEffDate(Date dntlEhbEffDate) {
		this.dntlEhbEffDate = dntlEhbEffDate;
	}

	public Float getStateEhbPercent() {
		return stateEhbPercent;
	}

	public void setStateEhbPercent(Float stateEhbPercent) {
		this.stateEhbPercent = stateEhbPercent;
	}

	public Date getStateEhbEffDate() {
		return stateEhbEffDate;
	}

	public void setStateEhbEffDate(Date stateEhbEffDate) {
		this.stateEhbEffDate = stateEhbEffDate;
	}
	
	public Float getDntlEhbAmt() {
		return dntlEhbAmt;
	}

	public void setDntlEhbAmt(Float dntlEhbAmt) {
		this.dntlEhbAmt = dntlEhbAmt;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public Long getPriorSsapApplicationid() {
		return priorSsapApplicationid;
	}

	public void setPriorSsapApplicationid(Long priorSsapApplicationid) {
		this.priorSsapApplicationid = priorSsapApplicationid;
	}

	public String getExternalHouseHoldCaseId() {
		return externalHouseHoldCaseId;
	}

	public void setExternalHouseHoldCaseId(String externalHouseHoldCaseId) {
		this.externalHouseHoldCaseId = externalHouseHoldCaseId;
	}

	public String getChangePlanAllowed() {
		return changePlanAllowed;
	}

	public void setChangePlanAllowed(String changePlanAllowed) {
		this.changePlanAllowed = changePlanAllowed;
	}

	public Date getPremiumPaidToDateEnd() {
		return premiumPaidToDateEnd;
	}

	public void setPremiumPaidToDateEnd(Date premiumPaidToDateEnd) {
		this.premiumPaidToDateEnd = premiumPaidToDateEnd;
	}

	public BigDecimal getStateSubsidyAmt() {
		return stateSubsidyAmt;
	}

	public void setStateSubsidyAmt(BigDecimal stateSubsidyAmt) {
		this.stateSubsidyAmt = stateSubsidyAmt;
	}

	public Date getStateSubsidyEffDate() {
		return stateSubsidyEffDate;
	}

	public void setStateSubsidyEffDate(Date stateSubsidyEffDate) {
		this.stateSubsidyEffDate = stateSubsidyEffDate;
	}
}
