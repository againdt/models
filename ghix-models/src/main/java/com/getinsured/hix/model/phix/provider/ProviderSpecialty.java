package com.getinsured.hix.model.phix.provider;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="provider_specialty")
public class ProviderSpecialty  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private ProviderSpecialtyDomain providerSpecialtyDomain;
	private ProviderTypeDomain providerTypeDomain;
	private ProviderProduct providerProduct;
	private ProviderBoardCertDomain providerBoardCertDomain;
	private String primarySpec;


	@Id 
	@SequenceGenerator(name="PROVIDER_SPECIALTY_SEQ", sequenceName="PROVIDER_SPECIALTY_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="PROVIDER_SPECIALTY_SEQ")
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SPECIALTY_ID")
	public ProviderSpecialtyDomain getProviderSpecialtyDomain() {
		return this.providerSpecialtyDomain;
	}

	public void setProviderSpecialtyDomain(ProviderSpecialtyDomain providerSpecialtyDomain) {
		this.providerSpecialtyDomain = providerSpecialtyDomain;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROVIDER_TYPE_ID")
	public ProviderTypeDomain getProviderTypeDomain() {
		return this.providerTypeDomain;
	}

	public void setProviderTypeDomain(ProviderTypeDomain providerTypeDomain) {
		this.providerTypeDomain = providerTypeDomain;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROVIDER_PRODUCT_ID")
	public ProviderProduct getProviderProduct() {
		return this.providerProduct;
	}

	public void setProviderProduct(ProviderProduct providerProduct) {
		this.providerProduct = providerProduct;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BOARD_CERT_ID")
	public ProviderBoardCertDomain getProviderBoardCertDomain() {
		return this.providerBoardCertDomain;
	}

	public void setProviderBoardCertDomain(ProviderBoardCertDomain providerBoardCertDomain) {
		this.providerBoardCertDomain = providerBoardCertDomain;
	}


	@Column(name="PRIMARY_SPEC", length=25)
	public String getPrimarySpec() {
		return this.primarySpec;
	}

	public void setPrimarySpec(String primarySpec) {
		this.primarySpec = primarySpec;
	}




}


