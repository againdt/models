package com.getinsured.hix.dto.directenrollment;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vijay
 *
 */

public class HCSCDenRiderCountyPremResDTO {
	private String status;
	private String code;
	private String message;
	
	private List<HCSCDentalRiderCountyPremiumDTO> planAndPremiumList = new ArrayList<HCSCDentalRiderCountyPremiumDTO>();	
	
	public HCSCDenRiderCountyPremResDTO() {
		super();		
	}


	public HCSCDenRiderCountyPremResDTO(String status, String code, String message, 
			List<HCSCDentalRiderCountyPremiumDTO> planAndPremiumList) {
		super();
		this.status = status;
		this.code = code;		
		this.message = message;
		this.planAndPremiumList = planAndPremiumList;		
	}

	@Override
	public String toString() {
		return "HCSCDenRiderCountyPremUtilStatusDTO [status=" + status + ", code="
				+ code + ", message=" + message + ", planAndPremiumList = " + planAndPremiumList + "]";
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public List<HCSCDentalRiderCountyPremiumDTO> getPlanAndPremiumList() {
		return planAndPremiumList;
	}	
	
}