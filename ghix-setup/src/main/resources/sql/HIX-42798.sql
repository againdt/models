----------------------------------------------------------------------------------
-------------- Below Insert statements are for INDIVIDUAL role.------------------- 
----------------------------------------------------------------------------------
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_VIEW_AND_PRINT'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_START_APP'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_RESUME_APP'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_ENROLL_APP'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_REPORT_CHANGES'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_VIEW_VER_RESULTS'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_VIEW_ELIG_RESULTS'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_CONTACT_US'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_SUBMIT_APPEAL'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_INBOX'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_ACCOUNT_SETTINGS'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_CHANGE_PASSWORD'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='INDIVIDUAL'), 
                                										(select id from permissions where name='PORTAL_CHANGE_SEC_QSTNS'));
