/*
--- Here is the sample sql statements to GHIX_OWNER has not been created, do
drop user GHIX_OWNER cascade;
create user GHIX_OWNER identified by GHIX_OWNER default tablespace devdbtbs temporary tablespace temp
quota unlimited on DEVDBTBS;
grant connect to GHIX_OWNER;
grant user_role to GHIX_OWNER;
!mkdir /home/oracle/dataload_dir
*/
CREATE DIRECTORY DATALOAD_DIR AS '/home/oracle/dataload_dir';
GRANT READ,WRITE ON DIRECTORY DATALOAD_DIR to GHIX_OWNER;
quit
