package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class LocationDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8057560569227277600L;
	private int id;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String countycode;
	private String zip;
	private String county;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountycode() {
		return countycode;
	}

	public void setCountycode(String countycode) {
		this.countycode = countycode;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id=").append(id).append(", ");
		sb.append("address1=").append(address1).append(", ");
		sb.append("address2=").append(address2).append(", ");
		sb.append("city=").append(city).append(", ");
		sb.append("county=").append(county).append(", ");
		sb.append("countycode=").append(countycode).append(", ");
		sb.append("zip=").append(zip).append(", ");
		sb.append("state=").append(state);
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LocationDto) {

			LocationDto loc = (LocationDto) obj;
			if (loc != null) {
				if ((loc.getAddress1() == null && this.getAddress1() == null)
						|| (loc.getAddress1() != null && this.getAddress1() != null
								&& this.getAddress1().trim().equalsIgnoreCase(loc.getAddress1().trim()))) {

					if ((loc.getAddress2() == null && this.getAddress2() == null)
							|| (loc.getAddress2() != null && this.getAddress2() != null
									&& this.getAddress2().trim().equalsIgnoreCase(loc.getAddress2().trim()))) {

						if ((loc.getCity()==null && this.getCity()==null) || (loc.getCity() != null && this.getCity() != null
								&& this.getCity().trim().equalsIgnoreCase(loc.getCity().trim()))) {

							if ((loc.getState()==null && this.getState()==null) || (loc.getState() != null && this.getState() != null
									&& this.getState().trim().equalsIgnoreCase(loc.getState().trim()))) {

								if ((loc.getZip()==null && this.getZip()==null) || (this.getZip().equals(loc.getZip()))) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

}
