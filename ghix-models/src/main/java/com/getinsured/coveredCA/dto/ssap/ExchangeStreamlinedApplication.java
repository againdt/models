package com.getinsured.coveredCA.dto.ssap;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class ExchangeStreamlinedApplication {

	
	@Expose
	private Boolean acceptPrivacyIndicator;
	@Expose
	private String ssapApplicationId;
	@Expose
	private String applicationGuid;
	@Expose
	private String exchangeType;   
	@Expose
	private String applicationStartDate;
	@Expose
	private Integer noOfHouseholdMembers;
	@Expose
	private String aptcAmount;
	@Expose
	private List<MemberRelationship> memberRelationship = new ArrayList<MemberRelationship>();
	@Expose
	private List<HouseholdMember> householdMember = new ArrayList<HouseholdMember>();
	@Expose
	private List<Integer> dependentsId = new ArrayList<Integer>();
	@Expose
	private SignAndSubmit signAndSubmit = new SignAndSubmit();
	@Expose
	private HouseholdIncome householdIncome = new HouseholdIncome();
	@Expose
	private SpecialEnrollmentPeriod specialEnrollmentPeriod = new SpecialEnrollmentPeriod();
	@Expose
	private Boolean isRidpVerified;
	@Expose
	private Integer householdRepeat = 1;
	@Expose
	private Integer spouseId = 0;
	@Expose
	private Boolean houseHoldMembersVerified;
	@Expose
	private Integer primaryTaxFilerId = 1;
	@Expose
	private Boolean screenerConfirmIndicator;
	@Expose
	private PlanDetails planDetails;
	@Expose
	private String proxyAptcAmount;
	
	
	
	
	public String getProxyAptcAmount() {
		return proxyAptcAmount;
	}

	public void setProxyAptcAmount(String proxyAptcAmount) {
		this.proxyAptcAmount = proxyAptcAmount;
	}

	public List<Integer> getDependentsId() {
		return dependentsId;
	}

	public void setDependentsId(List<Integer> dependentsId) {
		this.dependentsId = dependentsId;
	}

	public Integer getSpouseId() {
		return spouseId;
	}

	public void setSpouseId(Integer spouseId) {
		this.spouseId = spouseId;
	}
	
	

	public PlanDetails getPlanDetails() {
		return planDetails;
	}

	public void setPlanDetails(PlanDetails planDetails) {
		this.planDetails = planDetails;
	}

	public Boolean getHouseHoldMembersVerified() {
		return houseHoldMembersVerified;
	}

	public void setHouseHoldMembersVerified(Boolean houseHoldMembersVerified) {
		this.houseHoldMembersVerified = houseHoldMembersVerified;
	}


	public String getAptcAmount() {
		return aptcAmount;
	}

	public void setAptcAmount(String aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	

	public Integer getNoOfHouseholdMembers() {
		return noOfHouseholdMembers;
	}

	public void setNoOfHouseholdMembers(Integer noOfHouseholdMembers) {
		this.noOfHouseholdMembers = noOfHouseholdMembers;
	}

	
	public List<MemberRelationship> getMemberRelationship() {
		return memberRelationship;
	}

	public void setMemberRelationship(List<MemberRelationship> memberRelationship) {
		this.memberRelationship = memberRelationship;
	}

	public List<HouseholdMember> getHouseholdMember() {
		return householdMember;
	}

	public void setHouseholdMember(List<HouseholdMember> householdMember) {
		this.householdMember = householdMember;
	}

	public HouseholdIncome getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(HouseholdIncome householdIncome) {
		this.householdIncome = householdIncome;
	}

	public SpecialEnrollmentPeriod getSpecialEnrollmentPeriod() {
		return specialEnrollmentPeriod;
	}

	public void setSpecialEnrollmentPeriod(
			SpecialEnrollmentPeriod specialEnrollmentPeriod) {
		this.specialEnrollmentPeriod = specialEnrollmentPeriod;
	}

	public SignAndSubmit getSignAndSubmit() {
		return signAndSubmit;
	}

	public void setSignAndSubmit(SignAndSubmit signAndSubmit) {
		this.signAndSubmit = signAndSubmit;
	}

	

	public Boolean getAcceptPrivacyIndicator() {
		return acceptPrivacyIndicator;
	}

	public void setAcceptPrivacyIndicator(Boolean acceptPrivacyIndicator) {
		this.acceptPrivacyIndicator = acceptPrivacyIndicator;
	}

	public String getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(String ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}


	public String getApplicationGuid() {
		return applicationGuid;
	}

	public void setApplicationGuid(String applicationGuid) {
		this.applicationGuid = applicationGuid;
	}

	
	public String getApplicationStartDate() {
		return applicationStartDate;
	}

	public void setApplicationStartDate(String applicationStartDate) {
		this.applicationStartDate = applicationStartDate;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public Boolean getIsRidpVerified() {
		return isRidpVerified;
	}

	public void setIsRidpVerified(Boolean isRidpVerified) {
		this.isRidpVerified = isRidpVerified;
	}


	public Boolean getScreenerConfirmIndicator() {
		return screenerConfirmIndicator;
	}

	public void setScreenerConfirmIndicator(Boolean screenerConfirmIndicator) {
		this.screenerConfirmIndicator = screenerConfirmIndicator;
	}

	public void setHouseholdRepeat(Integer householdRepeat) {
		this.householdRepeat = householdRepeat;
	}

	public void setPrimaryTaxFilerId(Integer primaryTaxFilerId) {
		this.primaryTaxFilerId = primaryTaxFilerId;
	}

	public Integer getHouseholdRepeat() {
		return householdRepeat;
	}

	public Integer getPrimaryTaxFilerId() {
		return primaryTaxFilerId;
	}
	
	
	
}
