package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * @author sharma_k
 *
 */
public class EnrlDiscrepancyDataDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long enrollmentId;
	private Integer issuerEnrollmentId;
	private String subscriberIdentifier;
	private String hiosIssuerId;
	private String subscriberName;
	private String benefitEffectiveDate;
	private Map<String, Object> discrepancyStatusDataMap;
	private String lastReconFileName;
	private String lastReconcileDate;
	private Long numberOfTimeReconciled;
	
	public Long getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getSubscriberIdentifier() {
		return subscriberIdentifier;
	}
	public void setSubscriberIdentifier(String subscriberIdentifier) {
		this.subscriberIdentifier = subscriberIdentifier;
	}
	public String getLastReconFileName() {
		return lastReconFileName;
	}
	public void setLastReconFileName(String lastReconFileName) {
		this.lastReconFileName = lastReconFileName;
	}
	public Integer getIssuerEnrollmentId() {
		return issuerEnrollmentId;
	}
	public void setIssuerEnrollmentId(Integer issuerEnrollmentId) {
		this.issuerEnrollmentId = issuerEnrollmentId;
	}
	public Map<String, Object> getDiscrepancyStatusDataMap() {
		return discrepancyStatusDataMap;
	}
	public void setDiscrepancyStatusDataMap(Map<String, Object> discrepancyStatusDataMap) {
		this.discrepancyStatusDataMap = discrepancyStatusDataMap;
	}
	public String getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public void setBenefitEffectiveDate(String benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public Long getNumberOfTimeReconciled() {
		return numberOfTimeReconciled;
	}
	public void setNumberOfTimeReconciled(Long numberOfTimeReconciled) {
		this.numberOfTimeReconciled = numberOfTimeReconciled;
	}
	public String getLastReconcileDate() {
		return lastReconcileDate;
	}
	public void setLastReconcileDate(String lastReconcileDate) {
		this.lastReconcileDate = lastReconcileDate;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	public String getSubscriberName() {
		return subscriberName;
	}
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
}
