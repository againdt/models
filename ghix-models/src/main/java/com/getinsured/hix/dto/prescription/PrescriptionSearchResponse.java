package com.getinsured.hix.dto.prescription;

import java.util.List;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class PrescriptionSearchResponse 
{
	
	private String drugName;
	private String drugRxCode;
	private String drugNameForTooltip;
	private String tileDisplayVal;
	private String displayVal;
	private String isDrugCovered;
	private String showBrandOrGenericName;
	private String isAvailable;
	private List<PrescriptionSearchResponse> associatedDrugs;
	private String drugInfo;
	private String fairCost;
	
	public String getDrugInfo() {
		return drugInfo;
	}
	public void setDrugInfo(String drugInfo) {
		this.drugInfo = drugInfo;
	}
	public List<PrescriptionSearchResponse> getAssociatedDrugs() {
		return associatedDrugs;
	}
	public void setAssociatedDrugs(List<PrescriptionSearchResponse> associatedDrugs) {
		this.associatedDrugs = associatedDrugs;
	}
	public String getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(String isAvailable) {
		this.isAvailable = isAvailable;
	}
	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public String getDrugRxCode() {
		return drugRxCode;
	}
	public void setDrugRxCode(String drugRxCode) {
		this.drugRxCode = drugRxCode;
	}
	public String getDrugNameForTooltip() {
		return drugNameForTooltip;
	}
	public void setDrugNameForTooltip(String drugNameForTooltip) {
		this.drugNameForTooltip = drugNameForTooltip;
	}
	public String getTileDisplayVal() {
		return tileDisplayVal;
	}
	public void setTileDisplayVal(String tileDisplayVal) {
		this.tileDisplayVal = tileDisplayVal;
	}
	public String getDisplayVal() {
		return displayVal;
	}
	public void setDisplayVal(String displayVal) {
		this.displayVal = displayVal;
	}
	public String getIsDrugCovered() {
		return isDrugCovered;
	}
	public void setIsDrugCovered(String isDrugCovered) {
		this.isDrugCovered = isDrugCovered;
	}
	public String getShowBrandOrGenericName() {
		return showBrandOrGenericName;
	}
	public void setShowBrandOrGenericName(String showBrandOrGenericName) {
		this.showBrandOrGenericName = showBrandOrGenericName;
	}
	public String getFairCost() {
		return fairCost;
	}
	public void setFairCost(String fairCost) {
		this.fairCost = fairCost;
	}

}
