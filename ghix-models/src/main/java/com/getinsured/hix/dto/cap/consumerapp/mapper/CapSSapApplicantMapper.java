package com.getinsured.hix.dto.cap.consumerapp.mapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.platform.lookup.repository.ILookupRepository;

public class CapSSapApplicantMapper {

	@Autowired ILookupRepository iLookupRepository;
	
	public SsapApplicantDTO getDtoFromObject(Enrollee enrollee) {
		SsapApplicantDTO dto = null;
		
		if(enrollee  !=null) {
			dto = new SsapApplicantDTO();
			dto.setId(enrollee.getId());
			dto.setBirthDate(enrollee.getBirthDate());
			dto.setEligibilityProgramDTO(null);
			dto.setEligibilityProgramDTOList(null);
	//		dto.setEligibilityStatus(enrollee.getE)
			dto.setEmail(enrollee.getPreferredEmail());
	//		dto.setExternalApplicantId(enrollee.getEx)
			dto.setFirstName(enrollee.getFirstName());
			
			LookupValue genderlkp = enrollee.getGenderLkp();
			dto.setGender((genderlkp !=null) ? genderlkp.getLookupValueLabel() : null);
	//		dto.setHouseholdContactFlag(enrollee.getpr)
			dto.setId(enrollee.getId());
			dto.setLastName(enrollee.getLastName());
	//		dto.setMailingLocation(enrollee.getM)
			
			LookupValue marriedlkp = enrollee.getMaritalStatusLkp();			
			dto.setMarried((marriedlkp !=null) ? marriedlkp.getLookupValueLabel() : null);
			
			dto.setMiddleName(enrollee.getMiddleName());
			dto.setMonthlyAptcAmount((enrollee.getAptcAmt() !=null) ? enrollee.getAptcAmt().intValue() : 0);
			dto.setPhoneNumber(enrollee.getPrimaryPhoneNo());
			
			LookupValue relationshipLkp = enrollee.getRelationshipToHCPLkp(); 
			dto.setRelationship((relationshipLkp!=null) ? relationshipLkp.getLookupValueLabel() : null);
			
			LookupValue personType = enrollee.getPersonTypeLkp();
			dto.setPersonType((personType!=null) ? personType.getLookupValueLabel() : null);
			
			LookupValue tobaccoUsageLkp = enrollee.getTobaccoUsageLkp(); 
			dto.setTobaccoUser((tobaccoUsageLkp !=null) ? tobaccoUsageLkp.getLookupValueLabel() : null);
			
			dto.setHomeAddress(enrollee.getHomeAddressid());
			
			
			if(StringUtils.isNotEmpty(dto.getTobaccoUser())){
				dto.setTobaccoUser((tobaccoUsageLkp.getLookupValueLabel().equalsIgnoreCase("No Tobacco Use")||tobaccoUsageLkp.getLookupValueLabel().equalsIgnoreCase("Unknown Tobacco Use"))?"No":"Yes");
			}
			
			dto.setSsn(enrollee.getTaxIdNumber());
			
		}
		
		return dto;
	} 

	public Enrollee getEntity(SsapApplicantDTO dto) {
		Enrollee enrollee = null;
		
		if(dto != null) {
			enrollee = new Enrollee();
			if(dto.getId()!=0){
			 enrollee.setId((int) dto.getId());
			}
			enrollee.setBirthDate(dto.getBirthDate());
			enrollee.setPreferredEmail(dto.getEmail());
			enrollee.setFirstName(dto.getFirstName());
			enrollee.setLastName(dto.getLastName());
			enrollee.setMiddleName(dto.getMiddleName());
			if(null!=dto.getMonthlyAptcAmount()){
				enrollee.setAptcAmt(new Float(dto.getMonthlyAptcAmount()).floatValue());
			}
			enrollee.setPrimaryPhoneNo(dto.getPhoneNumber());
			enrollee.setTaxIdNumber(dto.getSsn());
			
			
		}
		return enrollee;
	}

}
