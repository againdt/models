package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author rajaramesh_g
 * @since 08/30/2013
 */
public class EnrolleeShopDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String policyNumber;
	private String firstName;
	private String lastName;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private Integer planId;
	private Integer enrollmentId;
	private Float monthlyPremium;
	private Float employerContribution;
	private Float monthlyPayrollDeduction;
	private Integer issuerId;
	private String exchgIndivIdentifier;
	private String eventReason;
	private String personType;
	private String enrolleeLookUpValue;
	private Integer age;
	private Float totalIndvResponsibilityAmt;
	private Date quotingDate;
	private Date birthDate;
	private String relationshipToHcpLkpCode; //HIX-109012
	private String homeAddress1;
	private String homeAddress2;
	private String homeCity;
	private String homeState;
	private String homeCounty;
	private String homeCountyCode;
	private String homeZip;
	
	
	public EnrolleeShopDTO()
	{
		//Default constructor
	}
	public EnrolleeShopDTO(String policyNumber, String firstName, String lastName,Date effectiveStartDate, Date effectiveEndDate, 
			String exchgIndivIdentifier, String personType, String enrolleeLookUpValue, Integer age, Float totalIndvResponsibilityAmt,
			Date quotingDate, Date birthDate, String relationshipToHcpLkpCode)
	{
		this.policyNumber = policyNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.effectiveStartDate = effectiveStartDate;
		this.effectiveEndDate = effectiveEndDate;
		this.exchgIndivIdentifier = exchgIndivIdentifier;
		this.personType = personType;
		this.enrolleeLookUpValue = enrolleeLookUpValue;
		this.age = age;
		this.totalIndvResponsibilityAmt=totalIndvResponsibilityAmt;
		this.quotingDate = quotingDate;
		this.birthDate = birthDate;
		this.relationshipToHcpLkpCode = relationshipToHcpLkpCode;
	}

	public EnrolleeShopDTO(String policyNumber, String firstName, String lastName, Date effectiveStartDate,
			Date effectiveEndDate, String exchgIndivIdentifier, String personType, String enrolleeLookUpValue,
			Integer age, Float totalIndvResponsibilityAmt, Date quotingDate, Date birthDate,
			String relationshipToHcpLkpCode, String address1, String address2, String city,
			String state, String county, String countycode, String zip) {
		this.policyNumber = policyNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.effectiveStartDate = effectiveStartDate;
		this.effectiveEndDate = effectiveEndDate;
		this.exchgIndivIdentifier = exchgIndivIdentifier;
		this.personType = personType;
		this.enrolleeLookUpValue = enrolleeLookUpValue;
		this.age = age;
		this.totalIndvResponsibilityAmt = totalIndvResponsibilityAmt;
		this.quotingDate = quotingDate;
		this.birthDate = birthDate;
		this.relationshipToHcpLkpCode = relationshipToHcpLkpCode;
		this.homeAddress1 = address1;
		this.homeAddress2 = address2;
		this.homeCity = city;
		this.homeState = state;
		this.homeCounty = county;
		this.homeCountyCode = countycode;
		this.homeZip = zip;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Float getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(Float monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	public Float getEmployerContribution() {
		return employerContribution;
	}
	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}
	public Float getMonthlyPayrollDeduction() {
		return monthlyPayrollDeduction;
	}
	public void setMonthlyPayrollDeduction(Float monthlyPayrollDeduction) {
		this.monthlyPayrollDeduction = monthlyPayrollDeduction;
	}
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public String getEventReason() {
		return eventReason;
	}
	public void setEventReason(String eventReason) {
		this.eventReason = eventReason;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public String getEnrolleeLookUpValue() {
		return enrolleeLookUpValue;
	}
	public void setEnrolleeLookUpValue(String enrolleeLookUpValue) {
		this.enrolleeLookUpValue = enrolleeLookUpValue;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Float getTotalIndvResponsibilityAmt() {
		return totalIndvResponsibilityAmt;
	}
	public void setTotalIndvResponsibilityAmt(Float totalIndvResponsibilityAmt) {
		this.totalIndvResponsibilityAmt = totalIndvResponsibilityAmt;
	}
	public Date getQuotingDate() {
		return quotingDate;
	}
	public void setQuotingDate(Date quotingDate) {
		this.quotingDate = quotingDate;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getRelationshipToHcpLkpCode() {
		return relationshipToHcpLkpCode;
	}
	public void setRelationshipToHcpLkpCode(String relationshipToHcpLkpCode) {
		this.relationshipToHcpLkpCode = relationshipToHcpLkpCode;
	}
	public String getHomeAddress1() {
		return homeAddress1;
	}
	public String getHomeAddress2() {
		return homeAddress2;
	}
	public String getHomeCity() {
		return homeCity;
	}
	public String getHomeState() {
		return homeState;
	}
	public String getHomeCounty() {
		return homeCounty;
	}
	public String getHomeCountyCode() {
		return homeCountyCode;
	}
	public String getHomeZip() {
		return homeZip;
	}
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}
	public void setHomeState(String homeState) {
		this.homeState = homeState;
	}
	public void setHomeCounty(String homeCounty) {
		this.homeCounty = homeCounty;
	}
	public void setHomeCountyCode(String homeCountycode) {
		this.homeCountyCode = homeCountycode;
	}
	public void setHomeZip(String homeZip) {
		this.homeZip = homeZip;
	}
}
