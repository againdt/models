package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class EmployeePlanRateBenefits implements Comparable<EmployeePlanRateBenefits>, Serializable{
	private static final long serialVersionUID = 1L;

	//public enum EHBCovered 	{ PARTIAL, COMPETE; }
	public enum PrimaryZipCodeBooleanFlag { YES, NO; }
	
	private int id;
	private String name;
	private String level;
	private Float totalPremium;
	private Float indvPremium;
	private Float dependentPremium;
	private String issuer;
	private String issuerText;
	private String networkType;
	private Integer employeeId;
	private String ehbCovered;
	private PrimaryZipCodeBooleanFlag availablePrimaryloc;
	private Map<String, HashMap<String, String>> planBenefits;

	public EmployeePlanRateBenefits(){

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Float getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Float totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Float getIndvPremium() {
		return indvPremium;
	}

	public void setIndvPremium(Float indvPremium) {
		this.indvPremium = indvPremium;
	}

	public Float getDependentPremium() {
		return dependentPremium;
	}

	public void setDependentPremium(Float dependentPremium) {
		this.dependentPremium = dependentPremium;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerText() {
		return issuerText;
	}

	public void setIssuerText(String issuerText) {
		this.issuerText = issuerText;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public Map<String, HashMap<String, String>> getPlanBenefits() {
		return planBenefits;
	}

	public void setPlanBenefits(Map<String, HashMap<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}



	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getEhbCovered() {
		return ehbCovered;
	}

	public void setEhbCovered(String ehbCovered) {
		this.ehbCovered = ehbCovered;
	}

	public PrimaryZipCodeBooleanFlag getAvailablePrimaryloc() {
		return availablePrimaryloc;
	}

	public void setAvailablePrimaryloc(PrimaryZipCodeBooleanFlag availablePrimaryloc) {
		this.availablePrimaryloc = availablePrimaryloc;
	}
	
	@Override
	public String toString() {
		return "EmployeePlanRateBenefits [id=" + id + ", name=" + name
				+ ", level=" + level + ", totalPremium=" + totalPremium
				+ ", indvPremium=" + indvPremium + ", dependentPremium="
				+ dependentPremium + ", issuer=" + issuer + ", issuerText="
				+ issuerText + ", networkType=" + networkType + ", employeeId="
				+ employeeId + ", ehbCovered=" + ehbCovered
				+ ", availablePrimaryloc=" + availablePrimaryloc
				+ ", planBenefits=" + planBenefits + "]";
	}

	@Override
	public int compareTo(EmployeePlanRateBenefits o) {
		return totalPremium.compareTo(o.totalPremium);
	}

}
