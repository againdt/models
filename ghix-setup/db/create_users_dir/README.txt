1. Assuming there is directory:/home/oracle/bk_setup/ on your DB server, 
    copy create_users_dir and its files to it.
   So there is /home/oracle/bk_setup/create_users_dir

2. Update DB_ENV.sh

Assuming the Oracle client installed on this machine,
 make sure ORACLE_BASE, ORACLE_HOME, PATH, LD_LIBRARY_PATH, and EZCONNECT
 are setup appropriately to your environment.
 
Example:

export ORACLE_BASE=/u01/app/oracle
export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/db_1
export PATH=/usr/sbin:$PATH
export PATH=$ORACLE_HOME/bin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib

3. Make sure an Oracle account named DEV_JT with DBA privileges exists.
   It can be dropped after the procedure is done.
   
4. Update cr_TEMPLATE_USERS.sh with correct DBA account and its password

5. Make sure there is a default tablespace and prefix of the schema/user/roles
   Example: 
     a.	Schema Prefix: GHIXPROD_0
     b.	Tablespace: GHIXPROD_0_DATA

6. cd /home/oracle/bk_setup/create_users_dir
./cr_USERS_by_prefix.sh GHIXPROD_0 GHIXPROD_0_DATA


The following will be created:
Schema/users:
GHIXPROD_0_OWNER, its password is: GHIXPROD_0_OWNER
GHIXPROD_0_USER, its password is: GHIXPROD_0_USER
GHIXPROD_0_READONLY, its password is: GHIXPROD_0_READONLY

Roles:
	GHIXPROD_0_USER_ROLE
	GHIXPROD_0_USER_READONLY
	
