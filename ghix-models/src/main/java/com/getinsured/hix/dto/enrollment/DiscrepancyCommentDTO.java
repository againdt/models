package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class DiscrepancyCommentDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer commentId;
	private String commentText;
	private Date commentAddedOn;
	private Date commentLastUpdatedOn;
	private	String commentedByUser;
	private	Integer discrepancyId;
	private String discrepancyCode;
	private String discrepancyLabel;
	public Integer getCommentId() {
		return commentId;
	}
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Date getCommentAddedOn() {
		return commentAddedOn;
	}
	public void setCommentAddedOn(Date commentAddedOn) {
		this.commentAddedOn = commentAddedOn;
	}
	public Date getCommentLastUpdatedOn() {
		return commentLastUpdatedOn;
	}
	public void setCommentLastUpdatedOn(Date commentLastUpdatedOn) {
		this.commentLastUpdatedOn = commentLastUpdatedOn;
	}
	public String getCommentedByUser() {
		return commentedByUser;
	}
	public void setCommentedByUser(String commentedByUser) {
		this.commentedByUser = commentedByUser;
	}
	public Integer getDiscrepancyId() {
		return discrepancyId;
	}
	public void setDiscrepancyId(Integer discrepancyId) {
		this.discrepancyId = discrepancyId;
	}
	public String getDiscrepancyCode() {
		return discrepancyCode;
	}
	public void setDiscrepancyCode(String discrepancyCode) {
		this.discrepancyCode = discrepancyCode;
	}
	public String getDiscrepancyLabel() {
		return discrepancyLabel;
	}
	public void setDiscrepancyLabel(String discrepancyLabel) {
		this.discrepancyLabel = discrepancyLabel;
	}
}
