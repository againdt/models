package com.getinsured.coveredCA.dto.proxy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class Questionnaire implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7894005422108365120L;
	private int id;
	private String question;
	private String selectedAnswer;
	private List<Answer> answers;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public List<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
	public void addAnswers(Answer answer) {
		if(this.answers == null){
			this.answers = new ArrayList<Answer>();
		}
		this.answers.add(answer);
	}
	public String getSelectedAnswer() {
		return selectedAnswer;
	}
	public void setSelectedAnswer(String selectedAnswer) {
		this.selectedAnswer = selectedAnswer;
	}
	
	
	
}
