package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name="ENRL_EXT_ENROLLMENT_SUMMARY")
public class ExternalEnrollmentSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_EXT_SUMMARY_SEQ")
	@SequenceGenerator(name = "ENRL_EXT_SUMMARY_SEQ", sequenceName = "ENRL_EXT_SUMMARY_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name = "DATE_RECEIVED")
	private Date dateReceived;	
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name ="BATCH_ID")
	private Long batchId;
	
	public int getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	public Date getDateReceived() {
		return dateReceived;
	}

	public String getStatus() {
		return status;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setDateReceived(new TSDate());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((batchId == null) ? 0 : batchId.hashCode());
		result = prime * result + ((dateReceived == null) ? 0 : dateReceived.hashCode());
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + id;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExternalEnrollmentSummary other = (ExternalEnrollmentSummary) obj;
		if (batchId == null) {
			if (other.batchId != null)
				return false;
		} else if (!batchId.equals(other.batchId))
			return false;
		if (dateReceived == null) {
			if (other.dateReceived != null)
				return false;
		} else if (!dateReceived.equals(other.dateReceived))
			return false;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (id != other.id)
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExternalEnrollmentSummary [id=" + id + ", fileName=" + fileName + ", dateReceived=" + dateReceived
				+ ", status=" + status + ", batchId=" + batchId + "]";
	}
}
