package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class Enrollment1095Response implements Serializable{
	

	private Integer totalRecords;
	private List<Enrollment1095DTO> data;
	
	
	public Integer getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
	public List<Enrollment1095DTO> getData() {
		return data;
	}
	public void setData(List<Enrollment1095DTO> data) {
		this.data = data;
	}
	
	
}
