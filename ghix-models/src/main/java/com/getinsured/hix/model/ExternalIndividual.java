package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the external Individuals database table.
 * 
 */
@Entity
@Table(name = "external_individual")
public class ExternalIndividual implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "External_Individual_Seq")
	@SequenceGenerator(name = "External_Individual_Seq", sequenceName = "External_Individual_Seq", allocationSize = 1)
	@Column(name = "id")
	private long serialId;

	@Column(name = "individual_case_id")
	private long id;

	@Column(name = "firstname")
	private String firstName;

	@Column(name = "middlename")
	private String middleName;

	@Column(name = "lastname")
	private String lastName;

	@Column(name = "case_start_date")
	private Date caseStartDate;

	@Column(name = "case_end_date")
	private Date caseEndDate;

	@Column(name = "pref_comm_method")
	private String prefCommMethod;

	@Column(name = "pref_spoken_lang")
	private String prefSokenLang;

	@Column(name = "pref_written_lang")
	private String prefWrittenLang;

	@Column(name = "gender")
	private String gender;

	@Column(name = "ssn")
	private long ssn;

	@Column(name = "citizen")
	private String citizen;

	@Column(name = "dob")
	private Date dob;

	@Column(name = "pri_phone")
	private long phone;

	@Column(name = "emailid")
	private String email;

	@Column(name = "addressline1")
	private String address1;

	@Column(name = "addressline2")
	private String address2;

	@Column(name = "zip")
	private String zip;

	@Column(name = "contactAddressCounty")
	private String contactAddressCounty;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@Column(name = "suffix")
	private String suffix;

	@Column(name = "headOfHouseholdName")
	private String headOfHouseholdName;

	@Column(name = "numberOfHouseholdMembers")
	private int numberOfHouseholdMembers;
	
	@Column(name="ELIGIBILITY_STATUS")
	private String eligibilityStatus;
	
	@Column(name="ENROLLMENT_STATUS")
	private String enrollmentStatus;
	
	@Column(name="HOUSEHOLD_INCOME")
	private Double householdIncome;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;

	

	public long getSerialId() {
		return serialId;
	}

	public void setSerialId(long sequenceId) {
		this.serialId = sequenceId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getCaseStartDate() {
		return caseStartDate;
	}

	public void setCaseStartDate(Date caseStartDate) {
		this.caseStartDate = caseStartDate;
	}

	public Date getCaseEndDate() {
		return caseEndDate;
	}

	public void setCaseEndDate(Date caseEndDate) {
		this.caseEndDate = caseEndDate;
	}

	public String getPrefCommMethod() {
		return prefCommMethod;
	}

	public void setPrefCommMethod(String pref_comm_method) {
		this.prefCommMethod = pref_comm_method;
	}

	public String getPrefSpokenLang() {
		return prefSokenLang;
	}

	public void setPrefSpokenLang(String pref_spoken_lang) {
		this.prefSokenLang = pref_spoken_lang;
	}

	public String getPrefWrittenLang() {
		return prefWrittenLang;
	}

	public void setPrefWrittenLang(String pref_written_lang) {
		this.prefWrittenLang = pref_written_lang;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getSsn() {
		return ssn;
	}

	public void setSsn(long ssn) {
		this.ssn = ssn;
	}

	public String getCitizen() {
		return citizen;
	}

	public void setCitizen(String citizen) {
		this.citizen = citizen;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date date) {
		this.dob = date;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long pri_phone) {
		this.phone = pri_phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmailid(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getHeadOfHouseholdName() {
		return headOfHouseholdName;
	}

	public void setHeadOfHouseholdName(String headOfHouseholdName) {
		this.headOfHouseholdName = headOfHouseholdName;
	}

	public int getNumberOfHouseholdMembers() {
		return numberOfHouseholdMembers;
	}

	public void setNumberOfHouseholdMembers(int numberOfHouseholdMembers) {
		this.numberOfHouseholdMembers = numberOfHouseholdMembers;
	}

	public String getPrefSokenLang() {
		return prefSokenLang;
	}

	public void setPrefSokenLang(String prefSokenLang) {
		this.prefSokenLang = prefSokenLang;
	}

	public String getContactAddressCounty() {
		return contactAddressCounty;
	}

	public void setContactAddressCounty(String contactAddressCounty) {
		this.contactAddressCounty = contactAddressCounty;
	}

	

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public Double getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(Double householdIncome) {
		this.householdIncome = householdIncome;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
	}
	
}
