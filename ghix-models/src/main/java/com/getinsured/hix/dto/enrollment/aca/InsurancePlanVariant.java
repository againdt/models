package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;


public class InsurancePlanVariant {

@JsonProperty("InsurancePlanVariantIdentification")
private InsurancePlanVariantIdentification InsurancePlanVariantIdentification;

@JsonProperty("InsurancePlanVariantIdentification")
public InsurancePlanVariantIdentification getInsurancePlanVariantIdentification() {
return InsurancePlanVariantIdentification;
}

@JsonProperty("InsurancePlanVariantIdentification")
public void setInsurancePlanVariantIdentification(InsurancePlanVariantIdentification insurancePlanVariantIdentification) {
this.InsurancePlanVariantIdentification = insurancePlanVariantIdentification;
}

}