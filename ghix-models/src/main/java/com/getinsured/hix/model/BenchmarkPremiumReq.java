package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name="benchmark_premium_req")
public class BenchmarkPremiumReq implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "benchmark_premium_Seq")
	@SequenceGenerator(name = "benchmark_premium_Seq", sequenceName = "benchmark_premium_seq", allocationSize = 1)
	private int id;
	
	@Column(name="household_case_id", length=20)
	private String householdCaseId;

	@Temporal(value = TemporalType.DATE)
	@Column(name="coverage_start_date")
	private Date coverageStartDate;

	@Column(name="census_data", length=2000)
	private String censusData;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="created",nullable=false)
	private Date created;
	
	@Column(name="response_code", length=10)
	private String responseCode;
	
	@Column(name="response_descr", length=1024)
	private String responseDescr;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getCensusData() {
		return censusData;
	}

	public void setCensusData(String censusData) {
		this.censusData = censusData;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescr() {
		return responseDescr;
	}

	public void setResponseDescr(String responseDescr) {
		this.responseDescr = responseDescr;
	}
}
