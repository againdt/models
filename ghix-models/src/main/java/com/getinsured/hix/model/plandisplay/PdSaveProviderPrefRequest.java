package com.getinsured.hix.model.plandisplay;

import java.util.List;

public class PdSaveProviderPrefRequest {
	private String householdId;
	private List<ProviderBean> providers;
	
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public List<ProviderBean> getProviders() {
		return providers;
	}
	public void setProviders(List<ProviderBean> providers) {
		this.providers = providers;
	}	
}
