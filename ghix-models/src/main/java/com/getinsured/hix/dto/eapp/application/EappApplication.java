package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.directenrollment.PaymentVerificationDTO;
import com.getinsured.hix.dto.tenant.EappTenantDTO;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.google.gson.Gson;

public class EappApplication implements Serializable{
	private static final long serialVersionUID = 1L;
	public static SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	private Household household;
	private EappTenantDTO broker;
	private Plan plan;
	private Issuer issuer;
	private UserInteraction interaction;
	private ReferenceDetails reference;
	private UIHelper uiHelper;
	private Payment payment;
	private SpecialEnrollment sep;
	
	/**
	 * MM/dd/yyyy
	 */
	private String creationDate;
	private String effectiveStartDate;
	private String year;
	private Long submittedTimestamp;
	
	private Map<String, Object> other;
	
	public Household getHousehold() {
		return household;
	}
	public void setHousehold(Household household) {
		this.household = household;
	}
	public EappTenantDTO getBroker() {
		return broker;
	}
	public void setBroker(EappTenantDTO broker) {
		this.broker = broker;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	
	public Map<String, Object> getOther() {
		return other;
	}
	public void setOther(Map<String, Object> other) {
		this.other = other;
	}
	public UserInteraction getInteraction() {
		return interaction;
	}
	public void setInteraction(UserInteraction interaction) {
		this.interaction = interaction;
	}
	public ReferenceDetails getReference() {
		return reference;
	}
	public void setReference(ReferenceDetails reference) {
		this.reference = reference;
	}
	
	public UIHelper getUiHelper() {
		return uiHelper;
	}
	public void setUiHelper(UIHelper uiHelper) {
		this.uiHelper = uiHelper;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	
	public SpecialEnrollment getSep() {
		return sep;
	}
	public void setSep(SpecialEnrollment sep) {
		this.sep = sep;
	}
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Long getSubmittedTimestamp() {
		return submittedTimestamp;
	}
	public void setSubmittedTimestamp(Long submittedTimestamp) {
		this.submittedTimestamp = submittedTimestamp;
	}
	/**
	 * validate dot separated expression with an object
	 * @param expr
	 * @return
	 */
	public static boolean isValidExpr(String expr){
		/**
		 * FIXME: implement this
		 */
		return true;
	}
	
	
	public static EappApplication getSampleInstance(){
		
		EappApplication app = new EappApplication();
		app.setIssuer(Issuer.getSampleInstance());
		app.setHousehold(Household.getSampleInstance());
		app.setBroker(EappTenantDTO.sampleInstance());
		app.setPlan(Plan.getSampleInstance());
		app.setCreationDate(format.format(new TSDate()));
		app.setEffectiveStartDate(format.format(new TSDate(System.currentTimeMillis()+9*24*3600*1000)));
		app.setInteraction(UserInteraction.getSampleInstance());
		app.setUiHelper(UIHelper.getSampleInstance());
		app.setPayment(Payment.getSampleInstance());
		app.setSep(SpecialEnrollment.getSampleInstance());
		app.setReference(ReferenceDetails.getSampleInstance());
		app.setYear(format.format(new TSDate()).substring(6, 10));
		app.setSubmittedTimestamp((new TSDate()).getTime());
		
		return app;
	}
	
	public static EappApplication getInstance(SsapApplicationDTO ssapDto){
		EappApplication application = new EappApplication();
		Plan plan = new Plan();
		plan.setId(String.valueOf(ssapDto.getPlanId()));
        if(ssapDto.getPremiumAfterCredit()!=null){
            plan.setPlanCost(ssapDto.getPremiumAfterCredit().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

		application.setPlan(plan);
		/**
		 * call plan api to get plan details like name and all
		 */
		application.setCreationDate(format.format(new TSDate()));
		application.setEffectiveStartDate(format.format(ssapDto.getEffectiveStartDate()));
		
		/**
		 * call plan management api to get issuer details
		 */
		Issuer issuer = new Issuer();
		//issuer.setId(ssapDto.get)
		ssapDto.getCoverageYear();
		Household household = new Household();
		application.setHousehold(Household.populateMemberData(household, ssapDto));
		application.setHousehold(household);
		issuer.setState(ssapDto.getState());
		household = application.getHousehold();
		application.setUiHelper(new UIHelper());
		int applicantCount = 1;


		if(application.getHousehold().getSpouse()!=null){
			application.getUiHelper().setHasSpouse(true);
			applicantCount++;
		}
		
		if(application.getHousehold().getDependents()!=null && application.getHousehold().getDependents().size()>0){
			application.getUiHelper().setHasDependent(true);
			applicantCount+=application.getHousehold().getDependents().size();
		}
		application.getUiHelper().setAnyTobaccoUsage(anySmokersInTheHouse(household));
		//setting applicant count for UI helper
		application.getUiHelper().setApplicantCount(applicantCount);
		UserInteraction interaction = new UserInteraction();
		application.setInteraction(interaction);
		application.getUiHelper().setChildOnlyApp(application.getHousehold().getIsChildOnlyApplication());
		
		/**
		 * setting open enrollment flag
		 */
		String oepFlag = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OEP_FLAG);
		if(StringUtils.isNotEmpty(oepFlag) && oepFlag.equalsIgnoreCase("ON")){
			application.getUiHelper().setOep(true);
		}
		
		/**
		 * eligiblity capture details
		 */
		EligibilityInput eligiblityInput = new EligibilityInput();
		eligiblityInput.setCountyCode(ssapDto.getCountyCode());
		eligiblityInput.setState(ssapDto.getState());
		eligiblityInput.setZipCode(ssapDto.getZipCode());
		household.setCensusData(eligiblityInput);
		
		/**
		 * generate a reference object here
		 */
		ReferenceDetails details = ReferenceDetails.getInstance(issuer.getBrand(), issuer.getState(), null, "doesnotexist");
		application.setReference(details);
		if(StringUtils.isNotBlank(application.getEffectiveStartDate())){
			application.setYear(application.getEffectiveStartDate().substring(6, 10));
		}
		
		return application;
	}

    public static String anySmokersInTheHouse(Household household){

        if(household.getPrimary()!=null && household.getPrimary().isTobaccoStatusBoolean()){
            return "Y";
        }

        if(household.getSpouse()!=null && household.getSpouse().isTobaccoStatusBoolean()){
            return "Y";
        }

        for(Person person : household.getDependents()){
           if(person.isTobaccoStatusBoolean()){
               return "Y";
           }
        }

        return "N";
    }
	
	
	public static void main(String args[]){
		EappApplication app = new EappApplication();
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		System.out.println(gson.toJson(EappApplication.getSampleInstance()));
		String key = "{\"household\":{\"primary\":{\"name\":{\"first\":\"kptest\",\"last\":\"test\"},\"applicantId\":1,\"dob\":\"10101980\",\"gender\":\"M\",\"tobaccoStatus\":\"false\",\"relationshipToPrimary\":\"SELF\",\"homeAddress\":{\"address1\":\"111 Main st\",\"city\":\"city\",\"state\":\"CA\",\"county\":\"Santa Clara\",\"zip\":\"95129\"},\"primaryPhone\":{\"number\":\"4545345345\"},\"emailAddress\":\"eapp148@mailinator.com\",\"ssn\":\"345345345\",\"age\":35,\"esignName\":{\"first\":\"kptest\",\"last\":\"test\"}},\"dependents\":[],\"isChildOnlyApplication\":false,\"censusData\":{\"zipCode\":\"95129\",\"countyCode\":\"06085\",\"countyName\":\"Santa Clara\",\"state\":\"CA\"}},\"broker\":{\"brokerName\":\"Vimo Inc DBA GetInsured\",\"brokerTIN\":\"870745172\",\"writingAgent\":{\"firstName\":\"Michael\",\"lastName\":\"Daugherty\",\"npn\":\"67191944\",\"emailAddress\":\"customersupport@getinsured.com\",\"phoneNumber\":\"8009723755\",\"faxNumber\":\"7702346335\",\"address\":{\"address1\":\"2110 Newmarket Parkway\",\"address2\":\"Suite 200\",\"city\":\"Marietta\",\"state\":\"GA\",\"zipCode\":\"30067\",\"county\":\"Cobb\"}},\"stateLicenses\":{\"LA\":\"513521\",\"UT\":\"347308\",\"FL\":\"P241259\",\"NC\":\"0001131360\",\"VA\":\"773806\",\"MD\":\"100041115\"},\"carriers\":{\"anthem\":{\"US\":{\"writingAgentId\":\"DHJQJNRKSZ\",\"brokerId\":\"DHNNMJJSTY\"}},\"uhc\":{\"US\":{\"writingAgentId\":\"AA3016325\"}},\"humana\":{\"US\":{\"writingAgentId\":\"1591417\",\"brokerId\":\"1403377\"}},\"hcsc\":{\"OK\":{\"writingAgentId\":\"200000080\"},\"IL\":{\"writingAgentId\":\"069849000\"},\"NM\":{\"writingAgentId\":\"900001854\"},\"TX\":{\"writingAgentId\":\"077594000\"},\"MT\":{\"writingAgentId\":\"063226000\"}},\"Kaiser\":{\"CA\":{\"writingAgentId\":\"305032\"},\"GA\":{\"writingAgentId\":\"BREBW\"},\"OR\":{\"writingAgentId\":\"12558\"},\"WA\":{\"writingAgentId\":\"12558\"},\"MD\":{\"writingAgentId\":\"2826\"},\"VA\":{\"writingAgentId\":\"2826\"},\"HI\":{\"writingAgentId\":\"181238\"},\"CO\":{\"writingAgentId\":\"498067\"}},\"Health Net\":{\"AZ\":{\"writingAgentId\":\"AP746\"},\"OR\":{\"writingAgentId\":\"AU135\"},\"CA\":{\"writingAgentId\":\"AT293\"}},\"BSBS Tennessee\":{\"TN\":{\"writingAgentId\":\"12502\"}},\"Blue Shield of CA\":{\"CA\":{\"writingAgentId\":\"467042592-870745172E\"}},\"BCBS MN\":{\"MN\":{\"writingAgentId\":\"VGI-678A\",\"brokerId\":\"VGI\"}},\"BCBS KC\":{\"US\":{\"writingAgentId\":\"05170TZ4\"}}}},\"plan\":{\"insuranceType\":\"HLT\",\"id\":\"598331\",\"planName\":\"Gold 80 HMO Coinsurance\",\"planCost\":416.92,\"issuerPlanId\":\"40513CA039002100\",\"networkType\":\"HMO\",\"hsaEligible\":false,\"exchangeType\":\"OFF\",\"hiosPlanId\":\"40513CA039002100\"},\"issuer\":{\"brand\":\"Kaiser Permanente\",\"state\":\"CA\",\"name\":\"Kaiser Foundation Health Plan of California\",\"logoUrl\":\"https://63dd94911e669e40eb19-69b69c851edd7177d0cefd97a293ce93.ssl.cf1.rackcdn.com/resources/issuers/logo//2015-11-1613:46:156/11371_40513Kaiser.png\",\"id\":\"11371\",\"hiosIssuerId\":\"40513\"},\"interaction\":{\"lastPageVisited\":\"Component-79\",\"paymentCallCount\":1,\"householdInSession\":true,\"userInSession\":false,\"isNewHousehold\":true},\"reference\":{\"d2cEnrollmentId\":5563,\"giAppId\":\"602005401600\"},\"uiHelper\":{\"hasSpouse\":false,\"hasDependent\":false,\"childOnlyApp\":false,\"oep\":false,\"applicantCount\":1},\"payment\":{\"token\":\"2110008\"},\"sep\":{\"qualifyingLifeEvent\":\"LHCC\",\"eventDate\":\"03032016\"},\"creationDate\":\"03/17/2016\",\"effectiveStartDate\":\"05/01/2016\",\"year\":\"2016\",\"submittedTimestamp\":1458253135827,\"other\":{\"sep\":{\"acknowledgedocreq\":true},\"payer\":{\"name\":{\"first\":\"kptest\",\"last\":\"test\"}},\"billingsameashomeaddress\":\"Y\",\"household\":{\"primary\":{\"esign\":{\"terms\":true}}}}}";
		EappApplication app2 = gson.fromJson(key, EappApplication.class);
		System.out.println("all good");
	}
	@Override
	public String toString() {
		return "EappApplication [household=" + household + ", broker=" + broker
				+ ", plan=" + plan + ", issuer=" + issuer + ", interaction="
				+ interaction + ", reference=" + reference + ", uiHelper="
				+ uiHelper + ", creationDate=" + creationDate
				+ ", effectiveStartDate=" + effectiveStartDate + ", other="
				+ other + "]";
	}
	
	/**
	 * clean up any data entered by UI for formatting
	 */
	public void sanitize(){
		if(this.household!=null){
			this.household.sanitize();
		}
	}
	
	public PaymentVerificationDTO decorate(PaymentVerificationDTO dto){
		if(this.household!=null && this.household.getPrimary()!=null && this.household.getPrimary().getName()!=null){
			dto.setFname(this.household.getPrimary().getName().getFirst());
			dto.setLname(this.household.getPrimary().getName().getLast());
		}
		
		if(this.household!=null && this.household.getPrimary()!=null && this.household.getPrimary().getHomeAddress()!=null){
			dto.setZip(this.household.getPrimary().getHomeAddress().getZip());
			if(dto.getCreditcard()!=null){
				if(dto.getCreditcard().getZip()==null){
					dto.getCreditcard().setZip(this.household.getPrimary().getHomeAddress().getZip());
				}
			}
		}
		
		if(this.plan!=null && this.plan.getPlanCost()!=null){
			dto.setAmount(this.plan.getPlanCost().doubleValue());
		}
		
		if(this.reference!=null && !StringUtils.isEmpty(this.reference.getGiAppId())){
			dto.setGuid(this.reference.getGiAppId());
		}else{
			/**
			 * this will not happen in real life, setting up random value
			 */
			dto.setGuid("" + 1234567*Math.random());
		}
		
		return dto;
	}
	
	
}
