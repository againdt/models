package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * Encapsulates the response received from AHBX webservices.
 * 
 * 
 */
public class BrokerResponse extends GHIXResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private int responseCode;

    private long brokerId;

    private String responseDescription;
    private String brokerDelegationCode;

    //Added for Enrollment Rest URL support
    private String brokerName;
    private String stateLicenseNumber;
    private String stateEINNumber;
    
    private Integer locationId;

    public String getBrokerName() {
	return brokerName;
    }

    public void setBrokerName(String brokerName) {
	this.brokerName = brokerName;
    }

    public String getStateLicenseNumber() {
	return stateLicenseNumber;
    }

    public void setStateLicenseNumber(String stateLicenseNumber) {
	this.stateLicenseNumber = stateLicenseNumber;
    }

    public String getStateEINNumber() {
	return stateEINNumber;
    }

    public void setStateEINNumber(String stateEINNumber) {
	this.stateEINNumber = stateEINNumber;
    }

    public void setResponseCode(int responseCode) {
	this.responseCode = responseCode;
    }

    public void setResponseDescription(String responseDescription) {
	this.responseDescription = responseDescription;
    }

    public int getResponseCode() {
	return responseCode;
    }

    public String getResponseDescription() {
	return responseDescription;
    }

    public long getBrokerId() {
	return brokerId;
    }

    public void setBrokerId(long brokerId) {
	this.brokerId = brokerId;
    }

    public String getBrokerDelegationCode() {
	return brokerDelegationCode;
    }

    public void setBrokerDelegationCode(String brokerDelegationCode) {
	this.brokerDelegationCode = brokerDelegationCode;
    }

    public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	@Override
    public String toString() {
    	return "BrokerResponse details: BrokerId = "+brokerId+", BrokerName = "+brokerName+", StateLicenseNumber = "+stateLicenseNumber+", StateEINNumber ="+stateEINNumber+
    			", ResponseCode = "+responseCode+", BrokerDelegationCode = "+brokerDelegationCode+", ResponseDescription = "+responseDescription;
    }
}
