--All events for these categories are not allowed plan selection
update
	sep_enrollment_events
set
	allow_plan_selection = 0
where
	event_category in ('DEMOGRAPHICS',
	'GAIN_MEC',
	'REMOVE',
	'DEPENDENT_CHILD_AGES_OUT');

--INCARCERATION_CHANGE category updates
update
	sep_enrollment_events
set
	allow_plan_selection = 0
where
	event_category = 'INCARCERATION_CHANGE'
	and event_name in ('INCARCERATED_DEPENDENTS',
	'INCARCERATED_DEPENDENTS_OTHER',
	'INCARCERATED_PRIMARY',
	'INCARCERATED_PRIMARY_OTHER',
	'INCARCERATION',
	'INCARCERATION_OTHER');

--ADDRESS_CHANGE category updates
update
	sep_enrollment_events
set
	allow_plan_selection = 0
where
	event_category = 'ADDRESS_CHANGE'
	and event_name in ('MOVED_OUT_OF_STATE_DEPENDENTS',
	'MOVED_OUT_OF_STATE');

--IMMIGRATION_STATUS_CHANGE category updates
update
	sep_enrollment_events
set
	allow_plan_selection = 0
where
	event_category = 'IMMIGRATION_STATUS_CHANGE'
	and event_name in ('LOSE_CITIZENSHIP');

--UPDATE_DEPENDENTS category updates
update
	sep_enrollment_events
set
	allow_plan_selection = 0
where
	event_category = 'UPDATE_DEPENDENTS'
	and event_name in ('REMOVE_DEPENDENT_OTHER' ,
	'REMOVE_DEPENDENT' ,
	'DEPENDENT_CHILD_AGES_OUT_OTHER' ,
	'DEPENDENT_CHILD_AGES_OUT' ,
	'DEATH_OTHER' ,
	'DEATH_DEPENDENTS_OTHER' ,
	'DEATH_DEPENDENTS' ,
	'DEATH');

--OTHER category updates
update
	sep_enrollment_events
set
	allow_plan_selection = 0
where
	event_category = 'OTHER'
	and event_name in ('LOSS_OF_ELIGIBILITY_AUTO',
	'INCOME_CHANGE_APTC_INCREASE_AUTO',
	'INCOME_CHANGE_APTC_DECREASE_AUTO',
	'AUTO_APTC_UPDATE',
	'AUTO_APTC_GAIN',
	'AUTO_APTC_CSR_GAIN');