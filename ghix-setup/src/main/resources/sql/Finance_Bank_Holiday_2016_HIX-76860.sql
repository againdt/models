insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'New Years Day', to_date('01-01-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Birthday of Martin Luther King, Jr.', to_date('01-18-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Washingtons Birthday', to_date('02-15-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Memorial Day', to_date('05-30-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Independence Day', to_date('07-04-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Labor Day', to_date('09-05-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Columbus Day', to_date('10-10-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Veterans Day', to_date('11-11-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Thanksgiving Day', to_date('11-24-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Christmas Day', to_date('12-26-2016','mm-dd-yyyy'), current_timestamp, current_timestamp, '2016');

commit;