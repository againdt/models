package com.getinsured.hix.model.directenrollment;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity

@Table(name="D2C_ECM_DOCUMENTS")
public class DirectEnrollmentECM {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "D2C_ECM_DOCUMENTS_SEQ")
	@SequenceGenerator(name = "D2C_ECM_DOCUMENTS_SEQ", sequenceName = "D2C_ECM_DOCUMENTS_SEQ", allocationSize = 1)
	private Long id;
	@Column(name="ECM_ID")
	private String ecmId;
	
	@ManyToOne
	@JoinColumn(name="D2C_ENROLLMENT_ID")
	private DirectEnrollment enrollment;
	
	@Column(name="FILE_NAME")
	private String fileName;
	@Column(name="DESCRIPTION")
	private String description;
	@Column(name="TYPE")
	private String type;
	
	@Column(name="ACTIVE")
	private Boolean active;
	
	@Column(name="LAST_ACCESSED_TIMESTAMP")
	private Date lastUpdatedTimestamp;
	@Column(name="CREATED_TIMESTAMP")
	private Date creationTimestamp;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEcmId() {
		return ecmId;
	}
	public void setEcmId(String ecmId) {
		this.ecmId = ecmId;
	}
	
	public DirectEnrollment getEnrollment() {
		return enrollment;
	}
	public void setEnrollment(DirectEnrollment enrollment) {
		this.enrollment = enrollment;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}
	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	
	@PrePersist
	public void prePersist(){
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdatedTimestamp(new TSDate());
	}
	
	@PreUpdate
	public void preUpdate(){
		this.setLastUpdatedTimestamp(new TSDate());
	}
	

}
