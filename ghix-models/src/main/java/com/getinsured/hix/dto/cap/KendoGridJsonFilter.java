package com.getinsured.hix.dto.cap;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

import com.google.gson.Gson;

/**
 * Created by kaul_s on 2/26/15.
 */

/**
 *
 * Here is what Kendo Grid will be sending to server(java) and what it will expect from server(java).
 *
 *  {
 *      "take":5,       // records to be selected (usually used in SQL statements for pagination)
 *      "skip":0,       // records to be skipped (usually used in SQL statements for pagination)
 *      "page":1,       // current page number
 *      "pageSize":5,   // page size
 *      "group":[],
 *      "filter":       // filter object
 *      {
 *          "filters":  // array of filter logics
 *          [
 *              {
 *                  "field":"Itemname", // filter to be applied on this file
 *                  "operator":"eq",    // filter operator (e.g. eq, neq, gte, gt, lte, lt)
 *                  "value":"3"         // filter value
 *              }
 *          ],
 *          "logic":"and"   // logic between n number of filter, in this I am using only one condition,
 *                          // there could me multiple conditions in “filters” array
 *      },
 *      "sort": // sorting object
 *      [
 *          {
 *              "field":"Name", // sort this filed
 *              "dir":"desc"    // sort direction (i.e. desc, asc)
 *          }
 *      ]
 *  }
 *  
 */
public class KendoGridJsonFilter {

    // "take": 5,
    // records to be selected (usually used in SQL statements for pagination)
    @JsonProperty("take")
    private Integer take;

    // "skip": 0,
    // records to be skipped (usually used in SQL statements for pagination)
    @JsonProperty("skip")
    private Integer skip;

    // "page": 1,
    // current page number
    @JsonProperty("page")
    private Integer page;

    // "pageSize": 5,
    // page size
    @JsonProperty("pageSize")
    private Integer pageSize;

    // "group": [],
    @JsonProperty("group")
    private List<?> group;
    
    private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("platformGson");
	}

    // filterLogic object
    public static class Filter {

        public static class Logic {
            // "field": "Itemname",
            // filterLogic to be applied on this file
            private String field;

            public String getField() {
                return field;
            }

            // "operator": "eq",
            // filterLogic operator (e.g. eq, neq, gte, gt, lte, lt)
            public enum Operator {
            	eq, neq, gte, gt, lte, lt, startswith, endswith, contains, isnull, isnotnull, startswithignorecase, endswithignorecase, containsignorecase
            }

            private Operator operator;

            public Operator getOperator() {
                return operator;
            }

            // "value": "3"
            // filterLogic value
            private String value;

            public String getValue() {
                return value;
            }
        }

        // "filters":
        // array of filterLogic logics
        private List<Logic> filters;

        public List<Logic> getFilters() {
            return filters;
        }

        // "logic": "and"
        // logic between n number of filterLogic, in this I am using only one condition,
        // there could me multiple conditions in “filters” array
        public enum LogicOperator {
            and, or
        }

        private LogicOperator logic;

        public LogicOperator getLogic() {
            return logic;
        }
    }

    // "filterLogic":
    // filterLogic object
    @JsonProperty("filter")
    private Filter filter;

    public List<Filter.Logic> getFiltersByField(String field) {
        List<Filter.Logic> result = new ArrayList<>();
        if (field != null && filter != null && filter.filters != null) {
            for (Filter.Logic logic: filter.filters) {
                if (field.equals(logic.field)) {
                    result.add(logic);
                }
            }
        }
        return result;
    }

    // sorting object
    public static class Sort {
        // "field": "Name",
        // sort this filed
        private String field;

        public String getField() {
            return field;
        }

        // "dir": "desc"
        // sort direction (i.e. desc, asc)
        public enum Direction {
            desc, asc
        }

        private Direction dir;

        public Direction getDir() {
            return dir;
        }

        private Sort(String field, Direction dir) {
            this.field = field;
            this.dir = dir;
        }

        public Sort(String field) {
            this.field = field;
            this.dir = Direction.asc;
        }
    }

    // "sort":
    // sorting object
    @JsonProperty("sort")
    private List<Sort> sort;

    public void setDefaultSort(String field, Sort.Direction direction) {
        if (sort == null) {
            sort = new ArrayList<>();
        }
        if (sort.isEmpty()) {
            sort.add(new Sort(field, direction));
        }
    }

    public void setDefaultSort(List<Sort> sort) {
        if (this.sort == null) {
            this.sort = new ArrayList<>();
        }
        if (this.sort.isEmpty()) {
            this.sort.addAll(sort);
        }
    }

    /////////////////////////////////

    public static KendoGridJsonFilter createFromJson(String filterJson) throws IOException {
        if (StringUtils.hasText(filterJson)) {
        	return gson.fromJson(filterJson, KendoGridJsonFilter.class);
        } else {
        	return gson.fromJson("{}", KendoGridJsonFilter.class);
        }
    }

    public QueryCreator queryCreator(Class<?> type) {
        return new QueryCreator(this, type);
    }

    public static class QueryCreator {

        private String baseQuery;

        private String countQuery;

        private Class<?> type;

        private KendoGridJsonFilter kendoFilter;

        private Map<String, String> aliasFields = new HashMap<>();

        private Map<String, String> subqueryAliasFields = new HashMap<>();

        private Map<String, String> subqueryFilters = new HashMap<>();

        private List<String> toIgnoreFilters = new ArrayList<>();

        private QueryCreator(KendoGridJsonFilter kendoFilter, Class<?> type) {
            this.kendoFilter = kendoFilter;
            this.type = type;
        }

        public void setBaseQuery(String baseQuery) {
            this.baseQuery = baseQuery;
        }

        public void setCountQuery(String countQuery) {
            this.countQuery = countQuery;
        }

        public void addAliasField(String alias, String field) {
            aliasFields.put(alias, field);
        }

        public void setDefaultSort(String field, Sort.Direction direction) {
            kendoFilter.setDefaultSort(field, direction);
        }

        public void setDefaultSort(List<Sort> sort) {
            kendoFilter.setDefaultSort(sort);
        }

        public void addSubqueryFilter(String alias, String subqueryName, String field) {
            subqueryAliasFields.put(alias, field);
            subqueryFilters.put(alias, subqueryName);
        }

        public void addToIgnoreFilter(String toIgnoreFilter) {
            toIgnoreFilters.add(toIgnoreFilter);
        }

        public static class FilterBuilder {
            private String comparison;
            private String field;
            private String parameter;
            private String value;
            private boolean date;
            private static int parameterCounter = 0;

            public static FilterBuilder create(String comparison, String field) {
                return new FilterBuilder(comparison, field, null, false);
            }

            public static FilterBuilder create(String comparison, String field, String value) {
                return new FilterBuilder(comparison, field, value, false);
            }

            public static FilterBuilder createForDate(String comparison, String field, String value) {
                return new FilterBuilder(comparison, field, value, true);
            }

            private FilterBuilder(String comparison, String field, String value, boolean date) {
                this.field = field;
                this.parameter = field.replace('.', '_') + nextParameterCounter();
                this.comparison = String.format(comparison, field, parameter);
                this.value = value;
                this.date = date;
            }

            public static int getParameterCounter() {
                return parameterCounter;
            }

            private static String nextParameterCounter() {
                if (parameterCounter == Integer.MAX_VALUE) {
                    parameterCounter = 0;
                }
                return String.valueOf(parameterCounter++);
            }

            public FilterBuilder setLogic(Filter.LogicOperator logic) {
                if (comparison.trim().indexOf(logic.toString()) != 0) {
                    comparison = String.format(" %s%s", logic, comparison);
                }
                return this;
            }

            public String getComparison() {
                return comparison;
            }

            public String getField() {
                return field;
            }

            public String getParameter() {
                return parameter;
            }

            public String getValue() {
                return value;
            }

            public boolean isDate() {
                return date;
            }

            public boolean isUnary() {
                return value == null;
            }
        }

        private List<FilterBuilder> baseFilters = new ArrayList<>();

        public void setBaseFilter(String fieldName, String operatorString, String value)
            throws InvalidArgumentException {
            baseFilters.clear();
            addBaseFilter(fieldName, operatorString, value);
        }

        public void addBaseFilter(String fieldName, String operatorString, String value)
                throws InvalidArgumentException {
            Filter.Logic.Operator operator = Filter.Logic.Operator.valueOf(operatorString);
            String field = getFieldFromAlias(fieldName);
            baseFilters.add(createFilterBuilder(field, operator, value));
        }

        public Integer getFirstResult() {
            Integer maxResults = getMaxResults();
            return (kendoFilter.skip != null) ? kendoFilter.skip : (kendoFilter.page != null && maxResults != null) ? kendoFilter.page * maxResults : null;
        }

        public Integer getMaxResults() {
            return (kendoFilter.take != null) ? kendoFilter.take : (kendoFilter.pageSize != null) ? kendoFilter.pageSize : null;
        }

        public String buildQueryString() {
            List<FilterBuilder> filterBuilderList = buildFilter();
            return buildEntityQueryString(filterBuilderList);
        }

        public String buildEntityQueryString(List<FilterBuilder> filterBuilderList) {
            StringBuilder queryString = (baseQuery != null)
                ? new StringBuilder(baseQuery)
                : new StringBuilder("select e from " + type.getSimpleName() + " e");

            addFilters(queryString, filterBuilderList);
            String sortString = buildSortString();
            if (!sortString.isEmpty()) {
                queryString.append(" order by").append(sortString);
            }
            return queryString.toString();
        }

        public String buildCountQueryString(List<FilterBuilder> filterBuilderList) {
            StringBuilder queryString;
            if (countQuery != null) {
                queryString = new StringBuilder(countQuery);

            } else if (baseQuery != null) {
                queryString = new StringBuilder(baseQuery.replaceFirst("^select e", "select count(e)"));

            } else {
                queryString = new StringBuilder("select count(e) from " + type.getSimpleName() + " e");
            }

            addFilters(queryString, filterBuilderList);

            return queryString.toString();
        }

        private void addFilters(StringBuilder queryString, List<FilterBuilder> filterBuilderList) {
            addSubqueryFilters(queryString, filterBuilderList);
            addMainQueryFilters(queryString, filterBuilderList);
        }

        private void addSubqueryFilters(StringBuilder queryString, List<FilterBuilder> filterBuilderList) {
            if (baseQuery == null || filterBuilderList.isEmpty()) {
                return;
            }
            String sqTag = "${SubqueryFilter.";
            int sqBegIdx;
            while ((sqBegIdx = queryString.indexOf(sqTag)) >= 0) {
                int sqEndIdx = queryString.indexOf("}", sqBegIdx);
                String sqName = queryString.substring(sqBegIdx + sqTag.length(), sqEndIdx);
                StringBuilder sqString = new StringBuilder();
                for (Map.Entry<String, String> sqFilter : subqueryFilters.entrySet()) {
                    if (!sqFilter.getValue().equalsIgnoreCase(sqName)) {
                        continue;
                    }
                    String alias = sqFilter.getKey();
                    // add if it's in subqueryAliasFields
                    String field = subqueryAliasFields.get(alias);
                    if (field == null) {
                        continue;
                    }
                    for (FilterBuilder filterBuilder : filterBuilderList) {
                        if (filterBuilder.getField().equals(field)) {
                            sqString.append(filterBuilder.comparison);
                        }
                    }
                }
                queryString.delete(sqBegIdx - 1, sqEndIdx + 1);
                queryString.insert(sqBegIdx - 1, sqString);
            }
        }

        public void addMainQueryFilters(StringBuilder queryString, List<FilterBuilder> filterBuilderList) {
            if (filterBuilderList.isEmpty()) {
                return;
            }
            boolean isFirstFilter = true;
            if (baseQuery != null) {
                String baseQueryLowerCase = baseQuery.toLowerCase();
                if (baseQueryLowerCase.lastIndexOf(" where ") > baseQueryLowerCase.lastIndexOf(" from ")
                        || baseQueryLowerCase.lastIndexOf(" and ") > baseQueryLowerCase.lastIndexOf(" where ")
                        || baseQueryLowerCase.lastIndexOf(" or ") > baseQueryLowerCase.lastIndexOf(" where ")) {
                    isFirstFilter = false;
                }
            }
            if (isFirstFilter) {
                queryString.append(" where");
            } else {
                queryString.append(" and");
            }
            for (FilterBuilder filterBuilder : filterBuilderList) {
                // don't add if it's in subqueryAliasFields but not in aliasFields
                String field = filterBuilder.getField();
                if (subqueryAliasFields.containsValue(field) && !aliasFields.containsValue(field)) {
                    continue;
                }
                queryString.append(filterBuilder.comparison);
            }
        }

        public static List<FilterBuilder> addQueryFilters(StringBuilder queryString, List<Filter.Logic> filterLogicList, Map<String, String> aliasFields) {
            if (filterLogicList == null || filterLogicList.isEmpty()) {
                return new ArrayList<>();
            }
            boolean isFirstFilter = true;

            String baseQueryLowerCase = queryString.toString().toLowerCase();
            if (baseQueryLowerCase.lastIndexOf(" where ") > baseQueryLowerCase.lastIndexOf(" from ")
                    || baseQueryLowerCase.lastIndexOf(" and ") > baseQueryLowerCase.lastIndexOf(" where ")
                    || baseQueryLowerCase.lastIndexOf(" or ") > baseQueryLowerCase.lastIndexOf(" where ")) {
                isFirstFilter = false;
            }

            if (isFirstFilter) {
                queryString.append(" where");
            } else {
                queryString.append(" and");
            }
            List<FilterBuilder> filterBuilderList = new ArrayList<>();
            for (Filter.Logic filterLogic : filterLogicList) {
                String fieldName = filterLogic.getField();
                String normalValue = filterLogic.getValue();
                Filter.Logic.Operator operator = filterLogic.getOperator();
                try {
                    String field = aliasFields.containsKey(fieldName) ? aliasFields.get(fieldName) : fieldName;
                    FilterBuilder oper = createFilterBuilder(field, operator, normalValue);

                    if (filterBuilderList.size() == 0) {
                        filterBuilderList.add(oper);
                    } else {
                        filterBuilderList.add(oper.setLogic(Filter.LogicOperator.and));
                    }
                } catch (InvalidArgumentException e) {
                    throw new GIRuntimeException(e);
                }
            }
            for (FilterBuilder filterBuilder : filterBuilderList) {
                queryString.append(filterBuilder.comparison);
            }
            return filterBuilderList;
        }

        public static void setParameters(Query query, List<FilterBuilder> filterBuilderList) {
            for (FilterBuilder filterBuilder : filterBuilderList) {
                if (!filterBuilder.isUnary()) {
                    setParameter(query, filterBuilder);
                }
            }
        }

        public Query createCountQuery(EntityManager entityManager) {
            List<FilterBuilder> filterBuilderList = buildCountFilter();
            String queryString = buildCountQueryString(filterBuilderList);
            Query query = entityManager.createQuery(queryString);
            for (FilterBuilder filterBuilder : filterBuilderList) {
                setParameter(query, filterBuilder);
            }
            return query;
        }

        public Query createNoPagedQuery(EntityManager entityManager) {
            List<FilterBuilder> filterBuilderList = buildFilter();
            String queryString = buildEntityQueryString(filterBuilderList);
            Query query = entityManager.createQuery(queryString);
            for (FilterBuilder filterBuilder : filterBuilderList) {
                if (!filterBuilder.isUnary()) {
                    setParameter(query, filterBuilder);
                }
            }
            return query;
        }

        private static void setParameter(Query query, FilterBuilder filterBuilder) {
            Class<?> paramType = query.getParameter(filterBuilder.parameter).getParameterType();
            if (filterBuilder.isDate() || paramType == Date.class) {
                try {
                    Date value = defaultDateFormat.parse(filterBuilder.value);
                    query.setParameter(filterBuilder.parameter, value, TemporalType.DATE);
                } catch (ParseException e) {
                    throw new GIRuntimeException(e);
                }
            } else if (paramType == String.class) {
                String value = filterBuilder.value;
                query.setParameter(filterBuilder.parameter, value);
            } else {
                try {
                    Constructor<?> constructor = paramType.getConstructor(String.class);
                    Object value = constructor.newInstance(filterBuilder.value);
                    query.setParameter(filterBuilder.parameter, value);
                } catch (InvocationTargetException | IllegalAccessException | InstantiationException
                    | NoSuchMethodException e) {
                    throw new GIRuntimeException(e);
                }
            }
        }

        public Query createPagedQuery(EntityManager entityManager) {
            Query query = createNoPagedQuery(entityManager);
            Integer firstResult = getFirstResult();
            if (firstResult != null) {
                query.setFirstResult(firstResult);
            }
            Integer maxResults = getMaxResults();
            if (maxResults != null) {
                query.setMaxResults(maxResults);
            }
            return query;
        }

        public String buildSortString() {

            if (kendoFilter.sort == null) {
                return "";
            }

            StringBuilder result = new StringBuilder();
            for (Sort s : kendoFilter.sort) {
                String field = getFieldFromAlias(s.getField());
                if (result.length() == 0) {
                    result.append(String.format(" %s %s ", field, s.getDir()));
                } else {
                    result.append(String.format(", %s %s ", field, s.getDir()));
                }
            }
            return result.toString();
        }

        public List<FilterBuilder> buildFilter() {
            return buildFilter(false);
        }

        public List<FilterBuilder> buildCountFilter() {
            return buildFilter(true);
        }

        /*
            The filterLogic operator (comparison).
            The supported operators are: "eq" (equal to), "neq" (not equal to), "lt" (less than),
            "lte" (less than or equal to), "gt" (greater than), "gte" (greater than or equal to),
            "isnull", "isnotnull", "startswith", "endswith", "contains".
            The last three are supported only for string fields.
        */
        public List<FilterBuilder> buildFilter(boolean excludeSubqueryFilters) {

            List<FilterBuilder> result = new ArrayList<>();

            for (FilterBuilder filter: baseFilters) {
                if (result.size() == 0) {
                    result.add(filter);
                } else {
                    result.add(filter.setLogic(kendoFilter.filter.getLogic()));
                }
            }

            if (kendoFilter.filter == null) {
                return result;
            }

            List<Filter.Logic> filterLogicList = kendoFilter.filter.getFilters();
            for (Filter.Logic filterLogic : filterLogicList) {

                String fieldName = filterLogic.getField();

                if (excludeSubqueryFilters) {
                    if (subqueryAliasFields.containsKey(fieldName) && !aliasFields.containsKey(fieldName)) {
                        continue;
                    }
                }
                if (toIgnoreFilters.contains(fieldName)) {
                    continue;
                }
                String normalValue = filterLogic.getValue();
                Filter.Logic.Operator operator = filterLogic.getOperator();

                try {
                    String field = getFieldFromAlias(fieldName);
                    FilterBuilder oper = createFilterBuilder(field, operator, normalValue);

                    if (result.size() == 0) {
                        result.add(oper);
                    } else {
                        result.add(oper.setLogic(kendoFilter.filter.getLogic()));
                    }

                } catch (InvalidArgumentException e) {
                    throw new GIRuntimeException(e);
                }
            }
            return result;
        }

        private static final DateFormat defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        private String getFieldFromAlias(String fieldAlias) {
            if (aliasFields.containsKey(fieldAlias)) {
                return aliasFields.get(fieldAlias);
            }
            if (subqueryAliasFields.containsKey(fieldAlias)) {
                return subqueryAliasFields.get(fieldAlias);
            }
            return "e." + fieldAlias;
        }

        public static FilterBuilder createFilterBuilder(String field, Filter.Logic.Operator operator, String value)
            throws InvalidArgumentException {

            FilterBuilder oper;
            boolean isDateType = false;
            if (value != null) {
                try {
                    Date date = defaultDateFormat.parse(value.replace('/', '-'));
                    isDateType = true;
                    value = defaultDateFormat.format(date);
                } catch (ParseException ignored) {
                }
            }
            if (isDateType) {
                switch (operator) {
                    case eq:
                        oper = FilterBuilder.createForDate(" trunc(%1$s) = :%2$s", field, value);
                        break;
                    case neq:
                        oper = FilterBuilder.createForDate(" trunc(%1$s) != :%2$s", field, value);
                        break;
                    case gte:
                        oper = FilterBuilder.createForDate(" trunc(%1$s) >= :%2$s", field, value);
                        break;
                    case gt:
                        oper = FilterBuilder.createForDate(" trunc(%1$s) > :%2$s", field, value);
                        break;
                    case lte:
                        oper = FilterBuilder.createForDate(" trunc(%1$s) <= :%2$s", field, value);
                        break;
                    case lt:
                        oper = FilterBuilder.createForDate(" trunc(%1$s) < :%2$s", field, value);
                        break;
                    default:
                        throw new InvalidArgumentException("Invalid Date Type.");
                }
            } else {
                switch (operator) {
                    case eq:
                        oper = FilterBuilder.create(" %1$s = :%2$s", field, value);
                        break;
                    case neq:
                        oper = FilterBuilder.create(" %1$s != :%2$s", field, value);
                        break;
                    case gte:
                        oper = FilterBuilder.create(" %1$s >= :%2$s", field, value);
                        break;
                    case gt:
                        oper = FilterBuilder.create(" %1$s > :%2$s", field, value);
                        break;
                    case lte:
                        oper = FilterBuilder.create(" %1$s <= :%2$s", field, value);
                        break;
                    case lt:
                        oper = FilterBuilder.create(" %1$s < :%2$s", field, value);
                        break;
                    case startswith:
                        oper = FilterBuilder.create(" %1$s like :%2$s", field, value + "%");
                        break;
                    case endswith:
                        oper = FilterBuilder.create(" %1$s like :%2$s", field, "%" + value);
                        break;
                    case contains:
                        oper = FilterBuilder.create(" %1$s like :%2$s", field, "%" + value + "%");
                        break;
                    case startswithignorecase:
					oper = FilterBuilder.create(" lower(%1$s) like :%2$s",
							field, value.toLowerCase() + "%");
					break;
                    case endswithignorecase:
					oper = FilterBuilder.create(" lower(%1$s) like :%2$s",
							field, "%" + value.toLowerCase());
					break;
                    case containsignorecase:
					oper = FilterBuilder.create(" lower(%1$s) like :%2$s",
							field, "%" + value.toLowerCase() + "%"); 
					break;
                    case isnull:
                        oper = FilterBuilder.create(" %1$s is null", field);
                        break;
                    case isnotnull:
                        oper = FilterBuilder.create(" %1$s is not null", field);
                        break;
                    default:
                        throw new InvalidArgumentException("Invalid Data Type.");
                }
            }
            return oper;
        }
    }
}
