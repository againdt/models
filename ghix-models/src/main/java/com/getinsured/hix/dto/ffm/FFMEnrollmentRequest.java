package com.getinsured.hix.dto.ffm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author rajaramesh_g
 *
 */

public class FFMEnrollmentRequest {

	/*The below information about the fields from FFMEnrollmentRequest map's to corresponding xmlPath variables generated in the ApplicantEnrollmentRequest XML*/
	
	//1.informationExchangeSystemId --> exch:ApplicantEnrollmentRequest/ext:PartnerWebSiteInformationExchangeSystem/hix-core:InformationExchangeSystemIdentification/nc:IdentificationID
	//2.userType --> exch:ApplicantEnrollmentRequest/ext:PartnerWebSiteUserCode
	/*3.stateExchangeCode --> concat(exch:ApplicantEnrollmentRequest/ext:ExchangeInformationExchangeSystem/hix-core:InformationExchangeSystemStateCode, 
	                           exch:ApplicantEnrollmentRequest/ext:ExchangeInformationExchangeSystem/hix-core:InformationExchangeSystemIdentification/nc:IdentificationID)*/
	//4.partnerAssignedConsumerId --> exch:ApplicantEnrollmentRequest/ext:SSFPrimaryContact/ext:SSFPrimaryContactPartnerAssignedIdentification/nc:IdentificationID
	//5.ffeAssignedConsumerId --> exch:ApplicantEnrollmentRequest/ext:SSFPrimaryContact/ext:SSFPrimaryContactExchangeAssignedIdentification/nc:IdentificationID
	//6.ffeUserId --> exch:ApplicantEnrollmentRequest/ext:ExchangeUser/ext:ExchangeUserIdentification/nc:IdentificationID
	//7.uiaFirstName --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = /exch:ApplicantEnrollmentRequest/ext:ExchangeUser/ext:RoleOfPersonReference/@s:ref]/nc:PersonName/nc:PersonGivenName
	//8.uiaMiddleName --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = /exch:ApplicantEnrollmentRequest/ext:ExchangeUser/ext:RoleOfPersonReference/@s:ref]/nc:PersonName/nc:PersonMiddleName
	//9.uiaLastName --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = /exch:ApplicantEnrollmentRequest/ext:ExchangeUser/ext:RoleOfPersonReference/@s:ref]/nc:PersonName/nc:PersonSurName
	//10.agentOrBrokerIndicator --> exch:ApplicantEnrollmentRequest/ext:ExchangeIntermediary/ext:ExchangeIntermediaryCode
	//11.agentOrBrokerNationalProducerNumber --> exch:ApplicantEnrollmentRequest/ext:ExchangeIntermediary/ext:NationalProducerIdentification/nc:IdentificationID
	//12.agentOrBrokerFirstName --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = /exch:ApplicantEnrollmentRequest/ext:ExchangeIntermediary/ext:ExchangeIntermediaryEntity/nc:EntityPersonReference/@s:ref]/nc:PersonName/nc:PersonGivenName
	//13.agentOrBrokerMiddleName --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = exch:ApplicantEnrollmentRequest/ext:ExchangeIntermediary/ext:ExchangeIntermediaryEntity/nc:EntityPersonReference/@s:ref]/nc:PersonName/nc:PersonMiddleName
	//14.agentOrBrokerLastName --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = /exch:ApplicantEnrollmentRequest/ext:ExchangeIntermediary/ext:ExchangeIntermediaryEntity/nc:EntityPersonReference/@s:ref]/nc:PersonName/nc:PersonSurName
	//15.agentOrBrokerNameSuffix --> exch:ApplicantEnrollmentRequest/ext:Person[@s:id = /exch:ApplicantEnrollmentRequest/ext:ExchangeIntermediary/ext:ExchangeIntermediaryEntity/nc:EntityPersonReference/@s:ref]/nc:PersonName/nc:PersonNameSuffixText
	//16.aptcAttestationFirstName --> exch:ApplicantEnrollmentRequest/ext:Person/nc:PersonName/nc:PersonGivenName
	//17.aptcAttestationMiddleName --> exch:ApplicantEnrollmentRequest/ext:Person/nc:PersonName/nc:PersonMiddleName
	//18.aptcAttestationLastName --> exch:ApplicantEnrollmentRequest/ext:Person/nc:PersonName/nc:PersonSurName
	//19.aptcAttestationSuffixName --> exch:ApplicantEnrollmentRequest/ext:Person/nc:PersonName/nc:PersonNameSuffixText
	//20.aptcAttestation --> exch:ApplicantEnrollmentRequest/ext:SSFPrimaryContact/ext:AttestationMetadata[@s:id = /exch:ApplicantEnrollmentRequest/ext:Person/@s:metadata]/ext:APTCAttestationProvidedIndicator
	//21.enrollmentGroupFFEAssignedPolicyNumber --> exch:ApplicantEnrollmentRequest/ext:MaintainedInsurancePolicy/ext:ExchangeAssignedInsurancePolicyIdentification/nc:IdentificationID
	//22.enrollmentGroupIssuerPolicyNumber --> exch:ApplicantEnrollmentRequest/ext:MaintainedInsurancePolicy/ext:IssuerAssignedInsurancePolicyIdentification/nc:IdentificationID
	//23.enrollmentGroupStartDate --> exch:ApplicantEnrollmentRequest/ext:MaintainedInsurancePolicy/hix-ee:InsurancePolicyEffectiveDate/nc:Date|exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/hix-ee:InsurancePolicyEffectiveDate/nc:Date
	//24.enrollmentGroupEndDate --> exch:ApplicantEnrollmentRequest/ext:MaintainedInsurancePolicy/hix-ee:InsurancePolicyExpirationDate/nc:Date| exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/hix-ee:InsurancePolicyExpirationDate/nc:Date
	//25.enrollmentGroupIssuerId --> (exch:ApplicantEnrollmentRequest/ext:MaintainedInsurancePolicy/hix-ee:InsurancePlan| exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:SelectedInsurancePlan)/hix-pm:Issuer/hix-pm:IssuerIdentification/nc:IdentificationID
	//26.enrollmentGroupAssignedPlanId --> concat (exch:ApplicantEnrollmentRequest/(ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:SelectedInsurancePlan/hix-pm:InsurancePlanStandardComponentIdentification/nc:IdentificationID| ext:MaintainedInsurancePolicy/hix-ee:InsurancePlan/hix-pm:InsurancePlanStandardComponentIdentification/nc:IdentificationID)[1],exch:ApplicantEnrollmentRequest/(ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:SelectedInsurancePlan/hix-pm:InsurancePlanVariant/hix-pm:InsurancePlanVariantIdentification/nc:IdentificationID| ext:MaintainedInsurancePolicy/hix-ee:InsurancePlan/hix-pm:InsurancePlanVariant/hix-pm:InsurancePlanVariantIdentification/nc:IdentificationID)[1])
	//27.enrollmentGroupTransactionType --> local-name(exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')][1])
	//28.memberFFEAssignedApplicantId --> (exch:ApplicantEnrollmentRequest/(ext:PolicyCreationActivity/ext:EnrollmentGroup|ext:MembersAdditionActivity)/ext:InsuranceApplicant| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant[@s:id = exch:ApplicantEnrollmentResponse/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:AffectedInsuranceMember/ext:OriginatingInsuranceApplicantReference/@s:ref])/ext:ExchangeAssignedInsuranceApplicantIdentification/nc:IdentificationID
	//29.memberIssuerAssignedMemberId --> exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:AffectedInsuranceMember/ext:IssuerAssignedInsuranceMemberIdentification/nc:IdentificationID
	//30.memberFFEAssignedMemberId --> (exch:ApplicantEnrollmentRequest/(ext:PolicyCreationActivity/ext:EnrollmentGroup| ext:MembersAdditionActivity )/ext:InsuranceApplicant| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant[@s:id = exch:ApplicantEnrollmentResponse/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:AffectedInsuranceMember/ext:OriginatingInsuranceApplicantReference/@s:ref])/ext:ExchangeAssignedInsuranceApplicantIdentification/nc:IdentificationID
	//31.memberSubscriberIndicator --> exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:InsuranceApplicant/ext:ProposedSubscriberIndicator| exch:ApplicantEnrollmentRequest/ext:MembersAdditionActivity/ext:InsuranceApplicant/ext:ProposedSubscriberIndicator	| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant/ext:ProposedSubscriberIndicator
	//32.memberRelationshipToSubscriber --> exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:InsuranceApplicant/ext:RelationshipToSubscriberCode| exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:AffectedInsuranceMember/ext:RelationshipToSubscriberCode| exch:ApplicantEnrollmentRequest/ext:MembersAdditionActivity/ext:InsuranceApplicant/ext:RelationshipToSubscriberCode| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant/ext:RelationshipToSubscriberCode
	//33.memberTobaccoUseIndicator --> exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:InsuranceApplicant/ext:TobaccoUseIndicator| exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:AffectedInsuranceMember/ext:TobaccoUseIndicator| exch:ApplicantEnrollmentRequest/ext:MembersAdditionActivity/ext:InsuranceApplicant/ext:TobaccoUseIndicator| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant/ext:TobaccoUseIndicator
	//34.memberLastDateOfTobaccoUse --> exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:InsuranceApplicant/ext:TobaccoLastUsageDate/nc:Date| exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:AffectedInsuranceMember/ext:TobaccoLastUsageDate/nc:Date| exch:ApplicantEnrollmentRequest/ext:MembersAdditionActivity/ext:InsuranceApplicant/ext:TobaccoLastUsageDate/nc:Date| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant/ext:TobaccoLastUsageDate
	//35.memberEnrollmentPeriodType --> exch:ApplicantEnrollmentRequest/ext:PolicyCreationActivity/ext:EnrollmentGroup/ext:InsuranceApplicant/ext:EnrollmentPeriodCode| exch:ApplicantEnrollmentRequest/ext:MembersAdditionActivity/ext:InsuranceApplicant/ext:EnrollmentPeriodCode| exch:ApplicantEnrollmentRequest/ext:ReferencedInsuranceApplicant/ext:EnrollmentPeriodCode
	//36.memberTypeCode --> exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:InsuranceMemberMaintenanceCode
	//37.memberReasonCode --> exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:InsuranceMemberMaintenanceReasonCode
	//38.memberActionEffectiveDate --> exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')]/ext:ActionEffectiveDate/nc:Date| exch:ApplicantEnrollmentRequest/ext:MembersMaintenanceActivity/ext:InsuranceMemberActivity/ext:ActionEffectiveDate/nc:Date
	//39.premiumRatingArea --> exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')]/ext:AffectedInsurancePremium/ext:RateArea/hix-pm:RateAreaIdentification/nc:IdentificationID
	//40.premiumTotalPremiumAmount --> exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')]/ext:AffectedInsurancePremium/hix-ee:InsurancePremiumAmount
	//41.premiumAPTCElectedPercentage --> exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')]/ext:AffectedInsurancePremium/ext:InsurancePremiumElectedAPTCPercentage
	//42.premiumAPTCAppliedAmount --> exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')]/ext:AffectedInsurancePremium/ext:InsurancePremiumAppliedAPTCAmount
	//43.premiumTotalIndividualResponsibilityAmount -->
	//44.premiumCSRLevelApplicable --> exch:ApplicantEnrollmentRequest/*[ends-with(name(), 'Activity')]/ext:AffectedInsurancePremium/hix-pm:InsurancePlanVariantCategoryNumericCode

	private Integer enrollmentId;
	
	// Partner/ Consumer Identification
	private String informationExchangeSystemId;
	private String userType;
	private String stateExchangeCode;
	private String partnerAssignedConsumerId;
	
	// User Identity Assertion
	private String ffeAssignedConsumerId;
	private String ffeUserId;
	private String uiaFirstName;
	private String uiaMiddleName;
	private String uiaLastName;
	
	// Agent/Broker Information
	private String agentOrBrokerIndicator;
	private String agentOrBrokerNationalProducerNumber;
	private String agentOrBrokerFirstName;
	private String agentOrBrokerMiddleName;
	private String agentOrBrokerLastName;
	private String agentOrBrokerNameSuffix;

	// APTC Attestation Information
	private String aptcAttestationFirstName;
	private String aptcAttestationMiddleName;
	private String aptcAttestationLastName;
	private String aptcAttestationSuffixName;
	private String aptcAttestationId;
	private String aptcAttestation;
	private String aptcAttestationSsn;
	private Date aptcAttestationDob;
	private String aptcAttestationGender;

	// Enrollment Group
	private String enrollmentGroupFFEAssignedPolicyNumber;
	private String enrollmentGroupIssuerPolicyNumber;
	private String enrollmentGroupStartDate;
	private String enrollmentGroupEndDate;
	private String enrollmentGroupIssuerId;
	private String enrollmentGroupAssignedPlanId;
	private String enrollmentGroupTransactionType;

	// Member Actions 
	private String memberFFEAssignedApplicantId;
	private String memberIssuerAssignedMemberId;
	private String memberFFEAssignedMemberId;
	private String memberSubscriberIndicator;
	private String memberRelationshipToSubscriber;
	private String memberTobaccoUseIndicator;
	private String memberLastDateOfTobaccoUse;
	private String memberEnrollmentPeriodType;
	private String memberTypeCode;
	private String memberReasonCode;
	private String memberActionEffectiveDate;

	// Premium Information
	private String premiumRatingArea;
	private Float premiumTotalPremiumAmount;
	private Float premiumAPTCElectedPercentage;
	private Float premiumAPTCAppliedAmount;
	private Float premiumTotalIndividualResponsibilityAmount;
	private String premiumCSRLevelApplicable;
	private Long ssapApplicationId;
	
	List<FFMEnrollmentMemberData> members = new ArrayList<FFMEnrollmentMemberData>();
 	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getInformationExchangeSystemId() {
		return informationExchangeSystemId;
	}
	public void setInformationExchangeSystemId(String informationExchangeSystemId) {
		this.informationExchangeSystemId = informationExchangeSystemId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getStateExchangeCode() {
		return stateExchangeCode;
	}
	public void setStateExchangeCode(String stateExchangeCode) {
		this.stateExchangeCode = stateExchangeCode;
	}
	public String getPartnerAssignedConsumerId() {
		return partnerAssignedConsumerId;
	}
	public void setPartnerAssignedConsumerId(String partnerAssignedConsumerId) {
		this.partnerAssignedConsumerId = partnerAssignedConsumerId;
	}
	public String getFfeAssignedConsumerId() {
		return ffeAssignedConsumerId;
	}
	public void setFfeAssignedConsumerId(String ffeAssignedConsumerId) {
		this.ffeAssignedConsumerId = ffeAssignedConsumerId;
	}
	public String getFfeUserId() {
		return ffeUserId;
	}
	public void setFfeUserId(String ffeUserId) {
		this.ffeUserId = ffeUserId;
	}
	public String getUiaFirstName() {
		return uiaFirstName;
	}
	public void setUiaFirstName(String uiaFirstName) {
		this.uiaFirstName = uiaFirstName;
	}
	public String getUiaMiddleName() {
		return uiaMiddleName;
	}
	public void setUiaMiddleName(String uiaMiddleName) {
		this.uiaMiddleName = uiaMiddleName;
	}
	public String getUiaLastName() {
		return uiaLastName;
	}
	public void setUiaLastName(String uiaLastName) {
		this.uiaLastName = uiaLastName;
	}
	public String getAgentOrBrokerIndicator() {
		return agentOrBrokerIndicator;
	}
	public void setAgentOrBrokerIndicator(String agentOrBrokerIndicator) {
		this.agentOrBrokerIndicator = agentOrBrokerIndicator;
	}
	public String getAgentOrBrokerNationalProducerNumber() {
		return agentOrBrokerNationalProducerNumber;
	}
	public void setAgentOrBrokerNationalProducerNumber(
			String agentOrBrokerNationalProducerNumber) {
		this.agentOrBrokerNationalProducerNumber = agentOrBrokerNationalProducerNumber;
	}
	public String getAgentOrBrokerFirstName() {
		return agentOrBrokerFirstName;
	}
	public void setAgentOrBrokerFirstName(String agentOrBrokerFirstName) {
		this.agentOrBrokerFirstName = agentOrBrokerFirstName;
	}
	public String getAgentOrBrokerMiddleName() {
		return agentOrBrokerMiddleName;
	}
	public void setAgentOrBrokerMiddleName(String agentOrBrokerMiddleName) {
		this.agentOrBrokerMiddleName = agentOrBrokerMiddleName;
	}
	public String getAgentOrBrokerLastName() {
		return agentOrBrokerLastName;
	}
	public void setAgentOrBrokerLastName(String agentOrBrokerLastName) {
		this.agentOrBrokerLastName = agentOrBrokerLastName;
	}
	public String getAgentOrBrokerNameSuffix() {
		return agentOrBrokerNameSuffix;
	}
	public void setAgentOrBrokerNameSuffix(String agentOrBrokerNameSuffix) {
		this.agentOrBrokerNameSuffix = agentOrBrokerNameSuffix;
	}
	public String getAptcAttestationFirstName() {
		return aptcAttestationFirstName;
	}
	public void setAptcAttestationFirstName(String aptcAttestationFirstName) {
		this.aptcAttestationFirstName = aptcAttestationFirstName;
	}
	public String getAptcAttestationMiddleName() {
		return aptcAttestationMiddleName;
	}
	public void setAptcAttestationMiddleName(String aptcAttestationMiddleName) {
		this.aptcAttestationMiddleName = aptcAttestationMiddleName;
	}
	public String getAptcAttestationLastName() {
		return aptcAttestationLastName;
	}
	public void setAptcAttestationLastName(String aptcAttestationLastName) {
		this.aptcAttestationLastName = aptcAttestationLastName;
	}
	public String getAptcAttestationSuffixName() {
		return aptcAttestationSuffixName;
	}
	public void setAptcAttestationSuffixName(String aptcAttestationSuffixName) {
		this.aptcAttestationSuffixName = aptcAttestationSuffixName;
	}
	
	
	public String getAptcAttestationId() {
		return aptcAttestationId;
	}
	public void setAptcAttestationId(String aptcAttestationId) {
		this.aptcAttestationId = aptcAttestationId;
	}
	public String getAptcAttestation() {
		return aptcAttestation;
	}
	public void setAptcAttestation(String aptcAttestation) {
		this.aptcAttestation = aptcAttestation;
	}
	
	public String getAptcAttestationSsn() {
		return aptcAttestationSsn;
	}
	public void setAptcAttestationSsn(String aptcAttestationSsn) {
		this.aptcAttestationSsn = aptcAttestationSsn;
	}
	public Date getAptcAttestationDob() {
		return aptcAttestationDob;
	}
	public void setAptcAttestationDob(Date aptcAttestationDob) {
		this.aptcAttestationDob = aptcAttestationDob;
	}
	public String getAptcAttestationGender() {
		return aptcAttestationGender;
	}
	public void setAptcAttestationGender(String aptcAttestationGender) {
		this.aptcAttestationGender = aptcAttestationGender;
	}
	public String getEnrollmentGroupFFEAssignedPolicyNumber() {
		return enrollmentGroupFFEAssignedPolicyNumber;
	}
	public void setEnrollmentGroupFFEAssignedPolicyNumber(
			String enrollmentGroupFFEAssignedPolicyNumber) {
		this.enrollmentGroupFFEAssignedPolicyNumber = enrollmentGroupFFEAssignedPolicyNumber;
	}
	public String getEnrollmentGroupIssuerPolicyNumber() {
		return enrollmentGroupIssuerPolicyNumber;
	}
	public void setEnrollmentGroupIssuerPolicyNumber(
			String enrollmentGroupIssuerPolicyNumber) {
		this.enrollmentGroupIssuerPolicyNumber = enrollmentGroupIssuerPolicyNumber;
	}
	public String getEnrollmentGroupStartDate() {
		return enrollmentGroupStartDate;
	}
	public void setEnrollmentGroupStartDate(String enrollmentGroupStartDate) {
		this.enrollmentGroupStartDate = enrollmentGroupStartDate;
	}
	public String getEnrollmentGroupEndDate() {
		return enrollmentGroupEndDate;
	}
	public void setEnrollmentGroupEndDate(String enrollmentGroupEndDate) {
		this.enrollmentGroupEndDate = enrollmentGroupEndDate;
	}
	public String getEnrollmentGroupIssuerId() {
		return enrollmentGroupIssuerId;
	}
	public void setEnrollmentGroupIssuerId(String enrollmentGroupIssuerId) {
		this.enrollmentGroupIssuerId = enrollmentGroupIssuerId;
	}
	public String getEnrollmentGroupAssignedPlanId() {
		return enrollmentGroupAssignedPlanId;
	}
	public void setEnrollmentGroupAssignedPlanId(
			String enrollmentGroupAssignedPlanId) {
		this.enrollmentGroupAssignedPlanId = enrollmentGroupAssignedPlanId;
	}
	public String getEnrollmentGroupTransactionType() {
		return enrollmentGroupTransactionType;
	}
	public void setEnrollmentGroupTransactionType(
			String enrollmentGroupTransactionType) {
		this.enrollmentGroupTransactionType = enrollmentGroupTransactionType;
	}
	public String getMemberFFEAssignedApplicantId() {
		return memberFFEAssignedApplicantId;
	}
	public void setMemberFFEAssignedApplicantId(String memberFFEAssignedApplicantId) {
		this.memberFFEAssignedApplicantId = memberFFEAssignedApplicantId;
	}
	public String getMemberIssuerAssignedMemberId() {
		return memberIssuerAssignedMemberId;
	}
	public void setMemberIssuerAssignedMemberId(String memberIssuerAssignedMemberId) {
		this.memberIssuerAssignedMemberId = memberIssuerAssignedMemberId;
	}
	public String getMemberFFEAssignedMemberId() {
		return memberFFEAssignedMemberId;
	}
	public void setMemberFFEAssignedMemberId(String memberFFEAssignedMemberId) {
		this.memberFFEAssignedMemberId = memberFFEAssignedMemberId;
	}
	public String getMemberSubscriberIndicator() {
		return memberSubscriberIndicator;
	}
	public void setMemberSubscriberIndicator(String memberSubscriberIndicator) {
		this.memberSubscriberIndicator = memberSubscriberIndicator;
	}
	public String getMemberRelationshipToSubscriber() {
		return memberRelationshipToSubscriber;
	}
	public void setMemberRelationshipToSubscriber(
			String memberRelationshipToSubscriber) {
		this.memberRelationshipToSubscriber = memberRelationshipToSubscriber;
	}
	public String getMemberTobaccoUseIndicator() {
		return memberTobaccoUseIndicator;
	}
	public void setMemberTobaccoUseIndicator(String memberTobaccoUseIndicator) {
		this.memberTobaccoUseIndicator = memberTobaccoUseIndicator;
	}
	public String getMemberLastDateOfTobaccoUse() {
		return memberLastDateOfTobaccoUse;
	}
	public void setMemberLastDateOfTobaccoUse(String memberLastDateOfTobaccoUse) {
		this.memberLastDateOfTobaccoUse = memberLastDateOfTobaccoUse;
	}
	public String getMemberEnrollmentPeriodType() {
		return memberEnrollmentPeriodType;
	}
	public void setMemberEnrollmentPeriodType(String memberEnrollmentPeriodType) {
		this.memberEnrollmentPeriodType = memberEnrollmentPeriodType;
	}
	public String getMemberTypeCode() {
		return memberTypeCode;
	}
	public void setMemberTypeCode(String memberTypeCode) {
		this.memberTypeCode = memberTypeCode;
	}
	public String getMemberReasonCode() {
		return memberReasonCode;
	}
	public void setMemberReasonCode(String memberReasonCode) {
		this.memberReasonCode = memberReasonCode;
	}
	public String getMemberActionEffectiveDate() {
		return memberActionEffectiveDate;
	}
	public void setMemberActionEffectiveDate(String memberActionEffectiveDate) {
		this.memberActionEffectiveDate = memberActionEffectiveDate;
	}
	public String getPremiumRatingArea() {
		return premiumRatingArea;
	}
	public void setPremiumRatingArea(String premiumRatingArea) {
		this.premiumRatingArea = premiumRatingArea;
	}
	public Float getPremiumTotalPremiumAmount() {
		return premiumTotalPremiumAmount;
	}
	public void setPremiumTotalPremiumAmount(Float premiumTotalPremiumAmount) {
		this.premiumTotalPremiumAmount = premiumTotalPremiumAmount;
	}
	public Float getPremiumAPTCElectedPercentage() {
		return premiumAPTCElectedPercentage;
	}
	public void setPremiumAPTCElectedPercentage(Float premiumAPTCElectedPercentage) {
		this.premiumAPTCElectedPercentage = premiumAPTCElectedPercentage;
	}
	public Float getPremiumAPTCAppliedAmount() {
		return premiumAPTCAppliedAmount;
	}
	public void setPremiumAPTCAppliedAmount(Float premiumAPTCAppliedAmount) {
		this.premiumAPTCAppliedAmount = premiumAPTCAppliedAmount;
	}
	public Float getPremiumTotalIndividualResponsibilityAmount() {
		return premiumTotalIndividualResponsibilityAmount;
	}
	public void setPremiumTotalIndividualResponsibilityAmount(
			Float premiumTotalIndividualResponsibilityAmount) {
		this.premiumTotalIndividualResponsibilityAmount = premiumTotalIndividualResponsibilityAmount;
	}
	public String getPremiumCSRLevelApplicable() {
		return premiumCSRLevelApplicable;
	}
	public void setPremiumCSRLevelApplicable(String premiumCSRLevelApplicable) {
		this.premiumCSRLevelApplicable = premiumCSRLevelApplicable;
	}
	
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public List<FFMEnrollmentMemberData> getMembers() {
		return members;
	}
	public void setMembers(List<FFMEnrollmentMemberData> members) {
		this.members = members;
	}

	
	
	
}
