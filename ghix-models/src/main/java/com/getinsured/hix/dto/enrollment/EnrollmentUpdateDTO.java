package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.dto.enrollment.EnrolleeDataUpdateDTO;
import com.getinsured.hix.platform.util.GhixConstants;

public class EnrollmentUpdateDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum ENROllMENT_ACTION{
		ENROLLMENT_START_DATE_CHANGE,MEMBER_START_DATE_CHANGE,ENROLLMENT_END_DATE_CHANGE,MEMBER_END_DATE_CHANGE, MEMBER_DATA_CHANGE, CANCEL, ADD
	}
	private Integer id;
	private Date benefitEffectiveDate;
	private Date benefitEndDate;
	private String benefitEffectiveDateStr;
	private String benefitEndDateStr;
	private String householdId;
	
	private String confirmationDateStr;
	private Date confirmationDate;
	
	private String Status;
	
	private String healthCoveragePolicyNo;
	
	private String maintainenceReasonCodeStr;
	
	List<EnrolleeDataUpdateDTO> members;
	
	private ENROllMENT_ACTION action;
	
	private boolean confirmIfNotConfirmed;
	
	private String comment;
	
	private List<EnrollmentPremiumDTO> enrollmentPremiumDtoList;
	
	private String loggedInUserEmail;
	
	private boolean send834=true;
	
	private Float aptcAmount;

    private BigDecimal stateSubsidyAmount;
	
	private String cancellationReasonCode;
	
	private Date premiumPaidToDateEnd;
	
	private String premiumPaidToDateEndStr;

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	private void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	private void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public String getMaintainenceReasonCodeStr() {
		return maintainenceReasonCodeStr;
	}

	public void setMaintainenceReasonCodeStr(String maintainenceReasonCodeStr) {
		this.maintainenceReasonCodeStr = maintainenceReasonCodeStr;
	}

	public List<EnrolleeDataUpdateDTO> getMembers() {
		return members;
	}

	public void setMembers(List<EnrolleeDataUpdateDTO> members) {
		this.members = members;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ENROllMENT_ACTION getAction() {
		return action;
	}

	public void setAction(ENROllMENT_ACTION action) {
		this.action = action;
	}

	public boolean isConfirmIfNotConfirmed() {
		return confirmIfNotConfirmed;
	}

	public void setConfirmIfNotConfirmed(boolean confirmIfNotConfirmed) {
		this.confirmIfNotConfirmed = confirmIfNotConfirmed;
	}

	public String getHealthCoveragePolicyNo() {
		return healthCoveragePolicyNo;
	}

	public void setHealthCoveragePolicyNo(String healthCoveragePolicyNo) {
		this.healthCoveragePolicyNo = healthCoveragePolicyNo;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	/**
	 * @return the benefitEffectiveDateStr
	 */
	public String getBenefitEffectiveDateStr() {
		return benefitEffectiveDateStr;
	}

	/**
	 * @param benefitEffectiveDateStr the benefitEffectiveDateStr to set
	 */
	public void setBenefitEffectiveDateStr(String benefitEffectiveDateStr)throws ParseException {
		this.benefitEffectiveDateStr = benefitEffectiveDateStr;
		if(StringUtils.isNotBlank(benefitEffectiveDateStr)){
			setBenefitEffectiveDate(StringToDate(benefitEffectiveDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
		}
	}

	/**
	 * @return the benefitEndDateStr
	 */
	public String getBenefitEndDateStr() {
		return benefitEndDateStr;
	}

	/**
	 * @param benefitEndDateStr the benefitEndDateStr to set
	 */
	public void setBenefitEndDateStr(String benefitEndDateStr)throws ParseException {
		this.benefitEndDateStr = benefitEndDateStr;
		if(StringUtils.isNotBlank(benefitEndDateStr)){
			setBenefitEndDate(StringToDate(benefitEndDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
		}
	}

	private Date StringToDate(String dateStr, String format) throws ParseException{
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    return dateFormat.parse(dateStr);
	}

	public List<EnrollmentPremiumDTO> getEnrollmentPremiumDtoList() {
		return enrollmentPremiumDtoList;
	}

	public void setEnrollmentPremiumDtoList(List<EnrollmentPremiumDTO> enrollmentPremiumDtoList) {
		this.enrollmentPremiumDtoList = enrollmentPremiumDtoList;
	}

	public String getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}

	public String getConfirmationDateStr() {
		return confirmationDateStr;
	}

	public void setConfirmationDateStr(String confirmationDateStr) throws ParseException {
		this.confirmationDateStr = confirmationDateStr;
		if(StringUtils.isNotBlank(confirmationDateStr)){
			setConfirmationDate(StringToDate(confirmationDateStr, GhixConstants.REQUIRED_DATE_FORMAT));
		}
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getLoggedInUserEmail() {
		return loggedInUserEmail;
	}

	public void setLoggedInUserEmail(String loggedInUserEmail) {
		this.loggedInUserEmail = loggedInUserEmail;
	}

	public Float getAptcAmount() {
		return aptcAmount;
	}

	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}

    public BigDecimal getStateSubsidyAmount() {
        return stateSubsidyAmount;
    }

    public void setStateSubsidyAmount(BigDecimal stateSubsidyAmount) {
        this.stateSubsidyAmount = stateSubsidyAmount;
    }

    public void setSend834(boolean send834) {
		this.send834 = send834;
	}

	public boolean isSend834() {
		return send834;
	}

	public String getCancellationReasonCode() {
		return cancellationReasonCode;
	}

	public void setCancellationReasonCode(String cancellationReasonCode) {
		this.cancellationReasonCode = cancellationReasonCode;
	}

	public Date getPremiumPaidToDateEnd() {
		return premiumPaidToDateEnd;
	}

	public String getPremiumPaidToDateEndStr() {
		return premiumPaidToDateEndStr;
	}

	public void setPremiumPaidToDateEnd(Date premiumPaidToDateEnd) {
		this.premiumPaidToDateEnd = premiumPaidToDateEnd;
	}

	public void setPremiumPaidToDateEndStr(String premiumPaidToDateEndStr) throws ParseException {
		this.premiumPaidToDateEndStr = premiumPaidToDateEndStr;
		if(StringUtils.isNotBlank(premiumPaidToDateEndStr)){
			setPremiumPaidToDateEnd(StringToDate(premiumPaidToDateEndStr, GhixConstants.REQUIRED_DATE_FORMAT));
		}
	}

	@Override
	public String toString() {
		return "EnrollmentUpdateDTO [id=" + id + ", benefitEffectiveDate=" + benefitEffectiveDate + ", benefitEndDate="
				+ benefitEndDate + ", benefitEffectiveDateStr=" + benefitEffectiveDateStr + ", benefitEndDateStr="
				+ benefitEndDateStr + ", householdId=" + householdId + ", confirmationDateStr=" + confirmationDateStr
				+ ", confirmationDate=" + confirmationDate + ", Status=" + Status + ", healthCoveragePolicyNo="
				+ healthCoveragePolicyNo + ", maintainenceReasonCodeStr=" + maintainenceReasonCodeStr + ", members="
				+ members + ", action=" + action + ", confirmIfNotConfirmed=" + confirmIfNotConfirmed + ", comment="
				+ comment + ", enrollmentPremiumDtoList=" + enrollmentPremiumDtoList + ", loggedInUserEmail="
				+ loggedInUserEmail + ", send834=" + send834 + ", aptcAmount=" + aptcAmount + ", stateSubsidyAmount=" + stateSubsidyAmount
				+ ", cancellationReasonCode=" + cancellationReasonCode + ", premiumPaidToDateEnd="
				+ premiumPaidToDateEnd + ", premiumPaidToDateEndStr=" + premiumPaidToDateEndStr + "]";
	}
	
	
}
