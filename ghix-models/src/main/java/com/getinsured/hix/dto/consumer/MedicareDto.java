package com.getinsured.hix.dto.consumer;

import java.io.Serializable;

public class MedicareDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean exchangeElible;
	private String  HICN;
	private String medicarePartAeffectiveDate;
	private String medicarePartAterminationDate;
	private String medicarePartBeffectiveDate;
	private String medicarePartBterminationDate;
	private String pinCode;
	private MedicareLeadPipeLineInfo leadPipeLineInfo;
	
	public MedicareLeadPipeLineInfo getLeadPipeLineInfo() {
		return leadPipeLineInfo;
	}
	public void setLeadPipeLineInfo(MedicareLeadPipeLineInfo leadPipeLineInfo) {
		this.leadPipeLineInfo = leadPipeLineInfo;
	}
	public boolean isExchangeElible() {
		return exchangeElible;
	}
	public void setExchangeElible(boolean exchangeElible) {
		this.exchangeElible = exchangeElible;
	}
	public String getHICN() {
		return HICN;
	}
	public void setHICN(String hICN) {
		HICN = hICN;
	}
	
	public String getMedicarePartBeffectiveDate() {
		return medicarePartBeffectiveDate;
	}
	public void setMedicarePartBeffectiveDate(String partB_EffectiveDate) {
		medicarePartBeffectiveDate = partB_EffectiveDate;
	}
	public String getMedicarePartBterminationDate() {
		return medicarePartBterminationDate;
	}
	public void setMedicarePartBterminationDate(String partB_TerminationDate) {
		medicarePartBterminationDate = partB_TerminationDate;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getMedicarePartAeffectiveDate() {
		return medicarePartAeffectiveDate;
	}
	public void setMedicarePartAeffectiveDate(String medicarePartAeffectiveDate) {
		this.medicarePartAeffectiveDate = medicarePartAeffectiveDate;
	}
	public String getMedicarePartAterminationDate() {
		return medicarePartAterminationDate;
	}
	public void setMedicarePartAterminationDate(String medicarePartAterminationDate) {
		this.medicarePartAterminationDate = medicarePartAterminationDate;
	}


}
