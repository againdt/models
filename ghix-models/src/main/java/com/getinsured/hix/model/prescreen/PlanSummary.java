package com.getinsured.hix.model.prescreen;



import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.solr.client.solrj.beans.Field;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.hix.model.RatingArea;


public class PlanSummary implements Serializable{

	private static final long serialVersionUID = 4376872127315053281L;
	
	@Field private String zip;
	@Field private String county;
	@Field private String state;
	@Field private String year;
	@Field private float slsp21;
	@Field private float lsp21;
	@Field private float lbp21;
	@Field private String giOnExchgPlanAvailability ;
	@Field("gIOffExchgPlanAvailability")
	private String giOffExchgPlanAvailability;
	@Field private float giLsp21CS1OnExchg;
	@Field private float giLsp21CS2OnExchg;
	@Field private float giLsp21CS3OnExchg;
	@Field private float giLsp21CS4OnExchg;
	@Field private float giLsp21CS5OnExchg;
	@Field private float giLsp21CS6OnExchg;
	@Field private float giLsp21CS1OnExchgTobacco;
	@Field private float giLsp21CS2OnExchgTobacco;
	@Field private float giLsp21CS3OnExchgTobacco;
	@Field private float giLsp21CS4OnExchgTobacco;
	@Field private float giLsp21CS5OnExchgTobacco;
	@Field private float giLsp21CS6OnExchgTobacco;
	@Field private float giTobaccoRatioLspOnExchg21;
	@Field private float giTobaccoRatioLspOnExchg22;
	@Field private float giTobaccoRatioLspOnExchg23;
	@Field private float giTobaccoRatioLspOnExchg24;
	@Field private float giTobaccoRatioLspOnExchg25;
	@Field private float giTobaccoRatioLspOnExchg26;
	@Field private float giTobaccoRatioLspOnExchg27;
	@Field private float giTobaccoRatioLspOnExchg28;
	@Field private float giTobaccoRatioLspOnExchg29;
	@Field private float giTobaccoRatioLspOnExchg30;
	@Field private float giTobaccoRatioLspOnExchg31;
	@Field private float giTobaccoRatioLspOnExchg32;
	@Field private float giTobaccoRatioLspOnExchg33;
	@Field private float giTobaccoRatioLspOnExchg34;
	@Field private float giTobaccoRatioLspOnExchg35;
	@Field private float giTobaccoRatioLspOnExchg36;
	@Field private float giTobaccoRatioLspOnExchg37;
	@Field private float giTobaccoRatioLspOnExchg38;
	@Field private float giTobaccoRatioLspOnExchg39;
	@Field private float giTobaccoRatioLspOnExchg40;
	@Field private float giTobaccoRatioLspOnExchg41;
	@Field private float giTobaccoRatioLspOnExchg42;
	@Field private float giTobaccoRatioLspOnExchg43;
	@Field private float giTobaccoRatioLspOnExchg44;
	@Field private float giTobaccoRatioLspOnExchg45;
	@Field private float giTobaccoRatioLspOnExchg46;
	@Field private float giTobaccoRatioLspOnExchg47;
	@Field private float giTobaccoRatioLspOnExchg48;
	@Field private float giTobaccoRatioLspOnExchg49;
	@Field private float giTobaccoRatioLspOnExchg50;
	@Field private float giTobaccoRatioLspOnExchg51;
	@Field private float giTobaccoRatioLspOnExchg52;
	@Field private float giTobaccoRatioLspOnExchg53;
	@Field private float giTobaccoRatioLspOnExchg54;
	@Field private float giTobaccoRatioLspOnExchg55;
	@Field private float giTobaccoRatioLspOnExchg56;
	@Field private float giTobaccoRatioLspOnExchg57;
	@Field private float giTobaccoRatioLspOnExchg58;
	@Field private float giTobaccoRatioLspOnExchg59;
	@Field private float giTobaccoRatioLspOnExchg60;
	@Field private float giTobaccoRatioLspOnExchg61;
	@Field private float giTobaccoRatioLspOnExchg62;
	@Field private float giTobaccoRatioLspOnExchg63;
	@Field private float giTobaccoRatioLspOnExchg64;
	@Field private float giLbp21OnExchg;
	@Field private float giLbp21OnExchgTobacco;
	@Field private float giTobaccoRatioLbpOnExchg21;
	@Field private float giTobaccoRatioLbpOnExchg22;
	@Field private float giTobaccoRatioLbpOnExchg23;
	@Field private float giTobaccoRatioLbpOnExchg24;
	@Field private float giTobaccoRatioLbpOnExchg25;
	@Field private float giTobaccoRatioLbpOnExchg26;
	@Field private float giTobaccoRatioLbpOnExchg27;
	@Field private float giTobaccoRatioLbpOnExchg28;
	@Field private float giTobaccoRatioLbpOnExchg29;
	@Field private float giTobaccoRatioLbpOnExchg30;
	@Field private float giTobaccoRatioLbpOnExchg31;
	@Field private float giTobaccoRatioLbpOnExchg32;
	@Field private float giTobaccoRatioLbpOnExchg33;
	@Field private float giTobaccoRatioLbpOnExchg34;
	@Field private float giTobaccoRatioLbpOnExchg35;
	@Field private float giTobaccoRatioLbpOnExchg36;
	@Field private float giTobaccoRatioLbpOnExchg37;
	@Field private float giTobaccoRatioLbpOnExchg38;
	@Field private float giTobaccoRatioLbpOnExchg39;
	@Field private float giTobaccoRatioLbpOnExchg40;
	@Field private float giTobaccoRatioLbpOnExchg41;
	@Field private float giTobaccoRatioLbpOnExchg42;
	@Field private float giTobaccoRatioLbpOnExchg43;
	@Field private float giTobaccoRatioLbpOnExchg44;
	@Field private float giTobaccoRatioLbpOnExchg45;
	@Field private float giTobaccoRatioLbpOnExchg46;
	@Field private float giTobaccoRatioLbpOnExchg47;
	@Field private float giTobaccoRatioLbpOnExchg48;
	@Field private float giTobaccoRatioLbpOnExchg49;
	@Field private float giTobaccoRatioLbpOnExchg50;
	@Field private float giTobaccoRatioLbpOnExchg51;
	@Field private float giTobaccoRatioLbpOnExchg52;
	@Field private float giTobaccoRatioLbpOnExchg53;
	@Field private float giTobaccoRatioLbpOnExchg54;
	@Field private float giTobaccoRatioLbpOnExchg55;
	@Field private float giTobaccoRatioLbpOnExchg56;
	@Field private float giTobaccoRatioLbpOnExchg57;
	@Field private float giTobaccoRatioLbpOnExchg58;
	@Field private float giTobaccoRatioLbpOnExchg59;
	@Field private float giTobaccoRatioLbpOnExchg60;
	@Field private float giTobaccoRatioLbpOnExchg61;
	@Field private float giTobaccoRatioLbpOnExchg62;
	@Field private float giTobaccoRatioLbpOnExchg63;
	@Field private float giTobaccoRatioLbpOnExchg64;
	@Field private float giLsp21OffExchg;
	@Field private float giLsp21OffExchgTobacco;
	@Field private float giTobaccoRatioLspOffExchg21;
	@Field private float giTobaccoRatioLspOffExchg22;
	@Field private float giTobaccoRatioLspOffExchg23;
	@Field private float giTobaccoRatioLspOffExchg24;
	@Field private float giTobaccoRatioLspOffExchg25;
	@Field private float giTobaccoRatioLspOffExchg26;
	@Field private float giTobaccoRatioLspOffExchg27;
	@Field private float giTobaccoRatioLspOffExchg28;
	@Field private float giTobaccoRatioLspOffExchg29;
	@Field private float giTobaccoRatioLspOffExchg30;
	@Field private float giTobaccoRatioLspOffExchg31;
	@Field private float giTobaccoRatioLspOffExchg32;
	@Field private float giTobaccoRatioLspOffExchg33;
	@Field private float giTobaccoRatioLspOffExchg34;
	@Field private float giTobaccoRatioLspOffExchg35;
	@Field private float giTobaccoRatioLspOffExchg36;
	@Field private float giTobaccoRatioLspOffExchg37;
	@Field private float giTobaccoRatioLspOffExchg38;
	@Field private float giTobaccoRatioLspOffExchg39;
	@Field private float giTobaccoRatioLspOffExchg40;
	@Field private float giTobaccoRatioLspOffExchg41;
	@Field private float giTobaccoRatioLspOffExchg42;
	@Field private float giTobaccoRatioLspOffExchg43;
	@Field private float giTobaccoRatioLspOffExchg44;
	@Field private float giTobaccoRatioLspOffExchg45;
	@Field private float giTobaccoRatioLspOffExchg46;
	@Field private float giTobaccoRatioLspOffExchg47;
	@Field private float giTobaccoRatioLspOffExchg48;
	@Field private float giTobaccoRatioLspOffExchg49;
	@Field private float giTobaccoRatioLspOffExchg50;
	@Field private float giTobaccoRatioLspOffExchg51;
	@Field private float giTobaccoRatioLspOffExchg52;
	@Field private float giTobaccoRatioLspOffExchg53;
	@Field private float giTobaccoRatioLspOffExchg54;
	@Field private float giTobaccoRatioLspOffExchg55;
	@Field private float giTobaccoRatioLspOffExchg56;
	@Field private float giTobaccoRatioLspOffExchg57;
	@Field private float giTobaccoRatioLspOffExchg58;
	@Field private float giTobaccoRatioLspOffExchg59;
	@Field private float giTobaccoRatioLspOffExchg60;
	@Field private float giTobaccoRatioLspOffExchg61;
	@Field private float giTobaccoRatioLspOffExchg62;
	@Field private float giTobaccoRatioLspOffExchg63;
	@Field private float giTobaccoRatioLspOffExchg64;
	@Field private float giLbp21OffExchg;
	@Field private float giLbp21OffExchgTobacco;
	@Field private float giTobaccoRatioLbpOffExchg21;
	@Field private float giTobaccoRatioLbpOffExchg22;
	@Field private float giTobaccoRatioLbpOffExchg23;
	@Field private float giTobaccoRatioLbpOffExchg24;
	@Field private float giTobaccoRatioLbpOffExchg25;
	@Field private float giTobaccoRatioLbpOffExchg26;
	@Field private float giTobaccoRatioLbpOffExchg27;
	@Field private float giTobaccoRatioLbpOffExchg28;
	@Field private float giTobaccoRatioLbpOffExchg29;
	@Field private float giTobaccoRatioLbpOffExchg30;
	@Field private float giTobaccoRatioLbpOffExchg31;
	@Field private float giTobaccoRatioLbpOffExchg32;
	@Field private float giTobaccoRatioLbpOffExchg33;
	@Field private float giTobaccoRatioLbpOffExchg34;
	@Field private float giTobaccoRatioLbpOffExchg35;
	@Field private float giTobaccoRatioLbpOffExchg36;
	@Field private float giTobaccoRatioLbpOffExchg37;
	@Field private float giTobaccoRatioLbpOffExchg38;
	@Field private float giTobaccoRatioLbpOffExchg39;
	public float getGiLsp21CS1OnExchg() {
		return giLsp21CS1OnExchg;
	}
	public void setGiLsp21CS1OnExchg(float giLsp21CS1OnExchg) {
		this.giLsp21CS1OnExchg = giLsp21CS1OnExchg;
	}
	public float getGiLsp21CS2OnExchg() {
		return giLsp21CS2OnExchg;
	}
	public void setGiLsp21CS2OnExchg(float giLsp21CS2OnExchg) {
		this.giLsp21CS2OnExchg = giLsp21CS2OnExchg;
	}
	public float getGiLsp21CS3OnExchg() {
		return giLsp21CS3OnExchg;
	}
	public void setGiLsp21CS3OnExchg(float giLsp21CS3OnExchg) {
		this.giLsp21CS3OnExchg = giLsp21CS3OnExchg;
	}
	public float getGiLsp21CS4OnExchg() {
		return giLsp21CS4OnExchg;
	}
	public void setGiLsp21CS4OnExchg(float giLsp21CS4OnExchg) {
		this.giLsp21CS4OnExchg = giLsp21CS4OnExchg;
	}
	public float getGiLsp21CS5OnExchg() {
		return giLsp21CS5OnExchg;
	}
	public void setGiLsp21CS5OnExchg(float giLsp21CS5OnExchg) {
		this.giLsp21CS5OnExchg = giLsp21CS5OnExchg;
	}
	public float getGiLsp21CS6OnExchg() {
		return giLsp21CS6OnExchg;
	}
	public void setGiLsp21CS6OnExchg(float giLsp21CS6OnExchg) {
		this.giLsp21CS6OnExchg = giLsp21CS6OnExchg;
	}
	public float getGiLsp21CS1OnExchgTobacco() {
		return giLsp21CS1OnExchgTobacco;
	}
	public void setGiLsp21CS1OnExchgTobacco(float giLsp21CS1OnExchgTobacco) {
		this.giLsp21CS1OnExchgTobacco = giLsp21CS1OnExchgTobacco;
	}
	public float getGiLsp21CS3OnExchgTobacco() {
		return giLsp21CS3OnExchgTobacco;
	}
	public float getGiLsp21CS2OnExchgTobacco() {
		return giLsp21CS2OnExchgTobacco;
	}
	public void setGiLsp21CS2OnExchgTobacco(float giLsp21CS2OnExchgTobacco) {
		this.giLsp21CS2OnExchgTobacco = giLsp21CS2OnExchgTobacco;
	}
	public void setGiLsp21CS3OnExchgTobacco(float giLsp21CS3OnExchgTobacco) {
		this.giLsp21CS3OnExchgTobacco = giLsp21CS3OnExchgTobacco;
	}
	public float getGiLsp21CS4OnExchgTobacco() {
		return giLsp21CS4OnExchgTobacco;
	}
	public void setGiLsp21CS4OnExchgTobacco(float giLsp21CS4OnExchgTobacco) {
		this.giLsp21CS4OnExchgTobacco = giLsp21CS4OnExchgTobacco;
	}
	public float getGiLsp21CS5OnExchgTobacco() {
		return giLsp21CS5OnExchgTobacco;
	}
	public void setGiLsp21CS5OnExchgTobacco(float giLsp21CS5OnExchgTobacco) {
		this.giLsp21CS5OnExchgTobacco = giLsp21CS5OnExchgTobacco;
	}
	public float getGiLsp21CS6OnExchgTobacco() {
		return giLsp21CS6OnExchgTobacco;
	}
	public void setGiLsp21CS6OnExchgTobacco(float giLsp21CS6OnExchgTobacco) {
		this.giLsp21CS6OnExchgTobacco = giLsp21CS6OnExchgTobacco;
	}

	@Field private float giTobaccoRatioLbpOffExchg40;
	@Field private float giTobaccoRatioLbpOffExchg41;
	@Field private float giTobaccoRatioLbpOffExchg42;
	@Field private float giTobaccoRatioLbpOffExchg43;
	@Field private float giTobaccoRatioLbpOffExchg44;
	@Field private float giTobaccoRatioLbpOffExchg45;
	@Field private float giTobaccoRatioLbpOffExchg46;
	@Field private float giTobaccoRatioLbpOffExchg47;
	@Field private float giTobaccoRatioLbpOffExchg48;
	@Field private float giTobaccoRatioLbpOffExchg49;
	@Field private float giTobaccoRatioLbpOffExchg50;
	@Field private float giTobaccoRatioLbpOffExchg51;
	@Field private float giTobaccoRatioLbpOffExchg52;
	@Field private float giTobaccoRatioLbpOffExchg53;
	@Field private float giTobaccoRatioLbpOffExchg54;
	@Field private float giTobaccoRatioLbpOffExchg55;
	@Field private float giTobaccoRatioLbpOffExchg56;
	@Field private float giTobaccoRatioLbpOffExchg57;
	@Field private float giTobaccoRatioLbpOffExchg58;
	@Field private float giTobaccoRatioLbpOffExchg59;
	@Field private float giTobaccoRatioLbpOffExchg60;
	@Field private float giTobaccoRatioLbpOffExchg61;
	@Field private float giTobaccoRatioLbpOffExchg62;
	@Field private float giTobaccoRatioLbpOffExchg63;
	@Field private float giTobaccoRatioLbpOffExchg64;
	@Field private int totalNoOfcarriers;
	@Field private int totalNoOfplans;
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public float getSlsp21() {
		return slsp21;
	}
	public void setSlsp21(float slsp21) {
		this.slsp21 = slsp21;
	}
	public float getLsp21() {
		return lsp21;
	}
	public void setLsp21(float lsp21) {
		this.lsp21 = lsp21;
	}
	public float getLbp21() {
		return lbp21;
	}
	public void setLbp21(float lbp21) {
		this.lbp21 = lbp21;
	}
	public String getGiOnExchgPlanAvailability() {
		return giOnExchgPlanAvailability;
	}
	public void setGiOnExchgPlanAvailability(String giOnExchgPlanAvailability) {
		this.giOnExchgPlanAvailability = giOnExchgPlanAvailability;
	}
	public String getGiOffExchgPlanAvailability() {
		return giOffExchgPlanAvailability;
	}
	public void setGiOffExchgPlanAvailability(String giOffExchgPlanAvailability) {
		this.giOffExchgPlanAvailability = giOffExchgPlanAvailability;
	}
	
	public float getGiTobaccoRatioLspOnExchg21() {
		return giTobaccoRatioLspOnExchg21;
	}
	public void setGiTobaccoRatioLspOnExchg21(float giTobaccoRatioLspOnExchg21) {
		this.giTobaccoRatioLspOnExchg21 = giTobaccoRatioLspOnExchg21;
	}
	public float getGiTobaccoRatioLspOnExchg22() {
		return giTobaccoRatioLspOnExchg22;
	}
	public void setGiTobaccoRatioLspOnExchg22(float giTobaccoRatioLspOnExchg22) {
		this.giTobaccoRatioLspOnExchg22 = giTobaccoRatioLspOnExchg22;
	}
	public float getGiTobaccoRatioLspOnExchg23() {
		return giTobaccoRatioLspOnExchg23;
	}
	public void setGiTobaccoRatioLspOnExchg23(float giTobaccoRatioLspOnExchg23) {
		this.giTobaccoRatioLspOnExchg23 = giTobaccoRatioLspOnExchg23;
	}
	public float getGiTobaccoRatioLspOnExchg24() {
		return giTobaccoRatioLspOnExchg24;
	}
	public void setGiTobaccoRatioLspOnExchg24(float giTobaccoRatioLspOnExchg24) {
		this.giTobaccoRatioLspOnExchg24 = giTobaccoRatioLspOnExchg24;
	}
	public float getGiTobaccoRatioLspOnExchg25() {
		return giTobaccoRatioLspOnExchg25;
	}
	public void setGiTobaccoRatioLspOnExchg25(float giTobaccoRatioLspOnExchg25) {
		this.giTobaccoRatioLspOnExchg25 = giTobaccoRatioLspOnExchg25;
	}
	public float getGiTobaccoRatioLspOnExchg26() {
		return giTobaccoRatioLspOnExchg26;
	}
	public void setGiTobaccoRatioLspOnExchg26(float giTobaccoRatioLspOnExchg26) {
		this.giTobaccoRatioLspOnExchg26 = giTobaccoRatioLspOnExchg26;
	}
	public float getGiTobaccoRatioLspOnExchg27() {
		return giTobaccoRatioLspOnExchg27;
	}
	public void setGiTobaccoRatioLspOnExchg27(float giTobaccoRatioLspOnExchg27) {
		this.giTobaccoRatioLspOnExchg27 = giTobaccoRatioLspOnExchg27;
	}
	public float getGiTobaccoRatioLspOnExchg28() {
		return giTobaccoRatioLspOnExchg28;
	}
	public void setGiTobaccoRatioLspOnExchg28(float giTobaccoRatioLspOnExchg28) {
		this.giTobaccoRatioLspOnExchg28 = giTobaccoRatioLspOnExchg28;
	}
	public float getGiTobaccoRatioLspOnExchg29() {
		return giTobaccoRatioLspOnExchg29;
	}
	public void setGiTobaccoRatioLspOnExchg29(float giTobaccoRatioLspOnExchg29) {
		this.giTobaccoRatioLspOnExchg29 = giTobaccoRatioLspOnExchg29;
	}
	public float getGiTobaccoRatioLspOnExchg30() {
		return giTobaccoRatioLspOnExchg30;
	}
	public void setGiTobaccoRatioLspOnExchg30(float giTobaccoRatioLspOnExchg30) {
		this.giTobaccoRatioLspOnExchg30 = giTobaccoRatioLspOnExchg30;
	}
	public float getGiTobaccoRatioLspOnExchg31() {
		return giTobaccoRatioLspOnExchg31;
	}
	public void setGiTobaccoRatioLspOnExchg31(float giTobaccoRatioLspOnExchg31) {
		this.giTobaccoRatioLspOnExchg31 = giTobaccoRatioLspOnExchg31;
	}
	public float getGiTobaccoRatioLspOnExchg32() {
		return giTobaccoRatioLspOnExchg32;
	}
	public void setGiTobaccoRatioLspOnExchg32(float giTobaccoRatioLspOnExchg32) {
		this.giTobaccoRatioLspOnExchg32 = giTobaccoRatioLspOnExchg32;
	}
	public float getGiTobaccoRatioLspOnExchg33() {
		return giTobaccoRatioLspOnExchg33;
	}
	public void setGiTobaccoRatioLspOnExchg33(float giTobaccoRatioLspOnExchg33) {
		this.giTobaccoRatioLspOnExchg33 = giTobaccoRatioLspOnExchg33;
	}
	public float getGiTobaccoRatioLspOnExchg34() {
		return giTobaccoRatioLspOnExchg34;
	}
	public void setGiTobaccoRatioLspOnExchg34(float giTobaccoRatioLspOnExchg34) {
		this.giTobaccoRatioLspOnExchg34 = giTobaccoRatioLspOnExchg34;
	}
	public float getGiTobaccoRatioLspOnExchg35() {
		return giTobaccoRatioLspOnExchg35;
	}
	public void setGiTobaccoRatioLspOnExchg35(float giTobaccoRatioLspOnExchg35) {
		this.giTobaccoRatioLspOnExchg35 = giTobaccoRatioLspOnExchg35;
	}
	public float getGiTobaccoRatioLspOnExchg36() {
		return giTobaccoRatioLspOnExchg36;
	}
	public void setGiTobaccoRatioLspOnExchg36(float giTobaccoRatioLspOnExchg36) {
		this.giTobaccoRatioLspOnExchg36 = giTobaccoRatioLspOnExchg36;
	}
	public float getGiTobaccoRatioLspOnExchg37() {
		return giTobaccoRatioLspOnExchg37;
	}
	public void setGiTobaccoRatioLspOnExchg37(float giTobaccoRatioLspOnExchg37) {
		this.giTobaccoRatioLspOnExchg37 = giTobaccoRatioLspOnExchg37;
	}
	public float getGiTobaccoRatioLspOnExchg38() {
		return giTobaccoRatioLspOnExchg38;
	}
	public void setGiTobaccoRatioLspOnExchg38(float giTobaccoRatioLspOnExchg38) {
		this.giTobaccoRatioLspOnExchg38 = giTobaccoRatioLspOnExchg38;
	}
	public float getGiTobaccoRatioLspOnExchg39() {
		return giTobaccoRatioLspOnExchg39;
	}
	public void setGiTobaccoRatioLspOnExchg39(float giTobaccoRatioLspOnExchg39) {
		this.giTobaccoRatioLspOnExchg39 = giTobaccoRatioLspOnExchg39;
	}
	public float getGiTobaccoRatioLspOnExchg40() {
		return giTobaccoRatioLspOnExchg40;
	}
	public void setGiTobaccoRatioLspOnExchg40(float giTobaccoRatioLspOnExchg40) {
		this.giTobaccoRatioLspOnExchg40 = giTobaccoRatioLspOnExchg40;
	}
	public float getGiTobaccoRatioLspOnExchg41() {
		return giTobaccoRatioLspOnExchg41;
	}
	public void setGiTobaccoRatioLspOnExchg41(float giTobaccoRatioLspOnExchg41) {
		this.giTobaccoRatioLspOnExchg41 = giTobaccoRatioLspOnExchg41;
	}
	public float getGiTobaccoRatioLspOnExchg42() {
		return giTobaccoRatioLspOnExchg42;
	}
	public void setGiTobaccoRatioLspOnExchg42(float giTobaccoRatioLspOnExchg42) {
		this.giTobaccoRatioLspOnExchg42 = giTobaccoRatioLspOnExchg42;
	}
	public float getGiTobaccoRatioLspOnExchg43() {
		return giTobaccoRatioLspOnExchg43;
	}
	public void setGiTobaccoRatioLspOnExchg43(float giTobaccoRatioLspOnExchg43) {
		this.giTobaccoRatioLspOnExchg43 = giTobaccoRatioLspOnExchg43;
	}
	public float getGiTobaccoRatioLspOnExchg44() {
		return giTobaccoRatioLspOnExchg44;
	}
	public void setGiTobaccoRatioLspOnExchg44(float giTobaccoRatioLspOnExchg44) {
		this.giTobaccoRatioLspOnExchg44 = giTobaccoRatioLspOnExchg44;
	}
	public float getGiTobaccoRatioLspOnExchg45() {
		return giTobaccoRatioLspOnExchg45;
	}
	public void setGiTobaccoRatioLspOnExchg45(float giTobaccoRatioLspOnExchg45) {
		this.giTobaccoRatioLspOnExchg45 = giTobaccoRatioLspOnExchg45;
	}
	public float getGiTobaccoRatioLspOnExchg46() {
		return giTobaccoRatioLspOnExchg46;
	}
	public void setGiTobaccoRatioLspOnExchg46(float giTobaccoRatioLspOnExchg46) {
		this.giTobaccoRatioLspOnExchg46 = giTobaccoRatioLspOnExchg46;
	}
	public float getGiTobaccoRatioLspOnExchg47() {
		return giTobaccoRatioLspOnExchg47;
	}
	public void setGiTobaccoRatioLspOnExchg47(float giTobaccoRatioLspOnExchg47) {
		this.giTobaccoRatioLspOnExchg47 = giTobaccoRatioLspOnExchg47;
	}
	public float getGiTobaccoRatioLspOnExchg48() {
		return giTobaccoRatioLspOnExchg48;
	}
	public void setGiTobaccoRatioLspOnExchg48(float giTobaccoRatioLspOnExchg48) {
		this.giTobaccoRatioLspOnExchg48 = giTobaccoRatioLspOnExchg48;
	}
	public float getGiTobaccoRatioLspOnExchg49() {
		return giTobaccoRatioLspOnExchg49;
	}
	public void setGiTobaccoRatioLspOnExchg49(float giTobaccoRatioLspOnExchg49) {
		this.giTobaccoRatioLspOnExchg49 = giTobaccoRatioLspOnExchg49;
	}
	public float getGiTobaccoRatioLspOnExchg50() {
		return giTobaccoRatioLspOnExchg50;
	}
	public void setGiTobaccoRatioLspOnExchg50(float giTobaccoRatioLspOnExchg50) {
		this.giTobaccoRatioLspOnExchg50 = giTobaccoRatioLspOnExchg50;
	}
	public float getGiTobaccoRatioLspOnExchg51() {
		return giTobaccoRatioLspOnExchg51;
	}
	public void setGiTobaccoRatioLspOnExchg51(float giTobaccoRatioLspOnExchg51) {
		this.giTobaccoRatioLspOnExchg51 = giTobaccoRatioLspOnExchg51;
	}
	public float getGiTobaccoRatioLspOnExchg52() {
		return giTobaccoRatioLspOnExchg52;
	}
	public void setGiTobaccoRatioLspOnExchg52(float giTobaccoRatioLspOnExchg52) {
		this.giTobaccoRatioLspOnExchg52 = giTobaccoRatioLspOnExchg52;
	}
	public float getGiTobaccoRatioLspOnExchg53() {
		return giTobaccoRatioLspOnExchg53;
	}
	public void setGiTobaccoRatioLspOnExchg53(float giTobaccoRatioLspOnExchg53) {
		this.giTobaccoRatioLspOnExchg53 = giTobaccoRatioLspOnExchg53;
	}
	public float getGiTobaccoRatioLspOnExchg54() {
		return giTobaccoRatioLspOnExchg54;
	}
	public void setGiTobaccoRatioLspOnExchg54(float giTobaccoRatioLspOnExchg54) {
		this.giTobaccoRatioLspOnExchg54 = giTobaccoRatioLspOnExchg54;
	}
	public float getGiTobaccoRatioLspOnExchg55() {
		return giTobaccoRatioLspOnExchg55;
	}
	public void setGiTobaccoRatioLspOnExchg55(float giTobaccoRatioLspOnExchg55) {
		this.giTobaccoRatioLspOnExchg55 = giTobaccoRatioLspOnExchg55;
	}
	public float getGiTobaccoRatioLspOnExchg56() {
		return giTobaccoRatioLspOnExchg56;
	}
	public void setGiTobaccoRatioLspOnExchg56(float giTobaccoRatioLspOnExchg56) {
		this.giTobaccoRatioLspOnExchg56 = giTobaccoRatioLspOnExchg56;
	}
	public float getGiTobaccoRatioLspOnExchg57() {
		return giTobaccoRatioLspOnExchg57;
	}
	public void setGiTobaccoRatioLspOnExchg57(float giTobaccoRatioLspOnExchg57) {
		this.giTobaccoRatioLspOnExchg57 = giTobaccoRatioLspOnExchg57;
	}
	public float getGiTobaccoRatioLspOnExchg58() {
		return giTobaccoRatioLspOnExchg58;
	}
	public void setGiTobaccoRatioLspOnExchg58(float giTobaccoRatioLspOnExchg58) {
		this.giTobaccoRatioLspOnExchg58 = giTobaccoRatioLspOnExchg58;
	}
	public float getGiTobaccoRatioLspOnExchg59() {
		return giTobaccoRatioLspOnExchg59;
	}
	public void setGiTobaccoRatioLspOnExchg59(float giTobaccoRatioLspOnExchg59) {
		this.giTobaccoRatioLspOnExchg59 = giTobaccoRatioLspOnExchg59;
	}
	public float getGiTobaccoRatioLspOnExchg60() {
		return giTobaccoRatioLspOnExchg60;
	}
	public void setGiTobaccoRatioLspOnExchg60(float giTobaccoRatioLspOnExchg60) {
		this.giTobaccoRatioLspOnExchg60 = giTobaccoRatioLspOnExchg60;
	}
	public float getGiTobaccoRatioLspOnExchg61() {
		return giTobaccoRatioLspOnExchg61;
	}
	public void setGiTobaccoRatioLspOnExchg61(float giTobaccoRatioLspOnExchg61) {
		this.giTobaccoRatioLspOnExchg61 = giTobaccoRatioLspOnExchg61;
	}
	public float getGiTobaccoRatioLspOnExchg62() {
		return giTobaccoRatioLspOnExchg62;
	}
	public void setGiTobaccoRatioLspOnExchg62(float giTobaccoRatioLspOnExchg62) {
		this.giTobaccoRatioLspOnExchg62 = giTobaccoRatioLspOnExchg62;
	}
	public float getGiTobaccoRatioLspOnExchg63() {
		return giTobaccoRatioLspOnExchg63;
	}
	public void setGiTobaccoRatioLspOnExchg63(float giTobaccoRatioLspOnExchg63) {
		this.giTobaccoRatioLspOnExchg63 = giTobaccoRatioLspOnExchg63;
	}
	public float getGiTobaccoRatioLspOnExchg64() {
		return giTobaccoRatioLspOnExchg64;
	}
	public void setGiTobaccoRatioLspOnExchg64(float giTobaccoRatioLspOnExchg64) {
		this.giTobaccoRatioLspOnExchg64 = giTobaccoRatioLspOnExchg64;
	}
	public float getGiLbp21OnExchg() {
		return giLbp21OnExchg;
	}
	public void setGiLbp21OnExchg(float giLbp21OnExchg) {
		this.giLbp21OnExchg = giLbp21OnExchg;
	}
	public float getGiLbp21OnExchgTobacco() {
		return giLbp21OnExchgTobacco;
	}
	public void setGiLbp21OnExchgTobacco(float giLbp21OnExchgTobacco) {
		this.giLbp21OnExchgTobacco = giLbp21OnExchgTobacco;
	}
	public float getGiTobaccoRatioLbpOnExchg21() {
		return giTobaccoRatioLbpOnExchg21;
	}
	public void setGiTobaccoRatioLbpOnExchg21(float giTobaccoRatioLbpOnExchg21) {
		this.giTobaccoRatioLbpOnExchg21 = giTobaccoRatioLbpOnExchg21;
	}
	public float getGiTobaccoRatioLbpOnExchg22() {
		return giTobaccoRatioLbpOnExchg22;
	}
	public void setGiTobaccoRatioLbpOnExchg22(float giTobaccoRatioLbpOnExchg22) {
		this.giTobaccoRatioLbpOnExchg22 = giTobaccoRatioLbpOnExchg22;
	}
	public float getGiTobaccoRatioLbpOnExchg23() {
		return giTobaccoRatioLbpOnExchg23;
	}
	public void setGiTobaccoRatioLbpOnExchg23(float giTobaccoRatioLbpOnExchg23) {
		this.giTobaccoRatioLbpOnExchg23 = giTobaccoRatioLbpOnExchg23;
	}
	public float getGiTobaccoRatioLbpOnExchg24() {
		return giTobaccoRatioLbpOnExchg24;
	}
	public void setGiTobaccoRatioLbpOnExchg24(float giTobaccoRatioLbpOnExchg24) {
		this.giTobaccoRatioLbpOnExchg24 = giTobaccoRatioLbpOnExchg24;
	}
	public float getGiTobaccoRatioLbpOnExchg25() {
		return giTobaccoRatioLbpOnExchg25;
	}
	public void setGiTobaccoRatioLbpOnExchg25(float giTobaccoRatioLbpOnExchg25) {
		this.giTobaccoRatioLbpOnExchg25 = giTobaccoRatioLbpOnExchg25;
	}
	public float getGiTobaccoRatioLbpOnExchg26() {
		return giTobaccoRatioLbpOnExchg26;
	}
	public void setGiTobaccoRatioLbpOnExchg26(float giTobaccoRatioLbpOnExchg26) {
		this.giTobaccoRatioLbpOnExchg26 = giTobaccoRatioLbpOnExchg26;
	}
	public float getGiTobaccoRatioLbpOnExchg27() {
		return giTobaccoRatioLbpOnExchg27;
	}
	public void setGiTobaccoRatioLbpOnExchg27(float giTobaccoRatioLbpOnExchg27) {
		this.giTobaccoRatioLbpOnExchg27 = giTobaccoRatioLbpOnExchg27;
	}
	public float getGiTobaccoRatioLbpOnExchg28() {
		return giTobaccoRatioLbpOnExchg28;
	}
	public void setGiTobaccoRatioLbpOnExchg28(float giTobaccoRatioLbpOnExchg28) {
		this.giTobaccoRatioLbpOnExchg28 = giTobaccoRatioLbpOnExchg28;
	}
	public float getGiTobaccoRatioLbpOnExchg29() {
		return giTobaccoRatioLbpOnExchg29;
	}
	public void setGiTobaccoRatioLbpOnExchg29(float giTobaccoRatioLbpOnExchg29) {
		this.giTobaccoRatioLbpOnExchg29 = giTobaccoRatioLbpOnExchg29;
	}
	public float getGiTobaccoRatioLbpOnExchg30() {
		return giTobaccoRatioLbpOnExchg30;
	}
	public void setGiTobaccoRatioLbpOnExchg30(float giTobaccoRatioLbpOnExchg30) {
		this.giTobaccoRatioLbpOnExchg30 = giTobaccoRatioLbpOnExchg30;
	}
	public float getGiTobaccoRatioLbpOnExchg31() {
		return giTobaccoRatioLbpOnExchg31;
	}
	public void setGiTobaccoRatioLbpOnExchg31(float giTobaccoRatioLbpOnExchg31) {
		this.giTobaccoRatioLbpOnExchg31 = giTobaccoRatioLbpOnExchg31;
	}
	public float getGiTobaccoRatioLbpOnExchg32() {
		return giTobaccoRatioLbpOnExchg32;
	}
	public void setGiTobaccoRatioLbpOnExchg32(float giTobaccoRatioLbpOnExchg32) {
		this.giTobaccoRatioLbpOnExchg32 = giTobaccoRatioLbpOnExchg32;
	}
	public float getGiTobaccoRatioLbpOnExchg33() {
		return giTobaccoRatioLbpOnExchg33;
	}
	public void setGiTobaccoRatioLbpOnExchg33(float giTobaccoRatioLbpOnExchg33) {
		this.giTobaccoRatioLbpOnExchg33 = giTobaccoRatioLbpOnExchg33;
	}
	public float getGiTobaccoRatioLbpOnExchg34() {
		return giTobaccoRatioLbpOnExchg34;
	}
	public void setGiTobaccoRatioLbpOnExchg34(float giTobaccoRatioLbpOnExchg34) {
		this.giTobaccoRatioLbpOnExchg34 = giTobaccoRatioLbpOnExchg34;
	}
	public float getGiTobaccoRatioLbpOnExchg35() {
		return giTobaccoRatioLbpOnExchg35;
	}
	public void setGiTobaccoRatioLbpOnExchg35(float giTobaccoRatioLbpOnExchg35) {
		this.giTobaccoRatioLbpOnExchg35 = giTobaccoRatioLbpOnExchg35;
	}
	public float getGiTobaccoRatioLbpOnExchg36() {
		return giTobaccoRatioLbpOnExchg36;
	}
	public void setGiTobaccoRatioLbpOnExchg36(float giTobaccoRatioLbpOnExchg36) {
		this.giTobaccoRatioLbpOnExchg36 = giTobaccoRatioLbpOnExchg36;
	}
	public float getGiTobaccoRatioLbpOnExchg37() {
		return giTobaccoRatioLbpOnExchg37;
	}
	public void setGiTobaccoRatioLbpOnExchg37(float giTobaccoRatioLbpOnExchg37) {
		this.giTobaccoRatioLbpOnExchg37 = giTobaccoRatioLbpOnExchg37;
	}
	public float getGiTobaccoRatioLbpOnExchg38() {
		return giTobaccoRatioLbpOnExchg38;
	}
	public void setGiTobaccoRatioLbpOnExchg38(float giTobaccoRatioLbpOnExchg38) {
		this.giTobaccoRatioLbpOnExchg38 = giTobaccoRatioLbpOnExchg38;
	}
	public float getGiTobaccoRatioLbpOnExchg39() {
		return giTobaccoRatioLbpOnExchg39;
	}
	public void setGiTobaccoRatioLbpOnExchg39(float giTobaccoRatioLbpOnExchg39) {
		this.giTobaccoRatioLbpOnExchg39 = giTobaccoRatioLbpOnExchg39;
	}
	public float getGiTobaccoRatioLbpOnExchg40() {
		return giTobaccoRatioLbpOnExchg40;
	}
	public void setGiTobaccoRatioLbpOnExchg40(float giTobaccoRatioLbpOnExchg40) {
		this.giTobaccoRatioLbpOnExchg40 = giTobaccoRatioLbpOnExchg40;
	}
	public float getGiTobaccoRatioLbpOnExchg41() {
		return giTobaccoRatioLbpOnExchg41;
	}
	public void setGiTobaccoRatioLbpOnExchg41(float giTobaccoRatioLbpOnExchg41) {
		this.giTobaccoRatioLbpOnExchg41 = giTobaccoRatioLbpOnExchg41;
	}
	public float getGiTobaccoRatioLbpOnExchg42() {
		return giTobaccoRatioLbpOnExchg42;
	}
	public void setGiTobaccoRatioLbpOnExchg42(float giTobaccoRatioLbpOnExchg42) {
		this.giTobaccoRatioLbpOnExchg42 = giTobaccoRatioLbpOnExchg42;
	}
	public float getGiTobaccoRatioLbpOnExchg43() {
		return giTobaccoRatioLbpOnExchg43;
	}
	public void setGiTobaccoRatioLbpOnExchg43(float giTobaccoRatioLbpOnExchg43) {
		this.giTobaccoRatioLbpOnExchg43 = giTobaccoRatioLbpOnExchg43;
	}
	public float getGiTobaccoRatioLbpOnExchg44() {
		return giTobaccoRatioLbpOnExchg44;
	}
	public void setGiTobaccoRatioLbpOnExchg44(float giTobaccoRatioLbpOnExchg44) {
		this.giTobaccoRatioLbpOnExchg44 = giTobaccoRatioLbpOnExchg44;
	}
	public float getGiTobaccoRatioLbpOnExchg45() {
		return giTobaccoRatioLbpOnExchg45;
	}
	public void setGiTobaccoRatioLbpOnExchg45(float giTobaccoRatioLbpOnExchg45) {
		this.giTobaccoRatioLbpOnExchg45 = giTobaccoRatioLbpOnExchg45;
	}
	public float getGiTobaccoRatioLbpOnExchg46() {
		return giTobaccoRatioLbpOnExchg46;
	}
	public void setGiTobaccoRatioLbpOnExchg46(float giTobaccoRatioLbpOnExchg46) {
		this.giTobaccoRatioLbpOnExchg46 = giTobaccoRatioLbpOnExchg46;
	}
	public float getGiTobaccoRatioLbpOnExchg47() {
		return giTobaccoRatioLbpOnExchg47;
	}
	public void setGiTobaccoRatioLbpOnExchg47(float giTobaccoRatioLbpOnExchg47) {
		this.giTobaccoRatioLbpOnExchg47 = giTobaccoRatioLbpOnExchg47;
	}
	public float getGiTobaccoRatioLbpOnExchg48() {
		return giTobaccoRatioLbpOnExchg48;
	}
	public void setGiTobaccoRatioLbpOnExchg48(float giTobaccoRatioLbpOnExchg48) {
		this.giTobaccoRatioLbpOnExchg48 = giTobaccoRatioLbpOnExchg48;
	}
	public float getGiTobaccoRatioLbpOnExchg49() {
		return giTobaccoRatioLbpOnExchg49;
	}
	public void setGiTobaccoRatioLbpOnExchg49(float giTobaccoRatioLbpOnExchg49) {
		this.giTobaccoRatioLbpOnExchg49 = giTobaccoRatioLbpOnExchg49;
	}
	public float getGiTobaccoRatioLbpOnExchg50() {
		return giTobaccoRatioLbpOnExchg50;
	}
	public void setGiTobaccoRatioLbpOnExchg50(float giTobaccoRatioLbpOnExchg50) {
		this.giTobaccoRatioLbpOnExchg50 = giTobaccoRatioLbpOnExchg50;
	}
	public float getGiTobaccoRatioLbpOnExchg51() {
		return giTobaccoRatioLbpOnExchg51;
	}
	public void setGiTobaccoRatioLbpOnExchg51(float giTobaccoRatioLbpOnExchg51) {
		this.giTobaccoRatioLbpOnExchg51 = giTobaccoRatioLbpOnExchg51;
	}
	public float getGiTobaccoRatioLbpOnExchg52() {
		return giTobaccoRatioLbpOnExchg52;
	}
	public void setGiTobaccoRatioLbpOnExchg52(float giTobaccoRatioLbpOnExchg52) {
		this.giTobaccoRatioLbpOnExchg52 = giTobaccoRatioLbpOnExchg52;
	}
	public float getGiTobaccoRatioLbpOnExchg53() {
		return giTobaccoRatioLbpOnExchg53;
	}
	public void setGiTobaccoRatioLbpOnExchg53(float giTobaccoRatioLbpOnExchg53) {
		this.giTobaccoRatioLbpOnExchg53 = giTobaccoRatioLbpOnExchg53;
	}
	public float getGiTobaccoRatioLbpOnExchg54() {
		return giTobaccoRatioLbpOnExchg54;
	}
	public void setGiTobaccoRatioLbpOnExchg54(float giTobaccoRatioLbpOnExchg54) {
		this.giTobaccoRatioLbpOnExchg54 = giTobaccoRatioLbpOnExchg54;
	}
	public float getGiTobaccoRatioLbpOnExchg55() {
		return giTobaccoRatioLbpOnExchg55;
	}
	public void setGiTobaccoRatioLbpOnExchg55(float giTobaccoRatioLbpOnExchg55) {
		this.giTobaccoRatioLbpOnExchg55 = giTobaccoRatioLbpOnExchg55;
	}
	public float getGiTobaccoRatioLbpOnExchg56() {
		return giTobaccoRatioLbpOnExchg56;
	}
	public void setGiTobaccoRatioLbpOnExchg56(float giTobaccoRatioLbpOnExchg56) {
		this.giTobaccoRatioLbpOnExchg56 = giTobaccoRatioLbpOnExchg56;
	}
	public float getGiTobaccoRatioLbpOnExchg57() {
		return giTobaccoRatioLbpOnExchg57;
	}
	public void setGiTobaccoRatioLbpOnExchg57(float giTobaccoRatioLbpOnExchg57) {
		this.giTobaccoRatioLbpOnExchg57 = giTobaccoRatioLbpOnExchg57;
	}
	public float getGiTobaccoRatioLbpOnExchg58() {
		return giTobaccoRatioLbpOnExchg58;
	}
	public void setGiTobaccoRatioLbpOnExchg58(float giTobaccoRatioLbpOnExchg58) {
		this.giTobaccoRatioLbpOnExchg58 = giTobaccoRatioLbpOnExchg58;
	}
	public float getGiTobaccoRatioLbpOnExchg59() {
		return giTobaccoRatioLbpOnExchg59;
	}
	public void setGiTobaccoRatioLbpOnExchg59(float giTobaccoRatioLbpOnExchg59) {
		this.giTobaccoRatioLbpOnExchg59 = giTobaccoRatioLbpOnExchg59;
	}
	public float getGiTobaccoRatioLbpOnExchg60() {
		return giTobaccoRatioLbpOnExchg60;
	}
	public void setGiTobaccoRatioLbpOnExchg60(float giTobaccoRatioLbpOnExchg60) {
		this.giTobaccoRatioLbpOnExchg60 = giTobaccoRatioLbpOnExchg60;
	}
	public float getGiTobaccoRatioLbpOnExchg61() {
		return giTobaccoRatioLbpOnExchg61;
	}
	public void setGiTobaccoRatioLbpOnExchg61(float giTobaccoRatioLbpOnExchg61) {
		this.giTobaccoRatioLbpOnExchg61 = giTobaccoRatioLbpOnExchg61;
	}
	public float getGiTobaccoRatioLbpOnExchg62() {
		return giTobaccoRatioLbpOnExchg62;
	}
	public void setGiTobaccoRatioLbpOnExchg62(float giTobaccoRatioLbpOnExchg62) {
		this.giTobaccoRatioLbpOnExchg62 = giTobaccoRatioLbpOnExchg62;
	}
	public float getGiTobaccoRatioLbpOnExchg63() {
		return giTobaccoRatioLbpOnExchg63;
	}
	public void setGiTobaccoRatioLbpOnExchg63(float giTobaccoRatioLbpOnExchg63) {
		this.giTobaccoRatioLbpOnExchg63 = giTobaccoRatioLbpOnExchg63;
	}
	public float getGiTobaccoRatioLbpOnExchg64() {
		return giTobaccoRatioLbpOnExchg64;
	}
	public void setGiTobaccoRatioLbpOnExchg64(float giTobaccoRatioLbpOnExchg64) {
		this.giTobaccoRatioLbpOnExchg64 = giTobaccoRatioLbpOnExchg64;
	}
	public float getGiLsp21OffExchg() {
		return giLsp21OffExchg;
	}
	public void setGiLsp21OffExchg(float giLsp21OffExchg) {
		this.giLsp21OffExchg = giLsp21OffExchg;
	}
	public float getGiLsp21OffExchgTobacco() {
		return giLsp21OffExchgTobacco;
	}
	public void setGiLsp21OffExchgTobacco(float giLsp21OffExchgTobacco) {
		this.giLsp21OffExchgTobacco = giLsp21OffExchgTobacco;
	}
	public float getGiTobaccoRatioLspOffExchg21() {
		return giTobaccoRatioLspOffExchg21;
	}
	public void setGiTobaccoRatioLspOffExchg21(float giTobaccoRatioLspOffExchg21) {
		this.giTobaccoRatioLspOffExchg21 = giTobaccoRatioLspOffExchg21;
	}
	public float getGiTobaccoRatioLspOffExchg22() {
		return giTobaccoRatioLspOffExchg22;
	}
	public void setGiTobaccoRatioLspOffExchg22(float giTobaccoRatioLspOffExchg22) {
		this.giTobaccoRatioLspOffExchg22 = giTobaccoRatioLspOffExchg22;
	}
	public float getGiTobaccoRatioLspOffExchg23() {
		return giTobaccoRatioLspOffExchg23;
	}
	public void setGiTobaccoRatioLspOffExchg23(float giTobaccoRatioLspOffExchg23) {
		this.giTobaccoRatioLspOffExchg23 = giTobaccoRatioLspOffExchg23;
	}
	public float getGiTobaccoRatioLspOffExchg24() {
		return giTobaccoRatioLspOffExchg24;
	}
	public void setGiTobaccoRatioLspOffExchg24(float giTobaccoRatioLspOffExchg24) {
		this.giTobaccoRatioLspOffExchg24 = giTobaccoRatioLspOffExchg24;
	}
	public float getGiTobaccoRatioLspOffExchg25() {
		return giTobaccoRatioLspOffExchg25;
	}
	public void setGiTobaccoRatioLspOffExchg25(float giTobaccoRatioLspOffExchg25) {
		this.giTobaccoRatioLspOffExchg25 = giTobaccoRatioLspOffExchg25;
	}
	public float getGiTobaccoRatioLspOffExchg26() {
		return giTobaccoRatioLspOffExchg26;
	}
	public void setGiTobaccoRatioLspOffExchg26(float giTobaccoRatioLspOffExchg26) {
		this.giTobaccoRatioLspOffExchg26 = giTobaccoRatioLspOffExchg26;
	}
	public float getGiTobaccoRatioLspOffExchg27() {
		return giTobaccoRatioLspOffExchg27;
	}
	public void setGiTobaccoRatioLspOffExchg27(float giTobaccoRatioLspOffExchg27) {
		this.giTobaccoRatioLspOffExchg27 = giTobaccoRatioLspOffExchg27;
	}
	public float getGiTobaccoRatioLspOffExchg28() {
		return giTobaccoRatioLspOffExchg28;
	}
	public void setGiTobaccoRatioLspOffExchg28(float giTobaccoRatioLspOffExchg28) {
		this.giTobaccoRatioLspOffExchg28 = giTobaccoRatioLspOffExchg28;
	}
	public float getGiTobaccoRatioLspOffExchg29() {
		return giTobaccoRatioLspOffExchg29;
	}
	public void setGiTobaccoRatioLspOffExchg29(float giTobaccoRatioLspOffExchg29) {
		this.giTobaccoRatioLspOffExchg29 = giTobaccoRatioLspOffExchg29;
	}
	public float getGiTobaccoRatioLspOffExchg30() {
		return giTobaccoRatioLspOffExchg30;
	}
	public void setGiTobaccoRatioLspOffExchg30(float giTobaccoRatioLspOffExchg30) {
		this.giTobaccoRatioLspOffExchg30 = giTobaccoRatioLspOffExchg30;
	}
	public float getGiTobaccoRatioLspOffExchg31() {
		return giTobaccoRatioLspOffExchg31;
	}
	public void setGiTobaccoRatioLspOffExchg31(float giTobaccoRatioLspOffExchg31) {
		this.giTobaccoRatioLspOffExchg31 = giTobaccoRatioLspOffExchg31;
	}
	public float getGiTobaccoRatioLspOffExchg32() {
		return giTobaccoRatioLspOffExchg32;
	}
	public void setGiTobaccoRatioLspOffExchg32(float giTobaccoRatioLspOffExchg32) {
		this.giTobaccoRatioLspOffExchg32 = giTobaccoRatioLspOffExchg32;
	}
	public float getGiTobaccoRatioLspOffExchg33() {
		return giTobaccoRatioLspOffExchg33;
	}
	public void setGiTobaccoRatioLspOffExchg33(float giTobaccoRatioLspOffExchg33) {
		this.giTobaccoRatioLspOffExchg33 = giTobaccoRatioLspOffExchg33;
	}
	public float getGiTobaccoRatioLspOffExchg34() {
		return giTobaccoRatioLspOffExchg34;
	}
	public void setGiTobaccoRatioLspOffExchg34(float giTobaccoRatioLspOffExchg34) {
		this.giTobaccoRatioLspOffExchg34 = giTobaccoRatioLspOffExchg34;
	}
	public float getGiTobaccoRatioLspOffExchg35() {
		return giTobaccoRatioLspOffExchg35;
	}
	public void setGiTobaccoRatioLspOffExchg35(float giTobaccoRatioLspOffExchg35) {
		this.giTobaccoRatioLspOffExchg35 = giTobaccoRatioLspOffExchg35;
	}
	public float getGiTobaccoRatioLspOffExchg36() {
		return giTobaccoRatioLspOffExchg36;
	}
	public void setGiTobaccoRatioLspOffExchg36(float giTobaccoRatioLspOffExchg36) {
		this.giTobaccoRatioLspOffExchg36 = giTobaccoRatioLspOffExchg36;
	}
	public float getGiTobaccoRatioLspOffExchg37() {
		return giTobaccoRatioLspOffExchg37;
	}
	public void setGiTobaccoRatioLspOffExchg37(float giTobaccoRatioLspOffExchg37) {
		this.giTobaccoRatioLspOffExchg37 = giTobaccoRatioLspOffExchg37;
	}
	public float getGiTobaccoRatioLspOffExchg38() {
		return giTobaccoRatioLspOffExchg38;
	}
	public void setGiTobaccoRatioLspOffExchg38(float giTobaccoRatioLspOffExchg38) {
		this.giTobaccoRatioLspOffExchg38 = giTobaccoRatioLspOffExchg38;
	}
	public float getGiTobaccoRatioLspOffExchg39() {
		return giTobaccoRatioLspOffExchg39;
	}
	public void setGiTobaccoRatioLspOffExchg39(float giTobaccoRatioLspOffExchg39) {
		this.giTobaccoRatioLspOffExchg39 = giTobaccoRatioLspOffExchg39;
	}
	public float getGiTobaccoRatioLspOffExchg40() {
		return giTobaccoRatioLspOffExchg40;
	}
	public void setGiTobaccoRatioLspOffExchg40(float giTobaccoRatioLspOffExchg40) {
		this.giTobaccoRatioLspOffExchg40 = giTobaccoRatioLspOffExchg40;
	}
	public float getGiTobaccoRatioLspOffExchg41() {
		return giTobaccoRatioLspOffExchg41;
	}
	public void setGiTobaccoRatioLspOffExchg41(float giTobaccoRatioLspOffExchg41) {
		this.giTobaccoRatioLspOffExchg41 = giTobaccoRatioLspOffExchg41;
	}
	public float getGiTobaccoRatioLspOffExchg42() {
		return giTobaccoRatioLspOffExchg42;
	}
	public void setGiTobaccoRatioLspOffExchg42(float giTobaccoRatioLspOffExchg42) {
		this.giTobaccoRatioLspOffExchg42 = giTobaccoRatioLspOffExchg42;
	}
	public float getGiTobaccoRatioLspOffExchg43() {
		return giTobaccoRatioLspOffExchg43;
	}
	public void setGiTobaccoRatioLspOffExchg43(float giTobaccoRatioLspOffExchg43) {
		this.giTobaccoRatioLspOffExchg43 = giTobaccoRatioLspOffExchg43;
	}
	public float getGiTobaccoRatioLspOffExchg44() {
		return giTobaccoRatioLspOffExchg44;
	}
	public void setGiTobaccoRatioLspOffExchg44(float giTobaccoRatioLspOffExchg44) {
		this.giTobaccoRatioLspOffExchg44 = giTobaccoRatioLspOffExchg44;
	}
	public float getGiTobaccoRatioLspOffExchg45() {
		return giTobaccoRatioLspOffExchg45;
	}
	public void setGiTobaccoRatioLspOffExchg45(float giTobaccoRatioLspOffExchg45) {
		this.giTobaccoRatioLspOffExchg45 = giTobaccoRatioLspOffExchg45;
	}
	public float getGiTobaccoRatioLspOffExchg46() {
		return giTobaccoRatioLspOffExchg46;
	}
	public void setGiTobaccoRatioLspOffExchg46(float giTobaccoRatioLspOffExchg46) {
		this.giTobaccoRatioLspOffExchg46 = giTobaccoRatioLspOffExchg46;
	}
	public float getGiTobaccoRatioLspOffExchg47() {
		return giTobaccoRatioLspOffExchg47;
	}
	public void setGiTobaccoRatioLspOffExchg47(float giTobaccoRatioLspOffExchg47) {
		this.giTobaccoRatioLspOffExchg47 = giTobaccoRatioLspOffExchg47;
	}
	public float getGiTobaccoRatioLspOffExchg48() {
		return giTobaccoRatioLspOffExchg48;
	}
	public void setGiTobaccoRatioLspOffExchg48(float giTobaccoRatioLspOffExchg48) {
		this.giTobaccoRatioLspOffExchg48 = giTobaccoRatioLspOffExchg48;
	}
	public float getGiTobaccoRatioLspOffExchg49() {
		return giTobaccoRatioLspOffExchg49;
	}
	public void setGiTobaccoRatioLspOffExchg49(float giTobaccoRatioLspOffExchg49) {
		this.giTobaccoRatioLspOffExchg49 = giTobaccoRatioLspOffExchg49;
	}
	public float getGiTobaccoRatioLspOffExchg50() {
		return giTobaccoRatioLspOffExchg50;
	}
	public void setGiTobaccoRatioLspOffExchg50(float giTobaccoRatioLspOffExchg50) {
		this.giTobaccoRatioLspOffExchg50 = giTobaccoRatioLspOffExchg50;
	}
	public float getGiTobaccoRatioLspOffExchg51() {
		return giTobaccoRatioLspOffExchg51;
	}
	public void setGiTobaccoRatioLspOffExchg51(float giTobaccoRatioLspOffExchg51) {
		this.giTobaccoRatioLspOffExchg51 = giTobaccoRatioLspOffExchg51;
	}
	public float getGiTobaccoRatioLspOffExchg52() {
		return giTobaccoRatioLspOffExchg52;
	}
	public void setGiTobaccoRatioLspOffExchg52(float giTobaccoRatioLspOffExchg52) {
		this.giTobaccoRatioLspOffExchg52 = giTobaccoRatioLspOffExchg52;
	}
	public float getGiTobaccoRatioLspOffExchg53() {
		return giTobaccoRatioLspOffExchg53;
	}
	public void setGiTobaccoRatioLspOffExchg53(float giTobaccoRatioLspOffExchg53) {
		this.giTobaccoRatioLspOffExchg53 = giTobaccoRatioLspOffExchg53;
	}
	public float getGiTobaccoRatioLspOffExchg54() {
		return giTobaccoRatioLspOffExchg54;
	}
	public void setGiTobaccoRatioLspOffExchg54(float giTobaccoRatioLspOffExchg54) {
		this.giTobaccoRatioLspOffExchg54 = giTobaccoRatioLspOffExchg54;
	}
	public float getGiTobaccoRatioLspOffExchg55() {
		return giTobaccoRatioLspOffExchg55;
	}
	public void setGiTobaccoRatioLspOffExchg55(float giTobaccoRatioLspOffExchg55) {
		this.giTobaccoRatioLspOffExchg55 = giTobaccoRatioLspOffExchg55;
	}
	public float getGiTobaccoRatioLspOffExchg56() {
		return giTobaccoRatioLspOffExchg56;
	}
	public void setGiTobaccoRatioLspOffExchg56(float giTobaccoRatioLspOffExchg56) {
		this.giTobaccoRatioLspOffExchg56 = giTobaccoRatioLspOffExchg56;
	}
	public float getGiTobaccoRatioLspOffExchg57() {
		return giTobaccoRatioLspOffExchg57;
	}
	public void setGiTobaccoRatioLspOffExchg57(float giTobaccoRatioLspOffExchg57) {
		this.giTobaccoRatioLspOffExchg57 = giTobaccoRatioLspOffExchg57;
	}
	public float getGiTobaccoRatioLspOffExchg58() {
		return giTobaccoRatioLspOffExchg58;
	}
	public void setGiTobaccoRatioLspOffExchg58(float giTobaccoRatioLspOffExchg58) {
		this.giTobaccoRatioLspOffExchg58 = giTobaccoRatioLspOffExchg58;
	}
	public float getGiTobaccoRatioLspOffExchg59() {
		return giTobaccoRatioLspOffExchg59;
	}
	public void setGiTobaccoRatioLspOffExchg59(float giTobaccoRatioLspOffExchg59) {
		this.giTobaccoRatioLspOffExchg59 = giTobaccoRatioLspOffExchg59;
	}
	public float getGiTobaccoRatioLspOffExchg60() {
		return giTobaccoRatioLspOffExchg60;
	}
	public void setGiTobaccoRatioLspOffExchg60(float giTobaccoRatioLspOffExchg60) {
		this.giTobaccoRatioLspOffExchg60 = giTobaccoRatioLspOffExchg60;
	}
	public float getGiTobaccoRatioLspOffExchg61() {
		return giTobaccoRatioLspOffExchg61;
	}
	public void setGiTobaccoRatioLspOffExchg61(float giTobaccoRatioLspOffExchg61) {
		this.giTobaccoRatioLspOffExchg61 = giTobaccoRatioLspOffExchg61;
	}
	public float getGiTobaccoRatioLspOffExchg62() {
		return giTobaccoRatioLspOffExchg62;
	}
	public void setGiTobaccoRatioLspOffExchg62(float giTobaccoRatioLspOffExchg62) {
		this.giTobaccoRatioLspOffExchg62 = giTobaccoRatioLspOffExchg62;
	}
	public float getGiTobaccoRatioLspOffExchg63() {
		return giTobaccoRatioLspOffExchg63;
	}
	public void setGiTobaccoRatioLspOffExchg63(float giTobaccoRatioLspOffExchg63) {
		this.giTobaccoRatioLspOffExchg63 = giTobaccoRatioLspOffExchg63;
	}
	public float getGiTobaccoRatioLspOffExchg64() {
		return giTobaccoRatioLspOffExchg64;
	}
	public void setGiTobaccoRatioLspOffExchg64(float giTobaccoRatioLspOffExchg64) {
		this.giTobaccoRatioLspOffExchg64 = giTobaccoRatioLspOffExchg64;
	}
	public float getGiLbp21OffExchg() {
		return giLbp21OffExchg;
	}
	public void setGiLbp21OffExchg(float giLbp21OffExchg) {
		this.giLbp21OffExchg = giLbp21OffExchg;
	}
	public float getGiLbp21OffExchgTobacco() {
		return giLbp21OffExchgTobacco;
	}
	public void setGiLbp21OffExchgTobacco(float giLbp21OffExchgTobacco) {
		this.giLbp21OffExchgTobacco = giLbp21OffExchgTobacco;
	}
	public float getGiTobaccoRatioLbpOffExchg21() {
		return giTobaccoRatioLbpOffExchg21;
	}
	public void setGiTobaccoRatioLbpOffExchg21(float giTobaccoRatioLbpOffExchg21) {
		this.giTobaccoRatioLbpOffExchg21 = giTobaccoRatioLbpOffExchg21;
	}
	public float getGiTobaccoRatioLbpOffExchg22() {
		return giTobaccoRatioLbpOffExchg22;
	}
	public void setGiTobaccoRatioLbpOffExchg22(float giTobaccoRatioLbpOffExchg22) {
		this.giTobaccoRatioLbpOffExchg22 = giTobaccoRatioLbpOffExchg22;
	}
	public float getGiTobaccoRatioLbpOffExchg23() {
		return giTobaccoRatioLbpOffExchg23;
	}
	public void setGiTobaccoRatioLbpOffExchg23(float giTobaccoRatioLbpOffExchg23) {
		this.giTobaccoRatioLbpOffExchg23 = giTobaccoRatioLbpOffExchg23;
	}
	public float getGiTobaccoRatioLbpOffExchg24() {
		return giTobaccoRatioLbpOffExchg24;
	}
	public void setGiTobaccoRatioLbpOffExchg24(float giTobaccoRatioLbpOffExchg24) {
		this.giTobaccoRatioLbpOffExchg24 = giTobaccoRatioLbpOffExchg24;
	}
	public float getGiTobaccoRatioLbpOffExchg25() {
		return giTobaccoRatioLbpOffExchg25;
	}
	public void setGiTobaccoRatioLbpOffExchg25(float giTobaccoRatioLbpOffExchg25) {
		this.giTobaccoRatioLbpOffExchg25 = giTobaccoRatioLbpOffExchg25;
	}
	public float getGiTobaccoRatioLbpOffExchg26() {
		return giTobaccoRatioLbpOffExchg26;
	}
	public void setGiTobaccoRatioLbpOffExchg26(float giTobaccoRatioLbpOffExchg26) {
		this.giTobaccoRatioLbpOffExchg26 = giTobaccoRatioLbpOffExchg26;
	}
	public float getGiTobaccoRatioLbpOffExchg27() {
		return giTobaccoRatioLbpOffExchg27;
	}
	public void setGiTobaccoRatioLbpOffExchg27(float giTobaccoRatioLbpOffExchg27) {
		this.giTobaccoRatioLbpOffExchg27 = giTobaccoRatioLbpOffExchg27;
	}
	public float getGiTobaccoRatioLbpOffExchg28() {
		return giTobaccoRatioLbpOffExchg28;
	}
	public void setGiTobaccoRatioLbpOffExchg28(float giTobaccoRatioLbpOffExchg28) {
		this.giTobaccoRatioLbpOffExchg28 = giTobaccoRatioLbpOffExchg28;
	}
	public float getGiTobaccoRatioLbpOffExchg29() {
		return giTobaccoRatioLbpOffExchg29;
	}
	public void setGiTobaccoRatioLbpOffExchg29(float giTobaccoRatioLbpOffExchg29) {
		this.giTobaccoRatioLbpOffExchg29 = giTobaccoRatioLbpOffExchg29;
	}
	public float getGiTobaccoRatioLbpOffExchg30() {
		return giTobaccoRatioLbpOffExchg30;
	}
	public void setGiTobaccoRatioLbpOffExchg30(float giTobaccoRatioLbpOffExchg30) {
		this.giTobaccoRatioLbpOffExchg30 = giTobaccoRatioLbpOffExchg30;
	}
	public float getGiTobaccoRatioLbpOffExchg31() {
		return giTobaccoRatioLbpOffExchg31;
	}
	public void setGiTobaccoRatioLbpOffExchg31(float giTobaccoRatioLbpOffExchg31) {
		this.giTobaccoRatioLbpOffExchg31 = giTobaccoRatioLbpOffExchg31;
	}
	public float getGiTobaccoRatioLbpOffExchg32() {
		return giTobaccoRatioLbpOffExchg32;
	}
	public void setGiTobaccoRatioLbpOffExchg32(float giTobaccoRatioLbpOffExchg32) {
		this.giTobaccoRatioLbpOffExchg32 = giTobaccoRatioLbpOffExchg32;
	}
	public float getGiTobaccoRatioLbpOffExchg33() {
		return giTobaccoRatioLbpOffExchg33;
	}
	public void setGiTobaccoRatioLbpOffExchg33(float giTobaccoRatioLbpOffExchg33) {
		this.giTobaccoRatioLbpOffExchg33 = giTobaccoRatioLbpOffExchg33;
	}
	public float getGiTobaccoRatioLbpOffExchg34() {
		return giTobaccoRatioLbpOffExchg34;
	}
	public void setGiTobaccoRatioLbpOffExchg34(float giTobaccoRatioLbpOffExchg34) {
		this.giTobaccoRatioLbpOffExchg34 = giTobaccoRatioLbpOffExchg34;
	}
	public float getGiTobaccoRatioLbpOffExchg35() {
		return giTobaccoRatioLbpOffExchg35;
	}
	public void setGiTobaccoRatioLbpOffExchg35(float giTobaccoRatioLbpOffExchg35) {
		this.giTobaccoRatioLbpOffExchg35 = giTobaccoRatioLbpOffExchg35;
	}
	public float getGiTobaccoRatioLbpOffExchg36() {
		return giTobaccoRatioLbpOffExchg36;
	}
	public void setGiTobaccoRatioLbpOffExchg36(float giTobaccoRatioLbpOffExchg36) {
		this.giTobaccoRatioLbpOffExchg36 = giTobaccoRatioLbpOffExchg36;
	}
	public float getGiTobaccoRatioLbpOffExchg37() {
		return giTobaccoRatioLbpOffExchg37;
	}
	public void setGiTobaccoRatioLbpOffExchg37(float giTobaccoRatioLbpOffExchg37) {
		this.giTobaccoRatioLbpOffExchg37 = giTobaccoRatioLbpOffExchg37;
	}
	public float getGiTobaccoRatioLbpOffExchg38() {
		return giTobaccoRatioLbpOffExchg38;
	}
	public void setGiTobaccoRatioLbpOffExchg38(float giTobaccoRatioLbpOffExchg38) {
		this.giTobaccoRatioLbpOffExchg38 = giTobaccoRatioLbpOffExchg38;
	}
	public float getGiTobaccoRatioLbpOffExchg39() {
		return giTobaccoRatioLbpOffExchg39;
	}
	public void setGiTobaccoRatioLbpOffExchg39(float giTobaccoRatioLbpOffExchg39) {
		this.giTobaccoRatioLbpOffExchg39 = giTobaccoRatioLbpOffExchg39;
	}
	public float getGiTobaccoRatioLbpOffExchg40() {
		return giTobaccoRatioLbpOffExchg40;
	}
	public void setGiTobaccoRatioLbpOffExchg40(float giTobaccoRatioLbpOffExchg40) {
		this.giTobaccoRatioLbpOffExchg40 = giTobaccoRatioLbpOffExchg40;
	}
	public float getGiTobaccoRatioLbpOffExchg41() {
		return giTobaccoRatioLbpOffExchg41;
	}
	public void setGiTobaccoRatioLbpOffExchg41(float giTobaccoRatioLbpOffExchg41) {
		this.giTobaccoRatioLbpOffExchg41 = giTobaccoRatioLbpOffExchg41;
	}
	public float getGiTobaccoRatioLbpOffExchg42() {
		return giTobaccoRatioLbpOffExchg42;
	}
	public void setGiTobaccoRatioLbpOffExchg42(float giTobaccoRatioLbpOffExchg42) {
		this.giTobaccoRatioLbpOffExchg42 = giTobaccoRatioLbpOffExchg42;
	}
	public float getGiTobaccoRatioLbpOffExchg43() {
		return giTobaccoRatioLbpOffExchg43;
	}
	public void setGiTobaccoRatioLbpOffExchg43(float giTobaccoRatioLbpOffExchg43) {
		this.giTobaccoRatioLbpOffExchg43 = giTobaccoRatioLbpOffExchg43;
	}
	public float getGiTobaccoRatioLbpOffExchg44() {
		return giTobaccoRatioLbpOffExchg44;
	}
	public void setGiTobaccoRatioLbpOffExchg44(float giTobaccoRatioLbpOffExchg44) {
		this.giTobaccoRatioLbpOffExchg44 = giTobaccoRatioLbpOffExchg44;
	}
	public float getGiTobaccoRatioLbpOffExchg45() {
		return giTobaccoRatioLbpOffExchg45;
	}
	public void setGiTobaccoRatioLbpOffExchg45(float giTobaccoRatioLbpOffExchg45) {
		this.giTobaccoRatioLbpOffExchg45 = giTobaccoRatioLbpOffExchg45;
	}
	public float getGiTobaccoRatioLbpOffExchg46() {
		return giTobaccoRatioLbpOffExchg46;
	}
	public void setGiTobaccoRatioLbpOffExchg46(float giTobaccoRatioLbpOffExchg46) {
		this.giTobaccoRatioLbpOffExchg46 = giTobaccoRatioLbpOffExchg46;
	}
	public float getGiTobaccoRatioLbpOffExchg47() {
		return giTobaccoRatioLbpOffExchg47;
	}
	public void setGiTobaccoRatioLbpOffExchg47(float giTobaccoRatioLbpOffExchg47) {
		this.giTobaccoRatioLbpOffExchg47 = giTobaccoRatioLbpOffExchg47;
	}
	public float getGiTobaccoRatioLbpOffExchg48() {
		return giTobaccoRatioLbpOffExchg48;
	}
	public void setGiTobaccoRatioLbpOffExchg48(float giTobaccoRatioLbpOffExchg48) {
		this.giTobaccoRatioLbpOffExchg48 = giTobaccoRatioLbpOffExchg48;
	}
	public float getGiTobaccoRatioLbpOffExchg49() {
		return giTobaccoRatioLbpOffExchg49;
	}
	public void setGiTobaccoRatioLbpOffExchg49(float giTobaccoRatioLbpOffExchg49) {
		this.giTobaccoRatioLbpOffExchg49 = giTobaccoRatioLbpOffExchg49;
	}
	public float getGiTobaccoRatioLbpOffExchg50() {
		return giTobaccoRatioLbpOffExchg50;
	}
	public void setGiTobaccoRatioLbpOffExchg50(float giTobaccoRatioLbpOffExchg50) {
		this.giTobaccoRatioLbpOffExchg50 = giTobaccoRatioLbpOffExchg50;
	}
	public float getGiTobaccoRatioLbpOffExchg51() {
		return giTobaccoRatioLbpOffExchg51;
	}
	public void setGiTobaccoRatioLbpOffExchg51(float giTobaccoRatioLbpOffExchg51) {
		this.giTobaccoRatioLbpOffExchg51 = giTobaccoRatioLbpOffExchg51;
	}
	public float getGiTobaccoRatioLbpOffExchg52() {
		return giTobaccoRatioLbpOffExchg52;
	}
	public void setGiTobaccoRatioLbpOffExchg52(float giTobaccoRatioLbpOffExchg52) {
		this.giTobaccoRatioLbpOffExchg52 = giTobaccoRatioLbpOffExchg52;
	}
	public float getGiTobaccoRatioLbpOffExchg53() {
		return giTobaccoRatioLbpOffExchg53;
	}
	public void setGiTobaccoRatioLbpOffExchg53(float giTobaccoRatioLbpOffExchg53) {
		this.giTobaccoRatioLbpOffExchg53 = giTobaccoRatioLbpOffExchg53;
	}
	public float getGiTobaccoRatioLbpOffExchg54() {
		return giTobaccoRatioLbpOffExchg54;
	}
	public void setGiTobaccoRatioLbpOffExchg54(float giTobaccoRatioLbpOffExchg54) {
		this.giTobaccoRatioLbpOffExchg54 = giTobaccoRatioLbpOffExchg54;
	}
	public float getGiTobaccoRatioLbpOffExchg55() {
		return giTobaccoRatioLbpOffExchg55;
	}
	public void setGiTobaccoRatioLbpOffExchg55(float giTobaccoRatioLbpOffExchg55) {
		this.giTobaccoRatioLbpOffExchg55 = giTobaccoRatioLbpOffExchg55;
	}
	public float getGiTobaccoRatioLbpOffExchg56() {
		return giTobaccoRatioLbpOffExchg56;
	}
	public void setGiTobaccoRatioLbpOffExchg56(float giTobaccoRatioLbpOffExchg56) {
		this.giTobaccoRatioLbpOffExchg56 = giTobaccoRatioLbpOffExchg56;
	}
	public float getGiTobaccoRatioLbpOffExchg57() {
		return giTobaccoRatioLbpOffExchg57;
	}
	public void setGiTobaccoRatioLbpOffExchg57(float giTobaccoRatioLbpOffExchg57) {
		this.giTobaccoRatioLbpOffExchg57 = giTobaccoRatioLbpOffExchg57;
	}
	public float getGiTobaccoRatioLbpOffExchg58() {
		return giTobaccoRatioLbpOffExchg58;
	}
	public void setGiTobaccoRatioLbpOffExchg58(float giTobaccoRatioLbpOffExchg58) {
		this.giTobaccoRatioLbpOffExchg58 = giTobaccoRatioLbpOffExchg58;
	}
	public float getGiTobaccoRatioLbpOffExchg59() {
		return giTobaccoRatioLbpOffExchg59;
	}
	public void setGiTobaccoRatioLbpOffExchg59(float giTobaccoRatioLbpOffExchg59) {
		this.giTobaccoRatioLbpOffExchg59 = giTobaccoRatioLbpOffExchg59;
	}
	public float getGiTobaccoRatioLbpOffExchg60() {
		return giTobaccoRatioLbpOffExchg60;
	}
	public void setGiTobaccoRatioLbpOffExchg60(float giTobaccoRatioLbpOffExchg60) {
		this.giTobaccoRatioLbpOffExchg60 = giTobaccoRatioLbpOffExchg60;
	}
	public float getGiTobaccoRatioLbpOffExchg61() {
		return giTobaccoRatioLbpOffExchg61;
	}
	public void setGiTobaccoRatioLbpOffExchg61(float giTobaccoRatioLbpOffExchg61) {
		this.giTobaccoRatioLbpOffExchg61 = giTobaccoRatioLbpOffExchg61;
	}
	public float getGiTobaccoRatioLbpOffExchg62() {
		return giTobaccoRatioLbpOffExchg62;
	}
	public void setGiTobaccoRatioLbpOffExchg62(float giTobaccoRatioLbpOffExchg62) {
		this.giTobaccoRatioLbpOffExchg62 = giTobaccoRatioLbpOffExchg62;
	}
	public float getGiTobaccoRatioLbpOffExchg63() {
		return giTobaccoRatioLbpOffExchg63;
	}
	public void setGiTobaccoRatioLbpOffExchg63(float giTobaccoRatioLbpOffExchg63) {
		this.giTobaccoRatioLbpOffExchg63 = giTobaccoRatioLbpOffExchg63;
	}
	public float getGiTobaccoRatioLbpOffExchg64() {
		return giTobaccoRatioLbpOffExchg64;
	}
	public void setGiTobaccoRatioLbpOffExchg64(float giTobaccoRatioLbpOffExchg64) {
		this.giTobaccoRatioLbpOffExchg64 = giTobaccoRatioLbpOffExchg64;
	}
	public int getTotalNoOfcarriers() {
		return totalNoOfcarriers;
	}
	public void setTotalNoOfcarriers(int totalNoOfcarriers) {
		this.totalNoOfcarriers = totalNoOfcarriers;
	}
	public int getTotalNoOfplans() {
		return totalNoOfplans;
	}
	public void setTotalNoOfplans(int totalNoOfplans) {
		this.totalNoOfplans = totalNoOfplans;
	}
	

	public void setFieldValues(Map<String, Object> fieldMap) {
		Set<Map.Entry<String, Object>> s = fieldMap.entrySet();
		Iterator<Map.Entry<String, Object>> cursor = s.iterator();
		Map.Entry<String, Object> me = null;
		while(cursor.hasNext()){
			me = cursor.next();
	//		this.setValue(me.getKey(), (String)me.getValue());
		}
	}

	
	
	
}
