
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * The persistent class for the bankinfo database table.
 * 
 */

@Audited
@TypeDef(name = "vimoEncryptedString",
typeClass = VimoEncryptedStringType.class)
@Entity
@Table(name="bank_info")
public class BankInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * This validation group provides groupings for fields  {@link #paymentMethodName}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditPaymentMethod extends Default{
		
	}
	
	public interface EditPaymentMethodBankInfo extends Default{
		
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BankInfo_Seq")
	@SequenceGenerator(name = "BankInfo_Seq", sequenceName = "bank_info_seq", allocationSize = 1)
	private int id;
	
	@NotEmpty(message="{err.bankName}",groups={BankInfo.EditPaymentMethod.class, BankInfo.EditPaymentMethodBankInfo.class})
	@Column(name="bank_name")
	private String bankName;
	
	@NotEmpty(message="{err.nameOnAccount}",groups={BankInfo.EditPaymentMethod.class, BankInfo.EditPaymentMethodBankInfo.class})
	@Column(name="name_on_account")
	private String nameOnAccount;
	
	@NotEmpty(message="{err.bankABARoutingNumber}",groups={BankInfo.EditPaymentMethod.class, BankInfo.EditPaymentMethodBankInfo.class})
	@Pattern(regexp="[0-9]{9}",message="{err.bankAcctNumber}",groups={BankInfo.EditPaymentMethod.class, BankInfo.EditPaymentMethodBankInfo.class})
	@Column(name="routing_number",length=9)
	private String routingNumber;

	@NotEmpty(message="{err.bankAcctNumber}",groups={BankInfo.EditPaymentMethod.class, BankInfo.EditPaymentMethodBankInfo.class})
	@Pattern(regexp="[0-9]*",message="{err.bankAcctNumber}",groups={BankInfo.EditPaymentMethodBankInfo.class})
	//@GreaterThanZero(groups={BankInfo.EditPaymentMethod.class})
	@Column(name="account_number")
	@Type(type = "vimoEncryptedString")
	private String accountNumber;

	@NotEmpty(message="{err.bankAcctType}",groups={BankInfo.EditPaymentMethod.class, BankInfo.EditPaymentMethodBankInfo.class})
	@Pattern(regexp="^C|S$",message="{err.bankAcctTypeProper}",groups={BankInfo.EditPaymentMethodBankInfo.class})
	@Column(name="account_type", length=1)
	private String accountType;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date created;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updated;
    
    @OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "bank_location_id")
    private Location bankLocation;
    
    @Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

    public BankInfo() {
    }
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getNameOnAccount() {
		return nameOnAccount;
	}

	public void setNameOnAccount(String nameOnAccount) {
		this.nameOnAccount = nameOnAccount;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Location getBankLocation() {
		return bankLocation;
	}

	public void setBankLocation(Location bankLocation) {
		this.bankLocation = bankLocation;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
//		this.setAccountNumber( encrypter.encrypt(this.accountNumber));
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
	
	public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("accountType=").append(accountType).append(", ");
        sb.append("bankName=").append(bankName).append(", ");
        sb.append("routingNumber=").append(routingNumber).append(", \n");
        sb.append("accountNumber=").append(accountNumber).append(", \n");
        sb.append("routingNumber=").append(routingNumber).append(" \n");
       
        return sb.toString();
    }
}
