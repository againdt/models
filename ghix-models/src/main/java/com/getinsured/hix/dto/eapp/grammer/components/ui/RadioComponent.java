package com.getinsured.hix.dto.eapp.grammer.components.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.getinsured.hix.dto.eapp.grammer.UIComponent;
import com.getinsured.hix.dto.eapp.grammer.UIOption;
import com.getinsured.hix.dto.eapp.grammer.UIValidationRule;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class RadioComponent extends UIComponent {

	private List<UIOption> options;
	
	public List<UIOption> getOptions() {
		return options;
	}

	public void setOptions(List<UIOption> options) {
		this.options = options;
	}

	@Override
	public String toString() {
		return "RadioComponent [options=" + options + " parent=" + super.toString() + "]";
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public UIComponent parse(String json) {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.fromJson(json, RadioComponent.class);
		
	}
	
	public static void main(String args[]){
		String s="{\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": [\"radio\"],\n \"inline\": true,\n \"width\": 3\n }";
		UIComponent comp = new RadioComponent();
		comp = comp.parse(s);
		System.out.println(comp);
	}

	@Override
	public String format() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}

	@Override
	public List<String> compile(String json) {
		decorate(json);
		List<String> errors = new ArrayList<String>();
		try {
			compileSummary(errors, UIValidationRule.REQUIRED, "label", this.label);
			compileSummary(errors, UIValidationRule.REQUIRED, "model", this.model);
			compileSummary(errors, UIValidationRule.REQUIRED, "type", this.type.name());
			compileSummary(errors, UIValidationRule.REQUIRED, "options", this.options); 
			
		}catch (Exception e) {
			e.printStackTrace();
			errors.add("Runtime error, contact dev");
		}
		return errors;
	}
	
	private void decorate(String json){
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		RadioComponent comp = gson.fromJson(json, RadioComponent.class);
		this.entity = comp.entity;
		this.label = comp.label;
		this.model = comp.model;
		this.type = comp.type;
		this.validation = comp.validation;
		this.options = comp.options;
	}

}
