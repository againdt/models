/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

/**
 * @author negi_s
 *
 */
public class EnrollmentConsumerPortalDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String logostr;
	private String issuerName;
	private String planName;
	private String insuranceType;
	private String enrollmentStatus;
	private String status;
	private String networkType;
	private String genericMedication;
	private String officeVisit;
	private String issuerSiteUrl;
	private Float monthlyPayment;
	private Float monthlyPremium;
	private Float aptcAmount;
	private Integer planId;
	private Integer enrollmentId;
	private Long ssapApplicationId;
	private Double indivDeductible;
	private Double familyDeductible;
	private Double individualOopMax;
	private Double familyOopMax;
	private int issuerId;
	
	private Float employeeContribution ;
	private Float employerContribution ;
	private String exchangeAssignPolicyNo;
	private String benefitEffectiveDateStr ;
	private Date benefitEffectiveDate ;
	private Date benefitEndDate ;
	private String enrolleeNames;
	private String companyLogo;
	private Long d2cEnrollmentId;
	
	private String carrierAppId;
	private String issuerAssignPolicyNo;
	private String benefitEffectiveDateStrWithOutSlashes;
	private String enrollmentModality;
	
	public String getLogostr() {
		return logostr;
	}
	public void setLogostr(String logostr) {
		this.logostr = logostr;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getMonthlyPayment() {
		return monthlyPayment;
	}
	public void setMonthlyPayment(Float monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}
	public Float getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(Float monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	
	
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getEnrollmentModality() {
		return enrollmentModality;
	}

	public void setEnrollmentModality(String enrollmentModality) {
		this.enrollmentModality = enrollmentModality;
	}

	public Double getIndivDeductible() {
		return indivDeductible;
	}
	public void setIndivDeductible(Double indivDeductible) {
		this.indivDeductible = indivDeductible;
	}
	public Double getFamilyDeductible() {
		return familyDeductible;
	}
	public void setFamilyDeductible(Double familyDeductible) {
		this.familyDeductible = familyDeductible;
	}
		
	public String getGenericMedication() {
		return genericMedication;
	}
	public void setGenericMedication(String genericMedication) {
		this.genericMedication = genericMedication;
	}
			
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public Double getIndividualOopMax() {
		return individualOopMax;
	}
	public void setIndividualOopMax(Double individualOopMax) {
		this.individualOopMax = individualOopMax;
	}
	public Double getFamilyOopMax() {
		return familyOopMax;
	}
	public void setFamilyOopMax(Double familyOopMax) {
		this.familyOopMax = familyOopMax;
	}
	public Float getAptcAmount() {
		return aptcAmount;
	}
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	public String getIssuerSiteUrl() {
		return issuerSiteUrl;
	}
	public void setIssuerSiteUrl(String issuerSiteUrl) {
		this.issuerSiteUrl = issuerSiteUrl;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Float getEmployeeContribution() {
		return employeeContribution;
	}
	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}
	public Float getEmployerContribution() {
		return employerContribution;
	}
	public void setEmployerContribution(Float employerContribution) {
		this.employerContribution = employerContribution;
	}
	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}
	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public String getBenefitEffectiveDateStr() {
		return benefitEffectiveDateStr;
	}
	public void setBenefitEffectiveDateStr(String benefitEffectiveDateStr) {
		this.benefitEffectiveDateStr = benefitEffectiveDateStr;
	}
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public Date getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public String getEnrolleeNames() {
		return enrolleeNames;
	}
	public void setEnrolleeNames(String enrolleeNames) {
		this.enrolleeNames = enrolleeNames;
	}
	public String getCompanyLogo() {
		return companyLogo;
	}
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
	
	public Long getD2cEnrollmentId() {
		return d2cEnrollmentId;
	}
	public void setD2cEnrollmentId(Long d2cEnrollmentId) {
		this.d2cEnrollmentId = d2cEnrollmentId;
	}
	
	public String getCarrierAppId() {
		return carrierAppId;
	}
	public void setCarrierAppId(String carrierAppId) {
		this.carrierAppId = carrierAppId;
	}
	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}
	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}
	
	
	
	public String getBenefitEffectiveDateStrWithOutSlashes() {
		return benefitEffectiveDateStrWithOutSlashes;
	}
	public void setBenefitEffectiveDateStrWithOutSlashes(
			String benefitEffectiveDateStrWithOutSlashes) {
		this.benefitEffectiveDateStrWithOutSlashes = benefitEffectiveDateStrWithOutSlashes;
	}
	@Override
	public String toString() {
		return "EnrollmentConsumerPortalDTO [logostr=" + logostr
				+ ", issuerName=" + issuerName + ", planName=" + planName
				+ ", insuranceType=" + insuranceType + ", enrollmentStatus="
				+ enrollmentStatus + ", status=" + status + ", networkType="
				+ networkType + ", genericMedication=" + genericMedication
				+ ", officeVisit=" + officeVisit + ", issuerSiteUrl="
				+ issuerSiteUrl + ", monthlyPayment=" + monthlyPayment
				+ ", monthlyPremium=" + monthlyPremium + ", aptcAmount="
				+ aptcAmount + ", planId=" + planId + ", enrollmentId="
				+ enrollmentId + ", ssapApplicationId=" + ssapApplicationId
				+ ", indivDeductible=" + indivDeductible
				+ ", familyDeductible=" + familyDeductible
				+ ", individualOopMax=" + individualOopMax + ", familyOopMax="
				+ familyOopMax + ", issuerId=" + issuerId
				+ ", employeeContribution=" + employeeContribution
				+ ", employerContribution=" + employerContribution
				+ ", exchangeAssignPolicyNo=" + exchangeAssignPolicyNo
				+ ", benefitEffectiveDate=" + benefitEffectiveDate
				+ ", benefitEndDate=" + benefitEndDate + ", enrolleeNames="
				+ enrolleeNames + ", companyLogo=" + companyLogo + "]";
	}
	
			
}
