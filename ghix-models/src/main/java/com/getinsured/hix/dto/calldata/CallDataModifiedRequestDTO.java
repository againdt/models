package com.getinsured.hix.dto.calldata;

public class CallDataModifiedRequestDTO {

	private String interaction_id;
	private String client_id;
	private String client;
	private String program_id;
	private String program_name;
	private String campaign_id;
	private String campaign_name;
	private String in_outbound_dialout;
	private String dim_aff_affiliate_key;
	private String dim_aff_flow_key;
	private String phone_number;
	private String caller_ani;
	private String start_time;
	private String end_time;
	private String last_agent_id;
	private String last_agent_full_name;
	private String agent_count;
	private String ring_time;
	private String hold_length;
	private String queue_length_in_secs;
	private String caller_talk_time;
	private String agent_talk_time_seconds;
	private String after_call_work_time;
	private String call_length;
	private String agent_total_time_seconds;
	private String state_code;
	private String in_business_hours;
	private String is_voicemail;
	
	private boolean is_abandoned;
	private String last_segment_call_dispostion;
	private boolean is_external_transfer;
	
	public String getInteraction_id() {
		return interaction_id;
	}
	public void setInteraction_id(String interaction_id) {
		this.interaction_id = interaction_id;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getProgram_id() {
		return program_id;
	}
	public void setProgram_id(String program_id) {
		this.program_id = program_id;
	}
	public String getProgram_name() {
		return program_name;
	}
	public void setProgram_name(String program_name) {
		this.program_name = program_name;
	}
	public String getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}
	public String getCampaign_name() {
		return campaign_name;
	}
	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}
	public String getIn_outbound_dialout() {
		return in_outbound_dialout;
	}
	public void setIn_outbound_dialout(String in_outbound_dialout) {
		this.in_outbound_dialout = in_outbound_dialout;
	}
	public String getDim_aff_affiliate_key() {
		return dim_aff_affiliate_key;
	}
	public void setDim_aff_affiliate_key(String dim_aff_affiliate_key) {
		this.dim_aff_affiliate_key = dim_aff_affiliate_key;
	}
	public String getDim_aff_flow_key() {
		return dim_aff_flow_key;
	}
	public void setDim_aff_flow_key(String dim_aff_flow_key) {
		this.dim_aff_flow_key = dim_aff_flow_key;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getCaller_ani() {
		return caller_ani;
	}
	public void setCaller_ani(String caller_ani) {
		this.caller_ani = caller_ani;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getLast_agent_id() {
		return last_agent_id;
	}
	public void setLast_agent_id(String last_agent_id) {
		this.last_agent_id = last_agent_id;
	}
	public String getLast_agent_full_name() {
		return last_agent_full_name;
	}
	public void setLast_agent_full_name(String last_agent_full_name) {
		this.last_agent_full_name = last_agent_full_name;
	}
	public String getAgent_count() {
		return agent_count;
	}
	public void setAgent_count(String agent_count) {
		this.agent_count = agent_count;
	}
	public String getRing_time() {
		return ring_time;
	}
	public void setRing_time(String ring_time) {
		this.ring_time = ring_time;
	}
	public String getHold_length() {
		return hold_length;
	}
	public void setHold_length(String hold_length) {
		this.hold_length = hold_length;
	}
	public String getQueue_length_in_secs() {
		return queue_length_in_secs;
	}
	public void setQueue_length_in_secs(String queue_length_in_secs) {
		this.queue_length_in_secs = queue_length_in_secs;
	}
	public String getCaller_talk_time() {
		return caller_talk_time;
	}
	public void setCaller_talk_time(String caller_talk_time) {
		this.caller_talk_time = caller_talk_time;
	}
	public String getAgent_talk_time_seconds() {
		return agent_talk_time_seconds;
	}
	public void setAgent_talk_time_seconds(String agent_talk_time_seconds) {
		this.agent_talk_time_seconds = agent_talk_time_seconds;
	}
	public String getAfter_call_work_time() {
		return after_call_work_time;
	}
	public void setAfter_call_work_time(String after_call_work_time) {
		this.after_call_work_time = after_call_work_time;
	}
	public String getCall_length() {
		return call_length;
	}
	public void setCall_length(String call_length) {
		this.call_length = call_length;
	}
	public String getAgent_total_time_seconds() {
		return agent_total_time_seconds;
	}
	public void setAgent_total_time_seconds(String agent_total_time_seconds) {
		this.agent_total_time_seconds = agent_total_time_seconds;
	}
	public String getState_code() {
		return state_code;
	}
	public void setState_code(String state_code) {
		this.state_code = state_code;
	}
	public String getIn_business_hours() {
		return in_business_hours;
	}
	public void setIn_business_hours(String in_business_hours) {
		this.in_business_hours = in_business_hours;
	}
	
	public String getLast_segment_call_dispostion() {
		return last_segment_call_dispostion;
	}
	public void setLast_segment_call_dispostion(String last_segment_call_dispostion) {
		this.last_segment_call_dispostion = last_segment_call_dispostion;
	}
	public boolean Is_external_transfer() {
		return is_external_transfer;
	}
	public void setIs_external_transfer(boolean is_external_transfer) {
		this.is_external_transfer = is_external_transfer;
	}
	
	public String Is_voicemail() {
		return is_voicemail;
	}
	public void setIs_voicemail(String is_voicemail) {
		this.is_voicemail = is_voicemail;
	}
	public boolean Is_abandoned() {
		return is_abandoned;
	}
	public void setIs_abandoned(boolean is_abandoned) {
		this.is_abandoned = is_abandoned;
	}
}
