package com.getinsured.hix.dto.cap.liveops;

public class SignOnRequest {
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCallcenterName() {
		return callcenterName;
	}
	public void setCallcenterName(String callcenterName) {
		this.callcenterName = callcenterName;
	}
	private String username;
	private String password;
	private String callcenterName;
	
}
