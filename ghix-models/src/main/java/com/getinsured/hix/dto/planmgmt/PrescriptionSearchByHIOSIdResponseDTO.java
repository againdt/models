/** @author santanu
 * @version 1.0
 * @since Jun 7, 2017 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class PrescriptionSearchByHIOSIdResponseDTO  extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<PrescriptionDTO> prescriptionList;

	/**
	 * @return the prescriptionList
	 */
	public List<PrescriptionDTO> getPrescriptionList() {
		return prescriptionList;
	}

	/**
	 * @param prescriptionList the prescriptionList to set
	 */
	public void setPrescriptionList(List<PrescriptionDTO> prescriptionList) {
		this.prescriptionList = prescriptionList;
	}
	
}
