package com.getinsured.hix.model.entity;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * The persistent class for the Assister database table.
 * 
 */
@Audited
@Entity
@Table(name = "ee_designate_assisters")
public class DesignateAssister implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_Designate_Assisters_Seq")
	@SequenceGenerator(name = "EE_Designate_Assisters_Seq", sequenceName = "ee_designate_assisters_Seq", allocationSize = 1)
	private int id;

	public enum Status {
		Active, Pending, InActive;
	}

	@Column(name = "assister_id")
	private int assisterId;

	@Column(name = "entity_id")
	private int entityId;

	@Column(name = "individual_id")
	private Integer individualId;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;

	@Column(name = "esign_by")
	private String esignBy;

	@Column(name = "esign_date", columnDefinition = "date")
	private Date esignDate;

	@Column(name = "show_switch_role_popup")
	private Character show_switch_role_popup;

	@Column(name = "delegation_code")
	private String delegationCode;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "last_updated_by")
	private Long lastUpdatedBy;

	public DesignateAssister() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getEsignBy() {
		return esignBy;
	}

	public void setEsignBy(String esignBy) {
		this.esignBy = esignBy;
	}

	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());

	}

	public Integer getIndividualId() {
		return individualId;
	}

	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}

	public Character getShow_switch_role_popup() {
		return show_switch_role_popup;
	}

	public void setShow_switch_role_popup(Character show_switch_role_popup) {
		this.show_switch_role_popup = show_switch_role_popup;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public String getDelegationCode() {
		return delegationCode;
	}

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	public int getAssisterId() {
		return assisterId;
	}

	public void setAssisterId(int assisterId) {
		this.assisterId = assisterId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Object clone() throws CloneNotSupportedException{
        DesignateAssister assisterObj = new DesignateAssister();
        assisterObj.setId(getId());
        assisterObj.setAssisterId(getAssisterId());
        assisterObj.setIndividualId(getIndividualId());
        assisterObj.setStatus(getStatus());
        assisterObj.setShow_switch_role_popup(getShow_switch_role_popup());
        return assisterObj;
 }
 
	@Override
	public String toString() {
		
		return " DesignateAssister Details: assisterId = " + assisterId + ", entityId = " + entityId + ", individualId = " + individualId  + ", status = " + (status != null ? status.toString() : "");
	}

}
