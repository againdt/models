package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExpenseEstimate;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalCondition;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.MedicalProcedure;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PreferencesLevel;

public class PlanPreferences implements Serializable{

	private PreferencesLevel medicalUse;

	private PreferencesLevel prescriptionUse;
	
	private ExpenseEstimate expenseEstimate;
	
	private List<PlanPreferencesDET> doctors;
	
	private List<DrugDTO> prescriptions;
	
	private List<PlanPreferencesDET> benefits;
	
	private Integer planScore;
	
	private MedicalCondition medicalCondition;
	
	private MedicalProcedure medicalProcedure;
	
	public Integer getPlanScore() {
		return planScore;
	}

	public void setPlanScore(Integer planScore) {
		this.planScore = planScore;
	}

	public ExpenseEstimate getExpenseEstimate() {
		return expenseEstimate;
	}

	public void setExpenseEstimate(ExpenseEstimate expenseEstimate) {
		this.expenseEstimate = expenseEstimate;
	}

	public List<PlanPreferencesDET> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<PlanPreferencesDET> doctors) {
		this.doctors = doctors;
	}

	public List<DrugDTO> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<DrugDTO> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public List<PlanPreferencesDET> getBenefits() {
		return benefits;
	}

	public void setBenefits(List<PlanPreferencesDET> benefits) {
		this.benefits = benefits;
	}

	public PreferencesLevel getMedicalUse() {
		return medicalUse;
	}

	public void setMedicalUse(PreferencesLevel medicalUse) {
		this.medicalUse = medicalUse;
	}

	public PreferencesLevel getPrescriptionUse() {
		return prescriptionUse;
	}

	public void setPrescriptionUse(PreferencesLevel prescriptionUse) {
		this.prescriptionUse = prescriptionUse;
	}
	
	public MedicalCondition getMedicalCondition() {
		return medicalCondition;
	}

	public void setMedicalCondition(MedicalCondition medicalCondition) {
		this.medicalCondition = medicalCondition;
	}

	public MedicalProcedure getMedicalProcedure() {
		return medicalProcedure;
	}

	public void setMedicalProcedure(MedicalProcedure medicalProcedure) {
		this.medicalProcedure = medicalProcedure;
	}
	
	
		
}
