set head off
set feedback off
set term off
set trims on
set lines 200
set pages 0
spool grant_and_create_synonyms_TEMPLATE_APP_USER.sql
---
----
---- grant DMLs 
select '---' from dual;
select '--- grant DMLs ' from dual;
select '---' from dual;
--- 
select 'grant select, insert, update, delete on TEMPLATE_APP_OWNER.'||object_name|| ' to TEMPLATE_APP_USER_ROLE;'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='TABLE';

select 'grant select on TEMPLATE_APP_OWNER.'||object_name|| ' to TEMPLATE_APP_USER_ROLE;'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='SEQUENCE';

select 'grant select on TEMPLATE_APP_OWNER.'||object_name|| ' to TEMPLATE_APP_USER_ROLE;'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='VIEW';

select 'grant execute on TEMPLATE_APP_OWNER.'||object_name|| ' to TEMPLATE_APP_USER_ROLE;'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='PROCEDURE';

/*
To make sure the USER able to create synonym locally
*/
select 'grant create synonym to TEMPLATE_APP_USER_ROLE;' from dual;


/*
ACCOUNT_PAYABLE(VIEW)
issuer_invoices inv, issuer_payment_invoice pay, issuers i
*/


 grant select on TEMPLATE_APP_OWNER.issuer_invoices to TEMPLATE_APP_USER;
 grant select on TEMPLATE_APP_OWNER.issuer_payment_invoice  to TEMPLATE_APP_USER;
 grant select on TEMPLATE_APP_OWNER.issuers to TEMPLATE_APP_USER;

/*
ACCOUNT_RECEIVEABLE(VIEW)
employer_invoices inv, employer_payment_invoice pay, employers e, employer_payments empPay
*/


grant select on TEMPLATE_APP_OWNER.employer_invoices to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.employer_payment_invoice to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.employers to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.employer_payments to TEMPLATE_APP_USER;

/*
QHP_ACCOUNT_RECEIVEABLE(VIEW)
Employers ; Employer_invoices_lineitems 
*/


grant select on TEMPLATE_APP_OWNER.Employers to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.Employer_invoices_lineitems to TEMPLATE_APP_USER;


/*
REMITTANCE (VIEW)
ISSUER_PAYMENTS inv, ISSUER_PAYMENT_DETAIL dtl, PAYMENT_METHODS pay, BANK_INFO bnk, ISSUER_INVOICES_LINEITEMS li, Employers emp, Employees empl
*/

grant select on TEMPLATE_APP_OWNER.ISSUER_PAYMENTS to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.ISSUER_PAYMENT_DETAIL to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.PAYMENT_METHODS to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.BANK_INFO to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.ISSUER_INVOICES_LINEITEMS to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.Employers to TEMPLATE_APP_USER;
grant select on TEMPLATE_APP_OWNER.Employees to TEMPLATE_APP_USER;

----
---- create synonymns
select '---' from dual;
select '--- create synonymns ' from dual;
select '---' from dual;

select 'create synonym TEMPLATE_APP_USER.'||object_name|| ' for TEMPLATE_APP_OWNER.'||object_name||';'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='TABLE';

select 'create synonym TEMPLATE_APP_USER.'||object_name|| ' for TEMPLATE_APP_OWNER.'||object_name||';'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='VIEW';

select 'create synonym TEMPLATE_APP_USER.'||object_name|| ' for TEMPLATE_APP_OWNER.'||object_name||';'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='SEQUENCE';

select 'create synonym TEMPLATE_APP_USER.'||object_name|| ' for TEMPLATE_APP_OWNER.'||object_name||';'
from dba_objects
where  OWNER='TEMPLATE_APP_OWNER'
and object_type='PROCEDURE';

spool off
@grant_and_create_synonyms_TEMPLATE_APP_USER.sql
quit
