package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class EnrlReconciliationResponse extends GHIXResponse implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EnrlSearchDiscrepancyDTO enrlSearchDiscrepancyDTO;
	private List<EnrlDiscrepancyDataDTO> enrlDiscrepancyDataDTOList;
	private Integer totalRecordCount;
	
	private EnrlReconMonthlyActivityDTO enrlReconMonthlyActivityDTO;
	
	private List<DiscrepancyCommentDTO> discrepancyCommentDTOList;
	private List<DiscrepancyDetailDTO> discrepancyDetailDTOList;
	private DiscrepancyEnrollmentDTO discrepancyEnrollmentDTO;
	
	
	public DiscrepancyEnrollmentDTO getDiscrepancyEnrollmentDTO() {
		return discrepancyEnrollmentDTO;
	}
	public void setDiscrepancyEnrollmentDTO(DiscrepancyEnrollmentDTO discrepancyEnrollmentDTO) {
		this.discrepancyEnrollmentDTO = discrepancyEnrollmentDTO;
	}
	public List<DiscrepancyCommentDTO> getDiscrepancyCommentDTOList() {
		return discrepancyCommentDTOList;
	}
	public void setDiscrepancyCommentDTOList(List<DiscrepancyCommentDTO> discrepancyCommentDTOList) {
		this.discrepancyCommentDTOList = discrepancyCommentDTOList;
	}
	public List<DiscrepancyDetailDTO> getDiscrepancyDetailDTOList() {
		return discrepancyDetailDTOList;
	}
	public void setDiscrepancyDetailDTOList(List<DiscrepancyDetailDTO> discrepancyDetailDTOList) {
		this.discrepancyDetailDTOList = discrepancyDetailDTOList;
	}
	public EnrlSearchDiscrepancyDTO getEnrlSearchDiscrepancyDTO() {
		return enrlSearchDiscrepancyDTO;
	}
	public void setEnrlSearchDiscrepancyDTO(EnrlSearchDiscrepancyDTO enrlSearchDiscrepancyDTO) {
		this.enrlSearchDiscrepancyDTO = enrlSearchDiscrepancyDTO;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<EnrlDiscrepancyDataDTO> getEnrlDiscrepancyDataDTOList() {
		return enrlDiscrepancyDataDTOList;
	}
	public void setEnrlDiscrepancyDataDTOList(List<EnrlDiscrepancyDataDTO> enrlDiscrepancyDataDTOList) {
		this.enrlDiscrepancyDataDTOList = enrlDiscrepancyDataDTOList;
	}
	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}
	public void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
	public EnrlReconMonthlyActivityDTO getEnrlReconMonthlyActivityDTO() {
		return enrlReconMonthlyActivityDTO;
	}
	public void setEnrlReconMonthlyActivityDTO(EnrlReconMonthlyActivityDTO enrlReconMonthlyActivityDTO) {
		this.enrlReconMonthlyActivityDTO = enrlReconMonthlyActivityDTO;
	}
}
