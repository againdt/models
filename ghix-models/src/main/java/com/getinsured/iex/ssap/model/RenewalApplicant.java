package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import com.getinsured.timeshift.util.TSDate;

import com.getinsured.eligibility.enums.RenewalStatus;

/**
 * The persistent class for the RENEWAL_APPLICATIONS database table.
 *
 */
@Audited
@Entity
@Table(name = "RENEWAL_APPLICANTS")
@DynamicInsert
@DynamicUpdate
public class RenewalApplicant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RENEWAL_APPLICANTS_ID_GENERATOR", sequenceName = "RENEWAL_APPLICANTS_SEQ",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RENEWAL_APPLICANTS_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 22)
	private Long id;

	@Column(nullable = false, precision = 22, name = "SSAP_APPLICATION_ID")
	private Long ssapApplicationId;

	@Column(unique = true,precision = 22, name = "SSAP_APPLICANT_ID")
	private Long ssapApplicantId;

	@Column(name = "ENROLLMENT_ID")
	private Long enrollmentId;

	@Column(name = "RENEWAL_STATUS", length = 50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus renewalStatus;

	@Column(name = "PLAN_ID")
	private String planId;

	@Column(name = "PLAN_TYPE")
	private String planType;

	@Column(name = "GROUP_REASON_CODE")
	private Integer groupReasonCode;

	@Column(name = "GROUP_RENEWAL_STATUS", length = 50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus groupRenewalStatus;

	@Column(name = "CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@Column(name = "MEMBER_FALLOUT_REASON_CODE")
	private Integer memberFalloutReasonCode;

	@Column(name="MEMBER_ID", length=100)
	private String memberId;
	
	@Column(name = "GI_MONITOR_ID")
	private Long giMonitorId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Long getSsapApplicantId() {
		return ssapApplicantId;
	}

	public void setSsapApplicantId(Long ssapApplicantId) {
		this.ssapApplicantId = ssapApplicantId;
	}

	public Long getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public Integer getGroupReasonCode() {
		return groupReasonCode;
	}

	public void setGroupReasonCode(Integer groupReasonCode) {
		this.groupReasonCode = groupReasonCode;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public RenewalStatus getRenewalStatus() {
		return renewalStatus;
	}

	public void setRenewalStatus(RenewalStatus renewalStatus) {
		this.renewalStatus = renewalStatus;
	}

	public RenewalStatus getGroupRenewalStatus() {
		return groupRenewalStatus;
	}

	public void setGroupRenewalStatus(RenewalStatus groupRenewalStatus) {
		this.groupRenewalStatus = groupRenewalStatus;
	}

	public Integer getMemberFalloutReasonCode() {
		return memberFalloutReasonCode;
	}

	public void setMemberFalloutReasonCode(Integer memberFalloutReasonCode) {
		this.memberFalloutReasonCode = memberFalloutReasonCode;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public Long getGiMonitorId() {
		return giMonitorId;
	}

	public void setGiMonitorId(Long giMonitorId) {
		this.giMonitorId = giMonitorId;
	}
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new Timestamp(new TSDate().getTime()));
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	@Override
	public String toString() {
		return "RenewalApplicant [id=" + id + ", ssapApplicationId="
				+ ssapApplicationId + ", ssapApplicantId=" + ssapApplicantId
				+ ", enrollmentId=" + enrollmentId + ", renewalStatus="
				+ renewalStatus + ", planId=" + planId + ", planType="
				+ planType + ", groupReasonCode=" + groupReasonCode
				+ ", groupRenewalStatus=" + groupRenewalStatus
				+ ", creationTimestamp=" + creationTimestamp
				+ ", lastUpdateTimestamp=" + lastUpdateTimestamp
				+ ", memberFalloutReasonCode=" + memberFalloutReasonCode
				+ ", memberId=" + memberId + ", giMonitorId=" + giMonitorId
				+ "]";
	}
}