package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;
import com.getinsured.timeshift.sql.TSTimestamp;


/**
 * The persistent class for the AT_SPAN_INFO database table.
 *
 */
@Audited
@Entity
@Table(name = "AT_SPAN_INFO")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name = "AccountTransferSpanInfo.findAll", query = "SELECT a FROM AccountTransferSpanInfo a")
public class AccountTransferSpanInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "AT_SPAN_INFO_ID_GENERATOR", sequenceName = "AT_SPAN_INFO_SEQ" , allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AT_SPAN_INFO_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 22)
	private long id;

	@NotAudited
	@Column(name = "CURRENT_SPAN")
	private BigDecimal currentSpan;

	@NotAudited
	@Column(name = "TOTAL_SPAN")
	private BigDecimal totalSpan;

	@NotAudited
	@Column(name = "CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@NotAudited
	@Temporal(TemporalType.DATE)
	@Column(name = "DUE_DATE")
	private Date dueDate;

	@NotAudited
	@Column(name = "PROCESSED_TIMESTAMP")
	private Timestamp processedTimestamp;

	@Column(name = "QUEUE_STATUS")
	@Enumerated(EnumType.STRING)
	private AccountTransferQueueStatus queueStatus;

	@Column(name = "PROCESS_STATUS")
	@Enumerated(EnumType.STRING)
	private AccountTransferProcessStatus processStatus;

	@NotAudited
	@Column(name = "FAILURE_MESSAGE", length = 100)
	private String failureMessage;

	@Column(name = "SSAP_APPLICATION_ID", precision = 22)
	private Long ssapApplicationId;

	@Column(name = "CMR_HOUSEHOLD_ID", precision = 22)
	private BigDecimal cmrHouseholdId;

	@NotAudited
	@Column(name = "COVERAGE_YEAR")
	private long coverageYear;

	@NotAudited
	@Column(name = "REFERRAL_RESPONSE_MAP")
	private String referralResponseMap;
	
	@NotAudited
	@Column(name = "ELIGIBILITY_START_DATE")
	private Date eligibilityStartDate;
	
	@NotAudited
	@Column(name = "ELIGIBILITY_END_DATE")
	private Date eligibilityEndDate;
	
	@NotAudited
	@Column(name="GI_WS_PAYLOAD_ID")
	private Integer giWsPayloadId;
	
	@NotAudited
	@Column(name="EXTERNAL_APPLICANT_ID", length=100)
	private String externalApplicantId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getCurrentSpan() {
		return currentSpan;
	}

	public void setCurrentSpan(BigDecimal currentSpan) {
		this.currentSpan = currentSpan;
	}

	public BigDecimal getTotalSpan() {
		return totalSpan;
	}

	public void setTotalSpan(BigDecimal totalSpan) {
		this.totalSpan = totalSpan;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Timestamp getProcessedTimestamp() {
		return processedTimestamp;
	}

	public void setProcessedTimestamp(Timestamp processedTimestamp) {
		this.processedTimestamp = processedTimestamp;
	}

	public AccountTransferQueueStatus getQueueStatus() {
		return queueStatus;
	}

	public void setQueueStatus(AccountTransferQueueStatus queueStatus) {
		this.queueStatus = queueStatus;
	}

	public AccountTransferProcessStatus getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(AccountTransferProcessStatus processStatus) {
		this.processStatus = processStatus;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public BigDecimal getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(BigDecimal cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getReferralResponseMap() {
		return referralResponseMap;
	}

	public void setReferralResponseMap(String referralResponseMap) {
		this.referralResponseMap = referralResponseMap;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}
	
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public String getExternalApplicantId() {
		return externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreationTimestamp(new TSTimestamp());
		this.setLastUpdateTimestamp(new TSTimestamp());
	}
	
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new TSTimestamp());
	}
}
