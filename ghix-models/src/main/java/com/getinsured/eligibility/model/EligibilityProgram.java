package com.getinsured.eligibility.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;


import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * The persistent class for the PROGRAM_ELIGIBILITIES database table.
 *
 */

@Audited
@Entity
@Table(name = "PROGRAM_ELIGIBILITIES")
@DynamicInsert
@DynamicUpdate
public class EligibilityProgram {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROGRAM_ELIGIBILITIES_SEQ")
	@SequenceGenerator(name = "PROGRAM_ELIGIBILITIES_SEQ", sequenceName = "PROGRAM_ELIGIBILITIES_SEQ", allocationSize = 1)
	private Long id;

	@NotAudited 
	@Column(name = "ELIGIBILITY_TYPE")
	private String eligibilityType;

	@NotAudited 
	@Column(name = "ELIGIBILITY_INDICATOR")
	private String eligibilityIndicator;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_START_DATE")
	private Date eligibilityStartDate;

	@NotAudited 
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_END_DATE")
	private Date eligibilityEndDate;

	@NotAudited 
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ELIGIBILITY_DETERMINATION_DATE")
	private Date eligibilityDeterminationDate;

	@NotAudited 
	@Column(name = "INELIGIBLE_REASON")
	private String ineligibleReason;

	@NotAudited 
	@Column(name = "ESTABLISHING_CATEGORY_CODE")
	private String establishingCategoryCode;

	@NotAudited 
	@Column(name = "ESTABLISHING_STATE_CODE")
	private String establishingStateCode;

	@NotAudited 
	@Column(name = "ESTABLISHING_COUNTY_NAME")
	private String establishingCountyName;

	/*@Column(name = "SSAP_APPLICANT_ID")
	private Long ssapApplicantId;*/

	@NotAudited 
	@ManyToOne
	@JoinColumn(name="SSAP_APPLICANT_ID")
	private SsapApplicant ssapApplicant;

	@NotAudited 
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "eligibilityProgram")
	private List<EligibilityProgramGiwspayload> eligibilityProgramGiwspayload;
	
	@Column(name = "ELIG_START_DATE_OVERRIDE")
	private String eligStartDateOverride;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEligibilityType() {
		return eligibilityType;
	}

	public void setEligibilityType(String eligibilityType) {
		this.eligibilityType = eligibilityType;
	}

	public String getEligibilityIndicator() {
		return eligibilityIndicator;
	}

	public void setEligibilityIndicator(String eligibilityIndicator) {
		this.eligibilityIndicator = eligibilityIndicator;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	public Date getEligibilityDeterminationDate() {
		return eligibilityDeterminationDate;
	}

	public void setEligibilityDeterminationDate(Date eligibilityDeterminationDate) {
		this.eligibilityDeterminationDate = eligibilityDeterminationDate;
	}

	public String getIneligibleReason() {
		return ineligibleReason;
	}

	public void setIneligibleReason(String ineligibleReason) {
		this.ineligibleReason = ineligibleReason;
	}

	public String getEstablishingCategoryCode() {
		return establishingCategoryCode;
	}

	public void setEstablishingCategoryCode(String establishingCategoryCode) {
		this.establishingCategoryCode = establishingCategoryCode;
	}

	public String getEstablishingStateCode() {
		return establishingStateCode;
	}

	public void setEstablishingStateCode(String establishingStateCode) {
		this.establishingStateCode = establishingStateCode;
	}

	public String getEstablishingCountyName() {
		return establishingCountyName;
	}

	public void setEstablishingCountyName(String establishingCountyName) {
		this.establishingCountyName = establishingCountyName;
	}



	public List<EligibilityProgramGiwspayload> getEligibilityProgramGiwspayload() {
		return eligibilityProgramGiwspayload;
	}

	public void setEligibilityProgramGiwspayload(
			List<EligibilityProgramGiwspayload> eligibilityProgramGiwspayload) {
		this.eligibilityProgramGiwspayload = eligibilityProgramGiwspayload;
	}

	public SsapApplicant getSsapApplicant() {
		return ssapApplicant;
	}

	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}

	public String getEligStartDateOverride() {
		return eligStartDateOverride;
	}

	public void setEligStartDateOverride(String eligStartDateOverride) {
		this.eligStartDateOverride = eligStartDateOverride;
	}

}
