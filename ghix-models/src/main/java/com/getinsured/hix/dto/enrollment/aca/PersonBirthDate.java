package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Awesome Pojo Generator
 * */
public class PersonBirthDate{
  @JsonProperty("Date")
  
  private com.getinsured.hix.dto.enrollment.aca.Date Date;
  public void setDate(com.getinsured.hix.dto.enrollment.aca.Date Date){
   this.Date=Date;
  }
  public com.getinsured.hix.dto.enrollment.aca.Date getDate(){
   return Date;
  }
}