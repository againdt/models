package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 * 
 * This is POJO class for PM_NETWORK_TRANSPARENCY database table.
 * @author Bhavin Parmar
 * @since July 22, 2016
 */
@Audited
@Entity
@Table(name = "PM_NETWORK_TRANSPARENCY")
public class NetworkTransparency implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum IS_DELETED {
		Y, N; // Y-Yes, N-No
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_NETWORK_TRANSPARENCY_SEQ")
	@SequenceGenerator(name = "PM_NETWORK_TRANSPARENCY_SEQ", sequenceName = "PM_NETWORK_TRANSPARENCY_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "HIOS_ISSUER_ID")
	private String hisoIssuerId;

	@Column(name = "NETWORK_ID")
	private String networkID;

	@Column(name = "ISSUER_PLAN_NUMBER")
	private String issuerPlanNumber;

	@Column(name = "COUNTY_NAME")
	private String countyName;

	@Column(name = "COUNTY_FIPS")
	private String countyFips;

	@Column(name = "RATING")
	private String rating;

	@Column(name = "APPLICABLE_YEAR")
	private Integer applicableYear;

	@Column(name = "IS_DELETED")
	private String isDeleted;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date creationTimestamp;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdatedTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdateBy;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdatedTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdatedTimestamp(new TSDate());
	}

	public NetworkTransparency() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHisoIssuerId() {
		return hisoIssuerId;
	}

	public void setHisoIssuerId(String hisoIssuerId) {
		this.hisoIssuerId = hisoIssuerId;
	}

	public String getNetworkID() {
		return networkID;
	}

	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCountyFips() {
		return countyFips;
	}

	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
}
