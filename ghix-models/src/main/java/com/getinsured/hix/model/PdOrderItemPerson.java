package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;

@Entity
@Table(name="PD_ORDER_ITEM_PERSON")
public class PdOrderItemPerson implements Serializable
{	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pd_order_item_person_seq")
	@SequenceGenerator(name = "pd_order_item_person_seq", sequenceName = "pd_order_item_person_seq", allocationSize = 1)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pd_order_item_id")
	private PdOrderItem pdOrderItem;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pd_person_id")
	private PdPerson pdPerson;
	
	@Column(name = "premium")
	private Float premium;
	
	@Column(name = "applied_subsidy")
	private Float appliedSubsidy;
	
	@Column(name = "region")
	private Integer region;
	
	@Column(name="subscriber_flag")
	@Enumerated(EnumType.STRING)
	private YorN subscriberFlag;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="creation_timestamp")
	private Date creationTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp")
	private Date lastUpdateTimestamp;  
    
    private Integer age;
    
    @Column(name = "person_coverage_start_date")
	private Date personCoverageStartDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public PdOrderItem getPdOrderItem() {
		return pdOrderItem;
	}

	public void setPdOrderItem(PdOrderItem pdOrderItem) {
		this.pdOrderItem = pdOrderItem;
	}

	public PdPerson getPdPerson() {
		return pdPerson;
	}

	public void setPdPerson(PdPerson pdPerson) {
		this.pdPerson = pdPerson;
	}

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	public Float getAppliedSubsidy() {
		return appliedSubsidy;
	}

	public void setAppliedSubsidy(Float appliedSubsidy) {
		this.appliedSubsidy = appliedSubsidy;
	}

	public Integer getRegion() {
		return region;
	}

	public void setRegion(Integer region) {
		this.region = region;
	}

	public YorN getSubscriberFlag() {
		return subscriberFlag;
	}

	public void setSubscriberFlag(YorN subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getPersonCoverageStartDate() {
		return personCoverageStartDate;
	}

	public void setPersonCoverageStartDate(Date personCoverageStartDate) {
		this.personCoverageStartDate = personCoverageStartDate;
	}

	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate()); 
	}
	
}
