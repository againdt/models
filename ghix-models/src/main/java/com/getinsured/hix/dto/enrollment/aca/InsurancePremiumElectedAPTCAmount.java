package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class InsurancePremiumElectedAPTCAmount{
  @JsonProperty("Value")
  
  private Float Value;
  public void setValue(Float Value){
   this.Value=Value;
  }
  public Float getValue(){
   return Value;
  }
}