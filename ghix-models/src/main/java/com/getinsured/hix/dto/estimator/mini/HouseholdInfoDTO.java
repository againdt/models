package com.getinsured.hix.dto.estimator.mini;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * Create DTO for outbound HouseholdInfo API to send pre-65 Retiree/Non-Retiree + Provider + Prescription data to AgentExpress.
 */
public class HouseholdInfoDTO {

	private long leadId;
	private boolean hasRetireeData;
	private RetireeInfoDTO retireeInfoDTO;
	private NonRetireeInfoDTO nonRetireeInfoDTO;
	private String providerData;
	private String prescriptionData;

	public HouseholdInfoDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public long getLeadId() {
		return leadId;
	}

	public void setLeadId(long leadId) {
		this.leadId = leadId;
	}

	public boolean isHasRetireeData() {
		return hasRetireeData;
	}

	public void setHasRetireeData(boolean hasRetireeData) {
		this.hasRetireeData = hasRetireeData;
	}

	public RetireeInfoDTO getRetireeInfoDTO() {
		return retireeInfoDTO;
	}

	public void setRetireeInfoDTO(RetireeInfoDTO retireeInfoDTO) {
		this.retireeInfoDTO = retireeInfoDTO;
	}

	public NonRetireeInfoDTO getNonRetireeInfoDTO() {
		return nonRetireeInfoDTO;
	}

	public void setNonRetireeInfoDTO(NonRetireeInfoDTO nonRetireeInfoDTO) {
		this.nonRetireeInfoDTO = nonRetireeInfoDTO;
	}

	public String getProviderData() {
		return providerData;
	}

	public void setProviderData(String providerData) {
		this.providerData = providerData;
	}

	public String getPrescriptionData() {
		return prescriptionData;
	}

	public void setPrescriptionData(String prescriptionData) {
		this.prescriptionData = prescriptionData;
	}
}
