package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;

/**
 * The persistent class for the employer_locations database table.
 * 
 */
@Entity
@Table(name="employer_locations")
public class EmployerLocation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static enum PrimaryLocation{YES, NO}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EmployerLocation_Seq")
	@SequenceGenerator(name = "EmployerLocation_Seq", sequenceName = "employer_locations_seq", allocationSize = 1)
	private int id;
	
	//bi-directional many-to-one association to Employer
    @ManyToOne
    @JoinColumn(name="employer_id",insertable=true,updatable=true)
	private Employer employer;
    
    //uni-directional many-to-one association to locations
    @Valid
    @OneToOne(cascade = {CascadeType.ALL} ) 
    @JoinColumn(name="location_id",insertable=true,updatable=true)
	private Location location;
    
	@Column(name="avg_days_seasonal_emp")
	private int avgDaysWorkSeasonalEmp;
	
	@Column(name = "primary_location")
	@Enumerated(EnumType.STRING)
	private PrimaryLocation primaryLocation;
	
	@Temporal( TemporalType.TIMESTAMP)
    @Column(name="creation_timestamp", nullable=false, updatable=false)
	private Date created;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="last_update_timestamp",nullable=false)
	private Date updated;
    
    @Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
    public EmployerLocation() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Employer getEmployer() {
		return this.employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}
	
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public int getAvgDaysWorkSeasonalEmp() {
		return avgDaysWorkSeasonalEmp;
	}

	public void setAvgDaysWorkSeasonalEmp(int avgDaysWorkSeasonalEmp) {
		this.avgDaysWorkSeasonalEmp = avgDaysWorkSeasonalEmp;
	}

	
	public PrimaryLocation getPrimaryLocation() {
		return primaryLocation;
	}

	public void setPrimaryLocation(PrimaryLocation primaryLocation) {
		this.primaryLocation = primaryLocation;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Override
	public  String toString() {
        StringBuilder sb = new  StringBuilder();
        sb.append("avgDaysWorkSeasonalEmp=").append(avgDaysWorkSeasonalEmp).append(", ");
        String locationStr = (getLocation() == null) ? "null" : getLocation().toString();
        sb.append("location=").append(locationStr);
        return  sb.toString();
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
