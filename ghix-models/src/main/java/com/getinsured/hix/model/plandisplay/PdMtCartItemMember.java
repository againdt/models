package com.getinsured.hix.model.plandisplay;

import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.MaleOrFemale;
import com.getinsured.hix.model.plandisplay.PdMtCartItemResponse.YesOrNo;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;

public class PdMtCartItemMember {
	
	private Relationship relationship;
	private String zip;
	private String dob;
	private YesOrNo tobacco;
	private MaleOrFemale gender;
	private YesOrNo subscriberFlag;
	private String countyCode;
	
	public Relationship getRelationship() {
		return relationship;
	}
	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public YesOrNo getTobacco() {
		return tobacco;
	}
	public void setTobacco(YesOrNo tobacco) {
		this.tobacco = tobacco;
	}
	public MaleOrFemale getGender() {
		return gender;
	}
	public void setGender(MaleOrFemale gender) {
		this.gender = gender;
	}
	public YesOrNo getSubscriberFlag() {
		return subscriberFlag;
	}
	public void setSubscriberFlag(YesOrNo subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	
	


}
