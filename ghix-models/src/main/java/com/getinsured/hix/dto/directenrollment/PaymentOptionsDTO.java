package com.getinsured.hix.dto.directenrollment;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vijay
 *
 */
public class PaymentOptionsDTO {
	private String carrier;
	private String state;
	private String planTypePrefix;
	private boolean isDefault;
	private List<PaymentTypeDTO> initialPayOptions;
	private List<PaymentTypeDTO> recurringPayOptions;
	
	
	
	public PaymentOptionsDTO() {
		super();
		initialPayOptions = new ArrayList<PaymentTypeDTO>();
		recurringPayOptions = new ArrayList<PaymentTypeDTO>();
	}


	public PaymentOptionsDTO(String carrier, String state, String planTypePrefix, boolean isDefault, 
			List<PaymentTypeDTO> initialPayOptions, List<PaymentTypeDTO> recurringPayOptions) {
		super();
		this.carrier = carrier;
		this.state = state;		
		this.planTypePrefix = planTypePrefix;
		this.isDefault = isDefault;
		this.initialPayOptions = initialPayOptions;
		this.recurringPayOptions = recurringPayOptions;
		
	}

	@Override
	public String toString() {
		return "PaymentOptionsDTO [carrier=" + carrier + ", state="
				+ state + ", planTypePrefix=" + planTypePrefix + ", isDefault = " + isDefault
				+ ", initialPayOptions=" + initialPayOptions + ", recurringPayOptions=" 
				+ recurringPayOptions + "]";
	}


	public String getCarrier() {
		return carrier;
	}


	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getPlanTypePrefix() {
		return planTypePrefix;
	}


	public void setPlanTypePrefix(String planTypePrefix) {
		this.planTypePrefix = planTypePrefix;
	}
	
	public boolean isDefault() {
		return isDefault;
	}
	

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	

	public List<PaymentTypeDTO> getInitialPayOptions() {
		return initialPayOptions;
	}


	public void setInitialPayOptions(List<PaymentTypeDTO> initialPayOptions) {
		this.initialPayOptions = initialPayOptions;
	}


	public List<PaymentTypeDTO> getRecurringPayOptions() {
		return recurringPayOptions;
	}


	public void setRecurringPayOptions(List<PaymentTypeDTO> recurringPayOptions) {
		this.recurringPayOptions = recurringPayOptions;
	}	
}