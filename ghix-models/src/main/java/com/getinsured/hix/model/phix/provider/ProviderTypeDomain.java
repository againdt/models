package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_type_domain")
public class ProviderTypeDomain  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer providerTypeId;
	private String providerType;
	private Set<ProviderSpecialty> providerSpecialties = new HashSet<ProviderSpecialty>(0);

	@Id 
	@Column(name="PROVIDER_TYPE_ID", unique=true, nullable=false)
	public Integer getProviderTypeId() {
		return this.providerTypeId;
	}

	public void setProviderTypeId(Integer providerTypeId) {
		this.providerTypeId = providerTypeId;
	}


	@Column(name="PROVIDER_TYPE", length=100)
	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerTypeDomain")
	public Set<ProviderSpecialty> getProviderSpecialties() {
		return this.providerSpecialties;
	}

	public void setProviderSpecialties(Set<ProviderSpecialty> providerSpecialties) {
		this.providerSpecialties = providerSpecialties;
	}

}


