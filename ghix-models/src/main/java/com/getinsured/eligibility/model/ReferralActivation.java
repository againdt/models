package com.getinsured.eligibility.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getinsured.eligibility.enums.AtResponseTypeEnum;
import com.getinsured.eligibility.enums.ReferralActivationEnum;

/**
 * The persistent class for the REFERRAL_ACTIVATION database table.
 *
 * @author chopra_s
 *
 */
@Audited
@Entity
@Table(name = "REFERRAL_ACTIVATION")
@DynamicInsert
@DynamicUpdate
public class ReferralActivation {
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REFERRAL_ACTIVATION_SEQ")
	@SequenceGenerator(name = "REFERRAL_ACTIVATION_SEQ", sequenceName = "REFERRAL_ACTIVATION_SEQ", allocationSize = 1)
	private Long id;

	@Column(name = "USER_ID")
	private Integer userId;

	@Column(name = "SSAP_APPLICATION_ID")
	private Integer ssapApplicationId;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdatedOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LOCK_TIMESTAMP")
	private Date lockTime;

	@Column(name = "SECURITY_RETRY_COUNT")
	private int securityRetryCount = 0;

	@Enumerated(EnumType.STRING)
	@Column(name = "WORKFLOW_STATUS")
	private ReferralActivationEnum workflowStatus;

	@Column(name = "STATUS")
	private String status = "ACTIVE";

	@Column(name = "IS_LCE")
	private String isLce = "N";

	@Enumerated(EnumType.STRING)
	@Column(name = "RESPONSE_TYPE")
	private AtResponseTypeEnum responseType;

	@Column(name = "CMR_HOUSEHOLD_ID")
	private Integer cmrHouseholdId;
	
	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public String getIsLce() {
		return isLce;
	}

	public void setIsLce(String isLce) {
		this.isLce = isLce;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Integer ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}
	
	public Date getLockTime() {
		return lockTime;
	}

	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}

	public int getSecurityRetryCount() {
		return securityRetryCount;
	}

	public void setSecurityRetryCount(int securityRetryCount) {
		this.securityRetryCount = securityRetryCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ReferralActivationEnum getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(ReferralActivationEnum workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	@JsonIgnore
	public boolean canStartSecurity() {
		return (this.workflowStatus == ReferralActivationEnum.INITIAL_PHASE);
	}

	@JsonIgnore
	public boolean isSecurityPhase() {
		return (this.workflowStatus == ReferralActivationEnum.START_SECURITY);
	}

	@JsonIgnore
	public boolean isSecurityFailed() {
		return (this.workflowStatus == ReferralActivationEnum.SECURITY_FAILED);
	}

	@JsonIgnore
	public boolean isCsrVerificationPending() {
		return (this.workflowStatus == ReferralActivationEnum.CSR_VERIFICATION_PENDING);
	}

	@JsonIgnore
	public boolean isCoverageYearPending() {
		return (this.workflowStatus == ReferralActivationEnum.COVERAGE_YEAR_PENDING);
	}

	@JsonIgnore
	public boolean isRidpPending() {
		return (this.workflowStatus == ReferralActivationEnum.RIDP_PENDING);
	}

	@JsonIgnore
	public boolean isLcePending() {
		return (this.workflowStatus == ReferralActivationEnum.LCE_PENDING);
	}

	@JsonIgnore
	public boolean isActivationComplete() {
		return (this.workflowStatus == ReferralActivationEnum.ACTIVATION_COMPELETE);
	}

	@JsonIgnore
	public boolean isCancelled() {
		return (this.workflowStatus == ReferralActivationEnum.CANCEL_ACTIVATION);
	}

	public AtResponseTypeEnum getResponseType() {
		return responseType;
	}

	public void setResponseType(AtResponseTypeEnum responseType) {
		this.responseType = responseType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReferralActivation [id=").append(id)
				.append(", userId=").append(userId)
				.append(", ssapApplicationId=").append(ssapApplicationId)
				.append(", cmrHouseholdId=").append(cmrHouseholdId).append("]");
		return builder.toString();
	}


	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}*/
}
