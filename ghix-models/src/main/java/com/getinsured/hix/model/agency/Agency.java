package com.getinsured.hix.model.agency;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TimeShifterUtil;


/**
 * The persistent class for the agency database table.
 * 
 */
@Audited
@Entity
@NamedQuery(name="Agency.findAll", query="SELECT a FROM Agency a")
public class Agency implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum CertificationStatus{
		
		INCOMPLETE("Incomplete"),
		PENDING("Pending"),
		SUSPENDED("Suspended"),
		TERMINATED("Terminated"),
		CERTIFIED("Certified");
		
		
		String value = null;
		
		CertificationStatus(String value){
			this.value = value;
		}
		
		@JsonValue
		public String getValue(){
			return value;
		}
		
		
		@JsonCreator
		public static CertificationStatus getCertificationStatus(String value){
			for(CertificationStatus certificationStatus : CertificationStatus.values()){
					if(certificationStatus.getValue().equalsIgnoreCase(value)){
						return certificationStatus;
					}
			}
			throw new GIRuntimeException("Invalid value for certification status : " +value);
		}
		
	}
	
	
	@Id
	@SequenceGenerator(name="AGENCY_ID_GENERATOR", sequenceName="AGENCY_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AGENCY_ID_GENERATOR")
	private Long id;

	@Column(name="agency_name")
	private String agencyName;

	@Column(name="business_legal_name")
	private String businessLegalName;

	@Column(name="certification_date")
	private Timestamp certificationDate;

	@Column(name="certification_number")
	private Long certificationNumber;

	@Column(name="certification_status")
	@Enumerated(EnumType.STRING)
	private CertificationStatus certificationStatus;

	@Column(name="comments_id")
	private Long commentsId;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="creation_timestamp")
	private Timestamp creationTimestamp;

	@Column(name="federal_tax_id")
	private String federalTaxId;

	@Column(name="last_update_timestamp")
	private Timestamp lastUpdateTimestamp;

	@Column(name="last_updated_by")
	private Long lastUpdatedBy;

	@Column(name="license_number")
	private String licenseNumber;

	@Column(name="recertify_date")
	private Timestamp recertifyDate;
	
	@Column(name = "AGENCY_SUPPORT_DOC_ID")
	private Integer supportDocumentId;
	
	@Column(name = "AGENCY_CONTRACT_DOC_ID")
	private Integer contractDocumentId;
	
	@Column(name = "AGENCY_E_O_DECLR_DOC_ID")
	private Integer eoDeclarationDocumentId;

	public Agency() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgencyName() {
		return this.agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getBusinessLegalName() {
		return this.businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public Timestamp getCertificationDate() {
		return this.certificationDate;
	}

	public void setCertificationDate(Timestamp certificationDate) {
		this.certificationDate = certificationDate;
	}

	public Long getCertificationNumber() {
		return this.certificationNumber;
	}

	public void setCertificationNumber(Long certificationNumber) {
		this.certificationNumber = certificationNumber;
	}

	public CertificationStatus getCertificationStatus() {
		return this.certificationStatus;
	}

	public void setCertificationStatus(CertificationStatus certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public Long getCommentsId() {
		return this.commentsId;
	}

	public void setCommentsId(Long commentsId) {
		this.commentsId = commentsId;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getFederalTaxId() {
		return this.federalTaxId;
	}

	public void setFederalTaxId(String federalTaxId) {
		this.federalTaxId = federalTaxId;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Long getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLicenseNumber() {
		return this.licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public Timestamp getRecertifyDate() {
		return this.recertifyDate;
	}

	public void setRecertifyDate(Timestamp recertifyDate) {
		this.recertifyDate = recertifyDate;
	}
	
	public Integer getSupportDocumentId() {
		return supportDocumentId;
	}

	public void setSupportDocumentId(Integer supportDocumentId) {
		this.supportDocumentId = supportDocumentId;
	}

	public Integer getContractDocumentId() {
		return contractDocumentId;
	}

	public void setContractDocumentId(Integer contractDocumentId) {
		this.contractDocumentId = contractDocumentId;
	}

	public Integer getEoDeclarationDocumentId() {
		return eoDeclarationDocumentId;
	}

	public void setEoDeclarationDocumentId(Integer eoDeclarationDocumentId) {
		this.eoDeclarationDocumentId = eoDeclarationDocumentId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}


}
