package com.getinsured.hix.ui.model.eligibility.prescreen;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.affiliate.model.AffiliateClick.ProductType;



/**
 * This class encapsulates the objects that are sent as a request from the
 * private exchange UI to the backend.
 *
 * @author Nikhil Talreja, Sunil Desu
 * @since August 20, 2013
 *
 */
public class PhixRequest {

	private String zipCode;

	private String countyCode;
	
	private String stateCode;
	
	private String stateName;
	
	private String callUs;
	
	private String sbeWebsite;
	
	private int familySize;

	private List<PhixMember> members;

	private double householdIncome=-1;

	private String docVisitFrequency;

	private String noOfPrescriptions;

	private String benefits;

	private long affiliateId;

	private String affiliateHouseholdId;
	
	private String name;
	
	private String email;

	private String phone;
	
	private String isOkToCall="N";
	
	private String apiKey;

	private Integer flowId;

	private long clickId;

	private String aptc;
	
	private String aptcEligible;
	
	private String subsidyEligible;
	
	private String csrLevel;
	
	private String allPlansAvailable;
	
	private String planInfoAvailable = "Y";
	
	private String expectedMonthlyPremium;

	private String slspPremiumAfterAptc;

	private String slspPremiumBeforeAptc;
	
	private String lbpPremiumAfterAptc;

	private String lbpPremiumBeforeAptc;

	private String lspPremiumAfterAptc;

	private String lspPremiumBeforeAptc;
	
	private String lbpPremiumBeforeAptcForTenant;
	
	private String lspPremiumBeforeAptcForTenant;
	
	private String medicaidEligible;

	private String medicareEligible;

	private String chipEligible;
	
	private String chipHousehold;
		
	private String medicareHousehold;
	
	private int noOfAptcCsr;
	
	private int noOfChip;
	
	private int noOfMedicare;
	
	private int noOfMedicaid;
	
	private String status;
	
	private String callToApiRequired;
	
	private String affFlowCustomField1;
	
	private String legacySiteUrl;
	
	private String issuerGoShoppingOnExchange = "N";
	
	private String mandatePenaltyAmount;
	
	private int eventId;
	
	private String affiliateOffExchgUrl="";
	private String affiliateOnExchgUrl="";
	private String affiliateMedicaidUrl="";
	
	private String affiliateMedicareUrl="";
	//Possible values are Y,N
	private String medicaidHousehold = "N";
	
	private String affFlowOffExchRedirectionInSameWindow="";
	private String affFlowOnExchRedirectionInSameWindow="";
	
	private String isSpouseSelected = "FALSE";
	
	private String shoppingType = null;
	private boolean skipEligibility=false;
		/**
	 * Added gender for STM requests
	 *     HIX-34084 : Capture STM household data in elig lead table
	 */
	private String coverageStartDate;
	
	//HIX-64221 Support for 2016 Issuer Plan Preview
	private boolean showIncomeSection = true;
	
	private int coverageYear;
	
	/* Start -EMX specific Attributes*/
	private String censusType;
	private double employerContributionAmount;
	private double hraAmount;
	private double cobraPremium;
	private String cobraElectionDueDate;
	private String hraTransitionDate;
	private String applicantFirstName;
	private String applicantLastName;
	private PhixRetireeHousehold retireeData;
	private ProductType productType;
	
	/* End */
	private Long leadId;

	private String ivrNumber;
	
	private boolean showNameOnQuoteform ;

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCallUs() {
		return callUs;
	}

	public void setCallUs(String callUs) {
		this.callUs = callUs;
	}

	public String getSbeWebsite() {
		return sbeWebsite;
	}

	public void setSbeWebsite(String sbeWebsite) {
		this.sbeWebsite = sbeWebsite;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public List<PhixMember> getMembers() {

		if(members == null){
			members = new ArrayList<PhixMember>();
		}

		return members;
	}

	public void setMembers(List<PhixMember> members) {
		this.members = members;
	}

	public double getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(double householdIncome) {
		this.householdIncome = householdIncome;
	}

	public String getDocVisitFrequency() {
		return docVisitFrequency;
	}

	public void setDocVisitFrequency(String docVisitFrequency) {
		this.docVisitFrequency = docVisitFrequency;
	}

	public String getNoOfPrescriptions() {
		return noOfPrescriptions;
	}

	public void setNoOfPrescriptions(String noOfPrescriptions) {
		this.noOfPrescriptions = noOfPrescriptions;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public String getAffiliateHouseholdId() {
		return affiliateHouseholdId;
	}

	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.affiliateHouseholdId = affiliateHouseholdId;
	}

	public String getName() {
		return name;
	}

	public boolean getShowIncomeSection() {
		return showIncomeSection;
	}

	public void setShowIncomeSection(boolean showIncomeSection) {
		this.showIncomeSection = showIncomeSection;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsOkToCall() {
		return isOkToCall;
	}

	public void setIsOkToCall(String isOkToCall) {
		this.isOkToCall = isOkToCall;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	public long getClickId() {
		return clickId;
	}

	public void setClickId(long clickId) {
		this.clickId = clickId;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getAptcEligible() {
		return aptcEligible;
	}

	public void setAptcEligible(String aptcEligible) {
		this.aptcEligible = aptcEligible;
	}

	public String getSubsidyEligible() {
		return subsidyEligible;
	}

	public void setSubsidyEligible(String subsidyEligible) {
		this.subsidyEligible = subsidyEligible;
	}

	public String getAllPlansAvailable() {
		return allPlansAvailable;
	}

	public void setAllPlansAvailable(String allPlansAvailable) {
		this.allPlansAvailable = allPlansAvailable;
	}

	public String getPlanInfoAvailable() {
		return planInfoAvailable;
	}

	public void setPlanInfoAvailable(String planInfoAvailable) {
		this.planInfoAvailable = planInfoAvailable;
	}

	public String getExpectedMonthlyPremium() {
		return expectedMonthlyPremium;
	}

	public void setExpectedMonthlyPremium(String expectedMonthlyPremium) {
		this.expectedMonthlyPremium = expectedMonthlyPremium;
	}

	public String getSlspPremiumAfterAptc() {
		return slspPremiumAfterAptc;
	}

	public void setSlspPremiumAfterAptc(String slspPremiumAfterAptc) {
		this.slspPremiumAfterAptc = slspPremiumAfterAptc;
	}

	public String getSlspPremiumBeforeAptc() {
		return slspPremiumBeforeAptc;
	}

	public void setSlspPremiumBeforeAptc(String slspPremiumBeforeAptc) {
		this.slspPremiumBeforeAptc = slspPremiumBeforeAptc;
	}

	public String getLbpPremiumAfterAptc() {
		return lbpPremiumAfterAptc;
	}

	public void setLbpPremiumAfterAptc(String lbpPremiumAfterAptc) {
		this.lbpPremiumAfterAptc = lbpPremiumAfterAptc;
	}

	public String getLbpPremiumBeforeAptc() {
		return lbpPremiumBeforeAptc;
	}

	public void setLbpPremiumBeforeAptc(String lbpPremiumBeforeAptc) {
		this.lbpPremiumBeforeAptc = lbpPremiumBeforeAptc;
	}

	public String getLspPremiumAfterAptc() {
		return lspPremiumAfterAptc;
	}

	public void setLspPremiumAfterAptc(String lspPremiumAfterAptc) {
		this.lspPremiumAfterAptc = lspPremiumAfterAptc;
	}

	public String getLspPremiumBeforeAptc() {
		return lspPremiumBeforeAptc;
	}

	public void setLspPremiumBeforeAptc(String lspPremiumBeforeAptc) {
		this.lspPremiumBeforeAptc = lspPremiumBeforeAptc;
	}

	public String getMedicaidEligible() {
		return medicaidEligible;
	}

	public void setMedicaidEligible(String medicaidEligible) {
		this.medicaidEligible = medicaidEligible;
	}

	public String getMedicareEligible() {
		return medicareEligible;
	}

	public void setMedicareEligible(String medicareEligible) {
		this.medicareEligible = medicareEligible;
	}

	public String getChipEligible() {
		return chipEligible;
	}

	public void setChipEligible(String chipEligible) {
		this.chipEligible = chipEligible;
	}

	public String getChipHousehold() {
		return chipHousehold;
	}

	public void setChipHousehold(String chipHousehold) {
		this.chipHousehold = chipHousehold;
	}

	public String getMedicareHousehold() {
		return medicareHousehold;
	}

	public void setMedicareHousehold(String medicareHousehold) {
		this.medicareHousehold = medicareHousehold;
	}

	public int getNoOfAptcCsr() {
		return noOfAptcCsr;
	}

	public void setNoOfAptcCsr(int noOfAptcCsr) {
		this.noOfAptcCsr = noOfAptcCsr;
	}

	public int getNoOfChip() {
		return noOfChip;
	}

	public void setNoOfChip(int noOfChip) {
		this.noOfChip = noOfChip;
	}

	public int getNoOfMedicare() {
		return noOfMedicare;
	}

	public void setNoOfMedicare(int noOfMedicare) {
		this.noOfMedicare = noOfMedicare;
	}
	
	public int getNoOfMedicaid() {
		return noOfMedicaid;
	}

	public void setNoOfMedicaid(int noOfMedicaid) {
		this.noOfMedicaid = noOfMedicaid;
	}
	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCallToApiRequired() {
		return callToApiRequired;
	}

	public void setCallToApiRequired(String callToApiRequired) {
		this.callToApiRequired = callToApiRequired;
	}

	public String getAffFlowCustomField1() {
		return affFlowCustomField1;
	}

	public void setAffFlowCustomField1(String affFlowCustomField1) {
		this.affFlowCustomField1 = affFlowCustomField1;
	}

	public String getLegacySiteUrl() {
		return legacySiteUrl;
	}

	public void setLegacySiteUrl(String legacySiteUrl) {
		this.legacySiteUrl = legacySiteUrl;
	}

	public String getIssuerGoShoppingOnExchange() {
		return issuerGoShoppingOnExchange;
	}

	public void setIssuerGoShoppingOnExchange(String issuerGoShoppingOnExchange) {
		this.issuerGoShoppingOnExchange = issuerGoShoppingOnExchange;
	}
	
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public String getMandatePenaltyAmount() {
		return mandatePenaltyAmount;
	}

	public void setMandatePenaltyAmount(String mandatePenaltyAmount) {
		this.mandatePenaltyAmount = mandatePenaltyAmount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PhixRequest [zipCode=").append(zipCode);
		builder.append(", countyCode=").append(countyCode);
		builder.append(", familySize=").append(familySize);
		builder.append(", members=").append(members);
		builder.append(", householdIncome=").append(householdIncome);
		builder.append(", docVisitFrequency=").append(docVisitFrequency);
		builder.append(", noOfPrescriptions=").append(noOfPrescriptions);
		builder.append(", benefits=").append(benefits);
		builder.append(", affiliateId=").append(affiliateId);
		builder.append(", affiliateHouseholdId=").append(affiliateHouseholdId);
		builder.append(", name=").append(name);
		builder.append(", email=").append(email);
		builder.append(", phone=").append(phone);
		builder.append(", isOkToCall=").append(isOkToCall);
		builder.append(", apiKey=").append(apiKey);
		builder.append(", flowId=").append(flowId);
		builder.append(", clickId=").append(clickId);
		builder.append(", aptc=").append(aptc);
		builder.append(", csrLevel=").append(csrLevel);
		builder.append(", allPlansAvailable=").append(allPlansAvailable);
		builder.append(", expectedMonthlyPremium=").append(expectedMonthlyPremium);
		builder.append(", slspPremiumAfterAptc=").append(slspPremiumAfterAptc);
		builder.append(", slspPremiumBeforeAptc=").append(slspPremiumBeforeAptc);
		builder.append(", medicaidEligible=").append(medicaidEligible);
		builder.append(", medicareEligible=").append(medicareEligible);
		builder.append(", chipEligible=").append(chipEligible);
		builder.append(",status=").append(status);
		builder.append(",mandatePenaltyAmount=").append(mandatePenaltyAmount);
		builder.append("]");
		return builder.toString();
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	
	public String getAffiliateOffExchgUrl() {
		return affiliateOffExchgUrl;
	}

	public void setAffiliateOffExchgUrl(String affiliateOffExchgUrl) {
		this.affiliateOffExchgUrl = affiliateOffExchgUrl;
	}
	
	public String getMedicaidHousehold() {
		return medicaidHousehold;
	}

	public void setMedicaidHousehold(String medicaidHousehold) {
		this.medicaidHousehold = medicaidHousehold;
	}
	public String getAffiliateOnExchgUrl() {
		return affiliateOnExchgUrl;
	}

	public void setAffiliateOnExchgUrl(String affiliateOnExchgUrl) {
		this.affiliateOnExchgUrl = affiliateOnExchgUrl;
	}

	public String getAffiliateMedicaidUrl() {
		return affiliateMedicaidUrl;
	}

	public void setAffiliateMedicaidUrl(String affiliateMedicaidUrl) {
		this.affiliateMedicaidUrl = affiliateMedicaidUrl;
	}

	public String getAffiliateMedicareUrl() {
		return affiliateMedicareUrl;
	}

	public void setAffiliateMedicareUrl(String affiliateMedicareUrl) {
		this.affiliateMedicareUrl = affiliateMedicareUrl;
	}
	public String getAffFlowOffExchRedirectionInSameWindow() {
		return affFlowOffExchRedirectionInSameWindow;
	}

	public void setAffFlowOffExchRedirectionInSameWindow(
			String affFlowOffExchRedirectionInSameWindow) {
		this.affFlowOffExchRedirectionInSameWindow = affFlowOffExchRedirectionInSameWindow;
	}

	public String getAffFlowOnExchRedirectionInSameWindow() {
		return affFlowOnExchRedirectionInSameWindow;
	}

	public void setAffFlowOnExchRedirectionInSameWindow(
			String affFlowOnExchRedirectionInSameWindow) {
		this.affFlowOnExchRedirectionInSameWindow = affFlowOnExchRedirectionInSameWindow;
	}

	public String getIsSpouseSelected() {
		return isSpouseSelected;
	}

	public void setIsSpouseSelected(String isSpouseSelected) {
		this.isSpouseSelected = isSpouseSelected;
	}

	public String getShoppingType() {
		return shoppingType;
	}

	public void setShoppingType(String shoppingType) {
		this.shoppingType = shoppingType;
	}
	public boolean isSkipEligibility() {
		return skipEligibility;
	}

	public void setSkipEligibility(boolean skipEligibility) {
		this.skipEligibility = skipEligibility;
	}

	/**
	 * @return the coverageYear
	 */
	public int getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getCensusType() {
		return censusType;
	}

	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}

	public double getEmployerContributionAmount() {
		return employerContributionAmount;
	}

	public void setEmployerContributionAmount(double employerContributionAmount) {
		this.employerContributionAmount = employerContributionAmount;
	}

	public double getHraAmount() {
		return hraAmount;
	}

	public void setHraAmount(double hraAmount) {
		this.hraAmount = hraAmount;
	}

	public double getCobraPremium() {
		return cobraPremium;
	}

	public void setCobraPremium(double cobraPremium) {
		this.cobraPremium = cobraPremium;
	}

	public String getCobraElectionDueDate() {
		return cobraElectionDueDate;
	}

	public void setCobraElectionDueDate(String cobraElectionDueDate) {
		this.cobraElectionDueDate = cobraElectionDueDate;
	}

	public String getHraTransitionDate() {
		return hraTransitionDate;
	}

	public void setHraTransitionDate(String hraTransitionDate) {
		this.hraTransitionDate = hraTransitionDate;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public PhixRetireeHousehold getRetireeData() {
		return retireeData;
	}

	public void setRetireeData(PhixRetireeHousehold retireeData) {
		this.retireeData = retireeData;
	}

	public String getApplicantFirstName() {
		return applicantFirstName;
	}

	public void setApplicantFirstName(String applicantFirstName) {
		this.applicantFirstName = applicantFirstName;
	}

	public String getApplicantLastName() {
		return applicantLastName;
	}

	public void setApplicantLastName(String applicantLastName) {
		this.applicantLastName = applicantLastName;
	}

	public String getLbpPremiumBeforeAptcForTenant() {
		return lbpPremiumBeforeAptcForTenant;
	}

	public void setLbpPremiumBeforeAptcForTenant(String lbpPremiumBeforeAptcForTenant) {
		this.lbpPremiumBeforeAptcForTenant = lbpPremiumBeforeAptcForTenant;
	}

	public String getLspPremiumBeforeAptcForTenant() {
		return lspPremiumBeforeAptcForTenant;
	}

	public void setLspPremiumBeforeAptcForTenant(String lspPremiumBeforeAptcForTenant) {
		this.lspPremiumBeforeAptcForTenant = lspPremiumBeforeAptcForTenant;
	}

	public String getIvrNumber() {
		return ivrNumber;
	}

	public void setIvrNumber(String ivrNumber) {
		this.ivrNumber = ivrNumber;
	}


	public boolean isShowNameOnQuoteform() {
		return showNameOnQuoteform;
	}

	public void setShowNameOnQuoteform(boolean showNameOnQuoteform) {
		this.showNameOnQuoteform = showNameOnQuoteform;
	}
}
