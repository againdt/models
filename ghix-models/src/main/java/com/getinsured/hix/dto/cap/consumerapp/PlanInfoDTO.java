package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class PlanInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4145400660022792452L;
	
	private int id;
	private String enryptedPlanId;
	private String type;
	private String name;
	private String carrierName;
	private String carrierUrl;
	private String metalTier;
	private float monthlyPremium;
	private String producerPassword;
	private String producerUserName;
	private String effectiveDate;
	private String exchangeaType;
	private String networkType;
	private String carrierLogo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEnryptedPlanId() {
		return enryptedPlanId;
	}
	public void setEnryptedPlanId(String enryptedPlanId) {
		this.enryptedPlanId = enryptedPlanId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	public String getCarrierUrl() {
		return carrierUrl;
	}
	public void setCarrierUrl(String carrierUrl) {
		this.carrierUrl = carrierUrl;
	}
	public String getMetalTier() {
		return metalTier;
	}
	public void setMetalTier(String metalTier) {
		this.metalTier = metalTier;
	}
	public float getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(float monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	public String getProducerUserName() {
		return producerUserName;
	}
	public void setProducerUserName(String producerUserName) {
		this.producerUserName = producerUserName;
	}
	public String getProducerPassword() {
		return producerPassword;
	}
	public void setProducerPassword(String producerPassword) {
		this.producerPassword = producerPassword;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExchangeaType() {
		return exchangeaType;
	}
	public void setExchangeaType(String exchangeaType) {
		this.exchangeaType = exchangeaType;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getCarrierLogo() {
		return carrierLogo;
	}
	public void setCarrierLogo(String carrierLogo) {
		this.carrierLogo = carrierLogo;
	}
	
}
