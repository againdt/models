package com.getinsured.hix.model.agency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="REVISION_INFO")
public class RevisionInfo {
	
	@Id
	@Column(name="REVISION_ID")
	@SequenceGenerator(name="REVISION_INFO_ID_GENERATOR", sequenceName="REVISION_INFO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REVISION_INFO_ID_GENERATOR")
	private Long id;
	
	@Column(name="REVISION_TIMESTAMP")
	private long revisionTimestamp;
	
	@Column(name="USER_ID")
	private int userId;
	
	@Column(name="USER_NAME")
	private String userName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getRevisionTimestamp() {
		return revisionTimestamp;
	}

	public void setRevisionTimestamp(long revisionTimestamp) {
		this.revisionTimestamp = revisionTimestamp;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
