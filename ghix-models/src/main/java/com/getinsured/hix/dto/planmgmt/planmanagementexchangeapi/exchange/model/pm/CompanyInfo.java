
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompanyInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompanyInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="companyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fein" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cocode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="groupCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="groupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="phoneExt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stateOfDomicile" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}StateAbbreviation"/>
 *         &lt;element name="address" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}Address"/>
 *         &lt;element name="stateLicense" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "companyName",
    "fein",
    "cocode",
    "groupCode",
    "groupName",
    "phone",
    "phoneExt",
    "fax",
    "stateOfDomicile",
    "address",
    "stateLicense"
})
@XmlRootElement(name="companyInfo")
public class CompanyInfo {

    @XmlElement(required = true)
    protected String companyName;
    @XmlElement(required = true)
    protected String fein;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cocode;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer groupCode;
    @XmlElement(required = true, nillable = true)
    protected String groupName;
    @XmlElement(required = true, nillable = true)
    protected String phone;
    @XmlElement(required = true, nillable = true)
    protected String phoneExt;
    @XmlElement(required = true, nillable = true)
    protected String fax;
    @XmlElement(required = true)
    protected String stateOfDomicile;
    @XmlElement(required = true)
    protected Address address;
    @XmlElement(required = true)
    protected String stateLicense;

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the fein property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFein() {
        return fein;
    }

    /**
     * Sets the value of the fein property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFein(String value) {
        this.fein = value;
    }

    /**
     * Gets the value of the cocode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCocode() {
        return cocode;
    }

    /**
     * Sets the value of the cocode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCocode(Integer value) {
        this.cocode = value;
    }

    /**
     * Gets the value of the groupCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGroupCode() {
        return groupCode;
    }

    /**
     * Sets the value of the groupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGroupCode(Integer value) {
        this.groupCode = value;
    }

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the phoneExt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneExt() {
        return phoneExt;
    }

    /**
     * Sets the value of the phoneExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneExt(String value) {
        this.phoneExt = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the stateOfDomicile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateOfDomicile() {
        return stateOfDomicile;
    }

    /**
     * Sets the value of the stateOfDomicile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateOfDomicile(String value) {
        this.stateOfDomicile = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the stateLicense property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateLicense() {
        return stateLicense;
    }

    /**
     * Sets the value of the stateLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateLicense(String value) {
        this.stateLicense = value;
    }

}
