package com.getinsured.hix.platform.util;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class GiSerializationProxy implements Externalizable{
	
	private Object proxyObj = null;
	private String jsonStr = null;
	private String objClassName = null;
	private static String NO_PROXY_AVAILABLE = "EMPTY";

	private static Logger LOGGER  = LoggerFactory.getLogger(GiSerializationProxy.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GiSerializationProxy(Object obj){
		this.proxyObj = obj;
	}
	
	public GiSerializationProxy(){
	}

	public Object getProxyObj() {
		if(this.proxyObj == null && this.objClassName != null && this.jsonStr != null){
			Class<?> cls = null;
			try{
				cls = Class.forName(this.objClassName);
			}catch(ClassNotFoundException ce){
				LOGGER.error("No proxy object available, check the serialization part, expected Class name, found: "+this.objClassName+", Expected JSON rep, found "+this.jsonStr);
				return null;
			}
			Gson gsonLib = (Gson) GHIXApplicationContext.getBean("platformGson");
			
			this.proxyObj = gsonLib.fromJson(this.jsonStr, cls);
		}
		return proxyObj;
	}

	public void setProxyObj(Object proxyObj) {
		this.proxyObj = proxyObj;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		String json = null;
		if(this.proxyObj != null){
			Gson gsonLib = (Gson) GHIXApplicationContext.getBean("platformGson");
			json = gsonLib.toJson(this.proxyObj);
			out.writeUTF(this.proxyObj.getClass().getName());
			out.writeUTF(json);
		}else{
			out.writeUTF(NO_PROXY_AVAILABLE);
		}
		out.flush();
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.objClassName = in.readUTF();
		if(this.objClassName.equalsIgnoreCase(NO_PROXY_AVAILABLE)){
			LOGGER.warn("No object available to de-serialization proxy");
			return;
		}
		this.jsonStr = in.readUTF();
	}
	
	public Object getProxyObj(Class<?> cls) {
		if(this.proxyObj == null && this.objClassName != null && this.jsonStr != null){
			Gson gsonLib = (Gson) GHIXApplicationContext.getBean("platformGson");
			this.proxyObj = gsonLib.fromJson(this.jsonStr, cls);
		}
		return proxyObj;
	}
}
