
package com.serff.planmanagementserffapi.serff.model.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.serff.planmanagementserffapi.common.model.service.BasicResponse;


public class ObjectFactory {

    private final static QName _UpdateExchangeWorkflowStatusResponse_QNAME = new QName("http://www.serff.com/planManagementSerffApi/serff/model/service", "updateExchangeWorkflowStatusResponse");
    private final static QName _TransferPlanReplyResponse_QNAME = new QName("http://www.serff.com/planManagementSerffApi/serff/model/service", "TransferPlanReplyResponse");
    private final static QName _UpdateExchangeWorkflowStatus_QNAME = new QName("http://www.serff.com/planManagementSerffApi/serff/model/service", "updateExchangeWorkflowStatus");
    private final static QName _TransferPlanReply_QNAME = new QName("http://www.serff.com/planManagementSerffApi/serff/model/service", "TransferPlanReply");
    private final static QName _ValidateAndTransformDataTemplateReplyResponse_QNAME = new QName("http://www.serff.com/planManagementSerffApi/serff/model/service", "validateAndTransformDataTemplateReplyResponse");
    private final static QName _ValidateAndTransformDataTemplateReply_QNAME = new QName("http://www.serff.com/planManagementSerffApi/serff/model/service", "validateAndTransformDataTemplateReply");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.serff.planmanagementserffapi.serff.model.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidateAndTransformDataTemplateReply }
     * 
     */
    public ValidateAndTransformDataTemplateReply createValidateAndTransformDataTemplateReply() {
        return new ValidateAndTransformDataTemplateReply();
    }

    /**
     * Create an instance of {@link UpdateExchangeWorkflowStatusResponse }
     * 
     */
    public UpdateExchangeWorkflowStatusResponse createUpdateExchangeWorkflowStatusResponse() {
        return new UpdateExchangeWorkflowStatusResponse();
    }

    /**
     * Create an instance of {@link TransferPlanReply }
     * 
     */
    public TransferPlanReply createTransferPlanReply() {
        return new TransferPlanReply();
    }

    /**
     * Create an instance of {@link DataTemplateOutcome }
     * 
     */
    public DataTemplateOutcome createDataTemplateOutcome() {
        return new DataTemplateOutcome();
    }

    /**
     * Create an instance of {@link ExchangeAttachment }
     * 
     */
    public ExchangeAttachment createExchangeAttachment() {
        return new ExchangeAttachment();
    }

    /**
     * Create an instance of {@link UpdateExchangeWorkflowStatus }
     * 
     */
    public UpdateExchangeWorkflowStatus createUpdateExchangeWorkflowStatus() {
        return new UpdateExchangeWorkflowStatus();
    }

    /**
     * Create an instance of {@link DataTemplateOutcomes }
     * 
     */
    public DataTemplateOutcomes createDataTemplateOutcomes() {
        return new DataTemplateOutcomes();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateExchangeWorkflowStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/serff/model/service", name = "updateExchangeWorkflowStatusResponse")
    public JAXBElement<UpdateExchangeWorkflowStatusResponse> createUpdateExchangeWorkflowStatusResponse(UpdateExchangeWorkflowStatusResponse value) {
        return new JAXBElement<UpdateExchangeWorkflowStatusResponse>(_UpdateExchangeWorkflowStatusResponse_QNAME, UpdateExchangeWorkflowStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BasicResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/serff/model/service", name = "TransferPlanReplyResponse")
    public JAXBElement<BasicResponse> createTransferPlanReplyResponse(BasicResponse value) {
        return new JAXBElement<BasicResponse>(_TransferPlanReplyResponse_QNAME, BasicResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateExchangeWorkflowStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/serff/model/service", name = "updateExchangeWorkflowStatus")
    public JAXBElement<UpdateExchangeWorkflowStatus> createUpdateExchangeWorkflowStatus(UpdateExchangeWorkflowStatus value) {
        return new JAXBElement<UpdateExchangeWorkflowStatus>(_UpdateExchangeWorkflowStatus_QNAME, UpdateExchangeWorkflowStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferPlanReply }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/serff/model/service", name = "TransferPlanReply")
    public JAXBElement<TransferPlanReply> createTransferPlanReply(TransferPlanReply value) {
        return new JAXBElement<TransferPlanReply>(_TransferPlanReply_QNAME, TransferPlanReply.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BasicResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/serff/model/service", name = "validateAndTransformDataTemplateReplyResponse")
    public JAXBElement<BasicResponse> createValidateAndTransformDataTemplateReplyResponse(BasicResponse value) {
        return new JAXBElement<BasicResponse>(_ValidateAndTransformDataTemplateReplyResponse_QNAME, BasicResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateAndTransformDataTemplateReply }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementSerffApi/serff/model/service", name = "validateAndTransformDataTemplateReply")
    public JAXBElement<ValidateAndTransformDataTemplateReply> createValidateAndTransformDataTemplateReply(ValidateAndTransformDataTemplateReply value) {
        return new JAXBElement<ValidateAndTransformDataTemplateReply>(_ValidateAndTransformDataTemplateReply_QNAME, ValidateAndTransformDataTemplateReply.class, null, value);
    }

}
