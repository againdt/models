package com.getinsured.coveredCA.dto.proxy;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

import com.getinsured.coveredCA.dto.ssap.ExchangeStreamlinedApplication;
/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class CoveredCAProxyResponse {
	public static enum STATUS {
		SUCCESS, FAILURE
	};
	
	public STATUS status;
	public String errorCode;
	public String errorMessage;
	
	public transient Long startTime;
	public Long executionTime;
	
	public String response;
	public ExchangeStreamlinedApplication exchangeStreamlinedApplication;
	
	
	public STATUS getStatus() {
		return status;
	}
	public void setStatus(STATUS status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public ExchangeStreamlinedApplication getExchangeStreamlinedApplication() {
		return exchangeStreamlinedApplication;
	}
	public void setExchangeStreamlinedApplication(
			ExchangeStreamlinedApplication exchangeStreamlinedApplication) {
		this.exchangeStreamlinedApplication = exchangeStreamlinedApplication;
	}
	
	
	public void startResponse() {
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}

	public void endResponse() {
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		this.executionTime = endTime - this.startTime;
	}
}
