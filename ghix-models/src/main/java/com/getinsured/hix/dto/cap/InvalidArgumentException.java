package com.getinsured.hix.dto.cap;

/**
 * Created by kaul_s on 2/26/15.
 */
public class InvalidArgumentException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidArgumentException(Throwable cause) {
        super(cause);
    }
}
