package com.getinsured.hix.dto.plandisplay.ind71g;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class IND71GMember {

    @NotNull(message="memberId" + IND71Enums.REQUIRED_FIELD)
    private String memberId;
	
	@Pattern(regexp=IND71Enums.MRC_REGEX,message="maintenanceReasonCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String maintenanceReasonCode;
    
	@Pattern(regexp=IND71Enums.YES_NO_REGEX,message="newPersonFlag" + IND71Enums.INVALID_REGEX_ERROR)
    private String newPersonFlag;
	
	@Pattern(regexp=IND71Enums.YES_NO_REGEX,message="disenrollMemberFlag" + IND71Enums.INVALID_REGEX_ERROR)
    private String disenrollMemberFlag;

	@Pattern(regexp=IND71Enums.DATE_REGEX,message="deathDate" + IND71Enums.INVALID_REGEX_ERROR)
    private String deathDate;

	@Pattern(regexp=IND71Enums.DATE_REGEX,message="terminationDate" + IND71Enums.INVALID_REGEX_ERROR)
	private String terminationDate;
	
	@Pattern(regexp=IND71Enums.MRC_REGEX,message="terminationReasonCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String terminationReasonCode;
	
	@Valid
    private List<IND71GRelationship> relationships = null;
    
	@Pattern(regexp=IND71Enums.DATE_REGEX,message="dob" + IND71Enums.INVALID_REGEX_ERROR)
    private String dob;
	
	@Pattern(regexp=IND71Enums.YES_NO_REGEX,message="tobacco" + IND71Enums.INVALID_REGEX_ERROR)
    private String tobacco;
	
	@Size(min=IND71Enums.STATE_MIN_LENGTH, max=IND71Enums.STATE_MAX_LENGTH,message="homeState" + IND71Enums.LENGTH_ERROR )
	@Pattern(regexp=IND71Enums.STATE_REGEX,message="homeState" + IND71Enums.INVALID_REGEX_ERROR)
    private String homeState;
	
	@Size(min=IND71Enums.ZIP_MIN_LENGTH, max=IND71Enums.ZIP_MAX_LENGTH,message="responsiblePersonHomeZip" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ZIP_REGEX,message="homeZip" + IND71Enums.INVALID_REGEX_ERROR)
    private String homeZip;
	
	@Size(min=IND71Enums.COUNTY_MIN_LENGTH, max=IND71Enums.COUNTY_MAX_LENGTH,message="countyCode" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.COUNTY_REGEX,message="countyCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String countyCode;
	
	@Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="firstName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="firstName" + IND71Enums.INVALID_REGEX_ERROR)
    private String firstName;
    
	@Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="middleName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="middleName" + IND71Enums.INVALID_REGEX_ERROR)
    private String middleName;

	@Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="lastName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="lastName" + IND71Enums.INVALID_REGEX_ERROR)
    private String lastName;

    @Size(min=IND71Enums.SUFFIX_MIN_LENGTH, max=IND71Enums.SUFFIX_MAX_LENGTH,message="suffix" + IND71Enums.LENGTH_ERROR )
    private String suffix;
    
    @Size(min=IND71Enums.SSN_MIN_LENGTH, max=IND71Enums.SSN_MAX_LENGTH,message="ssn" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.SSN_REGEX,message="ssn" + IND71Enums.INVALID_REGEX_ERROR)
    private String ssn;

    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="primaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="primaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String primaryPhone;

    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="secondaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="secondaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String secondaryPhone;

    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="preferredPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="preferredPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String preferredPhone;

    @Pattern(regexp=IND71Enums.EMAIL_REGEX,message="preferredEmail" + IND71Enums.INVALID_EMAIL_ERROR)
    private String preferredEmail;

    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="homeAddress1" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="homeAddress1" + IND71Enums.INVALID_REGEX_ERROR)
    private String homeAddress1;

    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="homeAddress2" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="homeAddress2" + IND71Enums.INVALID_REGEX_ERROR)
    private String homeAddress2;
    
    @Size(min=IND71Enums.CITY_MIN_LENGTH, max=IND71Enums.CITY_MAX_LENGTH,message="homeCity" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.CITY_REGEX,message="homeCity" + IND71Enums.INVALID_REGEX_ERROR)
    private String homeCity;

    @Pattern(regexp=IND71Enums.GENDER_REGEX,message="genderCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String genderCode;
    
    @Pattern(regexp=IND71Enums.MARITAL_STATUS_REGEX,message="maritalStatusCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String maritalStatusCode;

    @Pattern(regexp=IND71Enums.CITIZENSHIP_STATUS_REGEX,message="citizenshipStatusCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String citizenshipStatusCode;
    
    @Pattern(regexp=IND71Enums.SPOKEN_LANGUAGE_CODE_REGEX,message="spokenLanguageCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String spokenLanguageCode;
    
    @Pattern(regexp=IND71Enums.WRITTEN_LANGUAGE_CODE_REGEX,message="writtenLanguageCode" + IND71Enums.INVALID_REGEX_ERROR)
    private String writtenLanguageCode;
    
    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="mailingAddress1" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="mailingAddress1" + IND71Enums.INVALID_REGEX_ERROR)
    private String mailingAddress1;
    
    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="mailingAddress2" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="mailingAddress2" + IND71Enums.INVALID_REGEX_ERROR)
    private String mailingAddress2;
    
    @Size(min=IND71Enums.CITY_MIN_LENGTH, max=IND71Enums.CITY_MAX_LENGTH,message="mailingCity" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.CITY_REGEX,message="mailingCity" + IND71Enums.INVALID_REGEX_ERROR)
    private String mailingCity;
    
    @Size(min=IND71Enums.STATE_MIN_LENGTH, max=IND71Enums.STATE_MAX_LENGTH,message="mailingState" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.STATE_REGEX,message="mailingState" + IND71Enums.INVALID_REGEX_ERROR)
    private String mailingState;
   
    @Size(min=IND71Enums.ZIP_MIN_LENGTH, max=IND71Enums.ZIP_MAX_LENGTH,message="mailingZip" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ZIP_REGEX,message="mailingZip" + IND71Enums.INVALID_REGEX_ERROR)
    private String mailingZip;
    
    @Pattern(regexp=IND71Enums.YES_NO_REGEX,message="catastrophicEligible" + IND71Enums.INVALID_REGEX_ERROR)
    private String catastrophicEligible;
    
    private List<IND71GRace> race = null;
    private String custodialParentId;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="custodialParentFirstName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="custodialParentFirstName" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentFirstName;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="custodialParentMiddleName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="custodialParentMiddleName" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentMiddleName;
    
    @Size(min=IND71Enums.NAME_MIN_LENGTH, max=IND71Enums.NAME_MAX_LENGTH,message="custodialParentLastName" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.NAME_REGEX,message="custodialParentLastName" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentLastName;
    
    @Size(min=IND71Enums.SUFFIX_MIN_LENGTH, max=IND71Enums.SUFFIX_MAX_LENGTH,message="custodialParentSuffix" + IND71Enums.LENGTH_ERROR )
    private String custodialParentSuffix;
    
    @Size(min=IND71Enums.SSN_MIN_LENGTH, max=IND71Enums.SSN_MAX_LENGTH,message="custodialParentSsn" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.SSN_REGEX,message="custodialParentSsn" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentSsn;

    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="custodialParentPrimaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="custodialParentPrimaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentPrimaryPhone;

    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="custodialParentSecondaryPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="custodialParentSecondaryPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentSecondaryPhone;

    @Size(min=IND71Enums.PHONE_LENGTH, max=IND71Enums.PHONE_LENGTH,message="custodialParentPreferredPhone" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.PHONE_REGEX,message="custodialParentPreferredPhone" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentPreferredPhone;

    @Pattern(regexp=IND71Enums.EMAIL_REGEX,message="custodialParentPreferredEmail" + IND71Enums.INVALID_EMAIL_ERROR)
    private String custodialParentPreferredEmail;

    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="custodialParentHomeAddress1" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="custodialParentHomeAddress1" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentHomeAddress1;

    @Size(min=IND71Enums.ADDRESS_MIN_LENGTH, max=IND71Enums.ADDRESS_MAX_LENGTH,message="custodialParentHomeAddress2" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ADDRESS_REGEX,message="custodialParentHomeAddress2" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentHomeAddress2;

    @Size(min=IND71Enums.CITY_MIN_LENGTH, max=IND71Enums.CITY_MAX_LENGTH,message="custodialParentHomeCity" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.CITY_REGEX,message="custodialParentHomeCity" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentHomeCity;

    @Size(min=IND71Enums.STATE_MIN_LENGTH, max=IND71Enums.STATE_MAX_LENGTH,message="custodialParentHomeState" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.STATE_REGEX,message="custodialParentHomeState" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentHomeState;

    @Size(min=IND71Enums.ZIP_MIN_LENGTH, max=IND71Enums.ZIP_MAX_LENGTH,message="custodialParentHomeZip" + IND71Enums.LENGTH_ERROR )
    @Pattern(regexp=IND71Enums.ZIP_REGEX,message="custodialParentHomeZip" + IND71Enums.INVALID_REGEX_ERROR)
    private String custodialParentHomeZip;

    @Pattern(regexp=IND71Enums.YES_NO_REGEX,message="financialHardshipExemption" + IND71Enums.INVALID_REGEX_ERROR)
    private String financialHardshipExemption;
    
    @Pattern(regexp=IND71Enums.YES_NO_REGEX,message="childOnlyPlanEligibile" + IND71Enums.INVALID_REGEX_ERROR)
    private String childOnlyPlanEligibile;
    
    @Pattern(regexp=IND71Enums.RELATION_REGEX,message="responsiblePersonRelationship" + IND71Enums.INVALID_REGEX_ERROR)
    private String responsiblePersonRelationship;
    
	private String externalMemberId;

    

	public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMaintenanceReasonCode() {
        return maintenanceReasonCode;
    }

    public void setMaintenanceReasonCode(String maintenanceReasonCode) {
        this.maintenanceReasonCode = maintenanceReasonCode;
    }

    public String getNewPersonFlag() {
        return newPersonFlag;
    }

    public void setNewPersonFlag(String newPersonFlag) {
        this.newPersonFlag = newPersonFlag;
    }

    public String getDisenrollMemberFlag() {
        return disenrollMemberFlag;
    }

    public void setDisenrollMemberFlag(String disenrollMemberFlag) {
        this.disenrollMemberFlag = disenrollMemberFlag;
    }

    public String getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(String deathDate) {
        this.deathDate = deathDate;
    }

    public String getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(String terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getTerminationReasonCode() {
        return terminationReasonCode;
    }

    public void setTerminationReasonCode(String terminationReasonCode) {
        this.terminationReasonCode = terminationReasonCode;
    }

    public List<IND71GRelationship> getRelationships() {
        return relationships;
    }

    public void setRelationships(List<IND71GRelationship> relationships) {
        this.relationships = relationships;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTobacco() {
        return tobacco;
    }

    public void setTobacco(String tobacco) {
        this.tobacco = tobacco;
    }

    public String getHomeState() {
        return homeState;
    }

    public void setHomeState(String homeState) {
        this.homeState = homeState;
    }

    public String getHomeZip() {
        return homeZip;
    }

    public void setHomeZip(String homeZip) {
        this.homeZip = homeZip;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getPreferredPhone() {
        return preferredPhone;
    }

    public void setPreferredPhone(String preferredPhone) {
        this.preferredPhone = preferredPhone;
    }

    public String getPreferredEmail() {
        return preferredEmail;
    }

    public void setPreferredEmail(String preferredEmail) {
        this.preferredEmail = preferredEmail;
    }

    public String getHomeAddress1() {
        return homeAddress1;
    }

    public void setHomeAddress1(String homeAddress1) {
        this.homeAddress1 = homeAddress1;
    }

    public String getHomeAddress2() {
        return homeAddress2;
    }

    public void setHomeAddress2(String homeAddress2) {
        this.homeAddress2 = homeAddress2;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public String getMaritalStatusCode() {
        return maritalStatusCode;
    }

    public void setMaritalStatusCode(String maritalStatusCode) {
        this.maritalStatusCode = maritalStatusCode;
    }

    public String getCitizenshipStatusCode() {
        return citizenshipStatusCode;
    }

    public void setCitizenshipStatusCode(String citizenshipStatusCode) {
        this.citizenshipStatusCode = citizenshipStatusCode;
    }

    public String getSpokenLanguageCode() {
        return spokenLanguageCode;
    }

    public void setSpokenLanguageCode(String spokenLanguageCode) {
        this.spokenLanguageCode = spokenLanguageCode;
    }

    public String getWrittenLanguageCode() {
        return writtenLanguageCode;
    }

    public void setWrittenLanguageCode(String writtenLanguageCode) {
        this.writtenLanguageCode = writtenLanguageCode;
    }

    public String getMailingAddress1() {
        return mailingAddress1;
    }

    public void setMailingAddress1(String mailingAddress1) {
        this.mailingAddress1 = mailingAddress1;
    }

    public String getMailingAddress2() {
        return mailingAddress2;
    }

    public void setMailingAddress2(String mailingAddress2) {
        this.mailingAddress2 = mailingAddress2;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingState() {
        return mailingState;
    }

    public void setMailingState(String mailingState) {
        this.mailingState = mailingState;
    }

    public String getMailingZip() {
        return mailingZip;
    }

    public void setMailingZip(String mailingZip) {
        this.mailingZip = mailingZip;
    }

    public String getCatastrophicEligible() {
        return catastrophicEligible;
    }

    public void setCatastrophicEligible(String catastrophicEligible) {
        this.catastrophicEligible = catastrophicEligible;
    }

    public List<IND71GRace> getRace() {
        return race;
    }

    public void setRace(List<IND71GRace> race) {
        this.race = race;
    }

    public String getCustodialParentId() {
        return custodialParentId;
    }

    public void setCustodialParentId(String custodialParentId) {
        this.custodialParentId = custodialParentId;
    }

    public String getCustodialParentFirstName() {
        return custodialParentFirstName;
    }

    public void setCustodialParentFirstName(String custodialParentFirstName) {
        this.custodialParentFirstName = custodialParentFirstName;
    }

    public String getCustodialParentMiddleName() {
        return custodialParentMiddleName;
    }

    public void setCustodialParentMiddleName(String custodialParentMiddleName) {
        this.custodialParentMiddleName = custodialParentMiddleName;
    }

    public String getCustodialParentLastName() {
        return custodialParentLastName;
    }

    public void setCustodialParentLastName(String custodialParentLastName) {
        this.custodialParentLastName = custodialParentLastName;
    }

    public String getCustodialParentSuffix() {
        return custodialParentSuffix;
    }

    public void setCustodialParentSuffix(String custodialParentSuffix) {
        this.custodialParentSuffix = custodialParentSuffix;
    }

    public String getCustodialParentSsn() {
        return custodialParentSsn;
    }

    public void setCustodialParentSsn(String custodialParentSsn) {
        this.custodialParentSsn = custodialParentSsn;
    }

    public String getCustodialParentPrimaryPhone() {
        return custodialParentPrimaryPhone;
    }

    public void setCustodialParentPrimaryPhone(String custodialParentPrimaryPhone) {
        this.custodialParentPrimaryPhone = custodialParentPrimaryPhone;
    }

    public String getCustodialParentSecondaryPhone() {
        return custodialParentSecondaryPhone;
    }

    public void setCustodialParentSecondaryPhone(String custodialParentSecondaryPhone) {
        this.custodialParentSecondaryPhone = custodialParentSecondaryPhone;
    }

    public String getCustodialParentPreferredPhone() {
        return custodialParentPreferredPhone;
    }

    public void setCustodialParentPreferredPhone(String custodialParentPreferredPhone) {
        this.custodialParentPreferredPhone = custodialParentPreferredPhone;
    }

    public String getCustodialParentPreferredEmail() {
        return custodialParentPreferredEmail;
    }

    public void setCustodialParentPreferredEmail(String custodialParentPreferredEmail) {
        this.custodialParentPreferredEmail = custodialParentPreferredEmail;
    }

    public String getCustodialParentHomeAddress1() {
        return custodialParentHomeAddress1;
    }

    public void setCustodialParentHomeAddress1(String custodialParentHomeAddress1) {
        this.custodialParentHomeAddress1 = custodialParentHomeAddress1;
    }

    public String getCustodialParentHomeAddress2() {
        return custodialParentHomeAddress2;
    }

    public void setCustodialParentHomeAddress2(String custodialParentHomeAddress2) {
        this.custodialParentHomeAddress2 = custodialParentHomeAddress2;
    }

    public String getCustodialParentHomeCity() {
        return custodialParentHomeCity;
    }

    public void setCustodialParentHomeCity(String custodialParentHomeCity) {
        this.custodialParentHomeCity = custodialParentHomeCity;
    }

    public String getCustodialParentHomeState() {
        return custodialParentHomeState;
    }

    public void setCustodialParentHomeState(String custodialParentHomeState) {
        this.custodialParentHomeState = custodialParentHomeState;
    }

    public String getCustodialParentHomeZip() {
        return custodialParentHomeZip;
    }

    public void setCustodialParentHomeZip(String custodialParentHomeZip) {
        this.custodialParentHomeZip = custodialParentHomeZip;
    }

    public String getFinancialHardshipExemption() {
        return financialHardshipExemption;
    }

    public void setFinancialHardshipExemption(String financialHardshipExemption) {
        this.financialHardshipExemption = financialHardshipExemption;
    }

    public String getChildOnlyPlanEligibile() {
        return childOnlyPlanEligibile;
    }

    public void setChildOnlyPlanEligibile(String childOnlyPlanEligibile) {
        this.childOnlyPlanEligibile = childOnlyPlanEligibile;
    }
    
    public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}
	
	public String getResponsiblePersonRelationship() {
		return responsiblePersonRelationship;
	}

	public void setResponsiblePersonRelationship(String responsiblePersonRelationship) {
		this.responsiblePersonRelationship = responsiblePersonRelationship;
	}

}
