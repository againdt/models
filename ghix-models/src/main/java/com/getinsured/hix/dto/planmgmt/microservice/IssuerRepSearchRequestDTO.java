package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerRepSearchRequestDTO extends SearchDTO {

	private IssuerRepDTO issuerRepDTO;

	public IssuerRepSearchRequestDTO() {
		super();
	}

	public IssuerRepDTO getIssuerRepDTO() {
		return issuerRepDTO;
	}

	public void setIssuerRepDTO(IssuerRepDTO issuerRepDTO) {
		this.issuerRepDTO = issuerRepDTO;
	}
}
