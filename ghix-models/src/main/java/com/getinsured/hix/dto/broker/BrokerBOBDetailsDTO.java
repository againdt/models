package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class BrokerBOBDetailsDTO implements Serializable, Cloneable, Comparable<BrokerBOBDetailsDTO> {
	private static final long serialVersionUID = 1L;

	private String sortBy;
	private String sortOrder;
	private Integer startRecord;
	private Integer pageSize;
	private Integer totalCount;
	
	private int brokerId;
	private int individualId;
	private int planId;
	private String hiosPlanId;
	private int cecId;
	private int ssapApplicationId;
	private String firstName;
	private String lastName;
	private String applicationType;
	private String applicationStatus;
	private String city;
	private String state;
	private String zipCode;
	private String county;
	private String emailAddress;
	private String phoneNumber;
	private String address1;
	private String address2;
	private String nextStep;
	private String nextStepLink;
	private String dueDate;
	private String officeVisit;
	private String deductible;
	private String expenseEstimate;
	private String genericDrugs;
	private String designationStatus;
	private String issuerLogo;
	private String planName;
	private String planType;
	private String insuranceType;
	private String premium;
	private String userName;
	private String isEnrolled;
	private List<Integer> households;
	private String financialAssistanceFlag;
	private String currentStatus;
	private String enrollmentEndDate;
	private String encryptedIndividualId;
	private String showSwitchRolePopup;
	private String eligibilityStatus;
	private String exchangeEligibilityStatus;
	private String applicationData;
	private Double maxAPTC;
	private String issuerName;
	private String caseNumber;
	private boolean isUserExists;
	private String csrLevel;
	private Long coverageYear;
	private String grossPremiumAmt;
	private String aptcAmount;
	private String applicationDate;
	private String birthDateString;
	private String ssn;
	
	//IND72 & 73 fields
	private int noOfHouseholdMembers;
	private String activeSinceFrmDt;
	private String inactiveSinceFrmDt;
	private String middleName;
	private String requestSentDt;
	private String ecFirstName;
	private String ecLastName;
	private String applicationYear;
	private String enrollmentStatus;
	private String applicationValidationStatus;
	
	public String getApplicationValidationStatus() {
		return applicationValidationStatus;
	}

	public void setApplicationValidationStatus(String applicationValidationStatus) {
		this.applicationValidationStatus = applicationValidationStatus;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getBirthDateString() {
		return birthDateString;
	}

	public void setBirthDateString(String birthDateString) {
		this.birthDateString = birthDateString;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getHiosPlanId() {
		return hiosPlanId;
	}

	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}

	public int getCecId() {
		return cecId;
	}

	public void setCecId(int cecId) {
		this.cecId = cecId;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getActiveSinceFrmDt() {
		return activeSinceFrmDt;
	}

	public void setActiveSinceFrmDt(String activeSinceFrmDt) {
		this.activeSinceFrmDt = activeSinceFrmDt;
	}

	public String getInactiveSinceFrmDt() {
		return inactiveSinceFrmDt;
	}

	public void setInactiveSinceFrmDt(String inactiveSinceFrmDt) {
		this.inactiveSinceFrmDt = inactiveSinceFrmDt;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getRequestSentDt() {
		return requestSentDt;
	}

	public void setRequestSentDt(String requestSentDt) {
		this.requestSentDt = requestSentDt;
	}

	public int getNoOfHouseholdMembers() {
		return noOfHouseholdMembers;
	}

	public void setNoOfHouseholdMembers(int noOfHouseholdMembers) {
		this.noOfHouseholdMembers = noOfHouseholdMembers;
	}

	public Long getCoverageYear() {
		  return coverageYear;
	 }
	 
	 public void setCoverageYear(Long coverageYear) {
		 this.coverageYear = coverageYear;
	 }
	 
	public String getCsrLevel() {
		return csrLevel;
	}
	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public Double getMaxAPTC() {
		return maxAPTC;
	}
	public void setMaxAPTC(Double maxAPTC) {
		this.maxAPTC = maxAPTC;
	}
	public String getApplicationData() {
		return applicationData;
	}
	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}
	public String getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}
	public void setExchangeEligibilityStatus(String exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public int getIndividualId() {
		return individualId;
	}
	public void setIndividualId(int individualId) {
		this.individualId = individualId;
	}
	
	public int getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(int brokerId) {
		this.brokerId = brokerId;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getNextStep() {
		return nextStep;
	}
	public void setNextStep(String nextStep) {
		this.nextStep = nextStep;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getExpenseEstimate() {
		return expenseEstimate;
	}
	public void setExpenseEstimate(String expenseEstimate) {
		this.expenseEstimate = expenseEstimate;
	}
	public String getGenericDrugs() {
		return genericDrugs;
	}
	public void setGenericDrugs(String genericDrugs) {
		this.genericDrugs = genericDrugs;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Integer getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(Integer startRecord) {
		this.startRecord = startRecord;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getDesignationStatus() {
		return designationStatus;
	}
	public void setDesignationStatus(String designationStatus) {
		this.designationStatus = designationStatus;
	}
	public int getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(int ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPremium() {
		return premium;
	}
	public void setPremium(String premium) {
		this.premium = premium;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIsEnrolled() {
		return isEnrolled;
	}
	public void setIsEnrolled(String isEnrolled) {
		this.isEnrolled = isEnrolled;
	}
	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}
	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}
	public List<Integer> getHouseholds() {
		return households;
	}
	public void setHouseholds(List<Integer> households) {
		this.households = households;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getNextStepLink() {
		return nextStepLink;
	}
	public void setNextStepLink(String nextStepLink) {
		this.nextStepLink = nextStepLink;
	}
	public String getEnrollmentEndDate() {
		return enrollmentEndDate;
	}
	public void setEnrollmentEndDate(String enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}
	public String getEncryptedIndividualId() {
		return encryptedIndividualId;
	}
	public void setEncryptedIndividualId(String encryptedIndividualId) {
		this.encryptedIndividualId = encryptedIndividualId;
	}
	
	public String getShowSwitchRolePopup() {
		return showSwitchRolePopup;
	}
	public void setShowSwitchRolePopup(String showSwitchRolePopup) {
		this.showSwitchRolePopup = showSwitchRolePopup;
	}
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
	public String getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	public void setGrossPremiumAmt(String grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	public String getAptcAmount() {
		return aptcAmount;
	}

	public void setAptcAmount(String aptcAmount) {
		this.aptcAmount = aptcAmount;
	}

	@Override
	public int compareTo(BrokerBOBDetailsDTO o) {
		 return this.individualId - o.individualId;
	}

	public boolean getIsUserExists() {
		return isUserExists;
	}
	public void setIsUserExists(boolean isUserExists) {
		this.isUserExists = isUserExists;
	}

	public static final Comparator<BrokerBOBDetailsDTO> FIRST_NAME_COMPARATOR = new Comparator<BrokerBOBDetailsDTO>(){
        @Override
        public int compare(BrokerBOBDetailsDTO o1, BrokerBOBDetailsDTO o2) {
        	int status = 0;
        	if ((o1.firstName == null) || ("".equals(o1.firstName))) {
            	status = 1;
            } else if ((o2.firstName == null) || ("".equals(o2.firstName))) {
            	status = -1;
            } else if (o1.firstName.equalsIgnoreCase(o2.firstName)) {
        		return compareLastName(o1, o2);
        	} else {
            	status = o1.firstName.toUpperCase().compareTo(o2.firstName.toUpperCase());
            }

        	return status;
        }
        
        private int compareLastName(BrokerBOBDetailsDTO o1, BrokerBOBDetailsDTO o2) {
        	int status = 0;
        	if ((o1.lastName == null) || ("".equals(o1.lastName))) {
            	status = 1;
            } else if ((o2.lastName == null) || ("".equals(o2.lastName))) {
            	status = -1;
            } else if (o1.lastName.equalsIgnoreCase(o2.lastName)) {
        		status = 0;
            } else {
            	status = o1.lastName.toUpperCase().compareTo(o2.lastName.toUpperCase());
            }

        	return status;
        }
    };

    public static final Comparator<BrokerBOBDetailsDTO> LAST_NAME_COMPARATOR = new Comparator<BrokerBOBDetailsDTO>(){
        @Override
        public int compare(BrokerBOBDetailsDTO o1, BrokerBOBDetailsDTO o2) {
        	int status = 0;
        	if ((o1.lastName == null) || ("".equals(o1.lastName))) {
            	status = 1;
            } else if ((o2.lastName == null) || ("".equals(o2.lastName))) {
            	status = -1;
            } else if (o1.lastName.equalsIgnoreCase(o2.lastName)) {
        		return compareFirstName(o1, o2);
        	} else {
            	status = o1.lastName.toUpperCase().compareTo(o2.lastName.toUpperCase());
            }

        	return status;
        }
        
        private int compareFirstName(BrokerBOBDetailsDTO o1, BrokerBOBDetailsDTO o2) {
        	int status = 0;
        	if ((o1.firstName == null) || ("".equals(o1.firstName))) {
            	status = 1;
            } else if ((o2.firstName == null) || ("".equals(o2.firstName))) {
            	status = -1;
            } else if (o1.firstName.equalsIgnoreCase(o2.firstName)) {
        		status = 0;
            } else {
            	status = o1.firstName.toUpperCase().compareTo(o2.firstName.toUpperCase());
            }

        	return status;
        }
    };
    
	public static final Comparator<BrokerBOBDetailsDTO> DUE_DATE_COMPARATOR = new Comparator<BrokerBOBDetailsDTO>(){
		private static final String UI_DATE_FORMAT = "MMM dd, yyyy";
		
        @Override
        public int compare(BrokerBOBDetailsDTO o1, BrokerBOBDetailsDTO o2) {
        	int status = 0;
        	String date1 = o1.dueDate;
        	String date2 = o2.dueDate;
        	
        	try {
	        	if ("N/A".equalsIgnoreCase(date1) && "N/A".equalsIgnoreCase(date2)){
	        		status = 0;
	       	 	} else if("N/A".equalsIgnoreCase(date1)){
	       	 		status = -1;
	       	 	} else if("N/A".equalsIgnoreCase(date2)){
	       	 		status = 1;
	       	 	} else {
					Date d1 = new SimpleDateFormat(UI_DATE_FORMAT).parse(date1);
					Date d2 = new SimpleDateFormat(UI_DATE_FORMAT).parse(date2);
					
					status = d1.compareTo(d2); 
	       	 	}
        	} catch (ParseException e) {
				e.printStackTrace();
			}
        	return status;
        }
    };

    public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public String getEcFirstName() {
		return ecFirstName;
	}

	public void setEcFirstName(String ecFirstName) {
		this.ecFirstName = ecFirstName;
	}

	public String getEcLastName() {
		return ecLastName;
	}

	public void setEcLastName(String ecLastName) {
		this.ecLastName = ecLastName;
	}

	public String getApplicationYear() {
		return applicationYear;
	}

	public void setApplicationYear(String applicationYear) {
		this.applicationYear = applicationYear;
	}

}
