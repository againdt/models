/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author rajaramesh_g
 *
 */
public class EnrolleeRequest implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer employeeId;
    private Integer employeeApplicationId;
    private String enrollmentId;
    private Long ssapApplicationId;
    /*
     * Added for performance task
     */
    private String callType;
    
    private Integer enrolleeId;
    private Date lastInvoiceTimestamp;

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the employeeApplicationId
	 */
	public Integer getEmployeeApplicationId() {
		return employeeApplicationId;
	}

	/**
	 * @param employeeApplicationId the employeeApplicationId to set
	 */
	public void setEmployeeApplicationId(Integer employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}

	/**
	 * @return the ssapApplicationId
	 */
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	/**
	 * @param ssapApplicationId the ssapApplicationId to set
	 */
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	/**
	 * @return the enrolleeId
	 */
	public Integer getEnrolleeId() {
		return enrolleeId;
	}

	/**
	 * @param enrolleeId the enrolleeId to set
	 */
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}

	/**
	 * @return the lastInvoiceTimestamp
	 */
	public Date getLastInvoiceTimestamp() {
		return lastInvoiceTimestamp;
	}

	/**
	 * @param lastInvoiceTimestamp the lastInvoiceTimestamp to set
	 */
	public void setLastInvoiceTimestamp(Date lastInvoiceTimestamp) {
		this.lastInvoiceTimestamp = lastInvoiceTimestamp;
	}

	/**
	 * @return the callType
	 */
	public String getCallType() {
		return callType;
	}

	/**
	 * @param callType the callType to set
	 */
	public void setCallType(String callType) {
		this.callType = callType;
	}
}
