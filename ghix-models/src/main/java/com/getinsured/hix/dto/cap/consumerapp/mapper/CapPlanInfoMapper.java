package com.getinsured.hix.dto.cap.consumerapp.mapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.dto.cap.consumerapp.PlanInfoDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.VimoEncryptor;

public class CapPlanInfoMapper {

	@Autowired VimoEncryptor vimoEncryptor;
	
	public PlanInfoDTO getDtoFromObject(Plan plan) {
		PlanInfoDTO dto = null;
		
		if(plan !=null) {
			dto =  new PlanInfoDTO();
			
			dto.setId(plan.getId());
//			dto.setMetalTier(plan.getIssuerPlanNumber());
//			dto.setMonthlyPremium(plan.getPlanRate())
			dto.setName(plan.getName());
			dto.setType(plan.getInsuranceType());
			
			Issuer issuer = plan.getIssuer();
			if(issuer != null) {
				dto.setCarrierName(issuer.getName());
				dto.setCarrierUrl(issuer.getApplicationUrl());
				dto.setProducerUserName(issuer.getProducerUserName());
				if(!StringUtils.isEmpty(issuer.getProducerPassword())) {
					dto.setProducerPassword(issuer.getProducerPassword());
				}
			}
		}
				
		return dto;
	}

}
