package com.getinsured.hix.dto.entity;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.getinsured.hix.model.entity.Assister;

public class MapKeySerializer extends JsonSerializer<Assister>{
	 //private static final SerializerBase<Object> DEFAULT = new StdKeySerializer();
	 private static final ObjectMapper mapper = new ObjectMapper();

	@Override
	public void serialize(Assister value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		String json = mapper.writeValueAsString( value );
		jgen.writeFieldName( json );
	}
	 
}
