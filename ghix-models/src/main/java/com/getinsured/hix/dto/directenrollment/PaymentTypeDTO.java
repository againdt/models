package com.getinsured.hix.dto.directenrollment;

import java.util.List;
import java.util.ArrayList;


/**
 * 
 * @author Vijay
 *
 */
public class PaymentTypeDTO {
	private String paymentType;	
	private List<String> subTypes;	
	
	public PaymentTypeDTO() {
		super();
		subTypes = new ArrayList<String>();
	}


	public PaymentTypeDTO(String paymentType, List<String> subTypes) {
		super();
		this.paymentType = paymentType;		
		this.subTypes = subTypes;		
	}

	@Override
	public String toString() {
		return "PaymentTypeDTO [paymentType=" + paymentType + ", subTypes=" + subTypes + "]";
	}


	public String getPaymentType() {
		return paymentType;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


	public List<String> getSubTypes() {
		return subTypes;
	}


	public void setSubTypes(List<String> subTypes) {
		this.subTypes = subTypes;
	}	
}