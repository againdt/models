package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.eligibility.model.SepEvents;


/**
 * The persistent class for the SSAP_APPLICATION_EVENTS database table.
 * 
 */
@Audited
@Entity
@Table(name="SSAP_APPLICANT_EVENTS")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="SsapApplicantEvent.findAll", query="SELECT s FROM SsapApplicantEvent s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SsapApplicantEvent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum EventPrecedenceIndicator { Y, N }
	
	public enum ApplicantEventValidationStatus {
		INITIAL, NOTREQUIRED, PENDING, SUBMITTED, REJECTED, VERIFIED, CANCELLED, OVERRIDDEN, PENDING_INFO;
		
		private static final Map<ApplicantEventValidationStatus, Set<ApplicantEventValidationStatus>> NEXT_STATUS_WORKFLOW =
				new HashMap<ApplicantEventValidationStatus, Set<ApplicantEventValidationStatus>>(){
			
			private static final long serialVersionUID = 5813552356764345527L;
			{
				put(PENDING, EnumSet.of(SUBMITTED, CANCELLED, OVERRIDDEN));
				put(SUBMITTED, EnumSet.of(REJECTED, VERIFIED, CANCELLED, OVERRIDDEN));
				put(REJECTED, EnumSet.of(SUBMITTED, CANCELLED, OVERRIDDEN));
			}
		};
		
		public boolean isNextStatusValid(ApplicantEventValidationStatus nextStatus){
			
			if (this == nextStatus){
				return false;
			}
			
			if ((this == VERIFIED || this == CANCELLED  || this == OVERRIDDEN ) && nextStatus != null){
				return false;
			}
			
			if (NEXT_STATUS_WORKFLOW.containsKey(this)){
				if (NEXT_STATUS_WORKFLOW.get(this).contains(nextStatus)){
					return true;
				}
			}
			return false;
			
		}
	}

	@Id
	@SequenceGenerator(name="SSAP_APPLICANT_EVENTS_ID_GENERATOR", sequenceName="SSAP_APPLICANT_EVENTS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICANT_EVENTS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@NotAudited
	@ManyToOne
	@JoinColumn(name="SSAP_APPLICATION_EVENT_ID", nullable=false)
	private SsapApplicationEvent ssapAplicationEvent;

	@NotAudited
	@ManyToOne
	@JoinColumn(name="SSAP_APPLICANT_ID", nullable=false)
	private SsapApplicant ssapApplicant;
	
	@NotAudited
	@Column(name="COVERAGE_START_DATE")
	private Timestamp coverageStartDate;
	
	@NotAudited
	@Column(name="COVERAGE_END_DATE")
	private Timestamp coverageEndDate;
	
	@Column(name="EVENT_DATE")
	private Timestamp eventDate;
	
	@Column(name="REPORT_START_DATE")
	private Timestamp reportStartDate;
	
	@Column(name="REPORT_END_DATE")
	private Timestamp reportEndDate;
	
	@Column(name="ENROLLMENT_START_DATE")
	private Timestamp enrollmentStartDate;
	
	@Column(name="ENROLLMENT_END_DATE")
	private Timestamp enrollmentEndDate;
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name="CREATION_TIMESTAMP")
	private Timestamp createdTimeStamp;
	
	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;
	
	@NotAudited
	@OneToOne
	@JoinColumn(name="SEP_ENROLLMENT_EVENTS_ID")
	private SepEvents sepEvents;
	
	@NotAudited
	@Column(name="EVENT_PRECEDENCE_INDICATOR")
	@Enumerated(EnumType.STRING)
	private EventPrecedenceIndicator eventPrecedenceIndicator;
	
	@Column(name="VALIDATION_STATUS")
	@Enumerated(EnumType.STRING)
	private ApplicantEventValidationStatus validationStatus;
	
	@Column(name="REAL_TIME_STATUS")
	private String realTimeStatus;

	public EventPrecedenceIndicator getEventPrecedenceIndicator() {
		return eventPrecedenceIndicator;
	}

	public void setEventPrecedenceIndicator(
			EventPrecedenceIndicator eventPrecedenceIndicator) {
		this.eventPrecedenceIndicator = eventPrecedenceIndicator;
	}

	public SsapApplicantEvent() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SsapApplicationEvent getSsapAplicationEvent() {
		return ssapAplicationEvent;
	}

	public void setSsapAplicationEvent(SsapApplicationEvent ssapAplicationEvent) {
		this.ssapAplicationEvent = ssapAplicationEvent;
	}

	public SsapApplicant getSsapApplicant() {
		return ssapApplicant;
	}

	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}
	

	public Timestamp getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Timestamp coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Timestamp getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(Timestamp coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public Timestamp getEventDate() {
		return eventDate;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public Timestamp getReportStartDate() {
		return reportStartDate;
	}

	public void setReportStartDate(Timestamp reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	public Timestamp getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(Timestamp reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public Timestamp getEnrollmentStartDate() {
		return enrollmentStartDate;
	}

	public void setEnrollmentStartDate(Timestamp enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}

	public Timestamp getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Timestamp enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public SepEvents getSepEvents() {
		return sepEvents;
	}

	public void setSepEvents(SepEvents sepEvents) {
		this.sepEvents = sepEvents;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		// Add a comment to this line
		this.setCreatedTimeStamp(new TSTimestamp());
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	public ApplicantEventValidationStatus getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(ApplicantEventValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}

	public String getRealTimeStatus() {
		return realTimeStatus;
	}

	public void setRealTimeStatus(String realTimeStatus) {
		this.realTimeStatus = realTimeStatus;
	}
	
}
