package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="STATE_PLAN")
public class StatePlan implements Serializable{
	
	
	@Override
	public String toString() {
		return "StatePlan [id=" + id + ", state=" + state + ", specificPlan="
				+ specificPlan + ", zipCode=" + zipCode + ", quotingEngine="
				+ quotingEngine + "]";
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STATE_PLAN_SEQ")
	@SequenceGenerator(name = "STATE_PLAN_SEQ", sequenceName = "STATE_PLAN_SEQ", allocationSize = 1)
	private int id;	
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="SPECIFIC_PLAN")
	private String specificPlan;
	
	@Column(name="ZIP_CODE ")
	private int zipCode;
	
	@Column(name="QUOTING_ENGINE")
	private String quotingEngine;
	
	@Column(name="PLAN_ID")
	private String planId;
	
	@Column(name="COUNTY")
	private String county;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSpecificPlan() {
		return specificPlan;
	}

	public void setSpecificPlan(String specificPlan) {
		this.specificPlan = specificPlan;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getQuotingEngine() {
		return quotingEngine;
	}

	public void setQuotingEngine(String quotingEngine) {
		this.quotingEngine = quotingEngine;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

}
