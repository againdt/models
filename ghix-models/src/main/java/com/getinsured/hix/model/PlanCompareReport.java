package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PM_PLAN_COMP_REPORT")
public class PlanCompareReport {
	
	public static enum PlanReportStatus {
		INQUEUE, INPROGRESS, COMPLETE, ERROR;
	}
	
	public static enum PlanReportType {
		//Q: QHP Report, S: SADP Report
		Q, S;
	}
	
	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_plan_comp_report_seq")
	@SequenceGenerator(name = "pm_plan_comp_report_seq", sequenceName = "pm_plan_comp_report_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "PLAN_IDS", length = 2000)
	private String planIds;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ISSUER_ID")
	private Issuer issuer;
	
	@Column(name = "STATUS", length = 40)
	private String planReportStatus;
	
	@Column(name = "REPORT_TYPE", length = 1)
	private String planReportType;
	
	@Column(name = "ECM_DOC_ID", length = 400)
	private String ecmDocId;
	
	@Column(name = "ECM_DOC_NAME", length = 400)
	private String ecmDocName;
	
	@Column(name = "REPORT_EXCEPTION", length = 2000)
	private String planReportExceptions;
	
	@Column(name = "IS_DELETED", length = 1)
	private String isDeleted;
	
	@Column(name = "CREATED_BY")
	private Integer createdBy;
		
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;
		
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;
	
	@Column(name = "DELETED_BY")
	private Integer deletedBy;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the planIds
	 */
	public String getPlanIds() {
		return planIds;
	}

	/**
	 * @param planIds the planIds to set
	 */
	public void setPlanIds(String planIds) {
		this.planIds = planIds;
	}

	/**
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * @return the planReportStatus
	 */
	public String getPlanReportStatus() {
		return planReportStatus;
	}

	/**
	 * @param planReportStatus the planReportStatus to set
	 */
	public void setPlanReportStatus(String planReportStatus) {
		this.planReportStatus = planReportStatus;
	}

	/**
	 * @return the planReportType
	 */
	public String getPlanReportType() {
		return planReportType;
	}

	/**
	 * @param planReportType the planReportType to set
	 */
	public void setPlanReportType(String planReportType) {
		this.planReportType = planReportType;
	}

	/**
	 * @return the ecmDocId
	 */
	public String getEcmDocId() {
		return ecmDocId;
	}

	/**
	 * @param ecmDocId the ecmDocId to set
	 */
	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}

	/**
	 * @return the ecmDocName
	 */
	public String getEcmDocName() {
		return ecmDocName;
	}

	/**
	 * @param ecmDocName the ecmDocName to set
	 */
	public void setEcmDocName(String ecmDocName) {
		this.ecmDocName = ecmDocName;
	}

	/**
	 * @return the planReportExceptions
	 */
	public String getPlanReportExceptions() {
		return planReportExceptions;
	}

	/**
	 * @param planReportExceptions the planReportExceptions to set
	 */
	public void setPlanReportExceptions(String planReportExceptions) {
		this.planReportExceptions = planReportExceptions;
	}

	/**
	 * @return the isDeleted
	 */
	public String getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the creationTimestamp
	 */
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	/**
	 * @return the lastUpdateTimestamp
	 */
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	/**
	 * @param updationTimestamp the lastUpdateTimestamp to set
	 */
	public void setLastUpdateTimestamp(Date updationTimestamp) {
		this.lastUpdateTimestamp = updationTimestamp;
	}

	/**
	 * @return the deletedBy
	 */
	public Integer getDeletedBy() {
		return deletedBy;
	}

	/**
	 * @param deletedBy the deletedBy to set
	 */
	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}
}
