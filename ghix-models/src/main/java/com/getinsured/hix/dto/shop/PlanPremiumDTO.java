package com.getinsured.hix.dto.shop;

public class PlanPremiumDTO {
	
	private float premiumAmt;
	private Integer planId;
	
	public PlanPremiumDTO(Integer planId,float premiumAmt) {
		super();
		this.premiumAmt = premiumAmt;
		this.planId = planId;
	}
	
	public float getPremiumAmt() {
		return premiumAmt;
	}
	public void setPremiumAmt(float premiumAmt) {
		this.premiumAmt = premiumAmt;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	
	

}
