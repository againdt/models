package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;


public class CreditCard implements Serializable{
	private static final long serialVersionUID = 1L;
	private String number;
	private String expirationMonth;
	private String expirationYear;
	private String cvv;
	private String zip;
	private String nameOnCard;
	private String type;
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getExpirationMonth() {
		return expirationMonth;
	}
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	public String getExpirationYear() {
		return expirationYear;
	}
	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public static CreditCard getSampleInstance(){
		CreditCard cc = new CreditCard();
		cc.setCvv("234");
		cc.setExpirationMonth("03");
		cc.setExpirationYear("2018");
		cc.setNameOnCard("Marc Cooperson");
		cc.setNumber("4111222233334444");
		cc.setZip("94040");
		cc.setType("visa");
		return cc;
	}

}
