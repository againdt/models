package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class Enrollment834ResendDTO implements Serializable{
	
	
	public static enum EVENT_TYPE {
		ALL, LAST;
	}

	List<Integer> enrollmentIds;
	 EVENT_TYPE eventType;
	String comments;
	public List<Integer> getEnrollmentIds() {
		return enrollmentIds;
	}
	public void setEnrollmentIds(List<Integer> enrollmentIds) {
		this.enrollmentIds = enrollmentIds;
	}
	public EVENT_TYPE getEventType() {
		return eventType;
	}
	public void setEventType(EVENT_TYPE eventType) {
		this.eventType = eventType;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
}
