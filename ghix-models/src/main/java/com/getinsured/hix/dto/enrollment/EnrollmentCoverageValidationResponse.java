/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @author negi_s
 *
 */
public class EnrollmentCoverageValidationResponse implements Serializable{

	private String memberId;
	private Integer enrollmentId;
	private String issuerName;
	private String planName;
	private String coverageStartDate;
	private String coverageEndDate;
	/**
	 * @return the memberId
	 */
	public String getMemberId() {
		return memberId;
	}
	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	
	/**
	 * @return the coverageStartDate
	 */
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	/**
	 * @param coverageStartDate the coverageStartDate to set
	 */
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	/**
	 * @return the coverageEndDate
	 */
	public String getCoverageEndDate() {
		return coverageEndDate;
	}
	/**
	 * @param coverageEndDate the coverageEndDate to set
	 */
	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	
	public EnrollmentCoverageValidationResponse(String memberId, Integer enrollmentId,
			String issuerName, String planName, String coverageStartDate, String coverageEndDate) {
		super();
		this.memberId = memberId;
		this.enrollmentId = enrollmentId;
		this.issuerName = issuerName;
		this.planName = planName;
		this.coverageStartDate = coverageStartDate;
		this.coverageEndDate = coverageEndDate;
	}
}
