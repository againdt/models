package com.getinsured.hix.dto.prescription;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DrugPlanListJsonDTO {
	@SerializedName("drug_name")
	public String drugName;
	@SerializedName("plans")
	public List<DrugPlanDTO> plans = null;
	@SerializedName("rxnorm_id")
	public String rxnormId;

	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public List<DrugPlanDTO> getPlans() {
		return plans;
	}
	public void setPlans(List<DrugPlanDTO> plans) {
		this.plans = plans;
	}
	public String getRxnormId() {
		return rxnormId;
	}
	public void setRxnormId(String rxnormId) {
		this.rxnormId = rxnormId;
	}
}
