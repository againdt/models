package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name="SSAP_INTEGRATION_LOGS")
@DynamicInsert
@DynamicUpdate
public class SsapIntegrationLog implements Serializable {

	private static final long serialVersionUID = 9066274895730514214L;
	
	@Id
	@SequenceGenerator(name="SSAP_INTEGRATION_LOGS_ID_GENERATOR", sequenceName="SSAP_INTEGRATION_LOGS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_INTEGRATION_LOGS_ID_GENERATOR")
	@Column(unique=true, nullable = false, precision=22)
	private Long id;

	@Column(name="CORRELATION_ID", nullable = true)
	private String correlationId;
	
	@Column(name="SERVICE_NAME", nullable = true)
	private String serviceName;
	
	@Column(name="GI_WS_PAYLOAD_ID", nullable = true)
	private Long giWsPayloadId;
	
	@Column(name="PAYLOAD")
	private String payload;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="RESPONSE_TIME_MS")
	private Long responseTimeMillis;
	
	@Column(name="IS_SCHEDULED_DOWNTIME")
	private String isScheduledDowntime;

    @Column(name="SSAP_APPLICATION_ID", nullable = true)
	private Long ssapApplicationId;

	@Column(name="SSAP_APPLICANT_ID", nullable = true)
	private Long ssapApplicantId;
	
	@Column(name="SSAP_APPLICATION_EVENT_ID", nullable = true)
	private Long ssapApplicationEventId;
	
	@Column(name="CREATED_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Long giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String requestPayload) {
		this.payload = requestPayload;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getResponseTimeMillis() {
		return responseTimeMillis;
	}

	public void setResponseTimeMillis(Long responseTime) {
		this.responseTimeMillis = responseTime;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Long getSsapApplicantId() {
		return ssapApplicantId;
	}

	public void setSsapApplicantId(Long ssapApplicantId) {
		this.ssapApplicantId = ssapApplicantId;
	}

	public Long getSsapApplicationEventId() {
		return ssapApplicationEventId;
	}

	public void setSsapApplicationEventId(Long ssapApplicationEventId) {
		this.ssapApplicationEventId = ssapApplicationEventId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	   
    public String getIsScheduledDowntime() {
        return isScheduledDowntime;
    }

    public void setIsScheduledDowntime(String isScheduledDowntime) {
        this.isScheduledDowntime = isScheduledDowntime;
    }
    
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreatedDate(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setCreatedDate(new TSDate()); 
	}
}

