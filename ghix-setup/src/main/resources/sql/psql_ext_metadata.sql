create or replace view user_tables
  as
  select upper(table_name) table_name, upper(table_type) table_type from information_schema.tables where table_schema=current_schema();
  
create view user_tab_columns
  as
  select upper(table_name) table_name , upper(column_name) column_name, data_type,
  character_maximum_length data_length,  is_nullable NULLABLE
  from information_schema.columns where table_schema=current_schema();
  
create or replace view user_constraints
as
with usrcs as(
select 
upper(conname) constraint_name,
upper(contype) constraint_type,
upper(conrelid::oid::regclass::text) table_name,
 pg_catalog.pg_get_constraintdef(con.oid, true) search_condition,
 condeferrable, condeferred
from pg_catalog.pg_constraint con
where connamespace = (select oid from pg_catalog.pg_namespace n where n.nspname=current_schema())
)
select constraint_name,constraint_type,table_name, '"'||substring(search_condition from  position('(' in search_condition))||'"' search_condition,
 condeferrable, condeferred
 from usrcs;

create or replace view user_cons_columns
as
select 
upper(conname) constraint_name,
upper(conrelid::oid::regclass::text) table_name,
 upper(attr.attname) column_name,
 attr.attnum column_position,
 upper(contype) constraint_type
from pg_catalog.pg_constraint con,  pg_catalog.pg_attribute attr
where connamespace = (select oid from pg_catalog.pg_namespace n where n.nspname=current_schema())
and con.conrelid=attr.attrelid
and attr.attnum = ANY(con.conkey);

create or replace view user_indexes
as
SELECT 
upper(c.oid::oid::regclass::text) table_name,
upper(c2.relname) index_name,
upper(n.nspname) table_owner,
i.indisunique uniqueness,
i.indkey AS columns,
i.indisprimary, i.indisunique, i.indisclustered, i.indisvalid,
pg_catalog.pg_get_indexdef(i.indexrelid, 0, true) indexdef,
  i.indisreplident, c2.reltablespace
FROM pg_catalog.pg_class c, pg_catalog.pg_class c2, pg_catalog.pg_index i, pg_catalog.pg_namespace n
WHERE n.nspname=current_schema()  
AND n.oid = c.relnamespace 
AND c.oid = i.indrelid AND i.indexrelid = c2.oid;

create or replace view user_sequences
as
SELECT upper(n.nspname) as schema_name,
  upper(c.relname) as sequence_name,
  CASE c.relkind WHEN 'r' THEN 'table' WHEN 'v' THEN 'view' WHEN 'm' THEN 'materialized view' WHEN 'i' THEN 'index' WHEN 'S' THEN 'sequence' WHEN 's' THEN 'special' WHEN 'f' THEN 'foreign table' END as "Type",
  pg_catalog.pg_get_userbyid(c.relowner) as sequence_owner
FROM pg_catalog.pg_class c
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN ('S','')
  AND n.nspname=current_schema()
  AND pg_catalog.pg_table_is_visible(c.oid);

create or replace view user_views
as
SELECT  upper(n.nspname) as schema_name,
  upper(c.relname) as view_name,
  c.oid as view_oid,
  pg_catalog.pg_get_userbyid(c.relowner) as sequence_owner
FROM pg_catalog.pg_class c
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN ('v','')
  AND n.nspname=current_schema()
    AND n.nspname <> 'pg_catalog'
    AND n.nspname <> 'information_schema'
    AND n.nspname !~ '^pg_toast'
  AND pg_catalog.pg_table_is_visible(c.oid);

create or replace view user_ind_columns
as
SELECT 
upper(c.oid::oid::regclass::text) table_name,
upper(c2.relname) index_name,
 upper(attr.attname) column_name,
 attr.attnum column_position,
upper(n.nspname) table_owner,
i.indisunique uniqueness,
i.indisvalid indexvalid
FROM pg_catalog.pg_class c, pg_catalog.pg_class c2,
pg_catalog.pg_index i, pg_catalog.pg_namespace n,
pg_catalog.pg_attribute attr
WHERE n.nspname=current_schema()  
AND n.oid = c.relnamespace 
AND c.oid = i.indrelid AND i.indexrelid = c2.oid
AND c.oid = attr.attrelid
AND attr.attnum = ANY(i.indkey);


