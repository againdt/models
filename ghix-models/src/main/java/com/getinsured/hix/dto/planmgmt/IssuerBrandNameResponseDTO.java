/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author shengole_s
 *
 */
public class IssuerBrandNameResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<IssuerBrandNameDTO> issuerBrandNameDtoList;

	/**
	 * @return the issuerBrandNameDtoList
	 */
	public List<IssuerBrandNameDTO> getIssuerBrandNameDtoList() {
		return issuerBrandNameDtoList;
	}

	/**
	 * @param issuerBrandNameDtoList the issuerBrandNameDtoList to set
	 */
	public void setIssuerBrandNameDtoList(List<IssuerBrandNameDTO> issuerBrandNameDtoList) {
		this.issuerBrandNameDtoList = issuerBrandNameDtoList;
	}
	
	
}
