package com.getinsured.hix.dto.plandisplay;

import java.util.List;

public class DrugPropertiesRequestDTO {
	private List<String> drugRxCodeList;
	
	public List<String> getDrugRxCodeList() {
		return drugRxCodeList;
	}
	public void setDrugRxCodeList(List<String> drugRxCodeList) {
		this.drugRxCodeList = drugRxCodeList;
	}
}
