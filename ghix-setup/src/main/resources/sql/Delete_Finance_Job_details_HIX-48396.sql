DELETE FROM batch_job_execution_context 
WHERE job_execution_id IN(SELECT job_execution_id FROM batch_job_execution 
WHERE job_instance_id IN 
(SELECT job_instance_id FROM batch_job_instance WHERE job_name IN('employerBinderInvoiceGenerationJob',
'employerBinderAutoPayJob',
'employerBinderInvoicePastDueNoticeGenJob',
'cancelBinderEmployerEnrollmentJob',
'employerInvoiceGenerationJob',
'employerInvoicePdfGenerationJob',
'employerAutoPayJob',
'employerInvoiceNotificationGenerationJob',
'terminateEmployerEnrollmentGenerationJob',
'createIssuerInvoiceJob',
'issuerPaymentProcessingJob',
'remittanceReportGenJob',
'paymentEventReportJob',
'migratePciIssuerJob',
'employerAutoRefundJob')));

DELETE FROM batch_step_execution_context 
WHERE step_execution_id IN (SELECT step_execution_id FROM batch_step_execution WHERE step_name IN ('cancelEnrollment',
'issuerInvoice',
'employeeInvoiceDue',
'autoRefund',
'binderautoPay',
'binderInvoiceDue',
'createBinderInvoice',
'employerInvoice',
'sendNotification',
'createPdf',
'migratePciIssuer',
'paymentEvent',
'createBinderInvoice',
'autoPay',
'issuerPayment',
'remittanceRpt',
'autoRefund'));

DELETE FROM batch_step_execution WHERE step_name IN ('cancelEnrollment',
'issuerInvoice',
'employeeInvoiceDue',
'autoRefund',
'binderautoPay',
'binderInvoiceDue',
'createBinderInvoice',
'employerInvoice',
'sendNotification',
'createPdf',
'migratePciIssuer',
'paymentEvent',
'createBinderInvoice',
'autoPay',
'issuerPayment',
'remittanceRpt',
'autoRefund');

DELETE FROM batch_job_execution WHERE job_instance_id IN (SELECT job_instance_id FROM batch_job_instance WHERE job_name IN('employerBinderInvoiceGenerationJob',
'employerBinderAutoPayJob',
'employerBinderInvoicePastDueNoticeGenJob',
'cancelBinderEmployerEnrollmentJob',
'employerInvoiceGenerationJob',
'employerInvoicePdfGenerationJob',
'employerAutoPayJob',
'employerInvoiceNotificationGenerationJob',
'terminateEmployerEnrollmentGenerationJob',
'createIssuerInvoiceJob',
'issuerPaymentProcessingJob',
'remittanceReportGenJob',
'paymentEventReportJob',
'migratePciIssuerJob',
'employerAutoRefundJob'));

DELETE FROM batch_job_params WHERE job_instance_id IN(SELECT job_instance_id FROM batch_job_instance WHERE job_name IN('employerBinderInvoiceGenerationJob',
'employerBinderAutoPayJob',
'employerBinderInvoicePastDueNoticeGenJob',
'cancelBinderEmployerEnrollmentJob',
'employerInvoiceGenerationJob',
'employerInvoicePdfGenerationJob',
'employerAutoPayJob',
'employerInvoiceNotificationGenerationJob',
'terminateEmployerEnrollmentGenerationJob',
'createIssuerInvoiceJob',
'issuerPaymentProcessingJob',
'remittanceReportGenJob',
'paymentEventReportJob',
'migratePciIssuerJob',
'employerAutoRefundJob'));


DELETE FROM batch_job_instance WHERE job_name IN('employerBinderInvoiceGenerationJob',
'employerBinderAutoPayJob',
'employerBinderInvoicePastDueNoticeGenJob',
'cancelBinderEmployerEnrollmentJob',
'employerInvoiceGenerationJob',
'employerInvoicePdfGenerationJob',
'employerAutoPayJob',
'employerInvoiceNotificationGenerationJob',
'terminateEmployerEnrollmentGenerationJob',
'createIssuerInvoiceJob',
'issuerPaymentProcessingJob',
'remittanceReportGenJob',
'paymentEventReportJob',
'migratePciIssuerJob',
'employerAutoRefundJob');