package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 **
 * The persistent class for generating the unique Assister Certification Number for Assister
 * 
 */
@Entity
@Table(name = "ASSISTER_CERTIFICATION_NUMBER")
public class AssisterCertificationNumber implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSISTER_CERTIFICATION_NO_SEQ")
	@SequenceGenerator(name = "ASSISTER_CERTIFICATION_NO_SEQ", sequenceName = "ASSISTER_CERTIFICATION_NO_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "EE_ASSISTER_ID")
	private Integer assisterId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getAssisterId() {
		return assisterId;
	}

	public void setAssisterId(Integer assisterId) {
		this.assisterId = assisterId;
	}

	@Override
	public String toString() {
		return "AssisterCertificationNumber details: ID = "+id+", AssisterId = "+assisterId;
	}
}
