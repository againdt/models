package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GHIXResponse;
import com.google.gson.Gson;

public class PlanResponse extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String planName;
	private String issuerPlanNumber;
	private int issuerId;
	private String issuerName;
	private String issuerLegalName;
	private String issuerPhone;
	private String issuerSite;
	private Float csrAmount;
	private String taxIdNumber;
	private boolean isAvailable;
	private String planLevel;
	private String planRegionName;
	private String planMarket;
	private String networkType;
	private String officeVisit;
	private String genericMedications;
	private String deductible;
	private String deductibleFamily;
	private String oopMax;
	private String oopMaxFamily;
	private Map<String, Map<String, String>> planBenefits;	
	private Map<String, Map<String, String>> planCosts;
	private Map<String, Map<String, String>> optionalDeductible;
	private String insuranceType;
	private String hiosIssuerId;
	private Float planPremium;
	private String paymentUrl;
	private List planList;
	private String applicationUrl;
	private String providerUrl;
	private String exchangeType;
	private String hsa;	
	private String issuerLogo;
	private String genericDrugs;
	private String officeVisitCost;
	private String pediatricDentalComponent; 
	private String ehbPercentage;
	private String stateEhbMandatePercentage;
	
	/*	Adding new fields in Plan response	*/
	private String coinsurance;
	private String policyLength;
	private String guaranteedVsEstimatedRate;
	private String policyLengthUnit;
	private String policyLimit;
	private String outOfNetwkCoverage;
	private String outOfCountyCoverage;
	private String separateDrugDeductible;
	private String oopMaxAttr;
	
	
	private List<Map<String, String>> providers;
	
//	AME plans specific attributes
	
	private String deductibleAttrib;
	private String maxBenefitVal;
	private String maxBenefitAttrib;
	private String brochure;
	private String lmitationAndExclusions;
	
	private List<PlanResponse> planResponsesList;
	private String costSharing;
	private String formularyUrl;
	private String sbcDocUrl;
	private String tier2util;
	private String eocDocUrl;
	
	private String premiumPayment;
	private String outOfNetwkAvailability;
		
	private Map<String, String> issuerQualityRating;
	private String producerUserName;
	private String producerPassword;	
	
	// Life plan specific attribute.
	private String policyTerm;
	private String benefitUrl;
	
	private int planYear;
	private String maxCoinsForSpecialtyDrugs;
	
	private Map<String, Map<String, String>> plans;
	
	private String networkTransparencyRating;
	private String nonCommissionFlag;
	private SbcScenarioDTO  sbcScenarioDTO;

	// HIX-101472 send issuer commission 
	private Double firstYearCommDollarAmt;
	private Double firstYearCommPercentageAmt;
	private Double secondYearCommDollarAmt;
	private Double secondYearCommPercentageAmt;
	private String frequency;

	public PlanResponse() {
	}

	public PlanResponse(int planId, int issuerId, String hiosIssuerId, String issuerPlanNumber, String planName,
			String issuerName, String status, String insuranceType, String planLevel, int planYear, String taxIdNumber,
			String costSharing, Float ehbPercentage, String pediatricDentalComponent, String issuerLogo) {

		super();
		this.planId = planId;
		this.issuerId = issuerId;
		this.hiosIssuerId = hiosIssuerId;
		this.issuerPlanNumber = issuerPlanNumber;
		this.planName = planName;
		this.issuerName = issuerName;
		this.status = status;
		this.insuranceType = insuranceType;
		this.planLevel = planLevel;
		this.planYear = planYear;
		this.taxIdNumber = taxIdNumber;
		this.costSharing = costSharing;

		if (null != ehbPercentage) {
			this.ehbPercentage = Float.toString(ehbPercentage);
		}
//		this.stateEhbMandatePercentage = stateEhbMandatePercentage;
		this.pediatricDentalComponent = pediatricDentalComponent;
		this.issuerLogo = issuerLogo;
	}

	/**
	 * @return the plans
	 */
	public Map<String, Map<String, String>> getPlans() {
		return plans;
	}

	/**
	 * @param plans the plans to set
	 */
	public void setPlans(Map<String, Map<String, String>> plans) {
		this.plans = plans;
	}

	/**
	 * @return the tier2util
	 */
	public String getTier2util() {
		return tier2util;
	}

	/**
	 * @param tier2util the tier2util to set
	 */
	public void setTier2util(String tier2util) {
		this.tier2util = tier2util;
	}

	/**
	 * @return the costSharing
	 */
	public String getCostSharing() {
		return costSharing;
	}

	/**
	 * @param costSharing the costSharing to set
	 */
	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}

	/**
	 * @return the planResponsesList
	 */
	public List<PlanResponse> getPlanResponsesList() {
		return planResponsesList;
	}

	/**
	 * @param planResponsesList the planResponsesList to set
	 */
	public void setPlanResponsesList(List<PlanResponse> planResponsesList) {
		this.planResponsesList = planResponsesList;
	}
 
	 /**
	 * @return the coinsurance
	 */
	public String getCoinsurance() {
		return coinsurance;
	}

	/**
	 * @param coinsurance the coinsurance to set
	 */
	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}

	/**
	 * @return the policyLength
	 */
	public String getPolicyLength() {
		return policyLength;
	}

	/**
	 * @param policyLength the policyLength to set
	 */
	public void setPolicyLength(String policyLength) {
		this.policyLength = policyLength;
	}

	/**
	 * @return the guaranteedVsEstimatedRate
	 */
	public String getGuaranteedVsEstimatedRate() {
		return guaranteedVsEstimatedRate;
	}

	/**
	 * @param guaranteedVsEstimatedRate the guaranteedVsEstimatedRate to set
	 */
	public void setGuaranteedVsEstimatedRate(String guaranteedVsEstimatedRate) {
		this.guaranteedVsEstimatedRate = guaranteedVsEstimatedRate;
	}

	/**
	 * @return the policyLengthUnit
	 */
	public String getPolicyLengthUnit() {
		return policyLengthUnit;
	}

	/**
	 * @param policyLengthUnit the policyLengthUnit to set
	 */
	public void setPolicyLengthUnit(String policyLengthUnit) {
		this.policyLengthUnit = policyLengthUnit;
	}

	/**
	 * @return the policyLimit
	 */
	public String getPolicyLimit() {
		return policyLimit;
	}

	/**
	 * @param policyLimit the policyLimit to set
	 */
	public void setPolicyLimit(String policyLimit) {
		this.policyLimit = policyLimit;
	}

	/**
	 * @return the outOfNetwkCoverage
	 */
	public String getOutOfNetwkCoverage() {
		return outOfNetwkCoverage;
	}

	/**
	 * @param outOfNetwkCoverage the outOfNetwkCoverage to set
	 */
	public void setOutOfNetwkCoverage(String outOfNetwkCoverage) {
		this.outOfNetwkCoverage = outOfNetwkCoverage;
	}

	/**
	 * @return the outOfCountyCoverage
	 */
	public String getOutOfCountyCoverage() {
		return outOfCountyCoverage;
	}

	/**
	 * @param outOfCountyCoverage the outOfCountyCoverage to set
	 */
	public void setOutOfCountyCoverage(String outOfCountyCoverage) {
		this.outOfCountyCoverage = outOfCountyCoverage;
	}

	/**
	 * @return the separateDrugDeductible
	 */
	public String getSeparateDrugDeductible() {
		return separateDrugDeductible;
	}

	/**
	 * @param separateDrugDeductible the separateDrugDeductible to set
	 */
	public void setSeparateDrugDeductible(String separateDrugDeductible) {
		this.separateDrugDeductible = separateDrugDeductible;
	}

	/**
	 * @return the oopMaxAttr
	 */
	public String getOopMaxAttr() {
		return oopMaxAttr;
	}

	/**
	 * @param oopMaxAttr the oopMaxAttr to set
	 */
	public void setOopMaxAttr(String oopMaxAttr) {
		this.oopMaxAttr = oopMaxAttr;
	}

	/**
	 * @return the planId
	 */
	public int getPlanId() {
		return planId;
	}

	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(int planId) {
		this.planId = planId;
	}

	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the issuerPlanNumber
	 */
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	/**
	 * @param issuerPlanNumber the issuerPlanNumber to set
	 */
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	
	/**
	 * @return the issuerId
	 */
	public int getIssuerId() {
		return issuerId;
	}

	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the issuerLegalName
	 */
	public String getIssuerLegalName() {
		return issuerLegalName;
	}

	/**
	 * @param issuerLegalName the issuerLegalName to set
	 */
	public void setIssuerLegalName(String issuerLegalName) {
		this.issuerLegalName = issuerLegalName;
	}

	public String getIssuerPhone() {
		return issuerPhone;
	}

	public void setIssuerPhone(String issuerPhone) {
		this.issuerPhone = issuerPhone;
	}

	public String getIssuerSite() {
		return issuerSite;
	}

	public void setIssuerSite(String issuerSite) {
		this.issuerSite = issuerSite;
	}

	/**
	 * @return the csrAmount
	 */
	public Float getCsrAmount() {
		return csrAmount;
	}
	/**
	 * @param csrAmount the csrAmount to set
	 */
	public void setCsrAmount(Float csrAmount) {
		this.csrAmount = csrAmount;
	}

	/**
	 * @return the taxIdNumber
	 */
	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	/**
	 * @param taxIdNumber the taxIdNumber to set
	 */
	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	/**
	 * @return the isAvailable
	 */
	public boolean isAvailable() {
		return isAvailable;
	}

	/**
	 * @param isAvailable the isAvailable to set
	 */
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	@Override
	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}

	public String getPlanRegionName() {
		return planRegionName;
	}

	public void setPlanRegionName(String planRegionName) {
		this.planRegionName = planRegionName;
	}

	public String getPlanMarket() {
		return planMarket;
	}

	public void setPlanMarket(String planMarket) {
		this.planMarket = planMarket;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getOfficeVisit() {
		return officeVisit;
	}

	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}

	public String getGenericMedications() {
		return genericMedications;
	}

	public void setGenericMedications(String genericMedications) {
		this.genericMedications = genericMedications;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getOopMax() {
		return oopMax;
	}

	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}

	public Map<String, Map<String, String>> getPlanBenefits() {
		return planBenefits;
	}

	public void setPlanBenefits(Map<String, Map<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}

	public Map<String, Map<String, String>> getPlanCosts() {
		return planCosts;
	}

	public void setPlanCosts(Map<String, Map<String, String>> planCosts) {
		this.planCosts = planCosts;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	
	
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getDeductibleFamily() {
		return deductibleFamily;
	}

	public void setDeductibleFamily(String deductibleFamily) {
		this.deductibleFamily = deductibleFamily;
	}

	public String getOopMaxFamily() {
		return oopMaxFamily;
	}

	public void setOopMaxFamily(String oopMaxFamily) {
		this.oopMaxFamily = oopMaxFamily;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public List getPlanList() {
		return planList;
	}

	public void setPlanList(List planList) {
		this.planList = planList;
	}
	
	public String getProviderUrl() {
		return providerUrl;
	}

	public void setProviderUrl(String providerUrl) {
		this.providerUrl = providerUrl;
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public Float getPlanPremium() {
		return planPremium;
	}

	public void setPlanPremium(Float planPremium) {
		this.planPremium = planPremium;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getHsa() {
		return hsa;
	}

	public void setHsa(String hsa) {
		this.hsa = hsa;
	}
	
	/**
	 * @return the optionalDeductible
	 */
	public Map<String, Map<String, String>> getOptionalDeductible() {
		return optionalDeductible;
	}

	/**
	 * @param optionalDeductible the optionalDeductible to set
	 */
	public void setOptionalDeductible(
			Map<String, Map<String, String>> optionalDeductible) {
		this.optionalDeductible = optionalDeductible;
	}	
	
	public String getGenericDrugs() {
		return genericDrugs;
	}

	public void setGenericDrugs(String genericDrugs) {
		this.genericDrugs = genericDrugs;
	}

	public String getOfficeVisitCost() {
		return officeVisitCost;
	}

	public void setOfficeVisitCost(String officeVisitCost) {
		this.officeVisitCost = officeVisitCost;
	}

		public String getIssuerLogo() {
		return issuerLogo;
	}

	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}

	public String getPediatricDentalComponent() {
		return pediatricDentalComponent;
	}

	public void setPediatricDentalComponent(String pediatricDentalComponent) {
		this.pediatricDentalComponent = pediatricDentalComponent;
	}

	public String getEhbPercentage() {
		return ehbPercentage;
	}

	public void setEhbPercentage(String ehbPercentage) {
		this.ehbPercentage = ehbPercentage;
	}

	public String getStateEhbMandatePercentage() {
		return stateEhbMandatePercentage;
	}

	public void setStateEhbMandatePercentage(
			String stateEhbMandatePercentage) {
		this.stateEhbMandatePercentage = stateEhbMandatePercentage;
	}

	public List<Map<String, String>> getProviders() {
		return providers;
	}

	public void setProviders(List<Map<String, String>> providers) {
		this.providers = providers;
	}

	public String getDeductibleAttrib() {
		return deductibleAttrib;
	}

	public void setDeductibleAttrib(String deductibleAttrib) {
		this.deductibleAttrib = deductibleAttrib;
	}

	public String getMaxBenefitVal() {
		return maxBenefitVal;
	}

	public void setMaxBenefitVal(String maxBenefitVal) {
		this.maxBenefitVal = maxBenefitVal;
	}

	public String getMaxBenefitAttrib() {
		return maxBenefitAttrib;
	}

	public void setMaxBenefitAttrib(String maxBenefitAttrib) {
		this.maxBenefitAttrib = maxBenefitAttrib;
	}

	public String getBrochure() {
		return brochure;
	}

	public void setBrochure(String brochure) {
		this.brochure = brochure;
	}

	public String getLmitationAndExclusions() {
		return lmitationAndExclusions;
	}

	public void setLmitationAndExclusions(String lmitationAndExclusions) {
		this.lmitationAndExclusions = lmitationAndExclusions;
	}
	
	public String getFormularyUrl() {
		return formularyUrl;
	}

	public void setFormularyUrl(String formularyUrl) {
		this.formularyUrl = formularyUrl;
	}

	public String getSbcDocUrl() {
		return sbcDocUrl;
	}

	public void setSbcDocUrl(String sbcDocUrl) {
		this.sbcDocUrl = sbcDocUrl;
	}

	public String getEocDocUrl() {
		return eocDocUrl;
	}

	public void setEocDocUrl(String eocDocUrl) {
		this.eocDocUrl = eocDocUrl;
	}

	/**
	 * @return the premiumPayment
	 */
	public String getPremiumPayment() {
		return premiumPayment;
	}

	/**
	 * @param premiumPayment the premiumPayment to set
	 */
	public void setPremiumPayment(String premiumPayment) {
		this.premiumPayment = premiumPayment;
	}

	/**
	 * @return the outOfNetwkAvailability
	 */
	public String getOutOfNetwkAvailability() {
		return outOfNetwkAvailability;
	}

	/**
	 * @param outOfNetwkAvailability the outOfNetwkAvailability to set
	 */
	public void setOutOfNetwkAvailability(String outOfNetwkAvailability) {
		this.outOfNetwkAvailability = outOfNetwkAvailability;
	}

	public Map<String, String> getIssuerQualityRating() {
		return issuerQualityRating;
	}

	public void setIssuerQualityRating(Map<String, String> issuerQualityRating) {
		this.issuerQualityRating = issuerQualityRating;
	}
	
	/**
	 * @return the producerUserName
	 */
	public String getProducerUserName() {
		return producerUserName;
	}

	/**
	 * @param producerUserName the producerUserName to set
	 */
	public void setProducerUserName(String producerUserName) {
		this.producerUserName = producerUserName;
	}

	/**
	 * @return the producerPassword
	 */
	public String getProducerPassword() {
		return producerPassword;
	}

	/**
	 * @param producerPassword the producerPassword to set
	 */
	public void setProducerPassword(String producerPassword) {
		this.producerPassword = producerPassword;
	}



	/**
	 * @return the policyTerm
	 */
	public String getPolicyTerm() {
		return policyTerm;
	}

	/**
	 * @param policyTerm the policyTerm to set
	 */
	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}



	/**
	 * @return the benefitUrl
	 */
	public String getBenefitUrl() {
		return benefitUrl;
	}

	/**
	 * @param benefitUrl the benefitUrl to set
	 */
	public void setBenefitUrl(String benefitUrl) {
		this.benefitUrl = benefitUrl;
	}



	/**
	 * @return the planYear
	 */
	public int getPlanYear() {
		return planYear;
	}

	/**
	 * @param planYear the planYear to set
	 */
	public void setPlanYear(int planYear) {
		this.planYear = planYear;
	}

	/**
	 * @return the maxCoinsForSpecialtyDrugs
	 */
	public String getMaxCoinsForSpecialtyDrugs() {
		return maxCoinsForSpecialtyDrugs;
	}

	/**
	 * @param maxCoinsForSpecialtyDrugs the maxCoinsForSpecialtyDrugs to set
	 */
	public void setMaxCoinsForSpecialtyDrugs(String maxCoinsForSpecialtyDrugs) {
		this.maxCoinsForSpecialtyDrugs = maxCoinsForSpecialtyDrugs;
	}

	/**
	 * @return the networkTransparencyRating
	 */
	public String getNetworkTransparencyRating() {
		return networkTransparencyRating;
	}

	/**
	 * @param networkTransparencyRating the networkTransparencyRating to set
	 */
	public void setNetworkTransparencyRating(String networkTransparencyRating) {
		this.networkTransparencyRating = networkTransparencyRating;
	}
	
	public String getNonCommissionFlag() {
		return nonCommissionFlag;
	}

	public void setNonCommissionFlag(String nonCommissionFlag) {
		this.nonCommissionFlag = nonCommissionFlag;
	}

	public SbcScenarioDTO getSbcScenarioDTO() {
		return sbcScenarioDTO;
	}

	public void setSbcScenarioDTO(SbcScenarioDTO sbcScenarioDTO) {
		this.sbcScenarioDTO = sbcScenarioDTO;
	}

	/**
	 * @return the firstYearCommDollarAmt
	 */
	public Double getFirstYearCommDollarAmt() {
		return firstYearCommDollarAmt;
	}

	/**
	 * @param firstYearCommDollarAmt the firstYearCommDollarAmt to set
	 */
	public void setFirstYearCommDollarAmt(Double firstYearCommDollarAmt) {
		this.firstYearCommDollarAmt = firstYearCommDollarAmt;
	}

	/**
	 * @return the firstYearCommPercentageAmt
	 */
	public Double getFirstYearCommPercentageAmt() {
		return firstYearCommPercentageAmt;
	}

	/**
	 * @param firstYearCommPercentageAmt the firstYearCommPercentageAmt to set
	 */
	public void setFirstYearCommPercentageAmt(Double firstYearCommPercentageAmt) {
		this.firstYearCommPercentageAmt = firstYearCommPercentageAmt;
	}

	/**
	 * @return the secondYearCommDollarAmt
	 */
	public Double getSecondYearCommDollarAmt() {
		return secondYearCommDollarAmt;
	}

	/**
	 * @param secondYearCommDollarAmt the secondYearCommDollarAmt to set
	 */
	public void setSecondYearCommDollarAmt(Double secondYearCommDollarAmt) {
		this.secondYearCommDollarAmt = secondYearCommDollarAmt;
	}

	/**
	 * @return the secondYearCommPercentageAmt
	 */
	public Double getSecondYearCommPercentageAmt() {
		return secondYearCommPercentageAmt;
	}

	/**
	 * @param secondYearCommPercentageAmt the secondYearCommPercentageAmt to set
	 */
	public void setSecondYearCommPercentageAmt(
			Double secondYearCommPercentageAmt) {
		this.secondYearCommPercentageAmt = secondYearCommPercentageAmt;
	}

	/**
	 * @return the frequency
	 */
	public String getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public static Comparator<PlanResponse> PlanResponseComparatorByPlanId = new Comparator<PlanResponse>(){

		@Override
		public int compare(PlanResponse obj1, PlanResponse obj2) {
			Integer obj1PlanId = obj1.getPlanId();
			Integer obj2PlanId = obj2.getPlanId();
			return obj1PlanId.compareTo(obj2PlanId);
		}
	};
}
