package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.Date;

public class ConsumerDocumentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3652430110739252226L;

	private Long id;
	private String ecmDocumentId;
	private String documentName;
	private String documentCategory;
	private String documentType;
	private Long targetId;
	private String targetName;
	private Date createdDate;
	private Long createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEcmDocumentId() {
		return ecmDocumentId;
	}

	public void setEcmDocumentId(String ecmDocumentId) {
		this.ecmDocumentId = ecmDocumentId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

}
