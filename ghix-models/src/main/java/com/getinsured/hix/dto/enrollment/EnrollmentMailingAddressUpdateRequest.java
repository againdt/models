package com.getinsured.hix.dto.enrollment;

public class EnrollmentMailingAddressUpdateRequest {
	
	private Long householdCaseId;
	private Long updatedMailingAddressLocationID;
	private String householdContactPrimaryPhone;
    private String householdContactSecondaryPhone;
    private String householdContactPreferredPhone;
    private String householdContactPreferredEmail;
    private String householdContactSpokenLanguageCode;
    private String householdContactWrittenLanguageCode;
	public Long getHouseholdCaseId() {
		return householdCaseId;
	}
	public void setHouseholdCaseId(Long householdCaseId) {
		this.householdCaseId = householdCaseId;
	}
	public Long getUpdatedMailingAddressLocationID() {
		return updatedMailingAddressLocationID;
	}
	public void setUpdatedMailingAddressLocationID(Long updatedMailingAddressLocationID) {
		this.updatedMailingAddressLocationID = updatedMailingAddressLocationID;
	}
	public String getHouseholdContactPrimaryPhone() {
		return householdContactPrimaryPhone;
	}
	public void setHouseholdContactPrimaryPhone(String householdContactPrimaryPhone) {
		this.householdContactPrimaryPhone = householdContactPrimaryPhone;
	}
	public String getHouseholdContactSecondaryPhone() {
		return householdContactSecondaryPhone;
	}
	public void setHouseholdContactSecondaryPhone(String householdContactSecondaryPhone) {
		this.householdContactSecondaryPhone = householdContactSecondaryPhone;
	}
	public String getHouseholdContactPreferredPhone() {
		return householdContactPreferredPhone;
	}
	public void setHouseholdContactPreferredPhone(String householdContactPreferredPhone) {
		this.householdContactPreferredPhone = householdContactPreferredPhone;
	}
	public String getHouseholdContactPreferredEmail() {
		return householdContactPreferredEmail;
	}
	public void setHouseholdContactPreferredEmail(String householdContactPreferredEmail) {
		this.householdContactPreferredEmail = householdContactPreferredEmail;
	}
	public String getHouseholdContactSpokenLanguageCode() {
		return householdContactSpokenLanguageCode;
	}
	public void setHouseholdContactSpokenLanguageCode(String householdContactSpokenLanguageCode) {
		this.householdContactSpokenLanguageCode = householdContactSpokenLanguageCode;
	}
	public String getHouseholdContactWrittenLanguageCode() {
		return householdContactWrittenLanguageCode;
	}
	public void setHouseholdContactWrittenLanguageCode(String householdContactWrittenLanguageCode) {
		this.householdContactWrittenLanguageCode = householdContactWrittenLanguageCode;
	}
    
    
    

}
