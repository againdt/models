/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author lole_a
 *
 */
public class MedicareZipResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String zipCode;

	private List<Map<String, String>> countyList;
	
	
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public MedicareZipResponseDTO() {
	}

	public List<Map<String, String>> getCountyList() {
		return countyList;
	}

	public void setCountyList(List<Map<String, String>> countyList) {
		this.countyList = countyList;
	}

	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse() {
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON.
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse() {
		String transformedResponse = null;

		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
			public HierarchicalStreamWriter createWriter(Writer writer) {
				return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
			}
		});

		Validate.notNull(jsoner, "XStream instance improperly confugured.");

		transformedResponse = jsoner.toXML(this);

		return transformedResponse;
	}*/
}
