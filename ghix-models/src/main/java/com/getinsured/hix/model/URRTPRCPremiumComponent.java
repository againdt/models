package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "pm_urrt_prc_prm_comp")
@XmlAccessorType(XmlAccessType.NONE)
public class URRTPRCPremiumComponent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_urrt_plan_rc_prm_cmp_seq")
	@SequenceGenerator(name = "pm_urrt_plan_rc_prm_cmp_seq", sequenceName = "pm_urrt_plan_rc_prm_cmp_seq", allocationSize = 1)
	private int id;	
	
	//foreign key.
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pm_urrt_plan_rate_change_id")
	private URRTInsurancePlanRateChange unifiedRatePlanRateChange;
		
	@Column(name = "crp_pmpm_qty")
	private double changedRatePerPMPMQuantity;
		
	@Column(name = "drc_comp_type")
	private String definingRateChangeComponent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getChangedRatePerPMPMQuantity() {
		return changedRatePerPMPMQuantity;
	}

	public void setChangedRatePerPMPMQuantity(double changedRatePerPMPMQuantity) {
		this.changedRatePerPMPMQuantity = changedRatePerPMPMQuantity;
	}

	public String getDefiningRateChangeComponent() {
		return definingRateChangeComponent;
	}

	public void setDefiningRateChangeComponent(String definingRateChangeComponent) {
		this.definingRateChangeComponent = definingRateChangeComponent;
	}

	public URRTInsurancePlanRateChange getUnifiedRatePlanRateChange() {
		return unifiedRatePlanRateChange;
	}

	public void setUnifiedRatePlanRateChange(
			URRTInsurancePlanRateChange unifiedRatePlanRateChange) {
		this.unifiedRatePlanRateChange = unifiedRatePlanRateChange;
	}

}
