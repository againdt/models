package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;


public class EnrollmentPremium1095DTO implements Serializable{
	
	private Integer id;
	private Integer monthNumber;
	private String monthName;
	private Float slcspAmount;				
	private Float aptcAmount;
	private Float grossPremium;
	private Float ehbPercent;
	private Float pediatricEhbAmt;
	private String createdOn;
	private String updatedOn;
	private Integer createdBy;
	private Integer updatedBy;
	private String updateNotes;
	private String isActive = "Y";
	private Integer accountableMemberDental;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMonthNumber() {
		return monthNumber;
	}
	public void setMonthNumber(Integer monthNumber) {
		this.monthNumber = monthNumber;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public Float getSlcspAmount() {
		return slcspAmount;
	}
	public void setSlcspAmount(Float slcspAmount) {
		this.slcspAmount = slcspAmount;
	}
	public Float getAptcAmount() {
		return aptcAmount;
	}
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	public Float getGrossPremium() {
		return grossPremium;
	}
	public void setGrossPremium(Float grossPremium) {
		this.grossPremium = grossPremium;
	}
	public Float getEhbPercent() {
		return ehbPercent;
	}
	public void setEhbPercent(Float ehbPercent) {
		this.ehbPercent = ehbPercent;
	}
	public Float getPediatricEhbAmt() {
		return pediatricEhbAmt;
	}
	public void setPediatricEhbAmt(Float pediatricEhbAmt) {
		this.pediatricEhbAmt = pediatricEhbAmt;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdateNotes() {
		return updateNotes;
	}
	public void setUpdateNotes(String updateNotes) {
		this.updateNotes = updateNotes;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public Integer getAccountableMemberDental() {
		return accountableMemberDental;
	}
	public void setAccountableMemberDental(Integer accountableMemberDental) {
		this.accountableMemberDental = accountableMemberDental;
	}

}
