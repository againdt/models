package com.getinsured.hix.dto.consumer.ssap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;

public class SsapApplicationDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int cmrHouseholdId;
	private int planId;
	private long id;
	private long eligLeadId;
	private BigDecimal maximumAptc;
	private Date   effectiveStartDate;
	
	private String giHouseholdId;  //case_number
	private String applicationData;
	private String csrLevel;
	private String countyCode;
	private String ffmHouseholdId;  //external_application_id
	private String state;
	private String zipCode;
	private String applicationStatus;
	private String stage;
	private String exchangeType;
	private String memberData;
	private String applicationType;
	private STAGE applicationStage;
	private String displayStatusName;

	//HIX-48741
	private String subscriberUserName;
	private String subscriberPassword;
	private String d2cStatus;
	//End HIX-48741
	
	private BigDecimal premiumBeforeCredit;
	private BigDecimal premiumAfterCredit;
	
	private BigDecimal createdBy;
	
	//SSAP related fields
	private String eligibilityStatus;
	private String exchangeEligibilityStatus;
	private Integer coverageYear;
	
	private int userId;
	private int issuerId;
	private String issuerName;

	private Timestamp creationTimestamp;
	
	private String enrollmentStatus;
	
	private String acaTransactionURL;
	
	public String getSubscriberUserName() {
		return subscriberUserName;
	}
	public void setSubscriberUserName(String subscriberUserName) {
		this.subscriberUserName = subscriberUserName;
	}
	public String getSubscriberPassword() {
		return subscriberPassword;
	}
	public void setSubscriberPassword(String subscriberPassword) {
		this.subscriberPassword = subscriberPassword;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGiHouseholdId() {
		return giHouseholdId;
	}
	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}
	public String getApplicationData() {
		return applicationData;
	}
	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}
	public int getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(int cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	public String getCsrLevel() {
		return csrLevel;
	}
	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}
	public BigDecimal getMaximumAptc() {
		return maximumAptc;
	}
	public void setMaximumAptc(BigDecimal maximumAptc) {
		this.maximumAptc = maximumAptc;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getFfmHouseholdId() {
		return ffmHouseholdId;
	}
	public void setFfmHouseholdId(String ffmHouseholdId) {
		this.ffmHouseholdId = ffmHouseholdId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getMemberData() {
		return memberData;
	}
	public void setMemberData(String memberData) {
		this.memberData = memberData;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public STAGE getApplicationStage() {
		return applicationStage;
	}
	public void setApplicationStage(STAGE applicationStage) {
		this.applicationStage = applicationStage;
	}
	public long getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public String getD2cStatus() {
		return d2cStatus;
	}
	public void setD2cStatus(String d2cStatus) {
		this.d2cStatus = d2cStatus;
	}
	public BigDecimal getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}
	public void setPremiumBeforeCredit(BigDecimal premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}
	public BigDecimal getPremiumAfterCredit() {
		return premiumAfterCredit;
	}
	public void setPremiumAfterCredit(BigDecimal premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}
		
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public String getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}
	public void setExchangeEligibilityStatus(String exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}
	public Integer getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getDisplayStatusName() {
		return displayStatusName;
	}
	public void setDisplayStatusName(String displayStatusName) {
		this.displayStatusName = displayStatusName;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getAcaTransactionURL() {
		return acaTransactionURL;
	}
	public void setAcaTransactionURL(String acaTransactionURL) {
		this.acaTransactionURL = acaTransactionURL;
	}
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
		
	
}
