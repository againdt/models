package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

public class IssuerEnrollmentEndDateResponseDTO extends GHIXResponse implements Serializable {
	
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean enrollmentEndDateFlag;

	// constructor
	public IssuerEnrollmentEndDateResponseDTO(){
		this.startResponse();
	}
	
	/**
	 * @return the enrollmentEndDateFlag
	 */
	public boolean getEnrollmentEndDateFlag() {
		return enrollmentEndDateFlag;
	}

	/**
	 * @param enrollmentEndDateFlag the enrollmentEndDateFlag to set
	 */
	public void setEnrollmentEndDateFlag(boolean enrollmentEndDateFlag) {
		this.enrollmentEndDateFlag = enrollmentEndDateFlag;
	}

	
}
