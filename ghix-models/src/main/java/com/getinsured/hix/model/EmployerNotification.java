package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="employer_notification")
public class EmployerNotification 
{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMP_NOTIFICATION_SEQ")
	@SequenceGenerator(name = "EMP_NOTIFICATION_SEQ", sequenceName = "EMP_NOTIFICATION_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="NOTICES_ID")
	private Notice noticesId;
	
	@ManyToOne
    @JoinColumn(name="EMPLOYER_INVOICE_ID")
	private EmployerInvoices employerInvoiceId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="DATE_CREATED",nullable=false)
	private Date dateCreated;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "NUMBER_OF_ATTEMPTS")
	private int noOfAttempts;
	
	@Column(name = "ADDITIONAL_COMMENTS")
	private String additionalComments;
	
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE",nullable=false)
	private Date updatedDate;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	public Notice getNoticesId() {
		return noticesId;
	}

	public void setNoticesId(Notice noticesId) {
		this.noticesId = noticesId;
	}

	public EmployerInvoices getEmployerInvoiceId() {
		return employerInvoiceId;
	}

	public void setEmployerInvoiceId(EmployerInvoices employerInvoiceId) {
		this.employerInvoiceId = employerInvoiceId;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getNoOfAttempts() {
		return noOfAttempts;
	}

	public void setNoOfAttempts(int noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}
	
	public String getAdditionalComments() {
		return additionalComments;
	}

	public void setAdditionalComments(String additionalComments) {
		this.additionalComments = additionalComments;
	}
	
}
