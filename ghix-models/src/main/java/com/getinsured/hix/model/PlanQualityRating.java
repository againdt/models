package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the roles database table.
 * 
 */

@Entity
@Table(name="plan_quality_rating")

public class PlanQualityRating  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanQualityRating_Seq")
	@SequenceGenerator(name = "PlanQualityRating_Seq", sequenceName = "plan_quality_rating_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="plan_id")
	private Plan plan;
	
	@Column(name="quality_rating", length=10)
	private String qualityRating ;
	
	@Column(name="quality_source", length=50)
	private String qualitySource;
	
	@Column(name="quality_rating_details", length=200)
	private String qualityRatingDetails;
	
			
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getQualityRating() {
		return qualityRating;
	}

	public void setQualityRating(String qualityRating) {
		this.qualityRating = qualityRating;
	}

	public String getQualitySource() {
		return qualitySource;
	}

	public void setQualitySource(String qualitySource) {
		this.qualitySource = qualitySource;
	}

	public String getQualityRatingDetails() {
		return qualityRatingDetails;
	}

	public void setQualityRatingDetails(String qualityRatingDetails) {
		this.qualityRatingDetails = qualityRatingDetails;
	}

	
}