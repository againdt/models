/**
 * 
 */
package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * Pojo class for STM HCC Admin Fees 
 * @author sharma_va
 *
 */
@Audited
@Entity
@Table(name = "PM_STM_HCC_ADMIN_FEES")
public class STMHCCPlansAdminFees {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STM_HCC_ADMIN_FEES_SEQ")
	@SequenceGenerator(name = "STM_HCC_ADMIN_FEES_SEQ", sequenceName = "STM_HCC_ADMIN_FEES_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "duration")
	private String duration;
	
	@Column(name = "admin_elec_fee")
	private Double adminElecFee;
	
	@Column(name = "admin_paper_fee")
	private Double adminPaperFee;
	
	@Column(name = "assoc_fee")
	private Double associationFee;
	
	@Column(name = "comments")
	private String comments;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp" , nullable = false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp" , nullable = false)
	private Date lastUpdateTimestamp;
	
	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

	public STMHCCPlansAdminFees(){
		
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Double getAdminElecFee() {
		return adminElecFee;
	}

	public void setAdminElecFee(Double adminElecFee) {
		this.adminElecFee = adminElecFee;
	}

	public Double getAdminPaperFee() {
		return adminPaperFee;
	}

	public void setAdminPaperFee(Double adminPaperFee) {
		this.adminPaperFee = adminPaperFee;
	}

	public Double getAssociationFee() {
		return associationFee;
	}

	public void setAssociationFee(Double associationFee) {
		this.associationFee = associationFee;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
