package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to get response for "plan/getInfobyPlanIds" API.
 */
public class PlanListResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private HttpStatus statusCode;
	private String message; // optional: success message or error payload
	private long executionDuration;
	private long startTime;

	private List<PlanResponse> planList;

	public PlanListResponse() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getExecutionDuration() {
		return executionDuration;
	}

	public void startResponse() {
		startTime = Instant.now().toEpochMilli();
	}

	public void endResponse() {
		long endTime = Instant.now().toEpochMilli();
		executionDuration = endTime - startTime;
	}

	public List<PlanResponse> getPlanList() {
		return planList;
	}

	public void setPlanInList(PlanResponse plan) {

		if (CollectionUtils.isEmpty(planList)) {
			this.planList = new ArrayList<PlanResponse>();
		}
		this.planList.add(plan);
	}
}
