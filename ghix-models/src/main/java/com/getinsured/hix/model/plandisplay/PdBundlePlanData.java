package com.getinsured.hix.model.plandisplay;

public class PdBundlePlanData {
	private Integer planId;
	private Float premium;
	private String planType;
	private Long ssapApplicationId;	
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}	
	
}
