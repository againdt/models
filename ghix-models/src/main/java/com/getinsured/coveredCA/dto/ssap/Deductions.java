
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class Deductions extends Income {
	
	@Expose
	private String payTo;
	@Expose 
	private String typeOfDeduction;
	
	
	public String getPayTo() {
		return payTo;
	}
	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}
	public String getTypeOfDeduction() {
		return typeOfDeduction;
	}
	public void setTypeOfDeduction(String typeOfDeduction) {
		this.typeOfDeduction = typeOfDeduction;
	}
	
	

   
}
