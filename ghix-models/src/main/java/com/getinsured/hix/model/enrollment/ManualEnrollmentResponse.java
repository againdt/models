/**
 * 
 */
package com.getinsured.hix.model.enrollment;
import java.util.Date;
import com.getinsured.hix.model.GHIXResponse;

/**
 * @author Priya C
 *
 */
public class ManualEnrollmentResponse extends GHIXResponse{

    private Integer cmrHouseholdID;
	private String issuerName;
	private String planName;
	private String planlevel;
    private Float premium;
    private String confirmationNumber;
    private String exchangeType;
    private String enrollmentModality;
    private Float aptc ;
    private String policyNumber;
    private Integer enrollmentId;
    private String enrollmentStatus;
  
    public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public String getEnrollmentModality() {
		return enrollmentModality;
	}
	public void setEnrollmentModality(String enrollmentModality) {
		this.enrollmentModality = enrollmentModality;
	}
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public Float getAptc() {
		return aptc;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public String getPlanlevel() {
		return planlevel;
	}
	public void setPlanlevel(String planlevel) {
		this.planlevel = planlevel;
	}
	private Date effectiveDate;

	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public Integer getCmrHouseholdID() {
		return cmrHouseholdID;
	}
	public void setCmrHouseholdID(Integer cmrHouseholdID) {
		this.cmrHouseholdID = cmrHouseholdID;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	
     
   	
}
