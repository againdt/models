INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	                                VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	                               (select id from roles where name='CUSTOMER_SUPPORT'), (select id from permissions   where name=UPPER('MANAGE_ISSUER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	                                VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	                               (select id from roles where name='CUSTOMER_SUPPORT'),
	                               (select id from permissions   where name=UPPER('SEARCH_CONSUMERS_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id ) VALUES
                                   (role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	                               (select id from roles where name='CUSTOMER_SUPPORT'), 
	                               (select id from permissions   where name=UPPER('VIEW_CONSUMER_CRM')));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
                                   (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                                   (SELECT id FROM roles WHERE name='CUSTOMER_SUPPORT'),
                                   (SELECT id FROM permissions WHERE name='TKM_VIEW_TICKET'));								   
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
                                   (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                                   (SELECT id FROM roles WHERE name='CUSTOMER_SUPPORT'),
                                   (SELECT id FROM permissions WHERE name='TKM_SAVE_TICKET'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
                                   (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                                   (SELECT id FROM roles WHERE name='CUSTOMER_SUPPORT'),
                                   (SELECT id FROM permissions WHERE name='TKM_LIST_TICKET'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
                                   (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                                   (SELECT id FROM roles WHERE name='CUSTOMER_SUPPORT'),
                                   (SELECT id FROM permissions WHERE name='TKM_LIST_QUEUE'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
                                   (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                                   (SELECT id FROM roles WHERE name='CUSTOMER_SUPPORT'),
                                   (SELECT id FROM permissions WHERE name='CAP_POLICY_LIST'));								   
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
                                   (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                                   (SELECT id FROM roles WHERE name='CUSTOMER_SUPPORT'),
                                   (SELECT id FROM permissions WHERE name='CAP_POLICY_VIEW'));