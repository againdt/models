package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class InsurancePremiumIndividualResponsibilityTotalAmount{
  @JsonProperty("Value")
  
  private Double Value;
  public void setValue(Double Value){
   this.Value=Value;
  }
  public Double getValue(){
   return Value;
  }
}