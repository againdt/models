--truncate exchg related tables

TRUNCATE EXCHG_DNU;

TRUNCATE EXCHG_DNU_ERROR;

TRUNCATE EXCHG_DNU_RPT_IN;

TRUNCATE EXCHG_DNU_RPT_OUT;

TRUNCATE EXCHG_GS_RECON;

TRUNCATE EXCHG_ISA_RECON;

TRUNCATE EXCHG_TA1_RECON;

TRUNCATE EXCHG_999_RECON;

TRUNCATE EXCHG_RETURNCODE;

--insert initial data exchg in EXCHG_RETURNCODE table

INSERT INTO exchg_returncode (tool,returncode,description) VALUES 
 ('Translator','100','Translation ran successfully.')
,('Translator','105','Could not open the source schema.')
,('Translator','106','Guidelines specified in the API are not the same guidelines used in the map.')
,('Translator','110','Command line error or error loading INI file.')
,('Translator','111','One of the documents being translated must be HL7 when using the HL7 plug-in.')
,('Translator','117','The schema cannot be parsed or the XML data does not match it.')
,('Translator','124','Configuration invalid. If this occurred when using the API, this is often because it cannot read the INI file. Check TRANSRoot.')
,('Translator','131','Map doesn’t match the convert call. ( Example: Attempting to convert EDI to XML by using an XML-to-EDI map.)')
,('Translator','140','API return code for system failure.')
,('Translator','154','Converting between this type of source and target is not supported.')
,('Translator','155','Could not open the source guideline.')
,('Translator','156','Could not open the target schema.')
,('Translator','157','Could not open the target guideline.')
,('Translator','158','Could not open the map file.')
,('Translator','171','Configuration invalid. If this occurred when using the API, this is often because it cannot read the INI file. Check TRANSRoot.')
,('Translator','172','Missing source guideline.')
,('Translator','173','Missing target guideline.')
,('Translator','174','Missing map.')
,('Translator','175','Missing input data when using inputMemory while using API.')
,('Translator','176','Cannot access input file when using inputByFile while using API.')
,('Translator','177','The source file separators are invalid as specified by separator in the API.')
,('Translator','178','The target file separators are invalid as specified by separator in the API.')
,('Translator','180','Failed on OutputFile or Options while using API. Cannot write the output file, or options like CALLBACKBUFFER and UNA are the wrong format.')
,('Translator','199','The API is getting a JNI exception occurred and can’t run Java.')
,('Translator','201','There is an error in the input path. ( Examples: An invalid or unreadable input file has been specified. A parameter, such as –t, has been left empty.)')
,('HVInStream','100','Validation ran successfully.')
,('HVInStream','110','Validation did not run successfully. The command line syntax is incorrect.')
,('HVInStream','120','There was a problem loading HVInStream.DLL library.')
,('HVInStream','129','$dir.ini or fsdir.ini was found but cannot be opened.')
,('HVInStream','130','There was a problem with initialization of the validation engine caused by a setup problem ( i.e. registry setup error) or $dir.ini or fsdir.ini is missing or contains invalid paths.')
,('HVInStream','131','$dir.ini or fsdir.ini cannot be found.')
,('HVInStream','132','Cannot access the "BASEROOT" specified in the $dir.ini or fsdir.ini.')
,('HVInStream','133','Cannot access the Database directory.')
,('HVInStream','134','Generic failure to read ini settings.')
,('HVInStream','140','A critical error prevented Validation from running successfully.')
,('HVInStream','150','Cannot find or open FS_HIPAA.dat in the Bin directory.')
,('HVInStream','160','Deprecated. Exceeded maximum allowable threads ( only when using API for validation).')
,('HVInStream','170','Deprecated. Exceeded maximum allowable processors ( only when using API for validation).')
,('HVInStream','180','The activity was cancelled by the user ( when using API).')
,('HVInStream','185','When running validation, Docsplitter, and Response Generator together from an API, Docsplitter failed.')
,('HVInStream','186','When running validation, Docsplitter, and Response Generator together from an API, Response Generator failed.')
,('HVInStream','187','When running validation, Docsplitter, and Response Generator together from an API, Response Generator and Docsplitter both failed.')
,('HVInStream','188','DataSwapper failed when run from an API.')
,('HVInStream','191','A critical trading partner automation error prevented Validation from running successfully. For more information, see *.TA1 in the same directory as the validation detail results file.')
,('HVInStream','195','Cannot find the error message file. Check the [ErrMsgFile] of your $dir.ini or fsdir.ini file.')
,('HVInStream','200','Configuration path error when using the -c command-line parameter.')
,('HVInStream','201','The input file could not be accessed. Check filenames and paths. Put quotes around paths that contain spaces.')
,('HVInStream','202','Output file could not be opened. Check filenames and paths. Put quotes around paths that contain spaces.')
,('RespGen','100','Response generation succeeded.')
,('RespGen','110','Response generation failed.')
,('RespGen','180','Failed to initialize Response Generator.')
,('RespGen','185','When running validation, Docsplitter, and Response Generator together in the API, Docsplitter failed.')
,('RespGen','186','When running validation, Docsplitter, and Response Generator together in the API, Response Generator failed.')
,('RespGen','187','When running validation, Docsplitter, and Response Generator together in the API, Docsplitter and Response Generator both failed.')
,('RespGen','201','Could not open input file.')
,('RespGen','202','Could not open one or more output files.')
,('RespGen','203','Could not open the template file.')
,('RespGen','204','Requested an inappropriate response document for your original document, such as: 1) If a 997 is requested for an incoming 997; 2) If an 824 is requested for an incoming 824 or 997; 3) If a 277 is requested for any document type other than an 837.')
,('RespGen','205','STRUS and STRUE records in detail file are mismatched. This can occur when ISA-IEA, GS-GE, and ST-SE segments are not paired properly. Also, be sure that you are using STRUS=1 and STRUE=1 in the validation profile ( APF) to create STRUS and STRUE records in the validation detail file.')
,('RespGen','206','Could not open the specified file; the file either doesn’t exist or can’t be opened. Check the file for read access permission. Response generator failed.')
,('DocSplitter','100','Docsplitter ran successfully.')
,('DocSplitter','110','Docsplitter did not run successfully.')
,('DocSplitter','150','Docsplitter did not run successfully because it could not load one of its internal libraries. This is probably an installation problem. Contact TIBCO Foresight Technical Support.')
,('DocSplitter','180','Docsplitter did not run successfully because of a problem other than those listed below. Contact TIBCO Foresight Technical Support.')
,('DocSplitter','185','When running validation, Docsplitter, and Response Generator together in the API, Docsplitter failed.')
,('DocSplitter','186','When running validation, Docsplitter, and Response Generator together in the API, Response Generator failed.')
,('DocSplitter','187','When running validation, Docsplitter, and Response Generator together in the API, Docsplitter and Response Generator both failed.')
,('DocSplitter','200','Docsplitter could not access the EDI input file. Binary data was encountered in the EDI input file. Bad segment tag or other document structure problem.')
,('DocSplitter','201','Docsplitter could not open the EDI output file.')
,('DocSplitter','202','Docsplitter could not open the Instream detail file.')
,('DocSplitter','203','Docsplitter could not open the report file.')
,('DocSplitter','204','Docsplitter could not open the input 997 file.')
,('DocSplitter','205','Docsplitter could not open the INI config file.')
;

