package com.getinsured.hix.dto.cap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class CapConsumerInfoDto {
	
	private String householdId;
	
	// Basic information
	private String firstName;
	private String lastName;
	private String leadStatus;
	public LinkedHashMap<String, String> leadStatusMap = new LinkedHashMap<String, String>();
	private String accessCode;
	private boolean servicedByFlag;
	private String servicedBy;
	private int servicedById;
	private String lastUpdatedby;
	private String drxClientId;
	
	// Employer information
	private String censusType;
	private String consumerType;
	private String employer;
	private Double contributionAmount;
	private String contributionAmountText;
	private String hraFundingType;
	private String pinCode;
	private int employeeId;
	private int employerId;
	Map<Integer, String> employerMap = new HashMap<Integer, String>();
	
	// Contact information
	private String email;
	private String phoneNumber;
	private boolean phoneFlag;
	private String mobileNumber;
	private boolean mobilePhoneFlag;
	private String homeNumber;
	private boolean homePhoneFlag;
	private String poaPhone;
	private boolean poaPhoneFlag;
	private String permissionToCallBack;
	private String autoDialerConsent;
	private String dataSharingConsent;
	private boolean enableCapDataSharingOption;
	private String stateCode;
	private String zipCode;
	
	// Other information
	private String affiliateName;
	private String flowName;
	private String affiliateHouseholdId;
	private String ffmSbeHouseholdId;
	private long affiliateId;
	private Integer flowId;
	private List<Map<String, Object>> affiliateList = new ArrayList<Map<String, Object>>();
	private Map<Integer, String> flowMap = new HashMap<Integer, String>();
	private List<String> availableProducts; // getAvailableAncillaries() -> List<String>
	private CmrMemberDto cmrMemberDto;
	private String flowNotes;
	private String medicareDrxURL;
	private String rasAddtlInfoUrl;
	private String rasApiKey;
	private boolean isAffFlowEditable;
	private boolean configFlagEditAffFlow;
	public boolean getConfigFlagEditAffFlow() {
		return configFlagEditAffFlow;
	}
	public void setConfigFlagEditAffFlow(boolean configFlagEditAffFlow) {
		this.configFlagEditAffFlow = configFlagEditAffFlow;
	}
	private List<String> years;
	private List<CapEmployerContribution> capEmployerContriList;
	
	public String getCensusType() {
		return censusType;
	}
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}
	public String getConsumerType() {
		return consumerType;
	}
	public void setConsumerType(String consumerType) {
		this.consumerType = consumerType;
	}
	public String getEmployer() {
		return employer;
	}
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	public Double getContributionAmount() {
		return contributionAmount;
	}
	public void setContributionAmount(Double contributionAmount) {
		this.contributionAmount = contributionAmount;
	}
	public String getContributionAmountText() {
		return contributionAmountText;
	}
	public void setContributionAmountText(String contributionAmountText) {
		this.contributionAmountText = contributionAmountText;
	}
	public String getHraFundingType() {
		return hraFundingType;
	}
	public void setHraFundingType(String hraFundingType) {
		this.hraFundingType = hraFundingType;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getEmployerId() {
		return employerId;
	}
	public void setEmployerId(int employerId) {
		this.employerId = employerId;
	}
	public Map<Integer, String> getEmployerMap() {
		return employerMap;
	}
	public void setEmployerMap(Map<Integer, String> employerMap) {
		this.employerMap = employerMap;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLeadStatus() {
		return leadStatus;
	}
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}
	public LinkedHashMap<String, String> getLeadStatusMap() {
		return leadStatusMap;
	}
	public void setLeadStatusMap(LinkedHashMap<String, String> leadStatusMap) {
		this.leadStatusMap = leadStatusMap;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	public boolean isServicedByFlag() {
		return servicedByFlag;
	}
	public void setServicedByFlag(boolean servicedByFlag) {
		this.servicedByFlag = servicedByFlag;
	}
	public String getServicedBy() {
		return servicedBy;
	}
	public void setServicedBy(String servicedBy) {
		this.servicedBy = servicedBy;
	}
	public int getServicedById() {
		return servicedById;
	}
	public void setServicedById(int servicedById) {
		this.servicedById = servicedById;
	}
	public String getLastUpdatedby() {
		return lastUpdatedby;
	}
	public void setLastUpdatedby(String lastUpdatedby) {
		this.lastUpdatedby = lastUpdatedby;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean isPhoneFlag() {
		return phoneFlag;
	}
	public void setPhoneFlag(boolean phoneFlag) {
		this.phoneFlag = phoneFlag;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public boolean isMobilePhoneFlag() {
		return mobilePhoneFlag;
	}
	public void setMobilePhoneFlag(boolean mobilePhoneFlag) {
		this.mobilePhoneFlag = mobilePhoneFlag;
	}
	public String getHomeNumber() {
		return homeNumber;
	}
	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}
	public boolean isHomePhoneFlag() {
		return homePhoneFlag;
	}
	public void setHomePhoneFlag(boolean homePhoneFlag) {
		this.homePhoneFlag = homePhoneFlag;
	}
	public String getPoaPhone() {
		return poaPhone;
	}
	public void setPoaPhone(String poaNumber) {
		this.poaPhone = poaNumber;
	}
	public boolean isPoaPhoneFlag() {
		return poaPhoneFlag;
	}
	public void setPoaPhoneFlag(boolean poaPhoneFlag) {
		this.poaPhoneFlag = poaPhoneFlag;
	}
	public String getPermissionToCallBack() {
		return permissionToCallBack;
	}
	public void setPermissionToCallBack(String permissionToCallBack) {
		this.permissionToCallBack = permissionToCallBack;
	}
	public String getAutoDialerConsent() {
		return autoDialerConsent;
	}
	public void setAutoDialerConsent(String autoDialerConsent) {
		this.autoDialerConsent = autoDialerConsent;
	}
	public String getDataSharingConsent() {
		return dataSharingConsent;
	}
	public void setDataSharingConsent(String dataSharingConsent) {
		this.dataSharingConsent = dataSharingConsent;
	}
	public boolean isEnableCapDataSharingOption() {
		return enableCapDataSharingOption;
	}
	public void setEnableCapDataSharingOption(boolean enableCapDataSharingOption) {
		this.enableCapDataSharingOption = enableCapDataSharingOption;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getAffiliateName() {
		return affiliateName;
	}
	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getAffiliateHouseholdId() {
		return affiliateHouseholdId;
	}
	public void setAffiliateHouseholdId(String affiliateHouseholdId) {
		this.affiliateHouseholdId = affiliateHouseholdId;
	}
	public String getFfmSbeHouseholdId() {
		return ffmSbeHouseholdId;
	}
	public void setFfmSbeHouseholdId(String ffmSbeHouseholdId) {
		this.ffmSbeHouseholdId = ffmSbeHouseholdId;
	}
	public long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}
	public Integer getFlowId() {
		return flowId;
	}
	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}
	public List<Map<String, Object>> getAffiliateList() {
		return affiliateList;
	}
	public void setAffiliateList(List<Map<String, Object>> affiliateList) {
		this.affiliateList = affiliateList;
	}
	public Map<Integer, String> getFlowMap() {
		return flowMap;
	}
	public void setFlowMap(Map<Integer, String> flowMap) {
		this.flowMap = flowMap;
	}
	
	public List<String> getAvailableProducts() {
		return availableProducts;
	}
	public void setAvailableProducts(List<String> availableProducts) {
		this.availableProducts = availableProducts;
	}
	public CmrMemberDto getCmrMemberDto() {
		return cmrMemberDto;
	}
	public void setCmrMemberDto(CmrMemberDto cmrMemberDto) {
		this.cmrMemberDto = cmrMemberDto;
	}
	public String getFlowNotes() {
		return flowNotes;
	}
	public void setFlowNotes(String flowNotes) {
		this.flowNotes = flowNotes;
	}
	
	public String getMedicareDrxURL() {
		return medicareDrxURL;
	}
	public void setMedicareDrxURL(String medicareDrxURL) {
		this.medicareDrxURL = medicareDrxURL;
	}
	public String getDrxClientId() {
		return drxClientId;
	}
	public void setDrxClientId(String drxClientId) {
		this.drxClientId = drxClientId;
	}
	public String getRasAddtlInfoUrl() {
		return rasAddtlInfoUrl;
	}
	public void setRasAddtlInfoUrl(String rasAddtlInfoUrl) {
		this.rasAddtlInfoUrl = rasAddtlInfoUrl;
	}
	public String getRasApiKey() {
		return rasApiKey;
	}
	public void setRasApiKey(String rasApiKey) {
		this.rasApiKey = rasApiKey;
	}
	public boolean getIsAffFlowEditable() {
		return isAffFlowEditable;
	}
	public void setIsAffFlowEditable(boolean isAffFlowEditable) {
		this.isAffFlowEditable = isAffFlowEditable;
	}
	public List<String> getYears() {
		return years;
	}
	public void setYears(List<String> years) {
		this.years = years;
	}
	
	public List<CapEmployerContribution> getCapEmployerContriList() {
		return capEmployerContriList;
	}
	public void setCapEmployerContriList(List<CapEmployerContribution> capEmployerContriList) {
		this.capEmployerContriList = capEmployerContriList;
	}

}