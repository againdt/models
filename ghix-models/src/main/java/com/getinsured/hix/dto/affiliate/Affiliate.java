package com.getinsured.hix.dto.affiliate;

import java.io.Serializable;

public class Affiliate implements Serializable{

	public Long affiliateId;
	public String company;
	public String affiliateName;
	public String status;
	public String typeOfTraffic;
	public String webPayout;
	public String callPayout;
	
	
	public long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAffiliateName() {
		return affiliateName;
	}
	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTypeOfTraffic() {
		return typeOfTraffic;
	}
	public void setTypeOfTraffic(String typeOfTraffic) {
		this.typeOfTraffic = typeOfTraffic;
	}
	public String getWebPayout() {
		return webPayout;
	}
	public void setWebPayout(String webPayout) {
		this.webPayout = webPayout;
	}
	public String getCallPayout() {
		return callPayout;
	}
	public void setCallPayout(String callPayout) {
		this.callPayout = callPayout;
	}
	
}
