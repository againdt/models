package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;

public class PlanAvailabilityForDrugResponse {
	private String drugNdc;
	private List<Map<String, String>> plans;
	
	public String getDrugNdc() {
		return drugNdc;
	}
	public void setDrugNdc(String drugNdc) {
		this.drugNdc = drugNdc;
	}
	public List<Map<String, String>> getPlans() {
		return plans;
	}
	public void setPlans(List<Map<String, String>> plans) {
		this.plans = plans;
	}
}
