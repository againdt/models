/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

/**
 * CSV DTO for Enrollment Reconciliation WB Discrepancy Reports
 * @author negi_s
 * @since 22/03/2017
 *
 */
public class EnrlDiscrepancyReportCsvDTO {
	
	public static final String HEADER = "Exchange Assigned Policy ID,Plan ID,Member Last Name,Member First Name,Exchange Assigned Member ID,Issuer Assigned Member ID,Subscriber Last Name,Subscriber First Name,Exchange Assigned Subscriber ID,Issuer Assigned Subscriber ID,Discrepancy Reason Code,Discrepancy Reason Text,YHI Value,Issuer Value,Date of Discrepancy,Recon File Name,Autofixed by YHI,Assignee";
	
	private Long exchangeAssignedPolicyId;
	private String planId;
	private String memberLastName;
	private String memberFirstName;
	private String exchangeAssignedMemberId;
	private String issuerAssignedMemberId;
	private String subscriberLastName;
	private String subscriberFirstName;
	private String exchangeAssignedSubscriberId;
	private String issuerAssignedSubscriberId;
	private String discrepancyReasonCode;
	private String discrepancyReasonText;
	private String yhiValue;
	private String issuerValue;
	private String dateOfDiscrepancy;
	private String reconFileName;
	private String autoFixedByYhi;
	private String assignee;

	public Long getExchangeAssignedPolicyId() {
		return exchangeAssignedPolicyId;
	}
	public void setExchangeAssignedPolicyId(Long exchangeAssignedPolicyId) {
		this.exchangeAssignedPolicyId = exchangeAssignedPolicyId;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getMemberLastName() {
		return memberLastName;
	}
	public void setMemberLastName(String memberLastName) {
		this.memberLastName = memberLastName;
	}
	public String getMemberFirstName() {
		return memberFirstName;
	}
	public void setMemberFirstName(String memberFirstName) {
		this.memberFirstName = memberFirstName;
	}
	public String getExchangeAssignedMemberId() {
		return exchangeAssignedMemberId;
	}
	public void setExchangeAssignedMemberId(String exchangeAssignedMemberId) {
		this.exchangeAssignedMemberId = exchangeAssignedMemberId;
	}
	public String getIssuerAssignedMemberId() {
		return issuerAssignedMemberId;
	}
	public void setIssuerAssignedMemberId(String issuerAssignedMemberId) {
		this.issuerAssignedMemberId = issuerAssignedMemberId;
	}
	public String getSubscriberLastName() {
		return subscriberLastName;
	}
	public void setSubscriberLastName(String subscriberLastName) {
		this.subscriberLastName = subscriberLastName;
	}
	public String getSubscriberFirstName() {
		return subscriberFirstName;
	}
	public void setSubscriberFirstName(String subscriberFirstName) {
		this.subscriberFirstName = subscriberFirstName;
	}
	public String getExchangeAssignedSubscriberId() {
		return exchangeAssignedSubscriberId;
	}
	public void setExchangeAssignedSubscriberId(String exchangeAssignedSubscriberId) {
		this.exchangeAssignedSubscriberId = exchangeAssignedSubscriberId;
	}
	public String getIssuerAssignedSubscriberId() {
		return issuerAssignedSubscriberId;
	}
	public void setIssuerAssignedSubscriberId(String issuerAssignedSubscriberId) {
		this.issuerAssignedSubscriberId = issuerAssignedSubscriberId;
	}
	public String getDiscrepancyReasonCode() {
		return discrepancyReasonCode;
	}
	public void setDiscrepancyReasonCode(String discrepancyReasonCode) {
		this.discrepancyReasonCode = discrepancyReasonCode;
	}
	public String getDiscrepancyReasonText() {
		return discrepancyReasonText;
	}
	public void setDiscrepancyReasonText(String discrepancyReasonText) {
		this.discrepancyReasonText = discrepancyReasonText;
	}
	public String getYhiValue() {
		return yhiValue;
	}
	public void setYhiValue(String yhiValue) {
		this.yhiValue = yhiValue;
	}
	public String getIssuerValue() {
		return issuerValue;
	}
	public void setIssuerValue(String issuerValue) {
		this.issuerValue = issuerValue;
	}
	public String getDateOfDiscrepancy() {
		return dateOfDiscrepancy;
	}
	public void setDateOfDiscrepancy(String dateOfDiscrepancy) {
		this.dateOfDiscrepancy = dateOfDiscrepancy;
	}
	public String getReconFileName() {
		return reconFileName;
	}
	public void setReconFileName(String reconFileName) {
		this.reconFileName = reconFileName;
	}
	public String getAutoFixedByYhi() {
		return autoFixedByYhi;
	}
	public void setAutoFixedByYhi(String autoFixedByYhi) {
		this.autoFixedByYhi = autoFixedByYhi;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	@Override
	public String toString() {
		return "EnrlDiscrepancyReportDTO [exchangeAssignedPolicyId=" + exchangeAssignedPolicyId + ", planId=" + planId
				+ ", memberLastName=" + memberLastName + ", memberFirstName=" + memberFirstName
				+ ", exchangeAssignedMemberId=" + exchangeAssignedMemberId + ", issuerAssignedMemberId="
				+ issuerAssignedMemberId + ", subscriberLastName=" + subscriberLastName + ", subscriberFirstName="
				+ subscriberFirstName + ", exchangeAssignedSubscriberId=" + exchangeAssignedSubscriberId
				+ ", issuerAssignedSubscriberId=" + issuerAssignedSubscriberId + ", discrepancyReasonCode="
				+ discrepancyReasonCode + ", discrepancyReasonText=" + discrepancyReasonText + ", yhiValue=" + yhiValue
				+ ", issuerValue=" + issuerValue + ", dateOfDiscrepancy=" + dateOfDiscrepancy + ", reconFileName="
				+ reconFileName + ", autoFixedByYhi=" + autoFixedByYhi + ", assignee=" + assignee + "]";
	}
	
	public String toCsv(char separator){
		String csv =  String.valueOf(exchangeAssignedPolicyId) + separator
	            + planId +separator
				+ memberLastName +separator 
	            + memberFirstName +separator 
				+ exchangeAssignedMemberId +separator 
	            + issuerAssignedMemberId +separator
				+ subscriberLastName +separator
				+ subscriberFirstName +separator 
				+ exchangeAssignedSubscriberId+separator 
				+ issuerAssignedSubscriberId +separator
				+ discrepancyReasonCode +separator 
				+ discrepancyReasonText +separator
				+ yhiValue +separator
				+ issuerValue +separator
				+ dateOfDiscrepancy +separator
				+ reconFileName+separator
				+ autoFixedByYhi +separator
				+ assignee
				;
		return  csv.replaceAll("null", "");
	}
}
