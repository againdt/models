DELETE FROM GI_APP_CONFIG WHERE PROPERTY_KEY = 'serff.DrugValidatorOptions.CopayData';
DELETE FROM GI_APP_CONFIG WHERE PROPERTY_KEY = 'serff.DrugValidatorOptions.CoinsuranceData';

INSERT INTO GI_APP_CONFIG (ID, PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION, CREATED_BY, CREATION_TIMESTAMP, LAST_UPDATED_BY, LAST_UPDATE_TIMESTAMP) VALUES (GI_APP_CONFIG_SEQ.NEXTVAL, 'serff.DrugValidatorOptions.CopayData', 'no charge,not applicable,no charge after deductible,copay after deductible,copay before deductible', 'Used for validations of Drug copay data', NULL, SYSTIMESTAMP, NULL, SYSTIMESTAMP);

INSERT INTO GI_APP_CONFIG (ID, PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION, CREATED_BY, CREATION_TIMESTAMP, LAST_UPDATED_BY, LAST_UPDATE_TIMESTAMP) VALUES (GI_APP_CONFIG_SEQ.NEXTVAL, 'serff.DrugValidatorOptions.CoinsuranceData', 'no charge,not applicable,no charge after deductible,coinsurance after deductible', 'Used for validations of Drug coinsurance data', NULL, SYSTIMESTAMP, NULL, SYSTIMESTAMP);
