/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

/**
 * @author panda_p
 *
 */
public class EnrolleeExtDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String memberId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffix;
	private String dob;
	private String gender;
	private String ssn;
	private String subscriberIndicator;
	private String tobacco;
	private String preferredEmail;
	private String relationshipToHcp;
	private String ratingArea;
	private String primaryPhoneNumber;
	
	
	private List<EnrolleeExtRelationshipDTO> relationships;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getSubscriberIndicator() {
		return subscriberIndicator;
	}

	public void setSubscriberIndicator(String subscriberIndicator) {
		this.subscriberIndicator = subscriberIndicator;
	}

	public String getTobacco() {
		return tobacco;
	}

	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}

	public List<EnrolleeExtRelationshipDTO> getRelationships() {
		return relationships;
	}

	public void setRelationships(List<EnrolleeExtRelationshipDTO> relationships) {
		this.relationships = relationships;
	}
	public String getPreferredEmail() {
		return preferredEmail;
	}

	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}

	public String getRelationshipToHcp() {
		return relationshipToHcp;
	}

	public void setRelationshipToHcp(String relationshipToHcp) {
		this.relationshipToHcp = relationshipToHcp;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}
}
