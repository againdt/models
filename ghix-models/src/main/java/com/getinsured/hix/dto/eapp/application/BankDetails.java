package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class BankDetails implements Serializable{
	private static final long serialVersionUID = 1L;
	private String routingNumber;
	private String accountNumber;
	private String accountType;
	private String bankName;
	
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public static BankDetails getSampleInstance(){
		BankDetails bd = new BankDetails();
		bd.setAccountNumber("23434234234");
		bd.setRoutingNumber("5555544");
		bd.setAccountType("savings");
		bd.setBankName("Bank Of Saudi");
		return bd;
	}
	

}
