package com.getinsured.hix.model.enrollment;

public enum EnrolleeAttributeEnum {
	id("id"),
	enrollmentId("enrollment.id"),
	firstName("firstName"),
	lastName("lastName"),
	state("homeAddressid.state"),
	enrolleeStatusCode("enrolleeLkpValue.lookupValueCode"),
	personType("personTypeLkp.lookupValueLabel"),	
	creationTimestamp("createdOn"),
	primaryPhoneNo("primaryPhoneNo");
	
    private final String name;       

    private EnrolleeAttributeEnum(String s) {
        name = s;
    }
    
    public String toString() {
	       return this.name;
	}


}
