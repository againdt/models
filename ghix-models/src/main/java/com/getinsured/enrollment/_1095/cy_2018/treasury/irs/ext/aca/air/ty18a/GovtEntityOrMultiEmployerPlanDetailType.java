
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;GovtEntityOrMultiEmployerPlan Detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for GovtEntityOrMultiEmployerPlanDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GovtEntityOrMultiEmployerPlanDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MultiEmployerPlanInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanMailingAddress"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanEIN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanTINTypeCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanTINValidateNameResponseDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanContactName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanContactPhone"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GovtEntityOrMultiEmployerPlanDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "govtEntityOrMultiEmployerPlanNm",
    "multiEmployerPlanInd",
    "govtEntityOrMultiEmployerPlanMailingAddress",
    "govtEntityOrMultiEmployerPlanEIN",
    "govtEntityOrMultiEmployerPlanTINTypeCd",
    "govtEntityOrMultiEmployerPlanTINValidateNameResponseDetail",
    "govtEntityOrMultiEmployerPlanContactName",
    "govtEntityOrMultiEmployerPlanContactPhone",
    "errorDetail"
})
public class GovtEntityOrMultiEmployerPlanDetailType {

    @XmlElement(name = "GovtEntityOrMultiEmployerPlanNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String govtEntityOrMultiEmployerPlanNm;
    @XmlElement(name = "MultiEmployerPlanInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected Boolean multiEmployerPlanInd;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanMailingAddress", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MailingAddressGrpType govtEntityOrMultiEmployerPlanMailingAddress;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanEIN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String govtEntityOrMultiEmployerPlanEIN;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanTINTypeCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String govtEntityOrMultiEmployerPlanTINTypeCd;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanTINValidateNameResponseDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected TINValidateNameResponseDetailType govtEntityOrMultiEmployerPlanTINValidateNameResponseDetail;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanContactName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CompletePersonNameDetailType govtEntityOrMultiEmployerPlanContactName;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanContactPhone", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String govtEntityOrMultiEmployerPlanContactPhone;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityOrMultiEmployerPlanNm() {
        return govtEntityOrMultiEmployerPlanNm;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanNm(String value) {
        this.govtEntityOrMultiEmployerPlanNm = value;
    }

    /**
     * Gets the value of the multiEmployerPlanInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMultiEmployerPlanInd() {
        return multiEmployerPlanInd;
    }

    /**
     * Sets the value of the multiEmployerPlanInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiEmployerPlanInd(Boolean value) {
        this.multiEmployerPlanInd = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public MailingAddressGrpType getGovtEntityOrMultiEmployerPlanMailingAddress() {
        return govtEntityOrMultiEmployerPlanMailingAddress;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanMailingAddress(MailingAddressGrpType value) {
        this.govtEntityOrMultiEmployerPlanMailingAddress = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanEIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityOrMultiEmployerPlanEIN() {
        return govtEntityOrMultiEmployerPlanEIN;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanEIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanEIN(String value) {
        this.govtEntityOrMultiEmployerPlanEIN = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanTINTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityOrMultiEmployerPlanTINTypeCd() {
        return govtEntityOrMultiEmployerPlanTINTypeCd;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanTINTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanTINTypeCd(String value) {
        this.govtEntityOrMultiEmployerPlanTINTypeCd = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanTINValidateNameResponseDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public TINValidateNameResponseDetailType getGovtEntityOrMultiEmployerPlanTINValidateNameResponseDetail() {
        return govtEntityOrMultiEmployerPlanTINValidateNameResponseDetail;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanTINValidateNameResponseDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanTINValidateNameResponseDetail(TINValidateNameResponseDetailType value) {
        this.govtEntityOrMultiEmployerPlanTINValidateNameResponseDetail = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanContactName property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public CompletePersonNameDetailType getGovtEntityOrMultiEmployerPlanContactName() {
        return govtEntityOrMultiEmployerPlanContactName;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanContactName(CompletePersonNameDetailType value) {
        this.govtEntityOrMultiEmployerPlanContactName = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanContactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovtEntityOrMultiEmployerPlanContactPhone() {
        return govtEntityOrMultiEmployerPlanContactPhone;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanContactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanContactPhone(String value) {
        this.govtEntityOrMultiEmployerPlanContactPhone = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
