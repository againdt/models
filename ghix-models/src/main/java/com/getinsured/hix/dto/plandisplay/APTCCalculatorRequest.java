package com.getinsured.hix.dto.plandisplay;

import java.math.BigDecimal;
import java.util.List;

public class APTCCalculatorRequest {

	private int memberCount;
	private Float maxAptc;
	private Float currentAptc;
	private BigDecimal currentStateSubsidy;
	private Boolean isChildPresent;
	private List<APTCPlanRequestDTO> plans;
	private List<APTCCalculatorMemberData> memberData;

	public int getMemberCount() {
		return memberCount;
	}
	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}
	public List<APTCPlanRequestDTO> getPlans() {
		return plans;
	}
	public void setPlans(List<APTCPlanRequestDTO> plans) {
		this.plans = plans;
	}
	public Float getMaxAptc() {
		return maxAptc;
	}
	public void setMaxAptc(Float maxAptc) {
		this.maxAptc = maxAptc;
	}
	public Float getCurrentAptc() {
		return currentAptc;
	}
	public void setCurrentAptc(Float currentAptc) {
		this.currentAptc = currentAptc;
	}
	public Boolean getIsChildPresent() {
		return isChildPresent;
	}
	public void setIsChildPresent(Boolean isChildPresent) {
		this.isChildPresent = isChildPresent;
	}		

	public List<APTCCalculatorMemberData> getMemberData() {
		return memberData;
	}
	public void setMemberData(List<APTCCalculatorMemberData> memberData) {
		this.memberData = memberData;
	}

	public BigDecimal getCurrentStateSubsidy() {
		return currentStateSubsidy;
	}

	public void setCurrentStateSubsidy(BigDecimal currentStateSubsidy) {
		this.currentStateSubsidy = currentStateSubsidy;
	}
}
