package com.getinsured.hix.dto.eapp;

import com.getinsured.hix.dto.eapp.application.Broker;
import com.getinsured.hix.dto.eapp.application.Household;
import com.getinsured.hix.dto.eapp.application.Plan;


/**
 * Represents an insurance application 
 * generic enough yet useful to semantically present 
 * different entities
 * @author root
 *
 */
public class Eapp {

	private Broker broker;
	private Plan plan;
	private Household household;
	/**
	 * MM/dd/yyyy
	 */
	private String coverageStartDate;
	public Broker getBroker() {
		return broker;
	}
	public void setBroker(Broker broker) {
		this.broker = broker;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public Household getHousehold() {
		return household;
	}
	public void setHousehold(Household household) {
		this.household = household;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	
	
	
	

}
