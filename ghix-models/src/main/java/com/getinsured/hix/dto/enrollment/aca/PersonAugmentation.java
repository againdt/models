package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class PersonAugmentation{
  @JsonProperty("PersonContactInformationAssociation")
  
  private Object PersonContactInformationAssociation;
  @JsonProperty("PersonPreferredLanguage")
  
  private Object PersonPreferredLanguage;
  @JsonProperty("PersonAssociation")
  
  private List<PersonAssociation> PersonAssociation;
  public void setPersonContactInformationAssociation(Object PersonContactInformationAssociation){
   this.PersonContactInformationAssociation=PersonContactInformationAssociation;
  }
  public Object getPersonContactInformationAssociation(){
   return PersonContactInformationAssociation;
  }
  public void setPersonPreferredLanguage(Object PersonPreferredLanguage){
   this.PersonPreferredLanguage=PersonPreferredLanguage;
  }
  public Object getPersonPreferredLanguage(){
   return PersonPreferredLanguage;
  }
  public void setPersonAssociation(List<PersonAssociation> PersonAssociation){
   this.PersonAssociation=PersonAssociation;
  }
  public List<PersonAssociation> getPersonAssociation(){
   return PersonAssociation;
  }
}