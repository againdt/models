package com.getinsured.hix.dto.consumer.goodrx;

import java.util.List;

public class GoodRxResponseDTO {
	List<String> errors;
	DrugDetailsDTO data;
	boolean success;
	
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public DrugDetailsDTO getData() {
		return data;
	}
	public void setData(DrugDetailsDTO data) {
		this.data = data;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

	
}
