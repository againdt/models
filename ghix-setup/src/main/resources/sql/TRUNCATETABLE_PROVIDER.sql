CREATE OR REPLACE PROCEDURE TRUNCATETABLE (table_name VARCHAR)
IS
BEGIN

   --DBMS_OUTPUT.PUT_LINE( 'TRUNCATE TABLE: ' ||tmp_table_name);	
   execute immediate 'TRUNCATE TABLE '||table_name;
   
  EXCEPTION
  	WHEN OTHERS THEN
   		DBMS_OUTPUT.PUT_LINE ('Exception while Truncating table : ' || table_name ||', '|| SQLERRM);
   
END;