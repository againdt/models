/**
 * 
 * @author santanu
 * @version 1.0
 * @since Oct 16, 2015 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

public class PediatricPlanResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean isChildOnly;
	private boolean isAdultOnly;
	private boolean isAdultAndChildOnly;
	
	/**
	 * Constructor.
	 */
	public PediatricPlanResponseDTO() {
		this.startResponse();
	}
	
	/**
	 * @return the isChildOnly
	 */
	public boolean isChildOnly() {
		return isChildOnly;
	}
	/**
	 * @param isChildOnly the isChildOnly to set
	 */
	public void setChildOnly(boolean isChildOnly) {
		this.isChildOnly = isChildOnly;
	}
	/**
	 * @return the isAdultOnly
	 */
	public boolean isAdultOnly() {
		return isAdultOnly;
	}
	/**
	 * @param isAdultOnly the isAdultOnly to set
	 */
	public void setAdultOnly(boolean isAdultOnly) {
		this.isAdultOnly = isAdultOnly;
	}
	/**
	 * @return the isAdultAndChildOnly
	 */
	public boolean isAdultAndChildOnly() {
		return isAdultAndChildOnly;
	}
	/**
	 * @param isAdultAndChildOnly the isAdultAndChildOnly to set
	 */
	public void setAdultAndChildOnly(boolean isAdultAndChildOnly) {
		this.isAdultAndChildOnly = isAdultAndChildOnly;
	}
	
}
