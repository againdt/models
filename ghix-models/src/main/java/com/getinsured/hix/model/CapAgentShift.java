package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * 
 * @author kaul_s
 * CapAgentEmployment.  It holds the agent information
 */
@Audited
@Entity
@Table(name = "cap_agent_shifts")
public class CapAgentShift implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6623732306104245281L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAP_AGENT_SHIFTS_SEQ")
	@SequenceGenerator(name = "CAP_AGENT_SHIFTS_SEQ", sequenceName = "CAP_AGENT_SHIFTS_SEQ", allocationSize = 1)
	@Column(name = "id")
	private Integer id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "SHIFT_HOURS")
	private float shiftHours;

	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "START_TIMESTAMP")
	private Date startTime;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "END_TIMESTAMP")
	private Date endTime;
	
	@Column(name = "MON")
	private Integer mon;
	
	@Column(name = "TUE")
	private Integer tue;
	
	@Column(name = "WED")
	private Integer wed;
	
	@Column(name = "THU")
	private Integer thu;
	
	@Column(name = "FRI")
	private Integer fri;
	
	@Column(name = "SAT")
	private Integer sat;
	
	@Column(name = "SUN")
	private Integer sun;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getShiftHours() {
		return shiftHours;
	}

	public void setShiftHours(float shiftHours) {
		this.shiftHours = shiftHours;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getMon() {
		return mon;
	}

	public void setMon(Integer mon) {
		this.mon = mon;
	}

	public Integer getTue() {
		return tue;
	}

	public void setTue(Integer tue) {
		this.tue = tue;
	}

	public Integer getWed() {
		return wed;
	}

	public void setWed(Integer wed) {
		this.wed = wed;
	}

	public Integer getThu() {
		return thu;
	}

	public void setThu(Integer thu) {
		this.thu = thu;
	}

	public Integer getFri() {
		return fri;
	}

	public void setFri(Integer fri) {
		this.fri = fri;
	}

	public Integer getSat() {
		return sat;
	}

	public void setSat(Integer sat) {
		this.sat = sat;
	}

	public Integer getSun() {
		return sun;
	}

	public void setSun(Integer sun) {
		this.sun = sun;
	}
}
