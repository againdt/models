package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * 
 * @author shanbhag_a
 *
 * Used to save {@link com.getinsured.hix.model.Plan} specific data for Enrollment.
 * 
 */


@Entity
@XmlAccessorType(XmlAccessType.NONE)
@Table(name="ENROLLMENT_PLAN")
public class EnrollmentPlan implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_PLAN_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_PLAN_SEQ", sequenceName = "ENROLLMENT_PLAN_SEQ", allocationSize = 1)
	@XmlElement(name="id")
	private Integer id;

	@XmlElement(name="networkType")
	@Column(name = "NETWORK_TYPE", nullable = true)
	private String networkType;

	@XmlElement(name="indivOopMax")
	@Column(name = "INDV_OOP_MAX", nullable = true)
	private Double indivOopMax;
	
	@XmlElement(name="familyOopMax")
	@Column(name = "FAMILY_OOP_MAX", nullable = true)
	private Double familyOopMax;

	@XmlElement(name="indivDeductible")
	@Column(name = "INDV_DEDUCTIBLE", nullable = true)
	private Double indivDeductible;
	
	@XmlElement(name="familyDeductible")
	@Column(name = "FAMILY_DEDUCTIBLE", nullable = true)
	private Double familyDeductible;

	@XmlElement(name="officeVisit")
	@Column(name = "OFFICE_VISIT", nullable = true)
	private String officeVisit;

	@XmlElement(name="genericMedication")
	@Column(name = "GENERIC_MEDICATION", nullable= true)
	private String genericMedication;

	@XmlElement(name="issuerSiteUrl")
	@Column(name = "ISSUER_SITE_URL" , nullable = true)
	private String issuerSiteUrl;

	@XmlElement(name="phoneNumber")
	@Column(name = "PHONE_NUMBER", nullable = true)
	private String phoneNumber;
	
	@XmlElement(name="planId")
	@Column(name = "PLAN_ID")
	private Integer planId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOfficeVisit() {
		return officeVisit;
	}

	public void setOfficeVisit(String officeVisits) {
		this.officeVisit = officeVisits;
	}

	public String getGenericMedication() {
		return genericMedication;
	}

	public void setGenericMedication(String genericMedication) {
		this.genericMedication = genericMedication;
	}

	public String getIssuerSiteUrl() {
		return issuerSiteUrl;
	}

	public void setIssuerSiteUrl(String issuerSiteUrl) {
		this.issuerSiteUrl = issuerSiteUrl;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planID) {
		this.planId = planID;
	}

	
	public Double getIndivOopMax() {
		return indivOopMax;
	}

	public void setIndivOopMax(Double indivOopMax) {
		this.indivOopMax = indivOopMax;
	}

	public Double getFamilyOopMax() {
		return familyOopMax;
	}

	public void setFamilyOopMax(Double familyOopMax) {
		this.familyOopMax = familyOopMax;
	}

	public Double getIndivDeductible() {
		return indivDeductible;
	}

	public void setIndivDeductible(Double indivDeductible) {
		this.indivDeductible = indivDeductible;
	}

	public Double getFamilyDeductible() {
		return familyDeductible;
	}

	public void setFamilyDeductible(Double familyDeductible) {
		this.familyDeductible = familyDeductible;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
}
