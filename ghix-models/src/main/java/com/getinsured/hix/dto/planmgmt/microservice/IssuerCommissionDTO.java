package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerCommissionDTO extends AuditDTO {

	private String insuranceType;
	private String firstYearCommissionAmount;
	private String firstYearCommissionFormat;
	private String secondYearCommissionAmout;
	private String secondYearCommissionFormat;
	private String frequency;
	private String startDate;
	private String endDate;
	private String lastUpdateDate;

	public IssuerCommissionDTO() {
		super();
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getFirstYearCommissionAmount() {
		return firstYearCommissionAmount;
	}

	public void setFirstYearCommissionAmount(String firstYearCommissionAmount) {
		this.firstYearCommissionAmount = firstYearCommissionAmount;
	}

	public String getFirstYearCommissionFormat() {
		return firstYearCommissionFormat;
	}

	public void setFirstYearCommissionFormat(String firstYearCommissionFormat) {
		this.firstYearCommissionFormat = firstYearCommissionFormat;
	}

	public String getSecondYearCommissionAmout() {
		return secondYearCommissionAmout;
	}

	public void setSecondYearCommissionAmout(String secondYearCommissionAmout) {
		this.secondYearCommissionAmout = secondYearCommissionAmout;
	}

	public String getSecondYearCommissionFormat() {
		return secondYearCommissionFormat;
	}

	public void setSecondYearCommissionFormat(String secondYearCommissionFormat) {
		this.secondYearCommissionFormat = secondYearCommissionFormat;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the lastUpdateDate
	 */
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * @param lastUpdateDate the lastUpdateDate to set
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
}
