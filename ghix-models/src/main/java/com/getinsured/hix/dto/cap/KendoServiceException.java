package com.getinsured.hix.dto.cap;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import org.springframework.http.HttpStatus;

/**
 * Created by jabelardo on 7/22/14.
 */
public class KendoServiceException extends GIRuntimeException {

    private static final long serialVersionUID = -8939065843179851102L;

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public KendoServiceException() {
        super();
    }

    public KendoServiceException(String message) {
        super(message);
    }

    public KendoServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public KendoServiceException(Throwable cause) {
        super(cause);
    }

    public KendoServiceException(HttpStatus httpStatus) {
        super();
        this.httpStatus = httpStatus;
    }

    public KendoServiceException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public KendoServiceException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public KendoServiceException(Throwable cause, HttpStatus httpStatus) {
        super(cause);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
