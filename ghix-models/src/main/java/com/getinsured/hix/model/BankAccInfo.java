package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.TypeDef;


/**
 * The persistent class for the bank_acc_info database table.
 * 
 */

@TypeDef(name = "vimoEncryptedString",
typeClass = VimoEncryptedStringType.class)
@Entity
@Table(name="bank_acc_info")
public class BankAccInfo implements Serializable
{
	public enum FundsTransferStatus
	{
		Y,N;
	}
	
	public enum BookEntrySecTrfStatus
	{
		Y,N;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BankAccInfo_Seq")
	@SequenceGenerator(name = "BankAccInfo_Seq", sequenceName = "bank_acc_info_seq", allocationSize = 1)
	private int id;
	
	@Column(name="routing_number")
	private String routingNumber;
	
	@Column(name="telegraphic_name")
	private String telegraphicName;


	@Column(name="customer_name")
	private String customerName;
	
	@Column(name="state_abbr")
	private String stateAbbr;
	
	@Column(name="city")
	private String city;
	
	@Column(name="funds_transfer_status")
	private FundsTransferStatus fundsTransferStatus;
	
	@Column(name="funds_settlement_only_status")
	private String fundsSettlementOnlyStatus;
	
	@Column(name="book_entry_sec_trf_status")
	private BookEntrySecTrfStatus bookEntrySecTrfStatus;
	
	@Column(name="date_of_last_revision")
	private Date dateOfLastRevision;
	
	
	public int getId()
	{
		return id;
	}
	
	public String getRoutingNumber()
	{
		return routingNumber;
	}
	
	public void setRoutingNumber(String routingNumber)
	{
		this.routingNumber = routingNumber;
	}
	
	public String getTelegraphicName()
	{
		return telegraphicName;
	}
	
	public void setTelegraphicName(String telegraphicName)
	{
		this.telegraphicName = telegraphicName;
	}
	
	public String getCustomerName()
	{
		return customerName;
	}
	
	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}
	
	public String getStateAbbr()
	{
		return stateAbbr;
	}
	
	public void setStateAbbr(String stateAbbr)
	{
		this.stateAbbr = stateAbbr;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public void setCity(String city)
	{
		this.city = city;
	}
	
	public FundsTransferStatus getFundsTransferStatus()
	{
		return fundsTransferStatus;
	}
	
	public void setFundsTransferStatus(FundsTransferStatus fundsTransferStatus)
	{
		this.fundsTransferStatus = fundsTransferStatus;
	}
	
	public String getFundsSettlementOnlyStatus()
	{
		return fundsSettlementOnlyStatus;
	}
	
	public void setFundsSettlementOnlyStatus(String fundsSettlementOnlyStatus)
	{
		this.fundsSettlementOnlyStatus = fundsSettlementOnlyStatus;
	}
	
	public BookEntrySecTrfStatus getBookEntrySecTrfStatus()
	{
		return bookEntrySecTrfStatus;
	}
	
	public void setBookEntrySecTrfStatus(BookEntrySecTrfStatus bookEntrySecTrfStatus)
	{
		this.bookEntrySecTrfStatus = bookEntrySecTrfStatus;
	}
	
	public Date getDateOfLastRevision()
	{
		return dateOfLastRevision;
	}
	
	public void setDateOfLastRevision(Date dateOfLastRevision)
	{
		this.dateOfLastRevision = dateOfLastRevision;
	}
	
	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("routingNumber=").append(routingNumber).append(", ");
        sb.append("telegraphicName=").append(telegraphicName).append(", ");
        sb.append("customerName=").append(customerName).append(", \n");
        sb.append("stateAbbr=").append(stateAbbr).append(", \n");
        sb.append("city=").append(city).append(" \n");
        sb.append("fundsTransferStatus=").append(fundsTransferStatus).append(" \n");
        sb.append("fundsSettlementOnlyStatus=").append(fundsSettlementOnlyStatus).append(" \n");
        sb.append("bookEntrySecTrfStatus=").append(bookEntrySecTrfStatus).append(" \n");
        sb.append("dateOfLastRevision=").append(dateOfLastRevision).append(" \n");
        return sb.toString();
    }

}
