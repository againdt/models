//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.07.10 at 02:26:22 PM IST 
//


package com.serff.template.ncqa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for a package of benefits that serves as a template for a set of related insurance plans.
 * 
 * <p>Java class for InsuranceProductType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceProductType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-pm}InsuranceProductIdentification"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-pm}MarketCategoryCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceProductType", namespace = "http://hix.cms.gov/0.1/hix-pm", propOrder = {
    "insuranceProductIdentification",
    "marketCategoryCode"
})
public class InsuranceProductType
    extends ComplexObjectType
{

    @XmlElement(name = "InsuranceProductIdentification", required = true)
    protected IdentificationType insuranceProductIdentification;
    @XmlElement(name = "MarketCategoryCode", required = true)
    protected InsuranceMarketCategoryCodeType marketCategoryCode;

    /**
     * Gets the value of the insuranceProductIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *     
     */
    public IdentificationType getInsuranceProductIdentification() {
        return insuranceProductIdentification;
    }

    /**
     * Sets the value of the insuranceProductIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *     
     */
    public void setInsuranceProductIdentification(IdentificationType value) {
        this.insuranceProductIdentification = value;
    }

    /**
     * Gets the value of the marketCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceMarketCategoryCodeType }
     *     
     */
    public InsuranceMarketCategoryCodeType getMarketCategoryCode() {
        return marketCategoryCode;
    }

    /**
     * Sets the value of the marketCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceMarketCategoryCodeType }
     *     
     */
    public void setMarketCategoryCode(InsuranceMarketCategoryCodeType value) {
        this.marketCategoryCode = value;
    }

}
