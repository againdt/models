package com.getinsured.hix.dto.plandisplay;

public class DrugSearchDTO {
	
	private String drugID;
	private String drugName;
	private String drugType;
	private String genericID;
	private String strength;
	private String drugFullName;

	public String getDrugID() {
		return drugID;
	}
	public void setDrugID(String drugID) {
		this.drugID = drugID;
	}
	public String getDrugName() {
		return drugName;
	}
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}
	public String getDrugType() {
		return drugType;
	}
	public void setDrugType(String drugType) {
		this.drugType = drugType;
	}
	public String getGenericID() {
		return genericID;
	}
	public void setGenericID(String genericID) {
		this.genericID = genericID;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getDrugFullName() {
		return drugFullName;
	}
	public void setDrugFullName(String drugFullName) {
		this.drugFullName = drugFullName;
	}
	
	
}
