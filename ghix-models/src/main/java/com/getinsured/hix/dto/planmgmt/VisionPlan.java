/**
 * 
 * @author santanu
 * @version 1.0
 * @since Jan 27, 2015 
 *
 * Define Vision plan data in response 
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.Map;

public class VisionPlan {
	
	private int id;
	private String hiosPlanId;
	private String name;
	private int issuerId;
	private String issuerName;
	private String issuerLogo;
	private Float premium;	
	private String brochureUrl;
	private String providerNetworkUrl;
	private String premiumPayment;
	private String outOfNetwkCoverage;
	private String outOfNetwkAvailability;
	private Map<String, Map<String, String>> benefits;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the hiosPlanId
	 */
	public String getHiosPlanId() {
		return hiosPlanId;
	}
	/**
	 * @param hiosPlanId the hiosPlanId to set
	 */
	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the issuerId
	 */
	public int getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the issuerLogo
	 */
	public String getIssuerLogo() {
		return issuerLogo;
	}
	/**
	 * @param issuerLogo the issuerLogo to set
	 */
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	
	/**
	 * @return the premium
	 */
	public Float getPremium() {
		return premium;
	}
	/**
	 * @param premium the premium to set
	 */
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	/**
	 * @return the brochureUrl
	 */
	public String getBrochureUrl() {
		return brochureUrl;
	}
	/**
	 * @param brochureUrl the brochureUrl to set
	 */
	public void setBrochureUrl(String brochureUrl) {
		this.brochureUrl = brochureUrl;
	}
	/**
	 * @return the providerNetworkUrl
	 */
	public String getProviderNetworkUrl() {
		return providerNetworkUrl;
	}
	/**
	 * @param providerNetworkUrl the providerNetworkUrl to set
	 */
	public void setProviderNetworkUrl(String providerNetworkUrl) {
		this.providerNetworkUrl = providerNetworkUrl;
	}
	/**
	 * @return the premiumPayment
	 */
	public String getPremiumPayment() {
		return premiumPayment;
	}
	/**
	 * @param premiumPayment the premiumPayment to set
	 */
	public void setPremiumPayment(String premiumPayment) {
		this.premiumPayment = premiumPayment;
	}	
	/**
	 * @return the outOfNetwkCoverage
	 */
	public String getOutOfNetwkCoverage() {
		return outOfNetwkCoverage;
	}
	/**
	 * @param outOfNetwkCoverage the outOfNetwkCoverage to set
	 */
	public void setOutOfNetwkCoverage(String outOfNetwkCoverage) {
		this.outOfNetwkCoverage = outOfNetwkCoverage;
	}
	/**
	 * @return the outOfNetwkAvailability
	 */
	public String getOutOfNetwkAvailability() {
		return outOfNetwkAvailability;
	}
	/**
	 * @param outOfNetwkAvailability the outOfNetwkAvailability to set
	 */
	public void setOutOfNetwkAvailability(String outOfNetwkAvailability) {
		this.outOfNetwkAvailability = outOfNetwkAvailability;
	}
	
	/**
	 * @return the benefits
	 */
	public Map<String, Map<String, String>> getBenefits() {
		return benefits;
	}
	
	/**
	 * @param benefits the benefits to set
	 */
	public void setBenefits(Map<String, Map<String, String>> benefits) {
		this.benefits = benefits;
	}
	
	
}
