package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.Map;

public class SearchIndividualPlanDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Map<String, IndividualPlanDetailsDTO> individualPlanDetails;

	public Map<String, IndividualPlanDetailsDTO> getIndividualPlanDetails() {
		return individualPlanDetails;
	}

	public void setIndividualPlanDetails(Map<String, IndividualPlanDetailsDTO> individualPlanDetails) {
		this.individualPlanDetails = individualPlanDetails;
	}

}
