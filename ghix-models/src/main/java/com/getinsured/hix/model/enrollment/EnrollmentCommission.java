package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;

/**
 * The persistent class for the table.
 * 
 */

@Entity
@Table(name="ENROLLMENT_COMMISSION")
public class EnrollmentCommission implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_COMMISSION_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_COMMISSION_SEQ", sequenceName = "ENROLLMENT_COMMISSION_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="CARRIER_NAME")
	private String carrier;
	
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	@Column(name="ISSUER_ID")
	private Integer IssuerId ;
	
	@Column(name="STATE")
	private String state ;
	
	@Column(name="ENROLLMENT_ID")
	private Integer enrollmentId;	

	@Column(name = "COMMISSION_AMT")
	private Float commissionAmt;

	@Column(name="COMMISSION_DATE")
	@Temporal(value = TemporalType.DATE)
	private Date commissionDate ;
		
	@Column(name="CREATION_TIMESTAMP")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date createdOn;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date updatedOn;
	
	@Column(name="PREMIUM_MONTH")
	@Temporal(value = TemporalType.DATE)
	private Date premiumMonth;
	
	@Column(name="AMT_PAID_TO_DATE")
	private Float amtPaidToDate;
	
	@Column(name="COMMISSIONABLE_AMOUNT")
	private Float commissionableAmount;
	
	@Column(name="COMMISSION_MODE")
	private String mode;
	
	@Column(name="COMMISSION_TYPE")
	private String commissionType ;
	
	@Column(name="CARRIER_FEED_DETAILS_ID")
	private Integer carrierFeedDetailsId ;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIssuerId() {
		return IssuerId;
	}

	public void setIssuerId(Integer issuerId) {
		IssuerId = issuerId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Float getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(Float commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public Date getCommissionDate() {
		return commissionDate;
	}

	public void setCommissionDate(Date commissionDate) {
		this.commissionDate = commissionDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Date getPremiumMonth() {
		return premiumMonth;
	}

	public void setPremiumMonth(Date premiumMonth) {
		this.premiumMonth = premiumMonth;
	}

	public Float getAmtPaidToDate() {
		return amtPaidToDate;
	}

	public void setAmtPaidToDate(Float amtPaidToDate) {
		this.amtPaidToDate = amtPaidToDate;
	}

	public Float getCommissionableAmount() {
		return commissionableAmount;
	}

	public void setCommissionableAmount(Float commissionableAmount) {
		this.commissionableAmount = commissionableAmount;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}
	
	public Integer getCarrierFeedDetailsId() {
		return carrierFeedDetailsId;
	}

	public void setCarrierFeedDetailsId(Integer carrierFeedDetailsId) {
		this.carrierFeedDetailsId = carrierFeedDetailsId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	
	}
