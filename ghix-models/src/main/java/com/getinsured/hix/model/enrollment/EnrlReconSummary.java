package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_RECON_SUMMARY")
public class EnrlReconSummary  implements Serializable, SortableEntity {
	
	private static final long serialVersionUID = 1L;
	
	public enum SummaryStatus {
		LOADED("Loaded"),
		TRANSLATED("Translated"),
		COMPARED("Compared"),
		AUTOFIXED("Autofixed"),
		SUMMARIZED("Summarized"),
		COMPLETED("Completed"),
		DUPLICATE("Duplicate");
		
		private final String stage;       

	    private SummaryStatus(String s) {
	        stage = s;
	    }
	    
	    public String toString() {
		       return this.stage;
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_summary_seq")
	@SequenceGenerator(name = "enrl_recon_summary_seq", sequenceName = "ENRL_RECON_SUMMARY_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="FILE_NAME")
	private String fileName;
	
	@Column(name="HIOS_ISSUER_ID")
	private String hiosIssuerId;
	
	@Column(name="MONTH")
	private Integer month;
	
	@Column(name="YEAR")
	private Integer year;
	
	@Column(name="DATE_RECEIVED")
	private Date dateReceived;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="COMMENTS")
	private String comments;
	
	@Column(name="DATE_PROCESSED")
	private Date dateProcessed;
	
	@Column(name="TOTAL_ENRL_CNT_IN_FILE")
	private Integer totalEnrlCntInFile;
	
	@Column(name="TOTAL_ENRL_CNT_IN_HIX")
	private Integer totalEnrlCntInHix;
	
	@Column(name="ENRL_CNT_SUCCESS")
	private Integer enrlCntSuccess;
	
	@Column(name="ENRL_CNT_DISCREPANCIES")
	private Integer enrlCntDiscrepancies;
	
	@Column(name="ENRL_CNT_MISSING_IN_HIX")
	private Integer enrlCntMissingInHix;
	
	@Column(name="ENRL_CNT_MISSING_IN_FILE")
	private Integer enrlCntMissingInFile;
	
	@Column(name="REC_CNT_FAILED")
	private Integer recCntFailed;
	
	@Column(name="CNT_DISCREPANCIES")
	private Integer cntDiscrepancies;
	
	@Column(name="QHPID")
	private String qhpid;
	
	@Column(name="ISSUER_EXTRACT_DATE")
	private Date issuerExtractDate;
	
	@Column(name="ISSUER_TOTAL_RECORDS_CNT")
	private Integer issuerTotalRecordsCnt;
	
	@Column(name="ISSUER_TOTAL_DEPENDENT_CNT")
	private Integer issuerTotalDependentCnt;
	
	@Column(name="ISSUER_TOTAL_GROSS_PREMIUM")
	private Double issuerTotalGrossPremium;
	
	@Column(name="ISSUER_TOTAL_SUBSCRIBER_CNT")
	private Integer issuerTotalSubscriberCnt;
	
	@Column(name="ISSUER_TOTAL_APTC")
	private Double issuerTotalAptc;
	
	@Column(name="BATCH_ID")
	private Integer batchId;
	
	@Column(name="TOTAL_RECORDS_CNT")
	private Integer totalRecordsCnt;
	
	@Column(name="TOTAL_SUBSCRIBER_CNT")
	private Integer totalSubscriberCnt;
	
	@Column(name="TOTAL_DEPENDENT_CNT")
	private Integer totalDependentCnt;
	
	@Column(name="DISCREPANCY_RPT_NAME")
	private String discrepancyReportName;
	
	@Column(name="DISCREPANCY_RPT_CREATE_DATE")
	private Date discrepancyReportCreationTimestamp;
	
	@Column(name="ISSUER_CUTOFF_DATE")
	private Date issuerCutoffDate;
	
	@Column(name="DISCREPANCY_RPT_ERROR")
	private String discrepancyReportError;
	
	@Column(name="DISCREPANCY_RPT_ECM_DOC_ID")
	private String discrepancyReportEcmId;
	
	@Column(name="COVERAGE_YEAR")
	private Integer coverageYear;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Date getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getDateProcessed() {
		return dateProcessed;
	}

	public void setDateProcessed(Date dateProcessed) {
		this.dateProcessed = dateProcessed;
	}

	public Integer getTotalEnrlCntInFile() {
		return totalEnrlCntInFile;
	}

	public void setTotalEnrlCntInFile(Integer totalEnrlCntInFile) {
		this.totalEnrlCntInFile = totalEnrlCntInFile;
	}

	public Integer getTotalEnrlCntInHix() {
		return totalEnrlCntInHix;
	}

	public void setTotalEnrlCntInHix(Integer totalEnrlCntInHix) {
		this.totalEnrlCntInHix = totalEnrlCntInHix;
	}

	public Integer getEnrlCntSuccess() {
		return enrlCntSuccess;
	}

	public void setEnrlCntSuccess(Integer enrlCntSuccess) {
		this.enrlCntSuccess = enrlCntSuccess;
	}

	public Integer getEnrlCntDiscrepancies() {
		return enrlCntDiscrepancies;
	}

	public void setEnrlCntDiscrepancies(Integer enrlCntDiscrepancies) {
		this.enrlCntDiscrepancies = enrlCntDiscrepancies;
	}

	public Integer getEnrlCntMissingInHix() {
		return enrlCntMissingInHix;
	}

	public void setEnrlCntMissingInHix(Integer enrlCntMissingInHix) {
		this.enrlCntMissingInHix = enrlCntMissingInHix;
	}

	public Integer getEnrlCntMissingInFile() {
		return enrlCntMissingInFile;
	}

	public void setEnrlCntMissingInFile(Integer enrlCntMissingInFile) {
		this.enrlCntMissingInFile = enrlCntMissingInFile;
	}

	public Integer getRecCntFailed() {
		return recCntFailed;
	}

	public void setRecCntFailed(Integer recCntFailed) {
		this.recCntFailed = recCntFailed;
	}

	public Integer getCntDiscrepancies() {
		return cntDiscrepancies;
	}

	public void setCntDiscrepancies(Integer cntDiscrepancies) {
		this.cntDiscrepancies = cntDiscrepancies;
	}

	public String getQhpid() {
		return qhpid;
	}

	public void setQhpid(String qhpid) {
		this.qhpid = qhpid;
	}

	public Date getIssuerExtractDate() {
		return issuerExtractDate;
	}

	public void setIssuerExtractDate(Date issuerExtractDate) {
		this.issuerExtractDate = issuerExtractDate;
	}

	public Integer getIssuerTotalRecordsCnt() {
		return issuerTotalRecordsCnt;
	}

	public void setIssuerTotalRecordsCnt(Integer issuerTotalRecordsCnt) {
		this.issuerTotalRecordsCnt = issuerTotalRecordsCnt;
	}

	public Integer getIssuerTotalDependentCnt() {
		return issuerTotalDependentCnt;
	}

	public void setIssuerTotalDependentCnt(Integer issuerTotalDependentCnt) {
		this.issuerTotalDependentCnt = issuerTotalDependentCnt;
	}

	public Double getIssuerTotalGrossPremium() {
		return issuerTotalGrossPremium;
	}

	public void setIssuerTotalGrossPremium(Double issuerTotalGrossPremium) {
		this.issuerTotalGrossPremium = issuerTotalGrossPremium;
	}

	public Double getIssuerTotalAptc() {
		return issuerTotalAptc;
	}

	public void setIssuerTotalAptc(Double issuerTotalAptc) {
		this.issuerTotalAptc = issuerTotalAptc;
	}

	public Integer getBatchId() {
		return batchId;
	}

	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}

	public Integer getTotalRecordsCnt() {
		return totalRecordsCnt;
	}

	public void setTotalRecordsCnt(Integer totalRecordsCnt) {
		this.totalRecordsCnt = totalRecordsCnt;
	}

	public Integer getTotalSubscriberCnt() {
		return totalSubscriberCnt;
	}

	public void setTotalSubscriberCnt(Integer totalSubscriberCnt) {
		this.totalSubscriberCnt = totalSubscriberCnt;
	}

	public Integer getTotalDependentCnt() {
		return totalDependentCnt;
	}

	public void setTotalDependentCnt(Integer totalDependentCnt) {
		this.totalDependentCnt = totalDependentCnt;
	}

	public String getDiscrepancyReportName() {
		return discrepancyReportName;
	}

	public void setDiscrepancyReportName(String discrepancyReportName) {
		this.discrepancyReportName = discrepancyReportName;
	}

	public Date getDiscrepancyReportCreationTimestamp() {
		return discrepancyReportCreationTimestamp;
	}

	public void setDiscrepancyReportCreationTimestamp(Date discrepancyReportCreationTimestamp) {
		this.discrepancyReportCreationTimestamp = discrepancyReportCreationTimestamp;
	}
	

	public Date getIssuerCutoffDate() {
		return issuerCutoffDate;
	}

	public void setIssuerCutoffDate(Date issuerCutoffDate) {
		this.issuerCutoffDate = issuerCutoffDate;
	}

	public String getDiscrepancyReportError() {
		return discrepancyReportError;
	}

	public void setDiscrepancyReportError(String discrepancyReportError) {
		this.discrepancyReportError = discrepancyReportError;
		if(null != discrepancyReportError && discrepancyReportError.length()>1000){
			this.discrepancyReportError = discrepancyReportError.substring(0, 1000);
		}
	}

	public String getDiscrepancyReportEcmId() {
		return discrepancyReportEcmId;
	}

	public void setDiscrepancyReportEcmId(String discrepancyReportEcmId) {
		this.discrepancyReportEcmId = discrepancyReportEcmId;
	}

	public Integer getIssuerTotalSubscriberCnt() {
		return issuerTotalSubscriberCnt;
	}

	public void setIssuerTotalSubscriberCnt(Integer issuerTotalSubscriberCnt) {
		this.issuerTotalSubscriberCnt = issuerTotalSubscriberCnt;
	}

	/**
	 * @return the coverageYear
	 */
	public Integer getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setDateReceived(new TSDate());
		this.setDateProcessed(new TSDate()); 
	}
	
	@PreUpdate
	public void PreUpdate()
	{
		this.setDateProcessed(new TSDate()); 
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
        columnNames.add("id");
        columnNames.add("hiosIssuerId");
        return columnNames;
	}
}
