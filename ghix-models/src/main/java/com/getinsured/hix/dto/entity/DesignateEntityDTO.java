package com.getinsured.hix.dto.entity;

import java.io.Serializable;

/**
 * Implemented for IND-47. Encapsulates Request information to be sent to AHBX in event of
 * Agent/Assister delegation and when Agent/Assister accepts/declines delegation request.
 */
public class DesignateEntityDTO implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer brokerAssisterId;
	private Integer employerId;
	private Integer individualId;
	private String status;
	private String role;
	private String recordType;

	public DesignateEntityDTO() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public Integer getEmployerId() {
		return this.employerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getIndividualId() {
		return individualId;
	}

	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public Integer getBrokerAssisterId() {
		return brokerAssisterId;
	}

	public void setBrokerAssisterId(Integer brokerAssisterId) {
		this.brokerAssisterId = brokerAssisterId;
	}

	@Override
	public String toString() {
		return "DesignateEntityDTO details: Id = "+id+", BrokerAssisterId = "+brokerAssisterId+", " +
				"EmployerId = "+employerId+", IndividualId = "+individualId+", Status = "+status;
	}
}