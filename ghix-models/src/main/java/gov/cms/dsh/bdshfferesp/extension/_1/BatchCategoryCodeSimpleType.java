//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.10.19 at 01:02:39 PM IST 
//


package gov.cms.dsh.bdshfferesp.extension._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BatchCategoryCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BatchCategoryCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="IRS_EOY_SUCCESS_RESP"/>
 *     &lt;enumeration value="IRS_EOY_FILE_REJECTED_SCHEMA_INVALID_RESP"/>
 *     &lt;enumeration value="IRS_EOY_FILE_REJECTED_BUS_RULE_RESP"/>
 *     &lt;enumeration value="IRS_EOY_RECORD_CORRECTIONS_NEEDED_RESP"/>
 *     &lt;enumeration value="IRS_EOY_MISSING_FILE_RESP"/>
 *     &lt;enumeration value="IRS_EOY_DUPLICATE_FILE_REJECTED_RESP"/>
 *     &lt;enumeration value="IRS_EOY_FILE_REJECTED_ATTACHMENT_ERROR_RESP"/>
 *     &lt;enumeration value="IRS_EOY_ACK_RESP"/>
 *     &lt;enumeration value="IRS_EOY_NACK_FILE_REJECTED_BY_IRS_RESP"/>
 *     &lt;enumeration value="IRS_EOY_NACK_BATCH_REJECTED_BY_HUB_RESP"/>
 *     &lt;enumeration value="IRS_EOY_NACK_FILE_REJECTED_AT_IRS_PORTAL_RESP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BatchCategoryCodeSimpleType")
@XmlEnum
public enum BatchCategoryCodeSimpleType {


    /**
     * Identifies an End of Year (EOY) file accepted by the Internal Revenue Service (IRS) for which IRS finds no errors. Response returned without an attachment.
     * 
     */
    IRS_EOY_SUCCESS_RESP,

    /**
     * Identifies that an End of Year (EOY) file accepted by the Internal Revenue Service (IRS) failed IRS XML schema validation. Response returned without an attachment.
     * 
     */
    IRS_EOY_FILE_REJECTED_SCHEMA_INVALID_RESP,

    /**
     * Identifies that an End of Year (EOY) file accepted by the Internal Revenue Service (IRS) contains business errors identified by IRS. Response returned with an attachment.
     * 
     */
    IRS_EOY_FILE_REJECTED_BUS_RULE_RESP,

    /**
     * Identifies that an End of Year (EOY) file accepted by the Internal Revenue Service (IRS) contains record corrections identified by IRS. Response returned with an attachment.
     * 
     */
    IRS_EOY_RECORD_CORRECTIONS_NEEDED_RESP,

    /**
     * Identifies End of Year (EOY) files that the Internal Revenue Service (IRS) has identified as missing from the batch. Response returned without an attachment.
     * 
     */
    IRS_EOY_MISSING_FILE_RESP,

    /**
     * Identifies an End of Year (EOY) file accepted by the Internal Revenue Service (IRS) which IRS identifies as a duplicate of a file already received by IRS. Response returned without an attachment.
     * 
     */
    IRS_EOY_DUPLICATE_FILE_REJECTED_RESP,

    /**
     * Identifies an End of Year (EOY) file accepted by the Internal Revenue Service (IRS); however, rejects the file at the time of processing. Response returned without an attachment.
     * 
     */
    IRS_EOY_FILE_REJECTED_ATTACHMENT_ERROR_RESP,

    /**
     * Identifies an End of Year (EOY) file transmission accepted by the Internal Revenue Service (IRS). Response returned without an attachment.
     * 
     */
    IRS_EOY_ACK_RESP,

    /**
     * Identifies an End of Year (EOY) file transmission rejected by the Internal Revenue Service (IRS). Response returned without an attachment.
     * 
     */
    IRS_EOY_NACK_FILE_REJECTED_BY_IRS_RESP,

    /**
     * Identifies an End of Year (EOY) batch not accepted by the Hub due to validation failures. Response returned without an attachment.
     * 
     */
    IRS_EOY_NACK_BATCH_REJECTED_BY_HUB_RESP,

    /**
     * Identifies an End of Year (EOY) file transmission that the Internal Revenue Service (IRS) Portal rejects. Response returned without an attachment.
     * 
     */
    IRS_EOY_NACK_FILE_REJECTED_AT_IRS_PORTAL_RESP;

    public String value() {
        return name();
    }

    public static BatchCategoryCodeSimpleType fromValue(String v) {
        return valueOf(v);
    }

}
