
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Name {

    @Expose
    private String middleName;
    @Expose
    private String lastName;
    @Expose
    private String firstName;
    @Expose
    private String suffix;

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

}
