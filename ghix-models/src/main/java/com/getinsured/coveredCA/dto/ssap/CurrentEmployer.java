
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class CurrentEmployer {
	
	@Expose
	private String name;
	@Expose
	private String employeePremiumAmount;
	@Expose
	private String howOftenPremium;
	@Expose
	private Boolean meetMinimumStandardIndicator;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmployeePremiumAmount() {
		return employeePremiumAmount;
	}
	public void setEmployeePremiumAmount(String employeePremiumAmount) {
		this.employeePremiumAmount = employeePremiumAmount;
	}
	public String getHowOftenPremium() {
		return howOftenPremium;
	}
	public void setHowOftenPremium(String howOftenPremium) {
		this.howOftenPremium = howOftenPremium;
	}
	public Boolean getMeetMinimumStandardIndicator() {
		return meetMinimumStandardIndicator;
	}
	public void setMeetMinimumStandardIndicator(Boolean meetMinimumStandardIndicator) {
		this.meetMinimumStandardIndicator = meetMinimumStandardIndicator;
	}

    
}
