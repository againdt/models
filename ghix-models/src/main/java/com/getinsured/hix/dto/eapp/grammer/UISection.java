package com.getinsured.hix.dto.eapp.grammer;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a section
 * @author root
 *
 */
public class UISection {
	private String title;
	private String description;
	private String footnote;
	private List<UIComponent> questions;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFootnote() {
		return footnote;
	}
	public void setFootnote(String footnote) {
		this.footnote = footnote;
	}
	public List<UIComponent> getQuestions() {
		return questions;
	}
	public void setQuestions(List<UIComponent> questions) {
		this.questions = questions;
	}
	
	

	public static UISection getSampleInstance(){
		UISection section = new UISection();
		int seed = (int)(Math.random()*8769);
		section.setTitle("Section Title " + seed);
		section.setDescription("Section description " + seed);
		section.setFootnote("optional footnote " + seed);
		List<UIComponent> questions = new ArrayList<UIComponent>();
		questions.add(UIComponent.getSampleInstance());
		questions.add(UIComponent.getSampleInstance());
		questions.add(UIComponent.getSampleInstance());
		section.setQuestions(questions);
		return section;
	}
	

	
	
}
