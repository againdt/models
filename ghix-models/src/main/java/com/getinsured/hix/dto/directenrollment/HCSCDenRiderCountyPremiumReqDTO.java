package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;

public class HCSCDenRiderCountyPremiumReqDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1090849839369499983L;

	private String year;
	private String state;
	private String county;
	private String planName;	
	private int numOfAdult;						//	HIX-60004
	private int numOfChildren;					//	numOfAdult and numOfChildren used to calculate Family Dental plan premium
	private int numOfChildrenForPediatric;		//	numOfChildrenForPediatric used to calculate Pediatric Dental plan premium
	
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public int getNumOfAdult() {
		return numOfAdult;
	}
	public void setNumOfAdult(int numOfAdult) {
		this.numOfAdult = numOfAdult;
	}
	public int getNumOfChildren() {
		return numOfChildren;
	}
	public void setNumOfChildren(int numOfChildren) {
		this.numOfChildren = numOfChildren;
	}
	public int getNumOfChildrenForPediatric() {
		return numOfChildrenForPediatric;
	}
	public void setNumOfChildrenForPediatric(int numOfChildrenForPediatric) {
		this.numOfChildrenForPediatric = numOfChildrenForPediatric;
	}
}
