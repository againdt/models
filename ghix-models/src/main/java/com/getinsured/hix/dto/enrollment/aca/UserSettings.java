package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class UserSettings{
  // @JsonProperty("licenses")
  
  // private List<Licenses> licenses;
  @JsonProperty("orgName")
  
  private String orgName;
  @JsonProperty("ffmUserName")
  
  private String ffmUserName;
//  public void setLicenses(List<Licenses> licenses){
//   this.licenses=licenses;
//  }
//  public List<Licenses> getLicenses(){
//   return licenses;
//  }
  public void setOrgName(String orgName){
   this.orgName=orgName;
  }
  public String getOrgName(){
   return orgName;
  }
  public void setFfmUserName(String ffmUserName){
   this.ffmUserName=ffmUserName;
  }
  public String getFfmUserName(){
   return ffmUserName;
  }
}