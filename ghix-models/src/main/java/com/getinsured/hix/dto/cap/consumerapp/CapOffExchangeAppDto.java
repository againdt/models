package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.Date;

public class CapOffExchangeAppDto implements Serializable{

	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private String applicationStatus;
	private long ssapId;
	private String extenalAppId;
	private String userName;
	private String password;
	private String policyEffectiveDate;

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public long getSsapId() {
		return ssapId;
	}

	public void setSsapId(long ssapId) {
		this.ssapId = ssapId;
	}

	public String getExtenalAppId() {
		return extenalAppId;
	}

	public void setExtenalAppId(String extenalAppId) {
		this.extenalAppId = extenalAppId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPolicyEffectiveDate() {
		return policyEffectiveDate;
	}

	public void setPolicyEffectiveDate(String policyEffectiveDate) {
		this.policyEffectiveDate = policyEffectiveDate;
	}
}
