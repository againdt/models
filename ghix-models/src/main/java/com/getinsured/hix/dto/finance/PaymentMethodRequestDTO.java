package com.getinsured.hix.dto.finance;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.PaymentMethods;

/**
 * Request DTO class for PaymentMethod model.
 * @since 24th September 2014
 * @author Sharma_k
 *
 */
public class PaymentMethodRequestDTO implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	private Integer id = null;
	
	private PaymentMethods.PaymentType paymentType = null;
	
	private List<PaymentMethods.PaymentType> paymentTypeList = null;
	
	private PaymentMethods.PaymentStatus paymentMethodStatus = null;
	
	private List<PaymentMethods.PaymentStatus> paymentMethodStatusList = null;
	
	private PaymentMethods.PaymentIsDefault isDefaultPaymentMethod = null;
	
	private String paymentMethodName = null;
	
	private List<String> paymentMethodNameList = null;
	
	private List<PaymentMethodDTO> paymentMethodDTOList = null;
	
	private Integer moduleID = null;
	
	private PaymentMethods.ModuleName moduleName = null;
	
	private String subscriptionID = null;
	
	private Date updatedOn;
	
	private String sortOrder = null;
	private String sortBy = null;
	
	private int pageSize = 0;
	private int pageNumber = 0;
	private int startRecord = 0;
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	private boolean isPaginationRequired = false;
	
	/**
	 * HIX-70250
	 */
	private Integer lastUpdatedBy;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the paymentType
	 */
	public PaymentMethods.PaymentType getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(PaymentMethods.PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the paymentTypeList
	 */
	public List<PaymentMethods.PaymentType> getPaymentTypeList() {
		return paymentTypeList;
	}

	/**
	 * @param paymentTypeList the paymentTypeList to set
	 */
	public void setPaymentTypeList(List<PaymentMethods.PaymentType> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}

	/**
	 * @return the paymentMethodStatus
	 */
	public PaymentMethods.PaymentStatus getPaymentMethodStatus() {
		return paymentMethodStatus;
	}

	/**
	 * @param paymentMethodStatus the paymentMethodStatus to set
	 */
	public void setPaymentMethodStatus(
			PaymentMethods.PaymentStatus paymentMethodStatus) {
		this.paymentMethodStatus = paymentMethodStatus;
	}

	/**
	 * @return the paymentMethodStatusList
	 */
	public List<PaymentMethods.PaymentStatus> getPaymentMethodStatusList() {
		return paymentMethodStatusList;
	}

	/**
	 * @param paymentMethodStatusList the paymentMethodStatusList to set
	 */
	public void setPaymentMethodStatusList(
			List<PaymentMethods.PaymentStatus> paymentMethodStatusList) {
		this.paymentMethodStatusList = paymentMethodStatusList;
	}

	/**
	 * @return the isDefaultPaymentMethod
	 */
	public PaymentMethods.PaymentIsDefault getIsDefaultPaymentMethod() {
		return isDefaultPaymentMethod;
	}

	/**
	 * @param isDefaultPaymentMethod the isDefaultPaymentMethod to set
	 */
	public void setIsDefaultPaymentMethod(
			PaymentMethods.PaymentIsDefault isDefaultPaymentMethod) {
		this.isDefaultPaymentMethod = isDefaultPaymentMethod;
	}

	/**
	 * @return the paymentMethodName
	 */
	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	/**
	 * @param paymentMethodName the paymentMethodName to set
	 */
	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	/**
	 * @return the paymentMethodNameList
	 */
	public List<String> getPaymentMethodNameList() {
		return paymentMethodNameList;
	}

	/**
	 * @param paymentMethodNameList the paymentMethodNameList to set
	 */
	public void setPaymentMethodNameList(List<String> paymentMethodNameList) {
		this.paymentMethodNameList = paymentMethodNameList;
	}

	public List<PaymentMethodDTO> getPaymentMethodDTOList() {
		return paymentMethodDTOList;
	}

	public void setPaymentMethodDTOList(List<PaymentMethodDTO> paymentMethodDTOList) {
		this.paymentMethodDTOList = paymentMethodDTOList;
	}

	/**
	 * @return the moduleID
	 */
	public Integer getModuleID() {
		return moduleID;
	}

	/**
	 * @param moduleID the moduleID to set
	 */
	public void setModuleID(Integer moduleID) {
		this.moduleID = moduleID;
	}

	/**
	 * @return the moduleName
	 */
	public PaymentMethods.ModuleName getModuleName() {
		return moduleName;
	}

	/**
	 * @param moduleName the moduleName to set
	 */
	public void setModuleName(PaymentMethods.ModuleName moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * @return the subscriptionID
	 */
	public String getSubscriptionID() {
		return subscriptionID;
	}

	/**
	 * @param subscriptionID the subscriptionID to set
	 */
	public void setSubscriptionID(String subscriptionID) {
		this.subscriptionID = subscriptionID;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the sortBy
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * @param sortBy the sortBy to set
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * @return the startRecord
	 */
	public int getStartRecord() {
		return startRecord;
	}

	/**
	 * @param startRecord the startRecord to set
	 */
	public void setStartRecord(int startRecord) {
		this.startRecord = startRecord;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the isPaginationRequired
	 */
	public boolean isPaginationRequired() {
		return isPaginationRequired;
	}

	/**
	 * @param isPaginationRequired the isPaginationRequired to set
	 */
	public void setPaginationRequired(boolean isPaginationRequired) {
		this.isPaginationRequired = isPaginationRequired;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	
}
