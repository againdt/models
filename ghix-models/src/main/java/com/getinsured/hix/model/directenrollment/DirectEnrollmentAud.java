package com.getinsured.hix.model.directenrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.AudId;

@Entity
@Table(name="D2C_ENROLLMENT_AUD")
@IdClass(AudId.class)
public class DirectEnrollmentAud implements Serializable {
	
	private static final long serialVersionUID = -6693327775926803602L;

	@Id
	@Column(name = "ID")
	private Integer id;

	@Id
	@Column(name = "REV")
	private int rev;
	
	@Column(name="ISSUER_STATUS")
	private String issuerStatus;
	
	@Column(name="GI_STATUS")
	private String giStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_TIMESTAMP") 
	private Date updationTimestamp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;

	public String getIssuerStatus() {
		return issuerStatus;
	}

	public void setIssuerStatus(String issuerStatus) {
		this.issuerStatus = issuerStatus;
	}

	public String getGiStatus() {
		return giStatus;
	}

	public void setGiStatus(String giStatus) {
		this.giStatus = giStatus;
	}

	public Date getUpdationTimestamp() {
		return updationTimestamp;
	}

	public void setUpdationTimestamp(Date updationTimestamp) {
		this.updationTimestamp = updationTimestamp;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	

}
