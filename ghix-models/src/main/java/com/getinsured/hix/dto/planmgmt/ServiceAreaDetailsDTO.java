package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class ServiceAreaDetailsDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String issuerPlanNumber;
	private String startDate;
	private String endDate;
	private String serfServiceAreaId;
	private List<ZipCounty> zipCounty;
	
	public List<ZipCounty> getZipCounty() {
		return zipCounty;
	}
	public void setZipCounty(List<ZipCounty> zipCounty) {
		this.zipCounty = zipCounty;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSerfServiceAreaId() {
		return serfServiceAreaId;
	}
	public void setSerfServiceAreaId(String serfServiceAreaId) {
		this.serfServiceAreaId = serfServiceAreaId;
	}
	
}
