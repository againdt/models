package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;

public class PdACAExpressRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	private String externalKey;
	private UserSettings userSettings;
	private App app;
	private List<Plan> plans;
	
	public String getExternalKey() {
		return externalKey;
	}

	public void setExternalKey(String externalKey) {
		this.externalKey = externalKey;
	}

	public UserSettings getUserSettings() {
		return userSettings;
	}

	public void setUserSettings(UserSettings userSettings) {
		this.userSettings = userSettings;
	}

	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}

	public List<Plan> getPlans() {
		return plans;
	}

	public void setPlans(List<Plan> plans) {
		this.plans = plans;
	}

	public class UserSettings{
		private List<Appointment> appointments;
		private String ffmUserName;
		private String orgName;
		
		public List<Appointment> getAppointments() {
			return appointments;
		}
		public void setAppointments(List<Appointment> appointments) {
			this.appointments = appointments;
		}
		public String getFfmUserName() {
			return ffmUserName;
		}
		public void setFfmUserName(String ffmUserName) {
			this.ffmUserName = ffmUserName;
		}
		public String getOrgName() {
			return orgName;
		}
		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}		
	}
	
	public class Appointment{
		private String aorFirstName;
		private String aorLastName;
		private String npn;
		private String state;
		private List<String> issuerIds;
		
		public String getAorFirstName() {
			return aorFirstName;
		}
		public void setAorFirstName(String aorFirstName) {
			this.aorFirstName = aorFirstName;
		}
		public String getAorLastName() {
			return aorLastName;
		}
		public void setAorLastName(String aorLastName) {
			this.aorLastName = aorLastName;
		}
		public String getNpn() {
			return npn;
		}
		public void setNpn(String npn) {
			this.npn = npn;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public List<String> getIssuerIds() {
			return issuerIds;
		}
		public void setIssuerIds(List<String> issuerIds) {
			this.issuerIds = issuerIds;
		}
	}
	
	public class App{		
		private String firstName;
		private String lastName;
		private String email;
		private String phone;
		private String stateCode;
		private String zip;
		private String fips;
		private List<People> people;
		private String effectiveDate;
		private String annualIncome;
		private String csr;
		private String origin;
		
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getStateCode() {
			return stateCode;
		}
		public void setStateCode(String stateCode) {
			this.stateCode = stateCode;
		}
		public String getZip() {
			return zip;
		}
		public void setZip(String zip) {
			this.zip = zip;
		}
		public String getFips() {
			return fips;
		}
		public void setFips(String fips) {
			this.fips = fips;
		}
		public List<People> getPeople() {
			return people;
		}
		public void setPeople(List<People> people) {
			this.people = people;
		}
		public String getEffectiveDate() {
			return effectiveDate;
		}
		public void setEffectiveDate(String effectiveDate) {
			this.effectiveDate = effectiveDate;
		}
		public String getAnnualIncome() {
			return annualIncome;
		}
		public void setAnnualIncome(String annualIncome) {
			this.annualIncome = annualIncome;
		}
		public String getCsr() {
			return csr;
		}
		public void setCsr(String csr) {
			this.csr = csr;
		}
		public String getOrigin() {
			return origin;
		}
		public void setOrigin(String origin) {
			this.origin = origin;
		}
		
	}
	
	public class People{
		private Integer relationship;
		private String firstName;
		private String lastName;
		private Integer gender;
		private String birthDate;
		private Boolean smoker;
		private Boolean needsCoverage;
		
		public Integer getRelationship() {
			return relationship;
		}
		public void setRelationship(Integer relationship) {
			this.relationship = relationship;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public Integer getGender() {
			return gender;
		}
		public void setGender(Integer gender) {
			this.gender = gender;
		}
		public String getBirthDate() {
			return birthDate;
		}
		public void setBirthDate(String birthDate) {
			this.birthDate = birthDate;
		}
		public Boolean getSmoker() {
			return smoker;
		}
		public void setSmoker(Boolean smoker) {
			this.smoker = smoker;
		}
		public Boolean getNeedsCoverage() {
			return needsCoverage;
		}
		public void setNeedsCoverage(Boolean needsCoverage) {
			this.needsCoverage = needsCoverage;
		}										
	}
	
	public class Plan{
		private Float amount;
		private String refId;
		private String issuerId;
		private String category;
		
		public Float getAmount() {
			return amount;
		}
		public void setAmount(Float amount) {
			this.amount = amount;
		}
		public String getRefId() {
			return refId;
		}
		public void setRefId(String refId) {
			this.refId = refId;
		}
		public String getIssuerId() {
			return issuerId;
		}
		public void setIssuerId(String issuerId) {
			this.issuerId = issuerId;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
	}
	
}
