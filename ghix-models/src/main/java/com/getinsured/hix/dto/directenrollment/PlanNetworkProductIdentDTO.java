package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;

/**
 * Used to store product and network identifier 
 * to be passed for each Humana plan under D2C flow
 * @author root
 *
 */
public class PlanNetworkProductIdentDTO implements Serializable{
	
	private String name;
	private String issuerPlanNumber;
	private String exchangeType;
	private String networkId;
	private String networkUrl;
	private String networkIdent;
	private String productIdent;
	private String paymentType;
	
	public PlanNetworkProductIdentDTO() {
		super();
	}


	public PlanNetworkProductIdentDTO(String name, String issuerPlanNumber,
			String exchangeType, String networkId, String networkUrl,
			String networkIdent, String productIdent) {
		super();
		this.name = name;
		this.issuerPlanNumber = issuerPlanNumber;
		this.exchangeType = exchangeType;
		this.networkId = networkId;
		this.networkUrl = networkUrl;
		this.networkIdent = networkIdent;
		this.productIdent = productIdent;
	}

	@Override
	public String toString() {
		return "PlanNetworkProductIdent [name=" + name + ", issuerPlanNumber="
				+ issuerPlanNumber + ", exchangeType=" + exchangeType
				+ ", networkId=" + networkId + ", networkUrl=" + networkUrl
				+ ", networkIdent=" + networkIdent + ", productIdent="
				+ productIdent + "]";
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkUrl() {
		return networkUrl;
	}
	public void setNetworkUrl(String networkUrl) {
		this.networkUrl = networkUrl;
	}
	public String getNetworkIdent() {
		return networkIdent;
	}
	public void setNetworkIdent(String networkIdent) {
		this.networkIdent = networkIdent;
	}
	public String getProductIdent() {
		return productIdent;
	}
	public void setProductIdent(String productIdent) {
		this.productIdent = productIdent;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	
	
	
}
