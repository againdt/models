UPDATE state_plan SET county='Montgomery' where zip_code ='36101';
UPDATE state_plan SET county='Juneau Borough' where zip_code ='99801';
UPDATE state_plan SET county='Maricopa' where zip_code ='85001';
UPDATE state_plan SET county='Pulaski' where zip_code ='72201';
UPDATE state_plan SET county='Sacramento' where zip_code ='94203';
UPDATE state_plan SET county='Denver' where zip_code ='80201';
UPDATE state_plan SET county='Hartford' where zip_code ='6101';
UPDATE state_plan SET county='Kent' where zip_code ='19901';
UPDATE state_plan SET county='Leon' where zip_code ='32301';
UPDATE state_plan SET county='Fulton' where zip_code ='32301';
UPDATE state_plan SET county='Sangamon' where zip_code ='62701';
UPDATE state_plan SET county='Marion' where zip_code ='46201';
UPDATE state_plan SET county='Polk' where zip_code ='50301';
UPDATE state_plan SET county='Shawnee' where zip_code ='66601';
UPDATE state_plan SET county='Franklin' where zip_code ='40601';
UPDATE state_plan SET county='East Baton Rouge Parish' where zip_code ='70801';
UPDATE state_plan SET county='Suffolk' where zip_code ='2108';
UPDATE state_plan SET county='Ramsey' where zip_code ='55101';
UPDATE state_plan SET county='Cole' where zip_code ='65101';
UPDATE state_plan SET county='Lewis and Clark' where zip_code ='59601';
UPDATE state_plan SET county='Lancaster' where zip_code ='68501';
UPDATE state_plan SET county='Carson City' where zip_code ='89701';
UPDATE state_plan SET county='Merrimack' where zip_code ='3301';
UPDATE state_plan SET county='Mercer' where zip_code ='8601';
UPDATE state_plan SET county='Santa Fe' where zip_code ='87501';
UPDATE state_plan SET county='Wake' where zip_code ='27601';
UPDATE state_plan SET county='Burleigh' where zip_code ='58501';
UPDATE state_plan SET county='Franklin' where zip_code ='43201';
UPDATE state_plan SET county='Oklahoma' where zip_code ='73101';
UPDATE state_plan SET county='Marion' where zip_code ='97301';
UPDATE state_plan SET county='Dauphin' where zip_code ='17101';
UPDATE state_plan SET county='Richland' where zip_code ='29201';
UPDATE state_plan SET county='Hughes' where zip_code ='57501';
UPDATE state_plan SET county='Davidson' where zip_code ='37201';
UPDATE state_plan SET county='Travis' where zip_code ='73301';
UPDATE state_plan SET county='Salt Lake' where zip_code ='84101';
UPDATE state_plan SET county='Thurston' where zip_code ='98501';
UPDATE state_plan SET county='Kanawha' where zip_code ='25301';
UPDATE state_plan SET county='Dane' where zip_code ='53701';
UPDATE state_plan SET county='Laramie' where zip_code ='82001';
UPDATE state_plan SET county='District of Columbia' where zip_code ='20009';
UPDATE state_plan SET county='Honolulu' where zip_code ='96801';
UPDATE state_plan SET county='Ada' where zip_code ='83701';
UPDATE state_plan SET county='Anne Arundel' where zip_code ='21401';
UPDATE state_plan SET county='Ingham' where zip_code ='48901';
UPDATE state_plan SET county='Richmond City' where zip_code ='23218';
UPDATE state_plan SET county='Hinds' where zip_code ='39201';
UPDATE STATE_PLAN SP1 SET SP1.COUNTY = (SELECT UPPER(SP2.COUNTY) FROM STATE_PLAN SP2 WHERE SP1.ID = SP2.ID);