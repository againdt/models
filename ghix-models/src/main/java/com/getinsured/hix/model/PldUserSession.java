
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the PLD_USER_SESSION database table.
 * 
 */
@Entity
@Table(name="pld_user_session")
public class PldUserSession implements Serializable{
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLD_USER_SESSION_SEQ")
	@SequenceGenerator(name = "PLD_USER_SESSION_SEQ", sequenceName = "PLD_USER_SESSION_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="SESSION_ID", nullable=false)
	private String sessionId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP", nullable=false)
	private Date creationTimestamp;
	
	@Column(name="IP_ADDRESS")
	private String ipAddress;
	
	@Column(name="PLAN_REQUEST_NUM", nullable=false)
	private Integer planRequestNum;
	
	@Column(name="DOCTOR_VISITS")
	private String doctorVisits;
	
	@Column(name="DRUG_USE")
	private String drugUse;
	
	@Column(name="ADULT_DENTAL")
	private Character adultDental;
	
	@Column(name="CHILD_DENTAL")
	private Character childDental;
	
	@Column(name="ACUPUNCTURE")
	private Character acupuncture;
	
	@Column(name="CHIROPRACTIC")
	private Character chiropractic;
	
	@Column(name="OPT")
	private Character opt;
	
	@Column(name="HEARING_AIDS")
	private Character hearingAids;
	
	@Column(name="NUTRITIONAL_COUNSELING")
	private Character nutritionalCounseling;
	
	@Column(name="WEIGHT_LOSS_PROGRAMS")
	private Character weightLossPrograms;
	
	@Column(name="PLD_ORDER_ID")
	private Integer pldOrderId;
	
	/**
	 * Non table data
	 */
	private String aptc;
	private String countyCode;
	private String zipCode;
	private float age;
	private String enrollmentType;
	private String houseHoldId;
	private String smoker;
	private String renewalType;
	private String shoppingType;
	private int userid;
	private String userRole;
	
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPlanRequestNum() {
		return planRequestNum;
	}

	public void setPlanRequestNum(Integer planRequestNum) {
		this.planRequestNum = planRequestNum;
	}

	public String getDoctorVisits() {
		return doctorVisits;
	}

	public void setDoctorVisits(String doctorVisits) {
		this.doctorVisits = doctorVisits;
	}

	public String getDrugUse() {
		return drugUse;
	}

	public void setDrugUse(String drugUse) {
		this.drugUse = drugUse;
	}

	public Character getAdultDental() {
		return adultDental;
	}

	public void setAdultDental(Character adultDental) {
		this.adultDental = adultDental;
	}

	public Character getChildDental() {
		return childDental;
	}

	public void setChildDental(Character childDental) {
		this.childDental = childDental;
	}

	public Character getAcupuncture() {
		return acupuncture;
	}

	public void setAcupuncture(Character acupuncture) {
		this.acupuncture = acupuncture;
	}

	public Character getChiropractic() {
		return chiropractic;
	}

	public void setChiropractic(Character chiropractic) {
		this.chiropractic = chiropractic;
	}

	public Character getOpt() {
		return opt;
	}

	public void setOpt(Character opt) {
		this.opt = opt;
	}

	public Character getHearingAids() {
		return hearingAids;
	}

	public void setHearingAids(Character hearingAids) {
		this.hearingAids = hearingAids;
	}

	public Character getNutritionalCounseling() {
		return nutritionalCounseling;
	}

	public void setNutritionalCounseling(Character nutritionalCounseling) {
		this.nutritionalCounseling = nutritionalCounseling;
	}

	public Character getWeightLossPrograms() {
		return weightLossPrograms;
	}

	public void setWeightLossPrograms(Character weightLossPrograms) {
		this.weightLossPrograms = weightLossPrograms;
	}

	public Integer getPldOrderId() {
		return pldOrderId;
	}

	public void setPldOrderId(Integer pldOrderId) {
		this.pldOrderId = pldOrderId;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public float getAge() {
		return age;
	}

	public void setAge(float age) {
		this.age = age;
	}

	public String getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public String getHouseHoldId() {
		return houseHoldId;
	}

	public void setHouseHoldId(String houseHoldId) {
		this.houseHoldId = houseHoldId;
	}

	public String getSmoker() {
		return smoker;
	}

	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}

	public String getRenewalType() {
		return renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}

	public String getShoppingType() {
		return shoppingType;
	}

	public void setShoppingType(String shoppingType) {
		this.shoppingType = shoppingType;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	
	

}
