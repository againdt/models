package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.Date;

/**
 * DTO class is used to get/set data using SERFF_PLAN_MGMT_BATCH table.
 */
public class SerffPlanMgmtBatchDTO {

	private long id;
	private int userId;
	private long serffReqId;
	private Date ftpStartTime;
	private Date ftpEndTime;
	private String batchStatus;
	private Date batchStartTime;
	private Date batchEndTime;
	private String isDeleted;
	private String ftpStatus;
	private String issuerId;
	private String career;
	private String state;
	private String exchangeType;
	private String defaultTenant;
	private String operationType;

	public SerffPlanMgmtBatchDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public long getSerffReqId() {
		return serffReqId;
	}

	public void setSerffReqId(long serffReqId) {
		this.serffReqId = serffReqId;
	}

	public Date getFtpStartTime() {
		return ftpStartTime;
	}

	public void setFtpStartTime(Date ftpStartTime) {
		this.ftpStartTime = ftpStartTime;
	}

	public Date getFtpEndTime() {
		return ftpEndTime;
	}

	public void setFtpEndTime(Date ftpEndTime) {
		this.ftpEndTime = ftpEndTime;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public Date getBatchStartTime() {
		return batchStartTime;
	}

	public void setBatchStartTime(Date batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public Date getBatchEndTime() {
		return batchEndTime;
	}

	public void setBatchEndTime(Date batchEndTime) {
		this.batchEndTime = batchEndTime;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getDefaultTenant() {
		return defaultTenant;
	}

	public void setDefaultTenant(String defaultTenant) {
		this.defaultTenant = defaultTenant;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
}
