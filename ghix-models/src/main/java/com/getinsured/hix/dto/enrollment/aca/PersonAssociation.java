package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class PersonAssociation{
  @JsonProperty("FamilyRelationshipCode")
  
  private FamilyRelationshipCode FamilyRelationshipCode;
  @JsonProperty("PersonReference")
  
  private List<PersonReference> PersonReference;
  public void setFamilyRelationshipCode(FamilyRelationshipCode FamilyRelationshipCode){
   this.FamilyRelationshipCode=FamilyRelationshipCode;
  }
  public FamilyRelationshipCode getFamilyRelationshipCode(){
   return FamilyRelationshipCode;
  }
  public void setPersonReference(List<PersonReference> PersonReference){
   this.PersonReference=PersonReference;
  }
  public List<PersonReference> getPersonReference(){
   return PersonReference;
  }
}