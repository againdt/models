package com.getinsured.hix.dto.estimator.mini;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * Member DTO of Retiree data.
 */
public class RetireeMemberDTO {

	/* External  unique  ID  of  the household  member, And is Unique with in household */
	private String externalMemberId;
	/* Date of Birth of member in MM/DD/YYYY format */
	private String dateOfBirth;
	/* Relationship to the primary applicant SELF / SPOUSE / CHILD */
	private String relationshipToPrimary;
	/* HRA amount attributed to the Individual member */
	private Double individualHraAmount;
	/* First name of the member */
	private String firstName;
	/* Last name of the member */
	private String lastName;
	/* Gender(M/F) of the member */
	private String gender;
	/* Indicates Applicant’s address line1 */
	private String address1;
	/* Indicates Member’s address line2 */
	private String address2;
	/* Indicates Member’s city */
	private String city;
	/* Indicates Member’s state (Two character state code) */
	private String state;
	/* Indicates the ZIP code for the household. Standard US zipcode (only numbers are allowed in the string and it has to be a 5-digit string) */
	private String zipCode;
	/* Indicates the county code for the household. It is a combination of stateFIPS and countyFIPS */
	private String countyCode;
	/* Member's primary contact phone number */
	private String primaryPhone;
	/* Member’s email Address */
	private String emailAddress;
	/* SSN of the member */
	private String ssn;
	/* Tobacco Usage (Y/N) */
	private String tobaccoUsage;
	/* Seeking Coverage (Y/N) */
	private String seekingCoverage;

	public RetireeMemberDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(String relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

	public Double getIndividualHraAmount() {
		return individualHraAmount;
	}

	public void setIndividualHraAmount(Double individualHraAmount) {
		this.individualHraAmount = individualHraAmount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getTobaccoUsage() {
		return tobaccoUsage;
	}

	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}

	public String getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(String seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
	}
}
