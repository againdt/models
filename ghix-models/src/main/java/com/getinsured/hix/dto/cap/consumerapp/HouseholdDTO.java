package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

import com.getinsured.hix.dto.cap.CmrMemberDto;

public class HouseholdDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7578706368442384650L;

	private int id;
	private String state;
	private String email;
	private String phoneNumber;
	private String mobilePhone;
	private String homePhone;
	private String status;
	private String leadStatus;
	private String zipCode;
	private String firstName;
	private String lastName;
	private String serviceBy;
	private String preferredContactTime;// enum? PREFERRED_CONTACT_TIME
	private LocationDto location;
	private String eligLeadId;
	private String aptc;
	private String exchangeType; // REMOVE AFTER CLIENT SIDE CHANGES TO READING FROM EligLeadDTO
	private String premium;
	private Integer usersId;
	private String idEnc;
	private String ssapAppId;
	private String errorMsg;
	private boolean newHousehold = false;
	private CmrMemberDto cmrMemberDto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getServiceBy() {
		return serviceBy;
	}

	public void setServiceBy(String serviceBy) {
		this.serviceBy = serviceBy;
	}

	public String getPreferredContactTime() {
		return preferredContactTime;
	}

	public void setPreferredContactTime(String preferredContactTime) {
		this.preferredContactTime = preferredContactTime;
	}

	public String getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(String eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}
	
	public Integer getUsersId() {
		return usersId;
	}

	public void setUsersId(Integer usersId) {
		this.usersId = usersId;
	}

	public String getIdEnc() {
		return idEnc;
	}

	public void setIdEnc(String idEnc) {
		this.idEnc = idEnc;
	}

	public String getSsapAppId() {
		return ssapAppId;
	}

	public void setSsapAppId(String ssapAppId) {
		this.ssapAppId = ssapAppId;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public boolean isNewHousehold() {
		return newHousehold;
	}

	public void setNewHousehold(boolean newHousehold) {
		this.newHousehold = newHousehold;
	}

	public CmrMemberDto getCmrMemberDto() {
		return cmrMemberDto;
	}

	public void setCmrMemberDto(CmrMemberDto cmrMemberDto) {
		this.cmrMemberDto = cmrMemberDto;
	}
	
	
}