package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author jyothi
 *
 */
public class ConsumerRequestDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long eligLeadId;
	private int planId;
	private String zipCode;
	private String stateCode;
	private String cmrHouseholdId;
	private Date   effectiveStartDate;
	private String applicationType;
	
	private BigDecimal premiumBeforeCredit;
	private BigDecimal premiumAfterCredit;
	
	private String exchangeType;
	
	public long getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(String cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public BigDecimal getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}
	public void setPremiumBeforeCredit(BigDecimal premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}
	public BigDecimal getPremiumAfterCredit() {
		return premiumAfterCredit;
	}
	public void setPremiumAfterCredit(BigDecimal premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}
	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	
	

	
}
