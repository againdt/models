
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SerffStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SerffStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Assigned"/>
 *     &lt;enumeration value="Submitted"/>
 *     &lt;enumeration value="PendingIndustryResponse"/>
 *     &lt;enumeration value="PendingStateAction"/>
 *     &lt;enumeration value="Closed"/>
 *     &lt;enumeration value="Reopened"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "")
@XmlEnum
@XmlRootElement(name="serffStatus")
public enum SerffStatus {

    @XmlEnumValue("Assigned")
    ASSIGNED("Assigned"),
    @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"),
    @XmlEnumValue("PendingIndustryResponse")
    PENDING_INDUSTRY_RESPONSE("PendingIndustryResponse"),
    @XmlEnumValue("PendingStateAction")
    PENDING_STATE_ACTION("PendingStateAction"),
    @XmlEnumValue("Closed")
    CLOSED("Closed"),
    @XmlEnumValue("Reopened")
    REOPENED("Reopened");
    private final String value;

    SerffStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SerffStatus fromValue(String v) {
        for (SerffStatus c: SerffStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
