package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

/**
 * @author kaul_s
 *
 */
public class AgentLeadPipeLineEditRequest implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8841613448448117802L;

	private String agentId;
	
	private String leadId;
	
	private String memberId;
	
	private Integer rank;
	
	private String reqEffDate;
	
	private String dispositionLabel;
	
	private Integer dispositionCode;
	
	private Integer loggedInUserId;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getReqEffDate() {
		return reqEffDate;
	}

	public void setReqEffDate(String reqEffDate) {
		this.reqEffDate = reqEffDate;
	}

	public String getDispositionLabel() {
		return dispositionLabel;
	}

	public void setDispositionLabel(String dispositionLabel) {
		this.dispositionLabel = dispositionLabel;
	}

	public Integer getDispositionCode() {
		return dispositionCode;
	}

	public void setDispositionCode(Integer dispositionCode) {
		this.dispositionCode = dispositionCode;
	}

	public Integer getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(Integer loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}
	

}
