package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;

@Entity
@Table(name="PM_ISSUER_BRAND_NAME")
public class IssuerBrandName implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public interface AddBrandName extends Default{
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IssuerBrandName_Seq")
	@SequenceGenerator(name = "IssuerBrandName_Seq", sequenceName = "PM_ISSUER_BRAND_NAME_SEQ", allocationSize = 1)
	private int id;
	
	@Pattern(regexp="^[a-zA-Z -]+$",message="err.validaddNewBrandName",groups={IssuerBrandName.AddBrandName.class})
	@Column(name="BRAND_NAME", length=400)
	private String brandName;
		
	@Column(name="IS_DELETED")
	private String isDeleted;
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date creationTimestamp;
	
	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date lastUpdateTimestamp;

	@Column(name="DELETED_BY")
	private Integer deletedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="DELETED_TIMESTAMP")
	private Date deletedTimestamp;
	
	@Column(name="BRAND_URL")
	private String brandUrl;
	
	@Pattern(regexp="^[A-Z]+$",message="err.validaddNewBrandId",groups={IssuerBrandName.AddBrandName.class})
	@Column(name="BRAND_ID", length=8)
	private String brandId;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Integer deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedTimestamp() {
		return deletedTimestamp;
	}

	public void setDeletedTimestamp(Date deletedTimestamp) {
		this.deletedTimestamp = deletedTimestamp;
	}

	public String getBrandUrl() {
		return brandUrl;
	}

	public void setBrandUrl(String brandUrl) {
		this.brandUrl = brandUrl;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate()); 
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setLastUpdateTimestamp(new TSDate()); 
	}
}
