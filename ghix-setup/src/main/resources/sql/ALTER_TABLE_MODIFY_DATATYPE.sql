CREATE OR REPLACE FUNCTION ALTER_TABLE_MODIFY_DATATYPE(
    v_old_data_Type USER_TAB_COLUMNS.data_type%TYPE,
    v_new_data_type USER_TAB_COLUMNS.data_type%TYPE ) 
RETURNS VOID AS
  $$
  DECLARE
    v_record RECORD;
    v_sql_stmt VARCHAR2(1000);
  BEGIN
    FOR v_record IN
      SELECT *
      FROM USER_TAB_COLUMNS
      WHERE TABLE_NAME NOT IN(SELECT DISTINCT(VIEW_NAME) FROM USER_VIEWS)
       AND data_type =v_old_data_Type
    LOOP
      v_sql_stmt := 'ALTER TABLE '||v_record.table_name||' ALTER COLUMN '||v_record.column_name ||' TYPE ' ||v_new_data_type;
      EXECUTE v_sql_stmt;
      END LOOP;
RETURN;
END;
$$ LANGUAGE plpgsql;