package com.getinsured.hix.dto.eapp.grammer;

import java.io.ByteArrayInputStream;

import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * This is a pdf representation in eapp.
 * Each pdf object has set of operation.
 * @author root
 *
 */
public class EappPDFDocument {
	private PDDocument document;
	
	public PDDocument getDocument() {
		return document;
	}

	public void setDocument(PDDocument document) {
		this.document = document;
	}

	private EappPDFDocument(){}
	
	public static EappPDFDocument getInstance(byte[] bytes){
		EappPDFDocument eappDocument = new EappPDFDocument();
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		try{
		eappDocument.document = PDDocument.load(bais);
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
		return eappDocument;
	}
	
	public static EappPDFDocument getInstanceFromDocument(PDDocument document){
		EappPDFDocument eappDocument = new EappPDFDocument();
		eappDocument.document = document;
		return eappDocument;
	}
	
	

}
