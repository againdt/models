package com.getinsured.hix.dto.directenrollment;

import com.getinsured.hix.dto.directenrollment.DirectEnrollmentStatus.RedirectStatus;

/**
 * Redirection response DTO
 * @author root
 *
 */
public class RedirectApplicationResponseDTO {
	private String url;
	private String body;
	private DirectEnrollmentStatus.RedirectStatus status;
	private String webserviceResponse;
	
	public RedirectApplicationResponseDTO() {
		super();
	}
	public RedirectApplicationResponseDTO(String url, String body,
			RedirectStatus status) {
		super();
		this.url = url;
		this.body = body;
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public DirectEnrollmentStatus.RedirectStatus getStatus() {
		return status;
	}
	public void setStatus(DirectEnrollmentStatus.RedirectStatus status) {
		this.status = status;
	}
	public String getWebserviceResponse() {
		return webserviceResponse;
	}
	public void setWebserviceResponse(String webserviceResponse) {
		this.webserviceResponse = webserviceResponse;
	}
	
}
