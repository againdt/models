//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.11.18 at 12:04:19 PM IST 
//


package us.gov.treasury.irs.annual.ext.aca.air._5;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:5.0" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;T Record Detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for TrasmitterRecordDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TrasmitterRecordDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}UTID"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TaxYr"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}TransmitterTCC"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}TotalPayeeCnt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}TotalPayerRecordCnt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}ActualReceiptDt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}AdjustedReceiptDt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}ShipmentRecordNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}PriorYrFilingInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:5.0}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrasmitterRecordDetailType", propOrder = {
    "utid",
    "taxYr",
    "transmitterTCC",
    "totalPayeeCnt",
    "totalPayerRecordCnt",
    "actualReceiptDt",
    "adjustedReceiptDt",
    "shipmentRecordNum",
    "priorYrFilingInd",
    "errorDetail"
})
public class TrasmitterRecordDetailType {

    @XmlElement(name = "UTID", required = true)
    protected String utid;
    @XmlElement(name = "TaxYr", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected XMLGregorianCalendar taxYr;
    @XmlElement(name = "TransmitterTCC", required = true)
    protected String transmitterTCC;
    @XmlElement(name = "TotalPayeeCnt", required = true)
    protected String totalPayeeCnt;
    @XmlElement(name = "TotalPayerRecordCnt", required = true)
    protected String totalPayerRecordCnt;
    @XmlElement(name = "ActualReceiptDt", required = true)
    protected XMLGregorianCalendar actualReceiptDt;
    @XmlElement(name = "AdjustedReceiptDt", required = true)
    protected XMLGregorianCalendar adjustedReceiptDt;
    @XmlElement(name = "ShipmentRecordNum", required = true)
    protected String shipmentRecordNum;
    @XmlElement(name = "PriorYrFilingInd", required = true)
    protected YOrNIndType priorYrFilingInd;
    @XmlElement(name = "ErrorDetail")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the utid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUTID() {
        return utid;
    }

    /**
     * Sets the value of the utid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUTID(String value) {
        this.utid = value;
    }

    /**
     * Gets the value of the taxYr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxYr() {
        return taxYr;
    }

    /**
     * Sets the value of the taxYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxYr(XMLGregorianCalendar value) {
        this.taxYr = value;
    }

    /**
     * Gets the value of the transmitterTCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmitterTCC() {
        return transmitterTCC;
    }

    /**
     * Sets the value of the transmitterTCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmitterTCC(String value) {
        this.transmitterTCC = value;
    }

    /**
     * Gets the value of the totalPayeeCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPayeeCnt() {
        return totalPayeeCnt;
    }

    /**
     * Sets the value of the totalPayeeCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPayeeCnt(String value) {
        this.totalPayeeCnt = value;
    }

    /**
     * Gets the value of the totalPayerRecordCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPayerRecordCnt() {
        return totalPayerRecordCnt;
    }

    /**
     * Sets the value of the totalPayerRecordCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPayerRecordCnt(String value) {
        this.totalPayerRecordCnt = value;
    }

    /**
     * Gets the value of the actualReceiptDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualReceiptDt() {
        return actualReceiptDt;
    }

    /**
     * Sets the value of the actualReceiptDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualReceiptDt(XMLGregorianCalendar value) {
        this.actualReceiptDt = value;
    }

    /**
     * Gets the value of the adjustedReceiptDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAdjustedReceiptDt() {
        return adjustedReceiptDt;
    }

    /**
     * Sets the value of the adjustedReceiptDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAdjustedReceiptDt(XMLGregorianCalendar value) {
        this.adjustedReceiptDt = value;
    }

    /**
     * Gets the value of the shipmentRecordNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentRecordNum() {
        return shipmentRecordNum;
    }

    /**
     * Sets the value of the shipmentRecordNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentRecordNum(String value) {
        this.shipmentRecordNum = value;
    }

    /**
     * Gets the value of the priorYrFilingInd property.
     * 
     * @return
     *     possible object is
     *     {@link YOrNIndType }
     *     
     */
    public YOrNIndType getPriorYrFilingInd() {
        return priorYrFilingInd;
    }

    /**
     * Sets the value of the priorYrFilingInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link YOrNIndType }
     *     
     */
    public void setPriorYrFilingInd(YOrNIndType value) {
        this.priorYrFilingInd = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
