package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.PopulationServedWrapper;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.PaymentMethods;

/**
 * Used to wrap data fetched from database in ghix-entity and send it to
 * ghix-web
 * 
 */
public class EnrollmentEntityResponseDTO extends GHIXResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private EnrollmentEntity enrollmentEntity;
	private List<Site> siteList;
	private PaymentMethods paymentMethods;
	private List<Map<String, Object>> enrollmentEntityHistory;
	private PopulationServedWrapper populationServedWrapper;
	// private List<LookupValue> lookupLanguages;
	// private List<LookupValue> lookupEthnicities;
	// private List<LookupValue> lookupIndustries;
	private SiteLanguages siteLanguages;
	private Site site;
	private List<SiteLocationHours> siteLocationHoursList;
	private int responseCode;
	private String responseDescription;
	private Map<String, Boolean> navigationMenuMap;
	private EntityDocuments entityDocuments;
	private byte[] attachment;
	private String document;
	@JsonSerialize(using = MapSerializer.class)
	private Map<String, Object> entityListAndRecordCount;
	private int totalEnrollmentEntities;
	private int enrollmentEntitiesUpForRenewal;
	private int inactiveEnrollmentEntities;
	private int totalEnrollmentEntitiesBySearch;
	private List<SiteLanguages> siteLangugaesList;
	private Map<Integer, SiteLanguages> siteLanguagesMap;
	private Map<Integer, List<SiteLocationHours>> siteLocationHoursMap;
	private List<EnrollmentEntity> entityList;
	private EnrollmentEntityAssisterSearchResult enrollEntityAssisterSearchResult;
	private List<EnrollmentEntityAssisterSearchResult> listOfEnrollEntityAssisterSearchResult;
	private int noOfAssistersForEnrollmentEntity;
	private int noOfIndividualsForEnrollmentEntity;
	private List<String> sortedListOfMapKeys;

	public List<String> getSortedListOfMapKeys() {
		return sortedListOfMapKeys;
	}

	public void setSortedListOfMapKeys(List<String> sortedListOfMapKeys) {
		this.sortedListOfMapKeys = sortedListOfMapKeys;
	}

	public int getNoOfAssistersForEnrollmentEntity() {
		return noOfAssistersForEnrollmentEntity;
	}

	public void setNoOfAssistersForEnrollmentEntity(int noOfAssistersForEnrollmentEntity) {
		this.noOfAssistersForEnrollmentEntity = noOfAssistersForEnrollmentEntity;
	}

	public List<EnrollmentEntityAssisterSearchResult> getListOfEnrollEntityAssisterSearchResult() {
		return listOfEnrollEntityAssisterSearchResult;
	}

	public void setListOfEnrollEntityAssisterSearchResult(
			List<EnrollmentEntityAssisterSearchResult> listOfEnrollEntityAssisterSearchResult) {
		this.listOfEnrollEntityAssisterSearchResult = listOfEnrollEntityAssisterSearchResult;
	}

	public EnrollmentEntityAssisterSearchResult getEnrollEntityAssisterSearchResult() {
		return enrollEntityAssisterSearchResult;
	}

	public void setEnrollEntityAssisterSearchResult(
			EnrollmentEntityAssisterSearchResult enrollEntityAssisterSearchResult) {
		this.enrollEntityAssisterSearchResult = enrollEntityAssisterSearchResult;
	}

	public List<EnrollmentEntity> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<EnrollmentEntity> entityList) {
		this.entityList = entityList;
	}

	public int getTotalEnrollmentEntitiesBySearch() {
		return totalEnrollmentEntitiesBySearch;
	}

	public void setTotalEnrollmentEntitiesBySearch(int totalEnrollmentEntitiesBySearch) {
		this.totalEnrollmentEntitiesBySearch = totalEnrollmentEntitiesBySearch;
	}

	public Map<Integer, SiteLanguages> getSiteLanguagesMap() {
		return siteLanguagesMap;
	}

	public void setSiteLanguagesMap(Map<Integer, SiteLanguages> siteLanguagesMap) {
		this.siteLanguagesMap = siteLanguagesMap;
	}

	public Map<Integer, List<SiteLocationHours>> getSiteLocationHoursMap() {
		return siteLocationHoursMap;
	}

	public void setSiteLocationHoursMap(Map<Integer, List<SiteLocationHours>> siteLocationHoursMap) {
		this.siteLocationHoursMap = siteLocationHoursMap;
	}

	public List<SiteLanguages> getSiteLangugaesList() {
		return siteLangugaesList;
	}

	public void setSiteLangugaesList(List<SiteLanguages> siteLangugaesList) {
		this.siteLangugaesList = siteLangugaesList;
	}

	public int getTotalEnrollmentEntities() {
		return totalEnrollmentEntities;
	}

	public void setTotalEnrollmentEntities(int totalEnrollmentEntities) {
		this.totalEnrollmentEntities = totalEnrollmentEntities;
	}

	public int getEnrollmentEntitiesUpForRenewal() {
		return enrollmentEntitiesUpForRenewal;
	}

	public void setEnrollmentEntitiesUpForRenewal(int enrollmentEntitiesUpForRenewal) {
		this.enrollmentEntitiesUpForRenewal = enrollmentEntitiesUpForRenewal;
	}

	public int getInactiveEnrollmentEntities() {
		return inactiveEnrollmentEntities;
	}

	public void setInactiveEnrollmentEntities(int inactiveEnrollmentEntities) {
		this.inactiveEnrollmentEntities = inactiveEnrollmentEntities;
	}

	public Map<String, Object> getEntityListAndRecordCount() {
		return entityListAndRecordCount;
	}

	public void setEntityListAndRecordCount(Map<String, Object> entityListAndRecordCount) {
		this.entityListAndRecordCount = entityListAndRecordCount;
	}

	public Map<String, Boolean> getNavigationMenuMap() {
		return navigationMenuMap;
	}

	public void setNavigationMenuMap(Map<String, Boolean> navigationMenuMap) {
		this.navigationMenuMap = navigationMenuMap;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public EnrollmentEntity getEnrollmentEntity() {
		return enrollmentEntity;
	}

	public void setEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		this.enrollmentEntity = enrollmentEntity;
	}

	public List<Site> getSiteList() {
		return siteList;
	}

	public void setSiteList(List<Site> siteList) {
		this.siteList = siteList;
	}

	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public List<Map<String, Object>> getEnrollmentEntityHistory() {
		return enrollmentEntityHistory;
	}

	public void setEnrollmentEntityHistory(List<Map<String, Object>> enrollmentEntityHistory) {
		this.enrollmentEntityHistory = enrollmentEntityHistory;
	}

	public PopulationServedWrapper getPopulationServedWrapper() {
		return populationServedWrapper;
	}

	public void setPopulationServedWrapper(PopulationServedWrapper populationServedWrapper) {
		this.populationServedWrapper = populationServedWrapper;
	}

	// public List<LookupValue> getLookupLanguages() {
	// return lookupLanguages;
	// }
	//
	// public void setLookupLanguages(List<LookupValue> lookupLanguages) {
	// this.lookupLanguages = lookupLanguages;
	// }
	//
	// public List<LookupValue> getLookupEthnicities() {
	// return lookupEthnicities;
	// }
	//
	// public void setLookupEthnicities(List<LookupValue> lookupEthnicities) {
	// this.lookupEthnicities = lookupEthnicities;
	// }
	//
	// public List<LookupValue> getLookupIndustries() {
	// return lookupIndustries;
	// }
	//
	// public void setLookupIndustries(List<LookupValue> lookupIndustries) {
	// this.lookupIndustries = lookupIndustries;
	// }

	public SiteLanguages getSiteLanguages() {
		return siteLanguages;
	}

	public void setSiteLanguages(SiteLanguages siteLanguages) {
		this.siteLanguages = siteLanguages;
	}

	public List<SiteLocationHours> getSiteLocationHoursList() {
		return siteLocationHoursList;
	}

	public void setSiteLocationHoursList(List<SiteLocationHours> siteLocationHoursList) {
		this.siteLocationHoursList = siteLocationHoursList;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public EntityDocuments getEntityDocuments() {
		return entityDocuments;
	}

	public void setEntityDocuments(EntityDocuments entityDocuments) {
		this.entityDocuments = entityDocuments;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	public void setAttachment(byte[] attachment1) {
		if (attachment1 == null) {
			this.attachment = new byte[0];
		} else {
			this.attachment = Arrays.copyOf(attachment1, attachment1.length);
		}
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public int getNoOfIndividualsForEnrollmentEntity() {
		return noOfIndividualsForEnrollmentEntity;
	}

	public void setNoOfIndividualsForEnrollmentEntity(int noOfIndividualsForEnrollmentEntity) {
		this.noOfIndividualsForEnrollmentEntity = noOfIndividualsForEnrollmentEntity;
	}

}
