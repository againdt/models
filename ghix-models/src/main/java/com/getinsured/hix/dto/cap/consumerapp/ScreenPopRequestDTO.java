package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class ScreenPopRequestDTO implements Serializable {	
	private static final long serialVersionUID = -7057560569227277600L;
	private String callType;//Enum? INBOUND/OUTBOUND_DIALLER/OUTBOUND_MANUAL
	private String leadId;
	private String phoneCalledTo;	
	private String phoneCalledFrom;
	private String callId;
	private String agentId;
	private String isTransfer;
	private String transferAgent;
	private String dialedNumber;
	private Integer householdId;

	public ScreenPopRequestDTO(){}
	
	public ScreenPopRequestDTO(String callType, String leadId,
			String phoneCalledTo, String phoneCalledFrom, String callId,
			String agentId, String isTransfer, String transferAgent, String dialedNumber,
			Integer householdId) {
		super();
		this.callType = callType;
		this.leadId = leadId;
		this.phoneCalledTo = phoneCalledTo;
		this.phoneCalledFrom = phoneCalledFrom;
		this.callId = callId;
		this.agentId = agentId;
		this.isTransfer = isTransfer;
		this.transferAgent = transferAgent;
		this.dialedNumber = dialedNumber;
		this.householdId = householdId;
	}
	
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getPhoneCalledTo() {
		return phoneCalledTo;
	}
	public void setPhoneCalledTo(String phoneCalledTo) {
		this.phoneCalledTo = phoneCalledTo;
	}
	public String getPhoneCalledFrom() {
		return phoneCalledFrom;
	}
	public void setPhoneCalledFrom(String phoneCalledFrom) {
		this.phoneCalledFrom = phoneCalledFrom;
	}
	public String getCallId() {
		return callId;
	}
	public void setCallId(String callId) {
		this.callId = callId;
	}
	
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getIsTransfer() {
		return isTransfer;
	}
	public void setIsTransfer(String isTransfer) {
		this.isTransfer = isTransfer;
	}
	public String getTransferAgent() {
		return transferAgent;
	}
	public void setTransferAgent(String transferAgent) {
		this.transferAgent = transferAgent;
	}

	public String getDialedNumber() {
		return dialedNumber;
	}

	public void setDialedNumber(String dialedNumber) {
		this.dialedNumber = dialedNumber;
	}

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}
	
	
}
