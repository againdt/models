package com.getinsured.hix.dto.plandisplay;

import java.util.ArrayList;
import java.util.List;

public class DrugPropertiesResponseDTO {
	private List<DrugSearchDTO> drugList;

	public List<DrugSearchDTO> getDrugList() {
		return drugList;
	}

	public void setDrugList(List<DrugSearchDTO> drugList) {
		this.drugList = drugList;
	}

	public void addDrugToList(DrugSearchDTO drug) {
		if(drugList == null) {
			drugList = new ArrayList<DrugSearchDTO>();
		}
		drugList.add(drug);
	}

}
