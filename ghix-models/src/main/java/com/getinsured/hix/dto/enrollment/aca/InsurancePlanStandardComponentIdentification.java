package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsurancePlanStandardComponentIdentification{
  @JsonProperty("IdentificationID")
  
  private IdentificationID IdentificationID;
  public void setIdentificationID(IdentificationID IdentificationID){
   this.IdentificationID=IdentificationID;
  }
  public IdentificationID getIdentificationID(){
   return IdentificationID;
  }
}