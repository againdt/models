package com.getinsured.hix.model.phix.provider;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="provider_group_affil")
public class ProviderGroupAffil  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private ProviderProduct providerProduct;
	private String groupAffil;

	@Id 
	@SequenceGenerator(name="PROVIDER_GROUP_AFFIL_SEQ", sequenceName="PROVIDER_GROUP_AFFIL_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="PROVIDER_GROUP_AFFIL_SEQ")    
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROVIDER_PRODUCT_ID")
	public ProviderProduct getProviderProduct() {
		return this.providerProduct;
	}

	public void setProviderProduct(ProviderProduct providerProduct) {
		this.providerProduct = providerProduct;
	}


	@Column(name="GROUP_AFFIL")
	public String getGroupAffil() {
		return this.groupAffil;
	}

	public void setGroupAffil(String groupAffil) {
		this.groupAffil = groupAffil;
	}


}


