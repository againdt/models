package com.getinsured.hix.dto.cap;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Created by kaul_s on 2/26/15.
 */
public abstract class JsonUtil {

    public static int getInt(JsonNode node, String fieldName) {
        JsonNode field = node.get(fieldName);
        if (field == null) {
            throw new KendoServiceException("field not found: " + fieldName, HttpStatus.BAD_REQUEST);
        }
        if (!field.isNumber()) {
            throw new KendoServiceException("field is not a number: " + fieldName, HttpStatus.BAD_REQUEST);
        }
        return field.asInt();
    }

    public static String getText(JsonNode node, String fieldName) {
        JsonNode field = node.get(fieldName);
        if (field == null) {
            throw new KendoServiceException("field not found: " + fieldName, HttpStatus.BAD_REQUEST);
        }
        if (!field.isTextual()) {
            throw new KendoServiceException("field is not a text: " + fieldName, HttpStatus.BAD_REQUEST);
        }
        return field.asText();
    }

    public static JsonNode readTree(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(json);
        } catch (IOException e) {
            throw new KendoServiceException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public static String writeValueAsString(Object value, Module... modules) {
        ObjectMapper mapper = new ObjectMapper();
        for (Module module: modules) {
            mapper.registerModule(module);
        }
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new KendoServiceException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public static <T> T getPOJO(Class<T> clazz, JsonNode node, String fieldName) {
        JsonNode fieldNode = node.get(fieldName);
        if (fieldNode == null) {
            throw new KendoServiceException("field not found: " + fieldName, HttpStatus.BAD_REQUEST);
        }
        String field = null;
        if (fieldNode.isTextual()) {
            field = fieldNode.asText();
        } else if (fieldNode.isObject()) {
            field = fieldNode.toString();
        }
        if (field == null) {
            throw new KendoServiceException("field is not an object: " + fieldName, HttpStatus.BAD_REQUEST);
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(field, clazz);
        } catch (IOException e) {
            throw new KendoServiceException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public static <T> ObjectNode buildKendoGridResult(String typeName, List<T> entities, long totalCount) {
         /*
            JSON data received to JavaScript from server (JAVA) on every request
            {
                "Products": [     //array of product objects
                    {
                        "Itemname": "product 1",
                        "Itemprice": 23
                    },
                    {
                        "Itemname": "product 2",
                        "Itemprice": 90.99
                    }
                ],
                "TotalCount": 100   //total number of records
            }
            */

        ObjectMapper mapper = new ObjectMapper();

        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.putPOJO(typeName, entities);
        rootNode.put("TotalCount", totalCount);

        return rootNode;
    }
}
