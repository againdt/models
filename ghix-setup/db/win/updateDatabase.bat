set /p profName="Enter Profile in lowercase [ nm | ms | id | phix | ca | medicare ] : "

if  "%profName%"=="ca" (
call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog_configuration_%profName%.xml clearCheckSums

call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog_ca2.0_upgrade.xml clearCheckSums

call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog_configuration_%profName%.xml update

call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog_ca2.0_upgrade.xml update
) else (
call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog_configuration_%profName%.xml clearCheckSums

call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog.xml clearCheckSums

call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog_configuration_%profName%.xml update

call java -Xmx1024m -jar ..\lib\ghix-liquibase.jar  --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war  --pbePasswordVariable=PASSWORD_KEY --changeLogFile=META-INF/master.changelog.xml update
)
pause