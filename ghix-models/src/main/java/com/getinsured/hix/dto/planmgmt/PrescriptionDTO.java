/**
 * 
 * @author santanu
 * @version 1.0
 * @since June 7, 2017 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.util.List;
import java.util.Map;

public class PrescriptionDTO {

	private String rxCode;
	
	private List<Map<String, String>> supportedPlanList;

	/**
	 * @return the rxCode
	 */
	public String getRxCode() {
		return rxCode;
	}

	/**
	 * @param rxCode the rxCode to set
	 */
	public void setRxCode(String rxCode) {
		this.rxCode = rxCode;
	}

	/**
	 * @return the supportedPlanList
	 */
	public List<Map<String, String>> getSupportedPlanList() {
		return supportedPlanList;
	}

	/**
	 * @param supportedPlanList the supportedPlanList to set
	 */
	public void setSupportedPlanList(List<Map<String, String>> supportedPlanList) {
		this.supportedPlanList = supportedPlanList;
	}
	
}
