package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerPaymentResponseDTO extends GhixResponseDTO {

	private String issuerName;
	private String companyLogo;
	private IssuerPaymentInfoDTO issuerPaymentInfoDTO;

	public IssuerPaymentResponseDTO() {
		super();
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}

	public IssuerPaymentInfoDTO getIssuerPaymentInfoDTO() {
		return issuerPaymentInfoDTO;
	}

	public void setIssuerPaymentInfoDTO(IssuerPaymentInfoDTO issuerPaymentInfoDTO) {
		this.issuerPaymentInfoDTO = issuerPaymentInfoDTO;
	}
}
