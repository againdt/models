/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

/**
 * @author Krishna Gajulapalli
 *
 */
public class PlanListRequest implements Serializable{

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Integer> planIds;

	/**
	 * @return the planIds
	 */
	public List<Integer> getPlanIds() {
		return planIds;
	}

	/**
	 * @param planIds the planIds to set
	 */
	public void setPlanIds(List<Integer> planIds) {
		this.planIds = planIds;
	}
	
	

}
