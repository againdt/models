/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.util.Date;

import com.getinsured.hix.model.IssuerBrandName;

/**
 * @author Krishna Gajulapalli
 *
 */
public class ManageIssuerDTO {
	private int id;
	private String hiosIssuerId;
	private String name;
	private String stateOfDomicile;
	private String shortTenantList;
	private String longTenantList;
	private IssuerBrandName issuerBrandName;
	private Date lastUpdateTimestamp;
	private String certificationStatus;
	private String d2cFlowType;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	/**
	 * @param hiosIssuerId the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the stateOfDomicile
	 */
	public String getStateOfDomicile() {
		return stateOfDomicile;
	}
	/**
	 * @param stateOfDomicile the stateOfDomicile to set
	 */
	public void setStateOfDomicile(String stateOfDomicile) {
		this.stateOfDomicile = stateOfDomicile;
	}
	/**
	 * @return the shortTenantList
	 */
	public String getShortTenantList() {
		return shortTenantList;
	}
	/**
	 * @param shortTenantList the shortTenantList to set
	 */
	public void setShortTenantList(String shortTenantList) {
		this.shortTenantList = shortTenantList;
	}
	/**
	 * @return the longTenantList
	 */
	public String getLongTenantList() {
		return longTenantList;
	}
	/**
	 * @param longTenantList the longTenantList to set
	 */
	public void setLongTenantList(String longTenantList) {
		this.longTenantList = longTenantList;
	}
	/**
	 * @return the issuerBrandName
	 */
	public IssuerBrandName getIssuerBrandName() {
		return issuerBrandName;
	}
	/**
	 * @param issuerBrandName the issuerBrandName to set
	 */
	public void setIssuerBrandName(IssuerBrandName issuerBrandName) {
		this.issuerBrandName = issuerBrandName;
	}
	/**
	 * @return the lastUpdateTimestamp
	 */
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	/**
	 * @param lastUpdateTimestamp the lastUpdateTimestamp to set
	 */
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	/**
	 * @return the certificationStatus
	 */
	public String getCertificationStatus() {
		return certificationStatus;
	}
	/**
	 * @param certificationStatus the certificationStatus to set
	 */
	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	/**
	 * @return the d2cFlowType
	 */
	public String getD2cFlowType() {
		return d2cFlowType;
	}
	/**
	 * @param d2cFlowType the d2cFlowType to set
	 */
	public void setD2cFlowType(String d2cFlowType) {
		this.d2cFlowType = d2cFlowType;
	}
	
	
}
