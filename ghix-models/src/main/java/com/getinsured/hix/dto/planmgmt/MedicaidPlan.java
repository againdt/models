/**
 * 
 * @author santanu
 * @version 1.0
 * @since Jan 14, 2015 
 *
 * Define Medicaid plan data in response 
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.Map;

public class MedicaidPlan {
	
	private String id;
	private String name;
	private String issuerId;
	private String issuerName;
	private String issuerLogo;
	private String memberNumber;
	private String websiteURL;
	private String qualityRating;
	private Map<String, Map<String, String>> benefits;
	private Map<String, Map<String, String>> services;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the issuerId
	 */
	public String getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the issuerLogo
	 */
	public String getIssuerLogo() {
		return issuerLogo;
	}
	/**
	 * @param issuerLogo the issuerLogo to set
	 */
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	/**
	 * @return the memberNumber
	 */
	public String getMemberNumber() {
		return memberNumber;
	}
	/**
	 * @param memberNumber the memberNumber to set
	 */
	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}
	/**
	 * @return the websiteURL
	 */
	public String getWebsiteURL() {
		return websiteURL;
	}
	/**
	 * @param websiteURL the websiteURL to set
	 */
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}
	/**
	 * @return the qualityRating
	 */
	public String getQualityRating() {
		return qualityRating;
	}
	/**
	 * @param qualityRating the qualityRating to set
	 */
	public void setQualityRating(String qualityRating) {
		this.qualityRating = qualityRating;
	}	
	/**
	 * @return the benefits
	 */
	public Map<String, Map<String, String>> getBenefits() {
		return benefits;
	}
	
	/**
	 * @param benefits the benefits to set
	 */
	public void setBenefits(Map<String, Map<String, String>> benefits) {
		this.benefits = benefits;
	}
	/**
	 * @return the services
	 */
	public Map<String, Map<String, String>> getServices() {
		return services;
	}
	/**
	 * @param services the services to set
	 */
	public void setServices(Map<String, Map<String, String>> services) {
		this.services = services;
	}

}
