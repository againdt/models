package com.getinsured.hix.model;

public class BrokerAvailabilityRequest {
    private boolean available;

    public boolean getAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
