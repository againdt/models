package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class Address implements Serializable{
	private static final long serialVersionUID = 1L;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String county;
	private String zip;
	private String extendedZip;
	
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getExtendedZip() {
		return extendedZip;
	}
	public void setExtendedZip(String extendedZip) {
		this.extendedZip = extendedZip;
	}
	
	public Address copy(){
		Address address = new Address();
		address.city = this.city;
		address.county = this.county;
		address.zip = this.zip;
		address.extendedZip = this.extendedZip;
		address.state = this.state;
		address.address1 = this.address1;
		address.address2 = this.address2;
		return address;
	}

	public static Address getSampleInstance(){
		Address add = new Address();
		add.setCity("Miami");
		add.setCounty("Miami-Dade");
		add.setAddress1("123 main st");
		add.setAddress2("apt");
		add.setState("FL");
		add.setZip("33101");
		return add;
		
	}
	
	
}
