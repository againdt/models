package com.getinsured.coveredCA.dto.ssap;

public class Wrapper {

	private ExchangeStreamlinedApplication singleStreamlinedApplication;

	public ExchangeStreamlinedApplication getSingleStreamlinedApplication() {
		return singleStreamlinedApplication;
	}

	public void setSingleStreamlinedApplication(
			ExchangeStreamlinedApplication singleStreamlinedApplication) {
		this.singleStreamlinedApplication = singleStreamlinedApplication;
	}
}
