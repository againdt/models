package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.List;


/**
 * 
 * @author Krishna_prasad
 *
 */

public class SessionData implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	private boolean childFlag;
	private boolean isNMDental;
	private boolean showCatastrophicPlan;
	private Integer householdId;
	private String sadpFlag;
	private String exchangeType;
	private String ancillaryExchangeType;
	private String renewalType;
	private String planType;
	private String planTier;
	private String shoppingType;
	private String pgrmType;
	private String enrollmentType;
	private String anonymousIndividualType;
	private String isSubsidized;
	private String tenant;
	private String quotedMembers;	
	private String coverageStartDate;
	private String enrollmentCoverageStartDate;
	private String employerId;
	private String worksiteZip;
	private String worksiteCounty;
	private String healthAvailableFlag;
	private String dentalAvailableFlag;
	private String dentalQuotingGroup;
	private String keepOnly;
	private String initialPlanId;
	private GroupData groupData;
	private EmployerData employerData;
	private Float oldHealthPremium;
	private Float oldHealthAptc;
	private Float oldDentalPremium;
	private Float oldDentalAptc;
	private EmployerDTO employerDTO; 
	private List<GroupData> groupDataList;
	private String healthQuotingGroup;
	private String dentalPlanId;
	private String maintenanceReasonCode;
	private List<Integer> planIdList;
	private String gpsVersion;
	private String insuranceType;
	private boolean wasHealthPlanAvailable=false;
		
	public static SessionData clone(SessionData sessionData)
	{
		SessionData sessionDataCopy = new SessionData();
		sessionDataCopy.setHouseholdId(sessionData.getHouseholdId());
		sessionDataCopy.setSadpFlag(sessionData.getSadpFlag());
		sessionDataCopy.setNMDental(sessionData.isNMDental());
		sessionDataCopy.setChildFlag(sessionData.getChildFlag());
		sessionDataCopy.setShowCatastrophicPlan(sessionData.getShowCatastrophicPlan());
		sessionDataCopy.setExchangeType(sessionData.getExchangeType());
		sessionDataCopy.setRenewalType(sessionData.getRenewalType());
		sessionDataCopy.setPlanType(sessionData.getPlanType());
		sessionDataCopy.setPlanTier(sessionData.getPlanTier());
		sessionDataCopy.setShoppingType(sessionData.getShoppingType());
		sessionDataCopy.setPgrmType(sessionData.getPgrmType());
		sessionDataCopy.setEnrollmentType(sessionData.getEnrollmentType());
		sessionDataCopy.setAnonymousIndividualType(sessionData.getAnonymousIndividualType());
		sessionDataCopy.setIsSubsidized(sessionData.getIsSubsidized());
		sessionDataCopy.setTenant(sessionData.getTenant());
		sessionDataCopy.setQuotedMembers(sessionData.getQuotedMembers());
		sessionDataCopy.setCoverageStartDate(sessionData.getCoverageStartDate());
		sessionDataCopy.setEnrollmentCoverageStartDate(sessionData.getEnrollmentCoverageStartDate());
		sessionDataCopy.setEmployerId(sessionData.getEmployerId());
		sessionDataCopy.setWorksiteZip(sessionData.getWorksiteZip());
		sessionDataCopy.setWorksiteCounty(sessionData.getWorksiteCounty());
		sessionDataCopy.setHealthAvailableFlag(sessionData.getHealthAvailableFlag());
		sessionDataCopy.setDentalAvailableFlag(sessionData.getDentalAvailableFlag());
		sessionDataCopy.setDentalQuotingGroup(sessionData.getDentalQuotingGroup());
		sessionDataCopy.setKeepOnly(sessionData.getKeepOnly());
		sessionDataCopy.setOldHealthPremium(sessionData.getOldHealthPremium());
		sessionDataCopy.setOldHealthAptc(sessionData.getOldHealthAptc());
		sessionDataCopy.setOldDentalPremium(sessionData.getOldDentalPremium());
		sessionDataCopy.setOldDentalAptc(sessionData.getOldDentalAptc());
		sessionDataCopy.setGroupData(GroupData.clone(sessionData.getGroupData()));
		sessionDataCopy.setEmployerData(EmployerData.clone(sessionData.getEmployerData()));
		sessionDataCopy.setEmployerDTO(EmployerDTO.clone(sessionData.getEmployerDTO()));
		sessionDataCopy.setHealthQuotingGroup(sessionData.getHealthQuotingGroup());
		sessionDataCopy.setMaintenanceReasonCode(sessionData.getMaintenanceReasonCode());
		sessionDataCopy.setPlanIdList(sessionData.getPlanIdList());
		sessionDataCopy.setInsuranceType(sessionData.getInsuranceType());
		return sessionDataCopy;
	}
	
	public EmployerDTO getEmployerDTO() {
		return employerDTO;
	}

	public void setEmployerDTO(EmployerDTO employerDTO) {
		this.employerDTO = employerDTO;
	}

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

	public String getSadpFlag() {
		return sadpFlag;
	}

	public void setSadpFlag(String sadpFlag) {
		this.sadpFlag = sadpFlag;
	}	

	public boolean getChildFlag() {
		return childFlag;
	}

	public void setChildFlag(boolean childFlag) {
		this.childFlag = childFlag;
	}	

	public boolean isNMDental() {
		return isNMDental;
	}

	public void setNMDental(boolean isNMDental) {
		this.isNMDental = isNMDental;
	}

	public boolean getShowCatastrophicPlan() {
		return showCatastrophicPlan;
	}

	public void setShowCatastrophicPlan(boolean showCatastrophicPlan) {
		this.showCatastrophicPlan = showCatastrophicPlan;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getRenewalType() {
		return renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanTier() {
		return planTier;
	}

	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}

	public String getShoppingType() {
		return shoppingType;
	}

	public void setShoppingType(String shoppingType) {
		this.shoppingType = shoppingType;
	}

	public String getPgrmType() {
		return pgrmType;
	}

	public void setPgrmType(String pgrmType) {
		this.pgrmType = pgrmType;
	}

	public String getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public String getAnonymousIndividualType() {
		return anonymousIndividualType;
	}

	public void setAnonymousIndividualType(String anonymousIndividualType) {
		this.anonymousIndividualType = anonymousIndividualType;
	}	

	public String getIsSubsidized() {
		return isSubsidized;
	}

	public void setIsSubsidized(String isSubsidized) {
		this.isSubsidized = isSubsidized;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	public String getQuotedMembers() {
		return quotedMembers;
	}

	public void setQuotedMembers(String quotedMembers) {
		this.quotedMembers = quotedMembers;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getEnrollmentCoverageStartDate() {
		return enrollmentCoverageStartDate;
	}

	public void setEnrollmentCoverageStartDate(String enrollmentCoverageStartDate) {
		this.enrollmentCoverageStartDate = enrollmentCoverageStartDate;
	}

	public String getEmployerId() {
		return employerId;
	}

	public void setEmployerId(String employerId) {
		this.employerId = employerId;
	}

	public String getWorksiteZip() {
		return worksiteZip;
	}

	public void setWorksiteZip(String worksiteZip) {
		this.worksiteZip = worksiteZip;
	}

	public String getWorksiteCounty() {
		return worksiteCounty;
	}

	public void setWorksiteCounty(String worksiteCounty) {
		this.worksiteCounty = worksiteCounty;
	}
	
	public String getHealthAvailableFlag() {
		return healthAvailableFlag;
	}

	public void setHealthAvailableFlag(String healthAvailableFlag) {
		this.healthAvailableFlag = healthAvailableFlag;
	}
	 	 	
	public String getDentalAvailableFlag() {
		return dentalAvailableFlag;
	}

	public void setDentalAvailableFlag(String dentalAvailableFlag) {
		this.dentalAvailableFlag = dentalAvailableFlag;
	}
	
	public String getDentalQuotingGroup() {
		return dentalQuotingGroup;
	}

	public void setDentalQuotingGroup(String dentalQuotingGroup) {
		this.dentalQuotingGroup = dentalQuotingGroup;
	}

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}

	public String getInitialPlanId() {
		return initialPlanId;
	}

	public void setInitialPlanId(String initialPlanId) {
		this.initialPlanId = initialPlanId;
	}

	public GroupData getGroupData() {
		return groupData;
	}

	public void setGroupData(GroupData groupData) {
		this.groupData = groupData;
	}
	
	public EmployerData getEmployerData() {
		return employerData;
	}

	public void setEmployerData(EmployerData employerData) {
		this.employerData = employerData;
	}	
	
	public Float getOldHealthPremium() {
		return oldHealthPremium;
	}

	public void setOldHealthPremium(Float oldHealthPremium) {
		this.oldHealthPremium = oldHealthPremium;
	}

	public Float getOldHealthAptc() {
		return oldHealthAptc;
	}

	public void setOldHealthAptc(Float oldHealthAptc) {
		this.oldHealthAptc = oldHealthAptc;
	}

	public Float getOldDentalPremium() {
		return oldDentalPremium;
	}

	public void setOldDentalPremium(Float oldDentalPremium) {
		this.oldDentalPremium = oldDentalPremium;
	}

	public Float getOldDentalAptc() {
		return oldDentalAptc;
	}

	public void setOldDentalAptc(Float oldDentalAptc) {
		this.oldDentalAptc = oldDentalAptc;
	}
	
	public List<GroupData> getGroupDataList() {
		return groupDataList;
	}

	public void setGroupDataList(List<GroupData> groupDataList) {
		this.groupDataList = groupDataList;
	}
	
	public String getHealthQuotingGroup() {
		return healthQuotingGroup;
	}

	public void setHealthQuotingGroup(String healthQuotingGroup) {
		this.healthQuotingGroup = healthQuotingGroup;
	}

	public String getDentalPlanId() {
		return dentalPlanId;
	}

	public void setDentalPlanId(String dentalPlanId) {
		this.dentalPlanId = dentalPlanId;
	}

	public String getMaintenanceReasonCode() {
		return maintenanceReasonCode;
	}

	public void setMaintenanceReasonCode(String maintenanceReasonCode) {
		this.maintenanceReasonCode = maintenanceReasonCode;
	}
	
	public List<Integer> getPlanIdList() {
		return planIdList;
	}

	public void setPlanIdList(List<Integer> planIdList) {
		this.planIdList = planIdList;
	}

	public String getGpsVersion() {
		return gpsVersion;
	}

	public void setGpsVersion(String gpsVersion) {
		this.gpsVersion = gpsVersion;
	}

	public String getAncillaryExchangeType() {
		return ancillaryExchangeType;
	}

	public void setAncillaryExchangeType(String ancillaryExchangeType) {
		this.ancillaryExchangeType = ancillaryExchangeType;
	}
	
	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public boolean isWasHealthPlanAvailable() {
		return wasHealthPlanAvailable;
	}

	public void setWasHealthPlanAvailable(boolean wasHealthPlanAvailable) {
		this.wasHealthPlanAvailable = wasHealthPlanAvailable;
	}

	/**
	 * 
	 * @author Enums
	 *
	 */
	public enum ProgramType {
		SHOP, INDIVIDUAL;
	}	
	public enum PlanType {
		Both, Combined, Health, Separate;
	}
	public enum ShoppingType {
		PRESHOPPING, FFM, POSTSHOPPING, STM, DENTAL, AME, VISION, LIFE;
	}
	public enum EnrollmentType {
		I, S;
	}
	public enum ExchangeType {
		ON, OFF;
	}
	public enum AncillaryExchangeType {
		ON, OFF;
	}
	public enum RenewalType {
		NEW, KEEP;
	}
	public enum Tenant {
		GINS, BBAY;
	}
	public enum MissingDental {
		Adult, Child, AdultChild, None;
	}
	public enum AnonymousIndividualType {
		INDIVIDUAL, ANONYMOUS;
	}
	public enum Relationship {
		Self, Spouse, Child;
	}	
	public enum YESorNO {
		YES, NO;
	}
	public enum YorN {
		Y, N;
	}
	public enum GPSVersion
	{
		V1,BASELINE;
		
	}
}
