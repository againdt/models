package com.getinsured.hix.model.eligibility;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "STATE_POVERTY_GUIDELINES")
@DynamicInsert
@DynamicUpdate
public class CmsPovertyGuidelines implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STATE_POVERTY_GUIDELINES_SEQ")
	@SequenceGenerator(name = "STATE_POVERTY_GUIDELINES_SEQ", sequenceName = "STATE_POVERTY_GUIDELINES_SEQ", allocationSize = 1)
	private Long id;

	@Column(name = "POVERTY_FAMILYSIZE_1")
	private Double povertyFamilySize1;
	@Column(name = "POVERTY_FAMILYSIZE_2")
	private Double povertyFamilySize2;
	@Column(name = "POVERTY_FAMILYSIZE_3")
	private Double povertyFamilySize3;
	@Column(name = "POVERTY_FAMILYSIZE_4")
	private Double povertyFamilySize4;
	@Column(name = "POVERTY_FAMILYSIZE_5")
	private Double povertyFamilySize5;
	@Column(name = "POVERTY_FAMILYSIZE_6")
	private Double povertyFamilySize6;
	@Column(name = "POVERTY_FAMILYSIZE_7")
	private Double povertyFamilySize7;
	@Column(name = "POVERTY_FAMILYSIZE_8")
	private Double povertyFamilySize8;
	@Column(name = "ADDITIONAL_PERSON")
	private Double additionalPerson;
	@Column(name = "AP_PER_FPL_0_132_9_MINI")
	private Double applicablePercentMin132;
	@Column(name = "AP_PER_FPL_0_132_9_MAX")
	private Double applicablePercentMax132;
	@Column(name = "AP_PER_FPL_133_149_MINI")
	private Double applicablePercentMin133;
	@Column(name = "AP_PER_FPL_133_149_MAX")
	private Double applicablePercentMax133;
	@Column(name = "AP_PER_FPL_150_199_MINI")
	private Double applicablePercentMin150;
	@Column(name = "AP_PER_FPL_150_199_MAX")
	private Double applicablePercentMax150;
	@Column(name = "AP_PER_FPL_200_249_MINI")
	private Double applicablePercentMin200;
	@Column(name = "AP_PER_FPL_200_249_MAX")
	private Double applicablePercentMax200;
	@Column(name = "AP_PER_FPL_250_299_MINI")
	private Double applicablePercentMin250;
	@Column(name = "AP_PER_FPL_250_299_MAX")
	private Double applicablePercentMax250;
	@Column(name = "AP_PER_FPL_300_400_MINI")
	private Double applicablePercentMin300;
	@Column(name = "AP_PER_FPL_300_400_MAX")
	private Double applicablePercentMax300;


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the povertyFamilySize1
	 */
	public Double getPovertyFamilySize1() {
		return povertyFamilySize1;
	}

	/**
	 * @param povertyFamilySize1
	 *            the povertyFamilySize1 to set
	 */
	public void setPovertyFamilySize1(Double povertyFamilySize1) {
		this.povertyFamilySize1 = povertyFamilySize1;
	}

	/**
	 * @return the povertyFamilySize2
	 */
	public Double getPovertyFamilySize2() {
		return povertyFamilySize2;
	}

	/**
	 * @param povertyFamilySize2
	 *            the povertyFamilySize2 to set
	 */
	public void setPovertyFamilySize2(Double povertyFamilySize2) {
		this.povertyFamilySize2 = povertyFamilySize2;
	}

	/**
	 * @return the povertyFamilySize3
	 */
	public Double getPovertyFamilySize3() {
		return povertyFamilySize3;
	}

	/**
	 * @param povertyFamilySize3
	 *            the povertyFamilySize3 to set
	 */
	public void setPovertyFamilySize3(Double povertyFamilySize3) {
		this.povertyFamilySize3 = povertyFamilySize3;
	}

	/**
	 * @return the povertyFamilySize4
	 */
	public Double getPovertyFamilySize4() {
		return povertyFamilySize4;
	}

	/**
	 * @param povertyFamilySize4
	 *            the povertyFamilySize4 to set
	 */
	public void setPovertyFamilySize4(Double povertyFamilySize4) {
		this.povertyFamilySize4 = povertyFamilySize4;
	}

	/**
	 * @return the povertyFamilySize5
	 */
	public Double getPovertyFamilySize5() {
		return povertyFamilySize5;
	}

	/**
	 * @param povertyFamilySize5
	 *            the povertyFamilySize5 to set
	 */
	public void setPovertyFamilySize5(Double povertyFamilySize5) {
		this.povertyFamilySize5 = povertyFamilySize5;
	}

	/**
	 * @return the povertyFamilySize6
	 */
	public Double getPovertyFamilySize6() {
		return povertyFamilySize6;
	}

	/**
	 * @param povertyFamilySize6
	 *            the povertyFamilySize6 to set
	 */
	public void setPovertyFamilySize6(Double povertyFamilySize6) {
		this.povertyFamilySize6 = povertyFamilySize6;
	}

	/**
	 * @return the povertyFamilySize7
	 */
	public Double getPovertyFamilySize7() {
		return povertyFamilySize7;
	}

	/**
	 * @param povertyFamilySize7
	 *            the povertyFamilySize7 to set
	 */
	public void setPovertyFamilySize7(Double povertyFamilySize7) {
		this.povertyFamilySize7 = povertyFamilySize7;
	}

	/**
	 * @return the povertyFamilySize8
	 */
	public Double getPovertyFamilySize8() {
		return povertyFamilySize8;
	}

	/**
	 * @param povertyFamilySize8
	 *            the povertyFamilySize8 to set
	 */
	public void setPovertyFamilySize8(Double povertyFamilySize8) {
		this.povertyFamilySize8 = povertyFamilySize8;
	}

	/**
	 * @return the additionalPerson
	 */
	public Double getAdditionalPerson() {
		return additionalPerson;
	}

	/**
	 * @param additionalPerson
	 *            the additionalPerson to set
	 */
	public void setAdditionalPerson(Double additionalPerson) {
		this.additionalPerson = additionalPerson;
	}

	
	@Column(name = "YEAR", unique = true, nullable = false)
	private Long year;
	@Column(name = "STATE_CODE", unique = true, nullable = false)
	private String stateCode;

	/**
	 * @return the year
	 */
	public Long getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(Long year) {
		this.year = year;
	}

	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @param stateCode
	 *            the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @return the applicablePercentMin132
	 */
	public Double getApplicablePercentMin132() {
		return applicablePercentMin132;
	}

	/**
	 * @param applicablePercentMin132 the applicablePercentMin132 to set
	 */
	public void setApplicablePercentMin132(Double applicablePercentMin132) {
		this.applicablePercentMin132 = applicablePercentMin132;
	}

	/**
	 * @return the applicablePercentMax132
	 */
	public Double getApplicablePercentMax132() {
		return applicablePercentMax132;
	}

	/**
	 * @param applicablePercentMax132 the applicablePercentMax132 to set
	 */
	public void setApplicablePercentMax132(Double applicablePercentMax132) {
		this.applicablePercentMax132 = applicablePercentMax132;
	}

	/**
	 * @return the applicablePercentMin133
	 */
	public Double getApplicablePercentMin133() {
		return applicablePercentMin133;
	}

	/**
	 * @param applicablePercentMin133 the applicablePercentMin133 to set
	 */
	public void setApplicablePercentMin133(Double applicablePercentMin133) {
		this.applicablePercentMin133 = applicablePercentMin133;
	}

	/**
	 * @return the applicablePercentMax133
	 */
	public Double getApplicablePercentMax133() {
		return applicablePercentMax133;
	}

	/**
	 * @param applicablePercentMax133 the applicablePercentMax133 to set
	 */
	public void setApplicablePercentMax133(Double applicablePercentMax133) {
		this.applicablePercentMax133 = applicablePercentMax133;
	}

	/**
	 * @return the applicablePercentMin150
	 */
	public Double getApplicablePercentMin150() {
		return applicablePercentMin150;
	}

	/**
	 * @param applicablePercentMin150 the applicablePercentMin150 to set
	 */
	public void setApplicablePercentMin150(Double applicablePercentMin150) {
		this.applicablePercentMin150 = applicablePercentMin150;
	}

	/**
	 * @return the applicablePercentMax150
	 */
	public Double getApplicablePercentMax150() {
		return applicablePercentMax150;
	}

	/**
	 * @param applicablePercentMax150 the applicablePercentMax150 to set
	 */
	public void setApplicablePercentMax150(Double applicablePercentMax150) {
		this.applicablePercentMax150 = applicablePercentMax150;
	}

	/**
	 * @return the applicablePercentMin200
	 */
	public Double getApplicablePercentMin200() {
		return applicablePercentMin200;
	}

	/**
	 * @param applicablePercentMin200 the applicablePercentMin200 to set
	 */
	public void setApplicablePercentMin200(Double applicablePercentMin200) {
		this.applicablePercentMin200 = applicablePercentMin200;
	}

	/**
	 * @return the applicablePercentMax200
	 */
	public Double getApplicablePercentMax200() {
		return applicablePercentMax200;
	}

	/**
	 * @param applicablePercentMax200 the applicablePercentMax200 to set
	 */
	public void setApplicablePercentMax200(Double applicablePercentMax200) {
		this.applicablePercentMax200 = applicablePercentMax200;
	}

	/**
	 * @return the applicablePercentMin250
	 */
	public Double getApplicablePercentMin250() {
		return applicablePercentMin250;
	}

	/**
	 * @param applicablePercentMin250 the applicablePercentMin250 to set
	 */
	public void setApplicablePercentMin250(Double applicablePercentMin250) {
		this.applicablePercentMin250 = applicablePercentMin250;
	}

	/**
	 * @return the applicablePercentMax250
	 */
	public Double getApplicablePercentMax250() {
		return applicablePercentMax250;
	}

	/**
	 * @param applicablePercentMax250 the applicablePercentMax250 to set
	 */
	public void setApplicablePercentMax250(Double applicablePercentMax250) {
		this.applicablePercentMax250 = applicablePercentMax250;
	}

	/**
	 * @return the applicablePercentMin300
	 */
	public Double getApplicablePercentMin300() {
		return applicablePercentMin300;
	}

	/**
	 * @param applicablePercentMin300 the applicablePercentMin300 to set
	 */
	public void setApplicablePercentMin300(Double applicablePercentMin300) {
		this.applicablePercentMin300 = applicablePercentMin300;
	}

	/**
	 * @return the applicablePercentMax300
	 */
	public Double getApplicablePercentMax300() {
		return applicablePercentMax300;
	}

	/**
	 * @param applicablePercentMax300 the applicablePercentMax300 to set
	 */
	public void setApplicablePercentMax300(Double applicablePercentMax300) {
		this.applicablePercentMax300 = applicablePercentMax300;
	}



}
