package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AffectedInsurancePremium{
  @JsonProperty("InsurancePlanVariantCategoryNumericCode")
  
  private InsurancePlanVariantCategoryNumericCode InsurancePlanVariantCategoryNumericCode;
  @JsonProperty("RateArea")
  
  private RateArea RateArea;
  @JsonProperty("InsurancePremiumElectedAPTCAmount")
  
  private InsurancePremiumElectedAPTCAmount InsurancePremiumElectedAPTCAmount;
  @JsonProperty("InsurancePremiumIndividualResponsibilityTotalAmount")
  
  private InsurancePremiumIndividualResponsibilityTotalAmount InsurancePremiumIndividualResponsibilityTotalAmount;
  @JsonProperty("InsurancePremiumAmount")
  
  private InsurancePremiumAmount InsurancePremiumAmount;
  public void setInsurancePlanVariantCategoryNumericCode(InsurancePlanVariantCategoryNumericCode InsurancePlanVariantCategoryNumericCode){
   this.InsurancePlanVariantCategoryNumericCode=InsurancePlanVariantCategoryNumericCode;
  }
  public InsurancePlanVariantCategoryNumericCode getInsurancePlanVariantCategoryNumericCode(){
   return InsurancePlanVariantCategoryNumericCode;
  }
  public void setRateArea(RateArea RateArea){
   this.RateArea=RateArea;
  }
  public RateArea getRateArea(){
   return RateArea;
  }
  public void setInsurancePremiumElectedAPTCAmount(InsurancePremiumElectedAPTCAmount InsurancePremiumElectedAPTCAmount){
   this.InsurancePremiumElectedAPTCAmount=InsurancePremiumElectedAPTCAmount;
  }
  public InsurancePremiumElectedAPTCAmount getInsurancePremiumElectedAPTCAmount(){
   return InsurancePremiumElectedAPTCAmount;
  }
  public void setInsurancePremiumIndividualResponsibilityTotalAmount(InsurancePremiumIndividualResponsibilityTotalAmount InsurancePremiumIndividualResponsibilityTotalAmount){
   this.InsurancePremiumIndividualResponsibilityTotalAmount=InsurancePremiumIndividualResponsibilityTotalAmount;
  }
  public InsurancePremiumIndividualResponsibilityTotalAmount getInsurancePremiumIndividualResponsibilityTotalAmount(){
   return InsurancePremiumIndividualResponsibilityTotalAmount;
  }
  public void setInsurancePremiumAmount(InsurancePremiumAmount InsurancePremiumAmount){
   this.InsurancePremiumAmount=InsurancePremiumAmount;
  }
  public InsurancePremiumAmount getInsurancePremiumAmount(){
   return InsurancePremiumAmount;
  }
}