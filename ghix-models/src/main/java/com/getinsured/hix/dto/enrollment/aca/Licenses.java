package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class Licenses{
  @JsonProperty("issuerId")
  
  private Integer issuerId;
  @JsonProperty("aorLastName")
  
  private String aorLastName;
  @JsonProperty("state")
  
  private String state;
  @JsonProperty("aorFirstName")
  
  private String aorFirstName;
  @JsonProperty("npn")
  
  private Integer npn;
  public void setIssuerId(Integer issuerId){
   this.issuerId=issuerId;
  }
  public Integer getIssuerId(){
   return issuerId;
  }
  public void setAorLastName(String aorLastName){
   this.aorLastName=aorLastName;
  }
  public String getAorLastName(){
   return aorLastName;
  }
  public void setState(String state){
   this.state=state;
  }
  public String getState(){
   return state;
  }
  public void setAorFirstName(String aorFirstName){
   this.aorFirstName=aorFirstName;
  }
  public String getAorFirstName(){
   return aorFirstName;
  }
  public void setNpn(Integer npn){
   this.npn=npn;
  }
  public Integer getNpn(){
   return npn;
  }
}