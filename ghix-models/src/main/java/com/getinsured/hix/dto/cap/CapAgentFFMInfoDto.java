package com.getinsured.hix.dto.cap;

/*
 * Created by kaul_s on 2/26/15.
 */
public class CapAgentFFMInfoDto {

	String NPN;
	String ffmUsername;
	String ffmPassword;
	String proxy;
	int userId;

	public String getNPN() {
		return NPN;
	}

	public void setNPN(String nPN) {
		NPN = nPN;
	}

	public String getFfmUsername() {
		return ffmUsername;
	}

	public void setFfmUsername(String ffmUsername) {
		this.ffmUsername = ffmUsername;
	}

	public String getFfmPassword() {
		return ffmPassword;
	}

	public void setFfmPassword(String ffmPassword) {
		this.ffmPassword = ffmPassword;
	}

	public String getProxy() {
		return proxy;
	}

	public void setProxy(String proxy) {
		this.proxy = proxy;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
