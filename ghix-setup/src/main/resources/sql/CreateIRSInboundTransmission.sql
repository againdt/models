
  CREATE TABLE "IRS_INBOUND_TRANSMISSION"(
  ID NUMBER, 
  EFTFileName VARCHAR2(400 BYTE), 
  BATCH_ID VARCHAR2(400 BYTE), 
  HOUSEHOLD_CASE_ID CLOB, 	
  MONTH VARCHAR2(50 BYTE), 
  YEAR VARCHAR2(50 BYTE),
  IRS_ErrorMessageCd VARCHAR2(100 BYTE),
  DOCUMENT_SEQ_ID VARCHAR2(50 BYTE),  
  IRS_ErrorMessageTx VARCHAR2(400 BYTE),
  IRS_XpathContent VARCHAR2(400 BYTE),
  Response_Type VARCHAR2(50 BYTE),
  Hub_ResponseCode VARCHAR2(100 BYTE),
  Hub_ResponseDesc VARCHAR2(400 BYTE),
   constraint PK_IRS_INBOUND_TRANSMISSION primary key (ID)
  );

   
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.ID
  IS 'primary key';
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.EFTFileName
  IS
    'generated to store document file name in IRS Inbound Transmision';
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.HOUSEHOLD_CASE_ID
  IS
    'generated to store HOUSHOLDCASEID that was in Inbound IRS Content File';
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.BATCH_ID
  IS
    'generated to store BATCHID that was received in Inbound IRS xml file';
   COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.MONTH
  IS
    'generated to store month for which IRS file was submitted';
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.YEAR  
    IS 
   'generated to store Year for which IRS file was submitted';
     COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.IRS_ErrorMessageCd  
    IS 
   'generated to store ErrorMessageCd received from Inbound EOM OUT file';
   
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.IRS_ErrorMessageTx  
    IS 
   'generated to store ErrorMessageTx received from Inbound EOM OUT file';
   
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.Response_Type  
    IS 
   'generated to store Response_Type from Inbound transmission whether it is NACK or IRS response';
   
    COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.DOCUMENT_SEQ_ID  
    IS 
   'generated to store document sequence id received from Inbound EOM OUT IRS file';
   
     COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.IRS_XpathContent  
    IS 
     'generated to store XpathContent received from Inbound EOM OUT file';
     COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.Hub_ResponseCode  
    IS 
     'generated to store ResponseCode received from CMS inbound flow';
      COMMENT ON COLUMN IRS_INBOUND_TRANSMISSION.Hub_ResponseDesc  
    IS 
     'generated to store ResponseDesc received from CMS inbound flow';

     CREATE SEQUENCE IRS_INBOUND_TRANSMISSION_SEQ;




