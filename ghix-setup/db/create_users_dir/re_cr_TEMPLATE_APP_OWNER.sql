--drop user TEMPLATE_APP_OWNER cascade;
create user TEMPLATE_APP_OWNER identified by TEMPLATE_APP_OWNER 
default tablespace TEMPLATE_DATA temporary tablespace temp quota unlimited on TEMPLATE_DATA; 
--- for TDE
--alter user TEMPLATE_APP_OWNER quota unlimited on TEMPLATE_INDEX;

----
grant create session to TEMPLATE_APP_OWNER;
--grant user_role to TEMPLATE_APP_OWNER;
grant CREATE VIEW  to TEMPLATE_APP_OWNER;
grant CREATE SYNONYM  to TEMPLATE_APP_OWNER;
grant CREATE TABLE  to TEMPLATE_APP_OWNER;
grant CREATE SESSION  to TEMPLATE_APP_OWNER;
grant SELECT ANY TABLE  to TEMPLATE_APP_OWNER;
grant CREATE TRIGGER  to TEMPLATE_APP_OWNER;
grant CREATE PROCEDURE  to TEMPLATE_APP_OWNER;
grant CREATE SEQUENCE  to TEMPLATE_APP_OWNER;

--- grant to access directory DATALOAD_DIR
!mkdir /home/oracle/dataload_dir
CREATE DIRECTORY DATALOAD_DIR AS '/home/oracle/dataload_dir';
GRANT READ,WRITE ON DIRECTORY DATALOAD_DIR to TEMPLATE_APP_OWNER;

---- for Flashback Archive
GRANT FLASHBACK ARCHIVE ADMINISTER TO TEMPLATE_APP_OWNER;

create role TEMPLATE_APP_USER_ROLE;
create role TEMPLATE_APP_READONLY_ROLE;
quit

