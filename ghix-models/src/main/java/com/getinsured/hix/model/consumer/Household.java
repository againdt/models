package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.dto.consumer.HouseholdExtensionDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.json.type.JsonBinaryType;
import com.getinsured.timeshift.util.TSDate;


/**
 * The persistent class for the CMR_HOUSEHOLD database table.
 * 
 */

@Audited
@Entity
@Table(name="CMR_HOUSEHOLD")
@DynamicUpdate
@DynamicInsert
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@TypeDefs({
    @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class Household implements Serializable{
	
	public enum YESNOFLAG{YES, NO;}
	public enum PREFERREDCONTACTTIME{MORNING, AFTERNOON, EVENING;}
	public static ArrayList<String> ssn_not_provided_reasons = new ArrayList<String>() {{
	    add("Reason1");
	    add("Reason2");
	    add("Other");
	}};
	
	public String getFfmHouseholdID() {
		return ffmHouseholdID;
	}

	public void setFfmHouseholdID(String ffmHouseholdID) {
		this.ffmHouseholdID = ffmHouseholdID;
	}

	public String getGiHouseholdId() {
		return giHouseholdId;
	}

	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CMR_HOUSEHOLD_SEQ")
	@SequenceGenerator(name = "CMR_HOUSEHOLD_SEQ", sequenceName = "CMR_HOUSEHOLD_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "APTC")
	private String aptc;
	
	@Column(name = "FFM_HOUSEHOLD_ID")
	private String ffmHouseholdID;
	
	@Column(name = "AFFILIATE_NAME")
	private String affiliate; 
	
	@Column(name = "CSR")
	private String csr; 
	
	@Column(name = "COUNTY_CODE")
	private String countyCode; 

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name="ELIG_LEAD_ID")
	private EligLead eligLead; 
	
	@Column(name = "EMAIL_ADDRESS")
	private String email; 
	
	@Column(name = "FAMILY_SIZE")
	private Integer familySize; 
	
	@Column(name = "HOUSEHOLD_INCOME")
	private Double houseHoldIncome; 
	
	@Column(name = "LAST_VISITED")
	private String lastVisited; 
	
	@Column(name = "NO_OF_APPLICANTS")
	private Integer numberOfApplicants; 
	
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber; 
	
	@Column(name = "MOBILE_PHONE")
	private String mobilePhone; 
	
	@Column(name = "HOME_PHONE")
	private String homePhone; 
	
	@Column(name = "POA_PHONE")
	private String poaPhone; 
	
	@Column(name = "PREMIUM")
	private String premium; 
	
	@Column(name = "STAGE")
	private String stage; 
	
	@Column(name = "STATUS")
	private String status; 
	
	@Column(name = "LEAD_STATUS")
	private String leadStatus; 
	
	@Column(name = "EXTERNAL_HOUSEHOLD_CASE_ID")
	private String householdCaseId;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private AccountUser user;
	
	@Column(name = "ZIPCODE")
	private String zipCode; 

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable=false)
	private Date created;
	
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;

	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
	@Column(name="FIRST_NAME")
    private String firstName;
	
	@Column(name="LAST_NAME")
    private String lastName;
	
	@Column(name="STATE")
    private String state;
	
	@Column(name = "FFM_RESPONSE")
	private String ffmResponse;
	
	@Column(name="GI_HOUSEHOLD_ID")
    private String giHouseholdId;
	
	/* Removed RelationTargetAuditMode.NOT_AUDITED as part of HIX-84552 */
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name="CONTACT_LOCATION_ID")
	private Location location;
	
	/* Removed RelationTargetAuditMode.NOT_AUDITED as part of HIX-84552 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="LOCATION_ID")
	private Location locationId;
	
	
	@Column(name="PREF_SPOKEN_LANG")
	private Integer prefSpokenLang;
	
	
	@Column(name="PREF_WRITTEN_LANG")
	private Integer prefWrittenLang;

	@Column(name="PREF_CONTACT_METHOD")
	private Integer prefContactMethod;
	
	
	@Column(name="RIDP_VERIFIED")
	private String ridpVerified;
	
	@Column(name="DSH_REFERENCE_NUMBER")
	private String dshReferenceNumber;
    
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="NAME_SUFFIX")
	private String nameSuffix;
	
	@Column(name="RIDP_SOURCE")
	private String ridpSource;
	
    @Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "RIDP_DATE")
	private Date ridpDate;
	    
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "BIRTH_DATE")
	private Date birthDate;
	
	@Column(name="TENANT_ID")
    private Long tenantId;
	
	@Column(name="SSN")
    private String ssn;
	
	@Column(name="SSN_NOT_PROVIDED_REASON")
    private String ssnNotProvidedReason;
	
	public Integer getServicedBy() {
		return servicedBy;
	}

	public void setServicedBy(Integer servicedBy) {
		this.servicedBy = servicedBy;
	}

	@Column(name="SERVICED_BY")
	private Integer servicedBy;
	
	@Column(name="CONSENT_TO_CALLBACK")
	@Enumerated(EnumType.STRING)
	private YESNOFLAG consentToCallback;
	
	@Column(name="HEALTH_ADVOCATE")
	@Enumerated(EnumType.STRING)
	private YESNOFLAG healthAdvocate;
	
//	@Column(name="PREFERRED_CONTACT_TIME")
	@Transient
	@Enumerated(EnumType.STRING)
	private PREFERREDCONTACTTIME preferredContactTime;
	
	@NotAudited
	@Type(type = "jsonb")
	@Column(columnDefinition = "json",name="HOUSEHOLD_EXTENSION")
	private HouseholdExtensionDto householdExtension;
	
	@Column(name="text_me")
	private Boolean textMe;
	
	@Column(name="mobile_number_verified")
	private Boolean mobileNumberVerified;
	
	@Column(name="PAPERLESS_1095")
	private Boolean optInPaperless1095;
	
	@Column(name="PAPERLESS_1095_USER")
	private Integer paperless1095User;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "PAPERLESS_1095_DATE")
	private Date paperless1095Date;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "PREFERENCES_REVIEW_DATE")
	private Date preferencesReviewDate;
	
	public String getAffiliate() {
		return affiliate;
	}

	public void setAffiliate(String affiliate) {
		this.affiliate = affiliate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getFamilySize() {
		return familySize;
	}

	public void setFamilySize(Integer familySize) {
		this.familySize = familySize;
	}

	public Double getHouseHoldIncome() {
		return houseHoldIncome;
	}

	public void setHouseHoldIncome(Double houseHoldIncome) {
		this.houseHoldIncome = houseHoldIncome;
	}

	public String getLastVisited() {
		return lastVisited;
	}

	public void setLastVisited(String lastVisited) {
		this.lastVisited = lastVisited;
	}

	public Integer getNumberOfApplicants() {
		return numberOfApplicants;
	}

	public void setNumberOfApplicants(Integer numberOfApplicants) {
		this.numberOfApplicants = numberOfApplicants;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumberOne() {
		return mobilePhone;
	}

	public void setPhoneNumberOne(String phoneNumberOne) {
		this.mobilePhone = phoneNumberOne;
	}

	public String getPhoneNumberTwo() {
		return homePhone;
	}

	public void setPhoneNumberTwo(String phoneNumberTwo) {
		this.homePhone = phoneNumberTwo;
	}

	public String getPoaPhone() {
		return poaPhone;
	}

	public void setPoaPhone(String poaPhone) {
		this.poaPhone = poaPhone;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public EligLead getEligLead() {
		return eligLead;
	}

	public void setEligLead(EligLead eligLead) {
		this.eligLead = eligLead;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getFfmResponse() {
		return ffmResponse;
	}

	public void setFfmResponse(String ffmResponse) {
		this.ffmResponse = ffmResponse;
	}
	
	public YESNOFLAG getConsentToCallback() {
		return consentToCallback;
	}

	public void setConsentToCallback(YESNOFLAG consentToCallback) {
		this.consentToCallback = consentToCallback;
	}
	
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getRidpVerified() {
		return ridpVerified;
	}

	public void setRidpVerified(String ridpVerified) {
		this.ridpVerified = ridpVerified;
	}
	
	public String getDshReferenceNumber() {
		return dshReferenceNumber;
	}

	public void setDshReferenceNumber(String dshReferenceNumber) {
		this.dshReferenceNumber = dshReferenceNumber;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getRidpSource() {
		return ridpSource;
	}

	public void setRidpSource(String ridpSource) {
		this.ridpSource = ridpSource;
	}
	
	public Date getRidpDate() {
        return ridpDate;
    }

	public void setRidpDate(Date ridpDate) {
        this.ridpDate = ridpDate;
    }
	
	public YESNOFLAG getHealthAdvocate() {
		return healthAdvocate;
	}

	public void setHealthAdvocate(YESNOFLAG healthAdvocate) {
		this.healthAdvocate = healthAdvocate;
	}

	public Location getPrefContactLocation() {
		return locationId;
	}

	public void setPrefContactLocation(Location locationId) {
		this.locationId = locationId;
	}
	
	public Integer getPrefSpokenLang() {
		return prefSpokenLang;
	}

	public void setPrefSpokenLang(Integer prefSpokenLang) {
		this.prefSpokenLang = prefSpokenLang;
	}

	public Integer getPrefWrittenLang() {
		return prefWrittenLang;
	}

	public void setPrefWrittenLang(Integer prefWrittenLang) {
		this.prefWrittenLang = prefWrittenLang;
	}

	public Integer getPrefContactMethod() {
		return prefContactMethod;
	}

	public void setPrefContactMethod(Integer prefContactMethod) {
		this.prefContactMethod = prefContactMethod;
	}

	
	

	
	public PREFERREDCONTACTTIME getPreferredContactTime() {
		return preferredContactTime;
	}
	
	public void setPreferredContactTime(
			PREFERREDCONTACTTIME preferredContactTime) {
		this.preferredContactTime = preferredContactTime;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
		StringBuffer sb = new StringBuffer(UUID.randomUUID().toString());
		sb.deleteCharAt(8);
		sb.deleteCharAt(12);
		sb.deleteCharAt(16);
		sb.deleteCharAt(20);
		this.setGiHouseholdId(sb.toString());

		if (StringUtils.isEmpty(this.getHouseholdCaseId()) && !"MN".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))) {
			this.setHouseholdCaseId(sb.toString());
		}
		
		this.setFirstName(WordUtils.capitalizeFully(firstName));
		this.setLastName(WordUtils.capitalizeFully(lastName));
		/**
		 * Jira Id: HIX-105856 Making this changes to persist Email in Lower case
		 */
		if(StringUtils.isNotBlank(this.email)){
			this.email = StringUtils.lowerCase(this.email);
		}
		
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        setTenantId(TenantContextHolder.getTenant().getId());
		}
		this.setPreferencesReviewDate(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		AccountUser tmpUser = this.getUser();
		if(tmpUser != null && this.email != null && tmpUser.getEmail() != null && !tmpUser.getEmail().equalsIgnoreCase(this.email)){
			if(GhixPlatformConstants.USER_NAME_POLICY.equalsIgnoreCase("SAME_AS_EMAIL".intern())) {
				throw new GIRuntimeException("Invalid CMR Household record: Email address in the Household record does not match with User's email ID:"+this.getId());
			}
		}
		this.setUpdated(new TSDate());
		this.setFirstName(WordUtils.capitalizeFully(firstName));
		this.setLastName(WordUtils.capitalizeFully(lastName));
		
		/**
		 * Jira Id: HIX-105856 Making this changes to persist Email in Lower case
		 */
		if(StringUtils.isNotBlank(this.email)){
			this.email = StringUtils.lowerCase(this.email);
		}
	}
	
	public static void main(String args[]){
		StringBuffer sb = new StringBuffer(UUID.randomUUID().toString());
		System.out.println(sb);
		sb.deleteCharAt(8);
		sb.deleteCharAt(12);
		sb.deleteCharAt(16);
		sb.deleteCharAt(20);
		System.out.println(sb);
	
		
	}

	public HouseholdExtensionDto getHouseholdExtension() {
		return householdExtension;
	}

	public void setHouseholdExtension(HouseholdExtensionDto householdExtension) {
		this.householdExtension = householdExtension;
	}
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getSsnNotProvidedReason() {
		return ssnNotProvidedReason;
	}

	public void setSsnNotProvidedReason(String ssnNotProvidedReason) {
		this.ssnNotProvidedReason = ssnNotProvidedReason;
	}

	public Boolean getTextMe() {
		return textMe;
	}

	public void setTextMe(Boolean textMe) {
		this.textMe = textMe;
	}

	public Boolean getMobileNumberVerified() {
		return mobileNumberVerified;
	}

	public void setMobileNumberVerified(Boolean mobileNumberVerified) {
		this.mobileNumberVerified = mobileNumberVerified;
	}

	public String getHouseholdCaseId() { 
		return householdCaseId; 
	}
	
	public void setHouseholdCaseId(String householdCaseId) { 
		this.householdCaseId = householdCaseId;
	
	}
	
	public Boolean getOptInPaperless1095() {
		return optInPaperless1095;
	}

	public void setOptInPaperless1095(Boolean optInPaperless1095) {
		this.optInPaperless1095 = optInPaperless1095;
	}

	public Integer getPaperless1095User() {
		return paperless1095User;
	}

	public void setPaperless1095User(Integer paperless1095User) {
		this.paperless1095User = paperless1095User;
	}

	public Date getPaperless1095Date() {
		return paperless1095Date;
	}

	public void setPaperless1095Date(Date paperless1095Date) {
		this.paperless1095Date = paperless1095Date;
	}

	public Date getPreferencesReviewDate() {
		return preferencesReviewDate;
	}

	public void setPreferencesReviewDate(Date preferencesReviewDate) {
		this.preferencesReviewDate = preferencesReviewDate;
	}
	
}
