package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "pm_crosswalk")
public class CrossWalk implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public enum IS_DELETED {
		Y, N;	// Y-Yes, N-No
	}
	
	public enum DENTAL_ONLY {
		Y, N;	// Y-Yes, N-No
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_crosswalk_seq")
	@SequenceGenerator(name = "pm_crosswalk_seq", sequenceName = "pm_crosswalk_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "hios_issuer_id")
	private String hisoIssuerId;
	
	@Column(name = "current_hios_plan_id")
	private String currentHiosPlanId;
	
	@Column(name = "county_name")
	private String countyName;
	
	@Column(name = "county_code")
	private String countyCode;
	
	@Column(name = "crosswalk_tier")
	private String crosswalkTier;
	
	@Column(name = "zip_list")
	private String ziplist;
	
	@Column(name = "crosswalk_level")
	private String crosswalkLevel;
	
	@Column(name = "crosswalk_reason")
	private String crosswalkReason;
	
	@Column(name = "crosswalk_hios_plan_id")
	private String crosswalkHiosPlanId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date lastUpdatedTimestamp;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdateBy;
	
	@Column(name = "applicable_year")
	private Integer applicableYear;
	
	@Column(name = "is_deleted")
	private String isDeleted;

	@Column(name = "is_catastrophic_or_child_only")
	private String isCatastrophicOrChildOnly;

	@Column(name = "aging_off_crosswalk_plan_id")
	private String agingOffCrosswalkPlanId;

	@Column(name = "cur_plan_form_num")
	private String currentPlanFormNumber;

	@Column(name = "cur_plan_naic_track_num")
	private String currentPlanNAICTrackNumber;

	@Column(name = "cw_plan_form_num")
	private String crosswalkPlanFormNumber;

	@Column(name = "cw_plan_naic_track_num")
	private String crosswalkPlanNAICTrackNumber;

	@Column(name = "cw_agingoff_plan_form_num")
	private String crosswalkAgingOffPlanFormNumber;

	@Column(name = "cw_agingoff_plan_naic_trknum")
	private String crosswalkAgingOffPlanNAICTrackNumber;
	
	@Column(name = "dental_only")
	private String dentalOnly;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdatedTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdatedTimestamp(new TSDate());
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHisoIssuerId() {
		return hisoIssuerId;
	}

	public void setHisoIssuerId(String hisoIssuerId) {
		this.hisoIssuerId = hisoIssuerId;
	}

	public String getCurrentHiosPlanId() {
		return currentHiosPlanId;
	}

	public void setCurrentHiosPlanId(String currentHiosPlanId) {
		this.currentHiosPlanId = currentHiosPlanId;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getCrosswalkTier() {
		return crosswalkTier;
	}

	public void setCrosswalkTier(String crosswalkTier) {
		this.crosswalkTier = crosswalkTier;
	}

	public String getZiplist() {
		return ziplist;
	}

	public void setZiplist(String ziplist) {
		this.ziplist = ziplist;
	}

	public String getCrosswalkLevel() {
		return crosswalkLevel;
	}

	public void setCrosswalkLevel(String crosswalkLevel) {
		this.crosswalkLevel = crosswalkLevel;
	}

	public String getCrosswalkReason() {
		return crosswalkReason;
	}

	public void setCrosswalkReason(String crosswalkReason) {
		this.crosswalkReason = crosswalkReason;
	}

	public String getCrosswalkHiosPlanId() {
		return crosswalkHiosPlanId;
	}

	public void setCrosswalkHiosPlanId(String crosswalkHiosPlanId) {
		this.crosswalkHiosPlanId = crosswalkHiosPlanId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getIsCatastrophicOrChildOnly() {
		return isCatastrophicOrChildOnly;
	}

	public void setIsCatastrophicOrChildOnly(String isCatastrophicOrChildOnly) {
		this.isCatastrophicOrChildOnly = isCatastrophicOrChildOnly;
	}

	public String getAgingOffCrosswalkPlanId() {
		return agingOffCrosswalkPlanId;
	}

	public void setAgingOffCrosswalkPlanId(String agingOffCrosswalkPlanId) {
		this.agingOffCrosswalkPlanId = agingOffCrosswalkPlanId;
	}

	public String getCurrentPlanFormNumber() {
		return currentPlanFormNumber;
	}

	public void setCurrentPlanFormNumber(String currentPlanFormNumber) {
		this.currentPlanFormNumber = currentPlanFormNumber;
	}

	public String getCurrentPlanNAICTrackNumber() {
		return currentPlanNAICTrackNumber;
	}

	public void setCurrentPlanNAICTrackNumber(String currentPlanNAICTrackNumber) {
		this.currentPlanNAICTrackNumber = currentPlanNAICTrackNumber;
	}

	public String getCrosswalkPlanFormNumber() {
		return crosswalkPlanFormNumber;
	}

	public void setCrosswalkPlanFormNumber(String crosswalkPlanFormNumber) {
		this.crosswalkPlanFormNumber = crosswalkPlanFormNumber;
	}

	public String getCrosswalkPlanNAICTrackNumber() {
		return crosswalkPlanNAICTrackNumber;
	}

	public void setCrosswalkPlanNAICTrackNumber(String crosswalkPlanNAICTrackNumber) {
		this.crosswalkPlanNAICTrackNumber = crosswalkPlanNAICTrackNumber;
	}

	public String getCrosswalkAgingOffPlanFormNumber() {
		return crosswalkAgingOffPlanFormNumber;
	}

	public void setCrosswalkAgingOffPlanFormNumber(
			String crosswalkAgingOffPlanFormNumber) {
		this.crosswalkAgingOffPlanFormNumber = crosswalkAgingOffPlanFormNumber;
	}

	public String getCrosswalkAgingOffPlanNAICTrackNumber() {
		return crosswalkAgingOffPlanNAICTrackNumber;
	}

	public void setCrosswalkAgingOffPlanNAICTrackNumber(
			String crosswalkAgingOffPlanNAICTrackNumber) {
		this.crosswalkAgingOffPlanNAICTrackNumber = crosswalkAgingOffPlanNAICTrackNumber;
	}

	public String getDentalOnly() {
		return dentalOnly;
	}

	public void setDentalOnly(String dentalOnly) {
		this.dentalOnly = dentalOnly;
	}
}
