package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class ScreenPopEmployeeDto implements Serializable {

	private static final long serialVersionUID = 2411825571083587943L;

	private String employerName;
	private String employerId;
	private String censusType;
	private String contributionText;
	private String contributionAmt;
	private String pinCode;
	private String accessCode;
	private String censusTypeText;
	
	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getEmployerId() {
		return employerId;
	}

	public void setEmployerId(String employerId) {
		this.employerId = employerId;
	}

	public String getCensusType() {
		return censusType;
	}

	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}

	public String getContributionText() {
		return contributionText;
	}

	public void setContributionText(String contributionText) {
		this.contributionText = contributionText;
	}

	public String getContributionAmt() {
		return contributionAmt;
	}

	public void setContributionAmt(String contributionAmt) {
		this.contributionAmt = contributionAmt;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	
	public String getAccessCode() {
		return accessCode;
	}
	
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getCensusTypeText() {
		return censusTypeText;
	}

	public void setCensusTypeText(String censusTypeText) {
		this.censusTypeText = censusTypeText;
	}
}
