package com.getinsured.hix.dto.shop.employer.eps;

import java.util.List;
import java.util.Map;

public class PlanDTO implements Comparable <PlanDTO> {
	private Integer planId;
	private String issuerPlanNumber;
	private String tierName;
	private List<EmployeePlanDTO> employeePlanDTOs;
	private Float sumEmployeesPremium=0f;
	private Map<String, String> metalTierAvailability;
	
	/**
	 * @return the metalTierAvailability
	 */
	public Map<String, String> getMetalTierAvailability() {
		return metalTierAvailability;
	}
	/**
	 * @param metalTierAvailability the metalTierAvailability to set
	 */
	public void setMetalTierAvailability(Map<String, String> metalTierAvailability) {
		this.metalTierAvailability = metalTierAvailability;
	}
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	public String getTierName() {
		return tierName;
	}
	public void setTierName(String tierName) {
		this.tierName = tierName;
	}
	public List<EmployeePlanDTO> getEmployeePlanDTOs() {
		return employeePlanDTOs;
	}
	public void setEmployeePlanDTOs(List<EmployeePlanDTO> employeePlanDTOs) {
		this.employeePlanDTOs = employeePlanDTOs;
	}
	public Float getSumEmployeesPremium() {
		return sumEmployeesPremium;
	}
	public void setSumEmployeesPremium(Float sumEmployeesPremium) {
		this.sumEmployeesPremium = sumEmployeesPremium;
	}
	
	
	@Override
	public String toString() {
		String NewLine = System.getProperty( "line.separator" );
		String Indent  = "      ";
		StringBuilder strBuilder = new StringBuilder();
		
		strBuilder.append( "PlanDTO [ " +NewLine );
		strBuilder.append( Indent+"planId=" + planId +NewLine );
		strBuilder.append( Indent+"tierName=" + tierName +NewLine );
		strBuilder.append( Indent+"sumEmployeesPremium=" + sumEmployeesPremium +NewLine );
		strBuilder.append( Indent+"employeePlanDTOs ==[" +NewLine );
		strBuilder.append( Indent+Indent+employeePlanDTOs+"]"+NewLine );
		strBuilder.append( "]" );
		return strBuilder.toString();
	}
	
	@Override
	public int compareTo(PlanDTO o) {
		return sumEmployeesPremium.compareTo(o.sumEmployeesPremium);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planId == null) ? 0 : planId.hashCode());
		result = prime
				* result
				+ ((sumEmployeesPremium == null) ? 0 : sumEmployeesPremium
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanDTO other = (PlanDTO) obj;
		if (planId == null) {
			if (other.planId != null)
				return false;
		} else if (!planId.equals(other.planId))
			return false;
		if (sumEmployeesPremium == null) {
			if (other.sumEmployeesPremium != null)
				return false;
		} else if (!sumEmployeesPremium.equals(other.sumEmployeesPremium))
			return false;
		return true;
	}
	
	
}
