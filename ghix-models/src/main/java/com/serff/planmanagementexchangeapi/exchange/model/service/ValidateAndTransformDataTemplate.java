
package com.serff.planmanagementexchangeapi.exchange.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.serff.planmanagementexchangeapi.exchange.model.pm.DataTemplates;


/**
 * <p>Java class for ValidateAndTransformDataTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidateAndTransformDataTemplate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataTemplates" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}DataTemplates"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestId",
    "dataTemplates"
})
@XmlRootElement(name="validateAndTransformDataTemplate")
public class ValidateAndTransformDataTemplate {

    @XmlElement(required = true)
    protected String requestId;
    @XmlElement(required = true)
    protected DataTemplates dataTemplates;

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the dataTemplates property.
     * 
     * @return
     *     possible object is
     *     {@link DataTemplates }
     *     
     */
    public DataTemplates getDataTemplates() {
        return dataTemplates;
    }

    /**
     * Sets the value of the dataTemplates property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataTemplates }
     *     
     */
    public void setDataTemplates(DataTemplates value) {
        this.dataTemplates = value;
    }

}
