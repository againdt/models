package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.hix.platform.util.SortableEntity;
import com.getinsured.planmgmt.validation.GreaterThanCurrentDate;

/**
 * The persistent class for the roles database table.
 */
@Audited
@Entity
@Table(name = "plan")
@XmlAccessorType(XmlAccessType.NONE)
public class Plan implements Serializable, SortableEntity {
	private static final long serialVersionUID = 1L;

	public static enum PlanCategory {
		INDIVIDUAL, FAMILY;
	}

	public static enum PlanInsuranceType {
		HEALTH, DENTAL, LIFE, VISION, AME, STM, MEDICARE, MC_ADV, MC_SUP, MC_RX, MEDICAID;
	}

	public enum PlanStatus {
		CERTIFIED, DECERTIFIED, INCOMPLETE, LOADED;
	}

	public static enum PlanMarket {
		INDIVIDUAL, SHOP;
	}

	public static enum PlanLevel {
		PLATINUM, GOLD, SILVER, BRONZE, EXPANDEDBRONZE, CATASTROPHIC;
	}

	public static enum PlanLevelDental {
		HIGH, LOW, MEDICAL;
	}
	
	public static enum EnrollmentAvail {
		AVAILABLE, DEPENDENTSONLY, NOTAVAILABLE;
	}

	public static enum HSA {
		YES, NO;
	}

	public enum IssuerVerificationStatus {
		PENDING, VERIFIED;
	}

	public static enum Verified {
		VERIFIED, PENDING;
	}
	
	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}
	
	public enum IS_PUF {
		// Y-Yes, N-No
		Y, N;
	}
	
	public enum HSA_INDICATOR {
		// Y-Yes, N-No
		Y, N;
	}
	
	public enum EXCHANGE_TYPE {
		ON, OFF;
	}	
	
	public enum Tenant {
		GETINSURED, BENEFITBAY;
	}	
	
	public enum AVAILABLE_FOR {
		ADULTANDCHILD, CHILDONLY, ADULTONLY;
	}
	
	public static enum TerminateEnrollmentStatusResponse {
		FAILURE, SUCCESS;
	}
	
	public enum NON_COMMISSION_FLAG {
		// Y-Yes, N-No
		Y, N;
	}

	/**
	 * This validation group provides groupings for fields  {@link #status}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditCertificationStatus extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #status}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditCertificationStatusPHIX extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #status}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface DeCertificationStatus extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #status}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditEnrollmentAvailability extends Default{
		
	}
	
	/**
	 * This validation group provides groupings for fields  {@link #status}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface BulkUpdate extends Default{
		
	}
	
	public interface EnableOffExchangesFlow extends Default{
		
	}

	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Plan_Seq")
	@SequenceGenerator(name = "Plan_Seq", sequenceName = "plan_seq", allocationSize = 1)
	private int id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "issuer_id")
	private Issuer issuer;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "provider_network_id")
	private Network network;

	@Column(name = "insurance_type", length = 6)
	private String insuranceType;

	@Column(name = "market")
	private String market;

	@XmlAttribute(name = "planCoverageDescription")
	@Column(name = "name", length = 200)
	private String name;

	@Column(name = "issuer_plan_number", length = 30)
	private String issuerPlanNumber;

	@NotEmpty(message="{err.selectCertStatus}",groups={Plan.EditCertificationStatus.class,Plan.DeCertificationStatus.class,Plan.BulkUpdate.class,Plan.EditCertificationStatusPHIX.class})
	@Pattern(regexp="^CERTIFIED|DECERTIFIED|INCOMPLETE|LOADED$",message="{err.selectCertStatus}",groups={Plan.EditCertificationStatus.class,Plan.DeCertificationStatus.class,Plan.BulkUpdate.class,Plan.EditCertificationStatusPHIX.class})
	@Column(name = "status", length = 10)
	// @Enumerated(EnumType.STRING)
	private String status;

	@Column(name = "network_type", length = 5)
	private String networkType;

	/*@Column(name = "category", length = 12)
	private String category;*/

	/*@Column(name = "max_enrolee", length = 10)
	private String maxEnrolee;*/

	@Column(name = "brochure", length = 10)
	private String brochure;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "support_file", length = 100)
	private String supportFile;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;

	@Column(name = "certifiedby")
	private String certifiedby;

	@Column(name = "formularly_id")
	private String formularlyId;

	@Column(name = "formularly_url")
	private String formularlyUrl;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "hsa", length = 3)
	private String hsa;

	@Column(name = "enrollment_avail", length = 15)
	private String enrollmentAvail;
	
	@NotEmpty(message="{err.selectCertStatus}",groups={Plan.EditCertificationStatusPHIX.class})
	@Column(name = "issuer_verification_status", length = 100)
	private String issuerVerificationStatus;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "enrollment_avail_effdate")
	private Date enrollmentAvailEffDate;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "certified_on")
	private Date certifiedOn;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanHealth planHealth;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanDental planDental;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanSTM planStm;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanAME planAme;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanVision planVision;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanMedicare planMedicare;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlanLife planLife;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<TenantPlan> tenantPlan;

	@Column(name = "provider_network_id", insertable = false, updatable = false)
	private Integer providerNetworkId;

	@Column(name = "comment_id")
	private Integer commentId;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "verified", length = 3)
	private String verified;

	// Fields Added on 8May-2013-Start
	@Column(name = "hiosProduct_id")
	private String hiosProductId;

	@Column(name = "hpid")
	private String hpid;

	@Column(name = "network_id")
	private String networkId;

	@Column(name = "service_area_id")
	private Integer serviceAreaId;

	// FFM changes starts
	@Column(name = "state")
	private String state;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private  List<PlanRate> planRate;
	
	@NotEmpty(message="{err.selectCertStatus}",groups={Plan.EditEnrollmentAvailability.class,Plan.BulkUpdate.class})
	@Pattern(regexp="^AVAILABLE|DEPENDENTSONLY|NOTAVAILABLE$",message="{err.selectCertStatus}",groups={Plan.EditEnrollmentAvailability.class,Plan.BulkUpdate.class})
	@Column(name = "future_enrollment_avail")
	private String futureEnrollmentAvail; 
	
	@NotNull(message="{err.validateEfffDate}",groups={Plan.EditEnrollmentAvailability.class,Plan.BulkUpdate.class})
	@GreaterThanCurrentDate(message="{err.validateEfffDate}",groups={Plan.EditEnrollmentAvailability.class,Plan.BulkUpdate.class})
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "future_erl_avail_effdate")
	private Date futureErlAvailEffDate;
	
	@Column(name = "is_deleted")
	//@Enumerated(EnumType.STRING)
	private String isDeleted;

	@Column(name = "tp_id")
	private String tpId;
	
	@Column(name = "is_puf")
	@Enumerated(EnumType.STRING)
	private IS_PUF isPUF;
	
	@Column(name = "available_for")
	@Enumerated(EnumType.STRING)
	private AVAILABLE_FOR availableFor;
	
	@Column(name = "applicable_year")
    private Integer applicableYear;
	
	@Column(name = "BUSINESS_RULE_ID")
	private Integer businessRuleId ; 
	
	@Column(name = "eoc_doc_id")
	private String eocDocId;
	
	@Column(name = "eoc_doc_name")
	private String eocDocName;
	
	@Column(name = "composit_prm_avail_indicator")
	private String compositPremiumAvailableIndicator;
	
	@Column(name = "ehb_premium_fraction")
	private Float ehbPremiumFraction;
	
	@Column(name = "emp_hsahra_contri_indicator")
	private String empHSAHRAContributionIndicator;
	
	@Column(name = "emp_hsahra_contri_amount")
	private Float empHSAHRAContributionAmount;
	
	@Pattern(regexp="^YES|NO$",message="{err.selectOffExchangeFlag}",groups={Plan.EnableOffExchangesFlow.class,Plan.BulkUpdate.class})
	@Column(name = "enable_for_offexch_flow")
	private String enableForOffExchFlow;
	
	@Column(name = "non_commission_flag")
	private String nonCommissionFlag;
	
	public Plan() {
	}
	
	public Plan(int id, String name, int issuerId, String issuerName,
			String federalEin, Float csrAdvPayAmt, String planHealthLevel,
			String issuerPlanNumber, String insuranceType, String market) {
		this.id = id;
		this.name = name;
		this.issuer = new Issuer();
		this.issuer.setId(issuerId);
		this.issuer.setName(issuerName);
		this.issuer.setFederalEin(federalEin);
		this.planHealth = new PlanHealth();
		this.planHealth.setCsrAdvPayAmt(csrAdvPayAmt);
		this.planHealth.setPlanLevel(planHealthLevel);
		this.issuerPlanNumber = issuerPlanNumber;
		this.insuranceType = insuranceType;
		this.market = market;
	}
	
	public Plan(int id, String name, int issuerId, String issuerName,
			String federalEin, String planDentalLevel, String issuerPlanNumber,
			String insuranceType, String market) {
		this.id = id;
		this.name = name;
		this.issuer = new Issuer();
		this.issuer.setId(issuerId);
		this.issuer.setName(issuerName);
		this.issuer.setFederalEin(federalEin);
		this.planDental = new PlanDental();
		this.planDental.setPlanLevel(planDentalLevel);
		this.issuerPlanNumber = issuerPlanNumber;
		this.insuranceType = insuranceType;
		this.market = market;
	}

	// FFM changes ends

	// Fields Added on 8May-2013-Ends
	
	//Shashikant : Fields added on 30th August 2013 - start
	@Column(name = "brochure_ucm_id")
	private String brochureUCmId;
	
	@Column(name = "brochure_doc_name")
	private String brochureDocName;
	//Shashikant : Fields added on 30th August 2013 - end
	
	@Column(name = "exchange_type")
	private String exchangeType;	
	
	@NotNull(message="{err.deCertEffdate}",groups={Plan.DeCertificationStatus.class})
	@GreaterThanCurrentDate(message="{err.furtueDeCertDate}",groups={Plan.DeCertificationStatus.class})
	@Column(name = "decertification_eff_date")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date deCertificationEffDate;
	
	@NotNull(message="{err.enrollmentEndDate}",groups={Plan.DeCertificationStatus.class})
	@GreaterThanCurrentDate(message="{err.furtueDeCertDate}",groups={Plan.DeCertificationStatus.class})
	@Column(name = "enrollment_end_date")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date enrollmentEndDate;
	
	@Column(name = "pm_urrt_prc_id")
	private Integer unifiedPlanRateChangeId;
	
	@Column(name = "terminate_enrollment_status")
	private String terminateEnrollmentStatus;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	/*public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}*/

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}

	public Date getCertifiedOn() {
		return certifiedOn;
	}

	public void setCertifiedOn(Date certifiedOn) {
		this.certifiedOn = certifiedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFormularlyId() {
		return formularlyId;
	}

	public void setFormularlyId(String formularlyId) {
		this.formularlyId = formularlyId;
	}

	public String getFormularlyUrl() {
		return formularlyUrl;
	}

	public void setFormularlyUrl(String formularlyUrl) {
		this.formularlyUrl = formularlyUrl;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		// set default value as NO
		if(null == enableForOffExchFlow) {
		   enableForOffExchFlow = "No";
		}
		
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	/*public String getMaxEnrolee() {
		return maxEnrolee;
	}

	public void setMaxEnrolee(String maxEnrolee) {
		this.maxEnrolee = maxEnrolee;
	}*/

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer updatedby) {
		this.lastUpdatedBy = updatedby;
	}

	public String getCertifiedby() {
		return certifiedby;
	}

	public void setCertifiedby(String certifiedby) {
		this.certifiedby = certifiedby;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public PlanHealth getPlanHealth() {
		return planHealth;
	}

	public void setPlanHealth(PlanHealth planHealth) {
		this.planHealth = planHealth;
	}

	public PlanDental getPlanDental() {
		return planDental;
	}

	public void setPlanDental(PlanDental planDental) {
		this.planDental = planDental;
	}

	public PlanSTM getPlanStm() {
		return planStm;
	}

	public void setPlanStm(PlanSTM planStm) {
		this.planStm = planStm;
	}
	
	public PlanAME getPlanAme() {
		return planAme;
	}

	public void setPlanAme(PlanAME planAme) {
		this.planAme = planAme;
	}
	
	public PlanVision getPlanVision() {
		return planVision;
	}

	public void setPlanVision(PlanVision planVision) {
		this.planVision = planVision;
	}
	
	public PlanMedicare getPlanMedicare() {
		return planMedicare;
	}

	public void setPlanMedicare(PlanMedicare planMedicare) {
		this.planMedicare = planMedicare;
	}
	
	public PlanLife getPlanLife() {
		return planLife;
	}

	public void setPlanLife(PlanLife planLife) {
		this.planLife = planLife;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public String getBrochure() {
		return brochure;
	}

	public void setBrochure(String brochure) {
		this.brochure = brochure;
	}

	public String getIssuerVerificationStatus() {
		return issuerVerificationStatus;
	}

	public void setIssuerVerificationStatus(String issuerVerificationStatus) {
		this.issuerVerificationStatus = issuerVerificationStatus;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public Plan clone() throws CloneNotSupportedException {
		Plan plan = new Plan();
		plan.setInsuranceType(getInsuranceType());
		plan.setMarket(getMarket());
		plan.setIssuerPlanNumber(getIssuerPlanNumber());
		/*	Commenting for SERFF changes
		plan.setMaxEnrolee(getMaxEnrolee());*/
		plan.setName(getName());
		plan.setNetworkType(getNetworkType());
		plan.setStartDate(getStartDate());
		plan.setStatus(getStatus());
		plan.setEndDate(getEndDate());
		/*	Commenting for SERFF changes
		plan.setCategory(getCategory());*/
		plan.setIssuer(getIssuer());
		plan.setNetwork(getNetwork());
		plan.setStatus(getStatus());
		plan.setCreatedBy(getCreatedBy());
		plan.setLastUpdatedBy(getLastUpdatedBy());
		plan.setCertifiedby(getCertifiedby());
		plan.setCertifiedOn(getCertifiedOn());
		plan.setBrochure(getBrochure());
		plan.setHsa(getHsa());
		plan.setEnrollmentAvail(getEnrollmentAvail());
		plan.setEnrollmentAvailEffDate(getEnrollmentAvailEffDate());
		plan.setIssuerVerificationStatus(getIssuerVerificationStatus());
		plan.setFormularlyId(getFormularlyId());
		plan.setFormularlyUrl(getFormularlyUrl());
		plan.setVerified(getVerified());
		plan.setServiceAreaId(getServiceAreaId());
		plan.setIsDeleted(getIsDeleted());
		//setting remaining fields
		plan.setSupportFile(getSupportFile());
		plan.setPlanHealth(getPlanHealth());
		plan.setPlanDental(getPlanDental());
		plan.setPlanStm(getPlanStm());
		plan.setPlanAme(getPlanAme());
		plan.setPlanVision(getPlanVision());
		plan.setPlanMedicare(getPlanMedicare());
		plan.setTenantPlan(getTenantPlan());
		plan.setProviderNetworkId(getProviderNetworkId());
		plan.setHiosProductId(getHiosProductId());
		plan.setHpid(getHpid());
		plan.setState(getState());
		plan.setPlanRate(getPlanRate());
		plan.setBrochureUCmId(getBrochureUCmId());
		plan.setBrochureDocName(getBrochureDocName());
		plan.setExchangeType(getExchangeType());
		plan.setDeCertificationEffDate(getDeCertificationEffDate());
		plan.setTpId(getTpId());
		plan.setEnrollmentEndDate(getEnrollmentEndDate());
		plan.setIsPUF(getIsPUF());
		plan.setAvailableFor(getAvailableFor());
		plan.setApplicableYear(getApplicableYear());
		plan.setBusinessRuleId(getBusinessRuleId());
		plan.setFutureErlAvailEffDate(getFutureErlAvailEffDate());
		plan.setUnifiedPlanRateChangeId(getUnifiedPlanRateChangeId());
		plan.setEocDocId(getEocDocId());
		plan.setEocDocName(getEocDocName());
		return plan;
	}

	public static Map<PlanStatus, String> getPlanStatusOptionsList() {
		Map<PlanStatus, String> enumMap = new EnumMap<PlanStatus, String>(
				PlanStatus.class);
		enumMap.put(PlanStatus.CERTIFIED, "Certified");
		enumMap.put(PlanStatus.DECERTIFIED, "De-certified");
		enumMap.put(PlanStatus.INCOMPLETE, "Incomplete");
		enumMap.put(PlanStatus.LOADED, "Loaded");
		return enumMap;
	}
	
	public Map<PlanStatus, String> getPlanStatuses() {
		return Plan.getPlanStatusOptionsList();
	}
	
	public static Map<PlanStatus, String> getPlanStatusOptionsList(String excludeStatus) {
		Map<PlanStatus, String> enumMap = new EnumMap<PlanStatus, String>(PlanStatus.class);
		if(!PlanStatus.CERTIFIED.toString().equals(excludeStatus)){
			enumMap.put(PlanStatus.CERTIFIED, "Certified");
		}
		if(!PlanStatus.DECERTIFIED.toString().equals(excludeStatus)){
			enumMap.put(PlanStatus.DECERTIFIED, "De-certified");
		}
		if(!PlanStatus.INCOMPLETE.toString().equals(excludeStatus)){
			enumMap.put(PlanStatus.INCOMPLETE, "Incomplete");
		}
		return enumMap;
	}

	public Map<PlanStatus, String> getPlanStatuses(String excludeStatus) {
		return Plan.getPlanStatusOptionsList(excludeStatus);
	}

	public static Map<PlanMarket, String> getMarketTypeOptionsList() {
		Map<PlanMarket, String> enumMap = new EnumMap<PlanMarket, String>(
				PlanMarket.class);
		enumMap.put(PlanMarket.SHOP, "SHOP");
		enumMap.put(PlanMarket.INDIVIDUAL, "Individual");
		return enumMap;
	}

	public Map<PlanMarket, String> getMarketList() {
		return Plan.getMarketTypeOptionsList();
	}
	
	public Map<PlanLevel, String> getLevelList() {
		return Plan.getPlanLevelOptionsList();
	}

	public static Map<PlanLevel, String> getPlanLevelOptionsList() {
		Map<PlanLevel, String> enumMap = new EnumMap<PlanLevel, String>(
				PlanLevel.class);
		enumMap.put(PlanLevel.BRONZE, "Bronze");
		enumMap.put(PlanLevel.EXPANDEDBRONZE, "Expanded Bronze");
		enumMap.put(PlanLevel.SILVER, "Silver");
		enumMap.put(PlanLevel.GOLD, "Gold");
		enumMap.put(PlanLevel.PLATINUM, "Platinum");
		enumMap.put(PlanLevel.CATASTROPHIC, "Catastrophic");
		return enumMap;
	}

	public String getHsa() {
		return hsa;
	}

	public void setHsa(String hsa) {
		this.hsa = hsa;
	}

	public String getEnrollmentAvail() {
		return enrollmentAvail;
	}

	public void setEnrollmentAvail(String enrollmentAvail) {
		this.enrollmentAvail = enrollmentAvail;
	}

	public Date getEnrollmentAvailEffDate() {
		return enrollmentAvailEffDate;
	}

	public void setEnrollmentAvailEffDate(Date enrollmentAvailEffDate) {
		this.enrollmentAvailEffDate = enrollmentAvailEffDate;
	}

	public Map<EnrollmentAvail, String> getEnrollmentAvailList() {
		return Plan.getEnrollmentAvailOptionsList();
	}

	public static Map<EnrollmentAvail, String> getEnrollmentAvailOptionsList() {
		Map<EnrollmentAvail, String> enumMap = new EnumMap<EnrollmentAvail, String>(
				EnrollmentAvail.class);
		enumMap.put(EnrollmentAvail.AVAILABLE, "Available");
		enumMap.put(EnrollmentAvail.DEPENDENTSONLY, "Dependents Only");
		enumMap.put(EnrollmentAvail.NOTAVAILABLE, "Not Available");
		return enumMap;
	}

	public String getSupportFile() {
		return supportFile;
	}

	public void setSupportFile(String supportFile) {
		this.supportFile = supportFile;
	}

	public Integer getProviderNetworkId() {
		return providerNetworkId;
	}

	public void setProviderNetworkId(Integer providerNetworkId) {
		this.providerNetworkId = providerNetworkId;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public Map<HSA, String> getHsaList() {
		Map<HSA, String> enumMap = new EnumMap<HSA, String>(HSA.class);
		enumMap.put(HSA.YES, "Yes");
		enumMap.put(HSA.NO, "No");
		return enumMap;
	}

	public Map<Verified, String> getVerifiedList() {
		Map<Verified, String> enumMap = new EnumMap<Verified, String>(
				Verified.class);
		enumMap.put(Verified.VERIFIED, "Yes");
		enumMap.put(Verified.PENDING, "No");
		return enumMap;
	}
	
	public static Map<IssuerVerificationStatus, String> getIssuerVerificationOptionsList() {
		Map<IssuerVerificationStatus, String> enumMap = new EnumMap<IssuerVerificationStatus, String>(
				IssuerVerificationStatus.class);
		enumMap.put(IssuerVerificationStatus.VERIFIED, "VERIFIED");
		enumMap.put(IssuerVerificationStatus.PENDING, "PENDING");
		return enumMap;
	}
	
	public Map<IssuerVerificationStatus, String> getIssuerVerificationList() {
		return Plan.getIssuerVerificationOptionsList();
	}
	
	public Map<EXCHANGE_TYPE, String> getExchangeTypeList() {
		Map<EXCHANGE_TYPE, String> enumMap = new EnumMap<EXCHANGE_TYPE, String>(
				EXCHANGE_TYPE.class);
		enumMap.put(EXCHANGE_TYPE.ON, "On");
		enumMap.put(EXCHANGE_TYPE.OFF, "Off");
		return enumMap;
	}
	
	public Map<Tenant, String> getTenantList() {
		Map<Tenant, String> enumMap = new EnumMap<Tenant, String>(
				Tenant.class);
		enumMap.put(Tenant.BENEFITBAY, "BenefitBay");
		enumMap.put(Tenant.GETINSURED, "GetInsured");
		return enumMap;
	}

	public String getHiosProductId() {
		return hiosProductId;
	}

	public void setHiosProductId(String hiosProductId) {
		this.hiosProductId = hiosProductId;
	}

	public String getHpid() {
		return hpid;
	}

	public void setHpid(String hpid) {
		this.hpid = hpid;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public Integer getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(Integer serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}
	
	public List<PlanRate> getPlanRate() {
		return planRate;
	}

	public void setPlanRate(List<PlanRate> planRate) {
		this.planRate = planRate;
	}

	public String getFutureEnrollmentAvail() {
		return futureEnrollmentAvail;
	}
	
	public void setFutureEnrollmentAvail(String futureEnrollmentAvail) {
		this.futureEnrollmentAvail = futureEnrollmentAvail;
	}
	
	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getBrochureUCmId() {
		return brochureUCmId;
	}

	public void setBrochureUCmId(String brochureUCmId) {
		this.brochureUCmId = brochureUCmId;
	}

	public String getBrochureDocName() {
		return brochureDocName;
	}

	public void setBrochureDocName(String brochureDocName) {
		this.brochureDocName = brochureDocName;
	}	
	
	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}			
	public List<TenantPlan> getTenantPlan() {
		return tenantPlan;
	}

	public void setTenantPlan(List<TenantPlan> tenantPlan) {
		this.tenantPlan = tenantPlan;
	}

	public Date getDeCertificationEffDate() {
		return deCertificationEffDate;
	}

	public void setDeCertificationEffDate(Date deCertificationEffDate) {
		this.deCertificationEffDate = deCertificationEffDate;
	}

	public Date getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Date enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}
	
	public String getTpId() {
		return tpId;
	}

	public void setTpId(String tpId) {
		this.tpId = tpId;
	}

	public IS_PUF getIsPUF() {
		return isPUF;
	}

	public void setIsPUF(IS_PUF isPUF) {
		this.isPUF = isPUF;
	}

	public AVAILABLE_FOR getAvailableFor() {
		return availableFor;
	}

	public void setAvailableFor(AVAILABLE_FOR availableFor) {
		this.availableFor = availableFor;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}
	
	public Integer getBusinessRuleId() {
		return businessRuleId;
	}

	public void setBusinessRuleId(Integer businessRuleId) {
		this.businessRuleId = businessRuleId;
	}

	public Date getFutureErlAvailEffDate() {
		return futureErlAvailEffDate;
	}

	public void setFutureErlAvailEffDate(Date futureErlAvailEffDate) {
		this.futureErlAvailEffDate = futureErlAvailEffDate;
	}

	public Integer getUnifiedPlanRateChangeId() {
		return unifiedPlanRateChangeId;
	}

	public void setUnifiedPlanRateChangeId(Integer unifiedPlanRateChangeId) {
		this.unifiedPlanRateChangeId = unifiedPlanRateChangeId;
	}

	public String getEocDocId() {
		return eocDocId;
	}

	public void setEocDocId(String eocDocId) {
		this.eocDocId = eocDocId;
	}

	public String getEocDocName() {
		return eocDocName;
	}

	public void setEocDocName(String eocDocName) {
		this.eocDocName = eocDocName;
	}

	public String getCompositPremiumAvailableIndicator() {
		return compositPremiumAvailableIndicator;
	}

	public void setCompositPremiumAvailableIndicator(
			String compositPremiumAvailableIndicator) {
		this.compositPremiumAvailableIndicator = compositPremiumAvailableIndicator;
	}

	public Float getEhbPremiumFraction() {
		return ehbPremiumFraction;
	}

	public void setEhbPremiumFraction(Float ehbPremiumFraction) {
		this.ehbPremiumFraction = ehbPremiumFraction;
	}

	public String getEmpHSAHRAContributionIndicator() {
		return empHSAHRAContributionIndicator;
	}

	public void setEmpHSAHRAContributionIndicator(
			String empHSAHRAContributionIndicator) {
		this.empHSAHRAContributionIndicator = empHSAHRAContributionIndicator;
	}

	public Float getEmpHSAHRAContributionAmount() {
		return empHSAHRAContributionAmount;
	}

	public void setEmpHSAHRAContributionAmount(Float empHSAHRAContributionAmount) {
		this.empHSAHRAContributionAmount = empHSAHRAContributionAmount;
	}
	
	 public String getEnableForOffExchFlow() {
		 return enableForOffExchFlow;
	 }

	 public void setEnableForOffExchFlow(String enableForOffExchFlow) {
	     this.enableForOffExchFlow = enableForOffExchFlow;
	 }

	/**
	 * @return the terminateEnrollmentStatus
	 */
	public String getTerminateEnrollmentStatus() {
		return terminateEnrollmentStatus;
	}

	/**
	 * @param terminateEnrollmentStatus the terminateEnrollmentStatus to set
	 */
	public void setTerminateEnrollmentStatus(String terminateEnrollmentStatus) {
		this.terminateEnrollmentStatus = terminateEnrollmentStatus;
	}

	public String getNonCommissionFlag() {
		return nonCommissionFlag;
	}

	public void setNonCommissionFlag(String nonCommissionFlag) {
		this.nonCommissionFlag = nonCommissionFlag;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
		columnNames.add("issuerPlanNumber");
		columnNames.add("name");
		columnNames.add("market");
		columnNames.add("status");
		columnNames.add("enrollmentAvail");
		columnNames.add("futureEnrollmentAvail");
		columnNames.add("issuerVerificationStatus");
		columnNames.add("state");
		columnNames.add("networkType");
		columnNames.add("certifiedby");
		columnNames.add("startDate");
		columnNames.add("endDate");
		columnNames.add("enrollmentAvailEffDate");
		columnNames.add("futureErlAvailEffDate");
		columnNames.add("creationTimestamp");
		columnNames.add("lastUpdateTimestamp");
		columnNames.add("deCertificationEffDate");
		columnNames.add("enrollmentEndDate");
		columnNames.add("certifiedOn");
		columnNames.add("verified");
		columnNames.add("availableFor");
		columnNames.add("compositPremiumAvailableIndicator");
		columnNames.add("ehbPremiumFraction");
		columnNames.add("nonCommissionFlag");
		columnNames.add("exchangeType");
		columnNames.add("insuranceType");
		columnNames.add("planDental.planLevel");
		columnNames.add("planHealth.planLevel");
		columnNames.add("issuer.name");
	 	return columnNames;
	}
}
