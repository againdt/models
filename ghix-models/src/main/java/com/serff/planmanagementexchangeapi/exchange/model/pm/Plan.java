
package com.serff.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import com.serff.planmanagementexchangeapi.common.model.pm.SerffStatus;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocuments;


/**
 * <p>Java class for Plan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Plan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}standardComponentId"/>
 *         &lt;element name="planYear" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="dental" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="serffTrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stateTrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hiosProductId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="planName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="marketType" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}MarketType"/>
 *         &lt;element name="planLevel" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}PlanMetalLevel"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}serffStatus"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}serffStatusChangeDate"/>
 *         &lt;element name="statePlanStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statePlanStatusChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="planDispositionStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="planDispositionStatusChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="certificationStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="certificationStatusChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}exchangeWorkflowStatus"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}exchangeWorkflowStatusChangeDate"/>
 *         &lt;element name="stateAbbreviation" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}StateAbbreviation"/>
 *         &lt;element name="dateSubmitted" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}insurer"/>
 *         &lt;element name="dataTemplates" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}TransferDataTemplates"/>
 *         &lt;element name="supportingDocuments" type="{http://www.serff.com/planManagementExchangeApi/common/model/pm}SupportingDocuments"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "standardComponentId",
    "planYear",
    "dental",
    "serffTrackingNumber",
    "stateTrackingNumber",
    "hiosProductId",
    "planName",
    "marketType",
    "planLevel",
    "serffStatus",
    "serffStatusChangeDate",
    "statePlanStatus",
    "statePlanStatusChangeDate",
    "planDispositionStatus",
    "planDispositionStatusChangeDate",
    "certificationStatus",
    "certificationStatusChangeDate",
    "exchangeWorkflowStatus",
    "exchangeWorkflowStatusChangeDate",
    "stateAbbreviation",
    "dateSubmitted",
    "insurer",
    "dataTemplates",
    "supportingDocuments"
})
@XmlRootElement(name="plan")
public class Plan {

    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    protected String standardComponentId;
    protected short planYear;
    protected boolean dental;
    @XmlElement(required = true)
    protected String serffTrackingNumber;
    @XmlElement(required = true, nillable = true)
    protected String stateTrackingNumber;
    @XmlElement(required = true, nillable = true)
    protected String hiosProductId;
    @XmlElement(required = true, nillable = true)
    protected String planName;
    @XmlElement(required = true)
    protected MarketType marketType;
    @XmlElement(required = true, nillable = true)
    protected PlanMetalLevel planLevel;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    protected SerffStatus serffStatus;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serffStatusChangeDate;
    @XmlElement(required = true, nillable = true)
    protected String statePlanStatus;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statePlanStatusChangeDate;
    @XmlElement(required = true, nillable = true)
    protected String planDispositionStatus;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar planDispositionStatusChangeDate;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean certificationStatus;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar certificationStatusChangeDate;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true, nillable = true)
    protected String exchangeWorkflowStatus;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exchangeWorkflowStatusChangeDate;
    @XmlElement(required = true)
    protected String stateAbbreviation;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateSubmitted;
    @XmlElement(required = true)
    protected Insurer insurer;
    @XmlElement(required = true)
    protected TransferDataTemplates dataTemplates;
    @XmlElement(required = true)
    protected SupportingDocuments supportingDocuments;

    /**
     * Gets the value of the standardComponentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardComponentId() {
        return standardComponentId;
    }

    /**
     * Sets the value of the standardComponentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardComponentId(String value) {
        this.standardComponentId = value;
    }

    /**
     * Gets the value of the planYear property.
     * 
     */
    public short getPlanYear() {
        return planYear;
    }

    /**
     * Sets the value of the planYear property.
     * 
     */
    public void setPlanYear(short value) {
        this.planYear = value;
    }

    /**
     * Gets the value of the dental property.
     * 
     */
    public boolean isDental() {
        return dental;
    }

    /**
     * Sets the value of the dental property.
     * 
     */
    public void setDental(boolean value) {
        this.dental = value;
    }

    /**
     * Gets the value of the serffTrackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerffTrackingNumber() {
        return serffTrackingNumber;
    }

    /**
     * Sets the value of the serffTrackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerffTrackingNumber(String value) {
        this.serffTrackingNumber = value;
    }

    /**
     * Gets the value of the stateTrackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateTrackingNumber() {
        return stateTrackingNumber;
    }

    /**
     * Sets the value of the stateTrackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateTrackingNumber(String value) {
        this.stateTrackingNumber = value;
    }

    /**
     * Gets the value of the hiosProductId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiosProductId() {
        return hiosProductId;
    }

    /**
     * Sets the value of the hiosProductId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiosProductId(String value) {
        this.hiosProductId = value;
    }

    /**
     * Gets the value of the planName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanName() {
        return planName;
    }

    /**
     * Sets the value of the planName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanName(String value) {
        this.planName = value;
    }

    /**
     * Gets the value of the marketType property.
     * 
     * @return
     *     possible object is
     *     {@link MarketType }
     *     
     */
    public MarketType getMarketType() {
        return marketType;
    }

    /**
     * Sets the value of the marketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketType }
     *     
     */
    public void setMarketType(MarketType value) {
        this.marketType = value;
    }

    /**
     * Gets the value of the planLevel property.
     * 
     * @return
     *     possible object is
     *     {@link PlanMetalLevel }
     *     
     */
    public PlanMetalLevel getPlanLevel() {
        return planLevel;
    }

    /**
     * Sets the value of the planLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanMetalLevel }
     *     
     */
    public void setPlanLevel(PlanMetalLevel value) {
        this.planLevel = value;
    }

    /**
     * Gets the value of the serffStatus property.
     * 
     * @return
     *     possible object is
     *     {@link SerffStatus }
     *     
     */
    public SerffStatus getSerffStatus() {
        return serffStatus;
    }

    /**
     * Sets the value of the serffStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link SerffStatus }
     *     
     */
    public void setSerffStatus(SerffStatus value) {
        this.serffStatus = value;
    }

    /**
     * Gets the value of the serffStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSerffStatusChangeDate() {
        return serffStatusChangeDate;
    }

    /**
     * Sets the value of the serffStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSerffStatusChangeDate(XMLGregorianCalendar value) {
        this.serffStatusChangeDate = value;
    }

    /**
     * Gets the value of the statePlanStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatePlanStatus() {
        return statePlanStatus;
    }

    /**
     * Sets the value of the statePlanStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatePlanStatus(String value) {
        this.statePlanStatus = value;
    }

    /**
     * Gets the value of the statePlanStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatePlanStatusChangeDate() {
        return statePlanStatusChangeDate;
    }

    /**
     * Sets the value of the statePlanStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatePlanStatusChangeDate(XMLGregorianCalendar value) {
        this.statePlanStatusChangeDate = value;
    }

    /**
     * Gets the value of the planDispositionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanDispositionStatus() {
        return planDispositionStatus;
    }

    /**
     * Sets the value of the planDispositionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanDispositionStatus(String value) {
        this.planDispositionStatus = value;
    }

    /**
     * Gets the value of the planDispositionStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlanDispositionStatusChangeDate() {
        return planDispositionStatusChangeDate;
    }

    /**
     * Sets the value of the planDispositionStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlanDispositionStatusChangeDate(XMLGregorianCalendar value) {
        this.planDispositionStatusChangeDate = value;
    }

    /**
     * Gets the value of the certificationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCertificationStatus() {
        return certificationStatus;
    }

    /**
     * Sets the value of the certificationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCertificationStatus(Boolean value) {
        this.certificationStatus = value;
    }

    /**
     * Gets the value of the certificationStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertificationStatusChangeDate() {
        return certificationStatusChangeDate;
    }

    /**
     * Sets the value of the certificationStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertificationStatusChangeDate(XMLGregorianCalendar value) {
        this.certificationStatusChangeDate = value;
    }

    /**
     * Gets the value of the exchangeWorkflowStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeWorkflowStatus() {
        return exchangeWorkflowStatus;
    }

    /**
     * Sets the value of the exchangeWorkflowStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeWorkflowStatus(String value) {
        this.exchangeWorkflowStatus = value;
    }

    /**
     * Gets the value of the exchangeWorkflowStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExchangeWorkflowStatusChangeDate() {
        return exchangeWorkflowStatusChangeDate;
    }

    /**
     * Sets the value of the exchangeWorkflowStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExchangeWorkflowStatusChangeDate(XMLGregorianCalendar value) {
        this.exchangeWorkflowStatusChangeDate = value;
    }

    /**
     * Gets the value of the stateAbbreviation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateAbbreviation() {
        return stateAbbreviation;
    }

    /**
     * Sets the value of the stateAbbreviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateAbbreviation(String value) {
        this.stateAbbreviation = value;
    }

    /**
     * Gets the value of the dateSubmitted property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateSubmitted() {
        return dateSubmitted;
    }

    /**
     * Sets the value of the dateSubmitted property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateSubmitted(XMLGregorianCalendar value) {
        this.dateSubmitted = value;
    }

    /**
     * Gets the value of the insurer property.
     * 
     * @return
     *     possible object is
     *     {@link Insurer }
     *     
     */
    public Insurer getInsurer() {
        return insurer;
    }

    /**
     * Sets the value of the insurer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Insurer }
     *     
     */
    public void setInsurer(Insurer value) {
        this.insurer = value;
    }

    /**
     * Gets the value of the dataTemplates property.
     * 
     * @return
     *     possible object is
     *     {@link TransferDataTemplates }
     *     
     */
    public TransferDataTemplates getDataTemplates() {
        return dataTemplates;
    }

    /**
     * Sets the value of the dataTemplates property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferDataTemplates }
     *     
     */
    public void setDataTemplates(TransferDataTemplates value) {
        this.dataTemplates = value;
    }

    /**
     * Gets the value of the supportingDocuments property.
     * 
     * @return
     *     possible object is
     *     {@link SupportingDocuments }
     *     
     */
    public SupportingDocuments getSupportingDocuments() {
        return supportingDocuments;
    }

    /**
     * Sets the value of the supportingDocuments property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupportingDocuments }
     *     
     */
    public void setSupportingDocuments(SupportingDocuments value) {
        this.supportingDocuments = value;
    }

}
