package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrollmentOffExchangeEmailDTO implements Serializable{
	
	private String recipient;
	private String recipientName;
	private String planType;
	private String officeVisit;
	private String genericMedication;
	private String deductible;
	private String maxOop;
	private String netPremiumAmount;
	private String planName;
	private String link;
	private String insuranceCompany;
	private String insuranceType;
	private String companyLogo;
	private String coInsurance;
	private String planDuration;
	private String phoneNum;
	private String emailFrom;
	private String logoUrl;
	private String redirectUrl;
	private String companyName;
	private String disclaimerContent;
	private String callCenterHours;
	
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public String getGenericMedication() {
		return genericMedication;
	}
	public void setGenericMedication(String genericMedication) {
		this.genericMedication = genericMedication;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getMaxOop() {
		return maxOop;
	}
	public void setMaxOop(String maxOop) {
		this.maxOop = maxOop;
	}
	public String getNetPremiumAmount() {
		return netPremiumAmount;
	}
	public void setNetPremiumAmount(String netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getCompanyLogo() {
		return companyLogo;
	}
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
	public String getCoInsurance() {
		return coInsurance;
	}
	public void setCoInsurance(String coInsurance) {
		this.coInsurance = coInsurance;
	}
	public String getPlanDuration() {
		return planDuration;
	}
	public void setPlanDuration(String planDuration) {
		this.planDuration = planDuration;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	/**
	 * @return the disclaimerContent
	 */
	public String getDisclaimerContent() {
		return disclaimerContent;
	}
	/**
	 * @param disclaimerContent the disclaimerContent to set
	 */
	public void setDisclaimerContent(String disclaimerContent) {
		this.disclaimerContent = disclaimerContent;
	}
	
	/**
	 * @return the callCenterHours
	 */
	public String getCallCenterHours() {
		return callCenterHours;
	}
	/**
	 * @param callCenterHours the callCenterHours to set
	 */
	public void setCallCenterHours(String callCenterHours) {
		this.callCenterHours = callCenterHours;
	}
	@Override
	public String toString() {
		return "EnrollmentOffExchangeEmailDTO [recipient=" + recipient
				+ ", recipientName=" + recipientName + ", planType=" + planType
				+ ", officeVisit=" + officeVisit + ", genericMedication="
				+ genericMedication + ", deductible=" + deductible
				+ ", maxOop=" + maxOop + ", netPremiumAmount="
				+ netPremiumAmount + ", planName=" + planName + ", link="
				+ link + ", insuranceCompany=" + insuranceCompany
				+ ", insuranceType=" + insuranceType + ", companyLogo="
				+ companyLogo + ", coInsurance=" + coInsurance
				+ ", planDuration=" + planDuration + "]";
	}

}
