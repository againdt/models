package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class EnrollmentExtDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Float premium;
	private Float aptc;
	private String benefitEffectiveDate;
	private String issuerName;
	private String planName;
	private String planCsLevel;//CSR Level Like 01, 02,03
	private String planLevel;//PlanLevel like PLATINUM, GOLD, SILVER, BRONZE
	private String hiosPlanId;
	private String capAgentId;
	private String exchangeAssignPolicyNo;
	private String issuerAssignPolicyNo;
	private String ssapApplicationId;
	
	private String submittedToCarrierDate;
	private String householeCaseId;
	
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String stateCode;
	private int zip;
	private String countyCode;
	private String ratingArea;
	
	private String ffmUserName;
	private String aorFirstName;
	private String aorLastName;
	private String aorNpn;
	private String orgName;
	private String insuranceTypeLabel;
	
	private Integer planId;
	private Integer issuerId;
	
	private List<EnrolleeExtDTO> members;

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}

	public Float getAptc() {
		return aptc;
	}

	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}

	public String getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(String benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanCsLevel() {
		return planCsLevel;
	}

	public void setPlanCsLevel(String planCsLevel) {
		this.planCsLevel = planCsLevel;
	}

	public String getHiosPlanId() {
		return hiosPlanId;
	}

	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}

	public String getCapAgentId() {
		return capAgentId;
	}

	public void setCapAgentId(String capAgentId) {
		this.capAgentId = capAgentId;
	}

	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}

	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}

	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}

	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}

	public String getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(String ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getSubmittedToCarrierDate() {
		return submittedToCarrierDate;
	}

	public void setSubmittedToCarrierDate(String submittedToCarrierDate) {
		this.submittedToCarrierDate = submittedToCarrierDate;
	}

	public String getHouseholeCaseId() {
		return householeCaseId;
	}

	public void setHouseholeCaseId(String householeCaseId) {
		this.householeCaseId = householeCaseId;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public String getFfmUserName() {
		return ffmUserName;
	}

	public void setFfmUserName(String ffmUserName) {
		this.ffmUserName = ffmUserName;
	}

	public String getAorFirstName() {
		return aorFirstName;
	}

	public void setAorFirstName(String aorFirstName) {
		this.aorFirstName = aorFirstName;
	}

	public String getAorLastName() {
		return aorLastName;
	}

	public void setAorLastName(String aorLastName) {
		this.aorLastName = aorLastName;
	}

	public String getAorNpn() {
		return aorNpn;
	}

	public void setAorNpn(String aorNpn) {
		this.aorNpn = aorNpn;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public List<EnrolleeExtDTO> getMembers() {
		return members;
	}

	public void setMembers(List<EnrolleeExtDTO> members) {
		this.members = members;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getInsuranceTypeLabel() {
		return insuranceTypeLabel;
	}

	public void setInsuranceTypeLabel(String insuranceTypeLabel) {
		this.insuranceTypeLabel = insuranceTypeLabel;
	}
	
}
