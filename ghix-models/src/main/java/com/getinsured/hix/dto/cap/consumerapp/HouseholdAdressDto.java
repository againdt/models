package com.getinsured.hix.dto.cap.consumerapp;

import java.util.Map;

import com.getinsured.hix.model.consumer.Household.PREFERREDCONTACTTIME;
import com.getinsured.hix.model.leadpipeline.AgentLeadPipelineList;

public class HouseholdAdressDto {

	Integer householdId;
	Contact contact;
	LocationDto homeAdress;
	LocationDto mailingAdress;
	Phone phone;
	String emailAdress;
	String emailActionType;
	String encryptedId;
	AgentLeadPipelineList agentLeadPipelineList;
	Map<Integer,String> dispositionCodes;

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public LocationDto getHomeAdress() {
		return homeAdress;
	}

	public void setHomeAdress(LocationDto homeAdress) {
		this.homeAdress = homeAdress;
	}

	public LocationDto getMailingAdress() {
		return mailingAdress;
	}

	public void setMailingAdress(LocationDto mailingAdress) {
		this.mailingAdress = mailingAdress;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public String getEmailAdress() {
		return emailAdress;
	}

	public void setEmailAdress(String emailAdress) {
		this.emailAdress = emailAdress;
	}

	public class Contact {

		Name name;

		public Name getName() {
			return name;
		}

		public void setName(Name name) {
			this.name = name;
		}

		public	class Name {
			String first;
			String last;

			public String getLast() {
				return last;
			}

			public void setLast(String last) {
				this.last = last;
			}

			public String getFirst() {
				return first;
			}

			public void setFirst(String first) {
				this.first = first;
			}
		}
	}

	public class Phone {
		String home;
		String work;

		public String getHome() {
			return home;
		}

		public void setHome(String home) {
			this.home = home;
		}

		public String getWork() {
			return work;
		}

		public void setWork(String work) {
			this.work = work;
		}

		public String getOther() {
			return other;
		}

		public void setOther(String other) {
			this.other = other;
		}

		public String getBestNumber() {
			return bestNumber;
		}

		public void setBestNumber(String bestNumber) {
			this.bestNumber = bestNumber;
		}

		public PREFERREDCONTACTTIME getBestTime() {
			return bestTime;
		}

		public void setBestTime(PREFERREDCONTACTTIME preferredcontacttime) {
			this.bestTime = preferredcontacttime;
		}

		String other;
		String bestNumber;
		PREFERREDCONTACTTIME bestTime;
	}

	public String getEmailActionType() {
		return emailActionType;
	}

	public void setEmailActionType(String emailActionType) {
		this.emailActionType = emailActionType;
	}

	public String getEncryptedId() {
		return encryptedId;
	}

	public void setEncryptedId(String encryptedId) {
		this.encryptedId = encryptedId;
	}

	public AgentLeadPipelineList getAgentLeadPipelineList() {
		return agentLeadPipelineList;
	}

	public void setAgentLeadPipelineList(AgentLeadPipelineList agentLeadPipelineList) {
		this.agentLeadPipelineList = agentLeadPipelineList;
	}

	public Map<Integer, String> getDispositionCodes() {
		return dispositionCodes;
	}

	public void setDispositionCodes(Map<Integer, String> dispositionCodes) {
		this.dispositionCodes = dispositionCodes;
	}
}
