package com.getinsured.hix.dto.agency;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class AgencyDocumentDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long agencyId;
	private String documentName;
	private String createdDate;
	private String documentType;
	@NotNull
	private byte[] document;
	private String documentMime;
	private String status;
	private String originalFilename;
	private String documentId;
	
	public long getAgencyId() {
		return agencyId;
	}
	public void setAgencyId(long agencyId) {
		this.agencyId = agencyId;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public byte[] getDocument() {
		return document;
	}
	public void setDocument(byte[] document) {
		this.document = document;
	}
	public String getDocumentMime() {
		return documentMime;
	}
	public void setDocumentMime(String documentMime) {
		this.documentMime = documentMime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOriginalFilename() {
		return originalFilename;
	}
	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	
}
