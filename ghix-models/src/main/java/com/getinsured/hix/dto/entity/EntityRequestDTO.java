package com.getinsured.hix.dto.entity;

import java.io.Serializable;


/**
 * Implemented for entities like Assister / Agent / Enrollment Entity as a part
 * of IND-35
 */
public class EntityRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long recordId;
	private String recordType;

	private String firstName;
	private String middleName;
	private String lastName;
	private String businessLegalName;

	private Long entityIdNumber;
	private long functionalIdentifier;
	private String federalEIN;
	private String stateEIN;

	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zipCode;

	private String phoneNumber;
	private String email;

	private String agentLicenseNum;
	private String certiStatusCode;
	private String registrationStatusCd;
	private Long certificationNumber;
	private String certiStartDate;
	private String certiEndDate;
	private String statusDate;

	private String directDepositFlag;
	private String acctHolderName;
	private String bankRoutingNumber;
	private String bankAcctNumber;
	private String bankAcctType;

	private String recordIndicator;
	private String regiStartDate;
	private String regiEndDate;	
	private String orgType;
	private String entityType;	
	private String receivePayments;
	private String certiRenewalDate;
	private String status;
	private String changeType;
	
	public String getRegiStartDate() {
		return regiStartDate;
	}

	public void setRegiStartDate(String regiStartDate) {
		this.regiStartDate = regiStartDate;
	}

	public String getRegiEndDate() {
		return regiEndDate;
	}

	public void setRegiEndDate(String regiEndDate) {
		this.regiEndDate = regiEndDate;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getReceivePayments() {
		return receivePayments;
	}

	public void setReceivePayments(String receivePayments) {
		this.receivePayments = receivePayments;
	}

	public EntityRequestDTO() {
		
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public Long getEntityIdNumber() {
		return entityIdNumber;
	}

	public void setEntityIdNumber(Long entityIdNumber) {
		this.entityIdNumber = entityIdNumber;
	}

	public long getFunctionalIdentifier() {
		return functionalIdentifier;
	}

	public void setFunctionalIdentifier(long functionalIdentifier) {
		this.functionalIdentifier = functionalIdentifier;
	}

	public String getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(String federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getStateEIN() {
		return stateEIN;
	}

	public void setStateEIN(String stateEIN) {
		this.stateEIN = stateEIN;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAgentLicenseNum() {
		return agentLicenseNum;
	}

	public void setAgentLicenseNum(String agentLicenseNum) {
		this.agentLicenseNum = agentLicenseNum;
	}

	public String getCertiStatusCode() {
		return certiStatusCode;
	}

	public void setCertiStatusCode(String certiStatusCode) {
		this.certiStatusCode = certiStatusCode;
	}

	public String getRegistrationStatusCd() {
		return registrationStatusCd;
	}

	public void setRegistrationStatusCd(String registrationStatusCd) {
		this.registrationStatusCd = registrationStatusCd;
	}

	public Long getCertificationNumber() {
		return certificationNumber;
	}

	public void setCertificationNumber(Long certificationNumber) {
		this.certificationNumber = certificationNumber;
	}

	public String getCertiStartDate() {
		return certiStartDate;
	}

	public void setCertiStartDate(String certiStartDate) {
		this.certiStartDate = certiStartDate;
	}

	public String getCertiEndDate() {
		return certiEndDate;
	}

	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate;
	}

	public String getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}

	public String getDirectDepositFlag() {
		return directDepositFlag;
	}

	public void setDirectDepositFlag(String directDepositFlag) {
		this.directDepositFlag = directDepositFlag;
	}

	public String getAcctHolderName() {
		return acctHolderName;
	}

	public void setAcctHolderName(String acctHolderName) {
		this.acctHolderName = acctHolderName;
	}

	public String getBankRoutingNumber() {
		return bankRoutingNumber;
	}

	public void setBankRoutingNumber(String bankRoutingNumber) {
		this.bankRoutingNumber = bankRoutingNumber;
	}

	public String getBankAcctNumber() {
		return bankAcctNumber;
	}

	public void setBankAcctNumber(String bankAcctNumber) {
		this.bankAcctNumber = bankAcctNumber;
	}

	public String getBankAcctType() {
		return bankAcctType;
	}

	public void setBankAcctType(String bankAcctType) {
		this.bankAcctType = bankAcctType;
	}

	public String getRecordIndicator() {
		return recordIndicator;
	}

	public void setRecordIndicator(String recordIndicator) {
		this.recordIndicator = recordIndicator;
	}
	
	public String getCertiRenewalDate() {
		return certiRenewalDate;
	}

	public void setCertiRenewalDate(String certiRenewalDate) {
		this.certiRenewalDate = certiRenewalDate;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	@Override
	public String toString() {
		return "EntityRequestDTO details: RecordId = "+recordId+", EntityIdNumber = "+entityIdNumber+
				", EntityType = "+entityType+", StateEIN = "+stateEIN+", AgentLicenseNum = "+agentLicenseNum;
	}

}
