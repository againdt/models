package com.getinsured.hix.model.enrollment.enrollmentAhbxSync;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.AccountUser;

@Entity
@Table(name="ENRL_AHBX_SYNC")
public class EnrollmentAhbxSync implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static enum RecordType {
		IND20_INITIAL, IND20_UPDATE, IND21_UPDATE, 
		IND57_ADMINUPDATE, IND56_DISENROLLMENT, IND69_REINSTATE, 
		IND71_RENEWAL, IND71_AUTOSPECIAL, IND71G_CUSTOMGROUPING_SPECIAL, 
		EDIT_TOOL, APTC_CHANGE;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Enrl_ahbx_sync_seq")
	@SequenceGenerator(name = "Enrl_ahbx_sync_seq", sequenceName = "ENRL_AHBX_SYNC_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name = "EVENT_ID")
	private int eventId;
	
	@Column(name="HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId ;
	
	@Column(name="ENROLLMENT_JSON")
	private String enrollmentJson;
	
	@Column(name = "RECORD_TYPE")
	private String record_type;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@OneToOne
	@JoinColumn(name="created_by")
	private AccountUser createdBy;
	
	@Column(name="EXT_HOUSEHOLD_CASE_ID")
	private String externalHouseHoldCaseId;
	
	@Column(name = "ENROLLMENT_ID")
	private int enrollmentId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	public String getEnrollmentJson() {
		return enrollmentJson;
	}

	public void setEnrollmentJson(String enrollmentJson) {
		this.enrollmentJson = enrollmentJson;
	}

	public String getRecord_type() {
		return record_type;
	}

	public void setRecord_type(String record_type) {
		this.record_type = record_type;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public String getExternalHouseHoldCaseId() {
		return externalHouseHoldCaseId;
	}

	public void setExternalHouseHoldCaseId(String externalHouseHoldCaseId) {
		this.externalHouseHoldCaseId = externalHouseHoldCaseId;
	}

	public int getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(int enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	
}
