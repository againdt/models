package com.getinsured.hix.model.agency;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.DesignateBroker.Status;

@Entity
@IdClass(DesignateBrokerAudId.class)
@Table(name="DESIGNATE_BROKER_AUD")
public class DesignateBrokerAud implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Id
	@Column(name = "REV")
	private long rev;

	@Column(name = "REVTYPE")
	private int revType;
	
	@Column(name = "brokerid")
	private int brokerId;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;	

	@Column(name = "externalindividualid")
	private Integer externalIndividualId;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Column(name = "esign_by")
	private String esignBy;

	@Column(name = "esign_date", columnDefinition = "date")
	private Date esignDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getRev() {
		return rev;
	}

	public void setRev(long rev) {
		this.rev = rev;
	}

	public int getRevType() {
		return revType;
	}

	public void setRevType(int revType) {
		this.revType = revType;
	}

	public int getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(int brokerId) {
		this.brokerId = brokerId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getExternalIndividualId() {
		return externalIndividualId;
	}

	public void setExternalIndividualId(Integer externalIndividualId) {
		this.externalIndividualId = externalIndividualId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getEsignBy() {
		return esignBy;
	}

	public void setEsignBy(String esignBy) {
		this.esignBy = esignBy;
	}

	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}
	
}
