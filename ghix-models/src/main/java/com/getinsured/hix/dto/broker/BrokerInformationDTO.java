package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.Broker;

public class BrokerInformationDTO implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int brokerId;
	private Integer employerId;
	private Integer individualId;
	private String status;
	private String role;
	private Broker broker;
	private List<Integer> employerIds;

	public List<Integer> getEmployerIds() {
		return employerIds;
	}


	public void setEmployerIds(List<Integer> employerIds) {
		this.employerIds = employerIds;
	}


	public BrokerInformationDTO() {
	}

	
	public Broker getBroker() {
		return broker;
	}


	public void setBroker(Broker broker) {
		this.broker = broker;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(int brokerId) {
		this.brokerId = brokerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public Integer getEmployerId() {
		return this.employerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getIndividualId() {
		return individualId;
	}

	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}

	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "BrokerInformationDTO details: ID = "+id+", BrokerId = "+brokerId+", " +
				"EmployerId = "+employerId+", IndividualId = "+individualId+", Status = "+status;
	}
}