package com.getinsured.hix.dto.directenrollment;

import com.getinsured.hix.model.directenrollment.DirectEnrollmentECM;

public class DirectEnrollmentECMDTO {

	private String ecmId;
	private Long id;
	private String name;
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEcmId() {
		return ecmId;
	}

	public void setEcmId(String ecmId) {
		this.ecmId = ecmId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DirectEnrollmentECMDTO() {

	}

	public DirectEnrollmentECMDTO(DirectEnrollmentECM ecm) {
		if (ecm != null) {
			this.ecmId = ecm.getEcmId();
			this.name = ecm.getFileName();
			this.setId(ecm.getId());
			this.type = ecm.getType();
		}
	}

}
