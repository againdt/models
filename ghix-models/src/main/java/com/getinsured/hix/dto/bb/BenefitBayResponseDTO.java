package com.getinsured.hix.dto.bb;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * 
 * @author meher_a
 *
 */
public class BenefitBayResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	 private String acceptanceStatus;
	 private List<BenefitBayResponseDTO.AcceptanceStatusMessage> acceptanceStatusMessage;
	 
	 
	 
	 public String getAcceptanceStatus() {
		return acceptanceStatus;
	}



	public void setAcceptanceStatus(String acceptanceStatus) {
		this.acceptanceStatus = acceptanceStatus;
	}



	public List<BenefitBayResponseDTO.AcceptanceStatusMessage> getAcceptanceStatusMessage() {
		return acceptanceStatusMessage;
	}



	public void setAcceptanceStatusMessage(
			List<BenefitBayResponseDTO.AcceptanceStatusMessage> acceptanceStatusMessage) {
		this.acceptanceStatusMessage = acceptanceStatusMessage;
	}
	
	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return gson.toJson(this);
	}

	public static class AcceptanceStatusMessage {

	        protected BigInteger messageCode;
	        protected String messageText;


	        public BigInteger getMessageCode() {
	            return messageCode;
	        }


	        public void setMessageCode(BigInteger value) {
	            this.messageCode = value;
	        }


	        public String getMessageText() {
	            return messageText;
	        }


	        public void setMessageText(String value) {
	            this.messageText = value;
	        }
	        }
}
