package com.getinsured.hix.dto.cap.employer;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.emx.EmxEmployerDTO;

public class CapEmployerMapper {

	/**
	 * Get the EMXEmployerDto from CapEmployerDto
	 * @param capEmployerDto
	 * @return
	 */
	public EmxEmployerDTO getEMXEmployerDto(CapEmployerDto capEmployerDto) {
		EmxEmployerDTO ed = null;
		
		if(capEmployerDto != null) {
			ed = new EmxEmployerDTO();
			
			// Set the location
			Location location = new Location();
			location.setAddress1(capEmployerDto.getAddress());
			location.setZip(capEmployerDto.getZip());
			location.setCity(capEmployerDto.getCity());
			
			ed.setContactLocation(location);
			ed.setContactEmail(capEmployerDto.getEmail());
			ed.setContactFirstName(capEmployerDto.getContactFirstName());
			ed.setContactLastName(capEmployerDto.getContactLastName());
			ed.setContactNumber(capEmployerDto.getContactPhoneNumber());
			ed.setName(capEmployerDto.getCompanyName());
			
		}
		return ed;
	}

}
