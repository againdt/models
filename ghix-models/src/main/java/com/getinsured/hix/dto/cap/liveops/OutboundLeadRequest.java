package com.getinsured.hix.dto.cap.liveops;

public class OutboundLeadRequest {
	
	@Override
	public String toString() {
		return "{" + (phonenum != null ? "phonenum:" + phonenum  : "")
				+ (outboundBatchId != null ? ",outboundBatchId:" + outboundBatchId : "")  
		+ (timezone != null ? ",timezone:" + timezone : "") + "}" ;
	}
	String phonenum;
	String outboundBatchId;
	String timezone;
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getPhonenum() {
		return phonenum;
	}
	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}
	public String getOutboundBatchId() {
		return outboundBatchId;
	}
	public void setOutboundBatchId(String outboundBatchId) {
		this.outboundBatchId = outboundBatchId;
	}
	
		 
	
}
