package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Map;

/**
 * HIX-70722 EnrolleeTenantDTO for API
 * @author negi_s
 * @since 06/07/2015
 *
 */
public class EnrolleeTenantDTO implements Serializable{
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffix;
	private String subscriberFlag;
	private String exchgIndivIdentifier;
	private String taxIdNumber;
	private String dob;
	private Float totalIndvResponsibilityAmt;
	private String effectiveStartDate;
	private String effectiveEndDate;
	private String gender;
	private String tobaccoUsage;
	private String maritalStatus;
	private String primaryPhoneNo;
	private String secondaryPhoneNo;
	private String preferredSMS;
	private String preferredEmail;
	private String homeAddress1;
	private String homeAddress2;
	private String homeAddressCity;
	private String homeAddressState;
	private String homeAddressZip;
	private String homeAddressCountyCode;
	private String languageSpoken;
	private String languageWritten;
//	private List<EnrolleeRaceTenantDTO> enrolleeRace;
//	private List<EnrolleeRelationshipTenantDTO> enrolleeRelationship;
	private Map<String, String> enrolleeRace;
	private Map<String, String> enrolleeRelationship;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getSubscriberFlag() {
		return subscriberFlag;
	}
	public void setSubscriberFlag(String subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public String getTaxIdNumber() {
		return taxIdNumber;
	}
	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}
	public Float getTotalIndvResponsibilityAmt() {
		return totalIndvResponsibilityAmt;
	}
	public void setTotalIndvResponsibilityAmt(Float totalIndvResponsibilityAmt) {
		this.totalIndvResponsibilityAmt = totalIndvResponsibilityAmt;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTobaccoUsage() {
		return tobaccoUsage;
	}
	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getPrimaryPhoneNo() {
		return primaryPhoneNo;
	}
	public void setPrimaryPhoneNo(String primaryPhoneNo) {
		this.primaryPhoneNo = primaryPhoneNo;
	}
	public String getSecondaryPhoneNo() {
		return secondaryPhoneNo;
	}
	public void setSecondaryPhoneNo(String secondaryPhoneNo) {
		this.secondaryPhoneNo = secondaryPhoneNo;
	}
	public String getPreferredSMS() {
		return preferredSMS;
	}
	public void setPreferredSMS(String preferredSMS) {
		this.preferredSMS = preferredSMS;
	}
	public String getPreferredEmail() {
		return preferredEmail;
	}
	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}
	public String getHomeAddress1() {
		return homeAddress1;
	}
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}
	public String getHomeAddress2() {
		return homeAddress2;
	}
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}
	public String getHomeAddressCity() {
		return homeAddressCity;
	}
	public void setHomeAddressCity(String homeAddressCity) {
		this.homeAddressCity = homeAddressCity;
	}
	public String getHomeAddressState() {
		return homeAddressState;
	}
	public void setHomeAddressState(String homeAddressState) {
		this.homeAddressState = homeAddressState;
	}
	public String getHomeAddressZip() {
		return homeAddressZip;
	}
	public void setHomeAddressZip(String homeAddressZip) {
		this.homeAddressZip = homeAddressZip;
	}
	public String getHomeAddressCountyCode() {
		return homeAddressCountyCode;
	}
	public void setHomeAddressCountyCode(String homeAddressCountyCode) {
		this.homeAddressCountyCode = homeAddressCountyCode;
	}
	public String getLanguageSpoken() {
		return languageSpoken;
	}
	public void setLanguageSpoken(String languageSpoken) {
		this.languageSpoken = languageSpoken;
	}
	public String getLanguageWritten() {
		return languageWritten;
	}
	public void setLanguageWritten(String languageWritten) {
		this.languageWritten = languageWritten;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public Map<String, String> getEnrolleeRace() {
		return enrolleeRace;
	}
	public void setEnrolleeRace(Map<String, String> enrolleeRace) {
		this.enrolleeRace = enrolleeRace;
	}
	public Map<String, String> getEnrolleeRelationship() {
		return enrolleeRelationship;
	}
	public void setEnrolleeRelationship(Map<String, String> enrolleeRelationship) {
		this.enrolleeRelationship = enrolleeRelationship;
	}
	
	@Override
	public String toString() {
		return "EnrolleeTenantDTO [firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", suffix=" + suffix
				+ ", subscriberFlag=" + subscriberFlag
				+ ", exchgIndivIdentifier=" + exchgIndivIdentifier
				+ ", taxIdNumber=" + taxIdNumber + ", dob=" + dob
				+ ", totalIndvResponsibilityAmt=" + totalIndvResponsibilityAmt
				+ ", effectiveStartDate=" + effectiveStartDate
				+ ", effectiveEndDate=" + effectiveEndDate + ", gender="
				+ gender + ", tobaccoUsage=" + tobaccoUsage
				+ ", maritalStatus=" + maritalStatus + ", primaryPhoneNo="
				+ primaryPhoneNo + ", secondaryPhoneNo=" + secondaryPhoneNo
				+ ", preferredSMS=" + preferredSMS + ", preferredEmail="
				+ preferredEmail + ", homeAddress1=" + homeAddress1
				+ ", homeAddress2=" + homeAddress2 + ", homeAddressCity="
				+ homeAddressCity + ", homeAddressState=" + homeAddressState
				+ ", homeAddressZip=" + homeAddressZip
				+ ", homeAddressCountyCode=" + homeAddressCountyCode
				+ ", languageSpoken=" + languageSpoken + ", languageWritten="
				+ languageWritten + ", enrolleeRace=" + enrolleeRace
				+ ", enrolleeRelationship=" + enrolleeRelationship + "]";
	}
}
