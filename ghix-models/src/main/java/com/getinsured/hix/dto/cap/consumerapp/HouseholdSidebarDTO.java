package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class HouseholdSidebarDTO  implements Serializable {	
	private static final long serialVersionUID = -4557560569227277673L;
	
	private Integer openApps;
	private Integer enrollments;
	private String dateOfBirth; // CMR_HOUSEHOLD.BIRTH_DATE
	private String tobaccoUser;
	private String streetAddress1; // LOCATIONS.ADDRESS1
	private String streetAddress2; // LOCATIONS.ADDRESS2
	private String city; // LOCATIONS.CITY
	private String callBackOK; // CMR_HOUSEHOLD.CONSENT_TO_CALLBACK, NOT ELIG_LEAD.IS_OK_TO_CALL
	private String autoDialOK;
	private Integer affiliateId; // these 4 fields give data based on cmr_household, not phoneCalledTo 
								//		(which goes into the CallContext section and is in the previously
								//		sent AffiliateInfoDTO)
	private String  affiliateName;
	private Integer affiliateFlowId;
	private String affiliateFlowName;
	private String emailAddress;
	//
	
	public Integer getOpenApps() {
		return openApps;
	}
	public void setOpenApps(Integer openApps) {
		this.openApps = openApps;
	}
	public Integer getEnrollments() {
		return enrollments;
	}
	public void setEnrollments(Integer enrollments) {
		this.enrollments = enrollments;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getTobaccoUser() {
		return tobaccoUser;
	}
	public void setTobaccoUser(String tobaccoUser) {
		this.tobaccoUser = tobaccoUser;
	}
	public String getStreetAddress1() {
		return streetAddress1;
	}
	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}
	public String getStreetAddress2() {
		return streetAddress2;
	}
	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCallBackOK() {
		return callBackOK;
	}
	public void setCallBackOK(String callBackOK) {
		this.callBackOK = callBackOK;
	}
	public String getAutoDialOK() {
		return autoDialOK;
	}
	public void setAutoDialOK(String autoDialOK) {
		this.autoDialOK = autoDialOK;
	}
	public Integer getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(Integer affiliateId) {
		this.affiliateId = affiliateId;
	}
	public String getAffiliateName() {
		return affiliateName;
	}
	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}
	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}
	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}
	public String getAffiliateFlowName() {
		return affiliateFlowName;
	}
	public void setAffiliateFlowName(String affiliateFlowName) {
		this.affiliateFlowName = affiliateFlowName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	

}
