package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class EnrollmentSubscriberRequestDTO implements Serializable{
	public enum SortOrder{ASC, DESC};

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String sortByField;
	private SortOrder sortOrder;
	private Integer pageSize;
	private Integer pageNumber;
	private Integer enrollmentID;
	private String CMSPlanID;
	private String subscriberName;
	private String SSNLast4Digit;
	private String exchgSubscriberIdentifier;
	private String subscriberDateOfBirth;
	private String enrollmentStatus;
	private String coverageYear;
	private Integer issuerID;
	private String enrollmentTypeLookupCode;
	private String insuranceTypeLookupCode;
	
	public String getSortByField() {
		return sortByField;
	}
	public void setSortByField(String sortByField) {
		this.sortByField = sortByField;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getEnrollmentID() {
		return enrollmentID;
	}
	public void setEnrollmentID(Integer enrollmentID) {
		this.enrollmentID = enrollmentID;
	}
	public String getCMSPlanID() {
		return CMSPlanID;
	}
	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}
	public String getSubscriberName() {
		return subscriberName;
	}
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	public String getSSNLast4Digit() {
		return SSNLast4Digit;
	}
	public void setSSNLast4Digit(String sSNLast4Digit) {
		SSNLast4Digit = sSNLast4Digit;
	}
	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}
	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}
	public String getSubscriberDateOfBirth() {
		return subscriberDateOfBirth;
	}
	public void setSubscriberDateOfBirth(String subscriberDateOfBirth) {
		this.subscriberDateOfBirth = subscriberDateOfBirth;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}
	public Integer getIssuerID() {
		return issuerID;
	}
	public void setIssuerID(Integer issuerID) {
		this.issuerID = issuerID;
	}
	public String getEnrollmentTypeLookupCode() {
		return enrollmentTypeLookupCode;
	}
	public void setEnrollmentTypeLookupCode(String enrollmentTypeLookupCode) {
		this.enrollmentTypeLookupCode = enrollmentTypeLookupCode;
	}
	public String getInsuranceTypeLookupCode() {
		return insuranceTypeLookupCode;
	}
	public void setInsuranceTypeLookupCode(String insuranceTypeLookupCode) {
		this.insuranceTypeLookupCode = insuranceTypeLookupCode;
	}
}
