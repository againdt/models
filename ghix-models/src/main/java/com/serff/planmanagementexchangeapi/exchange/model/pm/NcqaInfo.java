
package com.serff.planmanagementexchangeapi.exchange.model.pm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for NcqaInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NcqaInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orgId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="issuerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="issuerState" type="{http://www.serff.com/planManagementExchangeApi/exchange/model/pm}StateAbbreviation"/>
 *         &lt;element name="naicCompanyCode" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *         &lt;element name="naicGroupCode" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *         &lt;element name="hiosIssuerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="marketType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accreditationStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accreditationSurveyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="dateCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orgId",
    "issuerName",
    "issuerState",
    "naicCompanyCode",
    "naicGroupCode",
    "hiosIssuerId",
    "marketType",
    "subId",
    "accreditationStatus",
    "accreditationSurveyType",
    "expirationDate",
    "dateCreated"
})
@XmlRootElement(name="ncqaInfo")
public class NcqaInfo {

    @XmlElement(required = true)
    protected String orgId;
    @XmlElement(required = true)
    protected String issuerName;
    @XmlElement(required = true)
    protected String issuerState;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger naicCompanyCode;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger naicGroupCode;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer hiosIssuerId;
    @XmlElement(required = true, nillable = true)
    protected String marketType;
    @XmlElement(required = true, nillable = true)
    protected String subId;
    @XmlElement(required = true, nillable = true)
    protected String accreditationStatus;
    @XmlElement(required = true, nillable = true)
    protected String accreditationSurveyType;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateCreated;

    /**
     * Gets the value of the orgId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * Sets the value of the orgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgId(String value) {
        this.orgId = value;
    }

    /**
     * Gets the value of the issuerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerName() {
        return issuerName;
    }

    /**
     * Sets the value of the issuerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerName(String value) {
        this.issuerName = value;
    }

    /**
     * Gets the value of the issuerState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerState() {
        return issuerState;
    }

    /**
     * Sets the value of the issuerState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerState(String value) {
        this.issuerState = value;
    }

    /**
     * Gets the value of the naicCompanyCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNaicCompanyCode() {
        return naicCompanyCode;
    }

    /**
     * Sets the value of the naicCompanyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNaicCompanyCode(BigInteger value) {
        this.naicCompanyCode = value;
    }

    /**
     * Gets the value of the naicGroupCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNaicGroupCode() {
        return naicGroupCode;
    }

    /**
     * Sets the value of the naicGroupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNaicGroupCode(BigInteger value) {
        this.naicGroupCode = value;
    }

    /**
     * Gets the value of the hiosIssuerId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHiosIssuerId() {
        return hiosIssuerId;
    }

    /**
     * Sets the value of the hiosIssuerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHiosIssuerId(Integer value) {
        this.hiosIssuerId = value;
    }

    /**
     * Gets the value of the marketType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketType() {
        return marketType;
    }

    /**
     * Sets the value of the marketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketType(String value) {
        this.marketType = value;
    }

    /**
     * Gets the value of the subId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubId() {
        return subId;
    }

    /**
     * Sets the value of the subId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubId(String value) {
        this.subId = value;
    }

    /**
     * Gets the value of the accreditationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccreditationStatus() {
        return accreditationStatus;
    }

    /**
     * Sets the value of the accreditationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccreditationStatus(String value) {
        this.accreditationStatus = value;
    }

    /**
     * Gets the value of the accreditationSurveyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccreditationSurveyType() {
        return accreditationSurveyType;
    }

    /**
     * Sets the value of the accreditationSurveyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccreditationSurveyType(String value) {
        this.accreditationSurveyType = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the dateCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateCreated() {
        return dateCreated;
    }

    /**
     * Sets the value of the dateCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateCreated(XMLGregorianCalendar value) {
        this.dateCreated = value;
    }

}
