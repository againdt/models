declare	cntr integer := 0;
begin
  select count(*) into cntr from user_tab_columns 
     where table_name=upper('admin_document') and column_name='CREATEDBY';
   if cntr > 0 then     
     UPDATE ADMIN_DOCUMENT set CREATEDBY = null;	
   end if;
end;