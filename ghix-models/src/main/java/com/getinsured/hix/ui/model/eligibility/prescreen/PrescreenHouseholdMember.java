package com.getinsured.hix.ui.model.eligibility.prescreen;


/**
 * This class encapsulates the household member information related to the
 * prescreen page.
 * 
 * <p>
 * Following are the specifics about the member variables.
 * <ol>
 * 
 * <li>memberDesc- This is the string value displayed as a tag on the UI for the
 * member on Household and income pages.</li>
 * 
 * <li>dateOfBirth - Date of birth of the member in the mm/dd/yyyy format.</li>
 * 
 * <li>isPregnant - Default is false.</li>
 * 
 * <li>isSeekingCoverage - Default is true.</li>
 * 
 * <li>relationshipWithClaimer - This field should be populated as per the
 * indications below. For the claimer - Populate the field with "self". For the
 * claimer's spouse - Populate the field with "spouse". For the claimer's
 * dependents - Populate the field with "child" or "relative" as selected on the
 * UI.</li>
 * 
 * <li>income - PrescreenIncome which derives its data from the 11 types of
 * incomes on the Income tab.</li>
 * 
 * </ol>
 * </p>
 * 
 * @author Sunil Desu
 * @since April 16 2013
 * 
 */
public class PrescreenHouseholdMember {

	private String memberDesc;
	private String dateOfBirth;
	private boolean isPregnant;
	private boolean isSeekingCoverage;
	private String relationshipWithClaimer;

	public String getmemberDesc() {
		return memberDesc;
	}

	public void setmemberDesc(String memberDesc) {
		this.memberDesc = memberDesc;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public boolean isPregnant() {
		return isPregnant;
	}

	public void setPregnant(boolean isPregnant) {
		this.isPregnant = isPregnant;
	}

	public boolean isSeekingCoverage() {
		return isSeekingCoverage;
	}

	public void setSeekingCoverage(boolean isSeekingCoverage) {
		this.isSeekingCoverage = isSeekingCoverage;
	}

	public String getRelationshipWithClaimer() {
		return relationshipWithClaimer;
	}

	public void setRelationshipWithClaimer(String relationshipWithClaimer) {
		this.relationshipWithClaimer = relationshipWithClaimer;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrescreenHouseholdMember [memberDesc=");
		builder.append(memberDesc);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", isPregnant=");
		builder.append(isPregnant);
		builder.append(", isSeekingCoverage=");
		builder.append(isSeekingCoverage);
		builder.append(", relationshipWithClaimer=");
		builder.append(relationshipWithClaimer);
		builder.append("]");
		return builder.toString();
	}
}
