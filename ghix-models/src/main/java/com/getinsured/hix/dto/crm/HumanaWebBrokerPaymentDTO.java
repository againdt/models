package com.getinsured.hix.dto.crm;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class HumanaWebBrokerPaymentDTO {
	
	@XmlElement(name="ContactEmail")
	private String contactEmailAddress;
	@XmlElement(name="Subscriber Identifier")
	private String subscriberIdentifer;
	//policy
	private String paymentTransactionId;
	/**
	 * fixme: this should be an enum
	 */
	@XmlElement(name="Exchange Type")
	private String exchangeType;
	@XmlElement(name="Market Indicator")
	private String marketIndicator;
	@XmlElement(name="Assigned QHP Identifier")
	private String assignedQHPIdentifier;
	@XmlElement(name="Total Amount Owed")
	private Float totalAmountOwed;
	@XmlElement(name="Premium Amount Total")
	private Float premiumAmountTotal;
	@XmlElement(name="APTC Amount")
	private Float aptcAmount;
	/**
	 * mm/dd/yyyy
	 * validation rule: length 10 char
	 */
	@XmlElement(name="Proposed Coverage Effective Date")
	private String coverageEffectiveDate;
	
	//contact identification
	@XmlElement(name="First Name")
	private String firstName;
	
	@XmlElement(name="Middle Name")
	private String middleName;
	
	@XmlElement(name="Last Name")
	private String lastName;
	
	@XmlElement(name="Suffix Name")
	private String suffix;
	
	@XmlElement(name="Partner Assigned Consumer ID")
	private String partnerAssignedConsumerId;
	
	@XmlElement(name="Street Name 1")
	private String street1;
	@XmlElement(name="Street Name 2")
	private String street2;
	@XmlElement(name="City Name")
	private String city;
	@XmlElement(name="State")
	private String state;
	@XmlElement(name="Zip Code")
	private String zipcode;
	
	/**
	 * Name of each enrollee whose coverage will be effectuated by the payment (format: “firstname1,lastname1,suffix1;firstname2,lastname2,suffix2...”). 
	 * @author root
	 *
	 */

	@XmlElement(name="Additional Information")
	private List<String> firstLastSuffixName = new ArrayList<String>();

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public String getSubscriberIdentifer() {
		return subscriberIdentifer;
	}

	public void setSubscriberIdentifer(String subscriberIdentifer) {
		this.subscriberIdentifer = subscriberIdentifer;
	}

	public String getPaymentTransactionId() {
		return paymentTransactionId;
	}

	public void setPaymentTransactionId(String paymentTransactionId) {
		this.paymentTransactionId = paymentTransactionId;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getAssignedQHPIdentifier() {
		return assignedQHPIdentifier;
	}

	public void setAssignedQHPIdentifier(String assignedQHPIdentifier) {
		this.assignedQHPIdentifier = assignedQHPIdentifier;
	}

	public Float getTotalAmountOwed() {
		return totalAmountOwed;
	}

	public void setTotalAmountOwed(Float totalAmountOwed) {
		this.totalAmountOwed = totalAmountOwed;
	}

	public Float getPremiumAmountTotal() {
		return premiumAmountTotal;
	}

	public void setPremiumAmountTotal(Float premiumAmountTotal) {
		this.premiumAmountTotal = premiumAmountTotal;
	}

	public Float getAptcAmount() {
		return aptcAmount;
	}

	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}

	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}

	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getPartnerAssignedConsumerId() {
		return partnerAssignedConsumerId;
	}

	public void setPartnerAssignedConsumerId(String partnerAssignedConsumerId) {
		this.partnerAssignedConsumerId = partnerAssignedConsumerId;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public List<String> getFirstLastSuffixName() {
		return firstLastSuffixName;
	}

	public void setFirstLastSuffixName(List<String> firstLastSuffixName) {
		this.firstLastSuffixName = firstLastSuffixName;
	}
	
	
}






