package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name="pm_zip_county_rating_area")
public class ZipCountyRatingArea implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_ZIP_COUNTY_RATING_AREA_SEQ")
	@SequenceGenerator(name = "PM_ZIP_COUNTY_RATING_AREA_SEQ", sequenceName = "PM_ZIP_COUNTY_RATING_AREA_SEQ", allocationSize = 1)
	private int id;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RATING_AREA_ID")
	private RatingArea ratingArea;
	
	@Column(name="COUNTY_FIPS")
	private String countyFips;
	
	public RatingArea getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(RatingArea ratingArea) {
		this.ratingArea = ratingArea;
	}

	@Column(name="ZIP")
	private String zip;

	@Column(name="COUNTY")
	private String county;
	
	
	@Column(name="applicable_year")
	private String applicableYear;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountyFips() {
		return countyFips;
	}

	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}
	
	public String getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(String applicableYear) {
		this.applicableYear = applicableYear;
	}

}
