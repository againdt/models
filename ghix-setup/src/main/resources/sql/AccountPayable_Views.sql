CREATE OR REPLACE 
VIEW ACCOUNT_PAYABLE(ID, ISSUER_ID, ISSUER_NAME, INVOICE_NUMBER, STATEMENT_DATE, TOTAL_AMOUNT_DUE, PAID_AMOUNT, BALANCE_AMOUNT)
AS
SELECT rownum, i.id, i.name, inv.invoice_number, inv.statement_date, inv.total_amount_due, pay.amount, cast((inv.total_amount_due - pay.amount) as float(126)) 
FROM issuer_invoices inv, issuer_payment_invoice pay, issuers i
WHERE inv.id = pay.invoice_id
AND i.id = inv.issuer_id
WITH READ ONLY;


CREATE OR REPLACE
VIEW ACCOUNT_RECEIVEABLE(ID, EMPLOYER_ID, EMPLOYER_NAME, INVOICE_NUMBER, STATEMENT_DATE, TOTAL_AMOUNT_DUE, PAID_AMOUNT, PENDING_AMOUNT)
AS
SELECT rownum, e.id, e.name, inv.invoice_number, inv.statement_date, inv.total_amount_due, pay.amount, cast((inv.total_amount_due - pay.amount) as float(126))
FROM employer_invoices inv, employer_payment_invoice pay, employers e
WHERE inv.id= pay.invoice_id
AND e.id = inv.employer_id
WITH READ ONLY;


CREATE OR REPLACE
VIEW QHP_ACCOUNT_RECEIVEABLE(EMPLOYER_ID, EMPLOYER_NAME, PLAN_TYPE, NET_AMOUNT, PAYMENT_RECEIVED, PENDING_AMOUNT)
AS
SELECT inv.employer_id,e.name,inv.plan_type, cast(sum(inv.net_amount) as float(126)), cast(sum(inv.payment_received) as float(126)),
cast((sum(inv.net_amount) - sum(inv.payment_received))as float(126))
FROM Employers e, employer_invoices_lineitems inv
WHERE e.id = inv.employer_id
GROUP BY inv.employer_id, inv.plan_type, e.name
ORDER BY inv.employer_id ASC
WITH READ ONLY;