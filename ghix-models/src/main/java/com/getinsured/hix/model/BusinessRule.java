package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "pm_business_rule")
public class BusinessRule {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_business_rule_seq")
	@SequenceGenerator(name = "pm_business_rule_seq", sequenceName = "pm_business_rule_seq", allocationSize = 1)
	private int id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "issuer_id")
	private Issuer issuer;

	@Column(name = "is_dpdnt_max_age")
	private String isDpdntMaxAge;

	@Column(name = "min_tobacco_months")
	private String minTobacoMonths;

	@Column(name = "domestic_partner_subsc")
	private String domesticPartnerSubsc;

	@Column(name = "same_sex_partner_subsc")
	private String sameSexPartnerSubsc;

	@Column(name = "max_children")
	private String maxChildren;

	@Column(name = "age_rule")
	private String ageRule;

	@Column(name = "enrollee_rate_rule")
	private String enrolleeRateRule;

	@Column(name = "single_parent_fmly_dpndt")
	private String singleParentFmlyDpndt;

	@Column(name = "two_parent_fmly_dpndt")
	private String twoParentFmlyDpndt;

	@Column(name = "spouse")
	private String spouse;

	@Column(name = "father_or_mother")
	private String fatherOrMother;

	@Column(name = "gfather_or_gmother")
	private String gfatherOrGmother;

	@Column(name = "gson_or_gdaughter")
	private String gSonOrGdaughter;

	@Column(name = "uncle_or_aunt")
	private String uncleOrAunt;

	@Column(name = "nephew_or_niece")
	private String nephewOrNiece;

	@Column(name = "cousin")
	private String cousin;

	@Column(name = "adopted_child")
	private String adoptedChild;

	@Column(name = "foster_child")
	private String fosterChild;

	@Column(name = "son_in_law_or_daughter_in_law")
	private String sonInLawOrDaughterInLaw;

	@Column(name = "bro_in_law_or_sis_in_law")
	private String broInLawOrSisInLaw;

	@Column(name = "father_in_law_or_mother_in_law")
	private String fatherInLawOrMotherInLaw;

	@Column(name = "brother_or_sister")
	private String brotherOrSister;

	@Column(name = "ward")
	private String ward;

	@Column(name = "step_parent")
	private String stepParent;

	@Column(name = "stepson_or_step_daughter")
	private String stepsonOrStepDaughter;

	@Column(name = "self")
	private String self;

	@Column(name = "child")
	private String child;

	@Column(name = "sponsored_dpdnt")
	private String sponsoredDpdnt;

	@Column(name = "dpndnt_on_minor_dpdnt")
	private String dpndntOnMinorDpdnt;

	@Column(name = "ex_spouse")
	private String exSpouse;

	@Column(name = "guardian")
	private String guardian;

	@Column(name = "court_app_guardian")
	private String courtAppGuardian;

	@Column(name = "collateral_dpdnt")
	private String colateralDpdnt;

	@Column(name = "life_partner")
	private String lifePartner;

	@Column(name = "annuitant")
	private String annuitant;

	@Column(name = "trustee")
	private String trustee;

	@Column(name = "other_relationship")
	private String otherRelationship;

	@Column(name = "other_relative")
	private String otherRelative;

	@Column(name = "rule_type")
	private Integer ruleType;

	@Column(name = "applies_to")
	private String appliesTo;

	@Column(name = "applicable_year")
	private Integer applicableYear;

	@Column(name = "is_obsolete")
	private String isObsolete ="N" ;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp")
	private Date createdTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_updated_timestamp")
	private Date lastUpdatedTimeStamp;

	@Column(name = "last_update_by")
	private Integer lastUpdateBy;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedTimestamp(new TSDate());
		this.setLastUpdatedTimeStamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdatedTimeStamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public String getIsDpdntMaxAge() {
		return isDpdntMaxAge;
	}

	public void setIsDpdntMaxAge(String isDpdntMaxAge) {
		this.isDpdntMaxAge = isDpdntMaxAge;
	}

	public String getMinTobacoMonths() {
		return minTobacoMonths;
	}

	public void setMinTobacoMonths(String minTobacoMonths) {
		this.minTobacoMonths = minTobacoMonths;
	}

	public String getDomesticPartnerSubsc() {
		return domesticPartnerSubsc;
	}

	public void setDomesticPartnerSubsc(String domesticPartnerSubsc) {
		this.domesticPartnerSubsc = domesticPartnerSubsc;
	}

	public String getSameSexPartnerSubsc() {
		return sameSexPartnerSubsc;
	}

	public void setSameSexPartnerSubsc(String sameSexPartnerSubsc) {
		this.sameSexPartnerSubsc = sameSexPartnerSubsc;
	}

	public String getMaxChildren() {
		return maxChildren;
	}

	public void setMaxChildren(String maxChildren) {
		this.maxChildren = maxChildren;
	}

	public String getAgeRule() {
		return ageRule;
	}

	public void setAgeRule(String ageRule) {
		this.ageRule = ageRule;
	}

	public String getEnrolleeRateRule() {
		return enrolleeRateRule;
	}

	public void setEnrolleeRateRule(String enrolleeRateRule) {
		this.enrolleeRateRule = enrolleeRateRule;
	}

	public String getSingleParentFmlyDpndt() {
		return singleParentFmlyDpndt;
	}

	public void setSingleParentFmlyDpndt(String singleParentFmlyDpndt) {
		this.singleParentFmlyDpndt = singleParentFmlyDpndt;
	}

	public String getTwoParentFmlyDpndt() {
		return twoParentFmlyDpndt;
	}

	public void setTwoParentFmlyDpndt(String twoParentFmlyDpndt) {
		this.twoParentFmlyDpndt = twoParentFmlyDpndt;
	}

	public String getSpouse() {
		return spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public String getFatherOrMother() {
		return fatherOrMother;
	}

	public void setFatherOrMother(String fatherOrMother) {
		this.fatherOrMother = fatherOrMother;
	}

	public String getGfatherOrGmother() {
		return gfatherOrGmother;
	}

	public void setGfatherOrGmother(String gfatherOrGmother) {
		this.gfatherOrGmother = gfatherOrGmother;
	}

	public String getgSonOrGdaughter() {
		return gSonOrGdaughter;
	}

	public void setgSonOrGdaughter(String gSonOrGdaughter) {
		this.gSonOrGdaughter = gSonOrGdaughter;
	}

	public String getUncleOrAunt() {
		return uncleOrAunt;
	}

	public void setUncleOrAunt(String uncleOrAunt) {
		this.uncleOrAunt = uncleOrAunt;
	}

	public String getNephewOrNiece() {
		return nephewOrNiece;
	}

	public void setNephewOrNiece(String nephewOrNiece) {
		this.nephewOrNiece = nephewOrNiece;
	}

	public String getCousin() {
		return cousin;
	}

	public void setCousin(String cousin) {
		this.cousin = cousin;
	}

	public String getAdoptedChild() {
		return adoptedChild;
	}

	public void setAdoptedChild(String adoptedChild) {
		this.adoptedChild = adoptedChild;
	}

	public String getFosterChild() {
		return fosterChild;
	}

	public void setFosterChild(String fosterChild) {
		this.fosterChild = fosterChild;
	}

	public String getSonInLawOrDaughterInLaw() {
		return sonInLawOrDaughterInLaw;
	}

	public void setSonInLawOrDaughterInLaw(String sonInLawOrDaughterInLaw) {
		this.sonInLawOrDaughterInLaw = sonInLawOrDaughterInLaw;
	}

	public String getBroInLawOrSisInLaw() {
		return broInLawOrSisInLaw;
	}

	public void setBroInLawOrSisInLaw(String broInLawOrSisInLaw) {
		this.broInLawOrSisInLaw = broInLawOrSisInLaw;
	}

	public String getFatherInLawOrMotherInLaw() {
		return fatherInLawOrMotherInLaw;
	}

	public void setFatherInLawOrMotherInLaw(String fatherInLawOrMotherInLaw) {
		this.fatherInLawOrMotherInLaw = fatherInLawOrMotherInLaw;
	}

	public String getBrotherOrSister() {
		return brotherOrSister;
	}

	public void setBrotherOrSister(String brotherOrSister) {
		this.brotherOrSister = brotherOrSister;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getStepParent() {
		return stepParent;
	}

	public void setStepParent(String stepParent) {
		this.stepParent = stepParent;
	}

	public String getStepsonOrStepDaughter() {
		return stepsonOrStepDaughter;
	}

	public void setStepsonOrStepDaughter(String stepsonOrStepDaughter) {
		this.stepsonOrStepDaughter = stepsonOrStepDaughter;
	}

	public String getSelf() {
		return self;
	}

	public void setSelf(String self) {
		this.self = self;
	}

	public String getChild() {
		return child;
	}

	public void setChild(String child) {
		this.child = child;
	}

	public String getSponsoredDpdnt() {
		return sponsoredDpdnt;
	}

	public void setSponsoredDpdnt(String sponsoredDpdnt) {
		this.sponsoredDpdnt = sponsoredDpdnt;
	}

	public String getDpndntOnMinorDpdnt() {
		return dpndntOnMinorDpdnt;
	}

	public void setDpndntOnMinorDpdnt(String dpndntOnMinorDpdnt) {
		this.dpndntOnMinorDpdnt = dpndntOnMinorDpdnt;
	}

	public String getExSpouse() {
		return exSpouse;
	}

	public void setExSpouse(String exSpouse) {
		this.exSpouse = exSpouse;
	}

	public String getGuardian() {
		return guardian;
	}

	public void setGuardian(String guardian) {
		this.guardian = guardian;
	}

	public String getCourtAppGuardian() {
		return courtAppGuardian;
	}

	public void setCourtAppGuardian(String courtAppGuardian) {
		this.courtAppGuardian = courtAppGuardian;
	}

	public String getColateralDpdnt() {
		return colateralDpdnt;
	}

	public void setColateralDpdnt(String colateralDpdnt) {
		this.colateralDpdnt = colateralDpdnt;
	}

	public String getLifePartner() {
		return lifePartner;
	}

	public void setLifePartner(String lifePartner) {
		this.lifePartner = lifePartner;
	}

	public String getAnnuitant() {
		return annuitant;
	}

	public void setAnnuitant(String annuitant) {
		this.annuitant = annuitant;
	}

	public String getTrustee() {
		return trustee;
	}

	public void setTrustee(String trustee) {
		this.trustee = trustee;
	}

	public String getOtherRelationship() {
		return otherRelationship;
	}

	public void setOtherRelationship(String otherRelationship) {
		this.otherRelationship = otherRelationship;
	}

	public String getOtherRelative() {
		return otherRelative;
	}

	public void setOtherRelative(String otherRelative) {
		this.otherRelative = otherRelative;
	}

	public Integer getRuleType() {
		return ruleType;
	}

	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}

	public String getAppliesTo() {
		return appliesTo;
	}

	public void setAppliesTo(String appliesTo) {
		this.appliesTo = appliesTo;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getIsObsolete() {
		return isObsolete;
	}

	public void setIsObsolete(String isObsolete) {
		this.isObsolete = isObsolete;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Date getLastUpdatedTimeStamp() {
		return lastUpdatedTimeStamp;
	}

	public void setLastUpdatedTimeStamp(Date lastUpdatedTimeStamp) {
		this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
}
