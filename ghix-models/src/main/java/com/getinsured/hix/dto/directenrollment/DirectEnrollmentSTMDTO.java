package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;


/*
 * Stores all Short term medical related census data
 */

public class DirectEnrollmentSTMDTO implements Serializable{
	private String state;
	private String zip;
	private String county;
	private String countyFips;
	private String effectiveDate;
	private boolean childOnlyPolicy;
	private List<DirectEnrollmentMemberDetailsDTO> members;
	private String email;
	private String planName;
	private String planNetwork;
	private String deductible;
	private String coinsurance;
	private String planType;
	private String planId;
	private String planCost;
	private String stmEvent;
	private String issuerPlanId;
	private String primaryFirstName;
	private String primaryLastName;
	private String primaryContactPhone;
	
	public static final SimpleDateFormat YEAR_DATE_FORMAT = new SimpleDateFormat("yyyy");
	public static final SimpleDateFormat MONTH_DATE_YEAR_FORMAT_WITH_SLASHES = new SimpleDateFormat("MM/dd/yyyy");
	
	/**
	 * After Marc's suggestion
	 */
	private String year;
	private String brandName;
	private String issuerName;
	private boolean openEnrollment;
	
	
	
	public DirectEnrollmentSTMDTO(){
		
	}
	
	public static DirectEnrollmentSTMDTO fromCensus(DirectEnrollmentCensusDTO dto){
		DirectEnrollmentSTMDTO obj = new DirectEnrollmentSTMDTO();
		if(dto!=null){
			obj.state = dto.getState();
			obj.deductible = dto.getDeductible();
			obj.email = dto.getPrimaryContactEmail();
			obj.effectiveDate = MONTH_DATE_YEAR_FORMAT_WITH_SLASHES.format(dto.getEffectiveDate());
			obj.issuerPlanId = dto.getIssuerPlanId();
			obj.members = dto.getMemberData();
			obj.planCost = dto.getPlanCost();
			obj.planId = String.valueOf(dto.getPlanId());
			obj.planName = dto.getPlanName();
			obj.planNetwork = dto.getNetworkType();
			obj.planType = "STM";
			if(dto.getCounties()!=null && dto.getCounties().get(0)!=null){
				obj.county = dto.getCounties().get(0).getCountyName();
				obj.countyFips = dto.getCounties().get(0).getCountyName();
			}
			obj.stmEvent = dto.getStmEvent();
			obj.zip = dto.getZipcode();
			obj.primaryFirstName = dto.getPrimaryContactFirstName();
			obj.primaryLastName = dto.getPrimaryContactLastName();
			obj.primaryContactPhone = dto.getPrimaryContactPhone();
			obj.brandName = dto.getBrandName();
			obj.issuerName = dto.getIssuerName();
			obj.openEnrollment = dto.isOpenEnrollment();
			obj.year = YEAR_DATE_FORMAT.format(dto.getEffectiveDate());
			obj.coinsurance = dto.getCoinsurance();
			
		}
		
		return obj;
	}
	
	
	public String getPrimaryFirstName() {
		return primaryFirstName;
	}

	public void setPrimaryFirstName(String primaryFirstName) {
		this.primaryFirstName = primaryFirstName;
	}

	public String getPrimaryLastName() {
		return primaryLastName;
	}

	public void setPrimaryLastName(String primaryLastName) {
		this.primaryLastName = primaryLastName;
	}


	private static final long serialVersionUID = 1L;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public boolean isChildOnlyPolicy() {
		return childOnlyPolicy;
	}
	public void setChildOnlyPolicy(boolean childOnlyPolicy) {
		this.childOnlyPolicy = childOnlyPolicy;
	}
	
	public List<DirectEnrollmentMemberDetailsDTO> getMembers() {
		return members;
	}

	public void setMembers(List<DirectEnrollmentMemberDetailsDTO> members) {
		this.members = members;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanNetwork() {
		return planNetwork;
	}
	public void setPlanNetwork(String planNetwork) {
		this.planNetwork = planNetwork;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getCoinsurance() {
		return coinsurance;
	}
	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getPlanCost() {
		return planCost;
	}
	public void setPlanCost(String planCost) {
		this.planCost = planCost;
	}
	public String getStmEvent() {
		return stmEvent;
	}
	public void setStmEvent(String stmEvent) {
		this.stmEvent = stmEvent;
	}
	public String getIssuerPlanId() {
		return issuerPlanId;
	}
	public void setIssuerPlanId(String issuerPlanId) {
		this.issuerPlanId = issuerPlanId;
	}
	
	
	
}
