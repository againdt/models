package com.getinsured.hix.model.ui;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.Role;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
@Entity
@Table(name="MENU_ROLES")
public class MenuRole {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="menu_role_sequence")
	@SequenceGenerator(name="menu_role_sequence",sequenceName="MENU_ROLE_SEQ",allocationSize=1)
	private long id;
	
	@OneToOne
	@JoinColumn(name="MENU_ITEMS_ID")
	private MenuItem menuItem;
	
	@OneToOne
	@JoinColumn(name="ROLE_ID")
	private Role role;
	
	@Column(name="CREATION_TIMESTAMP")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date lastUpdatedTimestamp;
}
