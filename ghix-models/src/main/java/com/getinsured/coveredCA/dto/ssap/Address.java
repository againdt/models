package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Address {
	@Expose
	private String postalCode;
	@Expose
	private String streetAddress1;
	@Expose
	private String state = "CA";
	@Expose
	private String city;
	@Expose
	private String aptSte;
	@Expose
	private String county;
	@Expose
	private String countyCode;

	


	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	


	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getAptSte() {
		return aptSte;
	}

	public void setAptSte(String aptSte) {
		this.aptSte = aptSte;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	
    
	
}
