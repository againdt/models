package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * Enrollment DTO to store policy amount information for Annual IRS processing
 * @author negi_s
 *
 */
public class EnrollmentAnnualIrsMonthlyAmountDTO implements Serializable{
	
	private Float monthlyPremiumAmt;
	private Float monthlyPremiumSLCSPAmt;
	private Float monthlyAdvancedPTCAmt;
	
	public Float getMonthlyPremiumAmt() {
		return monthlyPremiumAmt;
	}
	public void setMonthlyPremiumAmt(Float monthlyPremiumAmt) {
		this.monthlyPremiumAmt = monthlyPremiumAmt;
	}
	public Float getMonthlyPremiumSLCSPAmt() {
		return monthlyPremiumSLCSPAmt;
	}
	public void setMonthlyPremiumSLCSPAmt(Float monthlyPremiumSLCSPAmt) {
		this.monthlyPremiumSLCSPAmt = monthlyPremiumSLCSPAmt;
	}
	public Float getMonthlyAdvancedPTCAmt() {
		return monthlyAdvancedPTCAmt;
	}
	public void setMonthlyAdvancedPTCAmt(Float monthlyAdvancedPTCAmt) {
		this.monthlyAdvancedPTCAmt = monthlyAdvancedPTCAmt;
	}
}
