DELETE FROM LOOKUP_VALUE WHERE LOOKUP_TYPE_ID=(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='LANGUAGE');
DELETE FROM LOOKUP_TYPE WHERE NAME='LANGUAGE';

INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'LANGUAGE_SPOKEN', 'string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'eng','English','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'ara','Arabic','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'hye','Armenian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'fas','Farsi','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'khmr','Cambodian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'cesm','Cantonese','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'cmn','Mandarin','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'hmn','Hmong','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'kor','Korean','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'rus','Russian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'spa','Spanish','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'tgl','Tagalog','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'vie','Vietnamese','');


INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'LANGUAGE_WRITTEN ', 'string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'eng','English','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'ara','Arabic','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'hye','Armenian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'fas','Farsi','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'khmr','Cambodian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'zho','Traditional Chinese character','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'hmn','Hmong','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'kor','Korean','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'rus','Russian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'spa','Spanish','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'tgl','Tagalog','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'vie','Vietnamese','');

DELETE FROM LOOKUP_VALUE WHERE LOOKUP_TYPE_ID=(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2182-4','Cuban','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2148-5','Mexican, Mexican American or Chicano/a','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2180-8','Puerto Rican','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'1002-5','American Indian or Alaskan Native','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2029-7','Asian Indian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2054-5','Black or African American','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2034-7','Chinese','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2036-2','Filipino','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2086-7','Guamanian or Chamorro','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2039-6','Japanese','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2040-4','Korean','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2079-2','Native Hawaiian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2028-9','Other Asian','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2500-7','Other Pacific Islander','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2080-0','Samoan','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2047-9','Vietnamese','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2106-3','White','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,(SELECT LOOKUP_TYPE_ID FROM LOOKUP_TYPE WHERE NAME='RACE'),'2131-1','Other',''); 