package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;

public class Name implements Serializable{
	private static final long serialVersionUID = 1L;
	private String first;
	private String last;
	private String middle;
	private String prefix;
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public String getMiddle() {
		return middle;
	}
	public void setMiddle(String middle) {
		this.middle = middle;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public Name(){
	}
	public Name(String first, String last){
		super();
		this.first = first;
		this.last = last;
	}
	
	public static Name getSampleInstance(){
		Name name = new Name();
		String[] firstName = {"John", "Ada", "Gracia", "Carolin", "Dora", "Annita", "Ula", "Rosemary", "Gracia"};
		String[] lastName = {"Dow", "Horsey", "Deel", "Gu", "Rolling", "Holdaway", "Siems", "Bulman", "Renshaw"};
		String[] middleName = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
		
		name.setFirst(firstName[(int)(9*Math.random())]);
		name.setLast(lastName[(int)(9*Math.random())]);
		name.setMiddle(middleName[(int)(9*Math.random())]);
		name.setPrefix("Jr.");
		
		return name;
	}
	@Override
	public String toString() {
		return "Name [first=" + first + ", last=" + last + ", middle=" + middle
				+ ", prefix=" + prefix + "]";
	}

	
}
