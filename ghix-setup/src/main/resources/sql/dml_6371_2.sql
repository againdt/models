INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('ADD_NEW_ISSUER')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('EDIT_PLAN_BENEFITS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('EDIT_PLAN_RATES')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('MANAGE_QHP_PLANS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('MANAGE_SADP_PLANS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_CERTIFICATION_STATUS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_ENROLLMENT_AVAILABILITY')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_PLAN_BENEFITS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_PLAN_DETAILS')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_PLAN_HISTORY')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_PLAN_RATES')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(select id from roles where name='ISSUER_ADMIN'), (select id from permissions   where name=UPPER('VIEW_PROVIDER_NETWORK_ID')));
