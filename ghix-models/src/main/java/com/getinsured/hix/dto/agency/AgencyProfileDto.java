package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

/**
 * Agency DTO.
 * 
 * 
 */
public class AgencyProfileDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String agencyBusinessLegalName;
	private String agencyName;
	private List<AgencyManagerProfileDto> agencyManagers;
	private int responseCode;
	private String responseDescription;
	
	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getAgencyBusinessLegalName() {
		return agencyBusinessLegalName;
	}

	public void setAgencyBusinessLegalName(String agencyBusinessLegalName) {
		this.agencyBusinessLegalName = agencyBusinessLegalName;
	}

	public AgencyProfileDto() {	
	}

	public List<AgencyManagerProfileDto> getAgencyManagers() {
		return agencyManagers;
	}

	public void setAgencyManagers(List<AgencyManagerProfileDto> agencyManagers) {
		this.agencyManagers = agencyManagers;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}


}
