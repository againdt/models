package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a household with all members in family
 * @author bhatt_s
 *
 */
@Deprecated
public class Family implements Serializable {
	private Household household;
	private List<Member> members = new ArrayList<Member>();
	
	public Family(){}
	
	public Family(Household household, List<Member> members){
		this.household = household;
		this.members = members;
	}
	
	public void addMember(Member member){
		members.add(member);
	}
	
	public Household getHousehold() {
		return household;
	}
	public void setHousehold(Household household) {
		this.household = household;
	}
	public List<Member> getMembers() {
		return members;
	}
	public void setMembers(List<Member> members) {
		this.members = members;
	}
	

}
