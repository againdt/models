
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataTemplate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataTemplateId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataTemplateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}requiredInsurerSuppliedData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataTemplateId",
    "dataTemplateType",
    "requiredInsurerSuppliedData"
})
@XmlRootElement(name="dataTemplate")
public class DataTemplate {

    @XmlElement(required = true)
    protected String dataTemplateId;
    @XmlElement(required = true)
    protected String dataTemplateType;
    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    @XmlMimeType("application/octet-stream")
    protected DataHandler requiredInsurerSuppliedData;

    /**
     * Gets the value of the dataTemplateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplateId() {
        return dataTemplateId;
    }

    /**
     * Sets the value of the dataTemplateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplateId(String value) {
        this.dataTemplateId = value;
    }

    /**
     * Gets the value of the dataTemplateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplateType() {
        return dataTemplateType;
    }

    /**
     * Sets the value of the dataTemplateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplateType(String value) {
        this.dataTemplateType = value;
    }

    /**
     * Gets the value of the requiredInsurerSuppliedData property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getRequiredInsurerSuppliedData() {
        return requiredInsurerSuppliedData;
    }

    /**
     * Sets the value of the requiredInsurerSuppliedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setRequiredInsurerSuppliedData(DataHandler value) {
        this.requiredInsurerSuppliedData = value;
    }

}
