package com.getinsured.hix.dto.entity;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
/**
 * Used to wrap data fetched from database in ghix-entity and send it to
 * ghix-web
 * 
 */
public class AssisterDesignateResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private int responseCode;
	private String responseDescription;
	private Assister assister;
	private DesignateAssister designateAssister;

	public DesignateAssister getDesignateAssister() {
		return designateAssister;
	}

	public void setDesignateAssister(DesignateAssister designateAssister) {
		this.designateAssister = designateAssister;
	}

	public Assister getAssister() {
		return assister;
	}

	public void setAssister(Assister assister) {
		this.assister = assister;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	
	@Override
	public String toString() {
		return "AssisterResponseDTO Details: [Assister = " + assister + "], responseCode = " + responseCode + ", responseDescription = " + responseDescription + ", [designateAssister = " + designateAssister + " ] ";
	}

}
