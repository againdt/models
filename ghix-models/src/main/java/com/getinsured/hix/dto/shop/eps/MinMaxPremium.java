package com.getinsured.hix.dto.shop.eps;

public class MinMaxPremium {

	private Float min_totalPremium = 0f;
	private Float max_totalPremium = 0f;
	private Float min_indvPremium = 0f;
	private Float max_indvPremium = 0f;
	private Float min_depePremium = 0f;
	private Float max_depePremium = 0f;
	
	public Float getMin_totalPremium() {
		return min_totalPremium;
	}
	public void setMin_totalPremium(Float min_totalPremium) {
		this.min_totalPremium = min_totalPremium;
	}
	public Float getMax_totalPremium() {
		return max_totalPremium;
	}
	public void setMax_totalPremium(Float max_totalPremium) {
		this.max_totalPremium = max_totalPremium;
	}
	public Float getMin_indvPremium() {
		return min_indvPremium;
	}
	public void setMin_indvPremium(Float min_indvPremium) {
		this.min_indvPremium = min_indvPremium;
	}
	public Float getMax_indvPremium() {
		return max_indvPremium;
	}
	public void setMax_indvPremium(Float max_indvPremium) {
		this.max_indvPremium = max_indvPremium;
	}
	public Float getMin_depePremium() {
		return min_depePremium;
	}
	public void setMin_depePremium(Float min_depePremium) {
		this.min_depePremium = min_depePremium;
	}
	public Float getMax_depePremium() {
		return max_depePremium;
	}
	public void setMax_depePremium(Float max_depePremium) {
		this.max_depePremium = max_depePremium;
	}
}
