package com.getinsured.hix.dto.plandisplay.planavailability;

import java.io.Serializable;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;

public class PdOrderItemPersonResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String memberGUID;
	private Float premium;
	private Integer region;
	private YorN subscriberFlag;
	private String coverageStartDate;
	
	public String getMemberGUID() {
		return memberGUID;
	}
	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
	
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	public Integer getRegion() {
		return region;
	}
	public void setRegion(Integer region) {
		this.region = region;
	}
	
	public YorN getSubscriberFlag() {
		return subscriberFlag;
	}
	public void setSubscriberFlag(YorN subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
			
}
