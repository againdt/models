package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class PlanTenantDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String planNumber;
	private long tenantId;
	
	
	
	public PlanTenantDTO(){
		
	}



	public int getPlanId() {
		return planId;
	}



	public void setPlanId(int planId) {
		this.planId = planId;
	}



	public String getPlanNumber() {
		return planNumber;
	}



	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}



	public long getTenantId() {
		return tenantId;
	}



	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}
	
	

}
