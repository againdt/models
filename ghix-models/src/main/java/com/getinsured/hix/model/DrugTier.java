package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.timeshift.TimeShifterUtil;

/**
 * 
 * Entity class for PM_DRUG_TIER table.
 * 
 * @author vardekar_s
 *
 */

@Entity
@Table(name = "PM_DRUG_TIER")
@DynamicInsert
@DynamicUpdate
public class DrugTier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_DRUG_TIER_SEQ")
	@SequenceGenerator(name = "PM_DRUG_TIER_SEQ", sequenceName = "PM_DRUG_TIER_SEQ", allocationSize = 1)
	private long id;	
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "DRUG_LIST_ID", referencedColumnName = "ID")
	private DrugList drugList;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "drugTier", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private  List<Drug> drugs;	
	
	@Column(name = "DRUG_TIER_LEVEL")
	private Integer drugTierLevel;
	
	@Column(name = "DRUG_TIER_TYPE1")
	private String drugTierType1;
	
	@Column(name = "DRUG_TIER_TYPE2")
	private String drugTierType2;
	
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@NotNull
	private Timestamp lastUpdateTimestamp;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis())); 
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DrugList getDrugList() {
		return drugList;
	}

	public void setDrugList(DrugList drugList) {
		this.drugList = drugList;
	}

	public Integer getDrugTierLevel() {
		return drugTierLevel;
	}

	public void setDrugTierLevel(Integer drugTierLevel) {
		this.drugTierLevel = drugTierLevel;
	}

	public String getDrugTierType1() {
		return drugTierType1;
	}

	public void setDrugTierType1(String drugTierType1) {
		this.drugTierType1 = drugTierType1;
	}

	public String getDrugTierType2() {
		return drugTierType2;
	}

	public void setDrugTierType2(String drugTierType2) {
		this.drugTierType2 = drugTierType2;
	}
		
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public List<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<Drug> drugs) {
		this.drugs = drugs;
	}
}
