package com.getinsured.hix.dto.plandisplay;

import org.codehaus.jettison.json.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

/**
*Simple representation of provider address which can be converted into JSON object
*/
public class ProviderAddress implements Comparable<ProviderAddress>, JSONAware{
	private String addline1;
	private String addLine2;
	private String[] phone;
	private String[] fax;
	private String city;
	private String state;
	private String zip;
	private Double lat = 0.0d;
	private Double lon = 0.0d;
	private float proximity;
	private String prac_location;

	public ProviderAddress(String addressData){
		this.addline1 = addressData;
	}

	public double getDistanceFromUserLocation(){
		return this.proximity;
	}

	public String getAddline1() {
		return addline1;
	}

	public void setAddline1(String addline1) {
		this.addline1 = addline1;
	}

	public String getAddLine2() {
		if("null".equals(addLine2)){
			this.addLine2="";
		}
		return addLine2;
	}

	public void setAddLine2(String addLine2) {
		this.addLine2 = addLine2;
	}

	public String[] getPhone() {
		return this.phone;
	}

	public void setPhone(String[] phone) {
		this.phone = phone;
	}

	public String[] getFax() {
		return fax;
	}

	public void setFax(String[] fax) {
		this.fax = fax;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public float getProximity() {
		return proximity;
	}

	public void setProximity(float proximity) {
		this.proximity = proximity;
	}

	public String getPrac_location() {
		return prac_location;
	}

	public void setPrac_location(String prac_location) {
		if(prac_location != null){
			this.prac_location = prac_location;
			String[] tmp = this.prac_location.split(",",-1);
			try{
				if(tmp.length == 2){
					this.lon = Double.parseDouble(tmp[0]);
					this.lat = Double.parseDouble(tmp[1]);
				}
			}catch(NumberFormatException ne){
				this.prac_location = "Invalid data ["+this.prac_location+"]";
			}
		}
	}

	// Ascending sort
	@Override
	public int compareTo(ProviderAddress o) {
		if(this.lat.doubleValue() == 0.0d || this.lon.doubleValue() == 0.0d )
			return -1;
		double difference = (this.proximity - o.getProximity());
		if(difference > 0 ){
			return 1;
		}else if(difference < 0){
			return -1;
		}else if(difference == 0){
			return 0;
		}else{
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONArray faxes = null;
		JSONArray phones = null;
		JSONObject  obj = new JSONObject();
		obj.put("addline1", this.addline1);
		if(addLine2 != null){
			obj.put("addLine2", this.addLine2);
		}
		obj.put("city", this.city);
		obj.put("state", this.state);
		obj.put("zip", this.zip);
		obj.put("lat", this.lat);
		obj.put("lon", this.lon);
		obj.put("prac_location", this.prac_location);
		obj.put("proximity", this.proximity);
		if(this.phone != null){
			phones = new JSONArray();
			for(String phone: this.phone){
				phones.put(phone);
			}
			obj.put("phones", phones);
		}
		if(this.fax != null){
			faxes = new JSONArray();
			for(String faxNo: this.fax){
				faxes.put(faxNo);
			}
			obj.put("fax", faxes);
		}
		return obj.toJSONString();
	}
}
