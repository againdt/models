package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class EnrolleeCurrentMonthDTO implements Serializable{

	/**
	 * @author ajinkya_m
	 */

	private String eventName;
	private Date eventDate;
	private Date coverageStartDate;
	private Date coverageEndtDate;
	private Float enrollmentAmount;
	private Integer enrolleeId;
	private String enrolleeStatus;
	private Date createdOn;
	private String personType;
	private Float employerResponsibilityAmt;
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public Date getCoverageEndtDate() {
		return coverageEndtDate;
	}
	public void setCoverageEndtDate(Date coverageEndtDate) {
		this.coverageEndtDate = coverageEndtDate;
	}
	public Float getEnrollmentAmount() {
		return enrollmentAmount;
	}
	public void setEnrollmentAmount(Float enrollmentAmount) {
		this.enrollmentAmount = enrollmentAmount;
	}
	public Integer getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	public String getEnrolleeStatus() {
		return enrolleeStatus;
	}
	public void setEnrolleeStatus(String enrolleeStatus) {
		this.enrolleeStatus = enrolleeStatus;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public Float getEmployerResponsibilityAmt() {
		return employerResponsibilityAmt;
	}
	public void setEmployerResponsibilityAmt(Float employerResponsibilityAmt) {
		this.employerResponsibilityAmt = employerResponsibilityAmt;
	}
	
	
	@Override
	public String toString() {
		return "EnrolleeCurrentMonthDTO [eventName=" + eventName
				+ ", eventDate=" + eventDate + ", coverageStartDate="
				+ coverageStartDate + ", coverageEndtDate=" + coverageEndtDate
				+ ", enrollmentAmount=" + enrollmentAmount + ", enrolleeId="
				+ enrolleeId + ", enrolleeStatus=" + enrolleeStatus
				+ ", createdOn=" + createdOn + ", personType=" + personType
				+ ", employerResponsibilityAmt=" + employerResponsibilityAmt
				+ "]";
	}
	
	

}
