/*
 * @author santanu
 * @version 1.0
 * @since July 25, 2016 
 * 
 * This DTO will return limited issuer info
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;


public class IssuerByHIOSIdResponseDTO extends GHIXResponse implements Serializable {
		
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String hiosIssuerId;
	private String companyLogo;
	private String brandName;
	private String companyLegalName;
	private String marketingName;
	private  byte[] logo;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	/**
	 * @param hiosIssuerId the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	/**
	 * @return the companyLogo
	 */
	public String getCompanyLogo() {
		return companyLogo;
	}
	/**
	 * @param companyLogo the companyLogo to set
	 */
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the companyLegalName
	 */
	public String getCompanyLegalName() {
		return companyLegalName;
	}

	/**
	 * @param companyLegalName the companyLegalName to set
	 */
	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}

	/**
	 * @return the marketingName
	 */
	public String getMarketingName() {
		return marketingName;
	}

	/**
	 * @param marketingName the marketingName to set
	 */
	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	/**
	 * @return the logo
	 */
	public byte[] getLogo() {
		return logo;
	}

	/**
	 * @param logo the logo to set
	 */
	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
}
