package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_name_phix")
public class ProviderNamePhix  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String facilityName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffix;
	private Set<ProviderProduct> providerProducts = new HashSet<ProviderProduct>(0);

	@Id 
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@Column(name="FACILITY_NAME", length=150)
	public String getFacilityName() {
		return this.facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}


	@Column(name="FIRST_NAME", length=150)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	@Column(name="MIDDLE_NAME", length=150)
	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	@Column(name="LAST_NAME", length=150)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Column(name="SUFFIX", length=10)
	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerNamePhix")
	public Set<ProviderProduct> getProviderProducts() {
		return this.providerProducts;
	}

	public void setProviderProducts(Set<ProviderProduct> providerProducts) {
		this.providerProducts = providerProducts;
	}

}


