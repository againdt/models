package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ReconMonthlyPremiumDTO implements Serializable , Comparable< ReconMonthlyPremiumDTO>{
	private static final long serialVersionUID = 1L;
	
	private Integer coverageMonth;
	private Integer coverageYear;
	private Float aptcAmt;
	private Float csrAmt;
	private Float grossPremium;
	private Float issuerpremium;
	private String ratingArea;
	private Integer memberCount;
	private String monthStartdate;
	private String monthEnddate;
	@JsonIgnore
	private int activeDays;
	
	public Integer getCoverageMonth() {
		return coverageMonth;
	}
	public void setCoverageMonth(Integer coverageMonth) {
		this.coverageMonth = coverageMonth;
	}
	public Integer getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	public Float getAptcAmt() {
		return aptcAmt;
	}
	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}
	public Float getCsrAmt() {
		return csrAmt;
	}
	public void setCsrAmt(Float csrAmt) {
		this.csrAmt = roundFloat(csrAmt);
	}
	public Float getGrossPremium() {
		return grossPremium;
	}
	public void setGrossPremium(Float grossPremium) {
		this.grossPremium = roundFloat(grossPremium);
	}
	public Float getIssuerpremium() {
		return issuerpremium;
	}
	public void setIssuerpremium(Float issuerpremium) {
		this.issuerpremium = roundFloat(issuerpremium);
	}
	public String getRatingArea() {
		return ratingArea;
	}
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	public Integer getMemberCount() {
		return memberCount;
	}
	public void setMemberCount(Integer memberCount) {
		this.memberCount = memberCount;
	}
	public String getMonthStartdate() {
		return monthStartdate;
	}
	public void setMonthStartdate(String monthStartdate) {
		this.monthStartdate = monthStartdate;
	}
	public String getMonthEnddate() {
		return monthEnddate;
	}
	public void setMonthEnddate(String monthEnddate) {
		this.monthEnddate = monthEnddate;
	}
	
	
	@Override
	public int compareTo(ReconMonthlyPremiumDTO prem) {
		if(prem!=null){
			return (Integer.compare( Integer.parseInt(this.coverageYear+String.format("%02d", this.coverageMonth)),Integer.parseInt(prem.coverageYear+String.format("%02d", prem.coverageMonth))) );
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
    private Float roundFloat(Float d) {
    	if(d!=null){
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    	}else{
    		return null;
    	}
    }
	public int getActiveDays() {
		return activeDays;
	}
	public void setActiveDays(int activeDays) {
		this.activeDays = activeDays;
	}
    
    
	
}
