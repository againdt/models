package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @since 25th June 2015
 * @author Sharma_k
 * @version 1.0
 * Jira ID: HIX-70358
 * Created DTO class for providing Enrollment data through exposed API
 */
public class EnrollmentDataDTO implements Serializable
{
	private Integer enrollmentId;
	private Integer planId;
	private String houseHoldCaseId;
	private Date enrollmentBenefitEffectiveStartDate;
	private Date enrollmentBenefitEffectiveEndDate;
	private Float grossPremiumAmt;
	private Float netPremiumAmt;
	private String enrollmentStatus;
	private Date creationTimestamp;//Creation_timeStamp
	private String planType;//InsuranceTypeLkp label
	private String planLevel;//Metal Tier
	private String planName;
	private String carrierName;//INSURER_NAME
	private Integer carrierId;//HIOS_ISSUER_ID
	private Float aptcAmount;//elected_APTC
	private BigDecimal stateSubsidyAmount;
	private String renewalFlag;
	//TODO Active or Passive Renewal
	private Long priorEnrollmentId;
	private String csrLevel;
	private Date lastUpdateDate;
	private Integer lastUpdatedBy;
	private Date submittedToCarrierDate;//SubmittedDate
	private Date aptcEffectuveDate;//SubmittedDate
	private Date stateSubsidyEffectiveDate;
	private Integer createdBy; 
	//HIX-71635	Add additional fields for CAP
	private Float csrAmount;
	private String transactionId;
	private Long ssapApplicationId;
	private Long priorSsapApplicationId;
	private String ssapCaseNumber;
	private String CMSPlanID;
	private String issuerId;
	private Float ehbSlcspAmt;
	private Float esEhbPrmAmt;
	private Float dntlEhbAmt;


	/**
	 * HIX-73087 Populate the data in enrollment detail API
	 */
	private String lastUpdatedUserName;
	private String lastUpdatedRoleName;
	private String submittedByUserName;//Created By User Name
	private String submittedByRoleName;//Created By Role Name
	private Integer numberOfEnrollees;
	/* Jira Id: HIX-79982*/
	private Date enrollmentConfirmationDate;
	
	private String encEnrollmentId; //HIX-82194
	private Float ehbPercentage; //HIX-86244
	
	private List<EnrolleeDataDTO> enrolleeDataDtoList;
	private List<EnrollmentPremiumDTO> enrollmentPremiumDtoList;
	private String premiumEffDate; //HIX-87871
	private Float csrMultiplier;
	private Integer submittedByUserId;
	
	// Jira HIX-106484
	private String agentName;
	private Integer agentId;
	
	private String ratingArea;
	private Date ratingAreaEffectiveDate;
	private String agentTpaNumber;
	
	private String homeAddress1;
	private String homeAddress2;
	private String homeCity;
	private String homeState;
	private String homeZipcode;
	private String homeCounty;
	private String homeCountyCode;
	
	private String mailingAddress1;
	private String mailingAddress2;
	private String mailingCity;
	private String mailingState;
	private String mailingZipcode;
	private String mailingCounty;
	private String mailingCountyCode;
	
	private String eventType;
	private String eventReason;
	private Date eventCreationDate;
	private String eventCreatedBy;
	private String userRole;

	private boolean isHealthPlanSelectionComplete = false;
	private boolean isDentalPlanSelectionComplete = false;
	private boolean isEnrollmentWindowOpen = true;
	
	private Integer assisterBrokerId;
	private Float csrAmt;
	private Integer policyId;
	private String enrollmentType;
	private String logGlobalId;
	private Date financialEffectiveDate;
	private String externalHouseHoldCaseId;
	private String allowChangePlan;
	
	private String correlationId;//HIX-112533
	
	private transient boolean isSubscriberPresent = false;
	
	private String ptfFirstName;
	private String ptfLastName;
	private String ptfMiddleName;

	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the planId
	 */
	public Integer getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	/**
	 * @return the houseHoldCaseId
	 */
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}
	/**
	 * @param houseHoldCaseId the houseHoldCaseId to set
	 */
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}
	/**
	 * @return the enrollmentBenefitEffectiveStartDate
	 */
	public Date getEnrollmentBenefitEffectiveStartDate() {
		return enrollmentBenefitEffectiveStartDate;
	}
	/**
	 * @param enrollmentBenefitEffectiveStartDate the enrollmentBenefitEffectiveStartDate to set
	 */
	public void setEnrollmentBenefitEffectiveStartDate(
			Date enrollmentBenefitEffectiveStartDate) {
		this.enrollmentBenefitEffectiveStartDate = enrollmentBenefitEffectiveStartDate;
	}
	/**
	 * @return the enrollmentBenefitEffectiveEndDate
	 */
	public Date getEnrollmentBenefitEffectiveEndDate() {
		return enrollmentBenefitEffectiveEndDate;
	}
	/**
	 * @param enrollmentBenefitEffectiveEndDate the enrollmentBenefitEffectiveEndDate to set
	 */
	public void setEnrollmentBenefitEffectiveEndDate(
			Date enrollmentBenefitEffectiveEndDate) {
		this.enrollmentBenefitEffectiveEndDate = enrollmentBenefitEffectiveEndDate;
	}
	/**
	 * @return the grossPremiumAmt
	 */
	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}
	/**
	 * @param grossPremiumAmt the grossPremiumAmt to set
	 */
	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}
	/**
	 * @return the netPremiumAmt
	 */
	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}
	/**
	 * @param netPremiumAmt the netPremiumAmt to set
	 */
	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}
	/**
	 * @return the enrollmentStatus
	 */
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	/**
	 * @return the enrolleeDataDtoList
	 */
	public List<EnrolleeDataDTO> getEnrolleeDataDtoList() {
		return enrolleeDataDtoList;
	}
	/**
	 * @param enrolleeDataDtoList the enrolleeDataDtoList to set
	 */
	public void setEnrolleeDataDtoList(List<EnrolleeDataDTO> enrolleeDataDtoList) {
		this.enrolleeDataDtoList = enrolleeDataDtoList;
	}
	
	/**
	 * @return the creationTimestamp
	 */
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	/**
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	/**
	 * @return the planType
	 */
	public String getPlanType() {
		return planType;
	}
	/**
	 * @param planType the planType to set
	 */
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	/**
	 * @return the planLevel
	 */
	public String getPlanLevel() {
		return planLevel;
	}
	/**
	 * @param planLevel the planLevel to set
	 */
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the carrierName
	 */
	public String getCarrierName() {
		return carrierName;
	}
	/**
	 * @param carrierName the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	/**
	 * @return the carrierId
	 */
	public Integer getCarrierId() {
		return carrierId;
	}
	/**
	 * @param carrierId the carrierId to set
	 */
	public void setCarrierId(Integer carrierId) {
		this.carrierId = carrierId;
	}
	public String getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the aptcAmount
	 */
	public Float getAptcAmount() {
		return aptcAmount;
	}
	/**
	 * @param aptcAmount the aptcAmount to set
	 */
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	/**
	 * @return the renewalFlag
	 */
	public String getRenewalFlag() {
		return renewalFlag;
	}
	/**
	 * @param renewalFlag the renewalFlag to set
	 */
	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}
	/**
	 * @return the priorEnrollmentId
	 */
	public Long getPriorEnrollmentId() {
		return priorEnrollmentId;
	}
	/**
	 * @param priorEnrollmentId the priorEnrollmentId to set
	 */
	public void setPriorEnrollmentId(Long priorEnrollmentId) {
		this.priorEnrollmentId = priorEnrollmentId;
	}
	/**
	 * @return the csrLevel
	 */
	public String getCsrLevel() {
		return csrLevel;
	}
	/**
	 * @param csrLevel the csrLevel to set
	 */
	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}
	/**
	 * @return the lastUpdateDate
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	/**
	 * @param lastUpdateDate the lastUpdateDate to set
	 */
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	/**
	 * @return the submittedToCarrierDate
	 */
	public Date getSubmittedToCarrierDate() {
		return submittedToCarrierDate;
	}
	/**
	 * @param submittedToCarrierDate the submittedToCarrierDate to set
	 */
	public void setSubmittedToCarrierDate(Date submittedToCarrierDate) {
		this.submittedToCarrierDate = submittedToCarrierDate;
	}
	public Date getAptcEffectuveDate() {
		return aptcEffectuveDate;
	}
	public void setAptcEffectuveDate(Date aptcEffectuveDate) {
		this.aptcEffectuveDate = aptcEffectuveDate;
	}
	public Float getCsrAmount() {
		return csrAmount;
	}
	public void setCsrAmount(Float csrAmount) {
		this.csrAmount = csrAmount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getSsapCaseNumber() {
		return ssapCaseNumber;
	}
	public void setSsapCaseNumber(String ssapCaseNumber) {
		this.ssapCaseNumber = ssapCaseNumber;
	}
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the cMSPlanID
	 */
	public String getCMSPlanID() {
		return CMSPlanID;
	}
	/**
	 * @param cMSPlanID the cMSPlanID to set
	 */
	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}
	/**
	 * @return the lastUpdatedUserName
	 */
	public String getLastUpdatedUserName() {
		return lastUpdatedUserName;
	}
	/**
	 * @param lastUpdatedUserName the lastUpdatedUserName to set
	 */
	public void setLastUpdatedUserName(String lastUpdatedUserName) {
		this.lastUpdatedUserName = lastUpdatedUserName;
	}
	/**
	 * @return the lastUpdatedRoleName
	 */
	public String getLastUpdatedRoleName() {
		return lastUpdatedRoleName;
	}
	/**
	 * @param lastUpdatedRoleName the lastUpdatedRoleName to set
	 */
	public void setLastUpdatedRoleName(String lastUpdatedRoleName) {
		this.lastUpdatedRoleName = lastUpdatedRoleName;
	}
	/**
	 * @return the submittedByUserName
	 */
	public String getSubmittedByUserName() {
		return submittedByUserName;
	}
	/**
	 * @param submittedByUserName the submittedByUserName to set
	 */
	public void setSubmittedByUserName(String submittedByUserName) {
		this.submittedByUserName = submittedByUserName;
	}
	/**
	 * @return the submittedByRoleName
	 */
	public String getSubmittedByRoleName() {
		return submittedByRoleName;
	}
	/**
	 * @param submittedByRoleName the submittedByRoleName to set
	 */
	public void setSubmittedByRoleName(String submittedByRoleName) {
		this.submittedByRoleName = submittedByRoleName;
	}
	/**
	 * @return the numberOfEnrollees
	 */
	public Integer getNumberOfEnrollees() {
		return numberOfEnrollees;
	}
	/**
	 * @param numberOfEnrollees the numberOfEnrollees to set
	 */
	public void setNumberOfEnrollees(Integer numberOfEnrollees) {
		this.numberOfEnrollees = numberOfEnrollees;
	}
	/**
	 * @return the enrollmentConfirmationDate
	 */
	public Date getEnrollmentConfirmationDate() {
		return enrollmentConfirmationDate;
	}
	/**
	 * @param enrollmentConfirmationDate the enrollmentConfirmationDate to set
	 */
	public void setEnrollmentConfirmationDate(Date enrollmentConfirmationDate) {
		this.enrollmentConfirmationDate = enrollmentConfirmationDate;
	}

	public String getEncEnrollmentId() {
		return encEnrollmentId;
	}
	public void setEncEnrollmentId(String encEnrollmentId) {
		this.encEnrollmentId = encEnrollmentId;
	}
	/**
	 * @return the esEhbPrmAmt
	 */
	public Float getEsEhbPrmAmt() {
		return esEhbPrmAmt;
	}
	/**
	 * @param esEhbPrmAmt the esEhbPrmAmt to set
	 */
	public void setEsEhbPrmAmt(Float esEhbPrmAmt) {
		this.esEhbPrmAmt = esEhbPrmAmt;
	}
	/**
	 * @return the ehbSlcspAmt
	 */
	public Float getEhbSlcspAmt() {
		return ehbSlcspAmt;
	}
	/**
	 * @param ehbSlcspAmt the ehbSlcspAmt to set
	 */
	public void setEhbSlcspAmt(Float ehbSlcspAmt) {
		this.ehbSlcspAmt = ehbSlcspAmt;
	}
	/**
	 * @return the ehbPercentage
	 */
	public Float getEhbPercentage() {
		return ehbPercentage;
	}
	/**
	 * @param ehbPercentage the ehbPercentage to set
	 */
	public void setEhbPercentage(Float ehbPercentage) {
		this.ehbPercentage = ehbPercentage;
	}
	/**
	 * @return the enrollmentPremiumDtoList
	 */
	public List<EnrollmentPremiumDTO> getEnrollmentPremiumDtoList() {
		return enrollmentPremiumDtoList;
	}
	/**
	 * @param enrollmentPremiumDtoList the enrollmentPremiumDtoList to set
	 */
	public void setEnrollmentPremiumDtoList(List<EnrollmentPremiumDTO> enrollmentPremiumDtoList) {
		this.enrollmentPremiumDtoList = enrollmentPremiumDtoList;
	}
	public String getPremiumEffDate() {
		return premiumEffDate;
	}
	public void setPremiumEffDate(String premiumEffDate) {
		this.premiumEffDate = premiumEffDate;
	}
	public Float getDntlEhbAmt() {
		return dntlEhbAmt;
	}
	public void setDntlEhbAmt(Float dntlEhbAmt) {
		this.dntlEhbAmt = dntlEhbAmt;
	}
	public Float getCsrMultiplier() {
		return csrMultiplier;
	}
	public void setCsrMultiplier(Float csrMultiplier) {
		this.csrMultiplier = csrMultiplier;
	}
	public Integer getSubmittedByUserId() {
		return submittedByUserId;
	}
	public void setSubmittedByUserId(int i) {
		this.submittedByUserId = i;
	}
	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}
	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	/**
	 * @return the agentId
	 */
	public Integer getAgentId() {
		return agentId;
	}
	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	/**
	 * @return the ratingArea
	 */
	public String getRatingArea() {
		return ratingArea;
	}
	/**
	 * @param ratingArea the ratingArea to set
	 */
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	/**
	 * @return the ratingAreaEffectiveDate
	 */
	public Date getRatingAreaEffectiveDate() {
		return ratingAreaEffectiveDate;
	}
	/**
	 * @param ratingAreaEffectiveDate the ratingAreaEffectiveDate to set
	 */
	public void setRatingAreaEffectiveDate(Date ratingAreaEffectiveDate) {
		this.ratingAreaEffectiveDate = ratingAreaEffectiveDate;
	}
	/**
	 * @return the agentTpaNumber
	 */
	public String getAgentTpaNumber() {
		return agentTpaNumber;
	}
	/**
	 * @param agentTpaNumber the agentTpaNumber to set
	 */
	public void setAgentTpaNumber(String agentTpaNumber) {
		this.agentTpaNumber = agentTpaNumber;
	}
	/**
	 * @return the homeAddress1
	 */
	public String getHomeAddress1() {
		return homeAddress1;
	}
	/**
	 * @param homeAddress1 the homeAddress1 to set
	 */
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}
	/**
	 * @return the homeAddress2
	 */
	public String getHomeAddress2() {
		return homeAddress2;
	}
	/**
	 * @param homeAddress2 the homeAddress2 to set
	 */
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}
	/**
	 * @return the homeCity
	 */
	public String getHomeCity() {
		return homeCity;
	}
	/**
	 * @param homeCity the homeCity to set
	 */
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}
	/**
	 * @return the homeState
	 */
	public String getHomeState() {
		return homeState;
	}
	/**
	 * @param homeState the homeState to set
	 */
	public void setHomeState(String homeState) {
		this.homeState = homeState;
	}
	/**
	 * @return the homeZipcode
	 */
	public String getHomeZipcode() {
		return homeZipcode;
	}
	/**
	 * @param homeZipcode the homeZipcode to set
	 */
	public void setHomeZipcode(String homeZipcode) {
		this.homeZipcode = homeZipcode;
	}
	/**
	 * @return the homeCounty
	 */
	public String getHomeCounty() {
		return homeCounty;
	}
	/**
	 * @param homeCounty the homeCounty to set
	 */
	public void setHomeCounty(String homeCounty) {
		this.homeCounty = homeCounty;
	}
	/**
	 * @return the homeCountyCode
	 */
	public String getHomeCountyCode() {
		return homeCountyCode;
	}
	/**
	 * @param homeCountyCode the homeCountyCode to set
	 */
	public void setHomeCountyCode(String homeCountyCode) {
		this.homeCountyCode = homeCountyCode;
	}
	/**
	 * @return the mailingAddress1
	 */
	public String getMailingAddress1() {
		return mailingAddress1;
	}
	/**
	 * @param mailingAddress1 the mailingAddress1 to set
	 */
	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}
	/**
	 * @return the mailingAddress2
	 */
	public String getMailingAddress2() {
		return mailingAddress2;
	}
	/**
	 * @param mailingAddress2 the mailingAddress2 to set
	 */
	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}
	/**
	 * @return the mailingCity
	 */
	public String getMailingCity() {
		return mailingCity;
	}
	/**
	 * @param mailingCity the mailingCity to set
	 */
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	/**
	 * @return the mailingState
	 */
	public String getMailingState() {
		return mailingState;
	}
	/**
	 * @param mailingState the mailingState to set
	 */
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}
	/**
	 * @return the mailingZipcode
	 */
	public String getMailingZipcode() {
		return mailingZipcode;
	}
	/**
	 * @param mailingZipcode the mailingZipcode to set
	 */
	public void setMailingZipcode(String mailingZipcode) {
		this.mailingZipcode = mailingZipcode;
	}
	/**
	 * @return the mailingCounty
	 */
	public String getMailingCounty() {
		return mailingCounty;
	}
	/**
	 * @param mailingCounty the mailingCounty to set
	 */
	public void setMailingCounty(String mailingCounty) {
		this.mailingCounty = mailingCounty;
	}
	/**
	 * @return the mailingCountyCode
	 */
	public String getMailingCountyCode() {
		return mailingCountyCode;
	}
	/**
	 * @param mailingCountyCode the mailingCountyCode to set
	 */
	public void setMailingCountyCode(String mailingCountyCode) {
		this.mailingCountyCode = mailingCountyCode;
	}
	public boolean isHealthPlanSelectionComplete() {
		return isHealthPlanSelectionComplete;
	}
	public void setHealthPlanSelectionComplete(boolean isHealthPlanSelectionComplete) {
		this.isHealthPlanSelectionComplete = isHealthPlanSelectionComplete;
	}
	public boolean isDentalPlanSelectionComplete() {
		return isDentalPlanSelectionComplete;
	}
	public void setDentalPlanSelectionComplete(boolean isDentalPlanSelectionComplete) {
		this.isDentalPlanSelectionComplete = isDentalPlanSelectionComplete;
	}
	public boolean isEnrollmentWindowOpen() {
		return isEnrollmentWindowOpen;
	}
	public void setEnrollmentWindowOpen(boolean isEnrollmentWindowOpen) {
		this.isEnrollmentWindowOpen = isEnrollmentWindowOpen;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventReason() {
		return eventReason;
	}
	public void setEventReason(String eventReason) {
		this.eventReason = eventReason;
	}
	public Date getEventCreationDate() {
		return eventCreationDate;
	}
	public void setEventCreationDate(Date eventCreationDate) {
		this.eventCreationDate = eventCreationDate;
	}
	public String getEventCreatedBy() {
		return eventCreatedBy;
	}
	public void setEventCreatedBy(String eventCreatedBy) {
		this.eventCreatedBy = eventCreatedBy;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public Long getPriorSsapApplicationId() {
		return priorSsapApplicationId;
	}
	public void setPriorSsapApplicationId(Long priorSsapApplicationId) {
		this.priorSsapApplicationId = priorSsapApplicationId;
	}
	public Integer getAssisterBrokerId() {
		return assisterBrokerId;
	}
	public void setAssisterBrokerId(Integer assisterBrokerId) {
		this.assisterBrokerId = assisterBrokerId;
	}
	public Float getCsrAmt() {
		return csrAmt;
	}
	public void setCsrAmt(Float csrAmt) {
		this.csrAmt = csrAmt;
	}
	public Integer getPolicyId() {
		return policyId;
	}
	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	public String getLogGlobalId() {
		return logGlobalId;
	}
	public void setLogGlobalId(String logGlobalId) {
		this.logGlobalId = logGlobalId;
	}
	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}
	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}
	public String getExternalHouseHoldCaseId() {
		return externalHouseHoldCaseId;
	}
	public void setExternalHouseHoldCaseId(String externalHouseHoldCaseId) {
		this.externalHouseHoldCaseId = externalHouseHoldCaseId;
	}
	public String getAllowChangePlan() {
		return allowChangePlan;
	}
	public void setAllowChangePlan(String allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public boolean isSubscriberPresent() {
		return isSubscriberPresent;
	}
	public void setSubscriberPresent(boolean isSubscriberPresent) {
		this.isSubscriberPresent = isSubscriberPresent;
	}
	public String getPtfFirstName() {
		return ptfFirstName;
	}
	public void setPtfFirstName(String ptfFirstName) {
		this.ptfFirstName = ptfFirstName;
	}
	public String getPtfLastName() {
		return ptfLastName;
	}
	public void setPtfLastName(String ptfLastName) {
		this.ptfLastName = ptfLastName;
	}
	public String getPtfMiddleName() {
		return ptfMiddleName;
	}
	public void setPtfMiddleName(String ptfMiddleName) {
		this.ptfMiddleName = ptfMiddleName;
	}

	public BigDecimal getStateSubsidyAmount() {
		return stateSubsidyAmount;
	}

	public void setStateSubsidyAmount(BigDecimal stateSubsidyAmount) {
		this.stateSubsidyAmount = stateSubsidyAmount;
	}

	public Date getStateSubsidyEffectiveDate() {
		return stateSubsidyEffectiveDate;
	}

	public void setStateSubsidyEffectiveDate(Date stateSubsidyEffectiveDate) {
		this.stateSubsidyEffectiveDate = stateSubsidyEffectiveDate;
	}
}
