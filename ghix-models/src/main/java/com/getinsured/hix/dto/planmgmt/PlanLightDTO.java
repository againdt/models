package com.getinsured.hix.dto.planmgmt;



public class PlanLightDTO {
	
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String hiosProductId;
	private String issuerShortName;
	
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getHiosProductId() {
		return hiosProductId;
	}
	public void setHiosProductId(String hiosProductId) {
		this.hiosProductId = hiosProductId;
	}
	public String getIssuerShortName() {
		return issuerShortName;
	}
	public void setIssuerShortName(String issuerShortName) {
		this.issuerShortName = issuerShortName;
	}
	
	
	

}
