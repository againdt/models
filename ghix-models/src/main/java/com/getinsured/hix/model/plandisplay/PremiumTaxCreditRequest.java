/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.util.List;

import com.getinsured.hix.model.GHIXRequest;

/**
 * @author Mishra_m
 *
 */
public class PremiumTaxCreditRequest {
	private String coverageStartDate;
	private String zip;
	private List<Integer> ages;
	private double householdIncome;
	private int householdSize;
	private List<String> coverage;
	
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public List<Integer> getAges() {
		return ages;
	}
	public void setAges(List<Integer> ages) {
		this.ages = ages;
	}
	public double getHouseholdIncome() {
		return householdIncome;
	}
	public void setHouseholdIncome(double householdIncome) {
		this.householdIncome = householdIncome;
	}
	public int getHouseholdSize() {
		return householdSize;
	}
	public void setHouseholdSize(int householdSize) {
		this.householdSize = householdSize;
	}
	public List<String> getCoverage() {
		return coverage;
	}
	public void setCoverage(List<String> coverage) {
		this.coverage = coverage;
	}
}
