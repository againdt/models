/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Srikanth Nakka
 *
 */
public class PlanInfo {

	@NotEmpty(message = "planHiosId cannot be empty.")
	private String planHiosId;
	
	@NotEmpty(message = "planName cannot be empty.")
	private String planName;
	
	private String metalTier;
	private Float amtNetPremium;
	private Float amtGrossPremium;
	private Float amtAptc;
	
	@NotEmpty(message = "insuranceType cannot be empty.")
	private String insuranceType;
	
	public String getPlanHiosId() {
		return planHiosId;
	}
	public void setPlanHiosId(String planHiosId) {
		this.planHiosId = planHiosId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getMetalTier() {
		return metalTier;
	}
	public void setMetalTier(String metalTier) {
		this.metalTier = metalTier;
	}
	public Float getAmtNetPremium() {
		return amtNetPremium;
	}
	public void setAmtNetPremium(Float amtNetPremium) {
		this.amtNetPremium = amtNetPremium;
	}
	public Float getAmtGrossPremium() {
		return amtGrossPremium;
	}
	public void setAmtGrossPremium(Float amtGrossPremium) {
		this.amtGrossPremium = amtGrossPremium;
	}
	public Float getAmtAptc() {
		return amtAptc;
	}
	public void setAmtAptc(Float amtAptc) {
		this.amtAptc = amtAptc;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	
	
	
}
