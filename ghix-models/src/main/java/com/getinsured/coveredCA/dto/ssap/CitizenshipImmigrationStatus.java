
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class CitizenshipImmigrationStatus {

	
	 @Expose
	 private Boolean citizenshipStatusIndicator;
	 @Expose
	 private Boolean eligibleImmigrationStatusIndicator = true;
	 @Expose
	 private String eligibleImmigrationDocumentSelected;
	 @Expose
	 private Boolean qualifiedNonCitizenIndicator;
	 @Expose
	 private Boolean livedInTheUSSince1996Indicator;
	 @Expose
	 private Boolean honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
	 @Expose
	 private CitizenshipDocument citizenshipDocument=new CitizenshipDocument();
	 @Expose
	 private NameOnDocument nameOnDocument=new NameOnDocument();
    
   
   
	

	public Boolean getHonorablyDischargedOrActiveDutyMilitaryMemberIndicator() {
        return honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
    }

    public void setHonorablyDischargedOrActiveDutyMilitaryMemberIndicator(Boolean honorablyDischargedOrActiveDutyMilitaryMemberIndicator) {
        this.honorablyDischargedOrActiveDutyMilitaryMemberIndicator = honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
    }

    public Boolean getCitizenshipStatusIndicator() {
        return citizenshipStatusIndicator;
    }

    public void setCitizenshipStatusIndicator(Boolean citizenshipStatusIndicator) {
        this.citizenshipStatusIndicator = citizenshipStatusIndicator;
    }

    public Boolean getEligibleImmigrationStatusIndicator() {
        return eligibleImmigrationStatusIndicator;
    }

    public void setEligibleImmigrationStatusIndicator(Boolean eligibleImmigrationStatusIndicator) {
        this.eligibleImmigrationStatusIndicator = eligibleImmigrationStatusIndicator;
    }

    public CitizenshipDocument getCitizenshipDocument() {
        return citizenshipDocument;
    }

    public void setCitizenshipDocument(CitizenshipDocument citizenshipDocument) {
        this.citizenshipDocument = citizenshipDocument;
    }


    public String getEligibleImmigrationDocumentSelected() {
        return eligibleImmigrationDocumentSelected;
    }

    public void setEligibleImmigrationDocumentSelected(String eligibleImmigrationDocumentSelected) {
        this.eligibleImmigrationDocumentSelected = eligibleImmigrationDocumentSelected;
    }

   
    public Boolean getLivedInTheUSSince1996Indicator() {
        return livedInTheUSSince1996Indicator;
    }

    public void setLivedInTheUSSince1996Indicator(Boolean livedInTheUSSince1996Indicator) {
        this.livedInTheUSSince1996Indicator = livedInTheUSSince1996Indicator;
    } 

    public NameOnDocument getNameOnDocument() {
		return nameOnDocument;
	}

	public void setNameOnDocument(NameOnDocument nameOnDocument) {
		this.nameOnDocument = nameOnDocument;
	}

	public Boolean getQualifiedNonCitizenIndicator() {
		return qualifiedNonCitizenIndicator;
	}

	public void setQualifiedNonCitizenIndicator(Boolean qualifiedNonCitizenIndicator) {
		this.qualifiedNonCitizenIndicator = qualifiedNonCitizenIndicator;
	}
	

}
