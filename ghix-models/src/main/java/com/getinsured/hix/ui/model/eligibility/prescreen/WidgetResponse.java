package com.getinsured.hix.ui.model.eligibility.prescreen;

import java.util.ArrayList;
import java.util.List;


public class WidgetResponse {

	private static final String NOT_APPLICABLE = "N/A";
	
	private List<PhixMember> members;

	private String aptc;

	private String aptcEligible;

	private String subsidyEligible;

	private String csrLevel;


	private String expectedMonthlyPremium;

	private String lbpPremiumAfterAptc;

	private String lbpPremiumBeforeAptc;

	private String medicaidEligible;

	private String medicareEligible;

	private String chipEligible;

	private String chipHousehold;

	private String medicareHousehold;

	private int noOfAptcCsr;

	private int noOfChip;

	private int noOfMedicare;

	private String status;

	private String medicaidHousehold = "N";

	private String householdEligibilityMix;

	private String mandatePenaltyAmount;
	
	private String exchangeUrl;

	public String getExchangeUrl() {
		return exchangeUrl;
	}

	public void setExchangeUrl(String exchangeUrl) {
		this.exchangeUrl = exchangeUrl;
	}

	public String getOnExchangeCheapestPremiumAvailable() {
		return onExchangeCheapestPremiumAvailable;
	}

	public void setOnExchangeCheapestPremiumAvailable(String onExchangeCheapestPremiumAvailable) {
		this.onExchangeCheapestPremiumAvailable = onExchangeCheapestPremiumAvailable;
	}

	public String getOffExchangeCheapestPremiumAvailable() {
		return offExchangeCheapestPremiumAvailable;
	}

	public void setOffExchangeCheapestPremiumAvailable(String offExchangeCheapestPremiumAvailable) {
		this.offExchangeCheapestPremiumAvailable = offExchangeCheapestPremiumAvailable;
	}

	private String onExchangeCheapestPremiumAvailable=NOT_APPLICABLE;
	private String offExchangeCheapestPremiumAvailable=NOT_APPLICABLE;
	
	public String getMandatePenaltyAmount() {
		return mandatePenaltyAmount;
	}

	public void setMandatePenaltyAmount(String mandatePenaltyAmount) {
		this.mandatePenaltyAmount = mandatePenaltyAmount;
	}

	public String getHouseholdEligibilityMix() {
		return householdEligibilityMix;
	}

	public void setHouseholdEligibilityMix(String householdEligibilityMix) {
		this.householdEligibilityMix = householdEligibilityMix;
	}

	private String giOnExchangePlansAvailable=NOT_APPLICABLE;
	private String giOffExchangePlansAvailable=NOT_APPLICABLE;
	
	public String getGiOnExchangePlansAvailable() {
		return giOnExchangePlansAvailable;
	}

	public void setGiOnExchangePlansAvailable(String giOnExchangePlansAvailable) {
		this.giOnExchangePlansAvailable = giOnExchangePlansAvailable;
	}

	public String getGiOffExchangePlansAvailable() {
		return giOffExchangePlansAvailable;
	}

	public void setGiOffExchangePlansAvailable(String giOffExchangePlansAvailable) {
		this.giOffExchangePlansAvailable = giOffExchangePlansAvailable;
	}

	/* Start -EMX specific Attributes */
	private String censusType;
	private double employerContributionAmount;
	private double hraAmount;
	private double cobraPremium;
	private String cobraElectionDueDate;
	private String hraTransitionDate;
//	private String applicantFirstName;
//	private String applicantLastName;

	private PhixRetireeHousehold retireeData;
	
	/* End */
	private Long leadId;

	private String ivrNumber;

	public List<PhixMember> getMembers() {

		if (members == null) {
			members = new ArrayList<PhixMember>();
		}

		return members;
	}

	public void setMembers(List<PhixMember> members) {
		this.members = members;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getAptcEligible() {
		return aptcEligible;
	}

	public void setAptcEligible(String aptcEligible) {
		this.aptcEligible = aptcEligible;
	}

	public String getSubsidyEligible() {
		return subsidyEligible;
	}

	public void setSubsidyEligible(String subsidyEligible) {
		this.subsidyEligible = subsidyEligible;
	}

	
	public String getExpectedMonthlyPremium() {
		return expectedMonthlyPremium;
	}

	public void setExpectedMonthlyPremium(String expectedMonthlyPremium) {
		this.expectedMonthlyPremium = expectedMonthlyPremium;
	}

	public String getLbpPremiumAfterAptc() {
		return lbpPremiumAfterAptc;
	}

	public void setLbpPremiumAfterAptc(String lbpPremiumAfterAptc) {
		this.lbpPremiumAfterAptc = lbpPremiumAfterAptc;
	}

	public String getLbpPremiumBeforeAptc() {
		return lbpPremiumBeforeAptc;
	}

	public void setLbpPremiumBeforeAptc(String lbpPremiumBeforeAptc) {
		this.lbpPremiumBeforeAptc = lbpPremiumBeforeAptc;
	}

	public String getMedicaidEligible() {
		return medicaidEligible;
	}

	public void setMedicaidEligible(String medicaidEligible) {
		this.medicaidEligible = medicaidEligible;
	}

	public String getMedicareEligible() {
		return medicareEligible;
	}

	public void setMedicareEligible(String medicareEligible) {
		this.medicareEligible = medicareEligible;
	}

	public String getChipEligible() {
		return chipEligible;
	}

	public void setChipEligible(String chipEligible) {
		this.chipEligible = chipEligible;
	}

	public String getChipHousehold() {
		return chipHousehold;
	}

	public void setChipHousehold(String chipHousehold) {
		this.chipHousehold = chipHousehold;
	}

	public String getMedicareHousehold() {
		return medicareHousehold;
	}

	public void setMedicareHousehold(String medicareHousehold) {
		this.medicareHousehold = medicareHousehold;
	}

	public int getNoOfAptcCsr() {
		return noOfAptcCsr;
	}

	public void setNoOfAptcCsr(int noOfAptcCsr) {
		this.noOfAptcCsr = noOfAptcCsr;
	}

	public int getNoOfChip() {
		return noOfChip;
	}

	public void setNoOfChip(int noOfChip) {
		this.noOfChip = noOfChip;
	}

	public int getNoOfMedicare() {
		return noOfMedicare;
	}

	public void setNoOfMedicare(int noOfMedicare) {
		this.noOfMedicare = noOfMedicare;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(", members=").append(members);
		builder.append(", aptc=").append(aptc);
		builder.append(", csrLevel=").append(csrLevel);
	//	builder.append(", allPlansAvailable=").append(allPlansAvailable);
		builder.append(", expectedMonthlyPremium=").append(expectedMonthlyPremium);
		builder.append(", medicaidEligible=").append(medicaidEligible);
		builder.append(", medicareEligible=").append(medicareEligible);
		builder.append(", chipEligible=").append(chipEligible);
		builder.append(",status=").append(status);
		builder.append("]");
		return builder.toString();
	}

	

	public String getMedicaidHousehold() {
		return medicaidHousehold;
	}

	public void setMedicaidHousehold(String medicaidHousehold) {
		this.medicaidHousehold = medicaidHousehold;
	}

	
	public String getCensusType() {
		return censusType;
	}

	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}

	public double getEmployerContributionAmount() {
		return employerContributionAmount;
	}

	public void setEmployerContributionAmount(double employerContributionAmount) {
		this.employerContributionAmount = employerContributionAmount;
	}

	public double getHraAmount() {
		return hraAmount;
	}

	public void setHraAmount(double hraAmount) {
		this.hraAmount = hraAmount;
	}

	public double getCobraPremium() {
		return cobraPremium;
	}

	public void setCobraPremium(double cobraPremium) {
		this.cobraPremium = cobraPremium;
	}

	public String getCobraElectionDueDate() {
		return cobraElectionDueDate;
	}

	public void setCobraElectionDueDate(String cobraElectionDueDate) {
		this.cobraElectionDueDate = cobraElectionDueDate;
	}

	public String getHraTransitionDate() {
		return hraTransitionDate;
	}

	public void setHraTransitionDate(String hraTransitionDate) {
		this.hraTransitionDate = hraTransitionDate;
	}

	
	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public PhixRetireeHousehold getRetireeData() {
		return retireeData;
	}

	public void setRetireeData(PhixRetireeHousehold retireeData) {
		this.retireeData = retireeData;
	}

	/*public String getApplicantFirstName() {
		return applicantFirstName;
	}

	public void setApplicantFirstName(String applicantFirstName) {
		this.applicantFirstName = applicantFirstName;
	}

	public String getApplicantLastName() {
		return applicantLastName;
	}

	public void setApplicantLastName(String applicantLastName) {
		this.applicantLastName = applicantLastName;
	}
*/
	public String getIvrNumber() {
		return ivrNumber;
	}

	public void setIvrNumber(String ivrNumber) {
		this.ivrNumber = ivrNumber;
	}

}
