/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Srikanth Nakka
 *
 */
public class CarrierInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "name cannot be empty.")
	private String name;
	
	private String groupName;
	private String hiosId;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the hiosId
	 */
	public String getHiosId() {
		return hiosId;
	}
	/**
	 * @param hiosId the hiosId to set
	 */
	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}
	
	

}
