package com.getinsured.eligibility.enums;

/**
 * @author chopra_s
 * 
 */
public enum ReferralActivationEnum {
	INITIAL_PHASE, START_SECURITY, SECURITY_FAILED, CSR_VERIFICATION_PENDING, COVERAGE_YEAR_PENDING, RIDP_PENDING, LCE_PENDING, ACTIVATION_COMPELETE, CANCEL_ACTIVATION;

	public String value() {
		return name();
	}

	public static ReferralActivationEnum fromValue(String v) {
		ReferralActivationEnum data = null;
		for (ReferralActivationEnum c : ReferralActivationEnum.class
				.getEnumConstants()) {
			if (c.value().equals(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	public String toString() {
		return value();
	}
}
