/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author vasani_s
 * @since 20/03/2015 
 * 
 */
@Entity
@Table(name="ENRL_IN_RECON_REPORT")
public class EnrollmentInboundReconciliationReport implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_IN_RECON_REPORT_SEQ")
	@SequenceGenerator(name = "ENRL_IN_RECON_REPORT_SEQ", sequenceName = "ENRL_IN_RECON_REPORT_SEQ", allocationSize = 1)
	private int id;

	@Column(name="ENROLLMENT_ID")
	private Integer enrollmentId ;
	
	@Column(name="MEMBER_ID")
	private String memberId ;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="EVENT_ID")
	private EnrollmentEvent event ;  
	
	@Column(name="BATCH_EXECUTION_ID")
	private Long batchExecutionId ;

	@Column(name="XML_FILE_NAME")
	private String xmlFileName ;
	
	@Column(name="HIOS_ISSUER_ID")
	private String hiosIssuerId ;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE", nullable = false)
	private Date createdOn;
	
	@Column(name="ISSUER_NAME")
	private String issuerName ;
	
	@Column(name="CAREER_SENT_STATUS")
	private String careerSentStatus;
	
	@Column(name = "CARRIER_SENT_START_DATE")
	private Date carrierSentStartDate;
	
	@Column(name = "CARRIER_SENT_END_DATE")
	private Date carrierSentEndDate;
	
	@Column(name="ERROR_MESSAGE")
	private String errorMessage;
	
	@Column(name="CARRIER_SENT_MRC")
	private String carrierSentMrc;
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId;
	
	@Column(name = "ISA09")
	private String isa09;
	
	@Column(name = "ISA10")
	private String isa10;
	
	@Column(name = "ISA13")
	private Integer isa13;
	
	@Column(name = "GS04")
	private String gs04;
	
	@Column(name = "GS05")
	private String gs05;
	
	@Column(name = "GS06")
	private Integer gs06;
	
	@Column(name = "ST02")
	private String st02;
	
	@Column(name="EXCHG_RECON_ID")
	private Integer exchReconId;
	
	@Column(name="EDI_STATUS")
	private String ediStatus;
	
	@Column(name = "ACK_TA1_STATUS")
	private String ackTA1Status;
	
	@Column(name = "ACK_TA1_REVD_DATE")
	private Date ackTA1ReceivedDate;
	
	@Column(name = "ACK_999_STATUS")
	private String ack999Status;
	
	@Column(name = "ACK_999_REVD_DATE")
	private Date ack999ReceivedDate;
	
	public String getCareerSentStatus() {
		return careerSentStatus;
	}

	public void setCareerSentStatus(String careerSentStatus) {
		this.careerSentStatus = careerSentStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer getId() {
		return id;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Long getBatchExecutionId() {
		return batchExecutionId;
	}

	public void setBatchExecutionId(Long batchExecutionId) {
		this.batchExecutionId = batchExecutionId;
	}

	public String getXmlFileName() {
		return xmlFileName;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
	}

	public EnrollmentEvent getEvent() {
		return event;
	}

	public void setEvent(EnrollmentEvent event) {
		this.event = event;
	}

	public Date getCarrierSentStartDate() {
		return carrierSentStartDate;
	}

	public void setCarrierSentStartDate(Date carrierSentStartDate) {
		this.carrierSentStartDate = carrierSentStartDate;
	}

	public Date getCarrierSentEndDate() {
		return carrierSentEndDate;
	}

	public void setCarrierSentEndDate(Date carrierSentEndDate) {
		this.carrierSentEndDate = carrierSentEndDate;
	}

	public String getCarrierSentMrc() {
		return carrierSentMrc;
	}

	public void setCarrierSentMrc(String carrierSentMrc) {
		this.carrierSentMrc = carrierSentMrc;
	}

	/**
	 * @return the houseHoldCaseId
	 */
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	/**
	 * @param houseHoldCaseId the houseHoldCaseId to set
	 */
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	/**
	 * @return the isa09
	 */
	public String getIsa09() {
		return isa09;
	}

	/**
	 * @param isa09 the isa09 to set
	 */
	public void setIsa09(String isa09) {
		this.isa09 = isa09;
	}

	/**
	 * @return the isa10
	 */
	public String getIsa10() {
		return isa10;
	}

	/**
	 * @param isa10 the isa10 to set
	 */
	public void setIsa10(String isa10) {
		this.isa10 = isa10;
	}

	/**
	 * @return the isa13
	 */
	public Integer getIsa13() {
		return isa13;
	}

	/**
	 * @param isa13 the isa13 to set
	 */
	public void setIsa13(Integer isa13) {
		this.isa13 = isa13;
	}

	/**
	 * @return the gs04
	 */
	public String getGs04() {
		return gs04;
	}

	/**
	 * @param gs04 the gs04 to set
	 */
	public void setGs04(String gs04) {
		this.gs04 = gs04;
	}

	/**
	 * @return the gs05
	 */
	public String getGs05() {
		return gs05;
	}

	/**
	 * @param gs05 the gs05 to set
	 */
	public void setGs05(String gs05) {
		this.gs05 = gs05;
	}

	/**
	 * @return the gs06
	 */
	public Integer getGs06() {
		return gs06;
	}

	/**
	 * @param gs06 the gs06 to set
	 */
	public void setGs06(Integer gs06) {
		this.gs06 = gs06;
	}

	/**
	 * @return the st02
	 */
	public String getSt02() {
		return st02;
	}

	/**
	 * @param st02 the st02 to set
	 */
	public void setSt02(String st02) {
		this.st02 = st02;
	}

	/**
	 * @return the exchReconId
	 */
	public Integer getExchReconId() {
		return exchReconId;
	}

	/**
	 * @param exchReconId the exchReconId to set
	 */
	public void setExchReconId(Integer exchReconId) {
		this.exchReconId = exchReconId;
	}

	/**
	 * @return the ediStatus
	 */
	public String getEdiStatus() {
		return ediStatus;
	}

	/**
	 * @param ediStatus the ediStatus to set
	 */
	public void setEdiStatus(String ediStatus) {
		this.ediStatus = ediStatus;
	}

	/**
	 * @return the ackTA1Status
	 */
	public String getAckTA1Status() {
		return ackTA1Status;
	}

	/**
	 * @param ackTA1Status the ackTA1Status to set
	 */
	public void setAckTA1Status(String ackTA1Status) {
		this.ackTA1Status = ackTA1Status;
	}

	/**
	 * @return the ackTA1ReceivedDate
	 */
	public Date getAckTA1ReceivedDate() {
		return ackTA1ReceivedDate;
	}

	/**
	 * @param ackTA1ReceivedDate the ackTA1ReceivedDate to set
	 */
	public void setAckTA1ReceivedDate(Date ackTA1ReceivedDate) {
		this.ackTA1ReceivedDate = ackTA1ReceivedDate;
	}

	/**
	 * @return the ack999Status
	 */
	public String getAck999Status() {
		return ack999Status;
	}

	/**
	 * @param ack999Status the ack999Status to set
	 */
	public void setAck999Status(String ack999Status) {
		this.ack999Status = ack999Status;
	}

	/**
	 * @return the ack999ReceivedDate
	 */
	public Date getAck999ReceivedDate() {
		return ack999ReceivedDate;
	}

	/**
	 * @param ack999ReceivedDate the ack999ReceivedDate to set
	 */
	public void setAck999ReceivedDate(Date ack999ReceivedDate) {
		this.ack999ReceivedDate = ack999ReceivedDate;
	}
}
