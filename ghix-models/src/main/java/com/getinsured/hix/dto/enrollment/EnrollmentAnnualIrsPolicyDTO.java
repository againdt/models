/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Enrollment DTO for Annual IRS report processing
 * @author negi_s
 *
 */
public class EnrollmentAnnualIrsPolicyDTO implements Serializable{
	private Integer enrollmentId;
    private String marketPlacePolicyNum;
    private String policyIssuerNm;
    private Date policyStartDt;
    private Date policyTerminationDt;
    private EnrollmentAnnualIrsRecipientDTO recipientInfo;
	private Map<Integer, EnrollmentAnnualIrsMonthlyAmountDTO> policyAmountMap;
	private List<EnrollmentIrsCoveredIndividualDTO> coveredIndividualList;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Map<Integer, EnrollmentAnnualIrsMonthlyAmountDTO> getPolicyAmountMap() {
		return policyAmountMap;
	}
	public void setPolicyAmountMap(
			Map<Integer, EnrollmentAnnualIrsMonthlyAmountDTO> policyAmountMap) {
		this.policyAmountMap = policyAmountMap;
	}
	public String getMarketPlacePolicyNum() {
		return marketPlacePolicyNum;
	}
	public void setMarketPlacePolicyNum(String marketPlacePolicyNum) {
		this.marketPlacePolicyNum = marketPlacePolicyNum;
	}
	public String getPolicyIssuerNm() {
		return policyIssuerNm;
	}
	public void setPolicyIssuerNm(String policyIssuerNm) {
		this.policyIssuerNm = policyIssuerNm;
	}
	public Date getPolicyStartDt() {
		return policyStartDt;
	}
	public void setPolicyStartDt(Date policyStartDt) {
		this.policyStartDt = policyStartDt;
	}
	public Date getPolicyTerminationDt() {
		return policyTerminationDt;
	}
	public void setPolicyTerminationDt(Date policyTerminationDt) {
		this.policyTerminationDt = policyTerminationDt;
	}
	public EnrollmentAnnualIrsRecipientDTO getRecipientInfo() {
		return recipientInfo;
	}
	public void setRecipientInfo(EnrollmentAnnualIrsRecipientDTO recipientInfo) {
		this.recipientInfo = recipientInfo;
	}
	public List<EnrollmentIrsCoveredIndividualDTO> getCoveredIndividualList() {
		return coveredIndividualList;
	}
	public void setCoveredIndividualList(
			List<EnrollmentIrsCoveredIndividualDTO> coveredIndividualList) {
		this.coveredIndividualList = coveredIndividualList;
	}
	
	@Override
	public String toString() {
		return "EnrollmentAnnualIrsPolicyDTO [enrollmentId=" + enrollmentId
				+ ", marketPlacePolicyNum=" + marketPlacePolicyNum
				+ ", policyIssuerNm=" + policyIssuerNm + ", policyStartDt="
				+ policyStartDt + ", policyTerminationDt="
				+ policyTerminationDt + ", recipientInfo=" + recipientInfo
				+ ", policyAmountMap=" + policyAmountMap
				+ ", coveredIndividualList=" + coveredIndividualList + "]";
	} 

}
