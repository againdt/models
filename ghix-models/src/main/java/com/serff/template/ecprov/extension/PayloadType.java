//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.18 at 12:55:43 PM PDT 
//


package com.serff.template.ecprov.extension;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.serff.template.ecprov.hix.core.OrganizationType;
import com.serff.template.ecprov.hix.pm.HealthcareProviderType;
import com.serff.template.ecprov.hix.pm.IssuerType;
import com.serff.template.ecprov.niem.core.EntityType;
import com.serff.template.ecprov.niem.structures.ComplexObjectType;



/**
 * A data type for A Documented Component
 * 
 * <p>Java class for PayloadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayloadType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ecp.ffe.cms.gov/extension/1.0}Issuer"/>
 *         &lt;element ref="{http://ecp.ffe.cms.gov/extension/1.0}IssuerOrganization"/>
 *         &lt;element ref="{http://ecp.ffe.cms.gov/extension/1.0}HealthcareProviderEntityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ecp.ffe.cms.gov/extension/1.0}HealthcareProvider" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issuer",
    "issuerOrganization",
    "healthcareProviderEntityType",
    "healthcareProvider"
})
@XmlRootElement(name="Payload", namespace="http://ecp.ffe.cms.gov/exchange/1.0")
public class PayloadType
    extends ComplexObjectType
{

    @XmlElement(name = "Issuer", required = true)
    protected IssuerType issuer;
    @XmlElement(name = "IssuerOrganization", required = true)
    protected OrganizationType issuerOrganization;
    @XmlElement(name = "HealthcareProviderEntityType")
    protected List<EntityType> healthcareProviderEntityType;
    @XmlElement(name = "HealthcareProvider")
    protected List<HealthcareProviderType> healthcareProvider;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerType }
     *     
     */
    public IssuerType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerType }
     *     
     */
    public void setIssuer(IssuerType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the issuerOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationType }
     *     
     */
    public OrganizationType getIssuerOrganization() {
        return issuerOrganization;
    }

    /**
     * Sets the value of the issuerOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationType }
     *     
     */
    public void setIssuerOrganization(OrganizationType value) {
        this.issuerOrganization = value;
    }

    /**
     * Gets the value of the healthcareProviderEntityType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the healthcareProviderEntityType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHealthcareProviderEntityType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityType }
     * 
     * 
     */
    public List<EntityType> getHealthcareProviderEntityType() {
        if (healthcareProviderEntityType == null) {
            healthcareProviderEntityType = new ArrayList<EntityType>();
        }
        return this.healthcareProviderEntityType;
    }

    /**
     * Gets the value of the healthcareProvider property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the healthcareProvider property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHealthcareProvider().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HealthcareProviderType }
     * 
     * 
     */
    public List<HealthcareProviderType> getHealthcareProvider() {
        if (healthcareProvider == null) {
            healthcareProvider = new ArrayList<HealthcareProviderType>();
        }
        return this.healthcareProvider;
    }

}
