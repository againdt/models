package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * HIX-60431
 * This is pojo class for ISSUERS_EXT database table
 * @author sharma_va
 */
@Audited
@Entity
@Table(name = "ISSUERS_EXT")
public class IssuerExt implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum DataPartnerName {
		QUOTIT;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ISSUERS_EXT_SEQ")
	@SequenceGenerator(name = "ISSUERS_EXT_SEQ", sequenceName = "ISSUERS_EXT_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "ISSUER_ID")
	private Issuer issuer;

	@Column(name = "CARRIER_ID")
	private Integer carrierId;

	@Column(name = "INSURANCE_TYPE")
	private String insuranceType;

	@Column(name = "LOGO_LARGE")
	private String logoLarge;

	@Column(name = "LOGO_MEDIUM")
	private String logoMedium;

	@Column(name = "LOGO_SMALL")
	private String logoSmall;

	@Column(name = "DATA_PARTNER_NAME")
	private String dataPartnerName;

	@Column(name = "VERSION")
	private String version;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	public IssuerExt() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
//		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}*/

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Integer getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(Integer carrierId) {
		this.carrierId = carrierId;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getLogoLarge() {
		return logoLarge;
	}

	public void setLogoLarge(String logoLarge) {
		this.logoLarge = logoLarge;
	}

	public String getLogoMedium() {
		return logoMedium;
	}

	public void setLogoMedium(String logoMedium) {
		this.logoMedium = logoMedium;
	}

	public String getLogoSmall() {
		return logoSmall;
	}

	public void setLogoSmall(String logoSmall) {
		this.logoSmall = logoSmall;
	}

	public String getDataPartnerName() {
		return dataPartnerName;
	}

	public void setDataPartnerName(String dataPartnerName) {
		this.dataPartnerName = dataPartnerName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
