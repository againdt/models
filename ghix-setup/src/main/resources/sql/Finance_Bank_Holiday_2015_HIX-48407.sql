insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'New Years Day', to_date('01-01-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Birthday of Martin Luther King, Jr.', to_date('01-19-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Washingtons Birthday', to_date('02-16-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Memorial Day', to_date('05-25-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Independence Day', to_date('07-04-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Labor Day', to_date('09-07-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Columbus Day', to_date('10-12-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Veterans Day', to_date('11-11-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Thanksgiving Day', to_date('11-26-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

insert into bank_holiday (ID, OCCASION, HOLIDAY_DATE,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP,HOLIDAY_YEAR)
values(bank_holiday_seq.nextVal, 'Christmas Day', to_date('12-25-2015','mm-dd-yyyy'), current_timestamp, current_timestamp, '2015');

commit;