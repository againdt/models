package com.getinsured.hix.dto.shop.eps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PlanDTO implements Comparable<PlanDTO> {
	private Integer planId;
	private String name;
	private String level;
	private String issuer;
	private String issuerText;
	private String networkType;
	private Map<String, HashMap<String, String>> planBenefits;
	
	
	private String coInsurance;
	private String deductible;	
	private String officeVisit;
	private String oopMax;
	
	private Float totalPremium=0f;
	private Float indvPremium=0f;
	private Float depenPremium=0f;
	
	//------calculated fields from web rule service
	private Float employerTotalCost=0f;
	private Float employerIndvCost=0f;
	private Float employeeTotalCost=0f;
	private Float dependentTotalCost=0f;
	
	private List<Integer> employeeIds = new ArrayList<Integer>();
	private Map<Integer,EmployeePremiumRates> employeePremiumRates = new HashMap<Integer, EmployeePremiumRates>();
	private String issuerId;
	private String sbcDocUrl;
	
	
	public void addEmployeesPremiums(Float tPremium, Float iPremium,Float dPremium,Integer employeeId){
			totalPremium += tPremium;
			indvPremium += iPremium;
			depenPremium += dPremium;
			
			employeePremiumRates.put(employeeId, new EmployeePremiumRates(tPremium,iPremium,dPremium));
			employeeIds.add(employeeId);
	}
	
	//Getters and Setters 
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getIssuerText() {
		return issuerText;
	}
	public void setIssuerText(String issuerText) {
		this.issuerText = issuerText;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public Map<String, HashMap<String, String>> getPlanBenefits() {
		return planBenefits;
	}
	public void setPlanBenefits(Map<String, HashMap<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}
	public String getCoInsurance() {
		return coInsurance;
	}
	public void setCoInsurance(String coInsurance) {
		this.coInsurance = coInsurance;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public Float getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(Float totalPremium) {
		this.totalPremium = totalPremium;
	}
	public Float getIndvPremium() {
		return indvPremium;
	}
	public void setIndvPremium(Float indvPremium) {
		this.indvPremium = indvPremium;
	}
	public Float getDepenPremium() {
		return depenPremium;
	}
	public void setDepenPremium(Float depenPremium) {
		this.depenPremium = depenPremium;
	}
	public Float getEmployerTotalCost() {
		return employerTotalCost;
	}

	public void setEmployerTotalCost(Float employerTotalCost) {
		this.employerTotalCost = employerTotalCost;
	}

	public Float getEmployerIndvCost() {
		return employerIndvCost;
	}

	public void setEmployerIndvCost(Float employerIndvCost) {
		this.employerIndvCost = employerIndvCost;
	}

	public Float getEmployeeTotalCost() {
		return employeeTotalCost;
	}

	public void setEmployeeTotalCost(Float employeeTotalCost) {
		this.employeeTotalCost = employeeTotalCost;
	}

	public Float getDependentTotalCost() {
		return dependentTotalCost;
	}

	public void setDependentTotalCost(Float dependentTotalCost) {
		this.dependentTotalCost = dependentTotalCost;
	}

	public List<Integer> getEmployeeIds() {
		return employeeIds;
	}
	public void setEmployeeIds(List<Integer> employeeIds) {
		this.employeeIds = employeeIds;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((totalPremium == null) ? 0 : totalPremium.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanDTO other = (PlanDTO) obj;
		if (totalPremium == null) {
			if (other.totalPremium != null)
				return false;
		} else if (!totalPremium.equals(other.totalPremium))
			return false;
		return true;
	}

	@Override
	public int compareTo(PlanDTO o) {
		return totalPremium.compareTo(o.totalPremium);
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getSbcDocUrl() {
		return sbcDocUrl;
	}

	public void setSbcDocUrl(String sbcDocUrl) {
		this.sbcDocUrl = sbcDocUrl;
	}

	public Map<Integer, EmployeePremiumRates> getEmployeePremiumRates() {
		return employeePremiumRates;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getOopMax() {
		return oopMax;
	}

	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}
}
