package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class HouseholdMember {

	@Expose
	private Integer personId;
	@Expose
	private Name name=new Name();
	@Expose
	private String gender;
	@Expose
	private String dateOfBirth;
	@Expose
	private String memberEligibility;
	@Expose
	private String medicaidChild;
	@Expose
	private SocialSecurityCard socialSecurityCard=new SocialSecurityCard();
	@Expose
	private HouseholdContact householdContact=new HouseholdContact();
	@Expose
	private Boolean marriedIndicator;
	@Expose
	private Boolean applyingForCoverageIndicator;
	@Expose
	private CitizenshipImmigrationStatus citizenshipImmigrationStatus=new CitizenshipImmigrationStatus();
	@Expose
	private HealthCoverage healthCoverage=new HealthCoverage();
	@Expose
	private SpecialCircumstances specialCircumstances=new SpecialCircumstances();
	@Expose
	private Boolean personVerifiedIndicator;
	
	//Address
	@Expose
	private Boolean livesWithHouseholdContactIndicator;
	@Expose
	private Boolean sameMailingAddressWithHouseholdContactIndicator = true;
	
	//Values = CHIP, Medicare, APTC, CSR, APTC/CSR,IndainEligible, Ineligible Default is NA (for complete household Medicaid case)
	@Expose
	private String householdMemberEligibility;
	@Expose
	private String coveredCAMemberEligibility;
	@Expose
	private String coveredCAEligibilityStatus;
	
	
	
	
	public String getHouseholdMemberEligibility() {
		return householdMemberEligibility;
	}

	public void setHouseholdMemberEligibility(String householdMemberEligibility) {
		this.householdMemberEligibility = householdMemberEligibility;
	}

	public Boolean getPersonVerifiedIndicator() {
			return personVerifiedIndicator;
		}

	public void setPersonVerifiedIndicator(Boolean personVerifiedIndicator) {
			this.personVerifiedIndicator = personVerifiedIndicator;
		}
	
	public String getMemberEligibility() {
		return memberEligibility;
	}

	public void setMemberEligibility(String memberEligibility) {
		this.memberEligibility = memberEligibility;
	}
	
	public String getMedicaidChild() {
		return medicaidChild;
	}

	public void setMedicaidChild(String medicaidChild) {
		this.medicaidChild = medicaidChild;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public SpecialCircumstances getSpecialCircumstances() {
		return specialCircumstances;
	}

	public void setSpecialCircumstances(SpecialCircumstances specialCircumstances) {
		this.specialCircumstances = specialCircumstances;
	}

	public Boolean getLivesWithHouseholdContactIndicator() {
		return livesWithHouseholdContactIndicator;
	}

	public void setLivesWithHouseholdContactIndicator(Boolean livesWithHouseholdContactIndicator) {
		this.livesWithHouseholdContactIndicator = livesWithHouseholdContactIndicator;
	}

	public SocialSecurityCard getSocialSecurityCard() {
		return socialSecurityCard;
	}

	public void setSocialSecurityCard(SocialSecurityCard socialSecurityCard) {
		this.socialSecurityCard = socialSecurityCard;
	}


	public CitizenshipImmigrationStatus getCitizenshipImmigrationStatus() {
		return citizenshipImmigrationStatus;
	}

	public void setCitizenshipImmigrationStatus(CitizenshipImmigrationStatus citizenshipImmigrationStatus) {
		this.citizenshipImmigrationStatus = citizenshipImmigrationStatus;
	}

	
	public Boolean getMarriedIndicator() {
		return marriedIndicator;
	}

	public void setMarriedIndicator(Boolean marriedIndicator) {
		this.marriedIndicator = marriedIndicator;
	}


	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}


	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	public HouseholdContact getHouseholdContact() {
		return householdContact;
	}

	public void setHouseholdContact(HouseholdContact householdContact) {
		this.householdContact = householdContact;
	}

	
	public Boolean getApplyingForCoverageIndicator() {
		return applyingForCoverageIndicator;
	}

	public void setApplyingForCoverageIndicator(Boolean applyingForCoverageIndicator) {
		this.applyingForCoverageIndicator = applyingForCoverageIndicator;
	}

	

	public HealthCoverage getHealthCoverage() {
		return healthCoverage;
	}

	public void setHealthCoverage(HealthCoverage healthCoverage) {
		this.healthCoverage = healthCoverage;
	}

	
	public Boolean getSameMailingAddressWithHouseholdContactIndicator() {
		return sameMailingAddressWithHouseholdContactIndicator;
	}

	public void setSameMailingAddressWithHouseholdContactIndicator(
			Boolean sameMailingAddressWithHouseholdContactIndicator) {
		this.sameMailingAddressWithHouseholdContactIndicator = sameMailingAddressWithHouseholdContactIndicator;
	}

	public String getCoveredCAMemberEligibility() {
		return coveredCAMemberEligibility;
	}

	public void setCoveredCAMemberEligibility(String coveredCAMemberEligibility) {
		this.coveredCAMemberEligibility = coveredCAMemberEligibility;
	}

	public String getCoveredCAEligibilityStatus() {
		return coveredCAEligibilityStatus;
	}

	public void setCoveredCAEligibilityStatus(String coveredCAEligibilityStatus) {
		this.coveredCAEligibilityStatus = coveredCAEligibilityStatus;
	}
	
	

}
