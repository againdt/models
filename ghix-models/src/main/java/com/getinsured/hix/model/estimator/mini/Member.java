/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Family Member Object for the Mini Estimator API
 * @author Nikhil Talreja
 * @since 10 July, 2013
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Member {
	
	private int memberNumber;
	
	private String memberId; 
	
	private Double hraAmount; 
	
	private Double employerContributionAmount; 
	
	private String dateOfBirth;
	
	private String isTobaccoUser="N";
	
	private String isSeekingCoverage = "Y";
	
	private String isNativeAmerican = "N"; // As per assaf ,this holds good for alaska Natives also
	
	//Values = CHIP, Medicare, APTC, CSR, APTC/CSR,IndainEligible, Ineligible Default is NA (for complete household Medicaid case)
	private String memberEligibility = "NA";
	
	private String medicaidChild="N";
	
	private String isPregnant="N";

	private MemberRelationships relationshipToPrimary;
	
	private String firstName;
	
	private String lastName;

	private String gender;
	
	private List<String> seekingCoverageProducts = new ArrayList<String>();
	
	public enum MemberType {  
		EMPLOYEE, SPOUSE, CHILD1, CHILD2, CHILD3, CHILD4, CHILD5; 
	}

	private MemberType memberType;
	
	private String externalMemberId; 
	private Double individualHraAmount; 
	
	public MemberType getMemberType() {
		return memberType;
	}

	public void setMemberType(MemberType memberType) {
		this.memberType = memberType;
	}

	public int getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(int memberNumber) {
		this.memberNumber = memberNumber;
	}
	
	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public Double getIndividualHraAmount() {
		return individualHraAmount;
	}

	public void setIndividualHraAmount(Double individualHraAmount) {
		this.individualHraAmount = individualHraAmount;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getIsTobaccoUser() {
		return isTobaccoUser;
	}
	
	public void setIsTobaccoUser(String isTobaccoUser) {
		this.isTobaccoUser = isTobaccoUser;
	}
	
	public String getIsSeekingCoverage() {
		return isSeekingCoverage;
	}

	public void setIsSeekingCoverage(String isSeekingCoverage) {
		this.isSeekingCoverage = isSeekingCoverage;
	}
	
	
	public String getMemberEligibility() {
		return memberEligibility;
	}

	public void setMemberEligibility(String memberEligibility) {
		this.memberEligibility = memberEligibility;
	}

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}
	
	public String getIsNativeAmerican() {
		return isNativeAmerican;
	}

	public void setIsNativeAmerican(String isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}
	public String getMedicaidChild() {
		return medicaidChild;
	}

	public void setMedicaidChild(String medicaidChild) {
		this.medicaidChild = medicaidChild;
	}

	public String getIsPregnant() {
		return isPregnant;
	}

	public void setIsPregnant(String isPregnant) {
		this.isPregnant = isPregnant;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FamilyMember [memberNumber=");
		builder.append(memberNumber);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", isTobaccoUser=");
		builder.append(isTobaccoUser);
		builder.append(", isSeekingCoverage=");
		builder.append(isSeekingCoverage);
		builder.append(",isNativeAmerican=");
		builder.append(isNativeAmerican);
		builder.append(", memberEligibility=");
		builder.append(memberEligibility);
		builder.append(", relationshipToPrimary=").append(relationshipToPrimary);
		builder.append("]");
		return builder.toString();
	}
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	public class MemberComparatorByMemberNumber implements Comparator<Member>{
		@Override
		public int compare(Member memberObj1, Member memberObj2) {
			return memberObj1.getMemberNumber() < memberObj2.getMemberNumber() ? -1 : (memberObj1.getMemberNumber() == memberObj2.getMemberNumber() ? 0 : 1) ;
		}

	}


	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Double getHraAmount() {
		return hraAmount;
	}

	public void setHraAmount(Double hraAmount) {
		this.hraAmount = hraAmount;
	}

	public Double getEmployerContributionAmount() {
		return employerContributionAmount;
	}

	public void setEmployerContributionAmount(Double employerContributionAmount) {
		this.employerContributionAmount = employerContributionAmount;
	}

	public List<String> getSeekingCoverageProducts() {
		return seekingCoverageProducts;
	}

	public void setSeekingCoverageProducts(List<String> seekingCoverageProducts) {
		this.seekingCoverageProducts = seekingCoverageProducts;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
}
