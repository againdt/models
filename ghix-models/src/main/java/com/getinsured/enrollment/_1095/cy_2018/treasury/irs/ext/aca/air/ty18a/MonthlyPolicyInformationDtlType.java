
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Month by month Policy information 
 * 
 * <p>Java class for MonthlyPolicyInformationDtlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MonthlyPolicyInformationDtlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TotalQHPMonthlyPremiumAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}SLCSPAdjMonthlyPremiumAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}HouseholdAPTCAmt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthlyPolicyInformationDtlType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "totalQHPMonthlyPremiumAmt",
    "slcspAdjMonthlyPremiumAmt",
    "householdAPTCAmt"
})
public class MonthlyPolicyInformationDtlType {

    @XmlElement(name = "TotalQHPMonthlyPremiumAmt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected BigDecimal totalQHPMonthlyPremiumAmt;
    @XmlElement(name = "SLCSPAdjMonthlyPremiumAmt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected BigDecimal slcspAdjMonthlyPremiumAmt;
    @XmlElement(name = "HouseholdAPTCAmt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected BigDecimal householdAPTCAmt;

    /**
     * Gets the value of the totalQHPMonthlyPremiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalQHPMonthlyPremiumAmt() {
        return totalQHPMonthlyPremiumAmt;
    }

    /**
     * Sets the value of the totalQHPMonthlyPremiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalQHPMonthlyPremiumAmt(BigDecimal value) {
        this.totalQHPMonthlyPremiumAmt = value;
    }

    /**
     * Gets the value of the slcspAdjMonthlyPremiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSLCSPAdjMonthlyPremiumAmt() {
        return slcspAdjMonthlyPremiumAmt;
    }

    /**
     * Sets the value of the slcspAdjMonthlyPremiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSLCSPAdjMonthlyPremiumAmt(BigDecimal value) {
        this.slcspAdjMonthlyPremiumAmt = value;
    }

    /**
     * Gets the value of the householdAPTCAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHouseholdAPTCAmt() {
        return householdAPTCAmt;
    }

    /**
     * Sets the value of the householdAPTCAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHouseholdAPTCAmt(BigDecimal value) {
        this.householdAPTCAmt = value;
    }

}
