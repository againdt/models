--BROKERS table
COMMENT ON COLUMN BROKERS.ID IS 'Brokers Identification number';
COMMENT ON COLUMN BROKERS.APPLICATION_DATE IS 'Date when Broker application is created ';
COMMENT ON COLUMN BROKERS.AREA_SERVED IS 'Currently not in use';
COMMENT ON COLUMN BROKERS.CERTIFICATION_DATE IS 'Date when Broker is certified';
COMMENT ON COLUMN BROKERS.CERTIFICATION_STATUS IS 'Certification Status of Broker';
COMMENT ON COLUMN BROKERS.COMPANY_NAME IS 'Company name of Broker';
COMMENT ON COLUMN BROKERS.CONTACTNUMBER IS 'Contact Number of Broker';
COMMENT ON COLUMN BROKERS.CREATED IS 'Timestamp when Broker is created';
COMMENT ON COLUMN BROKERS.EDUCATION IS 'Specifies education for Broker';
COMMENT ON COLUMN BROKERS.ESIGNATURE IS 'Currently not in use';
COMMENT ON COLUMN BROKERS.LANGUAGES_SPOKEN IS 'Specifies the language spoken by Broker';
COMMENT ON COLUMN BROKERS.LICENSE_NUMBER IS 'Specifies the license number of Broker';
COMMENT ON COLUMN BROKERS.PHOTO IS 'Stores the blob file as photo of Broker';
COMMENT ON COLUMN BROKERS.PRODUCT_EXPERTISE IS 'Specifies the product expertise of Broker ';
COMMENT ON COLUMN BROKERS.UPDATED IS 'Timestamp when Broker record is updated';
COMMENT ON COLUMN BROKERS.LOCATION_ID IS 'Stores the location Id for Broker';
COMMENT ON COLUMN BROKERS.USERID IS 'Stores the userId of Broker';
COMMENT ON COLUMN BROKERS.TRAINING IS 'Currently not in use';
COMMENT ON COLUMN BROKERS.RECERTIFY_DATE IS 'Date when Broker is recertified';
COMMENT ON COLUMN BROKERS.COMMENTS IS 'Stores the Broker Comments';
COMMENT ON COLUMN BROKERS.DECERTIFY_DATE IS 'Date when Broker is decertified';
COMMENT ON COLUMN BROKERS.CLIENTS_SERVED IS 'This specifies the clients served by broker';
COMMENT ON COLUMN BROKERS.MAILING_LOCATON_ID IS 'Stores the mailing location Id of Broker';
COMMENT ON COLUMN BROKERS.ABOUT_ME IS 'This stores the information about Broker';
COMMENT ON COLUMN BROKERS.FEDERAL_EIN IS 'Specifies the federal tax Id for Broker';

--BROKERS_AUD table
COMMENT ON COLUMN BROKERS_AUD.ID IS 'Brokers Identification number';
COMMENT ON COLUMN BROKERS_AUD.REV IS 'Specifies the revision number';
COMMENT ON COLUMN BROKERS_AUD.REVTYPE IS 'Specifies the type of revision';
COMMENT ON COLUMN BROKERS_AUD.APPLICATION_DATE IS 'Date when Broker application is created ';
COMMENT ON COLUMN BROKERS_AUD.AREA_SERVED IS 'Currently not in use';
COMMENT ON COLUMN BROKERS_AUD.CERTIFICATION_DATE IS 'Date when Broker is certified';
COMMENT ON COLUMN BROKERS_AUD.CERTIFICATION_STATUS IS 'Certification Status of Broker';
COMMENT ON COLUMN BROKERS_AUD.COMPANY_NAME IS 'Company name of Broker';
COMMENT ON COLUMN BROKERS_AUD.CONTACTNUMBER IS 'Contact Number of Broker';
COMMENT ON COLUMN BROKERS_AUD.EDUCATION IS 'Specifies education for Broker';
COMMENT ON COLUMN BROKERS_AUD.ESIGNATURE IS 'Currently not in use';
COMMENT ON COLUMN BROKERS_AUD.LANGUAGES_SPOKEN IS 'Specifies the language spoken by Broker';
COMMENT ON COLUMN BROKERS_AUD.LICENSE_NUMBER IS 'Specifies the license number of Broker';
COMMENT ON COLUMN BROKERS_AUD.PHOTO IS 'Stores the blob file as photo of Broker';
COMMENT ON COLUMN BROKERS_AUD.PRODUCT_EXPERTISE IS 'Specifies the product expertise of Broker ';
COMMENT ON COLUMN BROKERS_AUD.UPDATED IS 'Timestamp when Broker record is updated';
COMMENT ON COLUMN BROKERS_AUD.LOCATION_ID IS 'Stores the location Id for Broker';
COMMENT ON COLUMN BROKERS_AUD.USERID IS 'Stores the userId of Broker';
COMMENT ON COLUMN BROKERS_AUD.TRAINING IS 'Currently not in use';
COMMENT ON COLUMN BROKERS_AUD.RECERTIFY_DATE IS 'Date when Broker is recertified';
COMMENT ON COLUMN BROKERS_AUD.COMMENTS IS 'Stores the Broker Comments';
COMMENT ON COLUMN BROKERS_AUD.DECERTIFY_DATE IS 'Date when Broker is decertified';
COMMENT ON COLUMN BROKERS_AUD.MAILING_LOCATON_ID IS 'Stores the mailing location Id of Broker';
COMMENT ON COLUMN BROKERS_AUD.ABOUT_ME IS 'This stores the information about Broker';
COMMENT ON COLUMN BROKERS_AUD.FEDERAL_EIN IS 'Specifies the federal tax Id for Broker';

--DESIGNATE_BROKER table
COMMENT ON COLUMN DESIGNATE_BROKER.ID IS 'Identification Number for Designated Broker ';
COMMENT ON COLUMN DESIGNATE_BROKER.BROKERID IS 'Stores the Foreign key for Broker ID';
COMMENT ON COLUMN DESIGNATE_BROKER.EMPLOYERID IS 'Stores the Foreign key for Employer ID';
COMMENT ON COLUMN DESIGNATE_BROKER.CREATED IS 'Timestamp when Designated Broker record is created';
COMMENT ON COLUMN DESIGNATE_BROKER.UPDATED IS 'Timestamp when Designated Broker record is updated';
COMMENT ON COLUMN DESIGNATE_BROKER.STATUS IS 'Specifies the status of Broker(Active,Inactive,Pending)';
COMMENT ON COLUMN DESIGNATE_BROKER.ESIGN_BY IS 'Specifies the employer name who designated the Broker';
COMMENT ON COLUMN DESIGNATE_BROKER.ESIGN_DATE IS 'Date when Broker is designated by Employer';
COMMENT ON COLUMN DESIGNATE_BROKER.INDIVIDUALID IS 'Stores the Foreign key for Individual ID';
COMMENT ON COLUMN DESIGNATE_BROKER.EXTERNALEMPLOYERID IS 'External employer Identification number';
COMMENT ON COLUMN DESIGNATE_BROKER.EXTERNALINDIVIDUALID IS 'External individual Identification number';

