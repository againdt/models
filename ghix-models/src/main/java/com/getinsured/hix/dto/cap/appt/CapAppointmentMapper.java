package com.getinsured.hix.dto.cap.appt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.cap.util.CapDateTimeUtil;
import com.getinsured.hix.dto.cap.CapAgentEmploymentDto;
import com.getinsured.hix.dto.cap.consumerapp.EligLeadDTO;
import com.getinsured.hix.model.CapAppointment;
import com.getinsured.hix.model.estimator.mini.EligLead;

public class CapAppointmentMapper {

    private static final Logger LOGGER =  LoggerFactory.getLogger(CapAppointmentMapper.class);

    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    public ApptDetailsDto entityToDto(CapAppointment appt, TimeZone timeZone) throws ParseException {
        ApptDetailsDto dto = null;

        if (appt != null) {
            dto = new ApptDetailsDto();
            //AccountUser user = userService.findById(userId);

            if (appt.getAppointmentDate() != null) {
                // show appointment date(UTC) in client's timezone
                dto.setAppointmentDate(CapDateTimeUtil.utcDate2LocalStrDate(appt.getAppointmentDate(), timeZone));
            }

            dto.setComments(appt.getComments());
            dto.setDuration(appt.getDuration());

            if (appt.getEndDate() != null) {
                dto.setEndDate(CapDateTimeUtil.utcDate2LocalStrDate(appt.getEndDate(), timeZone));
            }

            dto.setId(String.valueOf(appt.getId()));

            if (appt.getStartDate() != null) {
                dto.setStartDate(CapDateTimeUtil.utcDate2LocalStrDate(appt.getStartDate(), timeZone));
            }

            dto.setStatus(appt.getStatus());
            dto.setCreatedBy(appt.getCreatedBy());
            dto.setLastUpdatedBy(appt.getUpdatedBy());

            if (appt.getCapAgentEmployment() != null) {
                CapAgentEmploymentDto agentDto = new CapAgentEmploymentDto();
                agentDto.setAgentId(String.valueOf(appt.getCapAgentEmployment().getId()));
                String agentName = "";
                if(appt.getCapAgentEmployment().getUserId() != null){
                	agentName =  appt.getCapAgentEmployment().getUserId().getFirstName() + " " +appt.getCapAgentEmployment().getUserId().getLastName();
                }
                agentDto.setAgentName(agentName);
                dto.setCapAgentDto(agentDto);
            }

            // HIX-98222 Add callStatus field in appointment detail
            if(appt.getCallStatus() != null){
                dto.setCallStatus(appt.getCallStatus());
            }

            // HIX-98416 Backend for Appointment Type
            if(appt.getAppointmentType() != null){
                dto.setAppointmentType(appt.getAppointmentType());
            }

            // HIX-97759 Send out TimeZone in Appointment DTO
            if(appt.getCustomerTimeZone() != null){
                dto.setCustomerTimeZone(appt.getCustomerTimeZone());
            }

            EligLead lead = appt.getEligLead();
            if (lead != null) {
                EligLeadDTO leadDTO = new EligLeadDTO();
                leadDTO.setId(lead.getId());
                leadDTO.setName(lead.getName().replace("~^", " "));
                leadDTO.setPhoneNumber(lead.getPhoneNumber());
                leadDTO.setEmailAddress(lead.getEmailAddress());
                leadDTO.setZipcode(lead.getZipCode());
                leadDTO.setCountyCode(lead.getCountyCode());
                String leadStatus = (lead.getStatus() == null) ? EligLead.STATUS.PENDING.toString() : lead.getStatus().toString();
                leadDTO.setStatus(leadStatus);
                //HIX-87566:	Enable Click-to-Dial for Appointment Scheduler
                leadDTO.setClick2CallURL("https://getinsured.hostedcc.com/mason/agents/outbounddirect.html?phonenum=1" + lead.getPhoneNumber() + "&campaign_id=118879");
                dto.setEligLeadDTO(leadDTO);
            }

            dto.setStateCode(appt.getStateCode());

            // Calculate time slot text
            Date utcApptDate = appt.getAppointmentDate();
            String utcApptStrDate = DATE_TIME_FORMAT.format(utcApptDate);
            LOGGER.info("Appointment UTC Date: " + utcApptStrDate);
            String timeSlot = CapDateTimeUtil.getTimeSlotText(utcApptStrDate, appt.getDuration(), timeZone);

            dto.setTimeSlot(timeSlot);

            dto.setTimezone(timeZone.getID());

            // HIX-98865 Add "CustomerTimeSlot" in the API response
            if(StringUtils.isNotEmpty(appt.getCustomerTimeZone())) { // HIX-98862 Mismatch between the no of assigned appointments on the monthly calendar for the agent
            String customerTimeSlot = CapDateTimeUtil.getTimeSlotText(utcApptStrDate, appt.getDuration(), TimeZone.getTimeZone(appt.getCustomerTimeZone()));
            dto.setCustomerTimeSlot(customerTimeSlot);
        }
        }

        return dto;
    }

}