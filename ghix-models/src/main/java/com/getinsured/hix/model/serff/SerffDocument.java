/**
 *
 */
package com.getinsured.hix.model.serff;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * Model class for SERFF_DOCUMENT table
 * @author Nikhil Talreja
 * @since 06 March, 2013
 */
@Entity
@Table(name="SERFF_DOCUMENT")
@DynamicInsert
@DynamicUpdate
public class SerffDocument implements Serializable {

	private static final long serialVersionUID = -5353923004701462347L;

	public enum DOC_TYPE {
		pdf, jpg, jpeg, bmp, doc, docx, ppt, pptx, xml, xls, xlsx, xlsm, zip, txt, gif, png;
	}

	@Id
	@Column(name = "SERFF_DOC_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERFF_DOCUMENT_SEQ")
	@SequenceGenerator(name = "SERFF_DOCUMENT_SEQ", sequenceName = "SERFF_DOCUMENT_SEQ", allocationSize = 1)
	private long serffDocId;

	@ManyToOne(cascade = {CascadeType.DETACH} )
	@JoinColumn(name = "SERFF_REQ_ID")
	private SerffPlanMgmt serffReqId;

	@Column(name="ECM_DOC_ID")
	private String ecmDocId;

	@Column(name="DOCUMENT_NAME")
	private String docName;

	@Column(name="DOCUMENT_TYPE")
	@Enumerated(EnumType.STRING)
	private DOC_TYPE docType;

	@Column(name="DOCUMENT_SIZE")
	private long docSize;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP", updatable=false)
	private Date createdOn;

	public long getSerffDocId() {
		return serffDocId;
	}

	public void setSerffDocId(long serffDocId) {
		this.serffDocId = serffDocId;
	}

	public SerffPlanMgmt getSerffReqId() {
		return serffReqId;
	}

	public void setSerffReqId(SerffPlanMgmt serffReqId) {
		this.serffReqId = serffReqId;
	}

	public String getEcmDocId() {
		return ecmDocId;
	}

	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public DOC_TYPE getDocType() {
		return docType;
	}

	public void setDocType(DOC_TYPE docType) {
		this.docType = docType;
	}

	public long getDocSize() {
		return docSize;
	}

	public void setDocSize(long docSize) {
		this.docSize = docSize;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@javax.persistence.PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (serffDocId ^ (serffDocId >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SerffDocument)) {
			return false;
		}
		SerffDocument other = (SerffDocument) obj;
		if (serffDocId != other.serffDocId) {
			return false;
		}
		return true;
	}


}
