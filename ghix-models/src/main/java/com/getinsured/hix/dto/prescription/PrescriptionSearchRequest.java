package com.getinsured.hix.dto.prescription;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.planmgmt.PrescriptionSearchDTO;

/**
 * @author Shubhendu Singh (shubhendu.singh@getinsured.com)
 */
public class PrescriptionSearchRequest {

	private String drugNameForTooltip;
	private String drugRxCode;
	private String fairCost;
	private String drugName;
	private List<String> dosage = new ArrayList<String>();
	private String showBrandOrGenericName;
	private List<PrescriptionSearchRequest> associatedDrugs;
	
	public List<PrescriptionSearchRequest> getAssociatedDrugs() {
		return associatedDrugs;
	}

	public void setAssociatedDrugs(List<PrescriptionSearchRequest> associatedDrugs) {
		this.associatedDrugs = associatedDrugs;
	}

	public String getDrugNameForTooltip() {
		return drugNameForTooltip;
	}

	public void setDrugNameForTooltip(String drugNameForTooltip) {
		this.drugNameForTooltip = drugNameForTooltip;
	}

	public String getDrugRxCode() {
		return drugRxCode;
	}

	public void setDrugRxCode(String drugRxCode) {
		this.drugRxCode = drugRxCode;
	}

	public String getFairCost() {
		return fairCost;
	}

	public void setFairCost(String fairCost) {
		this.fairCost = fairCost;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public List<String> getDosage() {
		return dosage;
	}

	public void setDosage(List<String> dosage) {
		this.dosage = dosage;
	}

	public void addDosage(String dosage) {
		if (this.dosage != null) {
			this.dosage.add(dosage);
		}
	}

	public String getShowBrandOrGenericName() {
		return showBrandOrGenericName;
	}

	public void setShowBrandOrGenericName(String showBrandOrGenericName) {
		this.showBrandOrGenericName = showBrandOrGenericName;
	}

}
