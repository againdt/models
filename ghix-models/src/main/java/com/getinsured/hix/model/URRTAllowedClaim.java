package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "pm_urrt_allowed_claim")
@XmlAccessorType(XmlAccessType.NONE)
public class URRTAllowedClaim implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_urrt_allowed_claim_seq")
	@SequenceGenerator(name = "pm_urrt_allowed_claim_seq", sequenceName = "pm_urrt_allowed_claim_seq", allocationSize = 1)
	private int id;
	
	//foreign key.
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pm_urrt_id")
	private UnifiedRate unifiedRate;
	
	@Column(name = "ann_cost_qty")
	private double annualizedCostQuantity;
	
	@Column(name = "ann_util_qty")
	private double annualizedUtilizationQuantity;
	
	@Column(name = "cu_per_k_qty")
	private double credibilityUtilizationPer1000Quantity;
	
	@Column(name = "cac_per_svc_amt")
	private double creditabilityAverageCostPerServiceAmount;
	
	@Column(name = "cred_pmpm_amt")
	private double creditabilityPMPMAmount;
	
	@Column(name = "eac_per_svc_amt")
	private double experienceAverageCostPerServiceAmount;
	
	@Column(name = "exp_pmpm_amt")
	private double experiencePMPMAmount;
	
	@Column(name = "eu_desc_text")
	private String experienceUtilizationDescriptionText;
	
	@Column(name = "eu_per_k_qty")
	private double experienceUtilizationPer1000Quantity;
	
	@Column(name = "oac_qty")
	private double otherAnticipatedChangeQuantity;
	
	@Column(name = "prm_qty")
	private double populationRiskMorbidityQuantity;
	
	@Column(name = "pac_per_svc_amt")
	private double  projectionAverageCostPerServiceAmount;
	
	@Column(name = "proj_pmpm_amt")
	private double  projectionPMPMAmount;
	
	@Column(name = "pu_per_k_qty")
	private double  projectionUtilizationPer1000Quantity;
	
	@Column(name = "db_cost_type")	
	private String definingBenefitCostType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UnifiedRate getUnifiedRate() {
		return unifiedRate;
	}

	public void setUnifiedRate(UnifiedRate unifiedRate) {
		this.unifiedRate = unifiedRate;
	}

	public double getAnnualizedCostQuantity() {
		return annualizedCostQuantity;
	}

	public void setAnnualizedCostQuantity(double annualizedCostQuantity) {
		this.annualizedCostQuantity = annualizedCostQuantity;
	}

	public double getAnnualizedUtilizationQuantity() {
		return annualizedUtilizationQuantity;
	}

	public void setAnnualizedUtilizationQuantity(double annualizedUtilizationQuantity) {
		this.annualizedUtilizationQuantity = annualizedUtilizationQuantity;
	}

	public double getCreditabilityAverageCostPerServiceAmount() {
		return creditabilityAverageCostPerServiceAmount;
	}

	public void setCreditabilityAverageCostPerServiceAmount(
			double creditabilityAverageCostPerServiceAmount) {
		this.creditabilityAverageCostPerServiceAmount = creditabilityAverageCostPerServiceAmount;
	}

	public double getCreditabilityPMPMAmount() {
		return creditabilityPMPMAmount;
	}

	public void setCreditabilityPMPMAmount(double creditabilityPMPMAmount) {
		this.creditabilityPMPMAmount = creditabilityPMPMAmount;
	}

	public double getExperienceAverageCostPerServiceAmount() {
		return experienceAverageCostPerServiceAmount;
	}

	public void setExperienceAverageCostPerServiceAmount(
			double experienceAverageCostPerServiceAmount) {
		this.experienceAverageCostPerServiceAmount = experienceAverageCostPerServiceAmount;
	}

	public double getExperiencePMPMAmount() {
		return experiencePMPMAmount;
	}

	public void setExperiencePMPMAmount(double experiencePMPMAmount) {
		this.experiencePMPMAmount = experiencePMPMAmount;
	}

	public String getExperienceUtilizationDescriptionText() {
		return experienceUtilizationDescriptionText;
	}

	public void setExperienceUtilizationDescriptionText(
			String experienceUtilizationDescriptionText) {
		this.experienceUtilizationDescriptionText = experienceUtilizationDescriptionText;
	}

	public double getOtherAnticipatedChangeQuantity() {
		return otherAnticipatedChangeQuantity;
	}

	public void setOtherAnticipatedChangeQuantity(
			double otherAnticipatedChangeQuantity) {
		this.otherAnticipatedChangeQuantity = otherAnticipatedChangeQuantity;
	}

	public double getPopulationRiskMorbidityQuantity() {
		return populationRiskMorbidityQuantity;
	}

	public void setPopulationRiskMorbidityQuantity(
			double populationRiskMorbidityQuantity) {
		this.populationRiskMorbidityQuantity = populationRiskMorbidityQuantity;
	}

	public double getProjectionAverageCostPerServiceAmount() {
		return projectionAverageCostPerServiceAmount;
	}

	public void setProjectionAverageCostPerServiceAmount(
			double projectionAverageCostPerServiceAmount) {
		this.projectionAverageCostPerServiceAmount = projectionAverageCostPerServiceAmount;
	}

	public double getProjectionPMPMAmount() {
		return projectionPMPMAmount;
	}

	public void setProjectionPMPMAmount(double projectionPMPMAmount) {
		this.projectionPMPMAmount = projectionPMPMAmount;
	}

	public String getDefiningBenefitCostType() {
		return definingBenefitCostType;
	}

	public void setDefiningBenefitCostType(String definingBenefitCostType) {
		this.definingBenefitCostType = definingBenefitCostType;
	}

	public double getCredibilityUtilizationPer1000Quantity() {
		return credibilityUtilizationPer1000Quantity;
	}

	public void setCredibilityUtilizationPer1000Quantity(
			double credibilityUtilizationPer1000Quantity) {
		this.credibilityUtilizationPer1000Quantity = credibilityUtilizationPer1000Quantity;
	}

	public double getExperienceUtilizationPer1000Quantity() {
		return experienceUtilizationPer1000Quantity;
	}

	public void setExperienceUtilizationPer1000Quantity(
			double experienceUtilizationPer1000Quantity) {
		this.experienceUtilizationPer1000Quantity = experienceUtilizationPer1000Quantity;
	}

	public double getProjectionUtilizationPer1000Quantity() {
		return projectionUtilizationPer1000Quantity;
	}

	public void setProjectionUtilizationPer1000Quantity(
			double projectionUtilizationPer1000Quantity) {
		this.projectionUtilizationPer1000Quantity = projectionUtilizationPer1000Quantity;
	}
	
}
