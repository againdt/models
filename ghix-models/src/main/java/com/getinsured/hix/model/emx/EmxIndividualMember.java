package com.getinsured.hix.model.emx;

import java.io.Serializable;

public class EmxIndividualMember implements Serializable{



	private static final long serialVersionUID = 1L;
	
	private String type;
	private long Id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private Long affiliateFlowAccessId;
	private Boolean dataSharingConsent;
	
	private long affiliateId;
	private Integer flowId;
	private long clickId=-1;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Long getAffiliateFlowAccessId() {
		return affiliateFlowAccessId;
	}
	public void setAffiliateFlowAccessId(Long affiliateFlowAccessId) {
		this.affiliateFlowAccessId = affiliateFlowAccessId;
	}
	public long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}
	public Integer getFlowId() {
		return flowId;
	}
	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}
	public long getClickId() {
		return clickId;
	}
	public void setClickId(long clickId) {
		this.clickId = clickId;
	}
	public Boolean getDataSharingConsent() {
		return dataSharingConsent;
	}
	public void setDataSharingConsent(Boolean dataSharingConsent) {
		this.dataSharingConsent = dataSharingConsent;
	}
	
	
}
