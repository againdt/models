package com.getinsured.hix.dto.directenrollment;

import com.getinsured.hix.dto.directenrollment.payment.PaymentValidationResponseDTO;

/**
 * Submit response wrapper
 * @author root
 *
 */
public class SubmitApplicationResponseDTO {
	/**
	 * For some carriers payment validation is part of application submission
	 * This member variable 
	 */
	private PaymentValidationResponseDTO paymentValidationResponse;
	private DirectEnrollmentStatus.SubmitStatus status;
	
	public PaymentValidationResponseDTO getPaymentValidationResponse() {
		return paymentValidationResponse;
	}
	public void setPaymentValidationResponse(
			PaymentValidationResponseDTO paymentValidationResponse) {
		this.paymentValidationResponse = paymentValidationResponse;
	}
	public DirectEnrollmentStatus.SubmitStatus getStatus() {
		return status;
	}
	public void setStatus(DirectEnrollmentStatus.SubmitStatus status) {
		this.status = status;
	}
	

}
