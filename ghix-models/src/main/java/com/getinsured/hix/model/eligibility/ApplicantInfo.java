package com.getinsured.hix.model.eligibility;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * This is a ApplicantInfo model class. Used to retrieve the applicant info for 
 * for requested applicationId.
 * 
 * @author jyotisree
 * @since 02/13/2013
 */
public class ApplicantInfo {
	
	private String applicantType;
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffix;
	private String dob;
	private String ssn;
	private String residenceAddress1;
	private String residenceAddress2;
	private String residenceCity;
	private String residenceState;
	private String residenceZip;

	private String mailingAddress1;
	private String mailingAddress2;
	private String mailingCity;
	private String mailingState;
	private String mailingZip;

	private String email;
	private String contactNumber;
	private String fax; // contactNumber2
	private String smoker;
	private String headOfHouseHold;
	private String subscriber;
	private String gender;
	
	public String getApplicantType() {
		return applicantType;
	}
	public void setApplicantType(String applicantType) {
		this.applicantType = applicantType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getResidenceAddress1() {
		return residenceAddress1;
	}
	public void setResidenceAddress1(String residenceAddress1) {
		this.residenceAddress1 = residenceAddress1;
	}
	public String getResidenceAddress2() {
		return residenceAddress2;
	}
	public void setResidenceAddress2(String residenceAddress2) {
		this.residenceAddress2 = residenceAddress2;
	}
	public String getResidenceCity() {
		return residenceCity;
	}
	public void setResidenceCity(String residenceCity) {
		this.residenceCity = residenceCity;
	}
	public String getResidenceState() {
		return residenceState;
	}
	public void setResidenceState(String residenceState) {
		this.residenceState = residenceState;
	}
	public String getResidenceZip() {
		return residenceZip;
	}
	public void setResidenceZip(String residenceZip) {
		this.residenceZip = residenceZip;
	}
	public String getMailingAddress1() {
		return mailingAddress1;
	}
	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}
	public String getMailingAddress2() {
		return mailingAddress2;
	}
	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	public String getMailingState() {
		return mailingState;
	}
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}
	public String getMailingZip() {
		return mailingZip;
	}
	public void setMailingZip(String mailingZip) {
		this.mailingZip = mailingZip;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getSmoker() {
		return smoker;
	}
	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}
	public String getHeadOfHouseHold() {
		return headOfHouseHold;
	}
	public void setHeadOfHouseHold(String headOfHouseHold) {
		this.headOfHouseHold = headOfHouseHold;
	}
	public String getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String toString() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}
	
}
