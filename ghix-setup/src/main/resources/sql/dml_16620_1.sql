-- Entity Enrollment Status
INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL, 'ENTITY_ENROLLMENT_STATUS', 'string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'TR','Terminated','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'DE','Disenrolled','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'PE','Pending','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'CL','Cancel','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'IN','Initiate' ,'');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'CM','Confirm','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'OC','Order Confirmed','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'PR','Payment Received','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'SB','Submit','');
INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'TR','Term','');
