package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

//@Audited
@Entity
@Table(name="CAP_APPOINTMENTS")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class CapAppointment implements Serializable {

	private static final long serialVersionUID = -7931938613832018443L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAP_APPOINTMENTS_SEQ")
	@SequenceGenerator(name = "CAP_APPOINTMENTS_SEQ", sequenceName = "CAP_APPOINTMENTS_SEQ", allocationSize = 1)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="DURATION")
	private Integer duration; // appt duration : phase 1 : will be from slot config file

	// TODO: change type to ZonedDateTime
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="APPOINTMENT_TIMESTAMP")
	private Date appointmentDate;

	@Column(name="APPOINTMENT_TYPE")
	private String appointmentType;

	@Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="START_TIMESTAMP")
    private Date startDate;
    
	@Temporal(value = TemporalType.TIMESTAMP)
    @Column(name="END_TIMESTAMP")
    private Date endDate;
    
    @Column(name="COMMENTS")
    private String comments;
    
    @Column(name="STATUS")
	private String status;
    
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "LEAD_ID")
	private EligLead eligLead;
    
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "AGENT_ID")
	private CapAgentEmployment capAgentEmployment;
   
    @Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updated;
	
	@Column(name="TENANT_ID")
    private Long tenantId;

	@Column(name="STATE_CODE")
	private String stateCode;

	@Column(name="CONSUMER_COMMENT_ID")
	private Integer consumerCommentId;

	@Column(name="CALL_STATUS")
	private String callStatus;

	@Column(name="CUSTOMER_TIMEZONE")
	private String customerTimeZone;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public EligLead getEligLead() {
		return eligLead;
	}

	public void setEligLead(EligLead eligLead) {
		this.eligLead = eligLead;
	}

	public CapAgentEmployment getCapAgentEmployment() {
		return capAgentEmployment;
	}

	public void setCapAgentEmployment(CapAgentEmployment capAgentEmployment) {
		this.capAgentEmployment = capAgentEmployment;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public Long getTenantId() {
		return tenantId;
	}
	
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Integer getConsumerCommentId() {
		return consumerCommentId;
	}

	public void setConsumerCommentId(Integer consumerCommentId) {
		this.consumerCommentId = consumerCommentId;
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getCustomerTimeZone() {
		return customerTimeZone;
	}

	public void setCustomerTimeZone(String customerTimeZone) {
		this.customerTimeZone = customerTimeZone;
	}

	@PrePersist
	public void prePersist() {
		if (null == this.created) {
			this.setCreated(new TSDate());
		}
		
		this.setUpdated(new TSDate());
		if (TenantContextHolder.getTenant() != null	&& TenantContextHolder.getTenant().getId() != null) {
			setTenantId(TenantContextHolder.getTenant().getId());
		}
	}
    
    @PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

}
