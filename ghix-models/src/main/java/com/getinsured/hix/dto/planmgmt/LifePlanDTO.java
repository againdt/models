/**
 * 
 * @author santanu
 * @version 1.0
 * @since May 02, 2016 
 * 
 * This DTO is to set Life plan attributes 
 */

package com.getinsured.hix.dto.planmgmt;

public class LifePlanDTO {

	private int planId;
	private String name;
	private Float premium;
	private int issuerId;
	private String issuerName;
	private String issuerLogo;
	private String coverageAmount;
	private String policyTerm;
	private String benefitUrl;
	

	/**
	 * @return the planId
	 */
	public int getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the premium
	 */
	public Float getPremium() {
		return premium;
	}
	/**
	 * @param premium the premium to set
	 */
	public void setPremium(Float premium) {
		this.premium = premium;
	}
	/**
	 * @return the issuerId
	 */
	public int getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the issuerLogo
	 */
	public String getIssuerLogo() {
		return issuerLogo;
	}
	/**
	 * @param issuerLogo the issuerLogo to set
	 */
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	/**
	 * @return the coverageAmount
	 */
	public String getCoverageAmount() {
		return coverageAmount;
	}
	/**
	 * @param coverageAmount the coverageAmount to set
	 */
	public void setCoverageAmount(String coverageAmount) {
		this.coverageAmount = coverageAmount;
	}
	/**
	 * @return the policyTerm
	 */
	public String getPolicyTerm() {
		return policyTerm;
	}
	/**
	 * @param policyTerm the policyTerm to set
	 */
	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}
	/**
	 * @return the benefitUrl
	 */
	public String getBenefitUrl() {
		return benefitUrl;
	}
	/**
	 * @param benefitUrl the benefitUrl to set
	 */
	public void setBenefitUrl(String benefitUrl) {
		this.benefitUrl = benefitUrl;
	}
	
}
