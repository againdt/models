package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import us.gov.treasury.irs.common.IRSHouseholdGrpType;

public class EnrollmentIrsHouseholdDTO implements Serializable{
	
	private String householdId;
	private Map<Integer, EnrollmentIrsPolicyInfoDTO> householdMontwiseEnrollmentMap;
	private List<IRSHouseholdGrpType> householdInfoFromIndivPortal;
	private Map<Integer, List<EnrollmentIrsPolicyInfoDTO>> householdMonthwiseMultiEnrollmentMap;
	
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public Map<Integer, EnrollmentIrsPolicyInfoDTO> getHouseholdMontwiseEnrollmentMap() {
		return householdMontwiseEnrollmentMap;
	}
	public void setHouseholdMontwiseEnrollmentMap(
			Map<Integer, EnrollmentIrsPolicyInfoDTO> householdMontwiseEnrollmentMap) {
		this.householdMontwiseEnrollmentMap = householdMontwiseEnrollmentMap;
	}
	public List<IRSHouseholdGrpType> getHouseholdInfoFromIndivPortal() {
		return householdInfoFromIndivPortal;
	}
	public void setHouseholdInfoFromIndivPortal(
			List<IRSHouseholdGrpType> householdInfoFromIndivPortal) {
		this.householdInfoFromIndivPortal = householdInfoFromIndivPortal;
	}
	/**
	 * @return the householdMonthwiseMultiEnrollmentMap
	 */
	public Map<Integer, List<EnrollmentIrsPolicyInfoDTO>> getHouseholdMonthwiseMultiEnrollmentMap() {
		return householdMonthwiseMultiEnrollmentMap;
	}
	/**
	 * @param householdMonthwiseMultiEnrollmentMap the householdMonthwiseMultiEnrollmentMap to set
	 */
	public void setHouseholdMonthwiseMultiEnrollmentMap(
			Map<Integer, List<EnrollmentIrsPolicyInfoDTO>> householdMonthwiseMultiEnrollmentMap) {
		this.householdMonthwiseMultiEnrollmentMap = householdMonthwiseMultiEnrollmentMap;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EnrollmentIrsHouseholdDTO [householdId=" + householdId
				+ ", householdMontwiseEnrollmentMap="
				+ householdMontwiseEnrollmentMap
				+ ", householdInfoFromIndivPortal="
				+ householdInfoFromIndivPortal
				+ ", householdMonthwiseMultiEnrollmentMap="
				+ householdMonthwiseMultiEnrollmentMap + "]";
	}
	

}
