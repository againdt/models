package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerBrandNameDTO extends AuditDTO {

	private Integer id;
	private String brandName;
	private String brandId;
	private String brandUrl;

	public IssuerBrandNameDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandUrl() {
		return brandUrl;
	}

	public void setBrandUrl(String brandUrl) {
		this.brandUrl = brandUrl;
	}
}
