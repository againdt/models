package com.getinsured.eligibility.enums;

public enum SsapApplicantPersonType {

	PC("PC"),
	PC_DEP("PC_DEP"),
	PC_PTF("PC_PTF"),
	PTF("PTF"),
	DEP("DEP");
	
	String personType = null;
	
	SsapApplicantPersonType(String personType) {
		this.personType = personType;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}
}
