#!/bin/bash
echo " "
echo "Make sure jave version is 1.8 or above "
echo 

if [ $# -ne 1 ]
then
  echo " "
  echo "Usage $0 <profile in low-case>"
  echo "Usage $0 [nm|ms|id|wa|ct|mn|nv|phix|medicare|][ca]"
  echo "Example: $0 nm"
  echo " "
  exit 2
fi

echo $1
###
### For PostgreSQL
CONFL=../../conf/liquibase_postgres.properties
### For Oracle
###CONFL=../../conf/liquibase.properties
DETECTLOG=liquibase_run_log.txt

if [ "$1" == "ca" ]
then
  echo " "
  echo "do special task for CA upgrade"

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_ca2.0_upgrade.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml update
 
  echo " "
fi
###
### Determine if PostgreSQL 9.6
java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --classpath=../../../ghix-setup.war --logLevel=info --changeLogFile=META-INF/detect_postgres_version.xml releaseLocks

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --classpath=../../../ghix-setup.war --logLevel=info --changeLogFile=META-INF/detect_postgres_version.xml clearCheckSums

cat /dev/null >${DETECTLOG}
java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --classpath=../../../ghix-setup.war --logLevel=info --changeLogFile=META-INF/detect_postgres_version.xml update &>${DETECTLOG}
POSTGRESVERSION=`grep '::HIX-DETECT_POSTGRES::JT: Custom SQL executed' ${DETECTLOG}|wc -l`

if [ $POSTGRESVERSION -eq 1 ]; then
  echo " "
  echo "run 9.6 "
  echo " "
java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1_96.xml releaseLocks

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_pre_new_$1_96.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1_96.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/${1}96rebase.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new_96.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_pre_new_$1_96.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1_96.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new_96.xml update

date

  exit 0
fi

###
java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1.xml releaseLocks 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_pre_new_$1.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new_96.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_pre_new_$1.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=${CONFL} --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new_96.xml update

date
