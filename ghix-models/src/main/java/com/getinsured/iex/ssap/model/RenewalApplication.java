package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.timeshift.util.TSDate;

/**
 * The persistent class for the RENEWAL_APPLICATIONS database table.
 *
 */
@Audited
@Entity
@Table(name = "RENEWAL_APPLICATIONS")
@DynamicInsert
@DynamicUpdate
public class RenewalApplication implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RENEWAL_APPLICATIONS_ID_GENERATOR", sequenceName = "RENEWAL_APPLICATIONS_SEQ",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RENEWAL_APPLICATIONS_ID_GENERATOR")
	@Column(unique = true, nullable = false, precision = 22)
	private Long id;

	@Column(unique = true, nullable = false, precision = 22, name = "SSAP_APPLICATION_ID")
	private Long ssapApplicationId;

	@Column(name = "HEALTH_RENEWAL_STATUS", length = 50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus healthRenewalStatus;

	@Column(name = "DENTAL_RENEWAL_STATUS", length = 50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus dentalRenewalStatus;

	@Column(name = "EXTERNAL_HOUSEHOLD_CASE_ID")
	private String externalHouseholdCaseId;

	@Column(name = "CMR_HOUSEHOLD_ID")
	private Integer cmrHouseholdId;

	@Column(name = "GI_MONITOR_ID")
	private Long giMonitorId;

	@Column(name = "HEALTH_REASON_CODE")
	private Integer healthReasonCode;

	@Column(name = "DENTAL_REASON_CODE")
	private Integer dentalReasonCode;

	@Column(name = "HOUSEHOLD_RENEWAL_STATUS", length = 50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus householdRenewalStatus;

	@Column(name = "CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;
	
	@Column(name = "notice_id")
	private Integer noticeId;
	
	@Column(name = "failure_notice_id")
	private Integer failureNoticeId;
	
	@NotAudited
	@Column(name = "TO_CONSIDER_SERVER_NAME")
	private Integer toConsiderServerName;
	
	@NotAudited
	@Column(name = "AUTO_ENROLL_SERVER_NAME")
	private Integer autoEnrollServerName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public RenewalStatus getHealthRenewalStatus() {
		return healthRenewalStatus;
	}

	public void setHealthRenewalStatus(RenewalStatus healthRenewalStatus) {
		this.healthRenewalStatus = healthRenewalStatus;
	}

	public RenewalStatus getDentalRenewalStatus() {
		return dentalRenewalStatus;
	}

	public void setDentalRenewalStatus(RenewalStatus dentalRenewalStatus) {
		this.dentalRenewalStatus = dentalRenewalStatus;
	}

	public String getExternalHouseholdCaseId() {
		return externalHouseholdCaseId;
	}

	public void setExternalHouseholdCaseId(String externalHouseholdCaseId) {
		this.externalHouseholdCaseId = externalHouseholdCaseId;
	}

	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Long getGiMonitorId() {
		return giMonitorId;
	}

	public void setGiMonitorId(Long giMonitorId) {
		this.giMonitorId = giMonitorId;
	}

	public Integer getHealthReasonCode() {
		return healthReasonCode;
	}

	public void setHealthReasonCode(Integer healthReasonCode) {
		this.healthReasonCode = healthReasonCode;
	}

	public Integer getDentalReasonCode() {
		return dentalReasonCode;
	}

	public void setDentalReasonCode(Integer dentalReasonCode) {
		this.dentalReasonCode = dentalReasonCode;
	}

	public RenewalStatus getHouseholdRenewalStatus() {
		return householdRenewalStatus;
	}

	public void setHouseholdRenewalStatus(RenewalStatus householdRenewalStatus) {
		this.householdRenewalStatus = householdRenewalStatus;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(Integer noticeId) {
		this.noticeId = noticeId;
	}

	public Integer getFailureNoticeId() {
		return failureNoticeId;
	}

	public void setFailureNoticeId(Integer failureNoticeId) {
		this.failureNoticeId = failureNoticeId;
	}
	
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new Timestamp(new TSDate().getTime()));
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}
	
	public Integer getToConsiderServerName() {
		return toConsiderServerName;
	}

	public void setToConsiderServerName(Integer toConsiderServerName) {
		this.toConsiderServerName = toConsiderServerName;
	}

	public Integer getAutoEnrollServerName() {
		return autoEnrollServerName;
	}

	public void setAutoEnrollServerName(Integer autoEnrollServerName) {
		this.autoEnrollServerName = autoEnrollServerName;
	}

	@Override
	public String toString() {
		return "SsapRenewalApplication [id=" + id + ", ssapApplicationId=" + ssapApplicationId
				+ ", healthRenewalStatus=" + healthRenewalStatus + ", dentalRenewalStatus=" + dentalRenewalStatus
				+ ", externalHouseholdCaseId=" + externalHouseholdCaseId + ", cmrHouseholdId=" + cmrHouseholdId
				+ ", giMonitorId=" + giMonitorId + ", healthReasonCode=" + healthReasonCode + ", dentalReasonCode="
				+ dentalReasonCode + ", householdRenewalStatus=" + householdRenewalStatus + ", creationTimestamp="
				+ creationTimestamp + ", lastUpdateTimestamp=" + lastUpdateTimestamp + "]";
	}
}