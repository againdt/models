package com.getinsured.eligibility.enums;

/**
 * SSAP status codes.
 */
public enum ApplicationStatus {
	OPEN("OP"),
	SIGNED("SG"),
	SUBMITTED("SU"),
	ELIGIBILITY_RECEIVED("ER"),
	ENROLLED_OR_ACTIVE("EN"),
	CANCELLED("CC"),
	CLOSED("CL"),
	UNCLAIMED("UC"),
	PARTIALLY_ENROLLED("PN"),
	QUEUED("QU"),
	DEQUEUED("DQ");

	private String	applicationStatusCode;

	ApplicationStatus(String applicationStatusCode){
		this.applicationStatusCode = applicationStatusCode;
	}

	public String getApplicationStatusCode(){
		return applicationStatusCode;
	}

	@Override
	public String toString() {
		return this.applicationStatusCode;
	}

	public static ApplicationStatus fromString(String val) {
		for(ApplicationStatus status : ApplicationStatus.values()) {
			if(status.toString().equals(val)) {
				return status;
			}
		}

		return null;
	}
}
