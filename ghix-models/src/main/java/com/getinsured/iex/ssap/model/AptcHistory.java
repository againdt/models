package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the CMR_HH_APTC_HISTORY database table.
 *
 */
@Entity
@Table(name="CMR_HH_APTC_HISTORY")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="AptcHistory.findAll", query="SELECT s FROM AptcHistory s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AptcHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CMR_HH_APTC_HISTORY_ID_GENERATOR", sequenceName="CMR_HH_APTC_HISTORY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CMR_HH_APTC_HISTORY_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="APTC_AMT")
	private BigDecimal aptcAmount;

	@Column(name="COVERAGE_YEAR")
	private long coverageYear;
	
	@Column(name="SSAP_APPLICATION_ID", precision=22)
	private long ssapApplicationId;

	@Column(name="CMR_HOUSEOLD_ID", precision=22)
	private BigDecimal cmrHouseholdId;
	
	@Column(name="CREATED_BY", precision=10)
	private BigDecimal createdBy;

	@Column(name="CREATION_TIMESTAMP")
	private Date creationTimestamp;

	@Column(name="EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@Column(name="ELIG_START_DATE")
	private Date eligStartDate;
	
	@Column(name="ELIG_END_DATE")
	private Date eligEndDate;
	

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;

	@Column(name="LAST_UPDATED_BY", precision=10)
	private BigDecimal lastUpdatedBy;
	
	@Column(name = "SHOP_APTC_AMT")
	private BigDecimal shopAptcAmnt;

	@Column(name = "SHOP_STATE_SUBSIDY_AMT")
	private BigDecimal shopStateSubsidyAmnt;
	
	@Column(name = "SLCSP_AMOUNT")
	private BigDecimal slcspAmount;
	
	@Column(name="IS_RECALC", length=1) 
	private String isRecalc;

	@Column(name="STATE_SUBSIDY_AMT")
	private BigDecimal stateSubsidyAmt;

	@Column(name="STATE_SUBSIDY_START_DATE")
	private Date stateSubsidyStartDate;

	@Column(name="STATE_SUBSIDY_END_DATE")
	private Date stateSubsidyEndDate;

	public BigDecimal getShopAptcAmnt() {
		return shopAptcAmnt;
	}

	public void setShopAptcAmnt(BigDecimal shopAptcAmnt) {
		this.shopAptcAmnt = shopAptcAmnt;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		// Add a comment to this line
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getAptcAmount() {
		return aptcAmount;
	}

	public void setAptcAmount(BigDecimal aptcAmount) {
		this.aptcAmount = aptcAmount;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public BigDecimal getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(BigDecimal cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public Date getEligStartDate() {
		return eligStartDate;
	}

	public void setEligStartDate(Date eligStartDate) {
		this.eligStartDate = eligStartDate;
	}

	public Date getEligEndDate() {
		return eligEndDate;
	}

	public void setEligEndDate(Date eligEndDate) {
		this.eligEndDate = eligEndDate;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public BigDecimal getSlcspAmount() {
		return slcspAmount;
	}

	public void setSlcspAmount(BigDecimal slcspAmount) {
		this.slcspAmount = slcspAmount;
	}

	public String getIsRecalc() {
		return isRecalc;
	}

	public BigDecimal getStateSubsidyAmt() {
		return stateSubsidyAmt;
	}

	public void setStateSubsidyAmt(BigDecimal stateSubsidyAmt) {
		this.stateSubsidyAmt = stateSubsidyAmt;
	}

	public Date getStateSubsidyStartDate() {
		return stateSubsidyStartDate;
	}

	public void setStateSubsidyStartDate(Date stateSubsidyStartDate) {
		this.stateSubsidyStartDate = stateSubsidyStartDate;
	}

	public Date getStateSubsidyEndDate() {
		return stateSubsidyEndDate;
	}

	public void setStateSubsidyEndDate(Date stateSubsidyEndDate) {
		this.stateSubsidyEndDate = stateSubsidyEndDate;
	}

	public void setIsRecalc(String isRecalc) {
		this.isRecalc = isRecalc;
	}

	public BigDecimal getShopStateSubsidyAmnt() {
		return shopStateSubsidyAmnt;
	}

	public void setShopStateSubsidyAmnt(BigDecimal shopStateSubsidyAmnt) {
		this.shopStateSubsidyAmnt = shopStateSubsidyAmnt;
	}
}
