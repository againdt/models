
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for YOrNIndType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="YOrNIndType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Y"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "YOrNIndType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
@XmlEnum
public enum YOrNIndType {

    Y,
    N;

    public String value() {
        return name();
    }

    public static YOrNIndType fromValue(String v) {
        return valueOf(v);
    }

}
