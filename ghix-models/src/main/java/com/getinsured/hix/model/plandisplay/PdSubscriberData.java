package com.getinsured.hix.model.plandisplay;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public class PdSubscriberData {
	private Long householdId;
	private String initialSubscriberId;
	private EnrollmentFlowType specialEnrollmentFlowType;
	private InsuranceType insuranceType;
	
	public Long getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}
	public String getInitialSubscriberId() {
		return initialSubscriberId;
	}
	public void setInitialSubscriberId(String initialSubscriberId) {
		this.initialSubscriberId = initialSubscriberId;
	}
	public EnrollmentFlowType getSpecialEnrollmentFlowType() {
		return specialEnrollmentFlowType;
	}
	public void setSpecialEnrollmentFlowType(EnrollmentFlowType specialEnrollmentFlowType) {
		this.specialEnrollmentFlowType = specialEnrollmentFlowType;
	}
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

}
