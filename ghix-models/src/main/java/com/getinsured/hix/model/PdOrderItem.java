package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

@Entity
@Table(name = "PD_ORDER_ITEM")
public class PdOrderItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public enum Status {
		ACTIVE, DELETED, ENROLLED;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pd_order_item_seq")
	@SequenceGenerator(name = "pd_order_item_seq", sequenceName = "pd_order_item_seq", allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pd_household_id")
	private PdHousehold pdHousehold;

	@Column(name = "plan_id")
	private Long planId;

	@Column(name = "premium_before_credit")
	private Float premiumBeforeCredit;

	@Column(name = "premium_after_credit")
	private Float premiumAfterCredit;

	@Column(name = "monthly_subsidy")
	private Float monthlySubsidy;

	@Column(name = "insurance_type")
	@Enumerated(EnumType.STRING)
	private InsuranceType insuranceType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date creationTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date lastUpdateTimestamp;

	@Column(name = "SSAP_APPLICATION_ID")
	private Long ssapApplicationId;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Column(name = "EXCHANGE_TYPE")
	@Enumerated(EnumType.STRING)
	private ExchangeType exchangeType;
	
	@Column(name = "PREFERENCES")
	private String preferences;

	@Column(name = "monthly_state_subsidy")
	private BigDecimal monthlyStateSubsidy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PdHousehold getPdHousehold() {
		return pdHousehold;
	}

	public void setPdHousehold(PdHousehold pdHousehold) {
		this.pdHousehold = pdHousehold;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Float getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}

	public void setPremiumBeforeCredit(Float premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}

	public Float getPremiumAfterCredit() {
		return premiumAfterCredit;
	}

	public void setPremiumAfterCredit(Float premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}

	public Float getMonthlySubsidy() {
		return monthlySubsidy;
	}

	public void setMonthlySubsidy(Float monthlySubsidy) {
		this.monthlySubsidy = monthlySubsidy;
	}

	public InsuranceType getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
		 if(this.status == null) {
			 this.status = Status.ACTIVE;
		  }
	}

	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public ExchangeType getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getPreferences() {
		return preferences;
	}

	public void setPreferences(String preferences) {
		this.preferences = preferences;
	}

	public BigDecimal getMonthlyStateSubsidy() {
		return monthlyStateSubsidy;
	}

	public void setMonthlyStateSubsidy(BigDecimal monthlyStateSubsidy) {
		this.monthlyStateSubsidy = monthlyStateSubsidy;
	}
}
