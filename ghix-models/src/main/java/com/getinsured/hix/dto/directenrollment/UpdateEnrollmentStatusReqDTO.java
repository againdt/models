package com.getinsured.hix.dto.directenrollment;

public class UpdateEnrollmentStatusReqDTO {	
	
	private DirectEnrollmentStatus.EnrollmentStatus status;	
	private String enrollmentId;
	private String userName;

	public DirectEnrollmentStatus.EnrollmentStatus getStatus() {
		return status;
	}

	public void setStatus(DirectEnrollmentStatus.EnrollmentStatus status) {
		this.status = status;
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}