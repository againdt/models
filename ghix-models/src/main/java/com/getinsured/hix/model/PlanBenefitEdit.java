package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "PLAN_BENEFIT_EDITS")
public class PlanBenefitEdit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_BENEFIT_EDITS_SEQ")
	@SequenceGenerator(name = "PLAN_BENEFIT_EDITS_SEQ", sequenceName = "PLAN_BENEFIT_EDITS_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;
	
	@Column(name = "PLAN_HIOS_ID")
	private String planHiosId;
	
	@Column(name = "PLAN_BENEFIT_ID")
	private Integer planBenefitId;
	
	@Column(name = "BENEFIT_NAME")
	private String benefitName;
	
	@Column(name = "APPLICABLE_YEAR")
	private String applicableYear;
	
	@Column(name = "INSURANCE_TYPE")
	private String insuranceType;
	
	@Column(name = "NETWORK_T1_VALUE")
	private String networkT1Value;
	
	@Column(name = "NETWORK_T2_VALUE")
	private String networkT2Value;
	
	@Column(name = "OUT_OF_NETWORK_VALUE")
	private String outOfNetworkValue;
	
	@Column(name = "NETWORK_T1_TILE_VALUE")
	private String networkT1TileValue;
	
	@Column(name = "LIMIT_EXCEPTION_VALUE")
	private String limitExceptionValue;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name = "LAST_UPDATED_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	@Column(name = "NETWORK_T1_COPAY_VAL")
	private String networkT1CopayVal ;
	
	@Column(name = "NETWORK_T1_COPAY_ATTR")
	private String networkT1CopayAttr;
	
	@Column(name = "NETWORK_T1_COINSURANCE_VAL")
	private String networkT1CoinsuranceVal;
	
	@Column(name = "NETWORK_T1_COINSURANCE_ATTR")
	private String networkT1CoinsuranceAttr;
	
	@Column(name = "NETWORK_T2_COPAY_VAL")
	private String networkT2CopayVal ;
	
	@Column(name = "NETWORK_T2_COPAY_ATTR")
	private String networkT2CopayAttr;
	
	@Column(name = "NETWORK_T2_COINSURANCE_VAL")
	private String networkT2CoinsuranceVal;
	
	@Column(name = "NETWORK_T2_COINSURANCE_ATTR")
	private String networkT2CoinsuranceAttr; 
	
	@Column(name = "OUTNETWORK_COPAY_VAL")
	private String outNetworkCopayVal ;
	
	@Column(name = "OUTNETWORK_COPAY_ATTR")
	private String outNetworkCopayAttr;
	
	@Column(name = "OUTNETWORK_COINSURANCE_VAL")
	private String outNetworkCoinsuranceVal;
	
	@Column(name = "OUTNETWORK_COINSURANCE_ATTR")
	private String outNetworkCoinsuranceAttr; 
	
	public PlanBenefitEdit(){
		
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getPlanHiosId() {
		return planHiosId;
	}

	public void setPlanHiosId(String planHiosId) {
		this.planHiosId = planHiosId;
	}

	public Integer getPlanBenefitId() {
		return planBenefitId;
	}

	public void setPlanBenefitId(Integer planBenefitId) {
		this.planBenefitId = planBenefitId;
	}

	public String getBenefitName() {
		return benefitName;
	}

	public void setBenefitName(String benefitName) {
		this.benefitName = benefitName;
	}

	public String getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(String applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	

	public String getNetworkT1Value() {
		return networkT1Value;
	}

	public String getNetworkT2Value() {
		return networkT2Value;
	}

	public String getOutOfNetworkValue() {
		return outOfNetworkValue;
	}

	public String getNetworkT1TileValue() {
		return networkT1TileValue;
	}

	public String getLimitExceptionValue() {
		return limitExceptionValue;
	}

	public void setNetworkT1Value(String networkT1Value) {
		this.networkT1Value = networkT1Value;
	}

	public void setNetworkT2Value(String networkT2Value) {
		this.networkT2Value = networkT2Value;
	}

	public void setOutOfNetworkValue(String outOfNetworkValue) {
		this.outOfNetworkValue = outOfNetworkValue;
	}

	public void setNetworkT1TileValue(String networkT1TileValue) {
		this.networkT1TileValue = networkT1TileValue;
	}

	public void setLimitExceptionValue(String limitExceptionValue) {
		this.limitExceptionValue = limitExceptionValue;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getNetworkT1CopayVal() {
		return networkT1CopayVal;
	}

	public String getNetworkT1CopayAttr() {
		return networkT1CopayAttr;
	}

	public String getNetworkT1CoinsuranceVal() {
		return networkT1CoinsuranceVal;
	}

	public String getNetworkT1CoinsuranceAttr() {
		return networkT1CoinsuranceAttr;
	}

	public String getNetworkT2CopayVal() {
		return networkT2CopayVal;
	}

	public String getNetworkT2CopayAttr() {
		return networkT2CopayAttr;
	}

	public String getNetworkT2CoinsuranceVal() {
		return networkT2CoinsuranceVal;
	}

	public String getNetworkT2CoinsuranceAttr() {
		return networkT2CoinsuranceAttr;
	}

	public String getOutNetworkCopayVal() {
		return outNetworkCopayVal;
	}

	public String getOutNetworkCopayAttr() {
		return outNetworkCopayAttr;
	}

	public String getOutNetworkCoinsuranceVal() {
		return outNetworkCoinsuranceVal;
	}

	public String getOutNetworkCoinsuranceAttr() {
		return outNetworkCoinsuranceAttr;
	}

	public void setNetworkT1CopayVal(String networkT1CopayVal) {
		this.networkT1CopayVal = networkT1CopayVal;
	}

	public void setNetworkT1CopayAttr(String networkT1CopayAttr) {
		this.networkT1CopayAttr = networkT1CopayAttr;
	}

	public void setNetworkT1CoinsuranceVal(String networkT1CoinsuranceVal) {
		this.networkT1CoinsuranceVal = networkT1CoinsuranceVal;
	}

	public void setNetworkT1CoinsuranceAttr(String networkT1CoinsuranceAttr) {
		this.networkT1CoinsuranceAttr = networkT1CoinsuranceAttr;
	}

	public void setNetworkT2CopayVal(String networkT2CopayVal) {
		this.networkT2CopayVal = networkT2CopayVal;
	}

	public void setNetworkT2CopayAttr(String networkT2CopayAttr) {
		this.networkT2CopayAttr = networkT2CopayAttr;
	}

	public void setNetworkT2CoinsuranceVal(String networkT2CoinsuranceVal) {
		this.networkT2CoinsuranceVal = networkT2CoinsuranceVal;
	}

	public void setNetworkT2CoinsuranceAttr(String networkT2CoinsuranceAttr) {
		this.networkT2CoinsuranceAttr = networkT2CoinsuranceAttr;
	}

	public void setOutNetworkCopayVal(String outNetworkCopayVal) {
		this.outNetworkCopayVal = outNetworkCopayVal;
	}

	public void setOutNetworkCopayAttr(String outNetworkCopayAttr) {
		this.outNetworkCopayAttr = outNetworkCopayAttr;
	}

	public void setOutNetworkCoinsuranceVal(String outNetworkCoinsuranceVal) {
		this.outNetworkCoinsuranceVal = outNetworkCoinsuranceVal;
	}

	public void setOutNetworkCoinsuranceAttr(String outNetworkCoinsuranceAttr) {
		this.outNetworkCoinsuranceAttr = outNetworkCoinsuranceAttr;
	}
	
}
