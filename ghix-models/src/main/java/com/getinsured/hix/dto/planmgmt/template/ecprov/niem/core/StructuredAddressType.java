//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.18 at 12:55:43 PM PDT 
//


package com.getinsured.hix.dto.planmgmt.template.ecprov.niem.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.dto.planmgmt.template.ecprov.niem.fips.USStateCodeType;
import com.getinsured.hix.dto.planmgmt.template.ecprov.niem.proxy.xsd.String;
import com.getinsured.hix.dto.planmgmt.template.ecprov.niem.structures.ComplexObjectType;



/**
 * A data type for an address.
 * 
 * <p>Java class for StructuredAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructuredAddressType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationStreet"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationCityName"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationStateFIPS5-2AlphaCode"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationPostalCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuredAddressType", propOrder = {
    "locationStreet",
    "locationCityName",
    "locationStateFIPS52AlphaCode",
    "locationPostalCode"
})
public class StructuredAddressType
    extends ComplexObjectType
{

    @XmlElement(name = "LocationStreet", required = true, nillable = true)
    protected StreetType locationStreet;
    @XmlElement(name = "LocationCityName", required = true, nillable = true)
    protected ProperNameTextType locationCityName;
    @XmlElement(name = "LocationStateFIPS5-2AlphaCode", required = true, nillable = true)
    protected USStateCodeType locationStateFIPS52AlphaCode;
    @XmlElement(name = "LocationPostalCode", required = true, nillable = true)
    protected String locationPostalCode;

    /**
     * Gets the value of the locationStreet property.
     * 
     * @return
     *     possible object is
     *     {@link StreetType }
     *     
     */
    public StreetType getLocationStreet() {
        return locationStreet;
    }

    /**
     * Sets the value of the locationStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetType }
     *     
     */
    public void setLocationStreet(StreetType value) {
        this.locationStreet = value;
    }

    /**
     * Gets the value of the locationCityName property.
     * 
     * @return
     *     possible object is
     *     {@link ProperNameTextType }
     *     
     */
    public ProperNameTextType getLocationCityName() {
        return locationCityName;
    }

    /**
     * Sets the value of the locationCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProperNameTextType }
     *     
     */
    public void setLocationCityName(ProperNameTextType value) {
        this.locationCityName = value;
    }

    /**
     * Gets the value of the locationStateFIPS52AlphaCode property.
     * 
     * @return
     *     possible object is
     *     {@link USStateCodeType }
     *     
     */
    public USStateCodeType getLocationStateFIPS52AlphaCode() {
        return locationStateFIPS52AlphaCode;
    }

    /**
     * Sets the value of the locationStateFIPS52AlphaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link USStateCodeType }
     *     
     */
    public void setLocationStateFIPS52AlphaCode(USStateCodeType value) {
        this.locationStateFIPS52AlphaCode = value;
    }

    /**
     * Gets the value of the locationPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPostalCode() {
        return locationPostalCode;
    }

    /**
     * Sets the value of the locationPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPostalCode(String value) {
        this.locationPostalCode = value;
    }

}
