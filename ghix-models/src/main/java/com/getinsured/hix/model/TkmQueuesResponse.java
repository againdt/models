package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

public class TkmQueuesResponse extends GHIXResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<TkmQueues> tkmQueuesList = null;

	public List<TkmQueues> getTkmQueuesList() {
		return tkmQueuesList;
	}

	public void setTkmQueuesList(List<TkmQueues> tkmQueuesList) {
		this.tkmQueuesList = tkmQueuesList;
	}	

}
