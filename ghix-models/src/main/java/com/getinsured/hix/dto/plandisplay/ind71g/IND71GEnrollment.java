package com.getinsured.hix.dto.plandisplay.ind71g;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class IND71GEnrollment {

	@NotNull(message="enrollmentId" + IND71Enums.REQUIRED_FIELD)
    private String enrollmentId;
	
	@NotNull(message="insuranceType" + IND71Enums.REQUIRED_FIELD)
	@Pattern(regexp=IND71Enums.INSURANCE_TYPE_REGEX,message="insuranceType" + IND71Enums.INVALID_REGEX_ERROR)
    private String insuranceType;
	
    private Float aptc;

    private BigDecimal stateSubsidy;
    
    @Pattern(regexp=IND71Enums.CSR_REGEX,message="csr" + IND71Enums.INVALID_REGEX_ERROR)
    private String csr;
    
    @Pattern(regexp=IND71Enums.DATE_REGEX,message="coverageDate" + IND71Enums.INVALID_REGEX_ERROR)
    @NotNull(message="coverageDate" + IND71Enums.REQUIRED_FIELD)
    private String coverageDate;
    
    @Pattern(regexp=IND71Enums.DATE_REGEX,message="financialEffectiveDate" + IND71Enums.INVALID_REGEX_ERROR)
    private String financialEffectiveDate;
    
    @Valid
    private List<IND71GMember> members = null;

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public Float getAptc() {
        return aptc;
    }

    public void setAptc(Float aptc) {
        this.aptc = aptc;
    }

    public String getCsr() {
        return csr;
    }

    public void setCsr(String csr) {
        this.csr = csr;
    }

    public String getCoverageDate() {
        return coverageDate;
    }

    public void setCoverageDate(String coverageDate) {
        this.coverageDate = coverageDate;
    }

    public String getFinancialEffectiveDate() {
        return financialEffectiveDate;
    }

    public void setFinancialEffectiveDate(String financialEffectiveDate) {
        this.financialEffectiveDate = financialEffectiveDate;
    }

    public List<IND71GMember> getMembers() {
        return members;
    }

    public void setMembers(List<IND71GMember> members) {
        this.members = members;
    }

    public BigDecimal getStateSubsidy() {
        return stateSubsidy;
    }

    public void setStateSubsidy(BigDecimal stateSubsidy) {
        this.stateSubsidy = stateSubsidy;
    }
}
