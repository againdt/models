  CREATE TABLE CAP_TEAM_AUD 
   (	ID NUMBER, 
	REV NUMBER, 
	REVTYPE NUMBER(3,0), 
	TEAM_NAME VARCHAR2(200 CHAR), 
	CURRENT_TEAM_LEAD_USER_ID NUMBER, 
	PARENT_TEAM_ID NUMBER, 
	LAST_TEAM_NAME_UPDATE TIMESTAMP (6), 
	CREATION_TIMESTAMP TIMESTAMP (6), 
	LAST_UPDATE_TIMESTAMP TIMESTAMP (6), 
	START_DATE TIMESTAMP (6), 
	END_DATE TIMESTAMP (6), 
	TENANT_ID NUMBER
   ) ;

   COMMENT ON COLUMN CAP_TEAM_AUD.ID IS 'Primary Key of CAP_TEAM_AUD table.';
   COMMENT ON COLUMN CAP_TEAM_AUD.REV IS 'Revision number of the audit record.';
   COMMENT ON COLUMN CAP_TEAM_AUD.REVTYPE IS 'Type of revision of the audit record.';
   COMMENT ON COLUMN CAP_TEAM_AUD.TEAM_NAME IS 'Team Name.';
   COMMENT ON COLUMN CAP_TEAM_AUD.CURRENT_TEAM_LEAD_USER_ID IS 'Team lead user id. Capture numeric value.';
   COMMENT ON COLUMN CAP_TEAM_AUD.PARENT_TEAM_ID IS 'Id of the parent team.';
   COMMENT ON COLUMN CAP_TEAM_AUD.LAST_TEAM_NAME_UPDATE IS 'Update timestamp of the team name change';
   COMMENT ON COLUMN CAP_TEAM_AUD.CREATION_TIMESTAMP IS 'Creation timestamp of the record';
   COMMENT ON COLUMN CAP_TEAM_AUD.LAST_UPDATE_TIMESTAMP IS 'Update timestamp of the record';
   COMMENT ON COLUMN CAP_TEAM_AUD.START_DATE IS 'start date of the team';
   COMMENT ON COLUMN CAP_TEAM_AUD.END_DATE IS 'end date of the team';
   COMMENT ON COLUMN CAP_TEAM_AUD.TENANT_ID IS 'Stores the tenant id to which the team belongs';

  CREATE UNIQUE INDEX CAP_TEAM_AUD ON CAP_TEAM_AUD (ID, REV) 
  ;

  ALTER TABLE CAP_TEAM_AUD ADD CONSTRAINT CAP_TEAM_AUD PRIMARY KEY (ID, REV)
  USING INDEX  ENABLE;
  ALTER TABLE CAP_TEAM_AUD MODIFY (REV NOT NULL ENABLE);
  ALTER TABLE CAP_TEAM_AUD MODIFY (ID NOT NULL ENABLE);
