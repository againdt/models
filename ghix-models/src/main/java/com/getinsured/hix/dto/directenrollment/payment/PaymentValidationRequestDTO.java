package com.getinsured.hix.dto.directenrollment.payment;

import java.util.List;

import com.getinsured.hix.dto.directenrollment.PaymentDTO;
import com.getinsured.hix.dto.directenrollment.payment.PaymentConstants.PaymentMode;

/**
 * @author root
 *
 */
public class PaymentValidationRequestDTO {
	private PaymentDTO paymentDTO;
	/**
	 * List of all the payment types that need to be validated
	 * as part of this request.
	 * 
	 */
	private List<PaymentMode> mode;
	
	public PaymentDTO getPaymentDTO() {
		return paymentDTO;
	}
	public void setPaymentDTO(PaymentDTO paymentDTO) {
		this.paymentDTO = paymentDTO;
	}
	public List<PaymentMode> getMode() {
		return mode;
	}
	public void setMode(List<PaymentMode> mode) {
		this.mode = mode;
	}
	

}
