package com.getinsured.hix.dto.plandisplay.ind71g;

import javax.validation.constraints.Pattern;

public class IND71GRelationship {

    private String memberId;
    @Pattern(regexp=IND71Enums.RELATION_REGEX,message="houseHoldContactRelationship" + IND71Enums.INVALID_REGEX_ERROR)
    private String relationshipCode;
    
    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getRelationshipCode() {
        return relationshipCode;
    }

    public void setRelationshipCode(String relationshipCode) {
        this.relationshipCode = relationshipCode;
    }
}
