package com.getinsured.hix.dto.agency;

import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.getinsured.hix.model.agency.Agency.CertificationStatus;

public class AgencyInformationDto {
	
	String id;
	@NotBlank
	@Size(max=80)
	String agencyName;
	String federalTaxId;
	@Size(max=10,min=0)
	@Pattern(regexp="^[a-zA-Z0-9]{0,10}$")
	String licenseNumber;
	String businessLegalName;
	CertificationStatus certificationStatus;
	String nextRegistrationStep;
	
	private List<AgencyManagerProfileDto> agencyManagersInfoDto;
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAgencyName() {
		return agencyName;
	}
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	public String getFederalTaxId() {
		return federalTaxId;
	}
	public void setFederalTaxId(String federalTaxId) {
		this.federalTaxId = federalTaxId;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getBusinessLegalName() {
		return businessLegalName;
	}
	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}
	public CertificationStatus getCertificationStatus() {
		return certificationStatus;
	}
	public void setCertificationStatus(CertificationStatus certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	public String getNextRegistrationStep() {
		return nextRegistrationStep;
	}
	public void setNextRegistrationStep(String nextRegistrationStep) {
		this.nextRegistrationStep = nextRegistrationStep;
	}
	public List<AgencyManagerProfileDto> getAgencyManagersInfoDto() {
		return agencyManagersInfoDto;
	}
	public void setAgencyManagersInfoDto(List<AgencyManagerProfileDto> agencyManagersInfoDto) {
		this.agencyManagersInfoDto = agencyManagersInfoDto;
	}
	
}
