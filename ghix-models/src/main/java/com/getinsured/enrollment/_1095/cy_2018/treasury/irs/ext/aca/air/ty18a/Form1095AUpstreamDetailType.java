
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Upstream Transmission detail for 1095A Forms.&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095AUpstreamDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095AUpstreamDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecordSequenceNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TaxYr"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedRecordSequenceNum" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}VoidInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}VoidedRecordSequenceNum" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MarketplaceId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}Policy"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}Recipient"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientSpouse" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CoverageHouseholdGrp"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}RecipientPolicyInformation"/>
 *       &lt;/sequence>
 *       &lt;attribute name="recordType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="lineNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095AUpstreamDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "recordSequenceNum",
    "taxYr",
    "correctedInd",
    "correctedRecordSequenceNum",
    "voidInd",
    "voidedRecordSequenceNum",
    "marketplaceId",
    "policy",
    "recipient",
    "recipientSpouse",
    "coverageHouseholdGrp",
    "recipientPolicyInformation"
})
public class Form1095AUpstreamDetailType {

    @XmlElement(name = "RecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String recordSequenceNum;
    @XmlElement(name = "TaxYr", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String taxYr;
    @XmlElement(name = "CorrectedInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String correctedInd;
    @XmlElement(name = "CorrectedRecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String correctedRecordSequenceNum;
    @XmlElement(name = "VoidInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String voidInd;
    @XmlElement(name = "VoidedRecordSequenceNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String voidedRecordSequenceNum;
    @XmlElement(name = "MarketplaceId", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String marketplaceId;
    @XmlElement(name = "Policy", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected RecipientPolicyCoverageDtlType policy;
    @XmlElement(name = "Recipient", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected PersonInformationType recipient;
    @XmlElement(name = "RecipientSpouse", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected EOYPersonType recipientSpouse;
    @XmlElement(name = "CoverageHouseholdGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CoverageHouseholdGrpType coverageHouseholdGrp;
    @XmlElement(name = "RecipientPolicyInformation", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected TaxHouseholdPolicyInformationDtlType recipientPolicyInformation;
    @XmlAttribute(name = "recordType", required = true)
    protected String recordType;
    @XmlAttribute(name = "lineNum", required = true)
    protected BigInteger lineNum;

    /**
     * Gets the value of the recordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordSequenceNum() {
        return recordSequenceNum;
    }

    /**
     * Sets the value of the recordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordSequenceNum(String value) {
        this.recordSequenceNum = value;
    }

    /**
     * Gets the value of the taxYr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxYr() {
        return taxYr;
    }

    /**
     * Sets the value of the taxYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxYr(String value) {
        this.taxYr = value;
    }

    /**
     * Gets the value of the correctedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedInd() {
        return correctedInd;
    }

    /**
     * Sets the value of the correctedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedInd(String value) {
        this.correctedInd = value;
    }

    /**
     * Gets the value of the correctedRecordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedRecordSequenceNum() {
        return correctedRecordSequenceNum;
    }

    /**
     * Sets the value of the correctedRecordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedRecordSequenceNum(String value) {
        this.correctedRecordSequenceNum = value;
    }

    /**
     * Gets the value of the voidInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidInd() {
        return voidInd;
    }

    /**
     * Sets the value of the voidInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidInd(String value) {
        this.voidInd = value;
    }

    /**
     * Gets the value of the voidedRecordSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidedRecordSequenceNum() {
        return voidedRecordSequenceNum;
    }

    /**
     * Sets the value of the voidedRecordSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidedRecordSequenceNum(String value) {
        this.voidedRecordSequenceNum = value;
    }

    /**
     * Gets the value of the marketplaceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketplaceId() {
        return marketplaceId;
    }

    /**
     * Sets the value of the marketplaceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketplaceId(String value) {
        this.marketplaceId = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link RecipientPolicyCoverageDtlType }
     *     
     */
    public RecipientPolicyCoverageDtlType getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecipientPolicyCoverageDtlType }
     *     
     */
    public void setPolicy(RecipientPolicyCoverageDtlType value) {
        this.policy = value;
    }

    /**
     * Gets the value of the recipient property.
     * 
     * @return
     *     possible object is
     *     {@link PersonInformationType }
     *     
     */
    public PersonInformationType getRecipient() {
        return recipient;
    }

    /**
     * Sets the value of the recipient property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonInformationType }
     *     
     */
    public void setRecipient(PersonInformationType value) {
        this.recipient = value;
    }

    /**
     * Gets the value of the recipientSpouse property.
     * 
     * @return
     *     possible object is
     *     {@link EOYPersonType }
     *     
     */
    public EOYPersonType getRecipientSpouse() {
        return recipientSpouse;
    }

    /**
     * Sets the value of the recipientSpouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link EOYPersonType }
     *     
     */
    public void setRecipientSpouse(EOYPersonType value) {
        this.recipientSpouse = value;
    }

    /**
     * Gets the value of the coverageHouseholdGrp property.
     * 
     * @return
     *     possible object is
     *     {@link CoverageHouseholdGrpType }
     *     
     */
    public CoverageHouseholdGrpType getCoverageHouseholdGrp() {
        return coverageHouseholdGrp;
    }

    /**
     * Sets the value of the coverageHouseholdGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverageHouseholdGrpType }
     *     
     */
    public void setCoverageHouseholdGrp(CoverageHouseholdGrpType value) {
        this.coverageHouseholdGrp = value;
    }

    /**
     * Gets the value of the recipientPolicyInformation property.
     * 
     * @return
     *     possible object is
     *     {@link TaxHouseholdPolicyInformationDtlType }
     *     
     */
    public TaxHouseholdPolicyInformationDtlType getRecipientPolicyInformation() {
        return recipientPolicyInformation;
    }

    /**
     * Sets the value of the recipientPolicyInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxHouseholdPolicyInformationDtlType }
     *     
     */
    public void setRecipientPolicyInformation(TaxHouseholdPolicyInformationDtlType value) {
        this.recipientPolicyInformation = value;
    }

    /**
     * Gets the value of the recordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Sets the value of the recordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordType(String value) {
        this.recordType = value;
    }

    /**
     * Gets the value of the lineNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLineNum() {
        return lineNum;
    }

    /**
     * Sets the value of the lineNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLineNum(BigInteger value) {
        this.lineNum = value;
    }

}
