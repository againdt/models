package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "PLAN_STM_BENEFITS")
public class PlanSTMBenefit implements Serializable {

	private static final long serialVersionUID = -7262883274767071680L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "planSTMBenefits_Seq")
	@SequenceGenerator(name = "planSTMBenefits_Seq", sequenceName = "PLAN_STM_BENEFITS_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_STM_ID")
	private PlanSTM planSTM;

	@Column(name = "NAME")
	private String name;

	@Column(name = "NETWORK_T1_DISPLAY")
	private String networkT1Display;

	@Column(name = "NETWORK_T1_COPAY_VAL")
	private String networkT1CopayValue;

	@Column(name = "NETWORK_T1_COPAY_ATTR")
	private String networkT1CopayAttr;

	@Column(name = "NETWORK_T1_COINSURANCE_VAL")
	private String networkT1CoinsuranceValue;

	@Column(name = "NETWORK_T1_COINSURANCE_ATTR")
	private String networkT1CoinsuranceAttr;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public PlanSTMBenefit() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanSTM getPlanSTM() {
		return planSTM;
	}

	public void setPlanSTM(PlanSTM planSTM) {
		this.planSTM = planSTM;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNetworkT1Display() {
		return networkT1Display;
	}

	public void setNetworkT1Display(String networkT1Display) {
		this.networkT1Display = networkT1Display;
	}

	public String getNetworkT1CopayValue() {
		return networkT1CopayValue;
	}

	public void setNetworkT1CopayValue(String networkT1CopayValue) {
		this.networkT1CopayValue = networkT1CopayValue;
	}

	public String getNetworkT1CopayAttr() {
		return networkT1CopayAttr;
	}

	public void setNetworkT1CopayAttr(String networkT1CopayAttr) {
		this.networkT1CopayAttr = networkT1CopayAttr;
	}

	public String getNetworkT1CoinsuranceValue() {
		return networkT1CoinsuranceValue;
	}

	public void setNetworkT1CoinsuranceValue(String networkT1CoinsuranceValue) {
		this.networkT1CoinsuranceValue = networkT1CoinsuranceValue;
	}

	public String getNetworkT1CoinsuranceAttr() {
		return networkT1CoinsuranceAttr;
	}

	public void setNetworkT1CoinsuranceAttr(String networkT1CoinsuranceAttr) {
		this.networkT1CoinsuranceAttr = networkT1CoinsuranceAttr;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
