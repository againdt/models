package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class FormularyDrugItemDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String formularyId;
	private Integer drugTierLevel;
	private String drugTierType1;
	private String drugTierType2;
	private String rxcuiCode;
	private String authRequired;
	private String stepTherapyRequired;
	
	public String getFormularyId() {
		return formularyId;
	}
	
	public void setFormularyId(String formularyId) {
		this.formularyId = formularyId;
	}
	
	public Integer getDrugTierLevel() {
		return drugTierLevel;
	}
	
	public void setDrugTierLevel(Integer drugTierLevel) {
		this.drugTierLevel = drugTierLevel;
	}
	
	public String getDrugTierType1() {
		return drugTierType1;
	}
	
	public void setDrugTierType1(String drugTierType1) {
		this.drugTierType1 = drugTierType1;
	}
	
	public String getDrugTierType2() {
		return drugTierType2;
	}
	
	public void setDrugTierType2(String drugTierType2) {
		this.drugTierType2 = drugTierType2;
	}
	
	public String getRxcuiCode() {
		return rxcuiCode;
	}
	
	public void setRxcuiCode(String rxcuiCode) {
		this.rxcuiCode = rxcuiCode;
	}
	
	public String getAuthRequired() {
		return authRequired;
	}
	
	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}
	
	public String getStepTherapyRequired() {
		return stepTherapyRequired;
	}
	
	public void setStepTherapyRequired(String stepTherapyRequired) {
		this.stepTherapyRequired = stepTherapyRequired;
	}
}
