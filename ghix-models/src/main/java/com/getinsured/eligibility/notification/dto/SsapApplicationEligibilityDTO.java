package com.getinsured.eligibility.notification.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;

public class SsapApplicationEligibilityDTO {

	private long primaryKey;

	private String caseNumber;

	private String source;

	private long coverageYear;

	private String applicationStatus;

	private EligibilityStatus eligibilityStatus;

	private ExchangeEligibilityStatus exchangeEligibilityStatus;

	private String csrLevel;

	private BigDecimal maxAPTCAmount;

	private BigDecimal electedAPTCAmount;

	private String enableEnrollment;

	private BigDecimal cmrHouseholdId;

	private List<SsapApplicantEligibilityDTO> ssapApplicantEligibilityDTO;

	private List<SsapApplicantEligibilityDTO> ssapApplicantEligibleEligibilityDTO;

	private List<SsapApplicantEligibilityDTO> ssapApplicantIneligibleEligibilityDTO;

	private Date eligibilityReceivedDate;

	private String eligibilityResponseType;

	private String nativeAmericanHousehold;

	private String primaryFirstName;

	private String primaryLastName;

	private String primaryMiddleName;

	private String suffix;

	private boolean outboundATSent;

	private String brokerName;

	private String applicationEligibilityStatus;

	private boolean incomeDMIExists;

	private boolean nonIncomeDMIExists;

	private boolean containsMedicaidCHIPAssessed;

	private boolean containsHealthLinkEligible;

	private boolean containsHealthLinkInEligible;

	private boolean containsNotSeekingCoverage;

	private String householdCaseId;

	private boolean sepEligible;

	private String applicationType;

	private boolean insideOEWindow;

	public String getPrimaryFirstName() {
		return primaryFirstName;
	}

	public void setPrimaryFirstName(String primaryFirstName) {
		this.primaryFirstName = primaryFirstName;
	}

	public String getPrimaryLastName() {
		return primaryLastName;
	}

	public void setPrimaryLastName(String primaryLastName) {
		this.primaryLastName = primaryLastName;
	}

	public String getPrimaryMiddleName() {
		return primaryMiddleName;
	}

	public void setPrimaryMiddleName(String primaryMiddleName) {
		this.primaryMiddleName = primaryMiddleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public long getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(long primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public ExchangeEligibilityStatus getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}

	public void setExchangeEligibilityStatus(
			ExchangeEligibilityStatus exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public BigDecimal getMaxAPTCAmount() {
		return maxAPTCAmount;
	}

	public void setMaxAPTCAmount(BigDecimal maxAPTCAmount) {
		this.maxAPTCAmount = maxAPTCAmount;
	}

	public BigDecimal getElectedAPTCAmount() {
		return electedAPTCAmount;
	}

	public void setElectedAPTCAmount(BigDecimal electedAPTCAmount) {
		this.electedAPTCAmount = electedAPTCAmount;
	}

	public String getEnableEnrollment() {
		return enableEnrollment;
	}

	public void setEnableEnrollment(String enableEnrollment) {
		this.enableEnrollment = enableEnrollment;
	}

	public List<SsapApplicantEligibilityDTO> getSsapApplicantEligibilityDTO() {
		return ssapApplicantEligibilityDTO;
	}

	public void setSsapApplicantEligibilityDTO(
			List<SsapApplicantEligibilityDTO> ssapApplicantEligibilityDTO) {
		this.ssapApplicantEligibilityDTO = ssapApplicantEligibilityDTO;
	}

	public BigDecimal getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(BigDecimal cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public Date getEligibilityReceivedDate() {
		return eligibilityReceivedDate;
	}

	public void setEligibilityReceivedDate(Date eligibilityReceivedDate) {
		this.eligibilityReceivedDate = eligibilityReceivedDate;
	}

	public String getEligibilityResponseType() {
		return eligibilityResponseType;
	}

	public void setEligibilityResponseType(String eligibilityResponseType) {
		this.eligibilityResponseType = eligibilityResponseType;
	}

	public String getNativeAmericanHousehold() {
		return nativeAmericanHousehold;
	}

	public void setNativeAmericanHousehold(String nativeAmericanHousehold) {
		this.nativeAmericanHousehold = nativeAmericanHousehold;
	}

	public List<SsapApplicantEligibilityDTO> getSsapApplicantEligibleEligibilityDTO() {
		return ssapApplicantEligibleEligibilityDTO;
	}

	public void setSsapApplicantEligibleEligibilityDTO(
			List<SsapApplicantEligibilityDTO> ssapApplicantEligibleEligibilityDTO) {
		this.ssapApplicantEligibleEligibilityDTO = ssapApplicantEligibleEligibilityDTO;
	}

	public List<SsapApplicantEligibilityDTO> getSsapApplicantIneligibleEligibilityDTO() {
		return ssapApplicantIneligibleEligibilityDTO;
	}

	public void setSsapApplicantIneligibleEligibilityDTO(
			List<SsapApplicantEligibilityDTO> ssapApplicantIneligibleEligibilityDTO) {
		this.ssapApplicantIneligibleEligibilityDTO = ssapApplicantIneligibleEligibilityDTO;
	}

	public boolean isOutboundATSent() {
		return outboundATSent;
	}

	public void setOutboundATSent(boolean outboundATSent) {
		this.outboundATSent = outboundATSent;
	}

	public String getBrokerName() {
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}

	public String getApplicationEligibilityStatus() {
		return applicationEligibilityStatus;
	}

	public void setApplicationEligibilityStatus(String applicationEligibilityStatus) {
		this.applicationEligibilityStatus = applicationEligibilityStatus;
	}

	public boolean isIncomeDMIExists() {
		return incomeDMIExists;
	}

	public void setIncomeDMIExists(boolean incomeDMIExists) {
		this.incomeDMIExists = incomeDMIExists;
	}

	public boolean isNonIncomeDMIExists() {
		return nonIncomeDMIExists;
	}

	public void setNonIncomeDMIExists(boolean nonIncomeDMIExists) {
		this.nonIncomeDMIExists = nonIncomeDMIExists;
	}

	public boolean isContainsMedicaidCHIPAssessed() {
		return containsMedicaidCHIPAssessed;
	}

	public void setContainsMedicaidCHIPAssessed(boolean containsMedicaidCHIPAssessed) {
		this.containsMedicaidCHIPAssessed = containsMedicaidCHIPAssessed;
	}

	public boolean isContainsHealthLinkEligible() {
		return containsHealthLinkEligible;
	}

	public void setContainsHealthLinkEligible(boolean containsHealthLinkEligible) {
		this.containsHealthLinkEligible = containsHealthLinkEligible;
	}

	public boolean isContainsHealthLinkInEligible() {
		return containsHealthLinkInEligible;
	}

	public void setContainsHealthLinkInEligible(boolean containsHealthLinkInEligible) {
		this.containsHealthLinkInEligible = containsHealthLinkInEligible;
	}

	public boolean isContainsNotSeekingCoverage() {
		return containsNotSeekingCoverage;
	}

	public void setContainsNotSeekingCoverage(boolean containsNotSeekingCoverage) {
		this.containsNotSeekingCoverage = containsNotSeekingCoverage;
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	public boolean isSepEligible() {
		return sepEligible;
	}

	public void setSepEligible(boolean sepEligible) {
		this.sepEligible = sepEligible;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public boolean isInsideOEWindow() {
		return insideOEWindow;
	}

	public void setInsideOEWindow(boolean insideOEWindow) {
		this.insideOEWindow = insideOEWindow;
	}
}
