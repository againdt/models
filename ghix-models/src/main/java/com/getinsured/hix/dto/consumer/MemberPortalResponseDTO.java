package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;



/**
 * @author Suhasini
 *
 */
public class MemberPortalResponseDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	boolean returnStatus = true;
	
	Map<String, String> memberIdMap = new HashMap<String, String>();
	List<SsapApplicantDTO> applicantDTOList = new ArrayList<SsapApplicantDTO>();
	SsapApplicationDTO ssapApplicationDTO;
	List<SsapApplicationDTO> applicationDTOList;

	public Map<String, String> getMemberIdMap() {
		return memberIdMap;
	}

	public void setMemberIdMap(Map<String, String> memberIdMap) {
		this.memberIdMap = memberIdMap;
	}

	public List<SsapApplicantDTO> getApplicantDTOList() {
		return applicantDTOList;
	}

	public void setApplicantDTOList(List<SsapApplicantDTO> applicantDTOList) {
		this.applicantDTOList = applicantDTOList;
	}
	
	public boolean isReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(boolean returnStatus) {
		this.returnStatus = returnStatus;
	}

	public SsapApplicationDTO getSsapApplicationDTO() {
		return ssapApplicationDTO;
	}

	public void setSsapApplicationDTO(SsapApplicationDTO ssapApplicationDTO) {
		this.ssapApplicationDTO = ssapApplicationDTO;
	}

	public List<SsapApplicationDTO> getApplicationDTOList() {
		return applicationDTOList;
	}

	public void setApplicationDTOList(List<SsapApplicationDTO> applicationDTOList) {
		this.applicationDTOList = applicationDTOList;
	}
	
	
	
	
	
	
}
