package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the roles database table.
 * 
 */

@Entity
@Table(name="issuer_quality_rating")

public class IssuerQualityRating  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public enum IssuerQualityRatingsXMLTags { 
		GlobalRating,
		AccessRating,
		GettingTimelyAppointmentsAndCare,
		GettingCareWithYourPersonalDoctor,
		StayingHealthyRating,
		CheckinForCancer,
		HelpForHealthyBehaviors,
		WellnessCareForChildren,
		PlanServiceRating, 
		PlanCustomerService,
		GettingClaimsPaid,
		ShoppingServicesToFindAffordableMedicalCare,
		GettingAnInterpreterWhenSeeingTheDoctor,
		MemberComplaintsAndAppeals,
		ClinicalCareRating,
		GettingTheRightSafeCare,
		DiabetesCare,
		HeartCare,
		MentalHealthCare, 
		RespiratoryCare,
		MaternityCare;
	}
	
	public enum IssuerQualityRatingsXMLTagsForCA { 
		GlobalRating,
		SummaryClinicalQualityManagement,
		ClinicalEffectiveness,
		AsthmaCare,
		BehavioralHealth,
		CardiovascularCare,
		DiabetesCare,
		DomainPatientSafety,
		PatientSafety,
		Prevention, 
		CheckingForCancer,
		MaternalCare,
		StayingHealthyAdult,
		StayingHealthyChild,
		SummaryEnrolleeExperience,
		DomainAccessToCare,
		AccessToCare,
		DomainCareCoordination,
		CareCoordination,
		DomainDoctorAndCare, 
		DoctorAndCare,
		SummaryPlanEfficiencyAffordabilityAndManagement,
		EfficiencyAndAffordability,
		EfficientCare,
		PlanService,
		EnrolleeExperienceWithHealthPlan
		;
	}
	
	public enum IssuerQualityRatingField {
		 IssuerId, 
		 QualityRating,
		 QualitySource, 
		 EffectiveDate,
		 EffectiveEndDate,
		 PlanHiosId; 
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IssuerQualityRating_Seq")
	@SequenceGenerator(name = "IssuerQualityRating_Seq", sequenceName = "issuer_quality_rating_seq", allocationSize = 1)
	private int id;
	
    @Column(name="issuer_id")
	private int issuerId;
	
	@Column(name="quality_rating", length=10)
	private String qualityRating ;
	
	@Column(name="quality_source", length=50)
	private String qualitySource;
	
	@Column(name="quality_rating_details", length=200)
	private String qualityRatingDetails;
	
	//HIX-5704 - start - Schema Change
	@Column(name="effective_date",nullable=false)
	private Date effectiveDate;	
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	//HIX-5704 - end

	//HIX-8972 - schema change start here
	@Column(name="effective_end_date",nullable=true)
	private Date effectiveEndDate;	

	@Column(name="PLAN_HIOS_ID",nullable = true, length=25)
	private String planHiosId;

	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "updated_by")
	private Integer lastUpdatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "update_timestamp")
	private Date lastUpdateTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date creationTimestamp;
	
	// HIX-59143 - soft delete implemented
	
	@Column(name = "is_deleted")
	private String isDeleted;
	
	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	//HIX-8972 - schema change end here
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getQualityRating() {
		return qualityRating;
	}

	public void setQualityRating(String qualityRating) {
		this.qualityRating = qualityRating;
	}

	public String getQualitySource() {
		return qualitySource;
	}

	public void setQualitySource(String qualitySource) {
		this.qualitySource = qualitySource;
	}

	public String getQualityRatingDetails() {
		return qualityRatingDetails;
	}

	public void setQualityRatingDetails(String qualityRatingDetails) {
		this.qualityRatingDetails = qualityRatingDetails;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getPlanHiosId() {
		return planHiosId;
	}

	public void setPlanHiosId(String planHiosId) {
		this.planHiosId = planHiosId;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
}
