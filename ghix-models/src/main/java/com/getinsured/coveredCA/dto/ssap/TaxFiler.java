
package com.getinsured.coveredCA.dto.ssap;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class TaxFiler {

    @Expose
    private Integer spouseHouseholdMemberId;
    @Expose
    private Boolean claimingDependantsOnFTRIndicator;
    @Expose
    private List<TaxFilerDependant> taxFilerDependants = new ArrayList<TaxFilerDependant>();
    @Expose
    private Boolean liveWithSpouseIndicator;
    @Expose
    private Boolean beClaimedAsDependantIndicator;

    public Integer getSpouseHouseholdMemberId() {
        return spouseHouseholdMemberId;
    }

    public void setSpouseHouseholdMemberId(Integer spouseHouseholdMemberId) {
        this.spouseHouseholdMemberId = spouseHouseholdMemberId;
    }

    public Boolean getClaimingDependantsOnFTRIndicator() {
        return claimingDependantsOnFTRIndicator;
    }

    public void setClaimingDependantsOnFTRIndicator(Boolean claimingDependantsOnFTRIndicator) {
        this.claimingDependantsOnFTRIndicator = claimingDependantsOnFTRIndicator;
    }

    public List<TaxFilerDependant> getTaxFilerDependants() {
        return taxFilerDependants;
    }

    public void setTaxFilerDependants(List<TaxFilerDependant> taxFilerDependants) {
        this.taxFilerDependants = taxFilerDependants;
    }

    public Boolean getLiveWithSpouseIndicator() {
        return liveWithSpouseIndicator;
    }

    public void setLiveWithSpouseIndicator(Boolean liveWithSpouseIndicator) {
        this.liveWithSpouseIndicator = liveWithSpouseIndicator;
    }
    
    public Boolean getBeClaimedAsDependantIndicator() {
		return beClaimedAsDependantIndicator;
	}

	public void setBeClaimedAsDependantIndicator(
			Boolean beClaimedAsDependantIndicator) {
		this.beClaimedAsDependantIndicator = beClaimedAsDependantIndicator;
	}

}
