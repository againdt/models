package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class SearchAgencyDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String businessName;
	private String certificationStatus;
	private Integer pageNumber;
	private String sortBy;
	private String sortOrder;

	public SearchAgencyDto() {	
	}


	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}


	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

}
