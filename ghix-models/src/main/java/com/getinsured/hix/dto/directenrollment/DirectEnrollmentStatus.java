package com.getinsured.hix.dto.directenrollment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Submit and Send statuses will be returned by respective endpoints
 * stage value on Application status maps the following way to be used as ordinal
 * if used to restart next step. 
 * response from Submit endpoints -> stage(3)
 * response from send endpoint -> stage(4)
 * @author sachin
 *
 */
public class DirectEnrollmentStatus {
	
	private static final Logger logger = LoggerFactory.getLogger(DirectEnrollmentStatus.class);

	public enum ApplicationStatus {
		INCOMPLETE(1),
		INCOMPLETE_PAYMENT_ERROR(1),
		INCOMPLETE_MAPPING_ERROR(1),
		INCOMPLETE_SUBMIT_ERROR(1),
		INCOMPLETE_ESIGN(2),
		ESIGNED(2),
		SUBMITTED(3),
		SUBMIT_DOCUMENT_UPLOAD_ERROR(2),
		SUBMIT_PAYMENT_ERROR(2),
		SUBMIT_RECORD_NOT_FOUND_ERROR(2),
		SUBMIT_MAPPING_ERROR(2),
		SUBMIT_ERROR(2),
		REDIRECT(2),
		SENT(4),
		SENT_STP_GATEWAY(3),
		SENT_ERROR(3),
		SENT_NOTIFIED(4),
		SENT_SFTPD(4),
		SENT_REJECTED(3),
		PENDING(5),
		REJECTED(5),
		DECLINED(5),
		RETURNED(5),
		WITHDRAWN(6),
		APPROVED(6),
		VERIFIED(6),
		SUSPENDED(6),
		CANCEL(6),
		INFORCE(6),
		CANCEL_TEST(6);
		int stage;

		private ApplicationStatus(int stage) { 
			this.stage = stage;
		}
		
		public boolean isBiggerOrEqual(ApplicationStatus status){
			return this.stage >= status.stage;
		}
	}

	public static boolean isEnrollmentStatus(ApplicationStatus status) {
		try {
			EnrollmentStatus.valueOf(status.toString());
			return true;
		} catch (IllegalArgumentException e) {
			logger.debug("'" + status + "' is NOT an enrollment status");
			return false;
		} catch (Exception e) {
			logger.debug("'" + status + "' is NOT an enrollment status");
			return false;
		}
	}
	
	public enum SubmitStatus {
		SUCCESS, FAILED_DOCUMENT_UPLOAD, PAYMENT_ERROR, RECORD_NOT_FOUND, MAPPING_ERROR, ERROR
	}

	public enum SendStatus {
		SUCCESS, STP_GATEWAY_ERROR, ERROR, REJECTED, SENT_NOTIFIED, SENT_SFTPD
	}
	
	public enum EnrollmentStatus {
		CANCEL, INFORCE, WITHDRAWN, INVALID, PENDING, SUSPENDED, DECLINED, VERIFIED
	}
	
	public enum RedirectStatus{
		SUCCESS, FAILURE, CONNECTION_ERROR
	}

}

