package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

public class NextAppointment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String date;	
	 
	private String type;	
	 
	private String appointmentUrl;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAppointmentUrl() {
		return appointmentUrl;
	}

	public void setAppointmentUrl(String appointmentUrl) {
		this.appointmentUrl = appointmentUrl;
	}	
	 

	
}
