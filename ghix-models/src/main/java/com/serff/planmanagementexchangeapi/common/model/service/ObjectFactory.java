
package com.serff.planmanagementexchangeapi.common.model.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.serff.planmanagementexchangeapi.common.model.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClientException_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/service", "clientException");
    private final static QName _ServerException_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/common/model/service", "serverException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.serff.planmanagementexchangeapi.common.model.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServerException }
     * 
     */
    public ServerException createServerException() {
        return new ServerException();
    }

    /**
     * Create an instance of {@link ClientException }
     * 
     */
    public ClientException createClientException() {
        return new ClientException();
    }

    /**
     * Create an instance of {@link BasicResponse }
     * 
     */
    public BasicResponse createBasicResponse() {
        return new BasicResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/service", name = "clientException")
    public JAXBElement<ClientException> createClientException(ClientException value) {
        return new JAXBElement<ClientException>(_ClientException_QNAME, ClientException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServerException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/service", name = "serverException")
    public JAXBElement<ServerException> createServerException(ServerException value) {
        return new JAXBElement<ServerException>(_ServerException_QNAME, ServerException.class, null, value);
    }

}
