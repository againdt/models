/**
 * 
 */
package com.getinsured.hix.model.serff;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="qhp_plan_rates")
public class PUFPlanRates {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_qhp_plan")
	@SequenceGenerator(name = "seq_qhp_plan", sequenceName = "seq_qhp_plan", allocationSize = 1)
	private int id;

	
	@Column(name="state")
	private String state;
	
	@Column(name="county")
	private String county;
	
	@Column(name="metal_level")
	private String metalLevel;
	
	@Column(name="issuer_name")
	private String issuerName;
	
	@Column(name="plan_marketing_name")
	private String planMarketingName;
	
	@Column(name="plan_type")
	private String planType;
	
	@Column(name="plan_id_standard_component")
	private String planIdStandardComponent;

	@Column(name="rating_area")
	private String ratingArea;
	
	@Column(name="premium_a_i_age_21")
	private Double premiumAIAage21;
	
	@Column(name="premium_a_i_age_27")
	private Double premiumAIAage27;
	
	@Column(name="age")
	private String age;
	
	@Column(name="premium")
	private Double premium;
	
	@Column(name="tobacco_premium")
	private Double tobaccoPremium;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getMetalLevel() {
		return metalLevel;
	}

	public void setMetalLevel(String metalLevel) {
		this.metalLevel = metalLevel;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPlanMarketingName() {
		return planMarketingName;
	}

	public void setPlanMarketingName(String planMarketingName) {
		this.planMarketingName = planMarketingName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanIdStandardComponent() {
		return planIdStandardComponent;
	}

	public void setPlanIdStandardComponent(String planIdStandardComponent) {
		this.planIdStandardComponent = planIdStandardComponent;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public Double getPremiumAIAage21() {
		return premiumAIAage21;
	}

	public void setPremiumAIAage21(Double premiumAIAage21) {
		this.premiumAIAage21 = premiumAIAage21;
	}

	public Double getPremiumAIAage27() {
		return premiumAIAage27;
	}

	public void setPremiumAIAage27(Double premiumAIAage27) {
		this.premiumAIAage27 = premiumAIAage27;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Double getPremium() {
		return premium;
	}

	public void setPremium(Double premium) {
		this.premium = premium;
	}

	public Double getTobaccoPremium() {
		return tobaccoPremium;
	}

	public void setTobaccoPremium(Double tobaccoPremium) {
		this.tobaccoPremium = tobaccoPremium;
	}
	
}
