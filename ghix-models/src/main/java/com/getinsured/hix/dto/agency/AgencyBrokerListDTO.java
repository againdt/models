package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

public class AgencyBrokerListDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private long id;
	private long totalRecords;
	private String sortBy;
	private String sortOrder;
	private String pageNumber;
	private List<AgencyBrokerDTO> brokersList;
	private List<String> certificationStatus;
	private List<SearchBrokerDTO> brokers;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}
	public List<AgencyBrokerDTO> getBrokersList() {
		return brokersList;
	}
	public void setBrokersList(List<AgencyBrokerDTO> brokersList) {
		this.brokersList = brokersList;
	}
	public List<String> getCertificationStatus() {
		return certificationStatus;
	}
	public void setCertificationStatus(List<String> certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	public List<SearchBrokerDTO> getBrokers() {
		return brokers;
	}
	public void setBrokers(List<SearchBrokerDTO> brokers) {
		this.brokers = brokers;
	}
	
	
}