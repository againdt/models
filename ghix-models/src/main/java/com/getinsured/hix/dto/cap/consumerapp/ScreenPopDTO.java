package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.cap.consumerapp.enums.PageType;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;

public class ScreenPopDTO implements Serializable {	
	private static final long serialVersionUID = -8057560569227277600L;
	private CallContextDTO callContextDTO;
	private HouseholdDTO householdDTO;
	private List<SsapApplicantDTO> applicantDTOList;
	
	private List<SsapApplicationDTO> applicationDTOs;
	private List<com.getinsured.hix.dto.cap.consumerapp.EnrollmentDTO> enrollmentDTOs;
	private List<HouseholdDTO> householdDTOs; // for undetermined caller
	private AffiliateInfoDTO affiliateInfoDTO;
	private EligLeadDTO eligLeadDTO;
	private ScreenPopEmployeeDto screenPopEmployeeDto;
	private CustomerHistoryDTO customerHistoryDTO;
	private PageType pageType;
	
	
	public CallContextDTO getCallContextDTO() {
		return callContextDTO;
	}
	
	public HouseholdDTO getHouseholdDTO() {
		return householdDTO;
	}
	public void setHouseholdDTO(HouseholdDTO householdDTO) {
		this.householdDTO = householdDTO;
	}
	public List<SsapApplicantDTO> getApplicantDTOList() {
		return applicantDTOList;
	}
	public void setApplicantDTOList(List<SsapApplicantDTO> applicantDTOList) {
		this.applicantDTOList = applicantDTOList;
	}
	public List<SsapApplicationDTO> getApplicationDTOs() {
		return applicationDTOs;
	}
	public void setApplicationDTOs(List<SsapApplicationDTO> applicationDTOs) {
		this.applicationDTOs = applicationDTOs;
	}
	public List<com.getinsured.hix.dto.cap.consumerapp.EnrollmentDTO> getEnrollmentDTOs() {
		return enrollmentDTOs;
	}
	public void setEnrollmentDTOs(
			List<com.getinsured.hix.dto.cap.consumerapp.EnrollmentDTO> enrollmentDTOs) {
		this.enrollmentDTOs = enrollmentDTOs;
	}
	public List<HouseholdDTO> getHouseholdDTOs() {
		return householdDTOs;
	}
	public void setHouseholdDTOs(List<HouseholdDTO> householdDTOs) {
		this.householdDTOs = householdDTOs;
	}
	public AffiliateInfoDTO getAffiliateInfoDTO() {
		return affiliateInfoDTO;
	}
	public void setAffiliateInfoDTO(AffiliateInfoDTO affiliateInfoDTO) {
		this.affiliateInfoDTO = affiliateInfoDTO;
	}
	
	public EligLeadDTO getEligLeadDTO() {
		return eligLeadDTO;
	}

	public void setEligLeadDTO(EligLeadDTO eligLeadDTO) {
		this.eligLeadDTO = eligLeadDTO;
	}

	public CustomerHistoryDTO getCustomerHistoryDTO() {
		return customerHistoryDTO;
	}
	public void setCustomerHistoryDTO(CustomerHistoryDTO customerHistoryDTO) {
		this.customerHistoryDTO = customerHistoryDTO;
	}
	public void setCallContextDTO(CallContextDTO callContextDTO) {
		this.callContextDTO = callContextDTO;
	}

	public PageType getPageType() {
		return pageType;
	}

	public void setPageType(PageType pageType) {
		this.pageType = pageType;
	}
	
	public ScreenPopEmployeeDto getScreenPopEmployeeDto() {
		return screenPopEmployeeDto;
	}
	
	public void setScreenPopEmployeeDto(ScreenPopEmployeeDto screenPopEmployeeDto) {
		this.screenPopEmployeeDto = screenPopEmployeeDto;
	}
}	
