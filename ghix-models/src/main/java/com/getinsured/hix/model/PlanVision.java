package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;


/**
 * @author sharma_va
 *
 */

@Audited
@Entity
@Table(name = "PLAN_VISION")
public class PlanVision implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_VISION_SEQ")
	@SequenceGenerator(name = "PLAN_VISION_SEQ", sequenceName = "PLAN_VISION_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;

	@Column(name = "OON_AVAILABILITY")
	private String outOfNetAvailability;
	
	@Column(name = "OON_COVERAGE")
	private String outOfNetCoverage;
	
	@Column(name = "PROVIDER_NETWORK_URL")
	private String providerNetURL;
	
	@Column(name = "PREMIUM_PAYMENT")
	private String premiumPayment;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	public PlanVision(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getOutOfNetAvailability() {
		return outOfNetAvailability;
	}

	public void setOutOfNetAvailability(String outOfNetAvailability) {
		this.outOfNetAvailability = outOfNetAvailability;
	}

	public String getOutOfNetCoverage() {
		return outOfNetCoverage;
	}

	public void setOutOfNetCoverage(String outOfNetCoverage) {
		this.outOfNetCoverage = outOfNetCoverage;
	}

	public String getProviderNetURL() {
		return providerNetURL;
	}

	public void setProviderNetURL(String providerNetURL) {
		this.providerNetURL = providerNetURL;
	}

	public String getPremiumPayment() {
		return premiumPayment;
	}

	public void setPremiumPayment(String premiumPayment) {
		this.premiumPayment = premiumPayment;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	 /* To AutoUpdate created and updated dates while persisting object */
	 @PrePersist
	 public void prePersist() {
	  this.setCreationTimestamp(new TSDate());
	  this.setLastUpdateTimestamp(new TSDate());
	 }

	 /* To AutoUpdate updated dates while updating object */
	 @PreUpdate
	 public void preUpdate() {
	  this.setLastUpdateTimestamp(new TSDate());
	 }
	
}
