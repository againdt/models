package com.getinsured.hix.dto.enrollment.aca;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Awesome Pojo Generator
 */
public class PersonName {
	@JsonProperty("PersonGivenName")
	
	private PersonGivenName PersonGivenName;
	@JsonProperty("PersonSurName")
	
	private PersonSurName PersonSurName;

	@JsonProperty("PersonMiddleName")
	
	private PersonSurName PersonMiddleName;

	@JsonProperty("PersonNameSuffixText")
	
	private PersonSurName PersonNameSuffixText;

	public void setPersonGivenName(PersonGivenName PersonGivenName) {
		this.PersonGivenName = PersonGivenName;
	}

	public PersonGivenName getPersonGivenName() {
		return PersonGivenName;
	}

	public void setPersonSurName(PersonSurName PersonSurName) {
		this.PersonSurName = PersonSurName;
	}

	public PersonSurName getPersonSurName() {
		return PersonSurName;
	}
	
	public PersonSurName getPersonMiddleName() {
		return PersonMiddleName;
	}

	public void setPersonMiddleName(PersonSurName personMiddleName) {
		PersonMiddleName = personMiddleName;
	}

	public PersonSurName getPersonNameSuffixText() {
		return PersonNameSuffixText;
	}

	public void setPersonNameSuffixText(PersonSurName personNameSuffixText) {
		PersonNameSuffixText = personNameSuffixText;
	}

}