package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="ENROLLMENT_1095")
public class Enrollment1095 implements Serializable
{
	public enum YorNFlagIndicator{
		Y ,N;
	}
	
	public enum CorrectionSource {
		EDIT_TOOL, MANUAL, DB_REFRESH, INBOUND_MANUAL_CORRECTION;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_1095_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_1095_SEQ", sequenceName = "ENROLLMENT_1095_SEQ", allocationSize = 1)
	private int id;
	
	//@OneToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name = "EXCHG_ASSIGNED_POLICY_ID")
	@Column(name = "EXCHG_ASSIGNED_POLICY_ID")
	private Integer exchgAsignedPolicyId;
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private String houseHoldCaseId;
	
	@Column(name = "COVERAGE_YEAR")
	private Integer coverageYear;
	
	@Column(name = "CORRECTED_CHECKBOX_INDICATOR", length = 1)
	//@Enumerated(EnumType.STRING)
	private String correctedCheckBoxIndicator;
	
	
	@Column(name = "VOID_CHECKBOX_INDICATOR", length = 1)
	//@Enumerated(EnumType.STRING)
	private String voidCheckboxindicator;
	
	@Column(name = "POLICY_ISSUER_NAME")
	private String policyIssuerName; 
	
	@Column(name = "POLICY_START_DATE")
	private Date policyStartDate;
	
	@Column(name ="POLICY_END_DATE")
	private Date policyEndDate;
	
	@Column(name ="RESEND_PDF_FLAG", length = 1)
	//@Enumerated(EnumType.STRING)
	private String resendPdfFlag;
	
	@Column(name="CORRECTION_SOURCE")
	//@Enumerated(EnumType.STRING)
	private String correctionSource;
	
	@Column(name ="EDIT_TOOL_OVERWRITTEN")
	//@Enumerated(EnumType.STRING)
	private String editToolOverwritten;
	
	@Column(name ="CORRECTION_INDICATOR_PDF")
	//@Enumerated(EnumType.STRING)
	private String correctionIndicatorPdf;
	
	@Column(name ="CORRECTION_INDICATOR_XML")
	//@Enumerated(EnumType.STRING)
	private String correctionIndicatorXml;
		
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	//@Audited
	//@OneToOne
	//@JoinColumn(name="CREATED_BY")
	@Column(name="CREATED_BY")
	private Integer createdBy;

	//@Audited
	//@OneToOne
	//@JoinColumn(name="LAST_UPDATED_BY")
	@Column(name="LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Column(name = "CORRECTED_RECORD_SEQ_NUM")
	private String correctedRecordSeqNum;
	
	@Column(name = "PDF_GENERATION_TIMESTAMP")
	private Date pdfGeneratedOn;
	
	@Column(name = "CMS_XML_FILE_NAME")
	private String cmsXmlFileName;
	
	@Column(name = "CMS_XML_GENERATION_TIMESTAMP")
	private Date cmsXmlGeneratedOn;
	
	@Column(name = "UPDATE_NOTES")
	private String updateNotes;
	
	@Column(name = "PDF_SKIPPED_FLAG")
	private String pdfSkippedFlag;
	
	@Column(name = "PDF_SKIPPED_MSG")
	private String pdfSkippedMsg;
	
	@Column(name = "XML_SKIPPED_FLAG")
	private String xmlSkippedFlag;
	
	@Column(name = "XML_SKIPPED_MSG")
	private String xmlSkippedMsg;
	
	//@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	//@ManyToOne
	//@JoinColumn(name="NOTICE_ID")
	@Column(name="NOTICE_ID")
	private Integer notice;
	
	@Column(name = "IS_ACTIVE")
	private String isActive = "Y";
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy="enrollment1095", cascade = CascadeType.ALL)
	private List<EnrollmentMember1095> enrollmentMembers;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy="enrollment1095", cascade = CascadeType.ALL)
	private List<EnrollmentPremium1095> enrollmentPremiums;
	
	@Column(name = "ORIGINAL_BATCH_ID")
	private String originalBatchId;
	
	@Column(name = "PDF_FILE_NAME")
	private String pdfFileName;
	
	@Column(name = "BATCH_CATEGORY_CODE")
	private String batchCategoryCode;
	
	@Column(name = "INBOUND_BCC")
	private String inboundBatchCategoryCode;
	
	@Column(name = "INBOUND_ACTION")
	private String inboundAction;
	
	@Column(name = "ENROLLMENT_IN_1095_ID")
	private Integer enrollmentIn1095Id;
	
	@Column(name = "RESUB_CRRCTN_IND")
	private String resubCorrectionInd;
	
	@Column(name = "CMS_PLAN_ID")
	private String cmsPlanId;
	
	@Column(name = "ISSUER_EIN")
	private String issuerEin;
	
	@Column(name = "EMPLOYER_MEC")
	private String emoployerMec;
	
	@Column(name = "IS_ADDRESS_UPDATED")
	private String isAddressUpdated;
	
	@Column(name = "IS_OBSOLETE")
	private String isObsolete = "N";
	
	@Transient
	private Boolean isOverwritten;
	
	@Type(type = "numeric_boolean")
    @Column(name="IS_FINANCIAL")
    private boolean isFinancial;
	
	@Type(type = "numeric_boolean")
    @Column(name="IS_TERM_BY_NON_PAYMENT")
    private boolean isTermByNonPayment;
	
	@Column(name = "PREM_PAID_TO_DATE_END")
	private Date premiumPaidToDateEnd;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the exchgAsignedPolicyId
	 */
	public Integer getExchgAsignedPolicyId() {
		return exchgAsignedPolicyId;
	}

	/**
	 * @param exchgAsignedPolicyId the exchgAsignedPolicyId to set
	 */
	public void setExchgAsignedPolicyId(Integer exchgAsignedPolicyId) {
		this.exchgAsignedPolicyId = exchgAsignedPolicyId;
	}

	/**
	 * @return the houseHoldCaseId
	 */
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}

	/**
	 * @param houseHoldCaseId the houseHoldCaseId to set
	 */
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}

	/**
	 * @return the coverageYear
	 */
	public Integer getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	/**
	 * @return the correctedCheckBoxIndicator
	 */
	public String getCorrectedCheckBoxIndicator() {
		return correctedCheckBoxIndicator;
	}

	/**
	 * @param correctedCheckBoxIndicator the correctedCheckBoxIndicator to set
	 */
	public void setCorrectedCheckBoxIndicator(String correctedCheckBoxIndicator) {
		this.correctedCheckBoxIndicator = correctedCheckBoxIndicator;
	}

	/**
	 * @return the policyIssuerName
	 */
	public String getPolicyIssuerName() {
		return policyIssuerName;
	}

	/**
	 * @param policyIssuerName the policyIssuerName to set
	 */
	public void setPolicyIssuerName(String policyIssuerName) {
		this.policyIssuerName = policyIssuerName;
	}

	/**
	 * @return the policyStartDate
	 */
	public Date getPolicyStartDate() {
		return policyStartDate;
	}

	/**
	 * @param policyStartDate the policyStartDate to set
	 */
	public void setPolicyStartDate(Date policyStartDate) {
		this.policyStartDate = policyStartDate;
	}

	/**
	 * @return the policyEndDate
	 */
	public Date getPolicyEndDate() {
		return policyEndDate;
	}

	/**
	 * @param policyEndDate the policyEndDate to set
	 */
	public void setPolicyEndDate(Date policyEndDate) {
		this.policyEndDate = policyEndDate;
	}

	/**
	 * @return the resendPdfFlag
	 */
	public String getResendPdfFlag() {
		return resendPdfFlag;
	}

	/**
	 * @param resendPdfFlag the resendPdfFlag to set
	 */
	public void setResendPdfFlag(String resendPdfFlag) {
		this.resendPdfFlag = resendPdfFlag;
	}

	/**
	 * @return the correctionSource
	 */
	public String getCorrectionSource() {
		return correctionSource;
	}

	/**
	 * @param correctionSource the correctionSource to set
	 */
	public void setCorrectionSource(String correctionSource) {
		this.correctionSource = correctionSource;
	}

	/**
	 * @return the editToolOverwritten
	 */
	public String getEditToolOverwritten() {
		return editToolOverwritten;
	}

	/**
	 * @param editToolOverwritten the editToolOverwritten to set
	 */
	public void setEditToolOverwritten(String editToolOverwritten) {
		this.editToolOverwritten = editToolOverwritten;
	}

	/**
	 * @return the correctionIndicatorPdf
	 */
	public String getCorrectionIndicatorPdf() {
		return correctionIndicatorPdf;
	}

	/**
	 * @param correctionIndicatorPdf the correctionIndicatorPdf to set
	 */
	public void setCorrectionIndicatorPdf(String correctionIndicatorPdf) {
		this.correctionIndicatorPdf = correctionIndicatorPdf;
	}

	/**
	 * @return the correctionIndicatorXml
	 */
	public String getCorrectionIndicatorXml() {
		return correctionIndicatorXml;
	}

	/**
	 * @param correctionIndicatorXml the correctionIndicatorXml to set
	 */
	public void setCorrectionIndicatorXml(String correctionIndicatorXml) {
		this.correctionIndicatorXml = correctionIndicatorXml;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the correctedRecordSeqNum
	 */
	public String getCorrectedRecordSeqNum() {
		return correctedRecordSeqNum;
	}

	/**
	 * @param correctedRecordSeqNum the correctedRecordSeqNum to set
	 */
	public void setCorrectedRecordSeqNum(String correctedRecordSeqNum) {
		this.correctedRecordSeqNum = correctedRecordSeqNum;
	}

	/**
	 * @return the pdfGeneratedOn
	 */
	public Date getPdfGeneratedOn() {
		return pdfGeneratedOn;
	}

	/**
	 * @param pdfGeneratedOn the pdfGeneratedOn to set
	 */
	public void setPdfGeneratedOn(Date pdfGeneratedOn) {
		this.pdfGeneratedOn = pdfGeneratedOn;
	}

	/**
	 * @return the cmsXmlFileName
	 */
	public String getCmsXmlFileName() {
		return cmsXmlFileName;
	}

	/**
	 * @param cmsXmlFileName the cmsXmlFileName to set
	 */
	public void setCmsXmlFileName(String cmsXmlFileName) {
		this.cmsXmlFileName = cmsXmlFileName;
	}

	/**
	 * @return the cmsXmlGeneratedOn
	 */
	public Date getCmsXmlGeneratedOn() {
		return cmsXmlGeneratedOn;
	}

	/**
	 * @param cmsXmlGeneratedOn the cmsXmlGeneratedOn to set
	 */
	public void setCmsXmlGeneratedOn(Date cmsXmlGeneratedOn) {
		this.cmsXmlGeneratedOn = cmsXmlGeneratedOn;
	}

	/**
	 * @return the updateNotes
	 */
	public String getUpdateNotes() {
		return updateNotes;
	}

	/**
	 * @param updateNotes the updateNotes to set
	 */
	public void setUpdateNotes(String updateNotes) {
		this.updateNotes = updateNotes;
	}

	/**
	 * @return the pdfSkippedFlag
	 */
	public String getPdfSkippedFlag() {
		return pdfSkippedFlag;
	}

	/**
	 * @param pdfSkippedFlag the pdfSkippedFlag to set
	 */
	public void setPdfSkippedFlag(String pdfSkippedFlag) {
		this.pdfSkippedFlag = pdfSkippedFlag;
	}

	/**
	 * @return the pdfSkippedMsg
	 */
	public String getPdfSkippedMsg() {
		return pdfSkippedMsg;
	}

	/**
	 * @param pdfSkippedMsg the pdfSkippedMsg to set
	 */
	public void setPdfSkippedMsg(String pdfSkippedMsg) {
		this.pdfSkippedMsg = pdfSkippedMsg;
	}

	/**
	 * @return the xmlSkippedFlag
	 */
	public String getXmlSkippedFlag() {
		return xmlSkippedFlag;
	}

	/**
	 * @param xmlSkippedFlag the xmlSkippedFlag to set
	 */
	public void setXmlSkippedFlag(String xmlSkippedFlag) {
		this.xmlSkippedFlag = xmlSkippedFlag;
	}

	/**
	 * @return the xmlSkippedMsg
	 */
	public String getXmlSkippedMsg() {
		return xmlSkippedMsg;
	}

	/**
	 * @param xmlSkippedMsg the xmlSkippedMsg to set
	 */
	public void setXmlSkippedMsg(String xmlSkippedMsg) {
		this.xmlSkippedMsg = xmlSkippedMsg;
	}

	/**
	 * @return the notice
	 */
	public Integer getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(Integer notice) {
		this.notice = notice;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public List<EnrollmentMember1095> getEnrollmentMembers() {
		return enrollmentMembers;
	}

	public void setEnrollmentMembers(List<EnrollmentMember1095> enrollmentMembers) {
		this.enrollmentMembers = enrollmentMembers;
	}

	public List<EnrollmentPremium1095> getEnrollmentPremiums() {
		return enrollmentPremiums;
	}

	public void setEnrollmentPremiums(List<EnrollmentPremium1095> enrollmentPremiums) {
		this.enrollmentPremiums = enrollmentPremiums;
	}

	/**
	 * @return the originalBatchId
	 */
	public String getOriginalBatchId() {
		return originalBatchId;
	}

	/**
	 * @param originalBatchId the originalBatchId to set
	 */
	public void setOriginalBatchId(String originalBatchId) {
		this.originalBatchId = originalBatchId;
	}
	
	/**
	 * Compare flat fields
	 * @param other
	 * @return
	 */
	public boolean compareFlatFields(Enrollment1095 other) {
		if (other == null) {
			return false;
		}
		if (coverageYear == null) {
			if (other.coverageYear != null) {
				return false;
			}
		} else if (!(coverageYear.compareTo(other.coverageYear) == 0)) {
			return false;
		}
		if (houseHoldCaseId == null) {
			if (other.houseHoldCaseId != null) {
				return false;
			}
		} else if (!houseHoldCaseId.equals(other.houseHoldCaseId)) {
			return false;
		}
		if (policyEndDate == null) {
			if (other.policyEndDate != null) {
				return false;
			}
		} else if (!(policyEndDate.compareTo(other.policyEndDate) == 0)) {
			return false;
		}
		/* HIX-108188 : Suppress Corrected 1095A's when only change is carrier name
		 if (policyIssuerName == null) {
			if (other.policyIssuerName != null) {
				return false;
			}
		} else if (!policyIssuerName.equals(other.policyIssuerName)) {
			return false;
		}*/
		if (policyStartDate == null) {
			if (other.policyStartDate != null) {
				return false;
			}
		} else if (!(policyStartDate.compareTo(other.policyStartDate) == 0)) {
			return false;
		}
		if (premiumPaidToDateEnd == null) {
			if (other.premiumPaidToDateEnd != null) {
				return false;
			}
		} else if (!(premiumPaidToDateEnd.compareTo(other.premiumPaidToDateEnd) == 0)) {
			return false;
		}
		return true;
	}

	public String getVoidCheckboxindicator() {
		return voidCheckboxindicator;
	}

	public void setVoidCheckboxindicator(String voidCheckboxindicator) {
		this.voidCheckboxindicator = voidCheckboxindicator;
	}

	/**
	 * @return the pdfFileName
	 */
	public String getPdfFileName() {
		return pdfFileName;
	}

	/**
	 * @param pdfFileName the pdfFileName to set
	 */
	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}

	public String getBatchCategoryCode() {
		return batchCategoryCode;
	}

	public void setBatchCategoryCode(String batchCategoryCode) {
		this.batchCategoryCode = batchCategoryCode;
	}

	public String getInboundBatchCategoryCode() {
		return inboundBatchCategoryCode;
	}

	public void setInboundBatchCategoryCode(String inboundBatchCategoryCode) {
		this.inboundBatchCategoryCode = inboundBatchCategoryCode;
	}

	public String getInboundAction() {
		return inboundAction;
	}

	public void setInboundAction(String inboundAction) {
		this.inboundAction = inboundAction;
	}

	public Integer getEnrollmentIn1095Id() {
		return enrollmentIn1095Id;
	}

	public void setEnrollmentIn1095Id(Integer enrollmentIn1095Id) {
		this.enrollmentIn1095Id = enrollmentIn1095Id;
	}

	public String getResubCorrectionInd() {
		return resubCorrectionInd;
	}

	public void setResubCorrectionInd(String resubCorrectionInd) {
		this.resubCorrectionInd = resubCorrectionInd;
	}

	public Boolean getIsOverwritten() {
		return isOverwritten;
	}

	public void setIsOverwritten(Boolean isOverwritten) {
		this.isOverwritten = isOverwritten;
	}

	public String getCmsPlanId() {
		return cmsPlanId;
	}

	public void setCmsPlanId(String cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}



	public String getIssuerEin() {
		return issuerEin;
	}

	public void setIssuerEin(String issuerEin) {
		this.issuerEin = issuerEin;
	}

	public String getEmoployerMec() {
		return emoployerMec;
	}

	public void setEmoployerMec(String emoployerMec) {
		this.emoployerMec = emoployerMec;
	}
	
	public String getIsAddressUpdated() {
		return isAddressUpdated;
	}

	public void setIsAddressUpdated(String isAddressUpdated) {
		this.isAddressUpdated = isAddressUpdated;
	}

	public String getIsObsolete() {
		return isObsolete;
	}

	public void setIsObsolete(String isObsolete) {
		this.isObsolete = isObsolete;
	}

	public boolean isFinancial() {
		return isFinancial;
	}

	public void setFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}

	public boolean isTermByNonPayment() {
		return isTermByNonPayment;
	}

	public void setTermByNonPayment(boolean isTermByNonPayment) {
		this.isTermByNonPayment = isTermByNonPayment;
	}

	public Date getPremiumPaidToDateEnd() {
		return premiumPaidToDateEnd;
	}

	public void setPremiumPaidToDateEnd(Date premiumPaidToDateEnd) {
		this.premiumPaidToDateEnd = premiumPaidToDateEnd;
	}

	/**
	 * Get Recepient member of the enrollment 1095
	 * @return EnrollmentMember1095 recepient
	 */
	public EnrollmentMember1095 getRecepient(){
		EnrollmentMember1095 recepient = new EnrollmentMember1095();
		if(null != enrollmentMembers){
			for(EnrollmentMember1095  member : enrollmentMembers){
				if( null != member && (EnrollmentMember1095.MemberType.RECEPIENT.toString().equalsIgnoreCase(member.getMemberType()))){
					recepient = member;
					break;
				}
			}
		}
		return recepient;
	}
	
	public Map<Integer, EnrollmentPremium1095> getEnrollmentPremium1095Map() {
		Map<Integer, EnrollmentPremium1095> premiumMap = new HashMap<Integer, EnrollmentPremium1095>();
		if(null != this.enrollmentPremiums){
			for(EnrollmentPremium1095 enrollmentPremium : this.enrollmentPremiums){
				if(enrollmentPremium.getIsActive()==null || enrollmentPremium.getIsActive().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())){
					premiumMap.put(enrollmentPremium.getMonthNumber(), enrollmentPremium);
				}
			}
		}
		return premiumMap;
	}
	
	public EnrollmentPremium1095 getEnrollmentPremium1095ForMonth(Integer month) {
		return this.getEnrollmentPremium1095Map().get(month);
	}
	/**
	 * 
	 * @return
	 */
	public List<EnrollmentPremium1095> getActiveEnrollmentPremiums() {
		if(enrollmentPremiums != null && !enrollmentPremiums.isEmpty()){
			List<EnrollmentPremium1095> activeEnrollmentPremium1095List = new ArrayList<EnrollmentPremium1095>();
			for(EnrollmentPremium1095 enrollmentPremium : enrollmentPremiums){
				if(enrollmentPremium.getIsActive().equalsIgnoreCase("Y")){
					activeEnrollmentPremium1095List.add(enrollmentPremium);
				}
			}
			return activeEnrollmentPremium1095List;
		}
		return enrollmentPremiums;
	}
	
	public List<EnrollmentPremium1095> getEnrollmentPremiumsByIsActiveFlag(final String isActiveFlag) {
		if(enrollmentPremiums != null && !enrollmentPremiums.isEmpty()){
			List<EnrollmentPremium1095> enrollmentPremium1095List = new ArrayList<EnrollmentPremium1095>();
			for(EnrollmentPremium1095 enrollmentPremium : enrollmentPremiums){
				if(enrollmentPremium.getIsActive().equalsIgnoreCase(isActiveFlag)){
					enrollmentPremium1095List.add(enrollmentPremium);
				}
			}
			return enrollmentPremium1095List;
		}
		return enrollmentPremiums;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<EnrollmentMember1095> getActiveEnrollmentMembers() {
		if(enrollmentMembers != null && !enrollmentMembers.isEmpty()){
			List<EnrollmentMember1095> activeEnrollmentMember1095List = new ArrayList<EnrollmentMember1095>();
			for(EnrollmentMember1095 enrollmentMember1095 : enrollmentMembers){
				if(enrollmentMember1095.getIsActive().equalsIgnoreCase("Y")){
					activeEnrollmentMember1095List.add(enrollmentMember1095);
				}
			}
			return activeEnrollmentMember1095List;
		}
		return enrollmentMembers;
	}
	
	public List<EnrollmentMember1095> getEnrollmentMembersByIsActiveFlag(final String isActiveFlag) {
		if(enrollmentMembers != null && !enrollmentMembers.isEmpty()){
			List<EnrollmentMember1095> enrollmentMember1095List = new ArrayList<EnrollmentMember1095>();
			for(EnrollmentMember1095 enrollmentMember1095 : enrollmentMembers){
				if(enrollmentMember1095.getIsActive().equalsIgnoreCase(isActiveFlag)){
					enrollmentMember1095List.add(enrollmentMember1095);
				}
			}
			return enrollmentMember1095List;
		}
		return enrollmentMembers;
	}
}
