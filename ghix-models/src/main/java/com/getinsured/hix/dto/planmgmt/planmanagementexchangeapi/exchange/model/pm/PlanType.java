
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.pm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlanType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="HMO"/>
 *     &lt;enumeration value="PPO"/>
 *     &lt;enumeration value="EPO"/>
 *     &lt;enumeration value="POS"/>
 *     &lt;enumeration value="HSA"/>
 *     &lt;enumeration value="HDHP"/>
 *     &lt;enumeration value="FFS"/>
 *     &lt;enumeration value="OTH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "")
@XmlEnum
@XmlRootElement(name="planType")
public enum PlanType {

    HMO,
    PPO,
    EPO,
    POS,
    HSA,
    HDHP,
    FFS,
    OTH;

    public String value() {
        return name();
    }

    public static PlanType fromValue(String v) {
        return valueOf(v);
    }

}
