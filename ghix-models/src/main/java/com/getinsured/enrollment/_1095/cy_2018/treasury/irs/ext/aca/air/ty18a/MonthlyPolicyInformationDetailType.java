
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Month by month Policy information
 * 			
 * 
 * <p>Java class for MonthlyPolicyInformationDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MonthlyPolicyInformationDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TotalHsldMonthlyPremiumAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}SLCSPAdjMonthlyPremiumAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}HouseholdAPTCAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthlyPolicyInformationDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "totalHsldMonthlyPremiumAmt",
    "slcspAdjMonthlyPremiumAmt",
    "householdAPTCAmt",
    "errorDetail"
})
public class MonthlyPolicyInformationDetailType {

    @XmlElement(name = "TotalHsldMonthlyPremiumAmt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected BigDecimal totalHsldMonthlyPremiumAmt;
    @XmlElement(name = "SLCSPAdjMonthlyPremiumAmt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected BigDecimal slcspAdjMonthlyPremiumAmt;
    @XmlElement(name = "HouseholdAPTCAmt", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected BigDecimal householdAPTCAmt;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the totalHsldMonthlyPremiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalHsldMonthlyPremiumAmt() {
        return totalHsldMonthlyPremiumAmt;
    }

    /**
     * Sets the value of the totalHsldMonthlyPremiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalHsldMonthlyPremiumAmt(BigDecimal value) {
        this.totalHsldMonthlyPremiumAmt = value;
    }

    /**
     * Gets the value of the slcspAdjMonthlyPremiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSLCSPAdjMonthlyPremiumAmt() {
        return slcspAdjMonthlyPremiumAmt;
    }

    /**
     * Sets the value of the slcspAdjMonthlyPremiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSLCSPAdjMonthlyPremiumAmt(BigDecimal value) {
        this.slcspAdjMonthlyPremiumAmt = value;
    }

    /**
     * Gets the value of the householdAPTCAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHouseholdAPTCAmt() {
        return householdAPTCAmt;
    }

    /**
     * Sets the value of the householdAPTCAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHouseholdAPTCAmt(BigDecimal value) {
        this.householdAPTCAmt = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
