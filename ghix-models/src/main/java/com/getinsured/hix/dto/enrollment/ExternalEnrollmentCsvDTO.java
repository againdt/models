/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import lombok.Data;

/**
 * @author negi_s
 *
 */
@Data
public class ExternalEnrollmentCsvDTO {
	private String applicantExternalId;
	private String applicantFirstName;	
	private String applicantLastName;
	private String applicantDob;
	private String agentNpn;
	private String healthPlanId;
	private String dentalPlanId;
	private String healthExchangeAssignedPolicyId;
	private String dentalExchangeAssignedPolicyId;
	private String tobaccoIndicator;
	private String applicationExternalId;
	
	public String toCsv(char separator, String stateCode) {
		String csv = "".intern();
		if("MN".equalsIgnoreCase(stateCode) || "NV".equalsIgnoreCase(stateCode)) {
			csv = applicantExternalId + separator
					+ healthExchangeAssignedPolicyId +separator 
					+ healthPlanId +separator
					+ dentalExchangeAssignedPolicyId +separator 
					+ dentalPlanId +separator 
					+ tobaccoIndicator
					;
		}
		return  csv.replaceAll("null", "");
	}
}
