
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Form 1094C Detail Type&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1094CDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1094CDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}USID"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TaxYr"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PayerDLN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CorrectedInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}GovtEntityOrMultiEmployerPlanDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TotalFormsAttachedCnt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ALEInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}FullYearQualifyingOfferInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TransitionReliefInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}QualifyingOfferMethod2015Ind" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AggregatedGroupMemberInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AggregatedGroupMembers" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ALETransitionReliefYearRoundInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ALETransitionReliefMonthlyIndGrp" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AggregatedGroupMembersYearRoundInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}AggregatedGroupMemberMonthlyIndGrp" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MECFullTimeEmployeeYrRoundInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MECFullTimeEmployeeMonthlyIndGrp" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TotalMonthlyEmployeeCountGrp" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TotalYearRoundFullTimeEmplCnt" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}FullTimeEmployeeMonthlyTotalsGrp" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployerNotInBusinessGrp" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}JuratSignatureTxt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}Form1095CDetail" maxOccurs="unbounded"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1094CDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "usid",
    "taxYr",
    "payerDLN",
    "correctedInd",
    "employerDetail",
    "govtEntityOrMultiEmployerPlanDetail",
    "totalFormsAttachedCnt",
    "aleInd",
    "fullYearQualifyingOfferInd",
    "transitionReliefInd",
    "qualifyingOfferMethod2015Ind",
    "aggregatedGroupMemberInd",
    "aggregatedGroupMembers",
    "aleTransitionReliefYearRoundInd",
    "aleTransitionReliefMonthlyIndGrp",
    "aggregatedGroupMembersYearRoundInd",
    "aggregatedGroupMemberMonthlyIndGrp",
    "mecFullTimeEmployeeYrRoundInd",
    "mecFullTimeEmployeeMonthlyIndGrp",
    "totalMonthlyEmployeeCountGrp",
    "totalYearRoundFullTimeEmplCnt",
    "fullTimeEmployeeMonthlyTotalsGrp",
    "employerNotInBusinessGrp",
    "juratSignatureTxt",
    "form1095CDetail",
    "errorDetail"
})
public class Form1094CDetailType {

    @XmlElement(name = "USID", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String usid;
    @XmlElement(name = "TaxYr", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String taxYr;
    @XmlElement(name = "PayerDLN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String payerDLN;
    @XmlElement(name = "CorrectedInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String correctedInd;
    @XmlElement(name = "EmployerDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected EmployerDetailType employerDetail;
    @XmlElement(name = "GovtEntityOrMultiEmployerPlanDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected GovtEntityOrMultiEmployerPlanDetailType govtEntityOrMultiEmployerPlanDetail;
    @XmlElement(name = "TotalFormsAttachedCnt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String totalFormsAttachedCnt;
    @XmlElement(name = "ALEInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected Boolean aleInd;
    @XmlElement(name = "FullYearQualifyingOfferInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected Boolean fullYearQualifyingOfferInd;
    @XmlElement(name = "TransitionReliefInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected Boolean transitionReliefInd;
    @XmlElement(name = "QualifyingOfferMethod2015Ind", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected Boolean qualifyingOfferMethod2015Ind;
    @XmlElement(name = "AggregatedGroupMemberInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    @XmlSchemaType(name = "string")
    protected YOrNIndType aggregatedGroupMemberInd;
    @XmlElement(name = "AggregatedGroupMembers", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<AggregatedGroupMemberDetailType> aggregatedGroupMembers;
    @XmlElement(name = "ALETransitionReliefYearRoundInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String aleTransitionReliefYearRoundInd;
    @XmlElement(name = "ALETransitionReliefMonthlyIndGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected MonthlyIndicatorGrpType aleTransitionReliefMonthlyIndGrp;
    @XmlElement(name = "AggregatedGroupMembersYearRoundInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String aggregatedGroupMembersYearRoundInd;
    @XmlElement(name = "AggregatedGroupMemberMonthlyIndGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected MonthlyIndicatorGrpType aggregatedGroupMemberMonthlyIndGrp;
    @XmlElement(name = "MECFullTimeEmployeeYrRoundInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String mecFullTimeEmployeeYrRoundInd;
    @XmlElement(name = "MECFullTimeEmployeeMonthlyIndGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected MonthlyIndicatorGrpType mecFullTimeEmployeeMonthlyIndGrp;
    @XmlElement(name = "TotalMonthlyEmployeeCountGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected TotalMonthlyCountsGrpType totalMonthlyEmployeeCountGrp;
    @XmlElement(name = "TotalYearRoundFullTimeEmplCnt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String totalYearRoundFullTimeEmplCnt;
    @XmlElement(name = "FullTimeEmployeeMonthlyTotalsGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected TotalMonthlyCountsGrpType fullTimeEmployeeMonthlyTotalsGrp;
    @XmlElement(name = "EmployerNotInBusinessGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected MonthlyIndicatorGrpType employerNotInBusinessGrp;
    @XmlElement(name = "JuratSignatureTxt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String juratSignatureTxt;
    @XmlElement(name = "Form1095CDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected List<Form1095CDetailType> form1095CDetail;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSID() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSID(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the taxYr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxYr() {
        return taxYr;
    }

    /**
     * Sets the value of the taxYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxYr(String value) {
        this.taxYr = value;
    }

    /**
     * Gets the value of the payerDLN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerDLN() {
        return payerDLN;
    }

    /**
     * Sets the value of the payerDLN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerDLN(String value) {
        this.payerDLN = value;
    }

    /**
     * Gets the value of the correctedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedInd() {
        return correctedInd;
    }

    /**
     * Sets the value of the correctedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedInd(String value) {
        this.correctedInd = value;
    }

    /**
     * Gets the value of the employerDetail property.
     * 
     * @return
     *     possible object is
     *     {@link EmployerDetailType }
     *     
     */
    public EmployerDetailType getEmployerDetail() {
        return employerDetail;
    }

    /**
     * Sets the value of the employerDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployerDetailType }
     *     
     */
    public void setEmployerDetail(EmployerDetailType value) {
        this.employerDetail = value;
    }

    /**
     * Gets the value of the govtEntityOrMultiEmployerPlanDetail property.
     * 
     * @return
     *     possible object is
     *     {@link GovtEntityOrMultiEmployerPlanDetailType }
     *     
     */
    public GovtEntityOrMultiEmployerPlanDetailType getGovtEntityOrMultiEmployerPlanDetail() {
        return govtEntityOrMultiEmployerPlanDetail;
    }

    /**
     * Sets the value of the govtEntityOrMultiEmployerPlanDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link GovtEntityOrMultiEmployerPlanDetailType }
     *     
     */
    public void setGovtEntityOrMultiEmployerPlanDetail(GovtEntityOrMultiEmployerPlanDetailType value) {
        this.govtEntityOrMultiEmployerPlanDetail = value;
    }

    /**
     * Gets the value of the totalFormsAttachedCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalFormsAttachedCnt() {
        return totalFormsAttachedCnt;
    }

    /**
     * Sets the value of the totalFormsAttachedCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalFormsAttachedCnt(String value) {
        this.totalFormsAttachedCnt = value;
    }

    /**
     * Gets the value of the aleInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isALEInd() {
        return aleInd;
    }

    /**
     * Sets the value of the aleInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setALEInd(Boolean value) {
        this.aleInd = value;
    }

    /**
     * Gets the value of the fullYearQualifyingOfferInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFullYearQualifyingOfferInd() {
        return fullYearQualifyingOfferInd;
    }

    /**
     * Sets the value of the fullYearQualifyingOfferInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFullYearQualifyingOfferInd(Boolean value) {
        this.fullYearQualifyingOfferInd = value;
    }

    /**
     * Gets the value of the transitionReliefInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransitionReliefInd() {
        return transitionReliefInd;
    }

    /**
     * Sets the value of the transitionReliefInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransitionReliefInd(Boolean value) {
        this.transitionReliefInd = value;
    }

    /**
     * Gets the value of the qualifyingOfferMethod2015Ind property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isQualifyingOfferMethod2015Ind() {
        return qualifyingOfferMethod2015Ind;
    }

    /**
     * Sets the value of the qualifyingOfferMethod2015Ind property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setQualifyingOfferMethod2015Ind(Boolean value) {
        this.qualifyingOfferMethod2015Ind = value;
    }

    /**
     * Gets the value of the aggregatedGroupMemberInd property.
     * 
     * @return
     *     possible object is
     *     {@link YOrNIndType }
     *     
     */
    public YOrNIndType getAggregatedGroupMemberInd() {
        return aggregatedGroupMemberInd;
    }

    /**
     * Sets the value of the aggregatedGroupMemberInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link YOrNIndType }
     *     
     */
    public void setAggregatedGroupMemberInd(YOrNIndType value) {
        this.aggregatedGroupMemberInd = value;
    }

    /**
     * Gets the value of the aggregatedGroupMembers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aggregatedGroupMembers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAggregatedGroupMembers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AggregatedGroupMemberDetailType }
     * 
     * 
     */
    public List<AggregatedGroupMemberDetailType> getAggregatedGroupMembers() {
        if (aggregatedGroupMembers == null) {
            aggregatedGroupMembers = new ArrayList<AggregatedGroupMemberDetailType>();
        }
        return this.aggregatedGroupMembers;
    }

    /**
     * Gets the value of the aleTransitionReliefYearRoundInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALETransitionReliefYearRoundInd() {
        return aleTransitionReliefYearRoundInd;
    }

    /**
     * Sets the value of the aleTransitionReliefYearRoundInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALETransitionReliefYearRoundInd(String value) {
        this.aleTransitionReliefYearRoundInd = value;
    }

    /**
     * Gets the value of the aleTransitionReliefMonthlyIndGrp property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public MonthlyIndicatorGrpType getALETransitionReliefMonthlyIndGrp() {
        return aleTransitionReliefMonthlyIndGrp;
    }

    /**
     * Sets the value of the aleTransitionReliefMonthlyIndGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public void setALETransitionReliefMonthlyIndGrp(MonthlyIndicatorGrpType value) {
        this.aleTransitionReliefMonthlyIndGrp = value;
    }

    /**
     * Gets the value of the aggregatedGroupMembersYearRoundInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAggregatedGroupMembersYearRoundInd() {
        return aggregatedGroupMembersYearRoundInd;
    }

    /**
     * Sets the value of the aggregatedGroupMembersYearRoundInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAggregatedGroupMembersYearRoundInd(String value) {
        this.aggregatedGroupMembersYearRoundInd = value;
    }

    /**
     * Gets the value of the aggregatedGroupMemberMonthlyIndGrp property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public MonthlyIndicatorGrpType getAggregatedGroupMemberMonthlyIndGrp() {
        return aggregatedGroupMemberMonthlyIndGrp;
    }

    /**
     * Sets the value of the aggregatedGroupMemberMonthlyIndGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public void setAggregatedGroupMemberMonthlyIndGrp(MonthlyIndicatorGrpType value) {
        this.aggregatedGroupMemberMonthlyIndGrp = value;
    }

    /**
     * Gets the value of the mecFullTimeEmployeeYrRoundInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMECFullTimeEmployeeYrRoundInd() {
        return mecFullTimeEmployeeYrRoundInd;
    }

    /**
     * Sets the value of the mecFullTimeEmployeeYrRoundInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMECFullTimeEmployeeYrRoundInd(String value) {
        this.mecFullTimeEmployeeYrRoundInd = value;
    }

    /**
     * Gets the value of the mecFullTimeEmployeeMonthlyIndGrp property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public MonthlyIndicatorGrpType getMECFullTimeEmployeeMonthlyIndGrp() {
        return mecFullTimeEmployeeMonthlyIndGrp;
    }

    /**
     * Sets the value of the mecFullTimeEmployeeMonthlyIndGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public void setMECFullTimeEmployeeMonthlyIndGrp(MonthlyIndicatorGrpType value) {
        this.mecFullTimeEmployeeMonthlyIndGrp = value;
    }

    /**
     * Gets the value of the totalMonthlyEmployeeCountGrp property.
     * 
     * @return
     *     possible object is
     *     {@link TotalMonthlyCountsGrpType }
     *     
     */
    public TotalMonthlyCountsGrpType getTotalMonthlyEmployeeCountGrp() {
        return totalMonthlyEmployeeCountGrp;
    }

    /**
     * Sets the value of the totalMonthlyEmployeeCountGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalMonthlyCountsGrpType }
     *     
     */
    public void setTotalMonthlyEmployeeCountGrp(TotalMonthlyCountsGrpType value) {
        this.totalMonthlyEmployeeCountGrp = value;
    }

    /**
     * Gets the value of the totalYearRoundFullTimeEmplCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalYearRoundFullTimeEmplCnt() {
        return totalYearRoundFullTimeEmplCnt;
    }

    /**
     * Sets the value of the totalYearRoundFullTimeEmplCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalYearRoundFullTimeEmplCnt(String value) {
        this.totalYearRoundFullTimeEmplCnt = value;
    }

    /**
     * Gets the value of the fullTimeEmployeeMonthlyTotalsGrp property.
     * 
     * @return
     *     possible object is
     *     {@link TotalMonthlyCountsGrpType }
     *     
     */
    public TotalMonthlyCountsGrpType getFullTimeEmployeeMonthlyTotalsGrp() {
        return fullTimeEmployeeMonthlyTotalsGrp;
    }

    /**
     * Sets the value of the fullTimeEmployeeMonthlyTotalsGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalMonthlyCountsGrpType }
     *     
     */
    public void setFullTimeEmployeeMonthlyTotalsGrp(TotalMonthlyCountsGrpType value) {
        this.fullTimeEmployeeMonthlyTotalsGrp = value;
    }

    /**
     * Gets the value of the employerNotInBusinessGrp property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public MonthlyIndicatorGrpType getEmployerNotInBusinessGrp() {
        return employerNotInBusinessGrp;
    }

    /**
     * Sets the value of the employerNotInBusinessGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlyIndicatorGrpType }
     *     
     */
    public void setEmployerNotInBusinessGrp(MonthlyIndicatorGrpType value) {
        this.employerNotInBusinessGrp = value;
    }

    /**
     * Gets the value of the juratSignatureTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJuratSignatureTxt() {
        return juratSignatureTxt;
    }

    /**
     * Sets the value of the juratSignatureTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJuratSignatureTxt(String value) {
        this.juratSignatureTxt = value;
    }

    /**
     * Gets the value of the form1095CDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the form1095CDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForm1095CDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Form1095CDetailType }
     * 
     * 
     */
    public List<Form1095CDetailType> getForm1095CDetail() {
        if (form1095CDetail == null) {
            form1095CDetail = new ArrayList<Form1095CDetailType>();
        }
        return this.form1095CDetail;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
