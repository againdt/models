package com.getinsured.hix.ui.model.eligibility.prescreen;

import java.util.List;
import java.util.Map;

/**
 * This class encapsulates the objects that are sent as a request from the
 * prescreen UI to the backend.
 * 
 * @author Sunil Desu
 * @since April 16 2013
 * 
 */
public class PrescreenRequest {

	private PrescreenProfile prescreenProfile;
	private PrescreenHousehold prescreenHousehold;
	private boolean isAnyMemberDisabled;
	private List<PrescreenIncome> prescreenIncomes;
	private PrescreenDeductions prescreenDeductions;
	private String planId;
	private long aptcValue;
	private long prescreenRecordId;
	private double fplValue;
	private double applicablePercentage;
	private String csr;
	private double benchmarkPlanPremium;
	private String currentTab;
	private String resultsType;
	private boolean isBenchmarkCallNeeded;
	private String errorDescription;
	private String errorMessage;
	private Map<String,String> planData;
	
	public PrescreenProfile getPrescreenProfile() {
		return prescreenProfile;
	}

	public void setPrescreenProfile(PrescreenProfile prescreenProfile) {
		this.prescreenProfile = prescreenProfile;
	}

	public PrescreenHousehold getPrescreenHousehold() {
		return prescreenHousehold;
	}

	public void setPrescreenHousehold(PrescreenHousehold prescreenHousehold) {
		this.prescreenHousehold = prescreenHousehold;
	}
	
	public List<PrescreenIncome> getPrescreenIncomes() {
		return prescreenIncomes;
	}

	public void setPrescreenIncomes(List<PrescreenIncome> prescreenIncomes) {
		this.prescreenIncomes = prescreenIncomes;
	}

	public PrescreenDeductions getPrescreenDeductions() {
		return prescreenDeductions;
	}

	public void setPrescreenDeductions(PrescreenDeductions prescreenDeductions) {
		this.prescreenDeductions = prescreenDeductions;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public long getAptcValue() {
		return aptcValue;
	}

	public void setAptcValue(long aptcValue) {
		this.aptcValue = aptcValue;
	}

	public long getPrescreenRecordId() {
		return prescreenRecordId;
	}

	public void setPrescreenRecordId(long prescreenRecordId) {
		this.prescreenRecordId = prescreenRecordId;
	}

	public double getFplValue() {
		return fplValue;
	}

	public void setFplValue(double fplValue) {
		this.fplValue = fplValue;
	}

	public double getApplicablePercentage() {
		return applicablePercentage;
	}

	public void setApplicablePercentage(double applicablePercentage) {
		this.applicablePercentage = applicablePercentage;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public double getBenchmarkPlanPremium() {
		return benchmarkPlanPremium;
	}

	public void setBenchmarkPlanPremium(double benchmarkPlanPremium) {
		this.benchmarkPlanPremium = benchmarkPlanPremium;
	}

	public String getCurrentTab() {
		return currentTab;
	}

	public void setCurrentTab(String currentTab) {
		this.currentTab = currentTab;
	}

	public String getResultsType() {
		return resultsType;
	}

	public void setResultsType(String resultsType) {
		this.resultsType = resultsType;
	}

	public boolean getIsAnyMemberDisabled() {
		return isAnyMemberDisabled;
	}

	public void setIsAnyMemberDisabled(boolean isAnyMemberDisabled) {
		this.isAnyMemberDisabled = isAnyMemberDisabled;
	}
	
	public boolean getIsBenchmarkCallNeeded() {
		return isBenchmarkCallNeeded;
	}

	public void setIsBenchmarkCallNeeded(boolean isBenchmarkCallNeeded) {
		this.isBenchmarkCallNeeded = isBenchmarkCallNeeded;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Map<String, String> getPlanData() {
		return planData;
	}

	public void setPlanData(Map<String, String> planData) {
		this.planData = planData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("PrescreenRequest [prescreenProfile=");
		if(prescreenProfile != null){
			builder.append(prescreenProfile.toString());
		}
		
		builder.append(", prescreenHousehold=");
		if(prescreenHousehold != null){
			builder.append(prescreenHousehold.toString());
		}
		
		builder.append(", isAnyMemberDisabled=");
		builder.append(isAnyMemberDisabled);
		
		builder.append(", prescreenIncomes=");
		if(prescreenIncomes != null){
			builder.append(prescreenIncomes.toString());
		}
		
		builder.append(", prescreenDeductions=");
		if(prescreenDeductions != null){
			builder.append(prescreenDeductions.toString());
		}
		
		builder.append(", planId=");
		builder.append(planId);
		builder.append(", aptcValue=");
		builder.append(aptcValue);
		builder.append(", prescreenRecordId=");
		builder.append(prescreenRecordId);
		builder.append(", fplValue=");
		builder.append(fplValue);
		builder.append(", applicablePercentage=");
		builder.append(applicablePercentage);
		builder.append(", csr=");
		builder.append(csr);
		builder.append(", benchmarkPlanPremium=");
		builder.append(benchmarkPlanPremium);
		builder.append(", currentTab=");
		builder.append(currentTab);
		builder.append(", resultsType=");
		builder.append(resultsType);
		builder.append(", isBenchmarkCallNeeded=");
		builder.append(isBenchmarkCallNeeded);
		builder.append(", errorDescription=");
		builder.append(errorDescription);
		builder.append(", errorMessage=");
		builder.append(errorMessage);
		builder.append(", planData=");
		builder.append(planData);
		builder.append("]");
		return builder.toString();
	}
}
