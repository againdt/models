package com.getinsured.hix.dto.directenrollment.payment;

public class PaymentConstants {

	/**
	 * Possible payment modes available right now (20 Oct 2014)
	 * 
	 * Added NONE as an error case (richard@iGroup 30 Oct 2014)
	 *
	 */
	public enum PaymentMode {
		BANK, CREDITCARD, DIRECTPAY, NONE
	}

	/**
	 * Possible list of payment errors
	 * 
	 * @author root
	 *
	 */
	public enum PaymentError {
		BANK_ERROR("Error validating bank information"),
		BANK_INSUFFICIENT_FUNDS("Not enough funds available"),
		BANK_BADDATA("Format error"),
		CREDIT_CARD_ERROR("Error validating credit card information"),
		CREDIT_CARD_INVALID_CARD_NUMBER("Invalid credit card number."),
		CREDIT_CARD_CVV_DECLINE("CVV error"),
		CREDIT_CARD_INVALID_ADDRESS("Address not linked to credit card"),
		CREDIT_CARD_BADDATA("Format error"),
		CREDIT_CARD_NO_INTERNATIONAL("Credit card not issued in United States"),
		CREDIT_CARD_INSUFFICIENT_FUNDS("Not enough funds available"),
		CREDIT_CARD_BAD_ZIP("Invalid Zip"),
		CREDIT_CARD_EXPIRED("Expired card"),
		ERROR("Error validating payment");

		String details;

		private PaymentError(String details) {
			this.details = details;
		}

		public String getDetails() {
			return this.details;
		}

		public static PaymentError getError(PaymentValidationProvider provider, PaymentMode mode, String reason) {
			switch (provider) {
			case CYBERSOURCE:
				switch (mode) {
				case BANK:
					switch (reason) {
					case "220": // Processor declined the request
					case "222": // Bank account is frozen
					case "223": // Declined
						return BANK_INSUFFICIENT_FUNDS;
					case "101": // Missing one or more required fields
					case "102": // One or more fields contains invalid data
					case "388": // Routing number failed verification
						return BANK_BADDATA;
					default:
						return BANK_ERROR;
					}
				case CREDITCARD:
					switch (reason) {
					case "209": // CVN did not match
					case "211": // Invalid CVN
					case "230": // Approved by bank, but failed CyberSource CNV
								// check
						return CREDIT_CARD_CVV_DECLINE;
					case "200": // Approved by bank, but failed CyberSource AVS
						return CREDIT_CARD_INVALID_ADDRESS;
					case "101": // Missing one or more required fields
					case "102": return CREDIT_CARD_BADDATA;// One or more fields contains invalid data
					case "202": return CREDIT_CARD_EXPIRED;// Expired card, or expiry date does not match
								// date on file
					case "231": // Invalid account number
						return CREDIT_CARD_BADDATA;
					default:
						return CREDIT_CARD_ERROR;
					}
				default:
					return ERROR;
				}
			case ALACRITI:
				break;
			case TRUSTCOMMERCE:
				break;
			}

			return ERROR;
		}
	}

	/**
	 * Note to self: Is it a safe practice to have it readable by REST API from
	 * UI to select validation provider?
	 * 
	 * It could be done via using a internal code (less readable)
	 */
	public enum PaymentValidationProvider {
		TRUSTCOMMERCE("tc"), CYBERSOURCE("cc"), ALACRITI("al");
		String key;

		private PaymentValidationProvider(String key) {
			this.key = key;
		}

		public static PaymentValidationProvider getProvider(String key) {
			switch (key) {
			case "tc":
				return TRUSTCOMMERCE;
			case "al":
				return ALACRITI;
			case "cc":
				return CYBERSOURCE;
			default:
				return CYBERSOURCE;

			}
		}

		/**
		 * These value comes from
		 * https://docs.google.com/a/getinsured.com/spreadsheet
		 * /ccc?key=0AgOZ5N9-
		 * 0FI4dHVTQVRZUUZ6M0FNMi0yWUZWb0Y1SlE&usp=drive_web#gid=8
		 * 
		 * @param provider
		 * @param error
		 * @return
		 */
		public static String getProviderError(PaymentValidationProvider provider, PaymentError error) {
			if (provider.equals(CYBERSOURCE)) {
				switch (error) {
				case BANK_ERROR:
					return "There was a problem adding this form of payment. Please use a different form of payment or try again later.";
				case BANK_INSUFFICIENT_FUNDS:
					return "There was a problem adding this form of payment. Please use a different form of payment or try again later.";
				case CREDIT_CARD_INVALID_ADDRESS:
					return "The billing address entered does not match the address of this card.";
				case CREDIT_CARD_CVV_DECLINE:
					return "The security code entered does not match the security code of this card.";
				case CREDIT_CARD_BADDATA:
					return "Either the card number or payment type is incorrect.";
				case CREDIT_CARD_BAD_ZIP:
					return "Invalid Zip code";
				case BANK_BADDATA:
					return "Either the routing number or account number is incorrect.";
				default:
					return "Error validating payment";
				}
			} else {
				switch (error) {
				case BANK_ERROR:
					return "Either the card number or payment type is incorrect.  Please correct and retry.  If you continue to see this error, please use a different form of payment or try again later.";
				case BANK_INSUFFICIENT_FUNDS:
					return "There was a problem processing this transaction. If you continue to see this error, please use a different form of payment or try again later.";
				case CREDIT_CARD_NO_INTERNATIONAL:
					return "There was a problem processing this transaction, most likely because the card is not issued in the United States.  If this is the case, please use a card issued in the U.S.  If you continue to see this error, please use a different debit or credit card or try again later.";
				case CREDIT_CARD_ERROR:
					return "There was a problem processing this transaction. If you continue to see this error, please use a different form of payment or try again later.";
				case CREDIT_CARD_INVALID_ADDRESS:
					return "The billing address entered does not match the address of this card.  Enter the address associated with this card into the billing address fields above and try again. If you continue to see this error, please use a different debit or credit card or try again later.";
				case CREDIT_CARD_CVV_DECLINE:
					return "The security code entered does not match the security code of this card.  Enter the security code associated with this card into the CVV field and try again. If you continue to see this error, please use a different debit or credit card or try again later.";
				case CREDIT_CARD_BADDATA:
					return "Either the card number or payment type is incorrect.  Please correct and retry.  If you continue to see this error, please use a different form of payment or try again later.";
				case CREDIT_CARD_INSUFFICIENT_FUNDS:
					return "There was a problem processing this transaction. If you continue to see this error, please use a different form of payment or try again later.";
				case BANK_BADDATA:
					return "Either the card number or payment type is incorrect.  Please correct and retry.  If you continue to see this error, please use a different form of payment or try again later.";
				default:
					return "Error validating payment";
				}
			}
		}
	}
}
