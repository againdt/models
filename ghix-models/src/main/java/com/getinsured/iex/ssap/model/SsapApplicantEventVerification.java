package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.timeshift.TimeShifterUtil;

@Entity
@Table(name="SSAP_APPLICANT_EVENT_VRF")
public class SsapApplicantEventVerification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_APPLICANT_EVENT_VRF_ID_GENERATOR", sequenceName="SSAP_APPLICANT_EVENT_VRF_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICANT_EVENT_VRF_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="ISSUER_CODE")
	private String issuerCode;
	
	@Column(name="GROUP_NUMBER")
	private String groupNumber;
	
	@Column(name="POLICY_NUMBER")
	private String policyNumber;
	
	@OneToOne
	@JoinColumn(name="SSAP_APPLICANT_EVENT_ID", nullable=false)
	private SsapApplicantEvent ssapApplicantEvent;
		
	@Column(name="CREATED_BY")
	private Long createdBy;

	@Column(name="CREATION_DATE")
	private Timestamp creationDate;
	
	@Column(name="LAST_UPDATED_BY")
	private Long lastUpdatedBy;
	
	@Column(name="LAST_UPDATE_DATE")
	private Timestamp lastUpdateDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIssuerCode() {
		return issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	public String getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public SsapApplicantEvent getSsapApplicantEvent() {
		return ssapApplicantEvent;
	}

	public void setSsapApplicantEvent(SsapApplicantEvent ssapApplicantEvent) {
		this.ssapApplicantEvent = ssapApplicantEvent;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationDate(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateDate(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}
}
