/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author hardas_d
 * 
 */
public class TkmQueueUsersRequest extends GHIXRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4416981154655711530L;
	private Integer queueId;
	private List<Integer> userList;
	private Integer userId;
	private List<Integer>queueIdList;

	/**
	 * @return the queueId
	 */
	public Integer getQueueId() {
		return queueId;
	}

	/**
	 * @param queueId
	 *            the queueId to set
	 */
	public void setQueueId(Integer queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the userList
	 */
	public List<Integer> getUserList() {
		return userList;
	}

	/**
	 * @param userList
	 *            the userList to set
	 */
	public void setUserList(List<Integer> userList) {
		this.userList = userList;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<Integer> getQueueIdList() {
		return queueIdList;
	}

	public void setQueueIdList(List<Integer> queueIdList) {
		this.queueIdList = queueIdList;
	}
}
