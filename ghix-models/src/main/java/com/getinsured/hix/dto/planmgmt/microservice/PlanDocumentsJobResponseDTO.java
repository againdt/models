package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class PlanDocumentsJobResponseDTO extends GhixResponseDTO {

	private Integer totalRecordCount;
	private Integer pageSize;
	private Integer currentRecordCount;
	private List<PlanDocumentsJobDTO> planDocumentsJobList;

	public PlanDocumentsJobResponseDTO() {
		super();
	}

	public List<PlanDocumentsJobDTO> getPlanDocumentsJobList() {
		return planDocumentsJobList;
	}

	public void setPlanDocumentsJob(PlanDocumentsJobDTO planDocumentsJobDTO) {

		if (CollectionUtils.isEmpty(this.planDocumentsJobList)) {
			this.planDocumentsJobList = new ArrayList<PlanDocumentsJobDTO>();
		}
		this.planDocumentsJobList.add(planDocumentsJobDTO);
	}

	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCurrentRecordCount() {
		return currentRecordCount;
	}

	public void setCurrentRecordCount(Integer currentRecordCount) {
		this.currentRecordCount = currentRecordCount;
	}
}
