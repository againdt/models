/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

/**
 * HIX-70722 EnrollmentTenantDTO for API
 * @author negi_s
 * @since 06/07/2015
 *
 */
public class EnrollmentTenantDTO implements Serializable{
	
	public static enum EnrollmentStatus {
		APPROVED, INFORCE, WITHDRAWN, DUPLICATE, INVALID, VERIFIED, ESIG_PENDING, DOES_NOT_EXIST, SOLD, SUSPENDED, PAYMENT_PROCESSED, PAYMENT_FAILED, DECLINED, NOT_APPOINTED, CANCEL, TERM, CONFIRM, ORDER_CONFIRMED, PAYMENT_RECEIVED, PENDING, ABORTED, ECOMMITTED
	}

	public static enum EnrollmentType {
		I, S, A
	}
	
	private Integer enrollmentId;
	private String exchangePlanId;
	private String planTier;
	private String marketType;
	private String coverageStartDate;
	private String productCategory;
	private Float grossPremiumAmount;
	private Float aptcAmount;
	private Float netPremiumAmount;
	private String enrollmentSubmissionDate;
	private EnrollmentStatus enrollmentStatus; // Enumeration ??
	private EnrollmentType enrollmentType;
	private String insuranceType;
	private List<EnrolleeTenantDTO> enrollees;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getExchangePlanId() {
		return exchangePlanId;
	}
	public void setExchangePlanId(String exchangePlanId) {
		this.exchangePlanId = exchangePlanId;
	}
	public String getPlanTier() {
		return planTier;
	}
	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}
	public String getMarketType() {
		return marketType;
	}
	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public Float getGrossPremiumAmount() {
		return grossPremiumAmount;
	}
	public void setGrossPremiumAmount(Float grossPremiumAmount) {
		this.grossPremiumAmount = grossPremiumAmount;
	}
	public Float getAptcAmount() {
		return aptcAmount;
	}
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	public Float getNetPremiumAmount() {
		return netPremiumAmount;
	}
	public void setNetPremiumAmount(Float netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}
	public EnrollmentStatus getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(EnrollmentStatus enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public EnrollmentType getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(EnrollmentType enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getEnrollmentSubmissionDate() {
		return enrollmentSubmissionDate;
	}
	public void setEnrollmentSubmissionDate(String enrollmentSubmissionDate) {
		this.enrollmentSubmissionDate = enrollmentSubmissionDate;
	}
	public List<EnrolleeTenantDTO> getEnrollees() {
		return enrollees;
	}
	public void setEnrollees(List<EnrolleeTenantDTO> enrollees) {
		this.enrollees = enrollees;
	}
	
	@Override
	public String toString() {
		return "EnrollmentTenantDTO [enrollmentId=" + enrollmentId
				+ ", exchangePlanId=" + exchangePlanId + ", planTier="
				+ planTier + ", marketType=" + marketType
				+ ", coverageStartDate=" + coverageStartDate
				+ ", productCategory=" + productCategory
				+ ", grossPremiumAmount=" + grossPremiumAmount
				+ ", aptcAmount=" + aptcAmount + ", netPremiumAmount="
				+ netPremiumAmount + ", enrollmentSubmissionDate="
				+ enrollmentSubmissionDate + ", enrollmentStatus="
				+ enrollmentStatus + ", enrollmentType=" + enrollmentType
				+ ", insuranceType=" + insuranceType + ", enrollees="
				+ enrollees + "]";
	}
	

}
