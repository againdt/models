package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.broker.IndividualDTO;
import com.getinsured.hix.dto.broker.IndividualHouseholdDTO;
/**
 * The class encapsulating IndividualDetail Response from WebService.
 * 
 */
public class IndividualBOBresponse extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private int responseCode;
	private String responseDescription;
	private String errMsg;
	private String staus;
	private IndividualDTO individualDTO;
	private IndividualHouseholdDTO IndividualHouseholdDTO;
	private List<IndividualHouseholdDTO> IndividualHouseholdDTOlist;

	public IndividualHouseholdDTO getIndividualHouseholdDTO() {
		return IndividualHouseholdDTO;
	}

	public void setIndividualHouseholdDTO(
			IndividualHouseholdDTO individualHouseholdDTO) {
		IndividualHouseholdDTO = individualHouseholdDTO;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public IndividualDTO getIndividualDTO() {
		return individualDTO;
	}

	public void setIndividualDTO(IndividualDTO individualDTO) {
		this.individualDTO = individualDTO;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getStaus() {
		return staus;
	}

	public void setStaus(String staus) {
		this.staus = staus;
	}

	public List<IndividualHouseholdDTO> getIndividualHouseholdDTOlist() {
		return IndividualHouseholdDTOlist;
	}

	public void setIndividualHouseholdDTOlist(
			List<IndividualHouseholdDTO> individualHouseholdDTOlist) {
			IndividualHouseholdDTOlist = individualHouseholdDTOlist;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("IndividualBOBresponse details : responseCode=").append(responseCode).append(", responseDescription= ").append(responseDescription).append(", errMsg=").append(errMsg).append(", individualDTO=").append(individualDTO).toString();
	}
}
