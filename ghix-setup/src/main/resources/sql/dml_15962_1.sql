delete from role_permissions where permission_id = (select id from permissions  where name=UPPER('manage_entity')) AND role_id = (select id from roles where name='ASSISTERENROLLMENTENTITYADMIN');
delete from role_permissions where permission_id = (select id from permissions   where name=UPPER('manage_assister')) AND role_id = (select id from roles where name='ASSISTERENROLLMENTENTITYADMIN');
delete from permissions where name=UPPER('manage_entity');
delete from permissions where name=UPPER('manage_assister');

INSERT INTO PERMISSIONS (ID,NAME) VALUES (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_entity'));
INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_assister'));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('manage_entity')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('manage_assister')));
