package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.Date;

/**
 * DTO class is used to get/set data using SERFF_PLAN_DOCUMENTS_JOB table.
 */
public class PlanDocumentsJobDTO {

	private int id;
	private String documentType;
	private String documentNewName;
	private String documentOldName;
	private String errorMessage;
	private String hiosProductId;
	private String issuerPlanNumber;
	private Date lastUpdated;
	private String planId;
	private String ucmNewId;
	private String ucmOldId;
	private String ftpStatus;

	public PlanDocumentsJobDTO() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNewName() {
		return documentNewName;
	}

	public void setDocumentNewName(String documentNewName) {
		this.documentNewName = documentNewName;
	}

	public String getDocumentOldName() {
		return documentOldName;
	}

	public void setDocumentOldName(String documentOldName) {
		this.documentOldName = documentOldName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getHiosProductId() {
		return hiosProductId;
	}

	public void setHiosProductId(String hiosProductId) {
		this.hiosProductId = hiosProductId;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getUcmNewId() {
		return ucmNewId;
	}

	public void setUcmNewId(String ucmNewId) {
		this.ucmNewId = ucmNewId;
	}

	public String getUcmOldId() {
		return ucmOldId;
	}

	public void setUcmOldId(String ucmOldId) {
		this.ucmOldId = ucmOldId;
	}

	public String getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}
}
