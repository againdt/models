package com.getinsured.hix.dto.tkm;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author reddy_h 
 * Dto class. It holds the ticket/task specified columns
 *         information
 */
public class HistoryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date sortDate;
	private String status;
	private String comments;
	private String description;
	private String disDate;
	private String name;
	private String assignee;
	
	
	public Date getSortDate() {
		return sortDate;
	}
	public void setSortDate(Date sortDate) {
		this.sortDate = sortDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDisDate() {
		return disDate;
	}
	public void setDisDate(String disDate) {
		this.disDate = disDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	
	@Override
	public String toString() {
		return "HistoryDto [sortDate=" + sortDate + ", status=" + status
				+ ", comments=" + comments + ", description=" + description
				+ ", disDate=" + disDate + ", name=" + name + ", assignee="
				+ assignee + "]";
	}
	
	

}
