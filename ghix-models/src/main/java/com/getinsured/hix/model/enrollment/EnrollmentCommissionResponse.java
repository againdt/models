/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author panda_p
 *
 */

public class EnrollmentCommissionResponse extends GHIXResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<EnrollmentCommission> enrollmentCommissions;
	private EnrollmentCommission enrollmentCommission;
	public List<EnrollmentCommission> getEnrollmentCommissions() {
		return enrollmentCommissions;
	}
	public void setEnrollmentCommissions(
			List<EnrollmentCommission> enrollmentCommissions) {
		this.enrollmentCommissions = enrollmentCommissions;
	}
	public EnrollmentCommission getEnrollmentCommission() {
		return enrollmentCommission;
	}
	public void setEnrollmentCommission(EnrollmentCommission enrollmentCommission) {
		this.enrollmentCommission = enrollmentCommission;
	}
}
