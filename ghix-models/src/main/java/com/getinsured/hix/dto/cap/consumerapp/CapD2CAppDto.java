package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.Date;

public class CapD2CAppDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private long ssapAppId;
	private String effectiveStartDate;
	private String applicationStatus ;


	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public long getSsapAppId() {
		return ssapAppId;
	}
	
	public void setSsapAppId(long ssapAppId) {
		this.ssapAppId = ssapAppId;
	}
	
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
}
