--liquibase formatted sql

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='BROKER_ADMIN'), (select id from permissions   where name=UPPER('employer_registration_wizard')));