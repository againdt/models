package com.getinsured.hix.model.consumer;


import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;


/**
 * This is the model class for CMR_STAGE_TRACKING table
 * @author Suhasini Goli
 * @since  28th March, 2013
 */

@Entity
@Table(name="CMR_STAGE_TRACKING")
@DynamicInsert
@DynamicUpdate
public class StageTracking implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public enum StageTrackingStatus { NEW, IN_PROGRESS, COMPLETE;}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CMR_STAGE_TRACKING_SEQ")
	@SequenceGenerator(name = "CMR_STAGE_TRACKING_SEQ", sequenceName = "CMR_STAGE_TRACKING_SEQ", allocationSize = 1)
	private int id;
		
	@ManyToOne
	@JoinColumn(name="CMR_HOUSEHOLD_ID")
	private Household household; 
	
	
	@Column(name = "STAGE")
	@Enumerated(EnumType.STRING)	
	private STAGE stage;
	
		
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private StageTrackingStatus status;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="TICKET_ID")
	private TkmTickets ticket; 

	@Column(name="SSAP_APPLICATION_ID")
	private Long ssapApplicationId;
			
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false, updatable=false)
	private Date creationTimestamp;
	
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updateTimestamp;

	@Column(name="created_by")
	private Integer createdBy;
    
	@Column(name="last_updated_by")
    private Integer updatedBy;
	
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Household getHousehold() {
		return household;
	}

	public void setHousehold(Household household) {
		this.household = household;
	}
	
			

	public STAGE getStage() {
		return stage;
	}

	public void setStage(STAGE stage) {
		this.stage = stage;
	}

	public StageTrackingStatus getStatus() {
		return status;
	}

	public void setStatus(StageTrackingStatus status) {
		this.status = status;
	}
	
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}	

	public TkmTickets getTicket() {
		return ticket;
	}

	public void setTicket(TkmTickets ticket) {
		this.ticket = ticket;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Date updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setUpdateTimestamp(new TSDate());
		
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdateTimestamp(new TSDate());
	}
		
}
	
