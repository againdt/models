package com.getinsured.eligibility.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "RRV_SUBMISSION")
@DynamicInsert
@DynamicUpdate
public class RrvSubmission {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RRV_SUBMISSION_SEQ")
    @SequenceGenerator(name = "RRV_SUBMISSION_SEQ", sequenceName = "RRV_SUBMISSION_SEQ", allocationSize = 1)
    private Long id;

    @NotAudited
    @Column(name = "COVERAGE_YEAR")
    private long coverageYear;

    @NotAudited
    @Column(name = "RRV_BATCH_ID")
    private long rrvBatchId;

    @NotAudited
    @Column(name = "HOUSEHOLD_ID")
    private long householdId;

    @NotAudited
    @Column(name = "IFSV_STATUS")
    private String ifsvStatus;

    @NotAudited
    @Column(name = "SSAC_STATUS")
    private String ssacStatus;

    @NotAudited
    @Column(name = "EQFX_STATUS")
    private String eqfxStatus;

    @NotAudited
    @Column(name = "SMEC_STATUS")
    private String smecStatus;

    @NotAudited
    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @NotAudited
    @Column(name = "LAST_UPDATE_DATE")
    private Date lastUpdateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCoverageYear() {
        return coverageYear;
    }

    public void setCoverageYear(long coverageYear) {
        this.coverageYear = coverageYear;
    }

    public long getRrvBatchId() {
        return rrvBatchId;
    }

    public void setRrvBatchId(long rrvBatchId) {
        this.rrvBatchId = rrvBatchId;
    }

    public long getHouseholdId() {
        return householdId;
    }

    public void setHouseholdId(long householdId) {
        this.householdId = householdId;
    }

    public String getIfsvStatus() {
        return ifsvStatus;
    }

    public void setIfsvStatus(String ifsvStatus) {
        this.ifsvStatus = ifsvStatus;
    }

    public String getSsacStatus() {
        return ssacStatus;
    }

    public void setSsacStatus(String ssacStatus) {
        this.ssacStatus = ssacStatus;
    }

    public String getEqfxStatus() {
        return eqfxStatus;
    }

    public void setEqfxStatus(String eqfxStatus) {
        this.eqfxStatus = eqfxStatus;
    }

    public String getSmecStatus() {
        return smecStatus;
    }

    public void setSmecStatus(String smecStatus) {
        this.smecStatus = smecStatus;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
