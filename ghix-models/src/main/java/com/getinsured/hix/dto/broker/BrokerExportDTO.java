package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.Date;

/**
 * Broker data transfer class.
 * 
 * @author kanthi_v
 * 
 */
public class BrokerExportDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String firstName;
	private String lastName;
	private String companyName;
	private String federalEIN;
	private String certificationStatus;

	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;

	private String contactNumber;
	private String email;

	private String licenseNumber;
	private String licenseRenewalDate;

	private String prefMethodOfCommunication;
	private int pendingRequestsCount;
	private int activeRequestsCount;
	private int inactiveRequestsCount;
	
	
	public BrokerExportDTO(){
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(String federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	public String getLicenseRenewalDate() {
		return licenseRenewalDate;
	}

	public void setLicenseRenewalDate(String licenseRenewalDate) {
		this.licenseRenewalDate = licenseRenewalDate;
	}

	public String getPrefMethodOfCommunication() {
		return prefMethodOfCommunication;
	}

	public void setPrefMethodOfCommunication(String prefMethodOfCommunication) {
		this.prefMethodOfCommunication = prefMethodOfCommunication;
	}

	public int getPendingRequestsCount() {
		return pendingRequestsCount;
	}

	public void setPendingRequestsCount(int pendingRequestsCount) {
		this.pendingRequestsCount = pendingRequestsCount;
	}

	public int getActiveRequestsCount() {
		return activeRequestsCount;
	}

	public void setActiveRequestsCount(int activeRequestsCount) {
		this.activeRequestsCount = activeRequestsCount;
	}

	public int getInactiveRequestsCount() {
		return inactiveRequestsCount;
	}

	public void setInactiveRequestsCount(int inactiveRequestsCount) {
		this.inactiveRequestsCount = inactiveRequestsCount;
	}

	
	@Override
	public String toString() {
		return "BrokerExportDTO details: Id = "+id;
	}

}
