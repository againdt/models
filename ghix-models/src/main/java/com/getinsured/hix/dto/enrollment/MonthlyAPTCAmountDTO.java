package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class MonthlyAPTCAmountDTO implements Serializable{
	
	private Integer enrollment_id;
	private String healthOrDental;
	private String enrollment_Status;
	private Date enrollmentConfirmationDate;
	private Date enrollmentCoverageStartDate;
	private Date enrollmentCoverageEndDate;
	private Date nextMonthStartDate;
	private String cMSPlanId;
	private Float ehbPercentage;
	private Float nextMonthNetPremium;
	private Float nextMonthGrossPremium;
	private Float nextMonthAPTCAmount;
	
	// To indicate error
	private String  APIStatus;
	private String errMsg;
	
	public Integer getEnrollment_id() {
		return enrollment_id;
	}
	public void setEnrollment_id(Integer enrollment_id) {
		this.enrollment_id = enrollment_id;
	}
	public String getHealthOrDental() {
		return healthOrDental;
	}
	public void setHealthOrDental(String healthOrDental) {
		this.healthOrDental = healthOrDental;
	}
	public String getEnrollment_Status() {
		return enrollment_Status;
	}
	public void setEnrollment_Status(String enrollment_Status) {
		this.enrollment_Status = enrollment_Status;
	}
	public Date getEnrollmentConfirmationDate() {
		return enrollmentConfirmationDate;
	}
	public void setEnrollmentConfirmationDate(Date enrollmentConfirmationDate) {
		this.enrollmentConfirmationDate = enrollmentConfirmationDate;
	}
	public Date getEnrollmentCoverageStartDate() {
		return enrollmentCoverageStartDate;
	}
	public void setEnrollmentCoverageStartDate(Date enrollmentCoverageStartDate) {
		this.enrollmentCoverageStartDate = enrollmentCoverageStartDate;
	}
	public Date getEnrollmentCoverageEndDate() {
		return enrollmentCoverageEndDate;
	}
	public void setEnrollmentCoverageEndDate(Date enrollmentCoverageEndDate) {
		this.enrollmentCoverageEndDate = enrollmentCoverageEndDate;
	}
	public Date getNextMonthStartDate() {
		return nextMonthStartDate;
	}
	public void setNextMonthStartDate(Date nextMonthStartDate) {
		this.nextMonthStartDate = nextMonthStartDate;
	}
	public String getcMSPlanId() {
		return cMSPlanId;
	}
	public void setcMSPlanId(String cMSPlanId) {
		this.cMSPlanId = cMSPlanId;
	}
	public Float getEhbPercentage() {
		return ehbPercentage;
	}
	public void setEhbPercentage(Float ehbPercentage) {
		this.ehbPercentage = ehbPercentage;
	}
	public Float getNextMonthNetPremium() {
		return nextMonthNetPremium;
	}
	public void setNextMonthNetPremium(Float nextMonthNetPremium) {
		this.nextMonthNetPremium = nextMonthNetPremium;
	}
	public Float getNextMonthGrossPremium() {
		return nextMonthGrossPremium;
	}
	public void setNextMonthGrossPremium(Float nextMonthGrossPremium) {
		this.nextMonthGrossPremium = nextMonthGrossPremium;
	}
	public Float getNextMonthAPTCAmount() {
		return nextMonthAPTCAmount;
	}
	public void setNextMonthAPTCAmount(Float nextMonthAPTCAmount) {
		this.nextMonthAPTCAmount = nextMonthAPTCAmount;
	}	
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getAPIStatus() {
		return APIStatus;
	}
	public void setAPIStatus(String aPIStatus) {
		APIStatus = aPIStatus;
	}
	@Override
	public String toString() {
		return "MonthlyAPTCAmountDTO [enrollment_id=" + enrollment_id
				+ ", healthOrDental=" + healthOrDental + ", enrollment_Status="
				+ enrollment_Status + ", enrollmentConfirmationDate="
				+ enrollmentConfirmationDate + ", enrollmentCoverageStartDate="
				+ enrollmentCoverageStartDate + ", enrollmentCoverageEndDate="
				+ enrollmentCoverageEndDate + ", nextMonthStartDate="
				+ nextMonthStartDate + ", cMSPlanId=" + cMSPlanId
				+ ", ehbPercentage=" + ehbPercentage + ", nextMonthNetPremium="
				+ nextMonthNetPremium + ", nextMonthGrossPremium="
				+ nextMonthGrossPremium + ", nextMonthAPTCAmount="
				+ nextMonthAPTCAmount + "]";
	}
	   


}
