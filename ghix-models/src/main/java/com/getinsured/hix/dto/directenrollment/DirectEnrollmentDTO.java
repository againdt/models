package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.directenrollment.DirectEnrollment;
import com.getinsured.hix.model.directenrollment.DirectEnrollmentECM;
import com.getinsured.hix.model.directenrollment.DirectEnrollmentStatistic;
import com.google.gson.Gson;

public class DirectEnrollmentDTO implements Serializable{
	
	public static final SimpleDateFormat MONTH_DATE_YEAR_FORMAT = new SimpleDateFormat("MMddyyyy");
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4694931926225462432L;
	/**
	 * primary key
	 */
	private Long id;
	
	/**
	 * Unique guid on our side
	 * 32 char long GUID
	 */
	private String giApplicationId;
	
	/**
	 * application number assigned by carrier
	 */
	private String issuerApplicationId;
	
	/**
	 * status as a result of transaction
	 * with carrier web service
	 */
	private DirectEnrollmentStatus.ApplicationStatus issuerStatus;
	

	/**
	 * foreign key to household table.
	 * Each household can have multiple enrollments
	 */
	private Long cmrHouseholdId;
	
	private Long issuerId;
	/**
	 * issuer brand name
	 * UHC/Humana
	 */
	private String issuerBrandName;
	/**
	 * 2 char long state code
	 */
	private String state;
	
	/**
	 * current UI page
	 */
	private String currentPage;
	
	/**
	 * this value should be used by UI component only
	 * represents servers effective date
	 */
	private String displayEffectiveDate;
	
	
	
	/**
	 * application status assigned by GetInsured
	 * It might vary from what carriers assign.
	 * For instance when user is filling up initial application,
	 * it will be in "incomplete" status but carrierStatus would be empty
	 * as the application has not been sent to them yet.
	 */
	private DirectEnrollmentStatus.ApplicationStatus giStatus;
	/**
	 * Enrollment Meta id, this application is associated with
	 */
	private Long enrollmentMetaId;
	
	private String data;
	/**
	 * data from UI json
	 */
	private String uiData;
	/**
	 * effective date
	 */
	private Date effectiveDate;
	
	/**
	 * payment data
	 */
	private String paymentData;
	
	/**
	 * premium quoted at the time of record creation
	 */
	private Float premium;
	/**
	 * Each application is associated with a year
	 */
	private String year;
	/**
	 * plan id from GI plan table
	 */
	private Long planId;
	/**
	 * plan name 
	 */
	private String planName;
	
	//private Integer enrollmentId;
	
	private Set<DirectEnrollmentECMDTO> documents = new LinkedHashSet<DirectEnrollmentECMDTO>();
	
	/**
	 * Represents enrollmentId in Enrollment table for this record
	 */
	private Integer enrollmentId;
	
	/**
	 * one to one mapping between d2c application and ssap
	 */
	private Long ssapApplicationID;
	
	private Long tenantId;
	
	private String flowType;
	
	private Date creationTimestamp;
	
	
	public Float getPremium() {
		return premium;
	}


	public String getDisplayEffectiveDate() {
		return displayEffectiveDate;
	}

	public void setDisplayEffectiveDate(String displayEffectiveDate) {
		this.displayEffectiveDate = displayEffectiveDate;
	}




	public void setPremium(Float premium) {
		this.premium = premium;
	}




	public String getYear() {
		return year;
	}




	public void setYear(String year) {
		this.year = year;
	}




	public Long getPlanId() {
		return planId;
	}




	public void setPlanId(Long planId) {
		this.planId = planId;
	}




	public String getPlanName() {
		return planName;
	}




	public void setPlanName(String planName) {
		this.planName = planName;
	}


	public Long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Long issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerBrandName() {
		return issuerBrandName;
	}

	public void setIssuerBrandName(String issuerBrandName) {
		this.issuerBrandName = issuerBrandName;
	}

	public String getState() {
		return state;
	}




	public void setState(String state) {
		this.state = state;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	
	public Long getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Long cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	
	public String getIssuerApplicationId() {
		return issuerApplicationId;
	}
	
	public void setIssuerApplicationId(String issuerApplicationId) {
		this.issuerApplicationId = issuerApplicationId;
	}

	public String getGiApplicationId() {
		return giApplicationId;
	}

	public void setGiApplicationId(String giApplicationId) {
		this.giApplicationId = giApplicationId;
	}

	public Long getEnrollmentMetaId() {
		return enrollmentMetaId;
	}

	public void setEnrollmentMetaId(Long enrollmentMetaId) {
		this.enrollmentMetaId = enrollmentMetaId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getUiData() {
		return uiData;
	}

	public void setUiData(String uiData) {
		this.uiData = uiData;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	
	public String getPaymentData() {
		return paymentData;
	}
	
	public void setPaymentData(String paymentData) {
		this.paymentData = paymentData;
	}

	public Set<DirectEnrollmentECMDTO> getDocuments() {
		return documents;
	}


	public void setDocuments(Set<DirectEnrollmentECMDTO> documents) {
		this.documents = documents;
	}

	public void addDocument(DirectEnrollmentECMDTO document){
		this.documents.add(document);
	}


	public DirectEnrollmentDTO() {
		this.giApplicationId=UUID.randomUUID().toString();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	
	
	public Long getSsapApplicationID() {
		return ssapApplicationID;
	}


	public void setSsapApplicationID(Long ssapApplicationID) {
		this.ssapApplicationID = ssapApplicationID;
	}

	public DirectEnrollmentStatus.ApplicationStatus getIssuerStatus() {
		return issuerStatus;
	}


	public void setIssuerStatus(
			DirectEnrollmentStatus.ApplicationStatus issuerStatus) {
		this.issuerStatus = issuerStatus;
	}


	public DirectEnrollmentStatus.ApplicationStatus getGiStatus() {
		return giStatus;
	}


	public void setGiStatus(DirectEnrollmentStatus.ApplicationStatus giStatus) {
		this.giStatus = giStatus;
	}


	public DirectEnrollmentDTO(DirectEnrollment enrollment){
		if(enrollment!=null){
			this.cmrHouseholdId = enrollment.getCmrHouseholdId();
			this.currentPage = enrollment.getPageLastVisited();
			this.data = enrollment.getData();
			this.id = enrollment.getId();
			this.enrollmentMetaId = enrollment.getEnrollmentMetaId();
			this.giApplicationId = enrollment.getGiApplicationId();
			this.giStatus = enrollment.getGiStatus(); 
			this.issuerApplicationId = enrollment.getIssuerApplicationId();
			this.issuerBrandName = enrollment.getIssuerBrandName();
			this.issuerId = enrollment.getIssuerId();
			this.issuerStatus = enrollment.getIssuerStatus();
			this.state = enrollment.getState();
			this.uiData = enrollment.getUiData();
			this.effectiveDate = enrollment.getEffectiveDate();
			this.planName = enrollment.getPlanName();
			this.planId = enrollment.getPlanId();
			this.premium = enrollment.getPremium();
			this.paymentData = enrollment.getPaymentData();
			this.year = enrollment.getYear();
			this.enrollmentId = enrollment.getEnrollmentId();
			this.ssapApplicationID = enrollment.getSsapApplicationId();
			this.tenantId = enrollment.getTenantId();
			this.flowType = enrollment.getFlowType();
			this.creationTimestamp = enrollment.getCreationTimestamp();
			
			/**
			 * only for display
			 */
			if(enrollment.getEffectiveDate()!=null){
				this.displayEffectiveDate = MONTH_DATE_YEAR_FORMAT.format(enrollment.getEffectiveDate());
			}
			
			/**
			 * Sort the documents here
			 */
			if(enrollment.getDocuments()!=null){
				Set<DirectEnrollmentECM> documents = new TreeSet<DirectEnrollmentECM>(new DirectEnrollmentComparatorByCreationTime());
				
				for(DirectEnrollmentECM ecm : enrollment.getDocuments()){
					documents.add(ecm);
				}
				
				for(DirectEnrollmentECM ecm : documents){
					this.addDocument(new DirectEnrollmentECMDTO(ecm)); 
				}
			}
		}
	}
	
	/**
	 * For statistics only
	 * @param enrollment
	 */
	public DirectEnrollmentDTO(DirectEnrollmentStatistic enrollment){
		if(enrollment!=null){
			this.cmrHouseholdId = enrollment.getCmrHouseholdId();
			this.currentPage = enrollment.getPageLastVisited();
			this.data = enrollment.getData();
			this.id = enrollment.getId();
			this.enrollmentMetaId = enrollment.getEnrollmentMetaId();
			this.giApplicationId = enrollment.getGiApplicationId();
			this.giStatus = enrollment.getGiStatus(); 
			this.issuerApplicationId = enrollment.getIssuerApplicationId();
			this.issuerBrandName = enrollment.getIssuerBrandName();
			this.issuerId = enrollment.getIssuerId();
			this.issuerStatus = enrollment.getIssuerStatus();
			this.state = enrollment.getState();
			this.effectiveDate = enrollment.getEffectiveDate();
			this.planName = enrollment.getPlanName();
			this.planId = enrollment.getPlanId();
			this.premium = enrollment.getPremium();
			this.year = enrollment.getYear();
			this.enrollmentId = enrollment.getEnrollmentId();
			this.ssapApplicationID = enrollment.getSsapApplicationId();
			this.tenantId = enrollment.getTenantId();
			this.flowType = enrollment.getFlowType();
			this.creationTimestamp = enrollment.getCreationTimestamp();
			
			/**
			 * only for display
			 */
			if(enrollment.getEffectiveDate()!=null){
				this.displayEffectiveDate = MONTH_DATE_YEAR_FORMAT.format(enrollment.getEffectiveDate());
			}
			
			/**
			 * Sort the documents here
			 */
			if(enrollment.getDocuments()!=null){
				Set<DirectEnrollmentECM> documents = new TreeSet<DirectEnrollmentECM>(new DirectEnrollmentComparatorByCreationTime());
				
				for(DirectEnrollmentECM ecm : enrollment.getDocuments()){
					documents.add(ecm);
				}
				
				for(DirectEnrollmentECM ecm : documents){
					this.addDocument(new DirectEnrollmentECMDTO(ecm)); 
				}
			}
		}
	}
	
	public DirectEnrollment createModel(){
		DirectEnrollment enrollment = new DirectEnrollment();
		enrollment.setId(this.id);
		enrollment.setCmrHouseholdId(this.getCmrHouseholdId());
		enrollment.setData(this.getData());
		enrollment.setEffectiveDate(this.getEffectiveDate());
		enrollment.setEnrollmentMetaId(this.enrollmentMetaId);
		enrollment.setGiApplicationId(this.giApplicationId);
		enrollment.setGiStatus(this.giStatus);
		enrollment.setIssuerApplicationId(this.issuerApplicationId);
		enrollment.setIssuerBrandName(this.issuerBrandName);
		enrollment.setIssuerId(this.issuerId);
		enrollment.setIssuerStatus(this.issuerStatus);
		enrollment.setPageLastVisited(this.currentPage);
		enrollment.setState(this.state);
		enrollment.setPaymentData(this.paymentData);
		enrollment.setYear(this.year);
		enrollment.setPlanId(this.planId);
		enrollment.setPlanName(this.planName);
		enrollment.setPremium(this.premium);
		enrollment.setEnrollmentId(this.enrollmentId);
		enrollment.setSsapApplicationId(this.ssapApplicationID);
		enrollment.setUiData(this.uiData);
		enrollment.setTenantId(this.tenantId);
		enrollment.setFlowType(this.flowType);
		enrollment.setFlowType(this.flowType);
		enrollment.setTenantId(this.tenantId);
		enrollment.setCreationTimestamp(this.creationTimestamp);

		Set<DirectEnrollmentECM> docs = new TreeSet<DirectEnrollmentECM>();
		if(enrollment.getDocuments()!=null){
			for(DirectEnrollmentECM ecm : enrollment.getDocuments()){
				this.addDocument(new DirectEnrollmentECMDTO(ecm));
			}
		}
		enrollment.setDocuments(docs);
		return enrollment;
	}
	
	class DirectEnrollmentComparatorByCreationTime implements Comparator<DirectEnrollmentECM> {
		@Override
		public int compare(DirectEnrollmentECM ecm1, DirectEnrollmentECM ecm2) {
			return ecm2.getCreationTimestamp().compareTo(ecm1.getCreationTimestamp());
		}
	}

	public static void main(String args[]){
		DirectEnrollmentDTO dto = new DirectEnrollmentDTO();
		dto.setIssuerApplicationId("7890");
		dto.setIssuerId(76L);
		dto.setCurrentPage("7");
		dto.setEnrollmentMetaId(1L);
		dto.setIssuerBrandName("UHC");
		dto.setGiStatus(DirectEnrollmentStatus.ApplicationStatus.INCOMPLETE);
		dto.setData("{dummy json response here}");
		dto.setUiData("{fname:John, lname:Doe}");
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		System.out.println(gson.toJson(dto));
	}


	/**
	 * @return the tenantId
	 */
	public Long getTenantId() {
		return tenantId;
	}


	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}


	/**
	 * @return the flowType
	 */
	public String getFlowType() {
		return flowType;
	}


	/**
	 * @param flowType the flowType to set
	 */
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
}
