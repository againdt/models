package com.getinsured.hix.model.plandisplay;



public class IndividualRequest {
	
	private IndividualHousehold individualHousehold;
	private int loggedInUserId;
	
		public IndividualHousehold getIndividualHousehold() {
		return individualHousehold;
		}

		public void setIndividualHousehold(IndividualHousehold individualHousehold) {
			this.individualHousehold = individualHousehold;
		}

		public int getLoggedInUserId() {
			return loggedInUserId;
		}

		public void setLoggedInUserId(int loggedInUserId) {
			this.loggedInUserId = loggedInUserId;
		}

		
}

