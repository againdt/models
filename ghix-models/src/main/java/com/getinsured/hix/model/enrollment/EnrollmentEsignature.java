package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.model.Esignature;


/**
 * @author priya
 *
 */
@Entity
@Table(name="ENROLLMENT_ESIGNATURE")
public class EnrollmentEsignature implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_ESIGNATURE_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_ESIGNATURE_SEQ", sequenceName = "ENROLLMENT_ESIGNATURE_SEQ", allocationSize = 1)
	private Integer id;
	
		
	@ManyToOne
    @JoinColumn(name="ENROLLMENT_ID")
	private Enrollment enrollment;
	
	
	@OneToOne (cascade = {CascadeType.ALL})
    @JoinColumn(name="ESIGNATURE_ID")
	private Esignature esignature;
	
	@Column(name="TAXFILLER_ESIGN_INDICATOR")
	private String taxFillerEsignIndicator;
	
	
	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public Esignature getEsignature() {
		return esignature;
	}

	public void setEsignature(Esignature esignature) {
		this.esignature = esignature;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaxFillerEsignIndicator() {
		return taxFillerEsignIndicator;
	}

	public void setTaxFillerEsignIndicator(String taxFillerEsignIndicator) {
		this.taxFillerEsignIndicator = taxFillerEsignIndicator;
	}

	
	
}
