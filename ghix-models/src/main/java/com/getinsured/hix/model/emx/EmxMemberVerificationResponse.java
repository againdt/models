package com.getinsured.hix.model.emx;

import java.io.Serializable;

public class EmxMemberVerificationResponse implements Serializable {

	private static final long serialVersionUID = 1230951042501540394L;

	private String emxVerificationStatus;
	
	private EmxMemberVerificationDto memberDto;

	public String getEmxVerificationStatus() {
		return emxVerificationStatus;
	}

	public void setEmxVerificationStatus(String emxVerificationStatus) {
		this.emxVerificationStatus = emxVerificationStatus;
	}

	public EmxMemberVerificationDto getMemberDto() {
		return memberDto;
	}

	public void setMemberDto(EmxMemberVerificationDto memberDto) {
		this.memberDto = memberDto;
	}
	
	
}
