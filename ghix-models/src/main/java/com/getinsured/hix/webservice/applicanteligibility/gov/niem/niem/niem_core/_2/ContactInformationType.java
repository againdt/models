//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.String;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ComplexObjectType;



/**
 * A data type for how to contact a person or an organization.
 * 
 * <p>Java class for ContactInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactEmailID" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactMailingAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}ContactMainTelephoneNumber"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactMobileTelephoneNumber" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactInformationType", propOrder = {
    "contactEmailID",
    "contactMailingAddress",
    "contactMainTelephoneNumber",
    "contactMobileTelephoneNumber"
})
public class ContactInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "ContactEmailID", nillable = true)
    protected String contactEmailID;
    @XmlElement(name = "ContactMailingAddress", nillable = true)
    protected List<AddressType> contactMailingAddress;
    @XmlElement(name = "ContactMainTelephoneNumber", namespace = "http://hix.cms.gov/0.1/hix-core", required = true)
    protected TelephoneNumberType contactMainTelephoneNumber;
    @XmlElement(name = "ContactMobileTelephoneNumber", nillable = true)
    protected TelephoneNumberType contactMobileTelephoneNumber;

    /**
     * Gets the value of the contactEmailID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactEmailID() {
        return contactEmailID;
    }

    /**
     * Sets the value of the contactEmailID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactEmailID(String value) {
        this.contactEmailID = value;
    }

    /**
     * Gets the value of the contactMailingAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactMailingAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactMailingAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddressType }
     * 
     * 
     */
    public List<AddressType> getContactMailingAddress() {
        if (contactMailingAddress == null) {
            contactMailingAddress = new ArrayList<AddressType>();
        }
        return this.contactMailingAddress;
    }

    /**
     * Gets the value of the contactMainTelephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumberType }
     *     
     */
    public TelephoneNumberType getContactMainTelephoneNumber() {
        return contactMainTelephoneNumber;
    }

    /**
     * Sets the value of the contactMainTelephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumberType }
     *     
     */
    public void setContactMainTelephoneNumber(TelephoneNumberType value) {
        this.contactMainTelephoneNumber = value;
    }

    /**
     * Gets the value of the contactMobileTelephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumberType }
     *     
     */
    public TelephoneNumberType getContactMobileTelephoneNumber() {
        return contactMobileTelephoneNumber;
    }

    /**
     * Sets the value of the contactMobileTelephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumberType }
     *     
     */
    public void setContactMobileTelephoneNumber(TelephoneNumberType value) {
        this.contactMobileTelephoneNumber = value;
    }

}
