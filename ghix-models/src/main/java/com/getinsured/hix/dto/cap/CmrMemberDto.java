package com.getinsured.hix.dto.cap;

import java.util.Map;

public class CmrMemberDto {
	private Integer memberId;
	private String memberType; 
	private String medicarePin;
	private String hraContribution;
	private String hicn;
	private String medicarePartAeffectiveDate;
	private String medicarePartBeffectiveDate;
	private String medicarePartAterminationDate;
	private String medicarePartBterminationDate;
	private String emplpoyerHraEstimatedStartDate;
	private String externalId;
	private boolean isMedicareEligible;
	private Boolean enableMedicare;
	private String deathDate;
	private String birthDate;
	private Integer householdId;
	private Integer dispositionCode;
	private Map<Integer,String> dispositionCodes;
	private String drxSessionId;
	
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getMedicarePin() {
		return medicarePin;
	}
	public void setMedicarePin(String medicarePin) {
		this.medicarePin = medicarePin;
	}
	public String getHraContribution() {
		return hraContribution;
	}
	public void setHraContribution(String hraContribution) {
		this.hraContribution = hraContribution;
	}
	public String getHicn() {
		return hicn;
	}
	public void setHicn(String hicn) {
		this.hicn = hicn;
	}
	public String getMedicarePartAeffectiveDate() {
		return medicarePartAeffectiveDate;
	}
	public void setMedicarePartAeffectiveDate(String medicarePartAeffectiveDate) {
		this.medicarePartAeffectiveDate = medicarePartAeffectiveDate;
	}
	public String getMedicarePartBeffectiveDate() {
		return medicarePartBeffectiveDate;
	}
	public void setMedicarePartBeffectiveDate(String medicarePartBeffectiveDate) {
		this.medicarePartBeffectiveDate = medicarePartBeffectiveDate;
	}
	public String getMedicarePartAterminationDate() {
		return medicarePartAterminationDate;
	}
	public void setMedicarePartAterminationDate(String medicarePartAterminationDate) {
		this.medicarePartAterminationDate = medicarePartAterminationDate;
	}
	public String getMedicarePartBterminationDate() {
		return medicarePartBterminationDate;
	}
	public void setMedicarePartBterminationDate(String medicarePartBterminationDate) {
		this.medicarePartBterminationDate = medicarePartBterminationDate;
	}
	public String getEmplpoyerHraEstimatedStartDate() {
		return emplpoyerHraEstimatedStartDate;
	}
	public void setEmplpoyerHraEstimatedStartDate(String emplpoyerHraEstimatedStartDate) {
		this.emplpoyerHraEstimatedStartDate = emplpoyerHraEstimatedStartDate;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public boolean isMedicareEligible() {
		return isMedicareEligible;
	}
	public void setMedicareEligible(boolean isMedicareEligible) {
		this.isMedicareEligible = isMedicareEligible;
	}
	public String getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}
	public Integer getDispositionCode() {
		return dispositionCode;
	}
	public void setDispositionCode(Integer dispositionCode) {
		this.dispositionCode = dispositionCode;
	}
	public Map<Integer, String> getDispositionCodes() {
		return dispositionCodes;
	}
	public void setDispositionCodes(Map<Integer, String> dispositionCodes) {
		this.dispositionCodes = dispositionCodes;
	}
	public Boolean getEnableMedicare() {
		return enableMedicare;
	}
	public void setEnableMedicare(Boolean enableMedicare) {
		this.enableMedicare = enableMedicare;
	}
	public String getDrxSessionId() {
		return drxSessionId;
	}
	public void setDrxSessionId(String drxSessionId) {
		this.drxSessionId = drxSessionId;
	}
}