package com.getinsured.hix.dto.broker;

import java.io.Serializable;

/**
 * DTO for IND50
 *
 */
public class EmployerBOBDetailsDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String applicationEndDate;
	private String applicationInitiatedDate;
	private String applicationSubmitDate;
	private String applicationWithdrawalDate;
	private int averageAnnualSalary;
	private String businessLegalName;
	private String contactAddressCity;
	private String contactAddressCountry;
	private String contactAddressFirstLine;
	private String contactAddressSecondLine;
	private String contactAddressState;
	private String contactAddressZipCode;
	private String contactEmailID;
	private String contactFirstName;
	private String contactLastName;
	private long contactPhoneNumber;
	private String eligibilityStatus;
	//Same as Employer ID
	private long employerCaseID;
	private int enrolleesTotalCount;
	private String enrollmentStatus;
	private String federalEIN;
	private String preferedCommunicationMethod;
	private String reportingName;
	private String stateEIN;
	private int totalEmployees;
	private int totalFullTimeEmployees;

	private String responseDescription;
	private int responseCode;
	
	public String getApplicationEndDate() {
		return applicationEndDate;
	}

	public void setApplicationEndDate(String applicationEndDate) {
		this.applicationEndDate = applicationEndDate;
	}

	public String getApplicationInitiatedDate() {
		return applicationInitiatedDate;
	}

	public void setApplicationInitiatedDate(String applicationInitiatedDate) {
		this.applicationInitiatedDate = applicationInitiatedDate;
	}

	public String getApplicationSubmitDate() {
		return applicationSubmitDate;
	}

	public void setApplicationSubmitDate(String applicationSubmitDate) {
		this.applicationSubmitDate = applicationSubmitDate;
	}

	public String getApplicationWithdrawalDate() {
		return applicationWithdrawalDate;
	}

	public void setApplicationWithdrawalDate(String applicationWithdrawalDate) {
		this.applicationWithdrawalDate = applicationWithdrawalDate;
	}

	public int getAverageAnnualSalary() {
		return averageAnnualSalary;
	}

	public void setAverageAnnualSalary(int averageAnnualSalary) {
		this.averageAnnualSalary = averageAnnualSalary;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public String getContactAddressCity() {
		return contactAddressCity;
	}

	public void setContactAddressCity(String contactAddressCity) {
		this.contactAddressCity = contactAddressCity;
	}

	public String getContactAddressCountry() {
		return contactAddressCountry;
	}

	public void setContactAddressCountry(String contactAddressCountry) {
		this.contactAddressCountry = contactAddressCountry;
	}

	public String getContactAddressFirstLine() {
		return contactAddressFirstLine;
	}

	public void setContactAddressFirstLine(String contactAddressFirstLine) {
		this.contactAddressFirstLine = contactAddressFirstLine;
	}

	public String getContactAddressSecondLine() {
		return contactAddressSecondLine;
	}

	public void setContactAddressSecondLine(String contactAddressSecondLine) {
		this.contactAddressSecondLine = contactAddressSecondLine;
	}

	public String getContactAddressState() {
		return contactAddressState;
	}

	public void setContactAddressState(String contactAddressState) {
		this.contactAddressState = contactAddressState;
	}

	public String getContactAddressZipCode() {
		return contactAddressZipCode;
	}

	public void setContactAddressZipCode(String contactAddressZipCode) {
		this.contactAddressZipCode = contactAddressZipCode;
	}

	public String getContactEmailID() {
		return contactEmailID;
	}

	public void setContactEmailID(String contactEmailID) {
		this.contactEmailID = contactEmailID;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public long getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(long contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public long getEmployerCaseID() {
		return employerCaseID;
	}

	public void setEmployerCaseID(long employerCaseID) {
		this.employerCaseID = employerCaseID;
	}

	public int getEnrolleesTotalCount() {
		return enrolleesTotalCount;
	}

	public void setEnrolleesTotalCount(int enrolleesTotalCount) {
		this.enrolleesTotalCount = enrolleesTotalCount;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(String federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getPreferedCommunicationMethod() {
		return preferedCommunicationMethod;
	}

	public void setPreferedCommunicationMethod(
			String preferedCommunicationMethod) {
		this.preferedCommunicationMethod = preferedCommunicationMethod;
	}

	public String getReportingName() {
		return reportingName;
	}

	public void setReportingName(String reportingName) {
		this.reportingName = reportingName;
	}

	public String getStateEIN() {
		return stateEIN;
	}

	public void setStateEIN(String stateEIN) {
		this.stateEIN = stateEIN;
	}

	public int getTotalEmployees() {
		return totalEmployees;
	}

	public void setTotalEmployees(int totalEmployees) {
		this.totalEmployees = totalEmployees;
	}

	public int getTotalFullTimeEmployees() {
		return totalFullTimeEmployees;
	}

	public void setTotalFullTimeEmployees(int totalFullTimeEmployees) {
		this.totalFullTimeEmployees = totalFullTimeEmployees;
	}

	
	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	@Override
	public String toString() {
		return "EmployerBOBDetailsDTO details: EmployerCaseID = "+employerCaseID+", EnrollmentStatus = "+enrollmentStatus+", FederalEIN = "+federalEIN+", BusinessLegalName = "+businessLegalName +
				", StateEIN = "+stateEIN+", ResponseDescription = "+responseDescription+", " +
						"ResponseCode = "+responseCode;
	}
}
