package com.getinsured.hix.dto.cap;

import java.io.Serializable;
import java.util.Date;

public class CapConsumerDto implements Serializable {
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((householdId == null) ? 0 : householdId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		CapConsumerDto other = (CapConsumerDto) obj;
		
		
		if (householdId != null && null!=other.householdId) {
			
			if(Integer.parseInt(householdId) == Integer.parseInt(other.householdId))
			{
				return true;
			}
		}
	return false;	
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idEnc;
	private String firstName;
	private String lastName;
	private String state;
	private String zipCode;
	private String address;
	private String contactNumber;
	private String consumerStage;
	private String leadStatus;
	private String eligLeadstatus;
	private String lastUpdated;
	private String censusType;
	private String email;
	private String lastUpdatedBy;
	private String recordType;
	private String leadId;
	private String householdId;
	private String ssapId;
	private String ssn;
	private String ssnNotProvidedReason;
	private Date dob;

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getIdEnc() {
		return idEnc;
	}
	public void setIdEnc(String idEnc) {
		this.idEnc = idEnc;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getConsumerStage() {
		return consumerStage;
	}
	public void setConsumerStage(String consumerStage) {
		this.consumerStage = consumerStage;
	}
	public String getLeadStatus() {
		return leadStatus;
	}
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}
	public String getEligLeadstatus() {
		return eligLeadstatus;
	}
	public void setEligLeadstatus(String eligLeadstatus) {
		this.eligLeadstatus = eligLeadstatus;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getCensusType() {
		return censusType;
	}
	public void setCensusType(String censusType) {
		this.censusType = censusType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public String getSsapId() {
		return ssapId;
	}
	public void setSsapId(String ssapId) {
		this.ssapId = ssapId;
	}
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	public String getSsnNotProvidedReason() {
		return ssnNotProvidedReason;
	}

	public void setSsnNotProvidedReason(String ssnNotProvidedReason) {
		this.ssnNotProvidedReason = ssnNotProvidedReason;
	}


}