package com.getinsured.hix.model.estimator.mini;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.estimator.mini.EligLead.STATUS;

/**
 * HIX-70580
 * This is the model class for PRE_ELIGIBILITY_RESULTS table
 * @author Nikhil Talreja
 */
@Entity
@Table(name="PRE_ELIGIBILITY_RESULTS")
@DynamicUpdate
@DynamicInsert
public class PreEligibilityResults implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * Status and Stage enums will be from ELIG_LEAD
	 */
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRE_ELIGIBILITY_RESULTS_SEQ")
	@SequenceGenerator(name = "PRE_ELIGIBILITY_RESULTS_SEQ", sequenceName = "PRE_ELIGIBILITY_RESULTS_SEQ", allocationSize = 1)
	private long id;
	
	@JsonIgnore
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="LEAD_ID")
	private EligLead leadId;
	
	@Column(name = "ZIPCODE")
	private String zipCode;
	
	
	@Column(name = "COUNTY_CODE")
	private String countyCode;
	
	
	@Column(name = "FAMILY_SIZE")
	private int familySize;
	
	
	@Column(name = "NO_OF_APPLICANTS")
	private int noOfApplicants;
	
	
	@Column(name = "HOUSEHOLD_INCOME")
	private double householdIncome;
	
	
	@Column(name = "MEMBER_DATA")
	private String memberData;
	
	@Column(name = "API_OUTPUT")
	private String apiOutput;
	
	@Column(name = "OVERALL_API_STATUS")
	private String overallApiStatus;
	
	
	@Column(name = "ERROR_CODE")
	private int errorCode;
	
	@Column(name = "ERROR_MSG")
	private String errorMessage;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)	
	private STATUS status;
	
	@Column(name = "STAGE")
	@Enumerated(EnumType.STRING)	
	private STAGE stage;
	
	@Column(name = "CSR")
	private String csr;
	
	@Column(name = "APTC")
	private String aptc;
	
	
	@Column(name = "PREMIUM")
	private String premium;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "COVERAGE_START_DATE")
	private Date coverageStartDate;
	
	@Column(name = "COVERAGE_YEAR")
	private int coverageYear;
	
	
	@Column(name = "CREATED_BY")
	private long createdBy;
	
	
	@Column(name = "LAST_UPDATED_BY")
	private long lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date creationTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public EligLead getLeadId() {
		return leadId;
	}

	public void setLeadId(EligLead leadId) {
		this.leadId = leadId;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public int getNoOfApplicants() {
		return noOfApplicants;
	}

	public void setNoOfApplicants(int noOfApplicants) {
		this.noOfApplicants = noOfApplicants;
	}

	public double getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(double householdIncome) {
		this.householdIncome = householdIncome;
	}

	public String getMemberData() {
		return memberData;
	}

	public void setMemberData(String memberData) {
		this.memberData = memberData;
	}

	public String getApiOutput() {
		return apiOutput;
	}

	public void setApiOutput(String apiOutput) {
		this.apiOutput = apiOutput;
	}

	public String getOverallApiStatus() {
		return overallApiStatus;
	}

	public void setOverallApiStatus(String overallApiStatus) {
		this.overallApiStatus = overallApiStatus;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public STAGE getStage() {
		return stage;
	}

	public void setStage(STAGE stage) {
		this.stage = stage;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public Date getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public int getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	@PrePersist
	public void prePersist(){
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	@PreUpdate
	public void preUpdate(){
		this.setLastUpdateTimestamp(new TSDate());
	}
	
}
