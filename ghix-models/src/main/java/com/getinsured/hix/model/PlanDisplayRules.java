package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * Model class for PM_DISPLAY_RULES table
 * 
 * @since 25 November, 2014
 */
@Audited
@Entity
@Table(name = "PM_DISPLAY_RULES")
@DynamicUpdate
@DynamicInsert
public class PlanDisplayRules {

	public enum IS_ACTIVE {
		// Y-Yes, N-No
		Y, N;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_DISPLAY_RULES_SEQ")
	@SequenceGenerator(name = "PM_DISPLAY_RULES_SEQ", sequenceName = "PM_DISPLAY_RULES_SEQ", allocationSize = 1)
	private Long id;

	@Column(name = "HIOS_ISSUER_ID")
	private String hiosIssuerId;

	@Column(name = "COPAY_VALUE")
	private String copayValue;

	@Column(name = "COPAY_ATTR")
	private String copayAttribute;

	@Column(name = "COINS_VALUE")
	private String coinsuranceValue;

	@Column(name = "COINS_ATTR")
	private String coinsuranceAttribute;

	@Column(name = "TILE_DISP_TEMPLATE")
	private String tileDisplayTemplate;

	@Column(name = "STANDARD_DISP_TEMPLATE")
	private String standardDisplayTemplate;

	@Column(name = "PC_T1_FOR_NUM_OF_VISITS")
	private String primaryCareT1ForNumVisits;

	@Column(name = "PC_T1_FOR_NUM_OF_COPAY")
	private String primaryCareT1ForNumCopay;

	@Column(name = "PC_T1_FOR_COPAY_AND_VISITS")
	private String primaryCareT1ForCopayAndVisits;

	@Column(name = "IS_ACTIVE")
	private String isActive;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public PlanDisplayRules() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getCopayValue() {
		return copayValue;
	}

	public void setCopayValue(String copayValue) {
		this.copayValue = copayValue;
	}

	public String getCopayAttribute() {
		return copayAttribute;
	}

	public void setCopayAttribute(String copayAttribute) {
		this.copayAttribute = copayAttribute;
	}

	public String getCoinsuranceValue() {
		return coinsuranceValue;
	}

	public void setCoinsuranceValue(String coinsuranceValue) {
		this.coinsuranceValue = coinsuranceValue;
	}

	public String getCoinsuranceAttribute() {
		return coinsuranceAttribute;
	}

	public void setCoinsuranceAttribute(String coinsuranceAttribute) {
		this.coinsuranceAttribute = coinsuranceAttribute;
	}

	public String getTileDisplayTemplate() {
		return tileDisplayTemplate;
	}

	public void setTileDisplayTemplate(String tileDisplayTemplate) {
		this.tileDisplayTemplate = tileDisplayTemplate;
	}

	public String getStandardDisplayTemplate() {
		return standardDisplayTemplate;
	}

	public void setStandardDisplayTemplate(String standardDisplayTemplate) {
		this.standardDisplayTemplate = standardDisplayTemplate;
	}

	public String getPrimaryCareT1ForNumVisits() {
		return primaryCareT1ForNumVisits;
	}

	public void setPrimaryCareT1ForNumVisits(String primaryCareT1ForNumVisits) {
		this.primaryCareT1ForNumVisits = primaryCareT1ForNumVisits;
	}

	public String getPrimaryCareT1ForNumCopay() {
		return primaryCareT1ForNumCopay;
	}

	public void setPrimaryCareT1ForNumCopay(String primaryCareT1ForNumCopay) {
		this.primaryCareT1ForNumCopay = primaryCareT1ForNumCopay;
	}

	public String getPrimaryCareT1ForCopayAndVisits() {
		return primaryCareT1ForCopayAndVisits;
	}

	public void setPrimaryCareT1ForCopayAndVisits(
			String primaryCareT1ForCopayAndVisits) {
		this.primaryCareT1ForCopayAndVisits = primaryCareT1ForCopayAndVisits;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
	public String toString() {
		return "PlanDisplayRules [id=" + id + ", hiosIssuerId=" + hiosIssuerId
				+ ", copayValue=" + copayValue + ", copayAttribute="
				+ copayAttribute + ", coinsuranceValue=" + coinsuranceValue
				+ ", coinsuranceAttribute=" + coinsuranceAttribute
				+ ", tileDisplayTemplate=" + tileDisplayTemplate
				+ ", standardDisplayTemplate=" + standardDisplayTemplate
				+ ", primaryCareT1ForNumVisits=" + primaryCareT1ForNumVisits
				+ ", primaryCareT1ForNumCopay=" + primaryCareT1ForNumCopay
				+ ", primaryCareT1ForCopayAndVisits="
				+ primaryCareT1ForCopayAndVisits + ", isActive=" + isActive
				+ ", createdBy=" + createdBy + ", creationTimestamp="
				+ creationTimestamp + ", lastUpdateTimestamp="
				+ lastUpdateTimestamp + ", lastUpdatedBy=" + lastUpdatedBy
				+ "]";
	}
}
