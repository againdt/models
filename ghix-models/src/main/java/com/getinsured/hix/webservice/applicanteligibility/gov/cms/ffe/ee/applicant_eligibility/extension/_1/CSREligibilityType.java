//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.InsurancePlanVariantCategoryAlphaCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.InsurancePlanVariantCategoryNumericCodeType;


/**
 * A data type for cost sharing reduction (CSR) eligibility determination outcome.
 * 
 * <p>Java class for CSREligibilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CSREligibilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://applicant-eligibility.ee.ffe.cms.gov/extension/1.0}ProgramEligibilityType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-pm}InsurancePlanVariantCategoryCode" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CSREligibilityType", propOrder = {
    "insurancePlanVariantCategoryCode"
})
public class CSREligibilityType
    extends ProgramEligibilityType
{

    @XmlElementRef(name = "InsurancePlanVariantCategoryCode", namespace = "http://hix.cms.gov/0.1/hix-pm", type = JAXBElement.class)
    protected List<JAXBElement<?>> insurancePlanVariantCategoryCode;

    /**
     * Gets the value of the insurancePlanVariantCategoryCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insurancePlanVariantCategoryCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsurancePlanVariantCategoryCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link InsurancePlanVariantCategoryNumericCodeType }{@code >}
     * {@link JAXBElement }{@code <}{@link InsurancePlanVariantCategoryAlphaCodeType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getInsurancePlanVariantCategoryCode() {
        if (insurancePlanVariantCategoryCode == null) {
            insurancePlanVariantCategoryCode = new ArrayList<JAXBElement<?>>();
        }
        return this.insurancePlanVariantCategoryCode;
    }

}
