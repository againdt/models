/**
 * 
 */
package com.getinsured.planmgmt.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * @author gorai_k
 *
 */
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GreaterThanZeroValidator.class)
@Documented
public @interface GreaterThanZero {
	String message() default "{err.greaterThanZero}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
