package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

/**
 * 
 * @author kaul_s
 * CapAgentEmployment.  It holds the agent information
 */
@Audited
@Entity
@Table(name = "cap_agents")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class CapAgentEmployment implements Serializable {

	private static final long serialVersionUID = 9073375894943388728L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAP_AGENTS_SEQ")
	@SequenceGenerator(name = "CAP_AGENTS_SEQ", sequenceName = "CAP_AGENTS_SEQ", allocationSize = 1)
	@Column(name = "id")
	private Integer id;

	@Column(name = "AGENT_ID_CUIC")
	private Long externalAgentId;

	@Column(name = "AGENT_EXTENSION")
	private String agentExtension;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID")
	private AccountUser userId;

	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updated;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "HIRE_DATE")
	private Date hireDate;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "TERMINATION_DATE")
	private Date terminationDate;
	
	@Column(name="LOCATION") 
	String location;
	
	@Column(name = "STATUS")
	String status;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "SHIFT_ID")
	CapAgentShift capAgentShift;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "SHIFT_START_DATE")
	private Date shiftStartDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "SHIFT_END_DATE")
	private Date shiftEndDate;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "GROUPS_OF_USERS_ID")
	private CapTeam teamId;
	
	@Column(name = "SHIFT_HOURS_PER_WEEK")
	private Float shiftHours;
	
	@Column(name = "SHIFT_DETAILS")
	private String shiftDetails;

	@Column(name="TENANT_ID")
    private Long tenantId;

	@Column(name="DAILY_GOALS")
	private String dailyGoals;
	
	public enum agent_location
	{
		Atlanta("Atlanta"), Phoenix("Phoenix");
		
		private final String agent_location;

		private agent_location(String location) {
			agent_location = location;
		}
		
		public String getAgentLocation() {
			return agent_location;
		}
	}
	
	public enum agent_status
	{
		Active("Active"), Inactive("Inactive"),Terminated("Terminated"),Missing_Information("Missing Information");
		
		private final String agent_status;

		private agent_status(String status) {
			agent_status = status;
		}
		
		public String getAgentLocation() {
			return agent_status;
		}
			
		@Override public String toString() { return agent_status; }
		
		public static agent_status getEnum(String value) {
	        for(agent_status v : values())
	            if(v.getAgentLocation().equalsIgnoreCase(value)) return v;
	        throw new IllegalArgumentException();
	    }
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getExternalAgentId() {
		return externalAgentId;
	}

	public void setExternalAgentId(Long externalAgentId) {
		this.externalAgentId = externalAgentId;
	}

	public String getAgentExtension() {
		return agentExtension;
	}

	public void setAgentExtension(String agentExtension) {
		this.agentExtension = agentExtension;
	}

	public AccountUser getUserId() {
		return userId;
	}

	public void setUserId(AccountUser userId) {
		this.userId = userId;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public CapAgentShift getCapAgentShifts() {
		return capAgentShift;
	}

	public void setCapAgentShifts(CapAgentShift capAgentShift) {
		this.capAgentShift = capAgentShift;
	}

	public void setStatus(String status) {
		if(StringUtils.isEmpty(status)){
			status = "Missing Information";
		}
		this.status = status;
	}

	@PrePersist
	public void prePersist(){
		if(null==this.created){
		this.setCreated(new TSDate());
		}
		this.setUpdated(new TSDate());
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        setTenantId(TenantContextHolder.getTenant().getId());
		}
	}

	public Date getShiftStartDate() {
		return shiftStartDate;
	}

	public void setShiftStartDate(Date shiftStartDate) {
		this.shiftStartDate = shiftStartDate;
	}

	public Date getShiftEndDate() {
		return shiftEndDate;
	}

	public void setShiftEndDate(Date shiftEndDate) {
		this.shiftEndDate = shiftEndDate;
	}
	public CapTeam getTeamId() {
		return teamId;
	}

	public void setTeamId(CapTeam teamId) {
		this.teamId = teamId;
	}

	public Float getShiftHours() {
		return shiftHours;
	}

	public void setShiftHours(Float shiftHours) {
		this.shiftHours = shiftHours;
	}

	public String getShiftDetails() {
		return shiftDetails;
	}

	public void setShiftDetails(String shiftDetails) {
		this.shiftDetails = shiftDetails;
	}	
	
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	public void setDailyGoals(String dailyGoals) {
		this.dailyGoals = dailyGoals;
	}
	
	public String getDailyGoals() {
		return dailyGoals;
	}
}
