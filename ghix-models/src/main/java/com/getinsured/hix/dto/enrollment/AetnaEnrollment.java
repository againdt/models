package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;



public class AetnaEnrollment implements Serializable{

	private String subscriberFirstName;
	private String subscriberLastName;
	private String subscriberDob;
	private String subscriberZip;
	
	
	private String medicalPlanId;
	private String dentalIndicator;
	private String gender;
	private String maritalStatus;
	private String familyCode;
	private String enrollmentReason;
	private String tobaccoUse;
	private String citizenOfUnitedStates;
	private String notCitizenOfUnitedStates;
	private String insdNo;
	private String isAppSubmitted;
	private String reasonForSpecAnnualEnrollment;
	private String other;
	private String primarySpokenLanguage;
	private String spokenLanguage;
	private String primaryWrittenLanguage;
	private String writtenLanguage;
	private String areYouResidentOfState;
	private String callMorning;
	private String callAfternoon;
	private String callEvening;
	
	private String caseManagementCM100;
	private String caseManagementCM101;
	private String caseManagementCM102;
	private String caseManagementCM103;
	private String caseManagementCM104;
	private String caseManagementCM105;
	private String caseManagementCM106;
	private String caseManagementCM107;
	private String caseManagementCM108;
	private String caseManagementCM109;
	private String caseManagementCM110;
	private String caseManagementCM111;
	private String caseManagementCM112;
	private String caseManagementCM113;
	private String caseManagementCM114;
	private String caseManagementCM115;
	private String caseManagementCM116;
	private String caseManagementCM117;
	private String caseManagementCM118;
	private String caseManagementCM119;
	private String caseManagementCM120;
	private String caseManagementCM121;
	private String caseManagementCM122;
	
	private String caseManagementOther;
	private String sourceType;
	private String sourceReferenceId;
	
	
	private String valueOfContextCode;
	private String receiveEmailsFromUs;
	private String turnOffPaper;
	private String state;
	
	private String coveredIndividualStatus;
	
	private String arrivalDate;
	
	private String employmentInfo;
	private String stateOfBirth;
	private String timeSpentOutOfState;
	private String otherInsuranceForHCR;
	private String enrollmentPeriodConfirm;
	private String enrollmentPeriodQns;
	private String enrollmentPeriodOther;
	private String dateOfEvent;
	private String renewalDate;
		
	private String eligibleMilitaryVeteran;
	private String relationshipToApplicant;
	
	public String getSubscriberFirstName() {
		return subscriberFirstName;
	}
	public void setSubscriberFirstName(String subscriberFirstName) {
		this.subscriberFirstName = subscriberFirstName;
	}
	public String getSubscriberLastName() {
		return subscriberLastName;
	}
	public void setSubscriberLastName(String subscriberLastName) {
		this.subscriberLastName = subscriberLastName;
	}
	public String getSubscriberDob() {
		return subscriberDob;
	}
	public void setSubscriberDob(String subscriberDob) {
		this.subscriberDob = subscriberDob;
	}
	public String getSubscriberZip() {
		return subscriberZip;
	}
	public void setSubscriberZip(String subscriberZip) {
		this.subscriberZip = subscriberZip;
	}
		
	public String getMedicalPlanId() {
		return medicalPlanId;
	}
	public void setMedicalPlanId(String medicalPlanId) {
		this.medicalPlanId = medicalPlanId;
	}
	public String getDentalIndicator() {
		return dentalIndicator;
	}
	public void setDentalIndicator(String dentalIndicator) {
		this.dentalIndicator = dentalIndicator;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getFamilyCode() {
		return familyCode;
	}
	public void setFamilyCode(String familyCode) {
		this.familyCode = familyCode;
	}
	public String getEnrollmentReason() {
		return enrollmentReason;
	}
	public void setEnrollmentReason(String enrollmentReason) {
		this.enrollmentReason = enrollmentReason;
	}
	public String getTobaccoUse() {
		return tobaccoUse;
	}
	public void setTobaccoUse(String tobaccoUse) {
		this.tobaccoUse = tobaccoUse;
	}
	public String getCitizenOfUnitedStates() {
		return citizenOfUnitedStates;
	}
	public void setCitizenOfUnitedStates(String citizenOfUnitedStates) {
		this.citizenOfUnitedStates = citizenOfUnitedStates;
	}
	public String getNotCitizenOfUnitedStates() {
		return notCitizenOfUnitedStates;
	}
	public void setNotCitizenOfUnitedStates(String notCitizenOfUnitedStates) {
		this.notCitizenOfUnitedStates = notCitizenOfUnitedStates;
	}
	public String getInsdNo() {
		return insdNo;
	}
	public void setInsdNo(String insdNo) {
		this.insdNo = insdNo;
	}
	public String getIsAppSubmitted() {
		return isAppSubmitted;
	}
	public void setIsAppSubmitted(String isAppSubmitted) {
		this.isAppSubmitted = isAppSubmitted;
	}
	public String getReasonForSpecAnnualEnrollment() {
		return reasonForSpecAnnualEnrollment;
	}
	public void setReasonForSpecAnnualEnrollment(
			String reasonForSpecAnnualEnrollment) {
		this.reasonForSpecAnnualEnrollment = reasonForSpecAnnualEnrollment;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public String getPrimarySpokenLanguage() {
		return primarySpokenLanguage;
	}
	public void setPrimarySpokenLanguage(String primarySpokenLanguage) {
		this.primarySpokenLanguage = primarySpokenLanguage;
	}
	public String getSpokenLanguage() {
		return spokenLanguage;
	}
	public void setSpokenLanguage(String spokenLanguage) {
		this.spokenLanguage = spokenLanguage;
	}
	public String getPrimaryWrittenLanguage() {
		return primaryWrittenLanguage;
	}
	public void setPrimaryWrittenLanguage(String primaryWrittenLanguage) {
		this.primaryWrittenLanguage = primaryWrittenLanguage;
	}
	public String getWrittenLanguage() {
		return writtenLanguage;
	}
	public void setWrittenLanguage(String writtenLanguage) {
		this.writtenLanguage = writtenLanguage;
	}
	public String getAreYouResidentOfState() {
		return areYouResidentOfState;
	}
	public void setAreYouResidentOfState(String areYouResidentOfState) {
		this.areYouResidentOfState = areYouResidentOfState;
	}
	public String getCallMorning() {
		return callMorning;
	}
	public void setCallMorning(String callMorning) {
		this.callMorning = callMorning;
	}
	public String getCallAfternoon() {
		return callAfternoon;
	}
	public void setCallAfternoon(String callAfternoon) {
		this.callAfternoon = callAfternoon;
	}
	public String getCallEvening() {
		return callEvening;
	}
	public void setCallEvening(String callEvening) {
		this.callEvening = callEvening;
	}
	
	public String getCaseManagementCM100() {
		return caseManagementCM100;
	}
	public void setCaseManagementCM100(String caseManagementCM100) {
		this.caseManagementCM100 = caseManagementCM100;
	}
	public String getCaseManagementCM101() {
		return caseManagementCM101;
	}
	public void setCaseManagementCM101(String caseManagementCM101) {
		this.caseManagementCM101 = caseManagementCM101;
	}
	public String getCaseManagementCM102() {
		return caseManagementCM102;
	}
	public void setCaseManagementCM102(String caseManagementCM102) {
		this.caseManagementCM102 = caseManagementCM102;
	}
	public String getCaseManagementCM103() {
		return caseManagementCM103;
	}
	public void setCaseManagementCM103(String caseManagementCM103) {
		this.caseManagementCM103 = caseManagementCM103;
	}
	public String getCaseManagementCM104() {
		return caseManagementCM104;
	}
	public void setCaseManagementCM104(String caseManagementCM104) {
		this.caseManagementCM104 = caseManagementCM104;
	}
	public String getCaseManagementCM105() {
		return caseManagementCM105;
	}
	public void setCaseManagementCM105(String caseManagementCM105) {
		this.caseManagementCM105 = caseManagementCM105;
	}
	public String getCaseManagementCM106() {
		return caseManagementCM106;
	}
	public void setCaseManagementCM106(String caseManagementCM106) {
		this.caseManagementCM106 = caseManagementCM106;
	}
	public String getCaseManagementCM107() {
		return caseManagementCM107;
	}
	public void setCaseManagementCM107(String caseManagementCM107) {
		this.caseManagementCM107 = caseManagementCM107;
	}
	public String getCaseManagementCM108() {
		return caseManagementCM108;
	}
	public void setCaseManagementCM108(String caseManagementCM108) {
		this.caseManagementCM108 = caseManagementCM108;
	}
	public String getCaseManagementCM109() {
		return caseManagementCM109;
	}
	public void setCaseManagementCM109(String caseManagementCM109) {
		this.caseManagementCM109 = caseManagementCM109;
	}
	public String getCaseManagementCM110() {
		return caseManagementCM110;
	}
	public void setCaseManagementCM110(String caseManagementCM110) {
		this.caseManagementCM110 = caseManagementCM110;
	}
	public String getCaseManagementCM111() {
		return caseManagementCM111;
	}
	public void setCaseManagementCM111(String caseManagementCM111) {
		this.caseManagementCM111 = caseManagementCM111;
	}
	public String getCaseManagementCM112() {
		return caseManagementCM112;
	}
	public void setCaseManagementCM112(String caseManagementCM112) {
		this.caseManagementCM112 = caseManagementCM112;
	}
	public String getCaseManagementCM113() {
		return caseManagementCM113;
	}
	public void setCaseManagementCM113(String caseManagementCM113) {
		this.caseManagementCM113 = caseManagementCM113;
	}
	public String getCaseManagementCM114() {
		return caseManagementCM114;
	}
	public void setCaseManagementCM114(String caseManagementCM114) {
		this.caseManagementCM114 = caseManagementCM114;
	}
	public String getCaseManagementCM115() {
		return caseManagementCM115;
	}
	public void setCaseManagementCM115(String caseManagementCM115) {
		this.caseManagementCM115 = caseManagementCM115;
	}
	public String getCaseManagementCM116() {
		return caseManagementCM116;
	}
	public void setCaseManagementCM116(String caseManagementCM116) {
		this.caseManagementCM116 = caseManagementCM116;
	}
	public String getCaseManagementCM117() {
		return caseManagementCM117;
	}
	public void setCaseManagementCM117(String caseManagementCM117) {
		this.caseManagementCM117 = caseManagementCM117;
	}
	public String getCaseManagementCM118() {
		return caseManagementCM118;
	}
	public void setCaseManagementCM118(String caseManagementCM118) {
		this.caseManagementCM118 = caseManagementCM118;
	}
	public String getCaseManagementCM119() {
		return caseManagementCM119;
	}
	public void setCaseManagementCM119(String caseManagementCM119) {
		this.caseManagementCM119 = caseManagementCM119;
	}
	public String getCaseManagementCM120() {
		return caseManagementCM120;
	}
	public void setCaseManagementCM120(String caseManagementCM120) {
		this.caseManagementCM120 = caseManagementCM120;
	}
	public String getCaseManagementCM121() {
		return caseManagementCM121;
	}
	public void setCaseManagementCM121(String caseManagementCM121) {
		this.caseManagementCM121 = caseManagementCM121;
	}
	public String getCaseManagementCM122() {
		return caseManagementCM122;
	}
	public void setCaseManagementCM122(String caseManagementCM122) {
		this.caseManagementCM122 = caseManagementCM122;
	}
	public String getCaseManagementOther() {
		return caseManagementOther;
	}
	public void setCaseManagementOther(String caseManagementOther) {
		this.caseManagementOther = caseManagementOther;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public String getSourceReferenceId() {
		return sourceReferenceId;
	}
	public void setSourceReferenceId(String sourceReferenceId) {
		this.sourceReferenceId = sourceReferenceId;
	}
	
	public String getValueOfContextCode() {
		return valueOfContextCode;
	}
	public void setValueOfContextCode(String valueOfContextCode) {
		this.valueOfContextCode = valueOfContextCode;
	}
	public String getReceiveEmailsFromUs() {
		return receiveEmailsFromUs;
	}
	public void setReceiveEmailsFromUs(String receiveEmailsFromUs) {
		this.receiveEmailsFromUs = receiveEmailsFromUs;
	}
	public String getTurnOffPaper() {
		return turnOffPaper;
	}
	public void setTurnOffPaper(String turnOffPaper) {
		this.turnOffPaper = turnOffPaper;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCoveredIndividualStatus() {
		return coveredIndividualStatus;
	}
	public void setCoveredIndividualStatus(String coveredIndividualStatus) {
		this.coveredIndividualStatus = coveredIndividualStatus;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getEmploymentInfo() {
		return employmentInfo;
	}
	public void setEmploymentInfo(String employmentInfo) {
		this.employmentInfo = employmentInfo;
	}
	public String getStateOfBirth() {
		return stateOfBirth;
	}
	public void setStateOfBirth(String stateOfBirth) {
		this.stateOfBirth = stateOfBirth;
	}
	public String getTimeSpentOutOfState() {
		return timeSpentOutOfState;
	}
	public void setTimeSpentOutOfState(String timeSpentOutOfState) {
		this.timeSpentOutOfState = timeSpentOutOfState;
	}
	public String getOtherInsuranceForHCR() {
		return otherInsuranceForHCR;
	}
	public void setOtherInsuranceForHCR(String otherInsuranceForHCR) {
		this.otherInsuranceForHCR = otherInsuranceForHCR;
	}
	public String getEnrollmentPeriodConfirm() {
		return enrollmentPeriodConfirm;
	}
	public void setEnrollmentPeriodConfirm(String enrollmentPeriodConfirm) {
		this.enrollmentPeriodConfirm = enrollmentPeriodConfirm;
	}
	public String getEnrollmentPeriodQns() {
		return enrollmentPeriodQns;
	}
	public void setEnrollmentPeriodQns(String enrollmentPeriodQns) {
		this.enrollmentPeriodQns = enrollmentPeriodQns;
	}
	public String getEnrollmentPeriodOther() {
		return enrollmentPeriodOther;
	}
	public void setEnrollmentPeriodOther(String enrollmentPeriodOther) {
		this.enrollmentPeriodOther = enrollmentPeriodOther;
	}
	public String getDateOfEvent() {
		return dateOfEvent;
	}
	public void setDateOfEvent(String dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}
	public String getRenewalDate() {
		return renewalDate;
	}
	public void setRenewalDate(String renewalDate) {
		this.renewalDate = renewalDate;
	}
	public String getEligibleMilitaryVeteran() {
		return eligibleMilitaryVeteran;
	}
	public void setEligibleMilitaryVeteran(String eligibleMilitaryVeteran) {
		this.eligibleMilitaryVeteran = eligibleMilitaryVeteran;
	}
	public String getRelationshipToApplicant() {
		return relationshipToApplicant;
	}
	public void setRelationshipToApplicant(String relationshipToApplicant) {
		this.relationshipToApplicant = relationshipToApplicant;
	}


}
