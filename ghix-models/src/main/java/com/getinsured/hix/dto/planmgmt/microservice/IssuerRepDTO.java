package com.getinsured.hix.dto.planmgmt.microservice;

public class IssuerRepDTO extends AuditDTO {

	private String id;
	private String issuerId;
	private String userId;
	private String firstName;
	private String lastName;
	private String issuerRepTitle;
	private String phone;
	private String email;
	private String hiosId;
	private String confirmed;
	private String lastUpdateDate;
	private String primaryContact;

	public IssuerRepDTO() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIssuerRepTitle() {
		return issuerRepTitle;
	}

	public void setIssuerRepTitle(String issuerRepTitle) {
		this.issuerRepTitle = issuerRepTitle;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(String confirmed) {
		this.confirmed = confirmed;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}
}
