package com.getinsured.hix.model.entity;

import java.io.Serializable;

public interface PopulationData extends Serializable{

	String getValue();
}
