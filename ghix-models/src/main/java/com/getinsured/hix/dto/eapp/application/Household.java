package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.dto.directenrollment.DirectEnrollementDescComparator;
import com.getinsured.hix.dto.directenrollment.DirectEnrollementMemberComparator;
import com.getinsured.hix.dto.directenrollment.DirectEnrollmentMemberDetailsDTO;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;


/**
 * Represents a household
 * @author root
 *
 */
public class Household implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Person primary;
	private Person spouse;
	private List<Person> dependents = new ArrayList<Person>();
	private Double income;
	private Person payer;
	private Boolean isChildOnlyApplication = Boolean.FALSE; 
	/**
	 * information available from Census
	 */
	private EligibilityInput censusData;
	
	private Map<String, Object> other;
	
	
	
	public Household() {
		super();
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(Household.class);
	
	static GsonBuilder builder;
	static {
		builder = new GsonBuilder();
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}
		});
	}

	
	public Person getPrimary() {
		return primary;
	}

	public void setPrimary(Person primary) {
		this.primary = primary;
	}

	public Person getSpouse() {
		return spouse;
	}

	public void setSpouse(Person spouse) {
		this.spouse = spouse;
	}

	public Double getIncome() {
		return income;
	}

	public void setIncome(Double income) {
		this.income = income;
	}

	public Person getPayer() {
		return payer;
	}

	public void setPayer(Person payer) {
		this.payer = payer;
	}

	public Map<String, Object> getOther() {
		return other;
	}

	public void setOther(Map<String, Object> other) {
		this.other = other;
	}
	
	public void addDependent(Person person){
		this.dependents.add(person);
	}
	
	public List<Person> getDependents() {
		return dependents;
	}

	public void setDependents(List<Person> dependents) {
		this.dependents = dependents;
	}
	
	public Boolean getIsChildOnlyApplication() {
		return isChildOnlyApplication;
	}

	public void setIsChildOnlyApplication(Boolean isChildOnlyApplication) {
		this.isChildOnlyApplication = isChildOnlyApplication;
	}
	
	public EligibilityInput getCensusData() {
		return censusData;
	}

	public void setCensusData(EligibilityInput censusData) {
		this.censusData = censusData;
	}
	


	public static Household getSampleInstance(){
		Household household = new Household();
		household.setPrimary(Person.getSampleInstance());
		household.setSpouse(Person.getAdultFemale());
		List<Person> kids = new ArrayList<Person>();
		for(int i=0;i<5;i++){
			kids.add(Person.getKid());
		}
		household.setDependents(kids);
		household.setIncome(78000.00);
		EligibilityInput eligInput = EligibilityInput.getInstance(household.getPrimary().getMailingAddress().getZip(), household.getPrimary().getMailingAddress().getState());
		household.setCensusData(eligInput);
		return household;
	}
	
	/**
	 * Populate with member info and save to given household
	 * @param household
	 * @param memberdata
	 * @param effectiveStartDate
	 * @return
	 */
	public static Household populateMemberData(Household household, SsapApplicationDTO ssapDto){
		if(household==null){
			return household;
		}
		String memberData=ssapDto.getMemberData();
		
		if(StringUtils.isEmpty(memberData)){
			return household;
		}
		
		
		Date effectiveStartDate = ssapDto.getEffectiveStartDate();
		
		List<DirectEnrollmentMemberDetailsDTO> directEnrollmentmemberDetails = new ArrayList<DirectEnrollmentMemberDetailsDTO>();
		Gson gson = builder.create();
		MiniEstimatorResponse response = gson.fromJson(memberData, new TypeToken<MiniEstimatorResponse>() {
		}.getType());
		List<Member> members = response.getMembers();
		DirectEnrollmentMemberDetailsDTO directEnrollmentMember;
		boolean isPrimaryPresentAndSeekingCoverage = false;
		boolean isSpousePresentAndSeekingCoverage = false;
		for (Member member : members) {
			directEnrollmentMember = new DirectEnrollmentMemberDetailsDTO(member);
			directEnrollmentMember.calculateAge(member.getDateOfBirth(), effectiveStartDate);
			directEnrollmentMember.computeMinor(18);
			directEnrollmentMember.setIsSeekingCoverage(member.getIsSeekingCoverage());
			directEnrollmentMember.setIsTobaccoUser(member.getIsTobaccoUser());
			directEnrollmentMember.setMemberNumber(member.getMemberNumber());
			if(member.getGender()!=null){
				if("F".equalsIgnoreCase(member.getGender())){
					directEnrollmentMember.setGender("F");
				}else if(("M").equalsIgnoreCase(member.getGender())){
					directEnrollmentMember.setGender("M");
				}
			}
			/**
			 * HIX-46067 29 september 2014 Sarah suggested we change minors to
			 * child application
			 */
			//if (directEnrollmentMember.getIsMinor() && "Y".equalsIgnoreCase(directEnrollmentMember.getIsSeekingCoverage())) {
			//	directEnrollmentMember.setRelationshipToPrimary(MemberRelationships.CHILD);
			//}
			
			/**
			 * If either primary is absent or not seeking coverage we want to make
			 * spouse as primary(if it exists and seeks coverage)
			 * else
			 * set youngest child to primary
			 */
			if("Y".equalsIgnoreCase(directEnrollmentMember.getIsSeekingCoverage()) && MemberRelationships.SELF.equals(directEnrollmentMember.getRelationshipToPrimary())){
				/**
				 * if this is a minor, make it child
				 */
				if(directEnrollmentMember.getIsMinor()){
					directEnrollmentMember.setRelationshipToPrimary(MemberRelationships.CHILD);
				}else{
					isPrimaryPresentAndSeekingCoverage = true;
				}
				
			}
			
			if("Y".equalsIgnoreCase(directEnrollmentMember.getIsSeekingCoverage()) && MemberRelationships.SPOUSE.equals(directEnrollmentMember.getRelationshipToPrimary())){
				isSpousePresentAndSeekingCoverage = true;
			}
			
			directEnrollmentmemberDetails.add(directEnrollmentMember);
		}
		
		if(!isPrimaryPresentAndSeekingCoverage){
			if(isSpousePresentAndSeekingCoverage){
				setSpouseAsPrimary(directEnrollmentmemberDetails);
			}else{
				
				//make youngest kid as primary
				setYoungestChildAsPrimary(directEnrollmentmemberDetails);
				household.setIsChildOnlyApplication(true);
			}
		}
		
		/**
		 * Sort members primary, spouse and then kids
		 * kids to follow oldest to youngest.
		 * 
		 * If it's child only application, order by youngest to oldest.
		 */
		if(!isPrimaryPresentAndSeekingCoverage && !isSpousePresentAndSeekingCoverage){
			//Child only application, order by asc.
			Collections.sort(directEnrollmentmemberDetails, new DirectEnrollementMemberComparator());
		}else{
			Collections.sort(directEnrollmentmemberDetails, new DirectEnrollementDescComparator());
		}
		
		Person person = null;
		int i = 1;
		for(DirectEnrollmentMemberDetailsDTO dto: directEnrollmentmemberDetails){
			person = Person.getPerson(dto);
			person.setApplicantId(i++);
			if(MemberRelationships.SELF.equals(dto.getRelationshipToPrimary())){
				household.setPrimary(person);
			}else if(MemberRelationships.SPOUSE.equals(dto.getRelationshipToPrimary())){
				household.setSpouse(person);
			}else{
				household.addDependent(person);
			}
		}
		
		/**
		 * Set census information
		 */
		EligibilityInput censusData = EligibilityInput.getInstance(ssapDto.getZipCode(), ssapDto.getState(), ssapDto.getCountyCode());
		household.setCensusData(censusData);
		
		return household;
		
	}
	
	private static List<DirectEnrollmentMemberDetailsDTO> setSpouseAsPrimary(List<DirectEnrollmentMemberDetailsDTO> members){
		for(DirectEnrollmentMemberDetailsDTO member : members){
			if(MemberRelationships.SPOUSE.equals(member.getRelationshipToPrimary())){
				member.setRelationshipToPrimary(MemberRelationships.SELF);
			}
		}
		
		return members;
	}
	
	/**
	 * 
	 * @param members
	 * @return
	 */
	private static List<DirectEnrollmentMemberDetailsDTO> setYoungestChildAsPrimary(List<DirectEnrollmentMemberDetailsDTO> members){
		int youngest = -1;
		Date youngestAge = null;
		try{
			youngestAge = DirectEnrollmentMemberDetailsDTO.MMDDYYYY.parse("09/01/1870");
		}catch(ParseException e){
			LOGGER.error("parse error",e);
		}
		Date memberAge = null;
		DirectEnrollmentMemberDetailsDTO member;
		for(int i=0;i<members.size();i++){
			member = members.get(i);
			if(MemberRelationships.CHILD.equals(member.getRelationshipToPrimary())){
				try{
					memberAge = DirectEnrollmentMemberDetailsDTO.MMDDYYYY.parse(member.getDateOfBirth());
				}catch (ParseException e) {
					continue;
				}
				
				if(memberAge.compareTo(youngestAge)>0){
					youngest = i;
					youngestAge = memberAge;
				}
			}
		}
		member = members.get(youngest);
		member.setRelationshipToPrimary(MemberRelationships.SELF);
		
		return members;
	}
	
	private static Household getFamily(){
		Household household = new Household();
		int primary = Math.random()>0.5?0:1;
		if(primary==0){
			household.setPrimary(Person.getAdultMale());
			household.setSpouse(Person.getAdultFemale());
			
		}else{
			household.setPrimary(Person.getAdultFemale());
			household.setSpouse(Person.getAdultMale());
		}
		household.getSpouse().setRelationshipToPrimary(MemberRelationships.SPOUSE);
		household.setIncome(78000.00);
		EligibilityInput eligInput = EligibilityInput.getInstance(household.getPrimary().getMailingAddress().getZip(), household.getPrimary().getMailingAddress().getState());
		household.setCensusData(eligInput);
		return household;
	}
	
	public static Household getInstance(int size){
		
		if(size==1){
			return getSampleInstance();
		}
		
		if(size==2){
			return getFamily();
		}
		Household household = null;
		if(size>2){
			 household = getFamily();
			for(int i=2;i<size;i++){
				Person person = Person.getKid();
				household.addDependent(person);
			}
		}
		
		household.setIncome(78000.00);
		household.getPrimary().setMailingAddress(Address.getSampleInstance());
		EligibilityInput eligInput = EligibilityInput.getInstance(household.getPrimary().getMailingAddress().getZip(), household.getPrimary().getMailingAddress().getState());
		household.setCensusData(eligInput);
		return household;
	}

	@Override
	public String toString() {
		return "Household [primary=" + primary + ", spouse=" + spouse
				+ ", dependents=" + dependents + ", income=" + income
				+ ", payer=" + payer
				+ ", isChildOnlyApplication=" + isChildOnlyApplication
				+ ", censusData=" + censusData + ", other=" + other + "]";
	}
	
	public void sanitize(){
		for(Person person : this.dependents){
			person.sanitize();
		}
		if(this.primary!=null){
			this.primary.sanitize();
		}
		
		if(this.spouse!=null){
			this.spouse.sanitize();
		}
	}
	
	

}
