INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('MANAGE_ISSUER')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('MANAGE_QHP_PLANS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('MANAGE_SADP_PLANS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('MANAGE_USERS')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('ADD_CONSUMER_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('VIEW_CONSUMER_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('SEARCH_CONSUMERS_CRM')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('SWITCH_TO_CONSUMER_VIEW_CRM')));				
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('ADMIN_TEAM_LIST')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('ADMIN_TEAM_ADD')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('ADMIN_TEAM_DELETE')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('ADMIN_TEAM_EDIT')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('CAP_POLICY_VIEW')));
INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
	VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
	(select id from roles where name='SALES_DIRECTOR'), (select id from permissions   where name=UPPER('CAP_LEAD_VIEW')));
