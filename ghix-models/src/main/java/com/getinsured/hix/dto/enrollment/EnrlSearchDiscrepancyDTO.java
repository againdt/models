package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrlReconMonthlyActivityDTO.IssuerDetails;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;

public class EnrlSearchDiscrepancyDTO implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<EnrlDiscrepancyLookup> enrlDiscrepancyLookupList;
	private List<DiscrepancyStatus> discrepancyStatusList;
	private List<IssuerDetails> issuerDetailsList;
	private List<String> coverageYearList;
	private List<Integer> reconYearList;
	private List<Integer> reconMonthList;
	
	
	public List<EnrlDiscrepancyLookup> getEnrlDiscrepancyLookupList() {
		return enrlDiscrepancyLookupList;
	}

	public void setEnrlDiscrepancyLookupList(List<EnrlDiscrepancyLookup> enrlDiscrepancyLookupList) {
		this.enrlDiscrepancyLookupList = enrlDiscrepancyLookupList;
	}

	public List<DiscrepancyStatus> getDiscrepancyStatusList() {
		return discrepancyStatusList;
	}

	public void setDiscrepancyStatusList(List<DiscrepancyStatus> discrepancyStatusList) {
		this.discrepancyStatusList = discrepancyStatusList;
	}

	public List<IssuerDetails> getIssuerDetailsList() {
		return issuerDetailsList;
	}

	public void setIssuerDetailsList(List<IssuerDetails> issuerDetailsList) {
		this.issuerDetailsList = issuerDetailsList;
	}

	public List<String> getCoverageYearList() {
		return coverageYearList;
	}

	public void setCoverageYearList(List<String> coverageYearList) {
		this.coverageYearList = coverageYearList;
	}

	public List<Integer> getReconYearList() {
		return reconYearList;
	}

	public void setReconYearList(List<Integer> reconYearList) {
		this.reconYearList = reconYearList;
	}

	public List<Integer> getReconMonthList() {
		return reconMonthList;
	}

	public void setReconMonthList(List<Integer> reconMonthList) {
		this.reconMonthList = reconMonthList;
	}

	public static class EnrlDiscrepancyLookup{
		private Integer id;
		private String code;
		private String category;
		private String label;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}	
	}

}
