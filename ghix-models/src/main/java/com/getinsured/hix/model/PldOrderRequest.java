package com.getinsured.hix.model;

public class PldOrderRequest {
	
	
	public enum RequestType {
		PRESHOPPING, POSTSHOPPING,FFM,STM;
	}
	Long  applicationId;	
	String requestType;
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

}
