package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ssap.ConsumerApplicant.BooleanFlag;
import com.getinsured.hix.model.ssap.ConsumerApplicant.EligibilityStateValues;
import com.getinsured.hix.model.ssap.ConsumerApplicant.Gender;
import com.getinsured.hix.model.ssap.ConsumerApplicant.SmokerFlag;


public class FFMMemberResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	private String firstName;
	private String middleName;
	private String lastName;
	private Date birthDate;
	private Gender gender;
	private String phoneNumber;
	private String email;
	private Date eligibilityEndDate;
    private Date eligibilityStartDate;
	private EligibilityStateValues eligibilityStatus;
	private BooleanFlag isHouseHoldContact;
	private BooleanFlag isSeekingCoverage;
	private BooleanFlag aptcEligibilityIndicator;
	private SmokerFlag smoker;
	private Date aptcEligibilityEndDate;
    private Date aptcEligibilityStartDate;
	private BooleanFlag csrEligibilityIndicator;
	private Date csrEligibilityEndDate;
    private Date csrEligibilityStartDate;
	private String csrLevel;
	private Long ffeApplicantId;
	private BooleanFlag maritalStatus;
	private Integer monthlyAptcAmount;
	
	private BooleanFlag chipEligibilityIndicator;
	private BooleanFlag medicaidEligibilityIndicator;
	private BooleanFlag qhpEligibilityIndicator;
	
	private Date chipEligibilityEndDate;
    private Date chipEligibilityStartDate;
    
    private Date medicaidEligibilityEndDate;
    private Date medicaidEligibilityStartDate;
    
    private Date qhpEligibilityStartDate;
    private Date qhpEligibilityEndDate;
	
    private Location homeAddress;
    private Location mailingAddress;
	
	private String personId;
	private BooleanFlag exchangeEligibilityIndicator;
	private String ssn;
	private String suffix;
	private String insuranceApplicantId;
	
	private String familyRelationshipCode;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}
	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}
	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}
	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}
	public EligibilityStateValues getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(EligibilityStateValues eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public BooleanFlag getIsHouseHoldContact() {
		return isHouseHoldContact;
	}
	public void setIsHouseHoldContact(BooleanFlag isHouseHoldContact) {
		this.isHouseHoldContact = isHouseHoldContact;
	}
	public BooleanFlag getIsSeekingCoverage() {
		return isSeekingCoverage;
	}
	public void setIsSeekingCoverage(BooleanFlag isSeekingCoverage) {
		this.isSeekingCoverage = isSeekingCoverage;
	}
	public BooleanFlag getAptcEligibilityIndicator() {
		return aptcEligibilityIndicator;
	}
	public void setAptcEligibilityIndicator(
			BooleanFlag aptcEligibilityIndicator) {
		this.aptcEligibilityIndicator = aptcEligibilityIndicator;
	}
	public Date getAptcEligibilityEndDate() {
		return aptcEligibilityEndDate;
	}
	public void setAptcEligibilityEndDate(Date aptcEligibilityEndDate) {
		this.aptcEligibilityEndDate = aptcEligibilityEndDate;
	}
	public Date getAptcEligibilityStartDate() {
		return aptcEligibilityStartDate;
	}
	public void setAptcEligibilityStartDate(Date aptcEligibilityStartDate) {
		this.aptcEligibilityStartDate = aptcEligibilityStartDate;
	}
	public BooleanFlag getCsrEligibilityIndicator() {
		return csrEligibilityIndicator;
	}
	public void setCsrEligibilityIndicator(BooleanFlag csrEligibilityIndicator) {
		this.csrEligibilityIndicator = csrEligibilityIndicator;
	}
	public Date getCsrEligibilityEndDate() {
		return csrEligibilityEndDate;
	}
	public void setCsrEligibilityEndDate(Date csrEligibilityEndDate) {
		this.csrEligibilityEndDate = csrEligibilityEndDate;
	}
	public Date getCsrEligibilityStartDate() {
		return csrEligibilityStartDate;
	}
	public void setCsrEligibilityStartDate(Date csrEligibilityStartDate) {
		this.csrEligibilityStartDate = csrEligibilityStartDate;
	}
	public String getCsrLevel() {
		return csrLevel;
	}
	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}
	public Long getFfeApplicantId() {
		return ffeApplicantId;
	}
	public void setFfeApplicantId(Long ffeApplicantId) {
		this.ffeApplicantId = ffeApplicantId;
	}
	public BooleanFlag getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(BooleanFlag maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public Integer getMonthlyAptcAmount() {
		return monthlyAptcAmount;
	}
	public void setMonthlyAptcAmount(Integer monthlyAptcAmount) {
		this.monthlyAptcAmount = monthlyAptcAmount;
	}
	
	public BooleanFlag getChipEligibilityIndicator() {
		return chipEligibilityIndicator;
	}
	public void setChipEligibilityIndicator(
			BooleanFlag chipEligibilityIndicator) {
		this.chipEligibilityIndicator = chipEligibilityIndicator;
	}
	public BooleanFlag getMedicaidEligibilityIndicator() {
		return medicaidEligibilityIndicator;
	}
	public void setMedicaidEligibilityIndicator(
			BooleanFlag medicaidEligibilityIndicator) {
		this.medicaidEligibilityIndicator = medicaidEligibilityIndicator;
	}
	public Date getChipEligibilityEndDate() {
		return chipEligibilityEndDate;
	}
	public void setChipEligibilityEndDate(Date chipEligibilityEndDate) {
		this.chipEligibilityEndDate = chipEligibilityEndDate;
	}
	public Date getChipEligibilityStartDate() {
		return chipEligibilityStartDate;
	}
	public void setChipEligibilityStartDate(Date chipEligibilityStartDate) {
		this.chipEligibilityStartDate = chipEligibilityStartDate;
	}
	public Date getMedicaidEligibilityEndDate() {
		return medicaidEligibilityEndDate;
	}
	public void setMedicaidEligibilityEndDate(Date medicaidEligibilityEndDate) {
		this.medicaidEligibilityEndDate = medicaidEligibilityEndDate;
	}
	public Date getMedicaidEligibilityStartDate() {
		return medicaidEligibilityStartDate;
	}
	public void setMedicaidEligibilityStartDate(Date medicaidEligibilityStartDate) {
		this.medicaidEligibilityStartDate = medicaidEligibilityStartDate;
	}
	
		
	public BooleanFlag getQhpEligibilityIndicator() {
		return qhpEligibilityIndicator;
	}
	public void setQhpEligibilityIndicator(BooleanFlag qhpEligibilityIndicator) {
		this.qhpEligibilityIndicator = qhpEligibilityIndicator;
	}
	public Date getQhpEligibilityStartDate() {
		return qhpEligibilityStartDate;
	}
	public void setQhpEligibilityStartDate(Date qhpEligibilityStartDate) {
		this.qhpEligibilityStartDate = qhpEligibilityStartDate;
	}
	public Date getQhpEligibilityEndDate() {
		return qhpEligibilityEndDate;
	}
	public void setQhpEligibilityEndDate(Date qhpEligibilityEndDate) {
		this.qhpEligibilityEndDate = qhpEligibilityEndDate;
	}
	
	public SmokerFlag getSmoker() {
		return smoker;
	}
	public void setSmoker(SmokerFlag smoker) {
		this.smoker = smoker;
	}
	
	public Location getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(Location homeAddress) {
		this.homeAddress = homeAddress;
	}
	public Location getMailingAddress() {
		return mailingAddress;
	}
	public void setMailingAddress(Location mailingAddress) {
		this.mailingAddress = mailingAddress;
	}
	
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	
  public BooleanFlag getExchangeEligibilityIndicator() {
		return exchangeEligibilityIndicator;
	}
	public void setExchangeEligibilityIndicator(
			BooleanFlag exchangeEligibilityIndicator) {
		this.exchangeEligibilityIndicator = exchangeEligibilityIndicator;
	}
	
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public String getInsuranceApplicantId() {
		return insuranceApplicantId;
	}
	public void setInsuranceApplicantId(String insuranceApplicantId) {
		this.insuranceApplicantId = insuranceApplicantId;
	}
	
	public String getFamilyRelationshipCode() {
		return familyRelationshipCode;
	}
	public void setFamilyRelationshipCode(String familyRelationshipCode) {
		this.familyRelationshipCode = familyRelationshipCode;
	}
	@Override
	public String toString() {
		return "FFMMemberResponse [firstName=" + firstName + ", lastName="
				+ lastName + ", birthDate=" + birthDate + ", gender=" + gender
				+ ", phoneNumber=" + phoneNumber + ", email=" + email
				+ ", eligibilityEndDate=" + eligibilityEndDate
				+ ", eligibilityStartDate=" + eligibilityStartDate
				+ ", eligibilityStatus=" + eligibilityStatus
				+ ", isHouseHoldContact=" + isHouseHoldContact
				+ ", isSeekingCoverage=" + isSeekingCoverage
				+ ", aptcEligibilityIndicator=" + aptcEligibilityIndicator
				+ ", aptcEligibilityEndDate=" + aptcEligibilityEndDate
				+ ", aptcEligibilityStartDate=" + aptcEligibilityStartDate
				+ ", csrEligibilityIndicator=" + csrEligibilityIndicator
				+ ", csrEligibilityEndDate=" + csrEligibilityEndDate
				+ ", csrEligibilityStartDate=" + csrEligibilityStartDate
				+",  smoker=" + smoker
				+",  homeAddress id=" + homeAddress.getId()
				+",  mailingAddress id=" + mailingAddress.getId()
				+ ", csrLevel=" + csrLevel + ", ffeApplicantId="
				+ ffeApplicantId + ", maritalStatus=" + maritalStatus
				+ ", monthlyAptcAmount=" + monthlyAptcAmount + "]";
	}
	
	
	
}
