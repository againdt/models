package com.getinsured.hix.dto.eapp.grammer;

import com.google.gson.annotations.SerializedName;


/**
 * A question can be repeated for an entity.
 * For instance template may only specifiy a question at primary level and 
 * specify attribute "entity" as "applicant" to repeat it for
 * all the dynamic members on that application
 * @author root
 *
 */
public enum UIEntityTypes {
	@SerializedName("none")
	NONE,
	@SerializedName("applicant")
	APPLICANT,
	@SerializedName("primary")
	PRIMARY, 
	@SerializedName("spouse")
	SPOUSE,
	@SerializedName("dependent")
	DEPENDENT

}
