/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class RetireeMember extends EmployeeMember {
	
	/*private int memberNumber;
	private String externalMemberId; 
	private String firstName;
	private String lastName;
	private MemberRelationships relationshipToPrimary;
	
	private String dateOfBirth;
	private String gender;
    private String tobaccoUsage="N";
	private String seekingCoverage = "Y";
	private Double individualHraAmount; 
	private String isNativeAmerican = "N";*/
	
	
	private MemberRelationships relationshipToHId=MemberRelationships.SELF;  // Defaults to SELF
	private String title;
	private String middleName;
	private String suffix;
	private String isMedicareEligible = "N";
	private String ssn;
	private String primaryPhone;
	private String alternatePhone;
	private String emailAddress;
	private String dateOfDeath;
	
	 
	//Primary Address of Member
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private String countyCode;
	//Mailing Address of the Member
	private String mailingAddress1;
	private String mailingAddress2;
	private String mailingCity;
	private String mailingState;
	
	private String mailingZipCode;
	private String mailingCountyCode;
	
	private RetireeMedicare medicare;
	
	
	
	public String getIsMedicareEligible() {
		return isMedicareEligible;
	}

	public void setIsMedicareEligible(String isMedicareEligible) {
		this.isMedicareEligible = isMedicareEligible;
	}

	public RetireeMedicare getMedicare() {
		return medicare;
	}

	public void setMedicare(RetireeMedicare medicare) {
		this.medicare = medicare;
	}

	/*private List<String> seekingCoverageProducts = new ArrayList<String>();
	
	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getIndividualHraAmount() {
		return individualHraAmount;
	}

	public void setIndividualHraAmount(Double individualHraAmount) {
		this.individualHraAmount = individualHraAmount;
	}

	public int getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(int memberNumber) {
		this.memberNumber = memberNumber;
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	
	
	

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}
	
	public String getIsNativeAmerican() {
		return isNativeAmerican;
	}

	public void setIsNativeAmerican(String isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FamilyMember [memberNumber=");
		builder.append(memberNumber);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", isTobaccoUser=");
		builder.append(tobaccoUsage);
		
		builder.append(",isNativeAmerican=");
		builder.append(isNativeAmerican);
		builder.append(", relationshipToPrimary=").append(relationshipToPrimary);
		builder.append("]");
		return builder.toString();
	}
	public String getTobaccoUsage() {
		return tobaccoUsage;
	}

	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}

	public String getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(String seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	public class MemberComparatorByMemberNumber implements Comparator<RetireeMember>{
		@Override
		public int compare(RetireeMember memberObj1, RetireeMember memberObj2) {
			return memberObj1.getMemberNumber() < memberObj2.getMemberNumber() ? -1 : (memberObj1.getMemberNumber() == memberObj2.getMemberNumber() ? 0 : 1) ;
		}

	}

		public List<String> getSeekingCoverageProducts() {
		return seekingCoverageProducts;
	}

	public void setSeekingCoverageProducts(List<String> seekingCoverageProducts) {
		this.seekingCoverageProducts = seekingCoverageProducts;
	}
*/
	public MemberRelationships getRelationshipToHId() {
		return relationshipToHId;
	}

	public void setRelationshipToHId(MemberRelationships relationshipToHId) {
		this.relationshipToHId = relationshipToHId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getAlternatePhone() {
		return alternatePhone;
	}

	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getMailingAddress1() {
		return mailingAddress1;
	}

	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}

	public String getMailingAddress2() {
		return mailingAddress2;
	}

	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}

	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	public String getMailingZipCode() {
		return mailingZipCode;
	}

	public void setMailingZipCode(String mailingZipCode) {
		this.mailingZipCode = mailingZipCode;
	}

	public String getMailingCountyCode() {
		return mailingCountyCode;
	}

	public void setMailingCountyCode(String mailingCountyCode) {
		this.mailingCountyCode = mailingCountyCode;
	}
	
	
}
