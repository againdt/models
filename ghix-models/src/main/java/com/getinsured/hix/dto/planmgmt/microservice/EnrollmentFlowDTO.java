package com.getinsured.hix.dto.planmgmt.microservice;

public class EnrollmentFlowDTO extends AuditDTO {

	private String planYear;
	private String isD2cEnabled;
	private String d2cFlowType;

	public EnrollmentFlowDTO() {
		super();
	}

	public String getPlanYear() {
		return planYear;
	}

	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}

	public String getIsD2cEnabled() {
		return isD2cEnabled;
	}

	public void setIsD2cEnabled(String isD2cEnabled) {
		this.isD2cEnabled = isD2cEnabled;
	}

	public String getD2cFlowType() {
		return d2cFlowType;
	}

	public void setD2cFlowType(String d2cFlowType) {
		this.d2cFlowType = d2cFlowType;
	}
}
