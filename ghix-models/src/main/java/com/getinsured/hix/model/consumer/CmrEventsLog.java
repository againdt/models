package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name="CMR_EVENTS")
@DynamicInsert
@DynamicUpdate
public class CmrEventsLog implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CMR_EVENTS_SEQ")
	@SequenceGenerator(name = "CMR_EVENTS_SEQ", sequenceName = "CMR_EVENTS_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="EVENT_ID")
	private Integer eventId;
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "EVENT_SOURCE")
	private String evenetSource;
	
	@Column(name = "EVENT_LEVEL")
	private String evenetLevel;
	
	@Column(name = "EVENT_DETAILS_JSON")
	private String evenetDetails;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false, updatable=false)
	private Date created;
	
	@Column(name="CMR_HOUSEHOLD")
	private Integer householdId;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getEvenetSource() {
		return evenetSource;
	}

	public void setEvenetSource(String evenetSource) {
		this.evenetSource = evenetSource;
	}

	public String getEvenetLevel() {
		return evenetLevel;
	}

	public void setEvenetLevel(String evenetLevel) {
		this.evenetLevel = evenetLevel;
	}

	public String getEvenetDetails() {
		return evenetDetails;
	}

	public void setEvenetDetails(String evenetDetails) {
		this.evenetDetails = evenetDetails;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

}
