package com.getinsured.hix.model.agency;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.getinsured.hix.model.agency.Agency.CertificationStatus;

@Entity
@IdClass(AgencyAudId.class)
@Table(name="AGENCY_AUD")
public class AgencyAud implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	@Id
	@Column(name = "REV")
	private int rev;

	@Column(name="agency_name")
	private String agencyName;

	@Column(name="business_legal_name")
	private String businessLegalName;

	@Column(name="certification_date")
	private Timestamp certificationDate;

	@Column(name="certification_number")
	private Long certificationNumber;

	@Column(name="certification_status")
	@Enumerated(EnumType.STRING)
	private CertificationStatus certificationStatus;

	@Column(name="comments_id")
	private Long commentsId;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="creation_timestamp")
	private Timestamp creationTimestamp;

	@Column(name="federal_tax_id")
	private Long federalTaxId;

	@Column(name="last_update_timestamp")
	private Timestamp lastUpdateTimestamp;

	@Column(name="last_updated_by")
	private Long lastUpdatedBy;

	@Column(name="license_number")
	private String licenseNumber;

	@Column(name="recertify_date")
	private Timestamp recertifyDate;
	
	@Column(name = "AGENCY_SUPPORT_DOC_ID")
	private Integer supportDocumentId;
	
	@Column(name = "AGENCY_CONTRACT_DOC_ID")
	private Integer contractDocumentId;
	
	@Column(name = "AGENCY_E_O_DECLR_DOC_ID")
	private Integer eoDeclarationDocumentId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public Timestamp getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(Timestamp certificationDate) {
		this.certificationDate = certificationDate;
	}

	public Long getCertificationNumber() {
		return certificationNumber;
	}

	public void setCertificationNumber(Long certificationNumber) {
		this.certificationNumber = certificationNumber;
	}

	public CertificationStatus getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(CertificationStatus certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public Long getCommentsId() {
		return commentsId;
	}

	public void setCommentsId(Long commentsId) {
		this.commentsId = commentsId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Long getFederalTaxId() {
		return federalTaxId;
	}

	public void setFederalTaxId(Long federalTaxId) {
		this.federalTaxId = federalTaxId;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public Timestamp getRecertifyDate() {
		return recertifyDate;
	}

	public void setRecertifyDate(Timestamp recertifyDate) {
		this.recertifyDate = recertifyDate;
	}

	public Integer getSupportDocumentId() {
		return supportDocumentId;
	}

	public void setSupportDocumentId(Integer supportDocumentId) {
		this.supportDocumentId = supportDocumentId;
	}

	public Integer getContractDocumentId() {
		return contractDocumentId;
	}

	public void setContractDocumentId(Integer contractDocumentId) {
		this.contractDocumentId = contractDocumentId;
	}

	public Integer getEoDeclarationDocumentId() {
		return eoDeclarationDocumentId;
	}

	public void setEoDeclarationDocumentId(Integer eoDeclarationDocumentId) {
		this.eoDeclarationDocumentId = eoDeclarationDocumentId;
	}
	
	
}
