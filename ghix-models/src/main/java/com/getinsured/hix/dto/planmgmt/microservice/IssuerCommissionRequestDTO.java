package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerCommissionRequestDTO extends IssuerCommissionDTO {

	private List<Integer> issuerIdList;

	public IssuerCommissionRequestDTO() {
		super();
	}

	public List<Integer> getIssuerIdList() {
		return issuerIdList;
	}

	public void setIssuerId(Integer issuerId) {

		if (CollectionUtils.isEmpty(this.issuerIdList)) {
			this.issuerIdList = new ArrayList<Integer>();
		}
		this.issuerIdList.add(issuerId);
	}
}
