package com.getinsured.hix.gerritest;

public class GerritDTO {

	private String testGerrit;
	private String newTest;

	public String getNewTest() {
		return newTest;
	}

	public void setNewTest(String newTest) {
		this.newTest = newTest;
	}

	public String getTestGerrit() {
		return testGerrit;
	}

	public void setTestGerrit(String testGerrit) {
		this.testGerrit = testGerrit;
	}
}
