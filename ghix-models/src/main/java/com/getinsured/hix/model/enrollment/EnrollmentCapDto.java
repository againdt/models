package com.getinsured.hix.model.enrollment;

import java.util.List;

/**
 * 
 * @author Aditya_S
 * @since 30/03/2015 (DD/MM/YYYY)
 * 
 * HIX-64906
 *  
 */
public class EnrollmentCapDto {

	private Integer enrollmentId;
	private String applicationId;
	private String enrollmentStatus;
	private String planType;
	private String submitDate;
	private String lastUpdatedTimestamp;
	private String effectiveDate;
	private Float grossPremiumAmt;
	private Float aptcAmt ;
	private Integer capAgentId;
	private String capAgentName;
	private String exchangeType;
	private int noOfEnrollees;
	private String planTier;
	private String planName;
	private String issuerName;
	private Integer planId;
	private String exchangeAssignPolicyNo;
	private String issuerAssignPolicyNo;
	private String ssapId;
	private List<EnrolleeCapDto> enrolleeCapDtoList;
	
	/**
	 * HIX-73086 Set last update date in EnrollmentCapDto
	 */
	private String enrollmentLastStatusUpdateTimeStamp;

	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId the applicationId to set
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the enrollmentStatus
	 */
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	/**
	 * @return the planType
	 */
	public String getPlanType() {
		return planType;
	}

	/**
	 * @param planType the planType to set
	 */
	public void setPlanType(String planType) {
		this.planType = planType;
	}

	/**
	 * @return the submitDate
	 */
	public String getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the grossPremiumAmt
	 */
	public Float getGrossPremiumAmt() {
		return grossPremiumAmt;
	}

	/**
	 * @param grossPremiumAmt the grossPremiumAmt to set
	 */
	public void setGrossPremiumAmt(Float grossPremiumAmt) {
		this.grossPremiumAmt = grossPremiumAmt;
	}

	/**
	 * @return the aptcAmt
	 */
	public Float getAptcAmt() {
		return aptcAmt;
	}

	/**
	 * @param aptcAmt the aptcAmt to set
	 */
	public void setAptcAmt(Float aptcAmt) {
		this.aptcAmt = aptcAmt;
	}

	/**
	 * @return the capAgentId
	 */
	public Integer getCapAgentId() {
		return capAgentId;
	}

	/**
	 * @param capAgentId the capAgentId to set
	 */
	public void setCapAgentId(Integer capAgentId) {
		this.capAgentId = capAgentId;
	}

	/**
	 * @return the capAgentName
	 */
	public String getCapAgentName() {
		return capAgentName;
	}

	/**
	 * @param capAgentName the capAgentName to set
	 */
	public void setCapAgentName(String capAgentName) {
		this.capAgentName = capAgentName;
	}

	/**
	 * @return the exchangeType
	 */
	public String getExchangeType() {
		return exchangeType;
	}

	/**
	 * @param exchangeType the exchangeType to set
	 */
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	/**
	 * @return the noOfEnrollees
	 */
	public int getNoOfEnrollees() {
		return noOfEnrollees;
	}

	/**
	 * @param noOfEnrollees the noOfEnrollees to set
	 */
	public void setNoOfEnrollees(int noOfEnrollees) {
		this.noOfEnrollees = noOfEnrollees;
	}

	/**
	 * @return the planTier
	 */
	public String getPlanTier() {
		return planTier;
	}

	/**
	 * @param planTier the planTier to set
	 */
	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}

	public String getSsapId() {
		return ssapId;
	}

	public void setSsapId(String ssapId) {
		this.ssapId = ssapId;
	}

	/**
	 * @return the enrolleeCapDtoList
	 */
	public List<EnrolleeCapDto> getEnrolleeCapDtoList() {
		return enrolleeCapDtoList;
	}

	/**
	 * @param enrolleeCapDtoList the enrolleeCapDtoList to set
	 */
	public void setEnrolleeCapDtoList(List<EnrolleeCapDto> enrolleeCapDtoList) {
		this.enrolleeCapDtoList = enrolleeCapDtoList;
	}

	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}

	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}

	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	/**
	 * @return the planId
	 */
	public Integer getPlanId() {
		return planId;
	}

	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	
	/**
	 * @return the lastUpdatedTimestamp
	 */
	public String getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	/**
	 * @param lastUpdatedTimestamp the lastUpdatedTimestamp to set
	 */
	public void setLastUpdatedTimestamp(String lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	/**
	 * @return the enrollmentLastStatusUpdateTimeStamp
	 */
	public String getEnrollmentLastStatusUpdateTimeStamp() {
		return enrollmentLastStatusUpdateTimeStamp;
	}

	/**
	 * @param enrollmentLastStatusUpdateTimeStamp the enrollmentLastStatusUpdateTimeStamp to set
	 */
	public void setEnrollmentLastStatusUpdateTimeStamp(
			String enrollmentLastStatusUpdateTimeStamp) {
		this.enrollmentLastStatusUpdateTimeStamp = enrollmentLastStatusUpdateTimeStamp;
	}

	public String getExchangeAssignPolicyNo() {
		return exchangeAssignPolicyNo;
	}

	public void setExchangeAssignPolicyNo(String exchangeAssignPolicyNo) {
		this.exchangeAssignPolicyNo = exchangeAssignPolicyNo;
	}

	public String getIssuerAssignPolicyNo() {
		return issuerAssignPolicyNo;
	}

	public void setIssuerAssignPolicyNo(String issuerAssignPolicyNo) {
		this.issuerAssignPolicyNo = issuerAssignPolicyNo;
	}
}
