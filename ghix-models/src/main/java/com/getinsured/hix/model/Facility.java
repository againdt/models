package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the facility database table.
 * 
 */
@Entity

@Table(name="facility")
public class Facility implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum FacilityType{
		 M,V,D;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Facility_Seq")
	@SequenceGenerator(name = "Facility_Seq", sequenceName = "facility_seq", allocationSize = 1)
	private int id;

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name="provider_id" )
	private Provider provider;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "facility", fetch = FetchType.EAGER)
	private Set<FacilitySpecialityRelation> facilitySpecialRelation = new HashSet<FacilitySpecialityRelation>(0);

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "facility", fetch = FetchType.EAGER)
	private List<FacilityAddress> facilityAddress = new ArrayList<FacilityAddress>(0);

	@Column(name="type")
	@Enumerated(EnumType.STRING)
	private FacilityType type;
		
	@Column(name="website_url")
	private String websiteUrl;
	
	@Column(name="medical_group")
	private String medicalGroup;
	
	@Column(name="aha_id")
	private String ahaId;
    
    @Transient
	private FacilityAddress facilityAddressObj;
    
    @Transient
    private Specialty specialty;
    
    @Transient
    private Network network;
	
    @Column(name="facility_type_indicator")
	private String facility_type_indicator;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public FacilityType getType() {
		return type;
	}

	public void setType(FacilityType type) {
		this.type = type;
	}
	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String website) {
		this.websiteUrl = website;
	}

	public String getMedicalGroup() {
		return medicalGroup;
	}

	public void setMedicalGroup(String medicalGroup) {
		this.medicalGroup = medicalGroup;
	}
	
	public String getAhaId() {
		return ahaId;
	}

	public void setAhaId(String ahaId) {
		this.ahaId = ahaId;
	}

	public Set<FacilitySpecialityRelation> getFacilitySpecialRelation() {
		return facilitySpecialRelation;
	}

	public void setFacilitySpecialRelation(
			Set<FacilitySpecialityRelation> facilitySpecialRelation) {
		this.facilitySpecialRelation = facilitySpecialRelation;
	}

	public FacilityAddress getFacilityAddressObj() {
		return facilityAddressObj;
	}

	public void setFacilityAddressObj(FacilityAddress facilityAddressObj) {
		this.facilityAddressObj = facilityAddressObj;
	}
	
	public Specialty getSpecialty() {
		return specialty;
	}

	public void setSpecialty(Specialty specialty) {
		this.specialty = specialty;
	}

	public List<FacilityAddress> getFacilityAddress() {
		return facilityAddress;
	}

	public void setFacilityAddress(List<FacilityAddress> facilityAddress) {
		this.facilityAddress = facilityAddress;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}

	public String getFacility_type_indicator() {
		return facility_type_indicator;
	}

	public void setFacility_type_indicator(String facility_type_indicator) {
		this.facility_type_indicator = facility_type_indicator;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}
}
