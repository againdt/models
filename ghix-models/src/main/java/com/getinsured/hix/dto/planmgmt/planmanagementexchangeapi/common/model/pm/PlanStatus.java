
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanStatus.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlanStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Received"/>
 *     &lt;enumeration value="Updated"/>
 *     &lt;enumeration value="DoesNotExist"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "")
@XmlEnum
@XmlRootElement(name="planStatus")
public enum PlanStatus {

    @XmlEnumValue("Received")
    RECEIVED("Received"),
    @XmlEnumValue("Updated")
    UPDATED("Updated"),
    @XmlEnumValue("DoesNotExist")
    DOES_NOT_EXIST("DoesNotExist");
    private final String value;

    PlanStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlanStatus fromValue(String v) {
        for (PlanStatus c: PlanStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
