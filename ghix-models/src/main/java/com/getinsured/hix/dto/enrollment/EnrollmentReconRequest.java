package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;

public class EnrollmentReconRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * Request Fields for Monthly Activity API.
	 */
	private Integer issuerId;
	private Integer monthNum;
	private Integer year;
	private String hiosIssuerId;
	
	
	/*
	 * Request Fields for Enrollment Search Page.
	 */
	private Integer issuerEnrollmentId;
	private Integer hixEnrollmentId;
	private Integer subscriberId;
	private String discrepencyType;
	private Integer reconMonth;
	private Integer reconYear;
	private Integer coverageYear;
	private String status;
	private Integer fileId;
	
	private String sortByField;
	private String sortOrder;
	private Integer pageSize;
	private Integer pageNumber;
	private boolean isSearchOnlyDiscrepancy = false;
	private List<DiscrepancyStatus> discrepancyStatusList;
	
	
	public boolean isSearchOnlyDiscrepancy() {
		return isSearchOnlyDiscrepancy;
	}
	public void setSearchOnlyDiscrepancy(boolean isSearchOnlyDiscrepancy) {
		this.isSearchOnlyDiscrepancy = isSearchOnlyDiscrepancy;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	public Integer getMonthNum() {
		return monthNum;
	}
	public void setMonthNum(Integer monthNum) {
		this.monthNum = monthNum;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getIssuerEnrollmentId() {
		return issuerEnrollmentId;
	}
	public void setIssuerEnrollmentId(Integer issuerEnrollmentId) {
		this.issuerEnrollmentId = issuerEnrollmentId;
	}
	public Integer getHixEnrollmentId() {
		return hixEnrollmentId;
	}
	public void setHixEnrollmentId(Integer hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}
	public Integer getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(Integer subscriberId) {
		this.subscriberId = subscriberId;
	}
	public String getDiscrepencyType() {
		return discrepencyType;
	}
	public void setDiscrepencyType(String discrepencyType) {
		this.discrepencyType = discrepencyType;
	}
	public Integer getReconMonth() {
		return reconMonth;
	}
	public void setReconMonth(Integer reconMonth) {
		this.reconMonth = reconMonth;
	}
	public Integer getReconYear() {
		return reconYear;
	}
	public void setReconYear(Integer reconYear) {
		this.reconYear = reconYear;
	}
	public Integer getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getFileId() {
		return fileId;
	}
	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}
	public String getSortByField() {
		return sortByField;
	}
	public void setSortByField(String sortByField) {
		this.sortByField = sortByField;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public List<DiscrepancyStatus> getDiscrepancyStatusList() {
		return discrepancyStatusList;
	}
	public void setDiscrepancyStatusList(List<DiscrepancyStatus> discrepancyStatusList) {
		this.discrepancyStatusList = discrepancyStatusList;
	}
}
