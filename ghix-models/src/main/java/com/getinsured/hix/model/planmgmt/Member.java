package com.getinsured.hix.model.planmgmt;

import java.util.Calendar;

public class Member {
	
	
	private String memberType;
	private String gender;
	private int age;
	private boolean isTobaccoUser;
	private Calendar dateOfBirth;
	private boolean isStudent;
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Member [memberType=");
		builder.append(memberType);
		builder.append(", gender=");
		builder.append(gender);
		builder.append(", age=");
		builder.append(age);
		builder.append(", isTobaccoUser=");
		builder.append(isTobaccoUser);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", isStudent=");
		builder.append(isStudent);
		builder.append("]");
		return builder.toString();
	}
	
	public Calendar getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Calendar dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isTobaccoUser() {
		return isTobaccoUser;
	}
	public void setTobaccoUser(boolean isTobaccoUser) {
		this.isTobaccoUser = isTobaccoUser;
	}
	public boolean isStudent() {
		return isStudent;
	}

	public void setStudent(boolean isStudent) {
		this.isStudent = isStudent;
	}

}
