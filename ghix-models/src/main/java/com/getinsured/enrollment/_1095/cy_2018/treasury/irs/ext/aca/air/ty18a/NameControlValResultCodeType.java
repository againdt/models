
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NameControlValResultCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NameControlValResultCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EXACT_MATCH"/>
 *     &lt;enumeration value="PROXIMAL_MATCH"/>
 *     &lt;enumeration value="NO_MATCH"/>
 *     &lt;enumeration value="TIN_NOT_FOUND"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NameControlValResultCodeType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
@XmlEnum
public enum NameControlValResultCodeType {

    EXACT_MATCH,
    PROXIMAL_MATCH,
    NO_MATCH,
    TIN_NOT_FOUND;

    public String value() {
        return name();
    }

    public static NameControlValResultCodeType fromValue(String v) {
        return valueOf(v);
    }

}
