/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.io.Serializable;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author Venkata Tadepalli
 *
 */
public class CostComparisionResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Map<Long, Double> outputData;

	public Map<Long, Double> getOutputData() {
		return outputData;
	}

	public void setOutputData(Map<Long, Double> outputData) {
		this.outputData = outputData;
	}

		
		
	
}
