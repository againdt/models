package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for generating the unique Entity Number for Enrollment Entity
 * 
 */
@Entity
@Table(name = "ENTITY_NUMBER")
public class EntityNumber implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENTITY_NUMBER_SEQ")
	@SequenceGenerator(name = "ENTITY_NUMBER_SEQ", sequenceName = "ENTITY_NUMBER_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "EE_ENTITY_ID")
	private Integer entityId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	@Override
	public String toString() {
		return "EntityNumber details: ID = "+id+", EntityId = "+entityId;
	}
}
