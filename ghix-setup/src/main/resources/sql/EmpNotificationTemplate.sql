Insert into NOTICE_TYPES 
(ID,BCC_ADDRESS,CATEGORY,CC_ADDRESS,CREATED,DESCRIPTION,EMAIL_CLASS,FROM_ADDRESS,MODULE_NAME,REPLY_TO,RET_ADDRESS,SUBJECT,TITLE,TYPE,UNSUBSCRIBE_COUNT,UPDATED,LANGUAGE) 
values 
(NOTICE_TYPES_SEQ.NEXTVAL,'vimo@xoriant.com','ENDUSER','vimo@xoriant.com',SYSTIMESTAMP,'Employer Notice Template','EmployerPassedDueDateNotice','vimo@xoriant.com','notices','vimo@xoriant.com','vimo@xoriant.com','Employer passed payment due date','EmployerPassedDueDateNotice','HTML_EMAIL',null,SYSTIMESTAMP,'US_EN');




update NOTICE_TYPES set TEMPLATE=
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Employer Notification</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="537"  border="0" cellpadding="0" cellspacing="0" style="align: center; margin: auto;">
<tr>
<td colspan="3">
<img src="{host}/resources/images/slices_01.jpg" width="537" height="40" alt=""></td>
</tr>
<tr>
<td colspan="3">
<img src="{host}/resources/images/slice_top.gif" width="537" height="6" alt=""></td>
</tr>
<tr>
<td>
<img src="{host}/resources/images/slice_left.gif" width="5" height="650" alt=""></td>
<td width="526">
<table style="border: none; width: 400px; align: center; margin: auto; padding-bottom: 30px;">
<tr>
<td style="padding: 20px 0 30px 0;">
<img src="{host}/resources/images/ms_exchange_image.jpg" width="500" height="66" alt="Mississippi Health Exchange" />
</td>
</tr>
<tr>
<td style="text-align: center; font-family:Arial, Helvetica, sans-serif; font-size: 11px; color: #666; border-top: 1px solid #999; padding-top: 4px; padding-bottom: 4px;border-bottom: 1px solid #999;">
${SystemDate}
</td>
</tr>
<tr>
<td >
<table style="font-family:Arial, Helvetica, sans-serif; color: #333; font-size: 13px; width: 80%; margin: auto; padding-top: 20px; padding-bottom: 20px;">
<tr>
<td style="padding: 10px;">
Dear ${employerInvoices.employer.contactFirstName} ,
</td>
</tr>
<tr>
<td style="padding: 10px;">
Your invoice <strong>${employerInvoices.invoiceNumber}</strong> has passed the due date ${employerInvoices.paymentDueDate?date}.
</td>
</tr>
<tr>
<td style="padding: 10px; line-height: 18px;">
Please pay the due amount<strong> $ ${employerInvoices.totalAmountDue}</strong> otherwise,
Your coverage will be terminated.
</td>
</tr>
<tr>
<td style="padding: 10px;">
Thank you,<br />
<strong>Mississippi Exchange</strong><br />
1800-2500-0041<br />
<img src="{host}/resources/images/signature.jpg" alt="" /><br />
</td>
</tr>                    
</table>
</td>
</tr> 
<tr>
<td style="text-align: center; font-family:Arial, Helvetica, sans-serif; font-size: 11px; color: #666; border-top: 1px solid #999; padding-top: 4px; padding-bottom: 4px; border-bottom: 1px solid #999;">
<font style="color: #C30;">
2110 Newmarket Parkway Suite 200   Marietta, GA 30067<br />
</font>
For assistance, contact the Exchange Service Center at 800-321-6543
</td>
</tr>
</table>
</td>
<td>
<img src="{host}/resources/images/slice_right.gif" width="6" height="650" alt=""></td>
</tr>
<tr>
<td colspan="3">
<img src="{host}/resources/images/slice_bottom.gif" width="537" height="6" alt=""></td>
</tr>
<tr>
<td colspan="3">
<img src="{host}/resources/images/slices_09.jpg" width="537" height="39" alt=""></td>
</tr>
</table>
</body>
</html>' where EMAIL_CLASS ='EmployerPassedDueDateNotice';