package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "BENEFIT_PATCH")
public class BenefitPatch implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BENEFIT_PATCH_SEQ")
	@SequenceGenerator(name = "BENEFIT_PATCH_SEQ", sequenceName = "BENEFIT_PATCH_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_BENEFIT_EDITS_ID")
	private PlanBenefitEdit planBenefitEdits;
	
	@Column(name = "EXECUTED_BY")
	private Integer executedBy;
	
	@Column(name = "EXECUTED_ON")
	private Date executedOn;
	
	@Column(name = "STATUS")
	private String staus;
	
	@Column(name = "STATUS_MESSAGE")
	private String statusMessgae;
	
	@Column(name = "PATCH_ID")
	private String patchId;

	@Column(name = "DATA_SOURCE")
	private String dataSource;
	
	public BenefitPatch(){
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanBenefitEdit getPlanBenefitEdits() {
		return planBenefitEdits;
	}

	public void setPlanBenefitEdits(PlanBenefitEdit planBenefitEdits) {
		this.planBenefitEdits = planBenefitEdits;
	}

	public Integer getExecutedBy() {
		return executedBy;
	}

	public void setExecutedBy(Integer executedBy) {
		this.executedBy = executedBy;
	}

	public Date getExecutedOn() {
		return executedOn;
	}

	public void setExecutedOn(Date executedOn) {
		this.executedOn = executedOn;
	}

	public String getStaus() {
		return staus;
	}

	public void setStaus(String staus) {
		this.staus = staus;
	}

	public String getStatusMessgae() {
		return statusMessgae;
	}

	public void setStatusMessgae(String statusMessgae) {
		this.statusMessgae = statusMessgae;
	}

	public String getPatchId() {
		return patchId;
	}

	public void setPatchId(String patchId) {
		this.patchId = patchId;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	

}
