package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the LOOKUP_STANDARD_RLTNSHP database table.
 *
 *@author Dheeraj Maralkar
 */

@Entity
@Table(name = "LOOKUP_STANDARD_RLTNSHP")
@DynamicInsert
@DynamicUpdate
public class RelationshipType {
	
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOOKUP_STANDARD_RLTNSHP_SEQ")
	@SequenceGenerator(name = "LOOKUP_STANDARD_RLTNSHP_SEQ", sequenceName = "LOOKUP_STANDARD_RLTNSHP_SEQ", allocationSize = 1)
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "LOOKUP_VALUE_ID")
	private LookupValue lookupValue;

	@Column(name = "IS_STANDARD")
	private Boolean isStandard;
	
	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date creationTimeStamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdateTimestamp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LookupValue getLookupValue() {
		return lookupValue;
	}

	public void setLookupValue(LookupValue lookupValue) {
		this.lookupValue = lookupValue;
	}

	public Boolean getIsStandard() {
		return isStandard;
	}

	public void setIsStandard(Boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}


}
