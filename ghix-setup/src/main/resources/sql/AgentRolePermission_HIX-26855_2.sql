INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
				VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
						(select id from roles where name='BROKER'), 
						(select id from permissions where name='BROKER_DECLINE_EMPLOYER_CONSUMER'));
        
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
				VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
						(select id from roles where name='CSR'), 
						(select id from permissions where name='BROKER_DECLINE_EMPLOYER_CONSUMER'));
        
        
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
				VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
						(select id from roles where name='BROKER'), 
						(select id from permissions where name='BROKER_EDIT_CONSUMER'));
        
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
				VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
						(select id from roles where name='CSR'), 
						(select id from permissions where name='BROKER_EDIT_CONSUMER'));
        
        
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
				VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
						(select id from roles where name='CSR'), 
						(select id from permissions where name='ACCOUNT_SWITCH_EMPLOYER'));