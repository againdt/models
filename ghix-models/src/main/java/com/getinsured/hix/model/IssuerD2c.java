/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;


@Audited
@Entity
@Table(name = "ISSUER_D2C")
public class IssuerD2c implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static enum D2CFLOWTYPE {
		EAPP, LEGACY, REDIRECT;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issuer_d2c_seq")
	@SequenceGenerator(name = "issuer_d2c_seq", sequenceName = "issuer_d2c_seq", allocationSize = 1)
	private int id;
	
	@Column(name = "HIOS_ISSUER_ID")
	private String hiosIssuerId;
	
	@Column(name = "APPLICABLE_YEAR")
    private Integer applicableYear; 
	
	@Column(name = "D2CFLAG")
	private String d2cFlag;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	@Column(name = "CREATED_BY")
	private Integer createdBy;
	
	@Column(name = "D2C_FLOW_TYPE")
	@Enumerated(EnumType.STRING)
	private D2CFLOWTYPE d2cFlowType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getD2cFlag() {
		return d2cFlag;
	}

	public void setD2cFlag(String d2cFlag) {
		this.d2cFlag = d2cFlag;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public D2CFLOWTYPE getD2cFlowType() {
		return d2cFlowType;
	}

	public void setD2cFlowType(D2CFLOWTYPE d2cFlowType) {
		this.d2cFlowType = d2cFlowType;
	}
	
	public Map<D2CFLOWTYPE, String> getD2cFlowTypeList() {;
		return IssuerD2c.getD2cFlowTypeListOptionsList();
	}
	
	public static Map<D2CFLOWTYPE, String> getD2cFlowTypeListOptionsList() {
		Map<D2CFLOWTYPE, String> enumMap = new EnumMap<D2CFLOWTYPE, String>(
				D2CFLOWTYPE.class);
		enumMap.put(D2CFLOWTYPE.EAPP, "E-App");
		enumMap.put(D2CFLOWTYPE.LEGACY, "Legacy");
		enumMap.put(D2CFLOWTYPE.REDIRECT, "Redirect");
		return enumMap;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
}
