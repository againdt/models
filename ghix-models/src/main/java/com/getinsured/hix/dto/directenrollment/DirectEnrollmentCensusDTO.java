package com.getinsured.hix.dto.directenrollment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.plandisplay.ProviderBean;



/**
 * The sole purpose of this class to transfer existing information
 * to ui screens. 
 * 
 * Further ng-model will use this data to show on respective form elements. 
 * <p>
 * 
 * In addition this data can also be mapped to the json we get from carrier
 * specific web services.
 * @author root
 *
 */
public class DirectEnrollmentCensusDTO {
	private Long enrollmentId;
	private String giAppId;
	
	private String planName;
	private Long planId;
	private String planCost;
	private String primaryContactFirstName;
	private String state;
	private String zipcode;
	private String primaryContactLastName;
	private String primaryContactAddress1;
	private String primaryContactAddress2;
	private String primaryContactCity;
	private String primaryContactState;
	private String primaryContactPhone;
	private String membersCount;
	private String primaryContactEmail;
	private List<DirectEnrollmentMemberDetailsDTO> memberData;
	private Boolean selectedDoctor = false;
	private String issuerName;
	private Integer issuerId;
	private String stmEvent;
	private String paymentDataEndPoint;
	private String uiDataEndPoint;
	private String brandName;
	private List<DirectEnrollmentCountyDTO> counties = new ArrayList<DirectEnrollmentCountyDTO>();
	private PlanNetworkProductIdentDTO productNetwork;
	private String issuerPlanId;
	private String networkType;
	private boolean hsaEligible;
	private boolean openEnrollment;
	private List<ProviderBean> providers;
	private String displayEffectiveDate;
	private String deductible;
	private PaymentOptionsDTO paymentOptions;
	private HCSCDenRiderCountyPremResDTO hcscDenRiderCountyPremium;
	private Long ssapApplicationId;
	private String issuerLogo;
	private String issuerHiosId;
	private boolean isChildOnlyApplication;
	private String coinsurance;
	private Boolean householdInSession;
	private Boolean userInSession;
	/**
	 * Requested by Humana to be passed in xml
	 */
	private String individualDeductible;
	
	public String getGiAppId() {
		return giAppId;
	}
	public void setGiAppId(String giAppId) {
		this.giAppId = giAppId;
	}
	public boolean isHsaEligible() {
		return hsaEligible;
	}
	public void setHsaEligible(boolean hsaEligible) {
		this.hsaEligible = hsaEligible;
	}
	public boolean isOpenEnrollment() {
		return openEnrollment;
	}
	public void setOpenEnrollment(boolean openEnrollment) {
		this.openEnrollment = openEnrollment;
	}
	private DirectEnrollmentDTO enrollment;
	
	public List<DirectEnrollmentCountyDTO> getCounties() {
		return counties;
	}
	public void setCounties(List<DirectEnrollmentCountyDTO> counties) {
		this.counties = counties;
	}
	public void addCounties(DirectEnrollmentCountyDTO county){
		this.counties.add(county);
	}
	public String getStmEvent() {
		return stmEvent;
	}
	public void setStmEvent(String stmEvent) {
		this.stmEvent = stmEvent;
	}
	public String getPaymentDataEndPoint() {
		return paymentDataEndPoint;
	}
	public void setPaymentDataEndPoint(String paymentDataEndPoint) {
		this.paymentDataEndPoint = paymentDataEndPoint;
	}
	public String getUiDataEndPoint() {
		return uiDataEndPoint;
	}
	public void setUiDataEndPoint(String uiDataEndPoint) {
		this.uiDataEndPoint = uiDataEndPoint;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Boolean getSelectedDoctor() {
		return selectedDoctor;
	}
	public void setSelectedDoctor(Boolean selectedDoctor) {
		this.selectedDoctor = selectedDoctor;
	}
	/**
	 * can be pre-computed based on memberData
	 * Added to make it easier on front end
	 */
	private Boolean hasSpouse=false;
	/**
	 * All members excluding the primary dude
	 * dependentCount = memberCount-1
	 */
	private Integer dependentCount;
	private Date effectiveDate;
	
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	
	public String getPrimaryContactFirstName() {
		return primaryContactFirstName;
	}
	public void setPrimaryContactFirstName(String primaryContactFirstName) {
		this.primaryContactFirstName = primaryContactFirstName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getPrimaryContactLastName() {
		return primaryContactLastName;
	}
	public void setPrimaryContactLastName(String primaryContactLastName) {
		this.primaryContactLastName = primaryContactLastName;
	}
	public String getPrimaryContactAddress1() {
		return primaryContactAddress1;
	}
	public void setPrimaryContactAddress1(String primaryContactAddress1) {
		this.primaryContactAddress1 = primaryContactAddress1;
	}
	public String getPrimaryContactAddress2() {
		return primaryContactAddress2;
	}
	public void setPrimaryContactAddress2(String primaryContactAddress2) {
		this.primaryContactAddress2 = primaryContactAddress2;
	}
	public String getPrimaryContactCity() {
		return primaryContactCity;
	}
	public void setPrimaryContactCity(String primaryContactCity) {
		this.primaryContactCity = primaryContactCity;
	}
	public String getPrimaryContactState() {
		return primaryContactState;
	}
	public void setPrimaryContactState(String primaryContactState) {
		this.primaryContactState = primaryContactState;
	}
	public String getPrimaryContactPhone() {
		return primaryContactPhone;
	}
	public void setPrimaryContactPhone(String primaryContactPhone) {
		this.primaryContactPhone = primaryContactPhone;
	}
	public String getMembersCount() {
		return membersCount;
	}
	public void setMembersCount(String membersCount) {
		this.membersCount = membersCount;
	}
	public Long getPlanId() {
		return planId;
	}
	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	public String getPlanCost() {
		return planCost;
	}
	public void setPlanCost(String planCost) {
		this.planCost = planCost;
	}
	public String getPrimaryContactEmail() {
		return primaryContactEmail;
	}
	public void setPrimaryContactEmail(String primaryContactEmail) {
		this.primaryContactEmail = primaryContactEmail;
	}
	public List<DirectEnrollmentMemberDetailsDTO> getMemberData() {
		return memberData;
	}
	public void setMemberData(List<DirectEnrollmentMemberDetailsDTO> memberData) {
		this.memberData = memberData;
	}
	public Boolean getHasSpouse() {
		return hasSpouse;
	}
	public void setHasSpouse(Boolean hasSpouse) {
		this.hasSpouse = hasSpouse;
	}
	public Integer getDependentCount() {
		return dependentCount;
	}
	public void setDependentCount(Integer dependentCount) {
		this.dependentCount = dependentCount;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public Long getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Long enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	public DirectEnrollmentDTO getEnrollment() {
		return enrollment;
	}
	public void setEnrollment(DirectEnrollmentDTO enrollment) {
		this.enrollment = enrollment;
	}
	public PlanNetworkProductIdentDTO getProductNetwork() {
		return productNetwork;
	}
	public void setProductNetwork(PlanNetworkProductIdentDTO productNetwork) {
		this.productNetwork = productNetwork;
	}
	public String getIssuerPlanId() {
		return issuerPlanId;
	}
	public void setIssuerPlanId(String issuerPlanId) {
		this.issuerPlanId = issuerPlanId;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public List<ProviderBean> getProviders() {
		return providers;
	}
	public void setProviders(List<ProviderBean> providers) {
		this.providers = providers;
	}
	public String getDisplayEffectiveDate() {
		return displayEffectiveDate;
	}
	public void setDisplayEffectiveDate(String displayEffectiveDate) {
		this.displayEffectiveDate = displayEffectiveDate;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public PaymentOptionsDTO getPaymentOptions() {
		return paymentOptions;
	}
	public void setPaymentOptions(PaymentOptionsDTO paymentOptions) {
		this.paymentOptions = paymentOptions;
	}
	public HCSCDenRiderCountyPremResDTO getHcscDenRiderCountyPremium() {
		return hcscDenRiderCountyPremium;
	}
	public void setHcscDenRiderCountyPremium(
			HCSCDenRiderCountyPremResDTO hcscDenRiderCountyPremium) {
		this.hcscDenRiderCountyPremium = hcscDenRiderCountyPremium;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	public String getIssuerHiosId() {
		return issuerHiosId;
	}
	public void setIssuerHiosId(String issuerHiosId) {
		this.issuerHiosId = issuerHiosId;
	}
	public String getIndividualDeductible() {
		return individualDeductible;
	}
	public void setIndividualDeductible(String individualDeductible) {
		this.individualDeductible = individualDeductible;
	}
	public boolean isChildOnlyApplication() {
		return isChildOnlyApplication;
	}
	public void setChildOnlyApplication(boolean isChildOnlyApplication) {
		this.isChildOnlyApplication = isChildOnlyApplication;
	}
	public String getCoinsurance() {
		return coinsurance;
	}
	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}
	public Boolean getHouseholdInSession() {
		return householdInSession;
	}
	public void setHouseholdInSession(Boolean householdInSession) {
		this.householdInSession = householdInSession;
	}
	public Boolean getUserInSession() {
		return userInSession;
	}
	public void setUserInSession(Boolean userInSession) {
		this.userInSession = userInSession;
	}
	
	
}