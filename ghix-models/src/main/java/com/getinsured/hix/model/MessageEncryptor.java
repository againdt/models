/**
 * 
 */
package com.getinsured.hix.model;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;


/**
 * This class contains methods to encrypt/decrypt messages for Secure Inbox
 * @author Nihil Talreja
 * @since 2 January 2013
 *
 */
public class MessageEncryptor {
	
	/*private static KeyPair keypair;
	private static Cipher cipher;*/
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageEncryptor.class);
	
	/**
	 * Method to initialize the cipher 
	 *
	
	static{
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(1024);
			keypair = kpg.generateKeyPair();
			cipher = Cipher.getInstance("RSA");
		} 
		catch (Exception e) {
			LOGGER.error("Unable to initialize encryptor",e);
		}
	}
	 */
	//Method to encrypt the message
	public static String encrypt(String plaintext) throws InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		String encVal = null;
		if(plaintext == null || StringUtils.isEmpty(plaintext)){
			return null;
		}
		try {
			encVal= GhixAESCipherPool.encrypt(plaintext);
		} catch (Exception e) {
			throw new GIRuntimeException(e);
			
		}
		/*cipher.init(Cipher.ENCRYPT_MODE, keypair.getPublic());
		byte[] bytes = plaintext.getBytes("UTF-8");

		byte[] encrypted = blockCipher(bytes,Cipher.ENCRYPT_MODE);

		char[] encryptedTranspherable = Hex.encodeHex(encrypted);*/
		return encVal;
	}
	
	//Method to decrypt the message
	public static String decrypt(String encrypted) throws InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, DecoderException{
		String decVal = null;
		if(encrypted == null || StringUtils.isEmpty(encrypted)){
			return null;
		}
		
		try {
			decVal= GhixAESCipherPool.decrypt(encrypted);
		} catch (Exception e) {
			throw new GIRuntimeException(e);
		}
		
		return decVal;
		
	/*	cipher.init(Cipher.DECRYPT_MODE, keypair.getPrivate());
		byte[] bts = Hex.decodeHex(encrypted.toCharArray());

		byte[] decrypted = blockCipher(bts,Cipher.DECRYPT_MODE);

		return new String(decrypted,"UTF-8");*/
	}
	
	
	/*private static byte[] blockCipher(byte[] bytes, int mode) throws IllegalBlockSizeException, BadPaddingException{
		
		// scrambled will hold intermediate results
		byte[] scrambled = new byte[0];

		// toReturn will hold the total result
		byte[] toReturn = new byte[0];
		
		// if we encrypt we use 100 byte long blocks. Decryption requires 128 byte long blocks (because of RSA)
		int length = (mode == Cipher.ENCRYPT_MODE)? 100 : 128;

		// another buffer. this one will hold the bytes that have to be modified in this step
		byte[] buffer = new byte[length];

		for (int i=0; i< bytes.length; i++){

			// if we filled our buffer array we have our block ready for de- or encryption
			if ((i > 0) && (i % length == 0)){
				//execute the operation
				scrambled = cipher.doFinal(buffer);
				// add the result to our total result.
				toReturn = append(toReturn,scrambled);
				// here we calculate the length of the next buffer required
				int newlength = length;

				// if newlength would be longer than remaining bytes in the bytes array we shorten it.
				if (i + length > bytes.length) {
					 newlength = bytes.length - i;
				}
				// clean the buffer array
				buffer = new byte[newlength];
			}
			// copy byte into our buffer.
			buffer[i%length] = bytes[i];
		}

		// this step is needed if we had a trailing buffer. should only happen when encrypting.
		// example: we encrypt 110 bytes. 100 bytes per run means we "forgot" the last 10 bytes. they are in the buffer array
		scrambled = cipher.doFinal(buffer);

		// final step before we can return the modified data.
		toReturn = append(toReturn,scrambled);

		return toReturn;
	}*/
	
	/*private static byte[] append(byte[] prefix, byte[] suffix){
		byte[] toReturn = new byte[prefix.length + suffix.length];
		for (int i=0; i< prefix.length; i++){
			toReturn[i] = prefix[i];
		}
		for (int i=0; i< suffix.length; i++){
			toReturn[i+prefix.length] = suffix[i];
		}
		return toReturn;
	}*/
}
