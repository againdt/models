package com.getinsured.hix.dto.directenrollment;

import java.io.Serializable;

/**
 * Any audit table in this world
 * needs following 2 params as primary key.
 * @author root
 *
 */
public class AudId implements Serializable {
	
	private int id;
	private int rev;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

}
