/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Srikanth Nakka
 *
 */
public class AgentInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "externalId cannot be empty.")
	private String externalId;
	
	@NotEmpty(message = "aorName cannot be empty.")
	private String aorName;
	
	@NotEmpty(message = "aorNAN cannot be empty.")
	private String aorNPN;

	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * @param externalId the externalId to set
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * @return the aorName
	 */
	public String getAorName() {
		return aorName;
	}

	/**
	 * @param aorName the aorName to set
	 */
	public void setAorName(String aorName) {
		this.aorName = aorName;
	}

	/**
	 * @return the aorNAN
	 */
	public String getAorNPN() {
		return aorNPN;
	}

	/**
	 * @param aorNAN the aorNAN to set
	 */
	public void setAorNPN(String aorNPN) {
		this.aorNPN = aorNPN;
	}

	
}
