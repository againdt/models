#cd /home/oracle/dataload_dir

if [ $# -ne 1 ]
then
  echo " "
  echo "Usage $0 <profile in low-case>"
  echo "Usage $0 [nm|ms]"
  echo "Example: $0 nm"
  echo " "
  exit 2
fi

echo $1
OUTPUTFILE=liquibase_`date +'%Y%m%d'`.sql
echo $OUTPUTFILE

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml updateSQL >$OUTPUTFILE

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml updateSQL >>$OUTPUTFILE
