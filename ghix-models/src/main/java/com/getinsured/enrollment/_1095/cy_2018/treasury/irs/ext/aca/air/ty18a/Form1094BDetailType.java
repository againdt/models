
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Form 1094B Detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1094BDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1094BDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}USID"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}TaxYr"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PayerDLN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}IssuerDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TotalFormsAttachedCnt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}JuratSignatureTxt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}Form1095BDetail" maxOccurs="unbounded"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1094BDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "usid",
    "taxYr",
    "payerDLN",
    "issuerDetail",
    "totalFormsAttachedCnt",
    "juratSignatureTxt",
    "form1095BDetail",
    "errorDetail"
})
public class Form1094BDetailType {

    @XmlElement(name = "USID", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String usid;
    @XmlElement(name = "TaxYr", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String taxYr;
    @XmlElement(name = "PayerDLN", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String payerDLN;
    @XmlElement(name = "IssuerDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected IssuerDetailType issuerDetail;
    @XmlElement(name = "TotalFormsAttachedCnt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String totalFormsAttachedCnt;
    @XmlElement(name = "JuratSignatureTxt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String juratSignatureTxt;
    @XmlElement(name = "Form1095BDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected List<Form1095BDetailType> form1095BDetail;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSID() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSID(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the taxYr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxYr() {
        return taxYr;
    }

    /**
     * Sets the value of the taxYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxYr(String value) {
        this.taxYr = value;
    }

    /**
     * Gets the value of the payerDLN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerDLN() {
        return payerDLN;
    }

    /**
     * Sets the value of the payerDLN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerDLN(String value) {
        this.payerDLN = value;
    }

    /**
     * Gets the value of the issuerDetail property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerDetailType }
     *     
     */
    public IssuerDetailType getIssuerDetail() {
        return issuerDetail;
    }

    /**
     * Sets the value of the issuerDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerDetailType }
     *     
     */
    public void setIssuerDetail(IssuerDetailType value) {
        this.issuerDetail = value;
    }

    /**
     * Gets the value of the totalFormsAttachedCnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalFormsAttachedCnt() {
        return totalFormsAttachedCnt;
    }

    /**
     * Sets the value of the totalFormsAttachedCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalFormsAttachedCnt(String value) {
        this.totalFormsAttachedCnt = value;
    }

    /**
     * Gets the value of the juratSignatureTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJuratSignatureTxt() {
        return juratSignatureTxt;
    }

    /**
     * Sets the value of the juratSignatureTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJuratSignatureTxt(String value) {
        this.juratSignatureTxt = value;
    }

    /**
     * Gets the value of the form1095BDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the form1095BDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForm1095BDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Form1095BDetailType }
     * 
     * 
     */
    public List<Form1095BDetailType> getForm1095BDetail() {
        if (form1095BDetail == null) {
            form1095BDetail = new ArrayList<Form1095BDetailType>();
        }
        return this.form1095BDetail;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
