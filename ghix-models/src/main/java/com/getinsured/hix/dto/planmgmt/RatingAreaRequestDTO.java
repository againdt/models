/**
 * 
 * @author santanu
 * @version 1.0
 * @since Feb 22, 2016 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class RatingAreaRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String countyFips;
	
	private String stateCode;
	
	/**
	 * Constructor.
	 */
	public RatingAreaRequestDTO() {}
	
	/**
	 * @return the countyFips
	 */
	public String getCountyFips() {
		return countyFips;
	}

	/**
	 * @param countyFips the countyFips to set
	 */
	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}
	
	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

}
