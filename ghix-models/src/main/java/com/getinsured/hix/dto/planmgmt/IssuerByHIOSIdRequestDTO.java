/*
 * @author santanu
 * @version 1.0
 * @since July 25, 2016 
 * 
 * This DTO to accept hiosId as request object 
 */

package com.getinsured.hix.dto.planmgmt;

import com.getinsured.hix.model.GHIXRequest;


public class IssuerByHIOSIdRequestDTO extends GHIXRequest {

	private String hiosId;
	private boolean includeLogo;

	/**
	 * @return the hiosId
	 */
	public String getHiosId() {
		return hiosId;
	}

	/**
	 * @param hiosId the hiosId to set
	 */
	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	/**
	 * @return the includeLogo
	 */
	public boolean getIncludeLogo() {
		return includeLogo;
	}

	/**
	 * @param includeLogo the includeLogo to set
	 */
	public void setIncludeLogo(boolean includeLogo) {
		this.includeLogo = includeLogo;
	}
}
