INSERT
INTO NOTICE_TYPES
  (
    ID,
    NAME,
    LANGUAGE,
    USER_TYPE,
    TYPE,
    EXTERNAL_ID,
    TEMPLATE_LOCATION,
    EMAIL_SUBJECT,
    EMAIL_FROM,
    EMAIL_CLASS,
    EFFECTIVE_DATE,
    TERMINATION_DATE,
    CREATION_TIMESTAMP,
    LAST_UPDATE_TIMESTAMP
  )
  VALUES
  (
    notice_types_seq.NEXTVAL,
    'Enrollment Batch Failure Notification Template',
    'US_EN',
    'ENROLLMENT',
    'EMAIL',
    NULL,
    '/resources/notificationTemplate/EnrollmentBatchFailureEmailNotification.html',
    NULL,
    NULL,
    'EnrollmentBatchFailureEmailNotification',
    to_date('05-SEP-13','DD-MON-RR'),
    to_date('05-SEP-13','DD-MON-RR'),
    to_timestamp('05-SEP-13 05.12.37.453575000 AM','DD-MON-RR HH.MI.SS.FF AM'),
    to_timestamp('05-SEP-13 05.12.37.453575000 AM','DD-MON-RR HH.MI.SS.FF AM')
  );