package com.getinsured.hix.dto.cap.consumerapp.mapper;

import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.cap.util.DisplayLists;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.ssap.ConsumerApplication;
import com.getinsured.hix.model.ssap.ConsumerApplication.STATUS;
import com.getinsured.hix.platform.util.DateUtil;


public class CapSSapAppMapper {

	public SsapApplicationDTO getDtoFromApp(ConsumerApplication application) {
		SsapApplicationDTO  dto = null;
		
		if(application != null) {
			dto = new SsapApplicationDTO();
			dto.setId(application.getId());
			if(application.getApplicationType() != null)
			{	
				dto.setApplicationType(application.getApplicationType());
			}else{
				dto.setApplicationType(application.getSsapApplicationType());
			}
			if(null!=application.getAppleadStatus()){
				dto.setApplicationStatus(application.getAppleadStatus().toString());
			}	
			else{
				dto.setApplicationStatus("Active");
			}
			if(null!=application.getStage())
			{
				dto.setDisplayStatusName(DisplayLists.s_validLeadStagesDisplayMap.get(application.getStage().toString()));
			}

			dto.setFfmHouseholdId(application.getExternalApplicationId());
			dto.setApplicationStage(application.getStage());
			dto.setEffectiveStartDate(application.getStartDate());
			dto.setExchangeType(application.getExchangeType());
			dto.setState(application.getState());
			if(null!=application.getEligLead()){
				dto.setEligLeadId(application.getEligLead().getId());
			}
			dto.setPremiumAfterCredit(application.getPremiumAfterCredit());
			dto.setPremiumBeforeCredit(application.getPremiumBeforeCredit());
			dto.setMaximumAptc(application.getMaximumAPTC());
		}
		
		return dto;
	}

	public ConsumerApplication getEntity(SsapApplicationDTO appDto) {
		ConsumerApplication app = null;
		
		if(appDto != null) {
			app =  new ConsumerApplication();
			app.setId(appDto.getId());
			app.setApplicationType(appDto.getApplicationType());
			app.setExternalApplicationId(appDto.getFfmHouseholdId()); 
			if(null!=appDto.getEffectiveStartDate()){
				app.setStartDate(new Timestamp(appDto.getEffectiveStartDate().getTime()));		
				app.setCoverageYear(DateUtil.getYearFromDate(appDto.getEffectiveStartDate()));
			}
			app.setExchangeType(appDto.getExchangeType());
			app.setSource(appDto.getExchangeType()); 
			String appleadStatus =appDto.getApplicationStatus();
			if(StringUtils.isNotEmpty(appleadStatus)){
				app.setAppleadStatus(appleadStatus);	
			}
			else{
				app.setAppleadStatus("Active");
			}
			app.setEligibilityStatus("PE");
			app.setExchangeEligibilityStatus(" ");
			app.setApplicationData(" ");
			//set stage and status for manual application
			app.setStage(STAGE.ENROLLMENT_COMPLETED);
			app.setStatus(STATUS.PENDING);
			
		}
		return app;
	}
	
	
}

