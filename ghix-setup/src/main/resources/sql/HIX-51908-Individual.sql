INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='INDIVIDUAL'), (select id from permissions where name='PORTAL_DISENROLL'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='INDIVIDUAL'), (select id from permissions where name='PORTAL_APPEALS'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP, (select id from roles where name='INDIVIDUAL'), (select id from permissions where name='PORTAL_REFERRALS'));