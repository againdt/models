package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.PopulationServedWrapper;

/**
 * Used to wrap data received from UI and send it to ghix-entity for save
 */
public class AssisterRequestDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Assister assister;
	private AssisterLanguages assisterLanguages;
	private Integer id;
	private EnrollmentEntity enrollmentEntity;
	private Integer userId;
	private Integer moduleId;
	private Integer pageSize;
	private Integer startRecord;
	private String fromDate;
	private String toDate;
	private String assisterType;
	private PopulationServedWrapper populationServedWrapper;
	private String siteType;
	private EntityDocuments entityDocuments;
	private byte[] attachment;
	private String firstName;
	private String lastName;
	private String entityName;
	private String certificationStatus;
	private String statusInactive;
	private String statusactive;
	private int individualId;
	private int siteId;
	private int enrollmentEntityId;
	private String esignName;
	private Map<String, Object> searchCriteria;
	private Status desigStatus;
	private DesignateAssister designateAssister;
	private int totalAssistersBySearch;
	private String sortBy;
	private String sortOrder;
	private String siteName;
	List<Household> listOfIndividuals;
	private String documentId;
	private boolean isAdmin;
	private boolean isCertificationNumberUpdate;
	private List<Integer> listOfIndividualsToReassign;
	private String assisterNumber;
	

	public String getAssisterNumber() {
		return assisterNumber;
	}

	public void setAssisterNumber(String assisterNumber) {
		this.assisterNumber = assisterNumber;
	}

	public List<Integer> getListOfIndividualsToReassign() {
		return listOfIndividualsToReassign;
	}

	public void setListOfIndividualsToReassign(List<Integer> listOfIndividualsToReassign) {
		this.listOfIndividualsToReassign = listOfIndividualsToReassign;
	}

	public boolean isCertificationNumberUpdate() {
		return isCertificationNumberUpdate;
	}

	public void setCertificationNumberUpdate(boolean isCertificationNumberUpdate) {
		this.isCertificationNumberUpdate = isCertificationNumberUpdate;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public List<Household> getListOfIndividuals() {
		return listOfIndividuals;
	}

	public void setListOfIndividuals(List<Household> listOfIndividuals) {
		this.listOfIndividuals = listOfIndividuals;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getEsignName() {
		return esignName;
	}

	public void setEsignName(String esignName) {
		this.esignName = esignName;
	}

	public int getSiteId() {
		return siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public int getEnrollmentEntityId() {
		return enrollmentEntityId;
	}

	public void setEnrollmentEntityId(int enrollmentEntityId) {
		this.enrollmentEntityId = enrollmentEntityId;
	}

	public int getIndividualId() {
		return individualId;
	}

	public void setIndividualId(int individualId) {
		this.individualId = individualId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	public int getTotalAssistersBySearch() {
		return totalAssistersBySearch;
	}

	public void setTotalAssistersBySearch(int totalAssistersBySearch) {
		this.totalAssistersBySearch = totalAssistersBySearch;
	}

	public String getAssisterType() {
		return assisterType;
	}

	public void setAssisterType(String assisterType) {
		this.assisterType = assisterType;
	}

	public PopulationServedWrapper getPopulationServedWrapper() {
		return populationServedWrapper;
	}

	public void setPopulationServedWrapper(PopulationServedWrapper populationServedWrapper) {
		this.populationServedWrapper = populationServedWrapper;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	public void setAttachment(byte[] attachment1) {
		if (attachment1 == null) {
			this.attachment = new byte[0];
		} else {
			this.attachment = Arrays.copyOf(attachment1, attachment1.length);
		}
	}

	public EntityDocuments getEntityDocuments() {
		return entityDocuments;
	}

	public void setEntityDocuments(EntityDocuments entityDocuments) {
		this.entityDocuments = entityDocuments;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public EnrollmentEntity getEnrollmentEntity() {
		return enrollmentEntity;
	}

	public void setEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		this.enrollmentEntity = enrollmentEntity;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Assister getAssister() {
		return assister;
	}

	public void setAssister(Assister assister) {
		this.assister = assister;
	}

	public AssisterLanguages getAssisterLanguages() {
		return assisterLanguages;
	}

	public void setAssisterLanguages(AssisterLanguages assisterLanguages) {
		this.assisterLanguages = assisterLanguages;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStartRecord() {
		return startRecord;
	}

	public void setStartRecord(Integer startRecord) {
		this.startRecord = startRecord;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getStatusInactive() {
		return statusInactive;
	}

	public void setStatusInactive(String statusInactive) {
		this.statusInactive = statusInactive;
	}

	public String getStatusactive() {
		return statusactive;
	}

	public void setStatusactive(String statusactive) {
		this.statusactive = statusactive;
	}

	public Map<String, Object> getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(Map<String, Object> searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public Status getDesigStatus() {
		return desigStatus;
	}

	public void setDesigStatus(Status pending) {
		this.desigStatus = pending;
	}

	public DesignateAssister getDesignateAssister() {
		return designateAssister;
	}

	public void setDesignateAssister(DesignateAssister designateAssister) {
		this.designateAssister = designateAssister;
	}
	
	@Override
	public String toString() {
		return "AssisterRequestDTO details: " + "ID = "+id+", UserId = "+userId+", ModuleId = "+moduleId+", IndividualId = "+individualId+ 
				", EnrollmentEntityId = "+enrollmentEntityId+", DesigStatus = "+
				desigStatus+", Assister = ["+assister+"], DesignateAssister = ["+designateAssister+"]";
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}


}
