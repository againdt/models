package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.getinsured.hix.model.DateAdapter;

@XmlAccessorType(XmlAccessType.NONE)
public class HealthCoverage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="householdOrEmployeeCaseID")
	private String householdOrEmployeeCaseID;

	@XmlElement(name="classOfContractCode")
	private String classOfContractCode;
	
	@XmlElement(name="benefitEffectiveBeginDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date effectiveStartDate;
	
	@XmlElement(name="benefitEffectiveEndDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date effectiveEndDate;
	
	@XmlElement(name="lastPremiumPaidDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date lastPremiumPaidDate;
	
	@XmlElement(name="healthCoveragePolicyNo")
	private String healthCoveragePolicyNo;
	
	@XmlElement(name="employerGroupNo")
	private String employerGroupNo;
	
	
	@XmlElement(name="exchgAssignedPolicyID")
	private String exchgAssignedPolicyID;
	
	@XmlElement(name="premiumPaidToDateEnd")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date premiumPaidToDateEnd;
	
	public String getHouseholdOrEmployeeCaseID() {
		return householdOrEmployeeCaseID;
	}

	public void setHouseholdOrEmployeeCaseID(String householdOrEmployeeCaseID) {
		this.householdOrEmployeeCaseID = householdOrEmployeeCaseID;
	}

	public String getClassOfContractCode() {
		return classOfContractCode;
	}

	public void setClassOfContractCode(String classOfContractCode) {
		this.classOfContractCode = classOfContractCode;
	}
	
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	
	public Date getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}

	public void setLastPremiumPaidDate(Date lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}

	public String getEmployerGroupNo() {
		return employerGroupNo;
	}

	public void setEmployerGroupNo(String employerGroupNo) {
		this.employerGroupNo = employerGroupNo;
	}

	public String getHealthCoveragePolicyNo() {
		return healthCoveragePolicyNo;
	}

	public void setHealthCoveragePolicyNo(String healthCoveragePolicyNo) {
		this.healthCoveragePolicyNo = healthCoveragePolicyNo;
	}

	public String getExchgAssignedPolicyID() {
		return exchgAssignedPolicyID;
	}

	public void setExchgAssignedPolicyID(String exchgAssignedPolicyID) {
		this.exchgAssignedPolicyID = exchgAssignedPolicyID;
	}

	public Date getPremiumPaidToDateEnd() {
		return premiumPaidToDateEnd;
	}

	public void setPremiumPaidToDateEnd(Date premiumPaidToDateEnd) {
		this.premiumPaidToDateEnd = premiumPaidToDateEnd;
	}
}