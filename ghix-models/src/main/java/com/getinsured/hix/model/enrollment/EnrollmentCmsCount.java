package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
/**
 * 
 * @author meher_a
 *
 */
public class EnrollmentCmsCount implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String cmsOutFileName;
	private String fileProcessingStatus;
	private Integer errorWarnCount;
	
	public String getCmsOutFileName() {
		return cmsOutFileName;
	}
	public void setCmsOutFileName(String cmsOutFileName) {
		this.cmsOutFileName = cmsOutFileName;
	}
	public String getFileProcessingStatus() {
		return fileProcessingStatus;
	}
	public void setFileProcessingStatus(String fileProcessingStatus) {
		this.fileProcessingStatus = fileProcessingStatus;
	}
	public Integer getErrorWarnCount() {
		return errorWarnCount;
	}
	public void setErrorWarnCount(Integer errorWarnCount) {
		this.errorWarnCount = errorWarnCount;
	}
}
