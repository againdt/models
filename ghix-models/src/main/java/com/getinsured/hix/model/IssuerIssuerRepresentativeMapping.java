package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name="pm_issuer_issuerrep")
public class IssuerIssuerRepresentativeMapping {
	
	public enum PrimaryContact { 
		YES, NO; 
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_issuer_issuerrep_seq")
	@SequenceGenerator(name = "pm_issuer_issuerrep_seq", sequenceName = "PM_ISSUER_ISSUERREP_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="issuer_id")
	private Issuer issuer;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name="issuer_rep_id")
	private IssuerRepresentative issuerRepresentative;
	
	@NotAudited
	@Column(name="primary_contact", length=3)
	@Enumerated(EnumType.STRING)
	private PrimaryContact primaryContact;
		
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;

	@Column(name = "last_update_by")
	private Integer lastUpdateBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date lastUpdateTimeStamp;
	
	@Column(name="hios_issuer_id")
	private String hiosIssuerId;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimeStamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimeStamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public IssuerRepresentative getIssuerRepresentative() {
		return issuerRepresentative;
	}

	public void setIssuerRepresentative(IssuerRepresentative issuerRepresentative) {
		this.issuerRepresentative = issuerRepresentative;
	}

	public PrimaryContact getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(PrimaryContact primaryContact) {
		this.primaryContact = primaryContact;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Integer lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	
	
}
