package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * The persistent class for the roles database table.
 */
@Audited
@Entity
@Table(name = "pm_urrt")
@XmlAccessorType(XmlAccessType.NONE)
public class UnifiedRate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_urrt_seq")
	@SequenceGenerator(name = "pm_urrt_seq", sequenceName = "pm_urrt_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "issuer_id")
	private Issuer issuer;
		
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "unifiedRate", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<URRTAllowedClaim> allowedClaims;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "unifiedRate", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<URRTProductRateChange> productRateChangeList;	
		
	@Column(name = "applicable_year")
	private int applicableYear;
	
	@Column(name = "state")
	private String state;	
	
	@Column(name = "exp_prd_begin_date")
	private Date experiencePeriodBeginingDate;
	
	@Column(name = "priep_amt")
	private long PremiumsNetOfMlrRebateInExperiencePeriodAmount;
	
	@Column(name = "icie_prd_amt")
	private long incurredClaimsAmount;
	
	@Column(name = "allowed_claims_amt")
	private long allowedClaimsAmount;
	
	@Column(name = "epir_pmpm_qty")
	private double expPeriodIndexRatePMPMQuantity;
		
	@Column(name = "prj_prd_startdate")
	private Date projectionPeriodStartDate;
	
     @Column(name = "paec_pmpm_cred_ind_qty") 
     private double projectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity;
     
     @Column(name = "pamc_pmpm_cred_ind_qty") 
     private double projectedAllowedManualClaimsPMPMCredibilityIndexQuantity;
     
     @Column(name = "paec_pmpm_amt") 
     private double projectedAdjustedExperienceClaimsPMPMAmount;
     
     @Column(name = "paec_amt") 
     private double projectedAllowedExperienceClaimsAmount;
     
     @Column(name = "ptaaf_in_prj_prd_qty") 
     private double paidToAllowedAverageFactorInProjectionPeriodQuantity;
     
     @Column(name = "pic_bef_adj_amt") 
     private double projectedIncurredClaimsBeforeAdjustmentAmount;
     
     @Column(name = "pra_pmpm_amt") 
     private double projectedRiskAdjustmentsPMPMAmount;
     
     @Column(name = "pacarr_pmpm_amt") 
     private double projectedACAReinsuranceRecoveriesPMPMAmount;
     
     @Column(name = "ael_qty") 
     private double administrativeExpenseLoadQuantity;
     
     @Column(name = "ael_pmpm_amt") 
     private double administrativeExpenseLoadPMPMAmount;
     
     @Column(name = "par_load_qty") 
     private double profitAndRiskLoadQuantity;
     
     @Column(name = "par_load_pmpm_amt") 
     private double profitAndRiskLoadPMPMAmount;
     
     @Column(name = "taf_pcnt_qty") 
     private double taxesAndFeesPercentQuantity;
     
     @Column(name = "taf_pmpm_amt") 
     private double taxesAndFeesPMPMAmount;
     
     @Column(name = "srpgpar_pmpm_amt") 
     private double singleRiskPoolGrossPremiumAvgRatePMPMAmount;
     
     @Column(name = "irfpp_amt") 
     private double indexRateForProjectionPeriodAmount;
     
     @Column(name = "pi_ovr_exp_prd_qty") 
     private double percentIncreaseOverExperiencePeriodQuantity;
     
     @Column(name = "pmm_qty")
     private int projectedMemberMonthsQuantity;
     		
     @Column(name = "piml_type")
     private String poolingInsuranceMarketLevelType;
     
     @Column(name = "exp_prd_end_date")
     private Date experiencePeriodEndingDate;
     
     @Column(name = "rr_eff_date")
     private Date rateReviewEffectiveDate;
     		
     @Column(name = "priep_pmpm_amt") 
     private double premiumsNetOfMlrRebateInExperiencePeriodPMPMAmount;
     
     @Column(name = "priep_qty") 
     private double premiumsNetOfMlrRebateInExperiencePeriodQuantity;
     
     @Column(name = "iciep_pmpm_amt") 
     private double incurredClaimsInExperiencePeriodPMPMAmount;
     
     @Column(name = "iciep_qty") 
     private double incurredClaimsInExperiencePeriodQuantity;
     
     @Column(name = "ac_pmpm_amt") 
     private double allowedClaimsPMPMAmount;
     
     @Column(name = "ac_pcnt_qty") 
     private double allowedClaimsPercentQuantity;
     
     @Column(name = "epmm_qty")
     private int experiencePeriodMemberMonthsQuantity;
     
     @Column(name = "prj_prd_enddate")
     private Date projectionPeriodEndDate;
     
     @Column(name = "mptmp_mths_qty")
     private int midpointToMidpointMonthsQuantity;
     		
     @Column(name = "tepoaea_pmpm_amt") 
     private double totalExperiencePeriodOnActualExperienceAllowedPMPMAmount;
     
     @Column(name = "tpbca_pmpm_amt") 
     private double totalProjectionsBeforeCredibilityAdjustmentPMPMAmount;
     
     @Column(name = "tcm_pmpm_amt") 
     private double totalCredibilityManualPMPMAmount;
     
     @Column(name = "pic_pmpm_amt") 
     private double projectedIncurredClaimsPMPMAmount;
     
     @Column(name = "srpgpar_pmpm_t_amt") 
     private double singleRiskPoolGrossPremiumAvgRatePMPMTotalAmount;
     
     @Column(name = "taf_pmpm_t_amt") 
     private double taxesAndFeesPMPMTotalAmount;
     
     @Column(name = "parl_pmpm_t_amt") 
     private double profitAndRiskLoadPMPMTotalAmount;
     
     @Column(name = "ael_pmpm_t_amt") 
     private double administrativeExpenseLoadPMPMTotalAmount;
     
     @Column(name = "picba_pmpm_t_amt") 
     private double projectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount;
     
     @Column(name = "pic_pmpm_t_amt") 
     private double projectedIncurredClaimsPMPMTotalAmount;
     
     @Column(name = "pra_pmpm_t_amt") 
     private double projectedRiskAdjustmentsPMPMTotalAmount;
     
     @Column(name = "pacarr_pmpm_t_amt") 
     private double projectedACAReinsuranceRecoveriesPMPMTotalAmount;
     
     @Column(name = "pcnt_inc_ann_qty") 
     private double percentIncreaseAnnualizedQuantity;
     
     @Column(name = "picbrr_pmpm_amt") 
     private double projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount;
     
     @Column(name = "picbrr_pmpm_t_amt") 
     private double projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount;
     
     @Column(name = "is_deleted")
     private String isDeleted;
     
 	 @Temporal(value = TemporalType.TIMESTAMP)
 	 @Column(name = "created_timestamp", nullable = false)
 	 private Date creationTimestamp;

 	 @Temporal(value = TemporalType.TIMESTAMP)
 	 @Column(name = "last_updated_timestamp", nullable = false)
 	 private Date lastUpdateTimestamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public int getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(int applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	public long getIncurredClaimsAmount() {
		return incurredClaimsAmount;
	}

	public void setIncurredClaimsAmount(long incurredClaimsAmount) {
		this.incurredClaimsAmount = incurredClaimsAmount;
	}

	public double getExpPeriodIndexRatePMPMQuantity() {
		return expPeriodIndexRatePMPMQuantity;
	}

	public void setExpPeriodIndexRatePMPMQuantity(
			double expPeriodIndexRatePMPMQuantity) {
		this.expPeriodIndexRatePMPMQuantity = expPeriodIndexRatePMPMQuantity;
	}

	public Date getProjectionPeriodStartDate() {
		return projectionPeriodStartDate;
	}

	public void setProjectionPeriodStartDate(Date projectionPeriodStartDate) {
		this.projectionPeriodStartDate = projectionPeriodStartDate;
	}

	public double getProjectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity() {
		return projectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity;
	}

	public void setProjectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity(
			double projectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity) {
		this.projectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity = projectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity;
	}

	public double getProjectedAllowedManualClaimsPMPMCredibilityIndexQuantity() {
		return projectedAllowedManualClaimsPMPMCredibilityIndexQuantity;
	}

	public void setProjectedAllowedManualClaimsPMPMCredibilityIndexQuantity(
			double projectedAllowedManualClaimsPMPMCredibilityIndexQuantity) {
		this.projectedAllowedManualClaimsPMPMCredibilityIndexQuantity = projectedAllowedManualClaimsPMPMCredibilityIndexQuantity;
	}

	public double getProjectedAdjustedExperienceClaimsPMPMAmount() {
		return projectedAdjustedExperienceClaimsPMPMAmount;
	}

	public void setProjectedAdjustedExperienceClaimsPMPMAmount(
			double projectedAdjustedExperienceClaimsPMPMAmount) {
		this.projectedAdjustedExperienceClaimsPMPMAmount = projectedAdjustedExperienceClaimsPMPMAmount;
	}

	public double getProjectedAllowedExperienceClaimsAmount() {
		return projectedAllowedExperienceClaimsAmount;
	}

	public void setProjectedAllowedExperienceClaimsAmount(
			double projectedAllowedExperienceClaimsAmount) {
		this.projectedAllowedExperienceClaimsAmount = projectedAllowedExperienceClaimsAmount;
	}

	public double getPaidToAllowedAverageFactorInProjectionPeriodQuantity() {
		return paidToAllowedAverageFactorInProjectionPeriodQuantity;
	}

	public void setPaidToAllowedAverageFactorInProjectionPeriodQuantity(
			double paidToAllowedAverageFactorInProjectionPeriodQuantity) {
		this.paidToAllowedAverageFactorInProjectionPeriodQuantity = paidToAllowedAverageFactorInProjectionPeriodQuantity;
	}

	public double getProjectedIncurredClaimsBeforeAdjustmentAmount() {
		return projectedIncurredClaimsBeforeAdjustmentAmount;
	}

	public void setProjectedIncurredClaimsBeforeAdjustmentAmount(
			double projectedIncurredClaimsBeforeAdjustmentAmount) {
		this.projectedIncurredClaimsBeforeAdjustmentAmount = projectedIncurredClaimsBeforeAdjustmentAmount;
	}

	public double getProjectedRiskAdjustmentsPMPMAmount() {
		return projectedRiskAdjustmentsPMPMAmount;
	}

	public void setProjectedRiskAdjustmentsPMPMAmount(
			double projectedRiskAdjustmentsPMPMAmount) {
		this.projectedRiskAdjustmentsPMPMAmount = projectedRiskAdjustmentsPMPMAmount;
	}

	public double getProjectedACAReinsuranceRecoveriesPMPMAmount() {
		return projectedACAReinsuranceRecoveriesPMPMAmount;
	}

	public void setProjectedACAReinsuranceRecoveriesPMPMAmount(
			double projectedACAReinsuranceRecoveriesPMPMAmount) {
		this.projectedACAReinsuranceRecoveriesPMPMAmount = projectedACAReinsuranceRecoveriesPMPMAmount;
	}

	public double getAdministrativeExpenseLoadQuantity() {
		return administrativeExpenseLoadQuantity;
	}

	public void setAdministrativeExpenseLoadQuantity(
			double administrativeExpenseLoadQuantity) {
		this.administrativeExpenseLoadQuantity = administrativeExpenseLoadQuantity;
	}

	public double getAdministrativeExpenseLoadPMPMAmount() {
		return administrativeExpenseLoadPMPMAmount;
	}

	public void setAdministrativeExpenseLoadPMPMAmount(
			double administrativeExpenseLoadPMPMAmount) {
		this.administrativeExpenseLoadPMPMAmount = administrativeExpenseLoadPMPMAmount;
	}

	public double getProfitAndRiskLoadQuantity() {
		return profitAndRiskLoadQuantity;
	}

	public void setProfitAndRiskLoadQuantity(double profitAndRiskLoadQuantity) {
		this.profitAndRiskLoadQuantity = profitAndRiskLoadQuantity;
	}

	public double getProfitAndRiskLoadPMPMAmount() {
		return profitAndRiskLoadPMPMAmount;
	}

	public void setProfitAndRiskLoadPMPMAmount(double profitAndRiskLoadPMPMAmount) {
		this.profitAndRiskLoadPMPMAmount = profitAndRiskLoadPMPMAmount;
	}

	public double getTaxesAndFeesPercentQuantity() {
		return taxesAndFeesPercentQuantity;
	}

	public void setTaxesAndFeesPercentQuantity(double taxesAndFeesPercentQuantity) {
		this.taxesAndFeesPercentQuantity = taxesAndFeesPercentQuantity;
	}

	public double getTaxesAndFeesPMPMAmount() {
		return taxesAndFeesPMPMAmount;
	}

	public void setTaxesAndFeesPMPMAmount(double taxesAndFeesPMPMAmount) {
		this.taxesAndFeesPMPMAmount = taxesAndFeesPMPMAmount;
	}

	public double getSingleRiskPoolGrossPremiumAvgRatePMPMAmount() {
		return singleRiskPoolGrossPremiumAvgRatePMPMAmount;
	}

	public void setSingleRiskPoolGrossPremiumAvgRatePMPMAmount(
			double singleRiskPoolGrossPremiumAvgRatePMPMAmount) {
		this.singleRiskPoolGrossPremiumAvgRatePMPMAmount = singleRiskPoolGrossPremiumAvgRatePMPMAmount;
	}

	public double getIndexRateForProjectionPeriodAmount() {
		return indexRateForProjectionPeriodAmount;
	}

	public void setIndexRateForProjectionPeriodAmount(
			double indexRateForProjectionPeriodAmount) {
		this.indexRateForProjectionPeriodAmount = indexRateForProjectionPeriodAmount;
	}

	public double getPercentIncreaseOverExperiencePeriodQuantity() {
		return percentIncreaseOverExperiencePeriodQuantity;
	}

	public void setPercentIncreaseOverExperiencePeriodQuantity(
			double percentIncreaseOverExperiencePeriodQuantity) {
		this.percentIncreaseOverExperiencePeriodQuantity = percentIncreaseOverExperiencePeriodQuantity;
	}

	public String getPoolingInsuranceMarketLevelType() {
		return poolingInsuranceMarketLevelType;
	}

	public void setPoolingInsuranceMarketLevelType(
			String poolingInsuranceMarketLevelType) {
		this.poolingInsuranceMarketLevelType = poolingInsuranceMarketLevelType;
	}

	public Date getExperiencePeriodBeginingDate() {
		return experiencePeriodBeginingDate;
	}

	public void setExperiencePeriodBeginingDate(Date experiencePeriodBeginingDate) {
		this.experiencePeriodBeginingDate = experiencePeriodBeginingDate;
	}

	public Date getExperiencePeriodEndingDate() {
		return experiencePeriodEndingDate;
	}

	public void setExperiencePeriodEndingDate(Date experiencePeriodEndingDate) {
		this.experiencePeriodEndingDate = experiencePeriodEndingDate;
	}

	public Date getRateReviewEffectiveDate() {
		return rateReviewEffectiveDate;
	}

	public void setRateReviewEffectiveDate(Date rateReviewEffectiveDate) {
		this.rateReviewEffectiveDate = rateReviewEffectiveDate;
	}

	public double getPremiumsNetOfMlrRebateInExperiencePeriodPMPMAmount() {
		return premiumsNetOfMlrRebateInExperiencePeriodPMPMAmount;
	}

	public void setPremiumsNetOfMlrRebateInExperiencePeriodPMPMAmount(
			double premiumsNetOfMlrRebateInExperiencePeriodPMPMAmount) {
		this.premiumsNetOfMlrRebateInExperiencePeriodPMPMAmount = premiumsNetOfMlrRebateInExperiencePeriodPMPMAmount;
	}

	public double getPremiumsNetOfMlrRebateInExperiencePeriodQuantity() {
		return premiumsNetOfMlrRebateInExperiencePeriodQuantity;
	}

	public void setPremiumsNetOfMlrRebateInExperiencePeriodQuantity(
			double premiumsNetOfMlrRebateInExperiencePeriodQuantity) {
		this.premiumsNetOfMlrRebateInExperiencePeriodQuantity = premiumsNetOfMlrRebateInExperiencePeriodQuantity;
	}

	public double getIncurredClaimsInExperiencePeriodPMPMAmount() {
		return incurredClaimsInExperiencePeriodPMPMAmount;
	}

	public void setIncurredClaimsInExperiencePeriodPMPMAmount(
			double incurredClaimsInExperiencePeriodPMPMAmount) {
		this.incurredClaimsInExperiencePeriodPMPMAmount = incurredClaimsInExperiencePeriodPMPMAmount;
	}

	public double getIncurredClaimsInExperiencePeriodQuantity() {
		return incurredClaimsInExperiencePeriodQuantity;
	}

	public void setIncurredClaimsInExperiencePeriodQuantity(
			double incurredClaimsInExperiencePeriodQuantity) {
		this.incurredClaimsInExperiencePeriodQuantity = incurredClaimsInExperiencePeriodQuantity;
	}

	public double getAllowedClaimsPMPMAmount() {
		return allowedClaimsPMPMAmount;
	}

	public void setAllowedClaimsPMPMAmount(double allowedClaimsPMPMAmount) {
		this.allowedClaimsPMPMAmount = allowedClaimsPMPMAmount;
	}

	public double getAllowedClaimsPercentQuantity() {
		return allowedClaimsPercentQuantity;
	}

	public void setAllowedClaimsPercentQuantity(double allowedClaimsPercentQuantity) {
		this.allowedClaimsPercentQuantity = allowedClaimsPercentQuantity;
	}
		
	public Date getProjectionPeriodEndDate() {
		return projectionPeriodEndDate;
	}

	public void setProjectionPeriodEndDate(Date projectionPeriodEndDate) {
		this.projectionPeriodEndDate = projectionPeriodEndDate;
	}

	public double getTotalExperiencePeriodOnActualExperienceAllowedPMPMAmount() {
		return totalExperiencePeriodOnActualExperienceAllowedPMPMAmount;
	}

	public void setTotalExperiencePeriodOnActualExperienceAllowedPMPMAmount(
			double totalExperiencePeriodOnActualExperienceAllowedPMPMAmount) {
		this.totalExperiencePeriodOnActualExperienceAllowedPMPMAmount = totalExperiencePeriodOnActualExperienceAllowedPMPMAmount;
	}

	public double getTotalProjectionsBeforeCredibilityAdjustmentPMPMAmount() {
		return totalProjectionsBeforeCredibilityAdjustmentPMPMAmount;
	}

	public void setTotalProjectionsBeforeCredibilityAdjustmentPMPMAmount(
			double totalProjectionsBeforeCredibilityAdjustmentPMPMAmount) {
		this.totalProjectionsBeforeCredibilityAdjustmentPMPMAmount = totalProjectionsBeforeCredibilityAdjustmentPMPMAmount;
	}

	public double getTotalCredibilityManualPMPMAmount() {
		return totalCredibilityManualPMPMAmount;
	}

	public void setTotalCredibilityManualPMPMAmount(
			double totalCredibilityManualPMPMAmount) {
		this.totalCredibilityManualPMPMAmount = totalCredibilityManualPMPMAmount;
	}

	public double getProjectedIncurredClaimsPMPMAmount() {
		return projectedIncurredClaimsPMPMAmount;
	}

	public void setProjectedIncurredClaimsPMPMAmount(
			double projectedIncurredClaimsPMPMAmount) {
		this.projectedIncurredClaimsPMPMAmount = projectedIncurredClaimsPMPMAmount;
	}

	public double getSingleRiskPoolGrossPremiumAvgRatePMPMTotalAmount() {
		return singleRiskPoolGrossPremiumAvgRatePMPMTotalAmount;
	}

	public void setSingleRiskPoolGrossPremiumAvgRatePMPMTotalAmount(
			double singleRiskPoolGrossPremiumAvgRatePMPMTotalAmount) {
		this.singleRiskPoolGrossPremiumAvgRatePMPMTotalAmount = singleRiskPoolGrossPremiumAvgRatePMPMTotalAmount;
	}

	public double getTaxesAndFeesPMPMTotalAmount() {
		return taxesAndFeesPMPMTotalAmount;
	}

	public void setTaxesAndFeesPMPMTotalAmount(double taxesAndFeesPMPMTotalAmount) {
		this.taxesAndFeesPMPMTotalAmount = taxesAndFeesPMPMTotalAmount;
	}

	public double getProfitAndRiskLoadPMPMTotalAmount() {
		return profitAndRiskLoadPMPMTotalAmount;
	}

	public void setProfitAndRiskLoadPMPMTotalAmount(
			double profitAndRiskLoadPMPMTotalAmount) {
		this.profitAndRiskLoadPMPMTotalAmount = profitAndRiskLoadPMPMTotalAmount;
	}

	public double getAdministrativeExpenseLoadPMPMTotalAmount() {
		return administrativeExpenseLoadPMPMTotalAmount;
	}

	public void setAdministrativeExpenseLoadPMPMTotalAmount(
			double administrativeExpenseLoadPMPMTotalAmount) {
		this.administrativeExpenseLoadPMPMTotalAmount = administrativeExpenseLoadPMPMTotalAmount;
	}

	public double getProjectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount() {
		return projectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount;
	}

	public void setProjectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount(
			double projectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount) {
		this.projectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount = projectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount;
	}

	public double getProjectedIncurredClaimsPMPMTotalAmount() {
		return projectedIncurredClaimsPMPMTotalAmount;
	}

	public void setProjectedIncurredClaimsPMPMTotalAmount(
			double projectedIncurredClaimsPMPMTotalAmount) {
		this.projectedIncurredClaimsPMPMTotalAmount = projectedIncurredClaimsPMPMTotalAmount;
	}

	public double getProjectedRiskAdjustmentsPMPMTotalAmount() {
		return projectedRiskAdjustmentsPMPMTotalAmount;
	}

	public void setProjectedRiskAdjustmentsPMPMTotalAmount(
			double projectedRiskAdjustmentsPMPMTotalAmount) {
		this.projectedRiskAdjustmentsPMPMTotalAmount = projectedRiskAdjustmentsPMPMTotalAmount;
	}

	public double getProjectedACAReinsuranceRecoveriesPMPMTotalAmount() {
		return projectedACAReinsuranceRecoveriesPMPMTotalAmount;
	}

	public void setProjectedACAReinsuranceRecoveriesPMPMTotalAmount(
			double projectedACAReinsuranceRecoveriesPMPMTotalAmount) {
		this.projectedACAReinsuranceRecoveriesPMPMTotalAmount = projectedACAReinsuranceRecoveriesPMPMTotalAmount;
	}

	public double getPercentIncreaseAnnualizedQuantity() {
		return percentIncreaseAnnualizedQuantity;
	}

	public void setPercentIncreaseAnnualizedQuantity(
			double percentIncreaseAnnualizedQuantity) {
		this.percentIncreaseAnnualizedQuantity = percentIncreaseAnnualizedQuantity;
	}

	public double getProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount() {
		return projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount;
	}

	public void setProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount(
			double projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount) {
		this.projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount = projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount;
	}

	public double getProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount() {
		return projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount;
	}

	public void setProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount(
			double projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount) {
		this.projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount = projectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public List<URRTAllowedClaim> getAllowedClaims() {
		return allowedClaims;
	}

	public void setAllowedClaims(List<URRTAllowedClaim> allowedClaims) {
		this.allowedClaims = allowedClaims;
	}

	public List<URRTProductRateChange> getProductRateChangeList() {
		return productRateChangeList;
	}

	public void setProductRateChangeList(
			List<URRTProductRateChange> productRateChangeList) {
		this.productRateChangeList = productRateChangeList;
	}

	public long getPremiumsNetOfMlrRebateInExperiencePeriodAmount() {
		return PremiumsNetOfMlrRebateInExperiencePeriodAmount;
	}

	public void setPremiumsNetOfMlrRebateInExperiencePeriodAmount(
			long premiumsNetOfMlrRebateInExperiencePeriodAmount) {
		PremiumsNetOfMlrRebateInExperiencePeriodAmount = premiumsNetOfMlrRebateInExperiencePeriodAmount;
	}

	public long getAllowedClaimsAmount() {
		return allowedClaimsAmount;
	}

	public void setAllowedClaimsAmount(long allowedClaimsAmount) {
		this.allowedClaimsAmount = allowedClaimsAmount;
	}

	public int getProjectedMemberMonthsQuantity() {
		return projectedMemberMonthsQuantity;
	}

	public void setProjectedMemberMonthsQuantity(int projectedMemberMonthsQuantity) {
		this.projectedMemberMonthsQuantity = projectedMemberMonthsQuantity;
	}

	public int getExperiencePeriodMemberMonthsQuantity() {
		return experiencePeriodMemberMonthsQuantity;
	}

	public void setExperiencePeriodMemberMonthsQuantity(
			int experiencePeriodMemberMonthsQuantity) {
		this.experiencePeriodMemberMonthsQuantity = experiencePeriodMemberMonthsQuantity;
	}

	public int getMidpointToMidpointMonthsQuantity() {
		return midpointToMidpointMonthsQuantity;
	}

	public void setMidpointToMidpointMonthsQuantity(
			int midpointToMidpointMonthsQuantity) {
		this.midpointToMidpointMonthsQuantity = midpointToMidpointMonthsQuantity;
	}
}
