package com.getinsured.hix.model.directenrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.NotAudited;

import com.getinsured.hix.dto.directenrollment.DirectEnrollmentStatus;

	@Entity
	@Table(name="D2C_ENROLLMENT")
	public class DirectEnrollmentStatistic implements Serializable {

		private static final long serialVersionUID = 2453213624867956087L;

		@Id
		@Column(name = "ID")
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "D2C_ENROLLMENT_SEQ")
		@SequenceGenerator(name = "D2C_ENROLLMENT_SEQ", sequenceName = "D2C_ENROLLMENT_SEQ", allocationSize = 1)
		private Long id;

		@NotAudited
		@Column(name="ISSUER_ID")
		private Long issuerId;

		@NotAudited
		@Column(name="ISSUER_BRAND_NAME")
		private String issuerBrandName;

		@NotAudited
		@Column(name="STATE")
		private String state;

		@NotAudited
		@Column(name="EFFECTIVE_DATE")
		private Date effectiveDate;

		@NotAudited
		@Column(name="CMR_HOUSEHOLD_ID")
		private Long cmrHouseholdId;


		@Column(name="ISSUER_STATUS")
		@Enumerated(EnumType.STRING)
		private DirectEnrollmentStatus.ApplicationStatus issuerStatus;

		@NotAudited
		@Column(name="ACTIVE")
		private Boolean active;

		@NotAudited
		@Column(name="PAGE_LAST_VISITED")
		private String pageLastVisited;

		@NotAudited
		@Column(name="D2C_ENROLLMENT_META_ID")
		private Long enrollmentMetaId;

		@NotAudited
		@Column(name="ISSUER_APPLICATION_ID")
		private String issuerApplicationId;

		@NotAudited
		@Column(name="GI_APPLICATION_ID")
		private String giApplicationId;
		
		@Column(name="GI_STATUS")
		@Enumerated(EnumType.STRING)
		private DirectEnrollmentStatus.ApplicationStatus giStatus;

		@NotAudited
		@Column(name="DATA")
		private String data;

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="LAST_UPDATED_TIMESTAMP")
		private Date lastUpdatedTimestamp;

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="CREATION_TIMESTAMP")
		private Date creationTimestamp;

		@NotAudited
		@Column(name="YEAR")
		private String year;

		@NotAudited
		@Column(name="PLAN_ID")
		private Long planId;

		@NotAudited
		@Column(name="PLAN_NAME")
		private String planName;

		@NotAudited
		@Column(name="PREMIUM")
		private Float premium;

		@NotAudited
		@Column(name="ENROLLMENT_ID")
		private Integer enrollmentId;

		@NotAudited
		@Column(name="SSAP_APPLICATION_ID")
		private Long ssapApplicationId;
		
		@NotAudited
		@Column(name="TENANT_ID")
		private Long tenantId;

		@NotAudited
		@Column(name="FLOW_TYPE")
		private String flowType;

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getIssuerId() {
			return issuerId;
		}
		public void setIssuerId(Long issuerId) {
			this.issuerId = issuerId;
		}
		public String getIssuerBrandName() {
			return issuerBrandName;
		}
		public void setIssuerBrandName(String issuerBrandName) {
			this.issuerBrandName = issuerBrandName;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public Date getEffectiveDate() {
			return effectiveDate;
		}
		public void setEffectiveDate(Date effectiveDate) {
			this.effectiveDate = effectiveDate;
		}
		public Long getCmrHouseholdId() {
			return cmrHouseholdId;
		}
		public void setCmrHouseholdId(Long cmrHouseholdId) {
			this.cmrHouseholdId = cmrHouseholdId;
		}
		public DirectEnrollmentStatus.ApplicationStatus getIssuerStatus() {
			return issuerStatus;
		}
		public void setIssuerStatus(
				DirectEnrollmentStatus.ApplicationStatus issuerStatus) {
			this.issuerStatus = issuerStatus;
		}
		public Boolean getActive() {
			return active;
		}
		public void setActive(Boolean active) {
			this.active = active;
		}
		public String getPageLastVisited() {
			return pageLastVisited;
		}
		public void setPageLastVisited(String pageLastVisited) {
			this.pageLastVisited = pageLastVisited;
		}
		public Long getEnrollmentMetaId() {
			return enrollmentMetaId;
		}
		public void setEnrollmentMetaId(Long enrollmentMetaId) {
			this.enrollmentMetaId = enrollmentMetaId;
		}
		public String getIssuerApplicationId() {
			return issuerApplicationId;
		}
		public void setIssuerApplicationId(String issuerApplicationId) {
			this.issuerApplicationId = issuerApplicationId;
		}
		public String getGiApplicationId() {
			return giApplicationId;
		}
		public void setGiApplicationId(String giApplicationId) {
			this.giApplicationId = giApplicationId;
		}
		public DirectEnrollmentStatus.ApplicationStatus getGiStatus() {
			return giStatus;
		}
		public void setGiStatus(DirectEnrollmentStatus.ApplicationStatus giStatus) {
			this.giStatus = giStatus;
		}
		public String getData() {
			return data;
		}
		public void setData(String data) {
			this.data = data;
		}
		public Date getLastUpdatedTimestamp() {
			return lastUpdatedTimestamp;
		}
		public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
			this.lastUpdatedTimestamp = lastUpdatedTimestamp;
		}
		public Date getCreationTimestamp() {
			return creationTimestamp;
		}
		public void setCreationTimestamp(Date creationTimestamp) {
			this.creationTimestamp = creationTimestamp;
		}
		public String getYear() {
			return year;
		}
		public void setYear(String year) {
			this.year = year;
		}
		public Long getPlanId() {
			return planId;
		}
		public void setPlanId(Long planId) {
			this.planId = planId;
		}
		public String getPlanName() {
			return planName;
		}
		public void setPlanName(String planName) {
			this.planName = planName;
		}
		public Float getPremium() {
			return premium;
		}
		public void setPremium(Float premium) {
			this.premium = premium;
		}

		public Integer getEnrollmentId() {
			return enrollmentId;
		}
		public void setEnrollmentId(Integer enrollmentId) {
			this.enrollmentId = enrollmentId;
		}
		public Long getSsapApplicationId() {
			return ssapApplicationId;
		}
		public void setSsapApplicationId(Long ssapApplicationId) {
			this.ssapApplicationId = ssapApplicationId;
		}
		@PrePersist
		public void prePersist(){
			this.setCreationTimestamp(new TSDate());
			this.setLastUpdatedTimestamp(new TSDate());
		}

		@PreUpdate
		public void preUpdate(){
			this.setLastUpdatedTimestamp(new TSDate());
		}
		/**
		 * @return the tenantId
		 */
		public Long getTenantId() {
			return tenantId;
		}
		/**
		 * @param tenantId the tenantId to set
		 */
		public void setTenantId(Long tenantId) {
			this.tenantId = tenantId;
		}
		/**
		 * @return the flowType
		 */
		public String getFlowType() {
			return flowType;
		}
		/**
		 * @param flowType the flowType to set
		 */
		public void setFlowType(String flowType) {
			this.flowType = flowType;
		}
		public Set<DirectEnrollmentECM> getDocuments() {
			return null;
		}
		public void setDocuments(Set<DirectEnrollmentECM> documents) {
			
		}
		
	
		


	}
