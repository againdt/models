package com.getinsured.hix.model.planmgmt;

public class TeaserPlanRequest extends HouseHoldRequest{
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[");
		stringBuilder.append(super.toString());
		stringBuilder.append(", costSharingVariation=");
		stringBuilder.append(costSharingVariation);
		stringBuilder.append(" ]");
		return stringBuilder.toString();
	}

	private String costSharingVariation;

	public String getCostSharingVariation() {
		return costSharingVariation;
	}

	public void setCostSharingVariation(String costSharingVariation) {
		this.costSharingVariation = costSharingVariation;
	}

	
	
	
}
