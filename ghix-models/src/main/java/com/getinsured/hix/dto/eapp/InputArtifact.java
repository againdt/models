package com.getinsured.hix.dto.eapp;

/**
 * Represents a input artifact item
 * @author root
 *
 */
public interface InputArtifact {
	
	public String toString();
	
	public boolean validate();
	public Object parse();

}
