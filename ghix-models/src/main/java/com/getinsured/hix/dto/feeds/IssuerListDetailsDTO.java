package com.getinsured.hix.dto.feeds;

import java.util.List;

public class IssuerListDetailsDTO {
	
	// Request Details
	

	private String tenantCode;
	private String insuranceType;
	private String stateCode;
	private String status;
	private Integer errCode;
	private String errMsg;
	private List<IssuerInfoDTO> issuersList;

	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}


	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}


	public String getInsuranceType() {
		return insuranceType;
	}


	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}


	public String getStateCode() {
		return stateCode;
	}


	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the errCode
	 */
	public Integer getErrCode() {
		return errCode;
	}


	/**
	 * @param errCode the errCode to set
	 */
	public void setErrCode(Integer errCode) {
		this.errCode = errCode;
	}


	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}


	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}


	/**
	 * @return the issuersList
	 */
	public List<IssuerInfoDTO> getIssuersList() {
		return issuersList;
	}


	/**
	 * @param issuersList the issuersList to set
	 */
	public void setIssuersList(List<IssuerInfoDTO> issuersList) {
		this.issuersList = issuersList;
	}
	
	
	

}
