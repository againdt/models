package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

public class ZipCounty implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String zipCode;
	private String county;
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	

}
