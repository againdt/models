----------------------------------------------------------------------------------
------- Below Insert statements are permissions in INDIVIDUAL PORTAL.------------- 
----------------------------------------------------------------------------------
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_VIEW_AND_PRINT');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_START_APP');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_RESUME_APP');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_ENROLL_APP');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_EDIT_APP');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_REPORT_CHANGES');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_INIT_VERIFICATIONS');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_VIEW_VER_RESULTS');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_RUN_ELIGIBILITY');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_VIEW_ELIG_RESULTS');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_CONTACT_US');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_SUBMIT_APPEAL');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_DISENROLL');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_UPDATE_CARRIER');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_EDIT_SPCL_ENRLMNT_PERIOD');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_INBOX');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_ACCOUNT_SETTINGS');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_CHANGE_PASSWORD');
insert into permissions(ID, NAME) values(PERMISSIONS_SEQ.NEXTVAL,'PORTAL_CHANGE_SEC_QSTNS');