/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

import com.getinsured.hix.model.LookupValue;

/**
 * @author Pratap
 *
 */
public class EnrolleeOffExchangeDTO implements Serializable{

	/**
	 * 
	 */
	public EnrolleeOffExchangeDTO() {
		// TODO Auto-generated constructor stub
	}
	
	private Integer memberId;
	private String firstName;
	private String middleName;                     
	private String lastName;  
	private String genderCode;
	private Date dob;
	private String primaryPhone; 
	private String preferredEmail;  
	private String ssn; 
	private Boolean tobacco;
	private Boolean subscriberFlag;                 
	private Boolean disenrollSadpFlag;              
	private Boolean pldNewPersonFlag;               
	
	
	private Float individualPremium;              
	private LookupValue houseHoldContactRelationship;   
	private String ratingArea;                     
	private String catastrophicEligible;           
	private String maritalStatusCode;              
	private Integer existingQhpEnrollmentId;        
	private Boolean disenrollQhpFlag;               
	private Integer existingSadpEnrollmentId;       
	private String financialHardshipExemption;     
	private String childOnlyPlanEligibile;         
	private String maintenanceReasonCode;
	
	private String homeAddress1;
	private String homeAddress2;                   
	private String homeState;
	private String homeCity; 
	private String homeZip;     
	private String countyCode;
	private String county;
	
	
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGenderCode() {
		return genderCode;
	}
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getPrimaryPhone() {
		return primaryPhone;
	}
	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}
	public String getPreferredEmail() {
		return preferredEmail;
	}
	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public Boolean getTobacco() {
		return tobacco;
	}
	public void setTobacco(Boolean tobacco) {
		this.tobacco = tobacco;
	}
	public Boolean getSubscriberFlag() {
		return subscriberFlag;
	}
	public void setSubscriberFlag(Boolean subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}
	public Boolean getDisenrollSadpFlag() {
		return disenrollSadpFlag;
	}
	public void setDisenrollSadpFlag(Boolean disenrollSadpFlag) {
		this.disenrollSadpFlag = disenrollSadpFlag;
	}
	public Boolean getPldNewPersonFlag() {
		return pldNewPersonFlag;
	}
	public void setPldNewPersonFlag(Boolean pldNewPersonFlag) {
		this.pldNewPersonFlag = pldNewPersonFlag;
	}
	public Float getIndividualPremium() {
		return individualPremium;
	}
	public void setIndividualPremium(Float individualPremium) {
		this.individualPremium = individualPremium;
	}
	public LookupValue getHouseHoldContactRelationship() {
		return houseHoldContactRelationship;
	}
	public void setHouseHoldContactRelationship(LookupValue houseHoldContactRelationship) {
		this.houseHoldContactRelationship = houseHoldContactRelationship;
	}
	public String getRatingArea() {
		return ratingArea;
	}
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	public String getCatastrophicEligible() {
		return catastrophicEligible;
	}
	public void setCatastrophicEligible(String catastrophicEligible) {
		this.catastrophicEligible = catastrophicEligible;
	}
	public String getMaritalStatusCode() {
		return maritalStatusCode;
	}
	public void setMaritalStatusCode(String maritalStatusCode) {
		this.maritalStatusCode = maritalStatusCode;
	}
	public Integer getExistingQhpEnrollmentId() {
		return existingQhpEnrollmentId;
	}
	public void setExistingQhpEnrollmentId(Integer existingQhpEnrollmentId) {
		this.existingQhpEnrollmentId = existingQhpEnrollmentId;
	}
	public Boolean getDisenrollQhpFlag() {
		return disenrollQhpFlag;
	}
	public void setDisenrollQhpFlag(Boolean disenrollQhpFlag) {
		this.disenrollQhpFlag = disenrollQhpFlag;
	}
	public Integer getExistingSadpEnrollmentId() {
		return existingSadpEnrollmentId;
	}
	public void setExistingSadpEnrollmentId(Integer existingSadpEnrollmentId) {
		this.existingSadpEnrollmentId = existingSadpEnrollmentId;
	}
	public String getFinancialHardshipExemption() {
		return financialHardshipExemption;
	}
	public void setFinancialHardshipExemption(String financialHardshipExemption) {
		this.financialHardshipExemption = financialHardshipExemption;
	}
	public String getChildOnlyPlanEligibile() {
		return childOnlyPlanEligibile;
	}
	public void setChildOnlyPlanEligibile(String childOnlyPlanEligibile) {
		this.childOnlyPlanEligibile = childOnlyPlanEligibile;
	}
	public String getMaintenanceReasonCode() {
		return maintenanceReasonCode;
	}
	public void setMaintenanceReasonCode(String maintenanceReasonCode) {
		this.maintenanceReasonCode = maintenanceReasonCode;
	}
	public String getHomeAddress1() {
		return homeAddress1;
	}
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}
	public String getHomeAddress2() {
		return homeAddress2;
	}
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}
	public String getHomeState() {
		return homeState;
	}
	public void setHomeState(String homeState) {
		this.homeState = homeState;
	}
	public String getHomeCity() {
		return homeCity;
	}
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}
	public String getHomeZip() {
		return homeZip;
	}
	public void setHomeZip(String homeZip) {
		this.homeZip = homeZip;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}

}
