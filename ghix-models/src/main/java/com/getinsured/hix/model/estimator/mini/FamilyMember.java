/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

/**
 * Member model for the Medicaid Calculator of the mini-estimator
 * 
 * @author Nikhil Talreja
 * @since 08 July, 2013
 *
 */

public class FamilyMember {
	
	private String dateOfBirth;
	
	private int sequenceNumber;
	
	private boolean isPregnant=false;
	
	private boolean isSeekingCoverage = false;
	private String gender;
	
	
	private boolean isNativeAmerican = false;
	//Values = CHIP, Medicare, APTC. Default is NA (for complete household Medicaid case)
	private String memberEligibility = "NA";
	private String medicaidChild="N";
	private MemberRelationships relationshipToPrimary;
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public boolean isSeekingCoverage() {
		return isSeekingCoverage;
	}

	public void setSeekingCoverage(boolean isSeekingCoverage) {
		this.isSeekingCoverage = isSeekingCoverage;
	}

	public String getMemberEligibility() {
		return memberEligibility;
	}

	public void setMemberEligibility(String memberEligibility) {
		this.memberEligibility = memberEligibility;
	}
	public String getMedicaidChild() {
		return medicaidChild;
	}

	public void setMedicaidChild(String medicaidChild) {
		this.medicaidChild = medicaidChild;
	}
	public boolean isPregnant() {
		return isPregnant;
	}

	public void setPregnant(boolean isPregnant) {
		this.isPregnant = isPregnant;
	}
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Member [dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", sequenceNumber=");
		builder.append(sequenceNumber);
		builder.append(", isSeekingCoverage=");
		builder.append(isSeekingCoverage);
		builder.append(", memberEligibility=");
		builder.append(memberEligibility);
		builder.append("]");
		return builder.toString();
	}
	public boolean isNativeAmerican() {
		return isNativeAmerican;
	}
	public void setNativeAmerican(boolean isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}
	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

}
