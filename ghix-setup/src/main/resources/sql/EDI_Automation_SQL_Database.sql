insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  'NM0',
  '850327237',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  'NM0',
  '850327237',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '850327237',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '850327237',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  'NM0',
  '850327237',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '850327237',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  'NM0',
  '850327237',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850327237',
  'T',
  'NM0',
  '850327237',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  'FI',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '850327237',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Lovelace Health System, Inc.',
  '97132',
  '24',
  'Inbound',
  'ZZ',
  '850327237',
  'ZZ',
  'NM0',
  'T',
  '850327237',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  'NM0',
  '943037165',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  'NM0',
  '943037165',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '943037165',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '943037165',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  'NM0',
  '943037165',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '943037165',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  'NM0',
  '943037165',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '943037165',
  'T',
  'NM0',
  '943037165',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  'FI',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '943037165',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Health Plan, Inc',
  '57173',
  '24',
  'Inbound',
  'ZZ',
  '943037165',
  'ZZ',
  'NM0',
  'T',
  '943037165',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  'NM0',
  '451294709',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  'NM0',
  '451294709',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '451294709',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '451294709',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  'NM0',
  '451294709',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '451294709',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  'NM0',
  '451294709',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '451294709',
  'T',
  'NM0',
  '451294709',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  'FI',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '451294709',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'New Mexico Health Connections',
  '93091',
  '24',
  'Inbound',
  'ZZ',
  '451294709',
  'ZZ',
  'NM0',
  'T',
  '451294709',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  'NM0',
  '956042390',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  'NM0',
  '956042390',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '956042390',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '956042390',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  'NM0',
  '956042390',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '956042390',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  'NM0',
  '956042390',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '956042390',
  'T',
  'NM0',
  '956042390',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  'FI',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '956042390',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'BEST Life and Health Insurance Company',
  '26075',
  '24',
  'Inbound',
  'ZZ',
  '956042390',
  'ZZ',
  'NM0',
  'T',
  '956042390',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  'NM0',
  '850484337',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  'NM0',
  '850484337',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '850484337',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '850484337',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  'NM0',
  '850484337',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '850484337',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  'NM0',
  '850484337',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850484337',
  'T',
  'NM0',
  '850484337',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  'FI',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '850484337',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Presbyterian Insurance Company, Inc',
  '52744',
  '24',
  'Inbound',
  'ZZ',
  '850484337',
  'ZZ',
  'NM0',
  'T',
  '850484337',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  'NM0',
  '135123390',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  'NM0',
  '135123390',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '135123390',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '135123390',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  'NM0',
  '135123390',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '135123390',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  'NM0',
  '135123390',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '135123390',
  'T',
  'NM0',
  '135123390',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  'FI',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '135123390',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'The Guardian Life Insurance Company of America',
  '66440',
  '24',
  'Inbound',
  'ZZ',
  '135123390',
  'ZZ',
  'NM0',
  'T',
  '135123390',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  'NM0',
  '361236610',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  'NM0',
  '361236610',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '361236610',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '361236610',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  'NM0',
  '361236610',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '361236610',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  'NM0',
  '361236610',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '361236610',
  'T',
  'NM0',
  '361236610',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  'FI',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '361236610',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Blue Cross Blue Shield of New Mexico',
  '75605',
  '24',
  'Inbound',
  'ZZ',
  '361236610',
  'ZZ',
  'NM0',
  'T',
  '361236610',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  'NM0',
  '850224562',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '1',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  'NM0',
  '850224562',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromGHIX/NM0/X220A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'enrollmentRecord_to_834-X220A1.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '2',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '850224562',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '3',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '850224562',
  'NM0',
  '005010X220A1',
  '834',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X220A1',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '834-X220A1_to_enrollmentRecord.map',
  'PDSA5010HIX-834X220',
  'HIX_X220X306_custom.apf',
  '4',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  'NM0',
  '850224562',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromGHIX/NM0/X306',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  'paymentRecord_to_820-X306.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '5',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '850224562',
  'NM0',
  '005010X306',
  '820',
  '/home/tibcouser/data/fromCALHEERS',
  '/home/tibcouser/data/toGHIX/NM0/X306',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '820-X306_to_paymentRecord.map',
  'PDSA5010HIX-820X306',
  'HIX_X220X306_custom.apf',
  '6',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '7',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromGHIX/NM0/TA1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '8',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '9',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '',
  '',
  '',
  'TA1',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  '',
  '',
  '10',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  'NM0',
  '850224562',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '11',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Outbound',
  'ZZ',
  'NM0',
  'ZZ',
  '850224562',
  'T',
  'NM0',
  '850224562',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromGHIX/NM0/X231A1',
  '/home/tibcouser/data/toCALHEERS',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '12',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  'FI',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '850224562',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '13',
  systimestamp,
  systimestamp  
);

insert
into EXCHG_PartnerLookup values
(
  EXCHG_PrtLkp_seq.nextval,
  'Delta Dental Plan of New Mexico',
  '36629',
  '24',
  'Inbound',
  'ZZ',
  '850224562',
  'ZZ',
  'NM0',
  'T',
  '850224562',
  'NM0',
  '005010X231A1',
  '999',
  '/home/tibcouser/data/fromCALHEERS',
  '',
  '/home/tibcouser/data/inProcess',
  '/home/tibcouser/data/archive',
  '',
  'PDSA5010-999X231',
  'HIX_X220X306_custom.apf',
  '14',
  systimestamp,
  systimestamp  
);



insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '97132', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '57173', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '93091', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '26075', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '52744', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '66440', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '75605', '1' );
insert into EXCHG_ControlNum values ( EXCHG_ControlNum_seq.nextval, '36629', '1' );



insert all
into EXCHG_FilePattern values ( 1,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_CA_834_INDV_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 2,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_CA_834_SHOP_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 3,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_CA_834_RECONINDV_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 4,  'O', '834', '005010X220A1', 'to_<HIOS_Issuer_ID>_CA_834_RECONSHOP_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 5,  'O', '820', '005010X306', 'to_<HIOS_Issuer_ID>_CA_820_SHOP_<YYYYMMDDHHMMSS>', 'xml', 'edi', '' )
into EXCHG_FilePattern values ( 6,  'O', 'TA1', '', 'to_<HIOS_Issuer_ID>_CA_TA1_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 7,  'O', 'TA1', '', 'to_<HIOS_Issuer_ID>_CA_TA1_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 8,  'O', 'TA1', '', 'to_<HIOS_Issuer_ID>_CA_TA1_820_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 9,  'O', '999', '005010X231A1', 'to_<HIOS_Issuer_ID>_CA_999_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 10, 'O', '999', '005010X231A1', 'to_<HIOS_Issuer_ID>_CA_999_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 11, 'O', '999', '005010X231A1', 'to_<HIOS_Issuer_ID>_CA_999_820_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 12, 'I', '834', '005010X220A1', 'from_<HIOS_Issuer_ID>_CA_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'xml', '6,9' )
into EXCHG_FilePattern values ( 13, 'I', '834', '005010X220A1', 'from_<HIOS_Issuer_ID>_CA_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'xml', '7,10' )
into EXCHG_FilePattern values ( 14, 'I', '820', '005010X306', 'from_<HIOS_Issuer_ID>_CA_820_INDV_<YYYYMMDDHHMMSS>', 'edi', 'xml', '8,11' )
into EXCHG_FilePattern values ( 15, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_CA_TA1_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 16, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_CA_TA1_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 17, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_CA_TA1_834_RECONINDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 18, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_CA_TA1_834_RECONSHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 19, 'I', 'TA1', '', 'from_<HIOS_Issuer_ID>_CA_TA1_820_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 20, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_CA_999_834_INDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 21, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_CA_999_834_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 22, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_CA_999_834_RECONINDV_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 23, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_CA_999_834_RECONSHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
into EXCHG_FilePattern values ( 24, 'I', '999', '005010X231A1', 'from_<HIOS_Issuer_ID>_CA_999_820_SHOP_<YYYYMMDDHHMMSS>', 'edi', 'edi', '' )
SELECT * FROM dual;



insert all
into  EXCHG_FilePatternRole values (  1, '5010 Individual 834out', '1,3' )
into  EXCHG_FilePatternRole values (  2, '5010 SHOP 834out', '2,4' )
into  EXCHG_FilePatternRole values (  3, '5010 Individual 834in', '12' )
into  EXCHG_FilePatternRole values (  4, '5010 SHOP 834in', '13' )
into  EXCHG_FilePatternRole values (  5, '5010 SHOP 820out', '5' )
into  EXCHG_FilePatternRole values (  6, '5010 Individual 820in', '14' )
into  EXCHG_FilePatternRole values (  7, '5010 Individual TA1out', '6,8' )
into  EXCHG_FilePatternRole values (  8, '5010 SHOP TA1out', '7' )
into  EXCHG_FilePatternRole values (  9, '5010 Individual TA1in', '15,17' )
into  EXCHG_FilePatternRole values ( 10, '5010 SHOP TA1in', '16,18,19' )
into  EXCHG_FilePatternRole values ( 11, '5010 Individual 999out', '9,11' )
into  EXCHG_FilePatternRole values ( 12, '5010 SHOP 999out', '10' )
into  EXCHG_FilePatternRole values ( 13, '5010 Individual 999in', '20,22' )
into  EXCHG_FilePatternRole values ( 14, '5010 SHOP 999in', '21,23,24' )
SELECT * FROM dual;



insert all
into  EXCHG_ReturnCode values (  'Translator', '100', 'Translation ran successfully.' )
into  EXCHG_ReturnCode values (  'Translator', '105', 'Could not open the source schema.' )
into  EXCHG_ReturnCode values (  'Translator', '106', 'Guidelines specified in the API are not the same guidelines used in the map.' )
into  EXCHG_ReturnCode values (  'Translator', '110', 'Command line error or error loading INI file.' )
into  EXCHG_ReturnCode values (  'Translator', '111', 'One of the documents being translated must be HL7 when using the HL7 plug-in.' )
into  EXCHG_ReturnCode values (  'Translator', '117', 'The schema cannot be parsed or the XML data does not match it.' )
into  EXCHG_ReturnCode values (  'Translator', '124', 'Configuration invalid. If this occurred when using the API, this is often because it cannot read the INI file. Check TRANSRoot.' )
into  EXCHG_ReturnCode values (  'Translator', '131', 'Map doesn�t match the convert call. ( Example: Attempting to convert EDI to XML by using an XML-to-EDI map.)' )
into  EXCHG_ReturnCode values (  'Translator', '140', 'API return code for system failure.' )
into  EXCHG_ReturnCode values (  'Translator', '154', 'Converting between this type of source and target is not supported.' )
into  EXCHG_ReturnCode values (  'Translator', '155', 'Could not open the source guideline.' )
into  EXCHG_ReturnCode values (  'Translator', '156', 'Could not open the target schema.' )
into  EXCHG_ReturnCode values (  'Translator', '157', 'Could not open the target guideline.' )
into  EXCHG_ReturnCode values (  'Translator', '158', 'Could not open the map file.' )
into  EXCHG_ReturnCode values (  'Translator', '171', 'Configuration invalid. If this occurred when using the API, this is often because it cannot read the INI file. Check TRANSRoot.' )
into  EXCHG_ReturnCode values (  'Translator', '172', 'Missing source guideline.' )
into  EXCHG_ReturnCode values (  'Translator', '173', 'Missing target guideline.' )
into  EXCHG_ReturnCode values (  'Translator', '174', 'Missing map.' )
into  EXCHG_ReturnCode values (  'Translator', '175', 'Missing input data when using inputMemory while using API.' )
into  EXCHG_ReturnCode values (  'Translator', '176', 'Cannot access input file when using inputByFile while using API.' )
into  EXCHG_ReturnCode values (  'Translator', '177', 'The source file separators are invalid as specified by separator in the API.' )
into  EXCHG_ReturnCode values (  'Translator', '178', 'The target file separators are invalid as specified by separator in the API.' )
into  EXCHG_ReturnCode values (  'Translator', '180', 'Failed on OutputFile or Options while using API. Cannot write the output file, or options like CALLBACKBUFFER and UNA are the wrong format.' )
into  EXCHG_ReturnCode values (  'Translator', '199', 'The API is getting a JNI exception occurred and can�t run Java.' )
into  EXCHG_ReturnCode values (  'Translator', '201', 'There is an error in the input path. ( Examples: An invalid or unreadable input file has been specified. A parameter, such as �t, has been left empty.)' )
into  EXCHG_ReturnCode values (  'HVInStream', '100', 'Validation ran successfully.' )
into  EXCHG_ReturnCode values (  'HVInStream', '110', 'Validation did not run successfully. The command line syntax is incorrect.' )
into  EXCHG_ReturnCode values (  'HVInStream', '120', 'There was a problem loading HVInStream.DLL library.' )
into  EXCHG_ReturnCode values (  'HVInStream', '129', '$dir.ini or fsdir.ini was found but cannot be opened.' )
into  EXCHG_ReturnCode values (  'HVInStream', '130', 'There was a problem with initialization of the validation engine caused by a setup problem ( i.e. registry setup error) or $dir.ini or fsdir.ini is missing or contains invalid paths.' )
into  EXCHG_ReturnCode values (  'HVInStream', '131', '$dir.ini or fsdir.ini cannot be found.' )
into  EXCHG_ReturnCode values (  'HVInStream', '132', 'Cannot access the "BASEROOT" specified in the $dir.ini or fsdir.ini.' )
into  EXCHG_ReturnCode values (  'HVInStream', '133', 'Cannot access the Database directory.' )
into  EXCHG_ReturnCode values (  'HVInStream', '134', 'Generic failure to read ini settings.' )
into  EXCHG_ReturnCode values (  'HVInStream', '140', 'A critical error prevented Validation from running successfully.' )
into  EXCHG_ReturnCode values (  'HVInStream', '150', 'Cannot find or open FS_HIPAA.dat in the Bin directory.' )
into  EXCHG_ReturnCode values (  'HVInStream', '160', 'Deprecated. Exceeded maximum allowable threads ( only when using API for validation).' )
into  EXCHG_ReturnCode values (  'HVInStream', '170', 'Deprecated. Exceeded maximum allowable processors ( only when using API for validation).' )
into  EXCHG_ReturnCode values (  'HVInStream', '180', 'The activity was cancelled by the user ( when using API).' )
into  EXCHG_ReturnCode values (  'HVInStream', '185', 'When running validation, Docsplitter, and Response Generator together from an API, Docsplitter failed.' )
into  EXCHG_ReturnCode values (  'HVInStream', '186', 'When running validation, Docsplitter, and Response Generator together from an API, Response Generator failed.' )
into  EXCHG_ReturnCode values (  'HVInStream', '187', 'When running validation, Docsplitter, and Response Generator together from an API, Response Generator and Docsplitter both failed.' )
into  EXCHG_ReturnCode values (  'HVInStream', '188', 'DataSwapper failed when run from an API.' )
into  EXCHG_ReturnCode values (  'HVInStream', '191', 'A critical trading partner automation error prevented Validation from running successfully. For more information, see *.TA1 in the same directory as the validation detail results file.' )
into  EXCHG_ReturnCode values (  'HVInStream', '195', 'Cannot find the error message file. Check the [ErrMsgFile] of your $dir.ini or fsdir.ini file.' )
into  EXCHG_ReturnCode values (  'HVInStream', '200', 'Configuration path error when using the -c command-line parameter.' )
into  EXCHG_ReturnCode values (  'HVInStream', '201', 'The input file could not be accessed. Check filenames and paths. Put quotes around paths that contain spaces.' )
into  EXCHG_ReturnCode values (  'HVInStream', '202', 'Output file could not be opened. Check filenames and paths. Put quotes around paths that contain spaces.' )
into  EXCHG_ReturnCode values (  'RespGen', '100', 'Response generation succeeded.' )
into  EXCHG_ReturnCode values (  'RespGen', '110', 'Response generation failed.' )
into  EXCHG_ReturnCode values (  'RespGen', '180', 'Failed to initialize Response Generator.' )
into  EXCHG_ReturnCode values (  'RespGen', '185', 'When running validation, Docsplitter, and Response Generator together in the API, Docsplitter failed.' )
into  EXCHG_ReturnCode values (  'RespGen', '186', 'When running validation, Docsplitter, and Response Generator together in the API, Response Generator failed.' )
into  EXCHG_ReturnCode values (  'RespGen', '187', 'When running validation, Docsplitter, and Response Generator together in the API, Docsplitter and Response Generator both failed.' )
into  EXCHG_ReturnCode values (  'RespGen', '201', 'Could not open input file.' )
into  EXCHG_ReturnCode values (  'RespGen', '202', 'Could not open one or more output files.' )
into  EXCHG_ReturnCode values (  'RespGen', '203', 'Could not open the template file.' )
into  EXCHG_ReturnCode values (  'RespGen', '204', 'Requested an inappropriate response document for your original document, such as: 1) If a 997 is requested for an incoming 997; 2) If an 824 is requested for an incoming 824 or 997; 3) If a 277 is requested for any document type other than an 837.' )
into  EXCHG_ReturnCode values (  'RespGen', '205', 'STRUS and STRUE records in detail file are mismatched. This can occur when ISA-IEA, GS-GE, and ST-SE segments are not paired properly. Also, be sure that you are using STRUS=1 and STRUE=1 in the validation profile ( APF) to create STRUS and STRUE records in the validation detail file.' )
into  EXCHG_ReturnCode values (  'RespGen', '206', 'Could not open the specified file; the file either doesn�t exist or can�t be opened. Check the file for read access permission. Response generator failed.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '100', 'Docsplitter ran successfully.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '110', 'Docsplitter did not run successfully.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '150', 'Docsplitter did not run successfully because it could not load one of its internal libraries. This is probably an installation problem. Contact TIBCO Foresight Technical Support.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '180', 'Docsplitter did not run successfully because of a problem other than those listed below. Contact TIBCO Foresight Technical Support.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '185', 'When running validation, Docsplitter, and Response Generator together in the API, Docsplitter failed.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '186', 'When running validation, Docsplitter, and Response Generator together in the API, Response Generator failed.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '187', 'When running validation, Docsplitter, and Response Generator together in the API, Docsplitter and Response Generator both failed.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '200', 'Docsplitter could not access the EDI input file. Binary data was encountered in the EDI input file. Bad segment tag or other document structure problem.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '201', 'Docsplitter could not open the EDI output file.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '202', 'Docsplitter could not open the Instream detail file.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '203', 'Docsplitter could not open the report file.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '204', 'Docsplitter could not open the input 997 file.' )
into  EXCHG_ReturnCode values (  'DocSplitter', '205', 'Docsplitter could not open the INI config file.' )
SELECT * FROM dual;

