package com.getinsured.hix.dto.eapp;

public enum StateEnum {
	
	CA, MA, ID, NM, MS;

}
