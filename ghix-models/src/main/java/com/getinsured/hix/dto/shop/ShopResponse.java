package com.getinsured.hix.dto.shop;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.platform.util.GhixUtils;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

public class ShopResponse  extends GHIXResponse implements Serializable,IShopAPIError {
	private static final long serialVersionUID = 1L;
	private Map<String, Object> responseData = new HashMap<String, Object>();
	// variable to pass exception information from shop to web.
	private String nestedStackTrace;
	
	public static enum ResponseType {
		JSON, XML;
	}

	public ShopResponse() {
		this.setErrCode(IShopAPIError.ShopAPIErrors.NO_ERROR.getId());
		this.setErrMsg(IShopAPIError.ShopAPIErrors.NO_ERROR.getMessage());
		this.startResponse();
	}

	public Map<String, Object> getResponseData() {
		return responseData;
	}

	public void setResponseData(Map<String, Object> responseData) {
		this.responseData = responseData;
	}

	public void addResponseData(String key, Object data) {
		responseData.put(key, data);
	}

	public String sendResponse(ResponseType type){
		switch (type) {
		case JSON:
			return sendJsonResponse();
		case XML:
			return sendXmlResponse();
		}
		return "";
	}

	private String sendJsonResponse(){
		this.endResponse();
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}

	private String sendXmlResponse(){
		this.endResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();

		return xstream.toXML(this);
	}
	
	public String getNestedStackTrace() {
		return nestedStackTrace;
	}

	public void setNestedStackTrace(String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}

}
