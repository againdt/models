package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the broker_documents database table.
 * 
 */
@Entity
@Table(name = "broker_documents")
public class BrokerDocuments implements Serializable, Cloneable {
	private static final int FIFTY = 50;

	private static final int HUNDRED = 100;

	private static final long serialVersionUID = 1L;

	/*
	 * public static enum DOCUMENT_TYPE{ APPLICATION_SUPPORTING; }
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BrokerDocuments_Seq")
	@SequenceGenerator(name = "BrokerDocuments_Seq", sequenceName = "broker_documents_seq", allocationSize = 1)
	private int id;

	@Column(name = "DOCUMENT_TYPE", length = FIFTY)
	private String documentType;

	@Column(name = "DOCUMENT_NAME", length = HUNDRED)
	private String documentName;

	@Column(name = "ORG_DOC_NAME", length = HUNDRED)
	private String orgDocumentName;

	@Column(name = "DOCUMENT_MIME", length = HUNDRED)
	private String mimeType;

	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;

	@Column(name = "CREATEDBY")
	private String createdBy;

	public String getOrgDocumentName() {
		return orgDocumentName;
	}

	public void setOrgDocumentName(String orgDocumentName) {
		this.orgDocumentName = orgDocumentName;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/* To AutoUpdate created dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedDate(new TSDate());

	}
}
