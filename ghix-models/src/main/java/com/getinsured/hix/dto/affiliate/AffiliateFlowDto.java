package com.getinsured.hix.dto.affiliate;

import java.io.Serializable;

public class AffiliateFlowDto implements Serializable {

	private Long affiliateId;
	private Integer flowId;
	
	public Long getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(Long affiliateId) {
		this.affiliateId = affiliateId;
	}
	public Integer getFlowId() {
		return flowId;
	}
	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}
	
	
	
}
