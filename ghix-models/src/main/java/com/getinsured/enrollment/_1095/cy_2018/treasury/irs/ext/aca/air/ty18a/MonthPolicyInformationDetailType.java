
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Month Safe Harbor Indicator Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-05-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt;A group that contains monthly safe harbor indicators.&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for MonthPolicyInformationDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MonthPolicyInformationDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MonthNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MonthlyPolicyInformationDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthPolicyInformationDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "monthNum",
    "monthlyPolicyInformationDetail"
})
public class MonthPolicyInformationDetailType {

    @XmlElement(name = "MonthNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected int monthNum;
    @XmlElement(name = "MonthlyPolicyInformationDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MonthlyPolicyInformationDetailType monthlyPolicyInformationDetail;

    /**
     * Gets the value of the monthNum property.
     * 
     */
    public int getMonthNum() {
        return monthNum;
    }

    /**
     * Sets the value of the monthNum property.
     * 
     */
    public void setMonthNum(int value) {
        this.monthNum = value;
    }

    /**
     * Gets the value of the monthlyPolicyInformationDetail property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlyPolicyInformationDetailType }
     *     
     */
    public MonthlyPolicyInformationDetailType getMonthlyPolicyInformationDetail() {
        return monthlyPolicyInformationDetail;
    }

    /**
     * Sets the value of the monthlyPolicyInformationDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlyPolicyInformationDetailType }
     *     
     */
    public void setMonthlyPolicyInformationDetail(MonthlyPolicyInformationDetailType value) {
        this.monthlyPolicyInformationDetail = value;
    }

}
