package com.getinsured.hix.cap.util;
 
import java.util.LinkedHashMap;

import com.getinsured.hix.model.estimator.mini.EligLead;

public class DisplayLists {
	//private static Map<String,DisplayStatus> validLeadStagesMap
	
	//todo: Push this into db. Look at "LOOKUP semantics in Enrollment"
//	private static List<DisplayStatus> validLeadStagesList = null;
//	private static Map<String,String> validLeadStagesMap = null;
	 
	
	public static LinkedHashMap<String, String> s_validLeadStagesDisplayMap = new LinkedHashMap<String, String>();
	
	static
	{
		//Ordering is important - do not change the ordering..
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.REGISTERED.toString(), "New Account Created");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PRE_ELIGIBILITY.toString(), "Pre-Eligibility");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PRE_SHOPPING.toString(), "Eligibility Estimated");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PRE_SHOPPING_COMPLETE.toString(), "Plan Selected");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.REDIRECTED_TO_THE_FFM_SBE.toString(), "Sent To FFM");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.REDIRECTED_BACK_TO_GETINSURED.toString(), "Plan Confirmation");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PAYABLE_EVENT.toString(), "Enrollment Started");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.ENROLLMENT_COMPLETED.toString(), "Enrollment Completed");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SCREENER_PAGE.toString(), "SSAP Screener");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SCREENER_PASS.toString(), "Screener Passed");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SCREENER_FAILED.toString(), "Screener Failed");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.TRANSITION_PAGE.toString(), "Transition Page");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SSAP_ON_EXCHANGE_START_YOUR_APPLICATION.toString(), "SSAP Application Start");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SSAP_ON_EXCHANGE_FAMILY_AND_HOUSEHOLD.toString(), "SSAP Family Household");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SSAP_ON_EXCHANGE_INCOME.toString(), "SSAP Income");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SSAP_ON_EXCHANGE_ADDITIONAL_QUESTIONS.toString(), "SSAP Additional Questions");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.SSAP_ON_EXCHANGE_REVIEW_AND_SIGN.toString(), "SSAP Review");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PROXY_READY_TO_SUBMIT.toString(), "SSAP Ready to Submit");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PROXY_SUBMIT_IN_PROGRESS.toString(), "SSAP Submission Started");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PROXY_SUBMIT_FAILED.toString(), "SSAP Failed Once");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PROXY_FAILED.toString(), "Submission Failed");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PROXY_INCOMPLETE.toString(), "SSAP Submission Incomplete");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.PROXY_SUCCESS.toString(), "SSAP Success");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.HCGOV_SUBMIT_IN_PROGRESS.toString(), "Submission to FFM");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.REDIRECTED_TO_GI_HCGOV_ELIG_MISMATCH.toString(), "FFM GI APTC Mismatch");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.APP_SUBMIT_HCGOV_FAILED.toString(), "Submission to FFM Failed Once");
		s_validLeadStagesDisplayMap.put(EligLead.STAGE.APP_HCGOV_FAILED.toString(), "Submission to FFM Failed");
	}
	
	public static LinkedHashMap<String, String> s_ManageConsumerDisplayMapForStage = new LinkedHashMap<String, String>();
	
	static
	{
		//Ordering is important - do not change the ordering..
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.REGISTERED.toString(), "New Account Created");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.PRE_ELIGIBILITY.toString(), "Pre-Eligibility");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.PRE_SHOPPING.toString(), "Eligibility Estimated");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.PRE_SHOPPING_COMPLETE.toString(), "Plan Selected");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.REDIRECTED_TO_THE_FFM_SBE.toString(), "Sent To FFM");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.REDIRECTED_BACK_TO_GETINSURED.toString(), "Plan Confirmation");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.PAYABLE_EVENT.toString(), "Enrollment Started");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.ENROLLMENT_COMPLETED.toString(), "Enrollment Completed");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.HCGOV_SUBMIT_IN_PROGRESS.toString(), "Submission to FFM");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.REDIRECTED_TO_GI_HCGOV_ELIG_MISMATCH.toString(), "FFM GI APTC Mismatch");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.APP_SUBMIT_HCGOV_FAILED.toString(), "Submission to FFM Failed Once");
		s_ManageConsumerDisplayMapForStage.put(EligLead.STAGE.APP_HCGOV_FAILED.toString(), "Submission to FFM Failed");
	}
	
	public static LinkedHashMap<String, String> s_validLeadStagesDisplayMapForLeads = new LinkedHashMap<String, String>();
	
	static
	{
		//Ordering is important - do not change the ordering..
		
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PRE_SHOPPING_COMPLETE.toString(), "Plan Selected");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.REDIRECTED_TO_THE_FFM_SBE.toString(), "Sent To FFM");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.REDIRECTED_BACK_TO_GETINSURED.toString(), "Plan Confirmation");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PAYABLE_EVENT.toString(), "Enrollment Started");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.ENROLLMENT_COMPLETED.toString(), "Enrollment Completed");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SCREENER_PAGE.toString(), "SSAP Screener");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SCREENER_PASS.toString(), "Screener Passed");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SCREENER_FAILED.toString(), "Screener Failed");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.TRANSITION_PAGE.toString(), "Transition Page");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SSAP_ON_EXCHANGE_START_YOUR_APPLICATION.toString(), "SSAP Application Start");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SSAP_ON_EXCHANGE_FAMILY_AND_HOUSEHOLD.toString(), "SSAP Family Household");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SSAP_ON_EXCHANGE_INCOME.toString(), "SSAP Income");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SSAP_ON_EXCHANGE_ADDITIONAL_QUESTIONS.toString(), "SSAP Additional Questions");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.SSAP_ON_EXCHANGE_REVIEW_AND_SIGN.toString(), "SSAP Review");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PROXY_READY_TO_SUBMIT.toString(), "SSAP Ready to Submit");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PROXY_SUBMIT_IN_PROGRESS.toString(), "SSAP Submission Started");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PROXY_SUBMIT_FAILED.toString(), "SSAP Failed Once");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PROXY_FAILED.toString(), "Submission Failed");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PROXY_INCOMPLETE.toString(), "SSAP Submission Incomplete");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.PROXY_SUCCESS.toString(), "SSAP Success");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.HCGOV_SUBMIT_IN_PROGRESS.toString(), "Submission to FFM");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.REDIRECTED_TO_GI_HCGOV_ELIG_MISMATCH.toString(), "FFM GI APTC Mismatch");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.APP_SUBMIT_HCGOV_FAILED.toString(), "Submission to FFM Failed Once");
		  s_validLeadStagesDisplayMapForLeads.put(EligLead.STAGE.APP_HCGOV_FAILED.toString(), "Submission to FFM Failed");
	}
	
	
	
	public static LinkedHashMap<String, String> s_validLeadStatusDisplayMap = new LinkedHashMap<String, String>();
	
	static
	{
		//Ordering is important - do not change the ordering..
		s_validLeadStatusDisplayMap.put("ACTIVE",  "Active");
		s_validLeadStatusDisplayMap.put("NEW", "New");
		s_validLeadStatusDisplayMap.put("EXISTING", "Existing");
		s_validLeadStatusDisplayMap.put("NEXT_OEP", "Next OEP");
		s_validLeadStatusDisplayMap.put("DISQUALIFIED", "Disqualified");
		s_validLeadStatusDisplayMap.put("DEAD_NOT_INTERESTED", "Dead - Not Interested");
		s_validLeadStatusDisplayMap.put("DEAD_PURCHASED_FROM", "Dead - Purchased from Competitor");
		s_validLeadStatusDisplayMap.put("DEAD_COULD_NOT_CONTACT", "Dead - Could not contact");
		s_validLeadStatusDisplayMap.put("DEAD_PLAN_NOT_SUPPORTED", "Dead - Plan not supported");
		
		
	}
	
	public static LinkedHashMap<String, String> s_validLeadStatusNotDeadDisplayMap = new LinkedHashMap<String, String>();
	
	static
	{
		//Ordering is important - do not change the ordering..
		s_validLeadStatusNotDeadDisplayMap.put("ACTIVE",  "Active");
		s_validLeadStatusNotDeadDisplayMap.put("NEW", "New");
		s_validLeadStatusNotDeadDisplayMap.put("EXISTING", "Existing");
		s_validLeadStatusNotDeadDisplayMap.put("NEXT_OEP", "Next OEP");
		s_validLeadStatusNotDeadDisplayMap.put("DISQUALIFIED", "Disqualified");	
		
	}
	
//	private static List<DisplayStatus> getLeadStagesList()
//	{
//		if (validLeadStagesList != null )
//		{
//			return validLeadStagesList;
//		}
//			
//		//EligLead.STAGE.REGISTERED.name()
//				
//		validLeadStagesList = Arrays.asList (
//					new DisplayStatus("REGISTERED", "New Account Created",0),
//					new DisplayStatus("PRE_ELIGIBILITY",  "Pre-Eligibility",1),
//					new DisplayStatus("PRE_SHOPPING", "Eligibility Estimated",2),
//					new DisplayStatus("PRE_SHOPPING_COMPLETE", "Plan Selected",3),
//					new DisplayStatus("REDIRECTED_TO_THE_FFM_SBE", "Sent To FFM",4),
//					new DisplayStatus("REDIRECTED_BACK_TO_GETINSURED", "Plan Confirmation",5),
//					//TODO: Find out how many users are there in production with this flag..
//					new DisplayStatus("SHOPPING", "Plan Confirmation",6),
//			 		new DisplayStatus("PAYABLE_EVENT", "Enrollment Started",7),
//			 		//TODO: Find out how many users are there in production with this flag..
//					new DisplayStatus("ENROLLMENT", "Enrollment",8),
//					new DisplayStatus("ENROLLMENT_COMPLETED", "Enrollment Completed",9),
//					
//					//Deleted
//					new DisplayStatus("ISSUER_CONFIRMED_ENROLLMENT", "Issuer Confirmed Enrollment",10),
//					new DisplayStatus("COMMISSION_RECEIVED_FROM_CARRIER", "Commission Received From Carrier",11)
//				);
//		 Collections.sort(validLeadStagesList); //magic
//		 
//		 return validLeadStagesList;
//	}
//	private static Map<String,String> getLeadStagesDisplayMap()
//	{
//		if(validLeadStagesMap != null )
//		{
//			return validLeadStagesMap;
//		}
//		
//		validLeadStagesMap = new TreeMap<String,String>();
//		for(DisplayStatus status:validLeadStagesList)
//		{
//			validLeadStagesMap.put(status.getStatusName(), status.getDisplayName());
//		}
//		return validLeadStagesMap;
//	}
}
 

