
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Other Complete Person Name Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial Version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-06-04&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;CACInd&gt;Yes&lt;/CACInd&gt;
 * 					&lt;Status&gt;Active&lt;/Status&gt;
 * 					&lt;DescriptionTxt&gt;Global type definition for person's full name where all elements are optional.&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for OtherCompletePersonNameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OtherCompletePersonNameType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PersonFirstNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PersonMiddleNm" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PersonLastNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}SuffixNm" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherCompletePersonNameType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "personFirstNm",
    "personMiddleNm",
    "personLastNm",
    "suffixNm"
})
public class OtherCompletePersonNameType {

    @XmlElement(name = "PersonFirstNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String personFirstNm;
    @XmlElement(name = "PersonMiddleNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String personMiddleNm;
    @XmlElement(name = "PersonLastNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String personLastNm;
    @XmlElement(name = "SuffixNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected String suffixNm;

    /**
     * Gets the value of the personFirstNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonFirstNm() {
        return personFirstNm;
    }

    /**
     * Sets the value of the personFirstNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonFirstNm(String value) {
        this.personFirstNm = value;
    }

    /**
     * Gets the value of the personMiddleNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonMiddleNm() {
        return personMiddleNm;
    }

    /**
     * Sets the value of the personMiddleNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonMiddleNm(String value) {
        this.personMiddleNm = value;
    }

    /**
     * Gets the value of the personLastNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonLastNm() {
        return personLastNm;
    }

    /**
     * Sets the value of the personLastNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonLastNm(String value) {
        this.personLastNm = value;
    }

    /**
     * Gets the value of the suffixNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuffixNm() {
        return suffixNm;
    }

    /**
     * Sets the value of the suffixNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuffixNm(String value) {
        this.suffixNm = value;
    }

}
