package com.getinsured.hix.dto.agency;


import java.util.List;


public class AgencyAssistantApprovalDto {

	String id;
	String assistantNumber;
	String approvalNumber;
	String approvalDate;
	String applicationSubmissionDate;
	String approvalStatus;
	String status;
	String comment;
	String staffId;
	List<String> assitantApprovalStatusList;
	List<AgencyAssistantApprovalStatusHistory> approvalStatusHistory;
	String delegationCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAssistantNumber() {
		return assistantNumber;
	}
	public void setAssistantNumber(String assistantNumber) {
		this.assistantNumber = assistantNumber;
	}
	public String getApprovalNumber() {
		return approvalNumber;
	}
	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getApplicationSubmissionDate() {
		return applicationSubmissionDate;
	}
	public void setApplicationSubmissionDate(String applicationSubmissionDate) {
		this.applicationSubmissionDate = applicationSubmissionDate;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public List<String> getAssitantApprovalStatusList() {
		return assitantApprovalStatusList;
	}
	public void setAssitantApprovalStatusList(List<String> assitantApprovalStatusList) {
		this.assitantApprovalStatusList = assitantApprovalStatusList;
	}
	public List<AgencyAssistantApprovalStatusHistory> getApprovalStatusHistory() {
		return approvalStatusHistory;
	}
	public void setApprovalStatusHistory(List<AgencyAssistantApprovalStatusHistory> approvalStatusHistory) {
		this.approvalStatusHistory = approvalStatusHistory;
	}
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public String getDelegationCode() {
		return delegationCode;
	}
	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}
	
	
}
