package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class PersonReference{
  @JsonProperty("ref")
  
  private String ref;
  public void setRef(String ref){
   this.ref=ref;
  }
  public String getRef(){
   return ref;
  }
}