package com.getinsured.hix.model.plandisplay;

import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public class PdPreShopRequest {
	private InsuranceType insuranceType;
	private String effectiveStartDate;
	private EligLead eligLead;
	private GroupData groupData;
	
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	public EligLead getEligLead() {
		return eligLead;
	}
	public void setEligLead(EligLead eligLead) {
		this.eligLead = eligLead;
	}
	public GroupData getGroupData() {
		return groupData;
	}
	public void setGroupData(GroupData groupData) {
		this.groupData = groupData;
	}
}
