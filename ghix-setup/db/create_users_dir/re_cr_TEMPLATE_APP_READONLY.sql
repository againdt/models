create role TEMPLATE_APP_READONLY_ROLE;
--drop user TEMPLATE_APP_READONLY cascade;
create user TEMPLATE_APP_READONLY identified by TEMPLATE_APP_READONLY 
default tablespace TEMPLATE_DATA temporary tablespace temp quota unlimited on TEMPLATE_DATA; 

----
grant create session to TEMPLATE_APP_READONLY;
grant TEMPLATE_APP_READONLY_ROLE to TEMPLATE_APP_READONLY;
quit

