package com.getinsured.iex.ssap.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.timeshift.sql.TSTimestamp;


/**
 * The persistent class for the SSAP_APPLICANTS database table.
 *
 */
@Audited
@Entity
@Table(name="SSAP_APPLICANTS")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="SsapApplicant.findAll", query="SELECT s FROM SsapApplicant s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SsapApplicant implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String PRIMARY_TAX_FILER_APPLICANT_PERSON_TYPE = "PTF";
	private static final String PRIMARY_CONTACT_APPLICANT_PERSON_TYPE = "PC";
	
	@Id
	@SequenceGenerator(name="SSAP_APPLICANTS_ID_GENERATOR", sequenceName="SSAP_APPLICANTS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICANTS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@NotAudited
	@Column(name="APPLYING_FOR_COVERAGE", length=1)
	private String applyingForCoverage;
	@NotAudited
	@Temporal(TemporalType.DATE)
	@Column(name="BIRTH_DATE")
	private Date birthDate;

	@Column(name="CITIZENSHIP_IMMIGRATION_STATUS", length=50)
	private String citizenshipImmigrationStatus;

	@NotAudited
	@Column(name="CITIZENSHIP_IMMIGRATION_VID", precision=22)
	private BigDecimal citizenshipImmigrationVid;

	@Column(name="CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@Column(name="DEATH_STATUS", length=10)
	private String deathStatus;

	@NotAudited
	@Column(name="DEATH_VERIFICATION_VID", precision=22)
	private BigDecimal deathVerificationVid;

	@NotAudited
	@Column(name="EXTERNAL_APPLICANT_ID", length=100)
	private String externalApplicantId;

	@NotAudited
	@Column(name="FIRST_NAME", length=100)
	private String firstName;

	@NotAudited
	@Column(length=10)
	private String gender;

	@NotAudited
	@Column(name="HOUSEHOLD_CONTACT_FLAG", length=5)
	private String householdContactFlag;

	//@Column(name="HOUSEHOLD_CONTACT_VID", precision=22)
	//private BigDecimal householdContactVid;

	@Column(name="INCARCERATION_STATUS", length=50)
	private String incarcerationStatus;

	@NotAudited
	@Column(name="INCARCERATION_VID", precision=22)
	private BigDecimal incarcerationVid;

	@Column(name="INCOME_VERIFICATION_STATUS", length=50)
	private String incomeVerificationStatus;

	@NotAudited
	@Column(name="INCOME_VERIFICATION_VID", precision=22)
	private BigDecimal incomeVerificationVid;

	@NotAudited
	@Column(name="LAST_NAME", length=100)
	private String lastName;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@NotAudited
	@Column(name="MAILIING_LOCATION_ID", precision=22)
	private BigDecimal mailiingLocationId;

	@NotAudited
	@Column(length=10)
	private String married;

	@Column(name="MEC_VERIFICATION_STATUS", length=50)
	private String mecVerificationStatus;

	@NotAudited
	@Column(name="MEC_VERIFICATION_VID", precision=22)
	private BigDecimal mecVerificationVid;

	@NotAudited
	@Column(name = "NONESIMEC_VERIFICATION_STATUS")
	private String nonEsiMecVerificationStatus;

	@NotAudited
	@Column(name = "NONESIMEC_VERIFICATION_VID")
	private Long nonEsiMecVerificationVid;

	@NotAudited
	@Column(name="MIDDLE_NAME", length=100)
	private String middleName;

	@NotAudited
	@Column(name="PRIMARY_LOC_ID", precision=22)
	private BigDecimal otherLocationId;

	@Column(name="RESIDENCY_STATUS", length=50)
	private String residencyStatus;

	@NotAudited
	@Column(name="RESIDENCY_VID", precision=22)
	private BigDecimal residencyVid;

	@Column(name="SSN_VERIFICATION_STATUS", length=50)
	private String ssnVerificationStatus;

	@NotAudited
	@Column(name="SSN_VERIFICATION_VID", precision=22)
	private BigDecimal ssnVerificationVid;

	@NotAudited
	@Column(length=1)
	private String tobaccouser;

	@NotAudited
	@Column(name="PERSON_ID", precision=22)
	private long personId;

	@Column(name="VLP_VERIFICATION_STATUS", length=50)
	private String vlpVerificationStatus;

	@NotAudited
	@Column(name="VLP_VERIFICATION_VID", precision=22)
	private BigDecimal vlpVerificationVid;

	@NotAudited
	@Column(name="PHONE_NUMBER", length=25)
	private String phoneNumber;

	@NotAudited
	@Column(name="MOBILE_PHONE_NUMBER", length=25)
	private String mobilePhoneNumber;

	@NotAudited
	@Column(name="EMAIL_ADDRESS", length=100)
	private String emailAddress;

	@NotAudited
	@Column(name="FULL_TIME_STUDENT", length=4)
	private String fullTimeStudent;

	@NotAudited
	@Column(name="RELATIONSHIP", length=240)
	private String relationship;

	@NotAudited
	@Column(name="PREFERRED_TIME_TO_CONTACT", length=30)
	private String preferredTimeToContact;

	@Column(name="ELIGIBILITY_STATUS", length=50)
	private String eligibilityStatus;

	@NotAudited
	@Column(name="APPLICANT_GUID", length=100)
	private String applicantGuid;

	@NotAudited
	@Column(name="IS_ENRLD_CESS_PRGRM", length=1)
	private String isEnrldCessPrgrm;

	@NotAudited
	@Column(name="MARITIAL_STATUS_ID", precision=22)
	private BigDecimal maritialStatusId;

	@NotAudited
	@OneToMany(mappedBy="ssapApplicant", cascade = CascadeType.PERSIST)
	private List<SsapApplicantEvent> ssapApplicantEvents;


	@NotAudited
	@Column(name="ECN_NUMBER")
	private String ecnNumber;

	@NotAudited
	@Column(name="HARDSHIP_EXEMPT")
	private String hardshipExempt;
	
	@NotAudited
	@Column(name="MEC_SOURCE")
	private String mecSource;

	@NotAudited
	@Column(name="CSR_LEVEL", length=50)
	 private String csrLevel;

	@Column(name="STATUS")
	private String status;

	@Column(name="ON_APPLICATION", length=1)
	private String onApplication = "Y";
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@NotAudited
	@Column(name="NAME_SUFFIX", length=3)
	private String nameSuffix;
	
	@NotAudited
	@Column(name="PERSON_TYPE", length=10)
	@Enumerated(EnumType.STRING)
	private SsapApplicantPersonType personType;

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getOnApplication() {
		return onApplication;
	}

	public void setOnApplication(String onApplication) {
		this.onApplication = onApplication;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getIsEnrldCessPrgrm() {
		return isEnrldCessPrgrm;
	}

	public void setIsEnrldCessPrgrm(String isEnrldCessPrgrm) {
		this.isEnrldCessPrgrm = isEnrldCessPrgrm;
	}

	public BigDecimal getMaritialStatusId() {
		return maritialStatusId;
	}

	public void setMaritialStatusId(BigDecimal maritialStatusId) {
		this.maritialStatusId = maritialStatusId;
	}

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFullTimeStudent() {
		return fullTimeStudent;
	}

	public void setFullTimeStudent(String fullTimeStudent) {
		this.fullTimeStudent = fullTimeStudent;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getPreferredTimeToContact() {
		return preferredTimeToContact;
	}

	public void setPreferredTimeToContact(String preferredTimeToContact) {
		this.preferredTimeToContact = preferredTimeToContact;
	}

	//bi-directional many-to-one association to SsapApplication
	//@ManyToOne(fetch=FetchType.LAZY)
	@NotAudited
	@ManyToOne
	@JoinColumn(name="SSAP_APPLICATION_ID", nullable=false)
	private SsapApplication ssapApplication;

	//bi-directional many-to-one association to SsapVerification
	@NotAudited
	@OneToMany(mappedBy="ssapApplicant", cascade = CascadeType.ALL)
	private List<SsapVerification> ssapVerifications;

	@NotAudited
	@OneToMany(mappedBy="ssapApplicant", cascade = CascadeType.PERSIST)
	private List<EligibilityProgram> eligibilityProgram;

	@NotAudited
	@Column(name="SSN")
	private String ssn;

	@NotAudited
	@Column(name="NATIVE_AMERICAN_FLAG", length=1)
	private String nativeAmericanFlag;

	@Column(name="NAT_AMER_VERIFICATION_STATUS", length=50)
	private String nativeAmericanVerificationStatus;

	public String getNativeAmericanFlag() {
		return nativeAmericanFlag;
	}

	public void setNativeAmericanFlag(String nativeAmericanFlag) {
		this.nativeAmericanFlag = nativeAmericanFlag;
	}

	public String getNativeAmericanVerificationStatus() {
		return nativeAmericanVerificationStatus;
	}

	public void setNativeAmericanVerificationStatus(String nativeAmericanVerificationStatus) {
		this.nativeAmericanVerificationStatus = nativeAmericanVerificationStatus;
	}

	public SsapApplicant() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplyingForCoverage() {
		return this.applyingForCoverage;
	}

	public void setApplyingForCoverage(String applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCitizenshipImmigrationStatus() {
		return this.citizenshipImmigrationStatus;
	}

	public void setCitizenshipImmigrationStatus(String citizenshipImmigrationStatus) {
		this.citizenshipImmigrationStatus = citizenshipImmigrationStatus;
	}

	public BigDecimal getCitizenshipImmigrationVid() {
		return this.citizenshipImmigrationVid;
	}

	public void setCitizenshipImmigrationVid(BigDecimal citizenshipImmigrationVid) {
		this.citizenshipImmigrationVid = citizenshipImmigrationVid;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getDeathStatus() {
		return this.deathStatus;
	}

	public void setDeathStatus(String deathStatus) {
		this.deathStatus = deathStatus;
	}

	public BigDecimal getDeathVerificationVid() {
		return this.deathVerificationVid;
	}

	public void setDeathVerificationVid(BigDecimal deathVerificationVid) {
		this.deathVerificationVid = deathVerificationVid;
	}

	public String getExternalApplicantId() {
		return this.externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHouseholdContactFlag() {
		return this.householdContactFlag;
	}

	public void setHouseholdContactFlag(String householdContactFlag) {
		this.householdContactFlag = householdContactFlag;
	}

	/*public BigDecimal getHouseholdContactVid() {
		return this.householdContactVid;
	}

	public void setHouseholdContactVid(BigDecimal householdContactVid) {
		this.householdContactVid = householdContactVid;
	}*/

	public String getIncarcerationStatus() {
		return this.incarcerationStatus;
	}

	public void setIncarcerationStatus(String incarcerationStatus) {
		this.incarcerationStatus = incarcerationStatus;
	}

	public BigDecimal getIncarcerationVid() {
		return this.incarcerationVid;
	}

	public void setIncarcerationVid(BigDecimal incarcerationVid) {
		this.incarcerationVid = incarcerationVid;
	}

	public String getIncomeVerificationStatus() {
		return this.incomeVerificationStatus;
	}

	public void setIncomeVerificationStatus(String incomeVerificationStatus) {
		this.incomeVerificationStatus = incomeVerificationStatus;
	}

	public BigDecimal getIncomeVerificationVid() {
		return this.incomeVerificationVid;
	}

	public void setIncomeVerificationVid(BigDecimal incomeVerificationVid) {
		this.incomeVerificationVid = incomeVerificationVid;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public BigDecimal getMailiingLocationId() {
		return this.mailiingLocationId;
	}

	public void setMailiingLocationId(BigDecimal mailiingLocationId) {
		this.mailiingLocationId = mailiingLocationId;
	}

	public String getMarried() {
		return this.married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public String getMecVerificationStatus() {
		return this.mecVerificationStatus;
	}

	public void setMecVerificationStatus(String mecVerificationStatus) {
		this.mecVerificationStatus = mecVerificationStatus;
	}

	/**
	 * Gets Non Employee Sponsored Insurance Minimum Essential Coverage verification status.
	 * @return verification status for non ESI MEC.
	 */
	public String getNonEsiMecVerificationStatus() {
		return nonEsiMecVerificationStatus;
	}

	/**
	 * Sets status of Non Employee Sponsored Insurance Minimum Essential Coverage verification.
	 * @param nonEsiMecVerificationStatus status of non ESI MEC.
	 */
	public void setNonEsiMecVerificationStatus(String nonEsiMecVerificationStatus) {
		this.nonEsiMecVerificationStatus = nonEsiMecVerificationStatus;
	}

	/**
	 * Gets id from ssap_verifications table for this non-ESI MEC verification.
	 * @return id of verification from ssap_verifications table.
	 */
	public Long getNonEsiMecVerificationVid() {
		return nonEsiMecVerificationVid;
	}

	/**
	 * Sets id from ssap_verifications table for this non-ESI MEC verification.
	 * @param nonEsiMecVerificationVid id from ssap_verifications table.
	 */
	public void setNonEsiMecVerificationVid(Long nonEsiMecVerificationVid) {
		this.nonEsiMecVerificationVid = nonEsiMecVerificationVid;
	}

	public BigDecimal getMecVerificationVid() {
		return this.mecVerificationVid;
	}

	public void setMecVerificationVid(BigDecimal mecVerificationVid) {
		this.mecVerificationVid = mecVerificationVid;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public BigDecimal getOtherLocationId() {
		return this.otherLocationId;
	}

	public void setOtherLocationId(BigDecimal otherLocationId) {
		this.otherLocationId = otherLocationId;
	}

	public String getResidencyStatus() {
		return this.residencyStatus;
	}

	public void setResidencyStatus(String residencyStatus) {
		this.residencyStatus = residencyStatus;
	}

	public BigDecimal getResidencyVid() {
		return this.residencyVid;
	}

	public void setResidencyVid(BigDecimal residencyVid) {
		this.residencyVid = residencyVid;
	}

	public String getSsnVerificationStatus() {
		return this.ssnVerificationStatus;
	}

	public void setSsnVerificationStatus(String ssnVerificationStatus) {
		this.ssnVerificationStatus = ssnVerificationStatus;
	}

	public BigDecimal getSsnVerificationVid() {
		return this.ssnVerificationVid;
	}

	public void setSsnVerificationVid(BigDecimal ssnVerificationVid) {
		this.ssnVerificationVid = ssnVerificationVid;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getVlpVerificationStatus() {
		return vlpVerificationStatus;
	}

	public void setVlpVerificationStatus(String vlpVerificationStatus) {
		this.vlpVerificationStatus = vlpVerificationStatus;
	}

	public String getTobaccouser() {
		return this.tobaccouser;
	}

	public void setTobaccouser(String tobaccouser) {
		this.tobaccouser = tobaccouser;
	}

	public SsapApplication getSsapApplication() {
		return this.ssapApplication;
	}

	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	public List<SsapVerification> getSsapVerifications() {
		return this.ssapVerifications;
	}

	public void setSsapVerifications(List<SsapVerification> ssapVerifications) {
		this.ssapVerifications = ssapVerifications;
	}

	public SsapVerification addSsapVerification(SsapVerification ssapVerification) {
		getSsapVerifications().add(ssapVerification);
		ssapVerification.setSsapApplicant(this);

		return ssapVerification;
	}

	public SsapVerification removeSsapVerification(SsapVerification ssapVerification) {
		getSsapVerifications().remove(ssapVerification);
		ssapVerification.setSsapApplicant(null);

		return ssapVerification;
	}

	public BigDecimal getVlpVerificationVid() {
		return vlpVerificationVid;
	}

	public void setVlpVerificationVid(BigDecimal vlpVerificationVid) {
		this.vlpVerificationVid = vlpVerificationVid;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public List<EligibilityProgram> getEligibilityProgram() {
		return eligibilityProgram;
	}

	public void setEligibilityProgram(List<EligibilityProgram> eligibilityProgram) {
		this.eligibilityProgram = eligibilityProgram;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public List<SsapApplicantEvent> getSsapApplicantEvents() {
		return ssapApplicantEvents;
	}

	public void setSsapApplicantEvents(List<SsapApplicantEvent> ssapApplicantEvents) {
		this.ssapApplicantEvents = ssapApplicantEvents;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getEcnNumber() {
		return ecnNumber;
	}

	public void setEcnNumber(String ecnNumber) {
		this.ecnNumber = ecnNumber;
	}

	public String getHardshipExempt() {
		return hardshipExempt;
	}

	public void setHardshipExempt(String hardshipExempt) {
		this.hardshipExempt = hardshipExempt;

	}
	
	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}


	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		// Add a comment to this line
		this.setCreationTimestamp(new TSTimestamp());
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	public String getMecSource() {
		return mecSource;
	}

	public void setMecSource(String mecSource) {
		this.mecSource = mecSource;
	}
	
	public SsapApplicantPersonType getPersonType() {
		return personType;
	}

	public void setPersonType(SsapApplicantPersonType personType) {
		this.personType = personType;
	}
	
	public boolean isPrimaryContact(){
		if(this.personType != null && this.personType.getPersonType().contains(PRIMARY_CONTACT_APPLICANT_PERSON_TYPE)){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}
	
	public boolean isPrimaryTaxFiler(){
		if(this.personType != null && this.personType.getPersonType().contains(PRIMARY_TAX_FILER_APPLICANT_PERSON_TYPE)){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}
}
