/**
 * 
 */
package com.getinsured.hix.dto.ffm;

import java.util.Date;

/**
 * @author panda_p
 *
 */
public class FFMEnrollmentMemberData {

	
	private String memberFFEAssignedApplicantId;
	private String memberIssuerAssignedMemberId;
	private String memberFFEAssignedMemberId;
	private Boolean memberSubscriberIndicator;
	private String memberRelationshipToSubscriber;
	private Boolean memberTobaccoUseIndicator;
	private String memberLastDateOfTobaccoUse;
	private String memberEnrollmentPeriodType;
	private String memberTypeCode;
	private String memberReasonCode;
	private String memberActionEffectiveDate;
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffixName;
	private String ssn;
	private Date dob;
	private String gender;
	
	
	private String ffmPersonId;
	private String insuranceAplicantId;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuffixName() {
		return suffixName;
	}
	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Boolean getMemberTobaccoUseIndicator() {
		return memberTobaccoUseIndicator;
	}
	
	
	public String getMemberFFEAssignedApplicantId() {
		return memberFFEAssignedApplicantId;
	}
	public void setMemberFFEAssignedApplicantId(String memberFFEAssignedApplicantId) {
		this.memberFFEAssignedApplicantId = memberFFEAssignedApplicantId;
	}
	public String getMemberIssuerAssignedMemberId() {
		return memberIssuerAssignedMemberId;
	}
	public void setMemberIssuerAssignedMemberId(String memberIssuerAssignedMemberId) {
		this.memberIssuerAssignedMemberId = memberIssuerAssignedMemberId;
	}
	public String getMemberFFEAssignedMemberId() {
		return memberFFEAssignedMemberId;
	}
	public void setMemberFFEAssignedMemberId(String memberFFEAssignedMemberId) {
		this.memberFFEAssignedMemberId = memberFFEAssignedMemberId;
	}
	
	public Boolean isMemberSubscriberIndicator() {
		return memberSubscriberIndicator;
	}
	public void setMemberSubscriberIndicator(Boolean memberSubscriberIndicator) {
		this.memberSubscriberIndicator = memberSubscriberIndicator;
	}
	public String getMemberRelationshipToSubscriber() {
		return memberRelationshipToSubscriber;
	}
	public void setMemberRelationshipToSubscriber(
			String memberRelationshipToSubscriber) {
		this.memberRelationshipToSubscriber = memberRelationshipToSubscriber;
	}
	
	public Boolean isMemberTobaccoUseIndicator() {
		return memberTobaccoUseIndicator;
	}
	public void setMemberTobaccoUseIndicator(Boolean memberTobaccoUseIndicator) {
		this.memberTobaccoUseIndicator = memberTobaccoUseIndicator;
	}
	public String getMemberLastDateOfTobaccoUse() {
		return memberLastDateOfTobaccoUse;
	}
	public void setMemberLastDateOfTobaccoUse(String memberLastDateOfTobaccoUse) {
		this.memberLastDateOfTobaccoUse = memberLastDateOfTobaccoUse;
	}
	public String getMemberEnrollmentPeriodType() {
		return memberEnrollmentPeriodType;
	}
	public void setMemberEnrollmentPeriodType(String memberEnrollmentPeriodType) {
		this.memberEnrollmentPeriodType = memberEnrollmentPeriodType;
	}
	public String getMemberTypeCode() {
		return memberTypeCode;
	}
	public void setMemberTypeCode(String memberTypeCode) {
		this.memberTypeCode = memberTypeCode;
	}
	public String getMemberReasonCode() {
		return memberReasonCode;
	}
	public void setMemberReasonCode(String memberReasonCode) {
		this.memberReasonCode = memberReasonCode;
	}
	public String getMemberActionEffectiveDate() {
		return memberActionEffectiveDate;
	}
	public void setMemberActionEffectiveDate(String memberActionEffectiveDate) {
		this.memberActionEffectiveDate = memberActionEffectiveDate;
	}
	public String getInsuranceAplicantId() {
		return insuranceAplicantId;
	}
	public void setInsuranceAplicantId(String insuranceAplicantId) {
		this.insuranceAplicantId = insuranceAplicantId;
	}
	public String getFfmPersonId() {
		return ffmPersonId;
	}
	public void setFfmPersonId(String ffmPersonId) {
		this.ffmPersonId = ffmPersonId;
	}
		
}
