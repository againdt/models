package com.getinsured.hix.model.enrollment;

/**
 * 
 * @author Aditya_S
 * @since 30/03/2015 (DD/MM/YYYY)
 * 
 * HIX-64906
 *  
 */
public class EnrolleeCapDto {
	private String name;
	private String email;
	private String dob;
	private String phone;
	private boolean isSubscriber;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}
	
	/**
	 * @param dob the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the isSubscriber
	 */
	public boolean isSubscriber() {
		return isSubscriber;
	}

	/**
	 * @param isSubscriber the isSubscriber to set
	 */
	public void setSubscriber(boolean isSubscriber) {
		this.isSubscriber = isSubscriber;
	}
}
