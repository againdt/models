package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class EffectiveApplicationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String grossPremiumAmount;
	private String netPremiumAmount;
	private String aptcAmount;
	private String effectiveDate;
	private String applicationId;
	private String bestTimeToContact;
	private String ssapApplicationId;
	private String enrollmentId;
	private String applicationStatus;
	private String userName;
	private String password;
	private String loggedInUser;
	private String verificationEvent;
	
	public String getGrossPremiumAmount() {
		return grossPremiumAmount;
	}
	public void setGrossPremiumAmount(String grossPremiumAmount) {
		this.grossPremiumAmount = grossPremiumAmount;
	}
	public String getNetPremiumAmount() {
		return netPremiumAmount;
	}
	public void setNetPremiumAmount(String netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}
	public String getAptcAmount() {
		return aptcAmount;
	}
	public void setAptcAmount(String aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getBestTimeToContact() {
		return bestTimeToContact;
	}
	public void setBestTimeToContact(String bestTimeToContact) {
		this.bestTimeToContact = bestTimeToContact;
	}
	public String getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(String ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getVerificationEvent() {
		return verificationEvent;
	}
	public void setVerificationEvent(String verificationEvent) {
		this.verificationEvent = verificationEvent;
	}
	
}
