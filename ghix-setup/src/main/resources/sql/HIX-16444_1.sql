delete from lookup_value where lookup_value_code like 'TA1' and lookup_value_label like 'TA1';
delete from lookup_value where lookup_value_code like 'for 834' and lookup_value_label like 'for 834';

INSERT
INTO LOOKUP_VALUE
  (
    LOOKUP_VALUE_ID,
    LOOKUP_TYPE_ID,
    LOOKUP_VALUE_CODE,
    LOOKUP_VALUE_LABEL,
    DESCRIPTION
  )
  VALUES
  (
    LOOKUPVALUE_SEQ.NEXTVAL,
    (select lookup_type_id from lookup_type where name = 'ST01'),
    'TA1 for 834',
    'TA1 for 834',
    ''
  );
