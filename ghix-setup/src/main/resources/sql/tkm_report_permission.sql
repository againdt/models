INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
(role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(SELECT id FROM roles WHERE name='ADMIN'),
(SELECT id FROM permissions WHERE name='TKM_REPORT'));


INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id ) VALUES 
(role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
(SELECT id FROM roles WHERE name='OPERATIONS'),
(SELECT id FROM permissions WHERE name='TKM_REPORT'));

