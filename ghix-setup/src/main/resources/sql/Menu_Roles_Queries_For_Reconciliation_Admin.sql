------------------------------------------------------------------------------------------------------------
------- Script to add menus and submenus to MENU_ROLES table for RECONCILIATION_ADMIN role ----------
------------------------------------------------------------------------------------------------------------

DELETE FROM MENU_ROLES WHERE ROLE_ID=(SELECT ID FROM ROLES WHERE NAME='RECONCILIATION_ADMIN');

-----------------------------------
---------- ISSUERS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Issuers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Add New Issuer'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Issuers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- PLANS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View QHPs'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View SADPs'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Add Plans'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plan Status'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans Util'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans PDF Util'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Plans PDF Status'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where title ='Plan Reports'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View STMs'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View AMEs'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View VISIONs'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View Medicares'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Ancillaries'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Plans') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- CONSUMERS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Consumers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- USERS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Users'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Add User'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Users') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Users'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Users') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- TICKETS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tickets') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tickets') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Workgroups'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Tickets') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

---------------------------------------------------
---------- POLICY ADMIN CONFIG -------------
---------------------------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where title ='Policy Admin Config'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
60,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Feed Config'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Feed Summary'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='View Exceptions'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Commission Management'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where title ='Policy Admin'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Partner Payout'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='BOB Management'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where title ='Policy Admin Config') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- PAYROL -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Payroll'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
70,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Agent Variable Pay'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Payroll') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-----------------------------------
---------- PROVIDERS -------------
-----------------------------------

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values (MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Providers'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
80,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Mapping Files'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Providers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) 
values ( MENU_ROLES_SEQ.NEXTVAL ,(select id from menu_items where caption ='Manage Provider Networks'),(select id from roles where lower(name) =lower('RECONCILIATION_ADMIN')),
(select max(mr.MENU_ITEMS_ORDER) +1 from menu_roles mr, menu_items m where m.PARENT_ID = (select id from menu_items where caption = 'Providers') and mr.MENU_ITEMS_ID = m.id),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
