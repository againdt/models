package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @since 20th July 2015
 * @author Sharma_k
 * Jira ID: HIX-72107
 * Create a new Enrollment Events API for CAP Enrollment History functionality
 */
public class EnrollmentEventHistoryDTO implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String eventCreationDate;
	private String eventCreationTime;
	private String enrollmentEvent;
	private String enrollmentEventReason;
	private Integer revisionId;
	private String enrolleesName;
	private String eventCreatedBy;
	private String spclEnrollmentReason;
	private Integer subscriberEventId;
	private String userRole;
	
	
	/**
	 * @return the eventCreationDate
	 */
	public String getEventCreationDate() {
		return eventCreationDate;
	}
	/**
	 * @param eventCreationDate the eventCreationDate to set
	 */
	public void setEventCreationDate(String eventCreationDate) {
		this.eventCreationDate = eventCreationDate;
	}
	/**
	 * @return the eventCreationTime
	 */
	public String getEventCreationTime() {
		return eventCreationTime;
	}
	/**
	 * @param eventCreationTime the eventCreationTime to set
	 */
	public void setEventCreationTime(String eventCreationTime) {
		this.eventCreationTime = eventCreationTime;
	}
	/**
	 * @return the enrollmentEvent
	 */
	public String getEnrollmentEvent() {
		return enrollmentEvent;
	}
	/**
	 * @param enrollmentEvent the enrollmentEvent to set
	 */
	public void setEnrollmentEvent(String enrollmentEvent) {
		this.enrollmentEvent = enrollmentEvent;
	}
	/**
	 * @return the enrollmentEventReason
	 */
	public String getEnrollmentEventReason() {
		return enrollmentEventReason;
	}
	/**
	 * @param enrollmentEventReason the enrollmentEventReason to set
	 */
	public void setEnrollmentEventReason(String enrollmentEventReason) {
		this.enrollmentEventReason = enrollmentEventReason;
	}
	/**
	 * @return the revisionId
	 */
	public Integer getRevisionId() {
		return revisionId;
	}
	/**
	 * @param revisionId the revisionId to set
	 */
	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}
	/**
	 * @return the enrolleesName
	 */
	public String getEnrolleesName() {
		return enrolleesName;
	}
	/**
	 * @param enrolleesName the enrolleesName to set
	 */
	public void setEnrolleesName(String enrolleesName) {
		this.enrolleesName = enrolleesName;
	}
	/**
	 * @return the eventCreatedBy
	 */
	public String getEventCreatedBy() {
		return eventCreatedBy;
	}
	/**
	 * @param eventCreatedBy the eventCreatedBy to set
	 */
	public void setEventCreatedBy(String eventCreatedBy) {
		this.eventCreatedBy = eventCreatedBy;
	}
	/**
	 * @return the spclEnrollmentReason
	 */
	public String getSpclEnrollmentReason() {
		return spclEnrollmentReason;
	}
	/**
	 * @param spclEnrollmentReason the spclEnrollmentReason to set
	 */
	public void setSpclEnrollmentReason(String spclEnrollmentReason) {
		this.spclEnrollmentReason = spclEnrollmentReason;
	}
	public Integer getSubscriberEventId() {
		return subscriberEventId;
	}
	public void setSubscriberEventId(Integer subscriberEventId) {
		this.subscriberEventId = subscriberEventId;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
}
