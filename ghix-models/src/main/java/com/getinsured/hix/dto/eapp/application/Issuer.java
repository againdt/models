package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.util.Map;

import org.springframework.util.StringUtils;

/**
 * Insurance company
 * @author root
 *
 */
public class Issuer implements Serializable{
	private static final long serialVersionUID = 1L;
	private String brand;
	private String state;
	private String name;
	private String logoUrl;
	private String id;
	private String hiosIssuerId;
	private Map<String, Object> other;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	
	public Map<String, Object> getOther() {
		return other;
	}
	public void setOther(Map<String, Object> other) {
		this.other = other;
	}
	public static Issuer getSampleInstance(){
		Issuer issuer = new Issuer();
		issuer.setBrand("aetna");
		issuer.setState("FL");
		issuer.setName("Aetna Florida");
		issuer.setLogoUrl("https://972aa7cbf9b320dd3b1e-7a0a0f9fd6e850bb0070783e395dea5f.ssl.cf2.rackcdn.com/resources/img/2015/gi-poweredbyVimo-logo.png");
		issuer.setId("89874");
		
		return issuer;
	}
	
	public static Issuer getInstance(String carrier){
		Issuer issuer = new Issuer();
		issuer.setBrand(carrier);
		issuer.setState("FL");
		issuer.setName(StringUtils.capitalize(carrier) + " Florida");
		issuer.setLogoUrl("https://972aa7cbf9b320dd3b1e-7a0a0f9fd6e850bb0070783e395dea5f.ssl.cf2.rackcdn.com/resources/img/2015/gi-poweredbyVimo-logo.png");
		issuer.setId("76543");
		return issuer;
	}
	
}
