/**
 * 
 * @author santanu
 * @version 1.0
 * @since June 13, 2016 
 * 
 * This DTO to accept request object for Life Span Quoting of Enrollment
 */

package com.getinsured.hix.dto.planmgmt;

import java.util.List;

public class LifeSpanEnrollmentQuotingRequestDTO {
	
	private String planId;
	private List<LifeSpanPeriodLoopDTO> periodLoopList;
	
	
	/**
	 * @return the planId
	 */
	public String getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	
	/**
	 * @return the periodLoopList
	 */
	public List<LifeSpanPeriodLoopDTO> getPeriodLoopList() {
		return periodLoopList;
	}
	/**
	 * @param periodLoopList the periodLoopList to set
	 */
	public void setPeriodLoopList(List<LifeSpanPeriodLoopDTO> periodLoopList) {
		this.periodLoopList = periodLoopList;
	}  

}
