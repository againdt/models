package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.LookupValue;

@Entity
@IdClass(AudId.class)
@Table(name="ENROLLEE_RELATIONSHIP_AUD")
public class EnrolleeRelationshipAud implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	@Column(name = "REV")
	private int rev;
	
	
	
	
	@ManyToOne(cascade= {CascadeType.ALL})
    @JoinColumn(name="SOURCE_ENROLEE_ID")
	private Enrollee sourceEnrollee;
	
	@ManyToOne(cascade= {CascadeType.ALL})
    @JoinColumn(name="TARGET_ENROLEE_ID")
	private Enrollee targetEnrollee;

	@OneToOne
	@JoinColumn(name = "RELATIONSHIP_LKP")
	private LookupValue relationshipLkp;	
	

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP",nullable=false)
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP",nullable=false)
	private Date updatedOn;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name="created_by")
	private AccountUser createdBy;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;
	
	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Enrollee getTargetEnrollee() {
		return targetEnrollee;
	}

	public void setTargetEnrollee(Enrollee targetEnrollee) {
		this.targetEnrollee = targetEnrollee;
	}

	public Enrollee getSourceEnrollee() {
		return sourceEnrollee;
	}

	public void setSourceEnrollee(Enrollee sourceEnrollee) {
		this.sourceEnrollee = sourceEnrollee;
	}

	public LookupValue getRelationshipLkp() {
		return relationshipLkp;
	}

	public void setRelationshipLkp(LookupValue relationshipLkp) {
		this.relationshipLkp = relationshipLkp;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public int getRevType() {
		return revType;
	}

	public void setRevType(int revType) {
		this.revType = revType;
	}

	@Column(name = "REVTYPE")
	private int revType;
}
