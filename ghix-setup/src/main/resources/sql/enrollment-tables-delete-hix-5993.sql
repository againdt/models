DROP TABLE ENROLLMENT_EVENT;
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_CITIZENSHIP_STATUS_LKP";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_CUSTODIAL_ENROLLEE_ID";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_ENROLLEE_CREATED_BY";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_ENROLLEE_ENROLLMENT";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_ENROLLEE_HOME_LOCATION";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_ENROLLEE_MAILING_LOCATION";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_ENROLLEE_STATUS_LKP_LKP";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_ENROLLEE_UPDATED_BY";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_LANGUAGE_SPOKEN_LKP";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_LANGUAGE_WRITTEN_LKP";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_MARITAL_STATUS_LKP_ID";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_RESPONSIBLE_ENROLLEE_ID";
ALTER TABLE ENROLLEE DROP CONSTRAINT "FK_TOBACCO_USAGE_LKP";
ALTER TABLE ENROLLEE  DROP PRIMARY KEY CASCADE;
DROP TABLE ENROLLEE;
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_EMPID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_EMPLOYEE_PLAN";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_EMPR_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_FINANCIAL_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_IND_ORDER_ITEM";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_ISSUER_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENRLMNT_PLAN_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENROLLMENT_CREATED_BY";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENROLLMENT_STATUS_LKP_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENROLLMENT_TYPE_LKP_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_ENROLLMENT_UPDATED_BY";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_INSURANCE_LOOKUP_VALUE_ID";
ALTER TABLE ENROLLMENT DROP CONSTRAINT "FK_WORKSITE_ID_EMP_LOCATION_ID";
ALTER TABLE ENROLLMENT  DROP PRIMARY KEY CASCADE;
DROP TABLE ENROLLMENT;
DROP SEQUENCE ENROLLMENT_EVENT_SEQ;
DROP SEQUENCE ENROLLEE_SEQ;
DROP SEQUENCE ENROLLMENT_SEQ;
COMMIT;