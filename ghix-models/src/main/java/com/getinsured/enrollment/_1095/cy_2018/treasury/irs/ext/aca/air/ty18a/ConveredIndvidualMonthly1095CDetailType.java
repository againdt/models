
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Covered Individual&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for ConveredIndvidualMonthly1095CDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConveredIndvidualMonthly1095CDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CoveredIndividualName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}SSN" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}BirthDt" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CoveredIndividualYearRound1095CInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}CoveredIndivMonthly1095CIndGrp"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConveredIndvidualMonthly1095CDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "coveredIndividualName",
    "ssn",
    "birthDt",
    "coveredIndividualYearRound1095CInd",
    "coveredIndivMonthly1095CIndGrp",
    "errorDetail"
})
public class ConveredIndvidualMonthly1095CDetailType {

    @XmlElement(name = "CoveredIndividualName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CompletePersonNameDetailType coveredIndividualName;
    @XmlElement(name = "SSN", namespace = "urn:us:gov:treasury:irs:common")
    protected String ssn;
    @XmlElement(name = "BirthDt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDt;
    @XmlElement(name = "CoveredIndividualYearRound1095CInd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    @XmlSchemaType(name = "string")
    protected YOrNIndType coveredIndividualYearRound1095CInd;
    @XmlElement(name = "CoveredIndivMonthly1095CIndGrp", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MonthlyCoveredIndivIndGrpType coveredIndivMonthly1095CIndGrp;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the coveredIndividualName property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public CompletePersonNameDetailType getCoveredIndividualName() {
        return coveredIndividualName;
    }

    /**
     * Sets the value of the coveredIndividualName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public void setCoveredIndividualName(CompletePersonNameDetailType value) {
        this.coveredIndividualName = value;
    }

    /**
     * Gets the value of the ssn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSN() {
        return ssn;
    }

    /**
     * Sets the value of the ssn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSN(String value) {
        this.ssn = value;
    }

    /**
     * Gets the value of the birthDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDt() {
        return birthDt;
    }

    /**
     * Sets the value of the birthDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDt(XMLGregorianCalendar value) {
        this.birthDt = value;
    }

    /**
     * Gets the value of the coveredIndividualYearRound1095CInd property.
     * 
     * @return
     *     possible object is
     *     {@link YOrNIndType }
     *     
     */
    public YOrNIndType getCoveredIndividualYearRound1095CInd() {
        return coveredIndividualYearRound1095CInd;
    }

    /**
     * Sets the value of the coveredIndividualYearRound1095CInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link YOrNIndType }
     *     
     */
    public void setCoveredIndividualYearRound1095CInd(YOrNIndType value) {
        this.coveredIndividualYearRound1095CInd = value;
    }

    /**
     * Gets the value of the coveredIndivMonthly1095CIndGrp property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlyCoveredIndivIndGrpType }
     *     
     */
    public MonthlyCoveredIndivIndGrpType getCoveredIndivMonthly1095CIndGrp() {
        return coveredIndivMonthly1095CIndGrp;
    }

    /**
     * Sets the value of the coveredIndivMonthly1095CIndGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlyCoveredIndivIndGrpType }
     *     
     */
    public void setCoveredIndivMonthly1095CIndGrp(MonthlyCoveredIndivIndGrpType value) {
        this.coveredIndivMonthly1095CIndGrp = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
