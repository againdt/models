package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_practice_status_dom")
public class ProviderPracticeStatusDomain  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer practiceStatusId;
	private String practiceStatus;
	private Set<ProviderProduct> providerProducts = new HashSet<ProviderProduct>(0);

	@Id 
	@Column(name="PRACTICE_STATUS_ID", unique=true, nullable=false)
	public Integer getPracticeStatusId() {
		return this.practiceStatusId;
	}

	public void setPracticeStatusId(Integer practiceStatusId) {
		this.practiceStatusId = practiceStatusId;
	}


	@Column(name="PRACTICE_STATUS", length=50)
	public String getPracticeStatus() {
		return this.practiceStatus;
	}

	public void setPracticeStatus(String practiceStatus) {
		this.practiceStatus = practiceStatus;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerPracticeStatusDom")
	public Set<ProviderProduct> getProviderProducts() {
		return this.providerProducts;
	}

	public void setProviderProducts(Set<ProviderProduct> providerProducts) {
		this.providerProducts = providerProducts;
	}

}


