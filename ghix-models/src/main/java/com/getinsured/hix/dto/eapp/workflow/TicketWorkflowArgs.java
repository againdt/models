package com.getinsured.hix.dto.eapp.workflow;

import java.util.Map;


public class TicketWorkflowArgs {
	private String priority;
	private String subject;
	private String subTicketType;
	private String ticketType;
	private String description;
	
	
	public TicketWorkflowArgs() {
		super();
	}
	
	public TicketWorkflowArgs(Map<String,String> map){
		super();
		this.priority = map.get("priority");
		this.subject = map.get("subject");
		this.subTicketType = map.get("subTicketType");
		this.ticketType = map.get("ticketType");
		this.description = map.get("description");
	}
	
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTicketType() {
		return subTicketType;
	}
	public void setTicketType(String ticketType) {
		this.subTicketType = ticketType;
	}
	public String getWorkgroup() {
		return ticketType;
	}
	public void setWorkgroup(String workgroup) {
		this.ticketType = workgroup;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
