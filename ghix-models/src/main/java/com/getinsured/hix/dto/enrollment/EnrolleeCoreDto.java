package com.getinsured.hix.dto.enrollment;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.enrollment.Enrollee;

/**
 * 
 * @author Aditya_S
 * @since 26-05-2015
 *
 */
public class EnrolleeCoreDto extends Enrollee{

	public static enum ENROLLEE_ACTION{
		ADD,TERM,CHANGE,OTHER
	}

	private static final long serialVersionUID = 1689478674574504413L;

	private List<Map<String,String>> relationshipList;
	private Map<String, String> raceMap;

	/* Lookup Related String -- LookupValue Codes*/
	private String tobaccoUsageLkpStr;
	private String citizenshipStatusLkpStr;
	private String maritalStatusLkpStr;
	private String genderLkpStr;
	private String languageWrittenLkpStr;
	private String languageSpokenLkpStr;
	private String relationToHCPLkpStr;

	private ENROLLEE_ACTION enrolleeAction;

	private  EnrolleeCoreDto custodialParentDto; 

	private LocationCoreDto homeAddressDto;
	
	private LocationCoreDto mailingAddressDto;
	
	private String sercStr;
	
	public String getSercStr() {
		return sercStr;
	}

	public void setSercStr(String sercStr) {
		this.sercStr = sercStr;
	}

	public LocationCoreDto getHomeAddressDto() {
		return homeAddressDto;
	}

	public void setHomeAddressDto(LocationCoreDto homeAddressDto) {
		this.homeAddressDto = homeAddressDto;
	}

	public LocationCoreDto getMailingAddressDto() {
		return mailingAddressDto;
	}

	public void setMailingAddressDto(LocationCoreDto mailingAddressDto) {
		this.mailingAddressDto = mailingAddressDto;
	}

	public EnrolleeCoreDto getCustodialParentDto() {
		return custodialParentDto;
	}

	public void setCustodialParentDto(EnrolleeCoreDto custodialParentDto) {
		this.custodialParentDto = custodialParentDto;
	}

	public String getRelationToHCPLkpStr() {
		return relationToHCPLkpStr;
	}

	public void setRelationToHCPLkpStr(String relationToHCPLkpStr) {
		this.relationToHCPLkpStr = relationToHCPLkpStr;
	}

	/**
	 * @return the tobaccoUsageLkpStr
	 */
	public String getTobaccoUsageLkpStr() {
		return tobaccoUsageLkpStr;
	}

	/**
	 * @param tobaccoUsageLkpStr the tobaccoUsageLkpStr to set
	 */
	public void setTobaccoUsageLkpStr(String tobaccoUsageLkpStr) {
		this.tobaccoUsageLkpStr = tobaccoUsageLkpStr;
	}

	/**
	 * @return the citizenshipStatusLkpStr
	 */
	public String getCitizenshipStatusLkpStr() {
		return citizenshipStatusLkpStr;
	}

	/**
	 * @param citizenshipStatusLkpStr the citizenshipStatusLkpStr to set
	 */
	public void setCitizenshipStatusLkpStr(String citizenshipStatusLkpStr) {
		this.citizenshipStatusLkpStr = citizenshipStatusLkpStr;
	}

	/**
	 * @return the maritalStatusLkpStr
	 */
	public String getMaritalStatusLkpStr() {
		return maritalStatusLkpStr;
	}

	/**
	 * @param maritalStatusLkpStr the maritalStatusLkpStr to set
	 */
	public void setMaritalStatusLkpStr(String maritalStatusLkpStr) {
		this.maritalStatusLkpStr = maritalStatusLkpStr;
	}

	/**
	 * @return the genderLkpStr
	 */
	public String getGenderLkpStr() {
		return genderLkpStr;
	}

	/**
	 * @param genderLkpStr the genderLkpStr to set
	 */
	public void setGenderLkpStr(String genderLkpStr) {
		this.genderLkpStr = genderLkpStr;
	}

	/**
	 * @return the languageWrittenLkpStr
	 */
	public String getLanguageWrittenLkpStr() {
		return languageWrittenLkpStr;
	}

	/**
	 * @param languageWrittenLkpStr the languageWrittenLkpStr to set
	 */
	public void setLanguageWrittenLkpStr(String languageWrittenLkpStr) {
		this.languageWrittenLkpStr = languageWrittenLkpStr;
	}

	/**
	 * @return the languageSpokenLkpStr
	 */
	public String getLanguageSpokenLkpStr() {
		return languageSpokenLkpStr;
	}

	/**
	 * @param languageSpokenLkpStr the languageSpokenLkpStr to set
	 */
	public void setLanguageSpokenLkpStr(String languageSpokenLkpStr) {
		this.languageSpokenLkpStr = languageSpokenLkpStr;
	}



	/**
	 * @return the relationshipMap
	 */
	public List<Map<String,String>> getRelationshipList() {
		return relationshipList;
	}

	/**
	 * @param relationshipMap the relationshipMap to set
	 */
	public void setRelationshipList(List<Map<String,String>> relationshipList) {
		this.relationshipList = relationshipList;
	}

	/**
	 * @return the raceList
	 */
	public Map<String, String> getRaceMap() {
		return raceMap;
	}

	/**
	 * @param raceList the raceList to set
	 */
	public void setRaceMap(Map<String, String> raceMap) {
		this.raceMap = raceMap;
	}

	/**
	 * @return the enrolleeAction
	 */
	public ENROLLEE_ACTION getEnrolleeAction() {
		return enrolleeAction;
	}

	/**
	 * @param enrolleeAction the enrolleeAction to set
	 */
	public void setEnrolleeAction(ENROLLEE_ACTION enrolleeAction) {
		this.enrolleeAction = enrolleeAction;
	}
}
