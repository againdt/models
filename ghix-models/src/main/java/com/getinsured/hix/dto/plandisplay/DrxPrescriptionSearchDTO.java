package com.getinsured.hix.dto.plandisplay;

import java.util.List;

public class DrxPrescriptionSearchDTO {
	
	private static final long serialVersionUID = 1L;
	 
	 private String DrugID;
	 private String DrugName;
	 private String GenericDrugID;
	 private String DrugType;
	 private String GenericDrugName;
	 private List<DrxDosageDTO> Dosages;
	 

	 public String getDrugID() {
		return DrugID;
	}
	public void setDrugID(String drugID) {
		DrugID = drugID;
	}
	public String getDrugName() {
		return DrugName;
	}
	public void setDrugName(String drugName) {
		DrugName = drugName;
	}
	public String getGenericDrugID() {
		return GenericDrugID;
	}
	public void setGenericDrugID(String genericDrugID) {
		GenericDrugID = genericDrugID;
	}
	public String getDrugType() {
		return DrugType;
	}
	public void setDrugType(String drugType) {
		DrugType = drugType;
	}
	public String getGenericDrugName() {
		return GenericDrugName;
	}
	public void setGenericDrugName(String genericDrugName) {
		GenericDrugName = genericDrugName;
	}
	public List<DrxDosageDTO> getDosages() {
		return Dosages;
	}
	public void setDosages(List<DrxDosageDTO> dosages) {
		Dosages = dosages;
	}
}
