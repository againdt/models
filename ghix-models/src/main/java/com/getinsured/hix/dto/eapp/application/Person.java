package com.getinsured.hix.dto.eapp.application;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.dto.directenrollment.DirectEnrollmentMemberDetailsDTO;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;

/**
 * In case of health plans,
 * entity to be insured is an attribute of a Human being. 
 * 
 * A health/life related insurance can have one or more Person 
 * @author root
 *
 */
public class Person implements Serializable{
	private static final String FALSE = "false";

	private static final String TRUE = "true";

	private static final String Y = "Y";

	private static final long serialVersionUID = 1L;

	private Name name;
	private Integer applicantId;
	
	/**
	 * MM/dd/yyyy
	 */
	private String dob;
	private String gender;
	private String heightInFeet;
	private String weightInPounds;
	
	private String maritalStatus;
	private String tobaccoStatus;
    //HIX-84796 - remove String version of it after wider adoption
    //Kept both of them for backward compatibility
    private boolean tobaccoStatusBoolean;
	private String ethnicity;
	private CitizenshipInfo citizenshipInfo;
	private MemberRelationships relationshipToPrimary;
	private Address mailingAddress;
	private Address billingAddress;
	private Address homeAddress;
	private PhoneNumber primaryPhone;
	private PhoneNumber secondaryPhone;
	private String emailAddress;
	private String ssn;
	private int age;
	private Name esignName;
	
	/**
	 * Map<key, Map>
	 * All the other fields that we haven't captured already
	 */
	private Map<String, Object> other;
	public Name getEsignName() {
		return esignName;
	}

	public void setEsignName(Name esignName) {
		this.esignName = esignName;
	}

    public boolean isTobaccoStatusBoolean() {
        return tobaccoStatusBoolean;
    }

    public void setTobaccoStatusBoolean(boolean tobaccoStatusBoolean) {
        this.tobaccoStatusBoolean = tobaccoStatusBoolean;
    }

    public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHeightInFeet() {
		return heightInFeet;
	}

	public void setHeightInFeet(String heightInFeet) {
		this.heightInFeet = heightInFeet;
	}

	public String getWeightInPounds() {
		return weightInPounds;
	}

	public void setWeightInPounds(String weightInPounds) {
		this.weightInPounds = weightInPounds;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getTobaccoStatus() {
		return tobaccoStatus;
	}

	public void setTobaccoStatus(String tobaccoStatus) {
		this.tobaccoStatus = tobaccoStatus;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public CitizenshipInfo getCitizenshipInfo() {
		return citizenshipInfo;
	}

	public void setCitizenshipInfo(CitizenshipInfo citizenshipInfo) {
		this.citizenshipInfo = citizenshipInfo;
	}

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}

	public Map<String, Object> getOther() {
		return other;
	}

	public void setOther(Map<String, Object> other) {
		this.other = other;
	}
	
	public Address getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(Address mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}
	
	public PhoneNumber getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(PhoneNumber primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public PhoneNumber getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(PhoneNumber secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	public Address getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}
	
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	

	public Integer getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(Integer applicantId) {
		this.applicantId = applicantId;
	}

	public static Person getPerson(DirectEnrollmentMemberDetailsDTO member){
		Person person = new Person();
		person.setRelationshipToPrimary(member.getRelationshipToPrimary());
		person.setDob(member.getDateOfBirth());
		if(StringUtils.isNotBlank(member.getIsTobaccoUser()) && Y.equalsIgnoreCase(member.getIsTobaccoUser())){
			person.setTobaccoStatus(TRUE);
            person.setTobaccoStatusBoolean(true);
		}else{
			person.setTobaccoStatus(FALSE);
            person.setTobaccoStatusBoolean(false);
		}
		
		person.setAge(member.getAge());
		//we collect gender on some census pages like AME
		if(member.getGender()!=null){
			if(member.getGender().equalsIgnoreCase("male")){
				person.setGender("M");
			}else if(member.getGender().equalsIgnoreCase("female")){
				person.setGender("F");
			}
		}
		
		return person;
	}
	
	public static Person getSampleInstance(){
		Person person = new Person();
		person.setCitizenshipInfo(CitizenshipInfo.getSampleInstance());
		person.setDob(1+(int)Math.random()*12 + "/" + 1+(int)(28*Math.random()) + "/1985");
		person.setEthnicity("Hispanic");
		person.setGender("Male");
		person.setMaritalStatus("Single");
		person.setName(Name.getSampleInstance());
		person.setRelationshipToPrimary(MemberRelationships.SELF);
		person.setWeightInPounds("166");
		person.setHomeAddress(Address.getSampleInstance());
		person.setTobaccoStatus(TRUE);
		person.setBillingAddress(Address.getSampleInstance());
		person.setMailingAddress(Address.getSampleInstance());
		person.setEmailAddress("consumer@getinsured.com");
		person.setPrimaryPhone(PhoneNumber.getSampleInstance());
		person.setSecondaryPhone(PhoneNumber.getSampleInstance());
		person.setAge(30);
		person.setSsn("1234567");
		person.setApplicantId(1);
		person.setEsignName(person.getName());
		return person;
	}
	
	public static Person getAdultMale(){
		return getSampleInstance();
	}
	
	public static Person getAdultFemale(){
		Person person = new Person();
		person.setCitizenshipInfo(CitizenshipInfo.getSampleInstance());
		person.setDob(1+(int)Math.random()*12 + "/" + 1+(int)(28*Math.random()) + "/1986");
		person.setEthnicity("Hispanic");
		person.setGender("Female");
		person.setMaritalStatus("Single");
		person.setName(Name.getSampleInstance());
		person.setRelationshipToPrimary(MemberRelationships.SELF);
		person.setWeightInPounds("150");
		person.setTobaccoStatus(FALSE);
		person.setBillingAddress(Address.getSampleInstance());
		person.setMailingAddress(Address.getSampleInstance());
		person.setHomeAddress(Address.getSampleInstance());
		person.setEmailAddress("consumer@getinsured.com");
		person.setPrimaryPhone(PhoneNumber.getSampleInstance());
		person.setSecondaryPhone(PhoneNumber.getSampleInstance());
		person.setAge(29);
		person.setSsn("1234567");
		person.setApplicantId(2);
		person.setEsignName(person.getName());
		return person;
	}
	
	public static Person getKid(){
		Person person = new Person();
		person.setCitizenshipInfo(CitizenshipInfo.getSampleInstance());
		int yearBorn = (int)(10*Math.random());
		person.setDob(1+(int)Math.random()*12 + "/" + 1+(int)(28*Math.random()) + "/" + (2000 + yearBorn));
		person.setEthnicity("Hispanic");
		String gender = Math.random()>0.5?"Male":"Female";
		person.setGender(gender);
		person.setMaritalStatus("Single");
		person.setName(Name.getSampleInstance());
		person.setRelationshipToPrimary(MemberRelationships.CHILD);
		person.setWeightInPounds("150");
		person.setHomeAddress(Address.getSampleInstance());
		person.setTobaccoStatus(FALSE);
		person.setBillingAddress(Address.getSampleInstance());
		person.setMailingAddress(Address.getSampleInstance());
		person.setEmailAddress("consumer@getinsured.com");
		person.setPrimaryPhone(PhoneNumber.getSampleInstance());
		person.setSecondaryPhone(PhoneNumber.getSampleInstance());
		person.setSsn("1234567");
		person.setApplicantId(3);
		person.setEsignName(person.getName());
		person.setAge(15-yearBorn);
		return person;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", dob=" + dob + ", gender=" + gender
				+ ", heightInFeet=" + heightInFeet + ", weightInPounds="
				+ weightInPounds + ", maritalStatus=" + maritalStatus
				+ ", tobaccoStatus=" + tobaccoStatus + ", ethnicity="
				+ ethnicity + ", citizenshipInfo=" + citizenshipInfo
				+ ", relationshipToPrimary=" + relationshipToPrimary
				+ ", mailingAddress=" + mailingAddress + ", billingAddress="
				+ billingAddress + ", other=" + other + "]";
	}
	
	/**
	 * clean data if added by ui for formatting purpose
	 */
	public void sanitize(){
		if(!StringUtils.isEmpty(this.dob)){
			if(this.dob.contains("/")){
				this.dob = this.dob.replace("/", "");
			}
		}
	}
	
	public static void main(String args[]){
		Person person = new Person();
		person.setDob("12/12/2008");
		System.out.println(person);
		person.sanitize();
		System.out.println(person);
	}

}
