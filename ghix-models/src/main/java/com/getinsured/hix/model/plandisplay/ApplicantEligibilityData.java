/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityRequestType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityResponseType;

/**
 * @author Mishra_m
 *
 */
public class ApplicantEligibilityData extends GHIXResponse{
	
	private ApplicantEligibilityResponseType applicantEligibilityResponse;
	private ApplicantEligibilityRequestType ApplicantEligibilityRequest;
	private String eligLeadId;
	private String fFMAssignedConsumerId;
	private String ssapApplicationId;
	private Integer giWsPayloadId;
		
	public ApplicantEligibilityResponseType getApplicantEligibilityResponse() {
		return applicantEligibilityResponse;
	}
	public void setApplicantEligibilityResponse(
			ApplicantEligibilityResponseType applicantEligibilityResponse) {
		this.applicantEligibilityResponse = applicantEligibilityResponse;
	}
	public ApplicantEligibilityRequestType getApplicantEligibilityRequest() {
		return ApplicantEligibilityRequest;
	}
	public void setApplicantEligibilityRequest(
			ApplicantEligibilityRequestType applicantEligibilityRequest) {
		ApplicantEligibilityRequest = applicantEligibilityRequest;
	}
	public String getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(String eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	public String getfFMAssignedConsumerId() {
		return fFMAssignedConsumerId;
	}
	public void setfFMAssignedConsumerId(String fFMAssignedConsumerId) {
		this.fFMAssignedConsumerId = fFMAssignedConsumerId;
	}
	public String getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(String ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}
	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

}
