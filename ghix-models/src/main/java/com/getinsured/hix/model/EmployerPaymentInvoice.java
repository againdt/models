package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;


/**
 * The persistent class for the employer plans database table.
 * 
 */

@Audited
@Entity
@Table(name = "employer_payment_invoice")
public class EmployerPaymentInvoice implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employer_payinvoice_seq")
	@SequenceGenerator(name = "employer_payinvoice_seq", sequenceName = "employer_payinvoice_seq", allocationSize = 1)
	private int id;
	
	public enum IsActive{Y,N};
	
	@OneToOne
    @JoinColumn(name="invoice_id")
	private EmployerInvoices employerInvoices;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Date creationDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	// bi-directional many-to-one association to EmployerDetails
	@OneToOne(mappedBy = "employerPaymentInvoice", cascade = CascadeType.ALL)
	private EmployerPayments employerPayments;
	
	@Column(name = "excess_amount")
	private BigDecimal excessAmount;
	
	@Column(name = "gl_code")
	private Integer glCode;
	
	@Column(name = "confirm_number")
	private long confirmNumber;
	
	@Column(name = "is_active", length = 1)
	@Enumerated(EnumType.STRING)
	private IsActive isActive;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
		
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreationDate(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}
	
	public long getConfirmNumber() {
		return confirmNumber;
	}

	public void setConfirmNumber(long confirmNumber) {
		this.confirmNumber = confirmNumber;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployerInvoices getEmployerInvoices() {
		return employerInvoices;
	}

	public void setEmployerInvoices(EmployerInvoices employerInvoices) {
		this.employerInvoices = employerInvoices;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EmployerPayments getEmployerPayments() {
		return employerPayments;
	}

	public void setEmployerPayments(EmployerPayments employerPayments) {
		this.employerPayments = employerPayments;
	}

	public Integer getGlCode() {
		return glCode;
	}

	public void setGlCode(Integer glCode) {
		this.glCode = glCode;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getExcessAmount() {
		return excessAmount;
	}

	public void setExcessAmount(BigDecimal excessAmount) {
		this.excessAmount = excessAmount;
	}

	/**
	 * @return the isActive
	 */
	public IsActive getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(IsActive isActive) {
		this.isActive = isActive;
	}
	
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
