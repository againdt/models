package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * 
 * @author hardas_d
 * The entity holds the tasks of a ticket.
 */
@Audited
@Entity
@Table(name = "tkm_ticket_tasks")
public class TkmTicketTasks implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum task_status
	{
		Claimed("Claimed"), Closed("Closed"), UnClaimed("UnClaimed"), Canceled("Canceled");
		
		@SuppressWarnings("unused")
		private final String task_status;

		private task_status(String status) {
			task_status = status;
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TkmTicketTasks_Seq")
	@SequenceGenerator(name = "TkmTicketTasks_Seq", sequenceName = "tkm_ticket_tasks_seq", allocationSize = 1)
	@Column(name = "tkm_ticket_task_id")
	private Integer taskId;

	@Column(name = "status")
	private String status;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "due_date")
	private Date dueDate;

	@Column(name = "created_by")
	private Integer creator;

	@Column(name = "last_updated_by")
	private Integer updator;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tkm_ticket_id")
	private TkmTickets ticket;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tkm_queue_id")
	private TkmQueues queue;
	
	@NotAudited
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "assignee")
	private AccountUser assignee;
	
	@Column(name="name")
	private String taskName;

	@Column(name="tkm_instance_id")
	private String processInstanceId;
	
	@Column(name="comments")
	private String comments;
	
	@Column(name="activiti_task_id")
	private String activitiTaskId;
	
	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public Integer getUpdator() {
		return updator;
	}

	public void setUpdator(Integer updator) {
		this.updator = updator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public TkmTickets getTicket() {
		return ticket;
	}

	public void setTicket(TkmTickets ticket) {
		this.ticket = ticket;
	}

	public TkmQueues getQueue() {
		return queue;
	}

	public void setQueue(TkmQueues queue) {
		this.queue = queue;
	}

	public AccountUser getAssignee() {
		return assignee;
	}

	public void setAssignee(AccountUser assignee) {
		this.assignee = assignee;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getActivitiTaskId() {
		return activitiTaskId;
	}

	public void setActivitiTaskId(String activitiTaskId) {
		this.activitiTaskId = activitiTaskId;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	@Override
	public String toString() {
		return "TkmTicketTasks [taskId=" + taskId + ", status=" + status
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", dueDate=" + dueDate + ", creator=" + creator
				+ ", updator=" + updator + ", created=" + created
				+ ", updated=" + updated + ", ticket=" + ticket + ", queue="
				+ queue + ", assignee=" + assignee + "]";
	}
}
