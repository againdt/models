TRUNCATE EXCHG_PARTNERLOOKUP;

-- hios_issuer_id = 85736
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 34102
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;
-- hios_issuer_id = 63485
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 57129
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 31616
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 26825
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = CMS
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound','ZZ','CMSFFM','ZZ','FEP0106ID','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,17,current_timestamp,current_timestamp,6)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Outbound','ZZ','FEP0106ID','ZZ','CMSFFM','T',NULL,'ID0','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.2_MN__CMS.apf',16,current_timestamp,current_timestamp,5)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound','ZZ','CMSFFM','ZZ','FEP0106ID','T','ID0',NULL,'005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.2_MN__CMS.apf',18,current_timestamp,current_timestamp,6)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,19,current_timestamp,current_timestamp,1)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,20,current_timestamp,current_timestamp,3)
;

-- hios_issuer_id = IRS
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/batch/ghixhome/ghix-docs/irs/IRSReportHubFTPFolder','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,21,current_timestamp,current_timestamp,7)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/IRSMonthlyResponse','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,22,current_timestamp,current_timestamp,8)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/IRSMonthlyResponse','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,23,current_timestamp,current_timestamp,8)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/IRSMonthlyResponse','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,24,current_timestamp,current_timestamp,8)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,25,current_timestamp,current_timestamp,9)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,26,current_timestamp,current_timestamp,9)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,27,current_timestamp,current_timestamp,9)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,28,current_timestamp,current_timestamp,9)
;

TRUNCATE EXCHG_CONTROLNUM;

INSERT INTO EXCHG_CONTROLNUM (ID, HIOS_ISSUER_ID, CONTROL_NUM)  values
( nextval('exchg_controlnum_seq'), '34102', '1' ),
( nextval('exchg_controlnum_seq'), '63485', '1' ),
( nextval('exchg_controlnum_seq'), '57129', '1' ),
( nextval('exchg_controlnum_seq'), '31616', '1' ),
( nextval('exchg_controlnum_seq'), '26825', '1' ),
( nextval('exchg_controlnum_seq'), '85736', '1' ),
( nextval('exchg_controlnum_seq'), 'CMS', '1' ),
( nextval('exchg_controlnum_seq'), 'IRS', '1' );

TRUNCATE EXCHG_FILEPATTERN;

INSERT INTO EXCHG_FILEPATTERN (id,direction,filetype,gs08,filepattern,filepattern_ext_pre,filepattern_ext_post,xref_ids) VALUES 
 (1,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_INDV_<YYYYMMDDHHMMSS>','xml','edi','')
,(2,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_SHOP_<YYYYMMDDHHMMSS>','xml','edi','')
,(3,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_RECONINDV_<YYYYMMDDHHMMSS>','xml','edi','')
,(4,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_RECONSHOP_<YYYYMMDDHHMMSS>','xml','edi','')
,(5,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_RECONSUPPL_<YYYYMMDDHHMMSS>','xml','edi','')
,(6,'O','820','005010X306','to_<HIOS_Issuer_ID>_<state>_820_SHOP_<YYYYMMDDHHMMSS>','xml','edi','')
,(7,'O','TA1','','to_<HIOS_Issuer_ID>_<state>_TA1_834_INDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(8,'O','TA1','','to_<HIOS_Issuer_ID>_<state>_TA1_834_SHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(9,'O','TA1','','to_<HIOS_Issuer_ID>_<state>_TA1_820_INDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(10,'O','999','005010X231A1','to_<HIOS_Issuer_ID>_<state>_999_834_INDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(11,'O','999','005010X231A1','to_<HIOS_Issuer_ID>_<state>_999_834_SHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(12,'O','999','005010X231A1','to_<HIOS_Issuer_ID>_<state>_999_820_INDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(13,'I','834','005010X220A1','from_<HIOS_Issuer_ID>_<state>_834_INDV_<YYYYMMDDHHMMSS>','edi','xml','7,10')
,(14,'I','834','005010X220A1','from_<HIOS_Issuer_ID>_<state>_834_SHOP_<YYYYMMDDHHMMSS>','edi','xml','8,11')
,(15,'I','820','005010X306','from_<HIOS_Issuer_ID>_<state>_820_INDV_<YYYYMMDDHHMMSS>','edi','xml','9,12')
,(16,'I','TA1','','from_<HIOS_Issuer_ID>_<state>_TA1_834_INDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(17,'I','TA1','','from_<HIOS_Issuer_ID>_<state>_TA1_834_SHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(18,'I','TA1','','from_<HIOS_Issuer_ID>_<state>_TA1_834_RECONINDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(19,'I','TA1','','from_<HIOS_Issuer_ID>_<state>_TA1_834_RECONSHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(20,'I','TA1','','from_<HIOS_Issuer_ID>_<state>_TA1_834_RECONSUPPL_<YYYYMMDDHHMMSS>','edi','edi','')
,(21,'I','TA1','','from_<HIOS_Issuer_ID>_<state>_TA1_820_SHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(22,'I','999','005010X231A1','from_<HIOS_Issuer_ID>_<state>_999_834_INDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(23,'I','999','005010X231A1','from_<HIOS_Issuer_ID>_<state>_999_834_SHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(24,'I','999','005010X231A1','from_<HIOS_Issuer_ID>_<state>_999_834_RECONINDV_<YYYYMMDDHHMMSS>','edi','edi','')
,(25,'I','999','005010X231A1','from_<HIOS_Issuer_ID>_<state>_999_834_RECONSHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(26,'I','999','005010X231A1','from_<HIOS_Issuer_ID>_<state>_999_834_RECONSUPPL_<YYYYMMDDHHMMSS>','edi','edi','')
,(27,'I','999','005010X231A1','from_<HIOS_Issuer_ID>_<state>_999_820_SHOP_<YYYYMMDDHHMMSS>','edi','edi','')
,(28,'O','PAS','','<HIOS_Issuer_ID>.GROP.D<YYMMDD>.T<HHMMSSt>.<ISA15>','OUT','OUT','')
,(29,'O','834','005010X220A1','<CMS_IRS_TPID>.DSH.IS834.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN','xml','','')
,(30,'I','TA1','','<CMS_IRS_TPID>.TA1.D<YYMMDD>.T<HHMMSSt>.<ISA15>.OUT','','edi','')
,(31,'I','999','005010X231A1','<CMS_IRS_TPID>.999.D<YYMMDD>.T<HHMMSSt>.<ISA15>.OUT','','edi','')
,(32,'I','PAS','','<CMS_IRS_TPID>.I834BA.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(33,'I','PAS','','<CMS_IRS_TPID>.I834BA.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(34,'O','PAS','','<CMS_IRS_TPID>.DSH.EOMIN.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN','','','')
,(35,'I','PAS','','<CMS_IRS_TPID>.EOMIN.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN.errlog','','','')
,(36,'I','PAS','','<CMS_IRS_TPID>.EOMNAK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(37,'I','PAS','','<CMS_IRS_TPID>.EOMNAK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(38,'I','PAS','','<CMS_IRS_TPID>.EOMACK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(39,'I','PAS','','<CMS_IRS_TPID>.EOMACK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(40,'I','PAS','','<CMS_IRS_TPID>.EOMOUT.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(41,'I','PAS','','<CMS_IRS_TPID>.EOMOUT.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(42,'O','PAS','','<CMS_IRS_TPID>.DSH.EOYIN.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN','','','')
,(43,'I','PAS','','<CMS_IRS_TPID>.EOYIN.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN.errlog','','','')
,(44,'I','PAS','','<CMS_IRS_TPID>.EOYNAK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(45,'I','PAS','','<CMS_IRS_TPID>.EOYNAK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(46,'I','PAS','','<CMS_IRS_TPID>.EOYACK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(47,'I','PAS','','<CMS_IRS_TPID>.EOYACK.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(48,'I','PAS','','<CMS_IRS_TPID>.EOYOUT.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT','','','')
,(49,'I','PAS','','<CMS_IRS_TPID>.EOYOUT.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.OUT.errlog','','','')
,(50,'O','PAS','','<CMS_IRS_TPID>.MID.SDBI<YY>.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN','','','')
,(51,'I','PAS','','<CMS_IRS_TPID>.SDBI<YY>.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN.errlog','','','')
,(52,'I','PAS','','<CMS_IRS_TPID>.MID.SDBI<YY>.D<YYMMDD>.T<HHMMSSmmm>.<ISA15>.IN.errlog','','','')
,(53,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_RENTERM_<YYYYMMDDHHMMSS>','xml','edi','')
,(54,'O','834','005010X220A1','to_<HIOS_Issuer_ID>_<state>_834_INDV_<YYYYMMDDHHMMSS>.<YYYY>','xml','edi','')
,(55,'O','PAS','','to_<HIOS_Issuer_ID>_INDV_MONTHLYDISCREPANCY_<YYYY>_<HHMMSSmmm>.OUT','','','')
,(56,'I','PAS','','from_<HIOS_Issuer_ID>_INDV_MONTHLYRECON_<YYYY>_<YYYYMMDDHHMMSS>.IN','','','')
;

TRUNCATE EXCHG_FILEPATTERNROLE;

INSERT INTO EXCHG_FILEPATTERNROLE (id,name,filepattern_ids) VALUES 
 (1,'5010 Individual 834out','1,3,5,53,54')
,(2,'5010 SHOP 834out','2,4')
,(3,'5010 Individual 834in','13')
,(4,'5010 SHOP 834in','14')
,(5,'5010 SHOP 820out','6')
,(6,'5010 Individual 820in','15')
,(7,'5010 Individual TA1out','7,9')
,(8,'5010 SHOP TA1out','8')
,(9,'5010 Individual TA1in','16,18,20')
,(10,'5010 SHOP TA1in','17,19,21')
,(11,'5010 Individual 999out','10,12')
,(12,'5010 SHOP 999out','11')
,(13,'5010 Individual 999in','22,24,26')
,(14,'5010 SHOP 999in','23,25,27')
,(15,'Group Installation Out','28')
,(16,'5010 CMS 834out','29')
,(17,'5010 CMS TA1in','30')
,(18,'5010 CMS 999in','31')
,(19,'5010 CMS I834BA PASSTHRUin','32')
,(20,'5010 CMS errlog PASSTHRUin','33')
,(21,'5010 IRS EOMIN  PASSTHRUout','34')
,(22,'5010 IRS EOMNAK PASSTHRUin','36')
,(23,'5010 IRS EOMACK PASSTHRUin','38')
,(24,'5010 IRS EOMOUT PASSTHRUin','40')
,(25,'5010 IRS EOMIN  errlog PASSTHRUin','35')
,(26,'5010 IRS EOMNAK errlog PASSTHRUin','37')
,(27,'5010 IRS EOMACK errlog PASSTHRUin','39')
,(28,'5010 IRS EOMOUT errlog PASSTHRUin','41')
,(29,'5010 IRS EOYIN  PASSTHRUout','42')
,(30,'5010 IRS EOYIN, EOYNAK, EOYACK, EOYOUT PASSTHRUin','44,46,48')
,(31,'5010 IRS EOYIN, EOYNAK, EOYACK, EOYOUT errlog PASSTHRUin','43,45,47,49')
,(32,'5010 IRS PLR PASSTHRUout','50')
,(33,'5010 IRS PLR errlog PASSTHRUout','51,52')
,(34,'5010 MONTHLY RECON OUT','55')
,(35,'5010 MONTHLY RECON OUT','56')
;
