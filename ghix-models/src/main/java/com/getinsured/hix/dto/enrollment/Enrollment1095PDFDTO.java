package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
public class Enrollment1095PDFDTO implements Serializable{

	@Expose
	private String referenceNumber;
	private String pageNumber;
	
	private String voidFlag;
	private String correctedFlag;
	private String marketPlaceIdentifier;
	@Expose
	private String assignedPolicyNumber;
	private String policyIssuersName;
	@Expose
	private String recipientName;
	private String recipientSSN;
	private String recipientDOB;
	private String policyStartDate;
	private String policyTermDate;
	@Expose
	private String streetAddress;
	@Expose
	private String city;
	@Expose
	private String state;
	@Expose
	private String countryAndZip;
	@Expose
	private String address;
	
	private String spouseSSN;
	@Expose
	private String spouseName;
	private String spouseDOB;
	
	
	private EnrollmentPremium1095PDFDTO janEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO febEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO marEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO aprEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO mayEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO junEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO julEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO augEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO sepEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO octEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO novEnrollmentPremium;
	private EnrollmentPremium1095PDFDTO decEnrollmentPremium;
	
	private String annualPremium;
	private String annualSLCSP;
	private String annualAPTC;
	
	@Expose
	private List<EnrollmentMember1095PDFDTO> coveredIndividuals;

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getVoidFlag() {
		return voidFlag;
	}

	public void setVoidFlag(String voidFlag) {
		this.voidFlag = voidFlag;
	}

	public String getCorrectedFlag() {
		return correctedFlag;
	}

	public void setCorrectedFlag(String correctedFlag) {
		this.correctedFlag = correctedFlag;
	}

	public String getMarketPlaceIdentifier() {
		return marketPlaceIdentifier;
	}

	public void setMarketPlaceIdentifier(String marketPlaceIdentifier) {
		this.marketPlaceIdentifier = marketPlaceIdentifier;
	}

	public String getAssignedPolicyNumber() {
		return assignedPolicyNumber;
	}

	public void setAssignedPolicyNumber(String assignedPolicyNumber) {
		this.assignedPolicyNumber = assignedPolicyNumber;
	}

	public String getPolicyIssuersName() {
		return policyIssuersName;
	}

	public void setPolicyIssuersName(String policyIssuersName) {
		this.policyIssuersName = policyIssuersName;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getRecipientSSN() {
		return recipientSSN;
	}

	public void setRecipientSSN(String recipientSSN) {
		this.recipientSSN = recipientSSN;
	}

	public String getRecipientDOB() {
		return recipientDOB;
	}

	public void setRecipientDOB(String recipientDOB) {
		this.recipientDOB = recipientDOB;
	}

	public String getPolicyStartDate() {
		return policyStartDate;
	}

	public void setPolicyStartDate(String policyStartDate) {
		this.policyStartDate = policyStartDate;
	}

	public String getPolicyTermDate() {
		return policyTermDate;
	}

	public void setPolicyTermDate(String policyTermDate) {
		this.policyTermDate = policyTermDate;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public EnrollmentPremium1095PDFDTO getJanEnrollmentPremium() {
		return janEnrollmentPremium;
	}

	public void setJanEnrollmentPremium(
			EnrollmentPremium1095PDFDTO janEnrollmentPremium) {
		this.janEnrollmentPremium = janEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getFebEnrollmentPremium() {
		return febEnrollmentPremium;
	}

	public void setFebEnrollmentPremium(
			EnrollmentPremium1095PDFDTO febEnrollmentPremium) {
		this.febEnrollmentPremium = febEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getMarEnrollmentPremium() {
		return marEnrollmentPremium;
	}

	public void setMarEnrollmentPremium(
			EnrollmentPremium1095PDFDTO marEnrollmentPremium) {
		this.marEnrollmentPremium = marEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getAprEnrollmentPremium() {
		return aprEnrollmentPremium;
	}

	public void setAprEnrollmentPremium(
			EnrollmentPremium1095PDFDTO aprEnrollmentPremium) {
		this.aprEnrollmentPremium = aprEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getMayEnrollmentPremium() {
		return mayEnrollmentPremium;
	}

	public void setMayEnrollmentPremium(
			EnrollmentPremium1095PDFDTO mayEnrollmentPremium) {
		this.mayEnrollmentPremium = mayEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getJunEnrollmentPremium() {
		return junEnrollmentPremium;
	}

	public void setJunEnrollmentPremium(
			EnrollmentPremium1095PDFDTO junEnrollmentPremium) {
		this.junEnrollmentPremium = junEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getJulEnrollmentPremium() {
		return julEnrollmentPremium;
	}

	public void setJulEnrollmentPremium(
			EnrollmentPremium1095PDFDTO julEnrollmentPremium) {
		this.julEnrollmentPremium = julEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getAugEnrollmentPremium() {
		return augEnrollmentPremium;
	}

	public void setAugEnrollmentPremium(
			EnrollmentPremium1095PDFDTO augEnrollmentPremium) {
		this.augEnrollmentPremium = augEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getSepEnrollmentPremium() {
		return sepEnrollmentPremium;
	}

	public void setSepEnrollmentPremium(
			EnrollmentPremium1095PDFDTO sepEnrollmentPremium) {
		this.sepEnrollmentPremium = sepEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getOctEnrollmentPremium() {
		return octEnrollmentPremium;
	}

	public void setOctEnrollmentPremium(
			EnrollmentPremium1095PDFDTO octEnrollmentPremium) {
		this.octEnrollmentPremium = octEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getNovEnrollmentPremium() {
		return novEnrollmentPremium;
	}

	public void setNovEnrollmentPremium(
			EnrollmentPremium1095PDFDTO novEnrollmentPremium) {
		this.novEnrollmentPremium = novEnrollmentPremium;
	}

	public EnrollmentPremium1095PDFDTO getDecEnrollmentPremium() {
		return decEnrollmentPremium;
	}

	public void setDecEnrollmentPremium(
			EnrollmentPremium1095PDFDTO decEnrollmentPremium) {
		this.decEnrollmentPremium = decEnrollmentPremium;
	}

	public List<EnrollmentMember1095PDFDTO> getCoveredIndividuals() {
		return coveredIndividuals;
	}

	public void setCoveredIndividuals(
			List<EnrollmentMember1095PDFDTO> coveredIndividuals) {
		this.coveredIndividuals = coveredIndividuals;
	}

	public String getSpouseSSN() {
		return spouseSSN;
	}

	public void setSpouseSSN(String spouseSSN) {
		this.spouseSSN = spouseSSN;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getSpouseDOB() {
		return spouseDOB;
	}

	public void setSpouseDOB(String spouseDOB) {
		this.spouseDOB = spouseDOB;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountryAndZip() {
		return countryAndZip;
	}

	public void setCountryAndZip(String countryAndZip) {
		this.countryAndZip = countryAndZip;
	}

	public String getAnnualPremium() {
		return annualPremium;
	}

	public void setAnnualPremium(String annualPremium) {
		this.annualPremium = annualPremium;
	}

	public String getAnnualSLCSP() {
		return annualSLCSP;
	}

	public void setAnnualSLCSP(String annualSLCSP) {
		this.annualSLCSP = annualSLCSP;
	}

	public String getAnnualAPTC() {
		return annualAPTC;
	}

	public void setAnnualAPTC(String annualAPTC) {
		this.annualAPTC = annualAPTC;
	}

	/**
	 * @return the pageNumber
	 */
	public String getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}
}
