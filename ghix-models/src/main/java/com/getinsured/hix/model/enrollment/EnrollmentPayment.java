package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.hix.model.FinancialInfo;


/**
 * The persistent class for the households database table.
 * 
 */
@Entity
@Table(name="ENROLLMENT_PAYMENTS")
public class EnrollmentPayment implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Status 	{ 
		ACTIVE,CANCELLED,ERROR; 
	}  
	
	public enum PaymentMethod 	{ 
		SINGLE,RECURRING; 
	}  
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EnrollmentPayment_Seq")
	@SequenceGenerator(name = "EnrollmentPayment_Seq", sequenceName = "ENROLLMENT_PAYMENTS_SEQ", allocationSize = 1)
	private int id;

	//bi-directional many-to-one association to Household
    @ManyToOne
    @JoinColumn(name="ENROLLMENT_ID")
	private Enrollment enrollment;
    
  //bi-directional many-to-one association to Household
    @ManyToOne
    @JoinColumn(name="financial_info_id")
	private FinancialInfo financialInfo;
    
    @Column(name="payment_method")
    @Enumerated(EnumType.STRING)  
	private PaymentMethod paymentMethod;
    
    @Column(name="status")
	@Enumerated(EnumType.STRING)  
	private Status status;
    
    @Column(name="amount")
   	private String amount;
    
    @Column(name="notes")
    private String notes;
    
    @Column(name="transaction_id")
	private String transaction_id;
    
    @Temporal( TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;
    
	@Column(name="response_text")
	private String responseText;
    
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="created")
	private Date created;
	
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="updated")
	private Date updated;
    
	public EnrollmentPayment() {
    }
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public FinancialInfo getFinancialInfo() {
		return financialInfo;
	}

	public void setFinancialInfo(FinancialInfo financialInfo) {
		this.financialInfo = financialInfo;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getResponseText() {
		return responseText;
	}

	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate()); 
	}

}
