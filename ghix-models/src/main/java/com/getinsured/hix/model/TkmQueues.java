package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author hardas_d
 * The entity holds the information of the queues and their respective user groups.
 */
@Entity
@Table(name="tkm_queues")
public class TkmQueues implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TkmQueues_Seq")
	@SequenceGenerator(name="TkmQueues_Seq", sequenceName="tkm_queues_seq", allocationSize=1)
	@Column(name="tkm_queue_id")
	private Integer id;

	@Column(name="queue_name")
	private String name;
	
	@Column(name="role_id")
	private Integer roleId;

	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp", nullable=false)
	private Date created;

	@Column(name="created_by")
	private Integer creator;

	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp")
	private Date updated;
	
	@Column(name="last_updated_by")
	private Integer updator;
	
	@Column(name="IS_DEFAULT")
	private String isDefault;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getUpdator() {
		return updator;
	}

	public void setUpdator(Integer updator) {
		this.updator = updator;
	}
	
	public String getIsDefault() {
		return isDefault;
	}
	
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}

	@Override
	public String toString() {
		return "TkmQueues [id=" + id + ", name=" + name + ", roleId=" + roleId
				+ ", created=" + created + ", creator=" + creator
				+ ", updated=" + updated + ", updator=" + updator + "]";
	}
}
