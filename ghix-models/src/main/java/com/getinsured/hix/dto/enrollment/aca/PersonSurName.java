package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Awesome Pojo Generator
 * */
public class PersonSurName{
  @JsonProperty("Value")
  
  private String Value;
  public void setValue(String Value){
   this.Value=Value;
  }
  public String getValue(){
   return Value;
  }
}