package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class ReconAdditionalSubInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	    private Integer row ;
		private String exchangeAssignedSubscriberId;
		private String exchangeAssignedMemberId;
		private String issuerAssignedMemberId;
		private String issuerAssignedPolicyId;
		private String benefitStartDate;
		private String benefitEndDate;
		private Float aptcAmt;
		private String aptcEffectiveDate;
		private String aptcEndDate;
		private Float csrAmt;
		private String csrEffectiveDate;
		private String csrEndDate;
		private Float grossPremium;
		private String grossPremiumEffectiveDate;
		private String grossPremiumEndDate;
		public Integer getRow() {
			return row;
		}
		public void setRow(Integer row) {
			this.row = row;
		}
		public String getExchangeAssignedSubscriberId() {
			return exchangeAssignedSubscriberId;
		}
		public void setExchangeAssignedSubscriberId(String exchangeAssignedSubscriberId) {
			this.exchangeAssignedSubscriberId = exchangeAssignedSubscriberId;
		}
		public String getExchangeAssignedMemberId() {
			return exchangeAssignedMemberId;
		}
		public void setExchangeAssignedMemberId(String exchangeAssignedMemberId) {
			this.exchangeAssignedMemberId = exchangeAssignedMemberId;
		}
		public String getIssuerAssignedMemberId() {
			return issuerAssignedMemberId;
		}
		public void setIssuerAssignedMemberId(String issuerAssignedMemberId) {
			this.issuerAssignedMemberId = issuerAssignedMemberId;
		}
		public String getIssuerAssignedPolicyId() {
			return issuerAssignedPolicyId;
		}
		public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
			this.issuerAssignedPolicyId = issuerAssignedPolicyId;
		}
		public String getBenefitStartDate() {
			return benefitStartDate;
		}
		public void setBenefitStartDate(String benefitStartDate) {
			this.benefitStartDate = benefitStartDate;
		}
		public String getBenefitEndDate() {
			return benefitEndDate;
		}
		public void setBenefitEndDate(String benefitEndDate) {
			this.benefitEndDate = benefitEndDate;
		}
		public Float getAptcAmt() {
			return aptcAmt;
		}
		public void setAptcAmt(Float aptcAmt) {
			this.aptcAmt = aptcAmt;
		}
		public String getAptcEffectiveDate() {
			return aptcEffectiveDate;
		}
		public void setAptcEffectiveDate(String aptcEffectiveDate) {
			this.aptcEffectiveDate = aptcEffectiveDate;
		}
		public String getAptcEndDate() {
			return aptcEndDate;
		}
		public void setAptcEndDate(String aptcEndDate) {
			this.aptcEndDate = aptcEndDate;
		}
		public Float getCsrAmt() {
			return csrAmt;
		}
		public void setCsrAmt(Float csrAmt) {
			this.csrAmt = csrAmt;
		}
		public String getCsrEffectiveDate() {
			return csrEffectiveDate;
		}
		public void setCsrEffectiveDate(String csrEffectiveDate) {
			this.csrEffectiveDate = csrEffectiveDate;
		}
		public String getCsrEndDate() {
			return csrEndDate;
		}
		public void setCsrEndDate(String csrEndDate) {
			this.csrEndDate = csrEndDate;
		}
		public Float getGrossPremium() {
			return grossPremium;
		}
		public void setGrossPremium(Float grossPremium) {
			this.grossPremium = grossPremium;
		}
		public String getGrossPremiumEffectiveDate() {
			return grossPremiumEffectiveDate;
		}
		public void setGrossPremiumEffectiveDate(String grossPremiumEffectiveDate) {
			this.grossPremiumEffectiveDate = grossPremiumEffectiveDate;
		}
		public String getGrossPremiumEndDate() {
			return grossPremiumEndDate;
		}
		public void setGrossPremiumEndDate(String grossPremiumEndDate) {
			this.grossPremiumEndDate = grossPremiumEndDate;
		}
		
		
}
