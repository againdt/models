
package com.serff.planmanagementserffapi.serff.model.service;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;


public class ExchangeAttachment {

    protected String fileName;
    protected DataHandler exchangeSuppliedData;

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the exchangeSuppliedData property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getExchangeSuppliedData() {
        return exchangeSuppliedData;
    }

    /**
     * Sets the value of the exchangeSuppliedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setExchangeSuppliedData(DataHandler value) {
        this.exchangeSuppliedData = value;
    }

}
