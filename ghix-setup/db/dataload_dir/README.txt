1.	Create directory /home/oracle/dataload_dir on database server to run these scripts

2.	Copy these file to  /home/oracle/dataload_dir

3.	Make the *.sh are executable

4.	Update the DB_ENV.sh appropriately

5.	Setup privilege and create external table
	Make sure Oracle account DEV_JT and its password are correct in /home/oracle/dataload_dir/setup_load4zipcodes.sh
	or make the changes accordingly.
	
	Run
/home/oracle/dataload_dir/setup_load4zipcodes.sh <SCHEMA>_OWNER  <PASSWORD>

6.	If there is new zip codes csv file, do

cp -p /home/oracle/dataload_dir/CZ_zipcodes.csv /home/oracle/dataload_dir/CZ_zipcodes_b4`date '+%Y%m%d%H%M%S'`.csv

cp Zipcodes.txt CZ_zipcodes.csv

7.	Refresh the zipodes table
	Make sure Oracle account DEV_JT and its password are correct in  /home/oracle/dataload_dir/refresh_zipcodes.sh
	or make the changes accordingly
	
	Run
/home/oracle/dataload_dir/refresh_zipcodes.sh  <SCHEMA>_OWNER  <PASSWORD>