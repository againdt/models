package com.getinsured.eligibility.ssap.medicaid;

/*
 * @author jape_g
 * 
 */
public class DetermineEligibilityRequest {
	
	private Long applicationId;
	private Double income;
	
	
	
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	public Double getIncome() {
		return income;
	}
	public void setIncome(Double income) {
		this.income = income;
	}
	
	
}
