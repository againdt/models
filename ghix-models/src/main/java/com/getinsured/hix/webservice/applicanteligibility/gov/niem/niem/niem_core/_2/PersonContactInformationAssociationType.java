//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ReferenceType;



/**
 * A relationship A data type for an association between a person and contact information.
 * 
 * <p>Java class for PersonContactInformationAssociationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonContactInformationAssociationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}AssociationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactInformationReference"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactInformationIsPrimaryIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactInformationIsHomeIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactInformationIsWorkIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonContactInformationAssociationType", propOrder = {
    "contactInformationReference",
    "contactInformationIsPrimaryIndicator",
    "contactInformationIsHomeIndicator",
    "contactInformationIsWorkIndicator"
})
@XmlSeeAlso({
    com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.PersonContactInformationAssociationType.class
})
public class PersonContactInformationAssociationType
    extends AssociationType
{

    @XmlElement(name = "ContactInformationReference", required = true)
    protected ReferenceType contactInformationReference;
    @XmlElement(name = "ContactInformationIsPrimaryIndicator", nillable = true)
    protected Boolean contactInformationIsPrimaryIndicator;
    @XmlElement(name = "ContactInformationIsHomeIndicator", nillable = true)
    protected Boolean contactInformationIsHomeIndicator;
    @XmlElement(name = "ContactInformationIsWorkIndicator", nillable = true)
    protected Boolean contactInformationIsWorkIndicator;

    /**
     * Gets the value of the contactInformationReference property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceType }
     *     
     */
    public ReferenceType getContactInformationReference() {
        return contactInformationReference;
    }

    /**
     * Sets the value of the contactInformationReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceType }
     *     
     */
    public void setContactInformationReference(ReferenceType value) {
        this.contactInformationReference = value;
    }

    /**
     * Gets the value of the contactInformationIsPrimaryIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContactInformationIsPrimaryIndicator() {
        return contactInformationIsPrimaryIndicator;
    }

    /**
     * Sets the value of the contactInformationIsPrimaryIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactInformationIsPrimaryIndicator(Boolean value) {
        this.contactInformationIsPrimaryIndicator = value;
    }

    /**
     * Gets the value of the contactInformationIsHomeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContactInformationIsHomeIndicator() {
        return contactInformationIsHomeIndicator;
    }

    /**
     * Sets the value of the contactInformationIsHomeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactInformationIsHomeIndicator(Boolean value) {
        this.contactInformationIsHomeIndicator = value;
    }

    /**
     * Gets the value of the contactInformationIsWorkIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContactInformationIsWorkIndicator() {
        return contactInformationIsWorkIndicator;
    }

    /**
     * Sets the value of the contactInformationIsWorkIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactInformationIsWorkIndicator(Boolean value) {
        this.contactInformationIsWorkIndicator = value;
    }

}
