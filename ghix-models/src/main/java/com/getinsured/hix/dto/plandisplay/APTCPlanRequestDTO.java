package com.getinsured.hix.dto.plandisplay;

import java.math.BigDecimal;

public class APTCPlanRequestDTO{
	private Float premium;
	private Float ehbPercentage;
	private String level;
	private String insuranceType;
	private BigDecimal stateSubsidy;
	
	public Float getPremium() {
		return premium;
	}
	public void setPremium(Float premium) {
		this.premium = premium;
	}	
	public Float getEhbPercentage() {
		return ehbPercentage;
	}
	public void setEhbPercentage(Float ehbPercentage) {
		this.ehbPercentage = ehbPercentage;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public BigDecimal getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(BigDecimal stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}
}
