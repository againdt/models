package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.timeshift.TimeShifterUtil;

public class CapApplicationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8344606376608714367L;
		
	private SsapApplicationDTO ssapApplicationDTO;
	private List<SsapApplicantDTO> applicantDTOList;
	private HouseholdDTO householdDTO;
	private EnrollmentDTO enrollmentDTO;
	private ConsumerDocumentDTO consumerDocumentDTO;
	private PlanInfoDTO planInfoDTO;
	private EffectiveApplicationDTO effectiveApplicationDTO;
	private String loggedInUserName;// this is for accessing enrollmentEndPoints to set the real logged in user (set the cap agent id in case of csr is logged in). 
	private boolean pendingStatusOnly;
	private boolean ffmAgentSubmit;
	private boolean D2CApplication;
	
	private CapPaymentMethodDto capPaymentMethodDto;
	private String ssapId;
	
	public SsapApplicationDTO getSsapApplicationDTO() {
		return ssapApplicationDTO;
	}

	public void setSsapApplicationDTO(SsapApplicationDTO ssapApplicationDTO) {
		this.ssapApplicationDTO = ssapApplicationDTO;
	}

	public List<SsapApplicantDTO> getApplicantDTOList() {
		return applicantDTOList;
	}

	public void setApplicantDTOList(List<SsapApplicantDTO> applicantDTOList) {
		this.applicantDTOList = applicantDTOList;
	}

	public HouseholdDTO getHouseholdDTO() {
		return householdDTO;
	}

	public void setHouseholdDTO(HouseholdDTO householdDTO) {
		this.householdDTO = householdDTO;
	}

	public EnrollmentDTO getEnrollmentDTO() {
		return enrollmentDTO;
	}

	public void setEnrollmentDTO(EnrollmentDTO enrollmentDTO) {
		this.enrollmentDTO = enrollmentDTO;
	}

	public ConsumerDocumentDTO getConsumerDocumentDTO() {
		return consumerDocumentDTO;
	}

	public void setConsumerDocumentDTO(ConsumerDocumentDTO consumerDocumentDTO) {
		this.consumerDocumentDTO = consumerDocumentDTO;
	}

	public PlanInfoDTO getPlanInfoDTO() {
		return planInfoDTO;
	}

	public void setPlanInfoDTO(PlanInfoDTO planInfoDTO) {
		this.planInfoDTO = planInfoDTO;
	}
	
	public void setExchgIndivIdentifierForApplicantsIfNecessary() {
		int count=0;// needed to add  count as mili seconds are same for the execution time of the for loop
		for (SsapApplicantDTO applicant : applicantDTOList) {
			if (StringUtils.isEmpty(applicant.getExchgIndivIdentifier())) {
				applicant.setExchgIndivIdentifier("CAP_"
						+ TimeShifterUtil.currentTimeMillis()+count);
				count++;
			}
		}
	}

	public EffectiveApplicationDTO getEffectiveApplicationDTO() {
		return effectiveApplicationDTO;
	}

	public void setEffectiveApplicationDTO(
			EffectiveApplicationDTO effectiveApplicationDTO) {
		this.effectiveApplicationDTO = effectiveApplicationDTO;
	}

	public String getLoggedInUserName() {
		return loggedInUserName;
	}

	public void setLoggedInUserName(String loggedInUserName) {
		this.loggedInUserName = loggedInUserName;
	}
	
	public void setPendingStatusOnly(boolean pendingStatusOnly) {
		this.pendingStatusOnly = pendingStatusOnly;
	}
	
	public boolean isPendingStatusOnly() {
		return pendingStatusOnly;
	}
	
	public void setFfmAgentSubmit(boolean ffmAgentSubmit) {
		this.ffmAgentSubmit = ffmAgentSubmit;
	}
	
	public boolean isFfmAgentSubmit() {
		return ffmAgentSubmit;
	}

	public boolean isD2CApplication() {
		return D2CApplication;
	}

	public void setD2CApplication(boolean d2cApplication) {
		D2CApplication = d2cApplication;
	}

	public CapPaymentMethodDto getCapPaymentMethodDto() {
		return capPaymentMethodDto;
	}

	public void setCapPaymentMethodDto(CapPaymentMethodDto capPaymentMethodDto) {
		this.capPaymentMethodDto = capPaymentMethodDto;
	}

	public String getSsapId() {
		return ssapId;
	}

	public void setSsapId(String ssapId) {
		this.ssapId = ssapId;
	}
	
}
