update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Provides users access to the view ticket queue' where name ='TKM_VIEW_QUEUE';
update permissions set description = 'Allows Users to go to a ticket''s details page.' where name ='TKM_VIEW_TICKET';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'Users with this permission can use the "Consumer View" functionality and upload phone numbers to the company Do Not Call list.' where name ='VIEW_CONSUMER_CRM';
update permissions set description = 'Provides users with access to advanced ticket search tools and admin ticket functionality. Users with this permission can act on ANY ticket no matter what status it is in.' where name ='TKM_EXTENDEDLIST_TICKET';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_EDIT_MYDEPENDENTS';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_WAIVECOVERAGE_EMPLOYEE';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_SPENROLLMENT_EMPLOYEE';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_TERMINATECOVERAGE_EMPLOYEE';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_UPDATEMYINFO_EMPLOYEE';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_UPDATEACCOUNT_EMPLOYEE';
update permissions set description = 'N/A for ID HIX' where name ='SHOP_OPENENROLLMENT_EMPLOYEE';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'View Ticket Aging Report
Download Report' where name ='TKM_AGEING_REPORT';
update permissions set description = 'Provides designated users access to the Health and Dental plan management pages.' where name ='ADMIN_PORTAL_MANAGE_PLAN';
update permissions set description = 'This permission allows authorized users to preview the application while working on a ticket related to the document verification' where name ='SSAP_TKM_PREVIEW';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'This permission allows CSR to Relink households.' where name ='CAP_MEMBER_RELINKALLOWED';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'Provides users access to the ticket archiving permission. Tickets that are in a "Cancelled" or "Resolved" status can be archived and removed from the "Manage Tickets" page.' where name ='TKM_ARCHIVE_ALLOWED';
update permissions set description = 'Provides users access to the assign tickets.' where name ='TKM_ASSIGN_ALLOWED';
update permissions set description = 'Adding MANAGE_BATCH permission to manage permissions for batch ui operations' where name ='MANAGE_BATCH';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_REGISTRATION_WIZARD';
update permissions set description = 'This permissions allows authorized user to edit announcements from customer admin portal.' where name ='IND_PORTAL_EDIT_ANNOUNCEMENTS';
update permissions set description = 'This permission allows authorized users to edit non-financial application through the CSR Override.' where name ='IND_PORTAL_EDIT_APP';
update permissions set description = 'This permission allows authorized users to edit non-financial application through the CSR Override.' where name ='IND_PORTAL_EDIT_APP';
update permissions set description = 'This permission allows authorized users to edit non-financial application through the CSR Override.' where name ='IND_PORTAL_EDIT_APP';
update permissions set description = 'This permission allows authorized users to edit a special enrollment period using a CSR override.' where name ='IND_PORTAL_EDIT_SPCL_ENRLMNT_PERIOD';
update permissions set description = 'This permission allows authorized users to edit a special enrollment period using a CSR override.' where name ='IND_PORTAL_EDIT_SPCL_ENRLMNT_PERIOD';
update permissions set description = 'This permission allows authorized users to edit a special enrollment period using a CSR override.' where name ='IND_PORTAL_EDIT_SPCL_ENRLMNT_PERIOD';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page
- Pay for Health and Dental
- Update APTC' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page
- Pay for Health and Dental
- Update APTC' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page
- Pay for Health and Dental
- Update APTC' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'This permission allows authorized users to view member data.
 - View Member Account
 - Add Comment
 - Security Questions
 - Mark RIDP Verified
 - View Consumer dashboard
 - Ticket History
 - Appeal History
 - Edit Basic Information
 - Send Link activation
 - Enrollment History
 - Resend 834
 - History' where name ='CRM_VIEW_MEMBER';
update permissions set description = 'RIDP call for verification' where name ='RIDP_FARS';
update permissions set description = 'View RIDP Contact info and validate RIDP for Primary Applicant' where name ='RIDP_CONTACT_INFO';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'Start RIDP Flow' where name ='RIDP_GET_STARTED';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page
- Pay for Health and Dental
- Update APTC' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page
- Pay for Health and Dental
- Update APTC' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_REGISTRATION_WIZARD';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page
- Pay for Health and Dental
- Update APTC' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'This permission allows authorized user to go through Plan selection and Enroll.
- Finalize Plan Choice and Enroll
- Additional Information page' where name ='IND_PORTAL_ENROLL_APP';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This feature allows authorized user to view Secure Inbox.' where name ='IND_PORTAL_INBOX';
update permissions set description = 'This permission allows authorized users to Initiate verifications for a household through a CSR override.' where name ='IND_PORTAL_INIT_VERIFICATIONS';
update permissions set description = 'This permission allows authorized users to Initiate verifications for a household through a CSR override.' where name ='IND_PORTAL_INIT_VERIFICATIONS';
update permissions set description = 'This permission allows authorized users to Initiate verifications for a household through a CSR override.' where name ='IND_PORTAL_INIT_VERIFICATIONS';
update permissions set description = 'This permission is not used anymore. This was used earlier to show ''Pending Referrals'' link in portal (ID R3.X)' where name ='IND_PORTAL_REFERRALS';
update permissions set description = 'This permission is not used anymore. This was used earlier to show ''Pending Referrals'' link in portal (ID R3.X)' where name ='IND_PORTAL_REFERRALS';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_ACCEPT_VERIFICATION_EDIT';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'RIDP call for verification' where name ='RIDP_FARS';
update permissions set description = 'View RIDP Contact info and validate RIDP for Primary Applicant' where name ='RIDP_CONTACT_INFO';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'Start RIDP Flow' where name ='RIDP_GET_STARTED';
update permissions set description = 'This permission is not used anymore. This was used earlier to show ''Pending Referrals'' link in portal (ID R3.X)' where name ='IND_PORTAL_REFERRALS';
update permissions set description = 'This permission is not used anymore. This was used earlier to show ''Pending Referrals'' link in portal (ID R3.X)' where name ='IND_PORTAL_REFERRALS';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'This permission is not used anymore. This was used earlier to show ''Pending Referrals'' link in portal (ID R3.X)' where name ='IND_PORTAL_REFERRALS';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Allows Users to go to a ticket''s details page.' where name ='TKM_VIEW_TICKET';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'Users with this permission can use the "Consumer View" functionality and upload phone numbers to the company Do Not Call list.' where name ='VIEW_CONSUMER_CRM';
update permissions set description = 'Provides Users access to simplified ticket search tools. The TKM_EXTENDEDLIST_TICKET permission overrides this one.' where name ='TKM_SIMPLIFIEDVIEW_TICKET';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'This permission is not used anymore. This was used earlier to show ''Pending Referrals'' link in portal (ID R3.X)' where name ='IND_PORTAL_REFERRALS';
update permissions set description = 'This permission allows authorized users to reinstate enrollment for a household through a CSR override.' where name ='IND_PORTAL_REINSTATE_ENROLLMENT';
update permissions set description = 'This permission allows authorized users to reinstate enrollment for a household through a CSR override.' where name ='IND_PORTAL_REINSTATE_ENROLLMENT';
update permissions set description = 'This permission allows authorized users to reinstate enrollment for a household through a CSR override.' where name ='IND_PORTAL_REINSTATE_ENROLLMENT';
update permissions set description = 'This permission allows authorized user to report changes using the Report a Change Flow.
- Individual Portal -> Report a Change' where name ='IND_PORTAL_REPORT_CHANGES';
update permissions set description = 'This permission allows authorized user to report changes using the Report a Change Flow.
- Individual Portal -> Report a Change' where name ='IND_PORTAL_REPORT_CHANGES';
update permissions set description = 'This permission allows authorized user to report changes using the Report a Change Flow.
- Individual Portal -> Report a Change' where name ='IND_PORTAL_REPORT_CHANGES';
update permissions set description = 'This permission allows authorized user to report changes using the Report a Change Flow.
- Individual Portal -> Report a Change' where name ='IND_PORTAL_REPORT_CHANGES';
update permissions set description = 'This permission allows authorized user to report changes using the Report a Change Flow.
- Individual Portal -> Report a Change' where name ='IND_PORTAL_REPORT_CHANGES';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_ACCEPT_VERIFICATION_EDIT';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'This permission allows authorized users to view member data.
 - View Member Account
 - Add Comment
 - Security Questions
 - Mark RIDP Verified
 - View Consumer dashboard
 - Ticket History
 - Appeal History
 - Edit Basic Information
 - Send Link activation
 - Enrollment History
 - Resend 834
 - History' where name ='CRM_VIEW_MEMBER';
update permissions set description = 'View RIDP Contact info and validate RIDP for Primary Applicant' where name ='RIDP_CONTACT_INFO';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'This permission allows authorized user to report changes using the Report a Change Flow.
- Individual Portal -> Report a Change' where name ='IND_PORTAL_REPORT_CHANGES';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Allows Users to go to a ticket''s details page.' where name ='TKM_VIEW_TICKET';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'Users with this permission can use the "Consumer View" functionality and upload phone numbers to the company Do Not Call list.' where name ='VIEW_CONSUMER_CRM';
update permissions set description = 'Provides Users access to simplified ticket search tools. The TKM_EXTENDEDLIST_TICKET permission overrides this one.' where name ='TKM_SIMPLIFIEDVIEW_TICKET';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'This permission allows authorized users to resume an existing application for editing.' where name ='IND_PORTAL_RESUME_APP';
update permissions set description = 'This permission allows authorized users to resume an existing application for editing.' where name ='IND_PORTAL_RESUME_APP';
update permissions set description = 'This permission allows authorized users to resume an existing application for editing.' where name ='IND_PORTAL_RESUME_APP';
update permissions set description = 'N/A for Idaho HIX' where name ='ADD_EMPLOYEE';
update permissions set description = 'Provides user the ability to add "New Issuer Account"' where name ='ADD_NEW_ISSUER';
update permissions set description = 'Provides user the ability to add "New Issuer Account Payment Method"' where name ='ADD_PAYMENT_METHOD';
update permissions set description = 'Provides user the ability to add "New Issuer Representative"' where name ='ADD_REPRESENTATIVE';
update permissions set description = 'N/A for Idaho HIX' where name ='EDIT_EMPLOYEE';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_BENEFITS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_DETAILS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_RATES';
update permissions set description = 'Provides user the ability to edit issuer representative details' where name ='EDIT_REPRESENTATIVE';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_PLAN_SELECTION_WIZARD';
update permissions set description = 'Provides user the ability to manage issuer representatives for a given issuer' where name ='MANAGE_AUTHORIZED_REPRESENTATIVES';
update permissions set description = 'Provides user the ability to manage financial information for a given issuer' where name ='MANAGE_FINANCIAL_INFORMATION';
update permissions set description = 'Provides user the ability to view the list of all Issuers on the Exchange' where name ='MANAGE_ISSUER';
update permissions set description = 'Provides user the ability to view the list of all QHPs on the Exchange' where name ='MANAGE_QHP_PLANS';
update permissions set description = 'Provides user the ability to view the list of all SADPs on the Exchange' where name ='MANAGE_SADP_PLANS';
update permissions set description = '*This is not used*' where name ='UPLOAD_NEW_BENEFITS';
update permissions set description = '*This is not used*' where name ='UPLOAD_NEW_RATES';
update permissions set description = '*This is not used*' where name ='UPDATE_PROVIDER_NETWORK_ID';
update permissions set description = 'Provides user the ability to update Enrollment Availability Status of a plan' where name ='UPDATE_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides user the ability to update the Certification Status of a plan' where name ='UPDATE_CERTIFICATION_STATUS';
update permissions set description = 'Provides user the ability to manage issuer representatives for a given issuer' where name ='VIEW_AUTHORIZED_REPRESENTATIVES';
update permissions set description = 'Provides user the ability to view the Certification Status history of a plan' where name ='VIEW_CERTIFICATION_STATUS';
update permissions set description = 'Provides user the ability to view Enrollment Availability Status history of a plan' where name ='VIEW_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides user the ability to view benefits of a chosen plan' where name ='VIEW_PLAN_BENEFITS';
update permissions set description = 'Provides user the ability to view details of the chosen plan' where name ='VIEW_PLAN_DETAILS';
update permissions set description = 'Provides user the ability to view history of the updated made to the chosen plan' where name ='VIEW_PLAN_HISTORY';
update permissions set description = 'Provides user the ability to view rates of the chosen plan' where name ='VIEW_PLAN_RATES';
update permissions set description = 'Provides users access to User Management tools. Tools include managing existing user accounts and provisioning new user accounts.' where name ='MANAGE_USERS';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_REGISTRATION_WIZARD';
update permissions set description = 'Provides users access to the edit Ticket queue' where name ='TKM_EDIT_QUEUE';
update permissions set description = 'Provides Users access to manage ticket workgroups. Workgroups are used to control access to the different ticket types. Members can be added or removed from workgroups.' where name ='TKM_LIST_QUEUE';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Users with this permission can use the "Consumer View" functionality and upload phone numbers to the company Do Not Call list.' where name ='VIEW_CONSUMER_CRM';
update permissions set description = 'Provides users with access to advanced ticket search tools and admin ticket functionality. Users with this permission can act on ANY ticket no matter what status it is in.' where name ='TKM_EXTENDEDLIST_TICKET';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'View Ticket Aging Report
Download Report' where name ='TKM_AGEING_REPORT';
update permissions set description = 'This permission allows authorised user to have access to update enrollment status to trigger 1095-A''s.' where name ='IND_PORTAL_1095_OVERRIDE';
update permissions set description = 'This permission allows authorised user to have access to update his Account Settings' where name ='IND_PORTAL_ACCOUNT_SETTINGS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'List all appeals for the consumer in the individual portal' where name ='IND_PORTAL_APPEALS';
update permissions set description = 'This permission allows authorised user to have access to update his APTC.' where name ='IND_PORTAL_APPLY_COST_SAVINGS';
update permissions set description = '*This is not used anymore but the flexibility is available*' where name ='IND_PORTAL_CHANGE_PASSWORD';
update permissions set description = 'This permission allows authorised user to have access to update his password' where name ='IND_PORTAL_CHANGE_PASSWORD';
update permissions set description = '*This is not used anymore but the flexibility is available*' where name ='IND_PORTAL_CHANGE_PASSWORD';
update permissions set description = '*This is not used anymore but the flexibility is available*' where name ='IND_PORTAL_CHANGE_PASSWORD';
update permissions set description = '*This is not used anymore but the flexibility is available*' where name ='IND_PORTAL_CHANGE_SEC_QSTNS';
update permissions set description = 'This permission allows authorised user to have access to update his security questions.' where name ='IND_PORTAL_CHANGE_SEC_QSTNS';
update permissions set description = '*This is not used anymore but the flexibility is available*' where name ='IND_PORTAL_CHANGE_SEC_QSTNS';
update permissions set description = 'Provides Users access to the Policy Admin Portal, which can be used to search for and locate all of the enrollments associate with the current tenant.' where name ='CAP_POLICY_LIST';
update permissions set description = 'Provides Users permission to access the Enrollment details page which contains enrollment details, commissions received, and payouts to our partners. These pages are accessed by locating the enrollment in the Policy Admin Portal.' where name ='CAP_POLICY_VIEW';
update permissions set description = 'Allows Users to edit enrollment details for enrollments in the Policy Admin Portal.' where name ='CAP_POLICY_EDIT';
update permissions set description = 'Provides Users access to the "Manage Leads" page. This page should be reserved for Sales leadership and Admin roles.' where name ='CAP_LEAD_VIEW';
update permissions set description = 'Provides designated users access to the Health and Dental plan management pages.' where name ='ADMIN_PORTAL_MANAGE_PLAN';
update permissions set description = 'This permission allows authorized users to preview the application while working on a ticket related to the document verification' where name ='SSAP_TKM_PREVIEW';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_ACCEPT_VERIFICATION_EDIT';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'RIDP call for verification' where name ='RIDP_FARS';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'Start RIDP Flow' where name ='RIDP_GET_STARTED';
update permissions set description = '*This is not used anymore but the flexibility is available*' where name ='IND_PORTAL_CHANGE_SEC_QSTNS';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'Provides users access to the ticket archiving permission. Tickets that are in a "Cancelled" or "Resolved" status can be archived and removed from the "Manage Tickets" page.' where name ='TKM_ARCHIVE_ALLOWED';
update permissions set description = 'Provides users access to the edit plan details' where name ='EDIT_PLANS';
update permissions set description = 'Provides users access to the edit Issuer details' where name ='EDIT_ISSUER';
update permissions set description = 'Provides users access to the manage ancillary plan' where name ='SPL_MANAGE_PLAN';
update permissions set description = 'Provides users access to the assign tickets.' where name ='TKM_ASSIGN_ALLOWED';
update permissions set description = 'Allow user to resend 834 from CAP' where name ='CAP_ALLOW_RESEND834';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'This feature allows authorized users to contact YHI and log a ticket type. Contact Us on the Eligibility Results page. ' where name ='IND_PORTAL_CONTACT_US';
update permissions set description = 'Provides user access to view Quality Rating for all the plans on the Exchange' where name ='VIEW_QUALITY_RATING';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_PLAN_SELECTION_WIZARD';
update permissions set description = 'Provides user the ability to manage issuer representatives for a given issuer' where name ='VIEW_AUTHORIZED_REPRESENTATIVES';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_REGISTRATION_WIZARD';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'This permissions allows authorized user to create announcements from customer admin portal.' where name ='IND_PORTAL_CREATE_ANNOUNCEMENTS';
update permissions set description = 'This permissions allows authorized user to all the CSR Overrides' where name ='IND_PORTAL_CSR_OVERRIDES';
update permissions set description = 'This permissions allows authorized user to all the CSR Overrides' where name ='IND_PORTAL_CSR_OVERRIDES';
update permissions set description = 'This permissions allows authorized user to all the CSR Overrides' where name ='IND_PORTAL_CSR_OVERRIDES';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan.' where name ='IND_PORTAL_DISENROLL';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan.' where name ='IND_PORTAL_DISENROLL';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan.' where name ='IND_PORTAL_DISENROLL';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan.' where name ='IND_PORTAL_DISENROLL';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan.' where name ='IND_PORTAL_DISENROLL';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan.' where name ='IND_PORTAL_DISENROLL';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan using the CSR Override' where name ='IND_PORTAL_DISENROLL_WITH_DATE';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'RIDP call for verification' where name ='RIDP_FARS';
update permissions set description = 'RIDP_CSR_PAPER_APPLICATION' where name ='RIDP_CSR_PAPER_APPLICATION';
update permissions set description = 'View RIDP Contact info and validate RIDP for Primary Applicant' where name ='RIDP_CONTACT_INFO';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'Start RIDP Flow' where name ='RIDP_GET_STARTED';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan using the CSR Override' where name ='IND_PORTAL_DISENROLL_WITH_DATE';
update permissions set description = 'This feature allows authorized users to voluntarily disenroll from Health or Dental or Health and Dental plan using the CSR Override' where name ='IND_PORTAL_DISENROLL_WITH_DATE';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_REGISTRATION_WIZARD';
update permissions set description = 'Provides users access to the edit Ticket queue' where name ='TKM_EDIT_QUEUE';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'Provides users with access to advanced ticket search tools and admin ticket functionality. Users with this permission can act on ANY ticket no matter what status it is in.' where name ='TKM_EXTENDEDLIST_TICKET';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'Allows Designated Users to add issuers to any tenant. Issuers can be added directly from the "Add New Issuers" page.' where name ='ADD_NEW_ISSUER';
update permissions set description = 'Provides user the ability to add "New Issuer Representative"' where name ='ADD_REPRESENTATIVE';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_BENEFITS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_DETAILS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_RATES';
update permissions set description = 'Provides user the ability to edit issuer representative details' where name ='EDIT_REPRESENTATIVE';
update permissions set description = 'Provides user the ability to manage issuer representatives for a given issuer' where name ='MANAGE_AUTHORIZED_REPRESENTATIVES';
update permissions set description = 'Provides user the ability to manage financial information for a given issuer' where name ='MANAGE_FINANCIAL_INFORMATION';
update permissions set description = 'Provides user the ability to view the list of all Issuers on the Exchange' where name ='MANAGE_ISSUER';
update permissions set description = 'Provides user the ability to view the list of all QHPs on the Exchange' where name ='MANAGE_QHP_PLANS';
update permissions set description = 'Provides user the ability to view the list of all SADPs on the Exchange' where name ='MANAGE_SADP_PLANS';
update permissions set description = '*This is not used*' where name ='UPLOAD_NEW_BENEFITS';
update permissions set description = '*This is not used*' where name ='UPLOAD_NEW_RATES';
update permissions set description = '*This is not used*' where name ='UPDATE_PROVIDER_NETWORK_ID';
update permissions set description = 'Provides user the ability to update enrollment availability status of a plan' where name ='UPDATE_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides user the ability to update certification status of a plan' where name ='UPDATE_CERTIFICATION_STATUS';
update permissions set description = 'Provides user the ability to view certification status history  of a plan' where name ='VIEW_CERTIFICATION_STATUS';
update permissions set description = 'Provides user the ability to view enrollment availability status history of a plan' where name ='VIEW_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides user the ability to view benefits of a chosen plan' where name ='VIEW_PLAN_BENEFITS';
update permissions set description = 'Provides user the ability to view details of the chosen plan' where name ='VIEW_PLAN_DETAILS';
update permissions set description = 'Provides user the ability to view history of the updated made to the chosen plan' where name ='VIEW_PLAN_HISTORY';
update permissions set description = 'Provides user the ability to view rates of the chosen plan' where name ='VIEW_PLAN_RATES';
update permissions set description = '*This is not used*' where name ='VIEW_PROVIDER_NETWORK_ID';
update permissions set description = 'Provides users access to the edit Ticket queue' where name ='TKM_EDIT_QUEUE';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Allows Users to go to a ticket''s list page.' where name ='TKM_VIEW_QUEUE';
update permissions set description = 'Allows Users to go to a ticket''s details page.' where name ='TKM_VIEW_TICKET';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'Provides designated users access to the Health and Dental plan management pages.' where name ='ADMIN_PORTAL_MANAGE_PLAN';
update permissions set description = 'Provides users access to the edit plan details' where name ='EDIT_PLANS';
update permissions set description = 'Provides users access to the edit Issuer details' where name ='EDIT_ISSUER';
update permissions set description = 'Provides users access to the manage ancillary plan ' where name ='SPL_MANAGE_PLAN';
update permissions set description = 'Provides users access to the view plan quality ratings' where name ='VIEW_QUALITY_RATING';
update permissions set description = 'Provides users access to the add Issuer level accreditation documents' where name ='ADD_ACCREDITATION_DOCUMENTS';
update permissions set description = 'Provides users access to the add Issuer financial information' where name ='ADD_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO';
update permissions set description = 'Provides users access to the add Issuer representative' where name ='ADD_REPRESENTATIVE';
update permissions set description = 'Provides users access to the edit Issuer level accreditation documents' where name ='EDIT_ACCREDITATION_DOCUMENTS';
update permissions set description = 'Provides users access to the edit Issuer company profile page' where name ='EDIT_COMPANY_PROFILE';
update permissions set description = 'Provides users access to the edit Issuer individual market profile page' where name ='EDIT_INDIVIDUAL_MARKET_PROFILE';
update permissions set description = 'Provides users access to the edit Issuer financial information' where name ='EDIT_ISSUER_ACCOUNT_PROFILE_FINANCIAL_INFO';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_BENEFITS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_DETAILS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_RATES';
update permissions set description = 'Provides user the ability to edit issuer representative details' where name ='EDIT_REPRESENTATIVE';
update permissions set description = 'Provides users access to the edit Issuer SHOP market profile page' where name ='EDIT_SHOP_MARKET_PROFILE';
update permissions set description = 'Provides user the ability to view the list of all QHPs for a issuer' where name ='MANAGE_QHP_PLANS';
update permissions set description = 'Provides user the ability to view the list of all Issuer representatives of an issuer' where name ='MANAGE_REPRESENTATIVE';
update permissions set description = 'Provides user the ability to view the list of all SADPs of a given issuer' where name ='MANAGE_SADP_PLANS';
update permissions set description = '*This is not used*' where name ='UPLOAD_NEW_BENEFITS';
update permissions set description = '*This is not used*' where name ='UPLOAD_NEW_RATES';
update permissions set description = '*This is not used*' where name ='UPDATE_PROVIDER_NETWORK_ID';
update permissions set description = '*This is not used*' where name ='UPDATE_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides user the ability to mark a plan as verified' where name ='VERIFY_PLAN';
update permissions set description = 'Provides users access to the view Issuer level accreditation documents' where name ='VIEW_ACCREDITATION_DOCUMENTS';
update permissions set description = 'Provides users access to certification status of an issuer' where name ='VIEW_CERTIFICATION_STATUS';
update permissions set description = 'Provides users access to the view Issuer company profile page' where name ='VIEW_COMPANY_PROFILE';
update permissions set description = 'Provides users access to the view Issuer individual market profile page' where name ='VIEW_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides users access to the edit Issuer financial information' where name ='VIEW_FINANCIAL_LIST_INFO';
update permissions set description = 'Provides users access to the edit Issuer individual market profile page' where name ='VIEW_INDIVIDUAL_MARKET_PROFILE';
update permissions set description = 'Provides users access to the edit Issuer profile page' where name ='VIEW_ISSUER_PROFILE';
update permissions set description = 'Provides user the ability to view benefits of a chosen plan' where name ='VIEW_PLAN_BENEFITS';
update permissions set description = 'Provides user the ability to view details of the chosen plan' where name ='VIEW_PLAN_DETAILS';
update permissions set description = 'Provides user the ability to view history of the updated made to the chosen plan' where name ='VIEW_PLAN_HISTORY';
update permissions set description = 'Provides user the ability to view rates of the chosen plan' where name ='VIEW_PLAN_RATES';
update permissions set description = '*This is not used*' where name ='VIEW_PROVIDER_NETWORK_ID';
update permissions set description = 'Provides users access to the view Issuer SHOP market profile page' where name ='VIEW_SHOP_MARKET_PROFILE';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Provides user the ability to manage QHPs and SADP plans' where name ='ISSUER_PORTAL_MANAGE_PLAN';
update permissions set description = 'Allows Designated Users to add issuers to any tenant. Issuers can be added directly from the "Add New Issuers" page.' where name ='ADD_NEW_ISSUER';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_BENEFITS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_DETAILS';
update permissions set description = '*This is not used*' where name ='EDIT_PLAN_RATES';
update permissions set description = 'Provides user the ability to edit issuer representative details' where name ='EDIT_REPRESENTATIVE';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_PLAN_SELECTION_WIZARD';
update permissions set description = 'Provides user the ability to manage issuer representatives for a given issuer' where name ='MANAGE_AUTHORIZED_REPRESENTATIVES';
update permissions set description = 'Provides user the ability to view the list of all Issuers on the Exchange' where name ='MANAGE_ISSUER';
update permissions set description = 'Provides user the ability to view the list of all Payment Methods for a given Issuer' where name ='MANAGE_PAYMENT_METHODS';
update permissions set description = 'Provides user the ability to view the list of all QHPs on the Exchange' where name ='MANAGE_QHP_PLANS';
update permissions set description = 'Provides user the ability to view the list of all SADPs on the Exchange' where name ='MANAGE_SADP_PLANS';
update permissions set description = 'Provides user the ability to update Enrollment Availability Status of a plan' where name ='UPDATE_ENROLLMENT_AVAILABILITY';
update permissions set description = 'Provides user the ability to view issuer representatives for a given issuer' where name ='VIEW_AUTHORIZED_REPRESENTATIVES';
update permissions set description = 'Provides user the ability to view benefits of a chosen plan' where name ='VIEW_PLAN_BENEFITS';
update permissions set description = 'Provides user the ability to view details of the chosen plan' where name ='VIEW_PLAN_DETAILS';
update permissions set description = 'Provides user the ability to view history of the updated made to the chosen plan' where name ='VIEW_PLAN_HISTORY';
update permissions set description = 'Provides user the ability to view rates of the chosen plan' where name ='VIEW_PLAN_RATES';
update permissions set description = 'Provides users access to User Management tools. Tools include managing existing user accounts and provisioning new user accounts.' where name ='MANAGE_USERS';
update permissions set description = 'N/A for ID HIX' where name ='EMPLOYER_REGISTRATION_WIZARD';
update permissions set description = 'Provides users access to the edit Ticket queue' where name ='TKM_EDIT_QUEUE';
update permissions set description = 'Provides Users access to manage ticket workgroups. Workgroups are used to control access to the different ticket types. Members can be added or removed from workgroups.' where name ='TKM_LIST_QUEUE';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'This permission allows authorized users to resume an existing application for editing.' where name ='IND_PORTAL_RESUME_APP';
update permissions set description = 'This permission allows authorized users to resume an existing application for editing.' where name ='IND_PORTAL_RESUME_APP';
update permissions set description = 'This permission allows authorized users to rerun eligibility  for a household through a CSR override.' where name ='IND_PORTAL_RUN_ELIGIBILITY';
update permissions set description = 'This permission allows authorized users to rerun eligibility  for a household through a CSR override.' where name ='IND_PORTAL_RUN_ELIGIBILITY';
update permissions set description = 'This permission allows authorized users to rerun eligibility  for a household through a CSR override.' where name ='IND_PORTAL_RUN_ELIGIBILITY';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permission allows authorized users to Start an application from Individual Portal.
- Main Landing page
- My applications page' where name ='IND_PORTAL_START_APP';
update permissions set description = 'This permissions allows authorized user to submit an appeal from Eligibility Results page. Submit an Appeal action only shows up when eligibility is denied.' where name ='IND_PORTAL_SUBMIT_APPEAL';
update permissions set description = 'This permissions allows authorized user to submit an appeal from Eligibility Results page. Submit an Appeal action only shows up when eligibility is denied.' where name ='IND_PORTAL_SUBMIT_APPEAL';
update permissions set description = 'This permissions allows authorized user to submit an appeal from Eligibility Results page. Submit an Appeal action only shows up when eligibility is denied.' where name ='IND_PORTAL_SUBMIT_APPEAL';
update permissions set description = 'This permission allows authorized users to preview the application while working on a ticket related to the document verification' where name ='SSAP_TKM_PREVIEW';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_ACCEPT_VERIFICATION_EDIT';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'This permission allows authorized users to view member data.
 - View Member Account
 - Add Comment
 - Security Questions
 - Mark RIDP Verified
 - View Consumer dashboard
 - Ticket History
 - Appeal History
 - Edit Basic Information
 - Send Link activation
 - Enrollment History
 - Resend 834
 - History' where name ='CRM_VIEW_MEMBER';
update permissions set description = 'RIDP call for verification' where name ='RIDP_FARS';
update permissions set description = 'RIDP_CSR_PAPER_APPLICATION' where name ='RIDP_CSR_PAPER_APPLICATION';
update permissions set description = 'View RIDP Contact info and validate RIDP for Primary Applicant' where name ='RIDP_CONTACT_INFO';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'Push to Manual Verification' where name ='RIDP_CSR_PUSH_TO_MANUAL';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'Start RIDP Flow' where name ='RIDP_GET_STARTED';
update permissions set description = 'This permissions allows authorized user to submit an appeal from Eligibility Results page. Submit an Appeal action only shows up when eligibility is denied.' where name ='IND_PORTAL_SUBMIT_APPEAL';
update permissions set description = 'This permissions allows authorized user to submit an appeal from Eligibility Results page. Submit an Appeal action only shows up when eligibility is denied.' where name ='IND_PORTAL_SUBMIT_APPEAL';
update permissions set description = 'This permissions allows authorized user to submit an appeal from Eligibility Results page. Submit an Appeal action only shows up when eligibility is denied.' where name ='IND_PORTAL_SUBMIT_APPEAL';
update permissions set description = 'This permission allows authorized users to send updates to a carrier  for a household through a CSR override.' where name ='IND_PORTAL_UPDATE_CARRIER';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'This permission allows authorized users to send updates to a carrier  for a household through a CSR override.' where name ='IND_PORTAL_UPDATE_CARRIER';
update permissions set description = 'Provides users access to the assign tickets.' where name ='TKM_ASSIGN_ALLOWED';
update permissions set description = 'Provides users access to User Management tools. Tools include managing existing user accounts and provisioning new user accounts.' where name ='MANAGE_USERS';
update permissions set description = 'Provides users access to the edit Ticket queue' where name ='TKM_EDIT_QUEUE';
update permissions set description = 'Provides Users access to ticketing reports. These reports include: tickets by type, tickets by priority, aging reports, any more.' where name ='TKM_LIST_QUEUE';
update permissions set description = 'Provides Users access to the "Manage Tickets" page which is primarily used to search for and find tickets.' where name ='TKM_LIST_TICKET';
update permissions set description = 'Users with this permission can create new tickets. Tickets can be created directly from the "Create New Tickets" page, the "Manage Tickets" page, and from an individuals profile page.' where name ='TKM_SAVE_TICKET';
update permissions set description = 'Provides users access to the view ticket queue' where name ='TKM_VIEW_QUEUE';
update permissions set description = 'Allows Users to go to a ticket''s details page.' where name ='TKM_VIEW_TICKET';
update permissions set description = 'Designate Agents and EE Counselors' where name ='BROKER_DESIGNATE_EMPLOYER';
update permissions set description = 'Users with this permission can use the "Consumer View" functionality and upload phone numbers to the company Do Not Call list.' where name ='VIEW_CONSUMER_CRM';
update permissions set description = 'Provides Users access to manage ticket workgroups. Workgroups are used to control access to the different ticket types. Members can be added or removed from workgroups.' where name ='TKM_EXTENDEDLIST_TICKET';
update permissions set description = 'This permission allows authorized users to create tickets' where name ='TKM_WORKFLOW_DEFAULT';
update permissions set description = 'Allows users to edit basic ticket details (subject, priority, description) in the ticket details page.' where name ='TKM_EDIT_TICKET';
update permissions set description = 'Provides Users access to ticketing reports. These reports include: tickets by type, tickets by priority, aging reports, any more.' where name ='TKM_REPORT';
update permissions set description = 'View Ticket Aging Report
Download Report' where name ='TKM_AGEING_REPORT';
update permissions set description = 'This permission allows authorized users to send updates to a carrier  for a household through a CSR override.' where name ='IND_PORTAL_UPDATE_CARRIER';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view and Print application from application summary page.' where name ='IND_PORTAL_VIEW_AND_PRINT';
update permissions set description = 'This permissions allows authorized user to view announcements from customer admin portal.' where name ='IND_PORTAL_VIEW_ANNOUNCEMENTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permission allows authorized users to preview the application while working on a ticket related to the document verification' where name ='SSAP_TKM_PREVIEW';
update permissions set description = 'Allow authorized users to edit non-financial application' where name ='SSAP_EDIT';
update permissions set description = 'View Single Stream Application' where name ='SSAP_VIEW';
update permissions set description = 'Allow authorized users to view county and county code details' where name ='SSAP_COUNTY_VIEW';
update permissions set description = 'View SSAP American States Tribes' where name ='SSAP_TRIBE_VIEW';
update permissions set description = 'Check if RIDP is verified.' where name ='SSAP_RIDP_VIEW';
update permissions set description = 'Ping URL to keep session alive' where name ='SSAP_SESSION_EDIT';
update permissions set description = 'Override Verification Results' where name ='SSAP_VERIFICATIONS_EDIT';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_APPLICANT_DOC_CATEGORY_VIEW';
update permissions set description = 'Verification Flow - verify document category upload' where name ='SSAP_ACCEPT_VERIFICATION_EDIT';
update permissions set description = 'Upload documents for verifications' where name ='SSAP_UPLOAD_DOCUMENT_EDIT';
update permissions set description = 'This permission allow authorized users to add a Household' where name ='ADD_NEW_CONSUMER';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'This permission allows authorized users to view member data.
 - View Member Account
 - Add Comment
 - Security Questions
 - Mark RIDP Verified
 - View Consumer dashboard
 - Ticket History
 - Appeal History
 - Edit Basic Information
 - Send Link activation
 - Enrollment History
 - Resend 834
 - History' where name ='CRM_VIEW_MEMBER';
update permissions set description = 'RIDP call for verification' where name ='RIDP_FARS';
update permissions set description = 'RIDP_CSR_PAPER_APPLICATION' where name ='RIDP_CSR_PAPER_APPLICATION';
update permissions set description = 'View RIDP Contact info and validate RIDP for Primary Applicant' where name ='RIDP_CONTACT_INFO';
update permissions set description = 'RIDP Manual Verification' where name ='RIDP_MANUAL_VERIFICATION';
update permissions set description = 'Push to Manual Verification' where name ='RIDP_CSR_PUSH_TO_MANUAL';
update permissions set description = 'RIDP Questions' where name ='RIDP_QUESTION';
update permissions set description = 'Start RIDP Flow' where name ='RIDP_GET_STARTED';
update permissions set description = 'This permissions allows authorized user to view Eligibility Results page from My applications page.' where name ='IND_PORTAL_VIEW_ELIG_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'This permission allows CSR to Relink households.' where name ='CAP_MEMBER_RELINKALLOWED';
update permissions set description = 'View Agent profile' where name ='VIEW_AGENT_PROFILE';
update permissions set description = 'View Certified enrollment entity counselor profile' where name ='VIEW_CEC_PROFILE';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'Provides users access to the assign tickets.' where name ='TKM_ASSIGN_ALLOWED';
update permissions set description = 'Adding MANAGE_BATCH permission to manage permissions for batch ui operations' where name ='MANAGE_BATCH';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';
update permissions set description = 'Manage Members and Applicants
- Manage Members
- Manage Applicants' where name ='MEMBER_VIEW_LIST';
update permissions set description = 'Manage Members and Applicants
 - Search for a Member
 - Search for an Applicant' where name ='MEMBER_SEARCH_APPLICANTS';
update permissions set description = 'This permission allows authorized users to view member data.
 - View Member Account
 - Add Comment
 - Security Questions
 - Mark RIDP Verified
 - View Consumer dashboard
 - Ticket History
 - Appeal History
 - Edit Basic Information
 - Send Link activation
 - Enrollment History
 - Resend 834
 - History' where name ='CRM_VIEW_MEMBER';
update permissions set description = 'GIVES USERS ACCESS TO 1095 ENROLLMENT FLOWS' where name ='1095_ENROLLMENT_DATA';
update permissions set description = 'This permissions allows authorized user to view Verification Results page from My applications page.' where name ='IND_PORTAL_VIEW_VER_RESULTS';