package com.getinsured.hix.dto.enrollment;

import java.time.Instant;

import org.springframework.http.HttpStatus;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * -----------------------------------------------------------------------------
 * QhpReportResponseDTO class is used to get response of QHP Reports entries.
 * -----------------------------------------------------------------------------
 * 
 * @since May 23, 2019
 */
public class QhpReportResponseDTO {

	private HttpStatus statusCode;
	private String message; // optional: success message or error payload
	private long executionDuration;
	private long startTime;

	public QhpReportResponseDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getExecutionDuration() {
		return executionDuration;
	}

	public void startResponse() {
		startTime = Instant.now().toEpochMilli();
	}

	public void endResponse() {
		long endTime = Instant.now().toEpochMilli();
		executionDuration = endTime - startTime;
	}
}
