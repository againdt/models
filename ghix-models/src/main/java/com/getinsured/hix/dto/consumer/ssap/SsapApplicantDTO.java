package com.getinsured.hix.dto.consumer.ssap;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.dto.eligibility.ConsumerEligibilityProgramDTO;
import com.getinsured.hix.model.Location;

public class SsapApplicantDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private Date birthDate;
	private String firstName;
	private String middleName;
	private String suffix;
	private String ssn;

	private String lastName;
	private String gender;
	private String householdContactFlag;
	private Location mailingLocation;
	private Location otherLocation;
	private String married;
	private String tobaccoUser;
	private String ffeApplicantId;
	private String email;
	private String phoneNumber;
	private Integer monthlyAptcAmount;
	private String eligibilityStatus;
	private List<ConsumerEligibilityProgramDTO> eligibilityProgramDTOList;
	private ConsumerEligibilityProgramDTO eligibilityProgramDTO;
	private String relationship;
	private String enrollmentId;
	private String exchgIndivIdentifier;
	private String primaryPhoneNo;
	private String prefEmail;
	private Location homeAddress;
	private String personType;
	private String height;
	private String weight;
	private String birthDateString;
	private String seekingCoverage;
	private String citizenShipStatus;
	private String advancedPremiumTaxCredit;
	private String costSharingReduction;
	private String medicalEligibilityStatus;

	private String exchangeEligibility;
	private String aptcEligibility;
	private String csrEligibility;
	private String medicaidEligibility;
	private String chipEligibility;
	private String csrLevel;
	private Long ssapApplicationId;
	private String eligibilityType;
	private String eligibilityIndicator;
	private Date eligibilityStartDate;
	private Date eligibilityEndDate;
	private String nativeAmericanFlag;

	public String getAdvancedPremiumTaxCredit() {
		return advancedPremiumTaxCredit;
	}

	public void setAdvancedPremiumTaxCredit(String advancedPremiumTaxCredit) {
		this.advancedPremiumTaxCredit = advancedPremiumTaxCredit;
	}

	public String getCostSharingReduction() {
		return costSharingReduction;
	}

	public void setCostSharingReduction(String costSharingReduction) {
		this.costSharingReduction = costSharingReduction;
	}

	public String getCitizenShipStatus() {
		return citizenShipStatus;
	}

	public void setCitizenShipStatus(String citizenShipStatus) {
		this.citizenShipStatus = citizenShipStatus;
	}

	public String getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(String seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
	}

	private String fFMHouseholdResponse;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getExternalApplicantId() {
		return ffeApplicantId;
	}

	public void setExternalApplicantId(String ffeApplicantId) {
		this.ffeApplicantId = ffeApplicantId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHouseholdContactFlag() {
		return householdContactFlag;
	}

	public void setHouseholdContactFlag(String householdContactFlag) {
		this.householdContactFlag = householdContactFlag;
	}

	public Location getMailingLocation() {
		return mailingLocation;
	}

	public void setMailingLocation(Location mailingLocation) {
		this.mailingLocation = mailingLocation;
	}

	public Location getOtherLocation() {
		return otherLocation;
	}

	public void setOtherLocation(Location otherLocation) {
		this.otherLocation = otherLocation;
	}

	public String getMarried() {
		return married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public String getTobaccoUser() {
		return tobaccoUser;
	}

	public void setTobaccoUser(String tobaccoUser) {
		this.tobaccoUser = tobaccoUser;
	}

	public String getFfeApplicantId() {
		return ffeApplicantId;
	}

	public void setFfeApplicantId(String ffeApplicantId) {
		this.ffeApplicantId = ffeApplicantId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getMonthlyAptcAmount() {
		return monthlyAptcAmount;
	}

	public void setMonthlyAptcAmount(Integer monthlyAptcAmount) {
		this.monthlyAptcAmount = monthlyAptcAmount;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public List<ConsumerEligibilityProgramDTO> getEligibilityProgramDTOList() {
		return eligibilityProgramDTOList;
	}

	public void setEligibilityProgramDTOList(List<ConsumerEligibilityProgramDTO> eligibilityProgramDTOList) {
		this.eligibilityProgramDTOList = eligibilityProgramDTOList;
	}

	public ConsumerEligibilityProgramDTO getEligibilityProgramDTO() {
		return eligibilityProgramDTO;
	}

	public void setEligibilityProgramDTO(ConsumerEligibilityProgramDTO eligibilityProgramDTO) {
		this.eligibilityProgramDTO = eligibilityProgramDTO;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}

	public Location getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Location homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getPrimaryPhoneNo() {
		return primaryPhoneNo;
	}

	public void setPrimaryPhoneNo(String primaryPhoneNo) {
		this.primaryPhoneNo = primaryPhoneNo;
	}

	public String getPrefEmail() {
		return prefEmail;
	}

	public void setPrefEmail(String prefEmail) {
		this.prefEmail = prefEmail;
	}

	public String getfFMHouseholdResponse() {
		return fFMHouseholdResponse;
	}

	public void setfFMHouseholdResponse(String fFMHouseholdResponse) {
		this.fFMHouseholdResponse = fFMHouseholdResponse;
	}

	public String getBirthDateString() {
		return birthDateString;
	}

	public void setBirthDateString(String birthDateString) {
		this.birthDateString = birthDateString;
	}

	public String getMedicalEligibilityStatus() {
		return medicalEligibilityStatus;
	}

	public void setMedicalEligibilityStatus(String medicalEligibilityStatus) {
		this.medicalEligibilityStatus = medicalEligibilityStatus;
	}

	public String getExchangeEligibility() {
		return exchangeEligibility;
	}

	public void setExchangeEligibility(String exchangeEligibility) {
		this.exchangeEligibility = exchangeEligibility;
	}

	public String getAptcEligibility() {
		return aptcEligibility;
	}

	public void setAptcEligibility(String aptcEligibility) {
		this.aptcEligibility = aptcEligibility;
	}

	public String getCsrEligibility() {
		return csrEligibility;
	}

	public void setCsrEligibility(String csrEligibility) {
		this.csrEligibility = csrEligibility;
	}

	public String getMedicaidEligibility() {
		return medicaidEligibility;
	}

	public void setMedicaidEligibility(String medicaidEligibility) {
		this.medicaidEligibility = medicaidEligibility;
	}

	public String getChipEligibility() {
		return chipEligibility;
	}

	public void setChipEligibility(String chipEligibility) {
		this.chipEligibility = chipEligibility;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getEligibilityType() {
		return eligibilityType;
	}

	public void setEligibilityType(String eligibilityType) {
		this.eligibilityType = eligibilityType;
	}

	public String getEligibilityIndicator() {
		return eligibilityIndicator;
	}

	public void setEligibilityIndicator(String eligibilityIndicator) {
		this.eligibilityIndicator = eligibilityIndicator;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNativeAmericanFlag() {
		return nativeAmericanFlag;
	}

	public void setNativeAmericanFlag(String nativeAmericanFlag) {
		this.nativeAmericanFlag = nativeAmericanFlag;
	}

}
