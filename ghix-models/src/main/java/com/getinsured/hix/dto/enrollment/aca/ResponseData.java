package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ResponseData{
  @JsonProperty("MarketplaceId")
  
  private Object MarketplaceId;
  @JsonProperty("PolicyInfo")
  
  private List<PolicyInfo> PolicyInfo;
  @JsonProperty("InsuredPeople")
  
  private List<InsuredPeople> InsuredPeople;
  @JsonProperty("Subsidy")
  
  private Integer Subsidy;
  public void setMarketplaceId(Object MarketplaceId){
   this.MarketplaceId=MarketplaceId;
  }
  public Object getMarketplaceId(){
   return MarketplaceId;
  }
  public void setPolicyInfo(List<PolicyInfo> PolicyInfo){
   this.PolicyInfo=PolicyInfo;
  }
  public List<PolicyInfo> getPolicyInfo(){
   return PolicyInfo;
  }
  public void setInsuredPeople(List<InsuredPeople> InsuredPeople){
   this.InsuredPeople=InsuredPeople;
  }
  public List<InsuredPeople> getInsuredPeople(){
   return InsuredPeople;
  }
  public void setSubsidy(Integer Subsidy){
   this.Subsidy=Subsidy;
  }
  public Integer getSubsidy(){
   return Subsidy;
  }
}