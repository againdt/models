package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class CallContextDTO implements Serializable {	
	private static final long serialVersionUID = -8097560569227277600L;
	private String callType;
	private String callFrom;
	private String callTo;
	private String transferredFrom;


	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getCallFrom() {
		return callFrom;
	}
	public void setCallFrom(String callFrom) {
		this.callFrom = callFrom;
	}
	public String getCallTo() {
		return callTo;
	}
	public void setCallTo(String callTo) {
		this.callTo = callTo;
	}
	public String getTransferredFrom() {
		return transferredFrom;
	}
	public void setTransferredFrom(String transferredFrom) {
		this.transferredFrom = transferredFrom;
	}
	
	
}
