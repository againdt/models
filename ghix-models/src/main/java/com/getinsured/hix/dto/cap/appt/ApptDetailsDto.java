package com.getinsured.hix.dto.cap.appt;

import com.getinsured.hix.dto.cap.CapAgentEmploymentDto;
import com.getinsured.hix.dto.cap.consumerapp.EligLeadDTO;

import java.io.Serializable;

public class ApptDetailsDto implements Serializable {
	private static final long serialVersionUID = 8346862608161860703L;

	private String id;
	private Integer duration; 
	private String appointmentDate;
	private String startDate;
	private String endDate;
	private String comments;
	private String status;
	private Integer createdBy;
	private Integer lastUpdatedBy;
	private String timeSlot;
	private String zipcode;
	private String countyCode;
	private String timezone;
	private String stateCode;
	private String consumerComments;
	//	HIX-97759 Send out TimeZone in Appointment DTO
	private String customerTimeZone;
	// HIX-98865 Add "CustomerTimeSlot" in the API response
	private String customerTimeSlot;
	private String idEnc;

	// HIX-98222 Add callStatus field in appointment detail
	private String callStatus;

	// HIX-98416 Backend for Appointment Type
	private String appointmentType;

	private CapAgentEmploymentDto capAgentDto;
	private EligLeadDTO eligLeadDTO;

	private String errorMessage;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CapAgentEmploymentDto getCapAgentDto() {
		return capAgentDto;
	}

	public void setCapAgentDto(CapAgentEmploymentDto capAgentDto) {
		this.capAgentDto = capAgentDto;
	}

	public EligLeadDTO getEligLeadDTO() {
		return eligLeadDTO;
	}

	public void setEligLeadDTO(EligLeadDTO eligLeadDTO) {
		this.eligLeadDTO = eligLeadDTO;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String getTimeSlot() {
		return timeSlot;
	}
	
	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getConsumerComments() {
		return consumerComments;
	}

	public void setConsumerComments(String consumerComments) {
		this.consumerComments = consumerComments;
	}

	public String getCustomerTimeZone() {
		return customerTimeZone;
	}

	public void setCustomerTimeZone(String customerTimeZone) {
		this.customerTimeZone = customerTimeZone;
	}

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getCustomerTimeSlot() {
		return customerTimeSlot;
	}

	public void setCustomerTimeSlot(String customerTimeSlot) {
		this.customerTimeSlot = customerTimeSlot;
	}

	public String getIdEnc() {
		return idEnc;
	}

	public void setIdEnc(String idEnc) {
		this.idEnc = idEnc;
	}
	
}
