package com.getinsured.hix.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.prescription.PrescriptionSearchResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PlanTab;

public class IndividualPlan {

	private Integer id;
	private String issuerPlanNumber;
	private String name;
	private String level;
	private float premium;
	private String issuer;
	private String issuerText;
	private String networkType;
	//Plan NetworkKey(has_provider_data =YES values only
	private String networkKey;
	private String networkName;
	private Integer applicableYear;
	private float planScore;
	private float rawOutOfPocketCost;
	private float outOfPocketEstimate;
	private float premiumBeforeCredit;
	private float annualPremiumBeforeCredit;
	private float aptc;
	private BigDecimal stateSubsidy;
	private float totalContribution;
	private float premiumAfterCredit;
	private float annualPremiumAfterCredit;
	private float costSharingReductions;
	private float adjustedOop;
	private Float oopMax; 
	private Float childOopMax;
	private float maxTotalHealthCareCost;
	private float estimatedTotalHealthCareCost;
	private String costSharing;
	private Integer deductible;
	private Integer intgMediDrugDeductible;
	private Integer medicalDeductible;
	private Integer drugDeductible;
	private Integer planId;
	private Integer groupId;
	private String groupName;
	private String groupMembers;
	private Integer orderItemId;
	private String result;
	private String planType;
	private float ehb;
	private Integer enrollmentId;
	private Float oldPremium;
	private Integer smartScore;
	private String expenseEstimate;
	private String qoutingGroup;
	private Float ehbPrecentage;
	private Float ehbPremium;
	private String qualityRating;

	private Map<String, Map<String,String>> planTier1;
	private Map<String, Map<String,String>> planTier2;
	private Map<String, Map<String,String>> planOutNet;
	private Map<String, Map<String,String>> planAppliesToDeduct;
	private Map<String, Map<String,String>> netwkException;
	private Map<String, Map<String,String>> missingDentalCovg;
	private Map<String, Map<String,String>> benefitExplanation;
	private Map<String,String> issuerQualityRating;
	private List<Map<String, String>> planDetailsByMember;
	private List<HashMap<String, Object>> doctors;
	private List<HashMap<String, Object>> facilities;
	private List<HashMap<String, Object>> dentists;
	private int doctorsCount;
	private int facilitiesCount;
	private int dentistsCount;
	private String doctorsSupported;
	private String facilitiesSupported;
	private String dentistsSupported;
	private int totalProvidersCount;
	private int docfacilityCount;
	private String overAllQuality;
	private Float oldAptc;
	private String sbcUrl;
	private String planBrochureUrl;
	private Map<String, Map<String, String>> planCosts;
	private String providerLink;
	private String issuerLogo;
	private Integer issuerId;
	private String maxCoinseForSpecialtyDrugs; 
	private String maxNumDaysForChargingInpatientCopay ; 
	private String primaryCareCostSharingAfterSetNumberVisits; 
	private String primaryCareDeductOrCoinsAfterSetNumberCopays;
	private Map<String, String> benefitsCoverage;
	private String isPuf;
	private String formularyUrl;
	private String hsa;
	private List<Map<String, String>> providers;	
	private String coinsurance;
	private String policyLength;
	private String policyLimit;
	private String applicationFee;
	private Float intgMediDrugOopMax;
	private Float medicalOopMax;
	private Float drugOopMax;
	private String dentalGuarantee;
	private String exchangeType;
	private String separateDrugDeductible;
	private String outOfNetwkCoverage;
	private String outOfCountyCoverage;
	private String policyLengthUnit;
	private String oopMaxFamily;
	private String oopMaxIndDesc;
	private Map<String, Map<String, String>> optionalDeductible;
	private Map<String,Float> contributionData;
	private Integer maxBenefits;
	private Map<String, String> providerDensity;
	private String copayGenericDrug;
	private String copayOfficeVisit;
	private Integer annualLimit;
	private String tier2Coverage;
	private String encryptedPremiumBeforeCredit;
	private String encryptedPremiumAfterCredit;
	private Integer dentalPlanId;
	private Integer amePlanId;
	private String dentalPlanPremium;
	private String amePlanPremium;
	private String encryptedDentalPlanPremium;
	private String encryptedAmePlanPremium;
	private String encodedPremium;
	private float visionEyeExam;
	private float visionGlasses;
	private float visionContacts;
	private Integer visionPlanId;
	private String visionPlanPremium;
	private Integer recommendationRank;
	private String coverageAmount;
	private String policyTerm;
	private String benefitUrl;
	private String networkTransparencyRating;
	private List<DrugDTO> prescriptionResponseList =new ArrayList<DrugDTO>();
    private PlanTab planTab;
	private Integer havingBabySBC;
	private Integer havingDiabetesSBC;
	private Integer simpleFractureSBC;

	public Map<String, Float> getContributionData() {
		return contributionData;
	}

	public void setContributionData(Map<String, Float> contributionData) {
		this.contributionData = contributionData;
	}

	public String getHsa() {
		return hsa;
	}

	public void setHsa(String hsa) {
		this.hsa = hsa;
	}

	public String getIsPuf() {
		return isPuf;
	}

	public void setIsPuf(String isPuf) {
		this.isPuf = isPuf;
	}

	public int getDocfacilityCount() {
		return docfacilityCount;
	}

	public void setDocfacilityCount(int docfacilityCount) {
		this.docfacilityCount = docfacilityCount;
	}

	public int getTotalProvidersCount() {
		return totalProvidersCount;
	}

	public void setTotalProvidersCount(int totalProvidersCount) {
		this.totalProvidersCount = totalProvidersCount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNetworkKey() {
		return networkKey;
	}

	public void setNetworkKey(String networkKey) {
		this.networkKey = networkKey;
	}
	
	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	public Float getEhbPrecentage() {
		return ehbPrecentage;
	}

	public void setEhbPrecentage(Float ehbPrecentage) {
		this.ehbPrecentage = ehbPrecentage;
	}
	
	public Float getEhbPremium() {
		return ehbPremium;
	}

	public void setEhbPremium(Float ehbPremium) {
		this.ehbPremium = ehbPremium;
	}

	public String getQualityRating() {
		return qualityRating;
	}

	public void setQualityRating(String qualityRating) {
		this.qualityRating = qualityRating;
	}
	
	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerText() {
		return issuerText;
	}

	public void setIssuerText(String issuerText) {
		this.issuerText = issuerText;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public Float getPlanScore() {
		return planScore;
	}

	public void setPlanScore(Float planScore) {
		this.planScore = planScore;
	}

	public float getRawOutOfPocketCost() {
		return rawOutOfPocketCost;
	}

	public void setRawOutOfPocketCost(float rawOutOfPocketCost) {
		this.rawOutOfPocketCost = rawOutOfPocketCost;
	}

	public float getOutOfPocketEstimate() {
		return outOfPocketEstimate;
	}

	public void setOutOfPocketEstimate(float outOfPocketEstimate) {
		this.outOfPocketEstimate = outOfPocketEstimate;
	}

	public float getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}

	public void setPremiumBeforeCredit(float premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}

	public float getAnnualPremiumBeforeCredit() {
		return annualPremiumBeforeCredit;
	}

	public void setAnnualPremiumBeforeCredit(float annualPremiumBeforeCredit) {
		this.annualPremiumBeforeCredit = annualPremiumBeforeCredit;
	}

	public float getAptc() {
		return aptc;
	}

	public void setAptc(float aptc) {
		this.aptc = aptc;
	}

	public float getPremiumAfterCredit() {
		return premiumAfterCredit;
	}

	public void setPremiumAfterCredit(float premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}

	public float getAnnualPremiumAfterCredit() {
		return annualPremiumAfterCredit;
	}

	public void setAnnualPremiumAfterCredit(float annualPremiumAfterCredit) {
		this.annualPremiumAfterCredit = annualPremiumAfterCredit;
	}

	public float getCostSharingReductions() {
		return costSharingReductions;
	}

	public void setCostSharingReductions(float costSharingReductions) {
		this.costSharingReductions = costSharingReductions;
	}

	public float getAdjustedOop() {
		return adjustedOop;
	}

	public void setAdjustedOop(float adjustedOop) {
		this.adjustedOop = adjustedOop;
	}
	
	public Float getOopMax() {
		return oopMax;
	}

	public void setOopMax(Float oopMax) {
		this.oopMax = oopMax;
	}
	
	public Float getChildOopMax() {
		return childOopMax;
	}

	public void setChildOopMax(Float childOopMax) {
		this.childOopMax = childOopMax;
	}

	public float getMaxTotalHealthCareCost() {
		return maxTotalHealthCareCost;
	}

	public void setMaxTotalHealthCareCost(float maxTotalHealthCareCost) {
		this.maxTotalHealthCareCost = maxTotalHealthCareCost;
	}

	public float getEstimatedTotalHealthCareCost() {
		return estimatedTotalHealthCareCost;
	}

	public void setEstimatedTotalHealthCareCost(float estimatedTotalHealthCareCost) {
		this.estimatedTotalHealthCareCost = estimatedTotalHealthCareCost;
	}

	public String getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}
	
	public Integer getDeductible() {
		return deductible;
	}

	public void setDeductible(Integer deductible) {
		this.deductible = deductible;
	}
	
	public Integer getPlanId() {
		return planId;
	}
	
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	
	public Integer getGroupId() {
		return groupId;
	}
	
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupMembers() {
		return groupMembers;
	}

	public void setGroupMembers(String groupMembers) {
		this.groupMembers = groupMembers;
	}
	
	public Integer getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Map<String, Map<String,String>> getPlanTier1() {
		return planTier1;
	}

	public void setPlanTier1(Map<String, Map<String,String>> planTier1) {
		this.planTier1 = planTier1;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public float getEhb() {
		return ehb;
	}

	public void setEhb(float ehb) {
		this.ehb = ehb;
	}

	public Map<String, Map<String,String>> getPlanTier2() {
		return planTier2;
	}

	public void setPlanTier2(Map<String, Map<String,String>> planTier2) {
		this.planTier2 = planTier2;
	}

	public Map<String, String> getIssuerQualityRating() {
		return issuerQualityRating;
	}

	public void setIssuerQualityRating(Map<String, String> issuerQualityRating) {
		this.issuerQualityRating = issuerQualityRating;
	}

	public List<Map<String, String>> getPlanDetailsByMember() {
		return planDetailsByMember;
	}

	public void setPlanDetailsByMember(
			List<Map<String, String>> planDetailsByMember) {
		this.planDetailsByMember = planDetailsByMember;
	}

	public float getTotalContribution() {
		return totalContribution;
	}

	public void setTotalContribution(float totalContribution) {
		this.totalContribution = totalContribution;
	}
	
	public List<HashMap<String, Object>> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<HashMap<String, Object>> doctors) {
		this.doctors = doctors;
	}

	public List<HashMap<String, Object>> getFacilities() {
		return facilities;
	}

	public void setFacilities(List<HashMap<String, Object>> facilities) {
		this.facilities = facilities;
	}

	public List<HashMap<String, Object>> getDentists() {
		return dentists;
	}

	public void setDentists(List<HashMap<String, Object>> dentists) {
		this.dentists = dentists;
	}

	public int getDoctorsCount() {
		return doctorsCount;
	}

	public void setDoctorsCount(int doctorsCount) {
		this.doctorsCount = doctorsCount;
	}

	public int getFacilitiesCount() {
		return facilitiesCount;
	}

	public void setFacilitiesCount(int facilitiesCount) {
		this.facilitiesCount = facilitiesCount;
	}

	public int getDentistsCount() {
		return dentistsCount;
	}

	public void setDentistsCount(int dentistsCount) {
		this.dentistsCount = dentistsCount;
	}

	public String getDoctorsSupported() {
		return doctorsSupported;
	}

	public void setDoctorsSupported(String doctorsSupported) {
		this.doctorsSupported = doctorsSupported;
	}

	public String getFacilitiesSupported() {
		return facilitiesSupported;
	}

	public void setFacilitiesSupported(String facilitiesSupported) {
		this.facilitiesSupported = facilitiesSupported;
	}

	public String getDentistsSupported() {
		return dentistsSupported;
	}

	public void setDentistsSupported(String dentistsSupported) {
		this.dentistsSupported = dentistsSupported;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}	

	public Float getOldPremium() {
		return oldPremium;
	}

	public void setOldPremium(Float oldPremium) {
		this.oldPremium = oldPremium;
	}

	public String getOverAllQuality() {
		return overAllQuality;
	}

	public void setOverAllQuality(String overAllQuality) {
		this.overAllQuality = overAllQuality;
	}

	public Float getOldAptc() {
		return oldAptc;
	}

	public void setOldAptc(Float oldAptc) {
		this.oldAptc = oldAptc;
	}

	public Map<String, Map<String,String>> getPlanOutNet() {
		return planOutNet;
	}

	public void setPlanOutNet(Map<String, Map<String,String>> planOutNet) {
		this.planOutNet = planOutNet;
	}

	public Map<String, Map<String,String>> getPlanAppliesToDeduct() {
		return planAppliesToDeduct;
	}

	public void setPlanAppliesToDeduct(Map<String, Map<String,String>> planAppliesToDeduct) {
		this.planAppliesToDeduct = planAppliesToDeduct;
	}
	
	public Map<String, Map<String, String>> getNetwkException() {
		return netwkException;
	}

	public void setNetwkException(Map<String, Map<String, String>> netwkException) {
		this.netwkException = netwkException;
	}

	public String getSbcUrl() {
		return sbcUrl;
	}

	public void setSbcUrl(String sbcUrl) {
		this.sbcUrl = sbcUrl;
	}

	public String getPlanBrochureUrl() {
		return planBrochureUrl;
	}

	public void setPlanBrochureUrl(String planBrochureUrl) {
		this.planBrochureUrl = planBrochureUrl;
	}

	public Map<String, Map<String, String>> getPlanCosts() {
		return planCosts;
	}

	public void setPlanCosts(Map<String, Map<String, String>> planCosts) {
		this.planCosts = planCosts;
	}

	public String getProviderLink() {
		return providerLink;
	}

	public void setProviderLink(String providerLink) {
		this.providerLink = providerLink;
	}

	public String getIssuerLogo() {
		return issuerLogo;
	}

	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}

	public Integer getSmartScore() {
		return smartScore;
	}

	public void setSmartScore(Integer smartScore) {
		this.smartScore = smartScore;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public String getMaxCoinseForSpecialtyDrugs() {
		return maxCoinseForSpecialtyDrugs;
	}

	public void setMaxCoinseForSpecialtyDrugs(String maxCoinseForSpecialtyDrugs) {
		this.maxCoinseForSpecialtyDrugs = maxCoinseForSpecialtyDrugs;
	}

	public String getMaxNumDaysForChargingInpatientCopay() {
		return maxNumDaysForChargingInpatientCopay;
	}

	public void setMaxNumDaysForChargingInpatientCopay(
			String maxNumDaysForChargingInpatientCopay) {
		this.maxNumDaysForChargingInpatientCopay = maxNumDaysForChargingInpatientCopay;
	}

	public String getPrimaryCareCostSharingAfterSetNumberVisits() {
		return primaryCareCostSharingAfterSetNumberVisits;
	}

	public void setPrimaryCareCostSharingAfterSetNumberVisits(
			String primaryCareCostSharingAfterSetNumberVisits) {
		this.primaryCareCostSharingAfterSetNumberVisits = primaryCareCostSharingAfterSetNumberVisits;
	}

	public String getPrimaryCareDeductOrCoinsAfterSetNumberCopays() {
		return primaryCareDeductOrCoinsAfterSetNumberCopays;
	}

	public void setPrimaryCareDeductOrCoinsAfterSetNumberCopays(
			String primaryCareDeductOrCoinsAfterSetNumberCopays) {
		this.primaryCareDeductOrCoinsAfterSetNumberCopays = primaryCareDeductOrCoinsAfterSetNumberCopays;
	}

	public Map<String, String> getBenefitsCoverage() {
		return benefitsCoverage;
	}

	public void setBenefitsCoverage(Map<String, String> benefitsCoverage) {
		this.benefitsCoverage = benefitsCoverage;
	}

	public String getExpenseEstimate() {
		return expenseEstimate;
	}

	public void setExpenseEstimate(String expenseEstimate) {
		this.expenseEstimate = expenseEstimate;
	}
	
	public Map<String, Map<String, String>> getMissingDentalCovg() {
		return missingDentalCovg;
	}

	public void setMissingDentalCovg(
			Map<String, Map<String, String>> missingDentalCovg) {
		this.missingDentalCovg = missingDentalCovg;
	}
	public String getFormularyUrl() {
		return formularyUrl;
	}

	public void setFormularyUrl(String formularyUrl) {
		this.formularyUrl = formularyUrl;
	}

	public Integer getMedicalDeductible() {
		return medicalDeductible;
	}

	public void setMedicalDeductible(Integer medicalDeductible) {
		this.medicalDeductible = medicalDeductible;
	}

	public Integer getDrugDeductible() {
		return drugDeductible;
	}

	public void setDrugDeductible(Integer drugDeductible) {
		this.drugDeductible = drugDeductible;
	}
	public Integer getIntgMediDrugDeductible() {
		return intgMediDrugDeductible;
	}

	public void setIntgMediDrugDeductible(Integer intgMediDrugDeductible) {
		this.intgMediDrugDeductible = intgMediDrugDeductible;
	}
	public Float getIntgMediDrugOopMax() {
		return intgMediDrugOopMax;
	}

	public void setIntgMediDrugOopMax(Float intgMediDrugOopMax) {
		this.intgMediDrugOopMax = intgMediDrugOopMax;
	}

	public Float getMedicalOopMax() {
		return medicalOopMax;
	}

	public void setMedicalOopMax(Float medicalOopMax) {
		this.medicalOopMax = medicalOopMax;
	}

	public Float getDrugOopMax() {
		return drugOopMax;
	}

	public void setDrugOopMax(Float drugOopMax) {
		this.drugOopMax = drugOopMax;
	}
	
	public String getQoutingGroup() {
		return qoutingGroup;
	}

	public void setQoutingGroup(String qoutingGroup) {
		this.qoutingGroup = qoutingGroup;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}
	
	public Map<String, Map<String, String>> getOptionalDeductible() {
		return optionalDeductible;
	}

	public void setOptionalDeductible(Map<String, Map<String, String>> optionalDeductible) {
		this.optionalDeductible = optionalDeductible;
	}
	
	public String getDentalGuarantee() {
		return dentalGuarantee;
	}

	public void setDentalGuarantee(String dentalGuarantee) {
		this.dentalGuarantee = dentalGuarantee;
	}

	public List<Map<String, String>> getProviders() {
		return providers;
	}

	public void setProviders(List<Map<String, String>> providers) {
		this.providers = providers;
	}

	public String getCoinsurance() {
		return coinsurance;
	}

	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}

	public String getPolicyLength() {
		return policyLength;
	}

	public void setPolicyLength(String policyLength) {
		this.policyLength = policyLength;
	}

	public String getPolicyLimit() {
		return policyLimit;
	}

	public void setPolicyLimit(String policyLimit) {
		this.policyLimit = policyLimit;
	}

	public String getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(String applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getSeparateDrugDeductible() {
		return separateDrugDeductible;
	}

	public void setSeparateDrugDeductible(String separateDrugDeductible) {
		this.separateDrugDeductible = separateDrugDeductible;
	}

	public String getOutOfNetwkCoverage() {
		return outOfNetwkCoverage;
	}

	public void setOutOfNetwkCoverage(String outOfNetwkCoverage) {
		this.outOfNetwkCoverage = outOfNetwkCoverage;
	}

	public String getOutOfCountyCoverage() {
		return outOfCountyCoverage;
	}

	public void setOutOfCountyCoverage(String outOfCountyCoverage) {
		this.outOfCountyCoverage = outOfCountyCoverage;
	}

	public String getPolicyLengthUnit() {
		return policyLengthUnit;
	}

	public void setPolicyLengthUnit(String policyLengthUnit) {
		this.policyLengthUnit = policyLengthUnit;
	}

	public String getOopMaxFamily() {
		return oopMaxFamily;
	}

	public void setOopMaxFamily(String oopMaxFamily) {
		this.oopMaxFamily = oopMaxFamily;
	}

	public String getOopMaxIndDesc() {
		return oopMaxIndDesc;
	}

	public void setOopMaxIndDesc(String oopMaxIndDesc) {
		this.oopMaxIndDesc = oopMaxIndDesc;
	}

	public Integer getMaxBenefits() {
		return maxBenefits;
	}

	public void setMaxBenefits(Integer maxBenefits) {
		this.maxBenefits = maxBenefits;
	}

	public Map<String, String> getProviderDensity() {
		return providerDensity;
	}

	public void setProviderDensity(Map<String, String> providerDensity) {
		this.providerDensity = providerDensity;
	}

	public String getCopayGenericDrug() {
		return copayGenericDrug;
	}

	public void setCopayGenericDrug(String copayGenericDrug) {
		this.copayGenericDrug = copayGenericDrug;
	}

	public Integer getAnnualLimit() {
		return annualLimit;
	}

	public void setAnnualLimit(Integer annualLimit) {
		this.annualLimit = annualLimit;
	}
	
	public Map<String, Map<String, String>> getBenefitExplanation() {
		return benefitExplanation;
	}

	public void setBenefitExplanation(Map<String, Map<String, String>> benefitExplanation) {
		this.benefitExplanation = benefitExplanation;
	}

	public String getTier2Coverage() {
		return tier2Coverage;
	}

	public void setTier2Coverage(String tier2Coverage) {
		this.tier2Coverage = tier2Coverage;
	}

	public String getEncryptedPremiumBeforeCredit() {
		return encryptedPremiumBeforeCredit;
	}

	public void setEncryptedPremiumBeforeCredit(String encryptedPremiumBeforeCredit) {
		this.encryptedPremiumBeforeCredit = encryptedPremiumBeforeCredit;
	}

	public String getEncryptedPremiumAfterCredit() {
		return encryptedPremiumAfterCredit;
	}

	public void setEncryptedPremiumAfterCredit(String encryptedPremiumAfterCredit) {
		this.encryptedPremiumAfterCredit = encryptedPremiumAfterCredit;
	}

	public Integer getDentalPlanId() {
		return dentalPlanId;
	}

	public void setDentalPlanId(Integer dentalPlanId) {
		this.dentalPlanId = dentalPlanId;
	}

	public Integer getAmePlanId() {
		return amePlanId;
	}

	public void setAmePlanId(Integer amePlanId) {
		this.amePlanId = amePlanId;
	}

	public String getDentalPlanPremium() {
		return dentalPlanPremium;
	}

	public void setDentalPlanPremium(String dentalPlanPremium) {
		this.dentalPlanPremium = dentalPlanPremium;
	}

	public String getAmePlanPremium() {
		return amePlanPremium;
	}

	public void setAmePlanPremium(String amePlanPremium) {
		this.amePlanPremium = amePlanPremium;
	}

	public String getEncryptedDentalPlanPremium() {
		return encryptedDentalPlanPremium;
	}

	public void setEncryptedDentalPlanPremium(String encryptedDentalPlanPremium) {
		this.encryptedDentalPlanPremium = encryptedDentalPlanPremium;
	}

	public String getEncryptedAmePlanPremium() {
		return encryptedAmePlanPremium;
	}

	public void setEncryptedAmePlanPremium(String encryptedAmePlanPremium) {
		this.encryptedAmePlanPremium = encryptedAmePlanPremium;
	}

	public String getEncodedPremium() {
		return encodedPremium;
	}

	public void setEncodedPremium(String encodedPremium) {
		this.encodedPremium = encodedPremium;
	}

	public String getCopayOfficeVisit() {
		return copayOfficeVisit;
	}

	public void setCopayOfficeVisit(String copayOfficeVisit) {
		this.copayOfficeVisit = copayOfficeVisit;
	}
	
	public float getVisionEyeExam() {
		return visionEyeExam;
	}

	public void setVisionEyeExam(float visionEyeExam) {
		this.visionEyeExam = visionEyeExam;
	}
	
	public float getVisionGlasses() {
		return visionGlasses;
	}

	public void setVisionGlasses(float visionGlasses) {
		this.visionGlasses = visionGlasses;
	}
	
	public float getVisionContacts() {
		return visionContacts;
	}

	public void setVisionContacts(float visionContacts) {
		this.visionContacts = visionContacts;
	}

	public List<DrugDTO> getPrescriptionResponseList() {
		return prescriptionResponseList;
	}

	public void setPrescriptionResponseList(List<DrugDTO> prescriptionResponseList) {
		this.prescriptionResponseList = prescriptionResponseList;
	}

	public Integer getVisionPlanId() {
		return visionPlanId;
	}

	public void setVisionPlanId(Integer visionPlanId) {
		this.visionPlanId = visionPlanId;
	}

	public String getVisionPlanPremium() {
		return visionPlanPremium;
	}

	public void setVisionPlanPremium(String visionPlanPremium) {
		this.visionPlanPremium = visionPlanPremium;
	}

	public Integer getRecommendationRank() {
		return recommendationRank;
	}

	public void setRecommendationRank(Integer recommendationRank) {
		this.recommendationRank = recommendationRank;
	}

	public String getCoverageAmount() {
		return coverageAmount;
	}

	public void setCoverageAmount(String coverageAmount) {
		this.coverageAmount = coverageAmount;
	}

	public String getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}

	public String getBenefitUrl() {
		return benefitUrl;
	}

	public void setBenefitUrl(String benefitUrl) {
		this.benefitUrl = benefitUrl;
	}

	public String getNetworkTransparencyRating() {
		return networkTransparencyRating;
	}

	public void setNetworkTransparencyRating(String networkTransparencyRating) {
		this.networkTransparencyRating = networkTransparencyRating;
	}

	public PlanTab getPlanTab() {
		return planTab;
	}

	public void setPlanTab(PlanTab planTab) {
		this.planTab = planTab;
	}

	public Integer getHavingBabySBC() {
		return havingBabySBC;
	}

	public void setHavingBabySBC(Integer havingBabySBC) {
		this.havingBabySBC = havingBabySBC;
	}

	public Integer getHavingDiabetesSBC() {
		return havingDiabetesSBC;
	}

	public void setHavingDiabetesSBC(Integer havingDiabetesSBC) {
		this.havingDiabetesSBC = havingDiabetesSBC;
	}

	public Integer getSimpleFractureSBC() {
		return simpleFractureSBC;
	}

	public void setSimpleFractureSBC(Integer simpleFractureSBC) {
		this.simpleFractureSBC = simpleFractureSBC;
	}

	public BigDecimal getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(BigDecimal stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}
}
