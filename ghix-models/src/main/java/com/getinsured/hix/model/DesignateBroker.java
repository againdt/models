package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * The persistent class for the Broker database table.
 * 
 */
@Entity
@Audited
@Table(name = "designate_broker")
public class DesignateBroker implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Brokers_Seq")
	@SequenceGenerator(name = "Brokers_Seq", sequenceName = "brokers_Seq", allocationSize = 1)
	private int id;

	public enum Status {
		Active, Pending, InActive;
	}

	@Column(name = "brokerid")
	private int brokerId;

	@Column(name = "employerid")
	private Integer employerId;

	@Column(name = "individualid")
	private Integer individualId;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updated;	

	@Column(name = "esign_by")
	private String esignBy;

	@Column(name = "esign_date", columnDefinition = "date")
	private Date esignDate;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "externalemployerid")
	private Integer externalEmployerId;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "externalindividualid")
	private Integer externalIndividualId;
	
	@Column(name = "show_switch_role_popup")
	private Character show_switch_role_popup;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name="delegation_code")
	private String delegationCode;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "LAST_UPDATED_BY")
	private Integer updatedBy;
	
	public DesignateBroker() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public int getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(int brokerId) {
		this.brokerId = brokerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public Integer getEmployerId() {
		return this.employerId;

	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getEsignBy() {
		return esignBy;
	}

	public void setEsignBy(String esignBy) {
		this.esignBy = esignBy;
	}

	public Date getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Date esignDate) {
		this.esignDate = esignDate;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());

	}

	public Integer getIndividualId() {
		return individualId;
	}

	public void setIndividualId(Integer individualId) {
		this.individualId = individualId;
	}

	public Integer getExternalEmployerId() {
		return externalEmployerId;
	}

	public void setExternalEmployerId(Integer externalEmployerId) {
		this.externalEmployerId = externalEmployerId;
	}

	public Integer getExternalIndividualId() {
		return externalIndividualId;
	}

	public void setExternalIndividualId(Integer externalIndividualId) {
		this.externalIndividualId = externalIndividualId;
	}
	
	public Character getShow_switch_role_popup() {
		return show_switch_role_popup;
	}

	public void setShow_switch_role_popup(Character show_switch_role_popup) {
		this.show_switch_role_popup = show_switch_role_popup;
	}
	
	public String getDelegationCode() {
		return delegationCode;
	}

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdated(new TSDate());
	}

	public Object clone() {
		DesignateBroker brokerObj = new DesignateBroker();
		brokerObj.setId(getId());
		brokerObj.setBrokerId(getBrokerId());
		brokerObj.setEmployerId(getBrokerId());
		brokerObj.setIndividualId(getIndividualId());
		brokerObj.setStatus(getStatus());
		brokerObj.setExternalEmployerId(getExternalEmployerId());
		brokerObj.setExternalIndividualId(getExternalIndividualId());
		brokerObj.setShow_switch_role_popup(getShow_switch_role_popup());
		return brokerObj;
	}
}
