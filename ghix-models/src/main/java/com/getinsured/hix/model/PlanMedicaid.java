/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * @author sharma_va
 *
 */
@Audited
@Entity
@Table(name = "PLAN_MEDICAID")
public class PlanMedicaid implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_MEDICAID_SEQ")
	@SequenceGenerator(name = "PLAN_MEDICAID_SEQ", sequenceName = "PLAN_MEDICAID_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;
	
	@Column(name = "MEMBER_NUMBER")
	private String memberNumber;
	
	@Column(name = "WEBSITE_URL")
	private String websiteURL;
	
	@Column(name = "QUALITY_RATING")
	private String qualityRating;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
	@Column(name = "COVERAGE_AREA")
	private String coverageArea;
	
	public PlanMedicaid() {
		
	}
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public String getMemberNumber() {
		return memberNumber;
	}
	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}
	public String getWebsiteURL() {
		return websiteURL;
	}
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}
	public String getQualityRating() {
		return qualityRating;
	}
	public void setQualityRating(String qualityRating) {
		this.qualityRating = qualityRating;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getCoverageArea() {
		return coverageArea;
	}
	public void setCoverageArea(String coverageArea) {
		this.coverageArea = coverageArea;
	}
	
}
