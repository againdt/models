Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ASSISTERENROLLMENTENTITYADMIN','/entity/entityadmin/managelist','ASSISTERENROLLMENTENTITYADMIN',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED,POST_REG_URL) values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ASSISTERENROLLMENTENTITY','/entity/enrollmententity/entityinformation','ASSISTERENROLLMENTENTITY',SYSTIMESTAMP,'/enrollmententity/registration');

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('manage_entity')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('manage_assister')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ASSISTERENROLLMENTENTITYADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));

