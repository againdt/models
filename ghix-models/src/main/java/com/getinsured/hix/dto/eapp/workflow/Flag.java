package com.getinsured.hix.dto.eapp.workflow;

/**
 * Created by root on 7/27/16.
 */
public class Flag {
    private FlagName name;
    private boolean enabled;

    public void setName(FlagName name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public FlagName getName() {
        return name;
    }
}
