
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class TaxFilerDependant {

    @Expose
    private Integer dependantHouseholdMemeberId;

    @Expose
    private Boolean taxFilerDependantIndicator;
    
    public Boolean getTaxFilerDependantIndicator() {
		return taxFilerDependantIndicator;
	}

	public void setTaxFilerDependantIndicator(Boolean taxFilerDependantIndicator) {
		this.taxFilerDependantIndicator = taxFilerDependantIndicator;
	}

	public Integer getDependantHouseholdMemeberId() {
        return dependantHouseholdMemeberId;
    }

    public void setDependantHouseholdMemeberId(Integer dependantHouseholdMemeberId) {
        this.dependantHouseholdMemeberId = dependantHouseholdMemeberId;
    }

}
