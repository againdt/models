package com.getinsured.hix.dto.shop;

import java.util.List;

public class RestEmployerEnrollmentDTO {
	
	private List<Integer> employerEnrollmentList;
	private String coverageEligibilityDate;
	
	
	public List<Integer> getEmployerEnrollmentList() {
		return employerEnrollmentList;
	}
	public void setEmployerEnrollmentList(List<Integer> employerEnrollmentList) {
		this.employerEnrollmentList = employerEnrollmentList;
	}
	public String getCoverageEligibilityDate() {
		return coverageEligibilityDate;
	}
	public void setCoverageEligibilityDate(String coverageEligibilityDate) {
		this.coverageEligibilityDate = coverageEligibilityDate;
	}
	
	

}
