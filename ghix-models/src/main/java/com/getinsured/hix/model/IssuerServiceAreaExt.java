package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="PM_ISSUER_SERVICE_AREA_EXT")
public class IssuerServiceAreaExt implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2055563313749682524L;

	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_ISSUER_SERVICE_AREA_EXT_SEQ")
	@SequenceGenerator(name = "PM_ISSUER_SERVICE_AREA_EXT_SEQ", sequenceName = "PM_ISSUER_SERVICE_AREA_EXT_SEQ", allocationSize = 1)
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ISSUER_SERVICE_AREA_ID")
	private IssuerServiceArea serviceAreaId;

	@Column(name="COUNTY")
	private String county;

	@Column(name="PARTIAL_COUNTY")
	private String partialCounty;

	@Column(name="PARTIAL_COUNTY_JUSTIFICATION")
	private String partialCountyJustification;

	@OneToMany(mappedBy = "serviceAreaExtId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ServiceArea> serviceArea;

	@Column(name = "IS_DELETED")
	//@Enumerated(EnumType.STRING)
	private String isDeleted;

	@Column(name="ZIPLIST")
	private String zipList;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public IssuerServiceArea getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(IssuerServiceArea serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPartialCounty() {
		return partialCounty;
	}

	public void setPartialCounty(String partialCounty) {
		this.partialCounty = partialCounty;
	}

	public String getPartialCountyJustification() {
		return partialCountyJustification;
	}

	public void setPartialCountyJustification(String partialCountyJustification) {
		this.partialCountyJustification = partialCountyJustification;
	}

	public List<ServiceArea> getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(List<ServiceArea> serviceArea) {
		this.serviceArea = serviceArea;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getZipList() {
		return zipList;
	}

	public void setZipList(String zipList) {
		this.zipList = zipList;
	}
}
