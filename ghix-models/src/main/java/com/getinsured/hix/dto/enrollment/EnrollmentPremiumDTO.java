/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO for the Enrollment Premium table
 * @author negi_s
 * @since 06/20/2016
 */
public class EnrollmentPremiumDTO implements Serializable{
	
	private Integer month;
	private Integer year;
	private Float grossPremiumAmount;
	private Float aptcAmount;
	private BigDecimal stateSubsidyAmount;
	private Float netPremiumAmount;
	private Float slcspPremiumAmount;
	private String slcspPlanid;
	private boolean isFinancial;
	private boolean isAptcEligible;
	private boolean isStateSubsidyEligible;
	private Float maxAptc;
	private BigDecimal maxStateSubsidy;
	private Float actualAptcAmount;
	private BigDecimal actualStateSubsidyAmount;
	private BigDecimal minimumPremium;
	
	public Float getActualAptcAmount() {
		return actualAptcAmount;
	}

	public void setActualAptcAmount(Float actualAptcAmount) {
		this.actualAptcAmount = actualAptcAmount;
	}

	@Override
	public String toString() {
		return "EnrollmentPremiumDTO [month=" + month + ", year=" + year + ", grossPremiumAmount=" + grossPremiumAmount
				+ ", aptcAmount=" + aptcAmount + ", netPremiumAmount=" + netPremiumAmount + ", slcspPremiumAmount="
				+ slcspPremiumAmount + ", slcspPlanid=" + slcspPlanid + ", isFinancial=" + isFinancial + ", maxAptc="
				+ maxAptc + ", actualAptcAmount=" + actualAptcAmount + "]";
	}
	
	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}
	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}
	/**
	 * @return the grossPremiumAmount
	 */
	public Float getGrossPremiumAmount() {
		return grossPremiumAmount;
	}
	/**
	 * @param grossPremiumAmount the grossPremiumAmount to set
	 */
	public void setGrossPremiumAmount(Float grossPremiumAmount) {
		this.grossPremiumAmount = grossPremiumAmount;
	}
	/**
	 * @return the aptcAmount
	 */
	public Float getAptcAmount() {
		return aptcAmount;
	}
	/**
	 * @param aptcAmount the aptcAmount to set
	 */
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}
	/**
	 * @return the netPremiumAmount
	 */
	public Float getNetPremiumAmount() {
		return netPremiumAmount;
	}
	/**
	 * @param netPremiumAmount the netPremiumAmount to set
	 */
	public void setNetPremiumAmount(Float netPremiumAmount) {
		this.netPremiumAmount = netPremiumAmount;
	}
	/**
	 * @return the slcspPremiumAmount
	 */
	public Float getSlcspPremiumAmount() {
		return slcspPremiumAmount;
	}
	/**
	 * @param slcspPremiumAmount the slcspPremiumAmount to set
	 */
	public void setSlcspPremiumAmount(Float slcspPremiumAmount) {
		this.slcspPremiumAmount = slcspPremiumAmount;
	}

	public String getSlcspPlanid() {
		return slcspPlanid;
	}

	public void setSlcspPlanid(String slcspPlanid) {
		this.slcspPlanid = slcspPlanid;
	}

	public boolean isFinancial() {
		return isFinancial;
	}

	public void setFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}

	public boolean isAptcEligible() {
		return isAptcEligible;
	}

	public void setAptcEligible(boolean aptcEligible) {
		isAptcEligible = aptcEligible;
	}

	public boolean isStateSubsidyEligible() {
		return isStateSubsidyEligible;
	}

	public void setStateSubsidyEligible(boolean stateSubsidyEligible) {
		isStateSubsidyEligible = stateSubsidyEligible;
	}

	public Float getMaxAptc() {
		return maxAptc;
	}

	public void setMaxAptc(Float maxAptc) {
		this.maxAptc = maxAptc;
	}

	public BigDecimal getStateSubsidyAmount() {
		return stateSubsidyAmount;
	}

	public void setStateSubsidyAmount(BigDecimal stateSubsidyAmount) {
		this.stateSubsidyAmount = stateSubsidyAmount;
	}

	public BigDecimal getMaxStateSubsidy() {
		return maxStateSubsidy;
	}

	public void setMaxStateSubsidy(BigDecimal maxStateSubsidy) {
		this.maxStateSubsidy = maxStateSubsidy;
	}

	public BigDecimal getActualStateSubsidyAmount() {
		return actualStateSubsidyAmount;
	}

	public void setActualStateSubsidyAmount(BigDecimal actualStateSubsidyAmount) {
		this.actualStateSubsidyAmount = actualStateSubsidyAmount;
	}

	public BigDecimal getMinimumPremium() {
		return minimumPremium;
	}

	public void setMinimumPremium(BigDecimal minimumPremium) {
		this.minimumPremium = minimumPremium;
	}
}
