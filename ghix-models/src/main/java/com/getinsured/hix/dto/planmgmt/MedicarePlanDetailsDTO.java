/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author Krishna Gajulapalli
 *
 */
public class MedicarePlanDetailsDTO extends GHIXResponse implements Serializable{

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private List<MedicarePlan> planDetails;	
	
	/**
	 * @return the planDetails
	 */
	public List<MedicarePlan> getPlanDetails() {
		return planDetails;
	}

	/**
	 * @param planDetails the planDetails to set
	 */
	public void setPlanDetails(List<MedicarePlan> planDetails) {
		this.planDetails = planDetails;
	}

	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse(){		
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON. 
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse(){
		String transformedResponse = null;
		
		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
		    public HierarchicalStreamWriter createWriter(Writer writer) {
		        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		    }
		});
		
		Validate.notNull(jsoner, "XStream instance improperly confugured.");
		
		transformedResponse = jsoner.toXML(this);
		
		return transformedResponse;
	}	*/
	
}
