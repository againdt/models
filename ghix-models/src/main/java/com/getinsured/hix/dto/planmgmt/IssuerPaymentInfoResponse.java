package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

/**
 * This DTO is used to pass data from ISSUER_PAYMENT_INFO table to the calling method. Only payment related fields are
 * used in this DTO.
 * 
 * @author vardekar_s
 *
 */
public class IssuerPaymentInfoResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String hiosId;

	private String keyStoreFileLocation;
	
	private String privateKeyName;
	
	private String password;
	
	private String passwordSecuredKey;
	
	private String securityDnsName;
	
	private String securityAddress;
	
	private String securityKeyInfo;
	
	private String issuerAuthURL;
	
	private String securityCertName;

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getKeyStoreFileLocation() {
		return keyStoreFileLocation;
	}

	public void setKeyStoreFileLocation(String keyStoreFileLocation) {
		this.keyStoreFileLocation = keyStoreFileLocation;
	}

	public String getPrivateKeyName() {
		return privateKeyName;
	}

	public void setPrivateKeyName(String privateKeyName) {
		this.privateKeyName = privateKeyName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordSecuredKey() {
		return passwordSecuredKey;
	}

	public void setPasswordSecuredKey(String passwordSecuredKey) {
		this.passwordSecuredKey = passwordSecuredKey;
	}

	public String getSecurityDnsName() {
		return securityDnsName;
	}

	public void setSecurityDnsName(String securityDnsName) {
		this.securityDnsName = securityDnsName;
	}

	public String getSecurityAddress() {
		return securityAddress;
	}

	public void setSecurityAddress(String securityAddress) {
		this.securityAddress = securityAddress;
	}

	public String getSecurityKeyInfo() {
		return securityKeyInfo;
	}

	public void setSecurityKeyInfo(String securityKeyInfo) {
		this.securityKeyInfo = securityKeyInfo;
	}

	public String getIssuerAuthURL() {
		return issuerAuthURL;
	}

	public void setIssuerAuthURL(String issuerAuthURL) {
		this.issuerAuthURL = issuerAuthURL;
	}

	public String getSecurityCertName() {
		return securityCertName;
	}

	public void setSecurityCertName(String securityCertName) {
		this.securityCertName = securityCertName;
	}
	
}
