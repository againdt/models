package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for table EE_ASSISTER_LANGUAGES, to save languages
 * associated with assisters.
 * 
 */
@Entity
@Table(name = "EE_ASSISTER_LANGUAGES")
public class AssisterLanguages implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_ASSISTER_LANGUAGES_SEQ")
	@SequenceGenerator(name = "EE_ASSISTER_LANGUAGES_SEQ", sequenceName = "EE_ASSISTER_LANGUAGES_SEQ", allocationSize = 1)
	private int id;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToOne
	@JoinColumn(name = "EE_ASSISTER_ID")
	private Assister assister;

	@NotEmpty(message="label.validateSpokenLanguage")
	@Column(name = "SPOKEN_LANGUAGES")
	private String spokenLanguages;

	@NotEmpty(message="label.validateWrittenLanguage")
	@Column(name = "WRITTEN_LANGUAGES")
	private String writtenLanguages;

	public AssisterLanguages() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Assister getAssister() {
		return assister;
	}

	public void setAssister(Assister assister) {
		this.assister = assister;
	}

	public String getSpokenLanguages() {
		return spokenLanguages;
	}

	public void setSpokenLanguages(String spokenLanguages) {
		this.spokenLanguages = spokenLanguages;
	}

	public String getWrittenLanguages() {
		return writtenLanguages;
	}

	public void setWrittenLanguages(String writtenLanguages) {
		this.writtenLanguages = writtenLanguages;
	}

	@Override
	public String toString() {
		return "Assister Languages Details: languageId = " + id + ", assister details = [" + assister + "]";
	}

}