
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IRDBProcessedStatusIndType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IRDBProcessedStatusIndType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="N"/>
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="E"/>
 *     &lt;enumeration value="D"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IRDBProcessedStatusIndType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
@XmlEnum
public enum IRDBProcessedStatusIndType {

    N,
    A,
    E,
    D;

    public String value() {
        return name();
    }

    public static IRDBProcessedStatusIndType fromValue(String v) {
        return valueOf(v);
    }

}
