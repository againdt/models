package com.getinsured.hix.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the QHP_ACCOUNT_RECEIVEABLE oracle view.
 * @author sharma_k
 */
@Entity
@Table(name = "qhp_account_receiveable")
public class QhpAccountReceiveable 
{
	/*
	 * Added rownnum as id to play the role of primary key.
	 */
	@Id
	@Column(name = "row_number")
	private int id;

	@Column(name = "employer_id")
	private int employerId;
	
	@Column(name = "employer_name")
	private String employerName;
	
	@Column(name = "plan_type")
	private String planType;
	
	@Column(name = "net_amount")
	private float netAmount;
	
	@Column(name="payment_received")
	private float paymentReceived;
	
	@Column(name = "pending_amount")
	private float pendingAmount;
	
	public int getId() {
		return id;
	}

	public int getEmployerId() {
		return employerId;
	}

	public String getEmployerName() {
		return employerName;
	}

	public String getPlanType() {
		return planType;
	}

	public float getNetAmount() {
		return netAmount;
	}

	public float getPaymentReceived() {
		return paymentReceived;
	}

	public float getPendingAmount() {
		return pendingAmount;
	}

}
