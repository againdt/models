package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class EnrollmentApplicationIdDTO implements Serializable{
	
	private Integer enrollmentId;
	private Long ssapApplicationId;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	@Override
	public String toString() {
		return "EnrollmentApplicationIdDTO [enrollmentId=" + enrollmentId
				+ ", ssapApplicationId=" + ssapApplicationId + "]";
	}
}
