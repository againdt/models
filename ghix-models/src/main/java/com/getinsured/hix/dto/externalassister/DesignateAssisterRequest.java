package com.getinsured.hix.dto.externalassister;

import java.io.Serializable;

public class DesignateAssisterRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public enum ASSISTER_TYPE{BROKER, NAVIGATOR, CAC};

	private int householdId;

	private String assisterName;
	
	private ASSISTER_TYPE assisterType;
	
	private String federalTaxId;
	
	private String externalAssisterId;
	
	private String agentNpn;
	
	private String externalUserId;

	public int getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}

	public String getAssisterName() {
		return assisterName;
	}

	public void setAssisterName(String assisterName) {
		this.assisterName = assisterName;
	}

	public String getAssisterType() {
		return assisterType.toString();
	}

	public void setAssisterType(ASSISTER_TYPE assisterType) {
		this.assisterType = assisterType;
	}

	public String getFederalTaxId() {
		return federalTaxId;
	}

	public void setFederalTaxId(String federalTaxId) {
		this.federalTaxId = federalTaxId;
	}

	public String getExternalAssisterId() {
		return externalAssisterId;
	}

	public void setExternalAssisterId(String externalAssisterId) {
		this.externalAssisterId = externalAssisterId;
	}

	public String getAgentNpn() {
		return agentNpn;
	}

	public void setAgentNpn(String agentNpn) {
		this.agentNpn = agentNpn;
	}

	public String getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	
}

