package com.getinsured.hix.dto.estimator.mini;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * Create DTO to send pre-65 Non-Retiree data to AgentExpress.
 */
public class NonRetireeInfoDTO {

	/* Indicates the Affiliate ID which is unique for an affiliate */
	private long affiliateId;
	/* Indicates the flow ID which is unique for an affiliate */
	private Integer flowId;
	/* Indicates Applicant’s First Name */
	private String firstName;
	/* Indicates Applicant’s Last Name */
	private String lastName;
	/* Indicates Applicant’s phone number */
	private String phone;
	/* Indicates Applicant’s email ID */
	private String email;
	/* Indicates the ZIP code for the household. Standard US zipcode (only numbers are allowed in the string and it has to be a 5-digit string) */
	private String zipCode;
	/* Indicates the county code for the household. It is a combination of stateFIPS and countyFIPS */
	private String countyCode;
	/* Annual household income */
	private Double hhIncome;
	/* Number of family members seeking insurance */
	private Integer familySize;
	/* Qualifying Life Event. Possible values are as follows. Default, if nothing is specified: LOST_COVERAGE */
	private Long eventId;
	/* List of Household Members */
	private List<NonRetireeMemberDTO> memberList;

	public NonRetireeInfoDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public long getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public Double getHhIncome() {
		return hhIncome;
	}

	public void setHhIncome(Double hhIncome) {
		this.hhIncome = hhIncome;
	}

	public Integer getFamilySize() {
		return familySize;
	}

	public void setFamilySize(Integer familySize) {
		this.familySize = familySize;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public List<NonRetireeMemberDTO> getMemberList() {
		return memberList;
	}

	/**
	 * set MemberDTO in List
	 */
	public void setMemberInList(NonRetireeMemberDTO memberDTO) {

		if (null == this.memberList) {
			this.memberList = new ArrayList<NonRetireeMemberDTO>();
		}
		memberList.add(memberDTO);
	}
}
