
package com.getinsured.iex.ssap.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum SsapDocumentCategory {
	AMI_ALA_NATIVE_STATUS("American Indian/Alaska Native Status", "Verify Native American", getDTListForAmericanAlaska()), //required in ticketworkflow
	DEATH("Death", "Verify Death", getDTListForDeath()),
	MEC("Minimum Essential Coverage", "Verify MEC", getDTListForMEC()), //required in ticketworkflow
	NON_ESI_MEC("Non-ESI Minimum Essential Coverage", "Verify Non-ESI MEC", getDTListForNonESIMec()),
	LEGAL_PRESENCE("Legal Presence", "Verify Lawful Presence", getDTListForLegalPresence()),
	INCARCERATION_STATUS("Incarceration Status", "Verify Incarceration", getDTListForIncarceration()),
	INCOME("Income", "Verify Income", getDTListForIncome()),
	SSN("Social Security Number", "Verify SSN", getDTListForSSN()),
	RESIDENCY("Residency", "Verify Residency", getDTListForResidency()),//required in ticketworkflow
	CITIZENSHIP("Citizenship", "Verify Citizenship", getDTListForCitizenship()),
	VLP("Legal Presence", "Verify Lawful Presence", getDTListForCitizenship());

	private final String description;
	private final String tkmCategory;
	private final List<String> documentTypes;

	SsapDocumentCategory(String description, String tkmCategory, List<String> documentTypes) {

		this.description = description;
		this.tkmCategory = tkmCategory;
		this.documentTypes = documentTypes;
	}

	private static List<String> getDTListForAmericanAlaska() {
		return Arrays.asList(
				"Authentic document from a federally recognized tribe",
				"Certificate of Degree of Indian Blood",
				"Certificate of Indian status card",
				"Document issued by Bureau of Indian Affairs (BIA)",
				"Document issued by Indian Health Service (IHS)",
				"I-872 American Indian Card",
				"Tribal Enrollment/Membership card from a federally recognized tribe",
				"U.S. American Indian/Alaska Native tribal enrollment or shareholder documentation",
				"Letter from the Marketplace granting a tribal exemption",
				"Document on Tribal Letterhead",
				"Tribal Census Document",
				"Tribal Enrollment Card"
		);
	}

	private static List<String> getDTListForDeath() {
		List<String> list = new ArrayList<String>();
		list.add("Death Certificate");
		list.add("Funeral Home Statement");
		list.add("Newspaper Clipping/Collateral Contact");
		return list;

	}

	private static List<String> getDTListForMEC() {
		List<String> list = new ArrayList<String>();
		list.add("Health Insurance Cards");
		list.add("Insurance Company Letter");
		return list;

	}

	private static List<String> getDTListForNonESIMec() {
		List<String> list = new ArrayList<>();
		list.add("Health insurance letter, including coverage termination date");
		list.add("Letter from Veterans Affairs and/or Veterans Administration");
		list.add("Letter from Peace Corps");
		list.add("Letter or statement of Medicare benefits");
		list.add("Letter or statement of Medicaid or Children's Health Insurance Program (CHIP) benefits");
		list.add("Statement of health benefits");
		return list;
	}

	private static List<String> getDTListForLegalPresence() {
		List<String> list = new ArrayList<String>();
		list.add("DS2019 (Certificate of Eligibility for Exchange Visitor (J-1) Status)");
		list.add("I-181 (for American Indians from Canada)");
		list.add("I-20 (Certificate of Eligibility for Nonimmigrant (F-1) Student Status)");
		list.add("I-327 Permit to Reenter the United States");
		list.add("I-551 Alien Registration Receipt Card/Resident Alien Card");
		list.add("I-571 (Refugee Travel Document)");
		list.add("I-766 (Employment Authorization Card)");
		list.add("I-94 Arrival-Departure Record");
		list.add("Letter or other tribal document certifying at least 50% American Indian blood");
		list.add("Naturalization Certificate");
		list.add("Notice of Action (I-797)");
		list.add("Order from Immigration Judge");
		list.add("Passport stamped 'Processed for I-551'");
		list.add("Passport with an Immigrant Visa Stamp");
		list.add("Statement from Immigration (USCIS)");
		return list;

	}

	private static List<String> getDTListForIncome() {
		return Arrays.asList(
				"-- Documents that confirm your yearly income --", // -- TEXT -- will be ignored and serves as category separator/help text.
				"1040 federal or state tax return",
				"Pay stub",
				"Social Security Administration Statements",
				"Unemployment Benefits Letter",
				"Wages and tax statement (W2 and/or 1099, including 1099 MISC, 1099G, 1099R, 1099SSA, 1099DIV, 1099SS, 1099INT)",
				"-- Self-employment income --", // -- TEXT -- will be ignored and serves as category separator/help text.
				"1040 SE with Schedule C, F, SE (for self-employment income)",
				"1065 Schedule K1 with Schedule E",
				"Bookkeeping records",
				"Most recent quarterly or year-to-date profit and loss statement",
				"Receipts for all allowable expenses",
				"Signed time sheets and receipt of payroll, if you have employees",
				"Tax return",
				"-- Unearned income --", // -- TEXT -- will be ignored and serves as category separator/help text.
				"Annuity statement",
				"Interests and dividends income statement",
				"Letter, deposit, or other proof of deferred compensation payments",
				"Letter, deposit, or other proof of travel/business reimbursement pay",
				"Loan statement showing loan proceeds",
				"Pay stub indicating sick pay",
				"Pay stub indicating substitute/assistant pay",
				"Pay stub indicating vacation pay",
				"Prizes, settlements, and awards, including court-ordered awards letter",
				"Proof of gifts and contributions",
				"Proof of inheritances and cash or property",
				"Proof of strike pay and other benefits from unions",
				"Proof of bonus/incentive payments",
				"Proof of severance pay",
				"Proof of residuals",
				"Royalty income statement or 1099-MISC",
				"Sales receipts or other proof of money received from the sale, exchange, or replacement of things you own",
				"Statement of pension distribution from any government or private source",
				"Worker's compensation letter"
		);
//    List<String> list = new ArrayList<String>();
//    list.add("Award Letter");
//    list.add("Employer Statement");
//    list.add("Income Tax Return");
//    list.add("Letter or Document from Person/Agency Making Payment");
//    list.add("Pay Stubs"); // +
//    list.add("Personal Records");
//    list.add("Tax Records");
//    return list;

	}

	private static List<String> getDTListForSSN() {
		List<String> list = new ArrayList<String>();
		list.add("Social Security Card");
		return list;

	}

	private static List<String> getDTListForIncarceration() {
		List<String> list = new ArrayList<String>();
		list.add("Collateral Contact");
		list.add("Discharge Papers");
		return list;

	}

	private static List<String> getDTListForResidency() {
		List<String> list = new ArrayList<String>();
		list.add("Rent/Mortgage Receipt");
		list.add("Utility Bill");
		list.add("Valid Drivers License");
		list.add("Other ID with a Name and Address");

		return list;

	}

	private static List<String> getDTListForCitizenship() {
		List<String> list = new ArrayList<String>();
		list.add("Affidavit from U.S. Citizen");
		list.add("American Indian Card I-872");
		list.add("American Indian Tribal Census Records");
		list.add("BIA Tribal Census Record");
		list.add("Certificate of Naturalization");
		list.add("Certificate of U.S. Citizenship (N-560 or N-561)");
		list.add(" Proof of Civil Service Employment by the US Government prior to 1976");
		list.add("Evidence of Compliance with Child Citizen Act of 2000    ");
		list.add("Federal or State Census Records showing U.S. Birth  ");
		list.add("Final Adoption Decree Showing U.S. Birth");
		list.add("Hospital Birth Record");
		list.add("Hospital Souvenir Birth Certificate");
		list.add("Institutional Admission Papers");
		list.add("Insurance Record Showing U.S. Birth");
		list.add("Legal Birth Certificate");
		list.add("Northern Mariana Card I-873");
		list.add("Official Military Records showing U.S. Birth");
		list.add("Official Religious Record showing U.S. Birth");
		list.add("Other Birth Records");
		list.add("Power of Attorney Document");
		list.add("Qualifying School Record showing U.S. Birth");
		list.add("Religious Record");
		list.add("Report/Certificate of Birth abroad of U.S. Citizen (DS-1350, FS-240, or FS-545)   ");
		list.add("Statement signed by Physician/Midwife attending Birth  ");
		list.add("U.S. Citizen ID Card (I-197 or I-179)");
		list.add("U.S. Passport");
		list.add("Voter Registration Card");
		return list;

	}

	public List<String> getDocumentTypes() {
		return documentTypes;
	}

	public String getDescription() {
		return description;
	}

	public String getTkmCategory() {
		return tkmCategory;
	}
}
