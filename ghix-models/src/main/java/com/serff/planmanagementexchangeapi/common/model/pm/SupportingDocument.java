
package com.serff.planmanagementexchangeapi.common.model.pm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SupportingDocument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SupportingDocument">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userAdded" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}dateLastModified"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}scheduleItemStatus"/>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}scheduleItemStatusChangeDate"/>
 *         &lt;element name="attachments" type="{http://www.serff.com/planManagementExchangeApi/common/model/pm}Attachments"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "userAdded",
    "comment",
    "dateLastModified",
    "scheduleItemStatus",
    "scheduleItemStatusChangeDate",
    "attachments"
})
@XmlRootElement(name="supportingDocument")
public class SupportingDocument {

    @XmlElement(required = true)
    protected String name;
    protected boolean userAdded;
    @XmlElement(required = true, nillable = true)
    protected String comment;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateLastModified;
    @XmlElement(required = true, nillable = true)
    protected String scheduleItemStatus;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scheduleItemStatusChangeDate;
    @XmlElement(required = true)
    protected Attachments attachments;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the userAdded property.
     * 
     */
    public boolean isUserAdded() {
        return userAdded;
    }

    /**
     * Sets the value of the userAdded property.
     * 
     */
    public void setUserAdded(boolean value) {
        this.userAdded = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the dateLastModified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateLastModified() {
        return dateLastModified;
    }

    /**
     * Sets the value of the dateLastModified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateLastModified(XMLGregorianCalendar value) {
        this.dateLastModified = value;
    }

    /**
     * Gets the value of the scheduleItemStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleItemStatus() {
        return scheduleItemStatus;
    }

    /**
     * Sets the value of the scheduleItemStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleItemStatus(String value) {
        this.scheduleItemStatus = value;
    }

    /**
     * Gets the value of the scheduleItemStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScheduleItemStatusChangeDate() {
        return scheduleItemStatusChangeDate;
    }

    /**
     * Sets the value of the scheduleItemStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScheduleItemStatusChangeDate(XMLGregorianCalendar value) {
        this.scheduleItemStatusChangeDate = value;
    }

    /**
     * Gets the value of the attachments property.
     * 
     * @return
     *     possible object is
     *     {@link Attachments }
     *     
     */
    public Attachments getAttachments() {
        return attachments;
    }

    /**
     * Sets the value of the attachments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Attachments }
     *     
     */
    public void setAttachments(Attachments value) {
        this.attachments = value;
    }

}
