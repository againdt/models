package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * Model class for TENANT table.
 * 
 * @author Bhavin Parmar
 * @since 15 Oct, 2013
 */
@Entity
@Table(name = "PM_TENANT_PLAN")
@DynamicUpdate
@DynamicInsert
public class TenantPlan implements Serializable {

	private static final long serialVersionUID = 628093711142056868L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_TENANT_PLAN_SEQ")
	@SequenceGenerator(name = "PM_TENANT_PLAN_SEQ", sequenceName = "PM_TENANT_PLAN_SEQ", allocationSize = 1)
	private long id;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "TENANT_ID")
	private Tenant tenant;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
