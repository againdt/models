package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * The persistent class for the ACCOUNT_RECEIVEABLE oracle view.
 * @author sharma_k
 */

@Entity
@Table(name = "account_receiveable")
public class AccountReceiveable implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "employer_id")
	private int employerId;
	
	@Column(name = "employer_name")
	private String employerName;
	
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@Column(name = "statement_date")
	private Date statementDate;
	
	@Column(name = "total_amount_due")
	private float totalAmountDue;
	
	@Column(name = "paid_amount")
	private float paidAmount;
	
	@Column(name = "pending_amount")
	private float pendingAmount;
	
	@Column(name = "paid_status")
	private String paidStatus;
	
	public int getId()
	{
		return id;
	}
	
	public int getEmployerId()
	{
		return employerId;
	}
	
	public String getEmployerName()
	{
		return employerName;
	}
	
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}
	
	public Date getStatementDate()
	{
		return statementDate;
	}
	
	public float getTotalAmountDue()
	{
		return totalAmountDue;
	}
	
	public float getPaidAmount()
	{
		return paidAmount;
	}
	
	public float getPendingAmount()
	{
		return pendingAmount;
	}

	public String getPaidStatus() {
		return paidStatus;
	}
}
