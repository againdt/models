package com.getinsured.hix.consumer.history;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties({"date"})
public abstract class ConsumerHistoryBase  {
	Date date;

	/**
	 * @param date
	 */
	public ConsumerHistoryBase(Date date) {
		this.date = date;
	}

	public ConsumerHistoryBase() {
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
