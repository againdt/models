ALTER TABLE cmr_household ADD (
  CONSTRAINT fk_cmr_household_elig_lead_id FOREIGN KEY (elig_lead_id) REFERENCES elig_lead ("ID"),
  CONSTRAINT fk_cmr_household_user_id FOREIGN KEY (user_id) REFERENCES "USERS" ("ID")
);

ALTER TABLE cmr_member ADD (
  CONSTRAINT fk_cmr_member_household_id FOREIGN KEY (cmr_household_id) REFERENCES cmr_household (ID),
  CONSTRAINT fk_cmr_member_location_id FOREIGN KEY (location_id) REFERENCES locations (ID)
);

ALTER TABLE  employer_eligibility_status ADD (
  CONSTRAINT fk_ees_created_by FOREIGN KEY (created_by) REFERENCES USERS (ID),
  CONSTRAINT fk_ees_employer_id FOREIGN KEY (employer_id) REFERENCES employers (ID)
);

ALTER TABLE enrl_group_installation ADD (
  CONSTRAINT fk_enrollment_action_lkp FOREIGN KEY (enrollment_action_lkp)
   REFERENCES lookup_value (lookup_value_id)
);

ALTER TABLE ENROLLMENT_AUD add 
 (employee_id NUMBER);

COMMENT ON COLUMN enrollment_aud.employee_id IS 'Employee id';

ALTER TABLE enrl_group_plan  add (
  CONSTRAINT fk_plan_installation FOREIGN KEY ("GROUP_ID") REFERENCES enrl_group_installation ("ID")
);

ALTER TABLE external_individual drop (casestatus);

ALTER TABLE gi_monitor ADD(
  CONSTRAINT fk_gim_error_code FOREIGN KEY (error_code_id) REFERENCES gi_error_code (code_id),
  CONSTRAINT fk_gim_user_id FOREIGN KEY (user_id) REFERENCES "USERS" ("ID")
);

ALTER TABLE lookup_type  ADD ( CONSTRAINT UK_LOOKUP_TYPE UNIQUE ("NAME"));

ALTER TABLE plan_health  ADD (
  CONSTRAINT fk_plan_sbc_scenario FOREIGN KEY (plan_sbc_scenario_id)
 REFERENCES plan_sbc_scenario ("ID")
);

ALTER TABLE  serff_plan_mgmt_batch ADD (
  CONSTRAINT fk_serff_plan_mgmt_id FOREIGN KEY (serff_plan_mgmt_id) REFERENCES serff_plan_mgmt (serff_req_id)
);

COMMENT ON COLUMN account_payable."ID" IS 'Rownum associated with view for unique records';

COMMENT ON COLUMN account_payable.issuer_id IS 'Issuer unique id';

COMMENT ON COLUMN account_payable.issuer_name IS 'Name of Issuer';

COMMENT ON COLUMN account_payable.invoice_number IS 'Generated invoice number';

COMMENT ON COLUMN account_payable.statement_date IS 'Date on which invoice is generated';

COMMENT ON COLUMN account_payable.total_amount_due IS 'Total amount due which need to be paid to the issuer';

COMMENT ON COLUMN account_payable.paid_amount IS 'Total paid amount to the issuer';

COMMENT ON COLUMN account_payable.balance_amount IS 'Pending amount to be paid to the issuer, value is difference between TOTAL_AMOUNT_DUE and PAID_AMOUNT';

COMMENT ON COLUMN account_receiveable."ID" IS 'Rownum associated with view for unique records';

COMMENT ON COLUMN account_receiveable.employer_id IS 'Employer unique id';

COMMENT ON COLUMN account_receiveable.employer_name IS 'Name of employer';

COMMENT ON COLUMN account_receiveable.invoice_number IS 'Generated invoice number';

COMMENT ON COLUMN account_receiveable.statement_date IS 'Date on which invoice is created';

COMMENT ON COLUMN account_receiveable.total_amount_due IS 'Total premium amount which is due';

COMMENT ON COLUMN account_receiveable.paid_amount IS 'Amount paid by the employer';

COMMENT ON COLUMN account_receiveable.pending_amount IS 'Pending amount is the difference amount of TOTAL_AMOUNT_DUE and PAID_AMOUNT';


COMMENT ON COLUMN qhp_account_receiveable."ROW_NUMBER" IS 'Rownum associated with view for unique records';

COMMENT ON COLUMN qhp_account_receiveable.employer_id IS 'Employer unique id';

COMMENT ON COLUMN qhp_account_receiveable.employer_name IS 'Name of Employer';

COMMENT ON COLUMN qhp_account_receiveable.plan_type IS 'Plan opted for';

COMMENT ON COLUMN qhp_account_receiveable.net_amount IS 'Net amount to be paid to the issuer';

COMMENT ON COLUMN qhp_account_receiveable.payment_received IS 'Payment received from employer';

COMMENT ON COLUMN qhp_account_receiveable.pending_amount IS 'Remaining amount to be paid by Employer, value is difference between NET_AMOUNT and PAYMENT_RECEIVED';


