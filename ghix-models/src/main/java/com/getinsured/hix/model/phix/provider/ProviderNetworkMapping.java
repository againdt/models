package com.getinsured.hix.model.phix.provider;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="provider_network_mapping")
public class ProviderNetworkMapping  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private ProviderMapping providerMapping;
	private ProviderProduct providerProduct;

	@Id 
	@SequenceGenerator(name="PROVIDER_NETWORK_MAPPING_SEQ", sequenceName="PROVIDER_NETWORK_MAPPING_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="PROVIDER_NETWORK_MAPPING_SEQ")    
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NETWORK_MAPPING_ID")
	public ProviderMapping getProviderMapping() {
		return this.providerMapping;
	}

	public void setProviderMapping(ProviderMapping providerMapping) {
		this.providerMapping = providerMapping;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROVIDER_PRODUCT_ID")
	public ProviderProduct getProviderProduct() {
		return this.providerProduct;
	}

	public void setProviderProduct(ProviderProduct providerProduct) {
		this.providerProduct = providerProduct;
	}

}


