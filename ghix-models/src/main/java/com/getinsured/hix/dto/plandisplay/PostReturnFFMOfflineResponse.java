/**
 * Created: Oct 16, 2014
 */
package com.getinsured.hix.dto.plandisplay;

import java.io.Serializable;


/**
 * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
 *
 */
public class PostReturnFFMOfflineResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String response;

	private String status;

	private String errMsg;

	private Long ssapApplicationId;


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}




}
