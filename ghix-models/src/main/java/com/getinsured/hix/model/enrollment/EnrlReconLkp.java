package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.platform.util.SortableEntity;

@Entity
@Table(name="ENRL_RECON_LKP")
public class EnrlReconLkp  implements Serializable, SortableEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrl_recon_lkp_seq")
	@SequenceGenerator(name = "enrl_recon_lkp_seq", sequenceName = "ENRL_RECON_LKP_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="CATEGORY")
	private String category;
	
	@Column(name="CODE")
	private String code;
	
	@Column(name="LABEL")
	private String label;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="ASSIGNEE")
	private String assignee;
	
	@Column(name="SYSTEM_AUTOFIX")
	private String systemAutofix;
	
	@Column(name="FIELD_NAME")
	private String fieldName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getSystemAutofix() {
		return systemAutofix;
	}

	public void setSystemAutofix(String systemAutofix) {
		this.systemAutofix = systemAutofix;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public Set<String> getSortableColumnNamesSet() {
		Set<String> columnNames = new HashSet<String>();
        columnNames.add("code");
        columnNames.add("label");
        columnNames.add("category");
        return columnNames;
	}
}
