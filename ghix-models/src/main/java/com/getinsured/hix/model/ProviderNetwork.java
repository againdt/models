package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the provider_network database table.
 * 
 */

@Entity
@Table(name="provider_network")
public class ProviderNetwork implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProviderNetwork_Seq")
	@SequenceGenerator(name = "ProviderNetwork_Seq", sequenceName = "provider_network_seq", allocationSize = 1)
	@Column(name="id")
	private int id;
	
	@ManyToOne
    @JoinColumn(name="provider_id")
	private Provider provider;
	
	@ManyToOne
    @JoinColumn(name="network_id")
	private Network network;
	
	@Column(name="network_tier")
	private String networkTier;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public String getNetworkTier() {
		return networkTier;
	}

	public void setNetworkTier(String networkTier) {
		this.networkTier = networkTier;
	}
	
}


