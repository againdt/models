package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Set;

import com.getinsured.hix.model.GHIXResponse;

public class EnrollmentMemberDTO  extends GHIXResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer enrollmentId;
	private Long ssapApplicationId;
	private Set<String> memberIdSet;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public Set<String> getMemberIdSet() {
		return memberIdSet;
	}
	public void setMemberIdSet(Set<String> memberIdSet) {
		this.memberIdSet = memberIdSet;
	}


}
