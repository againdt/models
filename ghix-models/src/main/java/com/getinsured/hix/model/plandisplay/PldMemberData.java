package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.GHIXResponse;

public class PldMemberData extends GHIXResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5588364149655898305L;
	private Map<String, String> memberInfo;
	private List<Map<String, String>> raceInfo;
	private List<Map<String, String>> relationshipInfo;
	private List<String> enrollmentIds;
	private String appEventReason;
	public Map<String, String> getMemberInfo() {
		return memberInfo;
	}
	public void setMemberInfo(Map<String, String> memberInfo) {
		this.memberInfo = memberInfo;
	}
	public List<Map<String, String>> getRaceInfo() {
		return raceInfo;
	}
	public void setRaceInfo(List<Map<String, String>> raceInfo) {
		this.raceInfo = raceInfo;
	}
	public List<Map<String, String>> getRelationshipInfo() {
		return relationshipInfo;
	}
	public void setRelationshipInfo(List<Map<String, String>> relationshipInfo) {
		this.relationshipInfo = relationshipInfo;
	}
	public List<String> getEnrollmentIds() {
		return enrollmentIds;
	}
	public void setEnrollmentIds(List<String> enrollmentIds) {
		this.enrollmentIds = enrollmentIds;
	}
	public String getAppEventReason() {
		return appEventReason;
	}
	public void setAppEventReason(String appEventReason) {
		this.appEventReason = appEventReason;
	}
	
	
}
