package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name="ENRL_EXTERNAL_ENROLLMENT")
public class ExternalEnrollment implements Serializable
{
	public enum YorNFlagIndicator{
		Y ,N;
	}
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_EXTERNAL_ENROLLMENT_SEQ")
	@SequenceGenerator(name = "ENRL_EXTERNAL_ENROLLMENT_SEQ", sequenceName = "ENRL_EXTERNAL_ENROLLMENT_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name = "APPLICANT_EXTERNAL_ID")
	private String applicantExternalId;
	
	@Column(name = "APPLICANT_FIRST_NAME")
	private String applicantFirstName;	
	
	@Column(name = "APPLICANT_LAST_NAME")
	private String applicantLastName;
	
	@Column(name ="APPLICANT_DOB")
	private Date applicantDob;
	
	
	@Column(name = "AGENT_NPN")
	private String agentNpn;
	
	@Column(name = "HEALTH_PLAN_ID")
	private String healthPlanId;

	@Column(name = "DENTAL_PLAN_ID")
	private String dentalPlanId;

	
	@Column(name = "HEALTH_EXCHANGE_POLICY_ID")
	private String healthExchangeAssignedPolicyId;

	@Column(name = "DENTAL_EXCHANGE_POLICY_ID")
	private String dentalExchangeAssignedPolicyId;
	
	
	@Column(name = "TOBACCO_INDICATOR")
	private String tobaccoIndicator;
	
	@Column(name = "APPLICANT_MATCHED")
	private String applicantMatched;
	
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;
	
	@Column(name = "FILE_ID")
	private Integer fileId;
	
	@Column(name = "application_external_id")
 	private String applicationExternalId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApplicantExternalId() {
		return applicantExternalId;
	}

	public void setApplicantExternalId(String applicantExternalId) {
		this.applicantExternalId = returnNullIfEmpty(applicantExternalId);
	}

	public String getApplicantFirstName() {
		return applicantFirstName;
	}

	public void setApplicantFirstName(String applicantFirstName) {
		this.applicantFirstName = returnNullIfEmpty(applicantFirstName);
	}

	public String getApplicantLastName() {
		return applicantLastName;
	}

	public void setApplicantLastName(String applicantLastName) {
		this.applicantLastName = returnNullIfEmpty(applicantLastName);
	}

	public Date getApplicantDob() {
		return applicantDob;
	}

	public void setApplicantDob(Date applicantDob) {
		this.applicantDob = applicantDob;
	}

	public String getAgentNpn() {
		return agentNpn;
	}

	public void setAgentNpn(String agentNpn) {
		this.agentNpn = returnNullIfEmpty(agentNpn);
	}

	public String getHealthPlanId() {
		return healthPlanId;
	}

	public void setHealthPlanId(String healthPlanId) {
		this.healthPlanId = returnNullIfEmpty(healthPlanId);
	}

	public String getDentalPlanId() {
		return dentalPlanId;
	}

	public void setDentalPlanId(String dentalPlanId) {
		this.dentalPlanId = returnNullIfEmpty(dentalPlanId);
	}

	public String getHealthExchangeAssignedPolicyId() {
		return healthExchangeAssignedPolicyId;
	}

	public void setHealthExchangeAssignedPolicyId(String healthExchangeAssignedPolicyId) {
		this.healthExchangeAssignedPolicyId = returnNullIfEmpty(healthExchangeAssignedPolicyId);
	}

	public String getDentalExchangeAssignedPolicyId() {
		return dentalExchangeAssignedPolicyId;
	}

	public void setDentalExchangeAssignedPolicyId(String dentalExchangeAssignedPolicyId) {
		this.dentalExchangeAssignedPolicyId = returnNullIfEmpty(dentalExchangeAssignedPolicyId);
	}

	public String getTobaccoIndicator() {
		return tobaccoIndicator;
	}

	public void setTobaccoIndicator(String tobaccoIndicator) {
		this.tobaccoIndicator = returnNullIfEmpty(tobaccoIndicator);
	}

	public String getApplicantMatched() {
		return applicantMatched;
	}

	public void setApplicantMatched(String applicantMatched) {
		this.applicantMatched = applicantMatched;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	
	public String getApplicationExternalId() {
		return applicationExternalId;
	}

	public void setApplicationExternalId(String applicationExternalId) {
		this.applicationExternalId = returnNullIfEmpty(applicationExternalId);
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentNpn == null) ? 0 : agentNpn.hashCode());
		result = prime * result + ((applicantDob == null) ? 0 : applicantDob.hashCode());
		result = prime * result + ((applicantExternalId == null) ? 0 : applicantExternalId.hashCode());
		result = prime * result + ((applicantFirstName == null) ? 0 : applicantFirstName.hashCode());
		result = prime * result + ((applicantLastName == null) ? 0 : applicantLastName.hashCode());
		result = prime * result + ((applicantMatched == null) ? 0 : applicantMatched.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result
				+ ((dentalExchangeAssignedPolicyId == null) ? 0 : dentalExchangeAssignedPolicyId.hashCode());
		result = prime * result + ((dentalPlanId == null) ? 0 : dentalPlanId.hashCode());
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		result = prime * result
				+ ((healthExchangeAssignedPolicyId == null) ? 0 : healthExchangeAssignedPolicyId.hashCode());
		result = prime * result + ((healthPlanId == null) ? 0 : healthPlanId.hashCode());
		result = prime * result + id;
		result = prime * result + ((tobaccoIndicator == null) ? 0 : tobaccoIndicator.hashCode());
		result = prime * result + ((updatedOn == null) ? 0 : updatedOn.hashCode());
		result = prime * result + ((applicationExternalId == null) ? 0 : applicationExternalId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExternalEnrollment other = (ExternalEnrollment) obj;
		if (agentNpn == null) {
			if (other.agentNpn != null)
				return false;
		} else if (!agentNpn.equals(other.agentNpn))
			return false;
		if (applicantDob == null) {
			if (other.applicantDob != null)
				return false;
		} else if (!applicantDob.equals(other.applicantDob))
			return false;
		if (applicantExternalId == null) {
			if (other.applicantExternalId != null)
				return false;
		} else if (!applicantExternalId.equals(other.applicantExternalId))
			return false;
		if (applicantFirstName == null) {
			if (other.applicantFirstName != null)
				return false;
		} else if (!applicantFirstName.equals(other.applicantFirstName))
			return false;
		if (applicantLastName == null) {
			if (other.applicantLastName != null)
				return false;
		} else if (!applicantLastName.equals(other.applicantLastName))
			return false;
		if (applicantMatched == null) {
			if (other.applicantMatched != null)
				return false;
		} else if (!applicantMatched.equals(other.applicantMatched))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (dentalExchangeAssignedPolicyId == null) {
			if (other.dentalExchangeAssignedPolicyId != null)
				return false;
		} else if (!dentalExchangeAssignedPolicyId.equals(other.dentalExchangeAssignedPolicyId))
			return false;
		if (dentalPlanId == null) {
			if (other.dentalPlanId != null)
				return false;
		} else if (!dentalPlanId.equals(other.dentalPlanId))
			return false;
		if (fileId == null) {
			if (other.fileId != null)
				return false;
		} else if (!fileId.equals(other.fileId))
			return false;
		if (healthExchangeAssignedPolicyId == null) {
			if (other.healthExchangeAssignedPolicyId != null)
				return false;
		} else if (!healthExchangeAssignedPolicyId.equals(other.healthExchangeAssignedPolicyId))
			return false;
		if (healthPlanId == null) {
			if (other.healthPlanId != null)
				return false;
		} else if (!healthPlanId.equals(other.healthPlanId))
			return false;
		if (id != other.id)
			return false;
		if (tobaccoIndicator == null) {
			if (other.tobaccoIndicator != null)
				return false;
		} else if (!tobaccoIndicator.equals(other.tobaccoIndicator))
			return false;
		if (updatedOn == null) {
			if (other.updatedOn != null)
				return false;
		} else if (!updatedOn.equals(other.updatedOn))
			return false;
		if (applicationExternalId == null) {
			if (other.applicationExternalId != null)
				return false;
		} else if (!applicationExternalId.equals(other.applicationExternalId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExternalEnrollment [id=" + id + ", applicantExternalId=" + applicantExternalId + ", applicantFirstName="
				+ applicantFirstName + ", applicantLastName=" + applicantLastName + ", applicantDob=" + applicantDob
				+ ", agentNpn=" + agentNpn + ", healthPlanId=" + healthPlanId + ", dentalPlanId=" + dentalPlanId
				+ ", healthExchangeAssignedPolicyId=" + healthExchangeAssignedPolicyId
				+ ", dentalExchangeAssignedPolicyId=" + dentalExchangeAssignedPolicyId + ", tobaccoIndicator="
				+ tobaccoIndicator + ", applicantMatched=" + applicantMatched + ", createdOn=" + createdOn
				+ ", updatedOn=" + updatedOn + ", fileId=" + fileId + ", applicationExternalId="+applicationExternalId+"]";
	}
	
	public String toCsv(char separator, String stateCode) {
		String csv = "".intern();
		if("MN".equalsIgnoreCase(stateCode)) {
			csv = applicantExternalId + separator
					+ healthExchangeAssignedPolicyId +separator 
					+ healthPlanId +separator
					+ dentalExchangeAssignedPolicyId +separator 
					+ dentalPlanId +separator 
					+ tobaccoIndicator
					;
			
		}
		return  csv.replaceAll("null", "");
	}
	
	private String returnNullIfEmpty(String value){
		if(null != value && !value.trim().isEmpty()){
			return value.trim();
		}else{
			return null;
		}
	}
}
