/**
 *
 */
package com.getinsured.constant.phix;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Captures all EndPoints details for different GHIX artifacts.
 * Under GHIX Application we have 2 main categories:-
 * 		1. GHIX-WEB URL --> used by
 * 					a. GHIX-WEB to access its own resources like NoticeTypes etc.
 * 					b. To make reverse call to GHIX-WEB from any Service WARS; for example:- GHIX-PLANDISPLAY consumes GHIX-WEB REST CLIENT to get PLAN-MGMT data.
 *
 * 		2. Service WARS --> used by
 * 					a. GHIX-WEB and other modules to invoke REST service exposed by respective Service WARS.
 * 					   example:- GHIX-WEB and GHIX-SHOP consumes PLAN_DISPLAY services.
 * 								 GHIX-SHOP consumes GHIX-AHBX services.
 *
 *
 *
 * Overall structure is to have a serviceURL constant for a module and an inner class to have all services exposed.
 * 			example:- "ghixPlandisplaySvcURL" and an inner class PlandisplayEndpoints which will have all services exposed by PLAN-DISPLAY module
 *
 *
 * PLS NOTE:- configuration.properties will only have serviceURL constant for a module configured and "NO REST service URL mapping".
 * 			  Please add properties under #EndPoints details for different GHIX artifacts - Starts section in configuration.properties files.
 *
 *
 */

@Component
public final class GhixPHIXEndPoints {
	
	public static String ENROLLMENT_URL;
	public static String PLANDISPLAY_URL;
	public static String SHOP_URL;
	public static String ELIGIBILITY_URL;
	public static String GHIXWEB_SERVICE_URL;
	public static String GHIX_AHBX_URL;
	public static String TICKETMGMT_URL;
	public static String PLAN_MGMT_URL;
	public static String GHIX_EE_URL;
	public static String PRESCREEN_SVC_URL;
	public static String GHIX_BROKER_URL;
	public static String GHIX_SERFF_SERVICE_URL;

	public static String HUB_SERVICE_URL;
	public static String AFFILIATE_SERVICE_URL;
	public static String FINANCE_SERVICE_URL;
	public static String CONSUMER_SERVICE_URL;
	public static String APPSERVER_URL;
	
	public static String GHIX_SSAP_SVC_URL;
	
	public static String GHIX_SSAP_INTEGRATION_URL;
	
	public static String GHIX_ELIGIBILITY_SVC_URL;
	
	public static String GHIX_HUB_INTEGRATION_URL;
	
	public static String D2C_SERVICE_URL;
	public static String D2C_UHC_URL;
	public static String D2C_HUMANA_URL;
	public static String D2C_AETNA_URL;
	public static String D2C_HCSC_URL;

	public static String FEEDS_URL;
	public static String FEEDS_CONFIG_URL=FEEDS_URL+"/config";
	
	
	@Value("#{configProp['appServer']}")
	public void setAPPSERVER_URL(String aPPSERVER_URL) {
		APPSERVER_URL = aPPSERVER_URL;
	}
	

	@Value("#{configProp['ghixEnrollmentServiceURL']}")
	public void setENROLLMENT_URL(String eNROLLMENT_URL) {
		ENROLLMENT_URL = eNROLLMENT_URL;
	}

	@Value("#{configProp['ghixWebServiceUrl']}")
	public void setGhixWebServiceUrl(String ghixWebServiceUrl) {
		GHIXWEB_SERVICE_URL = ghixWebServiceUrl;
	}

	@Value("#{configProp['ghixPlandisplayServiceURL']}")
	public void setGhixPlandisplayServiceURL(String ghixPlandisplayServiceURL) {
		PLANDISPLAY_URL = ghixPlandisplayServiceURL;
	}

	@Value("#{configProp['ghixShopServiceURL']}")
	public void setGhixShopServiceURL(String ghixShopServiceURL) {
		SHOP_URL = ghixShopServiceURL;
	}

	@Value("#{configProp['ghixEligibilityServiceURL']}")
	public void setGhixEligibilityServiceURL(String ghixEligibilityServiceURL) {
		ELIGIBILITY_URL = ghixEligibilityServiceURL;
	}

	@Value("#{configProp['ghixAHBXServiceURL']}")
	public void setGhixAHBXServiceURL(String ghixAHBXServiceURL) {
		GHIX_AHBX_URL = ghixAHBXServiceURL;
	}

	@Value("#{configProp['ghixTicketMgmtServiceURL']}")
	public void setGhixTicketMgmtServiceURL(String ghixTicketMgmtServiceURL) {
		TICKETMGMT_URL = ghixTicketMgmtServiceURL;
	}

	@Value("#{configProp['ghixPlanMgmtServiceURL']}")
	public void setGhixPlanMgmtServiceURL(String ghixPlanMgmtServiceURL) {
		PLAN_MGMT_URL = ghixPlanMgmtServiceURL;
	}

	@Value("#{configProp['ghixEnrollmentEntityServiceURL']}")
	public void setGhixEnrollmentEntityServiceURL(String ghixEnrollmentEntityServiceURL) {
		GHIX_EE_URL = ghixEnrollmentEntityServiceURL;
	}

	@Value("#{configProp['ghixPrescreenServiceURL']}")
	public void setGhixPrescreenServiceURL(String ghixPrescreenServiceURL) {
		PRESCREEN_SVC_URL = ghixPrescreenServiceURL;
	}

	@Value("#{configProp['ghixBrokerServiceURL']}")
	public void setGhixBrokerServiceURL(String ghixBrokerServiceURL) {
		GHIX_BROKER_URL = ghixBrokerServiceURL;
	}

	@Value("#{configProp['ghixSerffServiceURL']}")
	public void setGhixSerffServiceURL(String ghixSerffServiceURL) {
		GHIX_SERFF_SERVICE_URL = ghixSerffServiceURL;
		}

	@Value("#{configProp['ghixAffiliateServiceURL']}")
	public void setGhixAffiliateServiceURL(String ghixAffiliateServiceURL) {
		AFFILIATE_SERVICE_URL = ghixAffiliateServiceURL;
	}

	@Value("#{configProp['ghixFinanceServiceURL']}")
	public void setGhixFinanceServiceURL(String ghixFinanceServiceURL) {
		FINANCE_SERVICE_URL = ghixFinanceServiceURL;
	}
	
	@Value("#{configProp['ghixConsumerServiceURL']}")
	public void setGhixConsumerServiceURL(String ghixConsumerServiceURL) {
		CONSUMER_SERVICE_URL = ghixConsumerServiceURL;
	}
	
	@Value("#{configProp['ghixHubServiceURL']}")
	public void setGhixHubServiceURL(String ghixHubServiceURL) {
		HUB_SERVICE_URL = ghixHubServiceURL;
	}
	
	@Value("#{configProp['ghixD2CServiceURL']}")
	public void setD2cServiceURL(String d2cServiceURL){
		D2C_SERVICE_URL = d2cServiceURL;
	}
	
	@Value("#{configProp['ghixD2CUHCServiceURL']}")
	public void setGhixD2CUHCServiceUrl(String ghixD2CUHCServiceUrl){
		D2C_UHC_URL = ghixD2CUHCServiceUrl;
	}
	
	@Value("#{configProp['ghixD2CHumanaServiceURL']}")
	public void setGhixD2CHuanaServiceUrl(String ghixD2CHumanaServiceUrl){
		D2C_HUMANA_URL = ghixD2CHumanaServiceUrl;
	}
	
	@Value("#{configProp['ghixD2CAetnaServiceURL']}")
	public void setGhixD2CAetnaServiceUrl(String ghixD2CAetnaServiceUrl){
		D2C_AETNA_URL = ghixD2CAetnaServiceUrl;
	}
	
	@Value("#{configProp['ghixD2CHCSCServiceURL']}")
	public void setGhixD2CHCSCServiceUrl(String ghixD2CHCSCServiceUrl){
		D2C_HCSC_URL = ghixD2CHCSCServiceUrl;
	}
	
	@Value("#{configProp['ghixFeedsConfigServiceURL']}")
	public void setGhixFeedConfigUrlServiceUrl(String ghixFeedConfigServiceUrl){
		FEEDS_URL = ghixFeedConfigServiceUrl;
	}
	
	
	@Value("#{configProp['ghixSsapSvcURL']}")
	public void setGhixSsapSvcURL(String ghixSsapSvcURL) {
		GHIX_SSAP_SVC_URL = ghixSsapSvcURL;
	}
	
	@Value("#{configProp['ghixEligibilitySvcURL']}")
	public void setGHIX_ELIGIBILITY_SVC_URL(String gHIX_ELIGIBILITY_SVC_URL) {
		GHIX_ELIGIBILITY_SVC_URL = gHIX_ELIGIBILITY_SVC_URL;
	}
	
	@Value("#{configProp['ghixHubIntegrationURL']}")
	public void setGhixHubIntegrationURL(String ghixHubIntegrationURL) {
		GHIX_HUB_INTEGRATION_URL = ghixHubIntegrationURL;
	}
	
	public static class CapFeedEndpoints {
		public static final String FEEDS_CONFIG_FINDALL_URL 	=  FEEDS_CONFIG_URL + "/findAll";
		public static final String FEEDS_FINDALLSUMMARY_URL 	=  FEEDS_URL + "/findCarrierSummaries";
		public static final String FEEDS_SHOWCARRIERDETAILS_URL	=  FEEDS_URL+	"/carrierDetail";
		public static final String FEEDS_ORIGINALSTATUSFEEDS_URL =  FEEDS_URL+"/feedDetail/{feedDetailsId}/originalFeedInfo";
		public static final String FEEDS_STATUSFEEDEXCEPTIONS_URL =FEEDS_URL+"/carrierDetail/exception";
		public static final String FEEDS_COMMISIONFEEDDETIALS_URL =FEEDS_URL+"/commissionDetail";
		public static final String FEEDS_COMMISIONFEEDEXCEPTIONS_URL= FEEDS_URL+"/commissionException";
		
	}	
	/**
	 * This inner class captures all REST ENDPOINTS exposed by PLAN-DISPALY module
	 *
	 */
	public static class PlandisplayEndpoints {
		public static final String CALCULATE_TAX_CREDIT_URL 	=  PLANDISPLAY_URL + "plandisplay/calculateTaxCredit";
		public static final String FIND_PLD_HOUSEHOLD_BYID_URL 	=  PLANDISPLAY_URL + "plandisplay/findPldHouseholdById";
		public static final String FIND_PLD_HOUSEHOLD_BY_CMR_HOSUSEHOLD_URL 	=  PLANDISPLAY_URL + "plandisplay/findCmrHouseholdId";
		public static final String FIND_PLD_ORDERITEM_BYID_URL 	=  PLANDISPLAY_URL + "plandisplay/findPldOrderItemById";
		public static final String SAVE_HOUSEHOLD_URL 			=  PLANDISPLAY_URL + "plandisplay/saveHousehold";
		public static final String SAVE_PLDGROUP_URL 			=  PLANDISPLAY_URL + "plandisplay/savePldGroup";
		public static final String FIND_PDLGROUP_BYID_URL 		=  PLANDISPLAY_URL + "plandisplay/findPldGroupById";
		public static final String FIND_BYPERSONID_ANDGROUPID_URL 			=  PLANDISPLAY_URL + "plandisplay/findByPersonIdAndGroupId";
		public static final String FIND_BYGROUPID_URL 			=  PLANDISPLAY_URL + "plandisplay/findByGroupId";
		public static final String DELETE_INDIVIDUAL_PERSON_GROUP_URL 		=  PLANDISPLAY_URL + "plandisplay/deleteIndividualPersonGroup";
		public static final String GET_CARTITEM_BYPLAN_URL 		=  PLANDISPLAY_URL + "plandisplay/getCartItemByPlan";
		public static final String DELETE_ORDERITEM_URL 		=  PLANDISPLAY_URL + "plandisplay/deleteOrderItem";
		public static final String GET_CARTITEMS_URL 			=  PLANDISPLAY_URL + "plandisplay/getCartItems";
		public static final String GET_INDIVIDUALPLANS_URL 		=  PLANDISPLAY_URL + "plandisplay/getplans";
		public static final String FIND_PROVIDER_BY_HOUSHOLDID 	=  PLANDISPLAY_URL + "plandisplay/findProviderByHousholdId";
		public static final String FIND_PROVIDER_BY_LEADID 	=  PLANDISPLAY_URL + "plandisplay/findProviderByLeadId";
		public static final String GET_PLAN_INFO_URL 			=  PLANDISPLAY_URL + "plandisplay/getplanInfo";
		public static final String POST_SPLITHOUSEHOLD_URL 		=  PLANDISPLAY_URL + "plandisplay/postSplitHousehold";
		public static final String SHOW_PREFERENCES_URL 		=  PLANDISPLAY_URL + "plandisplay/showPreferences";
		public static final String SHOW_CUSTOMGROUPS_URL 		=  PLANDISPLAY_URL + "plandisplay/showCustomgroups";
		public static final String POST_CUSTOMGROUPS_URL 		=  PLANDISPLAY_URL + "plandisplay/postCustomgroups";
		public static final String CREATE_ANDUPDATE_GROUP_URL	=  PLANDISPLAY_URL + "plandisplay/createAndUpdateGroup";
		public static final String FIND_GROUPS_BY_HOUSEHOLD	=  PLANDISPLAY_URL + "plandisplay/findGroupsByHousehold";
		public static final String SAVE_PREFERENCES_URL 		=  PLANDISPLAY_URL + "plandisplay/savePreferences";
		public static final String SHOWING_CART_URL 			=  PLANDISPLAY_URL + "plandisplay/showingCart";
		public static final String POPULATE_PDLHOUSEHOLD_URL 	=  PLANDISPLAY_URL + "plandisplay/populatepldhousehold";
		public static final String SAVE_CART_ITEMS_URL 			=  PLANDISPLAY_URL + "plandisplay/saveCartItems";
		public static final String FIND_PLDHOUSEHOLD_BYSHOPPINGID_URL 		=  PLANDISPLAY_URL + "plandisplay/findPldHouseholdByShoppingId";
		public static final String GET_HOUSEHOLD_DATA_URL 		=  PLANDISPLAY_URL + "plandisplay/getHouseholdData";
		public static final String FIND_PLDHOUSEHOLDPERSON_BYID_URL 		=  PLANDISPLAY_URL + "plandisplay/findpldHouseholdpersonbyid";
		public static final String INDIVIDUAL_INFORMATION 		= PLANDISPLAY_URL + "plandisplay/saveIndividualInformation";
		public static final String SAVE_APTC_URL 				= PLANDISPLAY_URL + "plandisplay/saveAptc";
		public static final String SAVE_ORDER_ITEM_URL 			= PLANDISPLAY_URL + "plandisplay/saveOrderItem";
		public static final String FORM_GROUP_LIST 		=  PLANDISPLAY_URL + "plandisplay/formGroupList";
		public static final String POST_FIND_ORDER_BY_HOUSEHOLDID 	= PLANDISPLAY_URL + "plandisplay/findOrderByHouseholdId"; //required by shop
		public static final String RESTORE_APTC_VALUE_URL 		=  PLANDISPLAY_URL + "plandisplay/restoreAptcValue";
		
		//Added by Enrollment Team
		public static final String FIND_BY_ORDERID 			    = PLANDISPLAY_URL + "plandisplay/findbyorderid/{id}";
		public static final String EXTRACT_PERSON_DATA_URL =  PLANDISPLAY_URL + "plandisplay/extractPersonData";
		public static final String SEND_ADMIN_UPDATE_URL 		=  PLANDISPLAY_URL + "plandisplay/sendAdminUpdate";

		public static final String SPECIAL_ENROLLMENT_KEEP_PLAN_URL 	=  PLANDISPLAY_URL + "plandisplay/keepPlan";
		public static final String SPECIAL_ENROLLMENT_PERSONDATA_UPDATE 	=  PLANDISPLAY_URL + "plandisplay/specialEnrollmentPersonDataUpdate";
		public static final String SPECIAL_ENROLLMENT_CHECK_ITEMINCART 	=  PLANDISPLAY_URL + "plandisplay/checkItemInCart";
		public static final String GET_PLDHOUSEHOLD_URL 		=  PLANDISPLAY_URL + "plandisplay/getHousehold";
		public static final String SAVE_INDIVIDUAL_PHIX 	=  PLANDISPLAY_URL + "plandisplay/saveIndividualPHIX";

		public static final String OUR_LOGGEDIN_PAGE = "/account/signup/consumer";
		public static final String GET_PREFERENCES_URL 		=  PLANDISPLAY_URL + "plandisplay/getPreferences";
		public static final String GET_SUBSCRIBER_DATA 		=  PLANDISPLAY_URL + "plandisplay/getSubscriberData";
		public static final String UPDATE_ORDER_STATUS 		=  PLANDISPLAY_URL + "plandisplay/updateOrderStatus";
		
		public static final String SAVE_HOUSEHOLD_APPLICANT_ELIGIBILITY 	=  PLANDISPLAY_URL + "plandisplay/saveHouseholdForApplicantEligibility";
		public static final String FIND_ORDERID_BY_LEADID = PLANDISPLAY_URL + "plandisplay/findOrderidByLeadid/";
		public static final String FIND_ORDERID_BY_SSAP_APPLICATION_ID = PLANDISPLAY_URL + "plandisplay/findOrderIdBySsapApplicationId/";
		public static final String FIND_HOUSEHOLDID_BY_LEADID = PLANDISPLAY_URL + "plandisplay/findHouseholdIdByLeadId";
		public static final String GET_SAML_RESPONSE = PLANDISPLAY_URL + "saml/returnSAMLOutput";
		public static final String GET_SAML_RESPONSE_ATTRIBUTES = PLANDISPLAY_URL + "saml/returnSAMLResponseAttributes";
		public static final String EXTRACT_FFM_HOUSEHOLD_BLOB 	=  PLANDISPLAY_URL + "plandisplay/extractFFMPldHouseholdBlobData";
		public static final String SAVE_FFM_ASSIGNED_CONSUMER_ID = PLANDISPLAY_URL + "plandisplay/saveFFMAssignedConsumerId";
		public static final String SAVE_APPLICANT_ELIGIBILITY_URL = PLANDISPLAY_URL + "plandisplay/saveApplicantEligibilityDetails";
		public static final String FIND_FAV_PLAN_BY_ELIG_LEAD_ID = PLANDISPLAY_URL + "plandisplay/findPlanIdByLeadId";
		public static final String FIND_PLAN_BY_APPLICATION_ID = PLANDISPLAY_URL + "plandisplay/findPlanIdByApplicationId";
		public static final String EXTRACT_FFM_PERSON_DATA_URL =  PLANDISPLAY_URL + "plandisplay/extractFFMPersonData";
		public static final String FIND_PLAN_BY_HOUSEHOLDID = PLANDISPLAY_URL + "plandisplay/findPlanIdByHouseholdId";
		
		public static final String GET_MEMBER_NAMES_FROM_HOUSEHOLD_BLOB = PLANDISPLAY_URL + "plandisplay/getMemberNamesFromHouseholdBlob";
		public static final String UPDATE_PLDPERSON_FOR_TOBACCO_USAGE = PLANDISPLAY_URL + "plandisplay/updatePldPersonForTobaccoUsage";
		
		public static final String LOGIN_PAGE = "/account/user/login";
		public static final String FIND_PLD_GROUP_BYID_URL 	=  PLANDISPLAY_URL + "plandisplay/findPldGroupById";
		public static final String FIND_OREDERID_BY_HOUSEHOLDID_CARTSTATUS 	=  PLANDISPLAY_URL + "plandisplay/getOrderIdByHIdandStatus";
		public static final String UPDATE_SSAP_APPLICATION_ID_FOR_ELIGLEAD_ID =  PLANDISPLAY_URL + "plandisplay/updateSsapApplicationIdForEligLeadId";
		public static final String FIND_PLD_HOUSEHOLD_BY_SSAP_APPLICATION_ID 	=  PLANDISPLAY_URL + "plandisplay/findPldHouseholdBySsapApplicationId";
	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by GHIX-AHBX module
	 *
	 */

	public static class AHBXEndPoints{
		public static final String PLATFORM_VALIDATE_ADDRESS_OEDQ = GHIX_AHBX_URL + "platform/validateaddress/oedq";
		public static final String PLATFORM_ADOBELIVECYCLE = GHIX_AHBX_URL + "platform/createnotice/adobelivecycle";
		public static final String PLATFORM_UCM_CREATECONTENT = GHIX_AHBX_URL + "platform/ecm/createcontent";
		public static final String PLATFORM_UCM_GET_FILE_CONTENT = GHIX_AHBX_URL + "platform/ecm/getfilecontent";

		//Shop urls
		public static final String GET_EMPLOYER_DETAILS  = GHIX_AHBX_URL + "employer/getEmployer";
		public static final String GET_CASE_ORDER_ID     = GHIX_AHBX_URL + "employer/sendOrderId";

		//Delegation urls
		public static String DELEGATIONCODE_RESPONSE = GHIX_AHBX_URL + "delegation/validation/delegationcode";

		//Enrollment Urls
		public static final String ENROLLMENT_IND21_CALL_URL = GHIX_AHBX_URL + "enrollment/sendCarrierUpdatedData";
		public static final String ENROLLMENT_IND20_CALL_URL = GHIX_AHBX_URL + "enrollment/individualplanselection";
		//public static final String ENROLLMENT_IND53_CALL_URL = GHIX_AHBX_URL + "enrollment/verifypin";

		//Agent/Broker Rest URLS (ghix-web to ghix-ahbx)
		public static String SEND_ENTITY_DESIGNATION_DETAIL = GHIX_AHBX_URL + "entity/senddesignationdetail";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_STATUS = GHIX_AHBX_URL + "broker/retrieveindividualBOBStatus";
		public static String GET_EXTERNAL_INDIVIDUAL_BOB_DETAIL= GHIX_AHBX_URL + "broker/retrieveindividualbobdetails";
		public static String GET_EXTERNAL_EMPLOYER_BOB_DETAIL = GHIX_AHBX_URL + "broker/retrieveemployerbobdetails";
		public static String GET_EXTERNAL_EMPLOYER_BOB_STATUS = GHIX_AHBX_URL + "broker/retrieveemployersbobstatus";

		// sync plan and issuer data url
		public static final String SYNC_PLAN_DATA = GHIX_AHBX_URL + "plan/syncWithAHBX";
		public static final String SYNC_ISSUER_DATA = GHIX_AHBX_URL + "issuer/syncWithAHBX";

		//Agent/Assister/Enrollment Entity Rest URLs (ghix-web to ghix-ahbx and ghix-entity to ghix-ahbx)
		public static String SEND_ENTITY_DETAIL = GHIX_AHBX_URL + "entity/sendentitydetail";
	}

	//Enrollment Entity Rest URLS (ghix-web to ghix-entity)
	public static class EnrollmentEntityEndPoints {
		public static final String GET_ALL_SITE_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getAllSites";
		public static final String GET_SITE_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getsitedetail";
		public static final String SAVE_SITE_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/savesitedetail";
	    public static final String GET_SITE_DETAILS_BY_ID=GHIX_EE_URL + "entity/enrollmentEntity/getsitedetailbyId";
	    public static final String GET_SITE_BY_TYPE=GHIX_EE_URL + "entity/enrollmentEntity/getsitebytype";
	    public static final String GET_ENROLLMENTENTITY_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitydetail";
		public static final String SAVE_ENROLLMENTENTITY_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententitydetail";
		public static final String GET_PAYMENT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getpaymentdetails";
		public static final String GET_PAYMENT_DETAILS_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getpaymentdetailsbyid";
		public static final String SAVE_PAYMENT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/savepaymentdetails";
		public static final String SAVE_ENROLLMENTENTITY_STATUS = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententityStatus";
		public static final String GET_ENTITY_ENROLLMENT_DETAILS_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getentitydetailsbyuserid";
		public static final String GET_ENTITY_ENROLLMENT_HISTORY_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententityhistory";
		public static final String GET_POPULATION_SERVED_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getpopulationserveddetail";
		public static final String SAVE_POPULATION_SERVED_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/savepopulationserveddetail";
		public static final String GET_ENRIOLLMENT_ENTITY_INFORMATION = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitybyid";
		public static final String GET_LIST_OF_ENROLLMENT_ENTITY_SITES = GHIX_EE_URL + "entity/enrollmentEntity/getEnrollmentEntitySites";
		public static final String GET_LIST_OF_ENROLLMENT_ENTITY_SITES_BY_SITE_TYPE = GHIX_EE_URL + "entity/enrollmentEntity/getEnrollmentEntitySitesbysitetype";
		public static final String SAVE_ASSISTER_DETAILS = GHIX_EE_URL + "entity/assister/saveassisterdetails";
		public static final String GET_ASSISTER_DETAIL = GHIX_EE_URL + "entity/assister/geteassisterdetailbyid";
		public static final String GET_LIST_OF_ASSISTERS_BY_EE = GHIX_EE_URL + "entity/assister/getlistofassistersbyenrollmententity";
		public static final String POPULATE_NAVIGATION_MENU_MAP = GHIX_EE_URL + "entity/enrollmentEntity/populatenavigationmenumap";
	    public static final String GET_SITE_BY_ID = GHIX_EE_URL + "entity/enrollmentEntity/getsitebyid";
	    public static final String GET_ENROLLMENT_ENTITY_CONTACT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitycontactdetail";
	    public static final String SAVE_ENROLLMENT_ENTITY_CONTACT_DETAILS = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententitycontactdetail";
	    public static final String GET_ENROLLMENTENTITY_DETAILS_BY_ID = GHIX_EE_URL + "entity/entityadmin/getenrollmententitydetailsbyid";
		public static final String GET_ENTITY_DOCUMENTS_DETAILS_BY_ID = GHIX_EE_URL + "entity/entityadmin/getentitydocumentsdetailsbyid";
		public static final String GET_DOCUMENTS_FROM_ECM_BY_ID = GHIX_EE_URL + "entity/entityadmin/getdocumentsfromecmbyid";
		public static final String SAVE_DOCUMENTS_ON_ECM = GHIX_EE_URL + "entity/entityadmin/savedocumentsonecm";
		public static final String SAVE_DOCUMENTS = GHIX_EE_URL + "entity/entityadmin/savedocuments";
		public static final String GET_ENTITY_ENROLLMENT_HISTORY_FOR_ADMIN_BY_ID = GHIX_EE_URL + "entity/entityadmin/getenrollmententityhistoryforadmin";
		public static final String GET_ENROLLMENTENTITY_MAP = GHIX_EE_URL + "entity/entityadmin/getenrollmententitymap";
		public static final String GET_ASSISTER_DETAIN_BY_USER_ID = GHIX_EE_URL + "entity/assister/getassisterdetailbyuserid";
		public static final String GET_ASSISTER_LIST= GHIX_EE_URL +"entity/assisteradmin/getassisterlist";
		public static final String SAVE_ASSISTER_STATUS= GHIX_EE_URL +"entity/assisteradmin/saveassisterstatus";
		public static final String UPDATE_ASSISTER_DETAIL = GHIX_EE_URL +"entity/assisteradmin/updateassisterdetail";
		public static final String GET_ASSISTER_ADMIN_DETAILS_BY_ID= GHIX_EE_URL +"entity/assisteradmin/getassisteradmindetailsbyid";
		public static final String GET_ASSISTER_LIST_FOR_EXCHANGEADMIN= GHIX_EE_URL +"/entity/entityadmin/getassisterlist";
		public static final String GET_ASSISTER_DETAIL_FOR_EXCHANGEADMIN = GHIX_EE_URL + "entity/entityadmin/getassisterdetailsbyid";
		public static final String SAVE_ASSISTER_STATUS_FOR_EXCHANGEADMIN=GHIX_EE_URL+"entity/entityadmin/saveassisterstatus";
		public static final String UPDATE_ASSISTER_INFORMATION = GHIX_EE_URL +"entity/assister/updateassisterinformation";
		public static final String GET_SEARCH_ASSISTER_LIST_FOR_EXCHANGEADMIN=GHIX_EE_URL +"entity/assisteradmin/getsearchassisterlist";
		public static final String GET_ASSISTER_ADMIN_DOCUMENTS_DETAILS_BY_ID=GHIX_EE_URL+"entity/assisteradmin/getassisteradmindocbyid";
		public static final String GET_ASSISTER_DOCUMENTS_FROM_ECM_BY_ID=GHIX_EE_URL+"entity/assisteradmin/getassisterdocfromecmbyid";
		public static final String GET_ASSISTER_DETAILS_BY_ID=GHIX_EE_URL+"entity/assisteradmin/getassisteradminbyid";
		public static final String SAVE_ASSISTER_DOCUMENTS_ON_ECM=GHIX_EE_URL+"entity/assisteradmin/saveassisterdoconecm";
		public static final String SAVE_ASSISTER_DOCUMENTS=GHIX_EE_URL+"entity/assisteradmin/saveassisterdocuments";
		public static final String GET_SEARCH_LIST_OF_ASSISTERS_BY_EE = GHIX_EE_URL +"entity/entityadmin/getsearchassisterlist";
		public static final String ASSISTER_POPULATE_NAVIGATION_MENU_MAP = GHIX_EE_URL + "entity/assister/populatenavigationmenumap";
		public static final String GET_ASSISTER_ACTIVITY_DETAILS_BY_ID= GHIX_EE_URL +"entity/assisteradmin/getassisterhistorydetailsbyid";
		public static final String SAVE_ENROLLMENTENTITY_WITH_DOCUMENT = GHIX_EE_URL + "entity/enrollmentEntity/saveenrollmententitywithdocument";
		public static final String GET_ENROLLMENT_DOCUMENT_HISTORY = GHIX_EE_URL + "entity/enrollmentEntity/getenrollmententitydocumenthistory";
		public static final String DELETE_DOCUMENT =  GHIX_EE_URL + "entity/enrollmentEntity/deletedocument";
		public static final String SEARCH_ENTITIES = GHIX_EE_URL + "entity/assister/searchentities";
		public static final String SEARCH_ASSISTERS = GHIX_EE_URL + "entity/assister/searchassisters";
		public static final String GET_ENROLLMENTENTITY_BY_ID = GHIX_EE_URL + "entity/assister/enrollmententitydetailbyid";
		public static final String GET_ASSISTERS_BY_ENROLLMENTENTITY = GHIX_EE_URL + "entity/assister/getassistersbyenrollmententityid";
		public static final String GET_NO_OF_ASSISTERS_BY_ENROLLMENTENTITY = GHIX_EE_URL + "entity/assister/noofassistersbyenrollmententityandsite";
		public static final String GET_ASSISTER_DETAIL_BY_ID = GHIX_EE_URL + "entity/assister/assisterdetailbyid";
		public static final String DESIGNATE_ASSISTER = GHIX_EE_URL + "entity/assister/designateassister";
		public static final String GET_ASSISTER_DESIGNATION_DETAIL_BY_INDIVIDUAL_ID = GHIX_EE_URL + "entity/assister/getdesignateassisterbyindividualid";
		public static final String GET_INDIVIDUAL_LIST_FOR_ENROLLMENTENTITY = GHIX_EE_URL + "entity/enrollmentEntity/getindividuallistforenrollmententity";
		public static final String GET_ASSISTERS_BY_ENROLLMENTENTITY_AND_SITE_ID = GHIX_EE_URL + "entity/assister/getassistersbyentityandsite";
		public static final String GET_INDIVIDUAL_INFO_BY_ID =GHIX_EE_URL + "entity/assister/getexternalindividualbyid";
		public static final String GET_INDIVIDUAL_LIST_FOR_ASSISTER = GHIX_EE_URL + "entity/assister/getindividuallistforassister";
		public static final String GET_ASSISTER_DESIGNATION_DETAIL_FOR_REQUEST = GHIX_EE_URL + "entity/assister/designateassisterforrequest";
		public static final String SAVE_ASSISTER_DESIGNATION = GHIX_EE_URL + "entity/assister/savedesignateassister";
		public static final String GET_ASSISTER_BY_USER_ID = GHIX_EE_URL + "entity/assister/getassisterbyuserid";
		public static final String DELETE_PAYMENT_METHOD =  GHIX_EE_URL + "entity/enrollmentEntity/deletepaymentmethod";
		public static final String UPDATE_RECEIVED_PAYMENT =  GHIX_EE_URL + "entity/enrollmentEntity/updateReceivedPayment";
		public static final String GET_LIST_OF_CEC = null;
		public static final String REASSIGN_INDIVIDUALS = null;
	}

	public static class EnrollmentEndPoints {
		public static final String LOGOUT_PAGE = "/account/user/logout";
		public static final String FIND_ENROLLMENT_PLANLEVEL_COUNT_BY_BROKER = ENROLLMENT_URL + "enrollment/findEnrollmentPlanLevelCountByBroker";
		public static final String CREATE_INDIVIDUAL_ENROLLMENT_URL_PRIVATE= ENROLLMENT_URL + "enrollment/createIndividualEnrollment";
		public static final String GET_INDIVIDUAL_PLAN_SELECTION_DETAILS_URL= ENROLLMENT_URL + "enrollment/getIndividualPlanSelectionDetails";
		public static final String FIND_ENROLLMENTS_BY_ISSUER_URL= ENROLLMENT_URL + "enrollment/findenrollmentsbyissuer";
		public static final String FIND_TAX_FILING_DATE_FOR_YEAR_URL= ENROLLMENT_URL + "enrollment/findtaxfilingdateforyear";
		public static final String FIND_ENROLLMENTS_BY_PLAN_URL= ENROLLMENT_URL + "enrollment/findenrollmentsbyplan";
		public static final String SEARCH_ENROLLEE_URL= ENROLLMENT_URL + "enrollee/searchEnrollee";
		public static final String FIND_ENROLLEE_BY_ID_URL= ENROLLMENT_URL + "enrollee/findbyid/";
		public static final String EMPLOYER_DIS_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/employerdisenrollment";
		public static final String FIND_ECOMMITTED_ENROLLMENT_BY_TICKET_URL= ENROLLMENT_URL + "offexchange/getecommittedenrollment";
		public static final String UPDATE_ECOMMITTED_ENROLLMENT_URL= ENROLLMENT_URL + "offexchange/updateecommittedenrollment";
		public static final String SAVE_ECOMMITTED_ENROLLMENT_URL= ENROLLMENT_URL + "offexchange/saveecommittedenrollment";
		public static final String SAVE_MANUAL_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/saveManualEnrollment";
		public static final String DELETE_MANUAL_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/deleteManualEnrollment";
		public static final String GET_MANUAL_ENROLLMENT_BY_CMR_HOUSEHOLD_URL= ENROLLMENT_URL + "enrollment/getManualEnrollmentByCmrHouseHoldId";
		public static final String UPDATE_ENROLLMENT_WITH_FFM_DATA_URL= ENROLLMENT_URL + "enrollment/updateenrollmentwithffmdata";
		public static final String RESEND_ENROLLMENT_TO_FFM_URL= ENROLLMENT_URL + "enrollment/resendenrollmenttoffm";
		public static final String FIND_ENROLLMENT_SSAP_APPLICATION_ID= ENROLLMENT_URL + "offexchange/getenrollmentbycmrhouseholdid";
		public static final String FIND_CONSUMER_DETAIL_BY_CMR_HOUSEHOLD_ID = ENROLLMENT_URL + "enrollment/getenrollmentdetailsforconsumerportal/";
		public static final String GET_ENROLLMENT_BY_SSAP_APPLICATION_ID = ENROLLMENT_URL + "offexchange/findenrollmentsbyssapid/";
		public static final String ABORT_ENROLLMENT_BY_SSAP_APPLICATION_ID = ENROLLMENT_URL + "offexchange/abortenrollmentbyssapid";
		
		public static final String CANCEL_PENDING_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/cancelpendingenrollment";
		public static final String GET_ISSUER_BY_INSURER_TAX_ID_URL= ENROLLMENT_URL + "enrollment/getissuerbyinsurertaxidnumber";
		public static final String ENROLLMENT_XML_URL= ENROLLMENT_URL + "enrollment/enrollmentxml";
		public static final String SEND_CARRIER_UPDATED_ENROLLEE_TO_AHBX_RESPONSE_URL= ENROLLMENT_URL + "enrollee/saveCarrierUpdatedDataResponse";
		public static final String ADMIN_UPDATE_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/adminupdateenrollment";
		public static final String DISENROLL_BY_ENROLLMENT_URL= ENROLLMENT_URL + "enrollment/disenrollbyenrollmentid";
		public static final String UPDATE_SHOP_ENROLLMENT_STATUS_URL= ENROLLMENT_URL + "enrollment/updateshopenrollmentstatus";
		public static final String GET_PLANID_AND_ORDERITEMID_BY_ENROLLMENT_ID_URL= ENROLLMENT_URL + "enrollment/getplanidandorderitemidbyenrollmentid";
		public static final String FIND_ENROLLMENTS_BY_CMR_HOUSEHOLD_ID_PRIVATE = ENROLLMENT_URL + "enrollment/findEnrollmentsByCmrHouseholdId/";
		public static final String FIND_ENROLLMENT_BY_SSAP_APPLICATION_ID = ENROLLMENT_URL + "enrollment/findEnrollmentBySsapApplicationId/";
		public static final String DISENROLL_BY_EMPLOYEE_URL= ENROLLMENT_URL + "enrollment/disenrollbyemployeeID";
		
		public static final String FIND_ENROLLMENT_BY_ADMIN = ENROLLMENT_URL + "enrollment/findenrollmentbyadmin";
		//Aetna enrollment application submission
		public static final String AETNA_ENROLLMENT_APPLICATION_SUBMISSION_URL = ENROLLMENT_URL + "aetna/webservice/aetna/applicationsubmission";

		//Added by plandisplay team
		public static final String FIND_ENROLLMENT_BY_ID_URL= ENROLLMENT_URL + "enrollment/findbyid/";
		//Added by Kuldeep for Finance Mgmt.
		public static final String FIND_ENROLLMENT_BY_EMPLOYEE = ENROLLMENT_URL + "enrollment/findbyemployer";
		public static final String SEARCH_ENROLLMENT_BY_CURRENTMONTH = ENROLLMENT_URL + "enrollment/searchenrollmentbycurrentmonth";
		public static final String SEARCH_SUBSCRIBER_BY_ENROLLMENT_ID = ENROLLMENT_URL + "enrollee/findsubscriberbyenrollmentid/";
		public static final String FIND_ENROLLEE_BY_EMPLOYEE = ENROLLMENT_URL + "enrollee/findenrolleebyemployeeid";
		//Added to verify enrollment exists or not.
		public static final String IS_ENROLLMENT_EXISTS = ENROLLMENT_URL + "enrollment/isenrollmentexists/";
		//Added to fetch Remittance data
		public static final String  GET_REMITTANCEDATABY_ENROLLMENT_ID = ENROLLMENT_URL + "enrollment/getremittancedatabyenrollmentid/";
		
		//D2c related EndPoints
		public static final String  CREATE_D2C_ENROLLMENT_WITH_ENROLLEES = ENROLLMENT_URL + "enrollment/createD2CEnrollmentWithEnrollees";
		public static final String  UPDATE_D2C_ENROLLMENT_STATUS = ENROLLMENT_URL + "enrollment/updateD2CEnrollmentStatus";
		public static final String  UPDATE_MANUAL_ENRL_STATUS=ENROLLMENT_URL+"enrollment/updatemanualenrollmentstatus"; 
		public static final String FIND_ENROLLMENT_BY_CARRIER_FEEDDATA = ENROLLMENT_URL + "enrollment/findEnrollmentByCarrierFeedDetails";
		public static final String  UPDATE_ENRL_PERSONAL_INFO=ENROLLMENT_URL+"enrollment/updateEnrollmentPersonalInfo";
		public static final String  UPDATE_POICY_MAJOR_INFO=ENROLLMENT_URL+"enrollment/updateEnrollmentMajorInfo";
		
	}

	public static class ShopEndPoints {
		public static final String GET_EMPLOYER_ENROLLMENT 			 = SHOP_URL + "getEmployerEnrollment";
		public static final String GET_EMPLOYER_CONTRIBUTION_AMOUNT	 = SHOP_URL + "getEmployerContributionAmt";
		public static final String GET_EMPLOYER_DETAILS     		 = SHOP_URL + "employer/getEmployer";
		public static final String GET_EMPLOYER_PLANS       		 = SHOP_URL + "employer/getEmployerPlans";
		public static final String GET_EMPLOYEE_PLANS       		 = SHOP_URL + "getEmployeePlan";
		public static final String UPDATE_EMPLOYER_ENROLLMENT_STATUS = SHOP_URL + "updateEmployerEnrollmentStatus";
		public static final String UPDATE_EMPLOYER_ELIGIBILITY_STATUS = SHOP_URL + "employer/updateEmployerEligibiltyStatus";
		public static final String UPDATE_EMPLOYER_PARTICIPATION_RATE = SHOP_URL + "updateEmployerParticipationRate";
		public static final String SEND_EMPLOYER_APPEAL_NOTIFICATION = SHOP_URL + "employer/appeal/sendEmployerAppealNotification";
		public static final String TERMINATE_EMPLOYER_ENROLLMENT = SHOP_URL + "/terminateEnrollment";
		public static final String GET_EMPLOYEE_AFFORDABILITY_STATUS = SHOP_URL + "getEmployeeAffordabilityStatus";
		public static final String GET_EMPLOYER_REFERENCE_PLANS       		 = SHOP_URL + "employer/getEmployerReferencePlans";
		public static final String GET_EMPLOYEE_DETAILS     		 = SHOP_URL + "employee/getEmployeeDetails";
		public static final String SEND_INDIVIDUAL_INFORMATION 		= SHOP_URL + "employee/sendIndividualInformation";
	}

	public static class EligibilityEndPoints {
		public static final String FPL_CALCULATOR_URL 		= SHOP_URL + "eligibility/fplCalculator";
	}
	
	public static class PhixEndPoints{
		public static final String ESTIMATOR_API = PRESCREEN_SVC_URL + "eligibility/estimator";
		public static final String UPDATE_CONTACT_DETAILS_FOR_LEAD = PRESCREEN_SVC_URL + "eligibility/updateContactDetailsInEligLeadRecord";
		public static final String UPDATE_OTHER_INPUTS_FOR_LEAD = PRESCREEN_SVC_URL + "eligibility/updateOtherInputsInEligLeadRecord";
		public static final String GET_LEAD = PRESCREEN_SVC_URL + "eligibility/getEligLeadRecord";
		public static final String UPDATE_FLOW_FOR_LEAD = PRESCREEN_SVC_URL + "eligibility/updateFlowTypeInEligLeadRecord";
		public static final String SAVE_LEAD = PRESCREEN_SVC_URL + "eligibility/saveEligLeadRecord";
		public static final String UPDATE_APP_STATUS = PRESCREEN_SVC_URL + "eligibility/updateAppStatusInEligLeadRecord";
		public static final String UPDATE_LEAD = PRESCREEN_SVC_URL + "eligibility/updateEligLeadRecord";
		public static final String GET_STATE_CHIP_MEDICAID_RECORD_BY_STATE = PRESCREEN_SVC_URL + "eligibility/getStateChipMedicaidRecordByState";
		public static final String GET_LATEST_LEAD_FOR_PHONE_NUMBER = PRESCREEN_SVC_URL + "eligibility/getLatestEligLeadForPhoneNumber";
	}

	public static class WebServiceEndPoints {
		public static final String GHIX_WEB_PLAN_MGMT_SERVICE = GHIXWEB_SERVICE_URL + "premiumRestService/houseHoldReq";
		public static final String GHIX_WEB_PLAN_MGMT_TRANSFER_PLAN_URL = GHIXWEB_SERVICE_URL + "ghixWebplanMgmtService/tranferPlanDTO";
		public static final String GHIX_WEB_PLAN_MGMT_PROCESS_CSR_URL = GHIXWEB_SERVICE_URL + "ghixWebplanMgmtService/processCSR";
		public static final String GHIX_WEB_PROVIDER_MANAGEMENT_CALHEERS_DATA_UPLOAD_URL = GHIXWEB_SERVICE_URL +"provider/network/filebaseupload";
		public static final String GHIX_WEB_CRM_CONSUMER_GET_ECOMMITMENT_DETAILS_URL = GHIXWEB_SERVICE_URL + "crm/consumer/getECommitmentDetails";
		public static final String GHIX_WEB_CRM_VIEW_CONSUMER_URL  = GHIXWEB_SERVICE_URL + "crm/consumer/viewconsumer";
	}

	public static class PlanMgmtEndPoints {
		public static final String GET_EMPLOYER_REFERENCE_PLANS = PLAN_MGMT_URL + "plan/getrefplan";
		public static final String CHECK_PLAN_AVAILABILITY = PLAN_MGMT_URL + "plan/checkavailability";
		public static final String CHECK_INDV_HEALTH_PLAN_AVAILABILITY = PLAN_MGMT_URL + "plan/checkindvhealthplanavailability";
		public static final String GET_PLAN_DATA_BY_ID_URL = PLAN_MGMT_URL + "plan/getinfo";
		public static final String GET_PLAN_ISSUER_DATA_BY_ID_URL = PLAN_MGMT_URL + "plan/getPlanIssuerInfo";
		public static final String GET_PLANS_BY_ZIP_AND_ISSUERNAME = PLAN_MGMT_URL + "plan/getplansbyissuer";
		public static final String GET_BENCHMARK_PREMIUM_URL = PLAN_MGMT_URL + "premiumRestService/houseHoldReq";
		public static final String GET_EMPLOYER_PLAN_RATE = PLAN_MGMT_URL +  "planratebenefit/getEmployerPlanRate";		
		public static final String GET_LOWEST_PREMIUM_URL = PLAN_MGMT_URL + "planratebenefit/findLowestPremiumPlanRate";
		//public static final String GET_EMPLOYEE_PLAN_RATE_URL = PLAN_MGMT_URL +  "planratebenefit/employee";
		public static final String GET_HOUSEHOLD_PLAN_RATE_URL = PLAN_MGMT_URL +  "planratebenefit/household";
		public static final String GET_BENCHMARK_PLAN_RATE_URL = PLAN_MGMT_URL +  "planratebenefit/rate/benchmark";
		public static final String PLAN_STATUS_UPDATE_URL = PLAN_MGMT_URL +  "planMgmtService/plan/status/update";
		public static final String GET_PLAN_DETAILS_URL = PLAN_MGMT_URL + "plan/getdetails";
		public static final String GET_STM_PLAN_RATE_BENEFIT_URL = PLAN_MGMT_URL +  "stmplanratebenefit/stmplan";

		public static final String TEASER_PLAN_API_URL =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/secondPlan";
		public static final String TEASER_ELIGIBILITY_PLANS_API_URL =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/eligiblityRequest";
		
		public static final String TEASER_PLAN_LOWEST =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/lowestPlan";
		public static final String TEASER_PLAN_NON_TOBACCO =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/nonTobacco";
		public static final String TEASER_PLAN_TOBACCO =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/tobacco";
		public static final String TEASER_PLAN_BRONZE =  PLAN_MGMT_URL + "teaserPlanRestService/teaserPlan/bronzePlan";
		
		public static final String GET_PLAN_BENEFIT_AND_COST_DATA = PLAN_MGMT_URL + "plan/getbenefitcostdata";
		public static final String GET_PLANAVAILABILITY_FOREMPLOYEES_URL = PLAN_MGMT_URL + "plan/checkPlanAvailabilityForEmployees";
		public static final String GET_MEMBER_LEVEL_PLAN_RATE = PLAN_MGMT_URL +  "planratebenefit/memberlevelpremium";

		// SERFF-PM REST CALL
		public static final String TRANSFER_PLAN_URL = PLAN_MGMT_URL + "planMgmtService/transferPlan";
		public static final String PLAN_STATUS_UPDATE = PLAN_MGMT_URL + "planMgmtService/plan/status/update";
		public static final String PRIVATE_EXCHANGE_JOB_URL = PLAN_MGMT_URL + "privateExchangeDailyChecker/sendNotification";
		
		public static final String DISCLAIMER_INFO = PLAN_MGMT_URL + "issuerInfo/disclaimerInfo";
		public static final String GET_ISSUERS_BY_ZIPCODE = PLAN_MGMT_URL + "issuerInfo/byzipcode";
		public static final String GET_ISSUERS_BY_ID = PLAN_MGMT_URL + "issuerInfo/byId";
		public static final String GET_ISSUERS_D2C_YEARS = PLAN_MGMT_URL + "issuerInfo/getIssuerD2CInfo";
		
		// cross sell REST APIS
		public static final String CROSS_SELL_GET_LOWEST_DENTAL_PREMIUM = PLAN_MGMT_URL + "crosssell/getlowestdentalpremium";
		public static final String CROSS_SELL_GET_DENTAL_PLANS = PLAN_MGMT_URL + "crosssell/getdentalplans";
		
	}

	public static class SerffEndPoints {
		public static final String PING_SERFF_MODULE = GHIX_SERFF_SERVICE_URL + "services/Ping";
		public static final String UPDATE_EXCHANGE_WORK_FLOW_STATUS = GHIX_SERFF_SERVICE_URL + "exchangePlanManagementRestService/updateExchangeWorkflowStatus";
	}

	public static class TicketMgmtEndPoints {
		public static final String SEARCHTICKETS 				= TICKETMGMT_URL + "ticketmgmt/searchtickets";
		public static final String GETTICKETTYPE 				= TICKETMGMT_URL + "ticketmgmt/gettickettype";
		public static final String SEARCH_TKMTICKETS_ID 		= TICKETMGMT_URL + "ticketmgmt/findbyid/";
		public static final String SEARCH_TKMTICKETS_SUBJECT 	= TICKETMGMT_URL + "ticketmgmt/getticketsubject";
		public static final String GETWORKFLOWLIST				= TICKETMGMT_URL + "ticketmgmt/getworkflowlist";
		public static final String QUEUEURL 					= TICKETMGMT_URL + "ticketmgmt/queueslist";
		public static final String ADDNEWTICKET_URL 			= TICKETMGMT_URL + "ticketmgmt/addnewtickets";
		public static final String EDIT_TICKET_URL 				= TICKETMGMT_URL + "ticketmgmt/editticket";
		//New URL mapping added by Kuldeep
		public static final String GETTICKETTASKLIST			= TICKETMGMT_URL + "ticketmgmt/gettickettasklist";
		//New URL mapping added by Kuldeep for saving Ticket Comments
		public static final String SAVECOMMENTS					= TICKETMGMT_URL + "ticketmgmt/savecomment";

		public static final String USERTICKETLIST				= TICKETMGMT_URL + "ticketmgmt/getUserTicketList";

		public static final String CLAIMUSERTASK				= TICKETMGMT_URL + "ticketmgmt/claimTicketTask";

		public static final String COMPLETEUSERTASK				= TICKETMGMT_URL + "ticketmgmt/completeTicketTask";
		//new URL mapping added for displaying ticket history
		public static final String TICKETHISTORY                = TICKETMGMT_URL + "ticketmgmt/findhistorybyid/";

		public static final String TASKFORMPROPERTIES			= TICKETMGMT_URL + "ticketmgmt/getTaskFormProperties";

		public static final String CREATENEWTICKET				= TICKETMGMT_URL + "ticketmgmt/createnewticket";
		public static final String UPDATETICKET					= TICKETMGMT_URL + "ticketmgmt/updateticket";
		public static final String GETTICKETSTATUS				= TICKETMGMT_URL + "ticketmgmt/getticketstatus";
		public static final String CANCELTICKET					= TICKETMGMT_URL + "ticketmgmt/cancelticket";
		public static final String GETTICKETDETAILS				= TICKETMGMT_URL + "ticketmgmt/getticketdetails";
		public static final String GETTICKETLIST				= TICKETMGMT_URL + "ticketmgmt/getticketlist";
		public static final String GETTICKETDOCUMENTPATHLIST	= TICKETMGMT_URL + "ticketmgmt/getticketdocuments";
		public static final String GETTICKETACTIVETASKS			= TICKETMGMT_URL + "ticketmgmt/getTicketActiveTask";
		public static final String REASSIGNTICKETACTIVETASKS	= TICKETMGMT_URL + "ticketmgmt/reassignTicketActiveTask";
		public static final String UPDATEQUEUEUSERS				= TICKETMGMT_URL + "ticketmgmt/updatequeueusers";
		public static final String SAVETICKETATTACHMENTS		= TICKETMGMT_URL + "ticketmgmt/saveTicketAttachments";
		public static final String GET_TICKETTASK_BY_ASSIGNEE 	= TICKETMGMT_URL + "ticketmgmt/gettickettaskbyassignee";
		public static final String DELETETICKETATTACHMENTS		= TICKETMGMT_URL + "ticketmgmt/deleteTicketAttachments";
		public static final String GET_TICKET_NUMBERS			= TICKETMGMT_URL + "ticketmgmt/getticketnumbers";
		public static final String GET_REQUESTERS				= TICKETMGMT_URL + "ticketmgmt/getrequesters";
		public static final String GETSIMPLEORDETAILEDHISTORY	= TICKETMGMT_URL + "ticketmgmt/getSimpleOrDetailedHistory";
	}

	/**
	 * This class contains the URLs related to the ghix-prescreen-svc module
	 * @author Sunil Desu
	 *
	 */
	public static class PrescreenServiceEndPoints{
		public static final String CALCULATE_APTC_URL 				= PRESCREEN_SVC_URL+"eligibility/calculateAptc";
		public static final String GET_APTC_URL 				= PRESCREEN_SVC_URL+"eligibility/getAptc";
		public static final String GET_PRESCREEN_RECORD 			= PRESCREEN_SVC_URL+"eligibility/getPrescreenRecord";
		public static final String GET_PRESCREEN_RECORD_BY_SESSION 	= PRESCREEN_SVC_URL+"eligibility/getPrescreenRecordBySessionId";
		public static final String SAVE_PRESCREEN_RECORD 			= PRESCREEN_SVC_URL+"eligibility/savePrescreenRecord";
		public static final String CALCULATE_CSR_URL 				= PRESCREEN_SVC_URL+"eligibility/computeCSR";
	}


	/**
	 * This class contains URLS for Rest apis implemented under ghix-broker
	 */
	public static class BrokerServiceEndPoints{
		public static final String GET_BROKER_DETAIL = GHIX_BROKER_URL+"/broker/getBrokerDetail/";
		public static final String GHIX_WEB_AGENT_SERVICE = GHIX_BROKER_URL + "/broker/designateEmployer/";
		public static final String GET_AGENT_INFORMATION_BY_EMPLOYERID = GHIX_BROKER_URL + "/broker/brokerinformation";
	}

	//Ghix hub URLs
	public static class HubEndPoints {
		public static final String PING_HUB_MODULE = HUB_SERVICE_URL + "services/Ping";
		//public static final String SAVE_APPLICANT_ELIGIBILITY = HUB_SERVICE_URL + "eligibility/applicantEligibility";
		public static final String APPLICANT_ENROLLMENTBB_URL = HUB_SERVICE_URL + "benefitbay/sendenrollmenttobb";
		public static final String APPLICANT_ELIGIBILITY_URL = HUB_SERVICE_URL + "eligibility/applicantEligibility";
		public static final String APPLICANT_ENROLLMENT_URL = HUB_SERVICE_URL + "enrollment/applicantenrollment";
		public static final String JH_AFFILIATE_HOUSEHOLD_DATA = HUB_SERVICE_URL + "jhEligibility/applicantData";
	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by AFFILIATE module
	 *
	 */
	public static class AffiliateServiceEndPoints{
		public static final String ADD_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/add";
		public static final String UPDATE_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/edit";
		public static final String SEARCH_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/search";
		public static final String SEARCH_AFFILIATEFLOW = AFFILIATE_SERVICE_URL +"affiliateflow/search";
		public static final String SEARCH_CAMPAIGN = AFFILIATE_SERVICE_URL +"campaign/search";
		public static final String VIEW_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/%d";
		public static final String GET_AFFILIATE = AFFILIATE_SERVICE_URL +"getaffiliate/%d";

		public static final String VIEW_CAMPAIGN = AFFILIATE_SERVICE_URL +"campaign/%d";
		public static final String UPDATE_CAMPAIGN = AFFILIATE_SERVICE_URL +"campaign/edit";

		public static final String VALIDATE_APIKEY = AFFILIATE_SERVICE_URL +"affiliate/validateapikey/%d/%s";
		public static final String CHECK_IVRNUMBEREXIST= AFFILIATE_SERVICE_URL +"affiliate/checkIfExistIVRNumber/%d";
		public static final String GET_AFFILIATEFLOW = AFFILIATE_SERVICE_URL +"affiliateflow/%d";
		public static final String AUTO_COMPLETE_MANAGE_AFFILIATE = AFFILIATE_SERVICE_URL +"affiliate/manageaffiliate";
		public static final String AUTO_COMPLETE_MANAGE_CAMPAIGN = AFFILIATE_SERVICE_URL +"affiliate/managecampaign";
		public static final String AUTO_COMPLETE_MANAGE_AFFILIATEFLOW = AFFILIATE_SERVICE_URL +"affiliate/manageaffiliateflow";
		//public static final String TRACK_CLICKS= AFFILIATE_SERVICE_URL +"affiliate/clicktracker/%d/%d/%s/%s/%s/%s/%d";
		public static final String TRACK_CLICKS= AFFILIATE_SERVICE_URL +"affiliate/clicktracker/add";
		public static final String GET_AFFILIATE_CLICK = AFFILIATE_SERVICE_URL +"affiliate/affiliateclick/%d";
		
		public static final String ADD_AFFILIATEFILEUPLOAD = AFFILIATE_SERVICE_URL +"affiliateFileUpload/add";
		public static final String GET_AFFILIATEFLOWDETAILS = AFFILIATE_SERVICE_URL +"affiliateFileUpload/getAffFlowDetails";

	}

	/**
	 * This inner class captures all REST ENDPOINTS exposed by FINANCE module
	 */
	public static class FinanceServiceEndPoints
	{
		public static final String SEARCH_NOTPAID_INVOICE = FINANCE_SERVICE_URL + "finance/notpaidemployerinvoices";
		public static final String SEARCH_PAID_INVOICE = FINANCE_SERVICE_URL + "finance/searchpaidemployerinvoices";
		public static final String SEARCH_INVOICE = FINANCE_SERVICE_URL +"finance/searchemployerinvoices";
		public static final String CREATE_PDF = FINANCE_SERVICE_URL +"employerpayment/createpdf/";
		public static final String REISSUE_INVOICE = FINANCE_SERVICE_URL +"employerpayment/reissueinvoice";
		public static final String EMPLOYER_INVOICE = FINANCE_SERVICE_URL +"finance/findemployerinvoicesbyid/";
		public static final String PROCESS_PAYMENT = FINANCE_SERVICE_URL +"employerpayment/processpayment";
		public static final String DUE_EMPLOYER_INVOICES = FINANCE_SERVICE_URL +"finance/finddueemployerinvoices";
		public static final String ISSUER_PAYMENT_PROCESSING = FINANCE_SERVICE_URL + "batch/issuerpaymentprocess";
		public static final String ACTIVE_EMPLOYER_INVOICES = FINANCE_SERVICE_URL + "/finance/findactiveemployerinvoices";
		public static final String EMPLOYER_PAYMENT_INVOICE = FINANCE_SERVICE_URL + "/finance/findemployerpaymentinvoicebyid/";
	}
	
	/**
	 * This class contains the URLs related to the ghix-consumer-svc module
	 * @author Suhasini Goli
	 *
	 */
	public static class ConsumerServiceEndPoints{
		public static final String SAVE_MEMBER_RECORD 		= CONSUMER_SERVICE_URL+"consumer/saveMember";
		public static final String GET_MEMBER_BY_ID 	    = CONSUMER_SERVICE_URL+"consumer/findMemberById/{id}";
		public static final String SAVE_HOUSEHOLD_RECORD 	= CONSUMER_SERVICE_URL+"consumer/saveHousehold";
		public static final String GET_HOUSEHOLD_BY_ID 	    = CONSUMER_SERVICE_URL+"consumer/findHouseholdById";
		public static final String SEARCH_CONSUMER 	    = CONSUMER_SERVICE_URL+"consumer/manageConsumers";
		public static final String SEARCH_LEADS 	    = CONSUMER_SERVICE_URL+"consumer/manageLeads";
		public static final String GET_HOUSEHOLD_BY_USER_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdByUserId";
		public static final String GET_HOUSEHOLD_RECORD_BY_USER_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdRecordByUserId";
		public static final String GET_HOUSEHOLD_BY_ELIG_LEAD_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdByEligLeadId";
		public static final String GET_MEMBERS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findMembersByHousehold";
		public static final String SAVE_FAMILY  = CONSUMER_SERVICE_URL+"consumer/saveFamily";
		public static final String GET_HOUSEHOLD_BY_GI_HOUSEHOLD_ID = CONSUMER_SERVICE_URL + "consumer/findHouseholdByGIHouseholdId";
		public static final String SAVE_FFM_RESPONSE  = CONSUMER_SERVICE_URL+"consumer/saveFFMResponse";
		public static final String SAVE_FAMILY_FROM_FFM  = CONSUMER_SERVICE_URL+"consumer/saveFamilyFromFFM";
		public static final String SAVE_HOUSEHOLD_INFO  = CONSUMER_SERVICE_URL+"consumer/saveHouseholdInformation";
		public static final String DRUG_SEARCH = CONSUMER_SERVICE_URL+"consumer/drugSearch";
		public static final String DRUG_DETAILS = CONSUMER_SERVICE_URL+"consumer/drugDetails";
		public static final String GET_PRESCRIPTION_BY_ID 	       = CONSUMER_SERVICE_URL+"consumer/findPrescriptionById";
		public static final String GET_PRESCRIPTIONS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findPrescriptionsByHousehold";
		public static final String GET_PRESCRIPTIONS_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findPrescriptionsByHouseholdId";
		public static final String SAVE_PRESCRIPTION               = CONSUMER_SERVICE_URL+"consumer/savePrescription";
		public static final String SAVE_PRESCRIPTIONS              = CONSUMER_SERVICE_URL+"consumer/savePrescriptions";
		public static final String GET_PRESCRIPTION_BY_NAME	       = CONSUMER_SERVICE_URL+"consumer/findPrescriptionByName";
		public static final String DELETE_PRESCRIPTION_BY_ID 	   = CONSUMER_SERVICE_URL+"consumer/deletePrescriptionById";
		public static final String DELETE_PRESCRIPTION     	       = CONSUMER_SERVICE_URL+"consumer/deletePrescription";
		public static final String UPDATE_SMOKER_INFO_FOR_MEMBERS  = CONSUMER_SERVICE_URL+"consumer/updateSmokerInfoForMembers";
		public static final String SAVE_LOCATION  = CONSUMER_SERVICE_URL+"consumer/saveLocation";
		public static final String SAVE_HOUSEHOLD_ENROLLMENT  = CONSUMER_SERVICE_URL+"consumer/saveHouseholdEnrollment";
		public static final String GET_HOUSEHOLD_ENROLLMENT_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findHouseholdEnrollmentByHouseholdId";
		public static final String SAVE_STATE_IN_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/saveStateInHousehold";
		public static final String SAVE_CALL_LOGS  = CONSUMER_SERVICE_URL+"consumer/saveCallLogs";
		public static final String SAVE_STAGE_TRACKING_RECORD  = CONSUMER_SERVICE_URL+"consumer/saveStageTracking";
		public static final String GET_STAGE_TRACKING_BY_ID  = CONSUMER_SERVICE_URL+"consumer/findStageTrackingById";
		public static final String GET_STAGE_TRACKING_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findStageTrackingByHouseholdId";
		public static final String UPDATE_STAGE_TRACKING_STATUS  = CONSUMER_SERVICE_URL+"consumer/updateStageTrackingStatus";
		public static final String GET_STAGE_TRACKING_BY_STATUS  = CONSUMER_SERVICE_URL+"consumer/findStageTrackingByStatus";
		public static final String PROCESS_STAGE_TRACKING_RECORDS  = CONSUMER_SERVICE_URL+"consumer/processStageTrackingRecords";
		public static final String GET_APPLICANTS_BY_APPLICATION_ID  = CONSUMER_SERVICE_URL+"consumer/findApplicantsByApplicationId";
		public static final String CREATE_SSAP_APPLICATION  = CONSUMER_SERVICE_URL+"consumer/createSsapApplication";
		public static final String DELETE_SSAP_APPLICATION  = CONSUMER_SERVICE_URL+"consumer/deleteSsapApplication";
		public static final String GET_APPLICATIONS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findApplicationsByHousehold";
		public static final String GET_APPLICATIONS_BY_HOUSEHOLD_ID  = CONSUMER_SERVICE_URL+"consumer/findApplicationsByHouseholdId";
		public static final String GET_SSAP_APPLICATIONS_BY_HOUSEHOLD  = CONSUMER_SERVICE_URL+"consumer/findSsapApplicationsByHousehold";
		public static final String GET_SSAP_APPLICATIONS_BY_ELIG_LEAD  = CONSUMER_SERVICE_URL+"consumer/findSsapApplicationsByEligLead";		
		public static final String FIND_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/getSsapApplicationById";
		public static final String UPDATE_STAGE_BY_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/updateStageBySsapApplicationId";
		public static final String UPDATE_STAGE_AND_STATUS_BY_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/updateStageAndStatusBySsapApplicationId";
		public static final String FIND_STAGE_AND_STATUS_BY_SSAP_APPLICATION_BY_ID  = CONSUMER_SERVICE_URL+"consumer/findStageAndStatusBySsapApplicationId";
		public static final String UPDATE_CMR_HOUSEHOLD_ID_FOR_SSAP_APPLICATIONS  = CONSUMER_SERVICE_URL+"consumer/updateCmrHouseholdIdForSSAPApplications";
		public static final String GET_CONSUMER_CALL_HISTORY 	    = CONSUMER_SERVICE_URL+"consumer/getConsumerCallLogHistory";
		public static final String SAVE_CONSUMER_CALL_LOG 	    = CONSUMER_SERVICE_URL+"consumer/saveConsumerCallLog";
		public static final String SAVE_CONSUMER_EVENT_LOG 	    = CONSUMER_SERVICE_URL+"consumer/saveConsumerEventLog";
		public static final String SAVE_CONSUMER_COMMENT_LOG 	    = CONSUMER_SERVICE_URL+"consumer/saveConsumerCommentLog";
		public static final String UPDATE_HOUSEHOLD_ID_AND_ELIG_LEAD_ID_BY_ANONYMOUS_ELIG_LEAD_ID  = CONSUMER_SERVICE_URL+"consumer/updateHouseholdIdAndEliLeadIdByAnonymousEligLeadId";
		public static final String GET_HOUSEHOLD_LIST = "";
		
	}
	
	public static class D2CServiceEndpoints{
		public static final String ENROLLMENT_RECORD = D2C_SERVICE_URL+ "enrollment";
		public static final String ENROLLMENTUI_RECORD = D2C_SERVICE_URL+ "enrollmentUI";
		public static final String ENROLLMENTPAYMENT_RECORD = D2C_SERVICE_URL+ "enrollmentPayment";
		public static final String READ_ENROLLMENT_BY_HOUSEHOLD_ID = D2C_SERVICE_URL + "enrollment/householdId";
		public static final String READ_COUNTY_BY_ZIP = D2C_SERVICE_URL + "zipcode";
		public static final String READ_HUMANA_NETWORK_IDENT = D2C_SERVICE_URL + "humanaProductIdentifier";
		public static final String ENROLLMENT_STATUS = D2C_SERVICE_URL + "enrollmentStatus";
		public static final String GI_ENROLLMENT_STATUS = D2C_SERVICE_URL + "giEnrollmentStatus";
		public static final String READ_HUMANA_PAYMENT_OPTIONS = D2C_SERVICE_URL + "humanaPaymentOptions";
		public static final String READ_HCSC_DENTAL_RIDER_COUNTY_PREMIUM = D2C_SERVICE_URL + "hcscDentalRiderCountyPremium";
		public static final String READ_ENROLLMENT_BY_SSAP_ID = D2C_SERVICE_URL + "enrollment/ssapid";
	}
	
	/**
	 * This inner class captures all REST ENDPOINTS exposed by Agent/Broker module
	 *
	 */
	public static class AgentServiceEndPoints {
		public static final String GET_AGENT_INFORMATION_BY_EMPLOYERID = GHIXWEB_SERVICE_URL + "broker/brokerinformation";
		public static final String GHIX_WEB_AGENT_SERVICE = GHIX_BROKER_URL + "broker/designateEmployer";
		
	}
	
	public static class SsapEndPoints {
		public static final String SSAP_DATA_ACCESS_URL = ELIGIBILITY_URL + "data/";
		public static final String SSAP_APPLICATION_DATA_URL = ELIGIBILITY_URL + "data/SsapApplication/";
		public static final String RELATTIVE_SSAP_APPLICATION_DATA_URL = "/ghix-eligibility/data/SsapApplication/";
		///ghix-eligibility/data/SsapApplication/17050
		public static final String SSAP_APPLICATION_APPLICANTS_DATA_URL = ELIGIBILITY_URL + "data/SsapApplication/{id}/ssapApplicants/";
		public static final String SSAP_APPLICANTS_DATA_URL = ELIGIBILITY_URL + "data/SsapApplicant/";
	}
}

