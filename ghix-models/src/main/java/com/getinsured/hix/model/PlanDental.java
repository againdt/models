package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name="plan_dental")
public class PlanDental implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanDental_Seq")
	@SequenceGenerator(name = "PlanDental_Seq", sequenceName = "plan_dental_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="plan_id")
	private Plan plan;
	
	@Column(name="parent_plan_id")
	private int parentPlanId;
	
	@Column(name="plan_level", length=10)
	private String planLevel;
	
	@Column(name="cost_sharing", length=5)
	private String costSharing;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="effective_start_date",nullable=true)
	private Date effectiveStartDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="effective_end_date",nullable=true)
	private Date effectiveEndDate;
	
	@Column(name="status", length=10)
	private String status;
	
	@Column(name="acturial_value_certificate", length=100)
	private String acturialValueCertificate;

	@Column(name="benefit_file", length=100)
	private String benefitFile;

	@Column(name="rate_file", length=100)
	private String rateFile;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name="created_by", length=10)
	private Integer createdBy;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	@Column(name="last_updated_by", length=10)
	private Integer lastUpdatedBy;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy="plan",cascade = CascadeType.ALL, fetch=FetchType.LAZY)
    @MapKey(name = "name")
	private Map<String, PlanDentalBenefit> planDentalBenefits = new HashMap<String, PlanDentalBenefit>();
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PlanDentalBenefit> planDentalBenefitVO;

	/*	Commenting for SERFF changes
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "planDental", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PlanDentalServiceRegion> planDentalServiceRegion;*/

	@Temporal(value = TemporalType.DATE)
	@Column(name="rate_effective_date")
	private Date rateEffectiveDate;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name="benefit_effective_date")
	private Date benefitEffectiveDate;

	@Column(name="comment_id")
	private Integer commentId;
	
	//Start of new columns added for HIX-8556
	@Column(name = "out_of_country_coverage", length = 5)
	private String outOfCountryCoverage;

	@Column(name = "out_of_country_coverage_desc", length = 100)
	private String outOfCountryCoverageDesc;

	@Column(name = "out_of_svc_area_coverage", length = 5)
	private String outOfServiceAreaCoverage;

	@Column(name = "out_of_svc_area_coverage_desc", length = 100)
	private String outOfServiceAreaCoverageDesc;

	@Column(name = "national_network", length = 5)
	private String nationalNetwork;
	//End of new columns added for HIX-8556
	
	@Column(name = "design_type")
	private String planDesignType;

	//Start: New columns for HIX-9907
	@Column(name = "is_qhp")
	private String isQHP;
	
	@Column(name = "plan_level_exclusions")
	private String planLevelExclusions;
	
	@Column(name = "av_calc_output_number")
	private String avCalcOutputNumber;
	
	@Column(name = "multiple_network_tiers")
	private String multipleNetworkTiers;
	
	@Column(name = "tier1_util")
	private String tier1Util;
	
	@Column(name = "tier2_util")
	private String tier2Util;
	
	@Column(name = "ehb_appt_for_pediatric_dental")
	private String ehbApptForPediatricDental;
	
	@Column(name = "guaranteed_vs_estimated_rate")
	private String guaranteedVsEstimatedRate;
	//End: New columns for HIX-9907

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "planDental", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PlanDentalCost> planDentalCostsList;
	
	@Column(name = "sbc_doc_name")
	private String sbcDocName;

	@Column(name = "sbc_ucm_id")
	private String sbcUcmId;
	
	@Column(name = "benefits_url")
	private String benefitsUrl;

	@Column(name = "enrollment_url")
	private String enrollmentUrl;
	
	@Column(name = "child_only_offering")
	private String childOnlyOffering;
	
	@Column(name = "is_community_rate")
	private String isCommunityRate;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getParentPlanId() {
		return parentPlanId;
	}

	public void setParentPlanId(int parentPlanId) {
		this.parentPlanId = parentPlanId;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}
	
	public Map<String, PlanDentalBenefit> getPlanDentalBenefits() {
		return planDentalBenefits;
	}

	public void setPlanDentalBenefits(
			Map<String, PlanDentalBenefit> planDentalBenefits) {
		this.planDentalBenefits = planDentalBenefits;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new TSDate()); 
	}
	
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer updatedBy) {
		this.lastUpdatedBy = updatedBy;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date created) {
		this.creationTimestamp = created;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date updated) {
		this.lastUpdateTimestamp = updated;
	}

	public String getActurialValueCertificate() {
		return acturialValueCertificate;
	}

	public void setActurialValueCertificate(String acturialValueCertificate) {
		this.acturialValueCertificate = acturialValueCertificate;
	}

	/*	Commenting for SERFF changes
	public List<PlanDentalServiceRegion> getPlanDentalServiceRegion() {
		return planDentalServiceRegion;
	}

	public void setPlanDentalServiceRegion(
			List<PlanDentalServiceRegion> planDentalServiceRegion) {
		this.planDentalServiceRegion = planDentalServiceRegion;
	}*/

	public String getBenefitFile() {
		return benefitFile;
	}

	public void setBenefitFile(String benefitFile) {
		this.benefitFile = benefitFile;
	}

	public String getRateFile() {
		return rateFile;
	}

	public void setRateFile(String rateFile) {
		this.rateFile = rateFile;
	}

	public Date getRateEffectiveDate() {
		return rateEffectiveDate;
	}

	public void setRateEffectiveDate(Date rateEffectiveDate) {
		this.rateEffectiveDate = rateEffectiveDate;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	
	public String getOutOfCountryCoverage() {
		return outOfCountryCoverage;
	}

	public void setOutOfCountryCoverage(String outOfCountryCoverage) {
		this.outOfCountryCoverage = outOfCountryCoverage;
	}

	public String getOutOfCountryCoverageDesc() {
		return outOfCountryCoverageDesc;
	}

	public void setOutOfCountryCoverageDesc(String outOfCountryCoverageDesc) {
		this.outOfCountryCoverageDesc = outOfCountryCoverageDesc;
	}

	public String getOutOfServiceAreaCoverage() {
		return outOfServiceAreaCoverage;
	}

	public void setOutOfServiceAreaCoverage(String outOfServiceAreaCoverage) {
		this.outOfServiceAreaCoverage = outOfServiceAreaCoverage;
	}

	public String getOutOfServiceAreaCoverageDesc() {
		return outOfServiceAreaCoverageDesc;
	}

	public void setOutOfServiceAreaCoverageDesc(String outOfServiceAreaCoverageDesc) {
		this.outOfServiceAreaCoverageDesc = outOfServiceAreaCoverageDesc;
	}

	public String getNationalNetwork() {
		return nationalNetwork;
	}

	public void setNationalNetwork(String nationalNetwork) {
		this.nationalNetwork = nationalNetwork;
	}

	public String getPlanDesignType() {
		return planDesignType;
	}

	public void setPlanDesignType(String planDesignType) {
		this.planDesignType = planDesignType;
	}

	public String getIsQHP() {
		return isQHP;
	}

	public void setIsQHP(String isQHP) {
		this.isQHP = isQHP;
	}

	public String getPlanLevelExclusions() {
		return planLevelExclusions;
	}

	public void setPlanLevelExclusions(String planLevelExclusions) {
		this.planLevelExclusions = planLevelExclusions;
	}

	public String getAvCalcOutputNumber() {
		return avCalcOutputNumber;
	}

	public void setAvCalcOutputNumber(String avCalcOutputNumber) {
		this.avCalcOutputNumber = avCalcOutputNumber;
	}

	public String getMultipleNetworkTiers() {
		return multipleNetworkTiers;
	}

	public void setMultipleNetworkTiers(String multipleNetworkTiers) {
		this.multipleNetworkTiers = multipleNetworkTiers;
	}

	public String getTier1Util() {
		return tier1Util;
	}

	public void setTier1Util(String tier1Util) {
		this.tier1Util = tier1Util;
	}

	public String getTier2Util() {
		return tier2Util;
	}

	public void setTier2Util(String tier2Util) {
		this.tier2Util = tier2Util;
	}

	public String getEhbApptForPediatricDental() {
		return ehbApptForPediatricDental;
	}

	public void setEhbApptForPediatricDental(String ehbApptForPediatricDental) {
		this.ehbApptForPediatricDental = ehbApptForPediatricDental;
	}

	public String getGuaranteedVsEstimatedRate() {
		return guaranteedVsEstimatedRate;
	}

	public void setGuaranteedVsEstimatedRate(String guaranteedVsEstimatedRate) {
		this.guaranteedVsEstimatedRate = guaranteedVsEstimatedRate;
	}

	public List<PlanDentalBenefit> getPlanDentalBenefitVO() {
		return planDentalBenefitVO;
	}

	public void setPlanDentalBenefitVO(List<PlanDentalBenefit> planDentalBenefitVO) {
		this.planDentalBenefitVO = planDentalBenefitVO;
	}

	public List<PlanDentalCost> getPlanDentalCostsList() {
		return planDentalCostsList;
	}

	public void setPlanDentalCostsList(List<PlanDentalCost> planDentalCostsList) {
		this.planDentalCostsList = planDentalCostsList;
	}

	public String getSbcDocName() {
		return sbcDocName;
	}

	public void setSbcDocName(String sbcDocName) {
		this.sbcDocName = sbcDocName;
	}

	public String getSbcUcmId() {
		return sbcUcmId;
	}

	public void setSbcUcmId(String sbcUcmId) {
		this.sbcUcmId = sbcUcmId;
	}

	public String getBenefitsUrl() {
		return benefitsUrl;
	}

	public void setBenefitsUrl(String benefitsUrl) {
		this.benefitsUrl = benefitsUrl;
	}

	public String getEnrollmentUrl() {
		return enrollmentUrl;
	}

	public void setEnrollmentUrl(String enrollmentUrl) {
		this.enrollmentUrl = enrollmentUrl;
	}

	public String getChildOnlyOffering() {
		return childOnlyOffering;
	}

	public void setChildOnlyOffering(String childOnlyOffering) {
		this.childOnlyOffering = childOnlyOffering;
	}

	public String getIsCommunityRate() {
		return isCommunityRate;
	}

	public void setIsCommunityRate(String isCommunityRate) {
		this.isCommunityRate = isCommunityRate;
	}
}
