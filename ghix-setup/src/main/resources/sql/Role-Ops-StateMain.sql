--liquibase formatted sql

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED,POST_REG_URL) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'OPERATIONS',(select landing_page from roles where name='ADMIN'),'OPERATIONS',SYSTIMESTAMP, (select POST_REG_URL from roles where name='ADMIN'));


