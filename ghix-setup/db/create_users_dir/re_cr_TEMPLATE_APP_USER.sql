create role TEMPLATE_APP_USER_ROLE;
--drop user TEMPLATE_APP_USER cascade;
create user TEMPLATE_APP_USER identified by TEMPLATE_APP_USER 
default tablespace TEMPLATE_DATA temporary tablespace temp quota unlimited on TEMPLATE_DATA; 

----
grant create session to TEMPLATE_APP_USER;
grant create synonym to TEMPLATE_APP_USER;
grant TEMPLATE_APP_USER_ROLE to TEMPLATE_APP_USER;
quit

