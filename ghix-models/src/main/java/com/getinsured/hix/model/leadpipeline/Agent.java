package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

public class Agent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String getCapAgentId() {
		return capAgentId;
	}

	public void setCapAgentId(String capAgentId) {
		this.capAgentId = capAgentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String capAgentId;	
	 
	private String  name;	
	 

}
