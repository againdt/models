package com.getinsured.hix.model.agency.assistant;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant.ApprovalStatus;

@Entity
@IdClass(AgencyAssistantAudId.class)
@Table(name="AGENCY_ASSISTANT_AUD")
public class AgencyAssistantAud implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	@Id
	@Column(name = "REV")
	private int rev;

	@Column(name = "REVTYPE")
	private int revType;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;

	@Column(name="primary_contact_number")
	private String primaryContactNumber;
	
	@Column(name="business_contact_number")
	private String businessContactNumber;
	
	@Column(name="personal_email_address")
	private String personalEmailAddress;
	
	@Column(name="business_email_address")
	private String businessEmailAddress;
	
	@Column(name = "communication_pref")
	private String communicationPreference;
	
	@Column(name = "business_legal_name")
	private String businessLegalName;
	
	@OneToOne(cascade = { CascadeType.ALL }) 
	@JoinColumn(name="business_address")
	private Location businessAddress;

	@OneToOne(cascade = { CascadeType.ALL }) 
	@JoinColumn(name="correspondence_address")
	private Location correspondenceAddress;
	
	@Column(name = "assistant_role")
	private String assistantRole;
	
	@Column(name = "agency_assistant_id")
	private String assistantNumber;
	
	@Column(name="approval_date")
	private Timestamp approvalDate;

	@Column(name="approval_number")
	private Long approvalNumber;

	@Column(name="approval_status")
	@Enumerated(EnumType.STRING)
	private ApprovalStatus approvalStatus;

	@Column(name="delegation_code")
	private String delegationCode;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="creation_date")
	private Timestamp creationDate;

	@Column(name="last_update_date")
	private Timestamp lastUpdateDate;

	@Column(name="last_updated_by")
	private Long lastUpdatedBy;
	
	@Column(name="comments_id")
	private Long commentsId;
	
	@ManyToOne
	@JoinColumn(name="agency_id")
	private Agency agency;
	
	@OneToOne
	@JoinColumn(name = "userid")
	private AccountUser user;
	
	@Column(name = "status")
	private String status;

	@Column(name="activity_mode")
	private String activityMode;
	
	@Column(name="ACTIVITY_COMMENTS_ID")
	private Long activityCommentsId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public int getRevType() {
		return revType;
	}

	public void setRevType(int revType) {
		this.revType = revType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPrimaryContactNumber() {
		return primaryContactNumber;
	}

	public void setPrimaryContactNumber(String primaryContactNumber) {
		this.primaryContactNumber = primaryContactNumber;
	}

	public String getBusinessContactNumber() {
		return businessContactNumber;
	}

	public void setBusinessContactNumber(String businessContactNumber) {
		this.businessContactNumber = businessContactNumber;
	}

	public String getPersonalEmailAddress() {
		return personalEmailAddress;
	}

	public void setPersonalEmailAddress(String personalEmailAddress) {
		this.personalEmailAddress = personalEmailAddress;
	}

	public String getBusinessEmailAddress() {
		return businessEmailAddress;
	}

	public void setBusinessEmailAddress(String businessEmailAddress) {
		this.businessEmailAddress = businessEmailAddress;
	}

	public String getCommunicationPreference() {
		return communicationPreference;
	}

	public void setCommunicationPreference(String communicationPreference) {
		this.communicationPreference = communicationPreference;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public Location getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(Location businessAddress) {
		this.businessAddress = businessAddress;
	}

	public Location getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	public void setCorrespondenceAddress(Location correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	public String getAssistantRole() {
		return assistantRole;
	}

	public void setAssistantRole(String assistantRole) {
		this.assistantRole = assistantRole;
	}

	public String getAssistantNumber() {
		return assistantNumber;
	}

	public void setAssistantNumber(String assistantNumber) {
		this.assistantNumber = assistantNumber;
	}

	public Timestamp getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Timestamp approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Long getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(Long approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getDelegationCode() {
		return delegationCode;
	}

	public void setDelegationCode(String delegationCode) {
		this.delegationCode = delegationCode;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Long getCommentsId() {
		return commentsId;
	}

	public void setCommentsId(Long commentsId) {
		this.commentsId = commentsId;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActivityMode() {
		return activityMode;
	}

	public void setActivityMode(String activityMode) {
		this.activityMode = activityMode;
	}
	
	public Long getActivityCommentsId() {
		return activityCommentsId;
	}

	public void setActivityCommentsId(Long activityCommentsId) {
		this.activityCommentsId = activityCommentsId;
	}
}
