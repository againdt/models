
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.ValidationErrors;

/**
 * <p>Java class for TransferPlanResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferPlanResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}exchangeWorkflowStatus"/>
 *         &lt;element name="validationErrors" type="{http://www.serff.com/planManagementExchangeApi/common/model/pm}ValidationErrors"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exchangeWorkflowStatus",
    "validationErrors"
})
@XmlRootElement(name="transferPlanResponse")
public class TransferPlanResponse {

    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true, nillable = true)
    protected String exchangeWorkflowStatus;
    @XmlElement(required = true, nillable = true)
    protected ValidationErrors validationErrors;

    /**
     * Gets the value of the exchangeWorkflowStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeWorkflowStatus() {
        return exchangeWorkflowStatus;
    }

    /**
     * Sets the value of the exchangeWorkflowStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeWorkflowStatus(String value) {
        this.exchangeWorkflowStatus = value;
    }

    /**
     * Gets the value of the validationErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationErrors }
     *     
     */
    public ValidationErrors getValidationErrors() {
        return validationErrors;
    }

    /**
     * Sets the value of the validationErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationErrors }
     *     
     */
    public void setValidationErrors(ValidationErrors value) {
        this.validationErrors = value;
    }

}
