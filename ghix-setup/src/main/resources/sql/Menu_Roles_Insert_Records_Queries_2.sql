insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) ='sales_director' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plans'),(select id from roles where lower(name) ='sales_director' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='sales_director' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Leads'),(select id from roles where lower(name) ='sales_director' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Users'),(select id from roles where lower(name) ='sales_director' ),4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) ='sales_director' ),5,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin Config'),(select id from roles where lower(name) ='sales_director' ),6,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Payroll'),(select id from roles where lower(name) ='sales_director' ),7,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agents'),(select id from roles where lower(name) ='sales_director' ),8,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) ='sales_director' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View QHPs'),(select id from roles where lower(name) ='sales_director' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='View SADPs'),(select id from roles where lower(name) ='sales_director' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create New Consumer'),(select id from roles where lower(name) ='sales_director' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='sales_director' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='DNC Tool'),(select id from roles where lower(name) ='sales_director' ),22,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Leads'),(select id from roles where lower(name) ='sales_director' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add User'),(select id from roles where lower(name) ='sales_director' ),40,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Users'),(select id from roles where lower(name) ='sales_director' ),41,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Teams'),(select id from roles where lower(name) ='sales_director' ),42,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) ='sales_director' ),50,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) ='sales_director' ),51,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Workgroup'),(select id from roles where lower(name) ='sales_director' ),52,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin'),(select id from roles where lower(name) ='sales_director' ),60,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Agent Variable Pay'),(select id from roles where lower(name) ='sales_director' ),70,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Agents'),(select id from roles where lower(name) ='sales_director' ),80,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Issuers'),(select id from roles where lower(name) ='customer_support' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Consumers'),(select id from roles where lower(name) ='customer_support' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Tickets'),(select id from roles where lower(name) ='customer_support' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin Config'),(select id from roles where lower(name) ='customer_support' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) ='customer_support' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create New Consumer'),(select id from roles where lower(name) ='customer_support' ),10,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Consumers'),(select id from roles where lower(name) ='customer_support' ),11,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='DNC Tool'),(select id from roles where lower(name) ='customer_support' ),12,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Create Ticket'),(select id from roles where lower(name) ='customer_support' ),20,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Tickets'),(select id from roles where lower(name) ='customer_support' ),21,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Policy Admin'),(select id from roles where lower(name) ='customer_support' ),30,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Affiliates'),(select id from roles where lower(name) ='affiliate_admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Add Affiliate  '),(select id from roles where lower(name) ='affiliate_admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Affiliate'),(select id from roles where lower(name) ='affiliate_admin' ),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Flow'),(select id from roles where lower(name) ='affiliate_admin' ),2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Campaign'),(select id from roles where lower(name) ='affiliate_admin' ),3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Census Files'),(select id from roles where lower(name) ='affiliate_admin' ),4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- Deleting incorrect entry of reports in issuer admin role
delete from menu_roles m_r
where m_r.id in(
SELECT mr.id
FROM menu_roles mr,
  roles r,
  menu_items m
WHERE mr.role_id     = r.id
AND mr.MENU_ITEMS_ID = m.id
AND r.name = 'ISSUER_ADMIN'
and m.caption = 'Reports');

insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Plan Reports'),(select id from roles where lower(name) ='issuer_admin' ),17,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
insert into menu_roles (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Manage Issuer Accounts'),(select id from roles where lower(name) ='issuer_admin' ),0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);