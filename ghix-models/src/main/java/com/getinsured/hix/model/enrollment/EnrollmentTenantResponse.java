/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.dto.enrollment.EnrollmentTenantDTO;

/**
 * HIX-70722 Enrollment Tenant Response Class
 * @author negi_s
 * @since 06/07/2015
 *
 */
public class EnrollmentTenantResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String leadId;
	private String agentId;
	private String userRole; // Enumeration ?
	private List<EnrollmentTenantDTO> enrollments;
	private String status;
	private String errMsg;
	private int errCode;
	
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public List<EnrollmentTenantDTO> getEnrollments() {
		return enrollments;
	}
	public void setEnrollments(List<EnrollmentTenantDTO> enrollments) {
		this.enrollments = enrollments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public int getErrCode() {
		return errCode;
	}
	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

}
