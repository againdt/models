
        INSERT INTO PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,'VIEW_ENROLLMENT_REPORT');		
		
		INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   
    		VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
    		(select id from roles where name = 'ADMIN'), (select id from permissions where name='VIEW_ENROLLMENT_REPORT'));
