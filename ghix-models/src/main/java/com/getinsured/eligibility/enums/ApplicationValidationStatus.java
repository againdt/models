package com.getinsured.eligibility.enums;

public enum ApplicationValidationStatus {
	
	INITIAL, NOTREQUIRED, PENDING, VERIFIED, CANCELLED, OVERRIDDEN, PENDING_INFO, HMS_INTRANSIT;

}
