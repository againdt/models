package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the external employers database table.
 * 
 */

@Entity
@Table(name = "external_employer")
public class ExternalEmployer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "External_Employer_Seq")
	@SequenceGenerator(name = "External_Employer_Seq", sequenceName = "External_Employer_Seq", allocationSize = 1)
	@Column(name = "id")
	private long serialID;
	
	@Column(name = "employer_case_id")
	private long id;

	@Column(name = "application_end_date")
	private Date applicationEndDate;
	
	@Column(name = "application_initiated_date")
	private Date applicationInitiatedDate;
	
	@Column(name = "application_submit_date")
	private Date applicationSubmitDate;
	
	@Column(name = "application_withdrawal_date")
	private Date applicationWithdrawalDate;
	
	@Column(name = "avg_annual_salary")
	private Float averageAnnualSalary;
	
	@Column(name = "business_legal_name")
	private String name;
	
	//EmployerBOBDetailsDTO : contactAddressCity
	@Column(name = "city")
	private String city;
	
	//EmployerBOBDetailsDTO : contactAddressCountry
	@Column(name = "country")
	private String country;
	
	//EmployerBOBDetailsDTO : contactAddressFirstLine
	@Column(name = "address1")
	private String address1;
	
	//EmployerBOBDetailsDTO : contactAddressSecondLine
	@Column(name = "address2")
	private String address2;
	
	//EmployerBOBDetailsDTO : contactAddressState
	@Column(name = "state")
	private String state;
	
	//EmployerBOBDetailsDTO : contactAddressZipCode
	@Column(name = "zip")
	private String zip;
	
	//EmployerBOBDetailsDTO : contactEmailID
	@Column(name = "contact_email")
	private String contactEmail;
	
	@Column(name = "contact_first_name")
	private String contactFirstName;
	
	@Column(name = "contact_last_name")
	private String contactLastName;
	
	//EmployerBOBDetailsDTO : contactPhoneNumber
	@Column(name = "contact_number")
	private Long contactNumber;
	
	@Column(name = "eligibility_status")
	private String eligibilityStatus;
	
/*	@OneToOne
	@JoinColumn(name="externalemployerid", referencedColumnName="externalemployerid",insertable=false,updatable=true)
	private DesignateBroker designateBroker; */
	
	@Column(name = "total_enrollees")
	private Integer enrolleesTotalCount;
	
	@Column(name = "enrollment_status")
	private String enrollmentStatus;
	
	@Column(name = "federal_ein")
	private Integer federalEIN;
	
	//EmployerBOBDetailsDTO : preferedCommunicationMethod
	@Column(name = "communication_pref")
	private String communicationPref;
	
	@Column(name = "reporting_name")
	private String reportingName;
	
	@Column(name = "state_ein")
	private Integer stateEIN;
	
	@Column(name = "total_employees")
	private Integer totalEmployees;
	
	//EmployerBOBDetailsDTO : totalFullTimeEmployees
	@Column(name = "fulltime_emp")
	private Integer fullTimeEmp;
	
	@Column(name = "EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@Column(name = "PRI_WS_ADDRESS_LINE1")
	private String primaryWorkSiteAddressLine1;
	
	@Column(name = "PRI_WS_ADDRESS_LINE2")
	private String primaryWorkSiteAddressLine2;
	
	@Column(name = "PRI_WS_CITY")
	private String primaryWorkSiteCity;
	
	@Column(name = "PRI_WS_STATE")
	private String primaryWorkSiteState;
	
	@Column(name = "PRI_WS_ZIP")
	private Integer primaryWorkSiteZip;
	
	@Column(name = "ORGANIZATION_TYPE")
	private String organizationType;
	
	@Column(name = "BENCHMARK_PLAN")
	private String benchMarkPlan;
	
	@Column(name = "PLAN_TIER")
	private String planTier;
	
	@Column(name = "LOCATIONS")
	private String locations;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSerialID() {
		return serialID;
	}

	public void setSerialID(long serialID) {
		this.serialID = serialID;
	}

	public Date getApplicationEndDate() {
		return applicationEndDate;
	}

	public void setApplicationEndDate(Date applicationEndDate) {
		this.applicationEndDate = applicationEndDate;
	}

	public Date getApplicationInitiatedDate() {
		return applicationInitiatedDate;
	}

	public void setApplicationInitiatedDate(Date applicationInitiatedDate) {
		this.applicationInitiatedDate = applicationInitiatedDate;
	}

	public Date getApplicationSubmitDate() {
		return applicationSubmitDate;
	}

	public void setApplicationSubmitDate(Date applicationSubmitDate) {
		this.applicationSubmitDate = applicationSubmitDate;
	}

	public Date getApplicationWithdrawalDate() {
		return applicationWithdrawalDate;
	}

	public void setApplicationWithdrawalDate(Date applicationWithdrawalDate) {
		this.applicationWithdrawalDate = applicationWithdrawalDate;
	}

	public Float getAverageAnnualSalary() {
		return averageAnnualSalary;
	}

	public void setAverageAnnualSalary(Float averageAnnualSalary) {
		this.averageAnnualSalary = averageAnnualSalary;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public Integer getEnrolleesTotalCount() {
		return enrolleesTotalCount;
	}

	public void setEnrolleesTotalCount(Integer enrolleesTotalCount) {
		this.enrolleesTotalCount = enrolleesTotalCount;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public Integer getFederalEIN() {
		return federalEIN;
	}

	public void setFederalEIN(Integer federalEIN) {
		this.federalEIN = federalEIN;
	}

	public String getCommunicationPref() {
		return communicationPref;
	}

	public void setCommunicationPref(String communicationPref) {
		this.communicationPref = communicationPref;
	}

	public String getReportingName() {
		return reportingName;
	}

	public void setReportingName(String reportingName) {
		this.reportingName = reportingName;
	}

	public Integer getStateEIN() {
		return stateEIN;
	}

	public void setStateEIN(Integer stateEIN) {
		this.stateEIN = stateEIN;
	}

	public Integer getTotalEmployees() {
		return totalEmployees;
	}

	public void setTotalEmployees(Integer totalEmployees) {
		this.totalEmployees = totalEmployees;
	}

	public Integer getFullTimeEmp() {
		return fullTimeEmp;
	}

	public void setFullTimeEmp(Integer fullTimeEmp) {
		this.fullTimeEmp = fullTimeEmp;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getPrimaryWorkSiteAddressLine1() {
		return primaryWorkSiteAddressLine1;
	}

	public void setPrimaryWorkSiteAddressLine1(String primaryWorkSiteAddressLine1) {
		this.primaryWorkSiteAddressLine1 = primaryWorkSiteAddressLine1;
	}

	public String getPrimaryWorkSiteAddressLine2() {
		return primaryWorkSiteAddressLine2;
	}

	public void setPrimaryWorkSiteAddressLine2(String primaryWorkSiteAddressLine2) {
		this.primaryWorkSiteAddressLine2 = primaryWorkSiteAddressLine2;
	}

	public String getPrimaryWorkSiteCity() {
		return primaryWorkSiteCity;
	}

	public void setPrimaryWorkSiteCity(String primaryWorkSiteCity) {
		this.primaryWorkSiteCity = primaryWorkSiteCity;
	}

	public String getPrimaryWorkSiteState() {
		return primaryWorkSiteState;
	}

	public void setPrimaryWorkSiteState(String primaryWorkSiteState) {
		this.primaryWorkSiteState = primaryWorkSiteState;
	}

	public Integer getPrimaryWorkSiteZip() {
		return primaryWorkSiteZip;
	}

	public void setPrimaryWorkSiteZip(Integer primaryWorkSiteZip) {
		this.primaryWorkSiteZip = primaryWorkSiteZip;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getBenchMarkPlan() {
		return benchMarkPlan;
	}

	public void setBenchMarkPlan(String benchMarkPlan) {
		this.benchMarkPlan = benchMarkPlan;
	}

	public String getPlanTier() {
		return planTier;
	}

	public void setPlanTier(String planTier) {
		this.planTier = planTier;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}
	
	
}
