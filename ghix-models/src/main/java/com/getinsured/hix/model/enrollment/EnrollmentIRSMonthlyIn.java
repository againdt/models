package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.model.LookupValue;

@Entity
@Table(name="ENRL_IRS_MONTHLY_IN")
public class EnrollmentIRSMonthlyIn implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_IRS_MONTHLY_IN_SEQ")
	@SequenceGenerator(name = "ENRL_IRS_MONTHLY_IN_SEQ", sequenceName = "ENRL_IRS_MONTHLY_IN_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name = "BATCH_ID")
	private String batchId;
	
	@Column(name = "PREVIOUS_BATCH_ID")
	private String previousBatchId;
	
	@Column(name = "BATCH_CATEGORY_CODE")
	private String batchCategoryCode;
	
	@Column(name = "DOCUMENT_SEQ_ID")
	private String documentSeqId;
	
	@Column(name="IN_PACKAGE_FILE_NAME")
	private String inPackageFileName;
	
	@Column(name = "RESPONSE_FILE_NAME")
	private String responseFileName;
	
	@OneToOne
    @JoinColumn(name="ERROR_CODE_LKP",nullable= true)
	private LookupValue errorCodeLkp;
	
	@Column(name = "ERROR_MESSAGE")
	private String errorMessage;
	
	@Column(name = "EFT_FILE_NAME")
	private String eftFileName;
	
	@Column(name = "JOBEXECUTION_ID")
	private String jobExecutionId;
	
	@Column(name = "PROCESS_ERROR")
	private String processError;
	
	@Column(name = "ERROR_CODE")
	private String errorCode;
	
	@Column(name = "IS_PROCESSED_FLAG")
	private String isProcessedFlag;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;
	
	@OneToMany(mappedBy="enrollmentMonthlyIn", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentIRSMonthlyInDtl> enrollmentIrsMonthlyInDtlList;
	
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the batchId
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return the previousBatchId
	 */
	public String getPreviousBatchId() {
		return previousBatchId;
	}

	/**
	 * @param previousBatchId the previousBatchId to set
	 */
	public void setPreviousBatchId(String previousBatchId) {
		this.previousBatchId = previousBatchId;
	}

	/**
	 * @return the batchCategoryCode
	 */
	public String getBatchCategoryCode() {
		return batchCategoryCode;
	}

	/**
	 * @param batchCategoryCode the batchCategoryCode to set
	 */
	public void setBatchCategoryCode(String batchCategoryCode) {
		this.batchCategoryCode = batchCategoryCode;
	}

	/**
	 * @return the documentSeqId
	 */
	public String getDocumentSeqId() {
		return documentSeqId;
	}

	/**
	 * @param documentSeqId the documentSeqId to set
	 */
	public void setDocumentSeqId(String documentSeqId) {
		this.documentSeqId = documentSeqId;
	}

	/**
	 * @return the inPackageFileName
	 */
	public String getInPackageFileName() {
		return inPackageFileName;
	}

	/**
	 * @param inPackageFileName the inPackageFileName to set
	 */
	public void setInPackageFileName(String inPackageFileName) {
		this.inPackageFileName = inPackageFileName;
	}

	/**
	 * @return the responseFileName
	 */
	public String getResponseFileName() {
		return responseFileName;
	}

	/**
	 * @param responseFileName the responseFileName to set
	 */
	public void setResponseFileName(String responseFileName) {
		this.responseFileName = responseFileName;
	}

	/**
	 * @return the errorCodeLkp
	 */
	public LookupValue getErrorCodeLkp() {
		return errorCodeLkp;
	}

	/**
	 * @param errorCodeLkp the errorCodeLkp to set
	 */
	public void setErrorCodeLkp(LookupValue errorCodeLkp) {
		this.errorCodeLkp = errorCodeLkp;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getEftFileName() {
		return eftFileName;
	}

	public void setEftFileName(String eftFileName) {
		this.eftFileName = eftFileName;
	}

	public String getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(String jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	public String getProcessError() {
		return processError;
	}

	public void setProcessError(String processError) {
		this.processError = processError;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the isProcessedFlag
	 */
	public String getIsProcessedFlag() {
		return isProcessedFlag;
	}

	/**
	 * @param isProcessedFlag the isProcessedFlag to set
	 */
	public void setIsProcessedFlag(String isProcessedFlag) {
		this.isProcessedFlag = isProcessedFlag;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the enrollmentIrsMonthlyInDtlList
	 */
	public List<EnrollmentIRSMonthlyInDtl> getEnrollmentIrsMonthlyInDtlList() {
		return enrollmentIrsMonthlyInDtlList;
	}

	/**
	 * @param enrollmentIrsMonthlyInDtlList the enrollmentIrsMonthlyInDtlList to set
	 */
	public void setEnrollmentIrsMonthlyInDtlList(List<EnrollmentIRSMonthlyInDtl> enrollmentIrsMonthlyInDtlList) {
		this.enrollmentIrsMonthlyInDtlList = enrollmentIrsMonthlyInDtlList;
	}
}
