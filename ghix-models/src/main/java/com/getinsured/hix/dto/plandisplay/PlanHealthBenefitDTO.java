package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.PlanHealthBenefit;

public class PlanHealthBenefitDTO extends PlanHealthBenefit{

	private static final long serialVersionUID = 1L;

	private int planHealthId;
	
	private int planId;

	private String effectiveStartDate;
	
	private String effectiveEndDate;

	public int getPlanHealthId() {
		return planHealthId;
	}

	public void setPlanHealthId(int planHealthId) {
		this.planHealthId = planHealthId;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}
	
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	
	
}
