package com.getinsured.hix.ui.model.eligibility.prescreen;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * This class encapsulates the household information as constructed from the
 * Household tab of the prescreen application UI.
 * 
 * <p>
 * It contains a list of household members and a boolean value indicating if any
 * person in the household is disabled. The boolean is default to false.
 * </p>
 * 
 * @author Sunil Desu
 * @since April 17 2013
 * 
 */
public class PrescreenHousehold {

	private List<PrescreenHouseholdMember> householdMembers;
	
	public List<PrescreenHouseholdMember> getHouseholdMembers() {
		return householdMembers;
	}

	public void setHouseholdMembers(
			List<PrescreenHouseholdMember> householdMembers) {
		this.householdMembers = householdMembers;
	}

	@JsonIgnore
	public int getNumberOfMembersSeekingCoverage() {
		if (householdMembers == null || householdMembers.size() == 0) {
			return 0;
		}
		int numberOfMembersSeekingCoverage = 0;
		for (PrescreenHouseholdMember householdMember : householdMembers) {
			if (householdMember.isSeekingCoverage()) {
				numberOfMembersSeekingCoverage++;
			}
		}
		return numberOfMembersSeekingCoverage;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrescreenHousehold [householdMembers=");
		builder.append(householdMembers);
		builder.append("]");
		return builder.toString();
	}
}
