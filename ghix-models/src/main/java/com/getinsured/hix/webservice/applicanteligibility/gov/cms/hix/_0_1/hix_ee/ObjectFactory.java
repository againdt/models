//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.ElectronicNoticeCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.InsurancePolicyStatusCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2.ReferenceType;



/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.hix._0_1.hix_ee package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InsurancePolicyStatus_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePolicyStatus");
    private final static QName _InsuranceMemberIdentification_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsuranceMemberIdentification");
    private final static QName _InsurancePlan_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePlan");
    private final static QName _EligibilityIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "EligibilityIndicator");
    private final static QName _SSFPrimaryContactElectronicNoticePreferenceCode_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "SSFPrimaryContactElectronicNoticePreferenceCode");
    private final static QName _InsurancePolicyEffectiveDate_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePolicyEffectiveDate");
    private final static QName _InsuranceApplicant_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsuranceApplicant");
    private final static QName _InsurancePremiumFrequency_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePremiumFrequency");
    private final static QName _InsurancePolicyIdentification_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePolicyIdentification");
    private final static QName _InsurancePolicyStatusCode_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePolicyStatusCode");
    private final static QName _InsuranceMember_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsuranceMember");
    private final static QName _InsuranceApplicantStudentIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsuranceApplicantStudentIndicator");
    private final static QName _SSFPrimaryContactPaperNoticesIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "SSFPrimaryContactPaperNoticesIndicator");
    private final static QName _InsuranceApplicationPrimaryContactReference_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsuranceApplicationPrimaryContactReference");
    private final static QName _SSFPrimaryContactElectronicNoticeIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "SSFPrimaryContactElectronicNoticeIndicator");
    private final static QName _InsurancePremium_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePremium");
    private final static QName _InsuranceApplicantFinancialAssistanceIndicator_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsuranceApplicantFinancialAssistanceIndicator");
    private final static QName _EligibilityDateRange_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "EligibilityDateRange");
    private final static QName _InsurancePolicyExpirationDate_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePolicyExpirationDate");
    private final static QName _InsurancePremiumAmount_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "InsurancePremiumAmount");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.hix._0_1.hix_ee
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsurancePolicyStatusType }
     * 
     */
    public InsurancePolicyStatusType createInsurancePolicyStatusType() {
        return new InsurancePolicyStatusType();
    }

    /**
     * Create an instance of {@link SSFPrimaryContactType }
     * 
     */
    public SSFPrimaryContactType createSSFPrimaryContactType() {
        return new SSFPrimaryContactType();
    }

    /**
     * Create an instance of {@link InsuranceApplicantType }
     * 
     */
    public InsuranceApplicantType createInsuranceApplicantType() {
        return new InsuranceApplicantType();
    }

    /**
     * Create an instance of {@link InsurancePremiumType }
     * 
     */
    public InsurancePremiumType createInsurancePremiumType() {
        return new InsurancePremiumType();
    }

    /**
     * Create an instance of {@link InsuranceMemberType }
     * 
     */
    public InsuranceMemberType createInsuranceMemberType() {
        return new InsuranceMemberType();
    }

    /**
     * Create an instance of {@link DisenrollmentActivityType }
     * 
     */
    public DisenrollmentActivityType createDisenrollmentActivityType() {
        return new DisenrollmentActivityType();
    }

    /**
     * Create an instance of {@link InsurancePolicyType }
     * 
     */
    public InsurancePolicyType createInsurancePolicyType() {
        return new InsurancePolicyType();
    }

    /**
     * Create an instance of {@link EligibilityType }
     * 
     */
    public EligibilityType createEligibilityType() {
        return new EligibilityType();
    }

    /**
     * Create an instance of {@link InsuranceApplicationType }
     * 
     */
    public InsuranceApplicationType createInsuranceApplicationType() {
        return new InsuranceApplicationType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsurancePolicyStatusType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePolicyStatus")
    public JAXBElement<InsurancePolicyStatusType> createInsurancePolicyStatus(InsurancePolicyStatusType value) {
        return new JAXBElement<InsurancePolicyStatusType>(_InsurancePolicyStatus_QNAME, InsurancePolicyStatusType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsuranceMemberIdentification")
    public JAXBElement<IdentificationType> createInsuranceMemberIdentification(IdentificationType value) {
        return new JAXBElement<IdentificationType>(_InsuranceMemberIdentification_QNAME, IdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsurancePlanType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePlan")
    public JAXBElement<InsurancePlanType> createInsurancePlan(InsurancePlanType value) {
        return new JAXBElement<InsurancePlanType>(_InsurancePlan_QNAME, InsurancePlanType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "EligibilityIndicator")
    public JAXBElement<Boolean> createEligibilityIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_EligibilityIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElectronicNoticeCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "SSFPrimaryContactElectronicNoticePreferenceCode")
    public JAXBElement<ElectronicNoticeCodeType> createSSFPrimaryContactElectronicNoticePreferenceCode(ElectronicNoticeCodeType value) {
        return new JAXBElement<ElectronicNoticeCodeType>(_SSFPrimaryContactElectronicNoticePreferenceCode_QNAME, ElectronicNoticeCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePolicyEffectiveDate")
    public JAXBElement<DateType> createInsurancePolicyEffectiveDate(DateType value) {
        return new JAXBElement<DateType>(_InsurancePolicyEffectiveDate_QNAME, DateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsuranceApplicantType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsuranceApplicant")
    public JAXBElement<InsuranceApplicantType> createInsuranceApplicant(InsuranceApplicantType value) {
        return new JAXBElement<InsuranceApplicantType>(_InsuranceApplicant_QNAME, InsuranceApplicantType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FrequencyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePremiumFrequency")
    public JAXBElement<FrequencyType> createInsurancePremiumFrequency(FrequencyType value) {
        return new JAXBElement<FrequencyType>(_InsurancePremiumFrequency_QNAME, FrequencyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePolicyIdentification")
    public JAXBElement<IdentificationType> createInsurancePolicyIdentification(IdentificationType value) {
        return new JAXBElement<IdentificationType>(_InsurancePolicyIdentification_QNAME, IdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsurancePolicyStatusCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePolicyStatusCode")
    public JAXBElement<InsurancePolicyStatusCodeType> createInsurancePolicyStatusCode(InsurancePolicyStatusCodeType value) {
        return new JAXBElement<InsurancePolicyStatusCodeType>(_InsurancePolicyStatusCode_QNAME, InsurancePolicyStatusCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsuranceMemberType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsuranceMember")
    public JAXBElement<InsuranceMemberType> createInsuranceMember(InsuranceMemberType value) {
        return new JAXBElement<InsuranceMemberType>(_InsuranceMember_QNAME, InsuranceMemberType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsuranceApplicantStudentIndicator")
    public JAXBElement<Boolean> createInsuranceApplicantStudentIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_InsuranceApplicantStudentIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "SSFPrimaryContactPaperNoticesIndicator")
    public JAXBElement<Boolean> createSSFPrimaryContactPaperNoticesIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SSFPrimaryContactPaperNoticesIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsuranceApplicationPrimaryContactReference")
    public JAXBElement<ReferenceType> createInsuranceApplicationPrimaryContactReference(ReferenceType value) {
        return new JAXBElement<ReferenceType>(_InsuranceApplicationPrimaryContactReference_QNAME, ReferenceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "SSFPrimaryContactElectronicNoticeIndicator")
    public JAXBElement<Boolean> createSSFPrimaryContactElectronicNoticeIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SSFPrimaryContactElectronicNoticeIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsurancePremiumType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePremium")
    public JAXBElement<InsurancePremiumType> createInsurancePremium(InsurancePremiumType value) {
        return new JAXBElement<InsurancePremiumType>(_InsurancePremium_QNAME, InsurancePremiumType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsuranceApplicantFinancialAssistanceIndicator")
    public JAXBElement<Boolean> createInsuranceApplicantFinancialAssistanceIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_InsuranceApplicantFinancialAssistanceIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateRangeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "EligibilityDateRange")
    public JAXBElement<DateRangeType> createEligibilityDateRange(DateRangeType value) {
        return new JAXBElement<DateRangeType>(_EligibilityDateRange_QNAME, DateRangeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePolicyExpirationDate")
    public JAXBElement<DateType> createInsurancePolicyExpirationDate(DateType value) {
        return new JAXBElement<DateType>(_InsurancePolicyExpirationDate_QNAME, DateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "InsurancePremiumAmount")
    public JAXBElement<AmountType> createInsurancePremiumAmount(AmountType value) {
        return new JAXBElement<AmountType>(_InsurancePremiumAmount_QNAME, AmountType.class, null, value);
    }

}
