package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.EnumMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * -----------------------------------------------------------------------------
 * HIX-106085 Load consolidated provider data file.
 * -----------------------------------------------------------------------------
 * 
 * This is POJO class for PROVIDER_UPLOAD database table.
 * 
 * @since February 19, 2018
 */
@Entity
@Table(name = "PROVIDER_UPLOAD")
public class ProviderUpload implements Serializable {

	private static final long serialVersionUID = 1L;

	// Possible values: P-Provider / F-Facility
	public static enum PROVIDER_TYPE_INDICATOR {
		P, F;
	}

	public static enum FILE_TYPE {
		TEXT, CSV, EXCEL;
	}

	// Possible values: WAITING / IN-PROGRESS / CANCEL / FAILED / COMPLETED
	public static enum STATUS {
		WAITING, IN_PROGRESS, CANCEL, FAILED, COMPLETED;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROVIDER_UPLOAD_SEQ")
	@SequenceGenerator(name = "PROVIDER_UPLOAD_SEQ", sequenceName = "PROVIDER_UPLOAD_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "APPLICABLE_YEAR")
	private Integer applicableYear;

	// Possible values: P-Physician / D-Dentist / H-Hospital
	@Column(name = "PROVIDER_TYPE_INDICATOR")
	private String providerTypeIndicator;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "FILE_TYPE")
	private String fileType;

	// Possible values: WAITING / IN-PROGRESS / CANCEL / FAILED / COMPLETED
	@Column(name = "STATUS")
	private String status;

	@Column(name = "SOURCE_FILE_ECM_ID")
	private String sourceFileEcmId;

	@Column(name = "LOGS")
	private String logs;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public ProviderUpload() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getProviderTypeIndicator() {
		return providerTypeIndicator;
	}

	public void setProviderTypeIndicator(String providerTypeIndicator) {
		this.providerTypeIndicator = providerTypeIndicator;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSourceFileEcmId() {
		return sourceFileEcmId;
	}

	public void setSourceFileEcmId(String sourceFileEcmId) {
		this.sourceFileEcmId = sourceFileEcmId;
	}

	public String getLogs() {
		return logs;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public static Map<PROVIDER_TYPE_INDICATOR, String> getProviderTypeIndicatorOptionsList() {

		Map<PROVIDER_TYPE_INDICATOR, String> enumMap = new EnumMap<PROVIDER_TYPE_INDICATOR, String>(PROVIDER_TYPE_INDICATOR.class);
		enumMap.put(PROVIDER_TYPE_INDICATOR.P, "Provider");
		enumMap.put(PROVIDER_TYPE_INDICATOR.F, "Facility");
		return enumMap;
	}

	public static Map<STATUS, String> getStatusOptionsList() {

		Map<STATUS, String> enumMap = new EnumMap<STATUS, String>(STATUS.class);
		enumMap.put(STATUS.WAITING, "Waiting");
		enumMap.put(STATUS.IN_PROGRESS, "In Progress");
		enumMap.put(STATUS.CANCEL, "Cancel");
		enumMap.put(STATUS.FAILED, "Failed");
		enumMap.put(STATUS.COMPLETED, "Completed");
		return enumMap;
	}
}
