UPDATE ENROLLEE EE 
SET EE.EFFECTIVE_END_DATE = (SELECT EN.BENEFIT_END_DATE FROM ENROLLMENT EN 
                              WHERE EN.ID=EE.ENROLLMENT_ID 
                              AND EN.ENROLLMENT_TYPE_LKP IN (SELECT LOOKUP_VALUE_ID 
                                                             FROM LOOKUP_VALUE 
                                                             WHERE LOOKUP_VALUE_CODE IN ('24')) 
                              AND EN.ENROLLMENT_STATUS_LKP IN (SELECT LOOKUP_VALUE_ID 
                                                               FROM LOOKUP_VALUE 
                                                               WHERE LOOKUP_VALUE_CODE NOT IN ('ABORTED')))
                                                               
WHERE EE.EFFECTIVE_END_DATE IS NULL;