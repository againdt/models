package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ENRL_IRS_MONTHLY_IN_DTL")
public class EnrollmentIRSMonthlyInDtl implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	public static enum FIELD_NAME{
		SSN, EIN;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_IRS_MONTHLY_IN_DTL_SEQ")
	@SequenceGenerator(name = "ENRL_IRS_MONTHLY_IN_DTL_SEQ", sequenceName = "ENRL_IRS_MONTHLY_IN_DTL_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "ENRL_IRS_MONTHLY_IN_ID")
	private EnrollmentIRSMonthlyIn enrollmentMonthlyIn;
	
/*	@Column(name = "ENROLLMENT_ID")
	private Integer enrollmentId;*/
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private Integer householdCaseId;
	
	@Column(name = "ERROR_CODE")
	private String errorCode;
	
	@Column(name = "ERROR_MESSAGE")
	private String errorMessage;
	
	@Column(name = "ERR_FIELD_NAME")
	private String errorFieldName;
	
	@Column(name = "XPATH_CONTENT")
	private String xpathContent;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the errorFieldName
	 */
	public String getErrorFieldName() {
		return errorFieldName;
	}

	/**
	 * @param errorFieldName the errorFieldName to set
	 */
	public void setErrorFieldName(String errorFieldName) {
		this.errorFieldName = errorFieldName;
	}

	/**
	 * @return the enrollmentMonthlyIn
	 */
	public EnrollmentIRSMonthlyIn getEnrollmentMonthlyIn() {
		return enrollmentMonthlyIn;
	}

	/**
	 * @param enrollmentMonthlyIn the enrollmentMonthlyIn to set
	 */
	public void setEnrollmentMonthlyIn(EnrollmentIRSMonthlyIn enrollmentMonthlyIn) {
		this.enrollmentMonthlyIn = enrollmentMonthlyIn;
	}

	/**
	 * @return the householdCaseId
	 */
	public Integer getHouseholdCaseId() {
		return householdCaseId;
	}

	/**
	 * @param householdCaseId the householdCaseId to set
	 */
	public void setHouseholdCaseId(Integer householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	/**
	 * @return the xpathContent
	 */
	public String getXpathContent() {
		return xpathContent;
	}

	/**
	 * @param xpathContent the xpathContent to set
	 */
	public void setXpathContent(String xpathContent) {
		this.xpathContent = xpathContent;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
