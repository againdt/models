package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="RECOMMENDED_PLANS")
public class RecommendedPlans {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECOMMENDED_PLANS_SEQ")
	@SequenceGenerator(name = "RECOMMENDED_PLANS_SEQ", sequenceName = "RECOMMENDED_PLANS_SEQ", allocationSize = 1)
	private Integer id;
		
	@Column(name="hashed_lead_id")
	private String hashedLeadId;
	
	@Column(name = "lead_id")
	private Integer leadId;
	
	@Column(name = "ssap_application_id")
	private Integer ssapApplicationId;
	
	@Column(name="plan_request")
	private String planRequest;
	
	@Column(name="plan_similar")
	private String planSimilar;
	
	@Column(name="plan_lowest_cost")
	private String planLowestCost;
	
	@Column(name="plan_highest_gps")
	private String planHighestGps;
	
	@Column(name="current_plan_id")
	private Integer currentPlanId;
	
	@Column(name="issuer_plan_number")
	private String issuerPlanNumber;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;
	
	@Column(name = "created_by")
    private Integer createdBy;

	@Column(name = "last_updated_by")
    private Integer lastUpdatedBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHashedLeadId() {
		return hashedLeadId;
	}

	public void setHashedLeadId(String hashedLeadId) {
		this.hashedLeadId = hashedLeadId;
	}

	public Integer getLeadId() {
		return leadId;
	}

	public void setLeadId(Integer leadId) {
		this.leadId = leadId;
	}

	public Integer getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Integer ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getPlanRequest() {
		return planRequest;
	}

	public void setPlanRequest(String planRequest) {
		this.planRequest = planRequest;
	}

	public String getPlanSimilar() {
		return planSimilar;
	}

	public void setPlanSimilar(String planSimilar) {
		this.planSimilar = planSimilar;
	}

	public String getPlanLowestCost() {
		return planLowestCost;
	}

	public void setPlanLowestCost(String planLowestCost) {
		this.planLowestCost = planLowestCost;
	}

	public String getPlanHighestGps() {
		return planHighestGps;
	}

	public void setPlanHighestGps(String planHighestGps) {
		this.planHighestGps = planHighestGps;
	}

	public Integer getCurrentPlanId() {
		return currentPlanId;
	}

	public void setCurrentPlanId(Integer currentPlanId) {
		this.currentPlanId = currentPlanId;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setUpdated(new TSDate()); 
	}
}
