package com.getinsured.hix.model.agency;

import java.io.Serializable;

public class DesignateBrokerAudId implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	
	private long rev;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getRev() {
		return rev;
	}

	public void setRev(long rev) {
		this.rev = rev;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (rev ^ (rev >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DesignateBrokerAudId other = (DesignateBrokerAudId) obj;
		if (id != other.id)
			return false;
		if (rev != other.rev)
			return false;
		return true;
	}
}
