package com.getinsured.hix.dto.offexchange;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

/**
 * 
 * @author rajaramesh_g
 *
 */
public class OffExchangeResponse extends GHIXResponse implements Serializable {

	private static final long serialVersionUID = 1L;
			
}
