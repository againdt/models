package com.getinsured.hix.dto.finance;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "payments")
public class Finance
{

    @XmlElement(name="HIOS_Issuer_ID", required = true)
    private String hiosIssuerID;
    @XmlElement(name="txnCreateDateTime", required = true)
    private String txnCreateDateTime;
    @XmlElement(name="payment", required = true)
    private Finance.Payment payment;

    /**
     * Gets the value of the hiosIssuerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHIOSIssuerID() {
        return hiosIssuerID;
    }

    /**
     * Sets the value of the hiosIssuerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHIOSIssuerID(String value) {
        this.hiosIssuerID = value;
    }

    /**
     * Gets the value of the txnCreateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxnCreateDateTime() {
        return txnCreateDateTime;
    }

    /**
     * Sets the value of the txnCreateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxnCreateDateTime(String value) {
        this.txnCreateDateTime = value;
    }

    /**
     * Gets the value of the payment property.
     * 
     * @return
     *     possible object is
     *     {@link Finance.Payment }
     *     
     */
    public Finance.Payment getPayment() {
        return payment;
    }

    /**
     * Sets the value of the payment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Finance.Payment }
     *     
     */
    public void setPayment(Finance.Payment value) {
        this.payment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="totalPymtAmt" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="pymtMethodCodeLkp">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="lookupValueCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ABAtransitRoutingNumDFI" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SWIFTidDFI" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CanadianBankBranchInstitutionNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="receivingDFIid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="demandDepositAcctNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="savingsAcctNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="creditCardAcctNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="receiverBankAcctNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="checkIssueOrEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="checkOrEFTtraceNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="exchangeAssignedQHPid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="issuerAssignedQHPid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="exchgAssignedEmployerGrpId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="issuerAssignedEmployerGroupOrPolicyNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payeeOrganizationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payeeFederalTIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payeeCMSPlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payerOriginatingCompanyNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payerAdminContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="payerAdminContactInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="payerPhoneNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="payerPhoneNumExtension" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="payerFaxNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="payerEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="remittanceInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="indivLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivMiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivNameSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivExchgAssignedSubscriberId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivExchgAssignedQHPid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivIssuerAssignedQHPid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivExchgAssignedEmployerGrpId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivIssuerAssignedEmployerGrpOrPolicyNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivExchgAssignedPolicyNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivIssuerAssignedHealthInsurPolicyNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivExchgAssignedAPTCcontributorTaxPayerPIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivIssuerAssignedAPTCcontributorClientNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivExchgAssignedDependentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivIssuerAssignedDependentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indivIssuerAssignedSubscriberNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="remittanceDetail">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="exchangePymtType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="pymtAmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="exchgReportDocCtlNum">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="exchgRptType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="exchgRptDocCtlNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="indivCoveragePeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}byte" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}byte" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.NONE)
    public static class Payment {
    	
        @XmlElement(name="checkIssueOrEffectiveDate", required = true)
        private String checkIssueOrEffectiveDate;
        @XmlElement(name="checkOrEFTtraceNum", required = true)
        private String checkOrEFTtraceNum;
        @XmlElement(name="payeeOrganizationName", required = true)
        private String payeeOrganizationName;
        @XmlElement(name="payeeFederalTIN", required = true)
        private String payeeFederalTIN;
        @XmlElement(name="payerName", required = true)
        private String payerName;
        @XmlElement(name="payerOriginatingCompanyNum", required = true)
        private String payerOriginatingCompanyNum;
        @XmlElement(name="remittanceInfo", required = true)
        private List<RemittanceInfo> remittanceInfo;
        @XmlElement(name="createdOn", required = true)
        protected String createdOn;
        @XmlAttribute
        private Byte id;

        /**
         * Gets the value of the checkIssueOrEffectiveDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckIssueOrEffectiveDate() {
            return checkIssueOrEffectiveDate;
        }

        /**
         * Sets the value of the checkIssueOrEffectiveDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckIssueOrEffectiveDate(String value) {
            this.checkIssueOrEffectiveDate = value;
        }

        /**
         * Gets the value of the checkOrEFTtraceNum property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckOrEFTtraceNum() {
            return checkOrEFTtraceNum;
        }

        /**
         * Sets the value of the checkOrEFTtraceNum property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckOrEFTtraceNum(String value) {
            this.checkOrEFTtraceNum = value;
        }

        /**
         * Gets the value of the payeeOrganizationName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayeeOrganizationName() {
            return payeeOrganizationName;
        }

        /**
         * Sets the value of the payeeOrganizationName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayeeOrganizationName(String value) {
            this.payeeOrganizationName = value;
        }

        /**
         * Gets the value of the payeeFederalTIN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayeeFederalTIN() {
            return payeeFederalTIN;
        }

        /**
         * Sets the value of the payeeFederalTIN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayeeFederalTIN(String value) {
            this.payeeFederalTIN = value;
        }

        /**
         * Gets the value of the payerName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayerName() {
            return payerName;
        }

        /**
         * Sets the value of the payerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayerName(String value) {
            this.payerName = value;
        }

        /**
         * Gets the value of the payerOriginatingCompanyNum property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayerOriginatingCompanyNum() {
            return payerOriginatingCompanyNum;
        }

        /**
         * Sets the value of the payerOriginatingCompanyNum property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayerOriginatingCompanyNum(String value) {
            this.payerOriginatingCompanyNum = value;
        }

        /**
         * 
         * @return
         */
        public List<RemittanceInfo> getRemittanceInfo() {
            return remittanceInfo;
        }

        /**
         * Sets the value of the remittanceInfo property.
         * 
         * @param value
         *     allowed list object is
         *     {RemittanceInfo }
         *     
         */
        public void setRemittanceInfo(List<RemittanceInfo> remittanceList) {
            this.remittanceInfo = remittanceList;
        }
        
        /**
         * Gets the value of the createdOn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreatedOn() {
            return createdOn;
        }

        /**
         * Sets the value of the createdOn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreatedOn(String value) {
            this.createdOn = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link Byte }
         *     
         */
        public Byte getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link Byte }
         *     
         */
        public void setId(Byte value) {
            this.id = value;
        }
    }
    
    @XmlAccessorType(XmlAccessType.NONE)
    public static class RemittanceInfo {

        @XmlElement(name = "indivLastName", required = true)
        private String indivLastName;
        @XmlElement(name = "indivFirstName", required = true)
        private String indivFirstName;
        @XmlElement(name="indivMiddleName", required = true)
        private String indivMiddleName;
        @XmlElement(name="indivNameSuffix", required = true)
        private String indivNameSuffix;
        @XmlElement(name="indivExchgAssignedSubscriberId", required = true)
        private String indivExchgAssignedSubscriberId;
        @XmlElement(name="indivExchgAssignedQHPid", required = true)
        private String indivExchgAssignedQHPid;
        @XmlElement(name="indivExchgAssignedEmployerGrpId", required = true)
        private String indivExchgAssignedEmployerGrpId;
        @XmlElement(name="indivExchgAssignedPolicyNum", required = true)
        private String indivExchgAssignedPolicyNum;
        @XmlElement(name="indivIssuerAssignedHealthInsurPolicyNum", required = true)
        private String indivIssuerAssignedHealthInsurPolicyNum;
        @XmlElement(name="indivIssuerAssignedSubscriberNum", required = true)
        private String indivIssuerAssignedSubscriberNum;
        @XmlElement(name="createdOn", required = true)
        private String createdOn;
        
        @XmlElement(name="remittanceDetail", required = true)
        private RemittanceInfo.RemittanceDetail remittanceDetail;
        
        
        @XmlAttribute
        private Byte id;

        /**
         * Gets the value of the indivLastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivLastName() {
            return indivLastName;
        }

        /**
         * Sets the value of the indivLastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivLastName(String value) {
            this.indivLastName = value;
        }

        /**
         * Gets the value of the indivFirstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivFirstName() {
            return indivFirstName;
        }

        /**
         * Sets the value of the indivFirstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivFirstName(String value) {
            this.indivFirstName = value;
        }

        /**
         * Gets the value of the indivMiddleName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivMiddleName() {
            return indivMiddleName;
        }

        /**
         * Sets the value of the indivMiddleName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivMiddleName(String value) {
            this.indivMiddleName = value;
        }

        /**
         * Gets the value of the indivNameSuffix property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivNameSuffix() {
            return indivNameSuffix;
        }

        /**
         * Sets the value of the indivNameSuffix property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivNameSuffix(String value) {
            this.indivNameSuffix = value;
        }

        /**
         * Gets the value of the indivExchgAssignedSubscriberId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivExchgAssignedSubscriberId() {
            return indivExchgAssignedSubscriberId;
        }

        /**
         * Sets the value of the indivExchgAssignedSubscriberId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivExchgAssignedSubscriberId(String value) {
            this.indivExchgAssignedSubscriberId = value;
        }

        /**
         * Gets the value of the indivExchgAssignedQHPid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivExchgAssignedQHPid() {
            return indivExchgAssignedQHPid;
        }

        /**
         * Sets the value of the indivExchgAssignedQHPid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivExchgAssignedQHPid(String value) {
            this.indivExchgAssignedQHPid = value;
        }

        
        /**
         * Gets the value of the indivExchgAssignedEmployerGrpId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivExchgAssignedEmployerGrpId() {
            return indivExchgAssignedEmployerGrpId;
        }

        /**
         * Sets the value of the indivExchgAssignedEmployerGrpId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivExchgAssignedEmployerGrpId(String value) {
            this.indivExchgAssignedEmployerGrpId = value;
        }


        /**
         * Gets the value of the indivExchgAssignedPolicyNum property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivExchgAssignedPolicyNum() {
            return indivExchgAssignedPolicyNum;
        }

        /**
         * Sets the value of the indivExchgAssignedPolicyNum property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivExchgAssignedPolicyNum(String value) {
            this.indivExchgAssignedPolicyNum = value;
        }

        /**
         * Gets the value of the indivIssuerAssignedHealthInsurPolicyNum property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivIssuerAssignedHealthInsurPolicyNum() {
            return indivIssuerAssignedHealthInsurPolicyNum;
        }

        /**
         * Sets the value of the indivIssuerAssignedHealthInsurPolicyNum property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivIssuerAssignedHealthInsurPolicyNum(String value) {
            this.indivIssuerAssignedHealthInsurPolicyNum = value;
        }

        /**
         * Gets the value of the indivIssuerAssignedSubscriberNum property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndivIssuerAssignedSubscriberNum() {
            return indivIssuerAssignedSubscriberNum;
        }

        /**
         * Sets the value of the indivIssuerAssignedSubscriberNum property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndivIssuerAssignedSubscriberNum(String value) {
            this.indivIssuerAssignedSubscriberNum = value;
        }

        /**
         * Gets the value of the remittanceDetail property.
         * 
         * @return
         *     possible object is
         *     {@link Finance.Payment.RemittanceInfo.RemittanceDetail }
         *     
         */
        public RemittanceInfo.RemittanceDetail getRemittanceDetail() {
            return remittanceDetail;
        }

        /**
         * Sets the value of the remittanceDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link Finance.Payment.RemittanceInfo.RemittanceDetail }
         *     
         */
        public void setRemittanceDetail(RemittanceInfo.RemittanceDetail value) {
            this.remittanceDetail = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link Byte }
         *     
         */
        public Byte getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link Byte }
         *     
         */
        public void setId(Byte value) {
            this.id = value;
        }

        public String getCreatedOn() {
    		return createdOn;
    	}

    	public void setCreatedOn(String createdOn) {
    		this.createdOn = createdOn;
    	}
    	
        @XmlAccessorType(XmlAccessType.NONE)
        public static class RemittanceDetail {
        	@XmlElement(name="exchangePymtType", required = true)
            private String exchangePymtType;
        	@XmlElement(name="pymtAmt", required = true)
            private String pymtAmt;
        	@XmlElement(name="indivCoveragePeriod", required = true)
            private String indivCoveragePeriod;
        	@XmlElement(name="createdOn", required = true)
            private String createdOn;

            /**
             * Gets the value of the exchangePymtType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExchangePymtType() {
                return exchangePymtType;
            }

            /**
             * Sets the value of the exchangePymtType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExchangePymtType(String value) {
                this.exchangePymtType = value;
            }

            /**
             * Gets the value of the pymtAmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPymtAmt() {
                return pymtAmt;
            }

            /**
             * Sets the value of the pymtAmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPymtAmt(String value) {
                this.pymtAmt = value;
            }

            /**
             * Gets the value of the indivCoveragePeriod property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIndivCoveragePeriod() {
                return indivCoveragePeriod;
            }

            /**
             * Sets the value of the indivCoveragePeriod property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIndivCoveragePeriod(String value) {
                this.indivCoveragePeriod = value;
            }

			
            public String getCreatedOn() {
				return createdOn;
			}

			public void setCreatedOn(String createdOn) {
				this.createdOn = createdOn;
			}

        }

    }

}
