/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @author negi_s
 *
 */
public class EnrollmentSftpFileTransferDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2965764351844606411L;
	private String fileName;
	private String fileSize;
	private String transferType;
	private String transferStatus;
	private String filePermissions;
	private String errorCode;
	private String errorMsg;
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the fileSize
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @return the transferType
	 */
	public String getTransferType() {
		return transferType;
	}
	/**
	 * @param transferType the transferType to set
	 */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	/**
	 * @return the transferStatus
	 */
	public String getTransferStatus() {
		return transferStatus;
	}
	/**
	 * @param transferStatus the transferStatus to set
	 */
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}
	/**
	 * @return the filePermissions
	 */
	public String getFilePermissions() {
		return filePermissions;
	}
	/**
	 * @param filePermissions the filePermissions to set
	 */
	public void setFilePermissions(String filePermissions) {
		this.filePermissions = filePermissions;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SftpFileTransferDto [fileName=" + fileName + ", fileSize=" + fileSize + ", transferType=" + transferType
				+ ", transferStatus=" + transferStatus + ", filePermissions=" + filePermissions + ", errorCode="
				+ errorCode + ", errorMsg=" + errorMsg + "]";
	}
}
