package com.getinsured.hix.dto.planmgmt;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoodRxAPIResponseData {
	 @JsonProperty("mobile_url")
	 private String mobile_url;
	 
	 @JsonProperty("form")
	 private String form;

	 @JsonProperty("url")
	 private String url;

	 @JsonProperty("brand")
	 private String[] brand;

	 @JsonProperty("dosage")
	 private String dosage;
	 
	 @JsonProperty("price")
	 private Double price;

	 @JsonProperty("generic")
	 private String[] generic;

	 @JsonProperty("quantity")
	 private Integer quantity;

	 @JsonProperty("display")
	 private String display;

	 @JsonProperty("manufacturer")
	 private String manufacturer;

	/**
	 * @return the mobile_url
	 */
	public String getMobile_url() {
		return mobile_url;
	}

	/**
	 * @param mobile_url the mobile_url to set
	 */
	public void setMobile_url(String mobile_url) {
		this.mobile_url = mobile_url;
	}

	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}

	/**
	 * @param form the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the brand
	 */
	public String[] getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String[] brand) {
		this.brand = brand;
	}

	/**
	 * @return the dosage
	 */
	public String getDosage() {
		return dosage;
	}

	/**
	 * @param dosage the dosage to set
	 */
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the generic
	 */
	public String[] getGeneric() {
		return generic;
	}

	/**
	 * @param generic the generic to set
	 */
	public void setGeneric(String[] generic) {
		this.generic = generic;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the display
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * @param manufacturer the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

}
