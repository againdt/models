package com.getinsured.hix.dto.agency;

import java.io.Serializable;
import java.util.List;

public class AgencyDelegationListDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long count;
	private List<AgencyDelegationDTO> delegations;

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<AgencyDelegationDTO> getDelegations() {
		return delegations;
	}

	public void setDelegations(List<AgencyDelegationDTO> delegations) {
		this.delegations = delegations;
	}
}
