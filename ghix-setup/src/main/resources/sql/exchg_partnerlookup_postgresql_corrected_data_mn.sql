--truncate exchg related tables

TRUNCATE EXCHG_DNU;

TRUNCATE EXCHG_DNU_ERROR;

TRUNCATE EXCHG_DNU_RPT_IN;

TRUNCATE EXCHG_DNU_RPT_OUT;

TRUNCATE EXCHG_GS_RECON;

TRUNCATE EXCHG_ISA_RECON;

TRUNCATE EXCHG_TA1_RECON;

TRUNCATE EXCHG_999_RECON;

TRUNCATE EXCHG_PARTNERLOOKUP;

TRUNCATE EXCHG_CONTROLNUM;

-- hios_issuer_id = 85736
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound','ZZ','MN0','ZZ','363573805','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','363573805','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','363573805','ZZ','MN0','T','363573805','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','363573805','ZZ','MN0','T','363573805','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound','ZZ','MN0','ZZ','363573805','T','MN0','363573805','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Outbound','ZZ','MN0','ZZ','363573805','T','MN0','363573805','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'UCare Minnesota','85736','FI','Inbound','ZZ','363573805','ZZ','MN0','T','363573805','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 34102
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound','ZZ','MN0','ZZ','410797853','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Outbound','ZZ','MN0','ZZ','410797853','T','MN0','410797853','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'HealthPartners','34102','FI','Inbound','ZZ','410797853','ZZ','MN0','T','410797853','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;
-- hios_issuer_id = 63485
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound','ZZ','MN0','ZZ','751233841','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','751233841','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','751233841','ZZ','MN0','T','751233841','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','751233841','ZZ','MN0','T','751233841','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound','ZZ','MN0','ZZ','751233841','T','MN0','751233841','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Outbound','ZZ','MN0','ZZ','751233841','T','MN0','751233841','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Dentegra Insurance Company','63485','FI','Inbound','ZZ','751233841','ZZ','MN0','T','751233841','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 57129
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound','ZZ','MN0','ZZ','416173747','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','416173747','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','416173747','ZZ','MN0','T','416173747','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','416173747','ZZ','MN0','T','416173747','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound','ZZ','MN0','ZZ','416173747','T','MN0','416173747','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Outbound','ZZ','MN0','ZZ','416173747','T','MN0','416173747','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Blue Plus','57129','FI','Inbound','ZZ','416173747','ZZ','MN0','T','416173747','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 31616
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound','ZZ','MN0','ZZ','411490988','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','411490988','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','411490988','ZZ','MN0','T','411490988','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','411490988','ZZ','MN0','T','411490988','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound','ZZ','MN0','ZZ','411490988','T','MN0','411490988','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Outbound','ZZ','MN0','ZZ','411490988','T','MN0','411490988','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Medica','31616','FI','Inbound','ZZ','411490988','ZZ','MN0','T','411490988','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = 26825
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound','ZZ','MN0','ZZ','410952670','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromGHIX/TA1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,7,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410952670','ZZ','MN0','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,9,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,34,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'REC','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/enrollment/CARRIERRECON/Inbound','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,35,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410952670','ZZ','MN0','T','410952670','MN0','005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',13,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410952670','ZZ','MN0','T','410952670','MN0','005010X220A1','834','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/X220A1','/opt/tibco/data/inProcess','/opt/tibco/data/archive','834-X220A1_to_enrollmentRecord_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',3,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound','ZZ','MN0','ZZ','410952670','T','MN0','410952670','005010X231A1','999','/opt/tibco/data/fromGHIX/X231A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.20_MN.apf',11,'2019-03-12 16:50:37.609','2019-03-12 16:50:37.609',NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Outbound','ZZ','MN0','ZZ','410952670','T','MN0','410952670','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.20_MN.apf',1,current_timestamp,current_timestamp,NULL)
,(NEXTVAL('exchg_partnerlookup_seq'),'Delta','26825','FI','Inbound','ZZ','410952670','ZZ','MN0','T','410952670','MN0','005010X306','820','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive','820-X306_to_paymentRecord.map','PDSA5010HIX-820X306','HIX_X220X306_8.5.0.20_MN.apf',6,current_timestamp,current_timestamp,NULL)
;

-- hios_issuer_id = CMS
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound','ZZ','CMSFFM','ZZ','FEP0106ID','T',NULL,NULL,NULL,'TA1','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,17,current_timestamp,current_timestamp,6)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Outbound','ZZ','FEP0106ID','ZZ','CMSFFM','T',NULL,'ID0','005010X220A1','834','/opt/tibco/data/fromGHIX/X220A1','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive','enrollmentRecord_to_834-X220A1_multipleGS.map','PDSA5010HIX-834X220','HIX_X220X306_8.5.0.2_MN__CMS.apf',16,current_timestamp,current_timestamp,5)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound','ZZ','CMSFFM','ZZ','FEP0106ID','T','ID0',NULL,'005010X231A1','999','/opt/tibco/data/fromMFT',NULL,'/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,'PDSA5010-999X231','HIX_X220X306_8.5.0.2_MN__CMS.apf',18,current_timestamp,current_timestamp,6)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,19,current_timestamp,current_timestamp,1)
,(NEXTVAL('exchg_partnerlookup_seq'),'CMS','CMS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,20,current_timestamp,current_timestamp,3)
;

-- hios_issuer_id = IRS
INSERT INTO exchg_partnerlookup (id,insurername,hios_issuer_id,market,direction,isa05,isa06,isa07,isa08,isa15,gs02,gs03,gs08,st01,source_dir,target_dir,inprocess_dir,archive_dir,map_name,validation_standard,apf,role_id,creation_timestamp,last_update_timestamp,comm_id) VALUES 
 (NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Outbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/batch/ghixhome/ghix-docs/irs/IRSReportHubFTPFolder','/opt/tibco/data/toMFT','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,21,current_timestamp,current_timestamp,7)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/IRSMonthlyResponse','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,22,current_timestamp,current_timestamp,8)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/IRSMonthlyResponse','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,23,current_timestamp,current_timestamp,8)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/batch/ghixhome/ghix-docs/IRSMonthlyResponse','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,24,current_timestamp,current_timestamp,8)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,25,current_timestamp,current_timestamp,9)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,26,current_timestamp,current_timestamp,9)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,27,current_timestamp,current_timestamp,9)
,(NEXTVAL('exchg_partnerlookup_seq'),'IRS','IRS','FI','Inbound',NULL,NULL,NULL,NULL,'T',NULL,NULL,NULL,'PAS','/opt/tibco/data/fromMFT','/opt/tibco/data/toGHIX/PASSTHRU','/opt/tibco/data/inProcess','/opt/tibco/data/archive',NULL,NULL,NULL,28,current_timestamp,current_timestamp,9)
;

INSERT INTO EXCHG_CONTROLNUM (ID, HIOS_ISSUER_ID, CONTROL_NUM)  values
( nextval('exchg_controlnum_seq'), '34102', '1' ),
( nextval('exchg_controlnum_seq'), '63485', '1' ),
( nextval('exchg_controlnum_seq'), '57129', '1' ),
( nextval('exchg_controlnum_seq'), '31616', '1' ),
( nextval('exchg_controlnum_seq'), '26825', '1' ),
( nextval('exchg_controlnum_seq'), '85736', '1' ),
( nextval('exchg_controlnum_seq'), 'CMS', '1' ),
( nextval('exchg_controlnum_seq'), 'IRS', '1' );