package com.getinsured.hix.dto.directenrollment.zip;

import java.io.Serializable;
import java.util.List;

/**
 * represents zip code/county entity
 * 
 * @author root
 *
 */
public class DirectEnrollmentZipCodeDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2953209360345975228L;
	private String zipcode;
	private List<DirectEnrollmentCountyDTO> counties;

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public List<DirectEnrollmentCountyDTO> getCounties() {
		return counties;
	}

	public void setCounties(List<DirectEnrollmentCountyDTO> counties) {
		this.counties = counties;
	}

}
