package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;


public class EnrollmentCountDTO implements Serializable{
	private String planLevel;
	private Integer enrollmentCount;

	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	public Integer getEnrollmentCount() {
		return enrollmentCount;
	}
	public void setEnrollmentCount(Integer enrollmentCount) {
		this.enrollmentCount = enrollmentCount;
	}
}