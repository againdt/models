package com.getinsured.hix.dto.broker;

import java.io.Serializable;

public class BrokerParticipationDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String phoneNumber;
    private String status;
    private boolean available;
    private int responseCode;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
