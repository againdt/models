package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author santanu
 * @version 1.0
 * @since Apr 19, 2013 
 *
 */
public class PlanRequest implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String planId;
	private String zip;
	private String effectiveDate;
	private String countyFIPS;
	private String marketType; // possible values are SHOP/INDIVIDUAL
	private String issuerName;
	private String planLevel;	// metal tier like GOLD/BRONZE/SILVER/PLATINUM
	private boolean minimizePlanData=false;
	private String insuranceType; // possible values are HEALTH/DENTAL
	private String exchangeType; //For HIX-31611. Possible values are ON/OFF
	private String csrValue; 
	private List<Integer> planIds;
	private List<Map<String, List<String>>> providerList;
	private List<String> strenussIdList ; // HIX-57901 get list of strenuss Ids
	private String tenantCode;
	private String hiosPlanId; // 
	private String medicarePlanDataSource;
	private String commEffectiveDate;
	
	/**
	 * @return the planIds
	 */
	public List<Integer> getPlanIds() {
		return planIds;
	}

	/**
	 * @param planIds the planIds to set
	 */
	public void setPlanIds(List<Integer> planIds) {
		this.planIds = planIds;
	}

	/**
	 * Constructor.
	 */
	public PlanRequest() {
	}
	
	/**
	 * @return the planId
	 */
	public String getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(String planId) {
		this.planId = planId;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getCountyFIPS() {
		return countyFIPS;
	}

	public void setCountyFIPS(String countyFIPS) {
		this.countyFIPS = countyFIPS;
	}

	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
	
	public boolean isMinimizePlanData() {
		return minimizePlanData;
	}

	public void setMinimizePlanData(boolean minimizePlanData) {
		this.minimizePlanData = minimizePlanData;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getCsrValue() {
		return csrValue;
	}

	public void setCsrValue(String csrValue) {
		this.csrValue = csrValue;
	}

	public List<Map<String, List<String>>> getProviderList() {
		return providerList;
	}

	public void setProviderList(List<Map<String, List<String>>> providerList) {
		this.providerList = providerList;
	}

	public List<String> getStrenussIdList() {
		return strenussIdList;
	}

	public void setStrenussIdList(List<String> strenussIdList) {
		this.strenussIdList = strenussIdList;
	}

	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}

	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getHiosPlanId() {
		return hiosPlanId;
	}

	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}

	/**
	 * @return the medicarePlanDataSource
	 */
	public String getMedicarePlanDataSource() {
		return medicarePlanDataSource;
	}

	/**
	 * @param medicarePlanDataSource the medicarePlanDataSource to set
	 */
	public void setMedicarePlanDataSource(String medicarePlanDataSource) {
		this.medicarePlanDataSource = medicarePlanDataSource;
	}

	/**
	 * @return the commEffectiveDate
	 */
	public String getCommEffectiveDate() {
		return commEffectiveDate;
	}

	/**
	 * @param commEffectiveDate the commEffectiveDate to set
	 */
	public void setCommEffectiveDate(String commEffectiveDate) {
		this.commEffectiveDate = commEffectiveDate;
	}
}
