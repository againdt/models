package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.model.serff.SerffPlanMgmt.REQUEST_STATE;
import com.getinsured.hix.model.serff.SerffPlanMgmt.REQUEST_STATUS;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.BATCH_STATUS;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.FTP_STATUS;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.IS_DELETED;
import com.getinsured.hix.platform.util.DateUtil;

/**
 * DTO class is used to get data from SerffPlanMgmtBatch & SerffPlanMgmt tables for Plan Load Status page grid.
 * 
 * @author Bhavin Parmar
 * @since 03 March, 2017
 */
public class SerffBatchInfoDTO {

	public enum STATUS {
		IN_PROGRESS, WAITING, IN_QUEUE, SUCCESS, FAILURE;
	}

	private Long serffReqID;				// SerffPlanMgmt.serffReqId
	private String issuerName;				// SerffPlanMgmtBatch.career
	private String hiosIssuerID;			// SerffPlanMgmtBatch.issuerId
	private String ftpStartTime;			// SerffPlanMgmtBatch.ftpStartTime
	private String ftpEndTime;				// SerffPlanMgmtBatch.ftpEndTime
	private String batchStartTime;			// SerffPlanMgmtBatch.batchStartTime
	private String batchEndTime;			// SerffPlanMgmtBatch.batchEndTime
	private String ftpStatus;				// SerffPlanMgmtBatch.ftpStatus
	private String batchStatus;				// SerffPlanMgmtBatch.batchStatus
	private String state;					// SerffPlanMgmtBatch.state
	private String exchangeType;			// SerffPlanMgmtBatch.exchangeType
	private String defaultTenant;			// SerffPlanMgmtBatch.defaultTenant
	private String operationType;			// SerffPlanMgmtBatch.operationType
	private String isDeleted;				// SerffPlanMgmtBatch.isDeleted
	private String remarks;					// SerffPlanMgmt.remarks
	private String startTime;				// SerffPlanMgmt.startTime
	private String endTime;					// SerffPlanMgmt.endTime
	private String requestXml;				// SerffPlanMgmt.requestXml
	private String responseXml;				// SerffPlanMgmt.responseXml
	private String pmResponseXml;			// SerffPlanMgmt.pmResponseXml
	private String requestStatus;			// SerffPlanMgmt.requestStatus
	private String requestState;			// SerffPlanMgmt.requestState
	private String requestStateDesc;		// SerffPlanMgmt.requestStateDesc
//	private String serffTrackNum;			// SerffPlanMgmt.serffTrackNum
//	private String stateTrackNum;			// SerffPlanMgmt.stateTrackNum
//	private String planStats;				// SerffPlanMgmt.planStats

	public SerffBatchInfoDTO() {
	}

	public SerffBatchInfoDTO(Long serffReqID, String issuerName, String hiosIssuerID, Date ftpStartTime,
			Date ftpEndTime, Date batchStartTime, Date batchEndTime, FTP_STATUS ftpStatus, BATCH_STATUS batchStatus,
			String state, String exchangeType, String defaultTenant, String operationType, IS_DELETED isDeleted,
			String remarks, Date startTime, Date endTime, String requestXml, String responseXml, String pmResponseXml,
			REQUEST_STATUS requestStatus, REQUEST_STATE requestState, String requestStateDesc) {

		this.serffReqID = serffReqID;

		if (null != issuerName && null != hiosIssuerID) {
			this.issuerName = issuerName.replaceAll("\\[" + hiosIssuerID + "\\]", StringUtils.EMPTY).trim();
		}
		this.hiosIssuerID = hiosIssuerID;

		final String DATE_FORMAT = "MMM dd, yyyy";
		if (null != ftpStartTime) {
			this.ftpStartTime = DateUtil.dateToString(ftpStartTime, DATE_FORMAT);
		}
		if (null != ftpEndTime) {
			this.ftpEndTime = DateUtil.dateToString(ftpEndTime, DATE_FORMAT);
		}
		if (null != batchStartTime) {
			this.batchStartTime = DateUtil.dateToString(batchStartTime, DATE_FORMAT);
		}
		if (null != batchEndTime) {
			this.batchEndTime = DateUtil.dateToString(batchEndTime, DATE_FORMAT);
		}

		if (null != ftpStatus) {
			this.ftpStatus = ftpStatus.toString();
		}

		if (null != batchStatus) {
			this.batchStatus = batchStatus.toString();
		}
		this.state = state;
		this.exchangeType = exchangeType;
		this.defaultTenant = defaultTenant;
		this.operationType = operationType;

		if (null != isDeleted) {
			this.isDeleted = isDeleted.toString();
		}
		this.remarks = remarks;
		if (null != startTime) {
			this.startTime = DateUtil.dateToString(startTime, DATE_FORMAT);
		}
		if (null != endTime) {
			this.endTime = DateUtil.dateToString(endTime, DATE_FORMAT);
		}
		this.requestXml = requestXml;
		this.responseXml = responseXml;
		this.pmResponseXml = pmResponseXml;

		if (null != requestStatus) {
			this.requestStatus = requestStatus.toString();
		}

		if (null != requestState) {
			this.requestState = requestState.toString();
		}
		this.requestStateDesc = requestStateDesc;
	}

	public SerffBatchInfoDTO(Long serffReqID, String issuerName, String hiosIssuerID, Date ftpStartTime,
			Date ftpEndTime, Date batchStartTime, Date batchEndTime, FTP_STATUS ftpStatus, BATCH_STATUS batchStatus,
			String state, String exchangeType, String defaultTenant, String operationType, IS_DELETED isDeleted,
			String remarks, Date startTime, Date endTime, String responseXml, String pmResponseXml,
			REQUEST_STATUS requestStatus, REQUEST_STATE requestState, String requestStateDesc) {

		this.serffReqID = serffReqID;

		if (null != issuerName && null != hiosIssuerID) {
			this.issuerName = issuerName.replaceAll("\\[" + hiosIssuerID + "\\]", StringUtils.EMPTY).trim();
		}
		this.hiosIssuerID = hiosIssuerID;

		final String DATE_FORMAT = "MMM dd, yyyy";
		if (null != ftpStartTime) {
			this.ftpStartTime = DateUtil.dateToString(ftpStartTime, DATE_FORMAT);
		}
		if (null != ftpEndTime) {
			this.ftpEndTime = DateUtil.dateToString(ftpEndTime, DATE_FORMAT);
		}
		if (null != batchStartTime) {
			this.batchStartTime = DateUtil.dateToString(batchStartTime, DATE_FORMAT);
		}
		if (null != batchEndTime) {
			this.batchEndTime = DateUtil.dateToString(batchEndTime, DATE_FORMAT);
		}

		if (null != ftpStatus) {
			this.ftpStatus = ftpStatus.toString();
		}

		if (null != batchStatus) {
			this.batchStatus = batchStatus.toString();
		}
		this.state = state;
		this.exchangeType = exchangeType;
		this.defaultTenant = defaultTenant;
		this.operationType = operationType;

		if (null != isDeleted) {
			this.isDeleted = isDeleted.toString();
		}
		this.remarks = remarks;
		if (null != startTime) {
			this.startTime = DateUtil.dateToString(startTime, DATE_FORMAT);
		}
		if (null != endTime) {
			this.endTime = DateUtil.dateToString(endTime, DATE_FORMAT);
		}
//		this.requestXml = requestXml;
		this.responseXml = responseXml;
		this.pmResponseXml = pmResponseXml;

		if (null != requestStatus) {
			this.requestStatus = requestStatus.toString();
		}

		if (null != requestState) {
			this.requestState = requestState.toString();
		}
		this.requestStateDesc = requestStateDesc;
	}

	public Long getSerffReqID() {
		return serffReqID;
	}

	public void setSerffReqID(Long serffReqID) {
		this.serffReqID = serffReqID;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosIssuerID() {
		return hiosIssuerID;
	}

	public void setHiosIssuerID(String hiosIssuerID) {
		this.hiosIssuerID = hiosIssuerID;
	}

	public String getFtpStartTime() {
		return ftpStartTime;
	}

	public void setFtpStartTime(String ftpStartTime) {
		this.ftpStartTime = ftpStartTime;
	}

	public String getFtpEndTime() {
		return ftpEndTime;
	}

	public void setFtpEndTime(String ftpEndTime) {
		this.ftpEndTime = ftpEndTime;
	}

	public String getBatchStartTime() {
		return batchStartTime;
	}

	public void setBatchStartTime(String batchStartTime) {
		this.batchStartTime = batchStartTime;
	}

	public String getBatchEndTime() {
		return batchEndTime;
	}

	public void setBatchEndTime(String batchEndTime) {
		this.batchEndTime = batchEndTime;
	}

	public String getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getDefaultTenant() {
		return defaultTenant;
	}

	public void setDefaultTenant(String defaultTenant) {
		this.defaultTenant = defaultTenant;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getRequestXml() {
		return requestXml;
	}

	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}

	public String getPmResponseXml() {
		return pmResponseXml;
	}

	public void setPmResponseXml(String pmResponseXml) {
		this.pmResponseXml = pmResponseXml;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getRequestState() {
		return requestState;
	}

	public void setRequestState(String requestState) {
		this.requestState = requestState;
	}

	public String getRequestStateDesc() {
		return requestStateDesc;
	}

	public void setRequestStateDesc(String requestStateDesc) {
		this.requestStateDesc = requestStateDesc;
	}
}
