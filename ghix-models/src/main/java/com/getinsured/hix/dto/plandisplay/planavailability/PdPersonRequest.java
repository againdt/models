package com.getinsured.hix.dto.plandisplay.planavailability;

import java.io.Serializable;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;

public class PdPersonRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String memberGUID;
	private String dob;
	private Relationship relationship;
	private String zipCode;
	private String countyCode;
	private YorN tobacco;
	private Long healthEnrollmentId;
	private Long dentalEnrollmentId;
	private YorN catastrophicEligible;
	
	public String getMemberGUID() {
		return memberGUID;
	}
	
	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
	
	public String getDob() {
		return dob;
	}
	
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	public Relationship getRelationship() {
		return relationship;
	}
	
	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getCountyCode() {
		return countyCode;
	}
	
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	
	public YorN getTobacco() {
		return tobacco;
	}
	
	public void setTobacco(YorN tobacco) {
		this.tobacco = tobacco;
	}
	
	public Long getHealthEnrollmentId() {
		return healthEnrollmentId;
	}
	
	public void setHealthEnrollmentId(Long healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}
	
	public Long getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}
	
	public void setDentalEnrollmentId(Long dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public YorN getCatastrophicEligible() {
		return catastrophicEligible;
	}

	public void setCatastrophicEligible(YorN catastrophicEligible) {
		this.catastrophicEligible = catastrophicEligible;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
