UPDATE API_BATCH_CONFIG set 
API_DESC = 'Leads to outbound call.' , 
REASON_TO_CALL= 'We called this person because they might be interested in health insurance.' , 
NEXT_STEPS= 'Convert lead to new customer account.|Estimate eligiblity for APTC.|Choose a plan &amp; enroll.' , 
AGENT_GREETING= 'Hi is (customers name) there ? Hello this is (agents full name) with Getinsured &amp; I am the licensed agent that will be assisiting you with enrolling in a health insurance plan today. Do you have any questions before we get started ?' 
WHERE api_type ='API_2';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'On Exchange E-commit.' , 
REASON_TO_CALL= 'Consumer e-committed an On Exchange QHP.' , 
NEXT_STEPS= 'Take your client to healthcare.gov or SBE (using the button in CAP).|Start a new application on Healthcare.gov or SBE.|Complete the enrollment.|Return to CAP to report the App ID.' , 
AGENT_GREETING= 'Hi is (customers name) there ? I am following up with you briefly regarding the health insurance application you initiated on our website. I am here to asssit you with the process where you left off. Do you have any questions before we get started ?' 
WHERE api_type ='API_4';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'Off Exchange E-commit.' , 
REASON_TO_CALL= 'Consumer e-committed an Off Exchange QHP.' , 
NEXT_STEPS= 'Take your client to the carrier''s website (using the button in CAP).|Start a new application on the carrier website.|Use co-browsing to complete e-signature and enrollment.|Return to CAP to report the App ID.' , 
AGENT_GREETING= 'Hi is (customers name) there ? I am following up with you briefly regarding the health insurance application you initiated on our website. I am here to asssit you with the process where you left off. Do you have any questions before we get started ?' 
WHERE api_type ='API_5';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'STM E-commit.' ,
REASON_TO_CALL= 'Consumer e-committed a Short Term Medical plan.' ,
NEXT_STEPS= 'Take your client to the carrier''s website (using the button in CAP).|Start a new application on the carrier website.|Use co-browsing to complete e-signature and enrollment.|Return to CAP to report the App ID.' ,
AGENT_GREETING= 'Hi is (customers name) there ? I am following up with you briefly regarding the Short Term health insurance application you initiated on our website. I am here to asssit you with the process where you left. Do you have any questions before we get started?' 
WHERE api_type ='API_6';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'Redirected back to Get Insured from FFM.' , 
REASON_TO_CALL= 'Consumer completed application on healthcare.gov. Customer did not yet enroll.' , 
NEXT_STEPS= 'Go to the Applications section below.|Click on the application with health plan.|Continue based on the instructions.' , 
AGENT_GREETING= 'Hi is (customers name) there ? My name is (agents name) with Getinsured and I am touching base with you because the health insurance application you completed has not been submitted. I was assigned to assist you with the final steps. Do you have any questions before we get started ?'
WHERE api_type ='API_7';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'Proxy successful but we were unable to submit the enrollment to HC.gov.' ,
REASON_TO_CALL= 'Consumer completed application on GetInsured. Customer did not yet enroll.' , 
NEXT_STEPS= 'Go to the Applications section below.|Click on the application with health plan.|Continue based on the instructions.' , 
AGENT_GREETING= 'Hi is (customers name) there ? My name is (agents name) with Getinsured and I am touching base with you because the health insurance application you completed has not been submitted. I was assigned to assist you with the final steps. Do you have any questions before we get started ?' 
WHERE api_type ='API_8';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'Proxy successful but since the tax credit amount from HC.gov is lower than the estimator we need to recieve the consumer confirmation to continue.' , 
REASON_TO_CALL= 'Consumer completed application on GetInsured. Customer did not yet enroll.' , 
NEXT_STEPS= 'Go to the Applications section below.|Click on the application with health plan.|Continue based on the instructions.' ,
AGENT_GREETING= 'Hi is (customers name) there ? My name is (agents name) with Getinsured. As we were reviewing your application we found a discrepancy in your tax credit amount. I am calling so we can fix the issue and properly submit your application. Do you have any questions before we start ?' 
WHERE api_type ='API_9';

UPDATE API_BATCH_CONFIG set 
API_DESC = 'We weren''t able to use the proxy.' , 
REASON_TO_CALL= 'Consumer completed application on GetInsured. There was an error with the processing of the application. New healthcare.gov application needed.' , 
NEXT_STEPS= 'Go to the Applications section below.|Click on the application with health plan.|Continue based on the instructions.' , 
AGENT_GREETING= 'Hi is (customers name) there? I am following up with you briefly regarding the health insurance application you initiated on our website. I am here to asssit you with the process where you left off. Do you have any questions before we get started ?' WHERE api_type ='API_10';


