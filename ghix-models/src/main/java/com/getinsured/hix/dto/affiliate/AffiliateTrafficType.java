package com.getinsured.hix.dto.affiliate;

/**
 * Enum to represent Affiliate Traffic Type.
 *
 * @author Vaibhav
 */
public enum AffiliateTrafficType{

	ORGANIC("Organic"),	PAID_SEARCH("Paid Search"),	EMAIL_NETWORK("Email Network"),	TV("TV"), RADIO("Radio"), AFFILIATE_NETWORK("Affiliate Network"), WEB_EMAIL_PUB("Web & Email Pub"), POSTED_LEADS("Posted Leads"), VIMO_LIVE("VimoLive"), AFFINITY("Affinity"), OUTSOURCING("OutSourcing"), PARTNERSHIP("Partnership");

	private String	description;

	private AffiliateTrafficType(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}