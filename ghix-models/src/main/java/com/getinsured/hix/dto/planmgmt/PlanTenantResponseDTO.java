package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

/**
 * Response DTO for Plan Tenant request
 *
 */
public class PlanTenantResponseDTO extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int planId;
	private int issuerId;
	private String planName;
	private String hiosIssuerId;
	private List<PlanTenantLightDTO> tenantPlanList;
	
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public List<PlanTenantLightDTO> getTenantPlanList() {
		return tenantPlanList;
	}
	public void setTenantPlanList(List<PlanTenantLightDTO> tenantPlanList) {
		this.tenantPlanList = tenantPlanList;
	}
	public int getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	
}
