package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * The persistent class for the employee_member_applications database table.
 * 
 * @author nakka_s
 * @since 06-19-2014
 */
@Audited 
@Entity
@Table(name="employee_member_applications")
@AuditTable(value = "employee_member_app_aud")
public class EmployeeDetailsApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	public interface EditMyInfoGroup extends Default{
	}
	
	public enum Type { EMPLOYEE, SPOUSE, CHILD, LIFE_PARTNER, WARD;	}

	public enum Gender{ MALE, FEMALE;	}

	public enum EmplDetailsBooleanFlag { YES, NO;	}
	
	public enum IsDeleted { Y, N;}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_MEMBER_APPS_SEQ")
	@SequenceGenerator(name = "EMPLOYEE_MEMBER_APPS_SEQ", sequenceName = "EMPLOYEE_MEMBER_APPS_SEQ", allocationSize = 1)
	private int id;

	@Column(name="employee_id")
    private int employeeId;
    
 	@Column(name="employee_member_id")
 	private int employeeMemberId;

 	@Pattern(regexp="^[a-zA-Z '.-]+$",message="{label.validateName}",groups={EmployeeDetailsApplication.EditMyInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
 	@Size(min=1, max=35,message="{label.checkFirstNameLength}",groups={EmployeeDetailsApplication.EditMyInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
 	@NotEmpty(message="{label.validateempfirstname}",groups={Employee.EmployeeInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
	@Column(name="first_name")
	private String firstName;

 	@Pattern(regexp="^[a-zA-Z '.-]+$",message="{label.validateName}",groups={EmployeeDetailsApplication.EditMyInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
 	@Size(min=1, max=60,message="{label.checkLastNameLength}",groups={EmployeeDetailsApplication.EditMyInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
 	@NotEmpty(message="{label.validateemplastname}",groups={Employee.EmployeeInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
	@Column(name="last_name")
	private String lastName;
	
 	@Pattern(regexp="|^[a-zA-Z '-]+$",message="{label.validatemiddlenamechar}",groups={EmployeeDetailsApplication.EditMyInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
	@Column(name="middle_initial")
	private String middleInitial;

	@Column(name="GI_TYPE")
	@Enumerated(EnumType.STRING)
	private Type type;

	@NotEmpty(message="{label.validateemailadr}",groups={EmployeeDetails.EmployeeEmailValidateGroup.class})
	@Email(message="{label.validateemail}",groups={EmployeeApplication.EditDependentsGroup.class,EmployeeDetails.EmployeeEmailValidateGroup.class})
	@Size(max=50,message="{label.validateemaillength}",groups={EmployeeApplication.EditDependentsGroup.class,EmployeeDetails.EmployeeEmailValidateGroup.class})
	@Column(name="email_address")
	private String email;

	@Pattern(regexp="^(\\([0-9]{3}\\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$",message="{label.validateempvtelephone}",groups=EmployeeDetailsApplication.EditMyInfoGroup.class)
	@Column(name="contact_phone_number")
	private String contactNumber;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="birth_day")
	private Date dob;

	@NotNull(message="{label.validategender}",groups={EmployeeDetailsApplication.EditMyInfoGroup.class,EmployeeApplication.EditDependentsGroup.class})
	@Column(name="gender")
	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Column(name="ssn")
	private String ssn;

	@Column(name="smoker")
	@Enumerated(EnumType.STRING)
	private EmplDetailsBooleanFlag smoker;

	@Column(name="native_amr")
	@Enumerated(EnumType.STRING)
	private EmplDetailsBooleanFlag nativeAmr;

	@Column(name="event_id")
	private Integer event_id;
	
	@Column(name="employee_address")
	private String isEmployeeAddress;

	@Valid
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name="home_location_id")
	private Location location;

	@Column(name="is_deleted")
	@Enumerated(EnumType.STRING)
	private IsDeleted isDeleted;
	
	@Column(name="suffix")
	//@Enumerated(EnumType.STRING)
	private String suffix;
	
	@NotEmpty(message="{label.validatemaritalstatus}",groups=EmployeeDetailsApplication.EditMyInfoGroup.class)
	@Column(name="marital_status")
	//@Enumerated(EnumType.STRING)
	private String maritalStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp")
	private Date created;

	@NotAudited
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp")
	private Date updated;
	
	@Column(name="tcp")
	@Enumerated(EnumType.STRING)
	private EmplDetailsBooleanFlag tobaccoCessPrg;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="employee_application_id")
	private EmployeeApplication employeeApplication;
	
	public EmployeeApplication getEmployeeApplication() {
		return employeeApplication;
	}

	public void setEmployeeApplication(EmployeeApplication employeeApplication) {
		this.employeeApplication = employeeApplication;
	}

	public EmployeeDetailsApplication() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmployeeId() {
		return employeeId;
	}
	
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getEmployeeMemberId() {
		return employeeMemberId;
	}

	public void setEmployeeMemberId(int employeeMemberId) {
		this.employeeMemberId = employeeMemberId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getEmail() {
		return StringUtils.lowerCase(email);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public EmplDetailsBooleanFlag getSmoker() {
		return smoker;
	}

	public void setSmoker(EmplDetailsBooleanFlag smoker) {
		this.smoker = smoker;
	}

	public EmplDetailsBooleanFlag getNativeAmr() {
		return nativeAmr;
	}

	public void setNativeAmr(EmplDetailsBooleanFlag nativeAmr) {
		this.nativeAmr = nativeAmr;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Integer getEvent_id() {
		return event_id;
	}

	public void setEvent_id(Integer event_id) {
		this.event_id = event_id;
	}
	
	public String getIsEmployeeAddress() {
		return isEmployeeAddress;
	}

	public void setIsEmployeeAddress(String isEmployeeAddress) {
		this.isEmployeeAddress = isEmployeeAddress;
	}

	public IsDeleted getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(IsDeleted isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public EmplDetailsBooleanFlag getTobaccoCessPrg() {
		return tobaccoCessPrg;
	}

	public void setTobaccoCessPrg(EmplDetailsBooleanFlag tobaccoCessPrg) {
		this.tobaccoCessPrg = tobaccoCessPrg;
	}
	
	

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdated(new TSDate());
	}
}
