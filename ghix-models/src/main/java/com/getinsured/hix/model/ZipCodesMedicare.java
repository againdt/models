package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "ZIPCODE_MEDICARE")
public class ZipCodesMedicare {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZIPCODE_MEDICARE_SEQ")
	@SequenceGenerator(name = "ZIPCODE_MEDICARE_SEQ", sequenceName = "ZIPCODE_MEDICARE_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "ZIPCODE")
	private String zipcode;

	@Column(name = "COUNTY")
	private String county;

	@Column(name = "COUNTY_FIPS")
	private String countyFips;

	@Column(name = "STATE")
	private String state;

	@Column(name = "STATE_FIPS")
	private String stateFips;

	@Column(name = "AREA_CODE")
	private String areaCode;

	@Column(name = "TIME_ZONE")
	private String timeZone;

	@Column(name = "LATTITUDE")
	private Float lattitude;

	@Column(name = "LONGITUDE")
	private Float longitude;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "CITY_TYPE")
	private String cityType;
	
	@Column(name = "STATE_NAME")
	private String stateName;
	
	@Column(name = "MSA")
	private String msa;
	
	@Column(name = "GMT_OFFSET")
	private int gmtOffSet;
	
	@Column(name = "DST")
	private String dst;
	
	public ZipCodesMedicare() {

	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountyFips() {
		return countyFips;
	}

	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateFips() {
		return stateFips;
	}

	public void setStateFips(String stateFips) {
		this.stateFips = stateFips;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public Float getLatitude() {
		return lattitude;
	}

	public void setLatitude(Float lattitude) {
		this.lattitude = lattitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityType() {
		return cityType;
	}

	public void setCityType(String cityType) {
		this.cityType = cityType;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getMsa() {
		return msa;
	}

	public void setMsa(String msa) {
		this.msa = msa;
	}

	public int getGmtOffSet() {
		return gmtOffSet;
	}

	public void setGmtOffSet(int gmtOffSet) {
		this.gmtOffSet = gmtOffSet;
	}

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

}
