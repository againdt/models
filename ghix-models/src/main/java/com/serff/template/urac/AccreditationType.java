//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.07.10 at 02:18:49 PM IST 
//


package com.serff.template.urac;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for A data type representing Insurance Accreditations granted by URAC (Issuer level) or NCQA (Product Category and Product Line level).
 * 
 * <p>Java class for AccreditationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccreditationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-pm}AccreditationStatusCode"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-pm}AccreditationIdentification"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-pm}ProductLineCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccreditationType", namespace = "http://hix.cms.gov/0.1/hix-pm", propOrder = {
    "accreditationStatusCode",
    "accreditationIdentification",
    "productLineCode"
})
@XmlSeeAlso({
    URACAccreditationType.class
})
public class AccreditationType
    extends ComplexObjectType
{

    @XmlElement(name = "AccreditationStatusCode", required = true)
    protected AccreditationStatusCodeSimpleType accreditationStatusCode;
    @XmlElement(name = "AccreditationIdentification", required = true)
    protected IdentificationType accreditationIdentification;
    @XmlElement(name = "ProductLineCode", required = true)
    protected ProductLineCodeSimpleType productLineCode;

    /**
     * Gets the value of the accreditationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link AccreditationStatusCodeSimpleType }
     *     
     */
    public AccreditationStatusCodeSimpleType getAccreditationStatusCode() {
        return accreditationStatusCode;
    }

    /**
     * Sets the value of the accreditationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccreditationStatusCodeSimpleType }
     *     
     */
    public void setAccreditationStatusCode(AccreditationStatusCodeSimpleType value) {
        this.accreditationStatusCode = value;
    }

    /**
     * Gets the value of the accreditationIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *     
     */
    public IdentificationType getAccreditationIdentification() {
        return accreditationIdentification;
    }

    /**
     * Sets the value of the accreditationIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *     
     */
    public void setAccreditationIdentification(IdentificationType value) {
        this.accreditationIdentification = value;
    }

    /**
     * Gets the value of the productLineCode property.
     * 
     * @return
     *     possible object is
     *     {@link ProductLineCodeSimpleType }
     *     
     */
    public ProductLineCodeSimpleType getProductLineCode() {
        return productLineCode;
    }

    /**
     * Sets the value of the productLineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductLineCodeSimpleType }
     *     
     */
    public void setProductLineCode(ProductLineCodeSimpleType value) {
        this.productLineCode = value;
    }

}
