insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Account Management' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Application Configuration' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Customer Management' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Enrollment Reconciliation' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Miscellaneous' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Partner Management' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Plan Management' );
insert into PERMISSIONS_GROUP values(PERMISSIONS_GROUP_SEQ.NEXTVAL, 'Ticket Management' );