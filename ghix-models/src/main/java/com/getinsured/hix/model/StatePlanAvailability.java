package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="STATE_PLAN_AVAILABILITY")
public class StatePlanAvailability implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STATE_PLAN_AVAILABILITY_SEQ")
	@SequenceGenerator(name = "STATE_PLAN_AVAILABILITY_SEQ", sequenceName = "STATE_PLAN_AVAILABILITY_SEQ", allocationSize = 1)
	private int id;	
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="ZIP_CODE ")
	private String zipCode;
	
	@Column(name="RUN_DATE")
	private Date runDate;
	
	@Column(name="PLAN_AVAILABLE ")
	private String planAvailable;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public String getState() {
		return state;
	}

	public String getPlanAvailable() {
		return planAvailable;
	}

	public void setPlanAvailable(String planAvailable) {
		this.planAvailable = planAvailable;
	}

	public void setState(String state) {
		this.state = state;
	}

}
