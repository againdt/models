package com.getinsured.hix.dto.plandisplay.planavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;


public class PdRequestPlanAvailability implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long householdId;
	private Long householdGUID;
	private String coverageDate;
	private Float maxSubsidy;
	private BigDecimal maxStateSubsidy;
	private PlanDisplayEnum.CostSharing costSharing;
	private PlanDisplayEnum.ShoppingType shoppingType;
	private PlanDisplayEnum.EnrollmentType enrollmentType;
	
	private List<PdPersonRequest> pdPersonCRDTOList;
	
	public Long getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}

	public Long getHouseholdGUID() {
		return householdGUID;
	}

	public void setHouseholdGUID(Long householdGUID) {
		this.householdGUID = householdGUID;
	}

	public String getCoverageDate() {
		return coverageDate;
	}

	public void setCoverageDate(String coverageDate) {
		this.coverageDate = coverageDate;
	}

	public Float getMaxSubsidy() {
		return maxSubsidy;
	}

	public void setMaxSubsidy(Float maxSubsidy) {
		this.maxSubsidy = maxSubsidy;
	}

	public PlanDisplayEnum.CostSharing getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(PlanDisplayEnum.CostSharing costSharing) {
		this.costSharing = costSharing;
	}

	public PlanDisplayEnum.ShoppingType getShoppingType() {
		return shoppingType;
	}

	public void setShoppingType(PlanDisplayEnum.ShoppingType shoppingType) {
		this.shoppingType = shoppingType;
	}

	public PlanDisplayEnum.EnrollmentType getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(PlanDisplayEnum.EnrollmentType enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public List<PdPersonRequest> getPdPersonCRDTOList() {
		return pdPersonCRDTOList;
	}

	public void setPdPersonCRDTOList(List<PdPersonRequest> pdPersonCRDTOList) {
		this.pdPersonCRDTOList = pdPersonCRDTOList;
	}

	public BigDecimal getMaxStateSubsidy() {
		return maxStateSubsidy;
	}

	public void setMaxStateSubsidy(BigDecimal maxStateSubsidy) {
		this.maxStateSubsidy = maxStateSubsidy;
	}
}
