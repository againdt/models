/**
 * 
 * @author santanu
 * @version 1.0
 * @since Aug 29, 2014 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class AmePlanRequestDTO implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;	
	private String effectiveDate;
	private List<Map<String, String>> memberList; // household member data
	private String tenantCode;
	
	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public List<Map<String, String>> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<Map<String, String>> memberList) {
		this.memberList = memberList;
	}

	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}

	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	
	
	
}