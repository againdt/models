/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author ganatra_h
 *
 */
@Entity
@Table(name="employee_documents")
public class EmployeeDocuments implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_DOCUMENTS_SEQ")
	@SequenceGenerator(name = "EMPLOYEE_DOCUMENTS_SEQ", sequenceName = "EMPLOYEE_DOCUMENTS_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="employee_id")
    private int employeeId;
	
	@Column(name="member_id")
    private int memberId;
	
	@Column(name="doc_id")
	private String docId;
	
	@Column(name="file_name")
	private String fileName;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="uploaded_date")
	private Date uploadedDate;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="deleted_date")
	private Date deletedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}
}
