package com.getinsured.hix.dto.planmgmt.microservice;


public class IssuerLightDTO extends IssuerBrandNameDTO {

	private Integer issuerId;
	private String issuerName;
	private String hiosId;
	private String tenantList;
	private String enrollmentFlowType;
	private String lastUpdatedTimestamp;
	private String certificationStatus;
	private String d2cFlowType;
	private String shortTenantList;
	private String longTenantList;
	private String stateOfDomicile;
	
	
	public IssuerLightDTO() {
		super();
	}

	/**
	 * @return the issuerId
	 */
	public Integer getIssuerId() {
		return issuerId;
	}

	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(Integer id) {
		this.issuerId = id;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getTenantList() {
		return tenantList;
	}

	public void setTenantList(String tenantList) {
		this.tenantList = tenantList;
	}

	public String getEnrollmentFlowType() {
		return enrollmentFlowType;
	}

	public void setEnrollmentFlowType(String enrollmentFlowType) {
		this.enrollmentFlowType = enrollmentFlowType;
	}

	public String getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(String lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}

	/**
	 * @return the d2cFlowType
	 */
	public String getD2cFlowType() {
		return d2cFlowType;
	}

	/**
	 * @param d2cFlowType the d2cFlowType to set
	 */
	public void setD2cFlowType(String d2cFlowType) {
		this.d2cFlowType = d2cFlowType;
	}
	
	/**
	 * @return the shortTenantList
	 */
	public String getShortTenantList() {
		return shortTenantList;
	}

	/**
	 * @param shortTenantList the shortTenantList to set
	 */
	public void setShortTenantList(String shortTenantList) {
		this.shortTenantList = shortTenantList;
	}

	/**
	 * @return the longTenantList
	 */
	public String getLongTenantList() {
		return longTenantList;
	}

	/**
	 * @param longTenantList the longTenantList to set
	 */
	public void setLongTenantList(String longTenantList) {
		this.longTenantList = longTenantList;
	}

	public String getStateOfDomicile() {
		return stateOfDomicile;
	}

	public void setStateOfDomicile(String stateOfDomicile) {
		this.stateOfDomicile = stateOfDomicile;
	}
	
	
}
