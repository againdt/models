package com.getinsured.hix.dto.eapp;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.lang3.StringUtils;



public class ExcelToConfig {
	
	private String inputFile;
	List<List<String>> finalFieldList;
	List<String> radioList;
	
	public final String QUOTE = "\"";
	public final String CURLY_S ="{";
	public final String CURLY_E ="}";
	public final String COLON =":";

	  public void setInputFile(String inputFile) {
	    this.inputFile = inputFile;
	  }
	  
	  public List<List<String>> read() throws IOException  {
		    File inputWorkbook = new File(inputFile);
		    Workbook w;
		    try {
		      w = Workbook.getWorkbook(inputWorkbook);
		      // Get the first sheet
		      Sheet sheet = w.getSheet(0);
		      // Loop over first 10 column and lines
		      finalFieldList = new ArrayList<>();
		      for (int j = 0; j < sheet.getRows(); j++) {
		    	  List<String> fieldList = new ArrayList<>();
		        for (int i = 0; i < sheet.getColumns(); i++) {
		          Cell cell = sheet.getCell(i, j);
		          fieldList.add(cell.getContents());
		        }
		        finalFieldList.add(fieldList);
		      }
		    } catch (BiffException e) {
		      e.printStackTrace();
		    }
		    
		    return finalFieldList;
	  }
	/**
	 * @param args
	 */
	public static void main(String[] args)  throws IOException  {
		ExcelToConfig test = new ExcelToConfig();
	    test.setInputFile("C:/Users/meher_a/Desktop/Atana.xls");
	    List<String> configList = test.getConfigList(test.read());
	    String configText = test.getConfigText(configList);
	    test.createFile(configText);
	}

	private String getConfigText(List<String> configList) {
		return addCurly( addQuote("fields") + COLON + addCurly(StringUtils.join(configList,',') ) );
	}

	private void createFile(String config) {
		try {	    
		    PrintWriter writer = new PrintWriter("C:/Users/meher_a/Desktop/the-file-name.conf", "UTF-8");
		    writer.println(config);
		    writer.close();
		} catch (IOException e) {
			System.out.println("IOException : "+e);
		}
		
	}

	private List<String> getConfigList(List<List<String>> read) {
		List<String> configList = new ArrayList<>();
		radioList = new ArrayList<>();
		String nodeStr = null;
		for (List<String> list : read) {
			String formatter = list.get(1);
			
			if(formatter.equalsIgnoreCase("Simple")){
				
				nodeStr = getStrForSimple(list);
				
			}else if(formatter.equalsIgnoreCase("formatter")){
				
				nodeStr = getStrForFormatter(list);
			
			}else if(formatter.equalsIgnoreCase("currentDate")){
				
				nodeStr = getStrForCurrentDate(list);
				
			}else if (formatter.equalsIgnoreCase("ifEqualElseComplex")){
				
				nodeStr = getStrForIfEqualElseComplex(list);
				
			}else if(formatter.equalsIgnoreCase("radio_collection_maps")){
				//Radio collection add to Radio List
				nodeStr = null;
				radioList.add(getRadioNode(list));
				
			}
			
			if(nodeStr!= null){
				configList.add(nodeStr);
			}
		}
		
		configList.add(getRadioCollectionNode(radioList));
		
		return configList;
	}

	private String getStrForIfEqualElseComplex(List<String> list) {
		String key = addQuote(list.get(0));
		
		StringBuilder value = new StringBuilder(addQuote("type") + COLON + addQuote("ifEqualElseComplex"));
		value.append(",");
		value.append(addQuote("argument") + COLON + addQuote(list.get(2)));
		value.append(",");
		value.append(addQuote("equals") + COLON + addQuote(list.get(3)));
		value.append(",");
		value.append(addQuote("value") + COLON + addQuote(list.get(4)));
		value.append(",");
		value.append(addQuote("else") + COLON + addQuote(list.get(5)));
		
		return key +":"+addCurly(value.toString());
	}

	private String getStrForCurrentDate(List<String> list) {
		String key = addQuote(list.get(0));
		
		StringBuilder value = new StringBuilder(addQuote("type") + COLON + addQuote("currentDate"));
		value.append(",");
		value.append(addQuote("inputFormat") + COLON + addQuote("epoch"));
		value.append(",");
		value.append(addQuote("outputFormat")+ COLON + addQuote("MM/dd/yyyy"));
		
		return key + COLON + addCurly(value.toString());
	}

	private String getRadioCollectionNode(List<String> radioNodeList) {
		
		return addQuote("radio_collection_maps") + COLON + addCurly(StringUtils.join(radioNodeList,','));
	}

	private String getRadioNode(List<String> list) {
		String value = addQuote(list.get(2));
		StringBuilder nodeBuilder = null;
		List<String> nodeList = new ArrayList<>();
		int index = 1;
		
		for (int i = 3; i < list.size(); i++) {
			if(list.get(i) != null && !list.get(i).equalsIgnoreCase("")){
				nodeBuilder = new StringBuilder();
				nodeBuilder.append(addQuote(list.get(i)));
				nodeBuilder.append(COLON);
				nodeBuilder.append(index++);
				
				nodeList.add(nodeBuilder.toString());
			}

		}
		
		return value+ COLON + addCurly(StringUtils.join(nodeList,','));
	}

	private String getStrForSimple(List<String> list) {
		return addQuote(list.get(0)) + COLON + addQuote(list.get(2));
	}
	
	private String getStrForFormatter(List<String> list) {
		String key = addQuote(list.get(0));
		
		StringBuilder value = new StringBuilder(addQuote("type") + COLON + addQuote("formatter"));
		value.append(",");
		value.append(addQuote("values")+ COLON +addCurly(list.get(2)));
		value.append(",");
		value.append(addQuote("formatter")+ COLON +addCurly(list.get(3)));
		
		
		return key +":"+addCurly(value.toString());
	}

	private String addCurly(String string) {
		return CURLY_S+string+CURLY_E;
	}
	
	private String addQuote(String string) {
		return QUOTE+string+QUOTE;
	}
}
