package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is Model class for DRUGDATA database table.
 * 
 * @since October 23, 2018
 */
@Entity
@Table(name = "DRUGDATA")
public class DrugData implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "RXCUI")
	private String rxcui;

	@Column(name = "NAME")
	private String name;

	@Column(name = "STRENGTH")
	private String strength;

	@Column(name = "ROUTE")
	private String route;

	@Column(name = "FULL_NAME")
	private String fullName;

	@Column(name = "RXTERM_DOSAGE_FORM")
	private String rxTermDosageForm;

	@Column(name = "RXNORM_DOSAGE_FORM")
	private String rxNormDosageForm;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "GENERIC_NAME")
	private String genericName;

	@Column(name = "GENERIC_RXCUI")
	private String genericRxcui;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "REMAPPED_TO")
	private String remappedTo;

	public DrugData() {
	}

	public String getRxcui() {
		return rxcui;
	}

	public void setRxcui(String rxcui) {
		this.rxcui = rxcui;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getRxTermDosageForm() {
		return rxTermDosageForm;
	}

	public void setRxTermDosageForm(String rxTermDosageForm) {
		this.rxTermDosageForm = rxTermDosageForm;
	}

	public String getRxNormDosageForm() {
		return rxNormDosageForm;
	}

	public void setRxNormDosageForm(String rxNormDosageForm) {
		this.rxNormDosageForm = rxNormDosageForm;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGenericName() {
		return genericName;
	}

	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}

	public String getGenericRxcui() {
		return genericRxcui;
	}

	public void setGenericRxcui(String genericRxcui) {
		this.genericRxcui = genericRxcui;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemappedTo() {
		return remappedTo;
	}

	public void setRemappedTo(String remappedTo) {
		this.remappedTo = remappedTo;
	}
}
