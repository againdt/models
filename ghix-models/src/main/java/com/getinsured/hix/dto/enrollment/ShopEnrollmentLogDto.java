package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;


public class ShopEnrollmentLogDto implements Serializable{
	private String eventDate;
	private String employeeName;
	private String enrolleeName;
	private String insuranceType;
	private String eventType;
	private String eventReason;
	private String serc;
	private String coverageStartDate;
	private String coverageEndDate;
	private String cmsPlanId;
	private String planName;

	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEnrolleeName() {
		return enrolleeName;
	}
	public void setEnrolleeName(String enrolleeName) {
		this.enrolleeName = enrolleeName;
	}

	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventReason() {
		return eventReason;
	}
	public void setEventReason(String eventReason) {
		this.eventReason = eventReason;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}
	public void setCoverageEndDate(String caoverageEndDate) {
		this.coverageEndDate = caoverageEndDate;
	}
	
	public String getSerc() {
		return serc;
	}
	public void setSerc(String serc) {
		this.serc = serc;
	}
	
	public String getCmsPlanId() {
		return cmsPlanId;
	}
	public void setCmsPlanId(String cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	
	@Override
	public String toString() {
		return "ShopEnrollmentLogDto [eventDate=" + eventDate
				+ ", employeeName=" + employeeName + ", enrolleeName="
				+ enrolleeName + ", insuranceType=" + insuranceType
				+ ", eventType=" + eventType + ", eventReason=" + eventReason
				+ ", serc=" + serc + ", coverageStartDate=" + coverageStartDate
				+ ", coverageEndDate=" + coverageEndDate + ", cmsPlanId="
				+ cmsPlanId + ", planName=" + planName + "]";
	}
	
}
