This is a standalone project to run liquibase on any database.

This uses an ghix.ear which needs to be present in ghix-setup\db folder. Manually copy latest ear in this folder.

The database connection details needs to be present in ../../conf/configuration.properties file.

There are some handy wrappers available for both windows and Unix environments.

Wrappers:-
==========

updateDatabase - updates the database with the latest changeSets which are not applied
statusOfDatabase - provides database status about which all changeSets are not applied
generateSQLScript - generates SQL scripts for all changeSets are not applied
generateDBdoc - generates database docs at dbdoc folder
