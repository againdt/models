package com.getinsured.hix.dto.entity;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.model.entity.Assister;

public class MapKeyDeserializer extends KeyDeserializer {
	static ObjectMapper mapper = new ObjectMapper();

	@Override
	public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		 return mapper.readValue( key, Assister.class );
	}

}
