package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.List;

public class EnrollmentDTO extends Enrollment implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String enrlStatusLabel;
	private String enrlStatusCode;
	private String enrollmentStartDate;
	private String enrollmentEndDate;
	private String enrlModalityLabel;
	private String insuranceTypeLabel;
	private String exchangeTypeLabel;
	private String fullName;
	
	private List<EnrolleeDTO> enrolleeDTOs;
	
	public String getEnrlStatusLabel() {
		return enrlStatusLabel;
	}
	public void setEnrlStatusLabel(String enrlStatusLabel) {
		this.enrlStatusLabel = enrlStatusLabel;
	}
	public String getEnrlStatusCode() {
		return enrlStatusCode;
	}
	public void setEnrlStatusCode(String enrlStatusCode) {
		this.enrlStatusCode = enrlStatusCode;
	}
	public String getEnrollmentStartDate() {
		return enrollmentStartDate;
	}
	public void setEnrollmentStartDate(String enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}
	public String getEnrollmentEndDate() {
		return enrollmentEndDate;
	}
	public void setEnrollmentEndDate(String enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}
	public String getEnrlModalityLabel() {
		return enrlModalityLabel;
	}
	public void setEnrlModalityLabel(String enrlModalityLabel) {
		this.enrlModalityLabel = enrlModalityLabel;
	}
	public String getInsuranceTypeLabel() {
		return insuranceTypeLabel;
	}
	public void setInsuranceTypeLabel(String insuranceTypeLabel) {
		this.insuranceTypeLabel = insuranceTypeLabel;
	}
	public String getExchangeTypeLabel() {
		return exchangeTypeLabel;
	}
	public void setExchangeTypeLabel(String exchangeTypeLabel) {
		this.exchangeTypeLabel = exchangeTypeLabel;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public List<EnrolleeDTO> getEnrolleeDTOs() {
		return enrolleeDTOs;
	}
	public void setEnrolleeDTOs(List<EnrolleeDTO> enrolleeDTOs) {
		this.enrolleeDTOs = enrolleeDTOs;
	}
	
	
}
