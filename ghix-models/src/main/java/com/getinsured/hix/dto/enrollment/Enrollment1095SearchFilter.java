package com.getinsured.hix.dto.enrollment;

import com.getinsured.hix.model.enrollment.Enrollment1095;

public class Enrollment1095SearchFilter extends Enrollment1095{
	

	private String firstName;
	private String lastName;
	private Long policyNumber;
	private String ssn;
	private String orderBy;
	private String sortOrder;
	private Integer pageSize;
	private Integer pageNumber;
	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(Long policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	@Override
	public String toString() {
		return "Enrollment1095SearchFilter [firstName=" + firstName + ", lastName="+ lastName + ", policyNumber=" + policyNumber 
				+ ", ssn=" + ssn + "]";
	}
	
}
