/**
 * 
 */
package com.getinsured.hix.model;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * @author Vani
 * This is POJO class for AME premium factor for Carrier like AIG
 * 
 */
@Audited
@Entity
@Table(name = "PM_AME_PREMIUM_FACTOR")
public class PlanAMEPremiumFactor {

	private static final long serialVersionUID = 1L;

	public enum IS_DELETED {
		// Y-Yes, N-No
		Y, N;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_AME_PREMIUM_FACTOR_SEQ")
	@SequenceGenerator(name = "PM_AME_PREMIUM_FACTOR_SEQ", sequenceName = "PM_AME_PREMIUM_FACTOR_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "ISSUER_HIOS_ID")
	private String issuerHiosId;

	@Column(name = "STATE")
	private String state;

	@Column(name = "ANNUAL_FACTOR")
	private Double annualFactor;

	@Column(name = "SEMI_ANNUAL_FACTOR")
	private Double semiAnnualFactor;

	@Column(name = "QUARTERLY_FACTOR")
	private Double quarterlyFactor;

	@Column(name = "MONTHLY_FACTOR")
	private Double monthlyFactor;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "IS_DELETED")
	private String isDeleted;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public PlanAMEPremiumFactor() {

	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIssuerHiosId() {
		return issuerHiosId;
	}

	public void setIssuerHiosId(String issuerHiosId) {
		this.issuerHiosId = issuerHiosId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Double getAnnualFactor() {
		return annualFactor;
	}

	public void setAnnualFactor(Double annualFactor) {
		this.annualFactor = annualFactor;
	}

	public Double getSemiAnnualFactor() {
		return semiAnnualFactor;
	}

	public void setSemiAnnualFactor(Double semiAnnualFactor) {
		this.semiAnnualFactor = semiAnnualFactor;
	}

	public Double getQuarterlyFactor() {
		return quarterlyFactor;
	}

	public void setQuarterlyFactor(Double quarterlyFactor) {
		this.quarterlyFactor = quarterlyFactor;
	}

	public Double getMonthlyFactor() {
		return monthlyFactor;
	}

	public void setMonthlyFactor(Double monthlyFactor) {
		this.monthlyFactor = monthlyFactor;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

}
