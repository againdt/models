package com.getinsured.hix.dto.cap.consumerapp;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.junit.Test;

import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicationDTO;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.google.gson.Gson;

public class CapApplicationDTOTest{

	@Test
	public void test() {

		List<SsapApplicantDTO> applicantDtoList = new ArrayList<SsapApplicantDTO>();
		
		SsapApplicantDTO applicantDto = new SsapApplicantDTO();
		applicantDto.setBirthDate(new TSDate());
		applicantDto.setFirstName("firstName1");
		applicantDto.setLastName("lastName1");
		applicantDto.setTobaccoUser("Yes");
		applicantDto.setGender("Male");
		applicantDto.setRelationship("Son");
		
		applicantDtoList.add(applicantDto);
				SsapApplicantDTO applicantDto1 = new SsapApplicantDTO();
		applicantDto1.setBirthDate(new TSDate());
		applicantDto1.setFirstName("firstName2");
		applicantDto1.setLastName("lastName2");
		applicantDto1.setTobaccoUser("No");
		applicantDto1.setGender("Female");
		applicantDto1.setRelationship("Spouse");
		applicantDtoList.add(applicantDto1);
		CapApplicationDTO capManualEnrollmentDto= new CapApplicationDTO();
		
		SsapApplicationDTO applicationDto = new SsapApplicationDTO();
		applicationDto.setId(121);
		applicationDto.setApplicationType("applicationType");
		applicationDto.setApplicationStatus("applicationStatus");
		applicationDto.setFfmHouseholdId("123");
		applicationDto.setApplicationStage(EligLead.STAGE.PRE_ELIGIBILITY);
		applicationDto.setEffectiveStartDate(new TSDate());
		applicationDto.setExchangeType("exchangeType");
		
		HouseholdDTO householdDTO = new HouseholdDTO();
		householdDTO.setFirstName("householdfirstname");
		householdDTO.setLastName("householdlastname");
		householdDTO.setEmail("household@mail.com");
		
		LocationDto locationDto = new LocationDto();
		locationDto.setAddress1("addr1");
		locationDto.setAddress2("addr2");
		locationDto.setState("state");
		locationDto.setZip("42323");
		
		householdDTO.setLocation(locationDto);
		householdDTO.setZipCode(locationDto.getZip());
		householdDTO.setPhoneNumber("91191919");
		householdDTO.setPreferredContactTime("Morning");
		
		PlanInfoDTO planInfoDTO = new PlanInfoDTO();
		
		planInfoDTO.setMetalTier("metalTier");
		planInfoDTO.setCarrierName("nsurance Company");
		planInfoDTO.setMonthlyPremium(34f);
		planInfoDTO.setName("Plan name");
		planInfoDTO.setType("Dental");
		planInfoDTO.setId(342);
		
		EnrollmentDTO enrollmentDTO = new EnrollmentDTO();
		
		capManualEnrollmentDto.setPlanInfoDTO(planInfoDTO);
		capManualEnrollmentDto.setHouseholdDTO(householdDTO);
		capManualEnrollmentDto.setSsapApplicationDTO(applicationDto);
		capManualEnrollmentDto.setApplicantDTOList(applicantDtoList);
		capManualEnrollmentDto.setEnrollmentDTO(enrollmentDTO);
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		String out = gson.toJson(capManualEnrollmentDto);
		
		System.out.println(out);
		
		out = gson.toJson(getCapApplicationDTO());
		System.out.println(out);

	}
	
	private CapApplicationDTO getCapApplicationDTO()
	{
		CapApplicationDTO retVal = new CapApplicationDTO();
		retVal.setPlanInfoDTO(getPlanInfoDTO());
		
		return retVal;
	}
	private PlanInfoDTO getPlanInfoDTO()
	{
		PlanInfoDTO retVal = new PlanInfoDTO();
		retVal.setCarrierName("Aetna");
		retVal.setId(1001);
		retVal.setMetalTier("SILVER");
		retVal.setMonthlyPremium(1001);
		retVal.setName("Premium 1001");
		retVal.setType("QHP");
		return retVal;
	}

}

/*

	private SsapApplicationDTO ssapApplicationDTO;
	private List<SsapApplicantDTO> applicantDTOList;
	private HouseholdDTO householdDTO;
	private EnrollmentDTO enrollmentDTO;
	private ConsumerDocumentDTO consumerDocumentDTO;
 PlanInfoDTO
*/
