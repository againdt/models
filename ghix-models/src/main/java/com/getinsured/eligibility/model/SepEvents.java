package com.getinsured.eligibility.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;



/**
 * The persistent class for the SPECIAL_ENROLLMENT_EVENTS database table.
 *
 */
@Entity
@Table(name="SEP_ENROLLMENT_EVENTS")
public class SepEvents implements Serializable
{
	private static final long serialVersionUID = 1047527895957077131L;

	public enum Financial { Y, N };
	
	public enum ChangeType {ADD, REMOVE, OTHER, QUALIFYING_EVENT};
	
	public enum Source {EXCHANGE, EXTERNAL, ALL};
	
	public enum Gated {Y, N};
	
	public enum RealTime {Y, N};
	
	public enum ManualVerification {Y, N};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEP_ENROLLMENT_EVENTS_SEQ")
	@SequenceGenerator(name = "SEP_ENROLLMENT_EVENTS_SEQ", sequenceName = "SEP_ENROLLMENT_EVENTS_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name="EVENT_NAME")
	private String name;

	@Column(name="EVENT_TYPE")
	private int type;

	@Column(name="MRC_CODE")
	private String mrcCode;

	@Column(name="EVENT_LABEL")
	private String label;

	@Column(name="EVENT_CATEGORY")
	private String category;

	@Column(name="EVENT_CATEGORY_LABEL")
	private String categoryLabel;
	
	@Column(name="VERIFICATIONS_REQD")
	private String verificationsRequired;

	@Column(name="ELIGIBILITY_RERUN_REQD")
	private String eligibilityRerunRequired;

	@Column(name="IS_FINANCIAL")
	@Enumerated(EnumType.STRING)
	private Financial financial;
	
	@Column(name="CHANGE_TYPE")
	private String changeType;
	
	@Column(name="EVENT_PRECEDENCE_ORDER")
	private Integer eventPrecedenceOrder;
	
	@Column(name="DISPLAY_ON_UI")
	private String displayOnUI;

	@Column(name="ALLOW_FUTURE_EVENT")
	private String allowFutureEvent;
	
	@Column(name="REFERENCE_FUTURE_EVENT")
	private Integer referenceFutureEvent;
	
	@Column(name="IS_GATED")
	@Enumerated(EnumType.STRING)
	private Gated gated;
	
	@Column(name="SOURCE")
	@Enumerated(EnumType.STRING)
	private Source source;
	
	@Column(name="REQD_DOCUMENTS")
	private String reqdDocuments;
	
	@Column(name="REAL_TIME_VERIFICATION")
	@Enumerated(EnumType.STRING)
	private RealTime realTimeVerificationReqd;
	
	@Column(name="MANUAL_VERIFICATION_REQD")
	@Enumerated(EnumType.STRING)
	private ManualVerification manualVerificationReqd;
	
	@Type(type = "numeric_boolean")
    @Column(name="ALLOW_PLAN_SELECTION")
    private boolean planSelectionAllowed;

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMrcCode() {
		return mrcCode;
	}

	public void setMrcCode(String mrcCode) {
		this.mrcCode = mrcCode;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryLabel() {
		return categoryLabel;
	}

	public void setCategoryLabel(String categoryLabel) {
		this.categoryLabel = categoryLabel;
	}

	public Financial getFinancial() {
		return financial;
	}

	public void setFinancial(Financial financial) {
		this.financial = financial;
	}

	public String getVerificationsRequired() {
		return verificationsRequired;
	}

	public void setVerificationsRequired(String verificationsRequired) {
		this.verificationsRequired = verificationsRequired;
	}

	public String getEligibilityRerunRequired() {
		return eligibilityRerunRequired;
	}

	public void setEligibilityRerunRequired(String eligibilityRerunRequired) {
		this.eligibilityRerunRequired = eligibilityRerunRequired;
	}

	public Integer getEventPrecedenceOrder() {
		return eventPrecedenceOrder;
	}

	public void setEventPrecedenceOrder(Integer eventPrecedenceOrder) {
		this.eventPrecedenceOrder = eventPrecedenceOrder;
	}

	public String getDisplayOnUI() {
		return displayOnUI;
	}

	public void setDisplayOnUI(String displayOnUI) {
		this.displayOnUI = displayOnUI;
	}

	public String getAllowFutureEvent() {
		return allowFutureEvent;
	}

	public void setAllowFutureEvent(String allowFutureEvent) {
		this.allowFutureEvent = allowFutureEvent;
	}

	public Integer getReferenceFutureEvent() {
		return referenceFutureEvent;
	}

	public void setReferenceFutureEvent(Integer referenceFutureEvent) {
		this.referenceFutureEvent = referenceFutureEvent;
	}
	
	public Gated getGated() {
		return gated;
	}

	public void setGated(Gated gated) {
		this.gated = gated;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public String getReqdDocuments() {
		return reqdDocuments;
	}

	public void setReqdDocuments(String reqdDocuments) {
		this.reqdDocuments = reqdDocuments;
	}

	public RealTime getRealTimeVerificationReqd() {
		return realTimeVerificationReqd;
	}

	public void setRealTimeVerificationReqd(RealTime realTimeVerificationReqd) {
		this.realTimeVerificationReqd = realTimeVerificationReqd;
	}

	public ManualVerification getManualVerificationReqd() {
		return manualVerificationReqd;
	}

	public void setManualVerificationReqd(ManualVerification manualVerificationReqd) {
		this.manualVerificationReqd = manualVerificationReqd;
	}

	public boolean isPlanSelectionAllowed() {
		return planSelectionAllowed;
	}

	public void setPlanSelectionAllowed(boolean planSelectionAllowed) {
		this.planSelectionAllowed = planSelectionAllowed;
	}
	
}