/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import java.sql.Date;

import javax.validation.constraints.NotNull;

/**
 * @author Srikanth Nakka
 *
 */
public class EnrollmentDates {
	
	@NotNull(message = "benefitEffectiveDate cannot be empty.")
	private String benefitEffectiveDate;
	
	private String benefitEndDate;
	private String dateClosed;
	private String submittedToCarrierDate;

	public String getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public void setBenefitEffectiveDate(String benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public String getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(String benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public String getDateClosed() {
		return dateClosed;
	}
	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}
	public String getSubmittedToCarrierDate() {
		return submittedToCarrierDate;
	}
	public void setSubmittedToCarrierDate(String submittedToCarrierDate) {
		this.submittedToCarrierDate = submittedToCarrierDate;
	}
	
	
	
}
