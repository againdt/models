package com.getinsured.eligibility.referral.ui.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.getinsured.eligibility.enums.ReferralActivationEnum;

/**
 * @author chopra_s
 * 
 */
public class PendingReferralDTO {

	private Long id;

	private String caseNumber;

	private Long ssapApplicationId;

	@Enumerated(EnumType.STRING)
	private ReferralActivationEnum workflowStatus;

	public PendingReferralDTO(Long id, ReferralActivationEnum workflowStatus,
			Long ssapApplicationId, String caseNumber) {
		this.id = id;
		this.caseNumber = caseNumber;
		this.ssapApplicationId = ssapApplicationId;
		this.workflowStatus = workflowStatus;
	}

	public Long getId() {
		return id;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public ReferralActivationEnum getWorkflowStatus() {
		return workflowStatus;
	}

}
