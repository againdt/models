/**
 * 
 * @author santanu
 * @version 1.0
 * @since Jan 27, 2015 
 *
 * This DTO to Vision plan response 
 *
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class VisionPlanResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private List<VisionPlan> visionPlanList;

	
	/**
	 * @return the visionPlanList
	 */
	public List<VisionPlan> getVisionPlanList() {
		return visionPlanList;
	}

	/**
	 * @param visionPlanList the visionPlanList to set
	 */
	public void setVisionPlanList(List<VisionPlan> visionPlanList) {
		this.visionPlanList = visionPlanList;
	}

	/**
	 * Method to send ReST webservice response.
	 *
	 * @return String the response JSON.
	 */
	/*public String sendResponse(){		
		return sendJsonResponse();
	}*/

	/**
	 * Method to send response as JSON. 
	 *
	 * @return String the response JSON.
	 */
	/*private String sendJsonResponse(){
		String transformedResponse = null;
		
		XStream jsoner = new XStream(new JsonHierarchicalStreamDriver() {
		    public HierarchicalStreamWriter createWriter(Writer writer) {
		        return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
		    }
		});
		
		Validate.notNull(jsoner, "XStream instance improperly confugured.");
		
		transformedResponse = jsoner.toXML(this);
		
		return transformedResponse;
	}*/

}
