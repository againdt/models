package com.getinsured.hix.dto.eapp.workflow;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class Workflow {
	private List<WorkflowStep> steps = new ArrayList<WorkflowStep>();
    private List<Flag> flags = new ArrayList<Flag>();
    

	public void add(WorkflowStep step){
		steps.add(step);
	}
	
	public List<WorkflowStep> getSteps() {
		return steps;
	}

	public void setSteps(List<WorkflowStep> steps) {
		this.steps = steps;
	}

    public List<Flag> getFlags() {
        return flags;
    }

    public void setFlags(List<Flag> flags) {
        this.flags = flags;
    }

    public boolean getFlag(FlagName name){
        if(name==null){
            return false;
        }
        for(Flag flag : this.flags){
            if(flag.getName().equals(name)){
                return flag.isEnabled();
            }
        }
        return false;
    }

    public static void main(String args[]){
		Workflow flow = new Workflow();
		
		WorkflowStep emailStep = new WorkflowStep();
		emailStep.setArgs(new EmailWorkflowArgs("test@abc.com", "subject", "empty body"));
		emailStep.setEnabled(true);
		emailStep.setName(WorkflowName.EMAIL);
		flow.add(emailStep);
		flow.add(new WorkflowStep(WorkflowName.SENDAPP));
		WorkflowStep ticketStep = new WorkflowStep();
		TicketWorkflowArgs ticketArgs = new TicketWorkflowArgs();
		ticketArgs.setDescription("This needs to be done today");
		ticketArgs.setPriority("Critical");
		ticketArgs.setTicketType("Data Update");
		ticketArgs.setWorkgroup("Data update Workgroup");
		ticketArgs.setSubject("D2C Enrollment");
		ticketStep.setArgs(ticketArgs);
		ticketStep.setEnabled(true);
		ticketStep.setName(WorkflowName.TICKET);
		flow.add(ticketStep);
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		System.out.println(gson.toJson(flow));
		
	}

}
