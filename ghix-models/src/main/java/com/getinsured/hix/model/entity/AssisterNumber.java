package com.getinsured.hix.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for generating the unique Assister Number for Assister
 * 
 */
@Entity
@Table(name = "ASSISTER_NUMBER")
public class AssisterNumber implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSISTER_NUMBER_SEQ")
	@SequenceGenerator(name = "ASSISTER_NUMBER_SEQ", sequenceName = "ASSISTER_NUMBER_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "EE_ASSISTER_ID")
	private Integer assisterId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getAssisterId() {
		return assisterId;
	}

	public void setAssisterId(Integer assisterId) {
		this.assisterId = assisterId;
	}

	@Override
	public String toString() {
		return "AssisterNumber details: AssisterId = "+assisterId+", ID = "+id;
	}
}
