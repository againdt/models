//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.exchange._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityRequestType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ResponseType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.ffe.ee.applicant_eligibility.exchange._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ApplicantEligibilityRequest_QNAME = new QName("http://applicant-eligibility.ee.ffe.cms.gov/exchange/1.0", "ApplicantEligibilityRequest");
    private final static QName _Response_QNAME = new QName("http://applicant-eligibility.ee.ffe.cms.gov/exchange/1.0", "Response");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.ffe.ee.applicant_eligibility.exchange._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicantEligibilityRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applicant-eligibility.ee.ffe.cms.gov/exchange/1.0", name = "ApplicantEligibilityRequest")
    public JAXBElement<ApplicantEligibilityRequestType> createApplicantEligibilityRequest(ApplicantEligibilityRequestType value) {
        return new JAXBElement<ApplicantEligibilityRequestType>(_ApplicantEligibilityRequest_QNAME, ApplicantEligibilityRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://applicant-eligibility.ee.ffe.cms.gov/exchange/1.0", name = "Response")
    public JAXBElement<ResponseType> createResponse(ResponseType value) {
        return new JAXBElement<ResponseType>(_Response_QNAME, ResponseType.class, null, value);
    }

}
