/**
 * Created: Nov 3, 2014
 */
package com.getinsured.hix.model.consumer;

import java.util.Date;

/**
 * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
 * DTO to store FFM response for a member in SSAP_APPLICANTS table
 */
public class ProxyFFMMemberResponse{

	private String firstName;
	private String middleName;
	private String lastName;
	private Date birthDate;
	private String insuranceApplicantId;
	private String familyRelationshipCode;
	private String personId;
	private Long ffeApplicantId;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getInsuranceApplicantId() {
		return insuranceApplicantId;
	}

	public void setInsuranceApplicantId(String insuranceApplicantId) {
		this.insuranceApplicantId = insuranceApplicantId;
	}

	public String getFamilyRelationshipCode() {
		return familyRelationshipCode;
	}

	public void setFamilyRelationshipCode(String familyRelationshipCode) {
		this.familyRelationshipCode = familyRelationshipCode;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public Long getFfeApplicantId() {
		return ffeApplicantId;
	}

	public void setFfeApplicantId(Long ffeApplicantId) {
		this.ffeApplicantId = ffeApplicantId;
	}

}
