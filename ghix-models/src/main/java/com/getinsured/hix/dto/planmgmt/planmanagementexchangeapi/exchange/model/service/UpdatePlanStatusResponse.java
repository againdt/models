
package com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.PlanStatus;


/**
 * <p>Java class for UpdatePlanStatusResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="UpdatePlanStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.serff.com/planManagementExchangeApi/common/model/pm}planId"/>
 *         &lt;element name="planStatus" type="{http://www.serff.com/planManagementExchangeApi/common/model/pm}PlanStatus"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "planId",
    "planStatus"
})
@XmlRootElement(name="updatePlanStatusResponse")
public class UpdatePlanStatusResponse {

    @XmlElement(namespace = "http://www.serff.com/planManagementExchangeApi/common/model/pm", required = true)
    protected String planId;
    @XmlElement(required = true)
    protected PlanStatus planStatus;

    /**
     * Gets the value of the planId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the planStatus property.
     *
     * @return
     *     possible object is
     *     {@link PlanStatus }
     *
     */
    public PlanStatus getPlanStatus() {
        return planStatus;
    }

    /**
     * Sets the value of the planStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link PlanStatus }
     *
     */
    public void setPlanStatus(PlanStatus value) {
        this.planStatus = value;
    }

	@Override
	public String toString() {
		return "UpdatePlanStatusResponse [planId=" + planId + ", planStatus="
				+ planStatus + "]";
	}

}
