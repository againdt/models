/**
 * 
 */
package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrolleeDetailsDto;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberDTO;
import com.getinsured.hix.model.GHIXResponse;

/**
 * @author panda_p
 *
 */
public class EnrolleeResponse extends GHIXResponse implements Serializable {

	private Enrollment enrollment=null;
	private Enrollee enrollee=null;
	private Map<String, Object> searchResultAndCount =null;
	private List<EnrollmentShopDTO> enrollmentShopDTOList =null;
	private List<EnrolleeShopDTO> enrolleeShopDTOList =null;
	private EnrolleeDetailsDto enrolleeDetailsDto = null;
	private List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList;
	private Long totalRecordCount;
	
	/**
	 * 
	 */
	public EnrolleeResponse() {
		// TODO Auto-generated constructor stub
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public Enrollee getEnrollee() {
		return enrollee;
	}

	public void setEnrollee(Enrollee enrollee) {
		this.enrollee = enrollee;
	}

	
	public Map<String, Object> getSearchResultAndCount() {
		return searchResultAndCount;
	}

	public void setSearchResultAndCount(Map<String, Object> searchResultAndCount) {
		this.searchResultAndCount = searchResultAndCount;
	}

	public List<EnrollmentShopDTO> getEnrollmentShopDTOList() {
		return enrollmentShopDTOList;
	}

	public void setEnrollmentShopDTOList(List<EnrollmentShopDTO> enrollmentShopDTOList) {
		this.enrollmentShopDTOList = enrollmentShopDTOList;
	}

	public List<EnrolleeShopDTO> getEnrolleeShopDTOList() {
		return enrolleeShopDTOList;
	}

	public void setEnrolleeShopDTOList(List<EnrolleeShopDTO> enrolleeShopDTOList) {
		this.enrolleeShopDTOList = enrolleeShopDTOList;
	}

	@Override
	public String toString() {
		return "EnroleeResponse [enrollment=" + enrollment + ", enrollee="
				+ enrollee + ", searchResultAndCount="
				+ searchResultAndCount + "]";
	}

	/**
	 * @return the enrolleeDetailsDto
	 */
	public EnrolleeDetailsDto getEnrolleeDetailsDto() {
		return enrolleeDetailsDto;
	}

	/**
	 * @param enrolleeDetailsDto the enrolleeDetailsDto to set
	 */
	public void setEnrolleeDetailsDto(EnrolleeDetailsDto enrolleeDetailsDto) {
		this.enrolleeDetailsDto = enrolleeDetailsDto;
	}

	public List<EnrollmentSubscriberDTO> getEnrollmentSubscriberDTOList() {
		return enrollmentSubscriberDTOList;
	}

	public void setEnrollmentSubscriberDTOList(List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList) {
		this.enrollmentSubscriberDTOList = enrollmentSubscriberDTOList;
	}

	public Long getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(Long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}
}
