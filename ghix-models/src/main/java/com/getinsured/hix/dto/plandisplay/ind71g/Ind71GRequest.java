package com.getinsured.hix.dto.plandisplay.ind71g;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Ind71GRequest {

	
    private String userRoleId;
    private String userRoleType;

    @NotNull(message="enrollmentType"+IND71Enums.REQUIRED_FIELD)
    private String enrollmentType;
    
    @NotNull(message="applicationId"+IND71Enums.REQUIRED_FIELD)
    @Size(min=1, max=10,message="applicationId"+IND71Enums.LENGTH_ERROR)
    private String applicationId;
    
    @NotNull(message="householdCaseId"+IND71Enums.REQUIRED_FIELD)
    private String householdCaseId;
    
    @Pattern(regexp=IND71Enums.YES_NO_REGEX,message="allowChangePlan" + IND71Enums.INVALID_REGEX_ERROR)
    private String allowChangePlan;
    
    @Valid
    private IND71GResponsiblePerson responsiblePerson;
    
    @Valid
    private IND71GHouseHoldContact houseHoldContact;
    
    @Valid
    private List<IND71GEnrollment> enrollments = null;
    
    private Float ehbAmount;

    public String getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(String userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserRoleType() {
        return userRoleType;
    }

    public void setUserRoleType(String userRoleType) {
        this.userRoleType = userRoleType;
    }

    public String getEnrollmentType() {
        return enrollmentType;
    }

    public void setEnrollmentType(String enrollmentType) {
        this.enrollmentType = enrollmentType;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getHouseholdCaseId() {
        return householdCaseId;
    }

    public void setHouseholdCaseId(String householdCaseId) {
        this.householdCaseId = householdCaseId;
    }

    public IND71GResponsiblePerson getResponsiblePerson() {
        return responsiblePerson;
    }

    public void setResponsiblePerson(IND71GResponsiblePerson responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    public IND71GHouseHoldContact getHouseHoldContact() {
        return houseHoldContact;
    }

    public void setHouseHoldContact(IND71GHouseHoldContact houseHoldContact) {
        this.houseHoldContact = houseHoldContact;
    }

    public List<IND71GEnrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<IND71GEnrollment> enrollments) {
        this.enrollments = enrollments;
    }

	public String getAllowChangePlan() {
		return allowChangePlan;
	}

	public void setAllowChangePlan(String allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}
	
	public Float getEhbAmount() {
		return ehbAmount;
	}

	public void setEhbAmount(Float ehbAmount) {
		this.ehbAmount = ehbAmount;
	}
	
}
