package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

/**
 * @since 25th June 2015
 * @author Sharma_k
 * @version 1.0
 * Jira ID: HIX-70358
 * Created DTO class for providing Enrollee data through exposed API
 *
 */
public class EnrolleeDataDTO implements Serializable
{
	private Integer enrollmentId;
	private String suffix;
	private String firstName;
	private String middleName;
	private String lastName;
	private Date enrolleeEffectiveStartDate;
	private Date enrolleeEffectiveEndDate;
	private String effectiveStartDate;
	private String effectiveEndDate;
	private String coveragePolicyNumber;
	private String relationshipToSubscriber;//•	Relationship to Primary Account Holder
	private String enrolleeAddress1;
	private String enrolleeAddress2;
	private String enrolleeCity;
	private String enrolleeState;
	private String enrolleeZipcode;
	private String enrolleeCounty;
	private String enrolleeCountyCode;
	//HIX-71635	Add additional fields for CAP
	private String phoneNumber;
	private String emailAddress;
	private String memberId;
	/*private String relationshipToHouseHoldContact;*/
	private Date birthDate;
	private String genderLookupCode;
	private String genderLookupLabel;
	private String taxIdentificationNumber;
	private Integer enrolleeId;
	private String SubscriberFlag;
	private String enrolleeStatus;
	
	//Jira HIX-106484
	private String tobaccoStatus;
	private String mailingAddress1;
	private String mailingAddress2;
	private String mailingCity;
	private String mailingState;
	private String mailingZipcode;
	private String mailingCounty;
	private String mailingCountyCode;
	private String ratingArea;
	private Date ratingAreaEffectiveDate;
	private String relationshipToHouseHoldContact;
	private Integer age;
	
	private String relationshipToSubscriberCode;
	private String tobaccoStatusCode;
	private String relationshipToHouseHoldContactCode;
	private String eventType;
	private String eventReason;
	
	private Date quotingDate;
	private String personType;
	
	private Float totalIndvResponsibleAmt;
	private Date lastPremiumPaidDate;                
	private Date terminationDate;                       
	private String terminationReasonLabel;
	private String terminationReasonCode;

	
	/**
	 * @return the enrollmentId
	 */
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	/**
	 * @param enrollmentId the enrollmentId to set
	 */
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the enrolleeEffectiveStartDate
	 */
	public Date getEnrolleeEffectiveStartDate() {
		return enrolleeEffectiveStartDate;
	}
	/**
	 * @param enrolleeEffectiveStartDate the enrolleeEffectiveStartDate to set
	 */
	public void setEnrolleeEffectiveStartDate(Date enrolleeEffectiveStartDate) {
		this.enrolleeEffectiveStartDate = enrolleeEffectiveStartDate;
	}
	/**
	 * @return the enrolleeEffectiveEndDate
	 */
	public Date getEnrolleeEffectiveEndDate() {
		return enrolleeEffectiveEndDate;
	}
	/**
	 * @param enrolleeEffectiveEndDate the enrolleeEffectiveEndDate to set
	 */
	public void setEnrolleeEffectiveEndDate(Date enrolleeEffectiveEndDate) {
		this.enrolleeEffectiveEndDate = enrolleeEffectiveEndDate;
	}
	/**
	 * @return the coveragePolicyNumber
	 */
	public String getCoveragePolicyNumber() {
		return coveragePolicyNumber;
	}
	/**
	 * @param coveragePolicyNumber the coveragePolicyNumber to set
	 */
	public void setCoveragePolicyNumber(String coveragePolicyNumber) {
		this.coveragePolicyNumber = coveragePolicyNumber;
	}
	/**
	 * @return the relationshipToSubscriber
	 */
	public String getRelationshipToSubscriber() {
		return relationshipToSubscriber;
	}
	/**
	 * @param relationshipToSubscriber the relationshipToSubscriber to set
	 */
	public void setRelationshipToSubscriber(String relationshipToSubscriber) {
		this.relationshipToSubscriber = relationshipToSubscriber;
	}
	/**
	 * @return the enrolleeAddress1
	 */
	public String getEnrolleeAddress1() {
		return enrolleeAddress1;
	}
	/**
	 * @param enrolleeAddress1 the enrolleeAddress1 to set
	 */
	public void setEnrolleeAddress1(String enrolleeAddress1) {
		this.enrolleeAddress1 = enrolleeAddress1;
	}
	/**
	 * @return the enrolleeAddress2
	 */
	public String getEnrolleeAddress2() {
		return enrolleeAddress2;
	}
	/**
	 * @param enrolleeAddress2 the enrolleeAddress2 to set
	 */
	public void setEnrolleeAddress2(String enrolleeAddress2) {
		this.enrolleeAddress2 = enrolleeAddress2;
	}
	/**
	 * @return the enrolleeCity
	 */
	public String getEnrolleeCity() {
		return enrolleeCity;
	}
	/**
	 * @param enrolleeCity the enrolleeCity to set
	 */
	public void setEnrolleeCity(String enrolleeCity) {
		this.enrolleeCity = enrolleeCity;
	}
	/**
	 * @return the enrolleeState
	 */
	public String getEnrolleeState() {
		return enrolleeState;
	}
	/**
	 * @param enrolleeState the enrolleeState to set
	 */
	public void setEnrolleeState(String enrolleeState) {
		this.enrolleeState = enrolleeState;
	}
	/**
	 * @return the enrolleeZipcode
	 */
	public String getEnrolleeZipcode() {
		return enrolleeZipcode;
	}
	/**
	 * @param enrolleeZipcode the enrolleeZipcode to set
	 */
	public void setEnrolleeZipcode(String enrolleeZipcode) {
		this.enrolleeZipcode = enrolleeZipcode;
	}
	/**
	 * @return the enrolleeCounty
	 */
	public String getEnrolleeCounty() {
		return enrolleeCounty;
	}
	/**
	 * @param enrolleeCounty the enrolleeCounty to set
	 */
	public void setEnrolleeCounty(String enrolleeCounty) {
		this.enrolleeCounty = enrolleeCounty;
	}
	/**
	 * @return the enrolleeCountyCode
	 */
	public String getEnrolleeCountyCode() {
		return enrolleeCountyCode;
	}
	/**
	 * @param enrolleeCountyCode the enrolleeCountyCode to set
	 */
	public void setEnrolleeCountyCode(String enrolleeCountyCode) {
		this.enrolleeCountyCode = enrolleeCountyCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	/**
	 * @return the relationshipToHouseHoldContact
	 */
	/*public String getRelationshipToHouseHoldContact() {
		return relationshipToHouseHoldContact;
	}*/
	/**
	 * @param relationshipToHouseHoldContact the relationshipToHouseHoldContact to set
	 */
	/*public void setRelationshipToHouseHoldContact(String relationshipToHouseHoldContact) {
		this.relationshipToHouseHoldContact = relationshipToHouseHoldContact;
	}*/
	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}
	/**
	 * @param suffix the suffix to set
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}
	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the taxIdentificationNumber
	 */
	public String getTaxIdentificationNumber() {
		return taxIdentificationNumber;
	}
	/**
	 * @param taxIdentificationNumber the taxIdentificationNumber to set
	 */
	public void setTaxIdentificationNumber(String taxIdentificationNumber) {
		this.taxIdentificationNumber = taxIdentificationNumber;
	}
	/**
	 * @return the genderLookupCode
	 */
	public String getGenderLookupCode() {
		return genderLookupCode;
	}
	/**
	 * @param genderLookupCode the genderLookupCode to set
	 */
	public void setGenderLookupCode(String genderLookupCode) {
		this.genderLookupCode = genderLookupCode;
	}
	/**
	 * @return the genderLookupLabel
	 */
	public String getGenderLookupLabel() {
		return genderLookupLabel;
	}
	/**
	 * @param genderLookupLabel the genderLookupLabel to set
	 */
	public void setGenderLookupLabel(String genderLookupLabel) {
		this.genderLookupLabel = genderLookupLabel;
	}
	
	public Integer getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	public String getEnrolleeStatus() {
		return enrolleeStatus;
	}
	public void setEnrolleeStatus(String enrolleeStatus) {
		this.enrolleeStatus = enrolleeStatus;
	}
	public String getSubscriberFlag() {
		return SubscriberFlag;
	}
	public void setSubscriberFlag(String subscriberFlag) {
		SubscriberFlag = subscriberFlag;
	}
	/**
	 * @return the tobaccoStatus
	 */
	public String getTobaccoStatus() {
		return tobaccoStatus;
	}
	/**
	 * @param tobaccoStatus the tobaccoStatus to set
	 */
	public void setTobaccoStatus(String tobaccoStatus) {
		this.tobaccoStatus = tobaccoStatus;
	}
	/**
	 * @return the mailingAddress1
	 */
	public String getMailingAddress1() {
		return mailingAddress1;
	}
	/**
	 * @param mailingAddress1 the mailingAddress1 to set
	 */
	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}
	/**
	 * @return the mailingAddress2
	 */
	public String getMailingAddress2() {
		return mailingAddress2;
	}
	/**
	 * @param mailingAddress2 the mailingAddress2 to set
	 */
	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}
	/**
	 * @return the mailingCity
	 */
	public String getMailingCity() {
		return mailingCity;
	}
	/**
	 * @param mailingCity the mailingCity to set
	 */
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	/**
	 * @return the mailingState
	 */
	public String getMailingState() {
		return mailingState;
	}
	/**
	 * @param mailingState the mailingState to set
	 */
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}
	/**
	 * @return the mailingZipcode
	 */
	public String getMailingZipcode() {
		return mailingZipcode;
	}
	/**
	 * @param mailingZipcode the mailingZipcode to set
	 */
	public void setMailingZipcode(String mailingZipcode) {
		this.mailingZipcode = mailingZipcode;
	}
	/**
	 * @return the mailingCounty
	 */
	public String getMailingCounty() {
		return mailingCounty;
	}
	/**
	 * @param mailingCounty the mailingCounty to set
	 */
	public void setMailingCounty(String mailingCounty) {
		this.mailingCounty = mailingCounty;
	}
	/**
	 * @return the mailingCountyCode
	 */
	public String getMailingCountyCode() {
		return mailingCountyCode;
	}
	/**
	 * @param mailingCountyCode the mailingCountyCode to set
	 */
	public void setMailingCountyCode(String mailingCountyCode) {
		this.mailingCountyCode = mailingCountyCode;
	}
	/**
	 * @return the ratingArea
	 */
	public String getRatingArea() {
		return ratingArea;
	}
	/**
	 * @param ratingArea the ratingArea to set
	 */
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}
	/**
	 * @return the ratingAreaEffectiveDate
	 */
	public Date getRatingAreaEffectiveDate() {
		return ratingAreaEffectiveDate;
	}
	/**
	 * @param ratingAreaEffectiveDate the ratingAreaEffectiveDate to set
	 */
	public void setRatingAreaEffectiveDate(Date ratingAreaEffectiveDate) {
		this.ratingAreaEffectiveDate = ratingAreaEffectiveDate;
	}
	/**
	 * @return the relationshipToHouseHoldContact
	 */
	public String getRelationshipToHouseHoldContact() {
		return relationshipToHouseHoldContact;
	}
	/**
	 * @param relationshipToHouseHoldContact the relationshipToHouseHoldContact to set
	 */
	public void setRelationshipToHouseHoldContact(String relationshipToHouseHoldContact) {
		this.relationshipToHouseHoldContact = relationshipToHouseHoldContact;
	}
	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	/**
	 * @return the relationshipToSubscriberCode
	 */
	public String getRelationshipToSubscriberCode() {
		return relationshipToSubscriberCode;
	}
	/**
	 * @param relationshipToSubscriberCode the relationshipToSubscriberCode to set
	 */
	public void setRelationshipToSubscriberCode(String relationshipToSubscriberCode) {
		this.relationshipToSubscriberCode = relationshipToSubscriberCode;
	}
	/**
	 * @return the tobaccoUsageCode
	 */
	public String getTobaccoStatusCode() {
		return tobaccoStatusCode;
	}
	/**
	 * @param tobaccoUsageCode the tobaccoUsageCode to set
	 */
	public void setTobaccoStatusCode(String tobaccoStatusCode) {
		this.tobaccoStatusCode = tobaccoStatusCode;
	}
	/**
	 * @return the relationshipToHouseHoldContactCode
	 */
	public String getRelationshipToHouseHoldContactCode() {
		return relationshipToHouseHoldContactCode;
	}
	/**
	 * @param relationshipToHouseHoldContactCode the relationshipToHouseHoldContactCode to set
	 */
	public void setRelationshipToHouseHoldContactCode(String relationshipToHouseHoldContactCode) {
		this.relationshipToHouseHoldContactCode = relationshipToHouseHoldContactCode;
	}
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventReason() {
		return eventReason;
	}
	public void setEventReason(String eventReason) {
		this.eventReason = eventReason;
	}
	public Date getQuotingDate() {
		return quotingDate;
	}
	public void setQuotingDate(Date quotingDate) {
		this.quotingDate = quotingDate;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public Float getTotalIndvResponsibleAmt() {
		return totalIndvResponsibleAmt;
	}
	public void setTotalIndvResponsibleAmt(Float totalIndvResponsibleAmt) {
		this.totalIndvResponsibleAmt = totalIndvResponsibleAmt;
	}
	public Date getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}
	public void setLastPremiumPaidDate(Date lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}
	public Date getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	public String getTerminationReasonLabel() {
		return terminationReasonLabel;
	}
	public void setTerminationReasonLabel(String terminationReasonLabel) {
		this.terminationReasonLabel = terminationReasonLabel;
	}
	public String getTerminationReasonCode() {
		return terminationReasonCode;
	}
	public void setTerminationReasonCode(String terminationReasonCode) {
		this.terminationReasonCode = terminationReasonCode;
	}
	@Override
	public String toString() {
		return "EnrolleeDataDTO [enrollmentId=" + enrollmentId + ", suffix=" + suffix + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName + ", enrolleeEffectiveStartDate="
				+ enrolleeEffectiveStartDate + ", enrolleeEffectiveEndDate=" + enrolleeEffectiveEndDate
				+ ", effectiveStartDate=" + effectiveStartDate + ", effectiveEndDate=" + effectiveEndDate
				+ ", coveragePolicyNumber=" + coveragePolicyNumber + ", relationshipToSubscriber="
				+ relationshipToSubscriber + ", enrolleeAddress1=" + enrolleeAddress1 + ", enrolleeAddress2="
				+ enrolleeAddress2 + ", enrolleeCity=" + enrolleeCity + ", enrolleeState=" + enrolleeState
				+ ", enrolleeZipcode=" + enrolleeZipcode + ", enrolleeCounty=" + enrolleeCounty
				+ ", enrolleeCountyCode=" + enrolleeCountyCode + ", phoneNumber=" + phoneNumber + ", emailAddress="
				+ emailAddress + ", memberId=" + memberId + ", birthDate=" + birthDate + ", genderLookupCode="
				+ genderLookupCode + ", genderLookupLabel=" + genderLookupLabel + ", taxIdentificationNumber="
				+ taxIdentificationNumber + ", enrolleeId=" + enrolleeId + ", SubscriberFlag=" + SubscriberFlag
				+ ", enrolleeStatus=" + enrolleeStatus + ", tobaccoStatus=" + tobaccoStatus + ", mailingAddress1="
				+ mailingAddress1 + ", mailingAddress2=" + mailingAddress2 + ", mailingCity=" + mailingCity
				+ ", mailingState=" + mailingState + ", mailingZipcode=" + mailingZipcode + ", mailingCounty="
				+ mailingCounty + ", mailingCountyCode=" + mailingCountyCode + ", ratingArea=" + ratingArea
				+ ", ratingAreaEffectiveDate=" + ratingAreaEffectiveDate + ", relationshipToHouseHoldContact="
				+ relationshipToHouseHoldContact + ", age=" + age + ", relationshipToSubscriberCode="
				+ relationshipToSubscriberCode + ", tobaccoStatusCode=" + tobaccoStatusCode
				+ ", relationshipToHouseHoldContactCode=" + relationshipToHouseHoldContactCode + ", eventType="
				+ eventType + ", eventReason=" + eventReason + ", quotingDate=" + quotingDate + ", personType="
				+ personType + "]";
	}
	
	
}
