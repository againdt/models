INSERT INTO LOOKUP_TYPE (LOOKUP_TYPE_ID, NAME, DATA_TYPE) VALUES (LOOKUPTYPE_SEQ.NEXTVAL,'SERFF_FAMILY_OPTION','string');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1001','Couple','Rate of a couple based on the pairing of a primary enrollee and a secondary subscriber (e.g. husband and spouse)');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1002','Primary Subscriber and One Dependent','Rate of a family based on a single parent with one dependent');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1003','Primary Subscriber and Two Dependents','Rate of a family based on a single parent with two dependents');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1004','Primary Subscriber and Three or More Dependents','Rate of a family based on a single parent with three or more dependents');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1005','Couple and One Dependent','Rate of a family based on a couple with one dependent');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1006','Couple and Two Dependents','Rate of a family based on a couple with two dependents');

INSERT INTO LOOKUP_VALUE ( LOOKUP_VALUE_ID,LOOKUP_TYPE_ID, LOOKUP_VALUE_CODE, LOOKUP_VALUE_LABEL, DESCRIPTION)  VALUES ( LOOKUPVALUE_SEQ.NEXTVAL,LOOKUPTYPE_SEQ.currval,'1007','Couple and Three or More Dependents','Rate of a family based on a couple with three or more dependents');