package com.getinsured.hix.dto.externalassister;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

public class DesignateAssisterResponse extends GHIXResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private int householdId;
	
	private int assisterUserId;

	private String assisterName;
	
	private String assisterType;
	
	private String federalTaxId;
	
	private String externalAssisterId;
	
	private String agentNpn;
	
	private String isActive;
	
	private String warningMsg;

	public int getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}

	public String getAssisterName() {
		return assisterName;
	}

	public void setAssisterName(String assisterName) {
		this.assisterName = assisterName;
	}

	public String getAssisterType() {
		return assisterType;
	}

	public void setAssisterType(String assisterType) {
		this.assisterType = assisterType;
	}

	public String getFederalTaxId() {
		return federalTaxId;
	}

	public void setFederalTaxId(String federalTaxId) {
		this.federalTaxId = federalTaxId;
	}

	public String getExternalAssisterId() {
		return externalAssisterId;
	}

	public void setExternalAssisterId(String externalAssisterId) {
		this.externalAssisterId = externalAssisterId;
	}

	public String getAgentNpn() {
		return agentNpn;
	}

	public void setAgentNpn(String agentNpn) {
		this.agentNpn = agentNpn;
	}

	public int getAssisterUserId() {
		return assisterUserId;
	}

	public void setAssisterUserId(int assisterUserId) {
		this.assisterUserId = assisterUserId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getWarningMsg() {
		return warningMsg;
	}

	public void setWarningMsg(String warningMsg) {
		this.warningMsg = warningMsg;
	}
	
}


