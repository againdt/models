package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

@SuppressWarnings("serial")
public class EnrollmentPersonalInfo implements Serializable{

	String txtFullName;
	String txtEmailId;
	String txtAddress;
	String txtPhone;
	String enrolleeId;
	
	public String getTxtFullName() {
		return txtFullName;
	}
	public void setTxtFullName(String txtFullName) {
		this.txtFullName = txtFullName;
	}
	public String getTxtEmailId() {
		return txtEmailId;
	}
	public void setTxtEmailId(String txtEmailId) {
		this.txtEmailId = txtEmailId;
	}
	public String getTxtAddress() {
		return txtAddress;
	}
	public void setTxtAddress(String txtAddress) {
		this.txtAddress = txtAddress;
	}
	public String getTxtPhone() {
		return txtPhone;
	}
	public void setTxtPhone(String txtPhone) {
		this.txtPhone = txtPhone;
	}
	public String getEnrolleeId() {
		return enrolleeId;
	}
	public void setEnrolleeId(String enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	
}
