package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AudId;
import com.getinsured.hix.model.DateAdapter;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.TenantIdPrePersistListener;

/**
 * The persistent class for the roles database table.
 * 
 */

@Entity
@IdClass(AudId.class)
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "ENROLLEE_AUD")
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
@EntityListeners(TenantIdPrePersistListener.class)
public class EnrolleeAud implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@XmlElement(name="id")
	private int id;
	
	@Id
	@Column(name = "REV")
	private int rev;
	
	@Column(name = "REVTYPE")
	private int revType;
	
	@ManyToOne
	@JoinColumn(name = "ENROLLMENT_ID")
	private Enrollment enrollment;
	
	@XmlElement(name="SSN")
	@Column(name = "TAX_ID_NUMBER")
	private String taxIdNumber;
	
	@OneToOne
	@JoinColumn(name = "MEMBER_EMPLOYER_ID", nullable = true)
	private Employer employer;

	
	@XmlElement(name="relationshipLkp")
	@OneToOne
	@JoinColumn(name = "RELATIONSHIP_HCP_LKP")
	private LookupValue relationshipToHCPLkp;
	
	@XmlElement(name="enrolleeLkpValue")
	@OneToOne
	@JoinColumn(name = "ENROLLEE_STATUS_LKP")
	private LookupValue enrolleeLkpValue;

	@XmlElement(name="exchgIndivIdentifier")
	@Column(name = "EXCHG_INDIV_IDENTIFIER")
	private String exchgIndivIdentifier;

	@XmlElement(name="issuerIndivIdentifier")
	@Column(name = "ISSUER_INDIV_IDENTIFIER")
	private String issuerIndivIdentifier;
	
	@XmlElement(name="healthCoveragePolicyNo")
	@Column(name = "HEALTH_COVERAGE_POLICY_NO")
	private String healthCoveragePolicyNo;

	@XmlElement(name="benefitEffectiveBeginDate")
	@Column(name = "EFFECTIVE_START_DATE")
	private Date effectiveStartDate;
	
	@XmlElement(name="benefitEffectiveEndDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	@Column(name = "EFFECTIVE_END_DATE")
	private Date effectiveEndDate;
	
	@XmlElement(name="lastName")
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@XmlElement(name="firstName")
	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@XmlElement(name="middleName")
	@Column(name = "MIDDLE_NAME")
	private String middleName;
	
	@XmlElement(name="nameSuffix")
	@Column(name = "SUFFIX")
	private String suffix;

	
	@XmlElement(name="nationalIndivID")
	@Column(name = "NATIONAL_INDIV_IDENTIFIER")
	private String nationalIndividualIdentifier;
	
	@XmlElement(name="primaryPhoneNo")
	@Column(name = "PRIMARY_PHONE_NO")
	private String primaryPhoneNo;
	
	@XmlElement(name="secondaryPhoneNo")
	@Column(name = "SECONDARY_PHONE_NO")
	private String secondaryPhoneNo;

	@XmlElement(name="languageSpokenLkp")
	@OneToOne
	@JoinColumn(name = "LANGUAGE_SPOKEN_LKP")
	private LookupValue languageLkp;

	@XmlElement(name="languageWrittenLkp")
	@OneToOne
	@JoinColumn(name = "LANGUAGE_WRITTEN_LKP")
	private LookupValue languageWrittenLkp;

	@XmlElement(name="preferredSMS")
	@Column(name = "PREF_SMS")
	private String preferredSMS;
	
	@XmlElement(name="preferredEmail")
	@Column(name = "PREF_EMAIL")
	private String preferredEmail;

	@XmlElement(name="homeAddress")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "HOME_ADDRESS_ID")
	private Location homeAddressid;

	
	@XmlElement(name="birthDate")
	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@XmlElement(name="totalIndivResponsibilityAmt")
	@Column(name = "TOTAL_INDV_RESPONSIBILITY_AMT")
	private Float totalIndvResponsibilityAmt;

	@XmlElement(name="genderLkp")
	@OneToOne
	@JoinColumn(name = "GENDER_LKP")
	private LookupValue genderLkp;

	@XmlElement(name="maritalStatusLkp")
	@OneToOne
	@JoinColumn(name = "MARITAL_STATUS_LKP")
	private LookupValue maritalStatusLkp;

	@XmlElement(name="citizenshipStatusLkp")
	@OneToOne
	@JoinColumn(name = "CITIZENSHIP_STATUS_LKP")
	private LookupValue citizenshipStatusLkp;

	@XmlElement(name="tobaccoUsageLkp")
	@OneToOne 
	@JoinColumn(name = "TOBACCO_USAGE_LKP")
	private LookupValue tobaccoUsageLkp; 
	
	@XmlElement(name="mailingAddress")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MAILING_ADDRESS_ID")
	private Location mailingAddressId;

	@XmlElement(name="maintenanceEffectiveDate")
	@Column(name = "MAINTENANCE_EFFECTIVE_DATE")
	private Date maintenanceEffectiveDate;

	@XmlElement(name="ratingArea")
	@Column(name = "RATING_AREA")
	private String ratingArea;

	@XmlElement(name="lastPremiumPaidDate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	@Column(name = "LAST_PREMIUM_PAID_DATE")
	private Date lastPremiumPaidDate;

	@XmlElement(name="TotEmpResponsibilityAmt")
	@Column(name = "TOT_EMP_RESPONSIBILITY_AMT")
	private Float TotEmpResponsibilityAmt;

	@XmlElement(name="idCode")
	@Column(name = "RESP_PERSON_ID_CODE")
	private String respPersonIdCode;

	@XmlElement(name="custodialParent")
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "CUSTODIAL_PARENT_ID")
	private Enrollee custodialParent;
	
	@XmlElement(name="healthCoveragePremiumDate")
	@Column(name="HEALTH_COV_PREM_EFF_DATE")
	private Date totIndvRespEffDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;
	
    @XmlElement(name="personType")
	@OneToOne
	@JoinColumn(name = "PERSON_TYPE_LKP")
	private LookupValue personTypeLkp;
	
	@OneToOne
	@JoinColumn(name="created_by")
	private AccountUser createdBy;
	
	@OneToOne
	@JoinColumn(name="LAST_UPDATED_BY")
	private AccountUser updatedBy;
   	
	@XmlElement(name="memberIndivDeathDate")
	@Column(name = "DEATH_DATE")
	private Date deathDate;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "LAST_EVENT_ID")
	private EnrollmentEvent lastEventId;
	
	@Column(name = "HEIGHT")
	private Float height;
	
	@Column(name = "WEIGHT")
	private Float weight;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "DISENROLL_TIMESTAMP")
	private Date disenrollTimestamp;
	
	@XmlElement(name="ratingAreaEffDate")
	@Column(name = "RATING_AREA_EFF_DATE")
	private Date ratingAreaEffDate;
	
	@Column(name = "LAST_TOBACCO_USE_DATE")
	private Date lastTobaccoUseDate;
	
	@Column(name = "AGE")
	private Integer age;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRev() {
		return rev;
	}

	public void setRev(int rev) {
		this.rev = rev;
	}

	public int getRevType() {
		return revType;
	}

	public void setRevType(int revType) {
		this.revType = revType;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Enrollment enrollment) {
		this.enrollment = enrollment;
	}

	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public LookupValue getRelationshipToHCPLkp() {
		return relationshipToHCPLkp;
	}

	public void setRelationshipToHCPLkp(LookupValue relationshipToHCPLkp) {
		this.relationshipToHCPLkp = relationshipToHCPLkp;
	}

	public LookupValue getEnrolleeLkpValue() {
		return enrolleeLkpValue;
	}

	public void setEnrolleeLkpValue(LookupValue enrolleeLkpValue) {
		this.enrolleeLkpValue = enrolleeLkpValue;
	}

	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}

	public String getIssuerIndivIdentifier() {
		return issuerIndivIdentifier;
	}

	public void setIssuerIndivIdentifier(String issuerIndivIdentifier) {
		this.issuerIndivIdentifier = issuerIndivIdentifier;
	}

	public String getHealthCoveragePolicyNo() {
		return healthCoveragePolicyNo;
	}

	public void setHealthCoveragePolicyNo(String healthCoveragePolicyNo) {
		this.healthCoveragePolicyNo = healthCoveragePolicyNo;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getNationalIndividualIdentifier() {
		return nationalIndividualIdentifier;
	}

	public void setNationalIndividualIdentifier(String nationalIndividualIdentifier) {
		this.nationalIndividualIdentifier = nationalIndividualIdentifier;
	}

	public String getPrimaryPhoneNo() {
		return primaryPhoneNo;
	}

	public void setPrimaryPhoneNo(String primaryPhoneNo) {
		this.primaryPhoneNo = primaryPhoneNo;
	}

	public String getSecondaryPhoneNo() {
		return secondaryPhoneNo;
	}

	public void setSecondaryPhoneNo(String secondaryPhoneNo) {
		this.secondaryPhoneNo = secondaryPhoneNo;
	}

	public LookupValue getLanguageLkp() {
		return languageLkp;
	}

	public void setLanguageLkp(LookupValue languageLkp) {
		this.languageLkp = languageLkp;
	}

	public LookupValue getLanguageWrittenLkp() {
		return languageWrittenLkp;
	}

	public void setLanguageWrittenLkp(LookupValue languageWrittenLkp) {
		this.languageWrittenLkp = languageWrittenLkp;
	}

	public String getPreferredSMS() {
		return preferredSMS;
	}

	public void setPreferredSMS(String preferredSMS) {
		this.preferredSMS = preferredSMS;
	}

	public String getPreferredEmail() {
		return preferredEmail;
	}

	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}

	public Location getHomeAddressid() {
		return homeAddressid;
	}

	public void setHomeAddressid(Location homeAddressid) {
		this.homeAddressid = homeAddressid;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Float getTotalIndvResponsibilityAmt() {
		return totalIndvResponsibilityAmt;
	}

	public void setTotalIndvResponsibilityAmt(Float totalIndvResponsibilityAmt) {
		this.totalIndvResponsibilityAmt = totalIndvResponsibilityAmt;
	}

	public LookupValue getGenderLkp() {
		return genderLkp;
	}

	public void setGenderLkp(LookupValue genderLkp) {
		this.genderLkp = genderLkp;
	}

	public LookupValue getMaritalStatusLkp() {
		return maritalStatusLkp;
	}

	public void setMaritalStatusLkp(LookupValue maritalStatusLkp) {
		this.maritalStatusLkp = maritalStatusLkp;
	}

	public LookupValue getCitizenshipStatusLkp() {
		return citizenshipStatusLkp;
	}

	public void setCitizenshipStatusLkp(LookupValue citizenshipStatusLkp) {
		this.citizenshipStatusLkp = citizenshipStatusLkp;
	}

	public LookupValue getTobaccoUsageLkp() {
		return tobaccoUsageLkp;
	}

	public void setTobaccoUsageLkp(LookupValue tobaccoUsageLkp) {
		this.tobaccoUsageLkp = tobaccoUsageLkp;
	}

	public Location getMailingAddressId() {
		return mailingAddressId;
	}

	public void setMailingAddressId(Location mailingAddressId) {
		this.mailingAddressId = mailingAddressId;
	}

	public Date getMaintenanceEffectiveDate() {
		return maintenanceEffectiveDate;
	}

	public void setMaintenanceEffectiveDate(Date maintenanceEffectiveDate) {
		this.maintenanceEffectiveDate = maintenanceEffectiveDate;
	}

	public String getRatingArea() {
		return ratingArea;
	}

	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	public Date getLastPremiumPaidDate() {
		return lastPremiumPaidDate;
	}

	public void setLastPremiumPaidDate(Date lastPremiumPaidDate) {
		this.lastPremiumPaidDate = lastPremiumPaidDate;
	}

	public Float getTotEmpResponsibilityAmt() {
		return TotEmpResponsibilityAmt;
	}

	public void setTotEmpResponsibilityAmt(Float totEmpResponsibilityAmt) {
		TotEmpResponsibilityAmt = totEmpResponsibilityAmt;
	}

	public String getRespPersonIdCode() {
		return respPersonIdCode;
	}

	public void setRespPersonIdCode(String respPersonIdCode) {
		this.respPersonIdCode = respPersonIdCode;
	}

	public Enrollee getCustodialParent() {
		return custodialParent;
	}

	public void setCustodialParent(Enrollee custodialParent) {
		this.custodialParent = custodialParent;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public LookupValue getPersonTypeLkp() {
		return personTypeLkp;
	}

	public void setPersonTypeLkp(LookupValue personTypeLkp) {
		this.personTypeLkp = personTypeLkp;
	}

	public AccountUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AccountUser createdBy) {
		this.createdBy = createdBy;
	}

	public AccountUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(AccountUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getTotIndvRespEffDate() {
		return totIndvRespEffDate;
	}

	public void setTotIndvRespEffDate(Date totIndvRespEffDate) {
		this.totIndvRespEffDate = totIndvRespEffDate;
	}

	public EnrollmentEvent getLastEventId() {
		return lastEventId;
	}

	public void setLastEventId(EnrollmentEvent lastEventId) {
		this.lastEventId = lastEventId;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Date getDisenrollTimestamp() {
		return disenrollTimestamp;
	}

	public void setDisenrollTimestamp(Date disenrollTimestamp) {
		this.disenrollTimestamp = disenrollTimestamp;
	}

	public Date getRatingAreaEffDate() {
		return ratingAreaEffDate;
	}

	public void setRatingAreaEffDate(Date ratingAreaEffDate) {
		this.ratingAreaEffDate = ratingAreaEffDate;
	}

	public Date getLastTobaccoUseDate() {
		return lastTobaccoUseDate;
	}

	public void setLastTobaccoUseDate(Date lastTobaccoUseDate) {
		this.lastTobaccoUseDate = lastTobaccoUseDate;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
}
