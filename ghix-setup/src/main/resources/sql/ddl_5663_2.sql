--BROKERS table
COMMENT ON TABLE BROKERS IS 'This table describes information about a Broker.';

--BROKERS_AUD table
COMMENT ON TABLE BROKERS_AUD IS 'This table describes the audit information of Broker.';

--DESIGNATE_BROKER table
COMMENT ON TABLE DESIGNATE_BROKER IS 'This table describes designation related information of Broker';
