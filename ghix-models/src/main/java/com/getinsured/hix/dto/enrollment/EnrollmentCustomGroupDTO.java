/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import com.getinsured.hix.dto.plandisplay.ind71g.IND71GEnrollment;
import com.getinsured.hix.dto.plandisplay.ind71g.Ind71GRequest;
import com.getinsured.hix.model.plandisplay.PdOrderResponse;

/**
 * @author panda_p
 *
 */
public class EnrollmentCustomGroupDTO implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EnrollmentCustomGroupDTO() {
		// TODO Auto-generated constructor stub
	}
	private Ind71GRequest ind71GRequest;
	private PdOrderResponse pdOrderResponse;
	
	public PdOrderResponse getPdOrderResponse() {
		return pdOrderResponse;
	}
	public void setPdOrderResponse(PdOrderResponse pdOrderResponse) {
		this.pdOrderResponse = pdOrderResponse;
	}
	public Ind71GRequest getInd71GRequest() {
		return ind71GRequest;
	}
	public void setInd71GRequest(Ind71GRequest ind71gRequest) {
		ind71GRequest = ind71gRequest;
	}
	
}
