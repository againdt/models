package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class IndividualPlanDetailsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String planId;
	private String enrollmentId;
	private String officeVisit;
	private String genericDrugs;
	private String deductible;
	private String issuerLogo;
	private String issuerName;
	private String planName;
	private String planType;
	private String issuerHiosId;
	
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public String getGenericDrugs() {
		return genericDrugs;
	}
	public void setGenericDrugs(String genericDrugs) {
		this.genericDrugs = genericDrugs;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getIssuerHiosId() {
		return issuerHiosId;
	}
	public void setIssuerHiosId(String issuerHiosId) {
		this.issuerHiosId = issuerHiosId;
	}
	
}
