/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

/**
 * @author shengole_s
 *
 */
public class IssuerBrandNameDTO {
	Integer issuerId;
	String issuerBrandName;
	/**
	 * @return the issuerId
	 */
	public Integer getIssuerId() {
		return issuerId;
	}
	/**
	 * @param issuerId the issuerId to set
	 */
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	/**
	 * @return the issuerBrandName
	 */
	public String getIssuerBrandName() {
		return issuerBrandName;
	}
	/**
	 * @param issuerBrandName the issuerBrandName to set
	 */
	public void setIssuerBrandName(String issuerBrandName) {
		this.issuerBrandName = issuerBrandName;
	}
	
	
}
