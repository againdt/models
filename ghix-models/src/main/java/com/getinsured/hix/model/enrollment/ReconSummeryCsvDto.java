package com.getinsured.hix.model.enrollment;

import static org.apache.commons.lang3.StringUtils.trimToNull;

/**
 * 
 * @author meher_a
 * used to convert csv data to java object
 *
 */
public class ReconSummeryCsvDto {
	
	private String recordCode;
	private String tradingPartnerId;
	private String spoeId;
	private String tenantId;
	private String hiosId;
	private String qhpidLookupKey;
	private String issuerExtractDate;
	private String totalNumberOfRecords;
	private String totalNumberOfSubscribers;
	private String totalNumberOfDependentMembers;
	private String totalPremiumAmount;
	private String totalAppliedAptcAmount;
	
	public String getRecordCode() {
		return recordCode;
	}
	public void setRecordCode(String recordCode) {
		this.recordCode = trimToNull(recordCode);
	}
	public String getTradingPartnerId() {
		return tradingPartnerId;
	}
	public void setTradingPartnerId(String tradingPartnerId) {
		this.tradingPartnerId = trimToNull(tradingPartnerId);
	}
	public String getSpoeId() {
		return spoeId;
	}
	public void setSpoeId(String spoeId) {
		this.spoeId = spoeId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = trimToNull(tenantId);
	}
	public String getHiosId() {
		return hiosId;
	}
	public void setHiosId(String hiosId) {
		this.hiosId = trimToNull(hiosId);
	}
	public String getQhpidLookupKey() {
		return qhpidLookupKey;
	}
	public void setQhpidLookupKey(String qhpidLookupKey) {
		this.qhpidLookupKey = trimToNull(qhpidLookupKey);
	}
	public String getIssuerExtractDate() {
		return issuerExtractDate;
	}
	public void setIssuerExtractDate(String issuerExtractDate) {
		this.issuerExtractDate = trimToNull(issuerExtractDate);
	}
	public String getTotalNumberOfRecords() {
		return totalNumberOfRecords;
	}
	public void setTotalNumberOfRecords(String totalNumberOfRecords) {
		this.totalNumberOfRecords = trimToNull(totalNumberOfRecords);
	}
	public String getTotalNumberOfSubscribers() {
		return totalNumberOfSubscribers;
	}
	public void setTotalNumberOfSubscribers(String totalNumberOfSubscribers) {
		this.totalNumberOfSubscribers = trimToNull(totalNumberOfSubscribers);
	}
	public String getTotalNumberOfDependentMembers() {
		return totalNumberOfDependentMembers;
	}
	public void setTotalNumberOfDependentMembers(String totalNumberOfDependentMembers) {
		this.totalNumberOfDependentMembers = trimToNull(totalNumberOfDependentMembers);
	}
	public String getTotalPremiumAmount() {
		return totalPremiumAmount;
	}
	public void setTotalPremiumAmount(String totalPremiumAmount) {
		this.totalPremiumAmount = trimToNull(totalPremiumAmount);
	}
	public String getTotalAppliedAptcAmount() {
		return totalAppliedAptcAmount;
	}
	public void setTotalAppliedAptcAmount(String totalAppliedAptcAmount) {
		this.totalAppliedAptcAmount = trimToNull(totalAppliedAptcAmount);
	}
	
	@Override
	public String toString() {
		return "ReconsilationCsvDto [recordCode=" + recordCode
				+ ", tradingPartnerId=" + tradingPartnerId + ", spoeId="
				+ spoeId + ", tenantId=" + tenantId + ", hiosId=" + hiosId
				+ ", qhpidLookupKey=" + qhpidLookupKey + ", issuerExtractDate="
				+ issuerExtractDate + ", totalNumberOfRecords="
				+ totalNumberOfRecords + ", totalNumberOfSubscribers="
				+ totalNumberOfSubscribers + ", totalNumberOfDependentMembers="
				+ totalNumberOfDependentMembers + ", totalPremiumAmount="
				+ totalPremiumAmount + ", totalAppliedAptcAmount="
				+ totalAppliedAptcAmount + "]";
	}
	public String toCsv(char separator) {
		String csv =  recordCode + separator
	            + tradingPartnerId +separator
				+ spoeId +separator 
	            + tenantId +separator 
				+ hiosId +separator 
	            + qhpidLookupKey +separator
				+ issuerExtractDate +separator
				+ totalNumberOfRecords +separator
				+ totalNumberOfSubscribers +separator
				+ totalNumberOfDependentMembers +separator
				+ totalPremiumAmount +separator
				+ totalAppliedAptcAmount
				;
		return  csv.replaceAll("null", "");
}
	
	

}
