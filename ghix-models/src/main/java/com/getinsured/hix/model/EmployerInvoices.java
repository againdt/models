package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;


/**
 * The persistent class for the employer plans database table.
 * 
 */

@Audited
@Entity
@Table(name = "employer_invoices")
public class EmployerInvoices implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum InvoiceType 	{ 
		BINDER_INVOICE, NORMAL;
	}
	
	public enum PaidStatus{ DUE,PAID,PARTIALLY_PAID,IN_PROCESS,CANCEL; }
	
	public enum IsManualAdjustment{Y, N;}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employer_invoices_seq")
	@SequenceGenerator(name = "employer_invoices_seq", sequenceName = "employer_invoices_seq", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "employer_id")
	private Employer employer;

	@Column(name = "case_id")
	private String caseId;
	
	@Column(name = "statement_date")
	private Date statementDate;
	
	
	/*@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employer_invoices_no_seq")
	@SequenceGenerator(name = "employer_invoices_no_seq", sequenceName = "employer_invoices_no_seq", allocationSize = 1)*/
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@Column(name = "period_covered")
	private String periodCovered;
	
	@Column(name = "payment_due_date")
	private Date paymentDueDate;
	
	@Column(name = "amount_due_from_last_invoice")
	private BigDecimal amountDueFromLastInvoice;
	
	@Column(name = "total_payment_received")
	private BigDecimal totalPaymentReceived;
	
	@Column(name = "premiums_this_period")
	private BigDecimal premiumThisPeriod;
	
	@Column(name = "adjustments")
	private BigDecimal adjustments;
	
	@Column(name = "exchange_fees")
	private BigDecimal exchangeFees;
	
	@Column(name = "total_amount_due")
	private BigDecimal totalAmountDue;
	
	@Column(name = "amount_enclosed")
	private BigDecimal amountEnclosed;
	
	@Column(name="paid_status", length=5)
	@Enumerated(EnumType.STRING)
	private PaidStatus paidStatus;
	
	// bi-directional many-to-one association to EmployerDetails
	@OneToMany(mappedBy = "employerInvoices", cascade = CascadeType.ALL)
	private List<EmployerInvoiceLineItems> employerInvoiceLineItems;
	
	@Column(name = "ecm_doc_id")
	private String ecmDocId;
	
	@Column(name="invoice_type", length=20)
	@Enumerated(EnumType.STRING)  
	private InvoiceType invoiceType;
	
	@Column(name="is_active")
	private String isActive;
	
	@Column(name="reissue_id")
	private int reissueId;
	
	@Column(name = "gl_code")
	private Integer glCode;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "paid_date_time")
	private Date paidDateTime;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "settlement_date_time")
	private Date settlementDateTime;
	
	@Column(name = "is_adjusted")
	@Enumerated(EnumType.STRING)
	private IsManualAdjustment isAdjusted;
	
	@Column(name = "manual_adj_amt")
	private BigDecimal manualAdjustmentAmt;
	
	@Column(name = "termination_reason")
	private String terminationReason;
	
	@Column(name = "termination_date")
	private Date terminationDate;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	
	/**
	 * Flag to check IssuerInvoices Generated fro this EmployerInvoices
	 *//*
	@Column(name="issuer_invoice_generated", length=1)
	@Enumerated(EnumType.STRING)
	private IssuerInvoicesGeneratedFlag issuerInvoiceGenerated;
	*/
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public List<EmployerInvoiceLineItems> getEmployerInvoiceLineItems() {
		return employerInvoiceLineItems;
	}

	public void setEmployerInvoiceLineItems(
			List<EmployerInvoiceLineItems> employerInvoiceLineItems) {
		this.employerInvoiceLineItems = employerInvoiceLineItems;
	}

	public PaidStatus getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(PaidStatus paidStatus) {
		this.paidStatus = paidStatus;
	}

	public String getEcmDocId() {
		return ecmDocId;
	}

	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}

	public InvoiceType getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public int getReissueId() {
		return reissueId;
	}

	public void setReissueId(int reissueId) {
		this.reissueId = reissueId;
	}

	public Integer getGlCode() {
		return glCode;
	}

	public void setGlCode(Integer glCode) {
		this.glCode = glCode;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	
/*	public IssuerInvoicesGeneratedFlag getIssuerInvoiceGenerated() {
		return issuerInvoiceGenerated;
	}

	
	public void setIssuerInvoiceGenerated(
			IssuerInvoicesGeneratedFlag issuerInvoiceGenerated) {
		this.issuerInvoiceGenerated = issuerInvoiceGenerated;
	}*/
	public BigDecimal getAmountDueFromLastInvoice() {
		return amountDueFromLastInvoice;
	}

	public void setAmountDueFromLastInvoice(BigDecimal amountDueFromLastInvoice) {
		this.amountDueFromLastInvoice = amountDueFromLastInvoice;
	}

	public BigDecimal getTotalPaymentReceived() {
		return totalPaymentReceived;
	}

	public void setTotalPaymentReceived(BigDecimal totalPaymentReceived) {
		this.totalPaymentReceived = totalPaymentReceived;
	}

	public BigDecimal getPremiumThisPeriod() {
		return premiumThisPeriod;
	}

	public void setPremiumThisPeriod(BigDecimal premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}

	public BigDecimal getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(BigDecimal adjustments) {
		this.adjustments = adjustments;
	}

	public BigDecimal getExchangeFees() {
		return exchangeFees;
	}

	public void setExchangeFees(BigDecimal exchangeFees) {
		this.exchangeFees = exchangeFees;
	}

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getAmountEnclosed() {
		return amountEnclosed;
	}

	public void setAmountEnclosed(BigDecimal amountEnclosed) {
		this.amountEnclosed = amountEnclosed;
	}

	public Date getPaidDateTime() {
		return paidDateTime;
	}

	public void setPaidDateTime(Date paidDateTime) {
		this.paidDateTime = paidDateTime;
	}

	public Date getSettlementDateTime() {
		return settlementDateTime;
	}

	public void setSettlementDateTime(Date settlementDateTime) {
		this.settlementDateTime = settlementDateTime;
	}

	public IsManualAdjustment getIsAdjusted() {
		return isAdjusted;
	}

	public void setIsAdjusted(IsManualAdjustment isAdjusted) {
		this.isAdjusted = isAdjusted;
	}

	public BigDecimal getManualAdjustmentAmt() {
		return manualAdjustmentAmt;
	}

	public void setManualAdjustmentAmt(BigDecimal manualAdjustmentAmt) {
		this.manualAdjustmentAmt = manualAdjustmentAmt;
	}	
	
	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String termiantionReason) {
		this.terminationReason = termiantionReason;
	}
	
	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}
	
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
