create or replace procedure change_seq(tabname varchar2, tabseqname varchar2)
is
    v integer :=1;
    nv integer;
BEGIN
      EXECUTE IMMEDIATE 'select max(id) from '||tabname INTO v ;  
      v:=v+1000;
      EXECUTE IMMEDIATE 'alter sequence '||tabseqname||' increment by '||v;
      EXECUTE IMMEDIATE 'select '||tabseqname||'.nextval from dual' INTO nv;
      EXECUTE IMMEDIATE 'alter sequence '||tabseqname||' increment by 1';
      EXECUTE IMMEDIATE 'select '||tabseqname||'.nextval from dual' INTO nv;
END;