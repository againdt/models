package com.getinsured.eligibility.enums;

public enum EligibilityStatus {

	PE("Pending", "Pending"),
	AM("Eligible", "Medicaid"),
	AX("Eligible", "Mixed"),
	AE("Eligible", "Exchange"),
	CA("Conditional", "Conditional"),
	CAM("Conditional", "Conditionally Medicaid"),
	CAX("Conditional", "Conditionally Mixed"),
	CAE("Conditional", "Conditionally Exchange"),
	DE("Not Eligible", "Denied");

	private String description;

	private String description2;

	private EligibilityStatus(String description, String description2){
		this.description = description;
		this.description2 = description2;
	}

	public String getDescription(){
		return description;
	}

	public String getDescription2() {
		return description2;
	}
}
