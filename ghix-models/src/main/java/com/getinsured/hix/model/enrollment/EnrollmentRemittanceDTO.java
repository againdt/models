package com.getinsured.hix.model.enrollment;

import java.io.Serializable;

public class EnrollmentRemittanceDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer enrollmentId = null;
	private String issuerSubscriberIdentifier = null;
	private String groupPolicyNumber = null;
	private String CMSPlanID = null;
	private String exchgSubscriberIdentifier = null;
	private String firstName = null;
	private String lastName = null;
	private String middleName = null;
	private String suffix = null;
	private String indivExchAssignedPolicyNum = null;
	/*
	 * HIX-44617 Adding new variable to hold value of enrollee healthCoveragePolicyNo detail
	 */
	private String healthCoveragePolicyNo = null;
			
	
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getIssuerSubscriberIdentifier() {
		return issuerSubscriberIdentifier;
	}
	public void setIssuerSubscriberIdentifier(String issuerSubscriberIdentifier) {
		this.issuerSubscriberIdentifier = issuerSubscriberIdentifier;
	}
	public String getGroupPolicyNumber() {
		return groupPolicyNumber;
	}
	public void setGroupPolicyNumber(String groupPolicyNumber) {
		this.groupPolicyNumber = groupPolicyNumber;
	}
	public String getCMSPlanID() {
		return CMSPlanID;
	}
	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}
	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}
	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	/**
	 * @return the indivExchAssignedPolicyNum
	 */
	public String getIndivExchAssignedPolicyNum() {
		return indivExchAssignedPolicyNum;
	}
	/**
	 * @param indivExchAssignedPolicyNum the indivExchAssignedPolicyNum to set
	 */
	public void setIndivExchAssignedPolicyNum(String indivExchAssignedPolicyNum) {
		this.indivExchAssignedPolicyNum = indivExchAssignedPolicyNum;
	}
	
	/**
	 * @return the healthCoveragePolicyNo
	 */
	public String getHealthCoveragePolicyNo() {
		return healthCoveragePolicyNo;
	}
	/**
	 * @param healthCoveragePolicyNo the healthCoveragePolicyNo to set
	 */
	public void setHealthCoveragePolicyNo(String healthCoveragePolicyNo) {
		this.healthCoveragePolicyNo = healthCoveragePolicyNo;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EnrollmentRemittanceDTO [enrollmentId=" + enrollmentId
				+ ", issuerSubscriberIdentifier=" + issuerSubscriberIdentifier
				+ ", groupPolicyNumber=" + groupPolicyNumber + ", CMSPlanID="
				+ CMSPlanID + ", exchgSubscriberIdentifier="
				+ exchgSubscriberIdentifier + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", middleName=" + middleName
				+ ", suffix=" + suffix + ", indivExchAssignedPolicyNum="
				+ indivExchAssignedPolicyNum + "]";
	}
}
