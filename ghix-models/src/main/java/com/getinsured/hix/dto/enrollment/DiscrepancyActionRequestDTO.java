package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

public class DiscrepancyActionRequestDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum Action {HOLD, HOLD_ALL, ADD_COMMENT, EDIT_COMMENT};
	
	private Action discrepancyAction;
	private Integer discrepancyId;
	private Integer enrollmentId;
	private Integer commentId;
	private String commentText;
	
	public Action getDiscrepancyAction() {
		return discrepancyAction;
	}
	public void setDiscrepancyAction(Action discrepancyAction) {
		this.discrepancyAction = discrepancyAction;
	}
	public Integer getDiscrepancyId() {
		return discrepancyId;
	}
	public void setDiscrepancyId(Integer discrepancyId) {
		this.discrepancyId = discrepancyId;
	}
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Integer getCommentId() {
		return commentId;
	}
	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
}
