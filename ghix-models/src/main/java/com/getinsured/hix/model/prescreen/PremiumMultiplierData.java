package com.getinsured.hix.model.prescreen;

import java.util.HashMap;
import java.util.Map;

/**
 * Static premium content. 
 * age multiplier
 */
public final class PremiumMultiplierData {
	
	private PremiumMultiplierData(){
		
	}
	
	private static Map<String, Map<Integer, Double>> multiplierMap = new HashMap<String, Map<Integer, Double>>();
	
	private static final int ADULT_AGE = 20;
	private static final int SENIOR_AGE = 64;
	
	static {
		final String defaultCSV = "20,0.635;21,1.000;22,1.000;23,1.000;24,1.000;25,1.004;26,1.024;27,"
				+ "1.048;28,1.087;29,1.119;30,1.135;31,1.159;32,1.183;33,1.198;34,1.214;35,1.222;36,1.230;"
				+ "37,1.238;38,1.246;39,1.262;40,1.278;41,1.302;42,1.325;43,1.357;44,1.397;45,1.444;46,1.500;"
				+ "47,1.563;48,1.635;49,1.706;50,1.786;51,1.865;52,1.952;53,2.040;54,2.135;55,2.230;56,2.333;"
				+ "57,2.437;58,2.548;59,2.603;60,2.714;61,2.810;62,2.873;63,2.952;64,3";
		
		final String utahCSV = "20,0.793;21,1;22,1.05;23,1.113;24,1.191;25,1.298;26,1.363;27,1.39;28,1.39;"
				+ "29,1.39;30,1.39;31,1.39;32,1.39;33,1.39;34,1.39;35,1.39;36,1.39;37,1.404;38,1.425;39,1.45;"
				+ "40,1.479;41,1.516;42,1.562;43,1.616;44,1.681;45,1.748;46,1.818;47,1.891;48,1.966;49,2.045;"
				+ "50,2.127;51,2.212;52,2.3;53,2.392;54,2.488;55,2.588;56,2.691;57,2.799;58,2.911;59,3;60,3;"
				+ "61,3;62,3;63,3;64,3;";
		
		final String dcCSV = "20,0.90;21,1.00;22,1.00;23,1.00;24,1.00;25,1.00;26,1.00;27,1.00;28,1.02;"
				+"29,1.05;30,1.07;31,1.10;32,1.12;33,1.15;34,1.18;35,1.20;36,1.23;37,1.26;38,1.28;39,1.29;"
				+"40,1.34;41,1.39;42,1.45;43,1.50;44,1.56;45,1.62;46,1.69;47,1.75;48,1.82;49,1.89;"
				+"50,1.97;51,2.05;52,2.13;53,2.21;54,2.29;55,2.38;56,2.48;57,2.57;58,2.67;59,2.78;60,2.89;"
				+"61,3.00;62,3.00;63,3.00;64,3.00;";
		
		final String mnCSV = "20,0.89;21,1;22,1;23,1;24,1;25,1.004;26,1.024;27,1.048;28,1.087;"
				+ "29,1.119;30,1.135;31,1.159;32,1.183;33,1.198;34,1.214;35,1.222;36,1.23;37,1.238;38,1.246;39,1.262;"
				+ "40,1.278;41,1.302;42,1.325;43,1.357;44,1.397;45,1.444;46,1.5;47,1.563;48,1.635;49,1.706;"
				+ "50,1.786;51,1.865;52,1.952;53,2.04;54,2.135;55,2.23;56,2.333;57,2.437;58,2.548;59,2.603;60,2.714;"
				+ "61,2.81;62,2.873;63,2.952;64,3";
		
		Map<Integer, Double> utahMultiplier = new HashMap<Integer, Double>();
		Map<Integer, Double> defaultMultiplier = new HashMap<Integer, Double>();
		Map<Integer, Double> dcMultiplier = new HashMap<Integer, Double>();
		Map<Integer, Double> mnMultiplier = new HashMap<Integer, Double>();
		
		parse(defaultCSV, defaultMultiplier);
		parse(utahCSV, utahMultiplier);
		parse(dcCSV, dcMultiplier);
		parse(mnCSV, mnMultiplier);
		
		multiplierMap.put(PremiumStates.DEFAULT.getCode(), defaultMultiplier);
		multiplierMap.put(PremiumStates.UT.getCode(), utahMultiplier);
		multiplierMap.put(PremiumStates.DC.getCode(), dcMultiplier);
		multiplierMap.put(PremiumStates.MN.getCode(), mnMultiplier);
	}

	private static void parse(String csv, Map<Integer, Double> map) {
		String[] ages = csv.split(";");
		String[] ageAndRate;
		for (String record : ages) {
			ageAndRate = record.split(",");
			assert (ageAndRate.length == 2);
			map.put(Integer.parseInt(ageAndRate[0]),
					Double.parseDouble(ageAndRate[1]));
		}
	}

	public static Double getPremiumMultiplier(String state, Integer age) {
		if (state != null && !state.isEmpty()) {
			Map<Integer, Double> ageMap;
			if (multiplierMap.containsKey(state)) {
				ageMap = multiplierMap.get(state);
			} else {
				ageMap = multiplierMap.get(PremiumStates.DEFAULT.getCode());
			}

			if (age < ADULT_AGE) {
				return ageMap.get(ADULT_AGE);
			} else if (age > SENIOR_AGE) {
				return ageMap.get(SENIOR_AGE);
			}

			return ageMap.get(age);
		}

		return null;
	}

	/*public static void main(String args[]) throws ParseException {
		for (int i = 0; i < 100; i++) {
			System.out.println(i + ", "	+ PremiumMultiplierData.getPremiumMultiplier("CA", i) + " " + PremiumMultiplierData.getPremiumMultiplier("UT", i)+ " " + PremiumMultiplierData.getPremiumMultiplier("DC", i));
		}

	}*/

}

/**
 * Contains all the states for which we have a different rate percentage
 * 
 * @author bhatt_s
 * 
 */
enum PremiumStates {
	
	DEFAULT("DEFAULT"), UT("UT"), DC("DC"),MN("MN");
	

	PremiumStates(String code) {
		this.code = code;
	}
	
	private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
