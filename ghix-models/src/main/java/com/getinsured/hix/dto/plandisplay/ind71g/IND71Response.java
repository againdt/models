package com.getinsured.hix.dto.plandisplay.ind71g;

import java.util.List;

public class IND71Response {

	private String giHouseholdId;
	
	private String responseCode;
	
	private String responseDescription;
	
	private List<String> errors;

	public String getGiHouseholdId() {
		return giHouseholdId;
	}

	public void setGiHouseholdId(String giHouseholdId) {
		this.giHouseholdId = giHouseholdId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
