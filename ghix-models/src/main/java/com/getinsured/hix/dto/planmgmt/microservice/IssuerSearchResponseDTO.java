package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerSearchResponseDTO extends GhixResponseDTO {

	private Integer totalNumOfRecords;
	private List<IssuerLightDTO> issuerLightDTOList;

	public IssuerSearchResponseDTO() {
	}

	public Integer getTotalNumOfRecords() {
		return totalNumOfRecords;
	}

	public void setTotalNumOfRecords(Integer totalNumOfRecords) {
		this.totalNumOfRecords = totalNumOfRecords;
	}

	public List<IssuerLightDTO> getIssuerLightDTOList() {
		return issuerLightDTOList;
	}

	public void setIssuerLightDTO(IssuerLightDTO issuerLightDTO) {

		if (CollectionUtils.isEmpty(this.issuerLightDTOList)) {
			this.issuerLightDTOList = new ArrayList<IssuerLightDTO>();
		}
		this.issuerLightDTOList.add(issuerLightDTO);
	}
	
	public void setIssuerLightDTOList(List<IssuerLightDTO> issuerDTOList){
		this.issuerLightDTOList = issuerDTOList;
	}
}
