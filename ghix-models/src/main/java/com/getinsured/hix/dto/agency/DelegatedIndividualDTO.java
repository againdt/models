package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class DelegatedIndividualDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String agentId;
	private String individualId;
	private String designationStatus;
	
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getIndividualId() {
		return individualId;
	}
	public void setIndividualId(String individualId) {
		this.individualId = individualId;
	}
	public String getDesignationStatus() {
		return designationStatus;
	}
	public void setDesignationStatus(String designationStatus) {
		this.designationStatus = designationStatus;
	}

	
}
