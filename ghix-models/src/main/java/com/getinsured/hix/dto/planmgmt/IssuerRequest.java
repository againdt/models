/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import com.getinsured.hix.model.GHIXRequest;

/**
 * This is the issuer request object of issuerService
 * 
 * @author gajulapalli_k
 * 
 */
public class IssuerRequest extends GHIXRequest {
	
	private String id;
	
	private String exchangeType; // possible values are ON or OFF

	private String zipCode;
	
	private String countyCode;
	
	private boolean minimizeIssuerData=false;
	
	private String hiosId;
	
	private String name;

	private boolean includeLogo;

	public IssuerRequest() {
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	/**
	 * @return the minimizeIssuerData
	 */
	public boolean isMinimizeIssuerData() {
		return minimizeIssuerData;
	}

	/**
	 * @param minimizeIssuerData the minimizeIssuerData to set
	 */
	public void setMinimizeIssuerData(boolean minimizeIssuerData) {
		this.minimizeIssuerData = minimizeIssuerData;
	}

	/**
	 * @return the hiosId
	 */
	public String getHiosId() {
		return hiosId;
	}

	/**
	 * @param hiosId the hiosId to set
	 */
	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the includeLogo
	 */
	public boolean getIncludeLogo() {
		return includeLogo;
	}

	/**
	 * @param includeLogo the includeLogo to set
	 */
	public void setIncludeLogo(boolean includeLogo) {
		this.includeLogo = includeLogo;
	}
}
