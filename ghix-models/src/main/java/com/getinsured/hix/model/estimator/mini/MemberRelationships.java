package com.getinsured.hix.model.estimator.mini;

public enum MemberRelationships {
	SELF("SELF"), SPOUSE("SPOUSE"), CHILD("CHILD"),DEFAULT("DEFAULT"), OTHER("OTHER"), DOMESTIC("DOMESTIC"), ADOPTED("ADOPTED"), GUARDIAN("GUARDIAN"), FOSTER("FOSTER");
	MemberRelationships(String relationship) {
		this.relationship = relationship;
	}

	String relationship;
}
