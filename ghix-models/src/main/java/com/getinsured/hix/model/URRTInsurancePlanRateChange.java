package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "pm_urrt_plan_rate_change")
@XmlAccessorType(XmlAccessType.NONE)
public class URRTInsurancePlanRateChange implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pm_urrt_plan_rate_change_seq")
	@SequenceGenerator(name = "pm_urrt_plan_rate_change_seq", sequenceName = "pm_urrt_plan_rate_change_seq", allocationSize = 1)
	private int id;

	//foreign key.	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pm_urrt_prod_rate_change_id")
	private URRTProductRateChange unifiedProductRateChange;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "unifiedRatePlanRateChange", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<URRTPRCPremiumComponent> insurancePremiumComponents;
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "unifiedRatePlanRateChange", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//URRTPRCPremiumClaimInfo is mapped to "projecedInsurancePlanRateChangePeriod" in template xml.
	private List<URRTPRCPremiumClaimInfo> premiumClaimsInfoList;
		
	@Column(name = "bi_hios_plan_id")
	private String basedInsurancePlanIdentifier;

	@Column(name = "trc_qty")
	private double totalRateChangeQuantity;

	@Column(name = "mcs_inc_qty")
	private double memberCostShareIncreaseQuantity;

	@Column(name = "acr_per_pmpm_amt")
	private double averageCurrentRatePerPMPMAmount;

	@Column(name = "pmm_qty")
	private int projectedMemberMonthsQuantity;

	@Column(name = "av_metal_val_qty")
	private double avMetalValueQuantity;

	@Column(name = "av_prc_val_qty")
	private double avPricingValueQuantity;

	@Column(name = "exch_plan_ind")
	private String exchangePlanIndicator;

	@Column(name = "dipbm_tier_type")
	private String definingInsurancePlanBenefitMetalTierType;

	@Column(name = "plan_name")
	private String planName;

	@Column(name = "plan_type")
	private String planType;

	@Column(name = "pr_eff_date")
	private Date proposedRateEffectiveDate;

	@Column(name = "rc_qty")
	private double rateChangeQuantity;

	@Column(name = "rcc_qty")
	private double rateCumulativeChangeQuantity;

	@Column(name = "rpc_qty")
	private double rateProjectedChangeQuantity;

	@Column(name = "is_obsolete")
	private String isObsolete;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp", nullable = false)
	private Date creationTimestamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public URRTProductRateChange getUnifiedProductRateChange() {
		return unifiedProductRateChange;
	}

	public void setUnifiedProductRateChange(
			URRTProductRateChange unifiedProductRateChange) {
		this.unifiedProductRateChange = unifiedProductRateChange;
	}

	public String getBasedInsurancePlanIdentifier() {
		return basedInsurancePlanIdentifier;
	}

	public void setBasedInsurancePlanIdentifier(String basedInsurancePlanIdentifier) {
		this.basedInsurancePlanIdentifier = basedInsurancePlanIdentifier;
	}

	public double getTotalRateChangeQuantity() {
		return totalRateChangeQuantity;
	}

	public void setTotalRateChangeQuantity(double totalRateChangeQuantity) {
		this.totalRateChangeQuantity = totalRateChangeQuantity;
	}

	public double getMemberCostShareIncreaseQuantity() {
		return memberCostShareIncreaseQuantity;
	}

	public void setMemberCostShareIncreaseQuantity(
			double memberCostShareIncreaseQuantity) {
		this.memberCostShareIncreaseQuantity = memberCostShareIncreaseQuantity;
	}

	public double getAverageCurrentRatePerPMPMAmount() {
		return averageCurrentRatePerPMPMAmount;
	}

	public void setAverageCurrentRatePerPMPMAmount(
			double averageCurrentRatePerPMPMAmount) {
		this.averageCurrentRatePerPMPMAmount = averageCurrentRatePerPMPMAmount;
	}

	public int getProjectedMemberMonthsQuantity() {
		return projectedMemberMonthsQuantity;
	}

	public void setProjectedMemberMonthsQuantity(int projectedMemberMonthsQuantity) {
		this.projectedMemberMonthsQuantity = projectedMemberMonthsQuantity;
	}

	public double getAvMetalValueQuantity() {
		return avMetalValueQuantity;
	}

	public void setAvMetalValueQuantity(double avMetalValueQuantity) {
		this.avMetalValueQuantity = avMetalValueQuantity;
	}

	public double getAvPricingValueQuantity() {
		return avPricingValueQuantity;
	}

	public void setAvPricingValueQuantity(double avPricingValueQuantity) {
		this.avPricingValueQuantity = avPricingValueQuantity;
	}

	public String getExchangePlanIndicator() {
		return exchangePlanIndicator;
	}

	public void setExchangePlanIndicator(String exchangePlanIndicator) {
		this.exchangePlanIndicator = exchangePlanIndicator;
	}

	public String getDefiningInsurancePlanBenefitMetalTierType() {
		return definingInsurancePlanBenefitMetalTierType;
	}

	public void setDefiningInsurancePlanBenefitMetalTierType(
			String definingInsurancePlanBenefitMetalTierType) {
		this.definingInsurancePlanBenefitMetalTierType = definingInsurancePlanBenefitMetalTierType;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}
	
	public Date getProposedRateEffectiveDate() {
		return proposedRateEffectiveDate;
	}

	public void setProposedRateEffectiveDate(Date proposedRateEffectiveDate) {
		this.proposedRateEffectiveDate = proposedRateEffectiveDate;
	}

	public double getRateChangeQuantity() {
		return rateChangeQuantity;
	}

	public void setRateChangeQuantity(double rateChangeQuantity) {
		this.rateChangeQuantity = rateChangeQuantity;
	}

	public double getRateCumulativeChangeQuantity() {
		return rateCumulativeChangeQuantity;
	}

	public void setRateCumulativeChangeQuantity(double rateCumulativeChangeQuantity) {
		this.rateCumulativeChangeQuantity = rateCumulativeChangeQuantity;
	}

	public double getRateProjectedChangeQuantity() {
		return rateProjectedChangeQuantity;
	}

	public void setRateProjectedChangeQuantity(double rateProjectedChangeQuantity) {
		this.rateProjectedChangeQuantity = rateProjectedChangeQuantity;
	}

	public String getIsObsolete() {
		return isObsolete;
	}

	public void setIsObsolete(String isObsolete) {
		this.isObsolete = isObsolete;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public List<URRTPRCPremiumComponent> getInsurancePremiumComponents() {
		return insurancePremiumComponents;
	}

	public void setInsurancePremiumComponents(
			List<URRTPRCPremiumComponent> insurancePremiumComponents) {
		this.insurancePremiumComponents = insurancePremiumComponents;
	}

	public List<URRTPRCPremiumClaimInfo> getPremiumClaimsInfoList() {
		return premiumClaimsInfoList;
	}

	public void setPremiumClaimsInfoList(
			List<URRTPRCPremiumClaimInfo> premiumClaimsInfoList) {
		this.premiumClaimsInfoList = premiumClaimsInfoList;
	}
	

}
