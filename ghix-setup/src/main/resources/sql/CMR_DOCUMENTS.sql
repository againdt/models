CREATE TABLE "CMR_DOCUMENTS" 
(	
    "ID" NUMBER, 
	"ECM_DOCUMENT_ID" VARCHAR2(500 CHAR), 
	"DOCUMENT_NAME" VARCHAR2(200 CHAR),
	"DOCUMENT_TYPE"	VARCHAR2(200 CHAR),
	"DOCUMENT_CATEGORY" VARCHAR2(200 CHAR),
	"TARGET_ID" NUMBER, 
	"TARGET_NAME" VARCHAR2(100 CHAR), 
	"ACCEPTED" VARCHAR2(10 CHAR), 
	"COMMENTS" VARCHAR2(500 CHAR), 
	"CREATED_DATE" TIMESTAMP,
	"CREATED_BY" NUMBER,
	CONSTRAINT PK_CMR_DOCUMENTS PRIMARY KEY (ID),
	CONSTRAINT FK_USERS_ID_CMR_DOCUMENTS FOREIGN KEY (CREATED_BY) REFERENCES USERS(ID)
);

 COMMENT ON COLUMN "CMR_DOCUMENTS"."ID" IS 'Primary key to uniquely identify a document';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."ECM_DOCUMENT_ID" IS 'ECM Document ID of the document uploaded to ECM';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."DOCUMENT_NAME" IS 'Document name i.e. file name of the document uploaded to ECM';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."DOCUMENT_TYPE" IS 'Document Type describes the kind of document i.e. Driving_License, Passport, Marriage_Certificate etc';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."DOCUMENT_CATEGORY" IS 'CAP Document Type which can be RIDP, SSAP_DOCUMENT etc.';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."TARGET_ID" IS 'Stores the ID of object to which the document is associated with.';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."TARGET_NAME" IS 'Stores the object type to which the document is associated with e.g. CMR_HOUSEHOLD, SSAP_APPLICATIONS, SSAP_APPLICANT etc';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."ACCEPTED" IS 'Stores whether or not the document was accepted by the CSR';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."COMMENTS" IS 'Stores the comments with which the CSR enters while accepting/rejecting a document';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."CREATED_DATE" IS 'Stores the timestamp when the document was uploaded/created';
 COMMENT ON COLUMN "CMR_DOCUMENTS"."CREATED_BY" IS 'Stores the user id of the user who uploaded the document';
 
 
 -- Sequence for the CMR_DOCUMENTS.ID
CREATE SEQUENCE CMR_DOCUMENTS_SEQ;

-- Index for CMR_DOCUMENTS.ID, CMR_DOCUMENTS.TARGET_ID, CMR_DOCUMENTS.TARGET_NAME 
CREATE INDEX CMR_DOCUMENTS_IND1 ON CMR_DOCUMENTS(ID, TARGET_ID, TARGET_NAME);