package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class EnrollmentIrsCoveredIndividualDTO implements Serializable{
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffixName;
	private String ssn;
	private Date dob;
	private Date coverageStartDt;
	private Date coverageEndDt;
	private String addressLine1Txt;
	private String addressLine2Txt;
	private String cityNm;
	private String usStateCd;
	private String uszipCd;
	private String uszipExtensionCd;
	private String memberId;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getSuffixName() {
		return suffixName;
	}
	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}
	public Date getCoverageStartDt() {
		return coverageStartDt;
	}
	public void setCoverageStartDt(Date coverageStartDt) {
		this.coverageStartDt = coverageStartDt;
	}
	public Date getCoverageEndDt() {
		return coverageEndDt;
	}
	public void setCoverageEndDt(Date coverageEndDt) {
		this.coverageEndDt = coverageEndDt;
	}
	/**
	 * @return the addressLine1Txt
	 */
	public String getAddressLine1Txt() {
		return addressLine1Txt;
	}
	/**
	 * @param addressLine1Txt the addressLine1Txt to set
	 */
	public void setAddressLine1Txt(String addressLine1Txt) {
		this.addressLine1Txt = addressLine1Txt;
	}
	/**
	 * @return the addressLine2Txt
	 */
	public String getAddressLine2Txt() {
		return addressLine2Txt;
	}
	/**
	 * @param addressLine2Txt the addressLine2Txt to set
	 */
	public void setAddressLine2Txt(String addressLine2Txt) {
		this.addressLine2Txt = addressLine2Txt;
	}
	/**
	 * @return the cityNm
	 */
	public String getCityNm() {
		return cityNm;
	}
	/**
	 * @param cityNm the cityNm to set
	 */
	public void setCityNm(String cityNm) {
		this.cityNm = cityNm;
	}
	/**
	 * @return the usStateCd
	 */
	public String getUsStateCd() {
		return usStateCd;
	}
	/**
	 * @param usStateCd the usStateCd to set
	 */
	public void setUsStateCd(String usStateCd) {
		this.usStateCd = usStateCd;
	}
	/**
	 * @return the uszipCd
	 */
	public String getUszipCd() {
		return uszipCd;
	}
	/**
	 * @param uszipCd the uszipCd to set
	 */
	public void setUszipCd(String uszipCd) {
		this.uszipCd = uszipCd;
	}
	/**
	 * @return the uszipExtensionCd
	 */
	public String getUszipExtensionCd() {
		return uszipExtensionCd;
	}
	/**
	 * @param uszipExtensionCd the uszipExtensionCd to set
	 */
	public void setUszipExtensionCd(String uszipExtensionCd) {
		this.uszipExtensionCd = uszipExtensionCd;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	@Override
	public String toString() {
		return "EnrollmentIrsCoveredIndividualDTO [firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName
				+ ", suffixName=" + suffixName + ", ssn=" + ssn + ", dob="
				+ dob + ", coverageStartDt=" + coverageStartDt
				+ ", coverageEndDt=" + coverageEndDt + ", addressLine1Txt="
				+ addressLine1Txt + ", addressLine2Txt=" + addressLine2Txt
				+ ", cityNm=" + cityNm + ", usStateCd=" + usStateCd
				+ ", uszipCd=" + uszipCd + ", uszipExtensionCd="
				+ uszipExtensionCd + ", memberId=" + memberId + "]";
	}
	
}
