package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;

public class DiscrepancyDetailDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer discrepancyId;
	private String discrepancyCode;
	private String discrepancyLabel;
	private String discrepancyCategory;
	private DiscrepancyStatus discrepancyStatus;
	private String hixValue;
	private String issuerValue;
	private String reportedDate;
	private Long hixEnrollmentId;
	private Integer issuerEnrollmentId;
	private Integer fileId;
	private Integer memberId;
	private String fieldName;
	private String fileName;
	
	public DiscrepancyDetailDTO(){
		
	}
	
	public DiscrepancyDetailDTO(Integer memberId, String discrepancyCode, String discrepancyCategory, Long hixEnrollmentId, 
			String fieldName, Integer issuerEnrollmentId, String fileName, String hixValue, String issuerValue){
		this.memberId = memberId;
		this.discrepancyCode = discrepancyCode;
		this.discrepancyCategory = discrepancyCategory;
		this.hixEnrollmentId = hixEnrollmentId;
		this.fieldName = fieldName;
		this.issuerEnrollmentId = issuerEnrollmentId;
		this.fileName = fileName;
		this.hixValue = hixValue;
		this.issuerValue = issuerValue;
		
	}
	
	public String getDiscrepancyCategory() {
		return discrepancyCategory;
	}
	public void setDiscrepancyCategory(String discrepancyCategory) {
		this.discrepancyCategory = discrepancyCategory;
	}
	public String getDiscrepancyCode() {
		return discrepancyCode;
	}
	public void setDiscrepancyCode(String discrepancyCode) {
		this.discrepancyCode = discrepancyCode;
	}
	public String getDiscrepancyLabel() {
		return discrepancyLabel;
	}
	public void setDiscrepancyLabel(String discrepancyLabel) {
		this.discrepancyLabel = discrepancyLabel;
	}
	public Integer getDiscrepancyId() {
		return discrepancyId;
	}
	public void setDiscrepancyId(Integer discrepancyId) {
		this.discrepancyId = discrepancyId;
	}
	public DiscrepancyStatus getDiscrepancyStatus() {
		return discrepancyStatus;
	}
	public void setDiscrepancyStatus(DiscrepancyStatus discrepancyStatus) {
		this.discrepancyStatus = discrepancyStatus;
	}
	public String getHixValue() {
		return hixValue;
	}
	public void setHixValue(String hixValue) {
		this.hixValue = hixValue;
	}
	public String getIssuerValue() {
		return issuerValue;
	}
	public void setIssuerValue(String issuerValue) {
		this.issuerValue = issuerValue;
	}
	public String getReportedDate() {
		return reportedDate;
	}
	public void setReportedDate(String reportedDate) {
		this.reportedDate = reportedDate;
	}
	public Long getHixEnrollmentId() {
		return hixEnrollmentId;
	}
	public void setHixEnrollmentId(Long hixEnrollmentId) {
		this.hixEnrollmentId = hixEnrollmentId;
	}
	public Integer getIssuerEnrollmentId() {
		return issuerEnrollmentId;
	}
	public void setIssuerEnrollmentId(Integer issuerEnrollmentId) {
		this.issuerEnrollmentId = issuerEnrollmentId;
	}
	public Integer getFileId() {
		return fileId;
	}
	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
