package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="ENRL_IRS_MONTHLY_EXEC")
public class EnrollmentIRSMonthlyExecution implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_IRS_MONTHLY_EXEC_SEQ")
	@SequenceGenerator(name = "ENRL_IRS_MONTHLY_EXEC_SEQ", sequenceName = "ENRL_IRS_MONTHLY_EXEC_SEQ", allocationSize = 1)
	private Integer id;
	
	//@OneToOne(fetch=FetchType.LAZY)
	@Column(name="ENROLLMENT_1095_ID")
	private Integer enrollment1095;
	
	@Column(name = "HOUSEHOLD_CASE_ID")
	private Integer householdCaseId;

	@Column(name = "IRS_INBOUND_ACTION")
	private String irsInboundAction;
	
	@Column(name = "IRS_INBOUND_BCC")
	private String irsInboundBatchCategoryCode;
	
	@Column(name = "IRS_SKIPPED_FLAG")
	private String irsSkippedFlag;
	
	@Column(name = "IRS_SKIPPED_MSG")
	private String irsSkippedMsg;
	
	@Column(name = "IRS_XML_FILE_NAME")
	private String irsXmlFileName;
	
	@Column(name = "IRS_XML_GENERATION_TIMESTAMP")
	private Date irsXmlGeneratedOn;
	
	@Column(name = "ORIGINAL_BATCH_ID")
	private String originalBatchId;
	
	@Column(name = "CURRENT_BATCH_ID")
	private String currentBatchId;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEnrollment1095() {
		return enrollment1095;
	}

	public void setEnrollment1095(Integer enrollment1095) {
		this.enrollment1095 = enrollment1095;
	}

	public Integer getHouseHoldCaseId() {
		return householdCaseId;
	}

	public void setHouseHoldCaseId(Integer houseHoldCaseId) {
		this.householdCaseId = houseHoldCaseId;
	}

	public String getIrsInboundAction() {
		return irsInboundAction;
	}

	public void setIrsInboundAction(String irsInboundAction) {
		this.irsInboundAction = irsInboundAction;
	}

	public String getIrsInboundBatchCategoryCode() {
		return irsInboundBatchCategoryCode;
	}

	public void setIrsInboundBatchCategoryCode(String irsInboundBatchCategoryCode) {
		this.irsInboundBatchCategoryCode = irsInboundBatchCategoryCode;
	}

	public String getIrsSkippedFlag() {
		return irsSkippedFlag;
	}

	public void setIrsSkippedFlag(String irsSkippedFlag) {
		this.irsSkippedFlag = irsSkippedFlag;
	}

	public String getIrsSkippedMsg() {
		return irsSkippedMsg;
	}

	public void setIrsSkippedMsg(String irsSkippedMsg) {
		this.irsSkippedMsg = irsSkippedMsg;
	}

	public String getIrsXmlFileName() {
		return irsXmlFileName;
	}

	public void setIrsXmlFileName(String irsXmlFileName) {
		this.irsXmlFileName = irsXmlFileName;
	}

	public Date getIrsXmlGeneratedOn() {
		return irsXmlGeneratedOn;
	}

	public void setIrsXmlGeneratedOn(Date irsXmlGeneratedOn) {
		this.irsXmlGeneratedOn = irsXmlGeneratedOn;
	}

	public String getOriginalBatchId() {
		return originalBatchId;
	}

	public void setOriginalBatchId(String originalBatchId) {
		this.originalBatchId = originalBatchId;
	}

	public String getCurrentBatchId() {
		return currentBatchId;
	}

	public void setCurrentBatchId(String currentBatchId) {
		this.currentBatchId = currentBatchId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}
}
