package com.getinsured.hix.dto.shop;

public class EmployerDTO extends com.getinsured.hix.model.Employer {
	private static final long serialVersionUID = 1L;

	private String currentCoverageStartDt;

	private String currentEnrollmentStatus;

	private String currentCostBasisPlan;

	private String upcomingCoverageStartDt;

	private String upcomingEnrollmentStatus;

	private String upcomingCostBasisPlan;

	public String getCurrentCoverageStartDt() {
		return currentCoverageStartDt;
	}

	public void setCurrentCoverageStartDt(String currentCoverageStartDt) {
		this.currentCoverageStartDt = currentCoverageStartDt;
	}

	public String getCurrentEnrollmentStatus() {
		return currentEnrollmentStatus;
	}

	public void setCurrentEnrollmentStatus(String currentEnrollmentStatus) {
		this.currentEnrollmentStatus = currentEnrollmentStatus;
	}

	public String getCurrentCostBasisPlan() {
		return currentCostBasisPlan;
	}

	public void setCurrentCostBasisPlan(String currentCostBasisPlan) {
		this.currentCostBasisPlan = currentCostBasisPlan;
	}

	public String getUpcomingCoverageStartDt() {
		return upcomingCoverageStartDt;
	}

	public void setUpcomingCoverageStartDt(String upcomingCoverageStartDt) {
		this.upcomingCoverageStartDt = upcomingCoverageStartDt;
	}

	public String getUpcomingEnrollmentStatus() {
		return upcomingEnrollmentStatus;
	}

	public void setUpcomingEnrollmentStatus(String upcomingEnrollmentStatus) {
		this.upcomingEnrollmentStatus = upcomingEnrollmentStatus;
	}

	public String getUpcomingCostBasisPlan() {
		return upcomingCostBasisPlan;
	}

	public void setUpcomingCostBasisPlan(String upcomingCostBasisPlan) {
		this.upcomingCostBasisPlan = upcomingCostBasisPlan;
	}

}
