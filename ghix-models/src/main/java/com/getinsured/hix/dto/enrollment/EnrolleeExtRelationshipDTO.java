/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * @author panda_p
 *
 */
public class EnrolleeExtRelationshipDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String memberId;
	private String relationshipCode;
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getRelationshipCode() {
		return relationshipCode;
	}
	public void setRelationshipCode(String relationshipCode) {
		this.relationshipCode = relationshipCode;
	}

	
	
}
