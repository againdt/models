package com.getinsured.hix.dto.eapp;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckbox;
import org.apache.pdfbox.pdmodel.interactive.form.PDChoiceButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioCollection;

public class InteractivePdfMonitor {

	private File file = null;

	static String getSafeValue(PDField field) throws IOException {
		try {
			return field.getValue() == null ? "null" : field.getValue();
		} catch (NullPointerException e) {
			return "null";
		}
	}

	public String createFileWithNamedTextFields() throws IOException, COSVisitorException {
		PDDocument document = loadDocument(file);
		PDDocumentCatalog docCatalog = document.getDocumentCatalog();
		PDAcroForm form = docCatalog.getAcroForm();
		if (form != null) {
			List<PDField> fields = form.getFields();
			for (PDField field : fields) {
				if (field.getFieldType().equals("Tx"))
					field.setValue(field.getFullyQualifiedName());
			}
		}
		String newName = "filled/filled-" + file.getName();
		document.save(newName);
		document.close();
		return newName;
	}

	public String getFieldsDump() throws IOException {
		PDDocument document = loadDocument(file);
		PDDocumentCatalog docCatalog = document.getDocumentCatalog();
		PDAcroForm form = docCatalog.getAcroForm();
		List<PDField> fields = form.getFields();
		String fieldsDump = buildFieldsDump(fields);
		document.close();
		return fieldsDump;
	}

	private void generateDumpFileFromPdf(List<PDField> fields, WritableWorkbook workbook) throws IOException, RowsExceededException, WriteException {
		workbook.createSheet("Report", 0);
		WritableSheet excelSheet = workbook.getSheet(0);

		// Get PDF Fields and Values in List
		String [][] pdfFieldArray = new String[fields.size()][3];
		PDField field = null;
		
		for(int i = 0; i < fields.size(); i++){
			field = fields.get(i);	
			String [] fieldAraay = new String[3];
			
			fieldAraay[0] = field.getFullyQualifiedName();
			fieldAraay[1] = field.getClass().getName().replaceAll("org.apache.pdfbox.pdmodel.interactive.form.","");
			
			if (field instanceof PDRadioCollection) {
				List<COSObjectable> fieldKids = field.getKids();
				if (fieldKids != null) {
					StringBuilder sb = new StringBuilder();
					for(int j = 0; j < fieldKids.size(); j++){
						COSObjectable cosObjectable = fieldKids.get(j);
						if(cosObjectable instanceof PDCheckbox){
							sb.append(j);
							sb.append(" - ");
							sb.append(((PDCheckbox)cosObjectable).getOnValue());
							sb.append(", ");
						}
					} 
					if(sb.length() > 1){
						sb.delete(sb.length() - 2, sb.length() - 1);
					}
					
					fieldAraay[2] = sb.toString();
				} 
			}
			
			pdfFieldArray[i] = fieldAraay;
		}
		
		// SET the pdf value list in EXCEL Sheet
		for(int i = 0 ; i < pdfFieldArray.length ; i++){
			for (int j = 0; j < pdfFieldArray[i].length; j++) {
				excelSheet.addCell((WritableCell) new Label(j,i , pdfFieldArray[i][j]));
			}
		}
		
	}
	private String buildFieldsDump(List<PDField> fields) throws IOException {
		StringBuilder builder = new StringBuilder();
		for (PDField field: fields) {
			builder.append("\"");
			builder.append(field.getFullyQualifiedName());
			builder.append("\" : \"");
			builder.append(field.getClass().getName().replaceAll("org.apache.pdfbox.pdmodel.interactive.form.",""));
			builder.append(" = ");
			builder.append(getSafeValue(field));
			if (field instanceof PDRadioCollection) {
				List<COSObjectable> fieldKids = field.getKids();
				if (fieldKids != null) {
					StringBuilder sb = new StringBuilder();
					sb.append(" [1..");
					sb.append(fieldKids.size());
					sb.append("]");
					builder.append(sb);
				} else {
					builder.append(" [0]");
				}
			}
			if (field instanceof PDChoiceButton) {
				List options = ((PDChoiceButton) field).getOptions();
				StringBuilder sb = new StringBuilder();

				sb.append(" { ");
				if (options != null) {
					for (Object option : options) {
						sb.append(option).append(" ");
					}
				} else {
					sb.append("null ");
				}
				sb.append("}");
				builder.append(sb);
			}
			builder.append("\"\n");
		}
		return builder.toString();
	}

	private PDDocument loadDocument(File file) throws IOException {
		String filePath = file.getAbsolutePath();
		return PDDocument.load(filePath);
	}

	public void setFile(File file) {
		this.file = file;
	}

	public boolean hasFile() {
		return file != null;
	}


	public static void main(String[] args) {
		String inputPutFolder = args[0];
		String outputFolder = args[1];
		
		//String inputPutFolder = "C:\\jobfolder\\D2C";
		//String outputFolder = "C:\\jobfolder\\D2C";
		
		InteractivePdfMonitor interactivePdfMonitor = new InteractivePdfMonitor();
		try {
			File[] filesInFolder = interactivePdfMonitor.getFilesInAFolder(inputPutFolder);
			for(int i = 0;i <filesInFolder.length;i++){
				interactivePdfMonitor.getFieldsDump(filesInFolder[i],outputFolder);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getFieldsDump(File pdfFile, String outputFileFolder) throws IOException, RowsExceededException, WriteException {
		PDDocument document = loadDocument(pdfFile);
		PDDocumentCatalog docCatalog = document.getDocumentCatalog();
		PDAcroForm form = docCatalog.getAcroForm();
		List<PDField> fields = form.getFields();
		
		WorkbookSettings wbSettings = new WorkbookSettings();
	    wbSettings.setLocale(new Locale("en", "EN"));
	    
	    String fileNameWithOutExt = pdfFile.getName().replaceFirst("[.][^.]+$", "");
	    
	    File file = new File(outputFileFolder + "\\" + fileNameWithOutExt + ".xls");
	    
	    WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	    
		generateDumpFileFromPdf(fields, workbook);
		
		document.close();
		workbook.write();
		workbook.close();
	}


	private File[] getFilesInAFolder(String folderName) throws Exception{
		final String fileExtn = ".pdf";
		File folder = new File(folderName);
		File[] listOFFiles= folder.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				if(pathname.isFile() && pathname.getName().endsWith(fileExtn)){
					return true;
				}
				return false;
			}
		});
		return listOFFiles;
	}
}
