package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="plan_dental_cost")
public class PlanDentalCost implements Serializable,BaseBenefit<PlanDental> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanDentalCost_Seq")
	@SequenceGenerator(name = "PlanDentalCost_Seq", sequenceName = "plan_dental_cost_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
    @JoinColumn(name="plan_dental_id")
	private PlanDental planDental;
	
	@Column(name="name", length=255)
	private String name;
	
	@Column(name="IN_NETWORK_IND")
	private Double inNetWorkInd;
	
	@Column(name="IN_NETWORK_FLY")
	private Double inNetWorkFly;
	
	@Column(name="IN_NETWORK_TIER2_IND")
	private Double inNetworkTier2Ind;
	
	@Column(name="IN_NETWORK_TIER2_FLY")
	private Double inNetworkTier2Fly;
	
	@Column(name="OUT_NETWORK_IND")
	private Double outNetworkInd;
	
	@Column(name="OUT_NETWORK_FLY")
	private Double outNetworkFly;
	
	@Column(name="COMBINED_IN_OUT_NETWORK_IND")
	private Double combinedInOutNetworkInd;
	
	@Column(name="COMBINED_IN_OUT_NETWORK_FLY")
	private Double combinedInOutNetworkFly;
	
	@Column(name="COMB_DEF_COINS_NETWORK_TIER1")
	private Double combDefCoinsNetworkTier1;
	
	@Column(name="COMB_DEF_COINS_NETWORK_TIER2")
	private Double combDefCoinsNetworkTier2;
	
	/** New Column added for HIX-43765 **/	
	@Column(name="LIMIT_EXCEP_DISPLAY")
	private String limitExcepDisplay;

	@Column(name="IN_NETWORK_FLY_PP")
	private Double inNetworkFlyPerPerson;
	
	@Column(name="IN_NETWORK_TIER2_FLY_PP")
	private Double inNetworkTier2FlyPerPerson;
	
	@Column(name="OUT_NETWORK_FLY_PP")
	private Double outNetworkFlyPerPerson;
	
	@Column(name="COMBINED_IN_OUT_NETWORK_FLY_PP")
	private Double combinedInOutNetworkFlyPerPerson;
	
	/*************/
	
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PlanDental getPlanDental() {
		return planDental;
	}
	
	public void setPlanDental(PlanDental planDental) {
		this.planDental = planDental;
	}

	@Override
	public void setParent(PlanDental parentPlan)
	{
		setPlanDental(parentPlan);
	}
	
	public Double getInNetWorkInd() {
		return inNetWorkInd;
	}

	public void setInNetWorkInd(Double inNetWorkInd) {
		this.inNetWorkInd = inNetWorkInd;
	}
	
	public Double getInNetWorkFly() {
		return inNetWorkFly;
	}

	public void setInNetWorkFly(Double inNetWorkFly) {
		this.inNetWorkFly = inNetWorkFly;
	}

	
	public Double getInNetworkTier2Ind() {
		return inNetworkTier2Ind;
	}
	public void setInNetworkTier2Ind(Double inNetworkTier2Ind) {
		this.inNetworkTier2Ind = inNetworkTier2Ind;
	}
	
	public Double getInNetworkTier2Fly() {
		return inNetworkTier2Fly;
	}
	public void setInNetworkTier2Fly(Double inNetworkTier2Fly) {
		this.inNetworkTier2Fly = inNetworkTier2Fly;
	}
	
	public Double getOutNetworkInd() {
		return outNetworkInd;
	}
	public void setOutNetworkInd(Double outNetworkInd) {
		this.outNetworkInd = outNetworkInd;
	}
	
	public Double getOutNetworkFly() {
		return outNetworkFly;
	}
	public void setOutNetworkFly(Double outNetworkFly) {
		this.outNetworkFly = outNetworkFly;
	}
	
	public Double getCombinedInOutNetworkInd() {
		return combinedInOutNetworkInd;
	}
	public void setCombinedInOutNetworkInd(Double combinedInOutNetworkInd) {
		this.combinedInOutNetworkInd = combinedInOutNetworkInd;
	}
	
	public Double getCombinedInOutNetworkFly () {
		return combinedInOutNetworkFly;
	}
	public void setCombinedInOutNetworkFly(Double combinedInOutNetworkFly) {
		this.combinedInOutNetworkFly = combinedInOutNetworkFly;
	}
	
	public Double  getCombDefCoinsNetworkTier1() {
		return combDefCoinsNetworkTier1;
	}
	public void setCombDefCoinsNetworkTier1(Double combDefCoinsNetworkTier1) {
		this.combDefCoinsNetworkTier1 = combDefCoinsNetworkTier1;
	}
	
	public Double getCombDefCoinsNetworkTier2() {
		return combDefCoinsNetworkTier2;
	}
	public void setCombDefCoinsNetworkTier2(Double combDefCoinsNetworkTier2) {
		this.combDefCoinsNetworkTier2 = combDefCoinsNetworkTier2;
	}

	public String getLimitExcepDisplay() {
		return limitExcepDisplay;
	}

	public void setLimitExcepDisplay(String limitExcepDisplay) {
		this.limitExcepDisplay = limitExcepDisplay;
	}

	public Double getInNetworkFlyPerPerson() {
		return inNetworkFlyPerPerson;
	}

	public void setInNetworkFlyPerPerson(Double inNetworkFlyPerPerson) {
		this.inNetworkFlyPerPerson = inNetworkFlyPerPerson;
	}

	public Double getInNetworkTier2FlyPerPerson() {
		return inNetworkTier2FlyPerPerson;
	}

	public void setInNetworkTier2FlyPerPerson(Double inNetworkTier2FlyPerPerson) {
		this.inNetworkTier2FlyPerPerson = inNetworkTier2FlyPerPerson;
	}

	public Double getOutNetworkFlyPerPerson() {
		return outNetworkFlyPerPerson;
	}

	public void setOutNetworkFlyPerPerson(Double outNetworkFlyPerPerson) {
		this.outNetworkFlyPerPerson = outNetworkFlyPerPerson;
	}

	public Double getCombinedInOutNetworkFlyPerPerson() {
		return combinedInOutNetworkFlyPerPerson;
	}

	public void setCombinedInOutNetworkFlyPerPerson(
			Double combinedInOutNetworkFlyPerPerson) {
		this.combinedInOutNetworkFlyPerPerson = combinedInOutNetworkFlyPerPerson;
	}
	
}
