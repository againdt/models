package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXRequest;

/**
 * 
 * @author Aditya_S
 * @since 26-05-2015
 * 
 * Used for LCE/Special Enrollment in case no plan change, to add/update/delete members.
 *
 */
public class EnrollmentUpdateRequest extends GHIXRequest implements Serializable{
	private List<EnrollmentCoreDto> enrollmentDto;

	/**
	 * @return the enrollmentDto
	 */
	public List<EnrollmentCoreDto> getEnrollmentDto() {
		return enrollmentDto;
	}

	/**
	 * @param enrollmentDto the enrollmentDto to set
	 */
	public void setEnrollmentDto(List<EnrollmentCoreDto> enrollmentDto) {
		this.enrollmentDto = enrollmentDto;
	}
}
