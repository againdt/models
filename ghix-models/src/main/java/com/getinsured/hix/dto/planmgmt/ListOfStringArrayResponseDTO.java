/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXResponse;

public class ListOfStringArrayResponseDTO extends GHIXResponse implements Serializable {

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private List<String[]> stringArrayList;
	private String name;
	private int count;
	
	public List<String[]> getStringArrayList() {
		return stringArrayList;
	}

	public void setStringArrayList(List<String[]> stringArrayList) {
		this.stringArrayList = stringArrayList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
