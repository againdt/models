package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CAP_ENROLLMENT_PAYMENT")
public class CapEnrollmentPayment implements Serializable {


	private static final long serialVersionUID = 7679763763558145968L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAP_ENROLLMENT_PAYMENT_SEQ")
	@SequenceGenerator(name = "CAP_ENROLLMENT_PAYMENT_SEQ", sequenceName = "CAP_ENROLLMENT_PAYMENT_SEQ", allocationSize = 1)
	@Column(name = "id")
	private Integer id;

	@Column(name = "ENROLLMENT_ID")
	private Integer enrollmentId;
	
	@Column(name = "ECM_DOC_ID")
	private String ecmDocumentId;
	
	@Column(name = "COMMENT_ID")
	private String commentId;
	
	@Column(name = "PAYMENT_LOOKUPVALUE_ID")
	private Integer paymentLkp;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getEcmDocumentId() {
		return ecmDocumentId;
	}

	public void setEcmDocumentId(String ecmDocumentId) {
		this.ecmDocumentId = ecmDocumentId;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public Integer getPaymentLkp() {
		return paymentLkp;
	}

	public void setPaymentLkp(Integer paymentLkp) {
		this.paymentLkp = paymentLkp;
	}
}
