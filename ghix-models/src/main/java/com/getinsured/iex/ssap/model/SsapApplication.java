package com.getinsured.iex.ssap.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.timeshift.sql.TSTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the SSAP_APPLICATIONS database table.
 *
 */
@Audited
@Entity
@Table(name="SSAP_APPLICATIONS")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="SsapApplication.findAll", query="SELECT s FROM SsapApplication s")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SsapApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SSAP_APPLICATIONS_ID_GENERATOR", sequenceName="SSAP_APPLICATIONS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SSAP_APPLICATIONS_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=22)
	private long id;

	@Column(name="APPLICATION_DATA")
	private String applicationData;

	@Column(name="APPLICATION_STATUS", length=50)
	private String applicationStatus;
	
	@Column(name="VALIDATION_STATUS")
	@Enumerated(EnumType.STRING)
	private ApplicationValidationStatus validationStatus;

	@NotAudited
	@Column(name="APPLICATION_TYPE", length=100)
	private String applicationType;

	@Column(name="CMR_HOUSEOLD_ID", precision=22)
	private BigDecimal cmrHouseoldId;

	@Column(name="CREATED_BY", precision=10)
	private BigDecimal createdBy;

	@Column(name="CREATION_TIMESTAMP")
	private Timestamp creationTimestamp;

	@NotAudited
	@Column(name="ESIGN_DATE")
	private Timestamp esignDate;

	@NotAudited
	@Column(name="ESIGN_FIRST_NAME", length=100)
	private String esignFirstName;

	@NotAudited
	@Column(name="ESIGN_LAST_NAME", length=100)
	private String esignLastName;

	@NotAudited
	@Column(name="ESIGN_MIDDLE_NAME", length=100)
	private String esignMiddleName;

	@NotAudited
	@Column(name="CASE_NUMBER", length=100)
	private String caseNumber;

	@NotAudited
	@Column(name="LAST_NOTICE_ID", precision=22)
	private BigDecimal lastNoticeId;

	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Timestamp lastUpdateTimestamp;

	@NotAudited
	@Column(length=100)
	private String source;

	@NotAudited
	@Column(name="SSAP_APPLICATION_SECTION_ID", precision=22)
	private BigDecimal ssapApplicationSectionId;

	@NotAudited
	@Column(name="CURRENT_PAGE_ID", length=100)
	private String currentPageId;

	@NotAudited
	@Column(name="COVERAGE_YEAR")
	private long coverageYear;

	@Column(name="ELIGIBILITY_STATUS", length=50)
	@Enumerated(EnumType.STRING)
	private EligibilityStatus eligibilityStatus;

	@Column(name="EXCHANGE_ELIGIBILITY_STATUS", length=10)
	@Enumerated(EnumType.STRING)
	private ExchangeEligibilityStatus exchangeEligibilityStatus;

	@NotAudited
	@Column(name="CSR_LEVEL", length=10)
	private String csrLevel;

	@NotAudited
	@Column(name="ENABLE_ENROLLMENT", length=1)
	private String allowEnrollment;

	@NotAudited
	@Column(name="ENROLLMENT_STATUS", length=50)
	private String enrollmentStatus;

	@NotAudited
	@Column(name = "MAXIMUM_APTC")
	private BigDecimal maximumAPTC;

	@NotAudited
	@Column(name = "MAXIMUM_STATE_SUBSIDY")
	private BigDecimal maximumStateSubsidy;

	@NotAudited
	@Column(name = "ELECTED_APTC")
	private BigDecimal electedAPTC;

	@NotAudited
	@Column(name = "FINANCIAL_ASSISTANCE_FLAG")
	private String financialAssistanceFlag;

	@NotAudited
	@Column(name= "EXTERNAL_APPLICATION_ID")
	private String externalApplicationId;

	@Column(name = "AVAILABLE_APTC")
	private BigDecimal availableAPTC;

	@NotAudited
	@Column(name = "ELIGIBILITY_RECEIVED_DATE")
	private Date eligibilityReceivedDate;

	@NotAudited
	@Column(name = "ELIGIBILITY_RESPONSE_TYPE")
	private String eligibilityResponseType;

	@NotAudited
	@Column(name = "EXEMPT_HOUSEHOLD")
	private String exemptHousehold;
	
	@Column(name="LAST_UPDATED_BY", precision=10)
	private BigDecimal lastUpdatedBy;
	
	@NotAudited
	@Column(name = "ZIPCODE")
	private String zipCode; 
	
	@NotAudited
	@Column(name = "COUNTY_CODE")
	private String countyCode; 
	
	@NotAudited
	@Column(name="STATE")
    private String state;
	
	@NotAudited
	@Column(name= "EXCHANGE_TYPE")     //SSAP Lite HIX-57885 gjape
	private  String exchangeType;
	
	@NotAudited
	@Column(name = "EHB_AMOUNT")
	private BigDecimal ehbAmount;
	
	public String getCurrentPageId() {
		return currentPageId;
	}

	public void setCurrentPageId(String currentPageId) {
		this.currentPageId = currentPageId;
	}
	
	@NotAudited
	@Column(name = "NATIVE_AMERICAN_FLAG")
	private String nativeAmerican;

	@NotAudited
	@Column(name="START_DATE")
	private Timestamp startDate;

	//bi-directional many-to-one association to SsapApplicant
	//@JsonManagedReference
	@NotAudited
	@OneToMany(mappedBy="ssapApplication", cascade = CascadeType.PERSIST)
	private List<SsapApplicant> ssapApplicants;

	//bi-directional many-to-one association to SsapApplicationEvent
	@NotAudited
	@OneToMany(mappedBy="ssapApplication", cascade = CascadeType.PERSIST)
	//@JsonManagedReference
	private List<SsapApplicationEvent> ssapApplicationEvents;
	
	@NotAudited
	@Column(name="PARENT_SSAP_APPLICATION_ID", precision=22)
	private BigDecimal parentSsapApplicationId;
	
	
	@NotAudited
	@Column(name="RENEWAL_STATUS", length=50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus renewalStatus;
	
	@NotAudited 
	@Column(name="SEP_QEP_DENIED", length=1) 
	private String sepQepDenied;
	
	@NotAudited
	@Column(name="DENTAL_RENEWAL_STATUS", length=50)
	@Enumerated(EnumType.STRING)
	private RenewalStatus dentalRenewalStatus;
	
	@NotAudited
	@Column(name = "SHOP_APTC_AMT")
	private BigDecimal shopAptcAmnt;

	@NotAudited
	@Column(name = "SHOP_STATE_SUBSIDY_AMT")
	private BigDecimal shopStateSubsidyAmnt;

//	@Enumerated(value = EnumType.STRING)
//	@Column(name = "application_status")
//	private ApplicationStatus applicationStatus;
	
	@NotAudited
	@Column(name = "EFFECTIVE_DATE")
	private Date effectiveDate;
	
	@NotAudited
	@Column(name="APPLICATION_DENTAL_STATUS")
	private String applicationDentalStatus;
	
	@NotAudited
	@Column(name="outbound_case_number", length=100)
	private String outboundCaseNumber;

	public String getExemptHousehold() {
		return exemptHousehold;
	}

	public void setExemptHousehold(String exemptHousehold) {
		this.exemptHousehold = exemptHousehold;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getShopAptcAmnt() {
		return shopAptcAmnt;
	}

	public void setShopAptcAmnt(BigDecimal shopAptcAmnt) {
		this.shopAptcAmnt = shopAptcAmnt;
	}

	public String getSepQepDenied() {
		return sepQepDenied;
	}

	public void setSepQepDenied(String sepQepDenied) {
		this.sepQepDenied = sepQepDenied;
	}

	public BigDecimal getParentSsapApplicationId() {
		return parentSsapApplicationId;
	}

	public void setParentSsapApplicationId(BigDecimal parentSsapApplicationId) {
		this.parentSsapApplicationId = parentSsapApplicationId;
	}

	public SsapApplication() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplicationData() {
		return this.applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public String getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getApplicationType() {
		return this.applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public BigDecimal getCmrHouseoldId() {
		return this.cmrHouseoldId;
	}

	public void setCmrHouseoldId(BigDecimal cmrHouseoldId) {
		this.cmrHouseoldId = cmrHouseoldId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEsignDate() {
		return this.esignDate;
	}

	public void setEsignDate(Timestamp esignDate) {
		this.esignDate = esignDate;
	}

	public String getEsignFirstName() {
		return this.esignFirstName;
	}

	public void setEsignFirstName(String esignFirstName) {
		this.esignFirstName = esignFirstName;
	}

	public String getEsignLastName() {
		return this.esignLastName;
	}

	public void setEsignLastName(String esignLastName) {
		this.esignLastName = esignLastName;
	}

	public String getEsignMiddleName() {
		return this.esignMiddleName;
	}

	public void setEsignMiddleName(String esignMiddleName) {
		this.esignMiddleName = esignMiddleName;
	}

	public String getCaseNumber() {
		return this.caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public BigDecimal getLastNoticeId() {
		return this.lastNoticeId;
	}

	public void setLastNoticeId(BigDecimal lastNoticeId) {
		this.lastNoticeId = lastNoticeId;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public BigDecimal getSsapApplicationSectionId() {
		return this.ssapApplicationSectionId;
	}

	public void setSsapApplicationSectionId(BigDecimal ssapApplicationSectionId) {
		this.ssapApplicationSectionId = ssapApplicationSectionId;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public List<SsapApplicant> getSsapApplicants() {
		return this.ssapApplicants;
	}

	public void setSsapApplicants(List<SsapApplicant> ssapApplicants) {
		this.ssapApplicants = ssapApplicants;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public SsapApplicant addSsapApplicant(SsapApplicant ssapApplicant) {
		getSsapApplicants().add(ssapApplicant);
		ssapApplicant.setSsapApplication(this);

		return ssapApplicant;
	}

	public SsapApplicant removeSsapApplicant(SsapApplicant ssapApplicant) {
		getSsapApplicants().remove(ssapApplicant);
		ssapApplicant.setSsapApplication(null);

		return ssapApplicant;
	}

	public List<SsapApplicationEvent> getSsapApplicationEvents() {
		return this.ssapApplicationEvents;
	}

	public void setSsapApplicationEvents(List<SsapApplicationEvent> ssapApplicationEvents) {
		this.ssapApplicationEvents = ssapApplicationEvents;
	}

	public SsapApplicationEvent addSsapApplicationEvent(SsapApplicationEvent ssapApplicationEvent) {
		getSsapApplicationEvents().add(ssapApplicationEvent);
		ssapApplicationEvent.setSsapApplication(this);

		return ssapApplicationEvent;
	}

	public SsapApplicationEvent removeSsapApplicationEvent(SsapApplicationEvent ssapApplicationEvent) {
		getSsapApplicationEvents().remove(ssapApplicationEvent);
		ssapApplicationEvent.setSsapApplication(null);

		return ssapApplicationEvent;
	}


	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getAllowEnrollment() {
		return allowEnrollment;
	}

	public void setAllowEnrollment(String allowEnrollment) {
		this.allowEnrollment = allowEnrollment;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public BigDecimal getMaximumAPTC() {
		return maximumAPTC;
	}

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
		this.maximumAPTC = maximumAPTC;
	}

	public BigDecimal getMaximumStateSubsidy() {
		return maximumStateSubsidy;
	}

	public void setMaximumStateSubsidy(BigDecimal maximumStateSubsidy) {
		this.maximumStateSubsidy = maximumStateSubsidy;
	}

	public BigDecimal getElectedAPTC() {
		return electedAPTC;
	}

	public void setElectedAPTC(BigDecimal electedAPTC) {
		this.electedAPTC = electedAPTC;
	}

	public ExchangeEligibilityStatus getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}

	public void setExchangeEligibilityStatus(ExchangeEligibilityStatus exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}

	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}

	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

	public BigDecimal getAvailableAPTC() {
		return availableAPTC;
	}

	public void setAvailableAPTC(BigDecimal availableAPTC) {
		this.availableAPTC = availableAPTC;
	}

	public Date getEligibilityReceivedDate() {
		return eligibilityReceivedDate;
	}

	public void setEligibilityReceivedDate(Date eligibilityReceivedDate) {
		this.eligibilityReceivedDate = eligibilityReceivedDate;
	}

	public String getEligibilityResponseType() {
		return eligibilityResponseType;
	}

	public void setEligibilityResponseType(String eligibilityResponseType) {
		this.eligibilityResponseType = eligibilityResponseType;
	}

	public String getNativeAmerican() {
		return nativeAmerican;
	}

	public void setNativeAmerican(String nativeAmerican) {
		this.nativeAmerican = nativeAmerican;
	}

	public BigDecimal getEhbAmount() {
		return ehbAmount;
	}

	public void setEhbAmount(BigDecimal ehbAmount) {
		this.ehbAmount = ehbAmount;
	}
	
	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		// Add a comment to this line
		this.setCreationTimestamp(new TSTimestamp());
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setLastUpdateTimestamp(new TSTimestamp());
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public RenewalStatus getRenewalStatus() {
		return renewalStatus;
	}

	public void setRenewalStatus(RenewalStatus renewalStatus) {
		this.renewalStatus = renewalStatus;
	}

	public RenewalStatus getDentalRenewalStatus() {
		return dentalRenewalStatus;
	}

	public void setDentalRenewalStatus(RenewalStatus dentalRenewalStatus) {
		this.dentalRenewalStatus = dentalRenewalStatus;
	}

	public ApplicationValidationStatus getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(ApplicationValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}

	public BigDecimal getShopStateSubsidyAmnt() {
		return shopStateSubsidyAmnt;
	}

	public void setShopStateSubsidyAmnt(BigDecimal shopStateSubsidyAmnt) {
		this.shopStateSubsidyAmnt = shopStateSubsidyAmnt;
	}

	public String getApplicationDentalStatus() {
		return applicationDentalStatus;
	}

	public void setApplicationDentalStatus(String applicationDentalStatus) {
		this.applicationDentalStatus = applicationDentalStatus;
	}

	public String getOutboundCaseNumber() {
		return outboundCaseNumber;
	}

	public void setOutboundCaseNumber(String outboundATCaseNumber) {
		this.outboundCaseNumber = outboundATCaseNumber;
	}
}
