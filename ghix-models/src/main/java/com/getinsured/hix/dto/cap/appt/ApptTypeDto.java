package com.getinsured.hix.dto.cap.appt;

/**
 * Created by chen_j on 2/14/17.
 */
public class ApptTypeDto {

    private String name;
    private String code;
    private int durationMinutes;
    private int preBufferMinutes;
    private int postBufferMinutes;
    private int intervalMinutes;
    
    public ApptTypeDto() {
    	this.name = "";
		this.code = "";
		this.durationMinutes = 0;
		this.preBufferMinutes = 0;
		this.postBufferMinutes = 0;
		this.intervalMinutes = 0;
    }
    
    public ApptTypeDto(String name, String code,int durationMinutes, int preBufferMinutes, int postBufferMinutes, int intervalMinutes) {
    	this.name = name;
		this.code = code;
		this.durationMinutes = durationMinutes;
		this.preBufferMinutes = preBufferMinutes;
		this.postBufferMinutes = postBufferMinutes;
		this.intervalMinutes = intervalMinutes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public int getPreBufferMinutes() {
        return preBufferMinutes;
    }

    public void setPreBufferMinutes(int preBufferMinutes) {
        this.preBufferMinutes = preBufferMinutes;
    }

    public int getPostBufferMinutes() {
        return postBufferMinutes;
    }

    public void setPostBufferMinutes(int postBufferMinutes) {
        this.postBufferMinutes = postBufferMinutes;
    }

    public int getIntervalMinutes() {
        return intervalMinutes;
    }

    public void setIntervalMinutes(int intervalMinutes) {
        this.intervalMinutes = intervalMinutes;
    }
}
