package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ENRL_CMS_FILE_TRANSFER_LOG")
public class EnrollmentCmsFileTransferLog implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum Direction {
		UPLOAD, DOWNLOAD
	}

	public static enum ReportType {
		IRS, PLR, CMS, ANNUAL, CARRIERRECON
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_FILE_TRANSFER_LOG_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_FILE_TRANSFER_LOG_SEQ", sequenceName = "ENRL_CMS_FILE_TRANSFER_LOG_SEQ", allocationSize = 1)
	private int id;

	@Column(name = "DIRECTION")
	private String direction;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "REPORT_TYPE")
	private String reportType;

	@Column(name = "SOURCE_DIR")
	private String sourceDirectory;

	@Column(name = "TARGET_DIR")
	private String targetDirectory;

	@Column(name = "FILE_SIZE")
	private Long fileSize;

	@Column(name = "BATCH_EXECUTION_ID")
	private Long batchExecutionId;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "ERROR_DESC")
	private String errorDescription;

	@Column(name = "ARCHIVE_DIR")
	private String archiveDirectory;
	
	@Column(name = "EXCHG_TRANSFER_STATUS")
	private String exchgTransferStatus;

	@Column(name = "EXCHG_FAILURE_MSG")
	private String exchgFailureMsg;

	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATED_TIMESTAMP", nullable = false)
	private Date updatedOn;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction
	 *            the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType
	 *            the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the sourceDirectory
	 */
	public String getSourceDirectory() {
		return sourceDirectory;
	}

	/**
	 * @param sourceDirectory
	 *            the sourceDirectory to set
	 */
	public void setSourceDirectory(String sourceDirectory) {
		this.sourceDirectory = sourceDirectory;
	}

	/**
	 * @return the targetDirectory
	 */
	public String getTargetDirectory() {
		return targetDirectory;
	}

	/**
	 * @param targetDirectory
	 *            the targetDirectory to set
	 */
	public void setTargetDirectory(String targetDirectory) {
		this.targetDirectory = targetDirectory;
	}

	/**
	 * @return the fileSize
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize
	 *            the fileSize to set
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the batchExecutionId
	 */
	public Long getBatchExecutionId() {
		return batchExecutionId;
	}

	/**
	 * @param batchExecutionId
	 *            the batchExecutionId to set
	 */
	public void setBatchExecutionId(Long batchExecutionId) {
		this.batchExecutionId = batchExecutionId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = truncateErrorDescription(errorDescription);
	}

	/**
	 * @return the archiveDirectory
	 */
	public String getArchiveDirectory() {
		return archiveDirectory;
	}

	/**
	 * @param archiveDirectory
	 *            the archiveDirectory to set
	 */
	public void setArchiveDirectory(String archiveDirectory) {
		this.archiveDirectory = archiveDirectory;
	}

	/**
	 * @return the exchgTransferStatus
	 */
	public String getExchgTransferStatus() {
		return exchgTransferStatus;
	}

	/**
	 * @param exchgTransferStatus the exchgTransferStatus to set
	 */
	public void setExchgTransferStatus(String exchgTransferStatus) {
		this.exchgTransferStatus = exchgTransferStatus;
	}

	/**
	 * @return the exchgFailureMsg
	 */
	public String getExchgFailureMsg() {
		return exchgFailureMsg;
	}

	/**
	 * @param exchgFailureMsg the exchgFailureMsg to set
	 */
	public void setExchgFailureMsg(String exchgFailureMsg) {
		this.exchgFailureMsg = truncateErrorDescription(exchgFailureMsg);
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	private String truncateErrorDescription(String errorDescription) {
		if(null != errorDescription && errorDescription.length() >1000){
			return errorDescription.substring(0, 1000);
		}
		return errorDescription;
	}
}
