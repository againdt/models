package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * -----------------------------------------------------------------------------
 * RenewalStatusNotToConsiderRequestDTO class is used to get request for setNotToConsiderRenewalStatus API.
 * -----------------------------------------------------------------------------
 * 
 * @since July 25, 2019
 */
public class RenewalStatusNotToConsiderRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long ssapApplicationId;
	private String planType;

	public RenewalStatusNotToConsiderRequestDTO() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}
}
