package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "PLAN_AME")
public class PlanAME implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_AME_SEQ")
	@SequenceGenerator(name = "PLAN_AME_SEQ", sequenceName = "PLAN_AME_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;

	@Column(name = "DEDUCTIBLE_VAL")
	private Integer deductibleValue;

	@Column(name = "DEDUCTIBLE_ATTR")
	private String deductibleAttribute;

	@Column(name = "BENEFIT_MAX_VAL")
	private Integer benefitMaxValue;

	@Column(name = "BENEFIT_MAX_ATTR")
	private String benefitMaxAttribute;

	@Column(name = "EXCLUSIONS_URL")
	private String exclusionURL;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public PlanAME() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Integer getDeductibleValue() {
		return deductibleValue;
	}

	public void setDeductibleValue(Integer deductibleValue) {
		this.deductibleValue = deductibleValue;
	}

	public String getDeductibleAttribute() {
		return deductibleAttribute;
	}

	public void setDeductibleAttribute(String deductibleAttribute) {
		this.deductibleAttribute = deductibleAttribute;
	}

	public Integer getBenefitMaxValue() {
		return benefitMaxValue;
	}

	public void setBenefitMaxValue(Integer benefitMaxValue) {
		this.benefitMaxValue = benefitMaxValue;
	}

	public String getBenefitMaxAttribute() {
		return benefitMaxAttribute;
	}

	public void setBenefitMaxAttribute(String benefitMaxAttribute) {
		this.benefitMaxAttribute = benefitMaxAttribute;
	}

	public String getExclusionURL() {
		return exclusionURL;
	}

	public void setExclusionURL(String exclusionURL) {
		this.exclusionURL = exclusionURL;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
