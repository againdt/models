package com.getinsured.hix.dto.externalnotices;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

public class ExternalNoticesResponseDTO extends GHIXResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String integratedCaseId;
	
	private String mnsureId;
	
	private String firstName;
	
	private String lastName;
	
	private String dateOfBirth;
	
	private String coverageYear;

	public String getIntegratedCaseId() {
		return integratedCaseId;
	}

	public void setIntegratedCaseId(String integratedCaseId) {
		this.integratedCaseId = integratedCaseId;
	}

	public String getMnsureId() {
		return mnsureId;
	}

	public void setMnsureId(String mnsureId) {
		this.mnsureId = mnsureId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}
	

}
