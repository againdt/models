DELETE FROM gi_app_config WHERE property_key IN('finance.AcceptCreditCard',
'finance.BinderGraceDays', 'finance.ElectronicPaymentOnly', 'finance.ExchangeDeductRemittence',
'finance.ExchangeFee', 'finance.IsBinderInvoice', 'finance.IsPaymentGateway', 'finance.MaximumPaymentThreshold',
'finance.MinimumPaymentThreshold', 'finance.ParitialPaymentAllocationType', 'finance.PartialPaymentSupported',
'finance.PaymentGracePeriod', 'finance.ProRatioSupported');


insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.IsBinderInvoice','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ElectronicPaymentOnly','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.BinderGraceDays','15','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ProRatioSupported','NO','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.PartialPaymentSupported','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.MinimumPaymentThreshold','80','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ParitialPaymentAllocationType','ratio','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.MaximumPaymentThreshold','200','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.ExchangeDeductRemittence','YES','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.AcceptCreditCard','NO','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.PaymentGracePeriod','30','',null,current_timestamp,null,current_timestamp);

insert into gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP)
values (GI_APP_CONFIG_SEQ.nextval,'finance.IsPaymentGateway','TRUE','',null,current_timestamp,null,current_timestamp);

INSERT INTO gi_app_config (ID,PROPERTY_KEY,PROPERTY_VALUE,DESCRIPTION,CREATED_BY,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP) 
VALUES (GI_APP_CONFIG_SEQ.nextval,'finance.ExchangeFee','7.5','',null,current_timestamp,null,current_timestamp);