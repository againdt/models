package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

/**
 * DTO for External Employer/Individual request
 *
 */
public class EmployerIndividualBOBDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long id;
	private List<Long> listOfIDs;
	
	public List<Long> getListOfIDs() {
		return listOfIDs;
	}
	public void setListOfIDs(List<Long> listOfIDs) {
		this.listOfIDs = listOfIDs;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
    @Override
    public String toString() {
    	return "EmployerIndividualBOBDTO details: Id = "+id+", ListOfIDs = "+listOfIDs;
    }
}
