package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Enrollment1095DTO implements Serializable{
	
	private String policyNumber;
	private String recepientFirstName;
	private String recepientLastName;
	private String spouseFirstName;
	private String spouseLastName;
	private String recepientSsn;
	private String spouseSsn;
	private Date recepientDob;
	private Date spouseDob;
	private String marketPlaceIdentifier;
	
	private int id;
	private String exchgAsignedPolicyId;
	private String houseHoldCaseId;
	private Integer coverageYear;
	private String correctedCheckBoxIndicator;
	private String voidCheckboxindicator;
	private String policyIssuerName; 
	private String policyStartDate;
	private String policyEndDate;
	private String resendPdfFlag;
	private String correctionSource;
	private String editToolOverwritten;
	private String correctionIndicatorPdf;
	private String correctionIndicatorXml;
	private String createdOn;
	private String updatedOn;
	private String correctedRecordSeqNum;
	private String ecmDocId;
	private String pdfGeneratedOn;
	private String cmsXmlFileName;
	private String cmsXmlGeneratedOn;
	private String updateNotes;
	private String pdfSkippedFlag;
	private String pdfSkippedMsg;
	private String xmlSkippedFlag;
	private String xmlSkippedMsg;
	private String isActive;
	private List<EnrollmentMember1095DTO> enrollmentMembers;
	private List<EnrollmentPremium1095DTO> enrollmentPremiums;
	private String originalBatchId;
	private String pdfFileName;
	
	public String getPolicyNumber() {	
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getRecepientFirstName() {
		return recepientFirstName;
	}
	public void setRecepientFirstName(String recepientFirstName) {
		this.recepientFirstName = recepientFirstName;
	}
	public String getRecepientLastName() {
		return recepientLastName;
	}
	public void setRecepientLastName(String recepientLastName) {
		this.recepientLastName = recepientLastName;
	}
	public String getSpouseFirstName() {
		return spouseFirstName;
	}
	public void setSpouseFirstName(String spouseFirstName) {
		this.spouseFirstName = spouseFirstName;
	}
	public String getSpouseLastName() {
		return spouseLastName;
	}
	public void setSpouseLastName(String spouseLastName) {
		this.spouseLastName = spouseLastName;
	}
	public String getRecepientSsn() {
		return recepientSsn;
	}
	public void setRecepientSsn(String recepientSsn) {
		this.recepientSsn = recepientSsn;
	}
	public String getSpouseSsn() {
		return spouseSsn;
	}
	public void setSpouseSsn(String spouseSsn) {
		this.spouseSsn = spouseSsn;
	}
	
	public Date getRecepientDob() {
		return recepientDob;
	}
	public void setRecepientDob(Date recepientDob) {
		this.recepientDob = recepientDob;
	}
	public Date getSpouseDob() {
		return spouseDob;
	}
	public void setSpouseDob(Date spouseDob) {
		this.spouseDob = spouseDob;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getExchgAsignedPolicyId() {
		return exchgAsignedPolicyId;
	}
	public void setExchgAsignedPolicyId(String exchgAsignedPolicyId) {
		this.exchgAsignedPolicyId = exchgAsignedPolicyId;
	}
	public String getHouseHoldCaseId() {
		return houseHoldCaseId;
	}
	public void setHouseHoldCaseId(String houseHoldCaseId) {
		this.houseHoldCaseId = houseHoldCaseId;
	}
	public Integer getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getCorrectedCheckBoxIndicator() {
		return correctedCheckBoxIndicator;
	}
	public void setCorrectedCheckBoxIndicator(String correctedCheckBoxIndicator) {
		this.correctedCheckBoxIndicator = correctedCheckBoxIndicator;
	}
	public String getVoidCheckboxindicator() {
		return voidCheckboxindicator;
	}
	public void setVoidCheckboxindicator(String voidCheckboxindicator) {
		this.voidCheckboxindicator = voidCheckboxindicator;
	}
	public String getPolicyIssuerName() {
		return policyIssuerName;
	}
	public void setPolicyIssuerName(String policyIssuerName) {
		this.policyIssuerName = policyIssuerName;
	}
	public String getPolicyStartDate() {
		return policyStartDate;
	}
	public void setPolicyStartDate(String policyStartDate) {
		this.policyStartDate = policyStartDate;
	}
	public String getPolicyEndDate() {
		return policyEndDate;
	}
	public void setPolicyEndDate(String policyEndDate) {
		this.policyEndDate = policyEndDate;
	}
	public String getResendPdfFlag() {
		return resendPdfFlag;
	}
	public void setResendPdfFlag(String resendPdfFlag) {
		this.resendPdfFlag = resendPdfFlag;
	}
	public String getCorrectionSource() {
		return correctionSource;
	}
	public void setCorrectionSource(String correctionSource) {
		this.correctionSource = correctionSource;
	}
	public String getEditToolOverwritten() {
		return editToolOverwritten;
	}
	public void setEditToolOverwritten(String editToolOverwritten) {
		this.editToolOverwritten = editToolOverwritten;
	}
	public String getCorrectionIndicatorPdf() {
		return correctionIndicatorPdf;
	}
	public void setCorrectionIndicatorPdf(String correctionIndicatorPdf) {
		this.correctionIndicatorPdf = correctionIndicatorPdf;
	}
	public String getCorrectionIndicatorXml() {
		return correctionIndicatorXml;
	}
	public void setCorrectionIndicatorXml(String correctionIndicatorXml) {
		this.correctionIndicatorXml = correctionIndicatorXml;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getCorrectedRecordSeqNum() {
		return correctedRecordSeqNum;
	}
	public void setCorrectedRecordSeqNum(String correctedRecordSeqNum) {
		this.correctedRecordSeqNum = correctedRecordSeqNum;
	}
	public String getEcmDocId() {
		return ecmDocId;
	}
	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}
	public String getPdfGeneratedOn() {
		return pdfGeneratedOn;
	}
	public void setPdfGeneratedOn(String pdfGeneratedOn) {
		this.pdfGeneratedOn = pdfGeneratedOn;
	}
	public String getCmsXmlFileName() {
		return cmsXmlFileName;
	}
	public void setCmsXmlFileName(String cmsXmlFileName) {
		this.cmsXmlFileName = cmsXmlFileName;
	}
	public String getCmsXmlGeneratedOn() {
		return cmsXmlGeneratedOn;
	}
	public void setCmsXmlGeneratedOn(String cmsXmlGeneratedOn) {
		this.cmsXmlGeneratedOn = cmsXmlGeneratedOn;
	}
	public String getUpdateNotes() {
		return updateNotes;
	}
	public void setUpdateNotes(String updateNotes) {
		this.updateNotes = updateNotes;
	}
	public String getPdfSkippedFlag() {
		return pdfSkippedFlag;
	}
	public void setPdfSkippedFlag(String pdfSkippedFlag) {
		this.pdfSkippedFlag = pdfSkippedFlag;
	}
	public String getPdfSkippedMsg() {
		return pdfSkippedMsg;
	}
	public void setPdfSkippedMsg(String pdfSkippedMsg) {
		this.pdfSkippedMsg = pdfSkippedMsg;
	}
	public String getXmlSkippedFlag() {
		return xmlSkippedFlag;
	}
	public void setXmlSkippedFlag(String xmlSkippedFlag) {
		this.xmlSkippedFlag = xmlSkippedFlag;
	}
	public String getXmlSkippedMsg() {
		return xmlSkippedMsg;
	}
	public void setXmlSkippedMsg(String xmlSkippedMsg) {
		this.xmlSkippedMsg = xmlSkippedMsg;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public List<EnrollmentMember1095DTO> getEnrollmentMembers() {
		return enrollmentMembers;
	}
	public void setEnrollmentMembers(List<EnrollmentMember1095DTO> enrollmentMembers) {
		this.enrollmentMembers = enrollmentMembers;
	}
	public List<EnrollmentPremium1095DTO> getEnrollmentPremiums() {
		return enrollmentPremiums;
	}
	public void setEnrollmentPremiums(List<EnrollmentPremium1095DTO> enrollmentPremiums) {
		this.enrollmentPremiums = enrollmentPremiums;
	}
	public String getOriginalBatchId() {
		return originalBatchId;
	}
	public void setOriginalBatchId(String originalBatchId) {
		this.originalBatchId = originalBatchId;
	}
	public String getPdfFileName() {
		return pdfFileName;
	}
	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}
	public String getMarketPlaceIdentifier() {
		return marketPlaceIdentifier;
	}
	public void setMarketPlaceIdentifier(String marketPlaceIdentifier) {
		this.marketPlaceIdentifier = marketPlaceIdentifier;
	}
	
	
}
