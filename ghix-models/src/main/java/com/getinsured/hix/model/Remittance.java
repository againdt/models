package com.getinsured.hix.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;


@TypeDef(name = "vimoEncryptedString",
typeClass = VimoEncryptedStringType.class)
@Entity
@Table(name="remittance")
public class Remittance 
{
	
	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@Column(name = "statement_date")
	private Date statementDate;
	
	@Column(name = "employer_name")
	private String employerName;
	
	@Column(name = "employee_name")
	private String employeeName;

	@Column(name = "carrier_name")
	private String carrierName;
	
	@Column(name = "plan_type")
	private String planType;
	
	@Column(name = "status")
	private String status;
	
	@Column(name="net_amount")
	private float netAmount;
	
	@Column(name="amount_paid_to_issuer")
	private float amountPaidToIssuer;
	
	@Column(name = "due_amount")
	private float dueAmount;
	
	@Column(name="exchange_fees")
	private float exchangeFees;
	
	@Column(name="deducted_exchange_fees")
	private float deductedExchangeFees;

	@Column(name = "payment_date")
	private Date paymentDate;
	
	@Column(name = "transaction_id")
	private String transactionId;
	
	@Column(name = "payment_type")
	private String paymentType;
	
	@Column(name="account_number")
	@Type(type = "vimoEncryptedString")
	private String accountNumber;
	
	public int getId() {
		return id;
	}
	
	public float getNetAmount() {
		return netAmount;
	}

	public float getAmountPaidToIssuer() {
		return amountPaidToIssuer;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public String getPlanType() {
		return planType;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public String getStatus() {
		return status;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}
	
	public float getExchangeFees() {
		return exchangeFees;
	}

	public float getDeductedExchangeFees() {
		return deductedExchangeFees;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	
	public String getEmployerName() {
		return employerName;
	}

	public String getEmployeeName() {
		return employeeName;
	}
	
	public float getDueAmount() {
		return dueAmount;
	}

	public String getTransactionId() {
		return transactionId;
	}
}
