package com.getinsured.eligibility.enums;

public enum ExchangeEligibilityStatus
{
  APTC_CSR("APTC/CSR"),
  APTC("APTC"),
  CSR("CSR"),
  QHP("QHP"),
  NONE("Not Applicable"),
  CHIP("CHIP"),
  MEDICAID("MEDICAID");

  private String description;

  ExchangeEligibilityStatus(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }
}
