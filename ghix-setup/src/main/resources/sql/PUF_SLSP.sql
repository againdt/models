create table PUF_SLSP (
       ZIPCODE VARCHAR2(5 CHAR) not null,
       COUNTY VARCHAR2(60 CHAR) not null,
       FIPS VARCHAR2(5 CHAR) not null,
       YEAR number(4) not null,
       STATE VARCHAR2(2 CHAR) not null,
       SLSP21 float not null,
       LSP21 float not null,
       LBP21 float,
       PLAN_COUNT number(3) not null,
       CARRIER_COUNT number(3) not null,
       EXCHANGE_TYPE VARCHAR2(3 CHAR) DEFAULT 'FFM' NOT NULL CHECK(EXCHANGE_TYPE IN ('FFM', 'SBE')),
       constraint PK_PUF_SLSP primary key (ZIPCODE, county, state, year)
);
COMMENT ON TABLE PUF_SLSP  IS 'store aggregated PUF information for use in the Quote form and teaser API.  The table is not accessed directly--instead it is loaded into SOLR, where we serve it.';
COMMENT ON COLUMN PUF_SLSP.ZIPCODE IS '5 digit zip code';
COMMENT ON COLUMN PUF_SLSP.YEAR IS '4 digit year';
COMMENT ON COLUMN PUF_SLSP.COUNTY IS 'County Name';
COMMENT ON COLUMN PUF_SLSP.FIPS IS 'This is the standard numeric code for the county';
COMMENT ON COLUMN PUF_SLSP.STATE IS 'Two character state code/abbreviation';
COMMENT ON COLUMN PUF_SLSP.SLSP21 IS 'Second Lowest cost Silver Plan for 21 year old individual, a dollar amount, used for eligibility calculations';
COMMENT ON COLUMN PUF_SLSP.LSP21 IS 'Lowest cost Silver Plan for 21 year old individual, a dollar amount';
COMMENT ON COLUMN PUF_SLSP.LBP21 IS 'Lowest cost Bronze Plan for 21 year old individual, a dollar amount';
COMMENT ON COLUMN PUF_SLSP.PLAN_COUNT IS 'Plan_Count is the total number of available plans for this zip + county + year combo';
COMMENT ON COLUMN PUF_SLSP.CARRIER_COUNT IS 'Carrier_Count is the number of distinct carriers in this rating area';
COMMENT ON COLUMN PUF_SLSP.EXCHANGE_TYPE IS 'Lists whether this is a SBE (State-based exchagne) or a FFM (Federally Facilitated Marketplace)';
