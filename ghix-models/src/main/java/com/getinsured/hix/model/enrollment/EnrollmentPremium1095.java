package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="ENROLLMENT_PREMIUM_1095")
public class EnrollmentPremium1095 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum YorNFlagIndicator{
		Y ,N;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLLMENT_PREMIUM_1095_SEQ")
	@SequenceGenerator(name = "ENROLLMENT_PREMIUM_1095_SEQ", sequenceName = "ENROLLMENT_PREMIUM_1095_SEQ", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="ENROLLMENT_1095_ID")
	private Enrollment1095 enrollment1095;
	
	@Column(name = "MONTH_NUMBER")
	private Integer monthNumber;
	@Column(name = "MONTH_NAME")
	private String monthName;
	@Column(name = "SLCSP_AMOUNT")
	private Float slcspAmount;				
	@Column(name = "APTC_AMOUNT")
	private Float aptcAmount;
	@Column(name = "GROSS_PREMIUM")
	private Float grossPremium;
	@Column(name = "EHB_PERCENT")
	private Float ehbPercent;
	@Column(name = "PEDIATRIC_EHB_AMT")
	private Float pediatricEhbAmt;
	
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;

	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date updatedOn;

	//@Audited
	//@OneToOne
	//@JoinColumn(name="CREATED_BY")
	@Column(name="CREATED_BY")
	private Integer createdBy;

	//@Audited
	//@OneToOne
	//@JoinColumn(name="LAST_UPDATED_BY")
	@Column(name="LAST_UPDATED_BY")
	private Integer updatedBy;
	
	@Column(name = "UPDATE_NOTES")
	private String updateNotes;
	
	@Column(name = "IS_ACTIVE")
	private String isActive = "Y";
	
	@Column(name = "ACCOUNTABLE_MEMBER_DENTAL")
	private Integer accountableMemberDental;
	
	@Column(name = "SSAP_APP_ID")
	private Long ssapAppId;

	@Column(name = "EMPLOYEE_CONTRIBUTION")
	private Float employeeContribution;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the enrollment1095
	 */
	public Enrollment1095 getEnrollment1095() {
		return enrollment1095;
	}

	/**
	 * @param enrollment1095 the enrollment1095 to set
	 */
	public void setEnrollment1095(Enrollment1095 enrollment1095) {
		this.enrollment1095 = enrollment1095;
	}

	/**
	 * @return the monthNumber
	 */
	public Integer getMonthNumber() {
		return monthNumber;
	}

	/**
	 * @param monthNumber the monthNumber to set
	 */
	public void setMonthNumber(Integer monthNumber) {
		this.monthNumber = monthNumber;
	}

	/**
	 * @return the monthName
	 */
	public String getMonthName() {
		return monthName;
	}

	/**
	 * @param monthName the monthName to set
	 */
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	/**
	 * @return the slcspAmount
	 */
	public Float getSlcspAmount() {
		return slcspAmount;
	}

	/**
	 * @param slcspAmount the slcspAmount to set
	 */
	public void setSlcspAmount(Float slcspAmount) {
		this.slcspAmount = slcspAmount;
	}

	/**
	 * @return the aptcAmount
	 */
	public Float getAptcAmount() {
		return aptcAmount;
	}

	/**
	 * @param aptcAmount the aptcAmount to set
	 */
	public void setAptcAmount(Float aptcAmount) {
		this.aptcAmount = aptcAmount;
	}

	/**
	 * @return the grossPremium
	 */
	public Float getGrossPremium() {
		return grossPremium;
	}

	/**
	 * @param grossPremium the grossPremium to set
	 */
	public void setGrossPremium(Float grossPremium) {
		this.grossPremium = grossPremium;
	}

	/**
	 * @return the ehbPercent
	 */
	public Float getEhbPercent() {
		return ehbPercent;
	}

	/**
	 * @param ehbPercent the ehbPercent to set
	 */
	public void setEhbPercent(Float ehbPercent) {
		this.ehbPercent = ehbPercent;
	}

	/**
	 * @return the pediatricEhbAmt
	 */
	public Float getPediatricEhbAmt() {
		return pediatricEhbAmt;
	}

	/**
	 * @param pediatricEhbAmt the pediatricEhbAmt to set
	 */
	public void setPediatricEhbAmt(Float pediatricEhbAmt) {
		this.pediatricEhbAmt = pediatricEhbAmt;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updateNotes
	 */
	public String getUpdateNotes() {
		return updateNotes;
	}

	/**
	 * @param updateNotes the updateNotes to set
	 */
	public void setUpdateNotes(String updateNotes) {
		this.updateNotes = updateNotes;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the accountableMemberDental
	 */
	public Integer getAccountableMemberDental() {
		return accountableMemberDental;
	}

	/**
	 * @param accountableMemberDental the accountableMemberDental to set
	 */
	public void setAccountableMemberDental(Integer accountableMemberDental) {
		this.accountableMemberDental = accountableMemberDental;
	}
	
	public Long getSsapAppId() {
		return ssapAppId;
	}

	public void setSsapAppId(Long ssapAppId) {
		this.ssapAppId = ssapAppId;
	}

	public Float getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(Float employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate() {
		this.setUpdatedOn(new TSDate());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aptcAmount == null) ? 0 : aptcAmount.hashCode());
		result = prime * result + ((ehbPercent == null) ? 0 : ehbPercent.hashCode());
		result = prime * result + ((grossPremium == null) ? 0 : grossPremium.hashCode());
		result = prime * result + ((monthName == null) ? 0 : monthName.hashCode());
		result = prime * result + ((monthNumber == null) ? 0 : monthNumber.hashCode());
		result = prime * result + ((pediatricEhbAmt == null) ? 0 : pediatricEhbAmt.hashCode());
		result = prime * result + ((slcspAmount == null) ? 0 : slcspAmount.hashCode());
		result = prime * result + ((accountableMemberDental == null) ? 0 : accountableMemberDental.hashCode());
		result = prime * result + ((ssapAppId == null) ? 0 : ssapAppId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EnrollmentPremium1095 other = (EnrollmentPremium1095) obj;
		if (aptcAmount == null) {
			if (other.aptcAmount != null) {
				return false;
			}
		} else if (!aptcAmount.equals(other.aptcAmount)) {
			return false;
		}
		if (ehbPercent == null) {
			if (other.ehbPercent != null) {
				return false;
			}
		} else if (!ehbPercent.equals(other.ehbPercent)) {
			return false;
		}
		if (grossPremium == null) {
			if (other.grossPremium != null) {
				return false;
			}
		} else if (!grossPremium.equals(other.grossPremium)) {
			return false;
		}
		if (monthName == null) {
			if (other.monthName != null) {
				return false;
			}
		} else if (!monthName.equals(other.monthName)) {
			return false;
		}
		if (monthNumber == null) {
			if (other.monthNumber != null) {
				return false;
			}
		} else if (!monthNumber.equals(other.monthNumber)) {
			return false;
		}
		if (pediatricEhbAmt == null) {
			if (other.pediatricEhbAmt != null) {
				return false;
			}
		} else if (!pediatricEhbAmt.equals(other.pediatricEhbAmt)) {
			return false;
		}
		if (slcspAmount == null) { 
			if (other.slcspAmount != null) {
				return false;
			}
		} else if (!slcspAmount.equals(other.slcspAmount)) {
			return false;
		}
		if (accountableMemberDental == null) {
			if (other.accountableMemberDental != null) {
				return false;
			}
		} else if (!accountableMemberDental.equals(other.accountableMemberDental)) {
			return false;
		}
		return true;
	}

}
