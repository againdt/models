insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (41,1224,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (61,1225,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (1,1125,1);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (2,1177,1);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (3,1090,1);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (4,1104,2);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (5,1108,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (6,1165,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (7,1149,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (8,1164,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (9,1106,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (10,1107,3);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (11,1146,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (12,1144,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (13,1145,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (14,1188,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (16,1194,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (17,1190,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (18,Null,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (19,1192,4);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (20,1187,5);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (21,Null,5);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (22,1199,5);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (23,1200,6);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (24,66,7);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (25,1147,7);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (26,63,7);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (27,1175,7);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (28,1168,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (29,1182,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (30,1120,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (31,1099,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (32,1100,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (33,1183,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (34,1101,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (35,1185,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (36,1103,8);
insert into PERMISSIONS_GROUP_MAPPING (ID,PERMISSION_ID,PERMISSION_GROUP_ID) values (81,1227,4);
 commit;
DROP SEQUENCE PERMISSIONS_GROUP_MAPPING_SEQ;
CREATE SEQUENCE PERMISSIONS_GROUP_MAPPING_SEQ START WITH 1081;

