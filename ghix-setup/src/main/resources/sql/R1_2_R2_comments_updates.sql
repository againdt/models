COMMENT ON COLUMN ENROLLEE.MEMBER_EMPLOYER_ID IS
'Member''s employer id'
;
COMMENT ON COLUMN ENROLLEE_AUD.MEMBER_EMPLOYER_ID IS
'Member''s employer id'
;
COMMENT ON COLUMN ENROLLMENT.IND_ORDER_ITEMS_ID IS
'Individual order''s items id'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_FED_TAX_PAYER_ID IS
'Broker''s Federal tax id'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_TPA_ACCOUNT_NUMBER_1 IS
'Broker''s  TPA (Third-party Administrator) account number 1'
;
COMMENT ON COLUMN ENROLLMENT.BROKER_TPA_ACCOUNT_NUMBER_2 IS
'Broker''s  TPA (Third-party Administrator) account number 2'
;
COMMENT ON COLUMN ENROLLMENT_AUD.IND_ORDER_ITEMS_ID IS
'Individual order''s items id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_FED_TAX_PAYER_ID IS
'Broker''s Federal tax id'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_TPA_ACCOUNT_NUMBER_1 IS
'Broker''s  TPA (Third-party Administrator) account number 1'
;
COMMENT ON COLUMN ENROLLMENT_AUD.BROKER_TPA_ACCOUNT_NUMBER_2 IS
'Broker''s  TPA (Third-party Administrator) account number 2'
;
COMMENT ON COLUMN PLAN_HEALTH.SBC_DOC_NAME IS
'This is column tell about SupDocName - the name of the supporting document'
;
COMMENT ON COLUMN PLAN_HEALTH_AUD.SBC_DOC_NAME IS
'This is column tell about SupDocName - the name of the supporting document'
;
COMMENT ON COLUMN PLD_HOUSEHOLD.YEARLY_APTC IS
'The annual tax credit amount based on the individual''s income which is provided on an advance basis to an eligible Individual enrolled in a QHP through the Exchange'
;
COMMENT ON COLUMN PRESCREEN_DATA.INPUT_PAGE IS
'Possible values are P-Product, M-Market'
;