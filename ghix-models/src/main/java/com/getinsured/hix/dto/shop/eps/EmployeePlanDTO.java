package com.getinsured.hix.dto.shop.eps;

import java.util.HashMap;
import java.util.Map;

public class EmployeePlanDTO {

	private Float salary=0f;
	private Integer dependentCount;
	private Integer employee_id;
	
	private MinMaxPremium employeePlanPremium;
	private Map<String, MinMaxPremium> tierPremiums = new HashMap<String, MinMaxPremium>();

	public void saveAndUpdatePremiumn(Float totalPremium, Float indvPremium, Float depenPremium, EmployerPlansDTO.PlanLevel planLevel){
		if(employeePlanPremium == null){
			employeePlanPremium = new MinMaxPremium();
		}
		setPremiumValues(totalPremium,indvPremium,depenPremium,employeePlanPremium);
		
		MinMaxPremium minMaxPremiumPlanTier = tierPremiums.get(planLevel.toString().toUpperCase());
		 if(minMaxPremiumPlanTier == null){
			 minMaxPremiumPlanTier = new MinMaxPremium();
			 tierPremiums.put(planLevel.toString().toUpperCase(), minMaxPremiumPlanTier);
		 }
		 
		 setPremiumValues(totalPremium,indvPremium,depenPremium,minMaxPremiumPlanTier);
	}
	
	private void setPremiumValues(Float totalPremium, Float indvPremium, Float depenPremium, MinMaxPremium employeePlanPremium) {
		
		// Min Max Premiumns
		 if(employeePlanPremium.getMin_totalPremium() == 0f || (totalPremium < employeePlanPremium.getMin_totalPremium())){
			 employeePlanPremium.setMin_totalPremium(totalPremium);
			 employeePlanPremium.setMin_indvPremium(indvPremium);
			 employeePlanPremium.setMin_depePremium(depenPremium);
		 }
		 if(employeePlanPremium.getMax_totalPremium() == 0f || (totalPremium > employeePlanPremium.getMax_totalPremium())){
			 employeePlanPremium.setMax_totalPremium(totalPremium);
			 employeePlanPremium.setMax_indvPremium(indvPremium);
			 employeePlanPremium.setMax_depePremium(depenPremium);
		 }		 
	  }
	
	
	public MinMaxPremium getEmployeePlanPremium() {
		return employeePlanPremium;
	}

	public void setEmployeePlanPremium(MinMaxPremium employeePlanPremium) {
		this.employeePlanPremium = employeePlanPremium;
	}

	public Map<String, MinMaxPremium> getTierPremiums() {
		return tierPremiums;
	}

	public void setTierPremiums(Map<String, MinMaxPremium> tierPremiums) {
		this.tierPremiums = tierPremiums;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public Integer getDependentCount() {
		return dependentCount;
	}

	public void setDependentCount(Integer dependentCount) {
		this.dependentCount = dependentCount;
	}

	public Integer getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(Integer employee_id) {
		this.employee_id = employee_id;
	}

}
