package com.getinsured.hix.dto.consumer.goodrx;

import java.io.Serializable;
import java.util.List;

public class DrugDetailsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * This DTO contains the attributes to store the data returned by the following GoodRx api - Drug Search API, Low Price API and Price Compare API.
	 * 
	 * Following is a set of common attributes that are returned by low price and compare price APIs
	 * */
	private List<String> brand;
	private List<String> generic;
	private String display;
	private String form;
	private String dosage;
	private Double quantity;
	private Double price;
	private String url;
	private String manufacturer;
	private String quantityFormDosage;
	private long prescriptionId;
	
	

	/**
	 Following is the data returned by the drug search api
	 **/
	private List<String> candidates;
	
	/**
	 * Attributes returned only by the compare price api
	 */
	List<Double> prices;
	PriceDetailsDTO price_detail;
	List<ComparePriceDTO> comparePriceList;

	
	public List<String> getBrand() {
		return brand;
	}
	public void setBrand(List<String> brand) {
		this.brand = brand;
	}
	public List<String> getGeneric() {
		return generic;
	}
	public void setGeneric(List<String> generic) {
		this.generic = generic;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getDosage() {
		return dosage;
	}
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public List<String> getCandidates() {
		return candidates;
	}
	public void setCandidates(List<String> candidates) {
		this.candidates = candidates;
	}
	public List<Double> getPrices() {
		return prices;
	}
	public void setPrices(List<Double> prices) {
		this.prices = prices;
	}
	public PriceDetailsDTO getPrice_detail() {
		return price_detail;
	}
	public void setPrice_detail(PriceDetailsDTO price_detail) {
		this.price_detail = price_detail;
	}
	public List<ComparePriceDTO> getComparePriceList() {
		return comparePriceList;
	}
	public void setComparePriceList(List<ComparePriceDTO> comparePriceList) {
		this.comparePriceList = comparePriceList;
	}
	public String getQuantityFormDosage() {
		return quantityFormDosage;
	}
	public void setQuantityFormDosage(String quantityFormDosage) {
		this.quantityFormDosage = quantityFormDosage;
	}
	public long getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}
	
	
		
			
}
