package com.getinsured.eligibility.startdates.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto.Status;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class EligibilityDatesOverrideDTO {

	@NotNull
	private Long cmrHouseholdId;
	@NotNull
	private Long enAppId;
	@NotNull
	private Long erAppId;
	@NotNull
	private Date enrollmentStartDate;
	@NotNull
	private Date enrollmentEndDate;
	@NotNull
	private Boolean editMode;

	private boolean dentalOnly;

	private Date financialEffectiveDate;
	private Status status;
	private boolean isUpdated;
}
