package com.getinsured.hix.dto.eapp;

import java.util.HashMap;
import java.util.Map;

/**
 * FIXME: Jeff we should rename these HLT codes to real things.
 * Humans are reading/writing this enum and hence let's not get into
 * LookupCode shit that has plagued our code base. 
 * @author root
 *
 */
public enum ProductTypeEnum {

	HLT("health"), STM("stm"), DTL("dental"), OTHER("other"), MC("medicare"), MC_SUP("medsup");
	
	private static final Map<String, ProductTypeEnum> ProductTypeEnumMap = new HashMap<String, ProductTypeEnum>();
	public String productName;
	
	static {
		for (ProductTypeEnum type : ProductTypeEnum.values()) {
			ProductTypeEnumMap.put(type.getProductName(), type);
		}
	}

	private ProductTypeEnum(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}
	
	public static ProductTypeEnum getEnumFromValue(String in) {
		ProductTypeEnum type = ProductTypeEnumMap.get(in == null ? "other" : in.toLowerCase());
		if (type == null)
			return ProductTypeEnum.OTHER;
		return type;
	}
	
}
