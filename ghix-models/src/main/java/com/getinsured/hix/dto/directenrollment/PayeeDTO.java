package com.getinsured.hix.dto.directenrollment;

public class PayeeDTO {
	private String fname;
	private String lname;
	private String addrStreet;
	private String addrCity;
	private String addrState;
	private String addrZip;
	private String email;

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getAddrStreet() {
		return addrStreet;
	}

	public void setAddrStreet(String addrStreet) {
		this.addrStreet = addrStreet;
	}

	public String getAddrCity() {
		return addrCity;
	}

	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}

	public String getAddrState() {
		return addrState;
	}

	public void setAddrState(String addrState) {
		this.addrState = addrState;
	}

	public String getAddrZip() {
		return addrZip;
	}

	public void setAddrZip(String addrZip) {
		this.addrZip = addrZip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "PayeeDTO [fname=" + fname + ", lname=" + lname + ", addrStreet=" + addrStreet + ", addrCity="
			+ addrCity + ", addrState=" + addrState + ", addrZip=" + addrZip + ", email=" + email + "]";
	}
}