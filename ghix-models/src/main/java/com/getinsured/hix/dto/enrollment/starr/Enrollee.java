/**
 * 
 */
package com.getinsured.hix.dto.enrollment.starr;

import java.io.Serializable;
import java.sql.Date;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Srikanth Nakka
 *
 */
public class Enrollee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Valid
	private EnrolleePersonalInfo personalInfo;
	
	private String externalEnrolleeId;
	
	@NotEmpty(message = "personType cannot be empty.")
	private String personType;
	
	@NotEmpty(message = "relationship_hcp cannot be empty.")
	private String relationship_hcp;
	
	private String effectiveEndDate;
	private String status;
	private String disenrollTimestamp;
	
	@Valid
	private Location location;
	
	
	public EnrolleePersonalInfo getPersonalInfo() {
		return personalInfo;
	}
	public void setPersonalInfo(EnrolleePersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}
	
	public String getExternalEnrolleeId() {
		return externalEnrolleeId;
	}
	public void setExternalEnrolleeId(String externalEnrolleeId) {
		this.externalEnrolleeId = externalEnrolleeId;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public String getRelationship_hcp() {
		return relationship_hcp;
	}
	public void setRelationship_hcp(String relationship_hcp) {
		this.relationship_hcp = relationship_hcp;
	}
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDisenrollTimestamp() {
		return disenrollTimestamp;
	}
	public void setDisenrollTimestamp(String disenrollTimestamp) {
		this.disenrollTimestamp = disenrollTimestamp;
	}
	
}
