package com.getinsured.hix.util.directenrollment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * All D2C endpoints are here.
 * 
 * values are coming from configuration.properties and are kept outside of project classpath
 * @author sachin
 *
 */
@Component
public final class D2CEndPoints {

	public static String GHIXWebURL;
	public static String D2C_SVC_URL;
	public static String D2C_UHC_URL;
	public static String D2C_HUMANA_URL;
	public static String D2C_AETNA_URL;
	public static String D2C_HCSC_URL;
	public static String GHIX_ENROLLMENT_URL;
	public static String GHIX_EAPP_URL;

	@Value("#{configProp['ghixEappServiceURL']}")
	public void setGHIXEappURL(String ghix_eapp_url) {
		GHIX_EAPP_URL = ghix_eapp_url;
	}


	@Value("#{configProp['ghixWebServiceUrl']}")
	public void setGHIXWebURL(String gHIXWebURL) {
		GHIXWebURL = gHIXWebURL;
	}

	@Value("#{configProp['ghixD2CServiceURL']}")
	public void setD2C_SVC_URL(String d2c_SVC_URL) {
		D2C_SVC_URL = d2c_SVC_URL;
	}

	@Value("#{configProp['ghixD2CUHCServiceURL']}")
	public void setD2C_UHC_URL(String d2c_UHC_URL) {
		D2C_UHC_URL = d2c_UHC_URL;
	}

	@Value("#{configProp['ghixD2CHumanaServiceURL']}")
	public void setD2C_HUMANA_URL(String d2c_HUMANA_URL) {
		D2C_HUMANA_URL = d2c_HUMANA_URL;
	}

	@Value("#{configProp['ghixD2CAetnaServiceURL']}")
	public void setD2C_AETNA_URL(String d2c_AETNA_URL) {
		D2C_AETNA_URL = d2c_AETNA_URL;
	}

	@Value("#{configProp['ghixD2CHCSCServiceURL']}")
	public void setD2C_HCSC_URL(String d2c_HCSC_URL) {
		D2C_HCSC_URL = d2c_HCSC_URL;
	}

	@Value("#{configProp['ghixEnrollmentServiceURL']}")
	public void setGHIX_ENROLLMENT_URL(String ghix_ENROLLMENT_URL) {
		GHIX_ENROLLMENT_URL = ghix_ENROLLMENT_URL;
	}

	public static class D2CServiceEndpoints{
		public static final String ENROLLMENT_RECORD = D2C_SVC_URL+ "enrollment";
		public static final String ENROLLMENTUI_RECORD = D2C_SVC_URL+ "enrollmentUI";
		public static final String ENROLLMENTPAYMENT_RECORD = D2C_SVC_URL+ "enrollmentPayment";
		public static final String READ_ENROLLMENT_BY_HOUSEHOLD_ID = D2C_SVC_URL + "enrollment/householdId";
		public static final String READ_COUNTY_BY_ZIP = D2C_SVC_URL + "countyName";
		public static final String READ_HUMANA_NETWORK_IDENT = D2C_SVC_URL + "humanaProductIdentifier";
		public static final String ENROLLMENT_STATUS = D2C_SVC_URL + "enrollmentStatus";
		public static final String ENROLLMENT_STATUS_BY_GUID = D2C_SVC_URL + "enrollmentStatus/guid";
		public static final String AUTHENTICATE = D2C_SVC_URL + "authenticate";
		public static final String DOCUMENT = D2C_SVC_URL + "document";
		public static final String ECM = D2C_SVC_URL + "ecm";
		public static final String ECM_BY_GI_APP_ID = D2C_SVC_URL + "ecm/giappid";

		public static final String GHIX_WEB_UPDATE_ENROLLMENT_STATUS = GHIXWebURL+ "directenrollment/updateEnrollmentStatus";
		public static final String HCSC_DENTAL_PDF_URL = D2C_HCSC_URL + "directenrollment/dentalpdf/";
	}

	public static class EnrollmentEndPoints {
		public static final String UPDATE_D2C_ENROLLMENT_STATUS = GHIX_ENROLLMENT_URL + "enrollment/private/updateD2CEnrollmentStatus";
	}
	
	public static class EAPPEndPoints{
		public static final String EAPP_GET_TENANT = GHIX_EAPP_URL + "tenant";
	}
}
