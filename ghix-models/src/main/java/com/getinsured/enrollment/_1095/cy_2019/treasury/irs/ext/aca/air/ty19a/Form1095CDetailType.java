//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.10.15 at 01:56:56 PM PDT 
//


package com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty19a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Form 1095C Detail Type&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for Form1095CDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Form1095CDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}URID"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}URIDOfCorrected1095C"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}PayerDLN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}PayeeDLN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}VoidInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}CorrectedInd" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}Employer1095CDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}EmployeeDetail" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}MECMinValueOfferedAllYearInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}MonthlyMECMinimumValueOfferedGrp"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}LowestPremShareEmplCostYrAmt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}LowestPremiumShareEmplMonthlyCostGrp"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}EmplESHEnrollmentAllYearInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}EmployeeESHMonthlyEnrollmentGrp"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}CoverageStmtCoveredIndividualCoverageInd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}ConveredIndvidualMonthlyDetailGrp"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty19a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Form1095CDetailType", propOrder = {
    "urid",
    "uridOfCorrected1095C",
    "payerDLN",
    "payeeDLN",
    "voidInd",
    "correctedInd",
    "employer1095CDetail",
    "employeeDetail",
    "mecMinValueOfferedAllYearInd",
    "monthlyMECMinimumValueOfferedGrp",
    "lowestPremShareEmplCostYrAmt",
    "lowestPremiumShareEmplMonthlyCostGrp",
    "emplESHEnrollmentAllYearInd",
    "employeeESHMonthlyEnrollmentGrp",
    "coverageStmtCoveredIndividualCoverageInd",
    "converedIndvidualMonthlyDetailGrp",
    "errorDetail"
})
public class Form1095CDetailType {

    @XmlElement(name = "URID", required = true)
    protected String urid;
    @XmlElement(name = "URIDOfCorrected1095C", required = true)
    protected String uridOfCorrected1095C;
    @XmlElement(name = "PayerDLN", required = true)
    protected String payerDLN;
    @XmlElement(name = "PayeeDLN", required = true)
    protected String payeeDLN;
    @XmlElement(name = "VoidInd")
    protected String voidInd;
    @XmlElement(name = "CorrectedInd")
    protected String correctedInd;
    @XmlElement(name = "Employer1095CDetail", required = true)
    protected Employer1095CDetailType employer1095CDetail;
    @XmlElement(name = "EmployeeDetail")
    protected EmployeeDetailType employeeDetail;
    @XmlElement(name = "MECMinValueOfferedAllYearInd", required = true)
    protected String mecMinValueOfferedAllYearInd;
    @XmlElement(name = "MonthlyMECMinimumValueOfferedGrp", required = true)
    protected Monthly1095CMECIndicatorGrpType monthlyMECMinimumValueOfferedGrp;
    @XmlElement(name = "LowestPremShareEmplCostYrAmt")
    protected long lowestPremShareEmplCostYrAmt;
    @XmlElement(name = "LowestPremiumShareEmplMonthlyCostGrp", required = true)
    protected TotalMonthlyAmountsGrpType lowestPremiumShareEmplMonthlyCostGrp;
    @XmlElement(name = "EmplESHEnrollmentAllYearInd", required = true)
    protected String emplESHEnrollmentAllYearInd;
    @XmlElement(name = "EmployeeESHMonthlyEnrollmentGrp", required = true)
    protected MonthlySafeHarborIndicatorGrpType employeeESHMonthlyEnrollmentGrp;
    @XmlElement(name = "CoverageStmtCoveredIndividualCoverageInd", required = true)
    protected String coverageStmtCoveredIndividualCoverageInd;
    @XmlElement(name = "ConveredIndvidualMonthlyDetailGrp", required = true)
    protected ConveredIndvidualMonthlyDetailGrpType converedIndvidualMonthlyDetailGrp;
    @XmlElement(name = "ErrorDetail")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the urid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURID() {
        return urid;
    }

    /**
     * Sets the value of the urid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURID(String value) {
        this.urid = value;
    }

    /**
     * Gets the value of the uridOfCorrected1095C property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURIDOfCorrected1095C() {
        return uridOfCorrected1095C;
    }

    /**
     * Sets the value of the uridOfCorrected1095C property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURIDOfCorrected1095C(String value) {
        this.uridOfCorrected1095C = value;
    }

    /**
     * Gets the value of the payerDLN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerDLN() {
        return payerDLN;
    }

    /**
     * Sets the value of the payerDLN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerDLN(String value) {
        this.payerDLN = value;
    }

    /**
     * Gets the value of the payeeDLN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayeeDLN() {
        return payeeDLN;
    }

    /**
     * Sets the value of the payeeDLN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayeeDLN(String value) {
        this.payeeDLN = value;
    }

    /**
     * Gets the value of the voidInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidInd() {
        return voidInd;
    }

    /**
     * Sets the value of the voidInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidInd(String value) {
        this.voidInd = value;
    }

    /**
     * Gets the value of the correctedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrectedInd() {
        return correctedInd;
    }

    /**
     * Sets the value of the correctedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrectedInd(String value) {
        this.correctedInd = value;
    }

    /**
     * Gets the value of the employer1095CDetail property.
     * 
     * @return
     *     possible object is
     *     {@link Employer1095CDetailType }
     *     
     */
    public Employer1095CDetailType getEmployer1095CDetail() {
        return employer1095CDetail;
    }

    /**
     * Sets the value of the employer1095CDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Employer1095CDetailType }
     *     
     */
    public void setEmployer1095CDetail(Employer1095CDetailType value) {
        this.employer1095CDetail = value;
    }

    /**
     * Gets the value of the employeeDetail property.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeDetailType }
     *     
     */
    public EmployeeDetailType getEmployeeDetail() {
        return employeeDetail;
    }

    /**
     * Sets the value of the employeeDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeDetailType }
     *     
     */
    public void setEmployeeDetail(EmployeeDetailType value) {
        this.employeeDetail = value;
    }

    /**
     * Gets the value of the mecMinValueOfferedAllYearInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMECMinValueOfferedAllYearInd() {
        return mecMinValueOfferedAllYearInd;
    }

    /**
     * Sets the value of the mecMinValueOfferedAllYearInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMECMinValueOfferedAllYearInd(String value) {
        this.mecMinValueOfferedAllYearInd = value;
    }

    /**
     * Gets the value of the monthlyMECMinimumValueOfferedGrp property.
     * 
     * @return
     *     possible object is
     *     {@link Monthly1095CMECIndicatorGrpType }
     *     
     */
    public Monthly1095CMECIndicatorGrpType getMonthlyMECMinimumValueOfferedGrp() {
        return monthlyMECMinimumValueOfferedGrp;
    }

    /**
     * Sets the value of the monthlyMECMinimumValueOfferedGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Monthly1095CMECIndicatorGrpType }
     *     
     */
    public void setMonthlyMECMinimumValueOfferedGrp(Monthly1095CMECIndicatorGrpType value) {
        this.monthlyMECMinimumValueOfferedGrp = value;
    }

    /**
     * Gets the value of the lowestPremShareEmplCostYrAmt property.
     * 
     */
    public long getLowestPremShareEmplCostYrAmt() {
        return lowestPremShareEmplCostYrAmt;
    }

    /**
     * Sets the value of the lowestPremShareEmplCostYrAmt property.
     * 
     */
    public void setLowestPremShareEmplCostYrAmt(long value) {
        this.lowestPremShareEmplCostYrAmt = value;
    }

    /**
     * Gets the value of the lowestPremiumShareEmplMonthlyCostGrp property.
     * 
     * @return
     *     possible object is
     *     {@link TotalMonthlyAmountsGrpType }
     *     
     */
    public TotalMonthlyAmountsGrpType getLowestPremiumShareEmplMonthlyCostGrp() {
        return lowestPremiumShareEmplMonthlyCostGrp;
    }

    /**
     * Sets the value of the lowestPremiumShareEmplMonthlyCostGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalMonthlyAmountsGrpType }
     *     
     */
    public void setLowestPremiumShareEmplMonthlyCostGrp(TotalMonthlyAmountsGrpType value) {
        this.lowestPremiumShareEmplMonthlyCostGrp = value;
    }

    /**
     * Gets the value of the emplESHEnrollmentAllYearInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmplESHEnrollmentAllYearInd() {
        return emplESHEnrollmentAllYearInd;
    }

    /**
     * Sets the value of the emplESHEnrollmentAllYearInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmplESHEnrollmentAllYearInd(String value) {
        this.emplESHEnrollmentAllYearInd = value;
    }

    /**
     * Gets the value of the employeeESHMonthlyEnrollmentGrp property.
     * 
     * @return
     *     possible object is
     *     {@link MonthlySafeHarborIndicatorGrpType }
     *     
     */
    public MonthlySafeHarborIndicatorGrpType getEmployeeESHMonthlyEnrollmentGrp() {
        return employeeESHMonthlyEnrollmentGrp;
    }

    /**
     * Sets the value of the employeeESHMonthlyEnrollmentGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthlySafeHarborIndicatorGrpType }
     *     
     */
    public void setEmployeeESHMonthlyEnrollmentGrp(MonthlySafeHarborIndicatorGrpType value) {
        this.employeeESHMonthlyEnrollmentGrp = value;
    }

    /**
     * Gets the value of the coverageStmtCoveredIndividualCoverageInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverageStmtCoveredIndividualCoverageInd() {
        return coverageStmtCoveredIndividualCoverageInd;
    }

    /**
     * Sets the value of the coverageStmtCoveredIndividualCoverageInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverageStmtCoveredIndividualCoverageInd(String value) {
        this.coverageStmtCoveredIndividualCoverageInd = value;
    }

    /**
     * Gets the value of the converedIndvidualMonthlyDetailGrp property.
     * 
     * @return
     *     possible object is
     *     {@link ConveredIndvidualMonthlyDetailGrpType }
     *     
     */
    public ConveredIndvidualMonthlyDetailGrpType getConveredIndvidualMonthlyDetailGrp() {
        return converedIndvidualMonthlyDetailGrp;
    }

    /**
     * Sets the value of the converedIndvidualMonthlyDetailGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConveredIndvidualMonthlyDetailGrpType }
     *     
     */
    public void setConveredIndvidualMonthlyDetailGrp(ConveredIndvidualMonthlyDetailGrpType value) {
        this.converedIndvidualMonthlyDetailGrp = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
