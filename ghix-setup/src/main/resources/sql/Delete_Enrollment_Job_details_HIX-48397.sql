DELETE
FROM batch_job_execution_context
WHERE job_execution_id IN
  (SELECT job_execution_id
  FROM batch_job_execution
  WHERE job_instance_id IN
    (SELECT job_instance_id
    FROM batch_job_instance
    WHERE job_name IN('shopEnrollmentReconciliationJob', 'enrollmentXMLShopJob', 'enrollmentXMLGroupJob')
    )
  );
DELETE
FROM batch_step_execution_context
WHERE step_execution_id IN
  (SELECT step_execution_id
  FROM batch_step_execution
  WHERE job_execution_id IN
    (SELECT job_execution_id
    FROM batch_job_execution
    WHERE job_instance_id IN
      (SELECT job_instance_id
      FROM batch_job_instance
      WHERE job_name IN('shopEnrollmentReconciliationJob', 'enrollmentXMLShopJob', 'enrollmentXMLGroupJob')
      )
    )
  );
DELETE
FROM batch_step_execution
WHERE job_execution_id IN
  (SELECT job_execution_id
  FROM batch_job_execution
  WHERE job_instance_id IN
    (SELECT job_instance_id
    FROM batch_job_instance
    WHERE job_name IN('shopEnrollmentReconciliationJob', 'enrollmentXMLShopJob', 'enrollmentXMLGroupJob')
    )
  );
  
DELETE
FROM batch_job_execution
WHERE job_instance_id IN
  (SELECT job_instance_id
  FROM batch_job_instance
  WHERE job_name IN('shopEnrollmentReconciliationJob','enrollmentXMLShopJob','enrollmentXMLGroupJob')
  );
  
DELETE
FROM batch_job_params
WHERE job_instance_id IN
  (SELECT job_instance_id
  FROM batch_job_instance
  WHERE job_name IN('shopEnrollmentReconciliationJob','enrollmentXMLShopJob','enrollmentXMLGroupJob')
  );
  
DELETE
FROM batch_job_instance
WHERE job_name IN('shopEnrollmentReconciliationJob','enrollmentXMLShopJob','enrollmentXMLGroupJob');