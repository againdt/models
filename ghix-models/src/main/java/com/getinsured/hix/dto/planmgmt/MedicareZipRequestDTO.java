/**
 * 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

/**
 * @author lole_a
 *
 */
public class MedicareZipRequestDTO  implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String zip;

	public MedicareZipRequestDTO() {
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zipCode) {
		this.zip = zipCode;
	}

}
