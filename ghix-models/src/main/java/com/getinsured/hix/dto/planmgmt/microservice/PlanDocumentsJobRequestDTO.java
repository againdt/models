package com.getinsured.hix.dto.planmgmt.microservice;

public class PlanDocumentsJobRequestDTO extends SearchDTO {

	private String issuerPlanNumber;
	private String lastUpdatedDate;
	private String status;

	public PlanDocumentsJobRequestDTO() {
		super();
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
