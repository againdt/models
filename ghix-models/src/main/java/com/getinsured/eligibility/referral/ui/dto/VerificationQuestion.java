package com.getinsured.eligibility.referral.ui.dto;


/**
 * @author chopra_s
 *
 */
public class VerificationQuestion {
	private String code;
	private String text;
	private String answer;

	public VerificationQuestion() {

	}

	public VerificationQuestion(final String code, final String text) {
		this.code = code;
		this.text = text;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

/*	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/
}
