----------------------------------------------------------------------
--This procedure is used to synchronise PM_SERVICE_AREA quarter table  
--Takes table name as parameter
--returns msg
---------------------------------------------------------------------
create or replace
PROCEDURE synchronizePMServiceArea(
    tableName IN VARCHAR2,
    msg OUT VARCHAR2 )
IS
	issuerSrvcAreaid  PM_ISSUER_SERVICE_AREA.id%type;
	issuerState PM_ISSUER_SERVICE_AREA.issuer_state%type;
	completeState PM_ISSUER_SERVICE_AREA.complete_state%type ;
	zipcode ZIPCODES.zipcode%type;
	county ZIPCODES.county%type;
  	countyName ZIPCODES.county%type;
	countyFips ZIPCODES.county_Fips%type;
	stateFips ZIPCODES.state_Fips%type;
	tempId INTEGER;
	tempExtID INTEGER;
	revNo INTEGER;
	revtime INTEGER;
	extId PM_ISSUER_SERVICE_AREA_EXT.id%type;
	extIdNew PM_ISSUER_SERVICE_AREA_EXT.id%type;
	extCounty PM_ISSUER_SERVICE_AREA_EXT.county%type;
	partialCounty  PM_ISSUER_SERVICE_AREA_EXT.partial_county%type;
	ziplist PM_ISSUER_SERVICE_AREA_EXT.ziplist%type;
	POS     NUMBER := 0;
	stmt_str_insert VARCHAR2(400);
	isNodata VARCHAR2(15) := 'no data found';
	state ZIPCODES.state%type;
	idCount INTEGER ;

	--------------------------------------------------------------
	--cursor to fetch all the ids from PM_ISSUER_SERVICE_AREA table.
	---------------------------------------------------------------
	CURSOR ISSUERSERVICEAREADATA_CUR
	IS
	SELECT id, issuer_state, complete_state FROM PM_ISSUER_SERVICE_AREA;
	
	-------------------------------------------------------------------------------------------------------------
	-- cursor to fetch id , county , partial_county issuer_service_area_id from PM_ISSUER_SERVICE_AREA_EXT table.
	-------------------------------------------------------------------------------------------------------------
	CURSOR ISSUER_SERVICE_AREA_EXT_CUR (issuerServiceAreaID PM_ISSUER_SERVICE_AREA.id%type)
	IS
	 SELECT DISTINCT ext.id ,ext.county,
      ext.partial_county,
      ext.ISSUER_SERVICE_AREA_ID ,
      ext.ziplist
    FROM PM_ISSUER_SERVICE_AREA_EXT ext,
      PM_ISSUER_SERVICE_AREA isa
    WHERE ext.ISSUER_SERVICE_AREA_ID=isa.ID
    AND ext.IS_DELETED= 'N'
    AND isa.ID = issuerServiceAreaID;
	--------------------------------------------------------------------
	-- cursor to  fetch county county_fips state from ZIPCODES table.
	--------------------------------------------------------------------
	CURSOR ZIPCODES_CUR (ZIP ZIPCODES.ZIPCODE%TYPE, curCOUNTY PM_ISSUER_SERVICE_AREA_EXT.COUNTY%TYPE) IS
    SELECT COUNTY, COUNTY_FIPS, STATE_FIPS FROM ZIPCODES WHERE ZIPCODE=ZIP AND COUNTY=curCOUNTY;
	
	-----------------------------------------------------------------------------------------------------------
	--cursor to fetch zipcode, county, county_fips, state_Fips from ZIPCODES table for given state and county.
	-----------------------------------------------------------------------------------------------------------
	CURSOR ZIPCODES_FROM_COUNTY_STATES(extCounty PM_ISSUER_SERVICE_AREA_EXT.COUNTY%type,issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type) IS
	 SELECT DISTINCT zipcode,
     lower(county),
      county_fips,
      state_Fips,
      state
	  FROM ZIPCODES
	  WHERE LOWER(county) = LOWER(extCounty)
      AND state    = issuerState;
	-----------------------------------------------------------------------------------
	--cursor to fetch zipcode, county, county_fips, state_Fips for given state.
	-----------------------------------------------------------------------------------
	 CURSOR ZIPS_FROM_STATE_CUR(issuerState PM_ISSUER_SERVICE_AREA.ISSUER_STATE%type) IS
	 SELECT DISTINCT zipcode,
      lower(county),
      county_fips,
      state_Fips,
      state
	  FROM ZIPCODES
	  WHERE state  = issuerState;
	
	  
BEGIN
----------------------------------------------------------
--statement to insert data into respective quarter table.
----------------------------------------------------------
stmt_str_insert := 'INSERT INTO ' || tableName || '(            
ID,            
SERVICE_AREA_ID,            
ZIP,            
COUNTY,            
FIPS,            
STATE,            
SERVICE_AREA_EXT_ID,            
IS_DELETED          
) VALUES       
(:pKey, :id, :zipcode, :county, :countyFips, :issuerState, :serviceAreaExtId, :isDeleted)';

	DBMS_OUTPUT.PUT_LINE('Procedure synchronizePMServiceArea Initiated!!!');
	--------------------------------------------------------
	--fetch revNo from REVISION_INFO_SEQ for audit entry.
	--------------------------------------------------------
	SELECT REVISION_INFO_SEQ.nextval INTO revNo FROM dual ;
	-------------------------------------------------------------------
	--[HIX-48149]get rev time for audit entry and insert into REVISION_INFO table
	-------------------------------------------------------------------
	SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'yymmddhh24miss')) INTO revtime FROM dual;
	INSERT INTO REVISION_INFO (revision_id  , revision_timestamp) VALUES(revNo , revtime);
	
	OPEN  ISSUERSERVICEAREADATA_CUR;
	---------------------------------------
	--main loop over each service_area_id
	---------------------------------------
	LOOP
		FETCH ISSUERSERVICEAREADATA_CUR INTO issuerSrvcAreaid, issuerState ,completeState;
		IF ISSUERSERVICEAREADATA_CUR%FOUND THEN
			isNodata := 'data found';
		END IF;
		EXIT WHEN issuerServiceAreaData_cur%NOTFOUND;
		
		----------------------------------------------------------------------------------------------------------------------
		-- if complete state ='Y' fetch all records from zipcodes table for that state and insert in respective quarter table.
		----------------------------------------------------------------------------------------------------------------------
		IF completeState = 'YES' THEN
			OPEN ZIPS_FROM_STATE_CUR(issuerState);
				LOOP
					FETCH ZIPS_FROM_STATE_CUR INTO zipcode ,countyName ,countyFips,stateFips,state;
					EXIT WHEN ZIPS_FROM_STATE_CUR%NOTFOUND;
						
					-------------------------------------------------------------------------------
					--check if issuer_service_area_id exist into PM_ISSUER_SERVICE_AREA_EXT table.
					--------------------------------------------------------------------------------
					SELECT COUNT(ID) INTO idCount FROM PM_ISSUER_SERVICE_AREA_EXT  WHERE 
						ISSUER_SERVICE_AREA_ID = issuerSrvcAreaid  
						AND LOWER(COUNTY) = LOWER(countyName) AND IS_DELETED ='N' ;
						
					IF idCount = 0 THEN
					
						----------------------------------------------------------------------------------------------------------------------------------------------------
						--if issuerSrvcAreaid entry is not present in PM_ISSUER_SERVICE_AREA_EXT get nextval from PM_ISSUER_SERVICE_AREA_EXT_SEQ and insert into this table .
						-----------------------------------------------------------------------------------------------------------------------------------------------------
						SELECT PM_ISSUER_SERVICE_AREA_EXT_SEQ.nextval INTO extIdNew FROM dual;	
						
						INSERT INTO PM_ISSUER_SERVICE_AREA_EXT (id ,issuer_service_area_id ,county ,partial_county
          						,partial_county_justification,is_deleted,ziplist) VALUES (extIdNew , issuerSrvcAreaid ,
								countyName , 'NO' ,NULL, 'N' ,NULL);
						
						-----------------------------------------------------------	
						--[HIX-48149]inserting audit entry in PM_ISSUER_SERVICE_AREA_EXT_AUD.
						-----------------------------------------------------------
						INSERT INTO PM_ISSUER_SERVICE_AREA_EXT_AUD (id , rev , revtype ,issuer_service_area_id ,county ,partial_county
          				,partial_county_justification,is_deleted,ziplist) VALUES (extIdNew ,revNo ,0, issuerSrvcAreaid ,
						countyName , 'NO' ,NULL, 'N' ,NULL);
						
					ELSE
						--------------------------------------------------------------
						--else get existing id from PM_ISSUER_SERVICE_AREA_EXT table.
						--------------------------------------------------------------
						SELECT ID into extIdNew  FROM PM_ISSUER_SERVICE_AREA_EXT  WHERE 
						ISSUER_SERVICE_AREA_ID = issuerSrvcAreaid  
						AND lower(COUNTY) = lower(countyName) AND IS_DELETED ='N' ;
             
            
					END IF ;
					-----------------------------------------------------
					--inserting data into PM_SERVICE_AREA Quarter table.
					-----------------------------------------------------
					SELECT PM_SERVICE_AREA_SEQ.nextval INTO tempId FROM dual;
					EXECUTE IMMEDIATE stmt_str_insert USING tempId,
					issuerSrvcAreaid,zipcode,countyName,
					stateFips||countyFips,state,extIdNew,'N';
					-------------------------------------------------------
					--[HIX-48149]inserting audit data into PM_SERVICE_AREA_AUD table.
					--------------------------------------------------------
					INSERT INTO PM_SERVICE_AREA_AUD (id,rev ,revtype, service_area_id, zip, county,fips,state,service_area_ext_id,is_deleted ) VALUES  
					(tempId ,revNo, 0 , issuerSrvcAreaid ,zipcode ,countyName,stateFips||countyFips,state,extIdNew,'N');
				
				END LOOP;
				
			CLOSE 	ZIPS_FROM_STATE_CUR;
			
		ELSE
			OPEN ISSUER_SERVICE_AREA_EXT_CUR(issuerSrvcAreaid);
			
				LOOP
				
			 		FETCH ISSUER_SERVICE_AREA_EXT_CUR INTO  extId ,extCounty,partialCounty ,issuerSrvcAreaid , ziplist ;
					EXIT WHEN ISSUER_SERVICE_AREA_EXT_CUR%NOTFOUND;
					---------------------------------------------------------------------------------------------------------------------------------------
					-- if partialCounty ='YES' fetch ziplist from PM_ISSUER_SERVICE_AREA_EXT table split with comma and insert in respective quarter table.
					---------------------------------------------------------------------------------------------------------------------------------------
						IF partialCounty = 'YES' THEN
							pos        := 1;
							WHILE (pos != 0) 
							LOOP
								pos     := INSTR(ZIPLIST,',',1,1);
								zipcode := SUBSTR(ZIPLIST,1,5);
								ziplist := SUBSTR(ZIPLIST,POS+1,LENGTH(ZIPLIST));  
								OPEN ZIPCODES_CUR(ZIPCODE, extCounty);
								FETCH ZIPCODES_CUR INTO county ,countyFips ,stateFips; 
								---------------------------------------------------------
								--IF zipcode is less then length 5 , return and rollback .
								----------------------------------------------------------
								If Length(Zipcode)<5 Then
									DBMS_OUTPUT.PUT_LINE ('Error reading zipcodes from PM_ISSUER_SERVICE_AREA_EXT table'); 
									rollback;
									RETURN;
								END IF;
								
								SELECT PM_SERVICE_AREA_SEQ.nextval INTO tempId FROM dual;
								-----------------------------------------------------------
								--inserting data into PM_SERVICE_AREA Quarter table.
								-----------------------------------------------------------
								EXECUTE IMMEDIATE stmt_str_insert USING tempId,
								issuerSrvcAreaid,zipcode,county,stateFips||countyFips,state,extId,'N';
								------------------------------------------------------------
								--[HIX-48149]inserting audit data into PM_SERVICE_AREA_AUD table.
								------------------------------------------------------------
								INSERT INTO PM_SERVICE_AREA_AUD (id,rev ,revtype, service_area_id, zip, county,fips,state,service_area_ext_id,is_deleted ) VALUES  
								(tempId ,revNo, 0 , issuerSrvcAreaid ,zipcode ,county,stateFips||countyFips,state,extId,'N');
								
								CLOSE ZIPCODES_CUR ;
							END LOOP;
						ELSE
						-----------------------------------------------------------------------------------------------------------------------------------------
						-- if partialCounty ='NO' fetch all records from zipcode table for that particular county and issuerState and insert in respective table.
						------------------------------------------------------------------------------------------------------------------------------------------
							IF partialCounty ='NO' THEN
							OPEN ZIPCODES_FROM_COUNTY_STATES(extCounty , issuerState );
								LOOP
									FETCH ZIPCODES_FROM_COUNTY_STATES INTO zipcode,county,countyFips,stateFips,state;
									EXIT WHEN ZIPCODES_FROM_COUNTY_STATES%NOTFOUND;
									SELECT PM_SERVICE_AREA_SEQ.nextval INTO tempId FROM dual;
									--insert data into PM_SERVICE_AREA Quarter table
									EXECUTE IMMEDIATE stmt_str_insert USING tempId,
									  issuerSrvcAreaid,
									  zipcode,
									  county,
									  stateFips||countyFips,
									  state,
									  extId,
									  'N';
									 -------------------------------------------------------
									 --inserting audit data into PM_SERVICE_AREA_AUD table.
									 -------------------------------------------------------
									INSERT INTO PM_SERVICE_AREA_AUD (id,rev ,revtype, service_area_id, zip, county,fips,state,service_area_ext_id,is_deleted ) VALUES  
									(tempId ,revNo, 0 , issuerSrvcAreaid ,zipcode ,county,stateFips||countyFips,state,extId,'N');
									  
									 
								END LOOP;
							CLOSE ZIPCODES_FROM_COUNTY_STATES;
							
							END IF;
						END IF;
				END LOOP;
			CLOSE ISSUER_SERVICE_AREA_EXT_CUR ;
			 
		END IF;
	----------------------	
	--closing main loop	.
	----------------------
	END LOOP;
	CLOSE ISSUERSERVICEAREADATA_CUR ;
	IF isNodata = 'no data found' THEN
		msg      := 'no data found in PM_ISSUER_SERVICE_AREA';
		Rollback;
    ELSE
		msg := 'Procedure Successfully Executed';
		commit;
    END IF;
	
	DBMS_OUTPUT.PUT_LINE('Procedure synchronizePMServiceArea Ended!!!');
	DBMS_OUTPUT.PUT_LINE(msg);
  
	EXCEPTION
	WHEN PROGRAM_ERROR THEN
	DBMS_OUTPUT.PUT_LINE ('Internal Program Error Occured.'); 
	Rollback;
	WHEN NO_DATA_FOUND THEN 
	DBMS_OUTPUT.PUT_LINE ('Select...InTo did not return any row.'); 
	Rollback;
	WHEN OTHERS THEN
	DBMS_OUTPUT.PUT_LINE('Unknown Exception Occured!');
	Rollback;
	
	IF ISSUERSERVICEAREADATA_CUR%ISOPEN THEN
		CLOSE ISSUERSERVICEAREADATA_CUR;
	  END IF;
	 
	  IF ISSUER_SERVICE_AREA_EXT_CUR%ISOPEN THEN
		CLOSE ISSUER_SERVICE_AREA_EXT_CUR;
	  END IF;
	 
	  IF ZIPCODES_CUR%ISOPEN THEN
		CLOSE ZIPCODES_CUR;
	  END IF ;
    
     IF ZIPCODES_FROM_COUNTY_STATES%ISOPEN THEN
		CLOSE ZIPCODES_FROM_COUNTY_STATES;
	  END IF ;
	  IF ZIPS_FROM_STATE_CUR%ISOPEN THEN
		CLOSE ZIPS_FROM_STATE_CUR;
	  END IF ;
	msg := DBMS_UTILITY.FORMAT_ERROR_STACK;
    DBMS_OUTPUT.PUT_LINE( DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
	DBMS_OUTPUT.PUT_LINE(msg);
End;
