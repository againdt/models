package com.getinsured.hix.dto.consumer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MemberExtensionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean isMedicareElgible;
	private String actualRelationshipToPrimary;
	private MedicareDto medicare;
	private Map<String,String> hra =new HashMap<String,String>();
	
	private String title;
	private String suffix;
	private String alternatePhone;
	private String externalMemberId;
	private Double individualHraAmount;
	private String dateOfDeath;
	private String drxSessionId;
	
	public String getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public MemberExtensionDto(){
		hra.put("individualHraAmount", "");
	}

	public boolean isMedicareElgible() {
		return isMedicareElgible;
	}

	public void setMedicareElgible(boolean isMedicareElgible) {
		this.isMedicareElgible = isMedicareElgible;
	}

	public String getActualRelationshipToPrimary() {
		return actualRelationshipToPrimary;
	}

	public void setActualRelationshipToPrimary(String actualRelationshipToPrimary) {
		this.actualRelationshipToPrimary = actualRelationshipToPrimary;
	}

	public MedicareDto getMedicare() {
		return medicare;
	}

	public void setMedicare(MedicareDto medicare) {
		this.medicare = medicare;
	}

	public Map<String, String> getHra() {
		return hra;
	}

	public void setHra(Map<String, String> hra) {
		this.hra = hra;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getAlternatePhone() {
		return alternatePhone;
	}

	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public Double getIndividualHraAmount() {
		return individualHraAmount;
	}

	public void setIndividualHraAmount(Double individualHraAmount) {
		this.individualHraAmount = individualHraAmount;
	}

	public String getDrxSessionId() {
		return drxSessionId;
	}

	public void setDrxSessionId(String drxSessionId) {
		this.drxSessionId = drxSessionId;
	}
}
