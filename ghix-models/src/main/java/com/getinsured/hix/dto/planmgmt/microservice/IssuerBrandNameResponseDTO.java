package com.getinsured.hix.dto.planmgmt.microservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class IssuerBrandNameResponseDTO extends GhixResponseDTO {

	private List<IssuerBrandNameDTO> issuerBrandNameList;

	public IssuerBrandNameResponseDTO() {
		super();
	}

	public List<IssuerBrandNameDTO> getIssuerBrandNameList() {
		return issuerBrandNameList;
	}

	public void setIssuerBrandNameDTO(IssuerBrandNameDTO issuerBrandNameDTO) {

		if (CollectionUtils.isEmpty(this.issuerBrandNameList)) {
			this.issuerBrandNameList = new ArrayList<IssuerBrandNameDTO>();
		}
		this.issuerBrandNameList.add(issuerBrandNameDTO);
	}
}
