//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.23 at 11:54:17 AM IST 
//


package com.getinsured.enrollment.cms.dsh.sbmr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SBMRHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SBMRHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FileControlNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value=" 200000000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FileCreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TenantId" type="{http://sbmr.dsh.cms.gov}TenantIdType"/>
 *         &lt;element name="CoverageYear">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="2016"/>
 *               &lt;maxInclusive value="2999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IssuerId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IssuerFileSetId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="7"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TotalIssuerFiles" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SBMRHeaderType", propOrder = {
    "fileControlNumber",
    "fileCreateDate",
    "tenantId",
    "coverageYear",
    "issuerId",
    "issuerFileSetId",
    "totalIssuerFiles"
})
public class SBMRHeaderType {

    @XmlElement(name = "FileControlNumber")
    protected int fileControlNumber;
    @XmlElement(name = "FileCreateDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fileCreateDate;
    @XmlElement(name = "TenantId", required = true)
    protected String tenantId;
    @XmlElement(name = "CoverageYear")
    protected int coverageYear;
    @XmlElement(name = "IssuerId")
    protected String issuerId;
    @XmlElement(name = "IssuerFileSetId")
    protected String issuerFileSetId;
    @XmlElement(name = "TotalIssuerFiles")
    protected Integer totalIssuerFiles;

    /**
     * Gets the value of the fileControlNumber property.
     * 
     */
    public int getFileControlNumber() {
        return fileControlNumber;
    }

    /**
     * Sets the value of the fileControlNumber property.
     * 
     */
    public void setFileControlNumber(int value) {
        this.fileControlNumber = value;
    }

    /**
     * Gets the value of the fileCreateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFileCreateDate() {
        return fileCreateDate;
    }

    /**
     * Sets the value of the fileCreateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFileCreateDate(XMLGregorianCalendar value) {
        this.fileCreateDate = value;
    }

    /**
     * Gets the value of the tenantId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * Sets the value of the tenantId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTenantId(String value) {
        this.tenantId = value;
    }

    /**
     * Gets the value of the coverageYear property.
     * 
     */
    public int getCoverageYear() {
        return coverageYear;
    }

    /**
     * Sets the value of the coverageYear property.
     * 
     */
    public void setCoverageYear(int value) {
        this.coverageYear = value;
    }

    /**
     * Gets the value of the issuerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * Sets the value of the issuerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Gets the value of the issuerFileSetId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerFileSetId() {
        return issuerFileSetId;
    }

    /**
     * Sets the value of the issuerFileSetId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerFileSetId(String value) {
        this.issuerFileSetId = value;
    }

    /**
     * Gets the value of the totalIssuerFiles property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalIssuerFiles() {
        return totalIssuerFiles;
    }

    /**
     * Sets the value of the totalIssuerFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalIssuerFiles(Integer value) {
        this.totalIssuerFiles = value;
    }

}
