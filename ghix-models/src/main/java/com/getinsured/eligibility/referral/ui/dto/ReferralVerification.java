package com.getinsured.eligibility.referral.ui.dto;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.eligibility.model.ReferralActivation;

/**
 * @author chopra_s
 *
 */
public class ReferralVerification {

	private ReferralActivation referralActivation;

	private boolean securityCheck = true;

	private List<VerificationQuestion> verificationQuestions = new ArrayList<VerificationQuestion>();

	public boolean isSecurityCheck() {
		return securityCheck;
	}

	public void setSecurityCheck(boolean securityCheck) {
		this.securityCheck = securityCheck;
	}

	public List<VerificationQuestion> getVerificationQuestions() {
		return verificationQuestions;
	}

	public void setVerificationQuestions(
			List<VerificationQuestion> verificationQuestions) {
		this.verificationQuestions = verificationQuestions;
	}

	public String getAnswer(String code) {
		String answer = null;
		for (VerificationQuestion verificationQuestion : this.verificationQuestions) {
			if(verificationQuestion.getCode().equals(code))
			{
				answer = verificationQuestion.getAnswer();
				break;
			}
		}
		return answer;
	}

	public ReferralActivation getReferralActivation() {
		return referralActivation;
	}

	public void setReferralActivation(ReferralActivation referralActivation) {
		this.referralActivation = referralActivation;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReferralVerification [referralActivation=")
				.append(referralActivation).append("]");
		return builder.toString();
	}



	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}*/
}