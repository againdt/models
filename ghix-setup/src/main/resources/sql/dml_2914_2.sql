Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ENTITY_ADMIN','/admin/entity/manage/list','ENTITY_ADMIN',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED,POST_REG_URL) values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ENROLLMENT_ENTITY','/entity/enrollmententity/viewentityinformation','ENROLLMENT_ENTITY',SYSTIMESTAMP,'/enrollmententity/registration');

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_entity'));

Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_assister'));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ENTITY_ADMIN'), (select id from permissions   where name=UPPER('manage_entity')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ENTITY_ADMIN'), (select id from permissions   where name=UPPER('manage_assister')));

INSERT INTO ROLE_PERMISSIONS ( ID, CREATED, Updated, role_id,permission_id )   VALUES ( role_permissions_SEQ.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,(select id from roles where name='ENTITY_ADMIN'), (select id from permissions   where name=UPPER('Update_Certification_Status')));

