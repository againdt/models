package com.getinsured.hix.dto.eapp;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.getinsured.hix.model.IssuerBrandName;

@Entity
@Table(name = "D2C_CONFIG")
public class D2CConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "D2C_CONFIG_SEQ")
	@SequenceGenerator(name = "D2C_CONFIG_SEQ", sequenceName = "D2C_CONFIG_SEQ", allocationSize = 1)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ISSUER_BRAND_NAME_ID")
	private IssuerBrandName issuerBrandName;

	@Column(name = "YEAR")
	private String year;

	@Column(name = "PRODUCT_TYPE")
	private String productType;

	@Column(name = "STATE")
	private String state;

	@Column(name = "DOC_TYPE")
	private String docType;

	@Column(name = "ECM_DOC_ID")
	private String ecmId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public IssuerBrandName getIssuerBrandName() {
		return issuerBrandName;
	}

	public void setIssuerBrandName(IssuerBrandName issuerBrandName) {
		this.issuerBrandName = issuerBrandName;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getEcmId() {
		return ecmId;
	}

	public void setEcmId(String ecmId) {
		this.ecmId = ecmId;
	}

}
