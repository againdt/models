@ECHO OFF

call java -jar ..\lib\ghix-liquibase.jar --defaultsFile=..\..\conf\liquibase.properties --classpath=..\ghix-setup.war --changeLogFile=META-INF/master.changelog.xml status

pause