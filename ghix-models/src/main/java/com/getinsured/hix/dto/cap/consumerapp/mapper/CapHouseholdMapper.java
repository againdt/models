package com.getinsured.hix.dto.cap.consumerapp.mapper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.dto.cap.consumerapp.HouseholdDTO;
import com.getinsured.hix.dto.cap.consumerapp.LocationDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.service.UserService;

public class CapHouseholdMapper {

	@Autowired private UserService userService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CapHouseholdMapper.class);
	
	public HouseholdDTO getDtofromHousehold(Household household) {
		HouseholdDTO householdDTO = null;
		
		if(household != null) {
			
			LocationDto locationDto =  getLocationDto(household.getLocation());
			
			String serviceByUserName = getServiceByName(household.getServicedBy()); 
			
			householdDTO = new HouseholdDTO();
			householdDTO.setId(household.getId());
			householdDTO.setEmail(household.getEmail());
			householdDTO.setFirstName(household.getFirstName());
			householdDTO.setHomePhone(household.getPhoneNumber());
			householdDTO.setLastName(household.getLastName());
			householdDTO.setLeadStatus(household.getLeadStatus());
			householdDTO.setLocation(locationDto);
			householdDTO.setMobilePhone(household.getPhoneNumberOne());
			householdDTO.setPhoneNumber(household.getPhoneNumberTwo());
			if(null!=household.getPreferredContactTime()){
				householdDTO.setPreferredContactTime(household.getPreferredContactTime().toString());
			}
			householdDTO.setServiceBy(serviceByUserName);
		}
		return householdDTO;
	}

	/**
	 * Get the user full name to populate the service by data
	 * @param servicedBy
	 * @return
	 */
	private String getServiceByName(Integer servicedBy) {
		String userName = StringUtils.EMPTY;
		
		if(servicedBy != null && servicedBy > 0) {
			try {
				AccountUser user = userService.findById(servicedBy);
				userName = (userName != null) ? user.getFullName() : userName;
			} catch (Exception e) {
				LOGGER.error("Unable to get the account user");
			}
		}
		
		return userName;
	}

	/**
	 * Fill the location dto from location object
	 * @param location
	 * @return
	 */
	private LocationDto getLocationDto(Location location) {
		LocationDto locationDto = null;
		
		if(location != null) {
			locationDto = new LocationDto();
			locationDto.setAddress1(location.getAddress1());
			locationDto.setAddress2(location.getAddress2());
			locationDto.setCity(location.getCity());
			locationDto.setCountycode(location.getCountycode());
			locationDto.setId(location.getId());
			locationDto.setState(location.getState());
			locationDto.setZip(location.getZip());
		}
		
		return locationDto;
	}

}
