package com.getinsured.hix.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;


/**
 * The persistent class for the employer plans database table.
 * 
 */

@Audited
@Entity
@Table(name = "employer_payments")
public class EmployerPayments implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employerpay_seq")
	@SequenceGenerator(name = "employerpay_seq", sequenceName = "employerpay_seq", allocationSize = 1)
	private int id;

	@Column(name = "case_id")
	private String caseId;
	
	@Column(name = "statement_date")
	private Date statementDate;
	
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@Column(name = "period_covered")
	private String periodCovered;
	
	@Column(name = "payment_due_date")
	private Date paymentDueDate;
	
	@Column(name = "amount_due_from_last_invoice")
	private BigDecimal amountDueFromLastInvoice;
	
	@Column(name = "total_payment_received")
	private BigDecimal totalPaymentReceived;
	
	@Column(name = "premiums_this_period")
	private BigDecimal premiumThisPeriod;
	
	@Column(name = "adjustments")
	private BigDecimal adjustments;

	@Column(name = "exchange_fees")
	private BigDecimal exchangeFees;
	
	@Column(name = "total_amount_due")
	private BigDecimal totalAmountDue;
	
	@Column(name = "amount_enclosed")
	private BigDecimal amountEnclosed;
	
	@OneToOne
    @JoinColumn(name="payment_type_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private PaymentMethods paymentMethods;
	
	@OneToOne
    @JoinColumn(name="employer_payment_invoice_id")
	private EmployerPaymentInvoice employerPaymentInvoice;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "transaction_id")
	private String transactionId;
	
	@Column(name="is_refund")
	private String isRefund;
	
	@Column(name="is_partial_payment",columnDefinition="char(1)")
	private Character isPartialPayment;
	
	/*@Column(name = "response")
	private String response;*/
	
	@Column(name = "refund_amt")
	private BigDecimal refundAmt;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	/*@Column(name = "cybersource_pymt_decision")
	private String cybersourcePymtDecision;*/
	
	@Column(name = "merchant_ref_code")
	private String merchantRefCode;
	
	/*@Column(name="reconciliation_id")
	private String reconciliationId;
	
	@Column(name="processor_transaction_id")
	private String processorTransactionID;*/
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPeriodCovered() {
		return periodCovered;
	}

	public void setPeriodCovered(String periodCovered) {
		this.periodCovered = periodCovered;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public BigDecimal getPremiumThisPeriod() {
		return premiumThisPeriod;
	}

	public void setPremiumThisPeriod(BigDecimal premiumThisPeriod) {
		this.premiumThisPeriod = premiumThisPeriod;
	}

	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public EmployerPaymentInvoice getEmployerPaymentInvoice() {
		return employerPaymentInvoice;
	}

	public void setEmployerPaymentInvoice(
			EmployerPaymentInvoice employerPaymentInvoice) {
		this.employerPaymentInvoice = employerPaymentInvoice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}

	public Character getIsPartialPayment() {
		return isPartialPayment;
	}

	public void setIsPartialPayment(Character isPartialPayment) {
		this.isPartialPayment = isPartialPayment;
	}

	/*public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}*/

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigDecimal getAmountDueFromLastInvoice() {
		return amountDueFromLastInvoice;
	}

	public void setAmountDueFromLastInvoice(BigDecimal amountDueFromLastInvoice) {
		this.amountDueFromLastInvoice = amountDueFromLastInvoice;
	}

	public BigDecimal getTotalPaymentReceived() {
		return totalPaymentReceived;
	}

	public void setTotalPaymentReceived(BigDecimal totalPaymentReceived) {
		this.totalPaymentReceived = totalPaymentReceived;
	}

	public BigDecimal getExchangeFees() {
		return exchangeFees;
	}

	public void setExchangeFees(BigDecimal exchangeFees) {
		this.exchangeFees = exchangeFees;
	}

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getAmountEnclosed() {
		return amountEnclosed;
	}

	public void setAmountEnclosed(BigDecimal amountEnclosed) {
		this.amountEnclosed = amountEnclosed;
	}

	public BigDecimal getRefundAmt() {
		return refundAmt;
	}

	public void setRefundAmt(BigDecimal refundAmt) {
		this.refundAmt = refundAmt;
	}
	
	public BigDecimal getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(BigDecimal adjustments) {
		this.adjustments = adjustments;
	}
	
	/**
	 * @return the merchantRefCode
	 */
	public String getMerchantRefCode() {
		return merchantRefCode;
	}

	/**
	 * @param merchantRefCode the merchantRefCode to set
	 */
	public void setMerchantRefCode(String merchantRefCode) {
		this.merchantRefCode = merchantRefCode;
	}

	/**
	 * @return the reconciliationId
	 */
	/*public String getReconciliationId() {
		return reconciliationId;
	}*/

	/**
	 * @param reconciliationId the reconciliationId to set
	 */
	/*public void setReconciliationId(String reconciliationId) {
		this.reconciliationId = reconciliationId;
	}*/

	/**
	 * @return the processorTransactionID
	 */
	/*public String getProcessorTransactionID() {
		return processorTransactionID;
	}*/

	/**
	 * @param processorTransactionID the processorTransactionID to set
	 */
	/*public void setProcessorTransactionID(String processorTransactionID) {
		this.processorTransactionID = processorTransactionID;
	}*/
	
}
