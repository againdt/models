package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.timeshift.util.TSDate;

/**
 * -----------------------------------------------------------------------------
 * HIX-114158: Generate QHP Reports data to populates Enrollments data by Coverage Year
 * -----------------------------------------------------------------------------
 * 
 * This is POJO class for QHP_REPORT database table.
 * @since May 23, 2019
 */
@Audited
@Entity
@Table(name = "QHP_REPORT")
public class QhpReport implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QHP_REPORT_SEQ")
	@SequenceGenerator(name = "QHP_REPORT_SEQ", sequenceName = "QHP_REPORT_SEQ", allocationSize = 1)
	private int id;

	/**
	 * The External(AHBX) case ID that is tied to an AT and the enrollment.
	 */
	@Column(name = "EXTERNAL_CASE_ID")
	private String externalCaseId;

	/**
	 * The 16 alphanumeric plan ID
	 */
	@Column(name = "HIOS_PLAN_ID")
	private String hiosPlanId; 

	/**
	 * Plan Type (Possible values: HEALTH, DENTAL)
	 */
	@Column(name = "PLAN_TYPE")
	private String planType;

	/**
	 * The Enrollment Year before Renewal Year
	 */
	@Column(name = "YEAR_OF_ENROLLMENT")
	private Integer yearOfEnrollment;

	/**
	 * ID that uniquely identifies an enrollment in the system
	 */
	@Column(name = "ENROLLMENT_ID")
	private Integer enrollmentId;

	/**
	 * Enrollment Status (Possible values: CONFIRM/CANCEL/TERM/PENDING/ABORTED)
	 */
	@Column(name = "ENROLLMENT_STATUS")
	private String enrollmentStatus;

	/**
	 * Carrier plan name
	 */
	@Column(name = "PLAN_NAME")
	private String planName;

	/**
	 * Plan Level (Possible values: PLATINUM, GOLD, SILVER, BRONZE, EXPANDEDBRONZE, CATASTROPHIC)
	 */
	@Column(name = "PLAN_LEVEL")
	private String planLevel;

	/**
	 * Carrier name
	 */
	@Column(name = "ISSUER_NAME")
	private String issuerName;

	/**
	 * Broker/Assister ID
	 */
	@Column(name = "BROKER_ASSISTER_ID")
	private Integer brokerAssisterId;

	/**
	 * Delegation Type (Possible values: AGENT(Broker)/ASSISTER Role)
	 */
	@Column(name = "DELEGATION_TYPE")
	private String delegationType;

	/**
	 * External Member ID
	 */
	@Column(name = "MEMBER_ID")
	private String memberId;

	/**
	 * Benefit start date of member
	 */
	@Column(name = "MEMBER_BENEFIT_START_DATE")
	private Date memberBenefitStartDate;

	/**
	 * Benefit end date of member
	 */
	@Column(name = "MEMBER_BENEFIT_END_DATE")
	private Date memberBenefitEndDate;

	/**
	 * Status of the Member(Enrollee)
	 */
	@Column(name = "MEMBER_STATUS")
	private String memberStatus;

	/**
	 * Subscriber flag (Possible values: TRUE/FALSE)
	 */
	@Column(name = "SUBSCRIBER_FLAG")
	private Boolean subscriberFlag;

	/**
	 * Store ID from BATCH_JOB_EXECUTION table for QHP Report JOB
	 */
	@Column(name = "BATCH_JOB_EXECUTION_ID")
	private Integer batchJobExecutionId;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public QhpReport() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getExternalCaseId() {
		return externalCaseId;
	}

	public void setExternalCaseId(String externalCaseId) {
		this.externalCaseId = externalCaseId;
	}

	public String getHiosPlanId() {
		return hiosPlanId;
	}

	public void setHiosPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public Integer getYearOfEnrollment() {
		return yearOfEnrollment;
	}

	public void setYearOfEnrollment(Integer yearOfEnrollment) {
		this.yearOfEnrollment = yearOfEnrollment;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public Integer getBrokerAssisterId() {
		return brokerAssisterId;
	}

	public void setBrokerAssisterId(Integer brokerAssisterId) {
		this.brokerAssisterId = brokerAssisterId;
	}

	public String getDelegationType() {
		return delegationType;
	}

	public void setDelegationType(String delegationType) {
		this.delegationType = delegationType;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getMemberBenefitStartDate() {
		return memberBenefitStartDate;
	}

	public void setMemberBenefitStartDate(Date memberBenefitStartDate) {
		this.memberBenefitStartDate = memberBenefitStartDate;
	}

	public Date getMemberBenefitEndDate() {
		return memberBenefitEndDate;
	}

	public void setMemberBenefitEndDate(Date memberBenefitEndDate) {
		this.memberBenefitEndDate = memberBenefitEndDate;
	}

	public String getMemberStatus() {
		return memberStatus;
	}

	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}

	public Boolean getSubscriberFlag() {
		return subscriberFlag;
	}

	public void setSubscriberFlag(Boolean subscriberFlag) {
		this.subscriberFlag = subscriberFlag;
	}

	public Integer getBatchJobExecutionId() {
		return batchJobExecutionId;
	}

	public void setBatchJobExecutionId(Integer batchJobExecutionId) {
		this.batchJobExecutionId = batchJobExecutionId;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
