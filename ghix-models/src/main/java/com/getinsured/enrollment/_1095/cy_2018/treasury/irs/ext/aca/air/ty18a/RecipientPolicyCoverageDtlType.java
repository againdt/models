
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;Recipient Policy Coverage Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2014-08-21&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;DescriptionTxt&gt; Tax Household policy coverage information&lt;/DescriptionTxt&gt;
 * 					&lt;DataElementID/&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for RecipientPolicyCoverageDtlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecipientPolicyCoverageDtlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}MarketPlacePolicyNum"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PolicyIssuerNm"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PolicyStartDt"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}PolicyTerminationDt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecipientPolicyCoverageDtlType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "marketPlacePolicyNum",
    "policyIssuerNm",
    "policyStartDt",
    "policyTerminationDt"
})
public class RecipientPolicyCoverageDtlType {

    @XmlElement(name = "MarketPlacePolicyNum", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String marketPlacePolicyNum;
    @XmlElement(name = "PolicyIssuerNm", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String policyIssuerNm;
    @XmlElement(name = "PolicyStartDt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar policyStartDt;
    @XmlElement(name = "PolicyTerminationDt", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar policyTerminationDt;

    /**
     * Gets the value of the marketPlacePolicyNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketPlacePolicyNum() {
        return marketPlacePolicyNum;
    }

    /**
     * Sets the value of the marketPlacePolicyNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketPlacePolicyNum(String value) {
        this.marketPlacePolicyNum = value;
    }

    /**
     * Gets the value of the policyIssuerNm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyIssuerNm() {
        return policyIssuerNm;
    }

    /**
     * Sets the value of the policyIssuerNm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyIssuerNm(String value) {
        this.policyIssuerNm = value;
    }

    /**
     * Gets the value of the policyStartDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyStartDt() {
        return policyStartDt;
    }

    /**
     * Sets the value of the policyStartDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyStartDt(XMLGregorianCalendar value) {
        this.policyStartDt = value;
    }

    /**
     * Gets the value of the policyTerminationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyTerminationDt() {
        return policyTerminationDt;
    }

    /**
     * Sets the value of the policyTerminationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyTerminationDt(XMLGregorianCalendar value) {
        this.policyTerminationDt = value;
    }

}
