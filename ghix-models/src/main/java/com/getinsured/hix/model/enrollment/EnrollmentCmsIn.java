package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "ENRL_CMS_IN")
public class EnrollmentCmsIn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENRL_CMS_IN_SEQ")
	@SequenceGenerator(name = "ENRL_CMS_IN_SEQ", sequenceName = "ENRL_CMS_IN_SEQ", allocationSize = 1)
	private Integer id;

	@Column(name = "INBOUND_FILE_NAME")
	private String inFileName;

	@Column(name = "FILE_TYPE")
	private String fileType;

	@Column(name = "FILE_ID")
	private String fileId;

	@Column(name = "FILE_STATUS")
	private String fileStatus;

	@Column(name = "OUTBOUND_FILE_NAME")
	private String outFileName;

	@Column(name = "HIOS_ISSUER_ID")
	private String hiosIssuerId;

	@Column(name = "MISSING_POLICY_IDS")
	private String missingPolicyIds;

	@Column(name = "INBOUND_FILE_READING_ERROR")
	private String inboundFileError;

	@OneToMany(mappedBy = "enrollmentCmsIn", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentCmsInDtl> enrollmentCmsInDtlList;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date createdOn;
	
	@Column(name = "TOT_PREV_POLICIES_NOT_SUB")
	private Integer totPrevPoliciesNotSubmitted;

	@Column(name = "NOT_SUBMITTED_EFFECTUATED")
	private Integer notSubmittedEffectuated;

	@Column(name = "NOT_SUBMITTED_TERMINATED")
	private Integer notSubmittedTerminated;

	@Column(name = "NOT_SUBMITTED_CANCELLED")
	private Integer notSubmittedCancelled;

	@Column(name = "TOTAL_RECORDS_PROCESSED")
	private Integer totalRecordsProcessed;

	@Column(name = "TOTAL_RECORDS_REJECTED")
	private Integer totalRecordsRejected;

	@Column(name = "TOTAL_POLICYRECORDS_APPROVED")
	private Integer totalPolicyrecordsApproved;

	@Column(name = "MATCH_POLY_NO_CHANGE_REQD")
	private Integer matchingPoliciesNoChange_required;

	@Column(name = "MATCH_POLY_CHANGE_APPLY")
	private Integer matchingPoliciesChangeApplied;

	@Column(name = "MATCH_POLY_CRCTD_CHNG_APPLY")
	private Integer matchingPoliciesCorrectedChangeApplied;

	@Column(name = "NEW_POLICIES_CREATED_AS_SENT")
	private Integer newPoliciesCreatedAsSent;

	@Column(name = "NEW_POLY_CREAT_CRCTN_APPLY")
	private Integer newPoliciesCreatedWithCorrectionApplied;

	@Column(name = "COUNT_OF_EFF_POLY_CANCEL")
	private Integer countOfEffectuatedPoliciesCancelled;
	
	@Transient
	private String inboundAction;
	
	@OneToMany(mappedBy="enrlCmsIn", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EnrollmentCmsInMissingPolicy> enrollmentCmsInMissingPolicies;
	
	public List<EnrollmentCmsInMissingPolicy> getEnrollmentMissingPolicies() {
		return enrollmentCmsInMissingPolicies;
	}

	public void setEnrollmentMissingPolicies(List<EnrollmentCmsInMissingPolicy> enrollmentCmsInMissingPolicies) {
		this.enrollmentCmsInMissingPolicies = enrollmentCmsInMissingPolicies;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the inFileName
	 */
	public String getInFileName() {
		return inFileName;
	}

	/**
	 * @param inFileName
	 *            the inFileName to set
	 */
	public void setInFileName(String inFileName) {
		this.inFileName = inFileName;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType
	 *            the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId
	 *            the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * @return the fileStatus
	 */
	public String getFileStatus() {
		return fileStatus;
	}

	/**
	 * @param fileStatus
	 *            the fileStatus to set
	 */
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	/**
	 * @return the outFileName
	 */
	public String getOutFileName() {
		return outFileName;
	}

	/**
	 * @param outFileName
	 *            the outFileName to set
	 */
	public void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}

	/**
	 * @return the hiosIssuerId
	 */
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	/**
	 * @param hiosIssuerId
	 *            the hiosIssuerId to set
	 */
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	/**
	 * @return the missingPolicyIds
	 */
	public String getMissingPolicyIds() {
		return missingPolicyIds;
	}

	/**
	 * @param missingPolicyIds
	 *            the missingPolicyIds to set
	 */
	public void setMissingPolicyIds(String missingPolicyIds) {
		this.missingPolicyIds = missingPolicyIds;
	}

	/**
	 * @return the inboundFileError
	 */
	public String getInboundFileError() {
		return inboundFileError;
	}

	/**
	 * @param inboundFileError
	 *            the inboundFileError to set
	 */
	public void setInboundFileError(String inboundFileError) {
		this.inboundFileError = inboundFileError;
	}

	/**
	 * @return the enrollmentCmsInDtlList
	 */
	public List<EnrollmentCmsInDtl> getEnrollmentCmsInDtlList() {
		return enrollmentCmsInDtlList;
	}

	/**
	 * @param enrollmentCmsInDtlList
	 *            the enrollmentCmsInDtlList to set
	 */
	public void setEnrollmentCmsInDtlList(List<EnrollmentCmsInDtl> enrollmentCmsInDtlList) {
		this.enrollmentCmsInDtlList = enrollmentCmsInDtlList;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getTotPrevPoliciesNotSubmitted() {
		return totPrevPoliciesNotSubmitted;
	}

	public void setTotPrevPoliciesNotSubmitted(Integer totPrevPoliciesNotSubmitted) {
		this.totPrevPoliciesNotSubmitted = totPrevPoliciesNotSubmitted;
	}

	public Integer getNotSubmittedEffectuated() {
		return notSubmittedEffectuated;
	}

	public void setNotSubmittedEffectuated(Integer notSubmittedEffectuated) {
		this.notSubmittedEffectuated = notSubmittedEffectuated;
	}

	public Integer getNotSubmittedTerminated() {
		return notSubmittedTerminated;
	}

	public void setNotSubmittedTerminated(Integer notSubmittedTerminated) {
		this.notSubmittedTerminated = notSubmittedTerminated;
	}

	public Integer getNotSubmittedCancelled() {
		return notSubmittedCancelled;
	}

	public void setNotSubmittedCancelled(Integer notSubmittedCancelled) {
		this.notSubmittedCancelled = notSubmittedCancelled;
	}

	public Integer getTotalRecordsProcessed() {
		return totalRecordsProcessed;
	}

	public void setTotalRecordsProcessed(Integer totalRecordsProcessed) {
		this.totalRecordsProcessed = totalRecordsProcessed;
	}

	public Integer getTotalRecordsRejected() {
		return totalRecordsRejected;
	}

	public void setTotalRecordsRejected(Integer totalRecordsRejected) {
		this.totalRecordsRejected = totalRecordsRejected;
	}

	public Integer getTotalPolicyrecordsApproved() {
		return totalPolicyrecordsApproved;
	}

	public void setTotalPolicyrecordsApproved(Integer totalPolicyrecordsApproved) {
		this.totalPolicyrecordsApproved = totalPolicyrecordsApproved;
	}

	public Integer getMatchingPoliciesNoChange_required() {
		return matchingPoliciesNoChange_required;
	}

	public void setMatchingPoliciesNoChange_required(
			Integer matchingPoliciesNoChange_required) {
		this.matchingPoliciesNoChange_required = matchingPoliciesNoChange_required;
	}

	public Integer getMatchingPoliciesChangeApplied() {
		return matchingPoliciesChangeApplied;
	}

	public void setMatchingPoliciesChangeApplied(
			Integer matchingPoliciesChangeApplied) {
		this.matchingPoliciesChangeApplied = matchingPoliciesChangeApplied;
	}

	public Integer getMatchingPoliciesCorrectedChangeApplied() {
		return matchingPoliciesCorrectedChangeApplied;
	}

	public void setMatchingPoliciesCorrectedChangeApplied(
			Integer matchingPoliciesCorrectedChangeApplied) {
		this.matchingPoliciesCorrectedChangeApplied = matchingPoliciesCorrectedChangeApplied;
	}

	public Integer getNewPoliciesCreatedAsSent() {
		return newPoliciesCreatedAsSent;
	}

	public void setNewPoliciesCreatedAsSent(Integer newPoliciesCreatedAsSent) {
		this.newPoliciesCreatedAsSent = newPoliciesCreatedAsSent;
	}

	public Integer getNewPoliciesCreatedWithCorrectionApplied() {
		return newPoliciesCreatedWithCorrectionApplied;
	}

	public void setNewPoliciesCreatedWithCorrectionApplied(
			Integer newPoliciesCreatedWithCorrectionApplied) {
		this.newPoliciesCreatedWithCorrectionApplied = newPoliciesCreatedWithCorrectionApplied;
	}

	public Integer getCountOfEffectuatedPoliciesCancelled() {
		return countOfEffectuatedPoliciesCancelled;
	}

	public void setCountOfEffectuatedPoliciesCancelled(
			Integer countOfEffectuatedPoliciesCancelled) {
		this.countOfEffectuatedPoliciesCancelled = countOfEffectuatedPoliciesCancelled;
	}

	public String getInboundAction() {
		return inboundAction;
	}

	public void setInboundAction(String inboundAction) {
		this.inboundAction = inboundAction;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist() {
		this.setCreatedOn(new TSDate());
	}
}
