/*
 * @author santanu
 * @version 1.0
 * @since June 13, 2016 
 * 
 * This DTO to accept request object for Period Loop
 */
package com.getinsured.hix.dto.planmgmt;

import java.util.List;

public final class LifeSpanPeriodLoopDTO {
	
	private String periodId;
	private String periodEffectiveDate;
	private String subscriberZipCode;
	private String subscriberCountyCode;
	private List<LifeSpanMemberRequestDTO> requestMemberList;
	private List<LifeSpanMemberResponseDTO> responseMemberList;
	private double slspPremium;
	private String slspHiosPlanId;
	private int errorCode;
	private String errorMsg;
	private String status;
	
	
	/**
	 * @return the periodId
	 */
	public String getPeriodId() {
		return periodId;
	}
	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}
	/**
	 * @return the periodEffectiveDate
	 */
	public String getPeriodEffectiveDate() {
		return periodEffectiveDate;
	}
	/**
	 * @param periodEffectiveDate the periodEffectiveDate to set
	 */
	public void setPeriodEffectiveDate(String periodEffectiveDate) {
		this.periodEffectiveDate = periodEffectiveDate;
	}
	/**
	 * @return the subscriberZipCode
	 */
	public String getSubscriberZipCode() {
		return subscriberZipCode;
	}
	/**
	 * @param subscriberZipCode the subscriberZipCode to set
	 */
	public void setSubscriberZipCode(String subscriberZipCode) {
		this.subscriberZipCode = subscriberZipCode;
	}
	/**
	 * @return the subscriberCountyCode
	 */
	public String getSubscriberCountyCode() {
		return subscriberCountyCode;
	}
	/**
	 * @param subscriberCountyCode the subscriberCountyCode to set
	 */
	public void setSubscriberCountyCode(String subscriberCountyCode) {
		this.subscriberCountyCode = subscriberCountyCode;
	}
	
	/**
	 * @return the requestMemberList
	 */
	public List<LifeSpanMemberRequestDTO> getRequestMemberList() {
		return requestMemberList;
	}
	/**
	 * @param requestMemberList the requestMemberList to set
	 */
	public void setRequestMemberList(List<LifeSpanMemberRequestDTO> requestMemberList) {
		this.requestMemberList = requestMemberList;
	}
	/**
	 * @return the responseMemberList
	 */
	public List<LifeSpanMemberResponseDTO> getResponseMemberList() {
		return responseMemberList;
	}
	/**
	 * @param responseMemberList the responseMemberList to set
	 */
	public void setResponseMemberList(List<LifeSpanMemberResponseDTO> responseMemberList) {
		this.responseMemberList = responseMemberList;
	}
	/**
	 * @return the slspPremium
	 */
	public double getSlspPremium() {
		return slspPremium;
	}
	/**
	 * @param slspPremium the slspPremium to set
	 */
	public void setSlspPremium(double slspPremium) {
		this.slspPremium = slspPremium;
	}
	/**
	 * @return the slspHiosPlanId
	 */
	public String getSlspHiosPlanId() {
		return slspHiosPlanId;
	}
	/**
	 * @param slspHiosPlanId the slspHiosPlanId to set
	 */
	public void setSlspHiosPlanId(String slspHiosPlanId) {
		this.slspHiosPlanId = slspHiosPlanId;
	}
	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
