package com.getinsured.hix.dto.cap.consumerapp.mapper;

import com.getinsured.hix.dto.cap.consumerapp.ConsumerDocumentDTO;
import com.getinsured.hix.model.ConsumerDocument;

public class CapCmrDocumentMapper {

	public ConsumerDocumentDTO getDtoFromObject(ConsumerDocument document) {
		ConsumerDocumentDTO dto = null;
		
		if(document !=null) {
			dto = new ConsumerDocumentDTO();
			dto.setCreatedBy(document.getCreatedBy());
			dto.setCreatedDate(document.getCreatedDate());
			dto.setDocumentCategory(document.getDocumentCategory());
			dto.setDocumentName(document.getDocumentName());
			dto.setDocumentType(document.getDocumentType());
			dto.setEcmDocumentId(document.getEcmDocumentId());
			dto.setId(document.getId());
			dto.setTargetId(document.getTargetId());
			dto.setTargetName(document.getTargetName());
		}	
		return  dto;
	}
	
	public ConsumerDocument getObjectFromDto(ConsumerDocumentDTO dto) {
		ConsumerDocument document = null;
		
		if(dto != null) {
			document = new ConsumerDocument();
			document.setCreatedBy(dto.getCreatedBy());
			document.setCreatedDate(dto.getCreatedDate());
			document.setDocumentCategory(dto.getDocumentCategory());
			document.setDocumentName(dto.getDocumentName());
			document.setDocumentType(dto.getDocumentType());
			document.setEcmDocumentId(dto.getEcmDocumentId());
			document.setId(dto.getId());
			document.setTargetId(dto.getTargetId());
			document.setTargetName(dto.getTargetName());
		}
		
		return document;
	}
}
