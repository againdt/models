package com.getinsured.hix.dto.consumer.goodrx;

import java.io.Serializable;
import java.util.List;

public class PriceDetailsDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private List<String> url;
	private List<Double> price;
	private List<String> savings;
	private List<String> type;
	private List<String> pharmacy;
	private List<String> manufacturer;
	public List<String> getUrl() {
		return url;
	}
	public void setUrl(List<String> url) {
		this.url = url;
	}
	public List<Double> getPrice() {
		return price;
	}
	public void setPrice(List<Double> price) {
		this.price = price;
	}
	public List<String> getSavings() {
		return savings;
	}
	public void setSavings(List<String> savings) {
		this.savings = savings;
	}
	public List<String> getType() {
		return type;
	}
	public void setType(List<String> type) {
		this.type = type;
	}
	public List<String> getPharmacy() {
		return pharmacy;
	}
	public void setPharmacy(List<String> pharmacy) {
		this.pharmacy = pharmacy;
	}
	public List<String> getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(List<String> manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	
	
	
}
