/**
 * 
 * @author santanu
 * @version 1.0
 * @since Aug 29, 2014 
 * This DTO will handle all AME plan response object 
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import com.getinsured.hix.model.GHIXResponse;

@Component("AmePlanResponseDTO")
public class AmePlanResponseDTO extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private List<AmePlan> amePlanList;

	/**
	 * Constructor.
	 */
	public AmePlanResponseDTO() {
		this.startResponse();
	}
	
	public List<AmePlan> getAmePlanList() {
		return amePlanList;
	}

	public void setAmePlanList(List<AmePlan> amePlanList) {
		this.amePlanList = amePlanList;
	}	
	
}
