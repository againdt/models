package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@SuppressWarnings("serial")
@Audited
@Entity
@Table(name="PM_ISSUER_SERVICE_AREA")
public class IssuerServiceArea implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICE_AREA_SEQ")
	@SequenceGenerator(name = "SERVICE_AREA_SEQ", sequenceName = "SERVICE_AREA_SEQ", allocationSize = 1)
	private int id;

	@Column(name="SERFF_SERVICE_AREA_ID")
	private String serffServiceAreaId;

	@Column(name="SERVICE_AREA_NAME")
	private String serviceAreaName;

	@Column(name="COMPLETE_STATE", length=255)
	private String completeState;

	@Column(name="HIOS_ISSUER_ID", length=5)
	@NotNull
	private String hiosIssuerId;

	@Column(name="ISSUER_STATE", length=25)
	private String issuerState;

	@OneToMany(mappedBy = "serviceAreaId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<IssuerServiceAreaExt> issuerServiceAreaExt;

	@Column(name = "APPLICABLE_YEAR")
    private Integer applicableYear;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSerffServiceAreaId() {
		return serffServiceAreaId;
	}

	public void setSerffServiceAreaId(String serffServiceAreaId) {
		this.serffServiceAreaId = serffServiceAreaId;
	}

	public String getServiceAreaName() {
		return serviceAreaName;
	}

	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	public String getCompleteState() {
		return completeState;
	}

	public void setCompleteState(String completeState) {
		this.completeState = completeState;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIssuerState() {
		return issuerState;
	}

	public void setIssuerState(String issuerState) {
		this.issuerState = issuerState;
	}

	public List<IssuerServiceAreaExt> getIssuerServiceAreaExt() {
		return issuerServiceAreaExt;
	}

	public void setIssuerServiceAreaExt(
			List<IssuerServiceAreaExt> issuerServiceAreaExt) {
		this.issuerServiceAreaExt = issuerServiceAreaExt;
	}

	public Integer getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(Integer applicableYear) {
		this.applicableYear = applicableYear;
	}
}
