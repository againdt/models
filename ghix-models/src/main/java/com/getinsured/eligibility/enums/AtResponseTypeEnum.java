package com.getinsured.eligibility.enums;

/**
 * @author chopra_s
 * 
 */
public enum AtResponseTypeEnum {
	FULL_DETERMINATION, UPDATED, ASSESSMENT;
	/**
	 * NM - For Full Response - FULL_DETERMINATION, ASSESSMENT
	 * 		For Referral - FULL_DETERMINATION
	 * ID - FULL_DETERMINATION, UPDATED
	 **/
	public String value() {
		return name();
	}

	public static AtResponseTypeEnum fromValue(String v) {
		AtResponseTypeEnum data = null;
		for (AtResponseTypeEnum c : AtResponseTypeEnum.class
				.getEnumConstants()) {
			if (c.value().equals(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	public String toString() {
		return value();
	}
}
