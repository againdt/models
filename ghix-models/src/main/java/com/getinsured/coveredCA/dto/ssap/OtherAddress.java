
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;
/**
 * @author chopra_s
 * 
 */
public class OtherAddress {

    @Expose
    private Address address=new Address();
   
    @Expose
    private Boolean livingOutsideofStateTemporarilyIndicator=false;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    public Boolean getLivingOutsideofStateTemporarilyIndicator() {
        return livingOutsideofStateTemporarilyIndicator;
    }

    public void setLivingOutsideofStateTemporarilyIndicator(Boolean livingOutsideofStateTemporarilyIndicator) {
        this.livingOutsideofStateTemporarilyIndicator = livingOutsideofStateTemporarilyIndicator;
    }

}
