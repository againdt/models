package com.getinsured.hix.model.eligibility;

import com.getinsured.hix.model.GHIXRequest;

/**
 * This is the super class request for Eligibility Module. All requests must
 * extend this.
 * 
 * @author polimetla_b
 * @since 20-Feb-2013
 */
public class EligibilityRequest extends GHIXRequest {

	private long applicationID;

	public long getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(long applicationID) {
		this.applicationID = applicationID;
	}

}
