
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Aggregated Group Memeber Names and SSNs&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for AggregatedGroupMemberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AggregatedGroupMemberType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Names" type="{urn:us:gov:treasury:irs:common}CompletePersonNameType"/>
 *         &lt;element name="EIN" type="{urn:us:gov:treasury:irs:common}EINType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregatedGroupMemberType", namespace = "urn:us:gov:treasury:irs:common", propOrder = {
    "names",
    "ein"
})
public class AggregatedGroupMemberType {

    @XmlElement(name = "Names", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected CompletePersonNameType names;
    @XmlElement(name = "EIN", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String ein;

    /**
     * Gets the value of the names property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameType }
     *     
     */
    public CompletePersonNameType getNames() {
        return names;
    }

    /**
     * Sets the value of the names property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameType }
     *     
     */
    public void setNames(CompletePersonNameType value) {
        this.names = value;
    }

    /**
     * Gets the value of the ein property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEIN() {
        return ein;
    }

    /**
     * Sets the value of the ein property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEIN(String value) {
        this.ein = value;
    }

}
