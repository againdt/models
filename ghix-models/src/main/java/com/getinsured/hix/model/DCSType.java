/**
 * 
 */
package com.getinsured.hix.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Persistence class 'DCSType' mapping to table 'DCS_TYPE'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 15, 2013 4:19:21 PM 
 *
 */
@Entity
@Table(name = "DCS_TYPE")
public class DCSType {
	@Id
	@SequenceGenerator(name = "DCSType_Seq", sequenceName = "DCS_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DCSType_Seq")
	private int id;

	@ManyToOne
	@JoinColumn(name = "DCS_TIER_TYPE_ID", referencedColumnName = "ID")
    private DCSTierType dcsTierTypeId;
	
	@Column(name = "IN_NETWORK_INDICATOR")
	@NotNull
	private Character inNetworkIndicator;

	@Column(name = "PHARMACY_CODE")
	@Length(max = 200)
	@NotNull
	private String pharmacyCode;

    @Column(name="MEASURE_POINT_VALUE")
	@NotNull
    private Integer measurePointValue;
    
    @Column(name = "TIME_UNIT_CODE", length = 10)
	@NotNull
    private String timeUnitcode;
    
    @Length(max= 200)
    @Column(name = "DCS_CODE", length = 200)
	@NotNull
    private String dcsCode;
    
    @Column(name = "DCS_COPAYMENT_AMOUNT")
	@NotNull
    private BigDecimal dcsCoPaymentAmount;
    
    @NotNull
    @Column(name = "DCS_COINSURANCE_PERCENT")
    private BigDecimal dcsCoInsurancePercent;
    
    @Column(name = "BENEFIT_OFFERED")
    @NotNull
    private Character benefitOffered;
	
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp created;
	
	@Column(name="LAST_UPDATED_TIMESTAMP")
	@NotNull
	private Timestamp updated;
	/**
	 * Default constructor.
	 */
	public DCSType() {
		this.inNetworkIndicator = 'N'; 
		this.benefitOffered = 'N';
	}
	/**
	 * Getter for 'id'.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Setter for 'id'.
	 *
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Getter for 'dcsTierTypeId'.
	 *
	 * @return the dcsTierTypeId
	 */
	public DCSTierType getDcsTierTypeId() {
		return dcsTierTypeId;
	}
	/**
	 * Setter for 'dcsTierTypeId'.
	 *
	 * @param dcsTierTypeId the dcsTierTypeId to set
	 */
	public void setDcsTierTypeId(DCSTierType dcsTierTypeId) {
		this.dcsTierTypeId = dcsTierTypeId;
	}
	/**
	 * Getter for 'inNetworkIndicator'.
	 *
	 * @return the inNetworkIndicator
	 */
	public Character getInNetworkIndicator() {
		return inNetworkIndicator;
	}
	/**
	 * Setter for 'inNetworkIndicator'.
	 *
	 * @param inNetworkIndicator the inNetworkIndicator to set
	 */
	public void setInNetworkIndicator(Character inNetworkIndicator) {
		this.inNetworkIndicator = inNetworkIndicator;
	}
	/**
	 * Getter for 'pharmacyCode'.
	 *
	 * @return the pharmacyCode
	 */
	public String getPharmacyCode() {
		return pharmacyCode;
	}
	/**
	 * Setter for 'pharmacyCode'.
	 *
	 * @param pharmacyCode the pharmacyCode to set
	 */
	public void setPharmacyCode(String pharmacyCode) {
		this.pharmacyCode = pharmacyCode;
	}
	/**
	 * Getter for 'measurePointValue'.
	 *
	 * @return the measurePointValue
	 */
	public Integer getMeasurePointValue() {
		return measurePointValue;
	}
	/**
	 * Setter for 'measurePointValue'.
	 *
	 * @param measurePointValue the measurePointValue to set
	 */
	public void setMeasurePointValue(Integer measurePointValue) {
		this.measurePointValue = measurePointValue;
	}
	/**
	 * Getter for 'timeUnitcode'.
	 *
	 * @return the timeUnitcode
	 */
	public String getTimeUnitcode() {
		return timeUnitcode;
	}
	/**
	 * Setter for 'timeUnitcode'.
	 *
	 * @param timeUnitcode the timeUnitcode to set
	 */
	public void setTimeUnitcode(String timeUnitcode) {
		this.timeUnitcode = timeUnitcode;
	}
	/**
	 * Getter for 'dcsCode'.
	 *
	 * @return the dcsCode
	 */
	public String getDcsCode() {
		return dcsCode;
	}
	/**
	 * Setter for 'dcsCode'.
	 *
	 * @param dcsCode the dcsCode to set
	 */
	public void setDcsCode(String dcsCode) {
		this.dcsCode = dcsCode;
	}
	/**
	 * Getter for 'dcsCoPaymentAmount'.
	 *
	 * @return the dcsCoPaymentAmount
	 */
	public BigDecimal getDcsCoPaymentAmount() {
		return dcsCoPaymentAmount;
	}
	/**
	 * Setter for 'dcsCoPaymentAmount'.
	 *
	 * @param dcsCoPaymentAmount the dcsCoPaymentAmount to set
	 */
	public void setDcsCoPaymentAmount(BigDecimal dcsCoPaymentAmount) {
		this.dcsCoPaymentAmount = dcsCoPaymentAmount;
	}
	/**
	 * Getter for 'dcsCoInsurancePercent'.
	 *
	 * @return the dcsCoInsurancePercent
	 */
	public BigDecimal getDcsCoInsurancePercent() {
		return dcsCoInsurancePercent;
	}
	/**
	 * Setter for 'dcsCoInsurancePercent'.
	 *
	 * @param dcsCoInsurancePercent the dcsCoInsurancePercent to set
	 */
	public void setDcsCoInsurancePercent(BigDecimal dcsCoInsurancePercent) {
		this.dcsCoInsurancePercent = dcsCoInsurancePercent;
	}
	/**
	 * Getter for 'benefitOffered'.
	 *
	 * @return the benefitOffered
	 */
	public Character getBenefitOffered() {
		return benefitOffered;
	}
	/**
	 * Setter for 'benefitOffered'.
	 *
	 * @param benefitOffered the benefitOffered to set
	 */
	public void setBenefitOffered(Character benefitOffered) {
		this.benefitOffered = benefitOffered;
	}
	/**
	 * Getter for 'created'.
	 *
	 * @return the created
	 */
	public Timestamp getCreated() {
		return created;
	}
	/**
	 * Setter for 'created'.
	 *
	 * @param created the created to set
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	/**
	 * Getter for 'updated'.
	 *
	 * @return the updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}
	/**
	 * Setter for 'updated'.
	 *
	 * @param updated the updated to set
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

}
