package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ReconEnrollmentDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String recordCode ;
	private String	tradingPartnerId;
	private String	spoeId;
	private String	tenantId;
	private	String hiosId;
	private	String partialQhpPlanId;
	private	String issuerExtractDate;
	private	String issuerExtractTime;
	private	String agentBrokerName;
	private	String agentBrokerNpn;
	private	String coverageYear;
	private	String paidThroughDate;
	private	String eoyTerminationIndicator;
	private	String qhpId;
	private	String initialPremiumPaidStatus;
	private	String issuerAssignedRecordTraceNumber;
	private String issuerAssignedPolicyId;
	private String exchangeAssignedPolicyid;
	private String enrollmentStatus;
	private String enrollmentconfirmationDate;
	private String insuranceTypeCode;
	
	private List<ReconMemberDTO> members;
	
	private List<ReconMonthlyPremiumDTO> monthlyPremiums;
	
	private List<ReconAdditionalSubInfoDTO> additionalSubInfo;

	public String getRecordCode() {
		return recordCode;
	}

	public void setRecordCode(String recordCode) {
		this.recordCode = returnNullIfEmpty(recordCode);
	}

	public String getTradingPartnerId() {
		return tradingPartnerId;
	}

	public void setTradingPartnerId(String tradingPartnerId) {
		this.tradingPartnerId = returnNullIfEmpty(tradingPartnerId);
	}

	public String getSpoeId() {
		return spoeId;
	}

	public void setSpoeId(String spoeId) {
		this.spoeId = returnNullIfEmpty(spoeId);
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = returnNullIfEmpty(tenantId);
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = returnNullIfEmpty(hiosId);
	}

	public String getPartialQhpPlanId() {
		return partialQhpPlanId;
	}

	public void setPartialQhpPlanId(String partialQhpPlanId) {
		this.partialQhpPlanId = returnNullIfEmpty(partialQhpPlanId);
	}

	public String getIssuerExtractDate() {
		return issuerExtractDate;
	}

	public void setIssuerExtractDate(String issuerExtractDate) {
		this.issuerExtractDate = returnNullIfEmpty(issuerExtractDate);
	}

	public String getIssuerExtractTime() {
		return issuerExtractTime;
	}

	public void setIssuerExtractTime(String issuerExtractTime) {
		this.issuerExtractTime = returnNullIfEmpty(issuerExtractTime);
	}

	public String getAgentBrokerName() {
		return agentBrokerName;
	}

	public void setAgentBrokerName(String agentBrokerName) {
		this.agentBrokerName = returnNullIfEmpty(agentBrokerName);
	}

	public String getAgentBrokerNpn() {
		return agentBrokerNpn;
	}

	public void setAgentBrokerNpn(String agentBrokerNpn) {
		this.agentBrokerNpn = returnNullIfEmpty(agentBrokerNpn);
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = returnNullIfEmpty(coverageYear);
	}

	public String getPaidThroughDate() {
		return paidThroughDate;
	}

	public void setPaidThroughDate(String paidThroughDate) {
		this.paidThroughDate = returnNullIfEmpty(paidThroughDate);
	}

	public String getEoyTerminationIndicator() {
		return eoyTerminationIndicator;
	}

	public void setEoyTerminationIndicator(String eoyTerminationIndicator) {
		this.eoyTerminationIndicator = returnNullIfEmpty(eoyTerminationIndicator);
	}

	public String getQhpId() {
		return qhpId;
	}

	public void setQhpId(String qhpId) {
		this.qhpId = returnNullIfEmpty(qhpId);
	}


	public String getInitialPremiumPaidStatus() {
		return initialPremiumPaidStatus;
	}

	public void setInitialPremiumPaidStatus(String initialPremiumPaidStatus) {
		this.initialPremiumPaidStatus = returnNullIfEmpty(initialPremiumPaidStatus);
	}

	public String getIssuerAssignedRecordTraceNumber() {
		return issuerAssignedRecordTraceNumber;
	}

	public void setIssuerAssignedRecordTraceNumber(String issuerAssignedRecordTraceNumber) {
		this.issuerAssignedRecordTraceNumber = returnNullIfEmpty(issuerAssignedRecordTraceNumber);
	}

	public List<ReconMemberDTO> getMembers() {
		return members;
	}

	public void setMembers(List<ReconMemberDTO> members) {
		this.members = members;
	}

	public List<ReconMonthlyPremiumDTO> getMonthlyPremiums() {
		return monthlyPremiums;
	}

	public void setMonthlyPremiums(List<ReconMonthlyPremiumDTO> monthlyPremiums) {
		this.monthlyPremiums = monthlyPremiums;
	}

	public List<ReconAdditionalSubInfoDTO> getAdditionalSubInfo() {
		return additionalSubInfo;
	}

	public void setAdditionalSubInfo(List<ReconAdditionalSubInfoDTO> additionalSubInfo) {
		this.additionalSubInfo = additionalSubInfo;
	}

	public String getIssuerAssignedPolicyId() {
		return issuerAssignedPolicyId;
	}

	public void setIssuerAssignedPolicyId(String issuerAssignedPolicyId) {
		this.issuerAssignedPolicyId = issuerAssignedPolicyId;
	}

	public String getExchangeAssignedPolicyid() {
		return exchangeAssignedPolicyid;
	}

	public void setExchangeAssignedPolicyid(String exchangeAssignedPolicyid) {
		this.exchangeAssignedPolicyid = exchangeAssignedPolicyid;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getEnrollmentconfirmationDate() {
		return enrollmentconfirmationDate;
	}

	public void setEnrollmentconfirmationDate(String enrollmentconfirmationDate) {
		this.enrollmentconfirmationDate = enrollmentconfirmationDate;
	}

	public ReconMemberDTO getMemberFromMemberId(Integer memberId) {
		if (null != this.members && !this.members.isEmpty() && null != memberId) {
			for (ReconMemberDTO member : this.members) {
				if (null != member.getExchangeAssignedMemberId()
						&& member.getExchangeAssignedMemberId().trim().equalsIgnoreCase(String.valueOf(memberId))) {
					return member;
				}
			}
		}
		return null;
	}

	@JsonIgnore
	public ReconMemberDTO getSubscriber() {
		if (null != this.members && !this.members.isEmpty()) {
			for (ReconMemberDTO member : this.members) {
				if ("Y".equalsIgnoreCase(member.getSubscriberIndicator())) {
					return member;
				}
			}
		}
		return null;
	}
	
	private String returnNullIfEmpty(String value){
		if(null != value && !value.trim().isEmpty()){
			return value.trim();
		}else{
			return null;
		}
	}

	public String getInsuranceTypeCode() {
		return insuranceTypeCode;
	}

	public void setInsuranceTypeCode(String insuranceTypeCode) {
		this.insuranceTypeCode = insuranceTypeCode;
	}
}
