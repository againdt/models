/**
 * PlanRateBenefitRequest.java
 * @author chalse_v
 * @version 1.0
 * @since Mar 20, 2013 
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;

/**
 * 
 * @author chalse_v
 * @version 1.0
 * @since Mar 20, 2013 
 *
 */

public class PlanRateBenefitRequest implements Serializable {
	/**
	 * Attribute Logger LOGGER
	 */
	private static final long serialVersionUID = 6603025199062982626L;
	public static enum RequestMethod { GET, POST}
	
	/**
	 * Attribute String url
	 */
	private String url;
	/**
	 * Attribute RequestMethod requestMethod
	 */
	private RequestMethod requestMethod;
	/**
	 * Attribute Map<String,String> requestParameters
	 */
	private Map<String, Object> requestParameters = new HashMap<String, Object>();
	
	/**
	 * Constructor.
	 */
	public PlanRateBenefitRequest() {}
	
	/**
	 * Attribute List<PrescriptionSearchRequest> for prescription search
	 */
	List<PrescriptionSearchRequest> prescriptionRequestList = new ArrayList<PrescriptionSearchRequest>();
	
	/**
	 * @return the prescriptionRequestList
	 */
	public List<PrescriptionSearchRequest> getPrescriptionRequestList() {
		return prescriptionRequestList;
	}

	/**
	 * @param prescriptionRequestList the prescriptionRequestList to set
	 */
	public void setPrescriptionRequestList(
			List<PrescriptionSearchRequest> prescriptionRequestList) {
		this.prescriptionRequestList = prescriptionRequestList;
	}

	/**
	 * Getter for 'url'.
	 *
	 * @return the url.
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Setter for 'url'.
	 *
	 * @param url the url to set.
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Getter for 'requestMethod'.
	 *
	 * @return the requestMethod.
	 */
	public RequestMethod getRequestMethod() {
		return this.requestMethod;
	}

	/**
	 * Setter for 'requestMethod'.
	 *
	 * @param requestMethod the requestMethod to set.
	 */
	public void setRequestMethod(RequestMethod requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * Getter for 'requestParameters'.
	 *
	 * @return the requestParameters.
	 */
	public Map<String, Object> getRequestParameters() {
		return this.requestParameters;
	}

	/**
	 * Setter for 'requestParameters'.
	 *
	 * @param requestParameters the requestParameters to set
	 */
	public void setRequestParameters(Map<String, Object> requestParameters) {
		this.requestParameters = requestParameters;
	}

	/**
	 * Method to add entries into requestParameters collection.
	 *
	 * @param key The key for request data.
	 * @param data The request data.
	 * 
	 * @return void
	 */
	public void addRequestParameters(String key, Object data) {
		this.requestParameters.put(key, data);
	}
	
	
}
