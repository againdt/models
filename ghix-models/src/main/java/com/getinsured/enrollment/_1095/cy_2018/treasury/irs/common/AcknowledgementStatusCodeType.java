
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcknowledgementStatusCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AcknowledgementStatusCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESS"/>
 *     &lt;enumeration value="FAILURE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AcknowledgementStatusCodeType", namespace = "urn:us:gov:treasury:irs:common")
@XmlEnum
public enum AcknowledgementStatusCodeType {

    SUCCESS,
    FAILURE;

    public String value() {
        return name();
    }

    public static AcknowledgementStatusCodeType fromValue(String v) {
        return valueOf(v);
    }

}
