package com.getinsured.hix.model.consumer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;

@Entity
@Table(name="CMR_CALL_LOGS")
@DynamicInsert
@DynamicUpdate
@FilterDef(name = "tenantIdFilter", parameters = { @ParamDef(name = "tenantId", type = "long") })
@Filters({ @Filter(name = "tenantIdFilter", condition = "TENANT_ID = :tenantId") })
public class ConsumerCallLog implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CALL_LOGS_SEQ")
	@SequenceGenerator(name = "CALL_LOGS_SEQ", sequenceName = "CALL_LOGS_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="HOUSEHOLD_ID")
	private Integer householdId;
	
	@Column(name="ELIG_LEAD_ID")
	private Integer eligLeadId;
	
	@Column(name="AGENT_ID")
	private Integer agentId;
	
	@Column(name="RCK")
	private BigDecimal rck;
	
	@Column(name="DAY")
	private BigDecimal day;
	
  	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false, updatable=false)
	private Date created;
  	
  	@Column(name="DURATION_IN_SEC")
	private Integer duration;
  	
  	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "START_TIME")
	private Date datetime;
  	
	@Column(name = "CALL_DATA")
	private String callData;

	@Column(name = "CALL_ID")
	private String callId;
	
  	@Column(name="RECOVERY_KEY")
	private BigDecimal recoveryKey;
	
	@Column(name = "CALL_TYPE")
	private String callType;//INBOUND,OUTBOUND_AUTO,OUTBOUOND_MANUAL

	@Column(name="TALK_TIME_IN_SEC")
	private Integer talkTime;

	@Column(name = "WRAPUP_DATA")
	private String wrapupData;

  	@Column(name="AFFILIATE_FLOW_ID")
	private Integer affiliateFlowId;


	@Column(name = "FLAG_INITIAL_CALL")
	private String flagInitialCall;
  	
	
	@Column(name = "FLAG_IS_TRANSFER")
	private String flagIsTransfer;

	@Column(name = "PHONE_CALLED_TO_OR_DIALED")
	private String phoneCalledToOrDialed;

	@Column(name = "PHONE_CALLED_FROM")
	private String phoneCalledFrom;

  	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "END_TIME")
	private Date endTime;
  	
  	@Column(name="TENANT_ID")
    private Long tenantId;
	
	@Column(name="CREATED_BY")
	private Integer createdBy;
	
  	
  	@Column(name="LAST_UPDATED_BY")
	private Integer lastUpdatedBy;
	
  	
  	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date lastUpdated;
  	
	
  	
	
  	
  	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public BigDecimal getRck() {
		return rck;
	}

	public void setRck(BigDecimal rck) {
		this.rck = rck;
	}

	public BigDecimal getDay() {
		return day;
	}

	public void setDay(BigDecimal day) {
		this.day = day;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Integer getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(Integer eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getStartTime() {
		return datetime;
	}

	public void setStartTime(Date startTime) {
		this.datetime = startTime;
	}

	public String getCallData() {
		return callData;
	}

	public void setCallData(String callData) {
		this.callData = callData;
	}
	
	
//	HIX-51089 : Reverting code to removed the ambiguity for setter and getter of var datetime
//	public Date getDatetime() {
//		return datetime;
//	}
//
//	public void setDatetime(Date datetime) {
//		this.datetime = datetime;
//	}

	public BigDecimal getRecoveryKey() {
		return recoveryKey;
	}

	public void setRecoveryKey(BigDecimal recoveryKey) {
		this.recoveryKey = recoveryKey;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public Integer getTalkTime() {
		return talkTime;
	}

	public void setTalkTime(Integer talkTime) {
		this.talkTime = talkTime;
	}

	public String getWrapupData() {
		return wrapupData;
	}

	public void setWrapupData(String wrapupData) {
		this.wrapupData = wrapupData;
	}

	public Integer getAffiliateFlowId() {
		return affiliateFlowId;
	}

	public void setAffiliateFlowId(Integer affiliateFlowId) {
		this.affiliateFlowId = affiliateFlowId;
	}

	public String getFlagInitialCall() {
		return flagInitialCall;
	}

	public void setFlagInitialCall(String flagInitialCall) {
		this.flagInitialCall = flagInitialCall;
	}

	public String getFlagIsTransfer() {
		return flagIsTransfer;
	}

	public void setFlagIsTransfer(String flagIsTransfer) {
		this.flagIsTransfer = flagIsTransfer;
	}

	public String getPhoneCalledToOrDialed() {
		return phoneCalledToOrDialed;
	}

	public void setPhoneCalledToOrDialed(String phoneCalledToOrDialed) {
		this.phoneCalledToOrDialed = phoneCalledToOrDialed;
	}

	public String getPhoneCalledFrom() {
		return phoneCalledFrom;
	}

	public void setPhoneCalledFrom(String phoneCalledFrom) {
		this.phoneCalledFrom = phoneCalledFrom;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getTenantId() {
		return tenantId;
	}
	
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/* To AutoUpdate created date while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreated(new TSDate());
		
		if(TenantContextHolder.getTenant() != null && TenantContextHolder.getTenant().getId() != null) {
	        this.setTenantId(TenantContextHolder.getTenant().getId());
		}
	}

	@Override
	public String toString() {
		return "consumerCallLog [id=" + id + ", householdId=" + householdId
				+ ", eligLeadId=" + eligLeadId + ", agentId=" + agentId + ", rck=" + rck + ", day=" + day
				+ ", created=" + created + ", datetime=" + datetime + ", callId=" + callId
				+ ", callType=" + callType + ", flagIsTransfer=" + flagIsTransfer
				+ ", phoneCalledToOrDialed=" + phoneCalledToOrDialed
				+ ", phoneCalledFrom=" + phoneCalledFrom + ", endTime=" + endTime + ", createdBy=" + createdBy
				+ ", plastUpdatedBy=" + lastUpdatedBy + ", lastUpdated=" + lastUpdated + "]";
	}

}
