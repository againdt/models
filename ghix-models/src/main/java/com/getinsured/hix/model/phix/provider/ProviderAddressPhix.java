package com.getinsured.hix.model.phix.provider;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="provider_address_phix")
public class ProviderAddressPhix  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String address;
	private String suite;
	private String city;
	private String state;
	private String county;
	private String zip;
	private Float lattitude;
	private Float longitude;
	private Float addressQualityId;
	private Set<ProviderProduct> providerProducts = new HashSet<ProviderProduct>(0);


	@Id 
	@Column(name="ID", unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@Column(name="ADDRESS", length=150)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	@Column(name="SUITE", length=150)
	public String getSuite() {
		return this.suite;
	}

	public void setSuite(String suite) {
		this.suite = suite;
	}


	@Column(name="CITY", length=50)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}


	@Column(name="STATE", length=2)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name="ZIP", length=5)
	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}


	@Column(name="LATTITUDE", precision=15, scale=9)
	public Float getLattitude() {
		return this.lattitude;
	}

	public void setLattitude(Float lattitude) {
		this.lattitude = lattitude;
	}


	@Column(name="LONGITUDE", precision=15, scale=9)
	public Float getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}


	@Column(name="ADDRESS_QUALITY_ID", precision=10, scale=0)
	public Float getAddressQualityId() {
		return this.addressQualityId;
	}

	public void setAddressQualityId(Float addressQualityId) {
		this.addressQualityId = addressQualityId;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="providerAddressPhix")
	public Set<ProviderProduct> getProviderProducts() {
		return this.providerProducts;
	}

	public void setProviderProducts(Set<ProviderProduct> providerProducts) {
		this.providerProducts = providerProducts;
	}

}


