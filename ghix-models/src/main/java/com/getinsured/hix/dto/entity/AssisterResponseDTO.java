package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.Site;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.consumer.Household;
/**
 * Used to wrap data fetched from database in ghix-entity and send it to
 * ghix-web
 * 
 */
public class AssisterResponseDTO extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Site> listOfSites;
	private int responseCode;
	private String responseDescription;
	private Assister assister;
	private AssisterLanguages assisterLanguages;
	private List<Assister> listOfAssisters;
	private Map<String, Boolean> navigationMenuMap;
	private EntityDocuments assisterDocuments;
	private byte[] attachment;
	private String document;
	private Map<String, Object> assisterListAndRecordCount;
	private List<Map<String, Object>> assisterEntityHistory;
	
	@JsonSerialize( keyUsing = MapKeySerializer.class)
	@JsonDeserialize( keyUsing = MapKeyDeserializer.class)
	private Map<Assister, AssisterLanguages> assisterWithLanguages;
	
	private DesignateAssister designateAssister;
	private int noOfAssistersForEEAndSite;
	private ExternalIndividual externalIndividual;
	private int noOfIndividualsForAssister;
	private int totalAssistersBySearch;
	private int totalAssisters;
	private int assistersUpForRenewal;
	private int inactiveAssisters;
	private Map<Integer, Integer> assisterClientCountMap;
	private Household houseHold;
	private List<AssisterExportDTO> assisterExportDTOList;
	
	public List<AssisterExportDTO> getAssisterExportDTOList() {
		return assisterExportDTOList;
	}

	public void setAssisterExportDTOList(List<AssisterExportDTO> assisterExportDTOList) {
		this.assisterExportDTOList = assisterExportDTOList;
	}

	public Household getHouseHold() {
		return houseHold;
	}

	public void setHouseHold(Household houseHold) {
		this.houseHold = houseHold;
	}

	/**
	 * Method to get assisterClientCountMap
	 * @return the assisterClientCountMap
	 */
	public Map<Integer, Integer> getAssisterClientCountMap() {
		return assisterClientCountMap;
	}

	/**
	 * Method to set assisterClientCountMap
	 * @param assisterClientCountMap the assisterClientCountMap to set
	 */
	public void setAssisterClientCountMap(
			Map<Integer, Integer> assisterClientCountMap) {
		this.assisterClientCountMap = assisterClientCountMap;
	}

	public int getNoOfAssistersForEEAndSite() {
		return noOfAssistersForEEAndSite;
	}

	public void setNoOfAssistersForEEAndSite(int noOfAssistersForEEAndSite) {
		this.noOfAssistersForEEAndSite = noOfAssistersForEEAndSite;
	}

	public DesignateAssister getDesignateAssister() {
		return designateAssister;
	}

	public void setDesignateAssister(DesignateAssister designateAssister) {
		this.designateAssister = designateAssister;
	}

	public Map<Assister, AssisterLanguages> getAssisterWithLanguages() {
		return assisterWithLanguages;
	}

	public void setAssisterWithLanguages(Map<Assister, AssisterLanguages> assisterWithLanguages) {
		this.assisterWithLanguages = assisterWithLanguages;
	}

	public int getTotalAssisters() {
		return totalAssisters;
	}

	public void setTotalAssisters(int totalAssisters) {
		this.totalAssisters = totalAssisters;
	}

	public int getAssistersUpForRenewal() {
		return assistersUpForRenewal;
	}

	public void setAssistersUpForRenewal(int assistersUpForRenewal) {
		this.assistersUpForRenewal = assistersUpForRenewal;
	}

	public int getInactiveAssisters() {
		return inactiveAssisters;
	}

	public void setInactiveAssisters(int inactiveAssisters) {
		this.inactiveAssisters = inactiveAssisters;
	}

	public int getTotalAssistersBySearch() {
		return totalAssistersBySearch;
	}

	public void setTotalAssistersBySearch(int totalAssistersBySearch) {
		this.totalAssistersBySearch = totalAssistersBySearch;
	}

	public List<Map<String, Object>> getAssisterEntityHistory() {
		return assisterEntityHistory;
	}

	public void setAssisterEntityHistory(List<Map<String, Object>> assisterEntityHistory) {
		this.assisterEntityHistory = assisterEntityHistory;
	}

	public Map<String, Boolean> getNavigationMenuMap() {
		return navigationMenuMap;
	}

	public void setNavigationMenuMap(Map<String, Boolean> navigationMenuMap) {
		this.navigationMenuMap = navigationMenuMap;
	}

	public EntityDocuments getAssisterDocuments() {
		return assisterDocuments;
	}

	public void setAssisterDocuments(EntityDocuments assisterDocuments) {
		this.assisterDocuments = assisterDocuments;
	}

	public byte[] getAttachment() {
		return attachment;
	}

	public void setAttachment(byte[] attachment1) {
		if (attachment1 == null) {
			this.attachment = new byte[0];
		} else {
			this.attachment = Arrays.copyOf(attachment1, attachment1.length);
		}
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Map<String, Object> getAssisterListAndRecordCount() {
		return assisterListAndRecordCount;
	}

	public void setAssisterListAndRecordCount(Map<String, Object> assisterListAndRecordCount) {
		this.assisterListAndRecordCount = assisterListAndRecordCount;
	}

	public List<Assister> getListOfAssisters() {
		return listOfAssisters;
	}

	public void setListOfAssisters(List<Assister> listOfAssisters) {
		this.listOfAssisters = listOfAssisters;
	}

	public Assister getAssister() {
		return assister;
	}

	public void setAssister(Assister assister) {
		this.assister = assister;
	}

	public AssisterLanguages getAssisterLanguages() {
		return assisterLanguages;
	}

	public void setAssisterLanguages(AssisterLanguages assisterLanguages) {
		this.assisterLanguages = assisterLanguages;
	}

	public List<Site> getListOfSites() {
		return listOfSites;
	}

	public void setListOfSites(List<Site> listOfSites) {
		this.listOfSites = listOfSites;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public ExternalIndividual getExternalIndividual() {
		return externalIndividual;
	}

	public void setExternalIndividual(ExternalIndividual externalIndividual) {
		this.externalIndividual = externalIndividual;
	}

	public int getNoOfIndividualsForAssister() {
		return noOfIndividualsForAssister;
	}

	public void setNoOfIndividualsForAssister(int noOfIndividualsForAssister) {
		this.noOfIndividualsForAssister = noOfIndividualsForAssister;
	}
	
	@Override
	public String toString() {
		return "AssisterResponseDTO Details: [Assister = " + assister + "], responseCode = " + responseCode + ", responseDescription = " + responseDescription + ", [designateAssister = " + designateAssister + " ] ";
	}

}
