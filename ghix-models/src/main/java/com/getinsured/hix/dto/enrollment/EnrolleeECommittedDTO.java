package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * @author rajaramesh_g
 * @since 22/11/2013
 */
public class EnrolleeECommittedDTO implements Serializable{
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private Date dob;
	private String useOfTobacco;
	private String enrolleeType;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getUseOfTobacco() {
		return useOfTobacco;
	}
	public void setUseOfTobacco(String useOfTobacco) {
		this.useOfTobacco = useOfTobacco;
	}
	public String getEnrolleeType() {
		return enrolleeType;
	}
	public void setEnrolleeType(String enrolleeType) {
		this.enrolleeType = enrolleeType;
	}
	
	@Override
	public String toString() {
		return "EnrolleeECommittedDTO [firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName
				+ ", gender=" + gender + ", dob=" + dob + ", useOfTobacco="
				+ useOfTobacco + ", enrolleeType=" + enrolleeType + "]";
	}
	
	
	
}