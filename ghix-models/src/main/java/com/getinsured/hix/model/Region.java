package com.getinsured.hix.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="region")
public class Region implements Serializable
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REGION_SEQ")
	@SequenceGenerator(name = "REGION_SEQ", sequenceName = "REGION_SEQ", allocationSize = 1)
	private int id;
	
	@Column(name="region_name", length=100)
	private String regionName;
	
	@Column(name="county", length=255)
	private String county;
	
	@Column(name="zip", length=5)
	private String zip;
	
	@Column(name="county_fips")
	private String countyFips;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountyFips() {
		return countyFips;
	}

	public void setCountyFips(String countyFips) {
		this.countyFips = countyFips;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}	
	
}