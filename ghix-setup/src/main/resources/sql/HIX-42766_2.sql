INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_VIEW_AND_PRINT'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_START_APP'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_ENROLL_APP'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_EDIT_APP'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_REPORT_CHANGES'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_INIT_VERIFICATIONS'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_VIEW_VER_RESULTS'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_RUN_ELIGIBILITY'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_VIEW_ELIG_RESULTS'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_CONTACT_US'));
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_SUBMIT_APPEAL'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_DISENROLL'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_UPDATE_CARRIER'));	
INSERT INTO ROLE_PERMISSIONS (id, created, updated, role_id, permission_id )   
            	VALUES (role_permissions_seq.NEXTVAL, SYSTIMESTAMP, SYSTIMESTAMP,
                          										(select id from roles where name='ADMIN'), 
                                										(select id from permissions where name='PORTAL_EDIT_SPCL_ENRLMNT_PERIOD'));	         
