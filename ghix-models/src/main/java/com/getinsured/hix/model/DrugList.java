package com.getinsured.hix.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.getinsured.timeshift.TimeShifterUtil;

/**
 * This is entity class for PM_DRUG_LIST table.
 *  
 * @author vardekar_s
 *
 */

@Entity
@Table(name = "PM_DRUG_LIST")
@DynamicInsert
@DynamicUpdate
public class DrugList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PM_DRUG_LIST_SEQ")
	@SequenceGenerator(name = "PM_DRUG_LIST_SEQ", sequenceName = "PM_DRUG_LIST_SEQ", allocationSize = 1)
	private long id;	
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "ISSUER_ID", referencedColumnName = "ID")
	private Issuer issuer;
	
	@Column(name = "FORMULARY_DRUG_LIST_ID")
	private Integer formularyDrugListId;	
	
	@Column(name="CREATION_TIMESTAMP")
	@NotNull
	private Timestamp creationTimestamp;
	
	@Column(name="LAST_UPDATE_TIMESTAMP")
	@NotNull
	private Timestamp lastUpdateTimestamp;
		
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "drugList", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private  List<Formulary> formulary;	
	
	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@OneToMany(mappedBy = "drugList", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private  List<DrugTier> drugsTiers;	
	
	@Column(name="IS_DELETED")
	private String isDeleted;

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist()
	{
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate()
	{
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis())); 
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	public Integer getFormularyDrugListId() {
		return formularyDrugListId;
	}

	public void setFormularyDrugListId(Integer formularyDrugListId) {
		this.formularyDrugListId = formularyDrugListId;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public List<Formulary> getFormulary() {
		return formulary;
	}

	public void setFormulary(List<Formulary> formulary) {
		this.formulary = formulary;
	}

	public List<DrugTier> getDrugsTiers() {
		return drugsTiers;
	}

	public void setDrugsTiers(List<DrugTier> drugsTiers) {
		this.drugsTiers = drugsTiers;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}			
}
