/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Map;

/**
 * @author hardas_d 
 * This model holds the REST parameters for the GHIX-TKM war
 */
public class TkmTicketsTaskRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer taskId;
	private Integer userId;
	private Integer groupId;
	private String activitiTaskId;
	private String processInstanceId;
	private String comments;
	private Map<String,Object> taskVariables;
	private String assigneeName;
	private String taskTicketId;

	/**
	 * @return the taskId
	 */
	public Integer getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId
	 *            the taskId to set
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the activitiTaskId
	 */
	public String getActivitiTaskId() {
		return activitiTaskId;
	}

	/**
	 * @param activitiTaskId
	 *            the activitiTaskId to set
	 */
	public void setActivitiTaskId(String activitiTaskId) {
		this.activitiTaskId = activitiTaskId;
	}

	/**
	 * @return the processInstanceId
	 */
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	/**
	 * @param processInstanceId
	 *            the processInstanceId to set
	 */
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	public Map<String, Object> getTaskVariables() {
		return taskVariables;
	}

	public void setTaskVariables(Map<String, Object> taskVariables) {
		this.taskVariables = taskVariables;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getTaskTicketId() {
		return taskTicketId;
	}

	public void setTaskTicketId(String taskTicketId) {
		this.taskTicketId = taskTicketId;
	}

}
