package com.getinsured.hix.dto.plandisplay.planavailability;

import java.util.List;

public class PdResponsePlanAvailability {

	private List<PdOrderItemResponse> pdOrderItemResponseList;
	private String response;
	private String nestedStackTrace;
	private String errCode;

	public List<PdOrderItemResponse> getPdOrderItemResponse() {
		return pdOrderItemResponseList;
	}

	public void setPdOrderItemResponse(List<PdOrderItemResponse> pdOrderItemResponseList) {
		this.pdOrderItemResponseList = pdOrderItemResponseList;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getNestedStackTrace() {
		return nestedStackTrace;
	}

	public void setNestedStackTrace(String nestedStackTrace) {
		this.nestedStackTrace = nestedStackTrace;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

}
