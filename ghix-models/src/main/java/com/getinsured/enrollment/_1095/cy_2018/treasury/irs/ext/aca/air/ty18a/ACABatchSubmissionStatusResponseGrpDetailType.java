
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.getinsured.enrollment._1095.cy_2018.treasury.irs.common.SubmissionStatusCodeType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DictionaryEntryNm&gt;ACA Form Submission Status Response Detail Type&lt;/DictionaryEntryNm&gt;
 * 					&lt;MajorVersionNum&gt;1&lt;/MajorVersionNum&gt;
 * 					&lt;MinorVersionNum&gt;0&lt;/MinorVersionNum&gt;
 * 					&lt;VersionEffectiveBeginDt&gt;2012-09-01&lt;/VersionEffectiveBeginDt&gt;
 * 					&lt;VersionDescriptionTxt&gt;Initial version&lt;/VersionDescriptionTxt&gt;
 * 					&lt;Description&gt;A group that provides ACA Form Submission Status response message related information&lt;/Description&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for ACABatchSubmissionStatusResponseGrpDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ACABatchSubmissionStatusResponseGrpDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}ReceiptId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}SubmissionReceivedTs"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}SubmissionStatusCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}RequestId"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}PaginationTotalNumHits" minOccurs="0"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}PaginationReturnedResults" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ACABatchSubmissionStatusResponseGrpDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "receiptId",
    "submissionReceivedTs",
    "submissionStatusCd",
    "requestId",
    "paginationTotalNumHits",
    "paginationReturnedResults"
})
public class ACABatchSubmissionStatusResponseGrpDetailType {

    @XmlElement(name = "ReceiptId", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String receiptId;
    @XmlElement(name = "SubmissionReceivedTs", namespace = "urn:us:gov:treasury:irs:common", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar submissionReceivedTs;
    @XmlElement(name = "SubmissionStatusCd", namespace = "urn:us:gov:treasury:irs:common", required = true)
    @XmlSchemaType(name = "string")
    protected SubmissionStatusCodeType submissionStatusCd;
    @XmlElement(name = "RequestId", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String requestId;
    @XmlElement(name = "PaginationTotalNumHits", namespace = "urn:us:gov:treasury:irs:common")
    protected BigInteger paginationTotalNumHits;
    @XmlElement(name = "PaginationReturnedResults", namespace = "urn:us:gov:treasury:irs:common")
    protected BigInteger paginationReturnedResults;

    /**
     * Gets the value of the receiptId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptId() {
        return receiptId;
    }

    /**
     * Sets the value of the receiptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptId(String value) {
        this.receiptId = value;
    }

    /**
     * Gets the value of the submissionReceivedTs property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubmissionReceivedTs() {
        return submissionReceivedTs;
    }

    /**
     * Sets the value of the submissionReceivedTs property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubmissionReceivedTs(XMLGregorianCalendar value) {
        this.submissionReceivedTs = value;
    }

    /**
     * Gets the value of the submissionStatusCd property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionStatusCodeType }
     *     
     */
    public SubmissionStatusCodeType getSubmissionStatusCd() {
        return submissionStatusCd;
    }

    /**
     * Sets the value of the submissionStatusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionStatusCodeType }
     *     
     */
    public void setSubmissionStatusCd(SubmissionStatusCodeType value) {
        this.submissionStatusCd = value;
    }

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the paginationTotalNumHits property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPaginationTotalNumHits() {
        return paginationTotalNumHits;
    }

    /**
     * Sets the value of the paginationTotalNumHits property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPaginationTotalNumHits(BigInteger value) {
        this.paginationTotalNumHits = value;
    }

    /**
     * Gets the value of the paginationReturnedResults property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPaginationReturnedResults() {
        return paginationReturnedResults;
    }

    /**
     * Sets the value of the paginationReturnedResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPaginationReturnedResults(BigInteger value) {
        this.paginationReturnedResults = value;
    }

}
