package com.getinsured.hix.model;

import java.io.Serializable;

/**
 * Contains the data that needs to be sent to AHBX for IND54
 */
public class DelegationRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long recordId;
	private long certificationId;
	private long phoneNumber;

	private long functionalId;
	private long entityNumber;
	private String agentLicenseNumber;
	private String certificationStausCode;
	private String recordType;
	private String contactFirstName;
	private String contactLastName;
	private String businessLegalName;
	private String addressLineOne;
	private String addressLineTwo;
	private String city;
	private String state;
	private String zipCode;
	private String assisterFirstName;
	private String assisterLastName;
	private String emailId;
	private String certificationDate;
	private int hiosIssuerId;

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getFunctionalId() {
		return functionalId;
	}

	public void setFunctionalId(long functionalId) {
		this.functionalId = functionalId;
	}

	public long getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(long entityNumber) {
		this.entityNumber = entityNumber;
	}

	public long getCertificationId() {
		return certificationId;
	}

	public void setCertificationId(long certificationId) {
		this.certificationId = certificationId;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCertificationStausCode() {
		return certificationStausCode;
	}

	public void setCertificationStausCode(String certificationStausCode) {
		this.certificationStausCode = certificationStausCode;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAssisterFirstName() {
		return assisterFirstName;
	}

	public void setAssisterFirstName(String assisterFirstName) {
		this.assisterFirstName = assisterFirstName;
	}

	public String getAssisterLastName() {
		return assisterLastName;
	}

	public void setAssisterLastName(String assisterLastName) {
		this.assisterLastName = assisterLastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}
	
	public int getHiosIssuerId() {
		return hiosIssuerId;
	}
	
	public void setHiosIssuerId(int hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
	
	public String getAgentLicenseNumber() {
		return agentLicenseNumber;
	}

	public void setAgentLicenseNumber(String agentLicenseNumber) {
		this.agentLicenseNumber = agentLicenseNumber;
	}

	@Override
	public String toString() {
		return "DelegationRequest details: RecordId = "+recordId+", CertificationId = "+certificationId+", " +
				"FunctionalId = "+functionalId+", EntityNumber = "+entityNumber+
						", ZipCode = "+zipCode+", CertificationStausCode = "+certificationStausCode+", RecordType = "+recordType;
	}

}
