package com.getinsured.hix.dto.plandisplay;

import java.math.BigDecimal;

public class APTCPlanResponseDTO{
	private Float appliedAptc;
	private BigDecimal appliedStateSubsidy;
	private Float premiumBeforeCredit;
	private Float premiumAfterCredit;
	private String insuranceType;
	
	public Float getAppliedAptc() {
		return appliedAptc;
	}
	public void setAppliedAptc(Float appliedAptc) {
		this.appliedAptc = appliedAptc;
	}
	public Float getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}
	public void setPremiumBeforeCredit(Float premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}
	public Float getPremiumAfterCredit() {
		return premiumAfterCredit;
	}
	public void setPremiumAfterCredit(Float premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}
	public String getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public BigDecimal getAppliedStateSubsidy() {
		return appliedStateSubsidy;
	}

	public void setAppliedStateSubsidy(BigDecimal appliedStateSubsidy) {
		this.appliedStateSubsidy = appliedStateSubsidy;
	}
}
