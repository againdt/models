/** @author santanu
 * @version 1.0
 * @since Feb 22, 2016 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

public class RatingAreaResponseDTO  extends GHIXResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String ratingArea;

	/**
	 * @return the ratingArea
	 */
	public String getRatingArea() {
		return ratingArea;
	}

	/**
	 * @param ratingArea the ratingArea to set
	 */
	public void setRatingArea(String ratingArea) {
		this.ratingArea = ratingArea;
	}

	

}
