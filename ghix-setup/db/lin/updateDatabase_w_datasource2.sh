#java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml update

#cd /home/oracle/dataload_dir

if [ $# -ne 1 ]
then
  echo " "
  echo "Usage $0 <profile in low-case>"
  echo "Usage $0 [nm|ms|id|phix]"
  echo "Example: $0 nm"
  echo " "
  exit 2
fi

echo $1

java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml clearCheckSums

java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml clearCheckSums

java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase_datasource2.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_datasource2.xml clearCheckSums

java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase_datasource2.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_datasource2.xml update


java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml update

java -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml update


