package com.getinsured.hix.dto.cap.consumerapp.enums;

public enum PageType {
	HOUSEHOLD_NONE,
	HOUSEHOLD_SINGLE,
	HOUSEHOLD_MULTI
}
