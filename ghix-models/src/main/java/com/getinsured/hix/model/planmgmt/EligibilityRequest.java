package com.getinsured.hix.model.planmgmt;

public class EligibilityRequest extends HouseHoldRequest{
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[");
		stringBuilder.append(super.toString());
		stringBuilder.append(", costSharingVariation=");
		stringBuilder.append(costSharingVariation);
		stringBuilder.append(" ]");
		return stringBuilder.toString();
	}

	private String exchangeFlowType;
	private String costSharingVariation;
	private String sendAllMetalTier;
	private boolean sendOnlySilverPlan;
	private String tenantCode;

	/**
	 * @return the sendAllMetalTier
	 */
	public String getSendAllMetalTier() {
		return sendAllMetalTier;
	}

	/**
	 * @param sendAllMetalTier the sendAllMetalTier to set
	 */
	public void setSendAllMetalTier(String sendAllMetalTier) {
		this.sendAllMetalTier = sendAllMetalTier;
	}

	public String getCostSharingVariation() {
		return costSharingVariation;
	}

	public void setCostSharingVariation(String costSharingVariation) {
		this.costSharingVariation = costSharingVariation;
	}

	public String getExchangeFlowType() {
		return exchangeFlowType;
	}

	public void setExchangeFlowType(String exchangeFlowType) {
		this.exchangeFlowType = exchangeFlowType;
	}

	public boolean getSendOnlySilverPlan() {
		return sendOnlySilverPlan;
	}

	public void setSendOnlySilverPlan(boolean sendOnlySilverPlan) {
		this.sendOnlySilverPlan = sendOnlySilverPlan;
	}

	/**
	 * @return the tenantCode
	 */
	public String getTenantCode() {
		return tenantCode;
	}

	/**
	 * @param tenantCode the tenantCode to set
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}


}
