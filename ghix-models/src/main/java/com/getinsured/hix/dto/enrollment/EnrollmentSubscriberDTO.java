package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class EnrollmentSubscriberDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer enrollmentID;
	private Integer issuerID;
	private String CMSPlanID;
	private String planName;
	private String planLevel;
	private String insurerName;
	private Date enrollmentBenefitStartDate;
	private Date enrollmentBenefitEndDate;
	private String enrollmentStatusLookupCode;
	private String enrollmentStatusLookupLabel;
	private String insuranceTypeLookupCode;
	private String insuranceTypeLookupLabel;
	
	private String subscriberFirstName;
	private String subscriberMiddleName;
	private String subscriberLastName;
	private Date birthDate;
	private String lastFourDigitSSN;
	private String subscriberFullName;
	private String exchgSubscriberIdentifier;
	private String coverageYear;
	public Integer getEnrollmentID() {
		return enrollmentID;
	}
	public void setEnrollmentID(Integer enrollmentID) {
		this.enrollmentID = enrollmentID;
	}
	public Integer getIssuerID() {
		return issuerID;
	}
	public void setIssuerID(Integer issuerID) {
		this.issuerID = issuerID;
	}
	public String getCMSPlanID() {
		return CMSPlanID;
	}
	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	public String getInsurerName() {
		return insurerName;
	}
	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}
	public Date getEnrollmentBenefitStartDate() {
		return enrollmentBenefitStartDate;
	}
	public void setEnrollmentBenefitStartDate(Date enrollmentBenefitStartDate) {
		this.enrollmentBenefitStartDate = enrollmentBenefitStartDate;
	}
	public Date getEnrollmentBenefitEndDate() {
		return enrollmentBenefitEndDate;
	}
	public void setEnrollmentBenefitEndDate(Date enrollmentBenefitEndDate) {
		this.enrollmentBenefitEndDate = enrollmentBenefitEndDate;
	}
	public String getEnrollmentStatusLookupCode() {
		return enrollmentStatusLookupCode;
	}
	public void setEnrollmentStatusLookupCode(String enrollmentStatusLookupCode) {
		this.enrollmentStatusLookupCode = enrollmentStatusLookupCode;
	}
	public String getEnrollmentStatusLookupLabel() {
		return enrollmentStatusLookupLabel;
	}
	public void setEnrollmentStatusLookupLabel(String enrollmentStatusLookupLabel) {
		this.enrollmentStatusLookupLabel = enrollmentStatusLookupLabel;
	}
	public String getSubscriberFirstName() {
		return subscriberFirstName;
	}
	public void setSubscriberFirstName(String subscriberFirstName) {
		this.subscriberFirstName = subscriberFirstName;
	}
	public String getSubscriberMiddleName() {
		return subscriberMiddleName;
	}
	public void setSubscriberMiddleName(String subscriberMiddleName) {
		this.subscriberMiddleName = subscriberMiddleName;
	}
	public String getSubscriberLastName() {
		return subscriberLastName;
	}
	public void setSubscriberLastName(String subscriberLastName) {
		this.subscriberLastName = subscriberLastName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getLastFourDigitSSN() {
		return lastFourDigitSSN;
	}
	public void setLastFourDigitSSN(String lastFourDigitSSN) {
		this.lastFourDigitSSN = lastFourDigitSSN;
	}
	public String getSubscriberFullName() {
		return subscriberFullName;
	}
	public void setSubscriberFullName(String subscriberFullName) {
		this.subscriberFullName = subscriberFullName;
	}
	public String getExchgSubscriberIdentifier() {
		return exchgSubscriberIdentifier;
	}
	public void setExchgSubscriberIdentifier(String exchgSubscriberIdentifier) {
		this.exchgSubscriberIdentifier = exchgSubscriberIdentifier;
	}
	public String getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getInsuranceTypeLookupCode() {
		return insuranceTypeLookupCode;
	}
	public void setInsuranceTypeLookupCode(String insuranceTypeLookupCode) {
		this.insuranceTypeLookupCode = insuranceTypeLookupCode;
	}
	public String getInsuranceTypeLookupLabel() {
		return insuranceTypeLookupLabel;
	}
	public void setInsuranceTypeLookupLabel(String insuranceTypeLookupLabel) {
		this.insuranceTypeLookupLabel = insuranceTypeLookupLabel;
	}
}
