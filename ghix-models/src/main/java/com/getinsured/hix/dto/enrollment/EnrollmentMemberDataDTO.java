package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class EnrollmentMemberDataDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String exchgIndivIdentifier;
	
	private Integer enrollmentID;
	private Long ssapApplicationID;
	private Long priorSsapApplicationid;
	private String houseHoldCaseID;
	private Date benefitEffectiveDate;
	private Date benefitEndDate;
	private String enrollmentStatus;
	private String insuranceType;
	
	private Date memberCoverageStartDate;
	private Date memberCoverageEndDate;
	private String enrolleeStatus;
	private String personType;
	private String CMSPlanID;
	private String CSRLevel;
	private String planLevel;
	private Float ehbPercent;

	private String hiosIssuerId;
	private String issuerName;
	private String planName;
	private Float netPremium;
	
	private String allowChangePlan;
	private Date enrollmentCreationDate;
	
	private List<EnrollmentPremiumDTO> enrollmentMonthlyPremium;
	
	public EnrollmentMemberDataDTO(String exchgIndivIdentifier, Integer enrollmentID, Long ssapApplicationID, Long priorSsapApplicationid, String houseHoldCaseID, Date benefitEffectiveDate, 
			Date benefitEndDate, String enrollmentStatus, String insuranceType, Date memberCoverageStartDate, Date memberCoverageEndDate, String enrolleeStatus, 
			String personType, String CMSPlanID, String CSRLevel, String planLevel, Float ehbPercent,String hiosIssuerId, String issuerName,  String planName,Float netPremium, String allowChangePlan,Date enrollmentCreationDate ){
		this.exchgIndivIdentifier = exchgIndivIdentifier;
		this.enrollmentID = enrollmentID;
		this.ssapApplicationID = ssapApplicationID;
		this.priorSsapApplicationid = priorSsapApplicationid; 
		this.houseHoldCaseID = houseHoldCaseID;
		this.benefitEffectiveDate = benefitEffectiveDate;
		this.benefitEndDate = benefitEndDate;
		this.enrollmentStatus = enrollmentStatus;
		this.insuranceType = insuranceType;
		this.memberCoverageStartDate = memberCoverageStartDate;
		this.memberCoverageEndDate = memberCoverageEndDate;
		this.enrolleeStatus = enrolleeStatus;
		this.personType = personType;
		this.CMSPlanID = CMSPlanID;
		this.CSRLevel = CSRLevel;
		this.planLevel = planLevel;
		this.ehbPercent = ehbPercent;
		this.hiosIssuerId = hiosIssuerId;
		this.issuerName = issuerName;
		this.planName = planName;
		this.netPremium = netPremium;
		this.allowChangePlan=allowChangePlan;
		this.enrollmentCreationDate=enrollmentCreationDate;
	}

	public EnrollmentMemberDataDTO(Integer enrollmentID, Long ssapApplicationID) {
		this.enrollmentID = enrollmentID;
		this.ssapApplicationID = ssapApplicationID;
	}

	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}

	public Integer getEnrollmentID() {
		return enrollmentID;
	}

	public void setEnrollmentID(Integer enrollmentID) {
		this.enrollmentID = enrollmentID;
	}

	public Long getSsapApplicationID() {
		return ssapApplicationID;
	}

	public void setSsapApplicationID(Long ssapApplicationID) {
		this.ssapApplicationID = ssapApplicationID;
	}

	public String getHouseHoldCaseID() {
		return houseHoldCaseID;
	}

	public void setHouseHoldCaseID(String houseHoldCaseID) {
		this.houseHoldCaseID = houseHoldCaseID;
	}

	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}

	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}

	public Date getBenefitEndDate() {
		return benefitEndDate;
	}

	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public Date getMemberCoverageStartDate() {
		return memberCoverageStartDate;
	}

	public void setMemberCoverageStartDate(Date memberCoverageStartDate) {
		this.memberCoverageStartDate = memberCoverageStartDate;
	}

	public Date getMemberCoverageEndDate() {
		return memberCoverageEndDate;
	}

	public void setMemberCoverageEndDate(Date memberCoverageEndDate) {
		this.memberCoverageEndDate = memberCoverageEndDate;
	}

	public String getEnrolleeStatus() {
		return enrolleeStatus;
	}

	public void setEnrolleeStatus(String enrolleeStatus) {
		this.enrolleeStatus = enrolleeStatus;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getCMSPlanID() {
		return CMSPlanID;
	}

	public void setCMSPlanID(String cMSPlanID) {
		CMSPlanID = cMSPlanID;
	}

	public String getCSRLevel() {
		return CSRLevel;
	}

	public void setCSRLevel(String cSRLevel) {
		CSRLevel = cSRLevel;
	}

	public List<EnrollmentPremiumDTO> getEnrollmentMonthlyPremium() {
		return enrollmentMonthlyPremium;
	}

	public void setEnrollmentMonthlyPremium(List<EnrollmentPremiumDTO> enrollmentMonthlyPremium) {
		this.enrollmentMonthlyPremium = enrollmentMonthlyPremium;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public Float getEhbPercent() {
		return ehbPercent;
	}

	public void setEhbPercent(Float ehbPercent) {
		this.ehbPercent = ehbPercent;
	}

	public Long getPriorSsapApplicationid() {
		return priorSsapApplicationid;
	}

	public void setPriorSsapApplicationid(Long priorSsapApplicationid) {
		this.priorSsapApplicationid = priorSsapApplicationid;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Float getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}

	public String getAllowChangePlan() {
		return allowChangePlan;
	}

	public void setAllowChangePlan(String allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}

	public Date getEnrollmentCreationDate() {
		return enrollmentCreationDate;
	}

	public void setEnrollmentCreationDate(Date enrollmentCreationDate) {
		this.enrollmentCreationDate = enrollmentCreationDate;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
}
