package com.getinsured.hix.dto.cap.consumerapp;

public class UpdateMemberDataRequest {
	
	private String householdId;
	
	private String oldDOB;
	
	private String newDOB;
	
	private String oldFirstName;
	
	private String newFirstName;
	
	private String oldLastName;
	
	private String newLastName;

	public String getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}

	public String getOldDOB() {
		return oldDOB;
	}

	public void setOldDOB(String oldDOB) {
		this.oldDOB = oldDOB;
	}

	public String getNewDOB() {
		return newDOB;
	}

	public void setNewDOB(String newDOB) {
		this.newDOB = newDOB;
	}

	public String getOldFirstName() {
		return oldFirstName;
	}

	public void setOldFirstName(String oldFirstName) {
		this.oldFirstName = oldFirstName;
	}

	public String getNewFirstName() {
		return newFirstName;
	}

	public void setNewFirstName(String newFirstName) {
		this.newFirstName = newFirstName;
	}

	public String getOldLastName() {
		return oldLastName;
	}

	public void setOldLastName(String oldLastName) {
		this.oldLastName = oldLastName;
	}

	public String getNewLastName() {
		return newLastName;
	}

	public void setNewLastName(String newLastName) {
		this.newLastName = newLastName;
	}

}
