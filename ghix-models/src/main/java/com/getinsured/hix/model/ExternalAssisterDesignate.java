package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.getinsured.timeshift.util.TSDate;

@Entity
@Table(name = "external_assister_designate")
public class ExternalAssisterDesignate implements Serializable, Cloneable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXTERNAL_ASSISTERDESIGNATE_SEQ")
	@SequenceGenerator(name = "EXTERNAL_ASSISTERDESIGNATE_SEQ", sequenceName = "EXTERNAL_ASSISTERDESIGNATE_SEQ", allocationSize = 1)
	@Column(name = "id")
	private int id;
	
	@Column(name = "external_assister_id")
	private int externalAssisterId;
	
	@Column(name = "cmr_household_id")
	private Integer cmrHouseHoldId;
	
	@Column(name = "active")
	private String active;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_timestamp", nullable = false)
	private Date created;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "last_update_timestamp", nullable = false)
	private Date updated;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getExternalAssisterId() {
		return externalAssisterId;
	}

	public void setExternalAssisterId(int externalAssisterId) {
		this.externalAssisterId = externalAssisterId;
	}

	public Integer getCmrHouseHoldId() {
		return cmrHouseHoldId;
	}

	public void setCmrHouseHoldId(Integer cmrHouseHoldId) {
		this.cmrHouseHoldId = cmrHouseHoldId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void PrePersist()
	{
		this.setCreated(new TSDate());
		this.setUpdated(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void PreUpdate()
	{
		this.setUpdated(new TSDate());
	}
	
}
