/**
 * 
 */
package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;

/**
 * HIX-70722 Enrollment EnrolleeRaceTenantDTO
 * @author negi_s
 * @since 06/07/2015
 *
 */
public class EnrolleeRaceTenantDTO implements Serializable{
	
	private String raceEthnicityCode; 
	private String raceEthnicityDesc;
	
	public String getRaceEthnicityCode() {
		return raceEthnicityCode;
	}
	public void setRaceEthnicityCode(String raceEthnicityCode) {
		this.raceEthnicityCode = raceEthnicityCode;
	}
	public String getRaceEthnicityDesc() {
		return raceEthnicityDesc;
	}
	public void setRaceEthnicityDesc(String raceEthnicityDesc) {
		this.raceEthnicityDesc = raceEthnicityDesc;
	}
	
	@Override
	public String toString() {
		return "EnrolleeRaceTenantDTO [raceEthnicityCode=" + raceEthnicityCode
				+ ", raceEthnicityDesc=" + raceEthnicityDesc + "]";
	}

}
