package com.getinsured.hix.cap.drx;

public class DrxProfileInfo{
	
	private String birthDate;
	private String firstName;
	private String lastName;
	private String WorkPhone;
	private String CellPhone;
	private String HomePhone;
	
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getWorkPhone() {
		return WorkPhone;
	}
	public void setWorkPhone(String workPhone) {
		WorkPhone = workPhone;
	}
	public String getCellPhone() {
		return CellPhone;
	}
	public void setCellPhone(String cellPhone) {
		CellPhone = cellPhone;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
}
