delete from role_permissions;
delete from permissions;
delete roles where name in ('ISSUER_ADMIN','PLAN_ADMIN','ASSISTER','ASSISTER_ADMIN','BROKER_ADMIN','EMPLOYER_ADMIN');

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ISSUER_ADMIN','/admin/manageissuer','ISSUER_ADMIN',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'PLAN_ADMIN','/individual/dashboard','PLAN_ADMIN',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ASSISTER','/individual/dashboard','ASSISTER',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'ASSISTER_ADMIN','/individual/dashboard','ASSISTER_ADMIN',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'BROKER_ADMIN','/admin/broker/manage/list','BROKER_ADMIN',SYSTIMESTAMP);

Insert into ROLES (ID,CREATED,DESCRIPTION,LANDING_PAGE,NAME,UPDATED) 
values (ROLES_SEQ.NEXTVAL,SYSTIMESTAMP,'EMPLOYER_ADMIN','/individual/dashboard','EMPLOYER_ADMIN',SYSTIMESTAMP);


Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('manage_broker'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Employer_Plan_Selection_wizard'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Selections'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Employer_Dashboard'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Dependent_Details'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Enrollment_Details '));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Delete_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Terminate_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Review_Employee_List'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Employee_List'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Search_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Waive_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Available_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Contribution_Selection'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Renew_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Terminate_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add_Payment_Method'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Payment_Methods'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Invoices'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Payments'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Make_Payment'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Participation_Report'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Payment_Report'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Account_Information'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Account_Information'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Eligibility_Results'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Authorized_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Authorized_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Appeals'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Complaints'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Terminate_Employer_Account'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add/Remove_Authorized_Employer_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Add_Employees'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Enroll_on_Behalf_of_Employee'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Cancel_Open_Enrollment'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Waive_Employee_Coverage'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Individual_Members'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_SHOP_Members'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Financial_Information'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Manage_Authorized_Representatives'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Qualified_Health_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Qualified_Dental_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Qualified_Vision_Plans'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Details'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Benefits'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Rates'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Provider_Network_ID'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Enrollment_Availability'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Certification_Status'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_History'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Edit_Plan_Details'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Upload_New_Benefits'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Upload_New_Rates'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Update_Provider_Network_ID'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Update_Enrollment_Availability'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('Update_Certification_Status'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Plan_Reports'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Issuer_Reports'));
Insert into PERMISSIONS (ID,NAME) values (PERMISSIONS_SEQ.NEXTVAL,UPPER('View_Broker_Reports'));
