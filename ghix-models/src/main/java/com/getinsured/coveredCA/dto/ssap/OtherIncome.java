
package com.getinsured.coveredCA.dto.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author gjape
 * 
 */
public class OtherIncome extends Income{
	
	@Expose
	private String typeOfOtherIncome;
	@Expose
	private Integer howManyWeeks;
	
	
	
	public String getTypeOfOtherIncome() {
		return typeOfOtherIncome;
	}

	public void setTypeOfOtherIncome(String typeOfOtherIncome) {
		this.typeOfOtherIncome = typeOfOtherIncome;
	}

	public Integer getHowManyWeeks() {
		return howManyWeeks;
	}

	public void setHowManyWeeks(Integer howManyWeeks) {
		this.howManyWeeks = howManyWeeks;
	}

	
	
	
	
	

   
}
