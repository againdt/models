package com.getinsured.hix.broker.enums;

import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class AEEBookOfBusinessEnums {
	private static String lastYear = Year.now().minus(1, ChronoUnit.YEARS).toString();
	private static String currentYear = Year.now().toString();
	private static String nextYear = Year.now().plus(1, ChronoUnit.YEARS).toString();

	public enum RECORD_TYPE {
		AGENT("Agent"), ASSISTER("Assister"), ENTITY("Entity");
		private String value;

		private RECORD_TYPE(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum DESIGNATION_STATUS {
		PENDING("Pending"), ACTIVE("Active"), INACTIVE("InActive");
		private String value;

		private DESIGNATION_STATUS(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum APPLICATION_TYPE_ID {
		FINANCIAL("Financial"), NONFINANCIAL("Non-Financial");
		private String value;

		private APPLICATION_TYPE_ID(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum APPLICATION_TYPE_CA {
		SUBSIDIZED("Subsidized"), UNSUBSIDIZED("Unsubsidized");
		private String value;

		private APPLICATION_TYPE_CA(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum CURRENT_STATUS_ID {
		NO_APPLICATION_FOUND("No Application Found"), INCOMPLETE_APPLICATION(
				"Incomplete Application"), APPLICATION_SUBMITTED("Application Submitted"), ELIGIBLE_FOR_SHOPPING(
						"Eligible for Shopping"), ENROLLED_IN_A_QUALIFIED_PLAN("Enrolled in a Qualified Plan"), CLOSED(
								"Closed"), CANCELED_APPLICATION(
										"Canceled Application"), NO_ACTIVE_ENROLLMENT("No Active Enrollment"),;
		private String value;

		private CURRENT_STATUS_ID(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum CURRENT_STATUS_CA {
		APPLICATION_NOT_STARTED("Application Not Started"), APPLICATION_IN_PROGRESS(
				"Application In Progress"), APPLICATION_WITHDRAWN(
						"Application Withdrawn"), CASE_INACTIVE("Case Inactive"), RENEWAL_OPT_OUT("Renewal Opt Out");
		private String value;

		private CURRENT_STATUS_CA(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum NEXT_STEPS_ID {
		SHOP_FOR_PLANS("Shop For Plans"), SUBMIT_THE_APPLICATION("Submit The Application"), REPORT_A_CHANGE(
				"Report A Change"), ENROLL_DURING_OEP("Enroll During OEP");
		private String value;

		private NEXT_STEPS_ID(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum NEXT_STEPS_CA {
		ROP_EXPIRING("ROP Expiring"), COMPLETE_REPORT_A_CHANGE("Complete Report a Change"), COMPLETE_PLAN_SELECTION(
				"Complete Plan Selection"), COMPLETE_RENEWAL("Complete Renewal");
		private String value;

		private NEXT_STEPS_CA(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum COVERAGE_YEAR_ID {
		LAST_YEAR(lastYear), CURRENT_YEAR(currentYear), NEXT_YEAR(nextYear);

		private String value;

		private COVERAGE_YEAR_ID(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	public enum COVERAGE_YEAR_CA {
		LAST_YEAR("Last Year"), CURRENT_YEAR("Current Year"), NEXT_YEAR("Next Year");

		private String value;

		private COVERAGE_YEAR_CA(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public static List<String> getEnumValues(String enumName) {
		List<String> list = new ArrayList<>();

		switch (enumName) {
		case "RECORD_TYPE":
			for (RECORD_TYPE key : RECORD_TYPE.values()) {
				list.add(key.getValue());
			}
			break;
		case "DESIGNATION_STATUS":
			for (DESIGNATION_STATUS key : DESIGNATION_STATUS.values()) {
				list.add(key.getValue());
			}
			break;
		case "APPLICATION_TYPE_ID":
			for (APPLICATION_TYPE_ID key : APPLICATION_TYPE_ID.values()) {
				list.add(key.getValue());
			}
			break;
		case "APPLICATION_TYPE_CA":
			for (APPLICATION_TYPE_CA key : APPLICATION_TYPE_CA.values()) {
				list.add(key.getValue());
			}
			break;
		case "CURRENT_STATUS_ID":
			for (CURRENT_STATUS_ID key : CURRENT_STATUS_ID.values()) {
				list.add(key.getValue());
			}
			break;
		case "CURRENT_STATUS_CA":
			for (CURRENT_STATUS_CA key : CURRENT_STATUS_CA.values()) {
				list.add(key.getValue());
			}
			break;
		case "NEXT_STEPS_ID":
			for (NEXT_STEPS_ID key : NEXT_STEPS_ID.values()) {
				list.add(key.getValue());
			}
			break;
		case "NEXT_STEPS_CA":
			for (NEXT_STEPS_CA key : NEXT_STEPS_CA.values()) {
				list.add(key.getValue());
			}
			break;
		case "COVERAGE_YEAR_ID":
			for (COVERAGE_YEAR_ID key : COVERAGE_YEAR_ID.values()) {
				list.add(key.getValue());
			}
			break;
		case "COVERAGE_YEAR_CA":
			for (COVERAGE_YEAR_CA key : COVERAGE_YEAR_CA.values()) {
				list.add(key.getValue());
			}
		}

		return list;
	}
	
	public enum EligibilityStatus {

		AE("Eligible", "Eligible"),IE("Ineligible", "Ineligible"),CE("Conditional Eligible", "Conditional Eligible");

		private String description;

		private String description2;

		private EligibilityStatus(String description, String description2){
			this.description = description;
			this.description2 = description2;
		}

		public String getDescription(){
			return description;
		}

		public String getDescription2() {
			return description2;
		}
	}
	
	public enum ApplicationStatus {
		
		WITHDRAWN("WITHDRAWN"),TERMINATED("TERMINATED"),
		COMPLETED("COMPLETED"), SUBMITTED("SUBMITTED"), IN_PROGRESS("IN PROGRESS"),
		RENEWAL("RENEWAL"), RAC_WITHDRAWN("RAC-WITHDRAWN"), CLOSED("CLOSED - NOT COMPLETED");

		private String	description;

		private ApplicationStatus(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}

	}

	public static void main(String[] adf) {
		;
		;
		
		//System.out.println(Year.now());
		//System.out.println(Year.now().minus(1, ChronoUnit.YEARS));
		//System.out.println(Year.now().plus(1, ChronoUnit.YEARS));
		// System.out.println(ApplicationStatus.values()[0].toString());
		System.out.println(getEnumValues("COVERAGE_YEAR_ID"));
		// System.out.println(AEEBookOfBusinessEnums.COVERAGE_YEAR.CURRENT_YEAR.getValue());
	}
}
