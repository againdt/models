package com.getinsured.hix.platform.util;

import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import oracle.sql.CLOB;

/**
 * This class is designed to be used with Oracle load java function. All the methods are intentionally made static and class declared as Abstract.
 * JSON processing related classes have been copied here from simple json 1.1.1 to ensure no external resolution is performed by Oracle 11g while
 * compiling the code which uses JDK 1.6 as of Oracle 11g R2
 *
 * You'll notice a lot of warnings for statements where we could have used generic statements, that's because I did not wan to fiddle with original
 * source code I copied here from simple json library
 *
 * @author chaudhary_a
 *
 */
public abstract class DBMaskerSingle {
    private static HashMap<String, Integer> keySet = new HashMap<String, Integer>();
    private static HashMap<Integer, String> keyMap = new HashMap<Integer, String>();
    private static HashMap<String, Integer> skipColumnSet = new HashMap<String,Integer>();
    private static final int EMAIL = 0;
    private static final int PLAIN = 1;
    private static final int SSN = 2;
    private static final int CC = 3;
    private static final int PASSWORD = 4;
    private static final int UUID = 5;
    private static final int PHONE = 6;
    private static final int LICENSE = 7;
    private static final int SEC_ANS = 8;
    private static final int DATE = 9;
    private static final int IP_ADDRESS=10;
    private static final int HOME_ADDRESS=11;
    
    private static final int RETURN_AS_IS = 0;
    private static final int RETURN_EMPTY = 1;
    private static List<SimpleDateFormat> formats = null;
    private static MessageDigest md = null;
    private static int[] days = {0,31,1,31,1,31,1,31,1,31,2,21,2,19,2,17,1,2,3,4,5,13,7,13,9,13,11,13,31,4,15,4};
    static {
        try {
            md = MessageDigest.getInstance("SHA-256");
            
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formats  = new ArrayList<SimpleDateFormat>();
        formats.add((new SimpleDateFormat("MM/dd/yyyy")));
        formats.add((new SimpleDateFormat("MM/dd/yy")));
        formats.add((new SimpleDateFormat("MM-dd-yyyy")));
        formats.add((new SimpleDateFormat("MM-dd-yy")));
        formats.add((new SimpleDateFormat("MMM dd, yyyy HH:mm:SS aa")));
        formats.add((new SimpleDateFormat("MMM dd, yyyy HH:mm:SS")));
        formats.add((new SimpleDateFormat("dd MMM yyyy")));
        formats.add((new SimpleDateFormat("dd-MMM-yyyy")));
        formats.add((new SimpleDateFormat("dd-MMM-yy")));
        formats.add((new SimpleDateFormat("dd-MMM-yy HH.mm.SS.sssssssss aa")));
        
        keyMap.put(EMAIL, "Email");
        keyMap.put(PLAIN, "Plain");
        keyMap.put(SSN, "SSN");
        keyMap.put(CC, "CC");
        keyMap.put(PASSWORD, "PASSWORD");
        keyMap.put(UUID, "UUID");
        keyMap.put(PHONE, "PHONE");
        keyMap.put(LICENSE, "LICENSE");
        keyMap.put(SEC_ANS, "SEC_ANS");
        keyMap.put(DATE, "DATE");
        keyMap.put(IP_ADDRESS, "IPADDRESS");
        keyMap.put(HOME_ADDRESS, "HOME_ADDRESS");
        
        /**
         * These columns will be a pass through if ever called upon
         */
        skipColumnSet.put("DATA".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("UI_DATA".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("CONFIGURATION".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("CONFIGURATIONS".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("EXCEPTION_STACK_TRACE".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("API_OUTPUT".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("MEMBER_DATA".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("EXCEPTION_MESSAGE".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("EXPRESSION".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("SERIALIZED_CONTEXT".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("UI_DATA".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("EXCEPTION".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("GROUP_DATA".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("SCHEMA".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("API_OUTPUT".toLowerCase(),RETURN_AS_IS);
        skipColumnSet.put("HUB_RESPONSE_PAYLOAD", RETURN_EMPTY);
        skipColumnSet.put("PAYLOAD".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("REQUEST_PAYLOAD".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("RESPONSE_PAYLOAD".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("EMAIL_BODY".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("DESCRIPTION_".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("DESCRIPTION".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("MSG_CLOB".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("COMMENT_TEXT".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("COMMENT".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("COMMENTS".toLowerCase(), RETURN_EMPTY);
        skipColumnSet.put("responsiblePersonHomeAddress1".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("SESSION_VAL".toLowerCase(),RETURN_EMPTY);
        skipColumnSet.put("ORG_DOC_NAME".toLowerCase(),RETURN_EMPTY);
        
        
        
        keySet.put("PASSWORD".toLowerCase(), PASSWORD);
        keySet.put("PWD_".toLowerCase(), PASSWORD);
        keySet.put("UUID".toLowerCase(), UUID);
        keySet.put("CARD_NUMBER".toLowerCase(),CC);
        
        
        keySet.put("LICENSE_NUMBER".toLowerCase(), LICENSE);
        
        keySet.put("SECURITY_ANSWER_1".toLowerCase(), SEC_ANS);
        keySet.put("SECURITY_ANSWER_2".toLowerCase(), SEC_ANS);
        keySet.put("SECURITY_ANSWER_3".toLowerCase(), SEC_ANS);
        
        /**
         * Phone number fields
         */
        keySet.put("CONTACT_PHONE_NUMBER".toLowerCase(), PHONE);
        keySet.put("TELEPHONE_NUMBER".toLowerCase(), PHONE);
        keySet.put("PRIMARY_PHONE_NO".toLowerCase(), PHONE);
        keySet.put("PRIMARY_PHONE_NUMBER".toLowerCase(), PHONE);
        keySet.put("SECONDARY_PHONE_NUMBER".toLowerCase(), PHONE);
        keySet.put("CONTACT_NUMBER".toLowerCase(), PHONE);
        keySet.put("BROKER_FAX".toLowerCase(), PHONE);
        keySet.put("BROKER_PHONE".toLowerCase(), PHONE);
        keySet.put("PHONE_NUMBER".toLowerCase(), PHONE);
        keySet.put("PHONENUMBER".toLowerCase(), PHONE);
        keySet.put("FAX_NUMBER".toLowerCase(), PHONE);
        keySet.put("ALTERNATE_PHONE_NUMBER".toLowerCase(), PHONE);
        keySet.put("PHONE".toLowerCase(), PHONE);
        keySet.put("PREF_PHONE".toLowerCase(), PHONE);
        keySet.put("HOME_PHONE".toLowerCase(), PHONE);
        keySet.put("MOBILE_PHONE".toLowerCase(), PHONE);
        keySet.put("PRI_CONTACT_PRIMARY_PHONE".toLowerCase(), PHONE);
        keySet.put("PRI_CONTACT_SECONDARY_PHONE".toLowerCase(), PHONE);
        keySet.put("FIN_CONTACT_FAX_NUMBER".toLowerCase(), PHONE);
        keySet.put("FIN_CONTACT_PRIMARY_PHONE".toLowerCase(), PHONE);
        keySet.put("houseHoldContactPrimaryPhone".toLowerCase(), PHONE);
        keySet.put("primaryPhone".toLowerCase(), PHONE);
        keySet.put("responsiblePersonPrimaryPhone".toLowerCase(), PHONE);
        keySet.put("phoneNumbers".toLowerCase(),PHONE);
        
        keySet.put("dateOfBirth".toLowerCase(), DATE);
        keySet.put("date_Of_Birth".toLowerCase(), DATE);
        keySet.put("BIRTH_DATE".toLowerCase(), DATE);
        keySet.put("BIRTH_DAY".toLowerCase(), DATE);
        keySet.put("DOB".toLowerCase(), DATE);
        
        /**
         * Email Fields
         */
        keySet.put("PRI_CONTACT_EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("SUBSCRIBER_EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("USERNAME".toLowerCase(), EMAIL);
        keySet.put("FIN_CONTACT_EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("PRIMARY_EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("BROKER_EMAIL".toLowerCase(), EMAIL);
        keySet.put("CC_EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("BCC_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("TO_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("FROM_EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("REPLY_TO".toLowerCase(), EMAIL);
        keySet.put("RET_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("EMAIL_ADDRESS".toLowerCase(), EMAIL);
        keySet.put("CONTACT_EMAIL".toLowerCase(), EMAIL);
        keySet.put("email".toLowerCase(), EMAIL);
        keySet.put("emailId".toLowerCase(), EMAIL);
        keySet.put("household.primary.email".toLowerCase(), EMAIL);
        keySet.put("emailAddress".toLowerCase(), EMAIL);
        keySet.put("ns4:ContactEmailID".toLowerCase(), EMAIL);
        keySet.put("PREF_EMAIL".toLowerCase(), EMAIL);
        keySet.put("POSTAL_MAIL".toLowerCase(), EMAIL);
        keySet.put("responsiblePersonPreferredEmail".toLowerCase(), EMAIL);
        keySet.put("houseHoldContactPreferredEmail".toLowerCase(), EMAIL);
        keySet.put("USER_IDENTIFIER".toLowerCase(), EMAIL);
        
        /**
         * Normal masking
         */
        keySet.put("Account_Execcutive".toLowerCase(), PLAIN);
        keySet.put("DOCUMENT_NAME".toLowerCase(), PLAIN);
        keySet.put("EE_ASSISSTERS".toLowerCase(), PLAIN);
        keySet.put("EE_DOCUMENTS".toLowerCase(), PLAIN);
        keySet.put("CREATED_BY_USERNAME".toLowerCase(), PLAIN);
        keySet.put("CONTACT_FIRST_NAME".toLowerCase(), PLAIN);
        keySet.put("MEDICAID_ID".toLowerCase(), PLAIN);
        keySet.put("PRIMARY_NAME".toLowerCase(), PLAIN);
        keySet.put("SSN_FIRST_NAME".toLowerCase(), PLAIN);
        keySet.put("CONTACT_LAST_NAME".toLowerCase(), PLAIN);
        keySet.put("PRODUCER_UNAME".toLowerCase(), PLAIN);
        keySet.put("BUSINESS_LEGAL_NAME".toLowerCase(), PLAIN);
        keySet.put("CONTACT_PERSON".toLowerCase(), PLAIN);
        keySet.put("PRIMARY_CONTACT".toLowerCase(), PLAIN);
        keySet.put("PRI_CONTACT_NAME".toLowerCase(), PLAIN);
        keySet.put("CERTIFICATION_NUMBER".toLowerCase(), PLAIN);
        keySet.put("DESCRIPTION".toLowerCase(), PLAIN);
        keySet.put("DESCRIPTION_".toLowerCase(), PLAIN);
        keySet.put("TEXT_".toLowerCase(), PLAIN);
        keySet.put("FEDERAL_EIN".toLowerCase(), PLAIN);
        keySet.put("SPONSOR_EIN".toLowerCase(), PLAIN);
        keySet.put("CUSTOMER_NAME".toLowerCase(), PLAIN);
        keySet.put("JSON_STRING".toLowerCase(), PLAIN);
        keySet.put("NAME".toLowerCase(), PLAIN);
        keySet.put("SPONSOR_NAME".toLowerCase(), PLAIN);
        keySet.put("CONTACT_LAST_NAME".toLowerCase(), PLAIN);
        keySet.put("ENTITY_NAME".toLowerCase(), PLAIN);
        keySet.put("firstName".toLowerCase(), PLAIN);
        keySet.put("MEDICAIDID".toLowerCase(),PLAIN);
        keySet.put("responsiblePersonFirstName".toLowerCase(), PLAIN);
        keySet.put("responsiblePersonLastName".toLowerCase(), PLAIN);
        keySet.put("houseHoldContactFirstName".toLowerCase(), PLAIN);
        keySet.put("houseHoldContactLastName".toLowerCase(), PLAIN);
        keySet.put("first_".toLowerCase(), PLAIN);
        keySet.put("middle_name".toLowerCase(), PLAIN);
        keySet.put("middle_".toLowerCase(), PLAIN);
        keySet.put("middlename".toLowerCase(), PLAIN);
        keySet.put("mname".toLowerCase(), PLAIN);
        keySet.put("first_name".toLowerCase(), PLAIN);
        keySet.put("fname".toLowerCase(), PLAIN);
        keySet.put("lastName".toLowerCase(), PLAIN);
        keySet.put("consumerName".toLowerCase(), PLAIN);
        keySet.put("last_name".toLowerCase(), PLAIN);
        keySet.put("last_".toLowerCase(), PLAIN);
        keySet.put("lname".toLowerCase(), PLAIN);
        keySet.put("Name.first".toLowerCase(), PLAIN);
        keySet.put("Name.last".toLowerCase(), PLAIN);
        keySet.put("LastName".toLowerCase(), PLAIN);
        keySet.put("fullName".toLowerCase(), PLAIN);
        keySet.put("NAME_ON_CARD".toLowerCase(),PLAIN);
        keySet.put("NAME_ON_ACCOUNT".toLowerCase(), PLAIN);
        keySet.put("ACCOUNT_NUMBER".toLowerCase(), PLAIN);
        keySet.put("ROUTING_NUMBER".toLowerCase(), PLAIN);
        keySet.put("CUSTOMER_NAME".toLowerCase(),PLAIN);
        keySet.put("ESIGN_BY".toLowerCase(), PLAIN);
        keySet.put("ESIGN_FIRST_NAME".toLowerCase(), PLAIN);
        keySet.put("ESIGN_LAST_NAME".toLowerCase(),PLAIN);
        keySet.put("BUSINESS_LEGAL_NAME".toLowerCase(), PLAIN);
        keySet.put("BROKER_TPA_ACCOUNT_NUMBER_1".toLowerCase(), PLAIN);
        keySet.put("BROKER_TPA_ACCOUNT_NUMBER_2".toLowerCase(), PLAIN);
        keySet.put("SUBSCRIBER_NAME".toLowerCase(), PLAIN);
        keySet.put("PRODUCER_PASSWORD".toLowerCase(), PLAIN);
        keySet.put("DSH_REFERENCE_NUMBER".toLowerCase(), PLAIN);
        keySet.put("ESIGNATURE".toLowerCase(), PLAIN);
        keySet.put("ESIGN".toLowerCase(), PLAIN);
        keySet.put("OWNER_USER_NAME".toLowerCase(), PLAIN);
        keySet.put("FROM_USER_NAME".toLowerCase(), PLAIN);
        keySet.put("homeAddress1".toLowerCase(), PLAIN);
        keySet.put("address1".toLowerCase(), PLAIN);
        
        /**
         * SSN Fields
         */
        keySet.put("ns2:TINIdentification".toLowerCase(), SSN);
        keySet.put("ssn".toLowerCase(), SSN);
        keySet.put("tax_id_number".toLowerCase(), SSN);
        keySet.put("household.primary.ssn".toLowerCase(), SSN);
        keySet.put("socialSecurityNumber".toLowerCase(), SSN);
        keySet.put("STATE_TAX_ID".toLowerCase(), SSN);
        keySet.put("FEDERAL_TAX_ID".toLowerCase(), SSN);
        keySet.put("SPONSOR_TAX_ID_NUMBER".toLowerCase(),SSN);
        keySet.put("INSURER_TAX_ID_NUMBER".toLowerCase(),SSN);
        keySet.put("BROKER_FED_TAX_PAYER_ID".toLowerCase(),SSN);
        keySet.put("responsiblePersonSsn".toLowerCase(),SSN);
        keySet.put("houseHoldContactFederalTaxIdNumber".toLowerCase(),SSN);
    }
    
    private static String getDate(String date){
    	Date dt = null;
    	String out = null;
    	int len = date.length();
    	for(SimpleDateFormat sdf: formats){
    		try{
    			if(sdf.toPattern().length() != len){
    				continue;
    			}
    			dt = sdf.parse(date);
    			Calendar cal = TSCalendar.getInstance();
                cal.setTime(dt);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int maskedDay = days[day];
                cal.set(Calendar.DAY_OF_MONTH,maskedDay);
                out = sdf.format(cal.getTime());
                break;
    		}catch(Exception e){
    			continue;
    		}
    	}
    	if(out != null){
    	return out;
    }
    	System.out.println("Date not able to mask:"+date);
    	return date;
    }
    
    private static JSONObject getJSON(String input) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(input);
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private static Document getXmlDocument(String input) {
        Document x = null;
        try {
            DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builder.newDocumentBuilder();
            x = docBuilder.parse(new ByteArrayInputStream(input.getBytes(Charset.defaultCharset())));
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return x;
    }
    
   
    
    public static String getSHA256(String input) {
        
        String err = null;
        byte[] hash  = null;
        int cursor = 0;
        if(input == null){
            return null;
        }
        if(input.length() == 0){
            return input;
        }
        try {
            synchronized (md) {
                md.update(input.toLowerCase().getBytes("UTF-8"));
                hash = md.digest();
            }
            return Base64.encodeBase64URLSafeString(hash);
        } catch (Exception e) {
            err = e.getMessage();
            if(err == null){
                err = "Exception: Hash Length:"+hash.length+": Cursor at:"+cursor+"Name:"+e.getClass().getSimpleName();
            }
        }
        return err;
    }
    
    public static String getSHA256(String dummy1, String dummy2, String input) {
        return getSHA256(input);
    }
    
    public static String maskSSN(String ssn) {
        if(ssn == null){
            return null;
        }else if(ssn.length() == 0){
            return ssn;
        }
        int hiphenIdx = ssn.indexOf("-");
        if (hiphenIdx != -1) {
            return "111-222-3333";
        } else {
            return "1112223333";
        }
    }
    
    public static String maskDate(String input) {
        if(input == null){
            return null;
        }else if(input.length() == 0){
            return input;
        }
        return getDate(input);
            
    }
    
    public static String maskEmail(String input) {
        if(input == null){
            return null;
        }else if(input.length() == 0){
            return input;
        }else if(input.endsWith("ghix.com")){
            return input;
        }
        int atIdx = input.indexOf('@');
        if(atIdx > 0){
            String domain = input.substring(atIdx + 1);
            return getSHA256(input) + '@' + domain+".test";
        }
        return getSHA256(input);
    }
    
    public static String maskCC(String input) {
        char startChar = input.trim().charAt(0);
        String cardInfo = "4111111111111111";
        switch (startChar) {
            case '4':
                cardInfo = "4111111111111111";
                break;
            case '3':
                cardInfo = "5555555555554444";
                break;
            case '5':
                cardInfo = "378282246310005";
                break;
            case '6':
                cardInfo = "6011111111111117";
                break;
            default:
                break;
        }
        return cardInfo;
    }
    
    public static Object maskPhone(Object input) {
    	if(input instanceof Long){
    		return 9999999999l;
    	}else if(input instanceof String){
    		return "999-999-9999";
    	}
    	return "999-999-9999";
    }
    
    public static String maskPassword(String input) {
        return "$2a$10$.SNF9LnSbESXh/EH5yjlPe8ePh7qyJJP2wvWsQ/Zf48KjXjS7TQRS";
    }
    
    public static String maskUUID(){
        return "2f398f71-5b6a-46ae-8032-e915391f141a";
    }
    
    
    public static Clob processField(String columnName, CLOB fieldVal){
        if(fieldVal == null){
            return null;
        }
        CLOB cdata = null;
        String data = null;
        Connection conn = null;
        String retVal = null;
        try{
            if(columnName != null && skipColumnSet.containsKey(columnName.toLowerCase())){
                return makeDefaultResponseForSkipColumn(skipColumnSet.get(columnName.toLowerCase()),fieldVal);
            }
            BufferedReader dataReader = new BufferedReader(fieldVal.getCharacterStream());
            StringBuilder sb = new StringBuilder(4096);
            String str = null;
            while((str = dataReader.readLine()) != null){
                sb.append(str);
            }
            //dataReader.close();
            sb.trimToSize();
            data = sb.toString();
            if(data.length() > 0){
                // Check if this is a JSON Object
                if (data.startsWith("{") && data.endsWith("}")) {
                    JSONObject obj = getJSON(data);
                    retVal= processJson(obj);
                }else if(data.startsWith("<") && data.endsWith(">")) {
                    Document xml = getXmlDocument(data);
                    retVal= processXml(xml);
                }else{
                    retVal= resolveValueFromKey(columnName, data, null)+"";
                }
            }
            conn = fieldVal.getJavaSqlConnection();
        }catch(Exception se){
            return null;
        }
        cdata = getOracleClob(conn, retVal);
        return cdata;
    }
    
    
    
    public static Clob processPldHouseHold(CLOB fieldVal){
        String startRoot = "<ROOT_MASK_PLD>".intern();
        String endRoot = "</ROOT_MASK_PLD>".intern();
        if(fieldVal == null){
            return null;
        }
        CLOB cdata = null;
        String data = null;
        Connection conn = null;
        String retVal = null;
        try{
        	conn = fieldVal.getJavaSqlConnection();
            BufferedReader dataReader = new BufferedReader(fieldVal.getCharacterStream());
            StringBuilder sb = new StringBuilder(4096);
            sb.append(startRoot);
            String str = null;
            while((str = dataReader.readLine()) != null){
                sb.append(str);
            }
            sb.append(endRoot);
            //dataReader.close();
            sb.trimToSize();
            data = sb.toString();
            if(data.length() > 0){
                Document xml = getXmlDocument(data);
                retVal= processXml(xml);
            }
            int idx = retVal.indexOf(startRoot);
            int endIdx = retVal.indexOf(endRoot);
            retVal = retVal.substring(idx+startRoot.length()+1, endIdx);
            cdata = getOracleClob(conn, retVal);
            return cdata;
        }catch(Exception se){
            return  getOracleClob(conn, "Error masking dropping the data");
        }
    }
    
    public static Clob processHtmlClob(CLOB fieldVal){
        
        if(fieldVal == null){
            return null;
        }
        CLOB cdata = null;
        Connection conn = null;
        String retVal = "<html>Data masked</html>";
        cdata = getOracleClob(conn, retVal);
        return cdata;
    }
    
    public static Clob processClobField(CLOB fieldVal){
        
        if(fieldVal == null){
            return null;
        }
        CLOB cdata = null;
        String data = null;
        Connection conn = null;
        String retVal = null;
        
        try{
        	conn = fieldVal.getJavaSqlConnection();
            BufferedReader dataReader = new BufferedReader(fieldVal.getCharacterStream());
            StringBuilder sb = new StringBuilder(4096);
            String str = null;
            while((str = dataReader.readLine()) != null){
                sb.append(str);
            }
            //dataReader.close();
            sb.trimToSize();
            data = sb.toString();
            if(data.length() > 0){
                // Check if this is a JSON Object
                if (data.startsWith("{") && data.endsWith("}")) {
                    JSONObject obj = getJSON(data);
                    retVal= processJson(obj);
                }else if(data.startsWith("<") && data.endsWith(">")) {
                    Document xml = getXmlDocument(data);
                    retVal= processXml(xml);
                }else{
                    retVal = "Masked, unknown format";
                }
            }
            cdata = getOracleClob(conn, retVal);
            return cdata;
        }catch(Exception se){
            return getOracleClob(conn, "Error masking, data dropped");
        }
    }
    
	public static String processClobField(Clob fieldVal){
	        
	        if(fieldVal == null){
	            return null;
	        }
	        String data = null;
	        String retVal = null;
	        
	        try{
	            BufferedReader dataReader = new BufferedReader(fieldVal.getCharacterStream());
	            StringBuilder sb = new StringBuilder(4096);
	            String str = null;
	            while((str = dataReader.readLine()) != null){
	                sb.append(str);
	            }
	            //dataReader.close();
	            sb.trimToSize();
	            data = sb.toString();
	            if(data.length() > 0){
	                // Check if this is a JSON Object
	                if (data.startsWith("{") && data.endsWith("}")) {
	                    JSONObject obj = getJSON(data);
	                    retVal= processJson(obj);
	                }else if(data.startsWith("<") && data.endsWith(">")) {
	                    Document xml = getXmlDocument(data);
	                    retVal= processXml(xml);
	                }else{
	                    retVal = "Masked, unknown format";
	                }
	            }
	          return retVal;
	        }catch(Exception se){
	           se.printStackTrace(); 
	        }
	        return null;
	    }
    
    private static Clob makeDefaultResponseForSkipColumn(Integer responseType, CLOB fieldVal) throws SQLException {
        switch(responseType){
            case RETURN_AS_IS:{
                return fieldVal;
            }
            case RETURN_EMPTY:{
                return null;
                /*String data = "";
                 Connection conn = fieldVal.getJavaSqlConnection();
                 return getOracleClob(conn, data);*/
            }
            default:{
                return fieldVal;
            }
        }
    }
    
    private static String makeVarcharResponseForSkipColumn(Integer responseType, String fieldVal)  {
        switch(responseType){
            case RETURN_AS_IS:{
                return fieldVal;
            }
            case RETURN_EMPTY:{
                return null;
                /*String data = "";
                 Connection conn = fieldVal.getJavaSqlConnection();
                 return getOracleClob(conn, data);*/
            }
            default:{
                return fieldVal;
            }
        }
    }
    
    private static Integer getNativeType(String type){
        if(type == null){
            return null;
        }
        Set<Entry<Integer, String>> entries = keyMap.entrySet();
        Iterator<Entry<Integer, String>> cursor = entries.iterator();
        String strEq = null;
        Entry<Integer, String> entry = null;
        while(cursor.hasNext()){
            entry = cursor.next();
            strEq = entry.getValue();
            if(strEq.equalsIgnoreCase(type)){
                return entry.getKey();
            }
        }
        return PLAIN;
    }
    public static String processFieldType(String columnName, String fieldVal, String type){
        if(fieldVal == null){
            return null;
        }
        if(columnName != null && skipColumnSet.containsKey(columnName.toLowerCase())){
            return makeVarcharResponseForSkipColumn(skipColumnSet.get(columnName.toLowerCase()),fieldVal);
        }
        String data = null;
        Integer columnType = getNativeType(type);
        try{
            if (fieldVal.startsWith("{") && fieldVal.endsWith("}")) {
                JSONObject obj = getJSON(fieldVal);
                data= processJson(obj);
            }else if(fieldVal.startsWith("<") && fieldVal.endsWith(">")) {
                Document xml = getXmlDocument(fieldVal);
                data= processXml(xml);
            }else{
                data= resolveValueFromKey(columnName, fieldVal, columnType)+"";
            }
        }catch(Exception se){
            return "Data masked "+se.getMessage();
        }
        return data;
    }
    
    public static String processSimpleField(String columnName, String fieldVal){
        if(fieldVal == null){
            return null;
        }
        if(columnName != null && skipColumnSet.containsKey(columnName.toLowerCase())){
            return makeVarcharResponseForSkipColumn(skipColumnSet.get(columnName.toLowerCase()),fieldVal);
        }
        String data = null;
        try{
            if (fieldVal.startsWith("{") && fieldVal.endsWith("}")) {
                JSONObject obj = getJSON(fieldVal);
                data= processJson(obj);
            }else if(fieldVal.startsWith("<") && fieldVal.endsWith(">")) {
                Document xml = getXmlDocument(fieldVal);
                data= processXml(xml);
            }else{
                data= resolveValueFromKey(columnName, fieldVal, null)+"";
            }
        }catch(Exception se){
            return "Data masking failed"+se.getMessage();
        }
        return data;
    }
    
    private static CLOB getOracleClob(Connection oracleConn, String data) {
        try{
            CLOB clob;
            Writer w = null;
            clob = CLOB.createTemporary(oracleConn, true, CLOB.DURATION_SESSION);
            if(data != null && data.length() > 0){
                w = clob.setCharacterStream(1l);
                w.write(data);
                w.flush();
            }
            return clob;
        }catch(Exception e){
            throw new RuntimeException("Error getting oracle clob:"+e.getMessage());
        }
    }
    
    private static String processXml(Document xml) {
        Node child = xml.getDocumentElement();
        processNode(child);
        return getStringFromDocument(xml);
        
    }
    
    private static void processNode(Node node) {
        NodeList nodeChilds = node.getChildNodes();
        int len = nodeChilds.getLength();
        for (int i = 0; i < len; i++) {
            Node child = nodeChilds.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE && child.getChildNodes().getLength() == 1) {
                String key = child.getNodeName();
                String currentVal=child.getFirstChild().getNodeValue();
                String newVal = resolveValueFromKey(key, currentVal, null)+"";
                child.getFirstChild().setNodeValue(newVal);
            } else {
                processNode(child);
            }
        }
    }
    
    private static class TestClob implements Clob{

    	private String testStr = null;
    	
    	private TestClob(String str){
    		testStr = str;
    	}
		@Override
		public long length() throws SQLException {
			return testStr.length();
		}

		@Override
		public String getSubString(long pos, int length) throws SQLException {
			return testStr.substring((int)pos, (int)((pos+length)));
		}

		@Override
		public Reader getCharacterStream() throws SQLException {
			return new BufferedReader(new InputStreamReader(new BufferedInputStream(new ByteArrayInputStream(this.testStr.getBytes()))));
		}

		@Override
		public InputStream getAsciiStream() throws SQLException {
			return new BufferedInputStream(new ByteArrayInputStream(this.testStr.getBytes()));
		}

		@Override
		public long position(String searchstr, long start) throws SQLException {
			String str = testStr.substring((int)start);
			int idx = str.indexOf(searchstr);
			long ret = 0l;
			return ret+idx;
		}

		@Override
		public long position(Clob searchstr, long start) throws SQLException {
			Reader reader = searchstr.getCharacterStream();
			char[] buf = new char[10*1024];
			try {
				int i = reader.read(buf, 0, 10*1024);
				String searchTmp = new String(buf,0,i);
				int idx = this.testStr.indexOf(searchTmp);
				return idx;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return -1;
			
		}

		@Override
		public int setString(long pos, String str) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int setString(long pos, String str, int offset, int len) throws SQLException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public OutputStream setAsciiStream(long pos) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Writer setCharacterStream(long pos) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void truncate(long len) throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void free() throws SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Reader getCharacterStream(long pos, long length) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
    	
    }
    
    private static Object resolveValueFromKey(String key, Object nodeValue, Integer appType) {
        if (key != null) {
            Integer type = keySet.get(key.toLowerCase());
            if(type == null){
                keySet.put(key, appType);
                type = appType;
            }
            if(type == null){
                return nodeValue;
            }
            switch (type) {
            	case DATE: {
            		return maskDate((String)nodeValue);
            	}
                case EMAIL: {
                    return maskEmail((String)nodeValue);
                }
                case PLAIN: {
                    return getSHA256((String)nodeValue);
                }
                case SSN: {
                    return maskSSN((String)nodeValue);
                }
                case PASSWORD:{
                    return maskPassword((String)nodeValue);
                }
                case UUID:{
                    return maskUUID();
                }
                case PHONE:{
                    return maskPhone(nodeValue);
                }case LICENSE:{
                    return "12-1234567";
                }case SEC_ANS:{
                    return "SEC_ANSWER";
                }case IP_ADDRESS:{
                    return "0.0.0.0";
                }
                    
            };
        }
        return nodeValue;
    }
    
    public static String getStringFromDocument(Document doc) {
        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        } catch (TransformerException ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
    }
    
    @SuppressWarnings("unchecked")
    private static String processJson(JSONObject obj) {
        Set<String> objKeySet = obj.keySet();
        Iterator<String> cursor = objKeySet.iterator();
        Object val = null;
        String key = null;
        while (cursor.hasNext()) {
            key = cursor.next();
            val = obj.get(key);
            if (val instanceof JSONObject) {
                //System.out.println("Child json object for key:"+key);
                processJson((JSONObject) val);
            } else if (val instanceof JSONArray) {
                JSONArray array = (JSONArray) val;
                processArray(array, key);
            } else if (val instanceof String || val instanceof Long || val instanceof Integer) {
                obj.put(key, resolveValueFromKey(key.toLowerCase(), val+"", null));
            }
        }
        return obj.toJSONString();
    }
    
    @SuppressWarnings("rawtypes")
    private static void processArray(JSONArray array, String key) {
        ListIterator iterator = array.listIterator();
        while (iterator.hasNext()) {
            Object temp = iterator.next();
            if (temp instanceof JSONArray) {
                processArray((JSONArray) temp, key);
            } else if (temp instanceof JSONObject) {
                processJson((JSONObject) temp);
            }else{
            	iterator.remove();
            	iterator.add((String)resolveValueFromKey(key, temp,null));
            }
        }
        
    }
    
    public static void showUsage() {
        System.out.println(
                           "java -jar DBMasker.jar -format <json/xml> -file <name of the file -column <name of the column> (optional)");
    }
    
    public static String getOption(String[] args, String key) {
        
        if (args == null || args.length == 0) {
            return null;
        }
        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-" + key)) {
                i++;
                return args[i];
            }
        }
        return null;
    }
    
    
    private static String getMembersDataElement(){
    	return "{\"members\":["
    			+ "{\"dateOfBirth\":\"08/01/1955\",\"dob\":\"02/21/1956\",\"memberNumber\":\"1\",\"isTobaccoUser\":\"N\",\"employerContributionAmount\":null,\"isSeekingCoverage\":\"Y\",\"isPregnant\":\"N\",\"memberEligibility\":\"APTC/CSR\",\"relationshipToPrimary\":\"SELF\",\"medicaidChild\":\"N\",\"gender\":null,\"memberId\":null,\"hraAmount\":null,\"isNativeAmerican\":\"N\"},"
    			+ "{\"dateOfBirth\":\"03/19/1963\",\"memberNumber\":\"2\",\"isTobaccoUser\":\"N\",\"employerContributionAmount\":null,\"isSeekingCoverage\":\"Y\",\"isPregnant\":\"N\",\"memberEligibility\":\"APTC/CSR\",\"relationshipToPrimary\":\"SPOUSE\",\"medicaidChild\":\"N\",\"gender\":null,\"memberId\":null,\"hraAmount\":null,\"isNativeAmerican\":\"N\"}"
    			+"]}";
    }
    
    
    static String readPayLoad() throws IOException{
    	File f = new File("payload.txt");
    	System.out.println(f.getAbsolutePath());
    	FileInputStream fin = new FileInputStream(f);
    	BufferedReader br = new BufferedReader(new InputStreamReader(fin));
    	StringBuilder sb = new StringBuilder();
    	String str = null;
    	while((str = br.readLine()) != null){
    		sb.append(str);
    	}
    	fin.close();
    	return sb.toString().trim();
    	
    }
    public static void main(String[] args) throws IOException, ParseException{
//        String json = "{\"name\": {\"middleName\": \"Walter\",\"lastName\": \"Chaudhary\",\"phoneNumbers\":[\"12345678\"],\"firstName\": \"null\",\"suffix\": null,\"phone\":{\"phoneNumber\":1234568976}}}";
//        Clob clob = new TestClob(readPayLoad());
//        System.out.println(processClobField(clob));0
    	System.out.println(maskDate("12-NOV-73"));
        
    }
}

class ItemList {
    private String sp = ",";
    @SuppressWarnings("rawtypes")	
    List items = new ArrayList();
    
    public ItemList() {
    }
    
    public ItemList(String s) {
        this.split(s, sp, items);
    }
    
    public ItemList(String s, String sp) {
        this.sp = s;
        this.split(s, sp, items);
    }
    
    public ItemList(String s, String sp, boolean isMultiToken) {
        split(s, sp, items, isMultiToken);
    }
    
    public List getItems() {
        return this.items;
    }
    
    public String[] getArray() {
        return (String[]) this.items.toArray();
    }
    
    public void split(String s, String sp, List append, boolean isMultiToken) {
        if (s == null || sp == null)
            return;
        if (isMultiToken) {
            StringTokenizer tokens = new StringTokenizer(s, sp);
            while (tokens.hasMoreTokens()) {
                append.add(tokens.nextToken().trim());
            }
        } else {
            this.split(s, sp, append);
        }
    }
    
    public void split(String s, String sp, List append) {
        if (s == null || sp == null)
            return;
        int pos = 0;
        int prevPos = 0;
        do {
            prevPos = pos;
            pos = s.indexOf(sp, pos);
            if (pos == -1)
                break;
            append.add(s.substring(prevPos, pos).trim());
            pos += sp.length();
        } while (pos != -1);
        append.add(s.substring(prevPos).trim());
    }
    
    public void setSP(String sp) {
        this.sp = sp;
    }
    
    public void add(int i, String item) {
        if (item == null)
            return;
        items.add(i, item.trim());
    }
    
    public void add(String item) {
        if (item == null)
            return;
        items.add(item.trim());
    }
    
    public void addAll(ItemList list) {
        items.addAll(list.items);
    }
    
    public void addAll(String s) {
        this.split(s, sp, items);
    }
    
    public void addAll(String s, String sp) {
        this.split(s, sp, items);
    }
    
    public void addAll(String s, String sp, boolean isMultiToken) {
        this.split(s, sp, items, isMultiToken);
    }
    
    /**
     * @param i
     *            0-based
     * @return
     */
    public String get(int i) {
        return (String) items.get(i);
    }
    
    public int size() {
        return items.size();
    }
    
    public String toString() {
        return toString(sp);
    }
    
    public String toString(String sp) {
        StringBuffer sb = new StringBuffer();
        
        for (int i = 0; i < items.size(); i++) {
            if (i == 0)
                sb.append(items.get(i));
            else {
                sb.append(sp);
                sb.append(items.get(i));
            }
        }
        return sb.toString();
        
    }
    
    public void clear() {
        items.clear();
    }
    
    public void reset() {
        sp = ",";
        items.clear();
    }
}

/**
 * A JSON array. JSONObject supports java.util.List interface.
 *
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
class JSONArray extends ArrayList implements JSONAware, JSONStreamAware {
    private static final long serialVersionUID = 3957988303675231981L;
    
    /**
     * Constructs an empty JSONArray.
     */
    public JSONArray() {
        super();
    }
    
    /**
     * Constructs a JSONArray containing the elements of the specified
     * collection, in the order they are returned by the collection's iterator.
     *
     * @param c
     *            the collection whose elements are to be placed into this
     *            JSONArray
     */
    public JSONArray(Collection c) {
        super(c);
    }
    
    /**
     * Encode a list into JSON text and write it to out. If this list is also a
     * JSONStreamAware or a JSONAware, JSONStreamAware and JSONAware specific
     * behaviours will be ignored at this top level.
     *
     * @see org.json.simple.JSONValue#writeJSONString(Object, Writer)
     *
     * @param collection
     * @param out
     */
    public static void writeJSONString(Collection collection, Writer out) throws IOException {
        if (collection == null) {
            out.write("null");
            return;
        }
        
        boolean first = true;
        Iterator iter = collection.iterator();
        
        out.write('[');
        while (iter.hasNext()) {
            if (first)
                first = false;
            else
                out.write(',');
            
            Object value = iter.next();
            if (value == null) {
                out.write("null");
                continue;
            }
            
            JSONValue.writeJSONString(value, out);
        }
        out.write(']');
    }
    
    public void writeJSONString(Writer out) throws IOException {
        writeJSONString(this, out);
    }
    
    /**
     * Convert a list to JSON text. The result is a JSON array. If this list is
     * also a JSONAware, JSONAware specific behaviours will be omitted at this
     * top level.
     *
     * @see org.json.simple.JSONValue#toJSONString(Object)
     *
     * @param collection
     * @return JSON text, or "null" if list is null.
     */
    public static String toJSONString(Collection collection) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(collection, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(byte[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(byte[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(short[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(short[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(int[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(int[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(long[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(long[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(float[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(float[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(double[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(double[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(boolean[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(boolean[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(char[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[\"");
            out.write(String.valueOf(array[0]));
            
            for (int i = 1; i < array.length; i++) {
                out.write("\",\"");
                out.write(String.valueOf(array[i]));
            }
            
            out.write("\"]");
        }
    }
    
    public static String toJSONString(char[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public static void writeJSONString(Object[] array, Writer out) throws IOException {
        if (array == null) {
            out.write("null");
        } else if (array.length == 0) {
            out.write("[]");
        } else {
            out.write("[");
            JSONValue.writeJSONString(array[0], out);
            
            for (int i = 1; i < array.length; i++) {
                out.write(",");
                JSONValue.writeJSONString(array[i], out);
            }
            
            out.write("]");
        }
    }
    
    public static String toJSONString(Object[] array) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public String toJSONString() {
        return toJSONString(this);
    }
    
    /**
     * Returns a string representation of this array. This is equivalent to
     * calling {@link JSONArray#toJSONString()}.
     */
    public String toString() {
        return toJSONString();
    }
}

interface JSONAware {
    /**
     * @return JSON text
     */
    String toJSONString();
}

/*
 * $Id: JSONObject.java,v 1.1 2006/04/15 14:10:48 platform Exp $ Created on
 * 2006-4-10
 */
class JSONObject extends HashMap implements Map, JSONAware, JSONStreamAware {
    
    private static final long serialVersionUID = -503443796854799292L;
    
    public JSONObject() {
        super();
    }
    
    /**
     * Allows creation of a JSONObject from a Map. After that, both the
     * generated JSONObject and the Map can be modified independently.
     *
     * @param map
     */
    public JSONObject(Map map) {
        super(map);
    }
    
    /**
     * Encode a map into JSON text and write it to out. If this map is also a
     * JSONAware or JSONStreamAware, JSONAware or JSONStreamAware specific
     * behaviours will be ignored at this top level.
     *
     * @see org.json.simple.JSONValue#writeJSONString(Object, Writer)
     *
     * @param map
     * @param out
     */
    public static void writeJSONString(Map map, Writer out) throws IOException {
        if (map == null) {
            out.write("null");
            return;
        }
        
        boolean first = true;
        Iterator iter = map.entrySet().iterator();
        
        out.write('{');
        while (iter.hasNext()) {
            if (first)
                first = false;
            else
                out.write(',');
            Map.Entry entry = (Map.Entry) iter.next();
            out.write('\"');
            out.write(escape(String.valueOf(entry.getKey())));
            out.write('\"');
            out.write(':');
            JSONValue.writeJSONString(entry.getValue(), out);
        }
        out.write('}');
    }
    
    public void writeJSONString(Writer out) throws IOException {
        writeJSONString(this, out);
    }
    
    /**
     * Convert a map to JSON text. The result is a JSON object. If this map is
     * also a JSONAware, JSONAware specific behaviours will be omitted at this
     * top level.
     *
     * @see org.json.simple.JSONValue#toJSONString(Object)
     *
     * @param map
     * @return JSON text, or "null" if map is null.
     */
    public static String toJSONString(Map map) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(map, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen with a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    public String toJSONString() {
        return toJSONString(this);
    }
    
    public String toString() {
        return toJSONString();
    }
    
    public static String toString(String key, Object value) {
        StringBuffer sb = new StringBuffer();
        sb.append('\"');
        if (key == null)
            sb.append("null");
        else
            JSONValue.escape(key, sb);
        sb.append('\"').append(':');
        
        sb.append(JSONValue.toJSONString(value));
        
        return sb.toString();
    }
    
    /**
     * Escape quotes, \, /, \r, \n, \b, \f, \t and other control characters
     * (U+0000 through U+001F). It's the same as JSONValue.escape() only for
     * compatibility here.
     *
     * @see org.json.simple.JSONValue#escape(String)
     *
     * @param s
     * @return
     */
    public static String escape(String s) {
        return JSONValue.escape(s);
    }
}

interface JSONStreamAware {
    /**
     * write JSON string to out.
     */
    void writeJSONString(Writer out) throws IOException;
}

/*
 * $Id: JSONValue.java,v 1.1 2006/04/15 14:37:04 platform Exp $ Created on
 * 2006-4-15
 */
class JSONValue {
    /**
     * Parse JSON text into java object from the input source. Please use
     * parseWithException() if you don't want to ignore the exception.
     *
     * @see org.json.simple.parser.JSONParser#parse(Reader)
     * @see #parseWithException(Reader)
     *
     * @param in
     * @return Instance of the following: org.json.simple.JSONObject,
     *         org.json.simple.JSONArray, java.lang.String, java.lang.Number,
     *         java.lang.Boolean, null
     *
     * @deprecated this method may throw an {@code Error} instead of returning
     *             {@code null}; please use
     *             {@link JSONValue#parseWithException(Reader)} instead
     */
    public static Object parse(Reader in) {
        try {
            JSONParser parser = new JSONParser();
            return parser.parse(in);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Parse JSON text into java object from the given string. Please use
     * parseWithException() if you don't want to ignore the exception.
     *
     * @see org.json.simple.parser.JSONParser#parse(Reader)
     * @see #parseWithException(Reader)
     *
     * @param s
     * @return Instance of the following: org.json.simple.JSONObject,
     *         org.json.simple.JSONArray, java.lang.String, java.lang.Number,
     *         java.lang.Boolean, null
     *
     * @deprecated this method may throw an {@code Error} instead of returning
     *             {@code null}; please use
     *             {@link JSONValue#parseWithException(String)} instead
     */
    public static Object parse(String s) {
        StringReader in = new StringReader(s);
        return parse(in);
    }
    
    /**
     * Parse JSON text into java object from the input source.
     *
     * @see org.json.simple.parser.JSONParser
     *
     * @param in
     * @return Instance of the following: org.json.simple.JSONObject,
     *         org.json.simple.JSONArray, java.lang.String, java.lang.Number,
     *         java.lang.Boolean, null
     *
     * @throws IOException
     * @throws ParseException
     */
    public static Object parseWithException(Reader in) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        return parser.parse(in);
    }
    
    public static Object parseWithException(String s) throws ParseException {
        JSONParser parser = new JSONParser();
        return parser.parse(s);
    }
    
    /**
     * Encode an object into JSON text and write it to out.
     * <p>
     * If this object is a Map or a List, and it's also a JSONStreamAware or a
     * JSONAware, JSONStreamAware or JSONAware will be considered firstly.
     * <p>
     * DO NOT call this method from writeJSONString(Writer) of a class that
     * implements both JSONStreamAware and (Map or List) with "this" as the
     * first parameter, use JSONObject.writeJSONString(Map, Writer) or
     * JSONArray.writeJSONString(List, Writer) instead.
     *
     * @see org.json.simple.JSONObject#writeJSONString(Map, Writer)
     * @see org.json.simple.JSONArray#writeJSONString(List, Writer)
     *
     * @param value
     * @param writer
     */
    public static void writeJSONString(Object value, Writer out) throws IOException {
        if (value == null) {
            out.write("null");
            return;
        }
        
        if (value instanceof String) {
            out.write('\"');
            out.write(escape((String) value));
            out.write('\"');
            return;
        }
        
        if (value instanceof Double) {
            if (((Double) value).isInfinite() || ((Double) value).isNaN())
                out.write("null");
            else
                out.write(value.toString());
            return;
        }
        
        if (value instanceof Float) {
            if (((Float) value).isInfinite() || ((Float) value).isNaN())
                out.write("null");
            else
                out.write(value.toString());
            return;
        }
        
        if (value instanceof Number) {
            out.write(value.toString());
            return;
        }
        
        if (value instanceof Boolean) {
            out.write(value.toString());
            return;
        }
        
        if ((value instanceof JSONStreamAware)) {
            ((JSONStreamAware) value).writeJSONString(out);
            return;
        }
        
        if ((value instanceof JSONAware)) {
            out.write(((JSONAware) value).toJSONString());
            return;
        }
        
        if (value instanceof Map) {
            JSONObject.writeJSONString((Map) value, out);
            return;
        }
        
        if (value instanceof Collection) {
            JSONArray.writeJSONString((Collection) value, out);
            return;
        }
        
        if (value instanceof byte[]) {
            JSONArray.writeJSONString((byte[]) value, out);
            return;
        }
        
        if (value instanceof short[]) {
            JSONArray.writeJSONString((short[]) value, out);
            return;
        }
        
        if (value instanceof int[]) {
            JSONArray.writeJSONString((int[]) value, out);
            return;
        }
        
        if (value instanceof long[]) {
            JSONArray.writeJSONString((long[]) value, out);
            return;
        }
        
        if (value instanceof float[]) {
            JSONArray.writeJSONString((float[]) value, out);
            return;
        }
        
        if (value instanceof double[]) {
            JSONArray.writeJSONString((double[]) value, out);
            return;
        }
        
        if (value instanceof boolean[]) {
            JSONArray.writeJSONString((boolean[]) value, out);
            return;
        }
        
        if (value instanceof char[]) {
            JSONArray.writeJSONString((char[]) value, out);
            return;
        }
        
        if (value instanceof Object[]) {
            JSONArray.writeJSONString((Object[]) value, out);
            return;
        }
        
        out.write(value.toString());
    }
    
    /**
     * Convert an object to JSON text.
     * <p>
     * If this object is a Map or a List, and it's also a JSONAware, JSONAware
     * will be considered firstly.
     * <p>
     * DO NOT call this method from toJSONString() of a class that implements
     * both JSONAware and Map or List with "this" as the parameter, use
     * JSONObject.toJSONString(Map) or JSONArray.toJSONString(List) instead.
     *
     * @see org.json.simple.JSONObject#toJSONString(Map)
     * @see org.json.simple.JSONArray#toJSONString(List)
     *
     * @param value
     * @return JSON text, or "null" if value is null or it's an NaN or an INF
     *         number.
     */
    public static String toJSONString(Object value) {
        final StringWriter writer = new StringWriter();
        
        try {
            writeJSONString(value, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Escape quotes, \, /, \r, \n, \b, \f, \t and other control characters
     * (U+0000 through U+001F).
     *
     * @param s
     * @return
     */
    public static String escape(String s) {
        if (s == null)
            return null;
        StringBuffer sb = new StringBuffer();
        escape(s, sb);
        return sb.toString();
    }
    
    /**
     * @param s
     *            - Must not be null.
     * @param sb
     */
    static void escape(String s, StringBuffer sb) {
        final int len = s.length();
        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);
            switch (ch) {
                case '"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                default:
                    // Reference: http://www.unicode.org/versions/Unicode5.1.0/
                    if ((ch >= '\u0000' && ch <= '\u001F') || (ch >= '\u007F' && ch <= '\u009F')
                        || (ch >= '\u2000' && ch <= '\u20FF')) {
                        String ss = Integer.toHexString(ch);
                        sb.append("\\u");
                        for (int k = 0; k < 4 - ss.length(); k++) {
                            sb.append('0');
                        }
                        sb.append(ss.toUpperCase());
                    } else {
                        sb.append(ch);
                    }
            }
        } // for
    }
    
}

/**
 * Container factory for creating containers for JSON object and JSON array.
 *
 * @see org.json.simple.parser.JSONParser#parse(java.io.Reader,
 *      ContainerFactory)
 *
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
interface ContainerFactory {
    /**
     * @return A Map instance to store JSON object, or null if you want to use
     *         org.json.simple.JSONObject.
     */
    Map createObjectContainer();
    
    /**
     * @return A List instance to store JSON array, or null if you want to use
     *         org.json.simple.JSONArray.
     */
    List creatArrayContainer();
}

/**
 * A simplified and stoppable SAX-like content handler for stream processing of
 * JSON text.
 *
 * @see org.xml.sax.ContentHandler
 * @see org.json.simple.parser.JSONParser#parse(java.io.Reader, ContentHandler,
 *      boolean)
 *
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
interface ContentHandler {
    /**
     * Receive notification of the beginning of JSON processing. The parser will
     * invoke this method only once.
     *
     * @throws ParseException
     *             - JSONParser will stop and throw the same exception to the
     *             caller when receiving this exception.
     */
    void startJSON() throws ParseException, IOException;
    
    /**
     * Receive notification of the end of JSON processing.
     *
     * @throws ParseException
     */
    void endJSON() throws ParseException, IOException;
    
    /**
     * Receive notification of the beginning of a JSON object.
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     *             - JSONParser will stop and throw the same exception to the
     *             caller when receiving this exception.
     * @see #endJSON
     */
    boolean startObject() throws ParseException, IOException;
    
    /**
     * Receive notification of the end of a JSON object.
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     *
     * @see #startObject
     */
    boolean endObject() throws ParseException, IOException;
    
    /**
     * Receive notification of the beginning of a JSON object entry.
     *
     * @param key
     *            - Key of a JSON object entry.
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     *
     * @see #endObjectEntry
     */
    boolean startObjectEntry(String key) throws ParseException, IOException;
    
    /**
     * Receive notification of the end of the value of previous object entry.
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     *
     * @see #startObjectEntry
     */
    boolean endObjectEntry() throws ParseException, IOException;
    
    /**
     * Receive notification of the beginning of a JSON array.
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     *
     * @see #endArray
     */
    boolean startArray() throws ParseException, IOException;
    
    /**
     * Receive notification of the end of a JSON array.
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     *
     * @see #startArray
     */
    boolean endArray() throws ParseException, IOException;
    
    /**
     * Receive notification of the JSON primitive values: java.lang.String,
     * java.lang.Number, java.lang.Boolean null
     *
     * @param value
     *            - Instance of the following: java.lang.String,
     *            java.lang.Number, java.lang.Boolean null
     *
     * @return false if the handler wants to stop parsing after return.
     * @throws ParseException
     */
    boolean primitive(Object value) throws ParseException, IOException;
    
}

/*
 * $Id: JSONParser.java,v 1.1 2006/04/15 14:10:48 platform Exp $ Created on
 * 2006-4-15
 */

/**
 * Parser for JSON text. Please note that JSONParser is NOT thread-safe.
 *
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
class JSONParser {
    public static final int S_INIT = 0;
    public static final int S_IN_FINISHED_VALUE = 1;// string,number,boolean,null,object,array
    public static final int S_IN_OBJECT = 2;
    public static final int S_IN_ARRAY = 3;
    public static final int S_PASSED_PAIR_KEY = 4;
    public static final int S_IN_PAIR_VALUE = 5;
    public static final int S_END = 6;
    public static final int S_IN_ERROR = -1;
    
    private LinkedList handlerStatusStack;
    private Yylex lexer = new Yylex((Reader) null);
    private Yytoken token = null;
    private int status = S_INIT;
    
    private int peekStatus(LinkedList statusStack) {
        if (statusStack.size() == 0)
            return -1;
        Integer status = (Integer) statusStack.getFirst();
        return status.intValue();
    }
    
    /**
     * Reset the parser to the initial state without resetting the underlying
     * reader.
     *
     */
    public void reset() {
        token = null;
        status = S_INIT;
        handlerStatusStack = null;
    }
    
    /**
     * Reset the parser to the initial state with a new character reader.
     *
     * @param in
     *            - The new character reader.
     * @throws IOException
     * @throws ParseException
     */
    public void reset(Reader in) {
        lexer.yyreset(in);
        reset();
    }
    
    /**
     * @return The position of the beginning of the current token.
     */
    public int getPosition() {
        return lexer.getPosition();
    }
    
    public Object parse(String s) throws ParseException {
        return parse(s, (ContainerFactory) null);
    }
    
    public Object parse(String s, ContainerFactory containerFactory) throws ParseException {
        StringReader in = new StringReader(s);
        try {
            return parse(in, containerFactory);
        } catch (IOException ie) {
            /*
             * Actually it will never happen.
             */
            throw new ParseException(-1, ParseException.ERROR_UNEXPECTED_EXCEPTION, ie);
        }
    }
    
    public Object parse(Reader in) throws IOException, ParseException {
        return parse(in, (ContainerFactory) null);
    }
    
    /**
     * Parse JSON text into java object from the input source.
     *
     * @param in
     * @param containerFactory
     *            - Use this factory to createyour own JSON object and JSON
     *            array containers.
     * @return Instance of the following: org.json.simple.JSONObject,
     *         org.json.simple.JSONArray, java.lang.String, java.lang.Number,
     *         java.lang.Boolean, null
     *
     * @throws IOException
     * @throws ParseException
     */
    public Object parse(Reader in, ContainerFactory containerFactory) throws IOException, ParseException {
        reset(in);
        LinkedList statusStack = new LinkedList();
        LinkedList valueStack = new LinkedList();
        
        try {
            do {
                nextToken();
                switch (status) {
                    case S_INIT:
                        switch (token.type) {
                            case Yytoken.TYPE_VALUE:
                                status = S_IN_FINISHED_VALUE;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(token.value);
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(createObjectContainer(containerFactory));
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(createArrayContainer(containerFactory));
                                break;
                            default:
                                status = S_IN_ERROR;
                        }// inner switch
                        break;
                        
                    case S_IN_FINISHED_VALUE:
                        if (token.type == Yytoken.TYPE_EOF)
                            return valueStack.removeFirst();
                        else
                            throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                        
                    case S_IN_OBJECT:
                        switch (token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                if (token.value instanceof String) {
                                    String key = (String) token.value;
                                    valueStack.addFirst(key);
                                    status = S_PASSED_PAIR_KEY;
                                    statusStack.addFirst(new Integer(status));
                                } else {
                                    status = S_IN_ERROR;
                                }
                                break;
                            case Yytoken.TYPE_RIGHT_BRACE:
                                if (valueStack.size() > 1) {
                                    statusStack.removeFirst();
                                    valueStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                break;
                            default:
                                status = S_IN_ERROR;
                                break;
                        }// inner switch
                        break;
                        
                    case S_PASSED_PAIR_KEY:
                        switch (token.type) {
                            case Yytoken.TYPE_COLON:
                                break;
                            case Yytoken.TYPE_VALUE:
                                statusStack.removeFirst();
                                String key = (String) valueStack.removeFirst();
                                Map parent = (Map) valueStack.getFirst();
                                parent.put(key, token.value);
                                status = peekStatus(statusStack);
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                statusStack.removeFirst();
                                key = (String) valueStack.removeFirst();
                                parent = (Map) valueStack.getFirst();
                                List newArray = createArrayContainer(containerFactory);
                                parent.put(key, newArray);
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newArray);
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                statusStack.removeFirst();
                                key = (String) valueStack.removeFirst();
                                parent = (Map) valueStack.getFirst();
                                Map newObject = createObjectContainer(containerFactory);
                                parent.put(key, newObject);
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newObject);
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        break;
                        
                    case S_IN_ARRAY:
                        switch (token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                List val = (List) valueStack.getFirst();
                                val.add(token.value);
                                break;
                            case Yytoken.TYPE_RIGHT_SQUARE:
                                if (valueStack.size() > 1) {
                                    statusStack.removeFirst();
                                    valueStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                val = (List) valueStack.getFirst();
                                Map newObject = createObjectContainer(containerFactory);
                                val.add(newObject);
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newObject);
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                val = (List) valueStack.getFirst();
                                List newArray = createArrayContainer(containerFactory);
                                val.add(newArray);
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newArray);
                                break;
                            default:
                                status = S_IN_ERROR;
                        }// inner switch
                        break;
                    case S_IN_ERROR:
                        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }// switch
                if (status == S_IN_ERROR) {
                    throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }
            } while (token.type != Yytoken.TYPE_EOF);
        } catch (IOException ie) {
            throw ie;
        }
        
        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
    }
    
    private void nextToken() throws ParseException, IOException {
        token = lexer.yylex();
        if (token == null)
            token = new Yytoken(Yytoken.TYPE_EOF, null);
    }
    
    private Map createObjectContainer(ContainerFactory containerFactory) {
        if (containerFactory == null)
            return new JSONObject();
        Map m = containerFactory.createObjectContainer();
        
        if (m == null)
            return new JSONObject();
        return m;
    }
    
    private List createArrayContainer(ContainerFactory containerFactory) {
        if (containerFactory == null)
            return new JSONArray();
        List l = containerFactory.creatArrayContainer();
        
        if (l == null)
            return new JSONArray();
        return l;
    }
    
    public void parse(String s, ContentHandler contentHandler) throws ParseException {
        parse(s, contentHandler, false);
    }
    
    public void parse(String s, ContentHandler contentHandler, boolean isResume) throws ParseException {
        StringReader in = new StringReader(s);
        try {
            parse(in, contentHandler, isResume);
        } catch (IOException ie) {
            /*
             * Actually it will never happen.
             */
            throw new ParseException(-1, ParseException.ERROR_UNEXPECTED_EXCEPTION, ie);
        }
    }
    
    public void parse(Reader in, ContentHandler contentHandler) throws IOException, ParseException {
        parse(in, contentHandler, false);
    }
    
    /**
     * Stream processing of JSON text.
     *
     * @see ContentHandler
     *
     * @param in
     * @param contentHandler
     * @param isResume
     *            - Indicates if it continues previous parsing operation. If set
     *            to true, resume parsing the old stream, and parameter 'in'
     *            will be ignored. If this method is called for the first time
     *            in this instance, isResume will be ignored.
     *
     * @throws IOException
     * @throws ParseException
     */
    public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException {
        if (!isResume) {
            reset(in);
            handlerStatusStack = new LinkedList();
        } else {
            if (handlerStatusStack == null) {
                isResume = false;
                reset(in);
                handlerStatusStack = new LinkedList();
            }
        }
        
        LinkedList statusStack = handlerStatusStack;
        
        try {
            do {
                switch (status) {
                    case S_INIT:
                        contentHandler.startJSON();
                        nextToken();
                        switch (token.type) {
                            case Yytoken.TYPE_VALUE:
                                status = S_IN_FINISHED_VALUE;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.primitive(token.value))
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startObject())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startArray())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                        }// inner switch
                        break;
                        
                    case S_IN_FINISHED_VALUE:
                        nextToken();
                        if (token.type == Yytoken.TYPE_EOF) {
                            contentHandler.endJSON();
                            status = S_END;
                            return;
                        } else {
                            status = S_IN_ERROR;
                            throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                        }
                        
                    case S_IN_OBJECT:
                        nextToken();
                        switch (token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                if (token.value instanceof String) {
                                    String key = (String) token.value;
                                    status = S_PASSED_PAIR_KEY;
                                    statusStack.addFirst(new Integer(status));
                                    if (!contentHandler.startObjectEntry(key))
                                        return;
                                } else {
                                    status = S_IN_ERROR;
                                }
                                break;
                            case Yytoken.TYPE_RIGHT_BRACE:
                                if (statusStack.size() > 1) {
                                    statusStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                if (!contentHandler.endObject())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                                break;
                        }// inner switch
                        break;
                        
                    case S_PASSED_PAIR_KEY:
                        nextToken();
                        switch (token.type) {
                            case Yytoken.TYPE_COLON:
                                break;
                            case Yytoken.TYPE_VALUE:
                                statusStack.removeFirst();
                                status = peekStatus(statusStack);
                                if (!contentHandler.primitive(token.value))
                                    return;
                                if (!contentHandler.endObjectEntry())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                statusStack.removeFirst();
                                statusStack.addFirst(new Integer(S_IN_PAIR_VALUE));
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startArray())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                statusStack.removeFirst();
                                statusStack.addFirst(new Integer(S_IN_PAIR_VALUE));
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startObject())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        break;
                        
                    case S_IN_PAIR_VALUE:
                        /*
                         * S_IN_PAIR_VALUE is just a marker to indicate the end of
                         * an object entry, it doesn't proccess any token, therefore
                         * delay consuming token until next round.
                         */
                        statusStack.removeFirst();
                        status = peekStatus(statusStack);
                        if (!contentHandler.endObjectEntry())
                            return;
                        break;
                        
                    case S_IN_ARRAY:
                        nextToken();
                        switch (token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                if (!contentHandler.primitive(token.value))
                                    return;
                                break;
                            case Yytoken.TYPE_RIGHT_SQUARE:
                                if (statusStack.size() > 1) {
                                    statusStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                if (!contentHandler.endArray())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startObject())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startArray())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                        }// inner switch
                        break;
                        
                    case S_END:
                        return;
                        
                    case S_IN_ERROR:
                        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }// switch
                if (status == S_IN_ERROR) {
                    throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }
            } while (token.type != Yytoken.TYPE_EOF);
        } catch (IOException ie) {
            status = S_IN_ERROR;
            throw ie;
        } catch (ParseException pe) {
            status = S_IN_ERROR;
            throw pe;
        } catch (RuntimeException re) {
            status = S_IN_ERROR;
            throw re;
        } catch (Error e) {
            status = S_IN_ERROR;
            throw e;
        }
        
        status = S_IN_ERROR;
        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
    }
}

class ParseException extends Exception {
    private static final long serialVersionUID = -7880698968187728547L;
    
    public static final int ERROR_UNEXPECTED_CHAR = 0;
    public static final int ERROR_UNEXPECTED_TOKEN = 1;
    public static final int ERROR_UNEXPECTED_EXCEPTION = 2;
    
    private int errorType;
    private Object unexpectedObject;
    private int position;
    
    public ParseException(int errorType) {
        this(-1, errorType, null);
    }
    
    public ParseException(int errorType, Object unexpectedObject) {
        this(-1, errorType, unexpectedObject);
    }
    
    public ParseException(int position, int errorType, Object unexpectedObject) {
        this.position = position;
        this.errorType = errorType;
        this.unexpectedObject = unexpectedObject;
    }
    
    public int getErrorType() {
        return errorType;
    }
    
    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }
    
    /**
     * @see org.json.simple.parser.JSONParser#getPosition()
     *
     * @return The character position (starting with 0) of the input where the
     *         error occurs.
     */
    public int getPosition() {
        return position;
    }
    
    public void setPosition(int position) {
        this.position = position;
    }
    
    /**
     * @see org.json.simple.parser.Yytoken
     *
     * @return One of the following base on the value of errorType:
     *         ERROR_UNEXPECTED_CHAR java.lang.Character ERROR_UNEXPECTED_TOKEN
     *         org.json.simple.parser.Yytoken ERROR_UNEXPECTED_EXCEPTION
     *         java.lang.Exception
     */
    public Object getUnexpectedObject() {
        return unexpectedObject;
    }
    
    public void setUnexpectedObject(Object unexpectedObject) {
        this.unexpectedObject = unexpectedObject;
    }
    
    public String getMessage() {
        StringBuffer sb = new StringBuffer();
        
        switch (errorType) {
            case ERROR_UNEXPECTED_CHAR:
                sb.append("Unexpected character (").append(unexpectedObject).append(") at position ").append(position)
                .append(".");
                break;
            case ERROR_UNEXPECTED_TOKEN:
                sb.append("Unexpected token ").append(unexpectedObject).append(" at position ").append(position)
                .append(".");
                break;
            case ERROR_UNEXPECTED_EXCEPTION:
                sb.append("Unexpected exception at position ").append(position).append(": ").append(unexpectedObject);
                break;
            default:
                sb.append("Unkown error at position ").append(position).append(".");
                break;
        }
        return sb.toString();
    }
}

class Yylex {
    
    /** This character denotes the end of file */
    public static final int YYEOF = -1;
    
    /** initial size of the lookahead buffer */
    private static final int ZZ_BUFFERSIZE = 16384;
    
    /** lexical states */
    public static final int YYINITIAL = 0;
    public static final int STRING_BEGIN = 2;
    
    /**
     * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
     * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l at the
     * beginning of a line l is of the form l = 2*k, k a non negative integer
     */
    private static final int ZZ_LEXSTATE[] = { 0, 0, 1, 1 };
    
    /**
     * Translates characters to character classes
     */
    private static final String ZZ_CMAP_PACKED = "\11\0\1\7\1\7\2\0\1\7\22\0\1\7\1\0\1\11\10\0"
    + "\1\6\1\31\1\2\1\4\1\12\12\3\1\32\6\0\4\1\1\5" + "\1\1\24\0\1\27\1\10\1\30\3\0\1\22\1\13\2\1\1\21"
    + "\1\14\5\0\1\23\1\0\1\15\3\0\1\16\1\24\1\17\1\20" + "\5\0\1\25\1\0\1\26\uff82\0";
    
    /**
     * Translates characters to character classes
     */
    private static final char[] ZZ_CMAP = zzUnpackCMap(ZZ_CMAP_PACKED);
    
    /**
     * Translates DFA states to action switch labels.
     */
    private static final int[] ZZ_ACTION = zzUnpackAction();
    
    private static final String ZZ_ACTION_PACKED_0 = "\2\0\2\1\1\2\1\3\1\4\3\1\1\5\1\6"
    + "\1\7\1\10\1\11\1\12\1\13\1\14\1\15\5\0" + "\1\14\1\16\1\17\1\20\1\21\1\22\1\23\1\24"
    + "\1\0\1\25\1\0\1\25\4\0\1\26\1\27\2\0" + "\1\30";
    
    private static int[] zzUnpackAction() {
        int[] result = new int[45];
        int offset = 0;
        offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
        return result;
    }
    
    private static int zzUnpackAction(String packed, int offset, int[] result) {
        int i = 0; /* index in packed string */
        int j = offset; /* index in unpacked array */
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            int value = packed.charAt(i++);
            do
                result[j++] = value;
            while (--count > 0);
        }
        return j;
    }
    
    /**
     * Translates a state to a row index in the transition table
     */
    private static final int[] ZZ_ROWMAP = zzUnpackRowMap();
    
    private static final String ZZ_ROWMAP_PACKED_0 = "\0\0\0\33\0\66\0\121\0\154\0\207\0\66\0\242"
    + "\0\275\0\330\0\66\0\66\0\66\0\66\0\66\0\66"
    + "\0\363\0\u010e\0\66\0\u0129\0\u0144\0\u015f\0\u017a\0\u0195" + "\0\66\0\66\0\66\0\66\0\66\0\66\0\66\0\66"
    + "\0\u01b0\0\u01cb\0\u01e6\0\u01e6\0\u0201\0\u021c\0\u0237\0\u0252" + "\0\66\0\66\0\u026d\0\u0288\0\66";
    
    private static int[] zzUnpackRowMap() {
        int[] result = new int[45];
        int offset = 0;
        offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
        return result;
    }
    
    private static int zzUnpackRowMap(String packed, int offset, int[] result) {
        int i = 0; /* index in packed string */
        int j = offset; /* index in unpacked array */
        int l = packed.length();
        while (i < l) {
            int high = packed.charAt(i++) << 16;
            result[j++] = high | packed.charAt(i++);
        }
        return j;
    }
    
    /**
     * The transition table of the DFA
     */
    private static final int ZZ_TRANS[] = { 2, 2, 3, 4, 2, 2, 2, 5, 2, 6, 2, 2, 7, 8, 2, 9, 2, 2, 2, 2, 2, 10, 11, 12,
    13, 14, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 18, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 4, 19, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 21, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, 16, 16, 16, 16, 16, 16, 16, 16, -1, -1, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, -1, -1, -1, -1, -1, -1, -1, -1, 24, 25, 26, 27, 28, 29, 30, 31, 32, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, 34, 35, -1, -1, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 38, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 39, -1, 39, -1, 39, -1, -1, -1, -1, -1, 39, 39, -1, -1, -1, -1, 39, 39, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 33, -1, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 38, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 40, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 41, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 42, -1, 42, -1, 42, -1, -1, -1, -1, -1, 42, 42, -1, -1, -1, -1, 42, 42, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 43, -1, 43, -1, 43, -1, -1, -1, -1, -1, 43, 43, -1, -1, -1, -1, 43, 43, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, 44, -1, 44, -1, 44, -1, -1, -1, -1, -1, 44, 44, -1, -1, -1, -1, 44, 44, -1, -1, -1, -1, -1,
    -1, -1, -1, };
    
    /* error codes */
    private static final int ZZ_UNKNOWN_ERROR = 0;
    private static final int ZZ_NO_MATCH = 1;
    private static final int ZZ_PUSHBACK_2BIG = 2;
    
    /* error messages for the codes above */
    private static final String ZZ_ERROR_MSG[] = { "Unkown internal scanner error", "Error: could not match input",
    "Error: pushback value was too large" };
    
    /**
     * ZZ_ATTRIBUTE[aState] contains the attributes of state <code>aState</code>
     */
    private static final int[] ZZ_ATTRIBUTE = zzUnpackAttribute();
    
    private static final String ZZ_ATTRIBUTE_PACKED_0 = "\2\0\1\11\3\1\1\11\3\1\6\11\2\1\1\11"
    + "\5\0\10\11\1\0\1\1\1\0\1\1\4\0\2\11" + "\2\0\1\11";
    
    private static int[] zzUnpackAttribute() {
        int[] result = new int[45];
        int offset = 0;
        offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
        return result;
    }
    
    private static int zzUnpackAttribute(String packed, int offset, int[] result) {
        int i = 0; /* index in packed string */
        int j = offset; /* index in unpacked array */
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            int value = packed.charAt(i++);
            do
                result[j++] = value;
            while (--count > 0);
        }
        return j;
    }
    
    /** the input device */
    private java.io.Reader zzReader;
    
    /** the current state of the DFA */
    private int zzState;
    
    /** the current lexical state */
    private int zzLexicalState = YYINITIAL;
    
    /**
     * this buffer contains the current text to be matched and is the source of
     * the yytext() string
     */
    private char zzBuffer[] = new char[ZZ_BUFFERSIZE];
    
    /** the textposition at the last accepting state */
    private int zzMarkedPos;
    
    /** the current text position in the buffer */
    private int zzCurrentPos;
    
    /** startRead marks the beginning of the yytext() string in the buffer */
    private int zzStartRead;
    
    /**
     * endRead marks the last character in the buffer, that has been read from
     * input
     */
    private int zzEndRead;
    
    /** number of newlines encountered up to the start of the matched text */
    private int yyline;
    
    /** the number of characters up to the start of the matched text */
    private int yychar;
    
    /**
     * the number of characters from the last newline up to the start of the
     * matched text
     */
    private int yycolumn;
    
    /**
     * zzAtBOL == true <=> the scanner is currently at the beginning of a line
     */
    private boolean zzAtBOL = true;
    
    /** zzAtEOF == true <=> the scanner is at the EOF */
    private boolean zzAtEOF;
    
    /* user code: */
    private StringBuffer sb = new StringBuffer();
    
    int getPosition() {
        return yychar;
    }
    
    /**
     * Creates a new scanner There is also a java.io.InputStream version of this
     * constructor.
     *
     * @param in
     *            the java.io.Reader to read input from.
     */
    Yylex(java.io.Reader in) {
        this.zzReader = in;
    }
    
    /**
     * Creates a new scanner. There is also java.io.Reader version of this
     * constructor.
     *
     * @param in
     *            the java.io.Inputstream to read input from.
     */
    Yylex(java.io.InputStream in) {
        this(new java.io.InputStreamReader(in));
    }
    
    /**
     * Unpacks the compressed character translation table.
     *
     * @param packed
     *            the packed character translation table
     * @return the unpacked character translation table
     */
    private static char[] zzUnpackCMap(String packed) {
        char[] map = new char[0x10000];
        int i = 0; /* index in packed string */
        int j = 0; /* index in unpacked array */
        while (i < 90) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do
                map[j++] = value;
            while (--count > 0);
        }
        return map;
    }
    
    /**
     * Refills the input buffer.
     *
     * @return <code>false</code>, iff there was new input.
     *
     * @exception java.io.IOException
     *                if any I/O-Error occurs
     */
    private boolean zzRefill() throws java.io.IOException {
        
        /* first: make room (if you can) */
        if (zzStartRead > 0) {
            System.arraycopy(zzBuffer, zzStartRead, zzBuffer, 0, zzEndRead - zzStartRead);
            
            /* translate stored positions */
            zzEndRead -= zzStartRead;
            zzCurrentPos -= zzStartRead;
            zzMarkedPos -= zzStartRead;
            zzStartRead = 0;
        }
        
        /* is the buffer big enough? */
        if (zzCurrentPos >= zzBuffer.length) {
            /* if not: blow it up */
            char newBuffer[] = new char[zzCurrentPos * 2];
            System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
            zzBuffer = newBuffer;
        }
        
        /* finally: fill the buffer with new input */
        int numRead = zzReader.read(zzBuffer, zzEndRead, zzBuffer.length - zzEndRead);
        
        if (numRead > 0) {
            zzEndRead += numRead;
            return false;
        }
        // unlikely but not impossible: read 0 characters, but not at end of
        // stream
        if (numRead == 0) {
            int c = zzReader.read();
            if (c == -1) {
                return true;
            } else {
                zzBuffer[zzEndRead++] = (char) c;
                return false;
            }
        }
        
        // numRead < 0
        return true;
    }
    
    /**
     * Closes the input stream.
     */
    public final void yyclose() throws java.io.IOException {
        zzAtEOF = true; /* indicate end of file */
        zzEndRead = zzStartRead; /* invalidate buffer */
        
        if (zzReader != null)
            zzReader.close();
    }
    
    /**
     * Resets the scanner to read from a new input stream. Does not close the
     * old reader.
     *
     * All internal variables are reset, the old input stream <b>cannot</b> be
     * reused (internal buffer is discarded and lost). Lexical state is set to
     * <tt>ZZ_INITIAL</tt>.
     *
     * @param reader
     *            the new input stream
     */
    public final void yyreset(java.io.Reader reader) {
        zzReader = reader;
        zzAtBOL = true;
        zzAtEOF = false;
        zzEndRead = zzStartRead = 0;
        zzCurrentPos = zzMarkedPos = 0;
        yyline = yychar = yycolumn = 0;
        zzLexicalState = YYINITIAL;
    }
    
    /**
     * Returns the current lexical state.
     */
    public final int yystate() {
        return zzLexicalState;
    }
    
    /**
     * Enters a new lexical state
     *
     * @param newState
     *            the new lexical state
     */
    public final void yybegin(int newState) {
        zzLexicalState = newState;
    }
    
    /**
     * Returns the text matched by the current regular expression.
     */
    public final String yytext() {
        return new String(zzBuffer, zzStartRead, zzMarkedPos - zzStartRead);
    }
    
    /**
     * Returns the character at position <tt>pos</tt> from the matched text.
     *
     * It is equivalent to yytext().charAt(pos), but faster
     *
     * @param pos
     *            the position of the character to fetch. A value from 0 to
     *            yylength()-1.
     *
     * @return the character at position pos
     */
    public final char yycharat(int pos) {
        return zzBuffer[zzStartRead + pos];
    }
    
    /**
     * Returns the length of the matched text region.
     */
    public final int yylength() {
        return zzMarkedPos - zzStartRead;
    }
    
    /**
     * Reports an error that occured while scanning.
     *
     * In a wellformed scanner (no or only correct usage of yypushback(int) and
     * a match-all fallback rule) this method will only be called with things
     * that "Can't Possibly Happen". If this method is called, something is
     * seriously wrong (e.g. a JFlex bug producing a faulty scanner etc.).
     *
     * Usual syntax/scanner level error handling should be done in error
     * fallback rules.
     *
     * @param errorCode
     *            the code of the errormessage to display
     */
    private void zzScanError(int errorCode) {
        String message;
        try {
            message = ZZ_ERROR_MSG[errorCode];
        } catch (ArrayIndexOutOfBoundsException e) {
            message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
        }
        
        throw new Error(message);
    }
    
    /**
     * Pushes the specified amount of characters back into the input stream.
     *
     * They will be read again by then next call of the scanning method
     *
     * @param number
     *            the number of characters to be read again. This number must
     *            not be greater than yylength()!
     */
    public void yypushback(int number) {
        if (number > yylength())
            zzScanError(ZZ_PUSHBACK_2BIG);
        
        zzMarkedPos -= number;
    }
    
    /**
     * Resumes scanning until the next regular expression is matched, the end of
     * input is encountered or an I/O-Error occurs.
     *
     * @return the next token
     * @exception java.io.IOException
     *                if any I/O-Error occurs
     */
    public Yytoken yylex() throws java.io.IOException, ParseException {
        int zzInput;
        int zzAction;
        
        // cached fields:
        int zzCurrentPosL;
        int zzMarkedPosL;
        int zzEndReadL = zzEndRead;
        char[] zzBufferL = zzBuffer;
        char[] zzCMapL = ZZ_CMAP;
        
        int[] zzTransL = ZZ_TRANS;
        int[] zzRowMapL = ZZ_ROWMAP;
        int[] zzAttrL = ZZ_ATTRIBUTE;
        
        while (true) {
            zzMarkedPosL = zzMarkedPos;
            
            yychar += zzMarkedPosL - zzStartRead;
            
            zzAction = -1;
            
            zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;
            
            zzState = ZZ_LEXSTATE[zzLexicalState];
            
        zzForAction: {
            while (true) {
                
                if (zzCurrentPosL < zzEndReadL)
                    zzInput = zzBufferL[zzCurrentPosL++];
                else if (zzAtEOF) {
                    zzInput = YYEOF;
                    break zzForAction;
                } else {
                    // store back cached positions
                    zzCurrentPos = zzCurrentPosL;
                    zzMarkedPos = zzMarkedPosL;
                    boolean eof = zzRefill();
                    // get translated positions and possibly new buffer
                    zzCurrentPosL = zzCurrentPos;
                    zzMarkedPosL = zzMarkedPos;
                    zzBufferL = zzBuffer;
                    zzEndReadL = zzEndRead;
                    if (eof) {
                        zzInput = YYEOF;
                        break zzForAction;
                    } else {
                        zzInput = zzBufferL[zzCurrentPosL++];
                    }
                }
                int zzNext = zzTransL[zzRowMapL[zzState] + zzCMapL[zzInput]];
                if (zzNext == -1)
                    break zzForAction;
                zzState = zzNext;
                
                int zzAttributes = zzAttrL[zzState];
                if ((zzAttributes & 1) == 1) {
                    zzAction = zzState;
                    zzMarkedPosL = zzCurrentPosL;
                    if ((zzAttributes & 8) == 8)
                        break zzForAction;
                }
                
            }
        }
            
            // store back cached position
            zzMarkedPos = zzMarkedPosL;
            
            switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
                case 11: {
                    sb.append(yytext());
                }
                case 25:
                    break;
                case 4: {
                    sb = null;
                    sb = new StringBuffer();
                    yybegin(STRING_BEGIN);
                }
                case 26:
                    break;
                case 16: {
                    sb.append('\b');
                }
                case 27:
                    break;
                case 6: {
                    return new Yytoken(Yytoken.TYPE_RIGHT_BRACE, null);
                }
                case 28:
                    break;
                case 23: {
                    Boolean val = Boolean.valueOf(yytext());
                    return new Yytoken(Yytoken.TYPE_VALUE, val);
                }
                case 29:
                    break;
                case 22: {
                    return new Yytoken(Yytoken.TYPE_VALUE, null);
                }
                case 30:
                    break;
                case 13: {
                    yybegin(YYINITIAL);
                    return new Yytoken(Yytoken.TYPE_VALUE, sb.toString());
                }
                case 31:
                    break;
                case 12: {
                    sb.append('\\');
                }
                case 32:
                    break;
                case 21: {
                    Double val = Double.valueOf(yytext());
                    return new Yytoken(Yytoken.TYPE_VALUE, val);
                }
                case 33:
                    break;
                case 1: {
                    throw new ParseException(yychar, ParseException.ERROR_UNEXPECTED_CHAR, new Character(yycharat(0)));
                }
                case 34:
                    break;
                case 8: {
                    return new Yytoken(Yytoken.TYPE_RIGHT_SQUARE, null);
                }
                case 35:
                    break;
                case 19: {
                    sb.append('\r');
                }
                case 36:
                    break;
                case 15: {
                    sb.append('/');
                }
                case 37:
                    break;
                case 10: {
                    return new Yytoken(Yytoken.TYPE_COLON, null);
                }
                case 38:
                    break;
                case 14: {
                    sb.append('"');
                }
                case 39:
                    break;
                case 5: {
                    return new Yytoken(Yytoken.TYPE_LEFT_BRACE, null);
                }
                case 40:
                    break;
                case 17: {
                    sb.append('\f');
                }
                case 41:
                    break;
                case 24: {
                    try {
                        int ch = Integer.parseInt(yytext().substring(2), 16);
                        sb.append((char) ch);
                    } catch (Exception e) {
                        throw new ParseException(yychar, ParseException.ERROR_UNEXPECTED_EXCEPTION, e);
                    }
                }
                case 42:
                    break;
                case 20: {
                    sb.append('\t');
                }
                case 43:
                    break;
                case 7: {
                    return new Yytoken(Yytoken.TYPE_LEFT_SQUARE, null);
                }
                case 44:
                    break;
                case 2: {
                    Long val = Long.valueOf(yytext());
                    return new Yytoken(Yytoken.TYPE_VALUE, val);
                }
                case 45:
                    break;
                case 18: {
                    sb.append('\n');
                }
                case 46:
                    break;
                case 9: {
                    return new Yytoken(Yytoken.TYPE_COMMA, null);
                }
                case 47:
                    break;
                case 3: {
                }
                case 48:
                    break;
                default:
                    if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
                        zzAtEOF = true;
                        return null;
                    } else {
                        zzScanError(ZZ_NO_MATCH);
                    }
            }
        }
    }
    
}

class Yytoken {
    public static final int TYPE_VALUE = 0;// JSON primitive value:
    // string,number,boolean,null
    public static final int TYPE_LEFT_BRACE = 1;
    public static final int TYPE_RIGHT_BRACE = 2;
    public static final int TYPE_LEFT_SQUARE = 3;
    public static final int TYPE_RIGHT_SQUARE = 4;
    public static final int TYPE_COMMA = 5;
    public static final int TYPE_COLON = 6;
    public static final int TYPE_EOF = -1;// end of file
    
    public int type = 0;
    public Object value = null;
    
    public Yytoken(int type, Object value) {
        this.type = type;
        this.value = value;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        switch (type) {
            case TYPE_VALUE:
                sb.append("VALUE(").append(value).append(")");
                break;
            case TYPE_LEFT_BRACE:
                sb.append("LEFT BRACE({)");
                break;
            case TYPE_RIGHT_BRACE:
                sb.append("RIGHT BRACE(})");
                break;
            case TYPE_LEFT_SQUARE:
                sb.append("LEFT SQUARE([)");
                break;
            case TYPE_RIGHT_SQUARE:
                sb.append("RIGHT SQUARE(])");
                break;
            case TYPE_COMMA:
                sb.append("COMMA(,)");
                break;
            case TYPE_COLON:
                sb.append("COLON(:)");
                break;
            case TYPE_EOF:
                sb.append("END OF FILE");
                break;
        }
        return sb.toString();
    }
}

interface Encoder {
    
    Object encode(Object source) throws Exception;
}

interface Decoder {
    
    Object decode(Object source) throws Exception;
}



interface BinaryEncoder extends Encoder {
    
    byte[] encode(byte[] source) throws Exception;
}

interface BinaryDecoder extends Decoder {
    
    byte[] decode(byte[] source) throws Exception;
}


/**
 * Abstract superclass for Base-N encoders and decoders.
 *
 * <p>
 * This class is thread-safe.
 * </p>
 *
 * @version $Id: BaseNCodec.java 1634404 2014-10-26 23:06:10Z ggregory $
 */
abstract class BaseNCodec implements BinaryEncoder, BinaryDecoder {
    
    /**
     * Holds thread context so classes can be thread-safe.
     *
     * This class is not itself thread-safe; each thread must allocate its own copy.
     *
     * @since 1.7
     */
    static class Context {
        
        /**
         * Place holder for the bytes we're dealing with for our based logic.
         * Bitwise operations store and extract the encoding or decoding from this variable.
         */
        int ibitWorkArea;
        
        /**
         * Place holder for the bytes we're dealing with for our based logic.
         * Bitwise operations store and extract the encoding or decoding from this variable.
         */
        long lbitWorkArea;
        
        /**
         * Buffer for streaming.
         */
        byte[] buffer;
        
        /**
         * Position where next character should be written in the buffer.
         */
        int pos;
        
        /**
         * Position where next character should be read from the buffer.
         */
        int readPos;
        
        /**
         * Boolean flag to indicate the EOF has been reached. Once EOF has been reached, this object becomes useless,
         * and must be thrown away.
         */
        boolean eof;
        
        /**
         * Variable tracks how many characters have been written to the current line. Only used when encoding. We use
         * it to make sure each encoded line never goes beyond lineLength (if lineLength &gt; 0).
         */
        int currentLinePos;
        
        /**
         * Writes to the buffer only occur after every 3/5 reads when encoding, and every 4/8 reads when decoding. This
         * variable helps track that.
         */
        int modulus;
        
        Context() {
        }
        
        /**
         * Returns a String useful for debugging (especially within a debugger.)
         *
         * @return a String useful for debugging.
         */
        @SuppressWarnings("boxing") // OK to ignore boxing here
        @Override
        public String toString() {
            return String.format("%s[buffer=%s, currentLinePos=%s, eof=%s, ibitWorkArea=%s, lbitWorkArea=%s, " +
                                 "modulus=%s, pos=%s, readPos=%s]", this.getClass().getSimpleName(), Arrays.toString(buffer),
                                 currentLinePos, eof, ibitWorkArea, lbitWorkArea, modulus, pos, readPos);
        }
    }
    
    /**
     * EOF
     *
     * @since 1.7
     */
    static final int EOF = -1;
    
    /**
     *  MIME chunk size per RFC 2045 section 6.8.
     *
     * <p>
     * The {@value} character limit does not count the trailing CRLF, but counts all other characters, including any
     * equal signs.
     * </p>
     *
     * @see <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045 section 6.8</a>
     */
    public static final int MIME_CHUNK_SIZE = 76;
    
    /**
     * PEM chunk size per RFC 1421 section 4.3.2.4.
     *
     * <p>
     * The {@value} character limit does not count the trailing CRLF, but counts all other characters, including any
     * equal signs.
     * </p>
     *
     * @see <a href="http://tools.ietf.org/html/rfc1421">RFC 1421 section 4.3.2.4</a>
     */
    public static final int PEM_CHUNK_SIZE = 64;
    
    private static final int DEFAULT_BUFFER_RESIZE_FACTOR = 2;
    
    /**
     * Defines the default buffer size - currently {@value}
     * - must be large enough for at least one encoded block+separator
     */
    private static final int DEFAULT_BUFFER_SIZE = 8192;
    
    /** Mask used to extract 8 bits, used in decoding bytes */
    protected static final int MASK_8BITS = 0xff;
    
    /**
     * Byte used to pad output.
     */
    protected static final byte PAD_DEFAULT = '='; // Allow static access to default
    
    /**
     * @deprecated Use {@link #pad}. Will be removed in 2.0.
     */
    @Deprecated
    protected final byte PAD = PAD_DEFAULT; // instance variable just in case it needs to vary later
    
    protected final byte pad; // instance variable just in case it needs to vary later
    
    /** Number of bytes in each full block of unencoded data, e.g. 4 for Base64 and 5 for Base32 */
    private final int unencodedBlockSize;
    
    /** Number of bytes in each full block of encoded data, e.g. 3 for Base64 and 8 for Base32 */
    private final int encodedBlockSize;
    
    /**
     * Chunksize for encoding. Not used when decoding.
     * A value of zero or less implies no chunking of the encoded data.
     * Rounded down to nearest multiple of encodedBlockSize.
     */
    protected final int lineLength;
    
    /**
     * Size of chunk separator. Not used unless {@link #lineLength} &gt; 0.
     */
    private final int chunkSeparatorLength;
    
    /**
     * Note <code>lineLength</code> is rounded down to the nearest multiple of {@link #encodedBlockSize}
     * If <code>chunkSeparatorLength</code> is zero, then chunking is disabled.
     * @param unencodedBlockSize the size of an unencoded block (e.g. Base64 = 3)
     * @param encodedBlockSize the size of an encoded block (e.g. Base64 = 4)
     * @param lineLength if &gt; 0, use chunking with a length <code>lineLength</code>
     * @param chunkSeparatorLength the chunk separator length, if relevant
     */
    protected BaseNCodec(final int unencodedBlockSize, final int encodedBlockSize,
                         final int lineLength, final int chunkSeparatorLength) {
        this(unencodedBlockSize, encodedBlockSize, lineLength, chunkSeparatorLength, PAD_DEFAULT);
    }
    
    /**
     * Note <code>lineLength</code> is rounded down to the nearest multiple of {@link #encodedBlockSize}
     * If <code>chunkSeparatorLength</code> is zero, then chunking is disabled.
     * @param unencodedBlockSize the size of an unencoded block (e.g. Base64 = 3)
     * @param encodedBlockSize the size of an encoded block (e.g. Base64 = 4)
     * @param lineLength if &gt; 0, use chunking with a length <code>lineLength</code>
     * @param chunkSeparatorLength the chunk separator length, if relevant
     * @param pad byte used as padding byte.
     */
    protected BaseNCodec(final int unencodedBlockSize, final int encodedBlockSize,
                         final int lineLength, final int chunkSeparatorLength, final byte pad) {
        this.unencodedBlockSize = unencodedBlockSize;
        this.encodedBlockSize = encodedBlockSize;
        final boolean useChunking = lineLength > 0 && chunkSeparatorLength > 0;
        this.lineLength = useChunking ? (lineLength / encodedBlockSize) * encodedBlockSize : 0;
        this.chunkSeparatorLength = chunkSeparatorLength;
        
        this.pad = pad;
    }
    
    /**
     * Returns true if this object has buffered data for reading.
     *
     * @param context the context to be used
     * @return true if there is data still available for reading.
     */
    boolean hasData(final Context context) {  // package protected for access from I/O streams
        return context.buffer != null;
    }
    
    /**
     * Returns the amount of buffered data available for reading.
     *
     * @param context the context to be used
     * @return The amount of buffered data available for reading.
     */
    int available(final Context context) {  // package protected for access from I/O streams
        return context.buffer != null ? context.pos - context.readPos : 0;
    }
    
    /**
     * Get the default buffer size. Can be overridden.
     *
     * @return {@link #DEFAULT_BUFFER_SIZE}
     */
    protected int getDefaultBufferSize() {
        return DEFAULT_BUFFER_SIZE;
    }
    
    /**
     * Increases our buffer by the {@link #DEFAULT_BUFFER_RESIZE_FACTOR}.
     * @param context the context to be used
     */
    private byte[] resizeBuffer(final Context context) {
        if (context.buffer == null) {
            context.buffer = new byte[getDefaultBufferSize()];
            context.pos = 0;
            context.readPos = 0;
        } else {
            final byte[] b = new byte[context.buffer.length * DEFAULT_BUFFER_RESIZE_FACTOR];
            System.arraycopy(context.buffer, 0, b, 0, context.buffer.length);
            context.buffer = b;
        }
        return context.buffer;
    }
    
    /**
     * Ensure that the buffer has room for <code>size</code> bytes
     *
     * @param size minimum spare space required
     * @param context the context to be used
     * @return the buffer
     */
    protected byte[] ensureBufferSize(final int size, final Context context){
        if ((context.buffer == null) || (context.buffer.length < context.pos + size)){
            return resizeBuffer(context);
        }
        return context.buffer;
    }
    
    /**
     * Extracts buffered data into the provided byte[] array, starting at position bPos, up to a maximum of bAvail
     * bytes. Returns how many bytes were actually extracted.
     * <p>
     * Package protected for access from I/O streams.
     *
     * @param b
     *            byte[] array to extract the buffered data into.
     * @param bPos
     *            position in byte[] array to start extraction at.
     * @param bAvail
     *            amount of bytes we're allowed to extract. We may extract fewer (if fewer are available).
     * @param context
     *            the context to be used
     * @return The number of bytes successfully extracted into the provided byte[] array.
     */
    int readResults(final byte[] b, final int bPos, final int bAvail, final Context context) {
        if (context.buffer != null) {
            final int len = Math.min(available(context), bAvail);
            System.arraycopy(context.buffer, context.readPos, b, bPos, len);
            context.readPos += len;
            if (context.readPos >= context.pos) {
                context.buffer = null; // so hasData() will return false, and this method can return -1
            }
            return len;
        }
        return context.eof ? EOF : 0;
    }
    
    /**
     * Checks if a byte value is whitespace or not.
     * Whitespace is taken to mean: space, tab, CR, LF
     * @param byteToCheck
     *            the byte to check
     * @return true if byte is whitespace, false otherwise
     */
    protected static boolean isWhiteSpace(final byte byteToCheck) {
        switch (byteToCheck) {
            case ' ' :
            case '\n' :
            case '\r' :
            case '\t' :
                return true;
            default :
                return false;
        }
    }
    
    /**
     * Encodes an Object using the Base-N algorithm. This method is provided in order to satisfy the requirements of
     * the Encoder interface, and will throw an EncoderException if the supplied object is not of type byte[].
     *
     * @param obj
     *            Object to encode
     * @return An object (of type byte[]) containing the Base-N encoded data which corresponds to the byte[] supplied.
     * @throws EncoderException
     *             if the parameter supplied is not of type byte[]
     */
    @Override
    public Object encode(final Object obj) throws Exception {
        if (!(obj instanceof byte[])) {
            throw new Exception("Parameter supplied to Base-N encode is not a byte[]");
        }
        return encode((byte[]) obj);
    }
    
    /**
     * Encodes a byte[] containing binary data, into a String containing characters in the Base-N alphabet.
     * Uses UTF8 encoding.
     *
     * @param pArray
     *            a byte array containing binary data
     * @return A String containing only Base-N character data
     */
    public String encodeToString(final byte[] pArray) {
        try {
            return new String(pArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Encodes a byte[] containing binary data, into a String containing characters in the appropriate alphabet.
     * Uses UTF8 encoding.
     *
     * @param pArray a byte array containing binary data
     * @return String containing only character data in the appropriate alphabet.
     */
    public String encodeAsString(final byte[] pArray){
        try {
            return new String(pArray,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Decodes an Object using the Base-N algorithm. This method is provided in order to satisfy the requirements of
     * the Decoder interface, and will throw a DecoderException if the supplied object is not of type byte[] or String.
     *
     * @param obj
     *            Object to decode
     * @return An object (of type byte[]) containing the binary data which corresponds to the byte[] or String
     *         supplied.
     * @throws DecoderException
     *             if the parameter supplied is not of type byte[]
     */
    @Override
    public Object decode(final Object obj) throws Exception {
        if (obj instanceof byte[]) {
            return decode((byte[]) obj);
        } else if (obj instanceof String) {
            return decode((String) obj);
        } else {
            throw new Exception("Parameter supplied to Base-N decode is not a byte[] or a String");
        }
    }
    
    /**
     * Decodes a String containing characters in the Base-N alphabet.
     *
     * @param pArray
     *            A String containing Base-N character data
     * @return a byte array containing binary data
     */
    public byte[] decode(final String pArray) {
        try {
            return decode(pArray.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Decodes a byte[] containing characters in the Base-N alphabet.
     *
     * @param pArray
     *            A byte array containing Base-N character data
     * @return a byte array containing binary data
     */
    @Override
    public byte[] decode(final byte[] pArray) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        final Context context = new Context();
        decode(pArray, 0, pArray.length, context);
        decode(pArray, 0, EOF, context); // Notify decoder of EOF.
        final byte[] result = new byte[context.pos];
        readResults(result, 0, result.length, context);
        return result;
    }
    
    /**
     * Encodes a byte[] containing binary data, into a byte[] containing characters in the alphabet.
     *
     * @param pArray
     *            a byte array containing binary data
     * @return A byte array containing only the basen alphabetic character data
     */
    @Override
    public byte[] encode(final byte[] pArray) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        final Context context = new Context();
        encode(pArray, 0, pArray.length, context);
        encode(pArray, 0, EOF, context); // Notify encoder of EOF.
        final byte[] buf = new byte[context.pos - context.readPos];
        readResults(buf, 0, buf.length, context);
        return buf;
    }
    
    // package protected for access from I/O streams
    abstract void encode(byte[] pArray, int i, int length, Context context);
    
    // package protected for access from I/O streams
    abstract void decode(byte[] pArray, int i, int length, Context context);
    
    /**
     * Returns whether or not the <code>octet</code> is in the current alphabet.
     * Does not allow whitespace or pad.
     *
     * @param value The value to test
     *
     * @return <code>true</code> if the value is defined in the current alphabet, <code>false</code> otherwise.
     */
    protected abstract boolean isInAlphabet(byte value);
    
    /**
     * Tests a given byte array to see if it contains only valid characters within the alphabet.
     * The method optionally treats whitespace and pad as valid.
     *
     * @param arrayOctet byte array to test
     * @param allowWSPad if <code>true</code>, then whitespace and PAD are also allowed
     *
     * @return <code>true</code> if all bytes are valid characters in the alphabet or if the byte array is empty;
     *         <code>false</code>, otherwise
     */
    public boolean isInAlphabet(final byte[] arrayOctet, final boolean allowWSPad) {
        for (int i = 0; i < arrayOctet.length; i++) {
            if (!isInAlphabet(arrayOctet[i]) &&
                (!allowWSPad || (arrayOctet[i] != pad) && !isWhiteSpace(arrayOctet[i]))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Tests a given String to see if it contains only valid characters within the alphabet.
     * The method treats whitespace and PAD as valid.
     *
     * @param basen String to test
     * @return <code>true</code> if all characters in the String are valid characters in the alphabet or if
     *         the String is empty; <code>false</code>, otherwise
     * @see #isInAlphabet(byte[], boolean)
     */
    public boolean isInAlphabet(final String basen) {
        try {
            return isInAlphabet(basen.getBytes("URF-8"), true);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * Tests a given byte array to see if it contains any characters within the alphabet or PAD.
     *
     * Intended for use in checking line-ending arrays
     *
     * @param arrayOctet
     *            byte array to test
     * @return <code>true</code> if any byte is a valid character in the alphabet or PAD; <code>false</code> otherwise
     */
    protected boolean containsAlphabetOrPad(final byte[] arrayOctet) {
        if (arrayOctet == null) {
            return false;
        }
        for (byte element : arrayOctet) {
            if (pad == element || isInAlphabet(element)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Calculates the amount of space needed to encode the supplied array.
     *
     * @param pArray byte[] array which will later be encoded
     *
     * @return amount of space needed to encoded the supplied array.
     * Returns a long since a max-len array will require &gt; Integer.MAX_VALUE
     */
    public long getEncodedLength(final byte[] pArray) {
        // Calculate non-chunked size - rounded up to allow for padding
        // cast to long is needed to avoid possibility of overflow
        long len = ((pArray.length + unencodedBlockSize-1)  / unencodedBlockSize) * (long) encodedBlockSize;
        if (lineLength > 0) { // We're using chunking
            // Round up to nearest multiple
            len += ((len + lineLength-1) / lineLength) * chunkSeparatorLength;
        }
        return len;
    }
}
class Base64 extends BaseNCodec {
    
    /**
     * BASE32 characters are 6 bits in length.
     * They are formed by taking a block of 3 octets to form a 24-bit string,
     * which is converted into 4 BASE64 characters.
     */
    private static final int BITS_PER_ENCODED_BYTE = 6;
    private static final int BYTES_PER_UNENCODED_BLOCK = 3;
    private static final int BYTES_PER_ENCODED_BLOCK = 4;
    
    /**
     * Chunk separator per RFC 2045 section 2.1.
     *
     * <p>
     * N.B. The next major release may break compatibility and make this field private.
     * </p>
     *
     * @see <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045 section 2.1</a>
     */
    static final byte[] CHUNK_SEPARATOR = {'\r', '\n'};
    
    /**
     * This array is a lookup table that translates 6-bit positive integer index values into their "Base64 Alphabet"
     * equivalents as specified in Table 1 of RFC 2045.
     *
     * Thanks to "commons" project in ws.apache.org for this code.
     * http://svn.apache.org/repos/asf/webservices/commons/trunk/modules/util/
     */
    private static final byte[] STANDARD_ENCODE_TABLE = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
    };
    
    /**
     * This is a copy of the STANDARD_ENCODE_TABLE above, but with + and /
     * changed to - and _ to make the encoded Base64 results more URL-SAFE.
     * This table is only used when the Base64's mode is set to URL-SAFE.
     */
    private static final byte[] URL_SAFE_ENCODE_TABLE = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
    };
    
    /**
     * This array is a lookup table that translates Unicode characters drawn from the "Base64 Alphabet" (as specified
     * in Table 1 of RFC 2045) into their 6-bit positive integer equivalents. Characters that are not in the Base64
     * alphabet but fall within the bounds of the array are translated to -1.
     *
     * Note: '+' and '-' both decode to 62. '/' and '_' both decode to 63. This means decoder seamlessly handles both
     * URL_SAFE and STANDARD base64. (The encoder, on the other hand, needs to know ahead of time what to emit).
     *
     * Thanks to "commons" project in ws.apache.org for this code.
     * http://svn.apache.org/repos/asf/webservices/commons/trunk/modules/util/
     */
    private static final byte[] DECODE_TABLE = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54,
    55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4,
    5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
    24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34,
    35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
    };
    
    /**
     * Base64 uses 6-bit fields.
     */
    /** Mask used to extract 6 bits, used when encoding */
    private static final int MASK_6BITS = 0x3f;
    
    // The static final fields above are used for the original static byte[] methods on Base64.
    // The private member fields below are used with the new streaming approach, which requires
    // some state be preserved between calls of encode() and decode().
    
    /**
     * Encode table to use: either STANDARD or URL_SAFE. Note: the DECODE_TABLE above remains static because it is able
     * to decode both STANDARD and URL_SAFE streams, but the encodeTable must be a member variable so we can switch
     * between the two modes.
     */
    private final byte[] encodeTable;
    
    // Only one decode table currently; keep for consistency with Base32 code
    private final byte[] decodeTable = DECODE_TABLE;
    
    /**
     * Line separator for encoding. Not used when decoding. Only used if lineLength &gt; 0.
     */
    private final byte[] lineSeparator;
    
    /**
     * Convenience variable to help us determine when our buffer is going to run out of room and needs resizing.
     * <code>decodeSize = 3 + lineSeparator.length;</code>
     */
    private final int decodeSize;
    
    /**
     * Convenience variable to help us determine when our buffer is going to run out of room and needs resizing.
     * <code>encodeSize = 4 + lineSeparator.length;</code>
     */
    private final int encodeSize;
    
    /**
     * Creates a Base64 codec used for decoding (all modes) and encoding in URL-unsafe mode.
     * <p>
     * When encoding the line length is 0 (no chunking), and the encoding table is STANDARD_ENCODE_TABLE.
     * </p>
     *
     * <p>
     * When decoding all variants are supported.
     * </p>
     */
    public Base64() {
        this(0);
    }
    
    /**
     * Creates a Base64 codec used for decoding (all modes) and encoding in the given URL-safe mode.
     * <p>
     * When encoding the line length is 76, the line separator is CRLF, and the encoding table is STANDARD_ENCODE_TABLE.
     * </p>
     *
     * <p>
     * When decoding all variants are supported.
     * </p>
     *
     * @param urlSafe
     *            if <code>true</code>, URL-safe encoding is used. In most cases this should be set to
     *            <code>false</code>.
     * @since 1.4
     */
    public Base64(final boolean urlSafe) {
        this(MIME_CHUNK_SIZE, CHUNK_SEPARATOR, urlSafe);
    }
    
    /**
     * Creates a Base64 codec used for decoding (all modes) and encoding in URL-unsafe mode.
     * <p>
     * When encoding the line length is given in the constructor, the line separator is CRLF, and the encoding table is
     * STANDARD_ENCODE_TABLE.
     * </p>
     * <p>
     * Line lengths that aren't multiples of 4 will still essentially end up being multiples of 4 in the encoded data.
     * </p>
     * <p>
     * When decoding all variants are supported.
     * </p>
     *
     * @param lineLength
     *            Each line of encoded data will be at most of the given length (rounded down to nearest multiple of
     *            4). If lineLength &lt;= 0, then the output will not be divided into lines (chunks). Ignored when
     *            decoding.
     * @since 1.4
     */
    public Base64(final int lineLength) {
        this(lineLength, CHUNK_SEPARATOR);
    }
    
    /**
     * Creates a Base64 codec used for decoding (all modes) and encoding in URL-unsafe mode.
     * <p>
     * When encoding the line length and line separator are given in the constructor, and the encoding table is
     * STANDARD_ENCODE_TABLE.
     * </p>
     * <p>
     * Line lengths that aren't multiples of 4 will still essentially end up being multiples of 4 in the encoded data.
     * </p>
     * <p>
     * When decoding all variants are supported.
     * </p>
     *
     * @param lineLength
     *            Each line of encoded data will be at most of the given length (rounded down to nearest multiple of
     *            4). If lineLength &lt;= 0, then the output will not be divided into lines (chunks). Ignored when
     *            decoding.
     * @param lineSeparator
     *            Each line of encoded data will end with this sequence of bytes.
     * @throws IllegalArgumentException
     *             Thrown when the provided lineSeparator included some base64 characters.
     * @since 1.4
     */
    public Base64(final int lineLength, final byte[] lineSeparator) {
        this(lineLength, lineSeparator, false);
    }
    
    /**
     * Creates a Base64 codec used for decoding (all modes) and encoding in URL-unsafe mode.
     * <p>
     * When encoding the line length and line separator are given in the constructor, and the encoding table is
     * STANDARD_ENCODE_TABLE.
     * </p>
     * <p>
     * Line lengths that aren't multiples of 4 will still essentially end up being multiples of 4 in the encoded data.
     * </p>
     * <p>
     * When decoding all variants are supported.
     * </p>
     *
     * @param lineLength
     *            Each line of encoded data will be at most of the given length (rounded down to nearest multiple of
     *            4). If lineLength &lt;= 0, then the output will not be divided into lines (chunks). Ignored when
     *            decoding.
     * @param lineSeparator
     *            Each line of encoded data will end with this sequence of bytes.
     * @param urlSafe
     *            Instead of emitting '+' and '/' we emit '-' and '_' respectively. urlSafe is only applied to encode
     *            operations. Decoding seamlessly handles both modes.
     *            <b>Note: no padding is added when using the URL-safe alphabet.</b>
     * @throws IllegalArgumentException
     *             The provided lineSeparator included some base64 characters. That's not going to work!
     * @since 1.4
     */
    public Base64(final int lineLength, final byte[] lineSeparator, final boolean urlSafe) {
        super(BYTES_PER_UNENCODED_BLOCK, BYTES_PER_ENCODED_BLOCK,
              lineLength,
              lineSeparator == null ? 0 : lineSeparator.length);
        // TODO could be simplified if there is no requirement to reject invalid line sep when length <=0
        // @see test case Base64Test.testConstructors()
        if (lineSeparator != null) {
            if (containsAlphabetOrPad(lineSeparator)) {
                String sep = null;
                try {
                    sep = new String (lineSeparator,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    sep = e.getMessage();
                }
                throw new IllegalArgumentException("lineSeparator must not contain base64 characters: [" + sep + "]");
            }
            if (lineLength > 0){ // null line-sep forces no chunking rather than throwing IAE
                this.encodeSize = BYTES_PER_ENCODED_BLOCK + lineSeparator.length;
                this.lineSeparator = new byte[lineSeparator.length];
                System.arraycopy(lineSeparator, 0, this.lineSeparator, 0, lineSeparator.length);
            } else {
                this.encodeSize = BYTES_PER_ENCODED_BLOCK;
                this.lineSeparator = null;
            }
        } else {
            this.encodeSize = BYTES_PER_ENCODED_BLOCK;
            this.lineSeparator = null;
        }
        this.decodeSize = this.encodeSize - 1;
        this.encodeTable = urlSafe ? URL_SAFE_ENCODE_TABLE : STANDARD_ENCODE_TABLE;
    }
    
    /**
     * Returns our current encode mode. True if we're URL-SAFE, false otherwise.
     *
     * @return true if we're in URL-SAFE mode, false otherwise.
     * @since 1.4
     */
    public boolean isUrlSafe() {
        return this.encodeTable == URL_SAFE_ENCODE_TABLE;
    }
    
    /**
     * <p>
     * Encodes all of the provided data, starting at inPos, for inAvail bytes. Must be called at least twice: once with
     * the data to encode, and once with inAvail set to "-1" to alert encoder that EOF has been reached, to flush last
     * remaining bytes (if not multiple of 3).
     * </p>
     * <p><b>Note: no padding is added when encoding using the URL-safe alphabet.</b></p>
     * <p>
     * Thanks to "commons" project in ws.apache.org for the bitwise operations, and general approach.
     * http://svn.apache.org/repos/asf/webservices/commons/trunk/modules/util/
     * </p>
     *
     * @param in
     *            byte[] array of binary data to base64 encode.
     * @param inPos
     *            Position to start reading data from.
     * @param inAvail
     *            Amount of bytes available from input for encoding.
     * @param context
     *            the context to be used
     */
    @Override
    void encode(final byte[] in, int inPos, final int inAvail, final Context context) {
        if (context.eof) {
            return;
        }
        // inAvail < 0 is how we're informed of EOF in the underlying data we're
        // encoding.
        if (inAvail < 0) {
            context.eof = true;
            if (0 == context.modulus && lineLength == 0) {
                return; // no leftovers to process and not using chunking
            }
            final byte[] buffer = ensureBufferSize(encodeSize, context);
            final int savedPos = context.pos;
            switch (context.modulus) { // 0-2
                case 0 : // nothing to do here
                    break;
                case 1 : // 8 bits = 6 + 2
                    // top 6 bits:
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea >> 2) & MASK_6BITS];
                    // remaining 2:
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea << 4) & MASK_6BITS];
                    // URL-SAFE skips the padding to further reduce size.
                    if (encodeTable == STANDARD_ENCODE_TABLE) {
                        buffer[context.pos++] = pad;
                        buffer[context.pos++] = pad;
                    }
                    break;
                    
                case 2 : // 16 bits = 6 + 6 + 4
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea >> 10) & MASK_6BITS];
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea >> 4) & MASK_6BITS];
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea << 2) & MASK_6BITS];
                    // URL-SAFE skips the padding to further reduce size.
                    if (encodeTable == STANDARD_ENCODE_TABLE) {
                        buffer[context.pos++] = pad;
                    }
                    break;
                default:
                    throw new IllegalStateException("Impossible modulus "+context.modulus);
            }
            context.currentLinePos += context.pos - savedPos; // keep track of current line position
            // if currentPos == 0 we are at the start of a line, so don't add CRLF
            if (lineLength > 0 && context.currentLinePos > 0) {
                System.arraycopy(lineSeparator, 0, buffer, context.pos, lineSeparator.length);
                context.pos += lineSeparator.length;
            }
        } else {
            for (int i = 0; i < inAvail; i++) {
                final byte[] buffer = ensureBufferSize(encodeSize, context);
                context.modulus = (context.modulus+1) % BYTES_PER_UNENCODED_BLOCK;
                int b = in[inPos++];
                if (b < 0) {
                    b += 256;
                }
                context.ibitWorkArea = (context.ibitWorkArea << 8) + b; //  BITS_PER_BYTE
                if (0 == context.modulus) { // 3 bytes = 24 bits = 4 * 6 bits to extract
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea >> 18) & MASK_6BITS];
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea >> 12) & MASK_6BITS];
                    buffer[context.pos++] = encodeTable[(context.ibitWorkArea >> 6) & MASK_6BITS];
                    buffer[context.pos++] = encodeTable[context.ibitWorkArea & MASK_6BITS];
                    context.currentLinePos += BYTES_PER_ENCODED_BLOCK;
                    if (lineLength > 0 && lineLength <= context.currentLinePos) {
                        System.arraycopy(lineSeparator, 0, buffer, context.pos, lineSeparator.length);
                        context.pos += lineSeparator.length;
                        context.currentLinePos = 0;
                    }
                }
            }
        }
    }
    
    /**
     * <p>
     * Decodes all of the provided data, starting at inPos, for inAvail bytes. Should be called at least twice: once
     * with the data to decode, and once with inAvail set to "-1" to alert decoder that EOF has been reached. The "-1"
     * call is not necessary when decoding, but it doesn't hurt, either.
     * </p>
     * <p>
     * Ignores all non-base64 characters. This is how chunked (e.g. 76 character) data is handled, since CR and LF are
     * silently ignored, but has implications for other bytes, too. This method subscribes to the garbage-in,
     * garbage-out philosophy: it will not check the provided data for validity.
     * </p>
     * <p>
     * Thanks to "commons" project in ws.apache.org for the bitwise operations, and general approach.
     * http://svn.apache.org/repos/asf/webservices/commons/trunk/modules/util/
     * </p>
     *
     * @param in
     *            byte[] array of ascii data to base64 decode.
     * @param inPos
     *            Position to start reading data from.
     * @param inAvail
     *            Amount of bytes available from input for encoding.
     * @param context
     *            the context to be used
     */
    @Override
    void decode(final byte[] in, int inPos, final int inAvail, final Context context) {
        if (context.eof) {
            return;
        }
        if (inAvail < 0) {
            context.eof = true;
        }
        for (int i = 0; i < inAvail; i++) {
            final byte[] buffer = ensureBufferSize(decodeSize, context);
            final byte b = in[inPos++];
            if (b == pad) {
                // We're done.
                context.eof = true;
                break;
            } else {
                if (b >= 0 && b < DECODE_TABLE.length) {
                    final int result = DECODE_TABLE[b];
                    if (result >= 0) {
                        context.modulus = (context.modulus+1) % BYTES_PER_ENCODED_BLOCK;
                        context.ibitWorkArea = (context.ibitWorkArea << BITS_PER_ENCODED_BYTE) + result;
                        if (context.modulus == 0) {
                            buffer[context.pos++] = (byte) ((context.ibitWorkArea >> 16) & MASK_8BITS);
                            buffer[context.pos++] = (byte) ((context.ibitWorkArea >> 8) & MASK_8BITS);
                            buffer[context.pos++] = (byte) (context.ibitWorkArea & MASK_8BITS);
                        }
                    }
                }
            }
        }
        
        // Two forms of EOF as far as base64 decoder is concerned: actual
        // EOF (-1) and first time '=' character is encountered in stream.
        // This approach makes the '=' padding characters completely optional.
        if (context.eof && context.modulus != 0) {
            final byte[] buffer = ensureBufferSize(decodeSize, context);
            
            // We have some spare bits remaining
            // Output all whole multiples of 8 bits and ignore the rest
            switch (context.modulus) {
                    //              case 0 : // impossible, as excluded above
                case 1 : // 6 bits - ignore entirely
                    // TODO not currently tested; perhaps it is impossible?
                    break;
                case 2 : // 12 bits = 8 + 4
                    context.ibitWorkArea = context.ibitWorkArea >> 4; // dump the extra 4 bits
                    buffer[context.pos++] = (byte) ((context.ibitWorkArea) & MASK_8BITS);
                    break;
                case 3 : // 18 bits = 8 + 8 + 2
                    context.ibitWorkArea = context.ibitWorkArea >> 2; // dump 2 bits
                    buffer[context.pos++] = (byte) ((context.ibitWorkArea >> 8) & MASK_8BITS);
                    buffer[context.pos++] = (byte) ((context.ibitWorkArea) & MASK_8BITS);
                    break;
                default:
                    throw new IllegalStateException("Impossible modulus "+context.modulus);
            }
        }
    }
    
    /**
     * Tests a given byte array to see if it contains only valid characters within the Base64 alphabet. Currently the
     * method treats whitespace as valid.
     *
     * @param arrayOctet
     *            byte array to test
     * @return <code>true</code> if all bytes are valid characters in the Base64 alphabet or if the byte array is empty;
     *         <code>false</code>, otherwise
     * @deprecated 1.5 Use {@link #isBase64(byte[])}, will be removed in 2.0.
     */
    @Deprecated
    public static boolean isArrayByteBase64(final byte[] arrayOctet) {
        return isBase64(arrayOctet);
    }
    
    /**
     * Returns whether or not the <code>octet</code> is in the base 64 alphabet.
     *
     * @param octet
     *            The value to test
     * @return <code>true</code> if the value is defined in the the base 64 alphabet, <code>false</code> otherwise.
     * @since 1.4
     */
    public static boolean isBase64(final byte octet) {
        return octet == PAD_DEFAULT || (octet >= 0 && octet < DECODE_TABLE.length && DECODE_TABLE[octet] != -1);
    }
    
    /**
     * Tests a given String to see if it contains only valid characters within the Base64 alphabet. Currently the
     * method treats whitespace as valid.
     *
     * @param base64
     *            String to test
     * @return <code>true</code> if all characters in the String are valid characters in the Base64 alphabet or if
     *         the String is empty; <code>false</code>, otherwise
     *  @since 1.5
     */
    public static boolean isBase64(final String base64) {
        try {
            return isBase64(base64.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * Tests a given byte array to see if it contains only valid characters within the Base64 alphabet. Currently the
     * method treats whitespace as valid.
     *
     * @param arrayOctet
     *            byte array to test
     * @return <code>true</code> if all bytes are valid characters in the Base64 alphabet or if the byte array is empty;
     *         <code>false</code>, otherwise
     * @since 1.5
     */
    public static boolean isBase64(final byte[] arrayOctet) {
        for (int i = 0; i < arrayOctet.length; i++) {
            if (!isBase64(arrayOctet[i]) && !isWhiteSpace(arrayOctet[i])) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Encodes binary data using the base64 algorithm but does not chunk the output.
     *
     * @param binaryData
     *            binary data to encode
     * @return byte[] containing Base64 characters in their UTF-8 representation.
     */
    public static byte[] encodeBase64(final byte[] binaryData) {
        return encodeBase64(binaryData, false);
    }
    
    /**
     * Encodes binary data using the base64 algorithm but does not chunk the output.
     *
     * NOTE:  We changed the behaviour of this method from multi-line chunking (commons-codec-1.4) to
     * single-line non-chunking (commons-codec-1.5).
     *
     * @param binaryData
     *            binary data to encode
     * @return String containing Base64 characters.
     * @since 1.4 (NOTE:  1.4 chunked the output, whereas 1.5 does not).
     */
    public static String encodeBase64String(final byte[] binaryData) {
        return new String(encodeBase64(binaryData, false));
    }
    
    /**
     * Encodes binary data using a URL-safe variation of the base64 algorithm but does not chunk the output. The
     * url-safe variation emits - and _ instead of + and / characters.
     * <b>Note: no padding is added.</b>
     * @param binaryData
     *            binary data to encode
     * @return byte[] containing Base64 characters in their UTF-8 representation.
     * @since 1.4
     */
    public static byte[] encodeBase64URLSafe(final byte[] binaryData) {
        return encodeBase64(binaryData, false, true);
    }
    
    /**
     * Encodes binary data using a URL-safe variation of the base64 algorithm but does not chunk the output. The
     * url-safe variation emits - and _ instead of + and / characters.
     * <b>Note: no padding is added.</b>
     * @param binaryData
     *            binary data to encode
     * @return String containing Base64 characters
     * @since 1.4
     */
    public static String encodeBase64URLSafeString(final byte[] binaryData) {
        return new String(encodeBase64(binaryData, false, true));
    }
    
    /**
     * Encodes binary data using the base64 algorithm and chunks the encoded output into 76 character blocks
     *
     * @param binaryData
     *            binary data to encode
     * @return Base64 characters chunked in 76 character blocks
     */
    public static byte[] encodeBase64Chunked(final byte[] binaryData) {
        return encodeBase64(binaryData, true);
    }
    
    /**
     * Encodes binary data using the base64 algorithm, optionally chunking the output into 76 character blocks.
     *
     * @param binaryData
     *            Array containing binary data to encode.
     * @param isChunked
     *            if <code>true</code> this encoder will chunk the base64 output into 76 character blocks
     * @return Base64-encoded data.
     * @throws IllegalArgumentException
     *             Thrown when the input array needs an output array bigger than {@link Integer#MAX_VALUE}
     */
    public static byte[] encodeBase64(final byte[] binaryData, final boolean isChunked) {
        return encodeBase64(binaryData, isChunked, false);
    }
    
    /**
     * Encodes binary data using the base64 algorithm, optionally chunking the output into 76 character blocks.
     *
     * @param binaryData
     *            Array containing binary data to encode.
     * @param isChunked
     *            if <code>true</code> this encoder will chunk the base64 output into 76 character blocks
     * @param urlSafe
     *            if <code>true</code> this encoder will emit - and _ instead of the usual + and / characters.
     *            <b>Note: no padding is added when encoding using the URL-safe alphabet.</b>
     * @return Base64-encoded data.
     * @throws IllegalArgumentException
     *             Thrown when the input array needs an output array bigger than {@link Integer#MAX_VALUE}
     * @since 1.4
     */
    public static byte[] encodeBase64(final byte[] binaryData, final boolean isChunked, final boolean urlSafe) {
        return encodeBase64(binaryData, isChunked, urlSafe, Integer.MAX_VALUE);
    }
    
    /**
     * Encodes binary data using the base64 algorithm, optionally chunking the output into 76 character blocks.
     *
     * @param binaryData
     *            Array containing binary data to encode.
     * @param isChunked
     *            if <code>true</code> this encoder will chunk the base64 output into 76 character blocks
     * @param urlSafe
     *            if <code>true</code> this encoder will emit - and _ instead of the usual + and / characters.
     *            <b>Note: no padding is added when encoding using the URL-safe alphabet.</b>
     * @param maxResultSize
     *            The maximum result size to accept.
     * @return Base64-encoded data.
     * @throws IllegalArgumentException
     *             Thrown when the input array needs an output array bigger than maxResultSize
     * @since 1.4
     */
    public static byte[] encodeBase64(final byte[] binaryData, final boolean isChunked,
                                      final boolean urlSafe, final int maxResultSize) {
        if (binaryData == null || binaryData.length == 0) {
            return binaryData;
        }
        
        // Create this so can use the super-class method
        // Also ensures that the same roundings are performed by the ctor and the code
        final Base64 b64 = isChunked ? new Base64(urlSafe) : new Base64(0, CHUNK_SEPARATOR, urlSafe);
        final long len = b64.getEncodedLength(binaryData);
        if (len > maxResultSize) {
            throw new IllegalArgumentException("Input array too big, the output array would be bigger (" +
                                               len +
                                               ") than the specified maximum size of " +
                                               maxResultSize);
        }
        
        return b64.encode(binaryData);
    }
    
    /**
     * Decodes a Base64 String into octets.
     * <p>
     * <b>Note:</b> this method seamlessly handles data encoded in URL-safe or normal mode.
     * </p>
     *
     * @param base64String
     *            String containing Base64 data
     * @return Array containing decoded data.
     * @since 1.4
     */
    public static byte[] decodeBase64(final String base64String) {
        return new Base64().decode(base64String);
    }
    
    /**
     * Decodes Base64 data into octets.
     * <p>
     * <b>Note:</b> this method seamlessly handles data encoded in URL-safe or normal mode.
     * </p>
     *
     * @param base64Data
     *            Byte array containing Base64 data
     * @return Array containing decoded data.
     */
    public static byte[] decodeBase64(final byte[] base64Data) {
        return new Base64().decode(base64Data);
    }
    
    // Implementation of the Encoder Interface
    
    // Implementation of integer encoding used for crypto
    /**
     * Decodes a byte64-encoded integer according to crypto standards such as W3C's XML-Signature.
     *
     * @param pArray
     *            a byte array containing base64 character data
     * @return A BigInteger
     * @since 1.4
     */
    public static BigInteger decodeInteger(final byte[] pArray) {
        return new BigInteger(1, decodeBase64(pArray));
    }
    
    /**
     * Encodes to a byte64-encoded integer according to crypto standards such as W3C's XML-Signature.
     *
     * @param bigInt
     *            a BigInteger
     * @return A byte array containing base64 character data
     * @throws NullPointerException
     *             if null is passed in
     * @since 1.4
     */
    public static byte[] encodeInteger(final BigInteger bigInt) {
        if (bigInt == null) {
            throw new NullPointerException("encodeInteger called with null parameter");
        }
        return encodeBase64(toIntegerBytes(bigInt), false);
    }
    
    /**
     * Returns a byte-array representation of a <code>BigInteger</code> without sign bit.
     *
     * @param bigInt
     *            <code>BigInteger</code> to be converted
     * @return a byte array representation of the BigInteger parameter
     */
    static byte[] toIntegerBytes(final BigInteger bigInt) {
        int bitlen = bigInt.bitLength();
        // round bitlen
        bitlen = ((bitlen + 7) >> 3) << 3;
        final byte[] bigBytes = bigInt.toByteArray();
        
        if (((bigInt.bitLength() % 8) != 0) && (((bigInt.bitLength() / 8) + 1) == (bitlen / 8))) {
            return bigBytes;
        }
        // set up params for copying everything but sign bit
        int startSrc = 0;
        int len = bigBytes.length;
        
        // if bigInt is exactly byte-aligned, just skip signbit in copy
        if ((bigInt.bitLength() % 8) == 0) {
            startSrc = 1;
            len--;
        }
        final int startDst = bitlen / 8 - len; // to pad w/ nulls as per spec
        final byte[] resizedBytes = new byte[bitlen / 8];
        System.arraycopy(bigBytes, startSrc, resizedBytes, startDst, len);
        return resizedBytes;
    }
    
    /**
     * Returns whether or not the <code>octet</code> is in the Base64 alphabet.
     *
     * @param octet
     *            The value to test
     * @return <code>true</code> if the value is defined in the the Base64 alphabet <code>false</code> otherwise.
     */
    @Override
    protected boolean isInAlphabet(final byte octet) {
        return octet >= 0 && octet < decodeTable.length && decodeTable[octet] != -1;
    }
}
