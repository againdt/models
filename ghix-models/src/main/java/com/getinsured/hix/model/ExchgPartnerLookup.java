package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the exchg_partnerlookup database table.
 * 
 */
@Entity
@Table(name="exchg_partnerlookup")
public class ExchgPartnerLookup implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PartnerLookup_Seq")
	@SequenceGenerator(name = "PartnerLookup_Seq", sequenceName = "EXCHG_PrtLkp_seq", allocationSize = 1)
	private int id;
	
	@Column(name="insurername", length=50)
	private String insurerName;
	
	@Column(name="hios_issuer_id", length=5)
	@NotNull
	private String hiosIssuerId;
	
	@Column(name="isa05", length=2)
	private String isa05;
	
	@Column(name="isa06", length=15)
	private String isa06;
	
	@Column(name="isa07", length=2)
	private String isa07;
	
	@Column(name="isa08", length=15)
	private String isa08;
	
	@Column(name="isa15", length=1)
	private String isa15;
	
	@Column(name="gs02", length=15)
	private String gs02;
	
	@Column(name="gs03", length=15)
	private String gs03;
	
	@Column(name="gs08", length=12)
	private String gs08;
	
	@Column(name="st01", length=3)
	private String st01;
	
	@Column(name="MARKET")
	private String market;
	
	@Column(name="DIRECTION")
	private String direction;
	
	@Column(name="Source_Dir")
	private String sourceDir;
	
	@Column(name="Target_Dir")
	private String targetDir;
	
	@Column(name="InProcess_Dir")
	private String inprogressDir;
	
	@Column(name="Archive_Dir")
	private String archiveDir;
	
	@Column(name="Map_Name")
	private String mapName;
	
	@Column(name="Validation_Standard")
	private String validationStandard;
	
	@Column(name="APF")
	private String apf;
	
	@Column(name="Role_ID")
	private Integer roleId;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="creation_timestamp",nullable=false)
	private Date creationTimestamp;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_update_timestamp",nullable=false)
	private Date lastUpdateTimestamp;
	
	public String getSourceDir() {
		return sourceDir;
	}

	public void setSourceDir(String sourceDir) {
		this.sourceDir = sourceDir;
	}

	public String getTargetDir() {
		return targetDir;
	}

	public void setTargetDir(String targetDir) {
		this.targetDir = targetDir;
	}

	public String getInprogressDir() {
		return inprogressDir;
	}

	public void setInprogressDir(String inprogressDir) {
		this.inprogressDir = inprogressDir;
	}

	public String getArchiveDir() {
		return archiveDir;
	}

	public void setArchiveDir(String archiveDir) {
		this.archiveDir = archiveDir;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getValidationStandard() {
		return validationStandard;
	}

	public void setValidationStandard(String validationStandard) {
		this.validationStandard = validationStandard;
	}

	public String getMarket() {
		return market;
	}
	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInsurerName() {
		return insurerName;
	}

	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public String getIsa05() {
		return isa05;
	}

	public void setIsa05(String isa05) {
		this.isa05 = isa05;
	}

	public String getIsa06() {
		return isa06;
	}

	public void setIsa06(String isa06) {
		this.isa06 = isa06;
	}

	public String getIsa07() {
		return isa07;
	}

	public void setIsa07(String isa07) {
		this.isa07 = isa07;
	}

	public String getIsa08() {
		return isa08;
	}

	public void setIsa08(String isa08) {
		this.isa08 = isa08;
	}

	public String getIsa15() {
		return isa15;
	}

	public void setIsa15(String isa15) {
		this.isa15 = isa15;
	}

	public String getGs02() {
		return gs02;
	}

	public void setGs02(String gs02) {
		this.gs02 = gs02;
	}

	public String getGs03() {
		return gs03;
	}

	public void setGs03(String gs03) {
		this.gs03 = gs03;
	}

	public String getGs08() {
		return gs08;
	}

	public void setGs08(String gs08) {
		this.gs08 = gs08;
	}

	public String getSt01() {
		return st01;
	}

	public void setSt01(String st01) {
		this.st01 = st01;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getApf() {
		return apf;
	}

	public void setApf(String apf) {
		this.apf = apf;
	}

}
