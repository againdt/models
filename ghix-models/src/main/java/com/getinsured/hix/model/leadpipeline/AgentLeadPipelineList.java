package com.getinsured.hix.model.leadpipeline;

import java.io.Serializable;

public class AgentLeadPipelineList implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Household household;
	
	private Member member;
	
	private NextAppointment nextAppointment;
	
	private Agent agent;

	public Household getHousehold() {
		return household;
	}

	public void setHousehold(Household household) {
		this.household = household;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public NextAppointment getNextAppointment() {
		return nextAppointment;
	}

	public void setNextAppointment(NextAppointment nextAppointment) {
		this.nextAppointment = nextAppointment;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	
}
