/**
 * 
 * @author santanu
 * @version 1.0
 * @since July 24, 2014 
 *
 */
package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CrossSellPlanRequestDTO implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String insuranceType;
	private String effectiveDate;
	private String exchangeType; // possible values ON/OFF
	private List<Map<String, String>> memberList; // household member data
	private String tenant;
	
	public CrossSellPlanRequestDTO(){
		
	}
	
	public CrossSellPlanRequestDTO(String insuranceType,String effectiveDate,String exchangeType,List<Map<String, String>> memberList){
		this.insuranceType = insuranceType;
		this.effectiveDate = effectiveDate;
		this.exchangeType = exchangeType;
		this.memberList = memberList;
	}
	
	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public List<Map<String, String>> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<Map<String, String>> memberList) {
		this.memberList = memberList;
	}

	/**
	 * @return the tenant
	 */
	public String getTenant() {
		return tenant;
	}

	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
		
}
