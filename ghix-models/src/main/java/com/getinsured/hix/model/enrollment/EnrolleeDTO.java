package com.getinsured.hix.model.enrollment;

import java.io.Serializable;
/**
 * 
 * @author meher_a
 *
 */

public class EnrolleeDTO extends Enrollee  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer enrollmentId;
	private String enrolleeStatusCode;
	private String createdDate;
	private String personTypeLabel;
	private String personTypeCode;
	private String relationshipToHCPCode;
	private String relationshipToHCPLabel;
	private String homeAddressState;
	private String homeAddressCity;
	private String homeAddressZip;
	private String homeAddress1;
	private String homeAddress2;
	
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public String getHomeAddressState() {
		return homeAddressState;
	}
	public void setHomeAddressState(String homeAddressState) {
		this.homeAddressState = homeAddressState;
	}
	public String getPersonTypeLabel() {
		return personTypeLabel;
	}
	public void setPersonTypeLabel(String personTypeLabel) {
		this.personTypeLabel = personTypeLabel;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getEnrolleeStatusCode() {
		return enrolleeStatusCode;
	}
	public void setEnrolleeStatusCode(String enrolleeStatusCode) {
		this.enrolleeStatusCode = enrolleeStatusCode;
	}
	public String getPersonTypeCode() {
		return personTypeCode;
	}
	public void setPersonTypeCode(String personTypeCode) {
		this.personTypeCode = personTypeCode;
	}
	public String getRelationshipToHCPCode() {
		return relationshipToHCPCode;
	}
	public void setRelationshipToHCPCode(String relationshipToHCPCode) {
		this.relationshipToHCPCode = relationshipToHCPCode;
	}
	public String getRelationshipToHCPLabel() {
		return relationshipToHCPLabel;
	}
	public void setRelationshipToHCPLabel(String relationshipToHCPLabel) {
		this.relationshipToHCPLabel = relationshipToHCPLabel;
	}
	public String getHomeAddressCity() {
		return homeAddressCity;
	}
	public void setHomeAddressCity(String homeAddressCity) {
		this.homeAddressCity = homeAddressCity;
	}
	public String getHomeAddressZip() {
		return homeAddressZip;
	}
	public void setHomeAddressZip(String homeAddressZip) {
		this.homeAddressZip = homeAddressZip;
	}
	public String getHomeAddress1() {
		return homeAddress1;
	}
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}
	public String getHomeAddress2() {
		return homeAddress2;
	}
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}
	
	

}
