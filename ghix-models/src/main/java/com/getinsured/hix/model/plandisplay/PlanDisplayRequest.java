/**
 * 
 */
package com.getinsured.hix.model.plandisplay;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.model.GroupList;
//import com.getinsured.hix.model.PldOrderRequest;
import com.getinsured.hix.model.PldOrderRequest;

/**
 * @author Mishra_m
 *
 */
public class PlanDisplayRequest {
	
	private Map<String, Object> inputData;	
	private GroupData groupData;
	private List<GroupList> groupListArr;
	//private List<PldHouseholdPerson> pldHouseholdPersonList;
	private List<GroupData> groupDataList;
	private List<ProviderBean> providersList;
	private Long eligLeadId;
	private Long ssapApplicationId;
	private List<PldOrderRequest> consumerData;
	private Long householdId;
	private PdPreferencesDTO pdPreferencesDTO;
	
	
	public Map<String, Object> getInputData() {
		return inputData;
	}
	public void setInputData(Map<String, Object> inputData) {
		this.inputData = inputData;
	}
	public GroupData getGroupData() {
		return groupData;
	}
	public void setGroupData(GroupData groupData) {
		this.groupData = groupData;
	}
	public List<GroupList> getGroupListArr() {
		return groupListArr;
	}
	public void setGroupListArr(List<GroupList> groupListArr) {
		this.groupListArr = groupListArr;
	}
	/*public List<PldHouseholdPerson> getPldHouseholdPersonList() {
		return pldHouseholdPersonList;
	}
	public void setPldHouseholdPersonList(
			List<PldHouseholdPerson> pldHouseholdPersonList) {
		this.pldHouseholdPersonList = pldHouseholdPersonList;
	}*/
	public List<GroupData> getGroupDataList() {
		return groupDataList;
	}
	public void setGroupDataList(List<GroupData> groupDataList) {
		this.groupDataList = groupDataList;
	}
	public List<ProviderBean> getProvidersList() {
		return providersList;
	}
	public void setProvidersList(List<ProviderBean> providersList) {
		this.providersList = providersList;
	}
	public Long getEligLeadId() {
		return eligLeadId;
	}
	public void setEligLeadId(Long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public List<PldOrderRequest> getConsumerData() {
		return consumerData;
	}
	public void setConsumerData(List<PldOrderRequest> consumerData) {
		this.consumerData = consumerData;
	}
	public Long getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}
	public PdPreferencesDTO getPdPreferencesDTO() {
		return pdPreferencesDTO;
	}
	public void setPdPreferencesDTO(PdPreferencesDTO pdPreferencesDTO) {
		this.pdPreferencesDTO = pdPreferencesDTO;
	}	
	
}
