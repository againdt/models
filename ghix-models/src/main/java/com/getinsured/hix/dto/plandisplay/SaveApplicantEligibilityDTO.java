/**
 * Created: Oct 28, 2014
 */
package com.getinsured.hix.dto.plandisplay;

import com.getinsured.hix.model.GHIXResponse;

/**
 * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
 * 
 */
public class SaveApplicantEligibilityDTO extends GHIXResponse {

	private String redirectionUrl;
	private String status;
	private String errorMsg;
	private boolean eligibleForEnrollment;
	private String reasonForEligibilityMismatch;
	private Double ffmAptc;

	public boolean isEligibleForEnrollment() {
		return eligibleForEnrollment;
	}

	public void setEligibleForEnrollment(boolean eligibleForEnrollment) {
		this.eligibleForEnrollment = eligibleForEnrollment;
	}

	public String getRedirectionUrl() {
		return redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getReasonForEligibilityMismatch() {
		return reasonForEligibilityMismatch;
	}

	public void setReasonForEligibilityMismatch(
			String reasonForEligibilityMismatch) {
		this.reasonForEligibilityMismatch = reasonForEligibilityMismatch;
	}

	public Double getFfmAptc() {
		return ffmAptc;
	}

	public void setFfmAptc(Double ffmAptc) {
		this.ffmAptc = ffmAptc;
	}

}
