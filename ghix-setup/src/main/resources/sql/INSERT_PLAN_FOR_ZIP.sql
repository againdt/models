-- HIX-51009 [Zip - Plans Analytics]
-- Creating INSERT_PLAN_FOR_ZIP function for populating data in PM_ZIP_PLANS table.
-- This is only for Dental and Health plans.

-- SQL for INSERT_PLAN_FOR_ZIP function:=
CREATE OR REPLACE FUNCTION INSERT_PLAN_FOR_ZIP (ZIP_INPUT IN VARCHAR2)
RETURN VARCHAR2 IS

BEGIN
	DBMS_OUTPUT.PUT_LINE('------------ Begin INSERT_PLAN_FOR_ZIP --------------');
	--Loop zip codes and call INSERT_PLAN_FOR_ZIP
	DBMS_OUTPUT.PUT_LINE('Inserting data for ZIP: ' || ZIP_INPUT);
	
	INSERT INTO PM_ZIP_PLANS (ID, CREATION_TIMESTAMP, ISSUER_PLAN_NUMBER, APPLICABLE_YEAR, ZIP, FIPS, NAME, INSURANCE_TYPE, HIOS_ISSUER_ID, PLAN_LEVEL) 
	SELECT PM_ZIP_PLANS_SEQ.NEXTVAL, CURRENT_TIMESTAMP, ISSUER_PLAN_NUMBER, APPLICABLE_YEAR, ZIP, FIPS, NAME, INSURANCE_TYPE, HIOS_ISSUER_ID, PLAN_LEVEL
	FROM (
		SELECT
			DISTINCT P.ISSUER_PLAN_NUMBER, P.APPLICABLE_YEAR, PSA.ZIP,
			PSA.FIPS, P.NAME, P.INSURANCE_TYPE, I.HIOS_ISSUER_ID, PH.PLAN_LEVEL
		FROM
			PLAN P, PM_PLAN_RATE PPR, ISSUERS I, PM_SERVICE_AREA PSA, PLAN_HEALTH PH
		WHERE
			I.ID = P.ISSUER_ID AND P.ID = PPR.PLAN_ID AND PSA.SERVICE_AREA_ID = P.SERVICE_AREA_ID
			AND P.ID = PH.PLAN_ID AND PPR.RATING_AREA_ID IN (SELECT DISTINCT RATING_AREA_ID
			FROM PM_ZIP_COUNTY_RATING_AREA WHERE ZIP = ZIP_INPUT OR COUNTY_FIPS
			IN (SELECT COUNTY_FIPS FROM ZIPCODES WHERE ZIPCODE = ZIP_INPUT))
			AND PSA.ZIP = ZIP_INPUT AND P.ENROLLMENT_AVAIL = 'AVAILABLE'
			AND I.CERTIFICATION_STATUS = 'CERTIFIED' AND P.STATUS = 'CERTIFIED'
			AND P.ISSUER_VERIFICATION_STATUS = 'VERIFIED' AND PSA.IS_DELETED = 'N'
			AND P.IS_DELETED = 'N' AND PPR.IS_DELETED = 'N'
		UNION
		SELECT
			DISTINCT P.ISSUER_PLAN_NUMBER, P.APPLICABLE_YEAR, PSA.ZIP,
			PSA.FIPS, P.NAME, P.INSURANCE_TYPE, I.HIOS_ISSUER_ID, PD.PLAN_LEVEL
		FROM
			PLAN P, PM_PLAN_RATE PPR, ISSUERS I, PM_SERVICE_AREA PSA, PLAN_DENTAL PD
		WHERE
			I.ID = P.ISSUER_ID AND P.ID = PPR.PLAN_ID AND PSA.SERVICE_AREA_ID = P.SERVICE_AREA_ID
			AND P.ID = PD.PLAN_ID AND PPR.RATING_AREA_ID IN (SELECT DISTINCT RATING_AREA_ID
			FROM PM_ZIP_COUNTY_RATING_AREA WHERE ZIP = ZIP_INPUT OR COUNTY_FIPS
			IN (SELECT COUNTY_FIPS FROM ZIPCODES WHERE ZIPCODE = ZIP_INPUT))
			AND PSA.ZIP = ZIP_INPUT AND P.ENROLLMENT_AVAIL = 'AVAILABLE'
			AND I.CERTIFICATION_STATUS = 'CERTIFIED' AND P.STATUS = 'CERTIFIED'
			AND P.ISSUER_VERIFICATION_STATUS = 'VERIFIED' AND PSA.IS_DELETED = 'N'
			AND P.IS_DELETED = 'N' AND PPR.IS_DELETED = 'N'
		ORDER BY ISSUER_PLAN_NUMBER, APPLICABLE_YEAR
	);
	COMMIT;
	DBMS_OUTPUT.PUT_LINE('------------ End INSERT_PLAN_FOR_ZIP --------------');
RETURN ZIP_INPUT;
END INSERT_PLAN_FOR_ZIP;