package com.getinsured.hix.dto.eapp.grammer.components.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.eapp.grammer.UIComponent;
import com.getinsured.hix.dto.eapp.grammer.UIComponentType;
import com.getinsured.hix.dto.eapp.grammer.UIValidationRule;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

public class GroupComponent extends UIComponent {
	
	private List<UIComponent> components;
	
	public List<UIComponent> getComponents() {
		return components;
	}

	public void setComponents(List<UIComponent> components) {
		this.components = components;
	}

	public GroupComponent(){
		this.type = UIComponentType.GROUP;
	}

	
	
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings({ "unchecked", "unchecked" })
	@Override
	public UIComponent parse(String json) {
		HashMap<String, Object> uimap = null;
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		try{
			uimap = gson.fromJson(json, HashMap.class);
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
		List<Object> comps = (List<Object>)uimap.get("components");
		UIComponent comp = new BaseComponent();
		UIComponent tmp = null;
		List<UIComponent> components = new ArrayList<UIComponent>();
		String inputJson;
		for(Object obj : comps){
			System.out.println(obj);
			inputJson = gson.toJson(obj, Map.class);
			tmp = comp.parse(inputJson);  
			components.add(comp);
		}
		
		/**
		 * parse current object
		 */
		decorate(json);
		this.components = components;
		
		return this;
	}
	
	
	public static void main(String args[]){
		//String s="\"components\": [\n {\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"First Name\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"validation\": \"name\",\n \"width\": 12,\n \"inputWidth\": \"md\"\n },\n {\n \"label\": \"Middle Initial\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"middleName\",\n \"id\": \"middle-name\",\n \"validation\": \"name\",\n \"width\": 2,\n \"inputWidth\": \"sm\"\n },\n {\n \"label\": \"Last Name\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.last\",\n \"id\": \"last-name\",\n \"validation\": \"name\",\n \"width\": 4,\n \"inputWidth\": \"md\"\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"name-group\"\n },\n {\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"Date of Birth\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.dob\",\n \"id\": \"date-of-birth\",\n \"validation\": \"date\",\n \"width\": 3\n },\n {\n \"label\": \"Age\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"age\",\n \"id\": \"age\",\n \"validation\": null,\n \"width\": 1\n },\n {\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": \"radio\",\n \"inline\": true,\n \"width\": 3\n },\n {\n \"label\": \"Tobacco User?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"tobacco\",\n \"id\": \"tobacco\",\n \"validation\": \"radio\",\n \"inline\": true,\n \"width\": 3\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"primary-dob-group\"\n },\n {\n \"label\": \"State of Birth\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"birthState\",\n \"id\": \"birth-state\",\n \"validation\": null,\n \"width\": 2\n },\n {\n \"label\": \"Marital Status\",\n \"type\": \"select\",\n \"options\": [\n {\n \"label\": \"Married\",\n \"value\": \"married\"\n },\n {\n \"label\": \"Domestic Partner\",\n \"value\": \"partner\"\n },\n {\n \"label\": \"Single\",\n \"value\": \"single\"\n }\n ],\n \"default\": \"--select status--\",\n \"showIf\": true,\n \"model\": \"maritalStatus\",\n \"id\": \"marital-status\",\n \"validation\": null,\n \"width\": 3\n },\n {\n \"label\": \"Are you a resident of the state in which you are applying?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"stateResident\",\n \"id\": \"state-resident\",\n \"validation\": null,\n \"inline\": true,\n \"width\": 6\n },\n {\n \"label\": \"Currently Employed?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"employmentStatus\",\n \"id\": \"employment-status\",\n \"validation\": \"name\",\n \"inline\": true,\n \"width\": 5\n },\n {\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"Occupation\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"occupation\",\n \"id\": \"occupation\",\n \"validation\": \"name\",\n \"width\": 4\n },\n {\n \"label\": \"Job Title\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"jobTitle\",\n \"id\": \"job-title\",\n \"validation\": \"name\",\n \"width\": 4\n }\n ]\n ],\n \"showIf\": {\n \"or\": [\n {\n \"model\": \"household.primary.name.first\",\n \"value\": \"Sachin\"\n },\n {\n \"model\": \"stateResident\",\n \"value\": \"true\"\n }\n ]\n },\n \"id\": \"employment-group\"\n }\n ]";
		//String s="{\"components\": [\n {\n \"label\": \"hola\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"FirstName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"validation\": \"name\",\n \"width\": 12,\n \"inputWidth\": \"md\"\n },\n {\n \"label\": \"MiddleInitial\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"middleName\",\n \"id\": \"middle-name\",\n \"validation\": \"name\",\n \"width\": 2,\n \"inputWidth\": \"sm\"\n },\n {\n \"label\": \"LastName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.last\",\n \"id\": \"last-name\",\n \"validation\": \"name\",\n \"width\": 4,\n \"inputWidth\": \"md\"\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"name-group\"\n },\n {\n \"label\": \"ming\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"DateofBirth\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.dob\",\n \"id\": \"date-of-birth\",\n \"validation\": \"date\",\n \"width\": 3\n },\n {\n \"label\": \"Age\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"age\",\n \"id\": \"age\",\n \"validation\": null,\n \"width\": 1\n },\n {\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": \"radio\",\n \"inline\": true,\n \"width\": 3\n },\n {\n \"label\": \"Tobacco User?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"tobacco\",\n \"id\": \"tobacco\",\n \"validation\": \"radio\",\n \"inline\": true,\n \"width\": 3\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"primary-dob-group\"\n },\n {\n \"label\": \"State of Birth\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"birthState\",\n \"id\": \"birth-state\",\n \"validation\": null,\n \"width\": 2\n },\n {\n \"label\": \"Marital Status\",\n \"type\": \"select\",\n \"options\": [\n {\n \"label\": \"Married\",\n \"value\": \"married\"\n },\n {\n \"label\": \"Domestic Partner\",\n \"value\": \"partner\"\n },\n {\n \"label\": \"Single\",\n \"value\": \"single\"\n }\n ],\n \"default\": \"--select status--\",\n \"showIf\": true,\n \"model\": \"maritalStatus\",\n \"id\": \"marital-status\",\n \"validation\": null,\n \"width\": 3\n },\n {\n \"label\": \"Are you a resident of the state in which you are applying?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"stateResident\",\n \"id\": \"state-resident\",\n \"validation\": null,\n \"inline\": true,\n \"width\": 6\n },\n {\n \"label\": \"Currently Employed?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"employmentStatus\",\n \"id\": \"employment-status\",\n \"validation\": \"name\",\n \"inline\": true,\n \"width\": 5\n },\n {\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"Occupation\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"occupation\",\n \"id\": \"occupation\",\n \"validation\": \"name\",\n \"width\": 4\n },\n {\n \"label\": \"Job Title\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"jobTitle\",\n \"id\": \"job-title\",\n \"validation\": \"name\",\n \"width\": 4\n }\n ]\n ],\n \"showIf\": {\n \"or\": [\n {\n \"model\": \"household.primary.name.first\",\n \"value\": \"Sachin\"\n },\n {\n \"model\": \"stateResident\",\n \"value\": \"true\"\n }\n ]\n },\n \"id\": \"employment-group\"\n }\n ]\n}";
		//String s="{\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"Date of Birth\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.dob\",\n \"id\": \"date-of-birth\",\n \"validation\": \"date\",\n \"width\": 3\n },\n {\n \"label\": \"Age\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"age\",\n \"id\": \"age\",\n \"validation\": null,\n \"width\": 1\n },\n {\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": \"radio\",\n \"inline\": true,\n \"width\": 3\n },\n {\n \"label\": \"Tobacco User?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"tobacco\",\n \"id\": \"tobacco\",\n \"validation\": \"radio\",\n \"inline\": true,\n \"width\": 3\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"primary-dob-group\"\n }";
		//String s="{\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"Date of Birth\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.dob\",\n \"id\": \"date-of-birth\",\n \"validation\": [\"date\"],\n \"width\": 3\n },\n {\n \"label\": \"Age\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"age\",\n \"id\": \"age\",\n \"width\": 1\n },\n {\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": [\"radio\"],\n \"inline\": true,\n \"width\": 3\n },\n {\n \"label\": \"Tobacco User?\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Yes\",\n \"value\": \"true\"\n },\n {\n \"label\": \"No\",\n \"value\": \"false\"\n }\n ],\n \"showIf\": true,\n \"model\": \"tobacco\",\n \"id\": \"tobacco\",\n \"validation\": [\"radio\"],\n \"inline\": true,\n \"width\": 3\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"primary-dob-group\"\n }";
		//String s="{\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": [\n [\n {\n \"label\": \"FirstName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"validation\": [\"name\"],\n \"width\": 12,\n \"inputWidth\": \"md\"\n },\n {\n \"label\": \"MiddleInitial\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"middleName\",\n \"id\": \"middle-name\",\n \"validation\": [\"name\"],\n \"width\": 2,\n \"inputWidth\": \"sm\"\n },\n {\n \"label\": \"LastName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.last\",\n \"id\": \"last-name\",\n \"validation\": [\"name\"],\n \"width\": 4,\n \"inputWidth\": \"md\"\n }\n ]\n ],\n \"showIf\": true,\n \"id\": \"name-group\"\n }";
		//String s="{\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": \n [\n {\n \"label\": \"FirstName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"validation\": [\"name\"],\n \"width\": 12,\n \"inputWidth\": \"md\"\n },\n {\n \"label\": \"MiddleInitial\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"middleName\",\n \"id\": \"middle-name\",\n \"validation\": [\"name\"],\n \"width\": 2,\n \"inputWidth\": \"sm\"\n },\n {\n \"label\": \"LastName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.last\",\n \"id\": \"last-name\",\n \"validation\": [\"name\"],\n \"width\": 4,\n \"inputWidth\": \"md\"\n }\n ],\n \"showIf\": true,\n \"id\": \"name-group\"\n }";
		//String s="{\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": \n [\n {\n \"label\": \"FirstName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"validation\": [\"name\"],\n \"width\": 12,\n \"inputWidth\": \"md\"\n },{\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": [\"radio\"],\n \"inline\": true,\n \"width\": 3\n },\n{\n \"label\": \"Marital Status\",\n \"type\": \"select\",\n \"options\": [\n {\n \"label\": \"Married\",\n \"value\": \"married\"\n },\n {\n \"label\": \"Domestic Partner\",\n \"value\": \"partner\"\n },\n {\n \"label\": \"Single\",\n \"value\": \"single\"\n }\n ],\n \"default\": \"--select status--\",\n \"showIf\": true,\n \"model\": \"maritalStatus\",\n \"id\": \"marital-status\",\n \"width\": 3\n },{\n \"label\": \"MiddleInitial\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"middleName\",\n \"id\": \"middle-name\",\n \"validation\": [\"name\"],\n \"width\": 2,\n \"inputWidth\": \"sm\"\n },\n {\n \"label\": \"LastName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.last\",\n \"id\": \"last-name\",\n \"validation\": [\"name\"],\n \"width\": 4,\n \"inputWidth\": \"md\"\n }\n ],\n \"showIf\": true,\n \"id\": \"name-group\"\n }";
		String s="{\n \"label\": \"\",\n \"type\": \"group\",\n \"components\": \n [\n {\n \"label\": \"FirstName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.first\",\n \"id\": \"first-name\",\n \"validation\": [\"name\"],\n \"width\": 12,\n \"inputWidth\": \"md\"\n },{\n \"label\": \"Gender\",\n \"type\": \"radio\",\n \"options\": [\n {\n \"label\": \"Male\",\n \"value\": \"m\"\n },\n {\n \"label\": \"Female\",\n \"value\": \"f\"\n }\n ],\n \"showIf\": true,\n \"model\": \"gender\",\n \"id\": \"gender\",\n \"validation\": [\"radio\"],\n \"inline\": true,\n \"width\": 3\n },\n{\n \"label\": \"MaritalStatus\",\n \"type\": \"select\",\n \"options\": [\n {\n \"label\": \"Married\",\n \"value\": \"married\"\n },\n {\n \"label\": \"DomesticPartner\",\n \"value\": \"partner\"\n },\n {\n \"label\": \"Single\",\n \"value\": \"single\"\n }\n ],\n \"default\": \"--selectst\",\n \"showIf\": true,\n \"model\": \"maritalStatus\",\n \"id\": \"marital-status\",\n \"width\": 3\n },{\n \"label\": \"MiddleInitial\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"middleName\",\n \"id\": \"middle-name\",\n \"validation\": [\"name\"],\n \"width\": 2,\n \"inputWidth\": \"sm\"\n },\n {\n \"label\": \"LastName\",\n \"type\": \"text\",\n \"options\": null,\n \"showIf\": true,\n \"model\": \"household.primary.name.last\",\n \"id\": \"last-name\",\n \"validation\": [\"name\"],\n \"width\": 4,\n \"inputWidth\": \"md\"\n }\n ],\n \"showIf\": true,\n \"id\": \"name-group\"\n }";
		UIComponent comp = new GroupComponent();
		comp = comp.parse(s);
	}
	
	private void decorate(String json){
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		BaseComponent comp = gson.fromJson(json, BaseComponent.class);
		this.entity = comp.getEntity();
		this.label = comp.getLabel();
		this.model = comp.getModel();
		this.type = comp.getType();
		this.validation = comp.getValidation();
		//this.components = comp.components;
	}

	@Override
	public String format() {
		Gson gson = (Gson) GHIXApplicationContext.getBean("platformGson");
		
		return gson.toJson(this);
	}

	@Override
	public List<String> compile(String json) {
		parse(json);
		List<String> errors = new ArrayList<String>();
		try {
			compileSummary(errors, UIValidationRule.REQUIRED, "label", this.label);
			compileSummary(errors, UIValidationRule.REQUIRED, "model", this.model);
			compileSummary(errors, UIValidationRule.REQUIRED, "type", this.type.name());
			compileSummary(errors, UIValidationRule.REQUIRED, "components", this.components); 
		}catch (Exception e) {
			e.printStackTrace();
			errors.add("Runtime error, contact dev");
		}
		return errors;
	}

}
