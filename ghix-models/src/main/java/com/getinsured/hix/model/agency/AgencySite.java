package com.getinsured.hix.model.agency;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SortNatural;

import com.getinsured.hix.model.Location;
import com.getinsured.timeshift.TimeShifterUtil;



/**
 * The persistent class for the agency_site database table.
 * 
 */
@Entity
@Table(name="agency_site")
@NamedQuery(name="AgencySite.findAll", query="SELECT a FROM AgencySite a")
public class AgencySite implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum SiteType {
		PRIMARY("Primary"),
		SUBSITE("Subsite");
		
		String value = null;
		SiteType(String value){
			this.value = value;
		}
		public String getValue(){
			return value;
		}
	}

	@Id
	@SequenceGenerator(name="AGENCY_SITE_ID_GENERATOR", sequenceName="AGENCY_SITE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AGENCY_SITE_ID_GENERATOR")
	private long id;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="creation_timestamp")
	private Timestamp creationTimestamp;

	@Column(name="email_address")
	private String emailAddress;

	@Column(name="last_update_timestamp")
	private Timestamp lastUpdateTimestamp;

	@Column(name="last_updated_by")
	private Long lastUpdatedBy;

	@OneToOne(cascade = { CascadeType.ALL }) 
	@JoinColumn(name="location_id")
	private Location location;

	@Column(name="phone_number")
	private String phoneNumber;

	@Column(name="site_location_name")
	private String siteLocationName;

	@Column(name="site_type")
	private String siteType;

	//uni-directional many-to-one association to Agency
	@ManyToOne
	@JoinColumn(name="agency_id")
	private Agency agency;
	
	//bi-directional many-to-one association to AgencySiteHour
	@SortNatural
	@OneToMany(mappedBy="agencySite",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private Set<AgencySiteHour> agencySiteHours;

	public AgencySite() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Long getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location locationId) {
		this.location = locationId;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSiteLocationName() {
		return this.siteLocationName;
	}

	public void setSiteLocationName(String siteLocationName) {
		this.siteLocationName = siteLocationName;
	}

	public String getSiteType() {
		return this.siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Agency getAgency() {
		return this.agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}
	
	public Set<AgencySiteHour> getAgencySiteHours() {
		return this.agencySiteHours;
	}

	public void setAgencySiteHours(Set<AgencySiteHour> agencySiteHours) {
		this.agencySiteHours = agencySiteHours;
	}
	
	
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}

	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
	}


}
