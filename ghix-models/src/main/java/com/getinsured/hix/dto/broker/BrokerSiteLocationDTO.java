package com.getinsured.hix.dto.broker;

import java.io.Serializable;
import java.util.List;

/**
 * Broker data transfer class.
 * 
 * @author kanthi_v
 * 
 */
public class BrokerSiteLocationDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private long siteId;
	
    

	public BrokerSiteLocationDTO() {	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getSiteId() {
		return siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}

	@Override
	public String toString() {
		return "BrokerSiteLocationDTO details: Id = "+id;
	}

}
