#!/bin/bash
echo " "
echo "Make sure jave version is 1.8 or above "
echo 

if [ $# -ne 1 ]
then
  echo " "
  echo "Usage $0 <profile in low-case>"
  echo "Usage $0 [nm|ms|id|phix|medicare][ca]"
  echo "Example: $0 nm"
  echo " "
  exit 2
fi

echo $1

if [ "$1" == "ca" ]
then
  echo " "
  echo "do special task for CA upgrade"

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml clearCheckSums

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_$1.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_ca2.0_upgrade.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog.xml update
 
  echo " "
fi

###
java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1.xml releaseLocks 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_pre_new_$1.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new.xml clearCheckSums 

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_pre_new_$1.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_configuration_new_$1.xml update

java -Djava.security.egd=file:///dev/urandom -jar ../lib/ghix-liquibase.jar --defaultsFile=../../conf/liquibase.properties --logLevel=info  --classpath=../../../ghix-setup.war --changeLogFile=META-INF/master.changelog_new.xml update



