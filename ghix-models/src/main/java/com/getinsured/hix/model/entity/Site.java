package com.getinsured.hix.model.entity;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for table EE_SITES, to save sites information.
 * 
 */
@Entity
@Table(name = "EE_SITES")
public class Site implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final int PRIME = 31;
	public enum site_type { 
		PRIMARY, SUBSITE; 
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EE_SITES_SEQ")
	@SequenceGenerator(name = "EE_SITES_SEQ", sequenceName = "EE_SITES_SEQ", allocationSize = 1)
	private int id;

	@OneToOne
	@JoinColumn(name = "EE_ENTITY_ID")
	private EnrollmentEntity entity;

	@NotEmpty(message="{label.entername}")
	@Size(max=100)
	@Column(name = "SITE_LOCATION_NAME")
	private String siteLocationName;
	
	//Pattern is required, because original @Email validation doesn't work as expected ^$
	@Pattern(regexp="^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$|^$")
	@Email(message="{label.validatePleaseEnterValidEmail}")
	@Size(max=100)
	@Column(name = "PRIMARY_EMAIL_ADDRESS")
	private String primaryEmailAddress;

	
	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="{label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9}")
	@Column(name = "PRIMARY_PHONE_NUMBER")
	private String primaryPhoneNumber;

	@Pattern(regexp="(^[1-9]{1}[0-9]{9}$)|(^$)",message="{label.validateSecondaryPhoneNo}")
	@Column(name = "SECONDARY_PHONE_NUMBER")
	private String secondaryPhoneNumber;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "MAILING_LOCATION_ID")
	private EntityLocation mailingLocation;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "PHYSICAL_LOCATION_ID")
	private EntityLocation physicalLocation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;

	@Column(name = "CREATED_BY")
	private Long createdBy;

	@Column(name = "LAST_UPDATED_BY")
	private Long updatedBy;
	
	@Column(name="SITE_TYPE")
	private String siteType;
	public Site() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EnrollmentEntity getEntity() {
		return entity;
	}

	public void setEntity(EnrollmentEntity entity) {
		this.entity = entity;
	}

	public String getSiteLocationName() {
		return siteLocationName;
	}

	public void setSiteLocationName(String primarySiteLocationName) {
		this.siteLocationName = primarySiteLocationName;
	}

	public String getPrimaryEmailAddress() {
		return primaryEmailAddress;
	}

	public void setPrimaryEmailAddress(String primaryEmailAddress) {
		this.primaryEmailAddress = primaryEmailAddress;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}

	public EntityLocation getMailingLocation() {
		return mailingLocation;
	}

	public void setMailingLocation(EntityLocation mailingLocation) {
		this.mailingLocation = mailingLocation;
	}

	public EntityLocation getPhysicalLocation() {
		return physicalLocation;
	}

	public void setPhysicalLocation(EntityLocation physicalLocation) {
		this.physicalLocation = physicalLocation;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setUpdatedOn(new TSDate());
	}

	@Override
	public String toString() {
		return "Site details: ID = "+id+", SiteType = "+siteType+", " +
						"EnrollmentEntity: ["+((entity!=null)?("EntityID = "+entity.getId()):"")+"]";
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 1;
		result = PRIME * result + ((entity == null) ? 0 : entity.hashCode());
		result = PRIME * result + id;
		result = PRIME * result
				+ ((createdBy == null) ? 0 : createdBy.hashCode());
		result = PRIME * result
				+ ((createdOn == null) ? 0 : createdOn.hashCode());
		
		result = PRIME
				* result
				+ ((siteLocationName == null) ? 0 : siteLocationName.hashCode());
		result = PRIME * result
				+ ((siteType == null) ? 0 : siteType.hashCode());
		result = PRIME * result
				+ ((updatedBy == null) ? 0 : updatedBy.hashCode());
		result = PRIME * result
				+ ((updatedOn == null) ? 0 : updatedOn.hashCode());
		result = hashCommunicationInfo(result);
		
		return result;
	}

	/**
	 * @param result
	 * @return
	 */
	private int hashCommunicationInfo(int result) {
		int computedResult = result;
		computedResult = PRIME * computedResult
				+ ((mailingLocation == null) ? 0 : mailingLocation.hashCode());
		computedResult = PRIME
				* computedResult
				+ ((physicalLocation == null) ? 0 : physicalLocation.hashCode());
		computedResult = PRIME
				* computedResult
				+ ((primaryEmailAddress == null) ? 0 : primaryEmailAddress
						.hashCode());
		computedResult = PRIME
				* computedResult
				+ ((primaryPhoneNumber == null) ? 0 : primaryPhoneNumber
						.hashCode());
		computedResult = PRIME
				* computedResult
				+ ((secondaryPhoneNumber == null) ? 0 : secondaryPhoneNumber
						.hashCode());
		return computedResult;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Site)) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		Site other = (Site) obj;
		return checkSiteData(other);
	}

	/**
	 * Method to check current object's site data for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSiteData(Site other) {
		return (checkLocationAndSiteData(other) && checkEntityAndCommunicationData(other) && checkIdCreationAndUpdateData(other));
	}

	/**
	 * Method to check current object's id, creation and update date for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkIdCreationAndUpdateData(Site other) {
		return (checkId(other) && checkCreationData(other) && checkUpdateData(other));
	}

	/**
	 * Method to check current object's entity and communication data for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntityAndCommunicationData(Site other) {
		return (checkEntity(other) && checkPrimaryEmailAddress(other) && checkPhoneNumber(other));
	}

	/**
	 * Method to check current object's location and site data for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLocationAndSiteData(Site other) {
		return (checkLocationData(other) && checkSiteLocationName(other) && checkSiteType(other));
	}

	/**
	 * Method to check current object's attribute 'entity' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkEntity(Site other) {
		if (entity == null) {
			if (other.entity != null) {
				return false;
			}
		} else if (!entity.equals(other.entity)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'id' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkId(Site other) {
		if (id != other.id) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's location data for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkLocationData(Site other) {
		return (checkMailingLocation(other) && checkPhysicalLocation(other));
	}

	/**
	 * Method to check current object's attribute 'physicalLocation' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPhysicalLocation(Site other) {
		if (physicalLocation == null) {
			if (other.physicalLocation != null) {
				return false;
			}
		} else if (!physicalLocation.equals(other.physicalLocation)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'mailingLocation' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkMailingLocation(Site other) {
		if (mailingLocation == null) {
			if (other.mailingLocation != null) {
				return false;
			}
		} else if (!mailingLocation.equals(other.mailingLocation)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'primaryEmailAddress' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryEmailAddress(Site other) {
		if (primaryEmailAddress == null) {
			if (other.primaryEmailAddress != null) {
				return false;
			}
		} else if (!primaryEmailAddress.equals(other.primaryEmailAddress)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's phone number for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPhoneNumber(Site other) {
		return (checkPrimaryPhoneNumber(other) && checkSecondaryPhoneNumber(other));
	}

	/**
	 * Method to check current object's attribute 'primaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkPrimaryPhoneNumber(Site other) {
		if (primaryPhoneNumber == null) {
			if (other.primaryPhoneNumber != null) {
				return false;
			}
		} else if (!primaryPhoneNumber.equals(other.primaryPhoneNumber)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Method to check current object's attribute 'secondaryPhoneNumber' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSecondaryPhoneNumber(Site other) {
		if (secondaryPhoneNumber == null) {
			if (other.secondaryPhoneNumber != null) {
				return false;
			}
		} else if (!secondaryPhoneNumber.equals(other.secondaryPhoneNumber)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'siteLocationName' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSiteLocationName(Site other) {
		if (siteLocationName == null) {
			if (other.siteLocationName != null) {
				return false;
			}
		} else if (!siteLocationName.equals(other.siteLocationName)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'siteType' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkSiteType(Site other) {
		if (siteType == null) {
			if (other.siteType != null) {
				return false;
			}
		} else if (!siteType.equals(other.siteType)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's update data for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdateData(Site other) {
		return (checkUpdatedBy(other) && checkUpdatedOn(other));
	}

	/**
	 * Method to check current object's attribute 'updatedOn' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdatedOn(Site other) {
		if (updatedOn == null) {
			if (other.updatedOn != null) {
				return false;
			}
		} else if (!updatedOn.equals(other.updatedOn)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'updatedBy' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkUpdatedBy(Site other) {
		if (updatedBy == null) {
			if (other.updatedBy != null) {
				return false;
			}
		} else if (!updatedBy.equals(other.updatedBy)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's creation data for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreationData(Site other) {
		return (checkCreatedBy(other) && checkCreatedOn(other));
	}

	/**
	 * Method to check current object's attribute 'createdBy' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreatedBy(Site other) {
		if (createdBy == null) {
			if (other.createdBy != null) {
				return false;
			}
		} else if (!createdBy.equals(other.createdBy)) {
			return false;
		}
		return true;
	}

	/**
	 * Method to check current object's attribute 'createdOn' for equality with given object.
	 * 
	 * @param other The other Site instance.
	 * @return Boolean The equality status.
	 */
	private boolean checkCreatedOn(Site other) {
		if (createdOn == null) {
			if (other.createdOn != null) {
				return false;
			}
		} else if (!createdOn.equals(other.createdOn)) {
			return false;
		}
		return true;
	}

}
