
  CREATE TABLE TKM_QUEUES 
   (	TKM_QUEUE_ID NUMBER, 
	QUEUE_NAME VARCHAR2(100), 
	ROLE_ID NUMBER, 
	CREATION_TIMESTAMP TIMESTAMP (6), 
	CREATED_BY NUMBER, 
	LAST_UPDATE_TIMESTAMP TIMESTAMP (6), 
	LAST_UPDATED_BY NUMBER, 
	IS_DEFAULT VARCHAR2(1) DEFAULT 'Y'
   ) ;

   COMMENT ON COLUMN TKM_QUEUES.TKM_QUEUE_ID IS 'Primary Key (surrogate key)';
   COMMENT ON COLUMN TKM_QUEUES.QUEUE_NAME IS 'Queue name.  It mpas to one of the actitity workflow (workflow engine) queues. The combination of QUEUE_NAME and ROLE_ID is unqiue in the entity/table.';
   COMMENT ON COLUMN TKM_QUEUES.ROLE_ID IS 'Role Id. It is from the user role entity.  The combination of QUEUE_NAME and ROLE_ID is unqiue in the entity/table.';
   COMMENT ON COLUMN TKM_QUEUES.CREATION_TIMESTAMP IS 'Creation timestamp of the record';
   COMMENT ON COLUMN TKM_QUEUES.CREATED_BY IS 'User (user id) who created this record';
   COMMENT ON COLUMN TKM_QUEUES.LAST_UPDATE_TIMESTAMP IS 'Timestamp of the last update';
   COMMENT ON COLUMN TKM_QUEUES.LAST_UPDATED_BY IS 'User (user id) did the last update';
   COMMENT ON COLUMN TKM_QUEUES.IS_DEFAULT IS 'Default queue for the user';
   COMMENT ON TABLE TKM_QUEUES  IS 'The entity holds the information of the queues and their respective user groups.  Through ROLE_ID, it will identify the group of users.';

  CREATE UNIQUE INDEX UK_TKM_QUEUES ON TKM_QUEUES (QUEUE_NAME, ROLE_ID) 
  ;

  CREATE UNIQUE INDEX PK_TKM_QUEUE ON TKM_QUEUES (TKM_QUEUE_ID) 
  ;

  ALTER TABLE TKM_QUEUES ADD CONSTRAINT PK_TKM_QUEUE PRIMARY KEY (TKM_QUEUE_ID) ENABLE;
  ALTER TABLE TKM_QUEUES MODIFY (TKM_QUEUE_ID NOT NULL ENABLE);
  ALTER TABLE TKM_QUEUES MODIFY (IS_DEFAULT NOT NULL ENABLE);

