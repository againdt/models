
package com.serff.planmanagementexchangeapi.exchange.model.pm;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.serff.planmanagementexchangeapi.exchange.model.pm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Insurer_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/pm", "insurer");
    private final static QName _ActuarialValue_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/pm", "actuarialValue");
    private final static QName _Plan_QNAME = new QName("http://www.serff.com/planManagementExchangeApi/exchange/model/pm", "plan");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.serff.planmanagementexchangeapi.exchange.model.pm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccreditationInfo }
     * 
     */
    public AccreditationInfo createAccreditationInfo() {
        return new AccreditationInfo();
    }

    /**
     * Create an instance of {@link TransferDataTemplates }
     * 
     */
    public TransferDataTemplates createTransferDataTemplates() {
        return new TransferDataTemplates();
    }

    /**
     * Create an instance of {@link DataTemplate }
     * 
     */
    public DataTemplate createDataTemplate() {
        return new DataTemplate();
    }

    /**
     * Create an instance of {@link TransferDataTemplate }
     * 
     */
    public TransferDataTemplate createTransferDataTemplate() {
        return new TransferDataTemplate();
    }

    /**
     * Create an instance of {@link InsurerAttachment }
     * 
     */
    public InsurerAttachment createInsurerAttachment() {
        return new InsurerAttachment();
    }

    /**
     * Create an instance of {@link NcqaInfo }
     * 
     */
    public NcqaInfo createNcqaInfo() {
        return new NcqaInfo();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link DataTemplates }
     * 
     */
    public DataTemplates createDataTemplates() {
        return new DataTemplates();
    }

    /**
     * Create an instance of {@link UracInfo }
     * 
     */
    public UracInfo createUracInfo() {
        return new UracInfo();
    }

    /**
     * Create an instance of {@link Plan }
     * 
     */
    public Plan createPlan() {
        return new Plan();
    }

    /**
     * Create an instance of {@link CompanyInfo }
     * 
     */
    public CompanyInfo createCompanyInfo() {
        return new CompanyInfo();
    }

    /**
     * Create an instance of {@link ExchangeAttachment }
     * 
     */
    public ExchangeAttachment createExchangeAttachment() {
        return new ExchangeAttachment();
    }

    /**
     * Create an instance of {@link Insurer }
     * 
     */
    public Insurer createInsurer() {
        return new Insurer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Insurer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/pm", name = "insurer")
    public JAXBElement<Insurer> createInsurer(Insurer value) {
        return new JAXBElement<Insurer>(_Insurer_QNAME, Insurer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/pm", name = "actuarialValue")
    public JAXBElement<BigDecimal> createActuarialValue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ActuarialValue_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Plan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.serff.com/planManagementExchangeApi/exchange/model/pm", name = "plan")
    public JAXBElement<Plan> createPlan(Plan value) {
        return new JAXBElement<Plan>(_Plan_QNAME, Plan.class, null, value);
    }

}
