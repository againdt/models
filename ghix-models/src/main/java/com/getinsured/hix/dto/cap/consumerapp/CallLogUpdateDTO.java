package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;

public class CallLogUpdateDTO implements Serializable {	
	private static final long serialVersionUID = -9037560569227277504L;
	public Integer eligLeadId;
	public Integer householdId;
	public Integer usersId;
	public String callId;
	
	public CallLogUpdateDTO(){}

	public CallLogUpdateDTO(Integer eligLeadId, Integer householdId,
			Integer usersId, String callId) {
		super();
		this.eligLeadId = eligLeadId;
		this.householdId = householdId;
		this.usersId = usersId;
		this.callId = callId;
	}


	public Integer getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(Integer eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

	public Integer getUsersId() {
		return usersId;
	}

	public void setUsersId(Integer usersId) {
		this.usersId = usersId;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}
	
}
