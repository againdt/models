package com.getinsured.hix.consumer.history;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


 


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({ @Type(value = ConsumerGenericEventHistory.class, name = "Generic"), @Type(value = ConsumerEnrollEventHistory.class, name = "Enrollment") })

public abstract class ConsumerEventMetaData  {
	
private String type;
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
		
}
