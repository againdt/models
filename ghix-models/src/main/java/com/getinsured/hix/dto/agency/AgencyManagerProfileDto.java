package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class AgencyManagerProfileDto implements Serializable,Comparable<AgencyManagerProfileDto> {

	private static final long serialVersionUID = 1L;
	private String brokerId; 
	private String agencyContactEmailId;
	private String agencyContactPhoneNumber;
	private boolean isAgencyManager;
	private String agencyManagerFirstName;
	private String agencyManagerMiddleName;
	private String agencyManagerLastName;
	
	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public String getAgencyContactEmailId() {
		return agencyContactEmailId;
	}

	public void setAgencyContactEmailId(String agencyContactEmailId) {
		this.agencyContactEmailId = agencyContactEmailId;
	}

	public String getAgencyContactPhoneNumber() {
		return agencyContactPhoneNumber;
	}

	public void setAgencyContactPhoneNumber(String agencyContactPhoneNumber) {
		this.agencyContactPhoneNumber = agencyContactPhoneNumber;
	}

	public String getAgencyManagerFirstName() {
		return agencyManagerFirstName;
	}

	public void setAgencyManagerFirstName(String agencyManagerFirstName) {
		this.agencyManagerFirstName = agencyManagerFirstName;
	}

	public String getAgencyManagerMiddleName() {
		return agencyManagerMiddleName;
	}

	public void setAgencyManagerMiddleName(String agencyManagerMiddleName) {
		this.agencyManagerMiddleName = agencyManagerMiddleName;
	}

	public String getAgencyManagerLastName() {
		return agencyManagerLastName;
	}

	public void setAgencyManagerLastName(String agencyManagerLastName) {
		this.agencyManagerLastName = agencyManagerLastName;
	}
	
	public boolean isAgencyManager() {
		return isAgencyManager;
	}

	public void setAgencyManager(boolean isAgencyManager) {
		this.isAgencyManager = isAgencyManager;
	}

	@Override
	public int compareTo(AgencyManagerProfileDto agencyManagerProfileDto) {
		 int value = this.getAgencyManagerFirstName().compareTo(((AgencyManagerProfileDto) agencyManagerProfileDto).getAgencyManagerFirstName());
		 if(value != 0){
			 return value;
		 }
		 return this.getAgencyManagerLastName().compareTo(((AgencyManagerProfileDto) agencyManagerProfileDto).getAgencyManagerLastName());
	}
}
