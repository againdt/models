/**
 * 
 * @author santanu
 * @version 1.0
 * @since May 02, 2016 
 * 
 * This DTO for Life plan response 
 */

package com.getinsured.hix.dto.planmgmt;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import com.getinsured.hix.model.GHIXResponse;

@Component("LifePlanResponseDTO")
public class LifePlanResponseDTO  extends GHIXResponse implements Serializable {
	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private List<LifePlanDTO> lifePlanList;

	/**
	 * @return the lifePlanList
	 */
	public List<LifePlanDTO> getLifePlanList() {
		return lifePlanList;
	}

	/**
	 * @param lifePlanList the lifePlanList to set
	 */
	public void setLifePlanList(List<LifePlanDTO> lifePlanList) {
		this.lifePlanList = lifePlanList;
	}
	
	
}
