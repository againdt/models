package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;

public class EnrollmentResendDTO implements Serializable{

	private String ffmDifConfirmation;
	private String enrollmentType;
	private List<Enrollment> enrollmentList;
	private String leadId;
	private String changeInPremium;
	private String changeInPercentage;
	private String isDifferenceInGIAndFFM;
	private String giPremiumAmount;
	private String ffmPremiumAmount;
	private String ffmAptcAmount;
	private Integer ffmLoggedInUserId;
	private String isUserSwitchToOtherView;
	
	public Integer getFfmLoggedInUserId() {
		return ffmLoggedInUserId;
	}
	public void setFfmLoggedInUserId(Integer ffmLoggedInUserId) {
		this.ffmLoggedInUserId = ffmLoggedInUserId;
	}
	public String getFfmDifConfirmation() {
		return ffmDifConfirmation;
	}
	public void setFfmDifConfirmation(String ffmDifConfirmation) {
		this.ffmDifConfirmation = ffmDifConfirmation;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	
	public List<Enrollment> getEnrollmentList() {
		return enrollmentList;
	}
	public void setEnrollmentList(List<Enrollment> enrollmentList) {
		this.enrollmentList = enrollmentList;
	}
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getChangeInPremium() {
		return changeInPremium;
	}
	public void setChangeInPremium(String changeInPremium) {
		this.changeInPremium = changeInPremium;
	}
	public String getChangeInPercentage() {
		return changeInPercentage;
	}
	public void setChangeInPercentage(String changeInPercentage) {
		this.changeInPercentage = changeInPercentage;
	}
	public String getIsDifferenceInGIAndFFM() {
		return isDifferenceInGIAndFFM;
	}
	public void setIsDifferenceInGIAndFFM(String isDifferenceInGIAndFFM) {
		this.isDifferenceInGIAndFFM = isDifferenceInGIAndFFM;
	}
	public String getGiPremiumAmount() {
		return giPremiumAmount;
	}
	public void setGiPremiumAmount(String giPremiumAmount) {
		this.giPremiumAmount = giPremiumAmount;
	}
	public String getFfmPremiumAmount() {
		return ffmPremiumAmount;
	}
	public void setFfmPremiumAmount(String ffmPremiumAmount) {
		this.ffmPremiumAmount = ffmPremiumAmount;
	}
	public String getFfmAptcAmount() {
		return ffmAptcAmount;
	}
	public void setFfmAptcAmount(String ffmAptcAmount) {
		this.ffmAptcAmount = ffmAptcAmount;
	}
	
	public String getIsUserSwitchToOtherView() {
		return isUserSwitchToOtherView;
	}
	public void setIsUserSwitchToOtherView(String isUserSwitchToOtherView) {
		this.isUserSwitchToOtherView = isUserSwitchToOtherView;
	}
	@Override
	public String toString() {
		return "EnrollmentResendDTO [ffmDifConfirmation=" + ffmDifConfirmation
				+ ", enrollmentType=" + enrollmentType + ", enrollmentList="
				+ enrollmentList + ", leadId=" + leadId + ", changeInPremium="
				+ changeInPremium + ", changeInPercentage="
				+ changeInPercentage + ", isDifferenceInGIAndFFM="
				+ isDifferenceInGIAndFFM + ", giPremiumAmount="
				+ giPremiumAmount + ", ffmPremiumAmount=" + ffmPremiumAmount
				+ ", ffmAptcAmount=" + ffmAptcAmount + "]";
	}
	
}
