package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.groups.Default;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The persistent class for the payment_methods database table.
 * 
 */

@Audited
@Entity
@Table(name = "payment_methods")
public class PaymentMethods implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum PaymentType {
		CREDITCARD, BANK, MANUAL,CHECK,EFT, ACH;
	}
	
	public enum PaymentStatus
	{
		Active, InActive;
	}
	
	public enum PaymentIsDefault
	{
		Y, N;
	}
	/**
	 * 
	 * Add module name in this enum if it does not exists
	 */
	public enum ModuleName
	{
		EMPLOYER, EMPLOYEE, ISSUER, BROKER, ISSUER_REPRESENTATIVE, ADMIN, INDIVIDUAL, ASSISTERENROLLMENTENTITYADMIN, ASSISTER,
		ASSISTERENROLLMENTENTITY;
	}
	
	public enum SubscriptionType
	{
		NORMAL, RECURRING
	}

	/**
	 * This validation group provides groupings for fields  {@link #paymentMethodName}, 
	 *
	 * during editing Company Profile
	 * @author kunal
	 *
	 */
	public interface EditPaymentMethod extends Default{
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_methods_seq")
	@SequenceGenerator(name = "payment_methods_seq", sequenceName = "payment_methods_seq", allocationSize = 1)
	private int id;
	
	/*@ManyToOne
	@JoinColumn(name = "module_user_id")
	private ModuleUser moduleUser;*/

	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "financial_info_id")
	private FinancialInfo financialInfo;

	@Column(name = "payment_type", length = 20)
	@Enumerated(EnumType.STRING)
	private PaymentType paymentType;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private PaymentStatus status;
	
	@Column(name = "is_default")
	@Enumerated(EnumType.STRING)
	private PaymentIsDefault isDefault;
	
	@NotEmpty(message="{err.paymentMethodName}",groups={PaymentMethods.EditPaymentMethod.class})
	@Column(name = "payment_method_name")
	private String paymentMethodName;
	
	@Column(name="module_id")
	private int moduleId;

	@Column(name="module_name")
	@Enumerated(EnumType.STRING)
	private ModuleName moduleName;
	
	@Column(name="subscription_id")
	private String subscriptionId;
	
	/* since already test data might be exist in database table and hence 'nullable = false' is not kept in @Column */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP")
	private Date createdOn;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP")
	private Date updatedOn;
	
	@Column(name="subscription_type")
	@Enumerated(EnumType.STRING)
	private SubscriptionType subscriptionType;
	
	@Column(name = "last_updated_by")
	private Integer lastUpdatedBy;
	
	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist(){
		this.setCreatedOn(new TSDate());
		this.setUpdatedOn(new TSDate());
	}
	
	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate(){
		this.setUpdatedOn(new TSDate());
	}
	
	/***
	 * need to be removed
	 */
	/*@ManyToOne
	@JoinColumn(name = "employer_id")
	private Employer employer;*/
	
	public PaymentMethods() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public FinancialInfo getFinancialInfo() {
		return financialInfo;
	}

	public void setFinancialInfo(FinancialInfo financialInfo) {
		this.financialInfo = financialInfo;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public PaymentIsDefault getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(PaymentIsDefault isDefault) {
		this.isDefault = isDefault;
	}
	
	public String getPaymentMethodName()
	{
		return paymentMethodName;
	}
	
	public void setPaymentMethodName(String payment_method_name)
	{
		this.paymentMethodName = payment_method_name;
	}
	
	public int getModuleId()
	{
		return moduleId;
	}
	
	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}
	
	public ModuleName getModuleName() {
		return moduleName;
	}

	public void setModuleName(ModuleName moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
	public String toString() {
		return "PaymentMethods [id=" + id + ", financialInfo=" + financialInfo
				+ ", paymentType=" + paymentType + ", status=" + status
				+ ", isDefault=" + isDefault + ", paymentMethodName="
				+ paymentMethodName + ", moduleId=" + moduleId
				+ ", moduleName=" + moduleName + ", subscriptionId="
				+ subscriptionId + "]";
	}
}
