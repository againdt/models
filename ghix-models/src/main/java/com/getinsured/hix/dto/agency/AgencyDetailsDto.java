package com.getinsured.hix.dto.agency;

import java.io.Serializable;

public class AgencyDetailsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String encryptedId;
	private String agencyName;
	private String businessLegalName;
	private String certificationStatus;
	private String submittedOn;
	private String certifiedOn;

	public AgencyDetailsDto() {	
	}

	public String getEncryptedId() {
		return encryptedId;
	}

	public void setEncryptedId(String encryptedId) {
		this.encryptedId = encryptedId;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getBusinessLegalName() {
		return businessLegalName;
	}

	public void setBusinessLegalName(String businessLegalName) {
		this.businessLegalName = businessLegalName;
	}

	public String getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(String submittedOn) {
		this.submittedOn = submittedOn;
	}

	public String getCertifiedOn() {
		return certifiedOn;
	}

	public void setCertifiedOn(String certifiedOn) {
		this.certifiedOn = certifiedOn;
	}

	public String getCertificationStatus() {
		return certificationStatus;
	}

	public void setCertificationStatus(String certificationStatus) {
		this.certificationStatus = certificationStatus;
	}
	
	@Override
	public String toString() {
		return "AgencyDetailsDto details: Id = "+encryptedId;
	}

}
