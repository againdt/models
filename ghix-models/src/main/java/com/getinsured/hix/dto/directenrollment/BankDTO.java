package com.getinsured.hix.dto.directenrollment;

/**
 * Bank details
 * 
 * @author root
 *
 */
public class BankDTO {
	private String type;
	private String name;
	private String routing;
	private String account;
	private int withdrawDay;
	
	/**
	 * address details for 
	 */
	private String addrStreet;
	private String addrCity;
	private String addrState;
	private String addrZip;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getWithdrawDay() {
		return withdrawDay;
	}

	public void setWithdrawDay(int withdrawDay) {
		this.withdrawDay = withdrawDay;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
		public String getAddrStreet() {
		return addrStreet;
	}

	public void setAddrStreet(String addrStreet) {
		this.addrStreet = addrStreet;
	}

	public String getAddrCity() {
		return addrCity;
	}

	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}

	public String getAddrState() {
		return addrState;
	}

	public void setAddrState(String addrState) {
		this.addrState = addrState;
	}

	public String getAddrZip() {
		return addrZip;
	}

	public void setAddrZip(String addrZip) {
		this.addrZip = addrZip;
	}

	@Override
	public String toString() {
		return "BankDTO [type=" + type + ", name=" + name + ", routing="
				+ routing + ", account=" + account + ", withdrawDay="
				+ withdrawDay + "]";
	}
	


}
