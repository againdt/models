package com.getinsured.hix.model.consumer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name="CMR_HOUSEHOLD_ENROLLMENT")
@DynamicInsert
@DynamicUpdate
public class HouseholdEnrollment implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOUSEHOLD_ENROLLMENT_SEQ")
	@SequenceGenerator(name = "HOUSEHOLD_ENROLLMENT_SEQ", sequenceName = "HOUSEHOLD_ENROLLMENT_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="HOUSEHOLD_ID")
	private Integer householdId;

	@Column(name="ENROLLMENT_ID")
	private Integer enrollmentId;

	@Column(name="APPLICATION_ID")
	private Long applicationId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHouseholdId() {
		return householdId;
	}

	public void setHouseholdId(Integer householdId) {
		this.householdId = householdId;
	}

	public Integer getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	public String toString() {
		return "HouseholdEnrollment [id=" + id + ", householdId=" + householdId
				+ ", enrollmentId=" + enrollmentId + ", applicationId="
				+ applicationId + "]";
	}

}
