//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.24 at 10:29:08 AM IST 
//


package com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.structures._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityRequestType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ApplicantEligibilityResponseType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ExchangeUserType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceSubcriberMemberAssociationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.MyComplexEnumerationCodeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.ResponseType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.ApplicantType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.ApplicationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.InformationExchangeSystemType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsuranceMemberType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsurancePolicyType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.InsurancePremiumType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_ee.SSFPrimaryContactType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_pm.InsurancePlanVariantType;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_pm.IssuerType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ActivityType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.AddressType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.AssociationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.ContactInformationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.FullTelephoneNumberType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonLanguageType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.PersonType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.StatusType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.StreetType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.niem_core._2.TelephoneNumberType;



/**
 * <p>Java class for ComplexObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplexObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}id"/>
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}metadata"/>
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}linkMetadata"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexObjectType")
@XmlSeeAlso({
    ApplicantEligibilityRequestType.class,
    ResponseType.class,
    ContactInformationType.class,
    DateRangeType.class,
    InformationExchangeSystemType.class,
    ExchangeUserType.class,
    InsurancePolicyType.class,
    DateType.class,
    InsurancePremiumType.class,
    ApplicantEligibilityResponseType.class,
    InsuranceSubcriberMemberAssociationType.class,
    MyComplexEnumerationCodeType.class,
    InsuranceMemberType.class,
    TelephoneNumberType.class,
    PersonLanguageType.class,
    FrequencyType.class,
    ApplicantType.class,
    PersonNameType.class,
    FullTelephoneNumberType.class,
    AddressType.class,
    StreetType.class,
    StructuredAddressType.class,
    PersonType.class,
    AssociationType.class,
    StatusType.class,
    InsurancePlanType.class,
    EligibilityType.class,
    SSFPrimaryContactType.class,
    ActivityType.class,
    ApplicationType.class,
    IdentificationType.class,
    IssuerType.class,
    InsurancePlanVariantType.class
})
public abstract class ComplexObjectType {

    @XmlAttribute(namespace = "http://niem.gov/niem/structures/2.0")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(namespace = "http://niem.gov/niem/structures/2.0")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> metadata;
    @XmlAttribute(namespace = "http://niem.gov/niem/structures/2.0")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> linkMetadata;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getMetadata() {
        if (metadata == null) {
            metadata = new ArrayList<Object>();
        }
        return this.metadata;
    }

    /**
     * Gets the value of the linkMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getLinkMetadata() {
        if (linkMetadata == null) {
            linkMetadata = new ArrayList<Object>();
        }
        return this.linkMetadata;
    }

}
