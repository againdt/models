Insert into MENU_ITEMS (ID,PARENT_ID,CAPTION,URL,REDIRECT_URL,TITLE,OPTIONAL,IS_ACTIVE,PERMISSION_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values
(MENU_ITEMS_SEQ.NEXTVAL,null,'Providers',null,null,'Providers','N','Y',(select id from permissions where name ='MANAGE_ISSUER'),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

Insert into MENU_ITEMS (ID,PARENT_ID,CAPTION,URL,REDIRECT_URL,TITLE,OPTIONAL,IS_ACTIVE,PERMISSION_ID,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values 
(MENU_ITEMS_SEQ.NEXTVAL,(select id from menu_items where caption = 'Providers'),'Mapping Files','/hix/admin/provider/viewPlanNetworkMappingReport','/hix/admin/provider/viewPlanNetworkMappingReport','Mapping Files','N','Y',(select id from permissions where name ='MANAGE_ISSUER'),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

Insert into MENU_ROLES (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Providers'),(select id from roles where lower(name) ='admin' ),15,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

Insert into MENU_ROLES (ID,MENU_ITEMS_ID,ROLE_ID,MENU_ITEMS_ORDER,CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP) values ( MENU_ROLES_SEQ.NEXTVAL ,(
select id from menu_items where caption ='Mapping Files'),(select id from roles where lower(name) ='admin' ),15,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
