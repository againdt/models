package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.Date;

public class EnrolleeDetailsDto implements Serializable{
	private Integer enrolleeId;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private Float totalIndividualResponsibilityAmount;
	private String enrolleeStatus;
	/**
	 * @return the enrolleeId
	 */
	public Integer getEnrolleeId() {
		return enrolleeId;
	}
	/**
	 * @param enrolleeId the enrolleeId to set
	 */
	public void setEnrolleeId(Integer enrolleeId) {
		this.enrolleeId = enrolleeId;
	}
	/**
	 * @return the effectiveStartDate
	 */
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	/**
	 * @param effectiveStartDate the effectiveStartDate to set
	 */
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	/**
	 * @return the effectiveEndDate
	 */
	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}
	/**
	 * @param effectiveEndDate the effectiveEndDate to set
	 */
	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}
	/**
	 * @return the totalIndividualResponsibilityAmount
	 */
	public Float getTotalIndividualResponsibilityAmount() {
		return totalIndividualResponsibilityAmount;
	}
	/**
	 * @param totalIndividualResponsibilityAmount the totalIndividualResponsibilityAmount to set
	 */
	public void setTotalIndividualResponsibilityAmount(
			Float totalIndividualResponsibilityAmount) {
		this.totalIndividualResponsibilityAmount = totalIndividualResponsibilityAmount;
	}
	/**
	 * @return the enrolleeStatus
	 */
	public String getEnrolleeStatus() {
		return enrolleeStatus;
	}
	/**
	 * @param enrolleeStatus the enrolleeStatus to set
	 */
	public void setEnrolleeStatus(String enrolleeStatus) {
		this.enrolleeStatus = enrolleeStatus;
	}
	
}
