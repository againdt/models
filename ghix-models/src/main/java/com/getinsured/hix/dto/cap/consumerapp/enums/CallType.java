package com.getinsured.hix.dto.cap.consumerapp.enums;
 

public enum CallType {
	INBOUND("inbound"), OUTBOUND_AUTO("dialerOutbound"), OUTBOUND_MANUAL("manualOutbound"),OUTBOUND("outbound");
	
	private String description;

	private CallType(String description){
		this.description = description;
		
	}
	
	public String getDescription(){
		return description;
	}
}