package com.getinsured.hix.dto.enrollment.aca;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class App{
	@JsonProperty("zip")
	private Integer zip;

	@JsonProperty("annualIncome")
	private Integer annualIncome;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("fips")
	private Integer fips;

	@JsonProperty("stateCode")
	private String stateCode;

	@JsonProperty("people")
	private List<People> people;

	@JsonProperty("email")
	private String email;

	@JsonProperty("effectiveDate")
	private String effectiveDate;

	@JsonProperty("csr")
	private String csr;

	public void setZip(Integer zip){
		this.zip=zip;
	}
	public Integer getZip(){
		return zip;
	}
	public void setAnnualIncome(Integer annualIncome){
		this.annualIncome=annualIncome;
	}
	public Integer getAnnualIncome(){
		return annualIncome;
	}
	public void setPhone(String phone){
		this.phone=phone;
	}
	public String getPhone(){
		return phone;
	}
	public void setFips(Integer fips){
		this.fips=fips;
	}
	public Integer getFips(){
		return fips;
	}
	public void setStateCode(String stateCode){
		this.stateCode=stateCode;
	}
	public String getStateCode(){
		return stateCode;
	}
	public void setPeople(List<People> people){
		this.people=people;
	}
	public List<People> getPeople(){
		return people;
	}
	public void setEmail(String email){
		this.email=email;
	}
	public String getEmail(){
		return email;
	}
	public void setEffectiveDate(String effectiveDate){
		this.effectiveDate=effectiveDate;
	}
	public String getEffectiveDate(){
		return effectiveDate;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	
}