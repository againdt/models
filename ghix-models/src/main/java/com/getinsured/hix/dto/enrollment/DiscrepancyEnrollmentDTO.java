package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DiscrepancyEnrollmentDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer enrollmentId;
	private Integer issuerEnrollmentIdValue;
	private String issuerName;
	private Map<String, Object> benefitEffectiveStartDate;
	private Map<String, Object> benefitEffectiveEndDate;
	private Map<String, Object> homeAddress;
	private Map<String, Object> mailingAddress;
	private Map<String, Object> agentName;
	private Map<String, Object> brokerFEDTaxPayerId;
	private Map<String, Object> phoneNumber;
	private Map<String, Object> memberCount;
	private Map<String, Object> cmsPlanId;
	private Map<String, Object> isEffectuated;
	
	private List<DiscrepancyMemberDTO> discrepancyMemberDTOList;
	private List<DiscrepancyPremiumDTO> discrepancyPremiumDTOList;
	private List<String> discrepancyNotesList;
	
	private String encEnrollmentID;
	
	public Integer getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(Integer enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public Integer getIssuerEnrollmentIdValue() {
		return issuerEnrollmentIdValue;
	}
	public void setIssuerEnrollmentIdValue(Integer issuerEnrollmentIdValue) {
		this.issuerEnrollmentIdValue = issuerEnrollmentIdValue;
	}
	public Map<String, Object> getBenefitEffectiveStartDate() {
		return benefitEffectiveStartDate;
	}
	public void setBenefitEffectiveStartDate(Map<String, Object> benefitEffectiveStartDate) {
		this.benefitEffectiveStartDate = benefitEffectiveStartDate;
	}
	public Map<String, Object> getBenefitEffectiveEndDate() {
		return benefitEffectiveEndDate;
	}
	public void setBenefitEffectiveEndDate(Map<String, Object> benefitEffectiveEndDate) {
		this.benefitEffectiveEndDate = benefitEffectiveEndDate;
	}
	public Map<String, Object> getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(Map<String, Object> homeAddress) {
		this.homeAddress = homeAddress;
	}
	public Map<String, Object> getMailingAddress() {
		return mailingAddress;
	}
	public void setMailingAddress(Map<String, Object> mailingAddress) {
		this.mailingAddress = mailingAddress;
	}
	public Map<String, Object> getAgentName() {
		return agentName;
	}
	public void setAgentName(Map<String, Object> agentName) {
		this.agentName = agentName;
	}
	public Map<String, Object> getBrokerFEDTaxPayerId() {
		return brokerFEDTaxPayerId;
	}
	public void setBrokerFEDTaxPayerId(Map<String, Object> brokerFEDTaxPayerId) {
		this.brokerFEDTaxPayerId = brokerFEDTaxPayerId;
	}
	public Map<String, Object> getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Map<String, Object> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Map<String, Object> getMemberCount() {
		return memberCount;
	}
	public void setMemberCount(Map<String, Object> memberCount) {
		this.memberCount = memberCount;
	}
	public Map<String, Object> getCmsPlanId() {
		return cmsPlanId;
	}
	public void setCmsPlanId(Map<String, Object> cmsPlanId) {
		this.cmsPlanId = cmsPlanId;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public List<DiscrepancyMemberDTO> getDiscrepancyMemberDTOList() {
		return discrepancyMemberDTOList;
	}
	public void setDiscrepancyMemberDTOList(List<DiscrepancyMemberDTO> discrepancyMemberDTOList) {
		this.discrepancyMemberDTOList = discrepancyMemberDTOList;
	}
	public List<DiscrepancyPremiumDTO> getDiscrepancyPremiumDTOList() {
		return discrepancyPremiumDTOList;
	}
	public void setDiscrepancyPremiumDTOList(List<DiscrepancyPremiumDTO> discrepancyPremiumDTOList) {
		this.discrepancyPremiumDTOList = discrepancyPremiumDTOList;
	}
	public Map<String, Object> getIsEffectuated() {
		return isEffectuated;
	}
	public void setIsEffectuated(Map<String, Object> isEffectuated) {
		this.isEffectuated = isEffectuated;
	}
	public List<String> getDiscrepancyNotesList() {
		return discrepancyNotesList;
	}
	public void setDiscrepancyNotesList(List<String> discrepancyNotesList) {
		this.discrepancyNotesList = discrepancyNotesList;
	}
	public String getEncEnrollmentID() {
		return encEnrollmentID;
	}
	public void setEncEnrollmentID(String encEnrollmentID) {
		this.encEnrollmentID = encEnrollmentID;
	}
}
