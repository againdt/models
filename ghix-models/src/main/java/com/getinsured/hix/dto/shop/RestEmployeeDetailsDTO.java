package com.getinsured.hix.dto.shop;

import java.util.List;


public class RestEmployeeDetailsDTO {

	private Integer employeeId;
	private List<Integer> memberIds;
	private Integer employeeApplicationId;
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public List<Integer> getMemberIds() {
		return memberIds;
	}
	public void setMemberIds(List<Integer> memberIds) {
		this.memberIds = memberIds;
	}
	public Integer getEmployeeApplicationId() {
		return employeeApplicationId;
	}
	public void setEmployeeApplicationId(Integer employeeApplicationId) {
		this.employeeApplicationId = employeeApplicationId;
	}
}
