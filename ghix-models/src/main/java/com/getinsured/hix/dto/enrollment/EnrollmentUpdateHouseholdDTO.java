package com.getinsured.hix.dto.enrollment;

import java.io.Serializable;
import java.util.List;

public class EnrollmentUpdateHouseholdDTO implements Serializable{

	public enum Status {
		SUCCESS, FAILURE
	}

	private Integer cmrHouseholdId;
	private List<Long> ssapIdList;
	private Status status;
	private String errorMsg;
	private int updateCount;
	
	public int getUpdateCount() {
		return updateCount;
	}

	public void setUpdateCount(int updateCount) {
		this.updateCount = updateCount;
	}

	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public List<Long> getSsapIdList() {
		return ssapIdList;
	}

	public void setSsapIdList(List<Long> ssapIdList) {
		this.ssapIdList = ssapIdList;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
