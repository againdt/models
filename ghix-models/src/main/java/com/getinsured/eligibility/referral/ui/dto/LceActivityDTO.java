package com.getinsured.eligibility.referral.ui.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author raguram_p
 * 
 */
public class LceActivityDTO {

	private Map<String, SepEventDTO> addedMembersMap = new HashMap<String, SepEventDTO>();

	private Map<String, SepEventDTO> removedMembersMap = new HashMap<String, SepEventDTO>();

	private Map<String, SepEventDTO> householdChangeMembersMap = new HashMap<String, SepEventDTO>();

	private Map<String, SepEventDTO> changeOfAddressMembersMap = new HashMap<String, SepEventDTO>();

	private Map<String, SepEventDTO> noChangeMembersMap = new HashMap<String, SepEventDTO>();

	private Map<String, SepEventDTO> qualifyingEventMap = new HashMap<String, SepEventDTO>();
	
	private Map<String, SepEventDTO> changeOfDobMembersMap = new HashMap<String, SepEventDTO>();

	private String householdChangeReason;

	private String householdChangeEventDate;

	private long referralActivationId;

	private long ssapApplicationId;

	private String zipCountyChangeEventDate;

	private long userId;

	private boolean applicationEventExists;

	private String householdDisplayed;

	private String caseNumber;

	private String qualifyEventSelected;

	private String qualifyEventDate;

	private String qualifyEventAllowFuture;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getReferralActivationId() {
		return referralActivationId;
	}

	public void setReferralActivationId(long referralActivationId) {
		this.referralActivationId = referralActivationId;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getZipCountyChangeEventDate() {
		return zipCountyChangeEventDate;
	}

	public void setZipCountyChangeEventDate(String zipCountyChangeEventDate) {
		this.zipCountyChangeEventDate = zipCountyChangeEventDate;
	}

	public Map<String, SepEventDTO> getAddedMembersMap() {
		return addedMembersMap;
	}

	public void setAddedMembersMap(Map<String, SepEventDTO> addedMembersMap) {
		this.addedMembersMap = addedMembersMap;
	}

	public Map<String, SepEventDTO> getRemovedMembersMap() {
		return removedMembersMap;
	}

	public void setRemovedMembersMap(Map<String, SepEventDTO> removedMembersMap) {
		this.removedMembersMap = removedMembersMap;
	}

	public Map<String, SepEventDTO> getHouseholdChangeMembersMap() {
		return householdChangeMembersMap;
	}

	public void setHouseholdChangeMembersMap(Map<String, SepEventDTO> householdChangeMembersMap) {
		this.householdChangeMembersMap = householdChangeMembersMap;
	}

	public Map<String, SepEventDTO> getChangeOfAddressMembersMap() {
		return changeOfAddressMembersMap;
	}

	public void setChangeOfAddressMembersMap(Map<String, SepEventDTO> changeOfAddressMembersMap) {
		this.changeOfAddressMembersMap = changeOfAddressMembersMap;
	}
	
	public Map<String, SepEventDTO> getChangeOfDobMembersMap() {
		return changeOfDobMembersMap;
	}

	public void setChangeOfDobMembersMap(Map<String, SepEventDTO> dobChangeMembersMap) {
		this.changeOfDobMembersMap = dobChangeMembersMap;
	}

	public Map<String, SepEventDTO> getNoChangeMembersMap() {
		return noChangeMembersMap;
	}

	public void setNoChangeMembersMap(Map<String, SepEventDTO> noChangeMembersMap) {
		this.noChangeMembersMap = noChangeMembersMap;
	}

	public String getHouseholdChangeReason() {
		return householdChangeReason;
	}

	public void setHouseholdChangeReason(String householdChangeReason) {
		this.householdChangeReason = householdChangeReason;
	}

	public String getHouseholdChangeEventDate() {
		return householdChangeEventDate;
	}

	public void setHouseholdChangeEventDate(String householdChangeEventDate) {
		this.householdChangeEventDate = householdChangeEventDate;
	}

	public boolean isApplicationEventExists() {
		return applicationEventExists;
	}

	public void setApplicationEventExists(boolean applicationEventExists) {
		this.applicationEventExists = applicationEventExists;
	}

	public String getHouseholdDisplayed() {
		return householdDisplayed;
	}

	public void setHouseholdDisplayed(String householdDisplayed) {
		this.householdDisplayed = householdDisplayed;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Map<String, SepEventDTO> getQualifyingEventMap() {
		return qualifyingEventMap;
	}

	public void setQualifyingEventMap(Map<String, SepEventDTO> qualifyingEventMap) {
		this.qualifyingEventMap = qualifyingEventMap;
	}

	public String getQualifyEventSelected() {
		return qualifyEventSelected;
	}

	public void setQualifyEventSelected(String qualifyEventSelected) {
		this.qualifyEventSelected = qualifyEventSelected;
	}

	public String getQualifyEventDate() {
		return qualifyEventDate;
	}

	public void setQualifyEventDate(String qualifyEventDate) {
		this.qualifyEventDate = qualifyEventDate;
	}

	public String getQualifyEventAllowFuture() {
		return qualifyEventAllowFuture;
	}

	public void setQualifyEventAllowFuture(String qualifyEventAllowFuture) {
		this.qualifyEventAllowFuture = qualifyEventAllowFuture;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LceActivityDTO [addedMembersMap=").append(addedMembersMap).append(", removedMembersMap=").append(removedMembersMap).append(", householdChangeMembersMap=").append(householdChangeMembersMap)
		        .append(", changeOfAddressMembersMap=").append(changeOfAddressMembersMap).append(", changeOfDobMembersMap=").append(changeOfDobMembersMap).append(", noChangeMembersMap=").append(noChangeMembersMap).append(", qualifyingEventMap=")
		        .append(qualifyingEventMap).append(", householdChangeReason=").append(householdChangeReason).append(", householdChangeEventDate=").append(householdChangeEventDate).append(", referralActivationId=").append(referralActivationId)
		        .append(", ssapApplicationId=").append(ssapApplicationId).append(", zipCountyChangeEventDate=").append(zipCountyChangeEventDate).append(", userId=").append(userId).append(", applicationEventExists=").append(applicationEventExists)
		        .append(", householdDisplayed=").append(householdDisplayed).append(", caseNumber=").append(caseNumber).append(", qualifyEventSelected=").append(qualifyEventSelected).append(", qualifyEventDate=").append(qualifyEventDate)
		        .append(", qualifyEventAllowFuture=").append(qualifyEventAllowFuture).append("]");
		return builder.toString();
	}

}
