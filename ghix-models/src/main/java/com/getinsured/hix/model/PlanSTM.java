package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

@Audited
@Entity
@Table(name = "PLAN_STM")
public class PlanSTM implements Serializable {

	private static final long serialVersionUID = -5307740056365883357L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlanSTM_Seq")
	@SequenceGenerator(name = "PlanSTM_Seq", sequenceName = "PLAN_STM_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;

	@Column(name = "PLAN_RESP_NAME")
	private String planResponseName;

	@Column(name = "MAX_DURATION")
	private Integer maxDuration;

	@Column(name = "DURATION_UNIT")
	private String durationUnit;

	@Column(name = "COINSURANCE")
	private Integer coinsurance;

	@Column(name = "COINSURANCE_ATTR")
	private String coinsuranceAttr;

	@Column(name = "POLICY_MAX")
	private Integer policyMax;

	@Column(name = "POLICY_MAX_ATTR")
	private String policyMaxAttr;

	@Column(name = "OUT_OF_NETWORK_COVERAGE")
	private String outOfNetworkCoverage;

	@Column(name = "OUT_OF_COUNTRY_COVERAGE")
	private String outOfCountryCoverage;

	@Column(name = "ADDITIONAL_INFO")
	private String additionalInfo;

	@Column(name = "RATING")
	private String rating;

	@Column(name = "ELECT_SIGN_APP_AVBL")
	private String electSignAppAvbl;

	@Column(name = "APP_FEE")
	private Float applicationFee;

	@Column(name = "EXCLUSIONS")
	private String exclusions;

	@Column(name = "OOP_MAX_VAL")
	private Integer oopMaxValue;

	@Column(name = "OOP_MAX_ATTR")
	private String oopMaxAttr;

	@Column(name = "OOP_MAX_FAMILY")
	private String oopMaxFmly;

	@Column(name = "TERM_MAXIMUM")
	private String termMaximum;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public PlanSTM() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public String getPlanResponseName() {
		return planResponseName;
	}

	public void setPlanResponseName(String planResponseName) {
		this.planResponseName = planResponseName;
	}

	public Integer getMaxDuration() {
		return maxDuration;
	}

	public void setMaxDuration(Integer maxDuration) {
		this.maxDuration = maxDuration;
	}

	public String getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public Integer getCoinsurance() {
		return coinsurance;
	}

	public void setCoinsurance(Integer coinsurance) {
		this.coinsurance = coinsurance;
	}

	public String getCoinsuranceAttr() {
		return coinsuranceAttr;
	}

	public void setCoinsuranceAttr(String coinsuranceAttr) {
		this.coinsuranceAttr = coinsuranceAttr;
	}

	public Integer getPolicyMax() {
		return policyMax;
	}

	public void setPolicyMax(Integer policyMax) {
		this.policyMax = policyMax;
	}

	public String getPolicyMaxAttr() {
		return policyMaxAttr;
	}

	public void setPolicyMaxAttr(String policyMaxAttr) {
		this.policyMaxAttr = policyMaxAttr;
	}

	public String getOutOfNetworkCoverage() {
		return outOfNetworkCoverage;
	}

	public void setOutOfNetworkCoverage(String outOfNetworkCoverage) {
		this.outOfNetworkCoverage = outOfNetworkCoverage;
	}

	public String getOutOfCountryCoverage() {
		return outOfCountryCoverage;
	}

	public void setOutOfCountryCoverage(String outOfCountryCoverage) {
		this.outOfCountryCoverage = outOfCountryCoverage;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getElectSignAppAvbl() {
		return electSignAppAvbl;
	}

	public void setElectSignAppAvbl(String electSignAppAvbl) {
		this.electSignAppAvbl = electSignAppAvbl;
	}

	public Float getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(Float applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getExclusions() {
		return exclusions;
	}

	public void setExclusions(String exclusions) {
		this.exclusions = exclusions;
	}

	public Integer getOopMaxValue() {
		return oopMaxValue;
	}

	public void setOopMaxValue(Integer oopMaxValue) {
		this.oopMaxValue = oopMaxValue;
	}

	public String getOopMaxAttr() {
		return oopMaxAttr;
	}

	public void setOopMaxAttr(String oopMaxAttr) {
		this.oopMaxAttr = oopMaxAttr;
	}

	public String getOopMaxFmly() {
		return oopMaxFmly;
	}

	public void setOopMaxFmly(String oopMaxFmly) {
		this.oopMaxFmly = oopMaxFmly;
	}

	public String getTermMaximum() {
		return termMaximum;
	}

	public void setTermMaximum(String termMaximum) {
		this.termMaximum = termMaximum;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
