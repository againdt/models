package com.getinsured.hix.dto.cap.consumerapp;

import java.io.Serializable;
import java.util.List;

public class EligLeadDTO implements Serializable {
    private static final long serialVersionUID = -7357860569227277657L;

    private long id;
    private String name;
    private String emailAddress;
    private String exchangeType;
    private String phoneNumber;
    private String status;
    private String zipcode;
    private String countyCode;
    private String errorMessage;
    private List<String> leadsPhoneList;
    //HIX-87566:	Enable Click-to-Dial for Appointment Scheduler
    private String click2CallURL;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getClick2CallURL() {
        return click2CallURL;
    }

    public void setClick2CallURL(String click2CallURL) {
        this.click2CallURL = click2CallURL;
    }

	public List<String> getLeadsPhone() {
		return leadsPhoneList;
	}

	public void setLeadsPhone(List<String> leadsPhone) {
		this.leadsPhoneList = leadsPhone;
	}
}
