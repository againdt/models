
package com.getinsured.enrollment._1095.cy_2018.treasury.irs.ext.aca.air.ty18a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;Component xmlns="urn:us:gov:treasury:irs:ext:aca:air:ty18a" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;
 * 					&lt;DescriptionTxt&gt;Employer Detail&lt;/DescriptionTxt&gt;
 * 				&lt;/Component&gt;
 * </pre>
 * 
 * 			
 * 
 * <p>Java class for EmployeeDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployeeDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployeeName"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:common}EmployeeSSN"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}TINTypeCd"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployeeTINValidateNameResponseDetail"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}EmployeeMailingAddress"/>
 *         &lt;element ref="{urn:us:gov:treasury:irs:ext:aca:air:ty18a}ErrorDetail" maxOccurs="99" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployeeDetailType", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", propOrder = {
    "employeeName",
    "employeeSSN",
    "tinTypeCd",
    "employeeTINValidateNameResponseDetail",
    "employeeMailingAddress",
    "errorDetail"
})
public class EmployeeDetailType {

    @XmlElement(name = "EmployeeName", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected CompletePersonNameDetailType employeeName;
    @XmlElement(name = "EmployeeSSN", namespace = "urn:us:gov:treasury:irs:common", required = true)
    protected String employeeSSN;
    @XmlElement(name = "TINTypeCd", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected String tinTypeCd;
    @XmlElement(name = "EmployeeTINValidateNameResponseDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected TINValidateNameResponseDetailType employeeTINValidateNameResponseDetail;
    @XmlElement(name = "EmployeeMailingAddress", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a", required = true)
    protected MailingAddressGrpType employeeMailingAddress;
    @XmlElement(name = "ErrorDetail", namespace = "urn:us:gov:treasury:irs:ext:aca:air:ty18a")
    protected List<ErrorDetailType> errorDetail;

    /**
     * Gets the value of the employeeName property.
     * 
     * @return
     *     possible object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public CompletePersonNameDetailType getEmployeeName() {
        return employeeName;
    }

    /**
     * Sets the value of the employeeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompletePersonNameDetailType }
     *     
     */
    public void setEmployeeName(CompletePersonNameDetailType value) {
        this.employeeName = value;
    }

    /**
     * Gets the value of the employeeSSN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeSSN() {
        return employeeSSN;
    }

    /**
     * Sets the value of the employeeSSN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeSSN(String value) {
        this.employeeSSN = value;
    }

    /**
     * Gets the value of the tinTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTINTypeCd() {
        return tinTypeCd;
    }

    /**
     * Sets the value of the tinTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTINTypeCd(String value) {
        this.tinTypeCd = value;
    }

    /**
     * Gets the value of the employeeTINValidateNameResponseDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public TINValidateNameResponseDetailType getEmployeeTINValidateNameResponseDetail() {
        return employeeTINValidateNameResponseDetail;
    }

    /**
     * Sets the value of the employeeTINValidateNameResponseDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TINValidateNameResponseDetailType }
     *     
     */
    public void setEmployeeTINValidateNameResponseDetail(TINValidateNameResponseDetailType value) {
        this.employeeTINValidateNameResponseDetail = value;
    }

    /**
     * Gets the value of the employeeMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public MailingAddressGrpType getEmployeeMailingAddress() {
        return employeeMailingAddress;
    }

    /**
     * Sets the value of the employeeMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailingAddressGrpType }
     *     
     */
    public void setEmployeeMailingAddress(MailingAddressGrpType value) {
        this.employeeMailingAddress = value;
    }

    /**
     * Gets the value of the errorDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetailType }
     * 
     * 
     */
    public List<ErrorDetailType> getErrorDetail() {
        if (errorDetail == null) {
            errorDetail = new ArrayList<ErrorDetailType>();
        }
        return this.errorDetail;
    }

}
