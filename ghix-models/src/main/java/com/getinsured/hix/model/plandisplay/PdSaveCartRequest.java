package com.getinsured.hix.model.plandisplay;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;

public class PdSaveCartRequest {
	
	private Long householdId;
	private InsuranceType insuranceType;
	private Long subscriberId;
	private Long planId;
	private Float premiumBeforeCredit;
	private Float premiumAfterCredit;
	private Float monthlySubsidy;
	private List<Map<String,String>> premiumData; 
	private Map<String,Float> subsidyData;
	private Long ssapApplicationId;
	private String requestType;
	private List<PdBundlePlanData> pdBundlePlanDataList;
	private ExchangeType exchangeType;
	private PlanPreferences planPreferences;
	private BigDecimal monthlyStateSubsidy;
	
	public Long getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}
	
	public InsuranceType getInsuranceType() {
		return insuranceType;
	}
	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}
	
	public Long getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(Long subscriberId) {
		this.subscriberId = subscriberId;
	}
	
	public Long getPlanId() {
		return planId;
	}
	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	
	public Float getPremiumBeforeCredit() {
		return premiumBeforeCredit;
	}
	public void setPremiumBeforeCredit(Float premiumBeforeCredit) {
		this.premiumBeforeCredit = premiumBeforeCredit;
	}
	
	public Float getPremiumAfterCredit() {
		return premiumAfterCredit;
	}
	public void setPremiumAfterCredit(Float premiumAfterCredit) {
		this.premiumAfterCredit = premiumAfterCredit;
	}
	
	public Float getMonthlySubsidy() {
		return monthlySubsidy;
	}
	public void setMonthlySubsidy(Float monthlySubsidy) {
		this.monthlySubsidy = monthlySubsidy;
	}
	
	public List<Map<String, String>> getPremiumData() {
		return premiumData;
	}
	public void setPremiumData(List<Map<String, String>> premiumData) {
		this.premiumData = premiumData;
	}
	
	public Map<String, Float> getSubsidyData() {
		return subsidyData;
	}
	public void setSubsidyData(Map<String, Float> subsidyData) {
		this.subsidyData = subsidyData;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public List<PdBundlePlanData> getPdBundlePlanDataList() {
		return pdBundlePlanDataList;
	}
	public void setPdBundlePlanDataList(List<PdBundlePlanData> pdBundlePlanDataList) {
		this.pdBundlePlanDataList = pdBundlePlanDataList;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public ExchangeType getExchangeType() {
		return exchangeType;
	}
	public void setExchangeType(ExchangeType exchangeType) {
		this.exchangeType = exchangeType;
	}
	public PlanPreferences getPlanPreferences() {
		return planPreferences;
	}
	public void setPlanPreferences(PlanPreferences planPreferences) {
		this.planPreferences = planPreferences;
	}

	public BigDecimal getMonthlyStateSubsidy() {
		return monthlyStateSubsidy;
	}

	public void setMonthlyStateSubsidy(BigDecimal monthlyStateSubsidy) {
		this.monthlyStateSubsidy = monthlyStateSubsidy;
	}
}
