/**
 * 
 */
package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


public class PldHousehold implements Serializable{

	private static final long serialVersionUID = 1L;
	public enum HouseholdType {
		HOUSEHOLD, INDIVIDUAL, GROUP;
	}
	public enum ProgramType {
		SHOP, INDIVIDUAL;
	}
	
	public enum Subsidized {
		YES, NO;
	}
	public enum ProductCategoryEnum {
		H, M;
	}
	
	private int id;
	
	private String externalId;
	
	private Character enrollmentType;
	
	private Float monthlyAptc;
	
	private Float yearlyAptc;
	
	private String costSharing;
	
	@Enumerated(EnumType.STRING)
	private HouseholdType shoppingGroup;
	
	private Date coverageStart;
	
	private Date financialEffectiveDate;
	
	@Enumerated(EnumType.STRING)
	private ProgramType programType;
	
	private String shoppingId;
	
	private String externalEmployerId;
	
	private String householdData;
	
	private int createdBy;
    
    private int updatedBy;
    
	private Date created;

	private Date updated;
    
	@Enumerated(EnumType.STRING)
    private Subsidized isSubsidized;
    
	private Integer employerId;
    
	private String requestType;
    
	private Long eligLeadId;

	private Integer cmrHouseholdId;
    
	private String stateExchangeCode;
    
    private Long ssapApplicationId;
    
	private Character keepOnly;

	private Character autoRenewal;
    
    private String healthSubscriberId;    

    private String dentalSubscriberId;    
    
	@Enumerated(EnumType.STRING)
    private ProductCategoryEnum productCategory; 
	
	private String renewalPlanId;
	
	public PldHousehold(){
    	monthlyAptc = new Float(0);
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Character getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(Character enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public Float getMonthlyAptc() {
		return monthlyAptc;
	}

	public void setMonthlyAptc(Float monthlyAptc) {
		this.monthlyAptc = monthlyAptc;
	}

	public Float getYearlyAptc() {
		return yearlyAptc;
	}

	public void setYearlyAptc(Float yearlyAptc) {
		this.yearlyAptc = yearlyAptc;
	}

	public String getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}

	public HouseholdType getShoppingGroup() {
		return shoppingGroup;
	}

	public void setShoppingGroup(HouseholdType shoppingGroup) {
		this.shoppingGroup = shoppingGroup;
	}
	
	public Date getCoverageStart() {
		return coverageStart;
	}

	public void setCoverageStart(Date coverageStart) {
		this.coverageStart = coverageStart;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public String getShoppingId() {
		return shoppingId;
	}

	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}
	
	public String getHouseholdData() {
		return householdData;
	}

	public void setHouseholdData(String householdData) {
		this.householdData = householdData;
	}
	
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getExternalEmployerId() {
		return externalEmployerId;
	}

	public void setExternalEmployerId(String externalEmployerId) {
		this.externalEmployerId = externalEmployerId;
	}

    public Subsidized getIsSubsidized() {
		return isSubsidized;
	}

	public void setIsSubsidized(Subsidized isSubsidized) {
		this.isSubsidized = isSubsidized;
	}

	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
    
	public Long getEligLeadId() {
		return eligLeadId;
	}

	public void setEligLeadId(Long eligLeadId) {
		this.eligLeadId = eligLeadId;
	}

	public Integer getCmrHouseholdId() {
		return cmrHouseholdId;
	}

	public void setCmrHouseholdId(Integer cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}

	public String getStateExchangeCode() {
		return stateExchangeCode;
	}
		
	public void setStateExchangeCode(String stateExchangeCode) {
		this.stateExchangeCode = stateExchangeCode;
	}

	public Character getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(Character keepOnly) {
		this.keepOnly = keepOnly;
	}

	public Character getAutoRenewal() {
		return autoRenewal;
	}

	public void setAutoRenewal(Character autoRenewal) {
		this.autoRenewal = autoRenewal;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getHealthSubscriberId() {
		return healthSubscriberId;
	}

	public void setHealthSubscriberId(String healthSubscriberId) {
		this.healthSubscriberId = healthSubscriberId;
	}

	public String getDentalSubscriberId() {
		return dentalSubscriberId;
	}

	public void setDentalSubscriberId(String dentalSubscriberId) {
		this.dentalSubscriberId = dentalSubscriberId;
	}

	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}

	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}

	public ProductCategoryEnum getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategoryEnum productCategory) {
		this.productCategory = productCategory;
	}

	public String getRenewalPlanId() {
		return renewalPlanId;
	}

	public void setRenewalPlanId(String renewalPlanId) {
		this.renewalPlanId = renewalPlanId;
	}
	
}
