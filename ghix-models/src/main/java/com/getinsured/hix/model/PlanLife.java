package com.getinsured.hix.model;

import java.io.Serializable;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * -----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life Insurance plans.
 * -----------------------------------------------------------------------------
 * 
 * This is POJO class for PLAN_LIFE database table.
 * 
 * @author Bhavin Parmar
 * @since April 28, 2016
 * 
 */
@Audited
@Entity
@Table(name = "PLAN_LIFE")
public class PlanLife implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAN_LIFE_SEQ")
	@SequenceGenerator(name = "PLAN_LIFE_SEQ", sequenceName = "PLAN_LIFE_SEQ", allocationSize = 1)
	private int id;

	@ManyToOne
	@JoinColumn(name = "PLAN_ID")
	private Plan plan;

	@Column(name = "COVERAGE_MIN")
	private Integer coverageMin;

	@Column(name = "COVERAGE_MAX")
	private Integer coverageMax;

	@Column(name = "ANNUAL_POLICY_FEE")
	private Double annualPolicyFee;

	@Column(name = "PLAN_DURATION")
	private Integer planDuration;

	@Column(name = "BENEFIT_URL")
	private String benefitURL;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "CREATION_TIMESTAMP", nullable = false)
	private Date creationTimestamp;

	@NotAudited
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_TIMESTAMP", nullable = false)
	private Date lastUpdateTimestamp;

	@Column(name = "LAST_UPDATED_BY")
	private Integer lastUpdatedBy;

	public PlanLife() {
	}

	/* To AutoUpdate created and updated dates while persisting object */
	@PrePersist
	public void prePersist() {
		this.setCreationTimestamp(new TSDate());
		this.setLastUpdateTimestamp(new TSDate());
	}

	/* To AutoUpdate updated dates while updating object */
	@PreUpdate
	public void preUpdate() {
		this.setLastUpdateTimestamp(new TSDate());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Integer getCoverageMin() {
		return coverageMin;
	}

	public void setCoverageMin(Integer coverageMin) {
		this.coverageMin = coverageMin;
	}

	public Integer getCoverageMax() {
		return coverageMax;
	}

	public void setCoverageMax(Integer coverageMax) {
		this.coverageMax = coverageMax;
	}

	public Double getAnnualPolicyFee() {
		return annualPolicyFee;
	}

	public void setAnnualPolicyFee(Double annualPolicyFee) {
		this.annualPolicyFee = annualPolicyFee;
	}

	public Integer getPlanDuration() {
		return planDuration;
	}

	public void setPlanDuration(Integer planDuration) {
		this.planDuration = planDuration;
	}

	public String getBenefitURL() {
		return benefitURL;
	}

	public void setBenefitURL(String benefitURL) {
		this.benefitURL = benefitURL;
	}

	public Date getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}
