package com.getinsured.hix.dto.enrollment;

import java.util.List;

import com.getinsured.hix.model.enrollment.Enrollment;

/**
 * 
 * @author Aditya_S
 * @since 26-05-2015
 * HIX-68691
 *
 */
public class EnrollmentCoreDto extends Enrollment{
	private static final long serialVersionUID = 1654784140830142871L;
	
	private List<EnrolleeCoreDto> enrolleeCoreDtoList;
	
	/* Lookup Related String -- LookupValue Codes*/
	private String maintainenceReasonCodeStr;
	
	private String sercStr;

	private String insuranceTypeStr;//Health/Dental
	
	
	public String getInsuranceTypeStr() {
		return insuranceTypeStr;
	}


	public void setInsuranceTypeStr(String insuranceTypeStr) {
		this.insuranceTypeStr = insuranceTypeStr;
	}


	public String getSercStr() {
		return sercStr;
	}


	public void setSercStr(String sercStr) {
		this.sercStr = sercStr;
	}


	/**
	 * @return the enrolleeCoreDtoList
	 */
	public List<EnrolleeCoreDto> getEnrolleeCoreDtoList() {
		return enrolleeCoreDtoList;
	}


	/**
	 * @param enrolleeCoreDtoList the enrolleeCoreDtoList to set
	 */
	public void setEnrolleeCoreDtoList(List<EnrolleeCoreDto> enrolleeCoreDtoList) {
		this.enrolleeCoreDtoList = enrolleeCoreDtoList;
	}


	/**
	 * @return the maintainenceReasonCodeStr
	 */
	public String getMaintainenceReasonCodeStr() {
		return maintainenceReasonCodeStr;
	}


	/**
	 * @param maintainenceReasonCodeStr the maintainenceReasonCodeStr to set
	 */
	public void setMaintainenceReasonCodeStr(String maintainenceReasonCodeStr) {
		this.maintainenceReasonCodeStr = maintainenceReasonCodeStr;
	}

}
