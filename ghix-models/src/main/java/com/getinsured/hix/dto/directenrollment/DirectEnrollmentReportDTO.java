package com.getinsured.hix.dto.directenrollment;

import java.util.Date;

import com.getinsured.hix.model.directenrollment.DirectEnrollment;
import com.getinsured.hix.model.directenrollment.DirectEnrollmentStatistic;

/**
 * This is for read only reporting work
 * @author root
 *
 */
public class DirectEnrollmentReportDTO extends DirectEnrollmentDTO {
	private static final long serialVersionUID = 1L;
	private Date creationTimestamp;
	private Date lastUpdatedTimestamp;
	
	public DirectEnrollmentReportDTO(DirectEnrollment enrollment){
		super(enrollment);
	}
	public DirectEnrollmentReportDTO(DirectEnrollmentStatistic enrollment){
		super(enrollment);
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}
	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}
	
}
