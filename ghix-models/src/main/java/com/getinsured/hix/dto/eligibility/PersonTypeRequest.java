package com.getinsured.hix.dto.eligibility;

import java.io.Serializable;
import java.util.List;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.google.gson.Gson;

/**
 * DTO class is used to send request for PersonType API.
 */
public class PersonTypeRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String primaryApplicantId; 
	private String primaryTaxFilerId;
	private List<PersonTypeDTO> list;

	public PersonTypeRequest() {
	}

	@Override
	public String toString() {
		Gson platformGson = (Gson) GHIXApplicationContext.getBean("platformGson");
		return platformGson.toJson(this);
	}

	public List<PersonTypeDTO> getList() {
		return list;
	}

	public void setList(List<PersonTypeDTO> list) {
		this.list = list;
	}

	public String getPrimaryApplicantId() {
		return primaryApplicantId;
	}

	public void setPrimaryApplicantId(String primaryApplicantId) {
		this.primaryApplicantId = primaryApplicantId;
	}

	public String getPrimaryTaxFilerId() {
		return primaryTaxFilerId;
	}

	public void setPrimaryTaxFilerId(String primaryTaxFilerId) {
		this.primaryTaxFilerId = primaryTaxFilerId;
	}
	
}
