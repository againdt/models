/**
 * 
 */
package com.getinsured.hix.model.estimator.mini;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class EmployeeMember {
	
	private int memberNumber;
	private String externalMemberId; 
	private String firstName;
	private String lastName;
	private MemberRelationships relationshipToPrimary;
	private String dateOfBirth;
	private String gender;
    private String tobaccoUsage="N";
	private String seekingCoverage = "Y";
	private Double individualHraAmount; 
	private String isNativeAmerican = "N"; 
	
	
   // To make it consistent with the current screener workflows. 
   // API can continue to accept the  seekingCoverage and tobaccoUsage, under the hood it will be 
   // written as isSeekingCoverage and isTobaccoUser so that current flows can work as-is

	public String isSeekingCoverage = "Y";
	public String isTobaccoUser = "N";
	
	private List<String> seekingCoverageProducts = new ArrayList<String>();
	
	public String getExternalMemberId() {
		return externalMemberId;
	}

	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getIndividualHraAmount() {
		return individualHraAmount;
	}

	public void setIndividualHraAmount(Double individualHraAmount) {
		this.individualHraAmount = individualHraAmount;
	}

	public int getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(int memberNumber) {
		this.memberNumber = memberNumber;
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	
	
	

	public MemberRelationships getRelationshipToPrimary() {
		return relationshipToPrimary;
	}

	public void setRelationshipToPrimary(MemberRelationships relationshipToPrimary) {
		this.relationshipToPrimary = relationshipToPrimary;
	}
	
	public String getIsNativeAmerican() {
		return isNativeAmerican;
	}

	public void setIsNativeAmerican(String isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FamilyMember [memberNumber=");
		builder.append(memberNumber);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", isTobaccoUser=");
		builder.append(tobaccoUsage);
		
		builder.append(",isNativeAmerican=");
		builder.append(isNativeAmerican);
		builder.append(", relationshipToPrimary=").append(relationshipToPrimary);
		builder.append("]");
		return builder.toString();
	}
	public String getTobaccoUsage() {
		return tobaccoUsage;
	}

	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
		this.isTobaccoUser=tobaccoUsage;
	}

	public String getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(String seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
		this.isSeekingCoverage=seekingCoverage;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	public class MemberComparatorByMemberNumber implements Comparator<EmployeeMember>{
		@Override
		public int compare(EmployeeMember memberObj1, EmployeeMember memberObj2) {
			return memberObj1.getMemberNumber() < memberObj2.getMemberNumber() ? -1 : (memberObj1.getMemberNumber() == memberObj2.getMemberNumber() ? 0 : 1) ;
		}

	}

		public List<String> getSeekingCoverageProducts() {
		return seekingCoverageProducts;
	}

	public void setSeekingCoverageProducts(List<String> seekingCoverageProducts) {
		this.seekingCoverageProducts = seekingCoverageProducts;
	}
	
	
	
	
	
}
