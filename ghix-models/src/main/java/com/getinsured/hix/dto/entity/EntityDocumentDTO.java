package com.getinsured.hix.dto.entity;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Used to wrap data fetched from database and send it to calling methods
 * 
 */
public class EntityDocumentDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer assisterPhotoDocumentId;
	private byte[] assisterPhoto;
	private String mimeType;

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	public Integer getAssisterPhotoDocumentId() {
		return assisterPhotoDocumentId;
	}
	public void setAssisterPhotoDocumentId(Integer assisterPhotoDocumentId) {
		this.assisterPhotoDocumentId = assisterPhotoDocumentId;
	}
	public byte[] getAssisterPhoto() {
		return assisterPhoto;
	}
	public void setAssisterPhoto(byte[] assisterPhoto1) {
		if (assisterPhoto1 == null) {
			this.assisterPhoto = new byte[0];
		} else {
			this.assisterPhoto = Arrays.copyOf(assisterPhoto1, assisterPhoto1.length);
		}
	}

}
